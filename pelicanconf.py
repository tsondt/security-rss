#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from functools import partial

AUTHOR = 'tsondt'
SITENAME = 'Security Feed'
SITESUBTITLE = 'Standing on the shoulders of giants'
# SITEURL = ''

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 25

ARTICLE_URL = '{date:%Y}/{date:%b}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%b}/{date:%d}/{slug}/index.html'

# global metadata to all the contents
DEFAULT_METADATA = {}

# path-specific metadata
EXTRA_PATH_METADATA = {
    'extra/android-chrome-96x96.png': {'path': 'android-chrome-96x96.png'},
    'extra/apple-touch-icon.png': {'path': 'apple-touch-icon.png'},
    'extra/browserconfig.xml': {'path': 'browserconfig.xml'},
    'extra/favicon-16x16.png': {'path': 'favicon-16x16.png'},
    'extra/favicon-32x32.png': {'path': 'favicon-32x32.png'},
    'extra/favicon.ico': {'path': 'favicon.ico'},
    'extra/mstile-150x150.png': {'path': 'mstile-150x150.png'},
    'extra/robots.txt': {'path': 'robots.txt'},
    'extra/safari-pinned-tab.svg': {'path': 'safari-pinned-tab.svg'},
    'extra/site.webmanifest': {'path': 'site.webmanifest'},
    'extra/404.html': {'path': '404.html'}
}

# static paths will be copied without parsing their contents
STATIC_PATHS = [
    'images',
    'extra/android-chrome-96x96.png',
    'extra/apple-touch-icon.png',
    'extra/browserconfig.xml',
    'extra/favicon-16x16.png',
    'extra/favicon-32x32.png',
    'extra/favicon.ico',
    'extra/mstile-150x150.png',
    'extra/robots.txt',
    'extra/safari-pinned-tab.svg',
    'extra/site.webmanifest',
    'extra/404.html'
]

READERS = {
    'html': None
}

# Theme
THEME = 'm.css/pelican-theme'
THEME_STATIC_DIR = 'static'
DIRECT_TEMPLATES = ['index', 'archives', 'tags']
PAGINATED_TEMPLATES = {
    'archives': 50,
    'tag': 25,
    'category': 25,
    'author': 25
}

# Plugin
PLUGIN_PATHS = [
    'plugins',
    'm.css/plugins'
]
PLUGINS = [
    'assets',
    'm.htmlsanity',
    'pelican.plugins.sitemap'
]

# Assets
ASSET_SOURCE_PATHS = [
    'scss'
]
ASSET_BUNDLES = (
    ('main-dark', ['m-dark.scss'], { 'filters': 'scss' }),
    ('main-light', ['m-light.scss'], { 'filters': 'scss' }),
)

# Sitemap
SITEMAP = {
    "format": "xml",
    "priorities": {
        "articles": 1,
        "indexes": 1,
        "pages": 1
    },
    "changefreqs": {
        "articles": "daily",
        "indexes": "daily",
        "pages": "monthly"
    }
}

# Custom Jinja filters
JINJA_FILTERS= {
    'sort_by_article_count': partial(
        sorted,
        key=lambda tags: len(tags[1]),
        reverse=True)
}

# m.css theme configuration
M_CSS_FILES = []
M_DARK_THEME = True
M_THEME_COLOR = '#22272e'

# M_BLOG_NAME = ''
# M_BLOG_URL = ''
M_BLOG_DESCRIPTION = 'Standing on the shoulders of giants - Security Feed Aggregation'

M_DISABLE_SOCIAL_META_TAGS = True
# M_SOCIAL_SITE_NAME =
# M_SOCIAL_IMAGE =
# M_SOCIAL_BLOG_SUMMARY =
# M_SOCIAL_TWITTER_SITE =
# M_SOCIAL_TWITTER_SITE_ID =


M_LINKS_NAVBAR1 = [('Archives', 'archives/', 'archives', [])]

# M_LINKS_NAVBAR2 = []

M_HIDE_ARTICLE_SUMMARY = True

M_FINE_PRINT = (f"{SITENAME}"
                ". Powered by `Pelican <https://getpelican.com>`_"
                " and `m.css <https://mcss.mosra.cz>`_."
                " Code is available on "
                "`GitLab <https://gitlab.com/tsondt/security-rss>`_.")

FORMATTED_FIELDS = ['summary', 'landing', 'header', 'footer']

M_NEWS_ON_INDEX = ("Latest news", 10)
