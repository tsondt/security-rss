Title: Fashion titan French Connection says 'FCUK' as REvil-linked ransomware makes off with data
Date: 2021-06-24T08:30:12+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: fashion-titan-french-connection-says-fcuk-as-revil-linked-ransomware-makes-off-with-data

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/24/french_connection_says_fcuk_as/){:target="_blank" rel="noopener"}

> Attack on an internal system shouldn't put customers at risk, biz claims Cheeky clothing firm French Connection, also known as FCUK, has become the latest victim of ransomware, with a gang understood to be linked to REvil having penetrated its back-end - making off with a selection of private internal data.... [...]
