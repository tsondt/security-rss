Title: Intel fixes 73 vulnerabilities in June 2021 Platform Update
Date: 2021-06-08T15:18:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: intel-fixes-73-vulnerabilities-in-june-2021-platform-update

[Source](https://www.bleepingcomputer.com/news/security/intel-fixes-73-vulnerabilities-in-june-2021-platform-update/){:target="_blank" rel="noopener"}

> Intel has addressed 73 security vulnerabilities as part of the June 2021 Patch Tuesday, including high severity ones impacting some versions of Intel's Security Library and the BIOS firmware for Intel processors. [...]
