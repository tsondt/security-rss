Title: Windows 11 includes the DNS-over-HTTPS privacy feature - How to use
Date: 2021-06-29T14:00:47-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: windows-11-includes-the-dns-over-https-privacy-feature-how-to-use

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-11-includes-the-dns-over-https-privacy-feature-how-to-use/){:target="_blank" rel="noopener"}

> Microsoft has added a privacy feature to Windows 11 called DNS-over-HTTPS, allowing users to perform encrypted DNS lookups to bypass censorship and Internet activity. [...]
