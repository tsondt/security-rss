Title: UK Cabinet Office's spending on cybersecurity training rises by 500% in a year
Date: 2021-06-29T15:45:10+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: uk-cabinet-offices-spending-on-cybersecurity-training-rises-by-500-in-a-year

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/29/cabinet_office_cybersecurity_training/){:target="_blank" rel="noopener"}

> No indication any of it went on preventing theft of CCTV footage, though, eh Matt? The Cabinet Office spaffed almost £300,000 on cybersecurity-related training for its staff in the last year – an eye-popping increase of almost 500 per cent on the year before.... [...]
