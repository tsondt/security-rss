Title: I think therefore IAM: It's not cool, it's not sexy, but it's one of the most important and difficult areas in modern IT
Date: 2021-06-08T08:30:06+00:00
Author: Dave Cartwright
Category: The Register
Tags: 
Slug: i-think-therefore-iam-its-not-cool-its-not-sexy-but-its-one-of-the-most-important-and-difficult-areas-in-modern-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/08/iam/){:target="_blank" rel="noopener"}

> When I grow up, I want to be an Identity and Access Management specialist – said no one ever Feature A search on LinkedIn's UK job site just now (1 June 2021) returned 5,265 roles for a network manager; 2,204 for a system administrator; 4,964 for a web developer; and 10,776 for a business analyst. None of these are a particular surprise – they're popular, sought-after careers.... [...]
