Title: Zephyr RTOS fixes Bluetooth bugs that may lead to code execution
Date: 2021-06-22T14:03:21-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: zephyr-rtos-fixes-bluetooth-bugs-that-may-lead-to-code-execution

[Source](https://www.bleepingcomputer.com/news/security/zephyr-rtos-fixes-bluetooth-bugs-that-may-lead-to-code-execution/){:target="_blank" rel="noopener"}

> The Zephyr real-time operating system (RTOS) for embedded devices received an update earlier this month that fixes multiple vulnerabilities that can cause a denial-of-service (DoS) condition and potentially lead to remote code execution. [...]
