Title: Musk-Themed ‘$SpaceX’ Cryptoscam Invades YouTube Advertising
Date: 2021-06-24T15:44:56+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cryptography;Web Security
Slug: musk-themed-spacex-cryptoscam-invades-youtube-advertising

[Source](https://threatpost.com/musk-spacex-cryptoscam-youtube-advertising/167219/){:target="_blank" rel="noopener"}

> Beware: The swindle uses legitimately purchased YouTube ads, real liquidity, legitimate DEX Uniswap, and the real wallet extension MetaMask to create an entirely convincing fake coin gambit. [...]
