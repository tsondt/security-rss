Title: Healthcare vendor Zoll patches high-risk vulnerabilities in defibrillator management software
Date: 2021-06-18T14:10:44+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: healthcare-vendor-zoll-patches-high-risk-vulnerabilities-in-defibrillator-management-software

[Source](https://portswigger.net/daily-swig/healthcare-vendor-zoll-patches-high-risk-vulnerabilities-in-defibrillator-management-software){:target="_blank" rel="noopener"}

> Anonymous tip-off helps improve healthcare security landscape [...]
