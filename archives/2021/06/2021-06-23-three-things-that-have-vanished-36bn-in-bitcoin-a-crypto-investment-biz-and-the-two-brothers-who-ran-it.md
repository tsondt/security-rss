Title: Three things that have vanished: $3.6bn in Bitcoin, a crypto investment biz, and the two brothers who ran it
Date: 2021-06-23T23:47:05+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: three-things-that-have-vanished-36bn-in-bitcoin-a-crypto-investment-biz-and-the-two-brothers-who-ran-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/23/africrypt_bitcoin_disappearance/){:target="_blank" rel="noopener"}

> We got hacked and we'll be right back, duo said... two months ago Up to $3.6bn in Bitcoin has disappeared from a South African cryptocurrency investment outfit as well as the two brothers who ran it.... [...]
