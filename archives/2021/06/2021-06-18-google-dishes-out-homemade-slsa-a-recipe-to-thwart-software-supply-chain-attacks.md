Title: Google dishes out homemade SLSA, a recipe to thwart software supply-chain attacks
Date: 2021-06-18T00:05:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: google-dishes-out-homemade-slsa-a-recipe-to-thwart-software-supply-chain-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/18/google_slsa_supply_chain_rust/){:target="_blank" rel="noopener"}

> Try it with phish'n'chips Google has proposed a framework called SLSA for dealing with supply chain attacks, a security risk exemplified by the recent compromise of the SolarWinds Orion IT monitoring platform.... [...]
