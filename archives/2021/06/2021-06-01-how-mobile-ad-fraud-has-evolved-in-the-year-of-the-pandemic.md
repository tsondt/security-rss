Title: How Mobile Ad Fraud has Evolved in the Year of the Pandemic
Date: 2021-06-01T13:00:06+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Sponsored;Web Security
Slug: how-mobile-ad-fraud-has-evolved-in-the-year-of-the-pandemic

[Source](https://threatpost.com/how-mobile-ad-fraud-has-evolved-in-the-year-of-the-pandemic/166430/){:target="_blank" rel="noopener"}

> Mobile ad fraud has always been a challenge for network operators in all parts of the globe, but the pandemic has made users more vulnerable than ever before due to the sheer amount of time they now spend with their devices. [...]
