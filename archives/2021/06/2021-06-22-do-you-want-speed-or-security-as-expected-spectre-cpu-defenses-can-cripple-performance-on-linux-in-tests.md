Title: Do you want speed or security as expected? Spectre CPU defenses can cripple performance on Linux in tests
Date: 2021-06-22T03:02:40+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: do-you-want-speed-or-security-as-expected-spectre-cpu-defenses-can-cripple-performance-on-linux-in-tests

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/22/spectre_linux_performance_test_analysis/){:target="_blank" rel="noopener"}

> All depends on whether your workload is making a lot of system calls or not The mitigations applied to exorcise Spectre, the family of data-leaking processor vulnerabilities, from computers hinders performance enough that disabling protection for the sake of speed may be preferable for some.... [...]
