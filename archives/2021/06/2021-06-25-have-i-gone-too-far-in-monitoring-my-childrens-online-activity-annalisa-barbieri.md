Title: Have I gone too far in monitoring my children’s online activity? | Annalisa Barbieri
Date: 2021-06-25T14:00:28+00:00
Author: Annalisa Barbieri
Category: The Guardian
Tags: Family;Children's tech;Digital media;Parents and parenting;Technology;Social media;Children;Young people;Life and style;Society;Media;Data and computer security;Computing;Privacy
Slug: have-i-gone-too-far-in-monitoring-my-childrens-online-activity-annalisa-barbieri

[Source](https://www.theguardian.com/lifeandstyle/2021/jun/25/have-i-gone-too-far-in-monitoring-my-childrens-online-activity){:target="_blank" rel="noopener"}

> At this stage, being a parent is more about negotiation and trust, says Annalisa Barbieri. Sit down as a family and talk about it – make rules together I have two children, aged nine and 11. We’ve always limited their tech but just before the pandemic, we bought them tablets to give them access to education, entertainment and their friends. Then I became concerned about their increasing use and placed more limits on screen time. Full disclosure: I am a phone addict. So I introduced a rule where we all put our devices in a box when we aren’t using [...]
