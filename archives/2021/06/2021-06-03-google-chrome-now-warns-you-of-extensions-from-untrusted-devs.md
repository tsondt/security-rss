Title: Google Chrome now warns you of extensions from untrusted devs
Date: 2021-06-03T13:11:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: google-chrome-now-warns-you-of-extensions-from-untrusted-devs

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-now-warns-you-of-extensions-from-untrusted-devs/){:target="_blank" rel="noopener"}

> Google has added new protection capabilities for Enhanced Safe Browsing users in Chrome, warning them when installing untrusted extensions and allowing them to request more in-depth scans of downloaded files. [...]
