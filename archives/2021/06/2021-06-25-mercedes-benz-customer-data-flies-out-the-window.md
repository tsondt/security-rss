Title: Mercedes-Benz Customer Data Flies Out the Window
Date: 2021-06-25T21:31:58+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Web Security
Slug: mercedes-benz-customer-data-flies-out-the-window

[Source](https://threatpost.com/mercedes-benz-customer-data-flies-out-the-window/167302/){:target="_blank" rel="noopener"}

> For over three years, a vendor was recklessly driving the cloud-stored data of luxury-car-owning customers and wannabe buyers. [...]
