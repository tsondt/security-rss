Title: PS3 Players Ban: Latest Victims of Surging Attacks on Gaming Industry
Date: 2021-06-25T21:03:14+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Web Security
Slug: ps3-players-ban-latest-victims-of-surging-attacks-on-gaming-industry

[Source](https://threatpost.com/ps3-players-ban-attacks-gaming/167303/){:target="_blank" rel="noopener"}

> Every Sony PlayStation 3 ID out there was compromised, provoking bans of legit players on the network. [...]
