Title: How Does One Get Hired by a Top Cybercrime Gang?
Date: 2021-06-15T15:41:26+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;alex holden;Alla Witte;DarkSide;Hold Security;trickbot;U.S. Department of Justice
Slug: how-does-one-get-hired-by-a-top-cybercrime-gang

[Source](https://krebsonsecurity.com/2021/06/how-does-one-get-hired-by-a-top-cybercrime-gang/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DOJ) last week announced the arrest of a 55-year-old Latvian woman who’s alleged to have worked as a programmer for Trickbot, a malware-as-a-service platform responsible for infecting millions of computers and seeding many of those systems with ransomware. Just how did a self-employed web site designer and mother of two come to work for one of the world’s most rapacious cybercriminal groups and then leave such an obvious trail of clues indicating her involvement with the gang? This post explores answers to those questions, as well as some of the ways Trickbot and other organized [...]
