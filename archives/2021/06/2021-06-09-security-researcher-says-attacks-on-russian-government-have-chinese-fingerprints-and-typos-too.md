Title: Security researcher says attacks on Russian government have Chinese fingerprints – and typos, too
Date: 2021-06-09T06:30:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: security-researcher-says-attacks-on-russian-government-have-chinese-fingerprints-and-typos-too

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/09/mail_o_malware_maybe_chinese/){:target="_blank" rel="noopener"}

> Malware was too loose to have come from a Western nation, according to Sentinel Labs An advanced persistent threat that Russia found inside government systems was too crude to have been the work of a Western nation, says security researcher Juan Andrés Guerrero-Saade of Sentinel Labs, before suggesting the malware came from a Chinese entity.... [...]
