Title: Ryuk ransomware recovery cost us $8.1m and counting, says Baltimore school authority
Date: 2021-06-16T16:37:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ryuk-ransomware-recovery-cost-us-81m-and-counting-says-baltimore-school-authority

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/16/baltimore_ryuk_ransomware_dollars_8_1m_recovery_cost/){:target="_blank" rel="noopener"}

> Spreadsheet breaks down spend on staving off future badness An organisation whose network was infected by Ryuk ransomware has spent $8.1m over seven months recovering from it – and that’s still not the end of it, according to US news reports.... [...]
