Title: Biden expands Chinese tech and military blocklist to 59 companies
Date: 2021-06-04T19:24:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: biden-expands-chinese-tech-and-military-blocklist-to-59-companies

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/04/biden_expands_blocklist/){:target="_blank" rel="noopener"}

> US president calls China’s military-industrial complex 'a threat' and condemns surveillance technology for human rights abuse US president Joe Biden has issued an executive order to expand the Trump-era ban preventing Chinese tech and defence companies from receiving American investment, upping it from 31 to 59 named entities.... [...]
