Title: Audi, Volkswagen data breach affects 3.3 million customers
Date: 2021-06-12T12:27:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: audi-volkswagen-data-breach-affects-33-million-customers

[Source](https://www.bleepingcomputer.com/news/security/audi-volkswagen-data-breach-affects-33-million-customers/){:target="_blank" rel="noopener"}

> Audi and Volkswagen have suffered a data breach affecting 3.3 million customers after a vendor exposed unsecured data on the Internet. [...]
