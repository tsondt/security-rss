Title: DoJ Charges Rhode Island Woman in Phishing Scheme Against Politicians
Date: 2021-06-02T12:54:16+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks
Slug: doj-charges-rhode-island-woman-in-phishing-scheme-against-politicians

[Source](https://threatpost.com/doj-woman-phishing-politicians/166594/){:target="_blank" rel="noopener"}

> Diana Lebeau allegedly tried to trick candidates for public office and related individuals into giving up account credentials by impersonating trusted associates and the Microsoft security team. [...]
