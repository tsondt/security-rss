Title: Deadline draws near to avoid auto-joining Amazon's mesh network Sidewalk
Date: 2021-06-02T23:05:16+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: deadline-draws-near-to-avoid-auto-joining-amazons-mesh-network-sidewalk

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/02/amazon_sidewalk_mesh/){:target="_blank" rel="noopener"}

> 'A stalker can abuse it to stalk people better. There are no mitigations mentioned' Owners of Amazon Echo assistants and Ring doorbells have until June 8 to avoid automatically opting into Sidewalk, the internet giant's mesh network that taps into people's broadband and may prove to be a privacy nightmare.... [...]
