Title: Microsoft Teams: Very Bad Tabs Could Have Led to BEC
Date: 2021-06-14T20:26:52+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: microsoft-teams-very-bad-tabs-could-have-led-to-bec

[Source](https://threatpost.com/microsoft-teams-tabs-bec/166909/){:target="_blank" rel="noopener"}

> Attackers could have used the bug to get read/write privileges for a victim user’s email, Teams chats, OneDrive, Sharepoint and loads of other services. [...]
