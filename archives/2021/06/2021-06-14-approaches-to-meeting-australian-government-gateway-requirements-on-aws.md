Title: Approaches to meeting Australian Government gateway requirements on AWS
Date: 2021-06-14T19:03:46+00:00
Author: John Hildebrandt
Category: AWS Security
Tags: Announcements;Government;Intermediate (200);Security, Identity, & Compliance;Compliance;Secure internet connections;Secure Network Connections;Security Blog
Slug: approaches-to-meeting-australian-government-gateway-requirements-on-aws

[Source](https://aws.amazon.com/blogs/security/approaches-to-meeting-australian-government-gateway-requirements-on-aws/){:target="_blank" rel="noopener"}

> Australian Commonwealth Government agencies are subject to specific requirements set by the Protective Security Policy Framework (PSPF) for securing connectivity between systems that are running sensitive workloads, and for accessing less trusted environments, such as the internet. These agencies have often met the requirements by using some form of approved gateway solution that provides network-based [...]
