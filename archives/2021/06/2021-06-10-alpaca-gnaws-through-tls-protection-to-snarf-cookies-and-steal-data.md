Title: ALPACA gnaws through TLS protection to snarf cookies and steal data
Date: 2021-06-10T00:07:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: alpaca-gnaws-through-tls-protection-to-snarf-cookies-and-steal-data

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/10/alpaca_tls_protection/){:target="_blank" rel="noopener"}

> Boffins find flaw in web security that enables certificate confusion Academics from three German universities have found a vulnerability in the Transport Layer Security (TLS) protocol that under limited circumstances allows the theft of session cookies and enables cross-site scripting attacks.... [...]
