Title: Hacker community jumps on hilarious Twitter meme mocking bad infosec advice from CISOs
Date: 2021-06-16T12:35:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: hacker-community-jumps-on-hilarious-twitter-meme-mocking-bad-infosec-advice-from-cisos

[Source](https://portswigger.net/daily-swig/hacker-community-jumps-on-hilarious-twitter-meme-mocking-bad-infosec-advice-from-cisos){:target="_blank" rel="noopener"}

> Satirical hot takes from non-technical position holders [...]
