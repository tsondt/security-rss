Title: How Cyber Safe is Your Drinking Water Supply?
Date: 2021-06-21T18:36:46+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: The Coming Storm;Andrew Hildick-Smith;Environmental Protection Agency;Kevin Collier;Michael Arceneaux;NBCNews;Teamviewer;Water Infrastructure Act of 2018;WaterISAC
Slug: how-cyber-safe-is-your-drinking-water-supply

[Source](https://krebsonsecurity.com/2021/06/how-cyber-safe-is-your-drinking-water-supply/){:target="_blank" rel="noopener"}

> Amid multiple recent reports of hackers breaking into and tampering with drinking water treatment systems comes a new industry survey with some sobering findings: A majority of the 52,000 separate drinking water systems in the United States still haven’t inventoried some or any of their information technology systems — a basic first step in protecting networks from cyberattacks. The Water Sector Coordinating Council surveyed roughly 600 employees of water and wastewater treatment facilities nationwide, and found 37.9 percent of utilities have identified all IT-networked assets, with an additional 21.7 percent working toward that goal. The Council found when it comes [...]
