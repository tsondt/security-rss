Title: An EPYC escape: Case-study of a KVM breakout
Date: 2021-06-29T08:58:00.002000-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: an-epyc-escape-case-study-of-a-kvm-breakout

[Source](https://googleprojectzero.blogspot.com/2021/06/an-epyc-escape-case-study-of-kvm.html){:target="_blank" rel="noopener"}

> Posted by Felix Wilhelm, Project Zero Introduction KVM (for Kernel-based Virtual Machine) is the de-facto standard hypervisor for Linux-based cloud environments. Outside of Azure, almost all large-scale cloud and hosting providers are running on top of KVM, turning it into one of the fundamental security boundaries in the cloud. In this blog post I describe a vulnerability in KVM’s AMD-specific code and discuss how this bug can be turned into a full virtual machine escape. To the best of my knowledge, this is the first public writeup of a KVM guest-to-host breakout that does not rely on bugs in user [...]
