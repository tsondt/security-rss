Title: Norton dodges UK courts after telling Brit watchdog it will be nicer to consumers
Date: 2021-06-14T14:56:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: norton-dodges-uk-courts-after-telling-brit-watchdog-it-will-be-nicer-to-consumers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/14/norton_cma/){:target="_blank" rel="noopener"}

> Admits nothing but promises amendments to auto-renewal, refund policies The UK's Competition and Markets Authority (CMA) has inked a deal with Norton where it will refund customers whose antivirus software subscription was automatically renewed.... [...]
