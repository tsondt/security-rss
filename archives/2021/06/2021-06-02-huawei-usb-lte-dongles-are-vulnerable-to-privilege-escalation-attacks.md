Title: Huawei USB LTE dongles are vulnerable to privilege escalation attacks
Date: 2021-06-02T10:33:30-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: huawei-usb-lte-dongles-are-vulnerable-to-privilege-escalation-attacks

[Source](https://www.bleepingcomputer.com/news/security/huawei-usb-lte-dongles-are-vulnerable-to-privilege-escalation-attacks/){:target="_blank" rel="noopener"}

> This week, a Trustwave security researcher disclosed a privilege escalation flaw in Huawei's USB LTE dongles. [...]
