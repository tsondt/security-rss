Title: Clop ransomware is back in business after recent arrests
Date: 2021-06-23T03:35:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: clop-ransomware-is-back-in-business-after-recent-arrests

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-is-back-in-business-after-recent-arrests/){:target="_blank" rel="noopener"}

> The Clop ransomware operation is back in business after recent arrests and has begun listing new victims on their data leak site again. [...]
