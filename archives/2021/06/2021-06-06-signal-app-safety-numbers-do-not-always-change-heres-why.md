Title: Signal app safety numbers do not always change — here's why
Date: 2021-06-06T12:35:54-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Mobile
Slug: signal-app-safety-numbers-do-not-always-change-heres-why

[Source](https://www.bleepingcomputer.com/news/security/signal-app-safety-numbers-do-not-always-change-heres-why/){:target="_blank" rel="noopener"}

> This week, security researchers have steered attention towards an interesting finding while using Signal apps across multiple platforms. When you or your contact reinstall the Signal app or switch over to a new device, the Signal safety number between you two does not always change. [...]
