Title: New Cloud Asset Inventory capabilities help assess your Google Cloud environment
Date: 2021-06-08T16:00:00+00:00
Author: Sophia Yang
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: new-cloud-asset-inventory-capabilities-help-assess-your-google-cloud-environment

[Source](https://cloud.google.com/blog/products/identity-security/improve-visibility-with-four-cloud-asset-inventory-features/){:target="_blank" rel="noopener"}

> Businesses that operate in complex cloud environments, large fleets, or sophisticated security operations all require visibility into their cloud assets in order to keep their teams nimble and their data secure. Cloud Asset Inventory (CAI) helps these teams understand their Google Cloud and Anthos environments by providing complete visibility, real-time monitoring, and powerful asset analysis capabilities. Today, Cloud Asset Inventory gets four new capabilities that help you understand your environment more clearly and easily than ever before. New user interface eases asset and insight discovery Cloud Asset Inventory console preview is now publicly available for GCP and Anthos customers. This [...]
