Title: Kids’ Apps on Google Play Rife with Privacy Violations
Date: 2021-06-22T16:24:02+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Privacy
Slug: kids-apps-on-google-play-rife-with-privacy-violations

[Source](https://threatpost.com/kids-apps-google-play-privacy-violations/167110/){:target="_blank" rel="noopener"}

> One in five of the most-popular apps for kids under 13 on Google Play don't comply with COPPA regulations on how children's information is collected and used. [...]
