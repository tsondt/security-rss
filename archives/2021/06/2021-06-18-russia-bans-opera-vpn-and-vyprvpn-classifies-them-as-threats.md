Title: Russia bans Opera VPN and VyprVPN, classifies them as threats
Date: 2021-06-18T11:37:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: russia-bans-opera-vpn-and-vyprvpn-classifies-them-as-threats

[Source](https://www.bleepingcomputer.com/news/security/russia-bans-opera-vpn-and-vyprvpn-classifies-them-as-threats/){:target="_blank" rel="noopener"}

> Roskomnadzor, Russia's telecommunications watchdog, has banned the use of Opera VPN and VyprVPN after classifying them as threats according to current Russian law. [...]
