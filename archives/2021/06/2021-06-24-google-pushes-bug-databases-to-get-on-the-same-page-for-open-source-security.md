Title: Google pushes bug databases to get on the same page for open-source security
Date: 2021-06-24T13:00:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: google-pushes-bug-databases-to-get-on-the-same-page-for-open-source-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/24/google_security_fix/){:target="_blank" rel="noopener"}

> Chocolate Factory proposes common interchange format for vulnerability data Google on Thursday introduced a unified vulnerability schema for open source projects, continuing its current campaign to shore up the security of open source software.... [...]
