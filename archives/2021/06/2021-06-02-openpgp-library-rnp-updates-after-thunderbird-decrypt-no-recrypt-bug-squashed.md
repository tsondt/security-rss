Title: OpenPGP library RNP updates after Thunderbird decrypt-no-recrypt bug squashed
Date: 2021-06-02T10:44:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: openpgp-library-rnp-updates-after-thunderbird-decrypt-no-recrypt-bug-squashed

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/02/openpgp_rnp_library/){:target="_blank" rel="noopener"}

> Not the obvious function, the other obvious function OpenPGP project RNP has patched its flagship product after Mozilla Thunderbird, a major user, was found to be saving users’ private keys in plain text.... [...]
