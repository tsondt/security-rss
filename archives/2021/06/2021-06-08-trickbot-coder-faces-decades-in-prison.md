Title: TrickBot Coder Faces Decades in Prison
Date: 2021-06-08T20:28:16+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware
Slug: trickbot-coder-faces-decades-in-prison

[Source](https://threatpost.com/trickbot-coder-decades-prison/166732/){:target="_blank" rel="noopener"}

> A Latvian malware developer known as "Max" has been arraigned on 19 counts related to fraud, identity theft, information theft and money laundering. [...]
