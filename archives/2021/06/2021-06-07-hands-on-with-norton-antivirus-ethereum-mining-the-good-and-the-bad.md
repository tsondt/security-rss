Title: Hands on with Norton antivirus Ethereum mining: The good and the bad
Date: 2021-06-07T13:52:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency;Software
Slug: hands-on-with-norton-antivirus-ethereum-mining-the-good-and-the-bad

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/hands-on-with-norton-antivirus-ethereum-mining-the-good-and-the-bad/){:target="_blank" rel="noopener"}

> Last week, NortonLifelock announced that the Norton 360 antivirus suite would soon be able to mine Ethereum cryptocurrency while the computer is idle. In this article, we go hands-on with the new 'Norton Crypto' feature to show what's good about it and what's bad. [...]
