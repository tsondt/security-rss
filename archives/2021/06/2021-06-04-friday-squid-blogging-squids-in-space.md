Title: Friday Squid Blogging: Squids in Space
Date: 2021-06-04T20:43:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-squids-in-space

[Source](https://www.schneier.com/blog/archives/2021/06/friday-squid-blogging-squids-in-space.html){:target="_blank" rel="noopener"}

> NASA is sending baby bobtail squid into space. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
