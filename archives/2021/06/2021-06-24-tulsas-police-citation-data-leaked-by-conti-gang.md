Title: Tulsa’s Police-Citation Data Leaked by Conti Gang
Date: 2021-06-24T13:14:56+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Web Security
Slug: tulsas-police-citation-data-leaked-by-conti-gang

[Source](https://threatpost.com/tulsa-police-data-leaked-conti-ransomware/167220/){:target="_blank" rel="noopener"}

> A May 6 ransomware attack caused disruption across several of the municipality’s online services and websites. [...]
