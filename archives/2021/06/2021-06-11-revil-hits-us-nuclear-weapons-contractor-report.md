Title: REvil Hits US Nuclear Weapons Contractor: Report
Date: 2021-06-11T18:16:45+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Cloud Security;Critical Infrastructure;Government;Web Security
Slug: revil-hits-us-nuclear-weapons-contractor-report

[Source](https://threatpost.com/revil-hits-us-nuclear-weapons-contractor-sol-oriens/166858/){:target="_blank" rel="noopener"}

> "We hereby keep a right (sic) to forward all of the relevant documentation and data to military agencies of our choise (sic)" REvil reportedly wrote. [...]
