Title: Cryptography whizz Phil Zimmermann looks back at 30 years of Pretty Good Privacy
Date: 2021-06-08T17:01:09+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: cryptography-whizz-phil-zimmermann-looks-back-at-30-years-of-pretty-good-privacy

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/08/pgp_at_30/){:target="_blank" rel="noopener"}

> The highs, the lows, the acquisitions, the resignations, and more Encryption and verification package Pretty Good Privacy (PGP) has celebrated a troubled 30 years of securing secrets and giving cypherpunks an excuse to meet in person, with original developer and security specialist Phil Zimmermann toasting a world where encryption is common but, he warns, still under threat.... [...]
