Title: Vulnerabilities in Weapons Systems
Date: 2021-06-08T10:32:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: vulnerabilities-in-weapons-systems

[Source](https://www.schneier.com/blog/archives/2021/06/vulnerabilities-in-weapons-systems.html){:target="_blank" rel="noopener"}

> “If you think any of these systems are going to work as expected in wartime, you’re fooling yourself.” That was Bruce’s response at a conference hosted by U.S. Transportation Command in 2017, after learning that their computerized logistical systems were mostly unclassified and on the internet. That may be necessary to keep in touch with civilian companies like FedEx in peacetime or when fighting terrorists or insurgents. But in a new era facing off with China or Russia, it is dangerously complacent. Any 21st century war will include cyber operations. Weapons and support systems will be successfully attacked. Rifles and [...]
