Title: Military infosec SNAFUs: What WhatsApp and bears in the woods can teach us
Date: 2021-06-07T08:32:12+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: military-infosec-snafus-what-whatsapp-and-bears-in-the-woods-can-teach-us

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/07/military_infosec_snafus_what_whatsapp/){:target="_blank" rel="noopener"}

> One can’t spell shit without IT, but for Pete's sake it doesn't need to be in your endpoints Column Fans of John le Carré’s Tinker Tailor Soldier Spy know how top military secrets are extracted from the enemy. Senior figures are turned in operations run by the most secret brains in the country, bluff and double-bluff mix with incredible feats of bravery, treachery and psychological manipulation.... [...]
