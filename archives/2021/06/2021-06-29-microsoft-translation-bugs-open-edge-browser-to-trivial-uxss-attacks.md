Title: Microsoft Translation Bugs Open Edge Browser to Trivial UXSS Attacks
Date: 2021-06-29T16:34:21+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Bug Bounty;Vulnerabilities;Web Security
Slug: microsoft-translation-bugs-open-edge-browser-to-trivial-uxss-attacks

[Source](https://threatpost.com/microsoft-edge-browser-uxss-attacks/167389/){:target="_blank" rel="noopener"}

> The bug in Edge's auto-translate could have let remote attackers pull off RCE on any foreign-language website just by sending a message with an XSS payload. [...]
