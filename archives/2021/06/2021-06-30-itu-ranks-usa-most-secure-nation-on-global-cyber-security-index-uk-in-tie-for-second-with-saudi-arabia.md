Title: ITU ranks USA most secure nation on Global Cyber Security Index, UK in tie for second with Saudi Arabia
Date: 2021-06-30T06:05:37+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: itu-ranks-usa-most-secure-nation-on-global-cyber-security-index-uk-in-tie-for-second-with-saudi-arabia

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/30/itu_global_cyber_security_index_2020/){:target="_blank" rel="noopener"}

> Global median score jumped by ten per cent in two years — and just as well The United Nations International Telecommunication Union (ITU) published its 2020 Global Cyber Security Index on Tuesday, and listed the US first in overall ranking, followed by a tie for second place tie between the UK and Saudi Arabia.... [...]
