Title: Billions of Compromised Records and Counting: Why the Application Layer is Still the Front Door for Data Breaches
Date: 2021-06-08T13:00:35+00:00
Author: Threatpost
Category: Threatpost
Tags: Sponsored;Web Security
Slug: billions-of-compromised-records-and-counting-why-the-application-layer-is-still-the-front-door-for-data-breaches

[Source](https://threatpost.com/billions-of-compromised-records-and-counting/166633/){:target="_blank" rel="noopener"}

> Security teams should brace for an unsettling and unprecedented year, as we’re on pace to see 40 billion records compromised by the end of 2021. Imperva’s Terry Ray explains what security teams need to do to bolster their defenses. [...]
