Title: Cloud CISO Perspectives: June 2021
Date: 2021-06-15T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: cloud-ciso-perspectives-june-2021

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-june-2021/){:target="_blank" rel="noopener"}

> It's been another busy month for security teams around the globe with no signs of slowing down. Many of us virtually attended RSA, and ransomware attacks continue to dominate headlines. The Biden Administration's Executive Order on Cybersecurity is officially underway, with important milestones like the NIST workshops where many of us discussed the Standards and Guidelines to Enhance Software Supply Chain Security. In this month’s post I’ll recap these topics, the latest security updates from our Google Cloud product teams, and more. Don’t forget we have a new newsletter sign up for this series, so you can get the latest [...]
