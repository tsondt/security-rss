Title: Binance reveals how data analytics led to ransomware-linked money laundering bust
Date: 2021-06-28T16:00:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: binance-reveals-how-data-analytics-led-to-ransomware-linked-money-laundering-bust

[Source](https://portswigger.net/daily-swig/binance-reveals-how-data-analytics-led-to-ransomware-linked-money-laundering-bust){:target="_blank" rel="noopener"}

> Crypto-exchange exploits OpSec mistakes to bust crooks [...]
