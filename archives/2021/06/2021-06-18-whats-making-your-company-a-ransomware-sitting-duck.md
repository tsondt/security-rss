Title: What’s Making Your Company a Ransomware Sitting Duck
Date: 2021-06-18T15:35:41+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Podcasts;Vulnerabilities;Web Security
Slug: whats-making-your-company-a-ransomware-sitting-duck

[Source](https://threatpost.com/ransomware-sitting-duck/167040/){:target="_blank" rel="noopener"}

> What's the low-hanging fruit for ransomware attackers? What steps could help to fend them off, and what’s stopping organizations from implementing those steps? [...]
