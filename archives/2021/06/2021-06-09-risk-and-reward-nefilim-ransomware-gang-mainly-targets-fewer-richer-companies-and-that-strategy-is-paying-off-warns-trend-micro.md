Title: Risk and reward: Nefilim ransomware gang mainly targets fewer, richer companies and that strategy is paying off, warns Trend Micro
Date: 2021-06-09T20:17:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: risk-and-reward-nefilim-ransomware-gang-mainly-targets-fewer-richer-companies-and-that-strategy-is-paying-off-warns-trend-micro

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/09/trend_micro_nefilim_ransomware_research/){:target="_blank" rel="noopener"}

> Criminal operators emerged from woodwork just as COVID hit the West The Nefilim ransomware gang might not be the best known or most prolific online extortion crew but their penchant for attacking small numbers of $1bn+ turnover firms is paying off, according tot he latest research.... [...]
