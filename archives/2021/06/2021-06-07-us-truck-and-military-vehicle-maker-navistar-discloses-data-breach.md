Title: US truck and military vehicle maker Navistar discloses data breach
Date: 2021-06-07T12:47:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-truck-and-military-vehicle-maker-navistar-discloses-data-breach

[Source](https://www.bleepingcomputer.com/news/security/us-truck-and-military-vehicle-maker-navistar-discloses-data-breach/){:target="_blank" rel="noopener"}

> Navistar International Corporation (Navistar), a US-based maker of trucks and military vehicles, says that unknown attackers have stolen data from its network following a cybersecurity incident discovered at the end of last month. [...]
