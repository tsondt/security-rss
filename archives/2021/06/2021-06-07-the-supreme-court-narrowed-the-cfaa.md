Title: The Supreme Court Narrowed the CFAA
Date: 2021-06-07T11:09:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: courts;fraud;machine learning
Slug: the-supreme-court-narrowed-the-cfaa

[Source](https://www.schneier.com/blog/archives/2021/06/the-supreme-court-narrowed-the-cfaa.html){:target="_blank" rel="noopener"}

> In a 6-3 ruling, the Supreme Court just narrowed the scope of the Computer Fraud and Abuse Act : In a ruling delivered today, the court sided with Van Buren and overturned his 18-month conviction. In a 37-page opinion written and delivered by Justice Amy Coney Barrett, the court explained that the “exceeds authorized access” language was, indeed, too broad. Justice Barrett said the clause was effectively making criminals of most US citizens who ever used a work resource to perform unauthorized actions, such as updating a dating profile, checking sports scores, or paying bills at work. What today’s ruling [...]
