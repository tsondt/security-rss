Title: Google Workspace delivers new levels of trusted collaboration for a hybrid work world
Date: 2021-06-14T10:00:00+00:00
Author: Erika Trautman
Category: GCP Security
Tags: Identity & Security;Google Workspace
Slug: google-workspace-delivers-new-levels-of-trusted-collaboration-for-a-hybrid-work-world

[Source](https://cloud.google.com/blog/products/workspace/new-google-workspace-security-features/){:target="_blank" rel="noopener"}

> Today we shared more powerful ways for people to stay connected and get more done together with Google Workspace—at home, at school, and at work. As hybrid work becomes the norm for many employees, security, data privacy, and trust continue to be the foundation that make anywhere, anytime collaboration possible. Google has been focused on these areas since the beginning, and today we’re announcing several ways that we’re strengthening this foundation in Google Workspace. We’ve long been advocates for making the web a more trusted place for everyone. In 2004, we launched Gmail with a mission to defeat spam, and [...]
