Title: Lexmark Printers Open to Arbitrary Code-Execution Zero-Day
Date: 2021-06-22T16:17:10+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities
Slug: lexmark-printers-open-to-arbitrary-code-execution-zero-day

[Source](https://threatpost.com/lexmark-printers-code-execution-zero-day/167111/){:target="_blank" rel="noopener"}

> “No remedy available as of June 21, 2021," according to the researcher who discovered the easy-to-exploit, no-user-action-required bug. [...]
