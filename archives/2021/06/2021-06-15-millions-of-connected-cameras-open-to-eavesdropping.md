Title: Millions of Connected Cameras Open to Eavesdropping
Date: 2021-06-15T20:51:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Vulnerabilities
Slug: millions-of-connected-cameras-open-to-eavesdropping

[Source](https://threatpost.com/millions-connected-cameras-eavesdropping/166950/){:target="_blank" rel="noopener"}

> A supply-chain component lays open camera feeds to remote attackers thanks to a critical security vulnerability. [...]
