Title: Atlassian Bugs Could Have Led to 1-Click Takeover
Date: 2021-06-24T10:00:47+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: atlassian-bugs-could-have-led-to-1-click-takeover

[Source](https://threatpost.com/atlassian-bugs-could-have-led-to-1-click-takeover/167203/){:target="_blank" rel="noopener"}

> A supply-chain attack could have siphoned sensitive information out of Jira, such as security issues on Atlassian cloud, Bitbucket and on-prem products. [...]
