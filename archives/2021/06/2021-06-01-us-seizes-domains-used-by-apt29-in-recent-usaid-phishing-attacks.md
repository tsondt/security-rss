Title: US seizes domains used by APT29 in recent USAID phishing attacks
Date: 2021-06-01T16:56:57-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Government
Slug: us-seizes-domains-used-by-apt29-in-recent-usaid-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-domains-used-by-apt29-in-recent-usaid-phishing-attacks/){:target="_blank" rel="noopener"}

> The US Department of Justice has seized two Internet domains used in recent phishing attacks impersonating the U.S. Agency for International Development (USAID) to distribute malware and gain access to internal networks. [...]
