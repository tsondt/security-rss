Title: Buckland: ministerial offices should be swept for hidden cameras – video
Date: 2021-06-28T11:50:49+00:00
Author: 
Category: The Guardian
Tags: Matt Hancock;Robert Buckland;Conservatives;Data and computer security;Privacy;UK news
Slug: buckland-ministerial-offices-should-be-swept-for-hidden-cameras-video

[Source](https://www.theguardian.com/politics/video/2021/jun/28/robert-buckland-ministerial-offices-should-be-swept-for-hidden-cameras-video){:target="_blank" rel="noopener"}

> The justice secretary has signalled that ministerial security may have been compromised as he called for regular security sweeps for cameras in ministers’ offices after the Matt Hancock scandal. Hancock resigned as health secretary after CCTV footage from his departmental office showing him kissing his senior aide was leaked to the press Hancock CCTV footage: security may have been breached, minister says Continue reading... [...]
