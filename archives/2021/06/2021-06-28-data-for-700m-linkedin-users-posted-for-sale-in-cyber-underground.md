Title: Data for 700M LinkedIn Users Posted for Sale in Cyber-Underground
Date: 2021-06-28T23:24:06+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Privacy;Web Security
Slug: data-for-700m-linkedin-users-posted-for-sale-in-cyber-underground

[Source](https://threatpost.com/data-700m-linkedin-users-cyber-underground/167362/){:target="_blank" rel="noopener"}

> After 500 million LinkedIn enthusiasts were affected in a data-scraping incident in April, it's happened again - with big security ramifications. [...]
