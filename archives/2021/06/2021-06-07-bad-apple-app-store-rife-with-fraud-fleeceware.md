Title: Bad Apple: App Store Rife with Fraud, Fleeceware
Date: 2021-06-07T19:37:51+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security
Slug: bad-apple-app-store-rife-with-fraud-fleeceware

[Source](https://threatpost.com/apple-app-store-fraud-fleeceware/166703/){:target="_blank" rel="noopener"}

> Malicious apps make up 2 percent of top grossing apps in Apple App Store. [...]
