Title: Phishing attack's unusual file attachment is a double-edged sword
Date: 2021-06-24T08:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: phishing-attacks-unusual-file-attachment-is-a-double-edged-sword

[Source](https://www.bleepingcomputer.com/news/security/phishing-attacks-unusual-file-attachment-is-a-double-edged-sword/){:target="_blank" rel="noopener"}

> A threat actor uses an unusual attachment to bypass security software that is a double-edged sword that may work against them. [...]
