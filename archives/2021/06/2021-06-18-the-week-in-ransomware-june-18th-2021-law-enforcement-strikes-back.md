Title: The Week in Ransomware - June 18th 2021 - Law enforcement strikes back
Date: 2021-06-18T18:11:21-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-june-18th-2021-law-enforcement-strikes-back

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-18th-2021-law-enforcement-strikes-back/){:target="_blank" rel="noopener"}

> Compared to the last few weeks, it has been a relatively quiet week with no ransomware attacks causing widespread disruption. [...]
