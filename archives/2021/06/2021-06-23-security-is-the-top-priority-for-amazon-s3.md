Title: Security is the top priority for Amazon S3
Date: 2021-06-23T16:14:03+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Amazon Simple Storage Services (S3);Announcements;Foundational (100);Security, Identity, & Compliance;Storage;Amazon S3;Security Blog
Slug: security-is-the-top-priority-for-amazon-s3

[Source](https://aws.amazon.com/blogs/security/security-is-the-top-priority-for-amazon-s3/){:target="_blank" rel="noopener"}

> Amazon Simple Storage Service (Amazon S3) launched 15 years ago in March 2006, and became the first generally available service from Amazon Web Services (AWS). AWS marked the fifteenth anniversary with AWS Pi Week—a week of in-depth streams and live events. During AWS Pi Week, AWS leaders and experts reviewed the history of AWS and [...]
