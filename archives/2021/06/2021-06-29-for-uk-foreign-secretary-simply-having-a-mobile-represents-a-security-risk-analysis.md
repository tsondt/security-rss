Title: For UK foreign secretary, simply having a mobile represents a security risk – analysis
Date: 2021-06-29T14:00:11+00:00
Author: Dan Sabbagh Defence and security editor
Category: The Guardian
Tags: Dominic Raab;UK security and counter-terrorism;Cybercrime;Foreign, Commonwealth and Development Office;Data and computer security;Privacy;Technology;Internet;Mobile phones;Politics;Telecoms;UK news;World news;Espionage
Slug: for-uk-foreign-secretary-simply-having-a-mobile-represents-a-security-risk-analysis

[Source](https://www.theguardian.com/politics/2021/jun/29/uk-foreign-secretary-having-mobile-security-risk-analysis){:target="_blank" rel="noopener"}

> Analysis: UK prides itself on GCHQ’s cyber capability – so availability of Raab’s number will have been embarrassing for him Dominic Raab’s mobile number freely available online for last decade Finding Dominic Raab’s mobile phone online is more than just embarrassing for the foreign secretary: it also represents a security risk, just as when it emerged Boris Johnson’s number could be easily found online in April. Sophisticated spyware technology – of the type available to a rapidly growing number of governments outside the west – can, in some circumstances, be secretly inserted into a person’s phone without any interaction from [...]
