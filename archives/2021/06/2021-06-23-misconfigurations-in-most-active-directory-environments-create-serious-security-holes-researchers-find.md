Title: Misconfigurations in most Active Directory environments create serious security holes, researchers find
Date: 2021-06-23T16:56:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: misconfigurations-in-most-active-directory-environments-create-serious-security-holes-researchers-find

[Source](https://portswigger.net/daily-swig/misconfigurations-in-most-active-directory-environments-create-serious-security-holes-researchers-find){:target="_blank" rel="noopener"}

> One default configuration deemed problematic failed meet Microsoft’s ‘bar for a security update’ [...]
