Title: NFC flaws let researchers hack an ATM by waving a phone
Date: 2021-06-25T19:52:13+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;ATM;banking;hacking;NFC;white hat
Slug: nfc-flaws-let-researchers-hack-an-atm-by-waving-a-phone

[Source](https://arstechnica.com/?p=1776333){:target="_blank" rel="noopener"}

> Enlarge (credit: Chalongrat Chuvaree | Getty Images) For years, security researchers and cybercriminals have hacked ATMs by using all possible avenues to their innards, from opening a front panel and sticking a thumb drive into a USB port to drilling a hole that exposes internal wiring. Now, one researcher has found a collection of bugs that allow him to hack ATMs—along with a wide variety of point-of-sale terminals—in a new way: with a wave of his phone over a contactless credit card reader. Josep Rodriguez, a researcher and consultant at security firm IOActive, has spent the last year digging up [...]
