Title: America world’s sole cyber superpower, ten years ahead of China, says Brit think tank
Date: 2021-06-29T04:44:48+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: america-worlds-sole-cyber-superpower-ten-years-ahead-of-china-says-brit-think-tank

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/29/international_institute_for_strategic_studies_cyber_study/){:target="_blank" rel="noopener"}

> UK, Australia, Russia ranked in second tier. North Korea and Iran are a step down but feisty The United States is comfortably the world’s most powerful nation when measured on “cyber capabilities that make the greatest difference to national power,” according to British think tank The International Institute for Strategic Studies.... [...]
