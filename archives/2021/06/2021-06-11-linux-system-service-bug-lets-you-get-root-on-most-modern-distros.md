Title: Linux system service bug lets you get root on most modern distros
Date: 2021-06-11T07:58:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Linux
Slug: linux-system-service-bug-lets-you-get-root-on-most-modern-distros

[Source](https://www.bleepingcomputer.com/news/security/linux-system-service-bug-lets-you-get-root-on-most-modern-distros/){:target="_blank" rel="noopener"}

> Unprivileged attackers can get a root shell by exploiting an authentication bypass vulnerability in the polkit auth system service installed by default on many modern Linux distributions. [...]
