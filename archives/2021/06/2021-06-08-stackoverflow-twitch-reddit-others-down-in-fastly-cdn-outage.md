Title: StackOverflow, Twitch, Reddit, others down in Fastly CDN outage
Date: 2021-06-08T06:33:56-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: stackoverflow-twitch-reddit-others-down-in-fastly-cdn-outage

[Source](https://www.bleepingcomputer.com/news/security/stackoverflow-twitch-reddit-others-down-in-fastly-cdn-outage/){:target="_blank" rel="noopener"}

> Major websites around the world are either completely down or not loading properly in a global outage. [...]
