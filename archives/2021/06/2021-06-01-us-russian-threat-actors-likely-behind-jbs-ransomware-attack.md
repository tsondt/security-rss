Title: US: Russian threat actors likely behind JBS ransomware attack
Date: 2021-06-01T15:33:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-russian-threat-actors-likely-behind-jbs-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/us-russian-threat-actors-likely-behind-jbs-ransomware-attack/){:target="_blank" rel="noopener"}

> The White House has confirmed today that JBS, the world's largest beef producer, was hit by a ransomware attack over the weekend coordinated by a group likely from Russia. [...]
