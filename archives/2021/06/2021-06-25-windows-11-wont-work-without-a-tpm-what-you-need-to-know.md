Title: Windows 11 won't work without a TPM - What you need to know
Date: 2021-06-25T09:15:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: windows-11-wont-work-without-a-tpm-what-you-need-to-know

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-11-wont-work-without-a-tpm-what-you-need-to-know/){:target="_blank" rel="noopener"}

> Windows 11 requires a TPM security processor to install or upgrade to Windows 11. Unfortunately, there has been a lot of confusion about what type of TPM you need and why you need it in the first place. [...]
