Title: UF Health Florida hospitals back to pen and paper after cyberattack
Date: 2021-06-03T16:10:04-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: uf-health-florida-hospitals-back-to-pen-and-paper-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/uf-health-florida-hospitals-back-to-pen-and-paper-after-cyberattack/){:target="_blank" rel="noopener"}

> UF Health Central Florida has suffered a reported ransomware attack that forced two hospitals to shut down portions of their IT network. [...]
