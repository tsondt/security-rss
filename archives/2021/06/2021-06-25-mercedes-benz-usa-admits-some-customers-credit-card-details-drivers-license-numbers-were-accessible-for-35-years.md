Title: Mercedes-Benz USA admits some customers’ credit card details, driver’s license numbers were accessible for 3.5 years
Date: 2021-06-25T16:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: mercedes-benz-usa-admits-some-customers-credit-card-details-drivers-license-numbers-were-accessible-for-35-years

[Source](https://portswigger.net/daily-swig/mercedes-benz-usa-admits-some-customers-credit-card-details-drivers-license-numbers-were-accessible-for-3-5-years){:target="_blank" rel="noopener"}

> Fewer than 1,000 individuals impacted by bungle [...]
