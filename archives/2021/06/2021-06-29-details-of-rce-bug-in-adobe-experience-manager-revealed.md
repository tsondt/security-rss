Title: Details of RCE Bug in Adobe Experience Manager Revealed
Date: 2021-06-29T11:34:38+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: details-of-rce-bug-in-adobe-experience-manager-revealed

[Source](https://threatpost.com/rce-bug-in-adobe-revealed/167382/){:target="_blank" rel="noopener"}

> Disclosure of a bug in Adobe’s content-management solution - used by Mastercard, LinkedIn and PlayStation – were released. [...]
