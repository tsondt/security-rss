Title: A Google Drive security update will break some of your shared links
Date: 2021-06-24T09:17:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google
Slug: a-google-drive-security-update-will-break-some-of-your-shared-links

[Source](https://www.bleepingcomputer.com/news/google/a-google-drive-security-update-will-break-some-of-your-shared-links/){:target="_blank" rel="noopener"}

> An upcoming security update for Google Drive will increase the security of your shared documents but likely break many of your shared links. [...]
