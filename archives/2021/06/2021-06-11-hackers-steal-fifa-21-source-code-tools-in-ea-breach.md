Title: Hackers Steal FIFA 21 Source Code, Tools in EA Breach
Date: 2021-06-11T11:43:18+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach
Slug: hackers-steal-fifa-21-source-code-tools-in-ea-breach

[Source](https://threatpost.com/hackers-fifa-21-source-code/166829/){:target="_blank" rel="noopener"}

> Raft of other proprietary game data and related software and developer kits also pilfered in the unspecified attack, which the company is investigating. [...]
