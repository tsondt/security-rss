Title: Massachusetts' largest ferry service hit by ransomware attack
Date: 2021-06-03T09:07:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: massachusetts-largest-ferry-service-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/massachusetts-largest-ferry-service-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> The Steamship Authority, Massachusetts' largest ferry service, was hit by a ransomware attack that led to ticketing and reservation disruptions. [...]
