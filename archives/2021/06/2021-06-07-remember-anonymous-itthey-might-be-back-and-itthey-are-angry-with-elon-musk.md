Title: Remember Anonymous? It/they might be back, and it/they are angry with Elon Musk
Date: 2021-06-07T13:45:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: remember-anonymous-itthey-might-be-back-and-itthey-are-angry-with-elon-musk

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/07/anonymous_musk/){:target="_blank" rel="noopener"}

> Has the hacktivist group had enough of Musk's manipulative crypto-Tweets, or has someone just donned the mask to protest their crashed Bitcoin balance? Entities using the name and iconography of hacktivist collective Anonymous have deemed Elon Musk's recent crypto-tweeting worthy of a re-emergence.... [...]
