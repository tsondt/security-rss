Title: Hiccup in Akamai’s DDoS Mitigation Service Triggers Massive String of Outages
Date: 2021-06-17T12:50:05+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure
Slug: hiccup-in-akamais-ddos-mitigation-service-triggers-massive-string-of-outages

[Source](https://threatpost.com/hiccup-akamais-ddos-outages/167004/){:target="_blank" rel="noopener"}

> An hour-long outage hit airlines, banks and the Hong Kong Stock exchange. It's thought to have been caused by a DDoS mitigation service. [...]
