Title: Monumental Supply-Chain Attack on Airlines Traced to State Actor
Date: 2021-06-11T14:23:57+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Malware
Slug: monumental-supply-chain-attack-on-airlines-traced-to-state-actor

[Source](https://threatpost.com/supply-chain-attack-airlines-state-actor/166842/){:target="_blank" rel="noopener"}

> Airlines are warned to scour networks for traces of the campaign, likely the work of APT41, lurking in networks. [...]
