Title: Mysterious Gelsemium APT was behind February compromise of NoxPlayer, says ESET
Date: 2021-06-09T15:43:14+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: mysterious-gelsemium-apt-was-behind-february-compromise-of-noxplayer-says-eset

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/09/eset_gelsemium_research/){:target="_blank" rel="noopener"}

> Malicious crew targets ME and Asia but also tries evading Chinese AV suites ESET has published details of an advanced persistent threat (APT) crew that appears to have deployed recent supply chain attack methods against targets including "electronics manufacturers," although it didn't specify which.... [...]
