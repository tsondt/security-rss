Title: Google Cloud welcomes EU’s new Standard Contractual Clauses for cross-border data transfers
Date: 2021-06-08T13:00:00+00:00
Author: Marc Crandall
Category: GCP Security
Tags: Identity & Security;Google Cloud Platform;Google Cloud in Europe;Compliance
Slug: google-cloud-welcomes-eus-new-standard-contractual-clauses-for-cross-border-data-transfers

[Source](https://cloud.google.com/blog/products/compliance/eus-new-standard-contractual-clauses-to-protect-user-privacy/){:target="_blank" rel="noopener"}

> The European Commission (EC) has recently published new Standard Contractual Clauses (SCCs, also known as Model Contractual Clauses) to help safeguard European personal data. Following the applicable transition period, these new SCCs will replace the SCCs previously adopted by the EC. Google Cloud plans to incorporate the new SCCs into our contracts to help protect our customers’ data and meet the requirements of European privacy legislation. Like the previous SCCs, these clauses can be used to facilitate lawful transfers of data under certain conditions. By imposing various contractual obligations, SCCs allow personal data subject to the EU’s General Data Protection [...]
