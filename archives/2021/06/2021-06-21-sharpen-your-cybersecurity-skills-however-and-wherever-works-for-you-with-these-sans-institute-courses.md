Title: Sharpen your cybersecurity skills, however and wherever works for you, with these SANS Institute courses
Date: 2021-06-21T06:30:09+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: sharpen-your-cybersecurity-skills-however-and-wherever-works-for-you-with-these-sans-institute-courses

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/21/sharpen_your_cybersecurity_skills/){:target="_blank" rel="noopener"}

> Network invaders haven't stopped learning... have you? Promo The last year has shown that lock down and travel restrictions are no barrier to learning. After all, when it comes to the cybersecurity world, miscreants seem to have learned plenty.... [...]
