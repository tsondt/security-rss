Title: OnlyFans, Twitter ban users for leaking politician's BDSM video
Date: 2021-06-21T15:27:51-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: onlyfans-twitter-ban-users-for-leaking-politicians-bdsm-video

[Source](https://www.bleepingcomputer.com/news/security/onlyfans-twitter-ban-users-for-leaking-politicians-bdsm-video/){:target="_blank" rel="noopener"}

> This week, Twitter and OnlyFans have banned user accounts that illicitly leaked a BSDM video featuring a New York City city council candidate. As reported by Motherboard, the video of 26-year old Zack Weiner began circulating on Twitter and OnlyFans via accounts with identical usernames and profile pictures. [...]
