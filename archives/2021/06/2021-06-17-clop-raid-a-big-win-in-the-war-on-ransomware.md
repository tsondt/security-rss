Title: Clop Raid: A Big Win in the War on Ransomware?
Date: 2021-06-17T20:46:11+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware
Slug: clop-raid-a-big-win-in-the-war-on-ransomware

[Source](https://threatpost.com/clop-raid-big-win-war-ransomware/167036/){:target="_blank" rel="noopener"}

> Cops arrest six, seize cars and cash in splashy raid, and experts are applauding. [...]
