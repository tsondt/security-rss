Title: Critical zero-day vulnerabilities found in ‘unsupported’ Fedena school management software
Date: 2021-06-07T13:12:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: critical-zero-day-vulnerabilities-found-in-unsupported-fedena-school-management-software

[Source](https://portswigger.net/daily-swig/critical-zero-day-vulnerabilities-found-in-unsupported-fedena-school-management-software){:target="_blank" rel="noopener"}

> Users urged to migrate to alternative application, with open source project long since abandoned [...]
