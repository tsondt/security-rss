Title: Microsoft: Big Cryptomining Attacks Hit Kubeflow
Date: 2021-06-10T16:26:28+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Hacks;Web Security
Slug: microsoft-big-cryptomining-attacks-hit-kubeflow

[Source](https://threatpost.com/microsoft-cryptomining-kubeflow/166777/){:target="_blank" rel="noopener"}

> Misconfigured dashboards are yet again at the heart of a widespread, ongoing cryptocurrency campaign squeezing Monero and Ethereum from Kubernetes clusters. [...]
