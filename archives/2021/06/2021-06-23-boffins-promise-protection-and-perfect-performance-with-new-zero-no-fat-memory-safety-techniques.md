Title: Boffins promise protection and perfect performance with new ZeRØ, No-FAT memory safety techniques
Date: 2021-06-23T13:27:05+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: boffins-promise-protection-and-perfect-performance-with-new-zero-no-fat-memory-safety-techniques

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/23/zero_no_fat_memory_safety/){:target="_blank" rel="noopener"}

> Fast, easy to implement, and knocks attacks like Spectre on the head – what's the catch? Researchers at the Columbia University School of Engineering and Applied Science have showcased two new approaches to providing computers with memory protection without sacrificing performance – and they're being implemented in silicon by the US Air Force Research Lab.... [...]
