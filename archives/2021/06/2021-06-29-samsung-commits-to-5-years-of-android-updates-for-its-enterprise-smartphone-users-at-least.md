Title: Samsung commits to 5 years of Android updates... for its enterprise smartphone users at least
Date: 2021-06-29T21:01:05+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: samsung-commits-to-5-years-of-android-updates-for-its-enterprise-smartphone-users-at-least

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/29/samsung_five_year_android/){:target="_blank" rel="noopener"}

> Impressive, but it's still no Apple Samsung today committed to provide its enterprise-edition flagships with half a decade's worth of security updates.... [...]
