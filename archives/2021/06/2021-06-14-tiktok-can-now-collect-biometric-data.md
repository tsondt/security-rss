Title: TikTok Can Now Collect Biometric Data
Date: 2021-06-14T15:11:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: biometrics;identification;privacy;social media
Slug: tiktok-can-now-collect-biometric-data

[Source](https://www.schneier.com/blog/archives/2021/06/tiktok-can-now-collect-biometric-data.html){:target="_blank" rel="noopener"}

> This is probably worth paying attention to: A change to TikTok’s U.S. privacy policy on Wednesday introduced a new section that says the social video app “may collect biometric identifiers and biometric information” from its users’ content. This includes things like “faceprints and voiceprints,” the policy explained. Reached for comment, TikTok could not confirm what product developments necessitated the addition of biometric data to its list of disclosures about the information it automatically collects from users, but said it would ask for consent in the case such data collection practices began. [...]
