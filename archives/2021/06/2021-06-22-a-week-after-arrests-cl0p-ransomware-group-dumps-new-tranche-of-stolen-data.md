Title: A week after arrests, Cl0p ransomware group dumps new tranche of stolen data
Date: 2021-06-22T20:07:09+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;Cl0p;law enforcement;ransomware
Slug: a-week-after-arrests-cl0p-ransomware-group-dumps-new-tranche-of-stolen-data

[Source](https://arstechnica.com/?p=1775362){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) A week after Ukrainian police arrested criminals affiliated with the notorious Cl0p ransomware gang, Cl0p has published a fresh batch of what’s purported to be confidential data stolen in a hack of a previously unknown victim. Ars won’t be identifying the possibly victimized company until there is confirmation that the data and the hack are genuine. If genuine, the dump shows that Cl0p remains intact and able to carry out its nefarious actions despite the arrests. That suggests that the suspects don’t include the core leaders but rather affiliates or others who play a lesser [...]
