Title: CVS Health Records for 1.1 Billion Customers Exposed
Date: 2021-06-17T16:47:56+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Web Security
Slug: cvs-health-records-for-11-billion-customers-exposed

[Source](https://threatpost.com/cvs-health-records-billion-customers-exposed/167011/){:target="_blank" rel="noopener"}

> A vendor exposed the records, which were accessible with no password or other authentication, likely because of a cloud-storage misconfiguration. [...]
