Title: Insider Versus Outsider: Navigating Top Data Loss Threats
Date: 2021-06-18T13:39:22+00:00
Author: Troy Gill
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;InfoSec Insider;Web Security
Slug: insider-versus-outsider-navigating-top-data-loss-threats

[Source](https://threatpost.com/insider-outsider-data-loss-threats/167063/){:target="_blank" rel="noopener"}

> Troy Gill, manager of security research at Zix, discusses the most common ways sensitive data is scooped up by nefarious sorts. [...]
