Title: Researchers: Booming Cyber-Underground Market for Initial-Access Brokers
Date: 2021-06-16T11:51:31+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: 
Slug: researchers-booming-cyber-underground-market-for-initial-access-brokers

[Source](https://threatpost.com/booming-cyber-underground-market-initial-access-brokers/166965/){:target="_blank" rel="noopener"}

> Ransomware gangs are increasingly buying their way into corporate networks, purchasing access from 'vendors' that have previously installed backdoors on targets. [...]
