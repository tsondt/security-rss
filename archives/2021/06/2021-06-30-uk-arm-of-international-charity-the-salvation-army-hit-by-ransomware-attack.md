Title: UK arm of international charity the Salvation Army hit by ransomware attack
Date: 2021-06-30T10:25:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-arm-of-international-charity-the-salvation-army-hit-by-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/30/salvation_army_ransomware_attack/){:target="_blank" rel="noopener"}

> Christian org becomes latest victim of latter-day IT scourge Exclusive Criminals infected the Salvation Army in the UK with ransomware and siphoned the organisation's data, The Register has learned.... [...]
