Title: Slilpp, the largest stolen logins market, seized by law enforcement
Date: 2021-06-10T15:17:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: slilpp-the-largest-stolen-logins-market-seized-by-law-enforcement

[Source](https://www.bleepingcomputer.com/news/security/slilpp-the-largest-stolen-logins-market-seized-by-law-enforcement/){:target="_blank" rel="noopener"}

> The US Justice Department has announced today that a multinational operation took down Slillpp, the largest online marketplace of stolen login credentials. [...]
