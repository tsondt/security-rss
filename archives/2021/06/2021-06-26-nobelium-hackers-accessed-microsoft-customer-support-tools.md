Title: Nobelium hackers accessed Microsoft customer support tools
Date: 2021-06-26T12:11:31-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: nobelium-hackers-accessed-microsoft-customer-support-tools

[Source](https://www.bleepingcomputer.com/news/microsoft/nobelium-hackers-accessed-microsoft-customer-support-tools/){:target="_blank" rel="noopener"}

> Microsoft says they have discovered new attacks conducted by the Russian state-sponsored Nobelium hacking group, including a hacked Microsoft support agent's computer that exposed customer's subscription information. [...]
