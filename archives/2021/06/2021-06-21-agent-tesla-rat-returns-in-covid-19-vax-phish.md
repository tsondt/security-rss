Title: Agent Tesla RAT Returns in COVID-19 Vax Phish
Date: 2021-06-21T17:20:24+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: agent-tesla-rat-returns-in-covid-19-vax-phish

[Source](https://threatpost.com/agent-tesla-covid-vax-phish/167082/){:target="_blank" rel="noopener"}

> An unsophisticated campaign shows that the pandemic still has long legs when it comes to being social-engineering bait. [...]
