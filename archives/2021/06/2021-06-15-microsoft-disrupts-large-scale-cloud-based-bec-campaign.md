Title: Microsoft Disrupts Large-Scale, Cloud-Based BEC Campaign
Date: 2021-06-15T16:46:53+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Web Security
Slug: microsoft-disrupts-large-scale-cloud-based-bec-campaign

[Source](https://threatpost.com/microsoft-disrupts-cloud-bec-campaign/166937/){:target="_blank" rel="noopener"}

> Varied cloud infrastructure was used to phish email credentials, monitor for and forward finance-related messages and automate operations. [...]
