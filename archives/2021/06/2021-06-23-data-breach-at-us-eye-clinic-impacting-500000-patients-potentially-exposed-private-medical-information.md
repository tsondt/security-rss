Title: Data breach at US eye clinic impacting 500,000 patients potentially exposed private medical information
Date: 2021-06-23T14:03:25+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-at-us-eye-clinic-impacting-500000-patients-potentially-exposed-private-medical-information

[Source](https://portswigger.net/daily-swig/data-breach-at-us-eye-clinic-impacting-500-000-patients-potentially-exposed-private-medical-information){:target="_blank" rel="noopener"}

> Organization blames ‘targeted cyber-attack’ [...]
