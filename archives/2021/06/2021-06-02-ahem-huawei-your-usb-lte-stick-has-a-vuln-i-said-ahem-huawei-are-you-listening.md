Title: Ahem, Huawei, your USB LTE stick has a vuln. I SAID AHEM, Huawei, are you listening?
Date: 2021-06-02T18:35:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ahem-huawei-your-usb-lte-stick-has-a-vuln-i-said-ahem-huawei-are-you-listening

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/02/huawei_lte_usb_stick_vulnerability/){:target="_blank" rel="noopener"}

> Embarrassing flaw in E3372 device finally patched Huawei has belatedly fixed a mild vulnerability in a USB connectivity dongle spotted by Trustwave after The Register intervened.... [...]
