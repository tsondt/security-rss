Title: Watchdog bans crypto super-exchange Binance from 'regulated activities' in the UK
Date: 2021-06-29T10:44:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: watchdog-bans-crypto-super-exchange-binance-from-regulated-activities-in-the-uk

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/29/binance_fca_ban/){:target="_blank" rel="noopener"}

> But Brits can seemingly keep using the website The UK's financial watchdog has fired a warning shot across the bow of Binance, and ordered it to place a notice on binance.com scaring off Brit crypto fans.... [...]
