Title: Critical VMware Carbon Black Bug Allows Authentication Bypass
Date: 2021-06-24T15:31:31+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities
Slug: critical-vmware-carbon-black-bug-allows-authentication-bypass

[Source](https://threatpost.com/vmware-carbon-black-authentication-bypass/167226/){:target="_blank" rel="noopener"}

> The 9.4-rated bug in AppC could give attackers admin rights, no authentication required, letting them attack anything from PoS to industrial control systems. [...]
