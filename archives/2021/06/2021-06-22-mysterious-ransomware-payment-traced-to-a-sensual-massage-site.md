Title: Mysterious ransomware payment traced to a sensual massage site
Date: 2021-06-22T10:09:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: mysterious-ransomware-payment-traced-to-a-sensual-massage-site

[Source](https://www.bleepingcomputer.com/news/security/mysterious-ransomware-payment-traced-to-a-sensual-massage-site/){:target="_blank" rel="noopener"}

> ​A ransomware targeting an Israeli company has led researchers to track a portion of a ransom payment to a website promoting sensual massages. [...]
