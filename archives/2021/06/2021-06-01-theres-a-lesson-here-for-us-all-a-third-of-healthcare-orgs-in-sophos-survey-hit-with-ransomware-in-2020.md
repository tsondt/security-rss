Title: There's a lesson here for us all: A third of healthcare orgs in Sophos survey 'hit with ransomware in 2020'
Date: 2021-06-01T18:00:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: theres-a-lesson-here-for-us-all-a-third-of-healthcare-orgs-in-sophos-survey-hit-with-ransomware-in-2020

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/01/healthcare_orgs_ransomware_sophos/){:target="_blank" rel="noopener"}

> What’s the remedy? Read on... Promo The thought of ransomware gripping the corporate systems we manage is enough to give any of us sleepless nights. The thought of a ransomware attack crippling the healthcare infrastructure all of us rely on is terrifying.... [...]
