Title: The Week in Ransomware - June 4th 2021 - Where's the beef?
Date: 2021-06-04T19:21:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-june-4th-2021-wheres-the-beef

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-4th-2021-wheres-the-beef/){:target="_blank" rel="noopener"}

> Ransomware has continued to be part of the 24-hour news cycle as another significant attack against critical infrastructure took place this week. [...]
