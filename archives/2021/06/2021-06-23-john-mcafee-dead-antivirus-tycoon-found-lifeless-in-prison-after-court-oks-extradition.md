Title: John McAfee dead: Antivirus tycoon found lifeless in prison after court OKs extradition
Date: 2021-06-23T19:52:44+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: john-mcafee-dead-antivirus-tycoon-found-lifeless-in-prison-after-court-oks-extradition

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/23/john_mcafee_dead/){:target="_blank" rel="noopener"}

> UK-born wild man of infosec faced trial in America for tax evasion John McAfee was found dead in his cell in a Barcelona prison on Wednesday, according to the Catalan justice department.... [...]
