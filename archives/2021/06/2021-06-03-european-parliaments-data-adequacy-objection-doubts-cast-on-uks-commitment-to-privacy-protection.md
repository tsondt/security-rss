Title: European Parliament's data adequacy objection: Doubts cast on UK's commitment to privacy protection
Date: 2021-06-03T08:30:07+00:00
Author: Adam Bowering
Category: The Register
Tags: 
Slug: european-parliaments-data-adequacy-objection-doubts-cast-on-uks-commitment-to-privacy-protection

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/03/uk_data_protection_eu_parl/){:target="_blank" rel="noopener"}

> Plus: Judgment in immigration exemption case makes things worse Comment Almost two weeks ago, the European Parliament took the step of objecting to the European Commission's decision to grant the UK data adequacy.... [...]
