Title: Ex-NSA bigwig Chris Inglis appointed America's national cyber director by Senate
Date: 2021-06-21T20:08:18+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: ex-nsa-bigwig-chris-inglis-appointed-americas-national-cyber-director-by-senate

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/21/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Impact of ransomware payments, CVS database not secured In brief Chris Inglis was last week appointed America’s national cyber director, responsible for coordinating the government’s computer security strategy and defending its networks. The former deputy director at the NSA, who spent nearly three decades at the agency, was approved by the Senate on Thursday.... [...]
