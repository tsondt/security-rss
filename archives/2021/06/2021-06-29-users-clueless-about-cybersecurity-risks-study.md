Title: Users Clueless About Cybersecurity Risks: Study
Date: 2021-06-29T19:49:24+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: users-clueless-about-cybersecurity-risks-study

[Source](https://threatpost.com/users-clueless-cybersecurity-risks-study/167404/){:target="_blank" rel="noopener"}

> The return to offices, coupled with uninformed users (including IT pros) has teed up an unprecedented risk of enterprise attack. [...]
