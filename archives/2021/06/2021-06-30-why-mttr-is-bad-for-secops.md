Title: Why MTTR is Bad for SecOps
Date: 2021-06-30T16:28:48+00:00
Author: Kerry Matre
Category: Threatpost
Tags: Breach;Cloud Security;Critical Infrastructure;Hacks;InfoSec Insider;Malware;Vulnerabilities;Web Security
Slug: why-mttr-is-bad-for-secops

[Source](https://threatpost.com/mttr-bad-secops/167440/){:target="_blank" rel="noopener"}

> Kerry Matre, senior director at Mandiant, discusses the appropriate metrics to use to measure SOC and analyst performance, and how MTTR leads to bad behavior. [...]
