Title: Intentional Flaw in GPRS Encryption Algorithm GEA-1
Date: 2021-06-17T18:51:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;algorithms;backdoors;cryptanalysis;encryption
Slug: intentional-flaw-in-gprs-encryption-algorithm-gea-1

[Source](https://www.schneier.com/blog/archives/2021/06/intentional-flaw-in-gprs-encryption-algorithm-gea-1.html){:target="_blank" rel="noopener"}

> General Packet Radio Service (GPRS) is a mobile data standard that was widely used in the early 2000s. The first encryption algorithm for that standard was GEA-1, a stream cipher built on three linear-feedback shift registers and a non-linear combining function. Although the algorithm has a 64-bit key, the effective key length is only 40 bits, due to “an exceptional interaction of the deployed LFSRs and the key initialization, which is highly unlikely to occur by chance.” GEA-1 was designed by the European Telecommunications Standards Institute in 1998. ETSI was — and maybe still is — under the auspices of [...]
