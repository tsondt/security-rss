Title: How Zebra Technologies manages security & risk using Security Command Center
Date: 2021-06-11T16:30:00+00:00
Author: Brad Skibitzki
Category: GCP Security
Tags: Google Cloud Platform;Customers;Identity & Security
Slug: how-zebra-technologies-manages-security-risk-using-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/how-zebra-technologies-uses-security-command-center/){:target="_blank" rel="noopener"}

> Zebra Technologies enables businesses around the world to gain a performance edge - our products, software, services, analytics and solutions are used principally in the manufacturing, retail, healthcare, transportation & logistics and public sectors. With more than 10,000 partners across 100 countries, our businesses and workflows operate across the interconnected world. Many of these workflows run through Google Cloud, where we host environments for both our own enterprise use and for our customer solutions. As CISO for Zebra, it's my team’s responsibility to keep our organization’s network and data secure. We must ensure the confidentiality, integrity, and availability of all [...]
