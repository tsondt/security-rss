Title: European Commission to hold ‘hackathon’ in bid to secure data sharing across EU
Date: 2021-06-22T15:44:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: european-commission-to-hold-hackathon-in-bid-to-secure-data-sharing-across-eu

[Source](https://portswigger.net/daily-swig/european-commission-to-hold-hackathon-in-bid-to-secure-data-sharing-across-eu){:target="_blank" rel="noopener"}

> Week-long competition asks for hackers’ help in solving ‘complex’ issue [...]
