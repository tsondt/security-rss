Title: Friday Squid Blogging: Fossil of Squid Eating and Being Eaten
Date: 2021-06-11T21:18:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-fossil-of-squid-eating-and-being-eaten

[Source](https://www.schneier.com/blog/archives/2021/06/friday-squid-blogging-fossil-of-squid-eating-and-being-eaten.html){:target="_blank" rel="noopener"}

> We now have a fossil of a squid eating a crustacean while it is being eaten by a shark. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
