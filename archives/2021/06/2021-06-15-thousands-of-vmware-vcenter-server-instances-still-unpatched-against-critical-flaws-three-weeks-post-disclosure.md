Title: Thousands of VMWare vCenter Server instances still unpatched against critical flaws three weeks post-disclosure
Date: 2021-06-15T13:52:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: thousands-of-vmware-vcenter-server-instances-still-unpatched-against-critical-flaws-three-weeks-post-disclosure

[Source](https://portswigger.net/daily-swig/thousands-of-vmware-vcenter-server-instances-still-unpatched-against-critical-flaws-three-weeks-post-disclosure){:target="_blank" rel="noopener"}

> Vulnerabilities could allow an attacker to execute commands with unrestricted privileges [...]
