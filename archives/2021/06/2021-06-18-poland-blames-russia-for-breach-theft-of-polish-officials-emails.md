Title: Poland blames Russia for breach, theft of Polish officials' emails
Date: 2021-06-18T13:40:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: poland-blames-russia-for-breach-theft-of-polish-officials-emails

[Source](https://www.bleepingcomputer.com/news/security/poland-blames-russia-for-breach-theft-of-polish-officials-emails/){:target="_blank" rel="noopener"}

> Poland's deputy prime minister Jarosław Kaczyński says last week's breach of multiple Polish officials' private email accounts was carried out from servers within the Russian Federation. [...]
