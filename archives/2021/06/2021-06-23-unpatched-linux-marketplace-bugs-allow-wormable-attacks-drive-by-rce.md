Title: Unpatched Linux Marketplace Bugs Allow Wormable Attacks, Drive-By RCE
Date: 2021-06-23T11:58:06+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: unpatched-linux-marketplace-bugs-allow-wormable-attacks-drive-by-rce

[Source](https://threatpost.com/unpatched-linux-marketplace-bugs-rce/167155/){:target="_blank" rel="noopener"}

> A pair of zero-days affecting Pling-based marketplaces could allow for some ugly attacks on unsuspecting Linux enthusiasts -- with no patches in sight. [...]
