Title: EU pushes plans for Joint Cyber Unit in fight against increased cyber-attacks
Date: 2021-06-23T15:58:25+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: eu-pushes-plans-for-joint-cyber-unit-in-fight-against-increased-cyber-attacks

[Source](https://portswigger.net/daily-swig/eu-pushes-plans-for-joint-cyber-unit-in-fight-against-increased-cyber-attacks){:target="_blank" rel="noopener"}

> Plans to form a cross-border rapid response security team are stepped up [...]
