Title: Microsoft Gets Second Shot at Banning hiQ from Scraping LinkedIn User Data
Date: 2021-06-15T12:39:12+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Privacy
Slug: microsoft-gets-second-shot-at-banning-hiq-from-scraping-linkedin-user-data

[Source](https://threatpost.com/court-linkedin-data-scraping/166927/){:target="_blank" rel="noopener"}

> Decision throws out previous ruling in favor of hiQ Labs that prevented Microsoft’s business networking platform to forbid the company from harvesting public info from user profiles. [...]
