Title: Critical Palo Alto Cyber-Defense Bug Allows Remote ‘War Room’ Access
Date: 2021-06-23T15:39:06+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: critical-palo-alto-cyber-defense-bug-allows-remote-war-room-access

[Source](https://threatpost.com/critical-palo-alto-bug-remote-war-room/167169/){:target="_blank" rel="noopener"}

> Remote, unauthenticated cyberattackers can infiltrate and take over the Cortex XSOAR platform, which anchors unified threat intelligence and incident responses. [...]
