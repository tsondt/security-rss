Title: Data leak marketplace pressures victims by emailing competitors
Date: 2021-06-21T11:13:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: data-leak-marketplace-pressures-victims-by-emailing-competitors

[Source](https://www.bleepingcomputer.com/news/security/data-leak-marketplace-pressures-victims-by-emailing-competitors/){:target="_blank" rel="noopener"}

> The Marketo data theft marketplace is applying maximum pressure on victims by emailing their competitors and offering sample packs of the stolen data. [...]
