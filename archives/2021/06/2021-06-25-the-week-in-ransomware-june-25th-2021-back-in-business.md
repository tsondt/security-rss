Title: The Week in Ransomware - June 25th 2021 - Back in Business
Date: 2021-06-25T19:09:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-june-25th-2021-back-in-business

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-25th-2021-back-in-business/){:target="_blank" rel="noopener"}

> It has been relatively quiet this week, with few attacks revealed and few new ransomware variants released. However, some interesting information came out that we have summarized below. [...]
