Title: Paul van Oorschot’s Computer Security and the Internet
Date: 2021-06-17T11:25:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: books;computer security
Slug: paul-van-oorschots-computer-security-and-the-internet

[Source](https://www.schneier.com/blog/archives/2021/06/paul-van-oorschots-computer-security-and-the-internet.html){:target="_blank" rel="noopener"}

> Paul van Oorschot’s webpage contains a complete copy of his book: Computer Security and the Internet: Tools and Jewels. It’s worth reading. [...]
