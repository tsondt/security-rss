Title: Zero-Day Used to Wipe My Book Live Devices
Date: 2021-06-30T16:08:46+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;Web Security
Slug: zero-day-used-to-wipe-my-book-live-devices

[Source](https://threatpost.com/zero-day-wipe-my-book-live/167422/){:target="_blank" rel="noopener"}

> Threat actors may have been duking it out for control of the compromised devices, first using a 2018 RCE, then password-protecting a new vulnerability. [...]
