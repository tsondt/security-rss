Title: EPUB vulnerabilities: Electronic reading systems riddled with browser-like flaws
Date: 2021-06-01T11:30:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: epub-vulnerabilities-electronic-reading-systems-riddled-with-browser-like-flaws

[Source](https://portswigger.net/daily-swig/epub-vulnerabilities-electronic-reading-systems-riddled-with-browser-like-flaws){:target="_blank" rel="noopener"}

> Read on for page-turning pwnage [...]
