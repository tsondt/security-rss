Title: APNIC left a dump from its Whois SQL database in a public Google Cloud bucket
Date: 2021-06-22T01:08:58+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: apnic-left-a-dump-from-its-whois-sql-database-in-a-public-google-cloud-bucket

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/22/apnic_whois_data_exposed/){:target="_blank" rel="noopener"}

> File was supposed to be private. It was not. And it was out in the open for months The Asia Pacific Network Information Centre (APNIC), the internet registry for the region, has admitted it left at least a portion of its Whois SQL database, which contains sensitive information, facing the public internet for three months.... [...]
