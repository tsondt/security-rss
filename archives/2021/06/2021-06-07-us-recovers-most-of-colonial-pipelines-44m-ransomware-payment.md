Title: US recovers most of Colonial Pipeline's $4.4M ransomware payment
Date: 2021-06-07T15:28:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: us-recovers-most-of-colonial-pipelines-44m-ransomware-payment

[Source](https://www.bleepingcomputer.com/news/security/us-recovers-most-of-colonial-pipelines-44m-ransomware-payment/){:target="_blank" rel="noopener"}

> The US Department of Justice has recovered the majority of the $4.4 million ransom payment paid by Colonial Pipeline to the DarkSide ransomware operation. [...]
