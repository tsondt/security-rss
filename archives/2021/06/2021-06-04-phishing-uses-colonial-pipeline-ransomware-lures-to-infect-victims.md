Title: Phishing uses Colonial Pipeline ransomware lures to infect victims
Date: 2021-06-04T14:51:32-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: phishing-uses-colonial-pipeline-ransomware-lures-to-infect-victims

[Source](https://www.bleepingcomputer.com/news/security/phishing-uses-colonial-pipeline-ransomware-lures-to-infect-victims/){:target="_blank" rel="noopener"}

> The recent ransomware attack on Colonial Pipeline inspired a threat actor to create create a new phishing lure to trick victims into downloading malicious files. [...]
