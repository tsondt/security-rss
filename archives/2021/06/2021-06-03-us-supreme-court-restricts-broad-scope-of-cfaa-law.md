Title: US Supreme Court restricts broad scope of CFAA law
Date: 2021-06-03T18:44:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Government;Legal
Slug: us-supreme-court-restricts-broad-scope-of-cfaa-law

[Source](https://www.bleepingcomputer.com/news/security/us-supreme-court-restricts-broad-scope-of-cfaa-law/){:target="_blank" rel="noopener"}

> Today, the US Supreme Court restricted the scope of the federal Computer Fraud and Abuse Act after overturning the conviction of a Georgia police officer who searched a police database for money. [...]
