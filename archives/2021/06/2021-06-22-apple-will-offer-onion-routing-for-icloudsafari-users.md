Title: Apple Will Offer Onion Routing for iCloud/Safari Users
Date: 2021-06-22T11:54:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: anonymity;Apple;cloud computing;Safari;Tor
Slug: apple-will-offer-onion-routing-for-icloudsafari-users

[Source](https://www.schneier.com/blog/archives/2021/06/apple-will-offer-onion-routing-for-icloud-safari-users.html){:target="_blank" rel="noopener"}

> At this year’s Apple Worldwide Developer Conference, Apple announced something called “iCloud Private Relay.” That’s basically its private version of onion routing, which is what Tor does. Privacy Relay is built into both the forthcoming iOS and MacOS versions, but it will only work if you’re an iCloud Plus subscriber and you have it enabled from within your iCloud settings. Once it’s enabled and you open Safari to browse, Private Relay splits up two pieces of information that — when delivered to websites together as normal — could quickly identify you. Those are your IP address (who and exactly where [...]
