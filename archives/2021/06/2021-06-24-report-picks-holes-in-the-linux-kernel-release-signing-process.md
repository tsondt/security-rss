Title: Report picks holes in the Linux kernel release signing process
Date: 2021-06-24T16:28:05+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: report-picks-holes-in-the-linux-kernel-release-signing-process

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/24/report_picks_holes_in_the/){:target="_blank" rel="noopener"}

> Security procedures need documenting, improving, and mandating - though they're better than they used to be A report looking into the security of the Linux kernel's release signing process has highlighted a range of areas for improvement, from failing to mandate the use of hardware security keys for authentication to use of static keys for SSH access.... [...]
