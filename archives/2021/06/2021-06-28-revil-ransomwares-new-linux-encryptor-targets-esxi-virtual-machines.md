Title: REvil ransomware's new Linux encryptor targets ESXi virtual machines
Date: 2021-06-28T17:26:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Linux
Slug: revil-ransomwares-new-linux-encryptor-targets-esxi-virtual-machines

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomwares-new-linux-encryptor-targets-esxi-virtual-machines/){:target="_blank" rel="noopener"}

> The REvil ransomware operation is now using a Linux encryptor that targets and encrypts Vmware ESXi virtual machines. [...]
