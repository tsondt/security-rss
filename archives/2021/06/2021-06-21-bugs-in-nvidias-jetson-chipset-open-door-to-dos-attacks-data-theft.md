Title: Bugs in NVIDIA’s Jetson Chipset Open Door to DoS Attacks, Data Theft
Date: 2021-06-21T20:21:36+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cryptography;IoT;Vulnerabilities
Slug: bugs-in-nvidias-jetson-chipset-open-door-to-dos-attacks-data-theft

[Source](https://threatpost.com/nvidia-jetson-chipset-dos-data-theft/167093/){:target="_blank" rel="noopener"}

> Chipmaker patches nine high-severity bugs in its Jetson SoC framework tied to the way it handles low-level cryptographic algorithms. [...]
