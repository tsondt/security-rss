Title: Amazon to share your Internet with neighbors on Tuesday - How to opt out
Date: 2021-06-06T09:16:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Technology
Slug: amazon-to-share-your-internet-with-neighbors-on-tuesday-how-to-opt-out

[Source](https://www.bleepingcomputer.com/news/technology/amazon-to-share-your-internet-with-neighbors-on-tuesday-how-to-opt-out/){:target="_blank" rel="noopener"}

> Amazon will be launching the Amazon Sidewalk service on Tuesday that automatically opts-in your Echo and Ring devices into a new feature that shares your Internet with your neighbors. Here's more about this new feature and how to opt-out of sharing your bandwidth with other Amazon devices. [...]
