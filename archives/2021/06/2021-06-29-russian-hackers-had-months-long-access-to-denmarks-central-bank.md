Title: Russian hackers had months-long access to Denmark's central bank
Date: 2021-06-29T13:48:21-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: russian-hackers-had-months-long-access-to-denmarks-central-bank

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-had-months-long-access-to-denmarks-central-bank/){:target="_blank" rel="noopener"}

> Russian state hackers compromised Denmark's central bank (Danmarks Nationalbank) and planted malware that gave them access to the network for more than half a year without being detected. [...]
