Title: International law enforcement op nukes Russian-language DoubleVPN service allegedly favoured by cybercriminals
Date: 2021-06-30T19:01:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: international-law-enforcement-op-nukes-russian-language-doublevpn-service-allegedly-favoured-by-cybercriminals

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/30/doublevpn_police_takedown/){:target="_blank" rel="noopener"}

> Vendor claimed not to log user data – we'll see Europol, the US Department of Justice, and Britain's National Crime Agency have taken down a VPN service they claimed was mainly used by criminals – boasting that they hoovered up "personal information, logs and statistics" from the site.... [...]
