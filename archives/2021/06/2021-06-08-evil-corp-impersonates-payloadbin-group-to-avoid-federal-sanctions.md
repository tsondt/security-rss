Title: Evil Corp Impersonates PayloadBin Group to Avoid Federal Sanctions
Date: 2021-06-08T12:30:14+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware
Slug: evil-corp-impersonates-payloadbin-group-to-avoid-federal-sanctions

[Source](https://threatpost.com/evil-corp-impersonates-payloadbin/166710/){:target="_blank" rel="noopener"}

> The cybercriminals try to pin new ransomware on Babuk Locker in an effort to fly under the radar of an ongoing FBI investigation. [...]
