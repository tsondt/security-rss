Title: NSA shares guidance on securing voice, video communications
Date: 2021-06-17T14:00:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: nsa-shares-guidance-on-securing-voice-video-communications

[Source](https://www.bleepingcomputer.com/news/security/nsa-shares-guidance-on-securing-voice-video-communications/){:target="_blank" rel="noopener"}

> The National Security Agency (NSA) has shared mitigations and best practices that systems administrators should follow when securing Unified Communications (UC) and Voice and Video over IP (VVoIP) call-processing systems. [...]
