Title: A well-meaning feature leaves millions of Dell PCs vulnerable
Date: 2021-06-26T10:15:39+00:00
Author: Eric Bangeman
Category: Ars Technica
Tags: Biz & IT;dell;firmware;PCs;Windows
Slug: a-well-meaning-feature-leaves-millions-of-dell-pcs-vulnerable

[Source](https://arstechnica.com/?p=1776495){:target="_blank" rel="noopener"}

> Enlarge / Dell has released a patch for a set of vulnerabilities that left as many as 30 million devices exposed. (credit: Artur Widak | Getty Images) Researchers have known for years about security issues with the foundational computer code known as firmware. It's often riddled with vulnerabilities, it's difficult to update with patches, and it's increasingly the target of real-world attacks. Now a well-intentioned mechanism to easily update the firmware of Dell computers is itself vulnerable as the result of four rudimentary bugs. And these vulnerabilities could be exploited to gain full access to target devices. The new findings [...]
