Title: Huawei flings open the doors of its third privacy and security transparency centre
Date: 2021-06-09T23:03:10+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: huawei-flings-open-the-doors-of-its-third-privacy-and-security-transparency-centre

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/09/huawei_opens_the_doors_on/){:target="_blank" rel="noopener"}

> The first based in China Huawei has opened another cyber security centre and, despite facing a crisis of trust in the West, has chosen to do so for the first time in its Chinese heartland.... [...]
