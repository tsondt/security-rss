Title: FBI drops subpoena to identify readers of USA Today article about shootout with agents
Date: 2021-06-08T00:56:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: fbi-drops-subpoena-to-identify-readers-of-usa-today-article-about-shootout-with-agents

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/08/fbi_usatoday_subpoena/){:target="_blank" rel="noopener"}

> Feds, facing pushback over constitutional concerns, say they no longer need the data after all The FBI on Saturday withdrew a subpoena issued to USA Today's parent company Gannett in April to find out who read an online news story published in February about a shootout that led to the deaths of two FBI agents and the wounding of three others.... [...]
