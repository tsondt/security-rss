Title: Spam Downpour Drips New IcedID Banking Trojan Variant
Date: 2021-06-25T01:05:45+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: spam-downpour-drips-new-icedid-banking-trojan-variant

[Source](https://threatpost.com/spam-icedid-banking-trojan-variant/167250/){:target="_blank" rel="noopener"}

> The primarily IcedID-flavored banking trojan spam campaigns were coming in at a fever pitch: Spikes hit more than 100 detections a day. [...]
