Title: Intel sticks another nail in the coffin of TSX with feature-disabling microcode update
Date: 2021-06-29T18:43:06+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: intel-sticks-another-nail-in-the-coffin-of-tsx-with-feature-disabling-microcode-update

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/29/intel_tsx_disabled/){:target="_blank" rel="noopener"}

> Plus: 10nm Sapphire Rapids Xeon chips delayed to Q1 2022 Intel has officially sounded the death knell for Transactional Synchronisation Extensions (TSX) on a selection of processors from Skylake to Coffee Lake – a security-enhancing move which will have an oversized performance impact on certain workloads.... [...]
