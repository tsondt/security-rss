Title: Encrypt global data client-side with AWS KMS multi-Region keys
Date: 2021-06-16T22:40:32+00:00
Author: Jeremy Stieglitz
Category: AWS Security
Tags: Advanced (300);AWS Key Management Service;Security, Identity, & Compliance;Availability;AWS KMS;Disaster Recovery;Encryption;Global tables;Security Blog
Slug: encrypt-global-data-client-side-with-aws-kms-multi-region-keys

[Source](https://aws.amazon.com/blogs/security/encrypt-global-data-client-side-with-aws-kms-multi-region-keys/){:target="_blank" rel="noopener"}

> Today, AWS Key Management Service (AWS KMS) is introducing multi-Region keys, a new capability that lets you replicate keys from one Amazon Web Services (AWS) Region into another. Multi-Region keys are designed to simplify management of client-side encryption when your encrypted data has to be copied into other Regions for disaster recovery or is replicated [...]
