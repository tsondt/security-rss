Title: NVIDIA Patches High-Severity GeForce Spoof-Attack Bug
Date: 2021-06-28T20:38:29+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: nvidia-patches-high-severity-geforce-spoof-attack-bug

[Source](https://threatpost.com/nvidia-high-severity-geforce-spoof-bug/167345/){:target="_blank" rel="noopener"}

> A vulnerability in NVIDIA’s GeForce Experience software opens the door to remote data access, manipulation and deletion. [...]
