Title: Apple Hurries Patches for Safari Bugs Under Active Attack
Date: 2021-06-15T11:43:20+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: apple-hurries-patches-for-safari-bugs-under-active-attack

[Source](https://threatpost.com/apple-patch-safari-active-attack/166922/){:target="_blank" rel="noopener"}

> Apple patched two bugs impacting its Safari browser WebKit engine that it said are actively being exploited. [...]
