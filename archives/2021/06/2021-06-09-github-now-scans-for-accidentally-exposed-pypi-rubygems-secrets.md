Title: GitHub now scans for accidentally-exposed PyPI, RubyGems secrets
Date: 2021-06-09T03:24:27-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: github-now-scans-for-accidentally-exposed-pypi-rubygems-secrets

[Source](https://www.bleepingcomputer.com/news/security/github-now-scans-for-accidentally-exposed-pypi-rubygems-secrets/){:target="_blank" rel="noopener"}

> GitHub has recently expanded its secrets scanning capabilities to repositories containing PyPI and RubyGems registry secrets. The move helps protect millions of applications built by Ruby and Python developers who may inadvertently be committing secrets and credentials to their public GitHub repos. [...]
