Title: Cisco Smart Switches Riddled with Severe Security Holes
Date: 2021-06-17T19:30:46+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: cisco-smart-switches-riddled-with-severe-security-holes

[Source](https://threatpost.com/cisco-smart-switches-security-holes/167031/){:target="_blank" rel="noopener"}

> The intro-level networking gear for SMBs could allow remote attacks designed to steal information, drop malware and disrupt operations. [...]
