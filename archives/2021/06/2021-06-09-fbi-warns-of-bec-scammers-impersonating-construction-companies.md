Title: FBI warns of BEC scammers impersonating construction companies
Date: 2021-06-09T15:39:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-bec-scammers-impersonating-construction-companies

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-bec-scammers-impersonating-construction-companies/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned private sector companies of scammers impersonating construction companies in business email compromise (BEC) attacks targeting organizations from multiple US critical infrastructure sectors. [...]
