Title: 'I put the interests of the country first': Colonial Pipeline CEO on why oil biz paid off ransomware crooks
Date: 2021-06-09T16:28:10+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: i-put-the-interests-of-the-country-first-colonial-pipeline-ceo-on-why-oil-biz-paid-off-ransomware-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/09/old_vpn_colonial_pipeline/){:target="_blank" rel="noopener"}

> Mandiant investigation says crims gained access through legacy VPN The boss of Colonial Pipeline has appeared before a Senate Committee to explain the events which led to US East Coast fuel supplies running dry last month and some $5m being handed over in ransom.... [...]
