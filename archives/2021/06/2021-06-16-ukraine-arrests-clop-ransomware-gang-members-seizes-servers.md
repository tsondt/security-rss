Title: Ukraine arrests Clop ransomware gang members, seizes servers
Date: 2021-06-16T08:59:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ukraine-arrests-clop-ransomware-gang-members-seizes-servers

[Source](https://www.bleepingcomputer.com/news/security/ukraine-arrests-clop-ransomware-gang-members-seizes-servers/){:target="_blank" rel="noopener"}

> Ukrainian law enforcement arrested cybercriminals associated with the Clop ransomware gang and shut down infrastructure used in attacks targeting victims worldwide since at least 2019. [...]
