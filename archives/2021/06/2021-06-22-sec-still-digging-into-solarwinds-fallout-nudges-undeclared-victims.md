Title: SEC still digging into SolarWinds fallout, nudges undeclared victims
Date: 2021-06-22T22:45:08+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: sec-still-digging-into-solarwinds-fallout-nudges-undeclared-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/22/sec_continues_to_probe_solarwinds/){:target="_blank" rel="noopener"}

> US markets watchdog sniffs around potential insider trading, data violations relating to hack US markets watchdog the Securities and Exchanges Commission (SEC) has begun a probe into last year's SolarWinds cyberattack, in a bid to find out who else might have been compromised.... [...]
