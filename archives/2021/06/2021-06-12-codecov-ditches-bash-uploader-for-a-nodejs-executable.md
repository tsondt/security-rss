Title: Codecov ditches Bash Uploader for a NodeJS executable
Date: 2021-06-12T08:44:04-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: codecov-ditches-bash-uploader-for-a-nodejs-executable

[Source](https://www.bleepingcomputer.com/news/security/codecov-ditches-bash-uploader-for-a-nodejs-executable/){:target="_blank" rel="noopener"}

> Codecov has now introduced a new cross-platform uploader meant to replace its former Bash Uploader. The new uploader is available as a static binary executable currently supporting the Windows, Linux, and macOS operating systems. However, some have raised concerns with the new uploader and the many dependencies it contains. [...]
