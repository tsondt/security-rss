Title: Avaddon ransomware's exit sheds light on victim landscape
Date: 2021-06-15T17:53:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: avaddon-ransomwares-exit-sheds-light-on-victim-landscape

[Source](https://www.bleepingcomputer.com/news/security/avaddon-ransomwares-exit-sheds-light-on-victim-landscape/){:target="_blank" rel="noopener"}

> A new report analyzes the recently released Avaddon ransomware decryption keys to shed light on the types of victims targeted by the threat actors and potential revenue they generated throughout their operation. [...]
