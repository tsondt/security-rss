Title: Seven-year-old make-me-root bug in Linux service polkit patched
Date: 2021-06-11T06:28:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: seven-year-old-make-me-root-bug-in-linux-service-polkit-patched

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/11/linux_polkit_package_patched/){:target="_blank" rel="noopener"}

> Error handling? Nah, let's just unlock everything and be done with it A seven-year-old privilege escalation vulnerability that's been lurking in several Linux distributions was patched last week in a coordinated disclosure.... [...]
