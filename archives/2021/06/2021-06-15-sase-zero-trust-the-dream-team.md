Title: SASE & Zero Trust: The Dream Team
Date: 2021-06-15T14:47:17+00:00
Author: Threatpost
Category: Threatpost
Tags: Podcasts;Sponsored;Web Security
Slug: sase-zero-trust-the-dream-team

[Source](https://threatpost.com/sase-zero-trust-the-dream-team/166880/){:target="_blank" rel="noopener"}

> Forcepoint’s Nico Fischbach, global CTO and VPE of SASE, and Chase Cunningham, chief strategy officer at Ericom Software, on using SASE to make Zero Trust real. [...]
