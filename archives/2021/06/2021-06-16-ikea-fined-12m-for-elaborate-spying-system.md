Title: IKEA Fined $1.2M for Elaborate ‘Spying System’
Date: 2021-06-16T18:38:28+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Privacy
Slug: ikea-fined-12m-for-elaborate-spying-system

[Source](https://threatpost.com/ikea-fined-spying-system/166991/){:target="_blank" rel="noopener"}

> A French court fined the furniture giant for illegal surveillance on 400 customers and staff. [...]
