Title: Supreme Court Limits Scope of Controversial Hacking Law
Date: 2021-06-04T14:15:16+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks
Slug: supreme-court-limits-scope-of-controversial-hacking-law

[Source](https://threatpost.com/court-limits-scope-hacking-law/166672/){:target="_blank" rel="noopener"}

> Judges rule that Georgia police officer did not violate CFAA when he accessed law-enforcement data in exchange for bribe money, a ruling that takes heat off ethical hackers. [...]
