Title: Microsoft warns of cryptomining attacks on Kubernetes clusters
Date: 2021-06-09T13:05:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency;Microsoft
Slug: microsoft-warns-of-cryptomining-attacks-on-kubernetes-clusters

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-cryptomining-attacks-on-kubernetes-clusters/){:target="_blank" rel="noopener"}

> Microsoft warns of an ongoing series of attacks compromising Kubernetes clusters running Kubeflow machine learning (ML) instances to deploy malicious containers that mine for Monero and Ethereum cryptocurrency. [...]
