Title: Intent redirection vulnerabilities in popular Android apps spotlight danger of dynamic code loading, warn researchers
Date: 2021-06-21T15:17:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: intent-redirection-vulnerabilities-in-popular-android-apps-spotlight-danger-of-dynamic-code-loading-warn-researchers

[Source](https://portswigger.net/daily-swig/intent-redirection-vulnerabilities-in-popular-android-apps-spotlight-danger-of-dynamic-code-loading-warn-researchers){:target="_blank" rel="noopener"}

> Bug could allow a malicious app to steal a plethora of sensitive data from user’s device [...]
