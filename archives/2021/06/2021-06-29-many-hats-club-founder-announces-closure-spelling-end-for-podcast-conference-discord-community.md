Title: Many Hats Club founder announces closure – spelling end for podcast, conference, Discord community
Date: 2021-06-29T13:09:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: many-hats-club-founder-announces-closure-spelling-end-for-podcast-conference-discord-community

[Source](https://portswigger.net/daily-swig/many-hats-club-founder-announces-closure-spelling-end-for-podcast-conference-discord-community){:target="_blank" rel="noopener"}

> The infosec community also raised $55,000 for charities and nonprofits [...]
