Title: Multiple vulnerabilities in WordPress plugin pose website remote code execution risk
Date: 2021-06-29T14:53:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: multiple-vulnerabilities-in-wordpress-plugin-pose-website-remote-code-execution-risk

[Source](https://portswigger.net/daily-swig/multiple-vulnerabilities-in-wordpress-plugin-pose-website-remote-code-execution-risk){:target="_blank" rel="noopener"}

> Quartet of critical web security flaws plague CMS software [...]
