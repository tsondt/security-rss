Title: Attackers Breach Microsoft Customer Service Accounts
Date: 2021-06-28T19:11:15+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Malware;Web Security
Slug: attackers-breach-microsoft-customer-service-accounts

[Source](https://threatpost.com/russian-attackers-breach-microsoft/167340/){:target="_blank" rel="noopener"}

> American IT companies and government have been targeted by the Nobelium state-sponsored group. [...]
