Title: Malicious PyPI packages hijack dev devices to mine cryptocurrency
Date: 2021-06-22T03:49:02-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: malicious-pypi-packages-hijack-dev-devices-to-mine-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/security/malicious-pypi-packages-hijack-dev-devices-to-mine-cryptocurrency/){:target="_blank" rel="noopener"}

> This week, multiple malicious packages were caught in the PyPI repository for Python projects that turned developers' workstations into cryptomining machines. [...]
