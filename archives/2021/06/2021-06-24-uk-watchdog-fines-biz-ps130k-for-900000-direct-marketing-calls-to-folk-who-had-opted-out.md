Title: UK watchdog fines biz £130k for 900,000+ direct marketing calls to folk who had opted out
Date: 2021-06-24T10:47:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: uk-watchdog-fines-biz-ps130k-for-900000-direct-marketing-calls-to-folk-who-had-opted-out

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/24/ico_colour_coat_fine/){:target="_blank" rel="noopener"}

> Colour Coat accused of lying, being rude and aggressive, and hanging up on cold-call victims A home improvement biz based in East Sussex is facing a fine of £130,000 for making upwards of 900,000 unsolicited marketing calls to individuals and businesses that had enrolled on the Telephone Preference Service (TPS).... [...]
