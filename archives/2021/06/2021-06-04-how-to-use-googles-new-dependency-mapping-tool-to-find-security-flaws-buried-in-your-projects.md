Title: How to use Google's new dependency mapping tool to find security flaws buried in your projects
Date: 2021-06-04T02:59:13+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: how-to-use-googles-new-dependency-mapping-tool-to-find-security-flaws-buried-in-your-projects

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/04/google_open_source_insights/){:target="_blank" rel="noopener"}

> Millions of Rust, JavaScript, Go, Maven repositories scanned and visualized Google has built an online tool that maps out all the dependencies in millions of open-source software libraries and flags up any unpatched vulnerabilities.... [...]
