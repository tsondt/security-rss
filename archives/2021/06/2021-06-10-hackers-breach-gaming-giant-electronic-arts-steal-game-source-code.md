Title: Hackers breach gaming giant Electronic Arts, steal game source code
Date: 2021-06-10T12:59:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Gaming
Slug: hackers-breach-gaming-giant-electronic-arts-steal-game-source-code

[Source](https://www.bleepingcomputer.com/news/security/hackers-breach-gaming-giant-electronic-arts-steal-game-source-code/){:target="_blank" rel="noopener"}

> Hackers have breached the network of gaming giant Electronic Arts (EA) and claim to have stolen roughly 750 GB of data, including game source code and debug tools. [...]
