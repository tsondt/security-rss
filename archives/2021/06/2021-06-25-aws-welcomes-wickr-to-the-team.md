Title: AWS welcomes Wickr to the team
Date: 2021-06-25T13:02:06+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements;Foundational (100);Learning Levels;Security, Identity, & Compliance;Security Blog
Slug: aws-welcomes-wickr-to-the-team

[Source](https://aws.amazon.com/blogs/security/aws-welcomes-wickr-to-the-team/){:target="_blank" rel="noopener"}

> We’re excited to share that AWS has acquired Wickr, an innovative company that has developed the industry’s most secure, end-to-end encrypted, communication technology. With Wickr, customers and partners benefit from advanced security features not available with traditional communications services – across messaging, voice and video calling, file sharing, and collaboration. This gives security conscious enterprises [...]
