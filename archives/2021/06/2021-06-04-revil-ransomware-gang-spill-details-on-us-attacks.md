Title: REvil Ransomware Gang Spill Details on US Attacks
Date: 2021-06-04T13:19:02+00:00
Author: Tom Spring
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: revil-ransomware-gang-spill-details-on-us-attacks

[Source](https://threatpost.com/revil-spill-details-us-attacks/166669/){:target="_blank" rel="noopener"}

> The REvil ransomware gang is interviewed on the Telegram channel called Russian OSINT. [...]
