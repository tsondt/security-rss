Title: Friday Squid Blogging: Colossal Squid Photographed off the Coast of Antarctica
Date: 2021-06-25T21:20:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-colossal-squid-photographed-off-the-coast-of-antarctica

[Source](https://www.schneier.com/blog/archives/2021/06/friday-squid-blogging-colossal-squid-photographed-off-the-coast-of-antarctica.html){:target="_blank" rel="noopener"}

> Wow. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
