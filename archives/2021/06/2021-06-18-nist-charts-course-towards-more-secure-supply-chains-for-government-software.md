Title: NIST charts course towards more secure supply chains for government software
Date: 2021-06-18T15:00:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nist-charts-course-towards-more-secure-supply-chains-for-government-software

[Source](https://portswigger.net/daily-swig/nist-charts-course-towards-more-secure-supply-chains-for-government-software){:target="_blank" rel="noopener"}

> Preliminary ideas emerge for more effective, ecosystem-wide standards and guidelines [...]
