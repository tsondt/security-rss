Title: JBS paid $11 million to REvil ransomware, $22.5M first demanded
Date: 2021-06-10T01:25:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: jbs-paid-11-million-to-revil-ransomware-225m-first-demanded

[Source](https://www.bleepingcomputer.com/news/security/jbs-paid-11-million-to-revil-ransomware-225m-first-demanded/){:target="_blank" rel="noopener"}

> JBS, the world's largest beef producer, has confirmed that they paid an $11 million ransom after the REvil ransomware operation initially demanded $22.5 million. [...]
