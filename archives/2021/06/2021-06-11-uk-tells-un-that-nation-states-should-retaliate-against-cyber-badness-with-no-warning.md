Title: UK tells UN that nation-states should retaliate against cyber badness with no warning
Date: 2021-06-11T16:44:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-tells-un-that-nation-states-should-retaliate-against-cyber-badness-with-no-warning

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/11/uk_ungge_cyber_norms_submission/){:target="_blank" rel="noopener"}

> Even nuclear missile attacks came with a 4-minute heads-up Comment Britain has told the UN that international cyber law should allow zero-notice digital punishment directed at countries that attack others' infrastructure.... [...]
