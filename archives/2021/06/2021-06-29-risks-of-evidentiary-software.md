Title: Risks of Evidentiary Software
Date: 2021-06-29T14:12:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: courts;cybersecurity;false positives;forensics;vulnerabilities
Slug: risks-of-evidentiary-software

[Source](https://www.schneier.com/blog/archives/2021/06/risks-of-evidentiary-software.html){:target="_blank" rel="noopener"}

> Over at Lawfare, Susan Landau has an excellent essay on the risks posed by software used to collect evidence (a Breathalyzer is probably the most obvious example). Bugs and vulnerabilities can lead to inaccurate evidence, but the proprietary nature of software makes it hard for defendants to examine it. The software engineers proposed a three-part test. First, the court should have access to the “Known Error Log,” which should be part of any professionally developed software project. Next the court should consider whether the evidence being presented could be materially affected by a software error. Ladkin and his co-authors noted [...]
