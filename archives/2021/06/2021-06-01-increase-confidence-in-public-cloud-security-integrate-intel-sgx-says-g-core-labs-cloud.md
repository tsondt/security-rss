Title: Increase confidence in public cloud security: Integrate Intel SGX, says G-Core Labs Cloud
Date: 2021-06-01T07:30:11+00:00
Author: Vsevolod Vayner, G-Core Labs Cloud Platforms Department Head
Category: The Register
Tags: 
Slug: increase-confidence-in-public-cloud-security-integrate-intel-sgx-says-g-core-labs-cloud

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/01/g_core_labs_cloud_sgx/){:target="_blank" rel="noopener"}

> Hear from one of the first providers to support this security functionality Sponsored Cloud infrastructure has many advantages over a corporate server. It’s easier to set it up and to get access to almost any resources in a matter of minutes, and you only pay for the capacity used. However, businesses are often concerned about how secure cloud solutions are.... [...]
