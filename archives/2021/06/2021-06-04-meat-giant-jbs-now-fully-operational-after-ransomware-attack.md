Title: Meat giant JBS now fully operational after ransomware attack
Date: 2021-06-04T06:42:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: meat-giant-jbs-now-fully-operational-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/meat-giant-jbs-now-fully-operational-after-ransomware-attack/){:target="_blank" rel="noopener"}

> JBS, the world's largest beef producer, has confirmed that all its global facilities are fully operational and operate at normal capacity after the REvil ransomware attack that hit its systems last weekend. [...]
