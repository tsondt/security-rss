Title: Ministers should not have cameras in their offices, Javid says
Date: 2021-06-28T08:59:41+00:00
Author: Aubrey Allegretti and Matthew Weaver
Category: The Guardian
Tags: Matt Hancock;Sajid Javid;Robert Buckland;UK news;Politics;Conservatives;Palace of Westminster;House of Commons;Data and computer security;Privacy;Technology
Slug: ministers-should-not-have-cameras-in-their-offices-javid-says

[Source](https://www.theguardian.com/politics/2021/jun/28/hancock-cctv-footage-security-may-have-been-breached-minister-says){:target="_blank" rel="noopener"}

> New health secretary says recording device in his office has been disabled following Hancock leak Cabinet ministers should not have security cameras in their offices, the new health secretary has said, after his predecessor was forced to quit when CCTV footage showed him breaking Covid rules by kissing an aide and paid adviser. Speaking on his second day in the job since taking over from Matt Hancock, Sajid Javid confirmed the recording device in his new office had been disabled. Continue reading... [...]
