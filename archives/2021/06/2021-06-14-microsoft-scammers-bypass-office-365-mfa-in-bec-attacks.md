Title: Microsoft: Scammers bypass Office 365 MFA in BEC attacks
Date: 2021-06-14T13:26:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-scammers-bypass-office-365-mfa-in-bec-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-scammers-bypass-office-365-mfa-in-bec-attacks/){:target="_blank" rel="noopener"}

> Microsoft 365 Defender researchers have disrupted the cloud-based infrastructure used by scammers behind a recent large-scale business email compromise (BEC) campaign. [...]
