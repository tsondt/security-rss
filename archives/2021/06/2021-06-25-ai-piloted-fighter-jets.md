Title: AI-Piloted Fighter Jets
Date: 2021-06-25T13:53:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: artificial intelligence;China;cyberwar;cyberweapons
Slug: ai-piloted-fighter-jets

[Source](https://www.schneier.com/blog/archives/2021/06/ai-piloted-fighter-jets.html){:target="_blank" rel="noopener"}

> News from Georgetown’s Center for Security and Emerging Technology: China Claims Its AI Can Beat Human Pilots in Battle: Chinese state media reported that an AI system had successfully defeated human pilots during simulated dogfights. According to the Global Times report, the system had shot down several PLA pilots during a handful of virtual exercises in recent years. Observers outside China noted that while reports coming out of state-controlled media outlets should be taken with a grain of salt, the capabilities described in the report are not outside the realm of possibility. Last year, for example, an AI agent defeated [...]
