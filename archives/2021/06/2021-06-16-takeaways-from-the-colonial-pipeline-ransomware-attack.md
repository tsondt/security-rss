Title: Takeaways from the Colonial Pipeline Ransomware Attack
Date: 2021-06-16T16:39:33+00:00
Author: Hank Schless
Category: Threatpost
Tags: Government;Hacks;InfoSec Insider;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: takeaways-from-the-colonial-pipeline-ransomware-attack

[Source](https://threatpost.com/takeaways-colonial-pipeline-ransomware/166980/){:target="_blank" rel="noopener"}

> Hank Schless, senior manager of security solutions at Lookout, notes basic steps that organizations can take to protect themselves as ransomware gangs get smarter. [...]
