Title: The Week in Ransomware - June 11th 2021 - Under Pressure
Date: 2021-06-11T17:51:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-june-11th-2021-under-pressure

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-11th-2021-under-pressure/){:target="_blank" rel="noopener"}

> It has been quite the week when it comes to ransomware, with ransoms being paid, ransoms being taken back, and a ransomware gang shutting down. [...]
