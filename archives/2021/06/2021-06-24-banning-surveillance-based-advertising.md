Title: Banning Surveillance-Based Advertising
Date: 2021-06-24T14:44:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: marketing;privacy;reports;surveillance
Slug: banning-surveillance-based-advertising

[Source](https://www.schneier.com/blog/archives/2021/06/banning-surveillance-based-advertising.html){:target="_blank" rel="noopener"}

> The Norwegian Consumer Council just published a fantastic new report: “ Time to Ban Surveillance-Based Advertising. ” From the Introduction: The challenges caused and entrenched by surveillance-based advertising include, but are not limited to: privacy and data protection infringements opaque business models manipulation and discrimination at scale fraud and other criminal activity serious security risks In the following chapters, we describe various aspects of these challenges and point out how today’s dominant model of online advertising is a threat to consumers, democratic societies, the media, and even to advertisers themselves. These issues are significant and serious enough that we believe [...]
