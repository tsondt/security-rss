Title: Intel's latest patch set plugs some serious holes in CPU, Bluetooth, server, and – ironically – security lines
Date: 2021-06-09T12:15:05+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: intels-latest-patch-set-plugs-some-serious-holes-in-cpu-bluetooth-server-and-ironically-security-lines

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/09/intels_latest_patch_set/){:target="_blank" rel="noopener"}

> Reports through Chipzilla's bug bounty scheme growing, but still in the minority Intel has pushed out a raft of security advisories for June, bringing its total discovered "potential vulnerabilities" for the year to date to 132, only a quarter of which were reported by external contributors and the company's bug bounty programme.... [...]
