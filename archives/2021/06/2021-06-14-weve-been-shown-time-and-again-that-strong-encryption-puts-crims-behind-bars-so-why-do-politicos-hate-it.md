Title: We've been shown time and again that strong encryption puts crims behind bars, so why do politicos hate it?
Date: 2021-06-14T09:16:08+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: weve-been-shown-time-and-again-that-strong-encryption-puts-crims-behind-bars-so-why-do-politicos-hate-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/14/kiloscrote_nab_strong_encryption/){:target="_blank" rel="noopener"}

> If we trust it, crooks will too – meaning Plod can get their kiloscrote nab Column Back in October, a call by spy agencies to weaken end-to-end encryption "because of the children" provoked a bit of analysis on how many times UK Home Secretaries had banged the same drum. All of them, it turned out. All of the time.... [...]
