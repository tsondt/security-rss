Title: Uncle Sam recovers 63.7 of 75 Bitcoins Colonial Pipeline paid to ransomware crew
Date: 2021-06-08T03:26:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: uncle-sam-recovers-637-of-75-bitcoins-colonial-pipeline-paid-to-ransomware-crew

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/08/antiransomware_task_force/){:target="_blank" rel="noopener"}

> All thanks to FBI that somehow had wallet's private key The US Department of Justice on Monday said it has recovered 63.7 Bitcoins, right now worth $2.1m and falling, of the 75 or so BTC the Colonial Pipeline operators paid the ransomware miscreants who infected the fuel provider's computers.... [...]
