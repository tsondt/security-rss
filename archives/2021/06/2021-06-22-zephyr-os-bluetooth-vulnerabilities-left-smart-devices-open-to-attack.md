Title: Zephyr OS Bluetooth vulnerabilities left smart devices open to attack
Date: 2021-06-22T12:30:06+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: zephyr-os-bluetooth-vulnerabilities-left-smart-devices-open-to-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/22/zephyr_os_bluetooth_vulnerabilities/){:target="_blank" rel="noopener"}

> The 'S' in 'IoT' stands for 'security' Vulnerabilities in the Zephyr real-time operating system's Bluetooth stack have been identified, leaving a wide variety of Internet of Things devices open to attack – unless upgraded to a patched version of the OS.... [...]
