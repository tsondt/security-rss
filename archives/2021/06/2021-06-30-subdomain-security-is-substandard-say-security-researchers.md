Title: Subdomain security is substandard, say security researchers
Date: 2021-06-30T02:32:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: subdomain-security-is-substandard-say-security-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/30/subdomain_vulnerabiilties/){:target="_blank" rel="noopener"}

> Admins tend to forget that subdomains don’t inherit security controls, leaving the likes of CNN, Harvard, Cisco, and US health authorities with vulnerabilities Abandoned or ignored subdomains often include overlooked vulnerabilities that leave organisations open to attack, according to a team of infosec researchers from the Vienna University of Technology and the Ca’ Foscari University of Venice. The team’s work will be presented at the 30th USENIX Security Symposium this August.... [...]
