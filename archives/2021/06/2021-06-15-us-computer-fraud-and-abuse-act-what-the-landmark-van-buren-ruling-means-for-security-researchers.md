Title: US Computer Fraud and Abuse Act: What the ‘landmark’ Van Buren ruling means for security researchers
Date: 2021-06-15T14:42:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-computer-fraud-and-abuse-act-what-the-landmark-van-buren-ruling-means-for-security-researchers

[Source](https://portswigger.net/daily-swig/us-computer-fraud-and-abuse-act-what-the-landmark-van-buren-ruling-means-for-security-researchers){:target="_blank" rel="noopener"}

> Industry breathes a sigh of relief as legal threat recedes [...]
