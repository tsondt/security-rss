Title: How Cyber Sleuths Cracked an ATM Shimmer Gang
Date: 2021-06-23T12:49:04+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: All About Skimmers;atm shimmer;ATM skimming;Citi;download card;Florian Tudor;Intacash;Jeffrey Dant;Organized Crime and Corruption Reporting Project;U.S. Secret Service
Slug: how-cyber-sleuths-cracked-an-atm-shimmer-gang

[Source](https://krebsonsecurity.com/2021/06/how-cyber-sleuths-cracked-an-atm-shimmer-gang/){:target="_blank" rel="noopener"}

> In 2015, police departments worldwide started finding ATMs compromised with advanced new “shimming” devices made to steal data from chip card transactions. Authorities in the United States and abroad had seized many of these shimmers, but for years couldn’t decrypt the data on the devices. This is a story of ingenuity and happenstance, and how one former Secret Service agent helped crack a code that revealed the contours of a global organized crime ring. Jeffrey Dant was a special agent at the U.S. Secret Service for 12 years until 2015. After that, Dant served as the global lead for the [...]
