Title: Criminals are mailing altered Ledger devices to steal cryptocurrency
Date: 2021-06-16T17:36:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency
Slug: criminals-are-mailing-altered-ledger-devices-to-steal-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/criminals-are-mailing-altered-ledger-devices-to-steal-cryptocurrency/){:target="_blank" rel="noopener"}

> Scammers are sending fake replacement devices to Ledger customers exposed in a recent data breach that are used to steal cryptocurrency wallets. [...]
