Title: South Korea's Nuclear Research agency hacked using VPN flaw
Date: 2021-06-19T13:59:31-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: south-koreas-nuclear-research-agency-hacked-using-vpn-flaw

[Source](https://www.bleepingcomputer.com/news/security/south-koreas-nuclear-research-agency-hacked-using-vpn-flaw/){:target="_blank" rel="noopener"}

> South Korea's 'Korea Atomic Energy Research Institute' disclosed yesterday that their internal networks were hacked last month by North Korean threat actors using a VPN vulnerability. [...]
