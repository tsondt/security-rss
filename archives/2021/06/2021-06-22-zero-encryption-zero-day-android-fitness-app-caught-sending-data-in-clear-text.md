Title: Zero-encryption zero-day – Android fitness app caught sending data in clear text
Date: 2021-06-22T13:33:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: zero-encryption-zero-day-android-fitness-app-caught-sending-data-in-clear-text

[Source](https://portswigger.net/daily-swig/zero-encryption-zero-day-android-fitness-app-caught-sending-data-in-clear-text){:target="_blank" rel="noopener"}

> VeryFitPro flaw decidedly unhealthy for user privacy [...]
