Title: It's 2021 and a printf format string in a wireless network's name can break iPhone Wi-Fi
Date: 2021-06-21T21:59:43+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: its-2021-and-a-printf-format-string-in-a-wireless-networks-name-can-break-iphone-wi-fi

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/21/wifi_ssid_flaw/){:target="_blank" rel="noopener"}

> Hope no one's created guest networks called '%Free %Coffee at %Starbucks' Joining a Wi-Fi network with a specific sequence of characters in its SSID name will break wireless connectivity for iOS devices. Thankfully the bug looks to be little more than an embarrassment and inconvenience.... [...]
