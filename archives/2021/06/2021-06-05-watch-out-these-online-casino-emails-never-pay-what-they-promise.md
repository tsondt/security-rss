Title: Watch out: These online casino emails never pay what they promise
Date: 2021-06-05T10:45:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: watch-out-these-online-casino-emails-never-pay-what-they-promise

[Source](https://www.bleepingcomputer.com/news/security/watch-out-these-online-casino-emails-never-pay-what-they-promise/){:target="_blank" rel="noopener"}

> Spammers are abusing affiliate programs to promote online casinos, such as Raging Bull Casino, Sports and Casino, Ducky Luck, and Royal Ace Casino, with misleading emails. [...]
