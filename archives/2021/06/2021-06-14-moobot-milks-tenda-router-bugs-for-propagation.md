Title: Moobot Milks Tenda Router Bugs for Propagation
Date: 2021-06-14T17:43:34+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Malware;Vulnerabilities;Web Security
Slug: moobot-milks-tenda-router-bugs-for-propagation

[Source](https://threatpost.com/moobot-tenda-router-bugs/166902/){:target="_blank" rel="noopener"}

> An analysis of the campaign revealed Cyberium, an active Mirai-variant malware hosting site. [...]
