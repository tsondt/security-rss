Title: US brokerage firms warned of ongoing phishing with penalty threats
Date: 2021-06-08T11:28:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-brokerage-firms-warned-of-ongoing-phishing-with-penalty-threats

[Source](https://www.bleepingcomputer.com/news/security/us-brokerage-firms-warned-of-ongoing-phishing-with-penalty-threats/){:target="_blank" rel="noopener"}

> FINRA, the US securities industry regulator, has warned brokerage firms of an ongoing phishing campaign threatening recipients with penalties unless they provide the information requested by the attackers. [...]
