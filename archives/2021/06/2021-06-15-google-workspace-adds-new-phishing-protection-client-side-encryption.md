Title: Google Workspace adds new phishing protection, client-side encryption
Date: 2021-06-15T08:00:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-workspace-adds-new-phishing-protection-client-side-encryption

[Source](https://www.bleepingcomputer.com/news/security/google-workspace-adds-new-phishing-protection-client-side-encryption/){:target="_blank" rel="noopener"}

> Google Workspace (formerly G Suite) has been updated with client-side encryption and new Google Drive phishing and malware content protection. [...]
