Title: Upcoming Speaking Engagements
Date: 2021-06-14T16:55:09+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: 
Slug: upcoming-speaking-engagements-9

[Source](https://www.schneier.com/blog/archives/2021/06/upcoming-speaking-engagements-9.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’ll be part of a European Internet Forum virtual debate on June 17, 2021. The topic is “Decrypting the encryption debate: How to ensure public safety with a privacy-preserving and secure Internet?” I’m speaking at the all-online Society for Philosophy and Technology Conference 2021, June 28-30, 2021. I’m keynoting the 5th International Symposium on Cyber Security Cryptology and Machine Learning (via Zoom), July 8-9, 2021. I’m speaking (via Internet) at SHIFT Business Festival in Finland, August 25-26, 2021. I’ll be speaking at an Informa event on [...]
