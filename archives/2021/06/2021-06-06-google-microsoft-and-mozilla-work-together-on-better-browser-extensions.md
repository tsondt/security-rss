Title: Google, Microsoft, and Mozilla work together on better browser extensions
Date: 2021-06-06T10:30:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: google-microsoft-and-mozilla-work-together-on-better-browser-extensions

[Source](https://www.bleepingcomputer.com/news/security/google-microsoft-and-mozilla-work-together-on-better-browser-extensions/){:target="_blank" rel="noopener"}

> Google, Microsoft, Apple, and Mozilla have launched the WebExtensions Community Group (WECG) to collaborate on standardizing browser extensions to enhance both security and performance. [...]
