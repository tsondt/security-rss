Title: Security researcher turns Apache Airflow into bug bounty cash cow
Date: 2021-06-14T15:55:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-researcher-turns-apache-airflow-into-bug-bounty-cash-cow

[Source](https://portswigger.net/daily-swig/security-researcher-turns-apache-airflow-into-bug-bounty-cash-cow){:target="_blank" rel="noopener"}

> $13,000 banked through scan and exploit attack methodology [...]
