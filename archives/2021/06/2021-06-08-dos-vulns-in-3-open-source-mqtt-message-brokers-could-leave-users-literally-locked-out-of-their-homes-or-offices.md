Title: DoS vulns in 3 open-source MQTT message brokers could leave users literally locked out of their homes or offices
Date: 2021-06-08T13:05:11+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: dos-vulns-in-3-open-source-mqtt-message-brokers-could-leave-users-literally-locked-out-of-their-homes-or-offices

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/08/mqtt_dos_vulnerabilities/){:target="_blank" rel="noopener"}

> If your IoT kit employs RabbitMQ, EMQ X or VerneMQ, it's time to get patching Synopsys Cybersecurity Research Centre (CyRC) has warned of easily triggered denial-of-service (DoS) vulnerabilities in three popular open-source Internet of Things message brokers: RabbitMQ, EMQ X, and VerneMQ.... [...]
