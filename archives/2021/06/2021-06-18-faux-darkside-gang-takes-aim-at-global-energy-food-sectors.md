Title: Faux ‘DarkSide’ Gang Takes Aim at Global Energy, Food Sectors
Date: 2021-06-18T11:56:22+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Web Security
Slug: faux-darkside-gang-takes-aim-at-global-energy-food-sectors

[Source](https://threatpost.com/darkside-global-energy-food/167056/){:target="_blank" rel="noopener"}

> A DarkSide doppelganger mounts a fraud campaign aimed at extorting nearly $4 million from each target. [...]
