Title: Fujifilm confirms ransomware attack disrupted business operations
Date: 2021-06-04T10:34:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fujifilm-confirms-ransomware-attack-disrupted-business-operations

[Source](https://www.bleepingcomputer.com/news/security/fujifilm-confirms-ransomware-attack-disrupted-business-operations/){:target="_blank" rel="noopener"}

> Today, Japanese multinational conglomerate Fujifilm officially confirmed that they had suffered a ransomware attack earlier this week that disrupted business operations. [...]
