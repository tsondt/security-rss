Title: CREST president Ian Glover to retire after 13 years – but where's the transparency, bossman?
Date: 2021-06-18T13:34:38+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: crest-president-ian-glover-to-retire-after-13-years-but-wheres-the-transparency-bossman

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/18/crest_president_ian_glover_retires/){:target="_blank" rel="noopener"}

> UK infosec accreditation body still won't publish exam cheatsheet scandal report nor be interviewed by El Reg Ian Glover, president of infosec accreditation body CREST, is stepping down from his post, he told the organisation's annual general meeting yesterday.... [...]
