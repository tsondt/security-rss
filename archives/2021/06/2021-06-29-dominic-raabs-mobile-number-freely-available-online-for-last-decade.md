Title: Dominic Raab’s mobile number freely available online for last decade
Date: 2021-06-29T14:00:11+00:00
Author: Jessica Elgot Deputy political editor
Category: The Guardian
Tags: Dominic Raab;UK security and counter-terrorism;Politics;Cybercrime;Technology;Internet;Counter-terrorism policy;UK news;Foreign, Commonwealth and Development Office;Data and computer security;Privacy
Slug: dominic-raabs-mobile-number-freely-available-online-for-last-decade

[Source](https://www.theguardian.com/politics/2021/jun/29/dominic-raab-mobile-number-freely-available-online){:target="_blank" rel="noopener"}

> Exclusive: Finding raises questions for security services weeks after similar revelations about PM’s number For UK foreign secretary, simply having a mobile represents a security risk The private mobile number of Dominic Raab, the UK foreign secretary, has been online for at least 11 years, raising questions for the security services weeks after the prime minister’s number was also revealed to be accessible to anyone. Raab’s number was discovered by a Guardian reader using a Google search. It appears to have been online since before he became an MP in 2010, and remained after he became foreign secretary and first [...]
