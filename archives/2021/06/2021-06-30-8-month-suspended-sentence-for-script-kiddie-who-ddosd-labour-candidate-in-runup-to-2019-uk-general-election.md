Title: 8-month suspended sentence for script kiddie who DDoS'd Labour candidate in runup to 2019 UK general election
Date: 2021-06-30T14:02:03+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 8-month-suspended-sentence-for-script-kiddie-who-ddosd-labour-candidate-in-runup-to-2019-uk-general-election

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/30/bradley_niblock_election_ddos/){:target="_blank" rel="noopener"}

> Now banned from using Tor or VPNs – and 'vanity' handles on social media A British script kiddie who DDoS'd a Labour Party parliamentary candidate's website in the runup to the last general election has been banned from using the Tor browser.... [...]
