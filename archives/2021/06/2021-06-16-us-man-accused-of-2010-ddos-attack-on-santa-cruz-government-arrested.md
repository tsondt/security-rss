Title: US man accused of 2010 DDoS attack on Santa Cruz government arrested
Date: 2021-06-16T15:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-man-accused-of-2010-ddos-attack-on-santa-cruz-government-arrested

[Source](https://portswigger.net/daily-swig/us-man-accused-of-2010-ddos-attack-on-santa-cruz-government-arrested){:target="_blank" rel="noopener"}

> Defendant said to have fled following allegations related to cyber-attack [...]
