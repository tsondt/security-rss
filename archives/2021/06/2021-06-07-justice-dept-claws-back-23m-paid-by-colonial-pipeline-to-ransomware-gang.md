Title: Justice Dept. Claws Back $2.3M Paid by Colonial Pipeline to Ransomware Gang
Date: 2021-06-07T23:18:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Ransomware;Colonial Pipeline;DarkSide ransomware;Elliptic;Nicholas Weaver;rEvil;Sodinokibi;Tom Robinson;U.S. Department of Justice
Slug: justice-dept-claws-back-23m-paid-by-colonial-pipeline-to-ransomware-gang

[Source](https://krebsonsecurity.com/2021/06/justice-dept-claws-back-2-3m-paid-by-colonial-pipeline-to-ransomware-gang/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice said today it has recovered $2.3 million worth of Bitcoin that Colonial Pipeline paid to ransomware extortionists last month. The funds had been sent to DarkSide, a ransomware-as-a-service syndicate that disbanded after a May 14 farewell message to affiliates saying its Internet servers and cryptocurrency stash were seized by unknown law enforcement entities. On May 7, the DarkSide ransomware gang sprang its attack against Colonial, which ultimately paid 75 Bitcoin (~$4.4 million) to its tormentors. The company said the attackers only hit its business IT networks — not its pipeline security and safety systems — [...]
