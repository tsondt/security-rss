Title: DarkSide Pwned Colonial With Old VPN Password
Date: 2021-06-09T12:58:22+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: darkside-pwned-colonial-with-old-vpn-password

[Source](https://threatpost.com/darkside-pwned-colonial-with-old-vpn-password/166743/){:target="_blank" rel="noopener"}

> Attackers accessed a VPN account that was no longer in use to freeze the company’s network in a ransomware attack whose repercussions are still vibrating. [...]
