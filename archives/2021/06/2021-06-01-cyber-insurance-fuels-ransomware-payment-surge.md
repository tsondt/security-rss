Title: Cyber-Insurance Fuels Ransomware Payment Surge
Date: 2021-06-01T21:05:44+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: cyber-insurance-fuels-ransomware-payment-surge

[Source](https://threatpost.com/cyber-insurance-ransomware-payments/166580/){:target="_blank" rel="noopener"}

> Companies relying on their cyber-insurance policies to pay off ransomware criminals are being blamed for a recent uptick in ransomware attacks. [...]
