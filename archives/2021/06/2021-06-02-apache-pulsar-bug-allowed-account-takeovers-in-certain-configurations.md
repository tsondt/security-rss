Title: Apache Pulsar bug allowed account takeovers in certain configurations
Date: 2021-06-02T11:43:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: apache-pulsar-bug-allowed-account-takeovers-in-certain-configurations

[Source](https://portswigger.net/daily-swig/apache-pulsar-bug-allowed-account-takeovers-in-certain-configurations){:target="_blank" rel="noopener"}

> Software maintainers downplay real-world impact of JWT vulnerability [...]
