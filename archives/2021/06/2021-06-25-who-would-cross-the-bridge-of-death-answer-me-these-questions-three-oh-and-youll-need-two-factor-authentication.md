Title: Who would cross the Bridge of Death? Answer me these questions three! Oh and you'll need two-factor authentication
Date: 2021-06-25T08:30:07+00:00
Author: Alistair Dabbs
Category: The Register
Tags: 
Slug: who-would-cross-the-bridge-of-death-answer-me-these-questions-three-oh-and-youll-need-two-factor-authentication

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/25/something_for_the_weekend/){:target="_blank" rel="noopener"}

> I'm not the robot, pal, you are Something for the Weekend, Sir? I have failed the Turing test – again. Apparently I am unable to exhibit intelligent behaviour equivalent to that of a human being.... [...]
