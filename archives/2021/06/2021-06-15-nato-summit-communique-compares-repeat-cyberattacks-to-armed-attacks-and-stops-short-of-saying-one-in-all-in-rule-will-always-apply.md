Title: NATO summit communiqué compares repeat cyberattacks to armed attacks – and stops short of saying 'one-in, all-in' rule will always apply
Date: 2021-06-15T06:00:16+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: nato-summit-communique-compares-repeat-cyberattacks-to-armed-attacks-and-stops-short-of-saying-one-in-all-in-rule-will-always-apply

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/15/nato_final_communique_cyber_policy/){:target="_blank" rel="noopener"}

> China lashed for ignoring norms, retorts that Western clique isn't playing fair A communiqué issued at the conclusion of the NATO summit has called for China to observe the laws of cyberspace, and set out new standards by which members of the alliance will consider cyberattacks.... [...]
