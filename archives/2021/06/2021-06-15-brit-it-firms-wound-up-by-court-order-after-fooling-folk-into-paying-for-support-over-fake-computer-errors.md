Title: Brit IT firms wound up by court order after fooling folk into paying for 'support' over fake computer errors
Date: 2021-06-15T12:30:10+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: brit-it-firms-wound-up-by-court-order-after-fooling-folk-into-paying-for-support-over-fake-computer-errors

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/15/high_court_winds_up_it_scam_companies/){:target="_blank" rel="noopener"}

> Companies were puppets to Indian biz Underpin Services Private Limited Two Kent-registered IT companies have been wound up in the High Court of England and Wales for trying to scam punters with fake pop-ups to generate tech support cons.... [...]
