Title: Alibaba suffers billion-item data leak of usernames and mobile numbers
Date: 2021-06-16T03:14:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: alibaba-suffers-billion-item-data-leak-of-usernames-and-mobile-numbers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/16/alibaba_tabao_scraped_data_leak/){:target="_blank" rel="noopener"}

> Shopping service Taobao scraped by affiliate marketer; developer and boss jailed Alibaba’s Chinese shopping operation Taobao has suffered a data breach of over a billion data points including usernames and mobile phone numbers. The info was lifted from the site by a crawler developed by an affiliate marketer.... [...]
