Title: Google force installs Massachusetts MassNotify Android COVID app
Date: 2021-06-19T16:14:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google
Slug: google-force-installs-massachusetts-massnotify-android-covid-app

[Source](https://www.bleepingcomputer.com/news/security/google-force-installs-massachusetts-massnotify-android-covid-app/){:target="_blank" rel="noopener"}

> Google is force-installing a Massachusetts COVID-19 tracking app on residents' Android devices without an easy way to uninstall it. [...]
