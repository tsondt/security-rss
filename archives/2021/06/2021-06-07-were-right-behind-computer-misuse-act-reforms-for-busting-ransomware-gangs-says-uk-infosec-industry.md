Title: We're right behind Computer Misuse Act reforms for busting ransomware gangs, says UK infosec industry
Date: 2021-06-07T11:30:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: were-right-behind-computer-misuse-act-reforms-for-busting-ransomware-gangs-says-uk-infosec-industry

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/07/cma_reforms_anti_ransomware_high_agenda/){:target="_blank" rel="noopener"}

> Plus: CyberUp campaign writes to Home Sec British infosec businessees mostly support beefing up the Computer Misuse Act to directly tackle the ransomware crisis – while reform campaign CyberUp has written to Home Secretary Priti Patel offering “support” for “a renewed, forward looking framework”.... [...]
