Title: John McAfee obituary
Date: 2021-06-27T16:37:33+00:00
Author: Michael Carlson
Category: The Guardian
Tags: John McAfee;Computing;Software;Malware;US news;Technology;Viruses;Data and computer security;Media;Spain;Belize;Internet
Slug: john-mcafee-obituary

[Source](https://www.theguardian.com/us-news/2021/jun/27/john-mcafee-obituary){:target="_blank" rel="noopener"}

> Controversial antivirus software pioneer who entered US politics and became a fugitive from justice As the inventor of the antivirus software that bears his name, John McAfee, who has died aged 75 after apparently taking his own life in a Spanish prison, turned paranoia into a fortune. He was one of the first successful self-promoting celebrity millionaires whose power and media exposure provide untold influence in the US. Moving from computer savant to spiritual guru, he then began an extended second act in Belize, where his outsized lifestyle fuelled his own personal paranoia, and led to his becoming the leading [...]
