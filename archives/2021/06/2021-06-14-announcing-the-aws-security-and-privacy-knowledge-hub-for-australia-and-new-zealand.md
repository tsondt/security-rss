Title: Announcing the AWS Security and Privacy Knowledge Hub for Australia and New Zealand
Date: 2021-06-14T05:23:28+00:00
Author: Phil Rodrigues
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;APRA;Australia;Australian Prudential Regulation Authority;AWS Compliance;IRAP;IRAP PROTECTED;New Zealand;Security Blog
Slug: announcing-the-aws-security-and-privacy-knowledge-hub-for-australia-and-new-zealand

[Source](https://aws.amazon.com/blogs/security/announcing-the-aws-security-and-privacy-knowledge-hub-for-australia-and-new-zealand/){:target="_blank" rel="noopener"}

> Cloud technology provides organizations across Australia and New Zealand with the flexibility to adapt quickly and scale their digital presences up or down in response to consumer demand. In 2021 and beyond, we expect to see cloud adoption continue to accelerate as organizations of all sizes realize the agility, operational, and financial benefits of moving [...]
