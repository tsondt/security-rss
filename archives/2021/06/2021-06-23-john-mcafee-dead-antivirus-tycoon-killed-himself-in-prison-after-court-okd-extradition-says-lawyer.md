Title: John McAfee dead: Antivirus tycoon killed himself in prison after court OK'd extradition, says lawyer
Date: 2021-06-23T19:52:44+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: john-mcafee-dead-antivirus-tycoon-killed-himself-in-prison-after-court-okd-extradition-says-lawyer

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/23/john_mcafee_suicide_prison_death/){:target="_blank" rel="noopener"}

> British-American infosec renegade faced trial in America for tax evasion British-American software tycoon John McAfee was found dead in his cell in a Barcelona prison on Wednesday.... [...]
