Title: Scammer arrested for phishing operation, sent 25,000 texts in a day
Date: 2021-06-23T13:04:03-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: scammer-arrested-for-phishing-operation-sent-25000-texts-in-a-day

[Source](https://www.bleepingcomputer.com/news/security/scammer-arrested-for-phishing-operation-sent-25-000-texts-in-a-day/){:target="_blank" rel="noopener"}

> The police has arrested an individual last week for sending fraudulent text messages to thousands of people to obtain banking details and defraud them. [...]
