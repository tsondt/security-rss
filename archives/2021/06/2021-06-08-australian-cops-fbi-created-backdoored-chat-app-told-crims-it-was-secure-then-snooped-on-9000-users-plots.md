Title: Australian cops, FBI created backdoored chat app, told crims it was secure – then snooped on 9,000 users' plots
Date: 2021-06-08T01:53:33+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: australian-cops-fbi-created-backdoored-chat-app-told-crims-it-was-secure-then-snooped-on-9000-users-plots

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/08/operation_ironside_anom/){:target="_blank" rel="noopener"}

> Hundreds of arrests already in Oz, details of European and US ops to be revealed soon The Australian Federal Police (AFP) has revealed it was able to decrypt messages sent on a supposedly secure messaging app that was seeded into the criminal underworld and promoted as providing snoop-proof comms.... [...]
