Title: Would-be password-killer FIDO Alliance aims to boost uptake with new UX guidelines
Date: 2021-06-24T20:30:05+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: would-be-password-killer-fido-alliance-aims-to-boost-uptake-with-new-ux-guidelines

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/24/wouldbe_passwordkiller_fido_alliance_aims/){:target="_blank" rel="noopener"}

> Throws a bone to complex enterprise deployment, too The FIDO Alliance, which operates with no smaller mission than to "reduce the world's over-reliance on passwords", has announced the release of new user experience (UX) guidelines aimed at bringing the more technophobic on board.... [...]
