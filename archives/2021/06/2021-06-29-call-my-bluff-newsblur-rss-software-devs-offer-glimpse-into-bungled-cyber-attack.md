Title: Call my bluff: NewsBlur RSS software devs offer glimpse into bungled ‘cyber-attack’
Date: 2021-06-29T11:25:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: call-my-bluff-newsblur-rss-software-devs-offer-glimpse-into-bungled-cyber-attack

[Source](https://portswigger.net/daily-swig/call-my-bluff-newsblur-rss-software-devs-offer-glimpse-into-bungled-cyber-attack){:target="_blank" rel="noopener"}

> Investigation revealed that ‘digital vandals’ fled empty handed [...]
