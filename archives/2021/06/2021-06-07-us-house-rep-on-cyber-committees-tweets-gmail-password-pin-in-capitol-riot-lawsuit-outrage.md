Title: US House Rep on cyber committees tweets Gmail password, PIN in Capitol riot lawsuit outrage
Date: 2021-06-07T20:38:42+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: us-house-rep-on-cyber-committees-tweets-gmail-password-pin-in-capitol-riot-lawsuit-outrage

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/07/brooks_gmail_password/){:target="_blank" rel="noopener"}

> Gonna need a little Mo security over here US House Rep Mo Brooks (R-AL) seemingly revealed his Gmail password and a PIN in a Sunday rage tweet about a lawsuit regarding the January 6 insurrection attempt.... [...]
