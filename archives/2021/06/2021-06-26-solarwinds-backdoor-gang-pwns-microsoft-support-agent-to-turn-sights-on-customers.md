Title: SolarWinds backdoor gang pwns Microsoft support agent to turn sights on customers
Date: 2021-06-26T03:28:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: solarwinds-backdoor-gang-pwns-microsoft-support-agent-to-turn-sights-on-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/26/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Amazon gobbles Wickr, automakers cough to privacy blunders, and more In brief The SolarWinds backdoor gang last month infiltrated Microsoft's support desk via a phishing attack to obtain information to use in cyber-attacks on some of the Windows giant's own customers, it was reported.... [...]
