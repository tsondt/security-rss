Title: SolarWinds backdoor gang pwned Microsoft support agent to turn sights on customers
Date: 2021-06-26T03:28:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: solarwinds-backdoor-gang-pwned-microsoft-support-agent-to-turn-sights-on-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/26/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Amazon gobbles Wickr, automakers cough to privacy blunders, and more In Brief The spies who backdoored SolarWinds' Orion software infiltrated Microsoft's support desk systems last month and obtained information to use in cyber-attacks on some of the Windows giant's customers, it was reported.... [...]
