Title: Threat Actors Use Google Docs to Host Phishing Attacks
Date: 2021-06-17T13:00:14+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Web Security
Slug: threat-actors-use-google-docs-to-host-phishing-attacks

[Source](https://threatpost.com/google-docs-host-attack/166998/){:target="_blank" rel="noopener"}

> Exploit in the widely used document service leveraged to send malicious links that appear legitimate but actually steal victims credentials. [...]
