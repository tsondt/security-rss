Title: CD Projekt: Data stolen in ransomware attack now circulating online
Date: 2021-06-10T17:57:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Gaming;Software
Slug: cd-projekt-data-stolen-in-ransomware-attack-now-circulating-online

[Source](https://www.bleepingcomputer.com/news/security/cd-projekt-data-stolen-in-ransomware-attack-now-circulating-online/){:target="_blank" rel="noopener"}

> CD Projekt is warning today that internal data stolen during their February ransomware attack is circulating on the Internet. [...]
