Title: Exclusive Ransomware Poll: 80% of Victims Don’t Pay Up
Date: 2021-06-16T18:01:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: eBook;Malware;Most Recent ThreatLists
Slug: exclusive-ransomware-poll-80-of-victims-dont-pay-up

[Source](https://threatpost.com/ransomware-victims-dont-pay-up/166989/){:target="_blank" rel="noopener"}

> Meanwhile, in a separate survey, 80 percent of organizations that paid the ransom said they were hit by a second attack. [...]
