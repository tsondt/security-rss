Title: REvil Ransomware Code Ripped Off by Rivals
Date: 2021-06-23T15:11:48+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: revil-ransomware-code-ripped-off-by-rivals

[Source](https://threatpost.com/revil-ransomware-code-rivals/167167/){:target="_blank" rel="noopener"}

> The LV ransomware operators likely used a hex editor to repurpose a REvil binary almost wholesale, for their own nefarious purposes. [...]
