Title: MyBook Users Urged to Unplug Devices from Internet
Date: 2021-06-25T20:23:28+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Time to Patch;Ars Technica;Bleeping Computer;CVE-2018-18472;MyBook;MyBook Live;MyBook Live Duo;National Vulnerability Database;Western Digital;Wizcase.com
Slug: mybook-users-urged-to-unplug-devices-from-internet

[Source](https://krebsonsecurity.com/2021/06/mybook-users-urged-to-unplug-devices-from-internet/){:target="_blank" rel="noopener"}

> Hard drive giant Western Digital is urging users of its MyBook Live brand of network storage drives to disconnect them from the Internet, warning that malicious hackers are remotely wiping the drives using a critical flaw that can be triggered by anyone who knows the Internet address of an affected device. One of many similar complaints on Western Digital’s user forum. Earlier this week, Bleeping Computer and Ars Technica pointed to a heated discussion thread on Western Digital’s user forum where many customers complained of finding their MyBook Live and MyBook Live Duo devices completely wiped of their data. “Western [...]
