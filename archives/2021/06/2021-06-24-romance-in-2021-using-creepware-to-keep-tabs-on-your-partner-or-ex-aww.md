Title: Romance in 2021: Using creepware to keep tabs on your partner or ex. Aww
Date: 2021-06-24T17:15:11+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: romance-in-2021-using-creepware-to-keep-tabs-on-your-partner-or-ex-aww

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/24/stalkerware_norton/){:target="_blank" rel="noopener"}

> With this app, I thee stalk Online stalking appears to be as much a part of modern relationships as lovingly sharing a single spoon and dessert in a dimly lit restaurant or arguing over who should put out the bins.... [...]
