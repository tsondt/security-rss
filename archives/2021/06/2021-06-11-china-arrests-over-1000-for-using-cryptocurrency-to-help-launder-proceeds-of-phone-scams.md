Title: China arrests over 1000 for using cryptocurrency to help launder proceeds of phone scams
Date: 2021-06-11T05:58:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: china-arrests-over-1000-for-using-cryptocurrency-to-help-launder-proceeds-of-phone-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/11/china_crypto_crackdown_continues/){:target="_blank" rel="noopener"}

> As local search engines stop providing results on crypto-keywords China’s crackdown on cryptocurrencies has reached a new crescendo, with the nation’s Ministry of Public Security on Wednesday proclaiming it has arrested over 1000 people and shut down 170 gangs that provided crypto-linked money-laundering services.... [...]
