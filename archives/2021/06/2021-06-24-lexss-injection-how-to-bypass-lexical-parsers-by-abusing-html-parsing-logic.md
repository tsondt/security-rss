Title: ‘LEXSS’ injection: How to bypass lexical parsers by abusing HTML parsing logic
Date: 2021-06-24T15:29:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: lexss-injection-how-to-bypass-lexical-parsers-by-abusing-html-parsing-logic

[Source](https://portswigger.net/daily-swig/lexss-injection-how-to-bypass-lexical-parsers-by-abusing-html-parsing-logic){:target="_blank" rel="noopener"}

> Researcher digs deeper into technique that uncovered flaws in popular WYSIWYG HTML text editors [...]
