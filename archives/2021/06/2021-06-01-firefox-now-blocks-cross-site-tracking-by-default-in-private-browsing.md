Title: Firefox now blocks cross-site tracking by default in private browsing
Date: 2021-06-01T11:00:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: firefox-now-blocks-cross-site-tracking-by-default-in-private-browsing

[Source](https://www.bleepingcomputer.com/news/security/firefox-now-blocks-cross-site-tracking-by-default-in-private-browsing/){:target="_blank" rel="noopener"}

> Mozilla says that Firefox users will be protected against cross-site tracking automatically while browsing the Internet in Private Browsing mode. [...]
