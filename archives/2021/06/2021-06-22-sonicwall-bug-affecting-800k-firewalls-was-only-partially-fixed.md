Title: SonicWall bug affecting 800K firewalls was only partially fixed
Date: 2021-06-22T14:59:53-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: sonicwall-bug-affecting-800k-firewalls-was-only-partially-fixed

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-bug-affecting-800k-firewalls-was-only-partially-fixed/){:target="_blank" rel="noopener"}

> New findings have emerged that shed light on a critical SonicWall vulnerability disclosed last year, which affected over 800,000 VPN firewalls and was initially thought to have been patched. Tracked as CVE-2020-5135, when exploited, the bug allows unauthenticated remote attackers to execute arbitrary code on the impacted devices. [...]
