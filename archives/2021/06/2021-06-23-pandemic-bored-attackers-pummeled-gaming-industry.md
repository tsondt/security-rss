Title: Pandemic-Bored Attackers Pummeled Gaming Industry
Date: 2021-06-23T16:53:01+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Most Recent ThreatLists;Web Security
Slug: pandemic-bored-attackers-pummeled-gaming-industry

[Source](https://threatpost.com/attackers-gaming-industry/167183/){:target="_blank" rel="noopener"}

> Akamai's 2020 gaming report shows that cyberattacks on the video game industry skyrocketed, shooting up 340 percent in 2020. [...]
