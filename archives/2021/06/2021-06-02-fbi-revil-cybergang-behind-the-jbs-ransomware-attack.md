Title: FBI: REvil cybergang behind the JBS ransomware attack
Date: 2021-06-02T20:42:03-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fbi-revil-cybergang-behind-the-jbs-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/fbi-revil-cybergang-behind-the-jbs-ransomware-attack/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigations has officially stated that the REvil operation, aka Sodinokibi, is behind the ransomware attack targeting JBS, the world's largest meat producer. [...]
