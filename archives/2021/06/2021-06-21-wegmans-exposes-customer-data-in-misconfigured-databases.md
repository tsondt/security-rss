Title: Wegmans Exposes Customer Data in Misconfigured Databases
Date: 2021-06-21T21:52:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Web Security
Slug: wegmans-exposes-customer-data-in-misconfigured-databases

[Source](https://threatpost.com/wegmans-exposes-customer-data-misconfigured-databases/167099/){:target="_blank" rel="noopener"}

> Cleanup in aisle "Oops": The supermarket chain said that it misconfigured two cloud databases, exposing customer data to public scrutiny. [...]
