Title: ‘Fancy Lazarus’ Cyberattackers Ramp up Ransom DDoS Efforts
Date: 2021-06-10T21:54:21+00:00
Author: Tara Seals
Category: Threatpost
Tags: Web Security
Slug: fancy-lazarus-cyberattackers-ramp-up-ransom-ddos-efforts

[Source](https://threatpost.com/fancy-lazarus-cyberattackers-ransom-ddos/166811/){:target="_blank" rel="noopener"}

> The group, known for masquerading as various APT groups, is back with a spate of attacks on U.S. companies. [...]
