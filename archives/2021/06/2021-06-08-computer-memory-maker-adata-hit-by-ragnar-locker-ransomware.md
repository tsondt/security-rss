Title: Computer memory maker ADATA hit by Ragnar Locker ransomware
Date: 2021-06-08T13:11:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: computer-memory-maker-adata-hit-by-ragnar-locker-ransomware

[Source](https://www.bleepingcomputer.com/news/security/computer-memory-maker-adata-hit-by-ragnar-locker-ransomware/){:target="_blank" rel="noopener"}

> Taiwan-based leading memory and storage manufacturer ADATA says that a ransomware attack forced it to take systems offline after hitting its network in late May. [...]
