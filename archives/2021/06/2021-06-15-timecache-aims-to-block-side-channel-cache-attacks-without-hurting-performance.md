Title: TimeCache aims to block side-channel cache attacks – without hurting performance
Date: 2021-06-15T13:45:08+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: timecache-aims-to-block-side-channel-cache-attacks-without-hurting-performance

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/15/timecache_aims_to_block_sidechannel/){:target="_blank" rel="noopener"}

> Research team: We've proved hardware-plus-software mods can keep your secrets secret Researchers from the University of Rochester have created TimeCache, an approach to system security claimed to protect against side-channel attacks like evict+reload and Spectre, without the usual deleterious impact to performance.... [...]
