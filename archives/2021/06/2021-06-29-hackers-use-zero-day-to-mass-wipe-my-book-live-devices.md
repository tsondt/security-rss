Title: Hackers use zero-day to mass-wipe My Book Live devices
Date: 2021-06-29T17:28:58-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Hardware
Slug: hackers-use-zero-day-to-mass-wipe-my-book-live-devices

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-zero-day-to-mass-wipe-my-book-live-devices/){:target="_blank" rel="noopener"}

> A zero-day vulnerability in Western Digital My Book Live NAS devices allowed a threat actor to perform mass-factory resets of devices last week, leading to data loss. [...]
