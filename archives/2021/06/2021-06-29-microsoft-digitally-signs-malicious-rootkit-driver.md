Title: Microsoft digitally signs malicious rootkit driver
Date: 2021-06-29T19:50:06+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;malware;microsoft;rootkits;tls transport layer security;transport layer security;Windows Hardware Compatibility Program
Slug: microsoft-digitally-signs-malicious-rootkit-driver

[Source](https://arstechnica.com/?p=1777105){:target="_blank" rel="noopener"}

> Enlarge Microsoft gave its digital imprimatur to a rootkit that decrypted encrypted communications and sent them to attacker-controlled servers, the company and outside researchers said. The blunder allowed the malware to be installed on Windows machines without users receiving a security warning or needing to take additional steps. For the past 13 years, Microsoft has required third-party drivers and other code that runs in the Windows kernel to be tested and digitally signed by the OS maker to ensure stability and security. Without a Microsoft certificate, these types of programs can’t be installed by default. Eavesdropping on SSL connections Earlier [...]
