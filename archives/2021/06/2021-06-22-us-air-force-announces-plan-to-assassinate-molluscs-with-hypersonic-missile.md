Title: US Air Force announces plan to assassinate molluscs with hypersonic missile
Date: 2021-06-22T08:31:05+00:00
Author: Matt Dupuy
Category: The Register
Tags: 
Slug: us-air-force-announces-plan-to-assassinate-molluscs-with-hypersonic-missile

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/22/usaf_snail_clam_missile_plan/){:target="_blank" rel="noopener"}

> No word on whether top brass considered just shelling them into submission The United States Air Force (USAF) has issued a strangely specific threat to certain mollusc species living in the area of an upcoming weapons test.... [...]
