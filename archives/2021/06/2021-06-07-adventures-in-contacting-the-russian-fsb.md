Title: Adventures in Contacting the Russian FSB
Date: 2021-06-07T13:35:06+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;BadB;C#;CryptoPro;fbi;Federal Security Service;FSB;GOST;Lance James;Treasury Department;Unit221B;virustotal;Vladislav Horohorin;Yandex
Slug: adventures-in-contacting-the-russian-fsb

[Source](https://krebsonsecurity.com/2021/06/adventures-in-contacting-the-russian-fsb/){:target="_blank" rel="noopener"}

> KrebsOnSecurity recently had occasion to contact the Russian Federal Security Service (FSB), the Russian equivalent of the U.S. Federal Bureau of Investigation (FBI). In the process of doing so, I encountered a small snag: The FSB’s website said in order to communicate with them securely, I needed to download and install an encryption and virtual private networking (VPN) appliance that is flagged by at least 20 antivirus products as malware. The FSB headquarters at Lubyanka Square, Moscow. Image: Wikipedia. The reason I contacted the FSB — one of the successor agencies to the Russian KGB — ironically enough had to [...]
