Title: Six Flags to Pay $36M Over Collection of Fingerprints
Date: 2021-06-22T11:14:29+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy
Slug: six-flags-to-pay-36m-over-collection-of-fingerprints

[Source](https://threatpost.com/six-flags-to-pay-36m-over-collection-of-fingerprints/167103/){:target="_blank" rel="noopener"}

> Illinois Supreme Court rules in favor of class action against company’s practice of scanning people’s fingers when they enter amusement parks. [...]
