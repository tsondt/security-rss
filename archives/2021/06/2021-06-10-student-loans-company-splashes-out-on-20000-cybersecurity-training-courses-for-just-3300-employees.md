Title: Student Loans Company splashes out on 20,000 cybersecurity training courses – for just 3,300 employees
Date: 2021-06-10T10:45:06+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: student-loans-company-splashes-out-on-20000-cybersecurity-training-courses-for-just-3300-employees

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/10/student_loans_company_security_training/){:target="_blank" rel="noopener"}

> FoI request details £76,800 in training fees, most of which went to staff security-specific departments The Student Loans Company (SLC) spent £76,800 on cybersecurity training over its previous two fiscal years – including a sudden and unsurprising interest in security in a work-from-home environment.... [...]
