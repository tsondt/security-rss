Title: Google Patches Critical Android RCE Bug
Date: 2021-06-08T19:02:25+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: google-patches-critical-android-rce-bug

[Source](https://threatpost.com/android-critical-rce-bug/166723/){:target="_blank" rel="noopener"}

> Google's June security bulletin addresses 90+ bugs in Android and Pixel devices. [...]
