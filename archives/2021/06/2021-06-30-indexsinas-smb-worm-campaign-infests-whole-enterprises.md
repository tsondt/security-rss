Title: Indexsinas SMB Worm Campaign Infests Whole Enterprises
Date: 2021-06-30T20:19:28+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: indexsinas-smb-worm-campaign-infests-whole-enterprises

[Source](https://threatpost.com/indexsinas-smb-worm-enterprises/167455/){:target="_blank" rel="noopener"}

> The self-propagating malware's attack chain is complex, using former NSA cyberweapons, and ultimately drops cryptominers on targeted machines. [...]
