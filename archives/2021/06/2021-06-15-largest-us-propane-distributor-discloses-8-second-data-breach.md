Title: Largest US propane distributor discloses '8-second' data breach
Date: 2021-06-15T08:37:23-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: largest-us-propane-distributor-discloses-8-second-data-breach

[Source](https://www.bleepingcomputer.com/news/security/largest-us-propane-distributor-discloses-8-second-data-breach/){:target="_blank" rel="noopener"}

> America's largest propane provider, AmeriGas, has disclosed a data breach that lasted ephemerally but impacted 123 employees and one resident. AmeriGas servers over 2 million customers in all 50 U.S. states and has over 2,500 distribution locations. [...]
