Title: 'Set it and forget it' attitude to open-source software has become a major security problem, says Veracode
Date: 2021-06-22T21:30:07+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: set-it-and-forget-it-attitude-to-open-source-software-has-become-a-major-security-problem-says-veracode

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/22/third_party_libraries_veracode/){:target="_blank" rel="noopener"}

> Study finds a whole sea of outdated third-party libraries There's a minefield of security problems bubbling under the surface of modern software, Veracode has claimed in its latest report, thanks to developers pulling third-party open-source libraries into their code bases – then never bothering to update them again.... [...]
