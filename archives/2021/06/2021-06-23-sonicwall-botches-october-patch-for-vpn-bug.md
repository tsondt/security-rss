Title: SonicWall ‘Botches’ October Patch for VPN Bug
Date: 2021-06-23T10:44:07+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: sonicwall-botches-october-patch-for-vpn-bug

[Source](https://threatpost.com/sonicwall-botches-critical-vpn-bug/167152/){:target="_blank" rel="noopener"}

> Company finally rolls out the complete fix this week for a flaw affecting some 800,000 devices that could result in crashes or prevent users from connecting to corporate resources. [...]
