Title: FBI paid renegade developer $180k for backdoored AN0M chat app that brought down drug underworld
Date: 2021-06-08T22:58:42+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: fbi-paid-renegade-developer-180k-for-backdoored-an0m-chat-app-that-brought-down-drug-underworld

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/08/fbi_trojan_shield/){:target="_blank" rel="noopener"}

> From hidden master keys to pineapples stuffed with Bolivian marching powder — this story has it all The FBI has revealed how it managed to hoodwink the criminal underworld with its secretly backdoored AN0M encrypted chat app, leading to hundreds of arrests, the seizure of 32 tons of drugs, 250 firearms, 55 luxury cars, more than $148M, and even cocaine-filled pineapples.... [...]
