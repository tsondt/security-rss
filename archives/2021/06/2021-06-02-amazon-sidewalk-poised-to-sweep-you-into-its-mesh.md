Title: Amazon Sidewalk Poised to Sweep You Into Its Mesh
Date: 2021-06-02T10:58:21+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: IoT;Mobile Security;Web Security
Slug: amazon-sidewalk-poised-to-sweep-you-into-its-mesh

[Source](https://threatpost.com/amazon-sidewalk-to-sweep-you-into-its-mesh/166581/){:target="_blank" rel="noopener"}

> On June 8, Amazon’s pulling all its devices into a device-to-device wireless mix, inspiring FUD along the way. Now's the time to opt out if you're be-FUDdled. [...]
