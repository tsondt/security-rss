Title: Paradise Ransomware source code released on a hacking forum
Date: 2021-06-15T11:56:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: paradise-ransomware-source-code-released-on-a-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/paradise-ransomware-source-code-released-on-a-hacking-forum/){:target="_blank" rel="noopener"}

> The complete source code for the Paradise Ransomware has been released on a hacking forum allowing any would-be cyber criminal to develop their own customized ransomware operation. [...]
