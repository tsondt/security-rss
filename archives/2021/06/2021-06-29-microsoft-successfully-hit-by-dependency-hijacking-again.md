Title: Microsoft successfully hit by dependency hijacking again
Date: 2021-06-29T03:40:52-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: microsoft-successfully-hit-by-dependency-hijacking-again

[Source](https://www.bleepingcomputer.com/news/security/microsoft-successfully-hit-by-dependency-hijacking-again/){:target="_blank" rel="noopener"}

> Microsoft has once again been successfully hit by a dependency hijacking attack. This month, another researcher found an npm internal dependency being used by an open-source project. After publishing a public dependency by the same name, he began receiving messages from Microsoft's servers. [...]
