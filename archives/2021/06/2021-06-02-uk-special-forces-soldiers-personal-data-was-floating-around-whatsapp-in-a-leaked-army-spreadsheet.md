Title: UK Special Forces soldiers' personal data was floating around WhatsApp in a leaked Army spreadsheet
Date: 2021-06-02T14:28:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-special-forces-soldiers-personal-data-was-floating-around-whatsapp-in-a-leaked-army-spreadsheet

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/02/uk_special_forces_data_breach_whatsapp/){:target="_blank" rel="noopener"}

> Bizarre promotion practice leads to near-inevitable breach Exclusive An astonishing data security blunder saw the personal data of Special Forces soldiers circulating around WhatsApp in a leaked British Army spreadsheet.... [...]
