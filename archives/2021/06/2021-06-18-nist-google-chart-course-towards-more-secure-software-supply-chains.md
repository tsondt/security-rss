Title: NIST, Google chart course towards more secure software supply chains
Date: 2021-06-18T15:00:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nist-google-chart-course-towards-more-secure-software-supply-chains

[Source](https://portswigger.net/daily-swig/nist-google-chart-course-towards-more-secure-software-supply-chains){:target="_blank" rel="noopener"}

> Momentum building for effective, ecosystem-wide standards and guidelines [...]
