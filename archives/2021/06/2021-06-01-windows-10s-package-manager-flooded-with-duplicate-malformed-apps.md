Title: Windows 10's package manager flooded with duplicate, malformed apps
Date: 2021-06-01T11:15:33-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: windows-10s-package-manager-flooded-with-duplicate-malformed-apps

[Source](https://www.bleepingcomputer.com/news/security/windows-10s-package-manager-flooded-with-duplicate-malformed-apps/){:target="_blank" rel="noopener"}

> Microsoft's Windows 10 package manager Winget's GitHub has been flooded with duplicate apps and malformed manifest files raising concerns among developers with regards to the integrity of apps. [...]
