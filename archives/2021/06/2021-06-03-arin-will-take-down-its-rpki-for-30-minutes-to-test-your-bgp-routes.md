Title: ARIN will take down its RPKI for 30 minutes to test your BGP routes
Date: 2021-06-03T02:40:17-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: arin-will-take-down-its-rpki-for-30-minutes-to-test-your-bgp-routes

[Source](https://www.bleepingcomputer.com/news/security/arin-will-take-down-its-rpki-for-30-minutes-to-test-your-bgp-routes/){:target="_blank" rel="noopener"}

> ARIN plans on performing unannounced maintenance of its RPKI, sometime in July, for about thirty minutes to check if networks are adhering to BGP best practices. [...]
