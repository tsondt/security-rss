Title: Security training org EC-Council pulls blog over copyright violations, promises editorial improvements
Date: 2021-06-28T11:20:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-training-org-ec-council-pulls-blog-over-copyright-violations-promises-editorial-improvements

[Source](https://portswigger.net/daily-swig/security-training-org-ec-council-pulls-blog-over-copyright-violations-promises-editorial-improvements){:target="_blank" rel="noopener"}

> Public speaker whose work was allegedly stolen says the company has ‘no respect’ for the infosec community [...]
