Title: West Virginia job seekers alerted to gov’t employment agency data breach
Date: 2021-06-24T13:38:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: west-virginia-job-seekers-alerted-to-govt-employment-agency-data-breach

[Source](https://portswigger.net/daily-swig/west-virginia-job-seekers-alerted-to-govt-employment-agency-data-breach){:target="_blank" rel="noopener"}

> WorkForce West Virginia’s MACC database was compromised earlier this year [...]
