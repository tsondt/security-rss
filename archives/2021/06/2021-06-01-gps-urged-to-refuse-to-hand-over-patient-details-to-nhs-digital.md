Title: GPs urged to refuse to hand over patient details to NHS Digital
Date: 2021-06-01T11:39:39+00:00
Author: Damien Gayle
Category: The Guardian
Tags: GPs;NHS;Doctors;Health;Society;England;UK news;Privacy;Data protection;Data and computer security;Technology
Slug: gps-urged-to-refuse-to-hand-over-patient-details-to-nhs-digital

[Source](https://www.theguardian.com/society/2021/jun/01/gps-urged-to-refuse-to-hand-over-patient-details-to-nhs-digital){:target="_blank" rel="noopener"}

> Senior doctors call on colleagues not to share personal data, in effort to buy time to raise awareness of plans Senior GPs have called on colleagues to refuse to hand over patients’ personal data to NHS Digital, in a move they hope will buy time to raise awareness of plans to place all medical records in England on a central database. All 36 doctors’ surgeries in Tower Hamlets, east London, have already agreed to withhold the data when collection begins on 1 July, the Guardian understands. An email has been circulated to about 100 practices across north-east London calling on [...]
