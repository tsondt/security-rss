Title: South Korea’s data watchdog barks warnings at Microsoft and five local firms
Date: 2021-06-10T06:28:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: south-koreas-data-watchdog-barks-warnings-at-microsoft-and-five-local-firms

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/10/koreas_government_data_watchdog_org/){:target="_blank" rel="noopener"}

> Fines, fines, everywhere there's fines Microsoft and five other companies have received fines totaling US$75K from South Korea's Personal Information Protection Commission (PIPC), for running afoul of local data protection laws.... [...]
