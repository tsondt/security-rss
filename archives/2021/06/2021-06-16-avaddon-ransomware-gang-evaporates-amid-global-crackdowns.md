Title: Avaddon Ransomware Gang Evaporates Amid Global Crackdowns
Date: 2021-06-16T12:04:53+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Web Security
Slug: avaddon-ransomware-gang-evaporates-amid-global-crackdowns

[Source](https://threatpost.com/avaddon-ransomware-global-crackdowns/166968/){:target="_blank" rel="noopener"}

> Ransomware group releases decryptors for nearly 3,000 victims, forfeiting millions in payouts. [...]
