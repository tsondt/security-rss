Title: The AN0M fake secure chat app may have been too clever for its own good
Date: 2021-06-14T05:03:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: the-an0m-fake-secure-chat-app-may-have-been-too-clever-for-its-own-good

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/14/an0m_and_yamamoto/){:target="_blank" rel="noopener"}

> Crims now know what not to trust, and how to stymie future infiltrations Comment In April 1943, Japanese admiral Isoroku Yamamoto was killed when the US Air Force shot down the plane carrying him to Balalae Airfield in the Solomon Islands.... [...]
