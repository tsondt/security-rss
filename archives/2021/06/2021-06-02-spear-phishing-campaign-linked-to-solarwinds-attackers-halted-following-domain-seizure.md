Title: Spear-phishing campaign linked to SolarWinds attackers halted following domain seizure
Date: 2021-06-02T14:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: spear-phishing-campaign-linked-to-solarwinds-attackers-halted-following-domain-seizure

[Source](https://portswigger.net/daily-swig/spear-phishing-campaign-linked-to-solarwinds-attackers-halted-following-domain-seizure){:target="_blank" rel="noopener"}

> APT29 accused of compromising USAID email account [...]
