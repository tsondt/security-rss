Title: How to implement SaaS tenant isolation with ABAC and AWS IAM
Date: 2021-06-09T16:10:14+00:00
Author: Michael Pelts
Category: AWS Security
Tags: AWS Identity and Access Management (IAM);Expert (400);Security, Identity, & Compliance;ABAC;Attribute-based access control;ISV;Multitenancy;Multitenant architecture;SaaS;SaaS Identity;Security Blog;Tenant isolation
Slug: how-to-implement-saas-tenant-isolation-with-abac-and-aws-iam

[Source](https://aws.amazon.com/blogs/security/how-to-implement-saas-tenant-isolation-with-abac-and-aws-iam/){:target="_blank" rel="noopener"}

> Multi-tenant applications must be architected so that the resources of each tenant are isolated and cannot be accessed by other tenants in the system. AWS Identity and Access Management (IAM) is often a key element in achieving this goal. One of the challenges with using IAM, however, is that the number and complexity of IAM [...]
