Title: Colonial Pipeline cyber-attack: US authorities seize $2.3m in DarkSide ransomware payments
Date: 2021-06-09T13:39:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: colonial-pipeline-cyber-attack-us-authorities-seize-23m-in-darkside-ransomware-payments

[Source](https://portswigger.net/daily-swig/colonial-pipeline-cyber-attack-us-authorities-seize-2-3m-in-darkside-ransomware-payments){:target="_blank" rel="noopener"}

> Funds represent a ‘significant portion’ of proceeds for those behind critical infrastructure attack [...]
