Title: FireEye sold to McAfee's new owners for $1.2bn as Mandiant split into standalone firm again
Date: 2021-06-03T12:55:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: fireeye-sold-to-mcafees-new-owners-for-12bn-as-mandiant-split-into-standalone-firm-again

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/03/fireeye_sold_stg_1_2bn_dollars_mandiant/){:target="_blank" rel="noopener"}

> Another big name buyout by STG FireEye has been sold for $1.2bn to the same American private equity fund that bought McAfee’s enterprise security business, severing it from infosec stablemate Mandiant.... [...]
