Title: ‘Sophisticated threat actor’ targeting Zyxel firewalls and VPNs, vendor warns
Date: 2021-06-28T13:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: sophisticated-threat-actor-targeting-zyxel-firewalls-and-vpns-vendor-warns

[Source](https://portswigger.net/daily-swig/sophisticated-threat-actor-targeting-zyxel-firewalls-and-vpns-vendor-warns){:target="_blank" rel="noopener"}

> Users are advised to bolster security to protect against cyber-attacks [...]
