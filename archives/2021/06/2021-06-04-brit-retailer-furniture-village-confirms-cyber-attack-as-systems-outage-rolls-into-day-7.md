Title: Brit retailer Furniture Village confirms 'cyber-attack' as systems outage rolls into Day 7
Date: 2021-06-04T08:15:10+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: brit-retailer-furniture-village-confirms-cyber-attack-as-systems-outage-rolls-into-day-7

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/04/furniture_village_confirms_cyberattack/){:target="_blank" rel="noopener"}

> Sofa, not-so-good: Angry customers still can't access systems, phones, and deliveries delayed Furniture Village – the UK's largest independent furniture retailer with 54 stores nationwide – has been hit by a "cyber-attack", the company confirmed to The Register.... [...]
