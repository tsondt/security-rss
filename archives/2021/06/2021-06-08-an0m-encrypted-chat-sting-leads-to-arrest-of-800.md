Title: ‘An0m’ Encrypted-Chat Sting Leads to Arrest of 800
Date: 2021-06-08T17:02:48+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government
Slug: an0m-encrypted-chat-sting-leads-to-arrest-of-800

[Source](https://threatpost.com/an0m-encrypted-chat-sting-arrest-800/166716/){:target="_blank" rel="noopener"}

> The FBI and Australian law enforcement set up the encrypted chat service and ran it for over 3 years, seizing weapons, drugs and over $48m in cash. [...]
