Title: Andrew Appel on New Hampshire’s Election Audit
Date: 2021-06-15T15:45:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: auditing;scanners;voting
Slug: andrew-appel-on-new-hampshires-election-audit

[Source](https://www.schneier.com/blog/archives/2021/06/andrew-appel-on-new-hampshires-election-audit.html){:target="_blank" rel="noopener"}

> Really interesting two part analysis of the audit conducted after the 2020 election in Windham, New Hampshire. Based on preliminary reports published by the team of experts that New Hampshire engaged to examine an election discrepancy, it appears that a buildup of dust in the read heads of optical-scan voting machines (possibly over several years of use) can cause paper-fold lines in absentee ballots to be interpreted as votes... New Hampshire (and other states) may need to maintain the accuracy of their optical-scan voting machines by paying attention to three issues: Routine risk-limiting audits to detect inaccuracies if/when they occur. [...]
