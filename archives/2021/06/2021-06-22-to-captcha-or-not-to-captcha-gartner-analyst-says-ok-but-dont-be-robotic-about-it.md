Title: To CAPTCHA or not to CAPTCHA? Gartner analyst says OK — but don’t be robotic about it
Date: 2021-06-22T08:06:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: to-captcha-or-not-to-captcha-gartner-analyst-says-ok-but-dont-be-robotic-about-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/22/to_use_captcha_or_not/){:target="_blank" rel="noopener"}

> Picking street signs from a matrix of images is out, cleverer challenges are OK Poll Analyst firm Gartner has advised in favour of the use of CAPTCHAs — but recommends using the least-annoying CAPTCHAs you can find.... [...]
