Title: UK legal firm Gateley warns of data breach following cyber-attack
Date: 2021-06-17T10:17:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uk-legal-firm-gateley-warns-of-data-breach-following-cyber-attack

[Source](https://portswigger.net/daily-swig/uk-legal-firm-gateley-warns-of-data-breach-following-cyber-attack){:target="_blank" rel="noopener"}

> ‘Core systems’ restored after unauthorized intrusion compromises client data [...]
