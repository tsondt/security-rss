Title: Adobe issues security updates for 41 vulnerabilities in 10 products
Date: 2021-06-08T12:48:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: adobe-issues-security-updates-for-41-vulnerabilities-in-10-products

[Source](https://www.bleepingcomputer.com/news/security/adobe-issues-security-updates-for-41-vulnerabilities-in-10-products/){:target="_blank" rel="noopener"}

> Adobe has released a giant Patch Tuesday security update release that fixes vulnerabilities in ten applications, including Adobe Acrobat, Reader, and Photoshop. [...]
