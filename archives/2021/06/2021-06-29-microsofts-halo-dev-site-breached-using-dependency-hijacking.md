Title: Microsoft's Halo dev site breached using dependency hijacking
Date: 2021-06-29T03:40:52-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: microsofts-halo-dev-site-breached-using-dependency-hijacking

[Source](https://www.bleepingcomputer.com/news/security/microsofts-halo-dev-site-breached-using-dependency-hijacking/){:target="_blank" rel="noopener"}

> Microsoft has once again been successfully hit by a dependency hijacking attack. This month, another researcher found an npm internal dependency being used by an open-source project. After publishing a public dependency by the same name, he began receiving messages from Microsoft's Halo game dev servers. [...]
