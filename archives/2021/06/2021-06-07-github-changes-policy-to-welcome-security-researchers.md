Title: GitHub changes policy to welcome security researchers
Date: 2021-06-07T15:55:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: github-changes-policy-to-welcome-security-researchers

[Source](https://portswigger.net/daily-swig/github-changes-policy-to-welcome-security-researchers){:target="_blank" rel="noopener"}

> Coding platforms explicitly permits proof of concept exploits [...]
