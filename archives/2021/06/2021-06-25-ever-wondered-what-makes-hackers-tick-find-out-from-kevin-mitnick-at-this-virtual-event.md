Title: Ever wondered what makes hackers tick? Find out from Kevin Mitnick at this virtual event
Date: 2021-06-25T14:00:09+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: ever-wondered-what-makes-hackers-tick-find-out-from-kevin-mitnick-at-this-virtual-event

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/25/rubrik_data_security_talks/){:target="_blank" rel="noopener"}

> Rubrik Data Security Talks also features Anonymous veteran, top security leaders Promo Despite putting thorough and rigorous defensive security measures in place, ransomware is still getting in and corrupting data, forcing organisations to pay massive ransom fees.... [...]
