Title: How HBO Max uses reCAPTCHA Enterprise to make its customer experience frictionless
Date: 2021-06-23T16:00:00+00:00
Author: Brian Lozada
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: how-hbo-max-uses-recaptcha-enterprise-to-make-its-customer-experience-frictionless

[Source](https://cloud.google.com/blog/products/identity-security/how-hbo-uses-recaptcha-enterprise-to-secure-signup/){:target="_blank" rel="noopener"}

> Editor’s note : Randy Gingeleski, Senior Staff Security Engineer for HBO Max and Brian Lozada, CISO for HBO Max, co-authored this blog to share their experiences with reCAPTCHA Enterprise and help other enterprises achieve the same level of security for their customer experiences. The COVID-19 pandemic gave audiences more time than ever to explore all the content hosted on HBO Max, and dramatically increased the demand for quick and reliable streaming. To support this demand, we made huge investments in our customer experience tools and digital experiences to continue bringing our customers the latest content while curating a best-in-class experience. [...]
