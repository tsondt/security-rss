Title: Scammers mail fake Ledger devices to steal your cryptocurrency
Date: 2021-06-16T17:36:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency
Slug: scammers-mail-fake-ledger-devices-to-steal-your-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/scammers-mail-fake-ledger-devices-to-steal-your-cryptocurrency/){:target="_blank" rel="noopener"}

> Scammers are sending fake replacement devices to Ledger customers exposed in a recent data breach that are used to steal cryptocurrency wallets. [...]
