Title: Dell SupportAssist contained RCE flaw allowing miscreants to remotely reflash your BIOS with code of their creation
Date: 2021-06-25T17:45:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: dell-supportassist-contained-rce-flaw-allowing-miscreants-to-remotely-reflash-your-bios-with-code-of-their-creation

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/25/dell_supportassist_biosconnect_vulns_rce/){:target="_blank" rel="noopener"}

> And it affects 129 models of PC and laptop... or about 30 million computers A chain of four vulnerabilities in Dell's SupportAssist remote firmware update utility could let malicious people run arbitrary code in no fewer than 129 different PCs and laptops models – while impersonating Dell to remotely upload a tampered BIOS.... [...]
