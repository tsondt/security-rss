Title: Insider Risks In the Work-From-Home World
Date: 2021-06-15T14:47:35+00:00
Author: Threatpost
Category: Threatpost
Tags: Podcasts;Sponsored
Slug: insider-risks-in-the-work-from-home-world

[Source](https://threatpost.com/insider-risks-work-from-home/166874/){:target="_blank" rel="noopener"}

> Forcepoint’s Michael Crouse talks about risk-adaptive data-protection approaches and how to develop a behavior-based approach to insider threats and risk, particularly with pandemic-expanded network perimeters. [...]
