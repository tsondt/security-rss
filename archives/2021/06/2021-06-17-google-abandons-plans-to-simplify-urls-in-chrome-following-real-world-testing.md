Title: Google abandons plans to simplify URLs in Chrome following real-world testing
Date: 2021-06-17T13:37:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-abandons-plans-to-simplify-urls-in-chrome-following-real-world-testing

[Source](https://portswigger.net/daily-swig/google-abandons-plans-to-simplify-urls-in-chrome-following-real-world-testing){:target="_blank" rel="noopener"}

> Grand omnibox experiment failed to hit the mark [...]
