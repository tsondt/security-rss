Title: Effective Adoption of SASE in 2021
Date: 2021-06-02T20:29:49+00:00
Author: Threatpost
Category: Threatpost
Tags: Cloud Security;Podcasts;Sponsored
Slug: effective-adoption-of-sase-in-2021

[Source](https://threatpost.com/effective-adoption-of-sase-in-2021/166605/){:target="_blank" rel="noopener"}

> In this Threatpost podcast, Forcepoint’s SASE and Zero Trust director describes how the pandemic jump-started SASE adoption. [...]
