Title: Euros-Driven Football Fever Nets Dumb Passwords
Date: 2021-06-16T15:50:33+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Web Security
Slug: euros-driven-football-fever-nets-dumb-passwords

[Source](https://threatpost.com/euros-football-fever-dumb-passwords/166974/){:target="_blank" rel="noopener"}

> The top easy-to-crack, football-inspired password in a database of 1 billion unique, clear-text, breached passwords? You probably guessed it: "Football." [...]
