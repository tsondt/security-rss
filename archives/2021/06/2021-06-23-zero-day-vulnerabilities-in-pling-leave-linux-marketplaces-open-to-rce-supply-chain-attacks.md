Title: Zero-day vulnerabilities in Pling leave Linux marketplaces open to RCE, supply chain attacks
Date: 2021-06-23T11:51:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: zero-day-vulnerabilities-in-pling-leave-linux-marketplaces-open-to-rce-supply-chain-attacks

[Source](https://portswigger.net/daily-swig/zero-day-vulnerabilities-in-pling-leave-linux-marketplaces-open-to-rce-supply-chain-attacks){:target="_blank" rel="noopener"}

> Security researcher warns against running PlingStore Electron or visiting affected websites [...]
