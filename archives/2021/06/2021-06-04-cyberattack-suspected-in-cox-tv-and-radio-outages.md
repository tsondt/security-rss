Title: Cyberattack Suspected in Cox TV and Radio Outages
Date: 2021-06-04T20:21:48+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware
Slug: cyberattack-suspected-in-cox-tv-and-radio-outages

[Source](https://threatpost.com/cyberattack-cox-outage-tv-radio/166680/){:target="_blank" rel="noopener"}

> Cox Media Group tv, radio station streams affected by a reported ransomware attack. [...]
