Title: JBS Foods ransomware gang: White House 'engaging directly' with Russia about attack on massive meat producer
Date: 2021-06-02T15:57:05+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: jbs-foods-ransomware-gang-white-house-engaging-directly-with-russia-about-attack-on-massive-meat-producer

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/02/jbs_fodds_ransomware/){:target="_blank" rel="noopener"}

> Aussie cops start probe and FBI and USDA lend a hand Australian police are investigating a ransomware attack at the facilities of JBS Foods — one of the largest producers of meat in the world – as the White House fingers Russia-based cybercriminals.... [...]
