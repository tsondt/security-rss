Title: Attackers are scanning for vulnerable VMware servers, patch now!
Date: 2021-06-04T14:23:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: attackers-are-scanning-for-vulnerable-vmware-servers-patch-now

[Source](https://www.bleepingcomputer.com/news/security/attackers-are-scanning-for-vulnerable-vmware-servers-patch-now/){:target="_blank" rel="noopener"}

> Threat actors are actively scanning for Internet-exposed VMware vCenter servers unpatched against a critical remote code execution (RCE) vulnerability impacting all vCenter deployments and patched by VMware ten days ago. [...]
