Title: 30M Dell Devices at Risk for Remote BIOS Attacks, RCE
Date: 2021-06-24T10:00:42+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: 30m-dell-devices-at-risk-for-remote-bios-attacks-rce

[Source](https://threatpost.com/dell-bios-attacks-rce/167195/){:target="_blank" rel="noopener"}

> Four separate security bugs would give attackers almost complete control and persistence over targeted devices, thanks to a faulty update mechanism. [...]
