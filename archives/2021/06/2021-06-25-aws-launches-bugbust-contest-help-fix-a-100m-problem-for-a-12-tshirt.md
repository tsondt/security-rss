Title: AWS launches BugBust contest: Help fix a $100m problem for a $12 tshirt
Date: 2021-06-25T21:36:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: aws-launches-bugbust-contest-help-fix-a-100m-problem-for-a-12-tshirt

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/25/aws_bugbust_contest/){:target="_blank" rel="noopener"}

> Did we mention you'll soon be paying to play in 30 days, too? AWS has set up a competition for its customers' developers to find and fix one million bugs.... [...]
