Title: Microsoft hooks up with MITRE to map Azure's ATT&amp;CK surface for 'proactive security'
Date: 2021-06-30T22:02:11+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: microsoft-hooks-up-with-mitre-to-map-azures-attck-surface-for-proactive-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/30/microsoft_mitre_azure/){:target="_blank" rel="noopener"}

> Amazon's AWS next cloud platform in line for adversarial tactics framework MITRE's Centre for Threat-Informed Defence (CTID) and Microsoft have jointly rolled out Security Stack Mappings for Azure, aimed at bringing the former's Adversarial Tactics, Techniques, and Common Knowledge (ATT&CK) framework into the latter's cloud platform – with rival platforms to follow.... [...]
