Title: Asian e-commerce giant Lazada launches first public bug bounty program
Date: 2021-06-14T13:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: asian-e-commerce-giant-lazada-launches-first-public-bug-bounty-program

[Source](https://portswigger.net/daily-swig/asian-e-commerce-giant-lazada-launches-first-public-bug-bounty-program){:target="_blank" rel="noopener"}

> Popular online shopping platform is offering up to $10k for ‘max critical’ vulnerabilities [...]
