Title: Microsoft adds Automatic HTTPS in Edge for secure browsing
Date: 2021-06-01T14:51:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: microsoft-adds-automatic-https-in-edge-for-secure-browsing

[Source](https://www.bleepingcomputer.com/news/security/microsoft-adds-automatic-https-in-edge-for-secure-browsing/){:target="_blank" rel="noopener"}

> Microsoft Edge now can automatically switch users to a secure HTTPS connection when visiting websites over HTTP, after enabling Automatic HTTPS. [...]
