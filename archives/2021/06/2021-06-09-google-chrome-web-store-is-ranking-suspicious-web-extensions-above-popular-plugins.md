Title: Google Chrome Web Store is ranking suspicious web extensions above popular plugins
Date: 2021-06-09T15:33:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-chrome-web-store-is-ranking-suspicious-web-extensions-above-popular-plugins

[Source](https://portswigger.net/daily-swig/google-chrome-web-store-is-ranking-suspicious-web-extensions-above-popular-plugins){:target="_blank" rel="noopener"}

> Privacy add-ons uBlock Origin and AdGuard are among the affected apps [...]
