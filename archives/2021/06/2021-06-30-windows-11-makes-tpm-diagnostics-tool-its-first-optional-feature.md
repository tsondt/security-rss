Title: Windows 11 makes TPM Diagnostics tool its first optional feature
Date: 2021-06-30T15:43:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: windows-11-makes-tpm-diagnostics-tool-its-first-optional-feature

[Source](https://www.bleepingcomputer.com/news/security/windows-11-makes-tpm-diagnostics-tool-its-first-optional-feature/){:target="_blank" rel="noopener"}

> ​Windows 11 comes with a new optional feature called 'TPM Diagnostics' that allows administrators to query the data stored on a device's TPM security processor. [...]
