Title: Customize requests and responses with AWS WAF
Date: 2021-06-21T16:24:34+00:00
Author: Kaustubh Phatak
Category: AWS Security
Tags: AWS WAF;Intermediate (200);Security, Identity, & Compliance;Application development;Application security;Security Blog
Slug: customize-requests-and-responses-with-aws-waf

[Source](https://aws.amazon.com/blogs/security/customize-requests-and-responses-with-aws-waf/){:target="_blank" rel="noopener"}

> In March 2021, AWS introduced support for custom responses and request header insertion with AWS WAF. This blog post will demonstrate how you can use these new features to customize your AWS WAF solution to improve the user experience and security posture of your applications. HTTP response codes are standard responses sent by a server [...]
