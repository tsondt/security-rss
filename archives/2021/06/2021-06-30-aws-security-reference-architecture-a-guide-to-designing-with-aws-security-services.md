Title: AWS Security Reference Architecture:  A guide to designing with AWS security services
Date: 2021-06-30T17:35:24+00:00
Author: Avik Mukherjee
Category: AWS Security
Tags: Advanced (300);Announcements;Security, Identity, & Compliance;AWS Security Reference Architecture;Multi-account security;Security Blog
Slug: aws-security-reference-architecture-a-guide-to-designing-with-aws-security-services

[Source](https://aws.amazon.com/blogs/security/aws-security-reference-architecture-a-guide-to-designing-with-aws-security-services/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is happy to announce the publication of the AWS Security Reference Architecture (AWS SRA). This is a comprehensive set of examples, guides, and design considerations that you can use to deploy the full complement of AWS security services in a multi-account environment that you manage through AWS Organizations. The architecture and [...]
