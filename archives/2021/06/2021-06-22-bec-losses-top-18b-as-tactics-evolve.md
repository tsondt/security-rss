Title: BEC Losses Top $1.8B as Tactics Evolve
Date: 2021-06-22T20:41:10+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: bec-losses-top-18b-as-tactics-evolve

[Source](https://threatpost.com/bec-losses-top-18b/167148/){:target="_blank" rel="noopener"}

> BEC attacks getting are more dangerous, and smart users are the ones who can stop it. [...]
