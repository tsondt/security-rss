Title: Oh FCUK! Fashion Label, Medical Diagnostics Firm Latest REvil Victims
Date: 2021-06-24T19:52:35+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Malware
Slug: oh-fcuk-fashion-label-medical-diagnostics-firm-latest-revil-victims

[Source](https://threatpost.com/fcuk-fashion-medical-diagnostics-revil/167245/){:target="_blank" rel="noopener"}

> The infamous ransomware group hit two big-name companies within hours of each other. [...]
