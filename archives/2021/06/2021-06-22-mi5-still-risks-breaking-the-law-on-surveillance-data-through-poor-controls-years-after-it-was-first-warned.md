Title: MI5 still risks breaking the law on surveillance data through poor controls – years after it was first warned
Date: 2021-06-22T10:46:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: mi5-still-risks-breaking-the-law-on-surveillance-data-through-poor-controls-years-after-it-was-first-warned

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/22/mi5_legal_compliance_ipco_reports/){:target="_blank" rel="noopener"}

> Yet spy agency overseer IPCO seems to be working as the public hoped Exclusive MI5's storage of personal data on espionage subjects is still facing "legal compliance risk" issues despite years of warnings from spy agency regulator IPCO, a Home Office report has revealed.... [...]
