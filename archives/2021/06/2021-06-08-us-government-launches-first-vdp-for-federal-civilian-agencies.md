Title: US government launches first VDP for federal civilian agencies
Date: 2021-06-08T12:41:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-government-launches-first-vdp-for-federal-civilian-agencies

[Source](https://portswigger.net/daily-swig/us-government-launches-first-vdp-for-federal-civilian-agencies){:target="_blank" rel="noopener"}

> Bug bounty vendor Bugcrowd to oversee the project [...]
