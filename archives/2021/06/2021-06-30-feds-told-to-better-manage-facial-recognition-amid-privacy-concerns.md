Title: Feds Told to Better Manage Facial Recognition, Amid Privacy Concerns
Date: 2021-06-30T12:39:15+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Privacy
Slug: feds-told-to-better-manage-facial-recognition-amid-privacy-concerns

[Source](https://threatpost.com/feds-manage-facial-recognition-privacy-concerns/167419/){:target="_blank" rel="noopener"}

> A GAO report finds government agencies are using the technology regularly in criminal investigations and to identify travelers, but need stricter management to protect people’s privacy and avoid inaccurate identification [...]
