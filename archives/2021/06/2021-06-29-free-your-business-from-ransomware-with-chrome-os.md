Title: Free your business from ransomware with Chrome OS
Date: 2021-06-29T17:00:00+00:00
Author: Thomas Riedl
Category: GCP Security
Tags: Chrome Insider;Identity & Security;Hybrid Work;Chrome Enterprise
Slug: free-your-business-from-ransomware-with-chrome-os

[Source](https://cloud.google.com/blog/products/chrome-enterprise/chrome-os-ransomware/){:target="_blank" rel="noopener"}

> We’ve ushered in the era of hybrid working, and the flexibility it allows has proven essential to both businesses and employees. However, a distributed workforce creates technology challenges that make it easier for cyber crime to thrive. Ransomware attacks skyrocketed 150% in 2020 ( Harvard Business Review, Group-IB ) with the global shift to remote working, and just since the start of 2021, we’ve seen ransomware attacks on multiple industries including oil, food, and transportation. Ransomware encrypts your data, making it inaccessible to you and your employees, and the financial implications can be devastating. Many organizations are forced to either [...]
