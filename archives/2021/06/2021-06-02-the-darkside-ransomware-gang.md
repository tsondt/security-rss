Title: The DarkSide Ransomware Gang
Date: 2021-06-02T14:09:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: business of security;crime;cybercrime;ransomware;Russia
Slug: the-darkside-ransomware-gang

[Source](https://www.schneier.com/blog/archives/2021/06/the-darkside-ransomware-gang.html){:target="_blank" rel="noopener"}

> The New York Times has a long story on the DarkSide ransomware gang. A glimpse into DarkSide’s secret communications in the months leading up to the Colonial Pipeline attack reveals a criminal operation on the rise, pulling in millions of dollars in ransom payments each month. DarkSide offers what is known as “ransomware as a service,” in which a malware developer charges a user fee to so-called affiliates like Woris, who may not have the technical skills to actually create ransomware but are still capable of breaking into a victim’s computer systems. DarkSide’s services include providing technical support for hackers, [...]
