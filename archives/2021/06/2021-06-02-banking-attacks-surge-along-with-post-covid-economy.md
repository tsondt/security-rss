Title: Banking Attacks Surge Along with Post-COVID Economy
Date: 2021-06-02T19:44:30+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: banking-attacks-surge-along-with-post-covid-economy

[Source](https://threatpost.com/banking-attacks-surge-along-with-post-covid-economy/166612/){:target="_blank" rel="noopener"}

> FinTech fraud spikes 159 percent in Q1 2021 along with stimulus spending. [...]
