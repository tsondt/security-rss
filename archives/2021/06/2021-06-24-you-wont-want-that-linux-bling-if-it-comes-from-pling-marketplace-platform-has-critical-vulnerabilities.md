Title: You won't want that Linux bling if it comes from Pling: Marketplace platform has critical vulnerabilities
Date: 2021-06-24T22:00:22+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: you-wont-want-that-linux-bling-if-it-comes-from-pling-marketplace-platform-has-critical-vulnerabilities

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/24/pling_linux_flaws/){:target="_blank" rel="noopener"}

> No one wants to be pwned by a drive-by RCE A Berlin startup has disclosed a remote-code-execution (RCE) vulnerability and a wormable cross-site-scripting (XSS) flaw in Pling, which is used by various Linux desktop theme marketplaces.... [...]
