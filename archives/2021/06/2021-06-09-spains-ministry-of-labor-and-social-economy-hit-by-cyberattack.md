Title: Spain's Ministry of Labor and Social Economy hit by cyberattack
Date: 2021-06-09T08:51:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: spains-ministry-of-labor-and-social-economy-hit-by-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/spains-ministry-of-labor-and-social-economy-hit-by-cyberattack/){:target="_blank" rel="noopener"}

> The Spanish Ministry of Labor and Social Economy (MITES) is working on restoring services after being hit by a cyberattack on Wednesday. [...]
