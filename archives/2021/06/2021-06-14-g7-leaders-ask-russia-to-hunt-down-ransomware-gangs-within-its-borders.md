Title: G7 leaders ask Russia to hunt down ransomware gangs within its borders
Date: 2021-06-14T15:47:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: g7-leaders-ask-russia-to-hunt-down-ransomware-gangs-within-its-borders

[Source](https://www.bleepingcomputer.com/news/security/g7-leaders-ask-russia-to-hunt-down-ransomware-gangs-within-its-borders/){:target="_blank" rel="noopener"}

> G7 (Group of 7) leaders have asked Russia to urgently disrupt ransomware gangs believed to be operating within its borders, following a stream of attacks targeting organizations from critical sectors worldwide. [...]
