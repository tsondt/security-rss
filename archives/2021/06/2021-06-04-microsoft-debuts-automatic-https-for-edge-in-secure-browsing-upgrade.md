Title: Microsoft debuts Automatic HTTPS for Edge in secure browsing upgrade
Date: 2021-06-04T16:02:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: microsoft-debuts-automatic-https-for-edge-in-secure-browsing-upgrade

[Source](https://portswigger.net/daily-swig/microsoft-debuts-automatic-https-for-edge-in-secure-browsing-upgrade){:target="_blank" rel="noopener"}

> The feature can be used to automatically switch from HTTP to HTTPS [...]
