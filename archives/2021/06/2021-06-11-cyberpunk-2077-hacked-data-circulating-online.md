Title: Cyberpunk 2077 Hacked Data Circulating Online
Date: 2021-06-11T16:39:10+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Malware;Web Security
Slug: cyberpunk-2077-hacked-data-circulating-online

[Source](https://threatpost.com/cyberpunk-2077-hacked-data-online/166852/){:target="_blank" rel="noopener"}

> CD Projekt Red confirmed that employee and game-related data appears to be floating around the cyber-underground, four months after a hack on the Witcher and Cyberpunk 2077 developer. [...]
