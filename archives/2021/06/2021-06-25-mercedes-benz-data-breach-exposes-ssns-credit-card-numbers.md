Title: Mercedes-Benz data breach exposes SSNs, credit card numbers
Date: 2021-06-25T15:26:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: mercedes-benz-data-breach-exposes-ssns-credit-card-numbers

[Source](https://www.bleepingcomputer.com/news/security/mercedes-benz-data-breach-exposes-ssns-credit-card-numbers/){:target="_blank" rel="noopener"}

> Mercedes-Benz USA has just disclosed a data breach impacting under 1,000 customers and potential buyers that exposed their credit card information, social security numbers, and driver license numbers. [...]
