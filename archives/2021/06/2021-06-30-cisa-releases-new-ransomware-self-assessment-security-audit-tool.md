Title: CISA releases new ransomware self-assessment security audit tool
Date: 2021-06-30T16:26:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-releases-new-ransomware-self-assessment-security-audit-tool

[Source](https://www.bleepingcomputer.com/news/security/cisa-releases-new-ransomware-self-assessment-security-audit-tool/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) has released the Ransomware Readiness Assessment (RRA), a new module for its Cyber Security Evaluation Tool (CSET). [...]
