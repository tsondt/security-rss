Title: Unpatched Bugs Found Lurking in Provisioning Platform Used with Cisco UC
Date: 2021-06-11T21:09:39+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: unpatched-bugs-found-lurking-in-provisioning-platform-used-with-cisco-uc

[Source](https://threatpost.com/unpatched-bugs-provisioning-cisco-uc/166882/){:target="_blank" rel="noopener"}

> A trio of security flaws open the door to remote-code execution and a malware tsunami. [...]
