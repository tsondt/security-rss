Title: The policy of truth: As ransomware claims rise, what's a cyber insurer to do?
Date: 2021-06-04T09:41:10+00:00
Author: Danny Bradbury
Category: The Register
Tags: 
Slug: the-policy-of-truth-as-ransomware-claims-rise-whats-a-cyber-insurer-to-do

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/04/ransomware_claims_feature/){:target="_blank" rel="noopener"}

> Never again is what you swore... the time before If you rely on your insurer to pay off crooks after a successful ransomware attack, you wouldn't be the only one.... [...]
