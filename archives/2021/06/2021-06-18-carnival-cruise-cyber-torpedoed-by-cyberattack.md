Title: Carnival Cruise Cyber-Torpedoed by Cyberattack
Date: 2021-06-18T15:18:50+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks;Web Security
Slug: carnival-cruise-cyber-torpedoed-by-cyberattack

[Source](https://threatpost.com/carnival-cruise-cyberattack/167065/){:target="_blank" rel="noopener"}

> This is the fourth time in a bit over a year that Carnival’s admitted to breaches, with two of them being ransomware attacks. [...]
