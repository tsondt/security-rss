Title: Ukrainian man sentenced to prison for his part in billion-dollar FIN7 cybercrime campaign
Date: 2021-06-25T11:07:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ukrainian-man-sentenced-to-prison-for-his-part-in-billion-dollar-fin7-cybercrime-campaign

[Source](https://portswigger.net/daily-swig/ukrainian-man-sentenced-to-prison-for-his-part-in-billion-dollar-fin7-cybercrime-campaign){:target="_blank" rel="noopener"}

> High-profile threat group sold stolen credit and debit card payment information [...]
