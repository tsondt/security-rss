Title: Norton 360 antivirus now lets you mine Ethereum cryptocurrency
Date: 2021-06-02T13:19:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency;Software
Slug: norton-360-antivirus-now-lets-you-mine-ethereum-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/norton-360-antivirus-now-lets-you-mine-ethereum-cryptocurrency/){:target="_blank" rel="noopener"}

> NortonLifelock has added the ability to mine Ethereum cryptocurrency directly within its Norton 360 antivirus program as a way to "protect" users from malicious mining software. [...]
