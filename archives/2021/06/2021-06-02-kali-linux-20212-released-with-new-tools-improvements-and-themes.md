Title: Kali Linux 2021.2 released with new tools, improvements, and themes
Date: 2021-06-02T11:50:46-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Linux
Slug: kali-linux-20212-released-with-new-tools-improvements-and-themes

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20212-released-with-new-tools-improvements-and-themes/){:target="_blank" rel="noopener"}

> ​Kali Linux 2021.2 was released today by Offensive Security and includes new themes and features, such as access to privileged ports, new tools, and a console-based configuration utility. [...]
