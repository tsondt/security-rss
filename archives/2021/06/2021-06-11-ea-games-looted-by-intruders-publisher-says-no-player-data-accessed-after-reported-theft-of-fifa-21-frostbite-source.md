Title: EA Games looted by intruders: Publisher says 'no player data accessed' after reported theft of FIFA 21, Frostbite source
Date: 2021-06-11T12:25:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ea-games-looted-by-intruders-publisher-says-no-player-data-accessed-after-reported-theft-of-fifa-21-frostbite-source

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/11/ea_games_code_theft/){:target="_blank" rel="noopener"}

> 'Surprise stealing mechanics' made short work of network perimeter security EA Games, publisher of Battlefield, The Sims and FIFA, has admitted to a "recent incident of intrusion into our network" in which attackers reportedly stole game source code and software development kits.... [...]
