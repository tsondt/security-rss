Title: SIP protocol abused to trigger XSS attacks via VoIP call monitoring software
Date: 2021-06-11T12:10:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: sip-protocol-abused-to-trigger-xss-attacks-via-voip-call-monitoring-software

[Source](https://portswigger.net/daily-swig/sip-protocol-abused-to-trigger-xss-attacks-via-voip-call-monitoring-software){:target="_blank" rel="noopener"}

> SIP devices could become unwitting access points for remote attacks on critical systems [...]
