Title: Creating a notification workflow from sensitive data discover with Amazon Macie, Amazon EventBridge, AWS Lambda, and Slack
Date: 2021-06-10T18:31:04+00:00
Author: Bruno Silviera
Category: AWS Security
Tags: Amazon Macie;AWS CloudFormation;AWS Lambda;Intermediate (200);Security, Identity, & Compliance;Amazon EventBridge;GDPR;Security Blog
Slug: creating-a-notification-workflow-from-sensitive-data-discover-with-amazon-macie-amazon-eventbridge-aws-lambda-and-slack

[Source](https://aws.amazon.com/blogs/security/creating-a-notification-workflow-from-sensitive-data-discover-with-amazon-macie-amazon-eventbridge-aws-lambda-and-slack/){:target="_blank" rel="noopener"}

> Following the example of the EU in implementing the General Data Protection Regulation (GDPR), many countries are implementing similar data protection laws. In response, many companies are forming teams that are responsible for data protection. Considering the volume of information that companies maintain, it’s essential that these teams are alerted when sensitive data is at [...]
