Title: Binance exchange helped track down Clop ransomware money launderers
Date: 2021-06-24T15:56:07-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: binance-exchange-helped-track-down-clop-ransomware-money-launderers

[Source](https://www.bleepingcomputer.com/news/security/binance-exchange-helped-track-down-clop-ransomware-money-launderers/){:target="_blank" rel="noopener"}

> Cryptocurrency exchange service Binance played an important part in the recent arrests of Clop ransomware group members, helping law enforcement in their effort to identify, and ultimately detain the suspects. [...]
