Title: GitLab fixes serious SSRF flaw that exposed orgs’ internal servers
Date: 2021-06-17T15:03:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: gitlab-fixes-serious-ssrf-flaw-that-exposed-orgs-internal-servers

[Source](https://portswigger.net/daily-swig/gitlab-fixes-serious-ssrf-flaw-that-exposed-orgs-internal-servers){:target="_blank" rel="noopener"}

> DevSecOops [...]
