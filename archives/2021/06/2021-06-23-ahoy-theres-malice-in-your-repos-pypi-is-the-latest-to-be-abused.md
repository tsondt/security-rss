Title: Ahoy, there’s malice in your repos—PyPI is the latest to be abused
Date: 2021-06-23T12:38:42+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;counterfeit;malware;npm;open source;pypi;rubygems
Slug: ahoy-theres-malice-in-your-repos-pypi-is-the-latest-to-be-abused

[Source](https://arstechnica.com/?p=1775539){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Counterfeit packages downloaded roughly 5,000 times from the official Python repository contained secret code that installed cryptomining software on infected machines, a security researcher has found. The malicious packages, which were available on the PyPI repository, in many cases used names that mimicked those of legitimate and often widely used packages already available there, Ax Sharma, a researcher at security firm Sonatype reported. So-called typosquatting attacks succeed when targets accidentally mistype a name such as typing “mplatlib” or “maratlib” instead of the legitimate and popular package matplotlib. Sharma said he found six packages that installed cryptomining [...]
