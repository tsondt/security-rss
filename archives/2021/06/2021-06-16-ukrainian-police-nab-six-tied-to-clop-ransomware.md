Title: Ukrainian Police Nab Six Tied to CLOP Ransomware
Date: 2021-06-16T14:42:42+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;Accellion;ATT&CK;Babuk;Clop;DarkSide;File Transfer Appliance;Intel 471;Jones Day;Krogers;MITRE;Qualys;ransomware;SingTel;Stanford University Medical School;University of Maryland and the University of California
Slug: ukrainian-police-nab-six-tied-to-clop-ransomware

[Source](https://krebsonsecurity.com/2021/06/ukrainian-police-nab-six-tied-to-clop-ransomware/){:target="_blank" rel="noopener"}

> Authorities in Ukraine this week charged six people alleged to be part of the CLOP ransomware group, a cybercriminal gang said to have extorted more than half a billion dollars from victims. Some of CLOP’s victims this year alone include Stanford University Medical School, the University of California, and University of Maryland. A still shot from a video showing Ukrainian police seizing a Tesla, one of many high-end vehicles seized in this week’s raids on the Clop gang. According to a statement and videos released today, the Ukrainian Cyber Police charged six defendants with various computer crimes linked to the [...]
