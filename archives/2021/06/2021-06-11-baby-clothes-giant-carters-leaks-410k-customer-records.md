Title: Baby Clothes Giant Carter’s Leaks 410K Customer Records
Date: 2021-06-11T18:29:48+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Web Security
Slug: baby-clothes-giant-carters-leaks-410k-customer-records

[Source](https://threatpost.com/baby-clothes-carters-leaks-customer-records/166866/){:target="_blank" rel="noopener"}

> Purchase automation software delivered shortened URLs without protections. [...]
