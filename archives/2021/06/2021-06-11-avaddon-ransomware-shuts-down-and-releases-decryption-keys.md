Title: Avaddon ransomware shuts down and releases decryption keys
Date: 2021-06-11T12:10:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: avaddon-ransomware-shuts-down-and-releases-decryption-keys

[Source](https://www.bleepingcomputer.com/news/security/avaddon-ransomware-shuts-down-and-releases-decryption-keys/){:target="_blank" rel="noopener"}

> The Avaddon ransomware gang has shut down operation and released the decryption keys for their victims to BleepingComputer.com. [...]
