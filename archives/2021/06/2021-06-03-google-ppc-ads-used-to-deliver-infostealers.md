Title: Google PPC Ads Used to Deliver Infostealers
Date: 2021-06-03T18:20:15+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: google-ppc-ads-used-to-deliver-infostealers

[Source](https://threatpost.com/google-ppc-ads-used-to-deliver-infostealers/166644/){:target="_blank" rel="noopener"}

> The crooks pay top dollar for Google search results for the popular AnyDesk, Dropbox & Telegram apps that lead to a malicious, infostealer-packed website. [...]
