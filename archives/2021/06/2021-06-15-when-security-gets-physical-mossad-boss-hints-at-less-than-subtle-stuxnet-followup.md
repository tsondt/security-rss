Title: When security gets physical: Mossad boss hints at less-than-subtle Stuxnet followup
Date: 2021-06-15T07:24:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: when-security-gets-physical-mossad-boss-hints-at-less-than-subtle-stuxnet-followup

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/15/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Alleged Trickbot developer cuffed by US agents In brief The outgoing head of Israeli foreign intelligence service Mossad has suggested that Stuxnet wasn't the only spanner in the works his agency put into Iran's nuclear programme.... [...]
