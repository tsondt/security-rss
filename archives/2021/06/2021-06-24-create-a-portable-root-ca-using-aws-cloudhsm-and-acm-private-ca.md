Title: Create a portable root CA using AWS CloudHSM and ACM Private CA
Date: 2021-06-24T17:30:04+00:00
Author: J.D. Bean
Category: AWS Security
Tags: AWS Certificate Manager;AWS CloudHSM;Expert (400);Security, Identity, & Compliance;Private Certificate Authority;Security Blog
Slug: create-a-portable-root-ca-using-aws-cloudhsm-and-acm-private-ca

[Source](https://aws.amazon.com/blogs/security/create-a-portable-root-ca-using-aws-cloudhsm-and-acm-private-ca/){:target="_blank" rel="noopener"}

> With AWS Certificate Manager Private Certificate Authority (ACM Private CA) you can create private certificate authority (CA) hierarchies, including root and subordinate CAs, without the investment and maintenance costs of operating an on-premises CA. In this post, I will explain how you can use ACM Private CA with AWS CloudHSM to operate a hybrid public [...]
