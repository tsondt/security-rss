Title: A blueprint for secure infrastructure on Google Cloud
Date: 2021-06-24T19:00:00+00:00
Author: Alicia Williams
Category: GCP Security
Tags: Google Cloud;Identity & Security;DevOps & SRE;Developers & Practitioners
Slug: a-blueprint-for-secure-infrastructure-on-google-cloud

[Source](https://cloud.google.com/blog/topics/developers-practitioners/blueprint-secure-infrastructure-google-cloud/){:target="_blank" rel="noopener"}

> When it comes to infrastructure security, every stakeholder has the same goal: maintain the confidentiality and integrity of their company’s data and systems. Period. Developing and operating in the Cloud provides the opportunity to achieve these goals by being more secure and having greater visibility and governance over your resources and data. This is due to the relatively uniform environment of cloud infrastructure (as compared with on-prem) and inherent service-centric architecture. In addition, cloud providers take on some of the key responsibilities for security doing their part in a shared responsibility model. However, translating this shared goal into reality can [...]
