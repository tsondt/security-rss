Title: ADATA suffers 700 GB data leak in Ragnar Locker ransomware attack
Date: 2021-06-21T11:56:19-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: adata-suffers-700-gb-data-leak-in-ragnar-locker-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/adata-suffers-700-gb-data-leak-in-ragnar-locker-ransomware-attack/){:target="_blank" rel="noopener"}

> The Ragnar Locker ransomware gang have published download links for more than 700GB of archived data stolen from Taiwanese memory and storage chip maker ADATA. [...]
