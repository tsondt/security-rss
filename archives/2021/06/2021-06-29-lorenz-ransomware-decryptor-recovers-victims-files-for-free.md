Title: Lorenz ransomware decryptor recovers victims' files for free
Date: 2021-06-29T20:59:31-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: lorenz-ransomware-decryptor-recovers-victims-files-for-free

[Source](https://www.bleepingcomputer.com/news/security/lorenz-ransomware-decryptor-recovers-victims-files-for-free/){:target="_blank" rel="noopener"}

> Dutch cybersecurity firm Tesorion has released a free decryptor for the Lorenz ransomware, allowing victims to recover some of their files for free without paying a ransom. [...]
