Title: 5G Security Vulnerabilities Fluster Mobile Operators
Date: 2021-06-28T21:17:16+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Mobile Security
Slug: 5g-security-vulnerabilities-fluster-mobile-operators

[Source](https://threatpost.com/mobile-operators-5g-security-vulnerabilities/167354/){:target="_blank" rel="noopener"}

> A survey from GSMA and Trend Micro shows a concerning lack of security capabilities for private 5G networks (think factories, smart cities, industrial IoT, utilities and more). [...]
