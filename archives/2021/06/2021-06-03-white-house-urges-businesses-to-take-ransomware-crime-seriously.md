Title: White House urges businesses to "take ransomware crime seriously"
Date: 2021-06-03T09:56:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: white-house-urges-businesses-to-take-ransomware-crime-seriously

[Source](https://www.bleepingcomputer.com/news/security/white-house-urges-businesses-to-take-ransomware-crime-seriously/){:target="_blank" rel="noopener"}

> The White House has urged business leaders and corporate executives to "take ransomware crime seriously" in a letter issued by Anne Neuberger, the National Security Council's chief cybersecurity adviser. [...]
