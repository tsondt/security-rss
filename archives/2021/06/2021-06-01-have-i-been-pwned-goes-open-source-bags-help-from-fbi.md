Title: Have I Been Pwned goes open source, bags help from FBI
Date: 2021-06-01T01:47:51+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: have-i-been-pwned-goes-open-source-bags-help-from-fbi

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/01/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: More Rowhammer research, Feds warn of Fortinet attacks, etc In brief The creator of the Have I Been Pwned (HIBP) website, which alerts you if it turns out your credentials have been swiped and leaked from an account database, has open sourced the project's internals.... [...]
