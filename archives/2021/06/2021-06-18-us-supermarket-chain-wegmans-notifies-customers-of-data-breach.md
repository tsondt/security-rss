Title: US supermarket chain Wegmans notifies customers of data breach
Date: 2021-06-18T09:15:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-supermarket-chain-wegmans-notifies-customers-of-data-breach

[Source](https://www.bleepingcomputer.com/news/security/us-supermarket-chain-wegmans-notifies-customers-of-data-breach/){:target="_blank" rel="noopener"}

> Wegmans Food Markets notified customers that some of their information was exposed after the company became aware that two of its databases were publicly accessible on the Internet because of a configuration issue. [...]
