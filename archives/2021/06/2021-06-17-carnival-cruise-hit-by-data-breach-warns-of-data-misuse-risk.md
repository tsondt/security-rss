Title: Carnival Cruise hit by data breach, warns of data misuse risk
Date: 2021-06-17T12:15:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: carnival-cruise-hit-by-data-breach-warns-of-data-misuse-risk

[Source](https://www.bleepingcomputer.com/news/security/carnival-cruise-hit-by-data-breach-warns-of-data-misuse-risk/){:target="_blank" rel="noopener"}

> Carnival Corporation, the world's largest cruise ship operator, has disclosed a data breach after attackers breached some email accounts and accessed personal, financial, and health information belonging to customers, employees, and crew. [...]
