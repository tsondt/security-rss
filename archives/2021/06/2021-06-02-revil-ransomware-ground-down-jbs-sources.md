Title: REvil Ransomware Ground Down JBS: Sources
Date: 2021-06-02T15:52:53+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: revil-ransomware-ground-down-jbs-sources

[Source](https://threatpost.com/revil-ransomware-ground-down-jbs-sources/166597/){:target="_blank" rel="noopener"}

> Responsible nations don't harbor cybercrooks, the Biden administration admonished Russia, home to the gang that reportedly froze the global food distributor's systems. [...]
