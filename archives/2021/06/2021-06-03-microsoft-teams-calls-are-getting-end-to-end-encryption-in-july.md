Title: Microsoft Teams calls are getting end-to-end encryption in July
Date: 2021-06-03T17:22:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-teams-calls-are-getting-end-to-end-encryption-in-july

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-teams-calls-are-getting-end-to-end-encryption-in-july/){:target="_blank" rel="noopener"}

> Microsoft Teams is getting better security and privacy next month with the addition of end-to-end encrypted 1:1 voice calls. [...]
