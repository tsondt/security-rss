Title: Ransomware attack on world’s biggest meat supplier JBS ‘came from Russia’
Date: 2021-06-02T13:25:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ransomware-attack-on-worlds-biggest-meat-supplier-jbs-came-from-russia

[Source](https://portswigger.net/daily-swig/ransomware-attack-on-worlds-biggest-meat-supplier-jbs-came-from-russia){:target="_blank" rel="noopener"}

> Company points finger after cyber-attack on US and Australian offices [...]
