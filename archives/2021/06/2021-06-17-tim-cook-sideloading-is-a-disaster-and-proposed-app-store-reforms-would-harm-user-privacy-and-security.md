Title: Tim Cook: Sideloading is a disaster and proposed App Store reforms would harm user privacy and security
Date: 2021-06-17T14:47:06+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: tim-cook-sideloading-is-a-disaster-and-proposed-app-store-reforms-would-harm-user-privacy-and-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/17/tim_cook_interview_sideloading/){:target="_blank" rel="noopener"}

> Apple CEO stays on message during interview while Epic case rumbles along Tim Cook has claimed that proposed reforms to the App Store are "not in the best interests of the user" and would "destroy the security of the iPhone."... [...]
