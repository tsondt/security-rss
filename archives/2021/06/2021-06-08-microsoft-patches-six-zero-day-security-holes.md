Title: Microsoft Patches Six Zero-Day Security Holes
Date: 2021-06-08T20:53:28+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;adobe;Automox;Christopher Hass;CVE-2021-28550;CVE-2021-31199;CVE-2021-31201;CVE-2021-31955;CVE-2021-31956;CVE-2021-31959;CVE-2021-31963;CVE-2021-33739;CVE-2021-33742;Immersive Labs;Kevin Breen;microsoft;Microsoft Patch Tuesday June 2021;Sharepoint;windows
Slug: microsoft-patches-six-zero-day-security-holes

[Source](https://krebsonsecurity.com/2021/06/microsoft-patches-six-zero-day-security-holes/){:target="_blank" rel="noopener"}

> Microsoft today released another round of security updates for Windows operating systems and supported software, including fixes for six zero-day bugs that malicious hackers already are exploiting in active attacks. June’s Patch Tuesday addresses just 49 security holes — about half the normal number of vulnerabilities lately. But what this month lacks in volume it makes up for in urgency: Microsoft warns that bad guys are leveraging a half-dozen of those weaknesses to break into computers in targeted attacks. Among the zero-days are: – CVE-2021-33742, a remote code execution bug in a Windows HTML component. – CVE-2021-31955, an information disclosure [...]
