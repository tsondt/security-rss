Title: Asia-Pacific internet registry APNIC says WHOIS admin passwords were mistakenly exposed for three months
Date: 2021-06-22T14:39:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: asia-pacific-internet-registry-apnic-says-whois-admin-passwords-were-mistakenly-exposed-for-three-months

[Source](https://portswigger.net/daily-swig/asia-pacific-internet-registry-apnic-says-whois-admin-passwords-were-mistakenly-exposed-for-three-months){:target="_blank" rel="noopener"}

> Internet org downplays threat to integrity of domain name registry [...]
