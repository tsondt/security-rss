Title: Ransomware-skewered meat producer JBS confesses to paying $11m for its freedom
Date: 2021-06-10T05:57:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: ransomware-skewered-meat-producer-jbs-confesses-to-paying-11m-for-its-freedom

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/10/jbs_foods_pays_ransom/){:target="_blank" rel="noopener"}

> Company also says large and well-funded IT department sorted recovery swiftly JBS Foods, one of the world’s largest meat producers, has revealed it handed over “the equivalent of $11 million” to resolve a ransomware infection that disrupted operations in Australia, the USA, and Canada.... [...]
