Title: Malicious PDFs Flood the Web, Lead to Password-Snarfing
Date: 2021-06-15T17:05:28+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: malicious-pdfs-flood-the-web-lead-to-password-snarfing

[Source](https://threatpost.com/rotten-pdfs-flood-web-password-snarfing/166932/){:target="_blank" rel="noopener"}

> SolarMarker makers are using SEO poisoning, stuffing thousands of PDFs with tens of thousands of pages full of SEO keywords & links to redirect to the malware. [...]
