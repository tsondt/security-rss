Title: AWS Verified episode 6: A conversation with Reeny Sondhi of Autodesk
Date: 2021-06-29T17:13:05+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Autodesk;Compliance;cybersecurity;data privacy;Security Blog;Verified
Slug: aws-verified-episode-6-a-conversation-with-reeny-sondhi-of-autodesk

[Source](https://aws.amazon.com/blogs/security/aws-verified-episode-6-a-conversation-with-reeny-sondhi-of-autodesk/){:target="_blank" rel="noopener"}

> I’m happy to share the latest episode of AWS Verified, where we bring you global conversations with leaders about issues impacting cybersecurity, privacy, and the cloud. We take this opportunity to meet with leaders from various backgrounds in security, technology, and leadership. For our latest episode of Verified, I had the opportunity to meet virtually [...]
