Title: Remember those wacky cyberpunk costumes in <i>Hackers</i>? They're on display in London this week
Date: 2021-06-01T14:32:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: remember-those-wacky-cyberpunk-costumes-in-hackers-theyre-on-display-in-london-this-week

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/01/hackers_movie_costumes/){:target="_blank" rel="noopener"}

> 'Medieval mixed with athletic wear' chic from 1995 Fans of 'cyber' flick Hackers can amuse themselves by visiting an exhibition of the characters’ costumes in London – but time is running short if you want to catch a glimpse of Angelina Jolie’s bizarre getups.... [...]
