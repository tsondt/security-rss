Title: Mollitiam Industries is the Newest Cyberweapons Arms Manufacturer
Date: 2021-06-23T11:01:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberweapons;hacking;key logging;malware;spyware
Slug: mollitiam-industries-is-the-newest-cyberweapons-arms-manufacturer

[Source](https://www.schneier.com/blog/archives/2021/06/mollitiam-industries-is-the-newest-cyberweapons-arms-manufacturer.html){:target="_blank" rel="noopener"}

> Wired is reporting on a company called Mollitiam Industries: Marketing materials left exposed online by a third-party claim Mollitiam’s interception products, dubbed “Invisible Man” and “Night Crawler,” are capable of remotely accessing a target’s files, location, and covertly turning on a device’s camera and microphone. Its spyware is also said to be equipped with a keylogger, which means every keystroke made on an infected device — including passwords, search queries and messages sent via encrypted messaging apps — can be tracked and monitored. To evade detection, the malware makes use of the company’s so-called “invisible low stealth technology” and its [...]
