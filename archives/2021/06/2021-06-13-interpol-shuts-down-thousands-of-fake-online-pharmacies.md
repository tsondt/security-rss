Title: Interpol shuts down thousands of fake online pharmacies
Date: 2021-06-13T10:27:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: interpol-shuts-down-thousands-of-fake-online-pharmacies

[Source](https://www.bleepingcomputer.com/news/security/interpol-shuts-down-thousands-of-fake-online-pharmacies/){:target="_blank" rel="noopener"}

> The Interpol (International Criminal Police Organisation) has taken down thousands of online marketplaces that posed as pharmacies and pushed dangerous fake and illicit drugs and medicine. [...]
