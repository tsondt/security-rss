Title: Tulsa warns of data breach after Conti ransomware leaks police citations
Date: 2021-06-23T11:53:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: tulsa-warns-of-data-breach-after-conti-ransomware-leaks-police-citations

[Source](https://www.bleepingcomputer.com/news/security/tulsa-warns-of-data-breach-after-conti-ransomware-leaks-police-citations/){:target="_blank" rel="noopener"}

> The City of Tulsa, Oklahoma, is warning residents that their personal data may have been exposed after a ransomware gang published police citations online. [...]
