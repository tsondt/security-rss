Title: Papa don't breach: UK data watchdog fines that other pizza place £10,000 over unsolicited marketing blitz
Date: 2021-06-16T12:03:24+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: papa-dont-breach-uk-data-watchdog-fines-that-other-pizza-place-ps10000-over-unsolicited-marketing-blitz

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/16/papa_johns_ico_fine/){:target="_blank" rel="noopener"}

> Papa John's falls foul of 'soft opt-in' exemption in PECR rules Pizza takeaway and delivery outfit Papa John's has been fined £10,000 by the UK's data watchdog for sending marketing fluff to punters without their say-so.... [...]
