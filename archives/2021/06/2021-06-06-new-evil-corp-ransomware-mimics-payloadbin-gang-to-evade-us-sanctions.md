Title: New Evil Corp ransomware mimics PayloadBin gang to evade US sanctions
Date: 2021-06-06T16:52:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: new-evil-corp-ransomware-mimics-payloadbin-gang-to-evade-us-sanctions

[Source](https://www.bleepingcomputer.com/news/security/new-evil-corp-ransomware-mimics-payloadbin-gang-to-evade-us-sanctions/){:target="_blank" rel="noopener"}

> The new PayloadBIN ransomware has been attributed to the Evil Corp cybercrime gang, rebranding to evade sanctions imposed by the US Treasury Department's Office of Foreign Assets Control (OFAC). [...]
