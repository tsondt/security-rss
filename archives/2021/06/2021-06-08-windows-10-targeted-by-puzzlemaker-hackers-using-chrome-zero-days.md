Title: Windows 10 targeted by PuzzleMaker hackers using Chrome zero-days
Date: 2021-06-08T14:20:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: windows-10-targeted-by-puzzlemaker-hackers-using-chrome-zero-days

[Source](https://www.bleepingcomputer.com/news/security/windows-10-targeted-by-puzzlemaker-hackers-using-chrome-zero-days/){:target="_blank" rel="noopener"}

> Kaspersky security researchers discovered a new threat actor dubbed PuzzleMaker, who has used a chain of Google Chrome and Windows 10 zero-day exploits in highly-targeted attacks against multiple companies worldwide. [...]
