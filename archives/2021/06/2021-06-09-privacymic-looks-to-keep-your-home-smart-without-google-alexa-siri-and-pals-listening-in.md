Title: PrivacyMic looks to keep your home smart without Google, Alexa, Siri and pals listening in
Date: 2021-06-09T18:03:09+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: privacymic-looks-to-keep-your-home-smart-without-google-alexa-siri-and-pals-listening-in

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/09/privacymic_smart_home/){:target="_blank" rel="noopener"}

> Raspberry Pi-powered prototype proves 95% accurate, 100% private, claim boffins Researchers at the University of Michigan have proposed a way to have your privacy cake and eat your home automation too. They've found a means of using a voice-activated smart speaker system without it having to listen to everything you say – and no, it's not "pressing a button."... [...]
