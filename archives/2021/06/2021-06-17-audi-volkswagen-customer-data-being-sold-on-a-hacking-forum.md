Title: Audi, Volkswagen customer data being sold on a hacking forum
Date: 2021-06-17T14:48:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: audi-volkswagen-customer-data-being-sold-on-a-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/audi-volkswagen-customer-data-being-sold-on-a-hacking-forum/){:target="_blank" rel="noopener"}

> Audi and Volkswagen customer data is being sold on a hacking forum after allegedly being stolen from an exposed Azure BLOB container. [...]
