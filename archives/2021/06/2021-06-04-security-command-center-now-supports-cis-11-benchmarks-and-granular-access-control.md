Title: Security Command Center now supports CIS 1.1 benchmarks and granular access control
Date: 2021-06-04T16:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: security-command-center-now-supports-cis-11-benchmarks-and-granular-access-control

[Source](https://cloud.google.com/blog/products/identity-security/new-capabilities-in-security-command-center-help-manage-risk/){:target="_blank" rel="noopener"}

> Security Command Center (SCC) is our native Google Cloud product that helps manage and improve your cloud security and risk posture. As a native offering, SCC is constantly evolving and adding new capabilities that deliver more insight to security practitioners. We’ve just released new capabilities in Security Command Center Premium that enable organizations to improve their security posture and efficiently manage risk for their Google Cloud environment. SCC now supports CIS benchmarks for Google Cloud Platform Foundation v1.1, enabling you to monitor and address compliance violations against industry best practices in your Google Cloud environment. Additionally, SCC now supports fine-grained [...]
