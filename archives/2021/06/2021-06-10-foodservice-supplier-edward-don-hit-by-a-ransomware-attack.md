Title: Foodservice supplier Edward Don hit by a ransomware attack
Date: 2021-06-10T18:44:56-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: foodservice-supplier-edward-don-hit-by-a-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/foodservice-supplier-edward-don-hit-by-a-ransomware-attack/){:target="_blank" rel="noopener"}

> Foodservice supplier Edward Don has suffered a ransomware attack that has caused the company to shut down portions of the network to prevent the attack's spread. [...]
