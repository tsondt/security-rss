Title: Supreme Court narrows Computer Fraud and Abuse Act: Misusing access not quite the same as breaking in
Date: 2021-06-03T20:45:31+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: supreme-court-narrows-computer-fraud-and-abuse-act-misusing-access-not-quite-the-same-as-breaking-in

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/03/supreme_court_cfaa/){:target="_blank" rel="noopener"}

> We'll explain everything for you The US Supreme Court on Thursday limited the scope of the 1986 Computer Fraud and Abuse Act (CFAA) in a ruling that found a former sergeant did not violate the law by misusing his access to a police database.... [...]
