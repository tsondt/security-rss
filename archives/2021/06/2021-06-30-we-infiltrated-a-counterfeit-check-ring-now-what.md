Title: We Infiltrated a Counterfeit Check Ring! Now What?
Date: 2021-06-30T20:34:54+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;The Coming Storm;Agari;B. Ware;BEC fraud;Brianna Ware;car wrap scam;CEO scams;romance scams;Ronnie Tokazowski;Scattered Canara
Slug: we-infiltrated-a-counterfeit-check-ring-now-what

[Source](https://krebsonsecurity.com/2021/06/we-infiltrated-a-counterfeit-check-ring-now-what/){:target="_blank" rel="noopener"}

> Imagine waking up each morning knowing the identities of thousands of people who are about to be mugged for thousands of dollars each. You know exactly when and where each of those muggings will take place, and you’ve shared this information in advance with the authorities each day for a year with no outward indication that they are doing anything about it. How frustrated would you be? A counterfeit check image [redacted] that was intended for a person helping this fraud gang print and mail phony checks tied to a raft of email-based scams. One fraud-fighting group is intercepting hundreds [...]
