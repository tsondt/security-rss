Title: Network security firm COO charged with medical center cyberattack
Date: 2021-06-11T09:03:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: network-security-firm-coo-charged-with-medical-center-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/network-security-firm-coo-charged-with-medical-center-cyberattack/){:target="_blank" rel="noopener"}

> The former chief operating officer of Securolytics, a network security company providing services for the health care industry, was charged with allegedly conducting a cyberattack on Georgia-based Gwinnett Medical Center (GMC). [...]
