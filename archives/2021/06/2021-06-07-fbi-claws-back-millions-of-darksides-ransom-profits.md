Title: FBI Claws Back Millions of DarkSide’s Ransom Profits
Date: 2021-06-07T20:54:07+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Web Security
Slug: fbi-claws-back-millions-of-darksides-ransom-profits

[Source](https://threatpost.com/fbi-claws-back-millions-darksides-ransom/166705/){:target="_blank" rel="noopener"}

> The tables have been turned, the FBI & DOJ said after announcing the use of blockchain technology to track down the contents of DarkSide's cryptocurrency wallet. [...]
