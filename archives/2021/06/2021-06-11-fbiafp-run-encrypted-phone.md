Title: FBI/AFP-Run Encrypted Phone
Date: 2021-06-11T11:32:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Australia;backdoors;encryption;FBI;law enforcement;trust
Slug: fbiafp-run-encrypted-phone

[Source](https://www.schneier.com/blog/archives/2021/06/fbi-afp-run-encrypted-phone.html){:target="_blank" rel="noopener"}

> For three years, the Federal Bureau of Investigation and the Australian Federal Police owned and operated a commercial encrypted phone app, called AN0M, that was used by organized crime around the world. Of course, the police were able to read everything — I don’t even know if this qualifies as a backdoor. This week, the world’s police organizations announced 800 arrests based on text messages sent over the app. We’ve seen law enforcement take over encrypted apps before: for example, EncroChat. This operation, code-named Trojan Shield, is the first time law enforcement managed an app from the beginning. If there [...]
