Title: Fashion firm French Connection says 'FCUK' as REvil-linked breach sees company data stolen
Date: 2021-06-24T08:30:12+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: fashion-firm-french-connection-says-fcuk-as-revil-linked-breach-sees-company-data-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/24/french_connection_says_fcuk_as/){:target="_blank" rel="noopener"}

> Attack on an internal system shouldn't put customers at risk, company claims Cheeky clothing firm French Connection, also known as FCUK, has become the latest victim of ransomware, with a gang understood to be linked to REvil having penetrated its back-end - making off with a selection of private internal data.... [...]
