Title: Security organizations join forces with EFF to lobby for DMCA reform
Date: 2021-06-24T16:45:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-organizations-join-forces-with-eff-to-lobby-for-dmca-reform

[Source](https://portswigger.net/daily-swig/security-organizations-join-forces-with-eff-to-lobby-for-dmca-reform){:target="_blank" rel="noopener"}

> ‘Good faith’ security research needs shielding from legal blunderbuss [...]
