Title: Embryology Data Breach Follows Fertility Clinic Ransomware Hit
Date: 2021-06-21T19:56:22+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Malware;Web Security
Slug: embryology-data-breach-follows-fertility-clinic-ransomware-hit

[Source](https://threatpost.com/embryology-data-breach-fertility-clinic-ransomware/167087/){:target="_blank" rel="noopener"}

> Approximately 38,000 of RBA's customers had their embryology data stolen by a ransomware gang. [...]
