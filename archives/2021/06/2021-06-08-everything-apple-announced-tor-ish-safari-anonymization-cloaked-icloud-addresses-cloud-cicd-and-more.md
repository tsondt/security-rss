Title: Everything Apple announced: Tor-ish Safari anonymization. Cloaked iCloud addresses. Cloud CI/CD. And more
Date: 2021-06-08T00:10:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: everything-apple-announced-tor-ish-safari-anonymization-cloaked-icloud-addresses-cloud-cicd-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/08/apple_2021_wwdc/){:target="_blank" rel="noopener"}

> No new hardware though loads of bits and bytes for software makers WWDC Apple on Monday opened its 2021 Worldwide Developer Conference by promising a raft of operating system and privacy improvements – including a relay system to anonymize Safari connections, and randomized email addresses for online account signups.... [...]
