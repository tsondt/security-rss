Title: Korenix patches multiple critical vulnerabilities in networking devices
Date: 2021-06-04T11:22:25+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: korenix-patches-multiple-critical-vulnerabilities-in-networking-devices

[Source](https://portswigger.net/daily-swig/korenix-patches-multiple-critical-vulnerabilities-in-networking-devices){:target="_blank" rel="noopener"}

> Security flaws could allow attackers unauthorized access [...]
