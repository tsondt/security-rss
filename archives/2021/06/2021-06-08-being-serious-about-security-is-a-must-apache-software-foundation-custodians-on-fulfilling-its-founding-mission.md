Title: ‘Being serious about security is a must’ – Apache Software Foundation custodians on fulfilling its founding mission
Date: 2021-06-08T15:58:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: being-serious-about-security-is-a-must-apache-software-foundation-custodians-on-fulfilling-its-founding-mission

[Source](https://portswigger.net/daily-swig/being-serious-about-security-is-a-must-apache-software-foundation-custodians-on-fulfilling-its-founding-mission){:target="_blank" rel="noopener"}

> Board members offer a behind-the-scenes look at the non-profit [...]
