Title: US convicts Russian national behind Kelihos botnet crypting service
Date: 2021-06-16T12:22:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-convicts-russian-national-behind-kelihos-botnet-crypting-service

[Source](https://www.bleepingcomputer.com/news/security/us-convicts-russian-national-behind-kelihos-botnet-crypting-service/){:target="_blank" rel="noopener"}

> Russian national Oleg Koshkin was convicted for charges related to the operation of a malware crypter service used by the Kelihos botnet to obfuscate malware payloads and evade detection. [...]
