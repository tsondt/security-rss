Title: Cyberattack Forces Meat Producer to Shut Down Operations in U.S., Australia
Date: 2021-06-01T12:57:58+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: cyberattack-forces-meat-producer-to-shut-down-operations-in-us-australia

[Source](https://threatpost.com/cyberattack-meat-producer-shut-down/166560/){:target="_blank" rel="noopener"}

> Global food distributor JBS Foods suffered an unspecified incident over the weekend that disrupted several servers supporting IT systems and could affect the supply chain for some time. [...]
