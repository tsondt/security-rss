Title: Iran Media Websites Seized by U.S. in Disinformation Campaign
Date: 2021-06-23T19:23:35+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Web Security
Slug: iran-media-websites-seized-by-us-in-disinformation-campaign

[Source](https://threatpost.com/iran-media-websites-seized-disinformation/167198/){:target="_blank" rel="noopener"}

> DoJ uses sanctions laws to shut down an alleged Iranian government malign influence campaign. [...]
