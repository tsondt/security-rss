Title: REvil ransomware hits US nuclear weapons contractor
Date: 2021-06-14T17:32:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: revil-ransomware-hits-us-nuclear-weapons-contractor

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-hits-us-nuclear-weapons-contractor/){:target="_blank" rel="noopener"}

> US nuclear weapons contractor Sol Oriens has suffered a cyberattack allegedly at the hands of the REvil ransomware gang, which claims to be auctioning data stolen during the attack. [...]
