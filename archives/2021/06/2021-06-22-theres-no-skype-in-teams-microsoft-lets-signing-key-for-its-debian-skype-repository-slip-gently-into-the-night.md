Title: There's no 'Skype' in Teams: Microsoft lets signing key for its Debian Skype repository slip gently into the night
Date: 2021-06-22T20:05:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: theres-no-skype-in-teams-microsoft-lets-signing-key-for-its-debian-skype-repository-slip-gently-into-the-night

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/22/skype_gpg_expire/){:target="_blank" rel="noopener"}

> Summer Solstice: A time for dancing, druids, and certificate errors Microsoft's inattentive approach to Linux has continued unabated, with reports that the signing key for its Debian Skype repository has expired.... [...]
