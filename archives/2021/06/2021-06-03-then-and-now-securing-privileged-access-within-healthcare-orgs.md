Title: Then and Now: Securing Privileged Access Within Healthcare Orgs
Date: 2021-06-03T12:00:56+00:00
Author: Joseph Carson
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;InfoSec Insider;Vulnerabilities;Web Security
Slug: then-and-now-securing-privileged-access-within-healthcare-orgs

[Source](https://threatpost.com/securing-privileged-access-healthcare/166477/){:target="_blank" rel="noopener"}

> Joseph Carson, chief security scientist and advisory CISO at ThycoticCentrify, discusses best practices for securing healthcare data against the modern threat landscape. [...]
