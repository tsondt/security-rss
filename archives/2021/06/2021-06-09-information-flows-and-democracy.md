Title: Information Flows and Democracy
Date: 2021-06-09T11:46:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;disinformation;national security policy
Slug: information-flows-and-democracy

[Source](https://www.schneier.com/blog/archives/2021/06/information-flows-and-democracy.html){:target="_blank" rel="noopener"}

> Henry Farrell and I published a paper on fixing American democracy: “ Rechanneling Beliefs: How Information Flows Hinder or Help Democracy.” It’s much easier for democratic stability to break down than most people realize, but this doesn’t mean we must despair over the future. It’s possible, though very difficult, to back away from our current situation towards one of greater democratic stability. This wouldn’t entail a restoration of a previous status quo. Instead, it would recognize that the status quo was less stable than it seemed, and a major source of the tensions that have started to unravel it. What [...]
