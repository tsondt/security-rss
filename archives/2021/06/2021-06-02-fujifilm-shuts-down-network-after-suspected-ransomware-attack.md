Title: FUJIFILM shuts down network after suspected ransomware attack
Date: 2021-06-02T15:03:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fujifilm-shuts-down-network-after-suspected-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/fujifilm-shuts-down-network-after-suspected-ransomware-attack/){:target="_blank" rel="noopener"}

> FujiFilm is investigating a ransomware attack and has shut down portions of its network to prevent the attack's spread. [...]
