Title: Poltergeist attack could leave autonomous vehicles blind to obstacles – or haunt them with new ones
Date: 2021-06-18T10:01:10+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: poltergeist-attack-could-leave-autonomous-vehicles-blind-to-obstacles-or-haunt-them-with-new-ones

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/18/poltergeist_autonomous_vehicles/){:target="_blank" rel="noopener"}

> First 'AMpLe' concept proves worryingly simple to implement with success Researchers at the Ubiquitous System Security Lab of Zhejiang University and the University of Michigan's Security and Privacy Research Group say they've found a way to blind autonomous vehicles to obstacles using simple audio signals.... [...]
