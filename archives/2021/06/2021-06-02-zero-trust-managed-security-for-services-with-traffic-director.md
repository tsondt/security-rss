Title: Zero-trust managed security for services with Traffic Director
Date: 2021-06-02T13:00:00+00:00
Author: Anoosh Saboori
Category: GCP Security
Tags: Identity & Security;Google Cloud Platform;Service Mesh;Networking
Slug: zero-trust-managed-security-for-services-with-traffic-director

[Source](https://cloud.google.com/blog/products/networking/traffic-director-integrates-with-ca-service/){:target="_blank" rel="noopener"}

> We created Traffic Director to bring to you a fully managed service mesh product that includes load balancing, traffic management and service discovery. And now, we’re happy to announce the availability of a fully-managed zero-trust security solution using Traffic Director with Google Kubernetes Engine (GKE) and Certificate Authority (CA) Service. When platform administrators and security professionals think about modernizing their applications with a forward-looking security posture, they look for "zero-trust" security. This security posture is based on few fundamental blocks: A means of allocating and asserting service identity (for example, using X.509 certificates) Mutual authentication (mTLS) or server authentication (TLS) [...]
