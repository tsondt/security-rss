Title: Microsoft Defender ATP now warns of jailbroken iPhones, iPads
Date: 2021-06-15T16:21:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-atp-now-warns-of-jailbroken-iphones-ipads

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-atp-now-warns-of-jailbroken-iphones-ipads/){:target="_blank" rel="noopener"}

> Microsoft has added support for detecting jailbroken iOS devices to Microsoft Defender for Endpoint, the enterprise version of its Windows 10 Defender antivirus. [...]
