Title: Cuffed: Ukraine police collar six Clop ransomware gang suspects in joint raids with South Korean cops
Date: 2021-06-16T13:37:20+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: cuffed-ukraine-police-collar-six-clop-ransomware-gang-suspects-in-joint-raids-with-south-korean-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/16/clop_ransomware_gang_arrests_ukraine/){:target="_blank" rel="noopener"}

> Cobalt Strike and Flawedammyy RAT named as favoured tools Ukrainian police have arrested six people, alleged to be members of the notorious Clop* ransomware gang, seizing cash, cars – and a number of Apple Mac laptops and desktops.... [...]
