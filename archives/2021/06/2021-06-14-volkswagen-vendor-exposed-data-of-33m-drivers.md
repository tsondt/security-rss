Title: Volkswagen Vendor Exposed Data of 3.3m Drivers
Date: 2021-06-14T15:12:50+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Privacy;Web Security
Slug: volkswagen-vendor-exposed-data-of-33m-drivers

[Source](https://threatpost.com/vw-data-3m-audi-drivers/166892/){:target="_blank" rel="noopener"}

> Nearly all of the leaked data was for owners or wannabe owners of the automaker’s luxury brand of Audis, now at greater risk for phishing, ransomware or car theft. [...]
