Title: Novel ‘Victory’ Backdoor Spotted in Chinese APT Campaign
Date: 2021-06-07T18:49:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware
Slug: novel-victory-backdoor-spotted-in-chinese-apt-campaign

[Source](https://threatpost.com/victory-backdoor-apt-campaign/166700/){:target="_blank" rel="noopener"}

> Researchers said the malware has been under development for at least three years. [...]
