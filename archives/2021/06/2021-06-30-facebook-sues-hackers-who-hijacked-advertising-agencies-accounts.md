Title: Facebook sues hackers who hijacked advertising agencies' accounts
Date: 2021-06-30T12:31:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: facebook-sues-hackers-who-hijacked-advertising-agencies-accounts

[Source](https://www.bleepingcomputer.com/news/security/facebook-sues-hackers-who-hijacked-advertising-agencies-accounts/){:target="_blank" rel="noopener"}

> Facebook has filed lawsuits against two groups of suspects who took over advertising agency employees' accounts and abused its ad platform to run unauthorized or deceptive ads. [...]
