Title: DoubleVPN servers, logs, and account info seized by law enforcement
Date: 2021-06-29T12:23:47-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: doublevpn-servers-logs-and-account-info-seized-by-law-enforcement

[Source](https://www.bleepingcomputer.com/news/security/doublevpn-servers-logs-and-account-info-seized-by-law-enforcement/){:target="_blank" rel="noopener"}

> ​Law enforcement has seized the servers and customer logs for DoubleVPN, a double-encryption service commonly used by threat actors to evade detection while performing malicious activities. [...]
