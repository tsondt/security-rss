Title: Intuit notifies customers of hacked TurboTax accounts
Date: 2021-06-12T10:51:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: intuit-notifies-customers-of-hacked-turbotax-accounts

[Source](https://www.bleepingcomputer.com/news/security/intuit-notifies-customers-of-hacked-turbotax-accounts/){:target="_blank" rel="noopener"}

> Financial software company Intuit has notified TurboTax customers that some of their personal and financial information was accessed by attackers following what looks like a series of account takeover attacks. [...]
