Title: US supermarket chain Wegmans suffers data breach due to ‘misconfigured’ databases
Date: 2021-06-21T11:57:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-supermarket-chain-wegmans-suffers-data-breach-due-to-misconfigured-databases

[Source](https://portswigger.net/daily-swig/us-supermarket-chain-wegmans-suffers-data-breach-due-to-misconfigured-databases){:target="_blank" rel="noopener"}

> Personal information belonging to customers was available online since late April [...]
