Title: Email Bug Allows Message Snooping, Credential Theft
Date: 2021-06-22T18:07:54+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: email-bug-allows-message-snooping-credential-theft

[Source](https://threatpost.com/email-bug-message-snooping-credential-theft/167125/){:target="_blank" rel="noopener"}

> A year-old proof-of-concept attack that allows an attacker to bypass TLS email protections to snoop on messages has been patched. [...]
