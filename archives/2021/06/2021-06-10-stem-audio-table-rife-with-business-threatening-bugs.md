Title: STEM Audio Table Rife with Business-Threatening Bugs
Date: 2021-06-10T18:58:31+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Malware;Vulnerabilities;Web Security
Slug: stem-audio-table-rife-with-business-threatening-bugs

[Source](https://threatpost.com/stem-audio-table-business-bugs/166798/){:target="_blank" rel="noopener"}

> The desktop conferencing IoT gadget allows remote attackers to install all kinds of malware and move laterally to other parts of enterprise networks. [...]
