Title: Sure looks like someone's pirating the REvil ransomware, tweaking the binary in a hex editor for their own crimes
Date: 2021-06-23T00:02:31+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: sure-looks-like-someones-pirating-the-revil-ransomware-tweaking-the-binary-in-a-hex-editor-for-their-own-crimes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/23/revil_ransomware_lv/){:target="_blank" rel="noopener"}

> It's a crook-eat-crook world out there It appears someone is pirating the infamous REvil ransomware by tweaking its files for their own purposes.... [...]
