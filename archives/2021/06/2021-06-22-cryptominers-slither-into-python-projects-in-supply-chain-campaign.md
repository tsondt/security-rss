Title: Cryptominers Slither into Python Projects in Supply-Chain Campaign
Date: 2021-06-22T19:27:35+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Malware;Vulnerabilities
Slug: cryptominers-slither-into-python-projects-in-supply-chain-campaign

[Source](https://threatpost.com/cryptominers-python-supply-chain/167135/){:target="_blank" rel="noopener"}

> These code bombs lurk in the PyPI package repository, waiting to be inadvertently baked into software developers' applications. [...]
