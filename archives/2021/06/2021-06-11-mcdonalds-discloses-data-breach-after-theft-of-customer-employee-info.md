Title: McDonald's discloses data breach after theft of customer, employee info
Date: 2021-06-11T12:45:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: mcdonalds-discloses-data-breach-after-theft-of-customer-employee-info

[Source](https://www.bleepingcomputer.com/news/security/mcdonalds-discloses-data-breach-after-theft-of-customer-employee-info/){:target="_blank" rel="noopener"}

> McDonald's, the largest fast-food chain globally, has disclosed a data breach after hackers breached its systems and stole information belonging to customers and employees from the US, South Korea, and Taiwan. [...]
