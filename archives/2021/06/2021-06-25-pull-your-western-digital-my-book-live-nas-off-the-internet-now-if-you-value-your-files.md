Title: Pull your Western Digital My Book Live NAS off the internet now if you value your files
Date: 2021-06-25T15:30:09+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: pull-your-western-digital-my-book-live-nas-off-the-internet-now-if-you-value-your-files

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/25/western_digital_nas_wiped/){:target="_blank" rel="noopener"}

> Storage giant fingers 'critical' bug allowing remote factory resets that wipe contents Western Digital has alerted customers to a critical bug on its My Book Live storage drives, warning them to disconnect the devices from the internet to protect the units from being remotely wiped.... [...]
