Title: Intel Plugs 29 Holes in CPUs, Bluetooth, Security
Date: 2021-06-09T16:17:39+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities
Slug: intel-plugs-29-holes-in-cpus-bluetooth-security

[Source](https://threatpost.com/intel-security-holes-cpus-bluetooth-security/166747/){:target="_blank" rel="noopener"}

> The higher-rated advisories focus on privilege-escalation bugs in CPU firmware: Tough to patch, hard to exploit, tempting to a savvy attacker. [...]
