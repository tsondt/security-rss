Title: CSP bypass: How one Chrome XSS bug took 2.5 years and an HTML spec change to fix
Date: 2021-06-21T12:55:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: csp-bypass-how-one-chrome-xss-bug-took-25-years-and-an-html-spec-change-to-fix

[Source](https://portswigger.net/daily-swig/csp-bypass-how-one-chrome-xss-bug-took-2-5-years-and-an-html-spec-change-to-fix){:target="_blank" rel="noopener"}

> Browser flaw enabled XSS attacks on protected pages [...]
