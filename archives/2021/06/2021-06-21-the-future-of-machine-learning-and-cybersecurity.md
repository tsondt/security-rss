Title: The Future of Machine Learning and Cybersecurity
Date: 2021-06-21T11:31:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;machine learning;reports
Slug: the-future-of-machine-learning-and-cybersecurity

[Source](https://www.schneier.com/blog/archives/2021/06/the-future-of-machine-learning-and-cybersecurity.html){:target="_blank" rel="noopener"}

> The Center for Security and Emerging Technology has a new report: “ Machine Learning and Cybersecurity: Hype and Reality.” Here’s the bottom line: The report offers four conclusions: Machine learning can help defenders more accurately detect and triage potential attacks. However, in many cases these technologies are elaborations on long-standing methods — not fundamentally new approaches — that bring new attack surfaces of their own. A wide range of specific tasks could be fully or partially automated with the use of machine learning, including some forms of vulnerability discovery, deception, and attack disruption. But many of the most transformative of [...]
