Title: Microsoft faces up to an old foe with out-of-band patch for PDF weirdness
Date: 2021-06-30T17:30:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: microsoft-faces-up-to-an-old-foe-with-out-of-band-patch-for-pdf-weirdness

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/30/microsoft_internet_explorer_pdf_patch/){:target="_blank" rel="noopener"}

> Internet Explorer 11 and the Adobe Reader plug-in? Is this bork bingo? Internet Explorer 11 may only have a year left, but Microsoft still found itself releasing a patch to resolve some PDF issues in the elderly browser.... [...]
