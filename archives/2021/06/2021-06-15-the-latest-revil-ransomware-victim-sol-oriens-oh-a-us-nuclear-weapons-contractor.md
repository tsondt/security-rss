Title: The latest REvil ransomware victim? Sol Oriens. Oh, a US nuclear weapons contractor
Date: 2021-06-15T11:28:13+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: the-latest-revil-ransomware-victim-sol-oriens-oh-a-us-nuclear-weapons-contractor

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/15/us_nuclear_weapons_contractor_sol_oriens/){:target="_blank" rel="noopener"}

> Company claims 'no current indication' top-secret data was plundered The REvil ransomware gang, thought to be behind an attack on meat producer JBS which netted an impressive $11m payoff, has found another victim. Worryingly, this one works with the US Department of Defence on the nation's nuclear weapons programme.... [...]
