Title: Cobalt Strike Usage Explodes Among Cybercrooks
Date: 2021-06-29T09:00:51+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: cobalt-strike-usage-explodes-among-cybercrooks

[Source](https://threatpost.com/cobalt-strike-cybercrooks/167368/){:target="_blank" rel="noopener"}

> The legit security tool has shown up 161 percent more, year-over-year, in cyberattacks, having “gone fully mainstream in the crimeware world.” [...]
