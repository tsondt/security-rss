Title: G7 nations call out Russia for harbouring ransomware crims ahead of Biden-Putin powwow
Date: 2021-06-14T21:29:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: g7-nations-call-out-russia-for-harbouring-ransomware-crims-ahead-of-biden-putin-powwow

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/14/g7_ransomware_communique_calls_out_russia/){:target="_blank" rel="noopener"}

> Hopes raised in West of an extradition or law enforcement agreement to stem the tide The G7 summit of western countries has called upon Russia to "identify, disrupt, and hold to account those within its borders who conduct ransomware attacks, abuse virtual currency to launder ransoms, and other cybercrimes."... [...]
