Title: Ex-NSA leaker Reality Winner released from prison early for 'exemplary' behavior
Date: 2021-06-14T20:36:21+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: ex-nsa-leaker-reality-winner-released-from-prison-early-for-exemplary-behavior

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/14/reality_winner_released/){:target="_blank" rel="noopener"}

> Will be transferred to a halfway house, attorney continues to fight for presidential pardon Reality Winner, the former NSA intelligence contractor who leaked evidence of Russian interference in a US Presidential election to the press, has been released from prison.... [...]
