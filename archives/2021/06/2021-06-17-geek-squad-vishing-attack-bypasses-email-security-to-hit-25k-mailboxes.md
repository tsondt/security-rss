Title: Geek Squad Vishing Attack Bypasses Email Security to Hit 25K Mailboxes
Date: 2021-06-17T17:06:10+00:00
Author: Tara Seals
Category: Threatpost
Tags: Web Security
Slug: geek-squad-vishing-attack-bypasses-email-security-to-hit-25k-mailboxes

[Source](https://threatpost.com/geek-squad-vishing-bypasses-email-security/167014/){:target="_blank" rel="noopener"}

> An email campaign asking victims to call a bogus number to suspend supposedly fraudulent subscriptions got right past Microsoft's native email controls. [...]
