Title: Microsoft finds Netgear router bugs enabling corporate breaches
Date: 2021-06-30T14:14:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-finds-netgear-router-bugs-enabling-corporate-breaches

[Source](https://www.bleepingcomputer.com/news/security/microsoft-finds-netgear-router-bugs-enabling-corporate-breaches/){:target="_blank" rel="noopener"}

> Attackers could use critical firmware vulnerabilities discovered by Microsoft in some NETGEAR router models as a stepping stone to move laterally within enterprise networks. [...]
