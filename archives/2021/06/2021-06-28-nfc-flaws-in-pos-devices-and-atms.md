Title: NFC Flaws in POS Devices and ATMs
Date: 2021-06-28T11:53:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: ATMs;point of sale;vulnerabilities
Slug: nfc-flaws-in-pos-devices-and-atms

[Source](https://www.schneier.com/blog/archives/2021/06/nfc-flaws-in-pos-devices-and-atms.html){:target="_blank" rel="noopener"}

> It’s a series of vulnerabilities : Josep Rodriguez, a researcher and consultant at security firm IOActive, has spent the last year digging up and reporting vulnerabilities in the so-called near-field communications reader chips used in millions of ATMs and point-of-sale systems worldwide. NFC systems are what let you wave a credit card over a reader — rather than swipe or insert it — to make a payment or extract money from a cash machine. You can find them on countless retail store and restaurant counters, vending machines, taxis, and parking meters around the globe. Now Rodriguez has built an Android [...]
