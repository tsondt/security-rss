Title: Where Bug Bounty Programs Fall Flat
Date: 2021-06-01T18:00:42+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Podcasts;RSAC;Vulnerabilities
Slug: where-bug-bounty-programs-fall-flat

[Source](https://threatpost.com/bug-bounty-fall-flat/166568/){:target="_blank" rel="noopener"}

> Some criminals package exploits into bundles to sell on cybercriminal forums years after they were zero days, while others say bounties aren't enough. [...]
