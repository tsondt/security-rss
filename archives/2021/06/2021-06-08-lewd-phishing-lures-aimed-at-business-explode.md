Title: Lewd Phishing Lures Aimed at Business Explode
Date: 2021-06-08T20:45:29+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Malware;Most Recent ThreatLists;Web Security
Slug: lewd-phishing-lures-aimed-at-business-explode

[Source](https://threatpost.com/lewd-phishing-lures-business-explode/166734/){:target="_blank" rel="noopener"}

> Socially engineered BEC attacks using X-rated material spike 974 percent. [...]
