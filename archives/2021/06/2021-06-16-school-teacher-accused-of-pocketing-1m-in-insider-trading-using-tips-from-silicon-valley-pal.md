Title: School teacher accused of pocketing $1m+ in insider trading using tips from Silicon Valley pal
Date: 2021-06-16T20:20:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: school-teacher-accused-of-pocketing-1m-in-insider-trading-using-tips-from-silicon-valley-pal

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/16/teacher_insider_training/){:target="_blank" rel="noopener"}

> One of six guys charged over Infinera, Fortinet securities A teacher who knew too much about some of Silicon Valley's financial figures has been charged with insider dealing by the US Securities and Exchange Commission, along with five alleged accomplices.... [...]
