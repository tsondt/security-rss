Title: FBI and AFP created a fake encrypted chat platform to catch criminals
Date: 2021-06-08T11:08:39-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fbi-and-afp-created-a-fake-encrypted-chat-platform-to-catch-criminals

[Source](https://www.bleepingcomputer.com/news/security/fbi-and-afp-created-a-fake-encrypted-chat-platform-to-catch-criminals/){:target="_blank" rel="noopener"}

> In the largest and most sophisticated law enforcement operations to date, a joint international law enforcement created a fake end-to-end encrypted chat platform designed solely to catch criminals. [...]
