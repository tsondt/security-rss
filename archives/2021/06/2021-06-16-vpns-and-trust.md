Title: VPNs and Trust
Date: 2021-06-16T11:17:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: privacy;risk assessment;surveillance;trust;VPN
Slug: vpns-and-trust

[Source](https://www.schneier.com/blog/archives/2021/06/vpns-and-trust.html){:target="_blank" rel="noopener"}

> TorrentFreak surveyed nineteen VPN providers, asking them questions about their privacy practices: what data they keep, how they respond to court order, what country they are incorporated in, and so on. Most interesting to me is the home countries of these companies. Express VPN is incorporated in the British Virgin Islands. NordVPN is incorporated in Panama. There are VPNs from the Seychelles, Malaysia, and Bulgaria. There are VPNs from more traditional companies like the U.S., Switzerland, Canada, and Sweden. Presumably all of those companies follow the laws on their home country. And it matters. I’ve been thinking about this since [...]
