Title: XSS flaw in Wire messaging app allowed attackers to ‘fully control’ user accounts
Date: 2021-06-18T11:46:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: xss-flaw-in-wire-messaging-app-allowed-attackers-to-fully-control-user-accounts

[Source](https://portswigger.net/daily-swig/xss-flaw-in-wire-messaging-app-allowed-attackers-to-fully-control-user-accounts){:target="_blank" rel="noopener"}

> Security fixes pushed down the wire [...]
