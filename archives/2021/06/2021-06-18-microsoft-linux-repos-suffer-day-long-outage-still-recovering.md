Title: Microsoft Linux repos suffer day-long outage, still recovering
Date: 2021-06-18T04:41:03-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Linux;Microsoft
Slug: microsoft-linux-repos-suffer-day-long-outage-still-recovering

[Source](https://www.bleepingcomputer.com/news/security/microsoft-linux-repos-suffer-day-long-outage-still-recovering/){:target="_blank" rel="noopener"}

> This week, Microsoft's Linux package repositories have been suffering hours-to-day long outages or performance issues. [...]
