Title: IoT security: Researchers discover Stem Audio smart speaker eavesdropping risk
Date: 2021-06-10T11:25:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: iot-security-researchers-discover-stem-audio-smart-speaker-eavesdropping-risk

[Source](https://portswigger.net/daily-swig/iot-security-researchers-discover-stem-audio-smart-speaker-eavesdropping-risk){:target="_blank" rel="noopener"}

> Conference room hardware hack symptomatic of more general malaise, warn security researchers [...]
