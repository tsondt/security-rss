Title: Biden to Putin: Get your ransomware gangs under control and don’t you dare cyber-attack our infrastructure
Date: 2021-06-17T06:00:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: biden-to-putin-get-your-ransomware-gangs-under-control-and-dont-you-dare-cyber-attack-our-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/17/biden_putin_summit_cybersecurity_discussion/){:target="_blank" rel="noopener"}

> Putin to Biden: чушь! You already attack us way more than we attack you! US President Joe Biden and his Russian Federation counterpart Vladimir Putin have traded barbs over cyber-attacks at a summit meeting staged yesterday in Switzerland.... [...]
