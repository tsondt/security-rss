Title: Fertility clinic discloses data breach exposing patient info
Date: 2021-06-20T10:06:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fertility-clinic-discloses-data-breach-exposing-patient-info

[Source](https://www.bleepingcomputer.com/news/security/fertility-clinic-discloses-data-breach-exposing-patient-info/){:target="_blank" rel="noopener"}

> A Georgia-based fertility clinic has disclosed a data breach after files containing sensitive patient information were stolen during a ransomware attack. [...]
