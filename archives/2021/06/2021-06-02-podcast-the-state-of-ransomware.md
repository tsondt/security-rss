Title: Podcast: The State of Ransomware
Date: 2021-06-02T20:33:28+00:00
Author: Threatpost
Category: Threatpost
Tags: Hacks;Malware;Podcasts;Vulnerabilities;Web Security
Slug: podcast-the-state-of-ransomware

[Source](https://threatpost.com/podcast-state-of-ransomware/166624/){:target="_blank" rel="noopener"}

> In this Threatpost podcast, Fortinet’s top researcher sketches out the ransom landscape, with takeaways from the DarkSide attack on Colonial Pipeline. [...]
