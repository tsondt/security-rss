Title: iPhone bug breaks WiFi when you join hotspot with unusual name
Date: 2021-06-19T12:01:40-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Apple
Slug: iphone-bug-breaks-wifi-when-you-join-hotspot-with-unusual-name

[Source](https://www.bleepingcomputer.com/news/security/iphone-bug-breaks-wifi-when-you-join-hotspot-with-unusual-name/){:target="_blank" rel="noopener"}

> A new iPhone bug has come to light that breaks your iPhone's wireless functionality by merely connecting to a certain WiFi hotspot.. Once triggered, the bug would render your iPhone unable to establish a WiFi connection, even if it is rebooted or the WiFi hotspot is renamed. [...]
