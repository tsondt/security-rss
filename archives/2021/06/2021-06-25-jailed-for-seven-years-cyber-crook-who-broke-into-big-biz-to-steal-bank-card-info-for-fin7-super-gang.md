Title: Jailed for seven years: Cyber-crook who broke into Big Biz to steal bank card info for FIN7 super-gang
Date: 2021-06-25T23:41:32+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: jailed-for-seven-years-cyber-crook-who-broke-into-big-biz-to-steal-bank-card-info-for-fin7-super-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/25/ukranian_fin7_pentest/){:target="_blank" rel="noopener"}

> Also ordered to pay back $2.5m on his way to American cooler An expert penetration tester working for the notorious cyber-crime gang FIN7 was sent down for seven years on Friday and told to cough up $2.5m for breaking into corporate computer systems.... [...]
