Title: Ransomware gangs now creating websites to recruit affiliates
Date: 2021-06-28T14:55:53-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: ransomware-gangs-now-creating-websites-to-recruit-affiliates

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-now-creating-websites-to-recruit-affiliates/){:target="_blank" rel="noopener"}

> Ever since two prominent Russian-speaking cybercrime forums banned ransomware-related topics [1, 2], criminal operations have been forced to promote their service through alternative methods. [...]
