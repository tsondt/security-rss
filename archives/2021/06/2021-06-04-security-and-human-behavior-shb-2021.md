Title: Security and Human Behavior (SHB) 2021
Date: 2021-06-04T11:05:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: conferences;fear;privacy;risks;security conferences
Slug: security-and-human-behavior-shb-2021

[Source](https://www.schneier.com/blog/archives/2021/06/security-and-human-behavior-shb-2021.html){:target="_blank" rel="noopener"}

> Today is the second day of the fourteenth Workshop on Security and Human Behavior. The University of Cambridge is the host, but we’re all on Zoom. SHB is a small, annual, invitational workshop of people studying various aspects of the human side of security, organized each year by Alessandro Acquisti, Ross Anderson, and myself. The forty or so attendees include psychologists, economists, computer security researchers, sociologists, political scientists, criminologists, neuroscientists, designers, lawyers, philosophers, anthropologists, business school professors, and a smattering of others. It’s not just an interdisciplinary event; most of the people here are individually interdisciplinary. Our goal is always [...]
