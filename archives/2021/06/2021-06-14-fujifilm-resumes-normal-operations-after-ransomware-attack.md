Title: Fujifilm resumes normal operations after ransomware attack
Date: 2021-06-14T09:02:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fujifilm-resumes-normal-operations-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/fujifilm-resumes-normal-operations-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Japanese multinational conglomerate Fujifilm says that it has resumed normal business and customer operations following a ransomware attack that forced it to shut the entire network on June 4. [...]
