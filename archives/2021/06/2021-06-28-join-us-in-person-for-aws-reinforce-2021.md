Title: Join us in person for AWS re:Inforce 2021
Date: 2021-06-28T20:07:37+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;re:Inforce;re:Inforce 2021;Security Blog
Slug: join-us-in-person-for-aws-reinforce-2021

[Source](https://aws.amazon.com/blogs/security/join-us-in-person-for-aws-reinforce-2021/){:target="_blank" rel="noopener"}

> I’d like to personally invite you to attend our security conference, AWS re:Inforce 2021 in Houston, TX on August 24–25. This event will offer interactive educational content to address your security, compliance, privacy, and identity management needs. As the Chief Information Security Officer of Amazon Web Services (AWS), my primary job is to help our [...]
