Title: US court offers clarity on evaluating ‘future risk’ injuries in data breach class action litigation
Date: 2021-06-03T11:41:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-court-offers-clarity-on-evaluating-future-risk-injuries-in-data-breach-class-action-litigation

[Source](https://portswigger.net/daily-swig/us-court-offers-clarity-on-evaluating-future-risk-injuries-in-data-breach-class-action-litigation){:target="_blank" rel="noopener"}

> Second Circuit opinion may have a sizeable impact on the US legal landscape, writes David Oberly [...]
