Title: Chinese threat actors hacked NYC MTA using Pulse Secure zero-day
Date: 2021-06-03T11:55:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: chinese-threat-actors-hacked-nyc-mta-using-pulse-secure-zero-day

[Source](https://www.bleepingcomputer.com/news/security/chinese-threat-actors-hacked-nyc-mta-using-pulse-secure-zero-day/){:target="_blank" rel="noopener"}

> Chinese-backed threat actors breached New York City's Metropolitan Transportation Authority (MTA) network in April using a Pulse Secure zero-day. Still, they failed to cause any data loss or gain access to systems controlling the transportation fleet. [...]
