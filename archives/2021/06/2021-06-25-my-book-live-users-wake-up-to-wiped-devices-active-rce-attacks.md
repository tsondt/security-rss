Title: My Book Live Users Wake Up to Wiped Devices, Active RCE Attacks
Date: 2021-06-25T15:50:32+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security
Slug: my-book-live-users-wake-up-to-wiped-devices-active-rce-attacks

[Source](https://threatpost.com/my-book-live-wiped-rce-attacks/167270/){:target="_blank" rel="noopener"}

> “I am totally screwed,” one user wailed after finding years of data nuked. Western Digital advised yanking the NAS storage devices offline ASAP: There's an exploit. [...]
