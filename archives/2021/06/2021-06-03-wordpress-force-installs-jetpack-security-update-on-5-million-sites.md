Title: WordPress force installs Jetpack security update on 5 million sites
Date: 2021-06-03T15:00:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: wordpress-force-installs-jetpack-security-update-on-5-million-sites

[Source](https://www.bleepingcomputer.com/news/security/wordpress-force-installs-jetpack-security-update-on-5-million-sites/){:target="_blank" rel="noopener"}

> Automattic, the company behind the WordPress content management system, force deploys a security update on over five million websites running the Jetpack WordPress plug-in. [...]
