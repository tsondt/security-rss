Title: Dell SupportAssist bugs put over 30 million PCs at risk
Date: 2021-06-24T06:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: dell-supportassist-bugs-put-over-30-million-pcs-at-risk

[Source](https://www.bleepingcomputer.com/news/security/dell-supportassist-bugs-put-over-30-million-pcs-at-risk/){:target="_blank" rel="noopener"}

> Security researchers have found four major security vulnerabilities in the BIOSConnect feature of Dell SupportAssist, allowing attackers to remotely execute code within the BIOS of impacted devices. [...]
