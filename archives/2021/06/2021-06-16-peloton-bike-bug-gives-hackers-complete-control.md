Title: Peloton Bike+ Bug Gives Hackers Complete Control
Date: 2021-06-16T11:19:34+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;IoT;Vulnerabilities;Web Security
Slug: peloton-bike-bug-gives-hackers-complete-control

[Source](https://threatpost.com/peloton-bike-bug-hackers-control/166960/){:target="_blank" rel="noopener"}

> An attacker with initial physical access (say, at a gym) could gain root entry to the interactive tablet, making for a bevy of remote attack scenarios. [...]
