Title: Detecting Deepfake Picture Editing
Date: 2021-06-10T11:19:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;deep fake;tamper detection
Slug: detecting-deepfake-picture-editing

[Source](https://www.schneier.com/blog/archives/2021/06/detecting-deepfake-picture-editing.html){:target="_blank" rel="noopener"}

> “Markpainting” is a clever technique to watermark photos in such a way that makes it easier to detect ML-based manipulation: An image owner can modify their image in subtle ways which are not themselves very visible, but will sabotage any attempt to inpaint it by adding visible information determined in advance by the markpainter. One application is tamper-resistant marks. For example, a photo agency that makes stock photos available on its website with copyright watermarks can markpaint them in such a way that anyone using common editing software to remove a watermark will fail; the copyright mark will be markpainted [...]
