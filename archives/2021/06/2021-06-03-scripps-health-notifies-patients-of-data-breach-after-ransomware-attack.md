Title: Scripps Health notifies patients of data breach after ransomware attack
Date: 2021-06-03T13:50:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: scripps-health-notifies-patients-of-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/scripps-health-notifies-patients-of-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> ​Nonprofit healthcare provider, Scripps Health in San Diego, has disclosed a data breach exposing patient information after suffering a ransomware attack last month. [...]
