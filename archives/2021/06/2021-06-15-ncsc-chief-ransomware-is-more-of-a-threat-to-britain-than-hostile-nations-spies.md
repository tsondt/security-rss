Title: NCSC chief: Ransomware is more of a threat to Britain than hostile nations' spies
Date: 2021-06-15T14:53:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ncsc-chief-ransomware-is-more-of-a-threat-to-britain-than-hostile-nations-spies

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/15/ncsc_chief_rusi_speech/){:target="_blank" rel="noopener"}

> Lindy Cameron gives private industry an unusual nod in speech full of interventionism Forget foreign spies. The head of Britain's National Cyber Security Center (NCSC) has warned it is ransomware that's the key threat for most people.... [...]
