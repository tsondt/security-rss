Title: You can hijack Google Cloud VMs using DHCP floods, says this guy, once the stars are aligned and...
Date: 2021-06-30T00:02:21+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: you-can-hijack-google-cloud-vms-using-dhcp-floods-says-this-guy-once-the-stars-are-aligned-and

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/30/gce_vm_vulnerability/){:target="_blank" rel="noopener"}

> An Ocean's 11 of exploitation involving guessable random numbers and hostname shenanigans Google Compute Engine virtual machines can be hijacked and made to hand over root shell access via a cunning DHCP attack, according to security researcher Imre Rad.... [...]
