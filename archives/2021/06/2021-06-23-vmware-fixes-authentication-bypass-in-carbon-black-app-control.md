Title: VMware fixes authentication bypass in Carbon Black App Control
Date: 2021-06-23T13:40:59-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: vmware-fixes-authentication-bypass-in-carbon-black-app-control

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-authentication-bypass-in-carbon-black-app-control/){:target="_blank" rel="noopener"}

> VMware Carbon Black App Control has been updated this week to fix a critical-severity vulnerability that allows access to the server without authentication. [...]
