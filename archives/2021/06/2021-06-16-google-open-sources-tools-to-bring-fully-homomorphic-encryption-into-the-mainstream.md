Title: Google open-sources tools to bring fully homomorphic encryption into the mainstream
Date: 2021-06-16T13:46:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-open-sources-tools-to-bring-fully-homomorphic-encryption-into-the-mainstream

[Source](https://portswigger.net/daily-swig/google-open-sources-tools-to-bring-fully-homomorphic-encryption-into-the-mainstream){:target="_blank" rel="noopener"}

> Cryptographic expertise not needed to enable computations on encrypted data, says tech giant [...]
