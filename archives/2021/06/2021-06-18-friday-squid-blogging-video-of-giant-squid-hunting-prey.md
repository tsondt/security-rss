Title: Friday Squid Blogging: Video of Giant Squid Hunting Prey
Date: 2021-06-18T21:06:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-video-of-giant-squid-hunting-prey

[Source](https://www.schneier.com/blog/archives/2021/06/friday-squid-blogging-video-of-giant-squid-hunting-prey.html){:target="_blank" rel="noopener"}

> Fantastic video of a giant squid hunting at depths between 1,827 and 3,117 feet. This is a follow-on from this post. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
