Title: Brave launches its privacy-focused no-tracking search engine
Date: 2021-06-22T15:14:08-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software;Technology
Slug: brave-launches-its-privacy-focused-no-tracking-search-engine

[Source](https://www.bleepingcomputer.com/news/software/brave-launches-its-privacy-focused-no-tracking-search-engine/){:target="_blank" rel="noopener"}

> Today, Brave launched their non-tracking privacy-centric search engine to bring another alternative to finding the information you want on the web without giving up your data. [...]
