Title: Chrome Browser Bug Under Active Attack
Date: 2021-06-10T20:07:53+00:00
Author: Tom Spring
Category: Threatpost
Tags: Hacks;Mobile Security;Vulnerabilities;Web Security
Slug: chrome-browser-bug-under-active-attack

[Source](https://threatpost.com/chrome-browser-bug-under-attack/166804/){:target="_blank" rel="noopener"}

> Google has patched its Chrome browser, fixing one critical cache issue and a second bug being actively exploited in the wild. [...]
