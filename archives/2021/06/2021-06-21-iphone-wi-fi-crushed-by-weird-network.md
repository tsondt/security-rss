Title: iPhone Wi-Fi Crushed by Weird Network
Date: 2021-06-21T16:58:52+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Mobile Security;Vulnerabilities
Slug: iphone-wi-fi-crushed-by-weird-network

[Source](https://threatpost.com/iphone-wi-fi-weird-network/167075/){:target="_blank" rel="noopener"}

> ... until you reset network settings and stop connecting to a weirdly named network, that is. FUD is spreading. iOS Wi-Fi demolition is not. [...]
