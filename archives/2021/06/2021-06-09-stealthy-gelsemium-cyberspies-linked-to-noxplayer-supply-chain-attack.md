Title: Stealthy Gelsemium cyberspies linked to NoxPlayer supply-chain attack
Date: 2021-06-09T09:56:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: stealthy-gelsemium-cyberspies-linked-to-noxplayer-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/stealthy-gelsemium-cyberspies-linked-to-noxplayer-supply-chain-attack/){:target="_blank" rel="noopener"}

> ESET researchers have linked a stealthy cyberespionage group known as Gelsemium to the NoxPlayer Android emulator supply-chain attack that targeted gamers earlier this year. [...]
