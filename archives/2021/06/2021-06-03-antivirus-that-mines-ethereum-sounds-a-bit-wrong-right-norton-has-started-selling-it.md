Title: Antivirus that mines Ethereum sounds a bit wrong, right? Norton has started selling it
Date: 2021-06-03T06:51:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: antivirus-that-mines-ethereum-sounds-a-bit-wrong-right-norton-has-started-selling-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/03/norton_crypto/){:target="_blank" rel="noopener"}

> Down continues to be the new up NortonLifeLock, the company that offers the consumer products Broadcom didn’t want when it bought Symantec, has started to offer Ethereum mining as a feature of its Norton 360 security suite.... [...]
