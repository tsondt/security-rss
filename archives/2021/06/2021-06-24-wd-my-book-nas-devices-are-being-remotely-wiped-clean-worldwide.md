Title: WD My Book NAS devices are being remotely wiped clean worldwide
Date: 2021-06-24T16:00:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: wd-my-book-nas-devices-are-being-remotely-wiped-clean-worldwide

[Source](https://www.bleepingcomputer.com/news/security/wd-my-book-nas-devices-are-being-remotely-wiped-clean-worldwide/){:target="_blank" rel="noopener"}

> Western Digital My Book NAS owners worldwide are finding that their devices have been mysteriously factory reset and all of their files deleted. [...]
