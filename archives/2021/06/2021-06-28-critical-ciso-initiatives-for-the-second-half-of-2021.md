Title: Critical CISO Initiatives for the Second Half of 2021
Date: 2021-06-28T16:00:11+00:00
Author: Saryu Nayyar
Category: Threatpost
Tags: InfoSec Insider
Slug: critical-ciso-initiatives-for-the-second-half-of-2021

[Source](https://threatpost.com/critical-ciso-initiatives-2021/167309/){:target="_blank" rel="noopener"}

> Saryu Nayyar, CEO at Gurucul, goes over what defenses CISOs need now, and how and why to prioritize the options. [...]
