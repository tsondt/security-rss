Title: Police Grab Slilpp, Biggest Stolen-Logins Market
Date: 2021-06-11T12:42:46+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Web Security
Slug: police-grab-slilpp-biggest-stolen-logins-market

[Source](https://threatpost.com/police-slilpp-market-stolen-logins/166812/){:target="_blank" rel="noopener"}

> There were more than 80 million login credentials for sale, used to inflict over $200 million in losses in the U.S. alone. [...]
