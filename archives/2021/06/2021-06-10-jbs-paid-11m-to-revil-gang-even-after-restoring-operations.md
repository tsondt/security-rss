Title: JBS Paid $11M to REvil Gang Even After Restoring Operations
Date: 2021-06-10T13:14:39+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: jbs-paid-11m-to-revil-gang-even-after-restoring-operations

[Source](https://threatpost.com/jbs-paid-11m/166767/){:target="_blank" rel="noopener"}

> The decision to pay the ransom demanded by the cybercriminal group was to avoid any further issues or potential problems for its customers, according to the company’s CEO. [...]
