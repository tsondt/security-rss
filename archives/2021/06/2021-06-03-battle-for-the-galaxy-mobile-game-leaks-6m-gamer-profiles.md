Title: ‘Battle for the Galaxy’ Mobile Game Leaks 6M Gamer Profiles
Date: 2021-06-03T22:06:39+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Vulnerabilities
Slug: battle-for-the-galaxy-mobile-game-leaks-6m-gamer-profiles

[Source](https://threatpost.com/battle-for-the-galaxy-leaks/166659/){:target="_blank" rel="noopener"}

> Unprotected server exposes AMT Games user data containing user emails and purchase information. [...]
