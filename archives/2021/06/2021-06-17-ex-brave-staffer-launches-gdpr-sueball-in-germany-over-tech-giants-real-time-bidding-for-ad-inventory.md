Title: Ex-Brave staffer launches GDPR sueball in Germany over tech giants' real-time bidding for ad inventory
Date: 2021-06-17T20:16:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ex-brave-staffer-launches-gdpr-sueball-in-germany-over-tech-giants-real-time-bidding-for-ad-inventory

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/17/johnny_ryan_lawsuit_adtech/){:target="_blank" rel="noopener"}

> Privacy browser's former chief policy officer calls web advertising ecosystem 'the Biggest. Data. Breach. Ever' Former Brave chief policy officer Johnny Ryan is continuing his crusade against the online advertising industry by filing a lawsuit against Google, Facebook, Amazon, Twitter, and US telco AT&T in Germany.... [...]
