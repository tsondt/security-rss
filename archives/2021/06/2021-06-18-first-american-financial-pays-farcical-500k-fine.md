Title: First American Financial Pays Farcical $500K Fine
Date: 2021-06-18T12:20:21+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;accepted the risk;American Land Title Association;First American Financial Corp.;mark rasch;Michael Volkov;New York State Department of Financial Services;simpleshowing.com;The Volkov Law Group;U.S. Securities and Exchange Commission
Slug: first-american-financial-pays-farcical-500k-fine

[Source](https://krebsonsecurity.com/2021/06/first-american-financial-pays-farcical-500k-fine/){:target="_blank" rel="noopener"}

> In May 2019, KrebsOnSecurity broke the news that the website of mortgage settlement giant First American Financial Corp. [ NYSE:FAF ] was leaking more than 800 million documents — many containing sensitive financial data — related to real estate transactions dating back 16 years. This week, the U.S. Securities and Exchange Commission settled its investigation into the matter after the Fortune 500 company agreed to pay a paltry penalty of less than $500,000. First American Financial Corp. If you bought or sold a property in the last two decades or so, chances are decent that you also gave loads of [...]
