Title: We've found another reason not to use Microsoft's Paint 3D – researchers
Date: 2021-06-16T15:07:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: weve-found-another-reason-not-to-use-microsofts-paint-3d-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/16/3d_paint_vuln/){:target="_blank" rel="noopener"}

> Scream if you wanna go raster: Vulnerability uncovered in unloved software As Microsoft preps the next version of Windows, a hole has been spotted in an earlier Great Hope for the company: MS Paint 3D.... [...]
