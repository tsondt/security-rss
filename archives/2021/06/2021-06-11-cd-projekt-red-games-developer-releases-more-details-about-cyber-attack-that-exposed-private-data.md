Title: CD Projekt Red: Games developer releases more details about cyber-attack that exposed private data
Date: 2021-06-11T14:17:25+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cd-projekt-red-games-developer-releases-more-details-about-cyber-attack-that-exposed-private-data

[Source](https://portswigger.net/daily-swig/cd-projekt-red-games-developer-releases-more-details-about-cyber-attack-that-exposed-private-data){:target="_blank" rel="noopener"}

> Polish gaming company reported security incident to multiple law enforcement agencies [...]
