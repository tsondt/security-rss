Title: GPRS-era mobile data encryption algorithm GEA/1 was 'weak by design', still lingers in today's phones
Date: 2021-06-17T01:44:20+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: gprs-era-mobile-data-encryption-algorithm-gea1-was-weak-by-design-still-lingers-in-todays-phones

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/17/gprs_encryption_backdoor/){:target="_blank" rel="noopener"}

> Just in case you travel back in time to 1998 The GEA/1 encryption algorithm used by GPRS phones in the 1990s was seemingly designed to be weaker than it appears to allow eavesdropping, according to European researchers.... [...]
