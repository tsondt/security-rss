Title: Eggfree Cake Box suffer data breach exposing credit card numbers
Date: 2021-06-17T17:47:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: eggfree-cake-box-suffer-data-breach-exposing-credit-card-numbers

[Source](https://www.bleepingcomputer.com/news/security/eggfree-cake-box-suffer-data-breach-exposing-credit-card-numbers/){:target="_blank" rel="noopener"}

> Eggfree Cake Box has disclosed a data breach after threat actors hacked their website to stole credit card numbers. [...]
