Title: FIN7 ‘Pen Tester’ Headed to Jail Amid $1B in Payment-Card Losses
Date: 2021-06-25T18:06:39+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: fin7-pen-tester-headed-to-jail-amid-1b-in-payment-card-losses

[Source](https://threatpost.com/fin7-pen-tester-jail/167293/){:target="_blank" rel="noopener"}

> One of the Carbanak cybergang's highest-level hackers is destined to serve seven years while making $2.5 million in restitution payments. [...]
