Title: Google announces new experimental Abuse Research Grants Program
Date: 2021-06-04T12:41:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: google-announces-new-experimental-abuse-research-grants-program

[Source](https://www.bleepingcomputer.com/news/security/google-announces-new-experimental-abuse-research-grants-program/){:target="_blank" rel="noopener"}

> Google has announced a new experimental Abuse Research Grants Program for abuse-related issues and tactics outside the scope of existing Vulnerability Research Grants and the Vulnerability Reward Program (VRP). [...]
