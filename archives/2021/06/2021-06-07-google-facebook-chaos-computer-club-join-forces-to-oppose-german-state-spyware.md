Title: Google, Facebook, Chaos Computer Club join forces to oppose German state spyware
Date: 2021-06-07T22:49:41+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: google-facebook-chaos-computer-club-join-forces-to-oppose-german-state-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/07/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: FBI boss says ransomware is terrorism 2.0, and more In brief Plans by the German government to allow the police to deploy malware on any target's devices, and force the tech world to help them, has run into some opposition, funnily enough.... [...]
