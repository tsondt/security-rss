Title: Fake DarkSide gang targets energy, food industry in extortion emails
Date: 2021-06-18T12:48:23-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fake-darkside-gang-targets-energy-food-industry-in-extortion-emails

[Source](https://www.bleepingcomputer.com/news/security/fake-darkside-gang-targets-energy-food-industry-in-extortion-emails/){:target="_blank" rel="noopener"}

> Threat actors impersonate the now-defunct DarkSide Ransomware operation in fake extortion emails sent to companies in the energy and food sectors. [...]
