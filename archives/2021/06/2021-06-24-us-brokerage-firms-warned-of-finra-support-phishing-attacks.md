Title: US brokerage firms warned of 'FINRA Support' phishing attacks
Date: 2021-06-24T10:12:12-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: us-brokerage-firms-warned-of-finra-support-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-brokerage-firms-warned-of-finra-support-phishing-attacks/){:target="_blank" rel="noopener"}

> US securities industry regulator FINRA is warning brokerage firms of an ongoing phishing attack pretending to be from 'FINRA Support.' [...]
