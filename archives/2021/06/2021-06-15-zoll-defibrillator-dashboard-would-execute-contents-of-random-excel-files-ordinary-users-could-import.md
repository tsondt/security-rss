Title: Zoll Defibrillator Dashboard would execute contents of random Excel files ordinary users could import
Date: 2021-06-15T18:16:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: zoll-defibrillator-dashboard-would-execute-contents-of-random-excel-files-ordinary-users-could-import

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/15/zoll_defibrillator_dashboard_vulnerabilities/){:target="_blank" rel="noopener"}

> Medical device cybersecurity raises its head in CISA warning A defibrillator management platform was riddled with vulnerabilities including a remote command execution flaw that could seemingly be invoked by uploading an Excel spreadsheet to the platform.... [...]
