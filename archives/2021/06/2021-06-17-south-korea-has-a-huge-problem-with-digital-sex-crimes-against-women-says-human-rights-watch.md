Title: South Korea has a huge problem with digital sex crimes against women says Human Rights Watch
Date: 2021-06-17T04:04:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: south-korea-has-a-huge-problem-with-digital-sex-crimes-against-women-says-human-rights-watch

[Source](https://go.theregister.com/feed/www.theregister.com/2021/06/17/human_rights_watch_south_korea_digital_crime_against_women_report/){:target="_blank" rel="noopener"}

> Big tech and local authorities are both far from helpful when victims try to delete unauthorised images or prosecute creeps International non-governmental organisation Human Rights Watch (HRW) released a report Wednesday describing digital sex crime in South Korea as rampant and pervasive, with the nation leading the world in use of spycams to capture women in vulnerable moments. The author calls on governments and companies to do more.... [...]
