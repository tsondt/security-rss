Title: Leaked Babuk Locker ransomware builder used in new attacks
Date: 2021-06-30T19:01:14-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: leaked-babuk-locker-ransomware-builder-used-in-new-attacks

[Source](https://www.bleepingcomputer.com/news/security/leaked-babuk-locker-ransomware-builder-used-in-new-attacks/){:target="_blank" rel="noopener"}

> A leaked tool used by the Babuk Locker operation to create custom ransomware executables is now being used by another threat actor in a very active campaign targeting victims worldwide. [...]
