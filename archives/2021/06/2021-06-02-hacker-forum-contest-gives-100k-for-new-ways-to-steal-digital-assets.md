Title: Hacker forum contest gives $100K for new ways to steal digital assets
Date: 2021-06-02T12:53:37-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hacker-forum-contest-gives-100k-for-new-ways-to-steal-digital-assets

[Source](https://www.bleepingcomputer.com/news/security/hacker-forum-contest-gives-100k-for-new-ways-to-steal-digital-assets/){:target="_blank" rel="noopener"}

> The administrator of a Russian-speaking cybercriminal forum has held a contest for the community to share uncommon methods to target cryptocurrency-related technology. [...]
