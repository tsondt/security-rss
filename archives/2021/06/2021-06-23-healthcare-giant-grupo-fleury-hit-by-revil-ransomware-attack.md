Title: Healthcare giant Grupo Fleury hit by REvil ransomware attack
Date: 2021-06-23T16:00:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: healthcare-giant-grupo-fleury-hit-by-revil-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/healthcare-giant-grupo-fleury-hit-by-revil-ransomware-attack/){:target="_blank" rel="noopener"}

> Brazilian medical diagnostic company Grupo Fleury has suffered a ransomware attack that has disrupted business operations after the company took its systems offline. [...]
