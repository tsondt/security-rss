Title: Win10 Admin Rights Tossed Off by Yet Another Plug-In
Date: 2021-08-25T18:23:58+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security
Slug: 2021-08-25-win10-admin-rights-tossed-off-by-yet-another-plug-in

[Source](https://threatpost.com/windows-10-admin-rights-steelseries-devices/168927/){:target="_blank" rel="noopener"}

> Then again, you don’t even need the actual device – in this case, a SteelSeries peripheral – since emulation works just fine to launch with full SYSTEM rights. [...]
