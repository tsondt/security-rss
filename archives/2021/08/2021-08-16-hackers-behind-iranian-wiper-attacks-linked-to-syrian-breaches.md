Title: Hackers behind Iranian wiper attacks linked to Syrian breaches
Date: 2021-08-16T09:06:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-16-hackers-behind-iranian-wiper-attacks-linked-to-syrian-breaches

[Source](https://www.bleepingcomputer.com/news/security/hackers-behind-iranian-wiper-attacks-linked-to-syrian-breaches/){:target="_blank" rel="noopener"}

> Destructive attacks that targeted Iran's transport ministry and national train system were coordinated by a threat actor dubbed Indra who previously deployed wiper malware on the networks of multiple Syrian organizations. [...]
