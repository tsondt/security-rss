Title: Research: Hundreds of high-traffic web domains vulnerable to same-site attacks
Date: 2021-08-12T15:10:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-12-research-hundreds-of-high-traffic-web-domains-vulnerable-to-same-site-attacks

[Source](https://portswigger.net/daily-swig/research-hundreds-of-high-traffic-web-domains-vulnerable-to-same-site-attacks){:target="_blank" rel="noopener"}

> ‘The same-site security boundary is becoming more and more relevant’ [...]
