Title: 100m T-Mobile Customer Records Purportedly Up for Sale
Date: 2021-08-16T15:12:24+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Mobile Security;Web Security
Slug: 2021-08-16-100m-t-mobile-customer-records-purportedly-up-for-sale

[Source](https://threatpost.com/t-mobile-investigates-100m-records/168689/){:target="_blank" rel="noopener"}

> The seller claims to have sucker-punched U.S. infrastructure out of retaliation. The offer: 30m records for ~1 penny each, with the rest being sold privately. [...]
