Title: Security researcher finds dangerous bug in Chromium, nabs $15,000 bounty
Date: 2021-08-03T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-researcher-finds-dangerous-bug-in-chromium-nabs-15000-bounty

[Source](https://portswigger.net/daily-swig/security-researcher-finds-dangerous-bug-in-chromium-nabs-15-000-bounty){:target="_blank" rel="noopener"}

> Site isolation security break uncovered [...]
