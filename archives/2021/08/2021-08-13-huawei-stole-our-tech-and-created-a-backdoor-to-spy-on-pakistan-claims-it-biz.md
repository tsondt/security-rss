Title: Huawei stole our tech and created a 'backdoor' to spy on Pakistan, claims IT biz
Date: 2021-08-13T01:54:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-13-huawei-stole-our-tech-and-created-a-backdoor-to-spy-on-pakistan-claims-it-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/13/huawei_accused_of_trade_secret/){:target="_blank" rel="noopener"}

> Allegations of purloined trade secrets, unfair competition, national security threats, and more packed into lawsuit A California-based IT consultancy has sued Huawei and its subsidiary in Pakistan alleging the Chinese telecom firm stole its trade secrets and failed to honor a contract to develop technology for Pakistani authorities.... [...]
