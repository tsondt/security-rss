Title: China orders annual security reviews for all critical information infrastructure operators
Date: 2021-08-18T07:58:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-08-18-china-orders-annual-security-reviews-for-all-critical-information-infrastructure-operators

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/18/china_critical_information_infrastructure_rules/){:target="_blank" rel="noopener"}

> Almost any org that could expose data needs a dedicated security team with an obligation to report breaches China's government has introduced rules for protection of critical information infrastructure.... [...]
