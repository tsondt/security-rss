Title: ProxyLogon flaw, evil emails, SQL injections used to open backdoors on Windows boxes
Date: 2021-08-25T19:50:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-25-proxylogon-flaw-evil-emails-sql-injections-used-to-open-backdoors-on-windows-boxes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/25/backdoor_security_asia/){:target="_blank" rel="noopener"}

> Multi-use toolkit deployed on victims' networks across Asia, North America ESET and TrendMicro have identified a novel and sophisticated backdoor tool that miscreants have slipped onto compromised Windows computers in companies mostly in Asia but also in North America.... [...]
