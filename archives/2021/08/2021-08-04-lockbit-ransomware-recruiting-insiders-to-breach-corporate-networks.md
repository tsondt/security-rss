Title: LockBit ransomware recruiting insiders to breach corporate networks
Date: 2021-08-04T12:19:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: lockbit-ransomware-recruiting-insiders-to-breach-corporate-networks

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-recruiting-insiders-to-breach-corporate-networks/){:target="_blank" rel="noopener"}

> The LockBit 2.0 ransomware gang is actively recruiting corporate insiders to help them breach and encrypt networks. In return, the insider is promised million-dollar payouts. [...]
