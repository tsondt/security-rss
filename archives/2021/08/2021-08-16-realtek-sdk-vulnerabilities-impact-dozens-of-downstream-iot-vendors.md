Title: Realtek SDK vulnerabilities impact dozens of downstream IoT vendors
Date: 2021-08-16T14:24:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-16-realtek-sdk-vulnerabilities-impact-dozens-of-downstream-iot-vendors

[Source](https://portswigger.net/daily-swig/realtek-sdk-vulnerabilities-impact-dozens-of-downstream-iot-vendors){:target="_blank" rel="noopener"}

> Wireless tech software flaws allowed attackers to achieve root access on vulnerable devices [...]
