Title: Breach at Deep South allergy clinic group exposed the health info of estimated 9,800 patients
Date: 2021-08-26T13:50:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-26-breach-at-deep-south-allergy-clinic-group-exposed-the-health-info-of-estimated-9800-patients

[Source](https://portswigger.net/daily-swig/breach-at-deep-south-allergy-clinic-group-exposed-the-health-info-of-estimated-9-800-patients){:target="_blank" rel="noopener"}

> Data leak might be linked to ransomware gang's data dump [...]
