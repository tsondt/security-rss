Title: UK to overhaul privacy rules in post-Brexit departure from GDPR
Date: 2021-08-26T09:19:37+00:00
Author: Alex Hern
Category: The Guardian
Tags: GDPR;Data protection;Data and computer security;Technology;Brexit;Politics;UK news
Slug: 2021-08-26-uk-to-overhaul-privacy-rules-in-post-brexit-departure-from-gdpr

[Source](https://www.theguardian.com/technology/2021/aug/26/uk-to-overhaul-privacy-rules-in-post-brexit-departure-from-gdpr){:target="_blank" rel="noopener"}

> Culture secretary says move could lead to an end to irritating cookie popups and consent requests online Britain will attempt to move away from European data protection regulations as it overhauls its privacy rules after Brexit, the government has announced. The freedom to chart its own course could lead to an end to irritating cookie popups and consent requests online, said the culture secretary, Oliver Dowden, as he called for rules based on “common sense, not box-ticking”. Continue reading... [...]
