Title: Computer hardware giant GIGABYTE hit by RansomEXX ransomware
Date: 2021-08-06T12:09:58-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: computer-hardware-giant-gigabyte-hit-by-ransomexx-ransomware

[Source](https://www.bleepingcomputer.com/news/security/computer-hardware-giant-gigabyte-hit-by-ransomexx-ransomware/){:target="_blank" rel="noopener"}

> ​Taiwanese motherboard maker Gigabyte has suffered a RansomEXX ransomware attack where threat actors threaten to release 112 GB of data if a ransom is not paid. [...]
