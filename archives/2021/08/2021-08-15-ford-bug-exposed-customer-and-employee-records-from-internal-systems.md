Title: Ford bug exposed customer and employee records from internal systems
Date: 2021-08-15T09:15:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-08-15-ford-bug-exposed-customer-and-employee-records-from-internal-systems

[Source](https://www.bleepingcomputer.com/news/security/ford-bug-exposed-customer-and-employee-records-from-internal-systems/){:target="_blank" rel="noopener"}

> A bug on Ford's website allowed for accessing sensitive systems and obtaining proprietary data, such as customer databases, employee records, internal tickets, etc. The data exposure stemmed from a misconfigured instance of Pega customer engagement system running on Ford's servers. [...]
