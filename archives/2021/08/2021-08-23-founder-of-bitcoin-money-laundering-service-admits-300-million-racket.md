Title: Founder of bitcoin money laundering service admits $300 million racket
Date: 2021-08-23T16:16:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-23-founder-of-bitcoin-money-laundering-service-admits-300-million-racket

[Source](https://portswigger.net/daily-swig/founder-of-bitcoin-money-laundering-service-admits-300-million-racket){:target="_blank" rel="noopener"}

> Helix unwound as bitcoin tumbler kingpin faces long prison sentence [...]
