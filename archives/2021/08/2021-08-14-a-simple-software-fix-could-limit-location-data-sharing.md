Title: A simple software fix could limit location data sharing
Date: 2021-08-14T10:30:27+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;AT&T;celluar;pretty good phone privacy;privacy;smartphones;t-mobile;verizon
Slug: 2021-08-14-a-simple-software-fix-could-limit-location-data-sharing

[Source](https://arstechnica.com/?p=1787083){:target="_blank" rel="noopener"}

> Enlarge / Pretty Good Phone Privacy wants to minimize how much your wireless provider knows about your location. (credit: Noam Galai | Getty Images) Location data sharing from wireless carriers has been a major privacy issue in recent years. Marketers, salespeople, and even bounty hunters were able to pay shadowy third-party companies to track where people have been, using information that carriers gathered from interactions between your phone and nearby cell towers. Even after promising to stop selling the data, the major carriers—AT&T, T-Mobile, and Verizon—reportedly continued the practice in the US until the Federal Communications Commission proposed nearly $200 [...]
