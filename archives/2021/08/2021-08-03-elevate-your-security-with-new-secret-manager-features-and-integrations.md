Title: Elevate your security with new Secret Manager features and integrations
Date: 2021-08-03T16:00:00+00:00
Author: Phillip Tischler
Category: GCP Security
Tags: Identity & Security
Slug: elevate-your-security-with-new-secret-manager-features-and-integrations

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-secret-manager-adds-free-of-charge-tier-and-more/){:target="_blank" rel="noopener"}

> Secret Manager is a Google Cloud service that provides a secure and convenient way to store API keys, passwords, certificates, and other sensitive data. It is the central place and single source of truth to manage, access, and audit secrets across Google Cloud. Since its launch, Secret Manager has helped secure millions of workloads and continues to provide industry-first features like replication policies and support for VPC perimeters. This blog post explores new Secret Manager capabilities and integrations that will help keep your secrets safer. New tier, free of charge No, you're not dreaming - Secret Manager now has a [...]
