Title: Colonial Pipeline reports data breach after May ransomware attack
Date: 2021-08-16T07:23:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-16-colonial-pipeline-reports-data-breach-after-may-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/colonial-pipeline-reports-data-breach-after-may-ransomware-attack/){:target="_blank" rel="noopener"}

> Colonial Pipeline, the largest fuel pipeline in the United States, is sending notification letters to individuals affected by the data breach resulting from the DarkSide ransomware attack that hit its network in May. [...]
