Title: Apple is about to start scanning iPhone users' devices for banned content, warns professor
Date: 2021-08-05T22:00:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: apple-is-about-to-start-scanning-iphone-users-devices-for-banned-content-warns-professor

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/05/apple_csam_scanning/){:target="_blank" rel="noopener"}

> For now it's child abuse material but the tech has mission creep written in Apple is about to announce a new technology for scanning individual users' iPhones for banned content. While it will be billed as a tool for detecting child abuse imagery, its potential for misuse is vast based on details entering the public domain.... [...]
