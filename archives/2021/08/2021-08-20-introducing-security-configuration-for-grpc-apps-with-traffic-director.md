Title: Introducing security configuration for gRPC apps with Traffic Director
Date: 2021-08-20T18:00:00+00:00
Author: Sanjay Pujare
Category: GCP Security
Tags: Identity & Security;Networking;Application Development
Slug: 2021-08-20-introducing-security-configuration-for-grpc-apps-with-traffic-director

[Source](https://cloud.google.com/blog/products/application-development/add-security-to-grpc-services-with-traffic-director/){:target="_blank" rel="noopener"}

> Developers use the gRPC RPC framework for use cases like backend service-to-service communications or client-server communications between web, mobile and cloud. In July 2020, we announced support for proxyless gRPC services to reduce operational complexity and improve performance of service meshes with Traffic Director, our managed control plane for application networking. Then, in August we added support for advanced traffic management features such as route matching and traffic splitting. And now, gRPC services can be configured via the Traffic Director control plane to use TLS and mutual TLS to establish secure communication with one another. In this blog post we [...]
