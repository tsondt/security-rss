Title: Four-fold increase in software supply chain attacks predicted in 2021 – report
Date: 2021-08-03T15:10:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: four-fold-increase-in-software-supply-chain-attacks-predicted-in-2021-report

[Source](https://portswigger.net/daily-swig/four-fold-increase-in-software-supply-chain-attacks-predicted-in-2021-report){:target="_blank" rel="noopener"}

> EU study warns of growing trend where one attack can leave countless organizations vulnerable [...]
