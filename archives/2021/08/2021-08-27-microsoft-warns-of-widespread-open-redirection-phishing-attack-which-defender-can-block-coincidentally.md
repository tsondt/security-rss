Title: Microsoft warns of widespread open redirection phishing attack – which Defender can block, coincidentally
Date: 2021-08-27T21:59:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-27-microsoft-warns-of-widespread-open-redirection-phishing-attack-which-defender-can-block-coincidentally

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/27/microsoft_phishing_defender/){:target="_blank" rel="noopener"}

> Some tactics never change much Microsoft has warned that it has been tracking a widespread credential-phishing campaign that relies on open redirector links, while simultaneously suggesting it can defend against such schemes.... [...]
