Title: Unpatched Fortinet Bug Allows Firewall Takeovers
Date: 2021-08-18T12:07:33+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-08-18-unpatched-fortinet-bug-allows-firewall-takeovers

[Source](https://threatpost.com/unpatched-fortinet-bug-firewall-takeovers/168764/){:target="_blank" rel="noopener"}

> The OS command-injection bug, in the web application firewall (WAF) platform known as FortiWeb, will get a patch at the end of the month. [...]
