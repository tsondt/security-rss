Title: Chinese espionage group targets Israel while suggesting the source could be Iran
Date: 2021-08-11T07:32:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-08-11-chinese-espionage-group-targets-israel-while-suggesting-the-source-could-be-iran

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/11/china_unc215_israel_attacks/){:target="_blank" rel="noopener"}

> FireEye says Israeli defense agencies were alert to compromises as China works to protect Belt and Road investments Security vendor FireEye says it has spotted a Chinese espionage group that successfully compromised targets within Israel, and that trying to make its efforts look like the work of Iranian actors is part of the group's modus operandi.... [...]
