Title: UK data watchdog sees its approach to government health tech during COVID-19 outbreak as 'pragmatic'
Date: 2021-08-04T08:28:03+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: uk-data-watchdog-sees-its-approach-to-government-health-tech-during-covid-19-outbreak-as-pragmatic

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/04/ico_annual_report/){:target="_blank" rel="noopener"}

> Pandemic also behind fall in breaches, according to ICO annual reaport The UK's data watchdog has defended its approach to regulating government health technologies during the pandemic as "pragmatic."... [...]
