Title: Good education: Cyber awareness initiative aims to close infosec workforce gap with free school curriculum
Date: 2021-08-19T12:53:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-19-good-education-cyber-awareness-initiative-aims-to-close-infosec-workforce-gap-with-free-school-curriculum

[Source](https://portswigger.net/daily-swig/good-education-cyber-awareness-initiative-aims-to-close-infosec-workforce-gap-with-free-school-curriculum){:target="_blank" rel="noopener"}

> Cyber.org leads program to bring better cybersecurity to US classrooms [...]
