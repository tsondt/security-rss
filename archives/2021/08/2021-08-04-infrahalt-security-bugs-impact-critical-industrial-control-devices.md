Title: INFRA:HALT security bugs impact critical industrial control devices
Date: 2021-08-04T10:16:17-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Legal
Slug: infrahalt-security-bugs-impact-critical-industrial-control-devices

[Source](https://www.bleepingcomputer.com/news/security/infra-halt-security-bugs-impact-critical-industrial-control-devices/){:target="_blank" rel="noopener"}

> High-severity and critical vulnerabilities collectively referred to as INFRA:HALT are affecting all versions of NicheStack below 4.3, a proprietary TCP/IP stack used by at least 200 industrial automation vendors, many in the leading segment of the market. [...]
