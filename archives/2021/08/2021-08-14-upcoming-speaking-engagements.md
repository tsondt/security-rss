Title: Upcoming Speaking Engagements
Date: 2021-08-14T17:01:46+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2021-08-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2021/08/upcoming-speaking-engagements-11.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking (via Internet) at SHIFT Business Festival in Finland, August 25-26, 2021. I’ll be speaking at an Informa event on September 14, 2021. Details to come. I’m keynoting CIISec Live —an all-online event—September 15-16, 2021. I’m speaking at the Cybersecurity and Data Privacy Law Conference in Plano, Texas, USA, September 22-23, 2021. The list is maintained on this page. [...]
