Title: Fight or flight: How one of the UK’s busiest airports defends against cyber-attacks
Date: 2021-08-13T18:06:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-13-fight-or-flight-how-one-of-the-uks-busiest-airports-defends-against-cyber-attacks

[Source](https://portswigger.net/daily-swig/fight-or-flight-how-one-of-the-uks-busiest-airports-defends-against-cyber-attacks){:target="_blank" rel="noopener"}

> Manchester Airport Group’s Tony Johnson reveals top threat to the sector – and it might not be what you’d expect [...]
