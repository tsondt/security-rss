Title: Fancy joining the SAS's secret hacker squad in Hereford as an electronics engineer for £33k?
Date: 2021-08-13T15:26:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-13-fancy-joining-the-sass-secret-hacker-squad-in-hereford-as-an-electronics-engineer-for-33k

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/13/sas_mab5_secret_hacker_squad_hereford/){:target="_blank" rel="noopener"}

> Hey MoD, nice to hear from you. What? Not secret any more, you say? A job ad blunder by the UK's Ministry of Defence has accidentally revealed the existence of a secret SAS mobile hacker squad.... [...]
