Title: Bot protection now generally available in Azure Web Application Firewall
Date: 2021-08-01T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: bot-protection-now-generally-available-in-azure-web-application-firewall

[Source](https://www.bleepingcomputer.com/news/security/bot-protection-now-generally-available-in-azure-web-application-firewall/){:target="_blank" rel="noopener"}

> Microsoft has announced that the Web Application Firewall (WAF) bot protection feature has reached general availability on Azure on Application Gateway starting this week. [...]
