Title: Microsoft Edge just got a 'Super Duper Secure Mode' upgrade
Date: 2021-08-05T06:38:40-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-edge-just-got-a-super-duper-secure-mode-upgrade

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-edge-just-got-a-super-duper-secure-mode-upgrade/){:target="_blank" rel="noopener"}

> Microsoft has announced that the Edge Vulnerability Research team is experimenting with a new feature dubbed "Super Duper Secure Mode" and designed to bring security improvements without significant performance losses. [...]
