Title: Black Hat USA: HTTP/2 flaws expose organizations to fresh wave of request smuggling attacks
Date: 2021-08-05T21:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: black-hat-usa-http2-flaws-expose-organizations-to-fresh-wave-of-request-smuggling-attacks

[Source](https://portswigger.net/daily-swig/black-hat-usa-http-2-flaws-expose-organizations-to-fresh-wave-of-request-smuggling-attacks){:target="_blank" rel="noopener"}

> Security researcher James Kettle digs deep into the web stack to reveal some shiny new attack surface [...]
