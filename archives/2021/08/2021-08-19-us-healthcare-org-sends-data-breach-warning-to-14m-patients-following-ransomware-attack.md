Title: US healthcare org sends data breach warning to 1.4m patients following ransomware attack
Date: 2021-08-19T14:38:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-19-us-healthcare-org-sends-data-breach-warning-to-14m-patients-following-ransomware-attack

[Source](https://portswigger.net/daily-swig/us-healthcare-org-sends-data-breach-warning-to-1-4m-patients-following-ransomware-attack){:target="_blank" rel="noopener"}

> Attackers gained access to St. Joseph’s/Candler network in December last year [...]
