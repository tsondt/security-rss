Title: Govt hackers impersonate HR employees to hit Israeli targets
Date: 2021-08-17T17:31:59-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-17-govt-hackers-impersonate-hr-employees-to-hit-israeli-targets

[Source](https://www.bleepingcomputer.com/news/security/govt-hackers-impersonate-hr-employees-to-hit-israeli-targets/){:target="_blank" rel="noopener"}

> Hackers associated with the Iranian government have focused attack efforts on IT and communication companies in Israel, likely in an attempt to pivot to their real targets. [...]
