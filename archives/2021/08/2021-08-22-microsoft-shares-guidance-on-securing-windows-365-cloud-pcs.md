Title: Microsoft shares guidance on securing Windows 365 Cloud PCs
Date: 2021-08-22T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Cloud;Security
Slug: 2021-08-22-microsoft-shares-guidance-on-securing-windows-365-cloud-pcs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-shares-guidance-on-securing-windows-365-cloud-pcs/){:target="_blank" rel="noopener"}

> Microsoft has shared guidance on securing Windows 365 Cloud PCs and more info on their built-in security capabilities. [...]
