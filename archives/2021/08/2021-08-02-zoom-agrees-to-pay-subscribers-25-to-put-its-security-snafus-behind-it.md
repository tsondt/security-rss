Title: Zoom agrees to pay subscribers $25 to put its security SNAFUs behind it
Date: 2021-08-02T05:29:03+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: zoom-agrees-to-pay-subscribers-25-to-put-its-security-snafus-behind-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/02/zoom_legal/){:target="_blank" rel="noopener"}

> Zoombombing class action offers US$85m in payments, meaning even free accounts get a few bucks US-based Zoom users may have a little cash coming their way after the video meeting outfit lodged a preliminary settlement in a class action related to some of its less-than-brilliant security and data protection practices.... [...]
