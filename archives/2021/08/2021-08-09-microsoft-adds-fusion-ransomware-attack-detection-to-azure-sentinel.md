Title: Microsoft adds Fusion ransomware attack detection to Azure Sentinel
Date: 2021-08-09T17:22:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-adds-fusion-ransomware-attack-detection-to-azure-sentinel

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-adds-fusion-ransomware-attack-detection-to-azure-sentinel/){:target="_blank" rel="noopener"}

> Microsoft says that the Azure Sentinel cloud-native SIEM (Security Information and Event Management) platform is now able to detect potential ransomware activity using the Fusion machine learning model. [...]
