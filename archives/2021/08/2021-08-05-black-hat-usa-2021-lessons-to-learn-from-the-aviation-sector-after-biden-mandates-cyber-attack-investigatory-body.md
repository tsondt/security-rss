Title: Black Hat USA 2021: Lessons to learn from the aviation sector after Biden mandates cyber-attack investigatory body
Date: 2021-08-05T16:47:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: black-hat-usa-2021-lessons-to-learn-from-the-aviation-sector-after-biden-mandates-cyber-attack-investigatory-body

[Source](https://portswigger.net/daily-swig/black-hat-usa-2021-lessons-to-learn-from-the-aviation-sector-after-biden-mandates-cyber-attack-investigatory-body){:target="_blank" rel="noopener"}

> ‘We might not make cyber-attacks as rare as airline disasters, but we can hopefully make them a more manageable problem’ [...]
