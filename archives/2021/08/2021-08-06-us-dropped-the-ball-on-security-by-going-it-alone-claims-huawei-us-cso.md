Title: US 'dropped the ball' on security by going it alone claims Huawei US CSO
Date: 2021-08-06T00:13:03+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: us-dropped-the-ball-on-security-by-going-it-alone-claims-huawei-us-cso

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/06/cso_huawei_us/){:target="_blank" rel="noopener"}

> Where there's a will, there's Huawei Andy Purdy, CSO for Huawei USA, believes the US needs to be more active in the development of global security standards rather than being aloof.... [...]
