Title: Boston Public Library discloses cyberattack, system-wide technical outage
Date: 2021-08-27T11:18:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-27-boston-public-library-discloses-cyberattack-system-wide-technical-outage

[Source](https://www.bleepingcomputer.com/news/security/boston-public-library-discloses-cyberattack-system-wide-technical-outage/){:target="_blank" rel="noopener"}

> The Boston Public Library (BPL) has disclosed today that its network was hit by a cyberattack on Wednesday, leading to a system-wide technical outage. [...]
