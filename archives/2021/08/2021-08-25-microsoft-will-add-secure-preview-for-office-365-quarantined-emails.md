Title: Microsoft will add secure preview for Office 365 quarantined emails
Date: 2021-08-25T15:15:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-08-25-microsoft-will-add-secure-preview-for-office-365-quarantined-emails

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-will-add-secure-preview-for-office-365-quarantined-emails/){:target="_blank" rel="noopener"}

> Microsoft is updating Defender for Office 365 to protect customers from embedded email threats while previewing quarantined emails. [...]
