Title: SteelSeries bug gives Windows 10 admin rights by plugging in a device
Date: 2021-08-24T14:54:17-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-24-steelseries-bug-gives-windows-10-admin-rights-by-plugging-in-a-device

[Source](https://www.bleepingcomputer.com/news/security/steelseries-bug-gives-windows-10-admin-rights-by-plugging-in-a-device/){:target="_blank" rel="noopener"}

> The official app for installing SteelSeries devices on Windows 10 can be exploited to obtain administrator rights, a security researcher has found. [...]
