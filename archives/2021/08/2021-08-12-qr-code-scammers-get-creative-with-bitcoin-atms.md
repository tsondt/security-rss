Title: QR Code Scammers Get Creative with Bitcoin ATMs
Date: 2021-08-12T13:59:18+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Web Security
Slug: 2021-08-12-qr-code-scammers-get-creative-with-bitcoin-atms

[Source](https://threatpost.com/qr-code-scammers-bitcoin-atms/168621/){:target="_blank" rel="noopener"}

> Threat actors are targeting everyone from job hunters to Bitcoin traders to college students wanting a break on their student loans, by exploiting the popular technology's trust relationship with users. [...]
