Title: Cream Finance DeFi Platform Rooked For $29M
Date: 2021-08-31T20:33:41+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Cryptography;Web Security
Slug: 2021-08-31-cream-finance-defi-platform-rooked-for-29m

[Source](https://threatpost.com/cream-finance-defi-29m/169077/){:target="_blank" rel="noopener"}

> Cream is latest DeFi platform to get fleeced in rash of attacks. [...]
