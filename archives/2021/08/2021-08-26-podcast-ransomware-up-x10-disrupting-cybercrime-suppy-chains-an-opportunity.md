Title: Podcast: Ransomware Up x10: Disrupting Cybercrime Suppy Chains an Opportunity
Date: 2021-08-26T13:00:11+00:00
Author: Threatpost
Category: Threatpost
Tags: Malware;Podcasts;Sponsored;Web Security
Slug: 2021-08-26-podcast-ransomware-up-x10-disrupting-cybercrime-suppy-chains-an-opportunity

[Source](https://threatpost.com/podcast-ransomware-up-tenfold-telecoms/168913/){:target="_blank" rel="noopener"}

> Derek Manky, Chief, Security Insights & Global Threat Alliances at Fortinet’s FortiGuard Labs, discusses the top threats and lessons learned from the first half of 2021. [...]
