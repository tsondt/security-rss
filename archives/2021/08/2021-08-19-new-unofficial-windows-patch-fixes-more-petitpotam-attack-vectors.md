Title: New unofficial Windows patch fixes more PetitPotam attack vectors
Date: 2021-08-19T14:30:33-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-08-19-new-unofficial-windows-patch-fixes-more-petitpotam-attack-vectors

[Source](https://www.bleepingcomputer.com/news/security/new-unofficial-windows-patch-fixes-more-petitpotam-attack-vectors/){:target="_blank" rel="noopener"}

> A second unofficial patch for the Windows PetitPotam NTLM relay attack has been released to fix further issues not addressed by Microsoft's official security update. [...]
