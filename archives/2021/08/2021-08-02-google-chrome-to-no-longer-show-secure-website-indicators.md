Title: Google Chrome to no longer show secure website indicators
Date: 2021-08-02T18:00:46-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Software
Slug: google-chrome-to-no-longer-show-secure-website-indicators

[Source](https://www.bleepingcomputer.com/news/google/google-chrome-to-no-longer-show-secure-website-indicators/){:target="_blank" rel="noopener"}

> Google Chrome will no longer show whether a site you are visiting is secure and only show when you visit an insecure website. [...]
