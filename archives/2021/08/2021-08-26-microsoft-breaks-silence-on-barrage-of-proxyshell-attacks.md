Title: Microsoft Breaks Silence on Barrage of ProxyShell Attacks
Date: 2021-08-26T12:39:54+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-08-26-microsoft-breaks-silence-on-barrage-of-proxyshell-attacks

[Source](https://threatpost.com/microsoft-barrage-proxyshell-attacks/168943/){:target="_blank" rel="noopener"}

> versions of the software are affected by a spate of bugs under active exploitations. [...]
