Title: Ransomware attack hits Italy's Lazio region, affects COVID-19 site
Date: 2021-08-03T14:13:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-attack-hits-italys-lazio-region-affects-covid-19-site

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-hits-italys-lazio-region-affects-covid-19-site/){:target="_blank" rel="noopener"}

> The Lazio region in Italy has suffered a reported ransomware attack that has disabled the region's IT systems, including the COVID-19 vaccination registration portal. [...]
