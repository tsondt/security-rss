Title: Critical Cisco Bug in VPN Routers Allows Remote Takeover
Date: 2021-08-06T16:07:55+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: critical-cisco-bug-in-vpn-routers-allows-remote-takeover

[Source](https://threatpost.com/critical-cisco-bug-vpn-routers/168449/){:target="_blank" rel="noopener"}

> Security researchers warned that at least 8,800 vulnerable systems are open to compromise. [...]
