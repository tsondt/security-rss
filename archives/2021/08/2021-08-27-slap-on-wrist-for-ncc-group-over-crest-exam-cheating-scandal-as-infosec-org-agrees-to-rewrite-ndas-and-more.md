Title: Slap on wrist for NCC Group over CREST exam-cheating scandal as infosec org agrees to rewrite NDAs and more
Date: 2021-08-27T15:55:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-27-slap-on-wrist-for-ncc-group-over-crest-exam-cheating-scandal-as-infosec-org-agrees-to-rewrite-ndas-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/27/ncc_group_crest_exam_cheating_scandal_statement/){:target="_blank" rel="noopener"}

> Two 'historic' incidents nearly a decade ago, says statement British infosec firm NCC Group has been rapped over the knuckles after infosec accreditation body CREST found it was "vicariously responsible" for employees who helped staff cheat certification exams.... [...]
