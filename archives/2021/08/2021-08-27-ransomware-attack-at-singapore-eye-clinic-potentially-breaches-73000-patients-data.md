Title: Ransomware attack at Singapore eye clinic potentially breaches 73,000 patients’ data
Date: 2021-08-27T13:00:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-27-ransomware-attack-at-singapore-eye-clinic-potentially-breaches-73000-patients-data

[Source](https://portswigger.net/daily-swig/ransomware-attack-at-singapore-eye-clinic-potentially-breaches-73-000-patients-data){:target="_blank" rel="noopener"}

> Healthcare provider hit by cyber-attack earlier this month [...]
