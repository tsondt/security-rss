Title: COVID-19 cases surge as do sales of fake vaccination cards – around $100 for something you could get free
Date: 2021-08-12T10:33:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-12-covid-19-cases-surge-as-do-sales-of-fake-vaccination-cards-around-100-for-something-you-could-get-free

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/12/fake_vaccine_cards/){:target="_blank" rel="noopener"}

> Vaccine deceit is infectious The number of COVID cases in the US and elsewhere is again rising, thanks to the Delta Variant, lagging vaccination rates, and mask resistance among some.... [...]
