Title: Hacker claims to steal data of 100 million T-mobile customers
Date: 2021-08-15T18:27:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-15-hacker-claims-to-steal-data-of-100-million-t-mobile-customers

[Source](https://www.bleepingcomputer.com/news/security/hacker-claims-to-steal-data-of-100-million-t-mobile-customers/){:target="_blank" rel="noopener"}

> A threat actor claims to have hacked T-Mobile's servers and stolen databases containing the personal data of approximately 100 million customers. [...]
