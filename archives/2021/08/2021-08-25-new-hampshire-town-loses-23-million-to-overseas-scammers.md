Title: New Hampshire town loses $2.3 million to overseas scammers
Date: 2021-08-25T12:54:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-25-new-hampshire-town-loses-23-million-to-overseas-scammers

[Source](https://www.bleepingcomputer.com/news/security/new-hampshire-town-loses-23-million-to-overseas-scammers/){:target="_blank" rel="noopener"}

> Peterborough, a small New Hampshire town, has lost $2.3 million after BEC scammers redirected several bank transfers using forged documents sent to the town's Finance Department staff in multiple email exchanges. [...]
