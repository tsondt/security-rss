Title: Accenture Confirms LockBit Ransomware Attack
Date: 2021-08-11T21:56:00+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;News;Vulnerabilities;Web Security
Slug: 2021-08-11-accenture-confirms-lockbit-ransomware-attack

[Source](https://threatpost.com/accenture-lockbit-ransomware-attack/168594/){:target="_blank" rel="noopener"}

> LockBit offered Accenture's purported databases and made a requisite jab at its purportedly sad security. Accenture says it recovered just fine from backups. [...]
