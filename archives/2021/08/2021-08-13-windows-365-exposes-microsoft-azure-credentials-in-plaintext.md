Title: Windows 365 exposes Microsoft Azure credentials in plaintext
Date: 2021-08-13T14:24:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-08-13-windows-365-exposes-microsoft-azure-credentials-in-plaintext

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-365-exposes-microsoft-azure-credentials-in-plaintext/){:target="_blank" rel="noopener"}

> A security researcher has figured out a way to dump a user's unencrypted plaintext Microsoft Azure credentials from Microsoft's new Windows 365 Cloud PC service using Mimikatz. [...]
