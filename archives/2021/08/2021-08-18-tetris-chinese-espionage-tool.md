Title: Tetris: Chinese Espionage Tool
Date: 2021-08-18T11:23:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;cyberespionage;espionage;spyware
Slug: 2021-08-18-tetris-chinese-espionage-tool

[Source](https://www.schneier.com/blog/archives/2021/08/tetris-chinese-espionage-tool.html){:target="_blank" rel="noopener"}

> I’m starting to see writings about a Chinese espionage tool that exploits website vulnerabilities to try and identify Chinese dissidents. [...]
