Title: Army Testing Facial Recognition in Child-Care Centers
Date: 2021-08-30T20:32:41+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Privacy
Slug: 2021-08-30-army-testing-facial-recognition-in-child-care-centers

[Source](https://threatpost.com/army-facial-recognition-child-care/169036/){:target="_blank" rel="noopener"}

> Army looking for AI to layer over daycare CCTV to boost ‘family quality of life.’ [...]
