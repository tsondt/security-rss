Title: Zero trust is a must: Supporting our customers with new BeyondCorp Enterprise features
Date: 2021-08-20T16:00:00+00:00
Author: Jian Zhen
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-08-20-zero-trust-is-a-must-supporting-our-customers-with-new-beyondcorp-enterprise-features

[Source](https://cloud.google.com/blog/products/identity-security/new-beyondcorp-enterprise-zero-trust-capabilities/){:target="_blank" rel="noopener"}

> Since launching BeyondCorp Enterprise in January, our team has been busy working with customers to understand how they are using the product and what we can do to better support their needs as they continue on their zero trust journey. We believe zero trust is an effective way to enhance overall security and provide a better user experience and BeyondCorp Enterprise can help make this possible. Today, we’re excited to announce three new BeyondCorp Enterprise features designed to help our customers provide their users simple and secure access to key applications. Certificate-based access via VPC-SC First, certificate-based access for GCP [...]
