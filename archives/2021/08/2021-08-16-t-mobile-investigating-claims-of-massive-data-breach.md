Title: T-Mobile Investigating Claims of Massive Data Breach
Date: 2021-08-16T23:53:37+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;IntelSecrets;IRDev;John Erin Binn;T-Mobile;Und0xxed;V0rtex
Slug: 2021-08-16-t-mobile-investigating-claims-of-massive-data-breach

[Source](https://krebsonsecurity.com/2021/08/t-mobile-investigating-claims-of-massive-data-breach/){:target="_blank" rel="noopener"}

> Communications giant T-Mobile said today it is investigating the extent of a breach that hackers claim has exposed sensitive personal data on 100 million T-Mobile USA customers, in many cases including the name, Social Security number, address, date of birth, phone number, security PINs and details that uniquely identify each customer’s mobile device. On Sunday, Vice.com broke the news that someone was selling data on 100 million people, and that the data came from T-Mobile. In a statement published on its website today, the company confirmed it had suffered an intrusion involving “some T-Mobile data,” but said it was too [...]
