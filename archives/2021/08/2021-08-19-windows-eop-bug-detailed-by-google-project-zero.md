Title: Windows EoP Bug Detailed by Google Project Zero
Date: 2021-08-19T16:58:54+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-08-19-windows-eop-bug-detailed-by-google-project-zero

[Source](https://threatpost.com/windows-eop-bug-detailed-by-google-project-zero/168823/){:target="_blank" rel="noopener"}

> Microsoft first dismissed the elevation of privilege flaw but decided yesterday that attackers injecting malicious code is worthy of attention. [...]
