Title: Kerberos Authentication Spoofing: Don’t Bypass the Spec
Date: 2021-08-18T13:19:15+00:00
Author: Yaron Kassner
Category: Threatpost
Tags: Hacks;InfoSec Insider;Vulnerabilities;Web Security
Slug: 2021-08-18-kerberos-authentication-spoofing-dont-bypass-the-spec

[Source](https://threatpost.com/kerberos-authentication-spoofing/168767/){:target="_blank" rel="noopener"}

> Yaron Kassner, CTO at Silverfort, discusses authentication-bypass bugs in Cisco ASA, F5 Big-IP, IBM QRadar and Palo Alto Networks PAN-OS. [...]
