Title: Cisco Issues Critical Fixes for High-End Nexus Gear
Date: 2021-08-25T22:48:34+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2021-08-25-cisco-issues-critical-fixes-for-high-end-nexus-gear

[Source](https://threatpost.com/cisco-issues-critical-fixes-for-high-end-nexus-gear/168939/){:target="_blank" rel="noopener"}

> Networking giant issues two critical patches and six high-severity patches. [...]
