Title: Kaseya’s ‘Master Key’ to REvil Attack Leaked Online
Date: 2021-08-11T15:34:13+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-08-11-kaseyas-master-key-to-revil-attack-leaked-online

[Source](https://threatpost.com/kaseyas-master-key-to-revil-attack-leaked-online/168565/){:target="_blank" rel="noopener"}

> The decryptor is of little use to other companies hit in the spate of attacks unleashed before the notorious ransomware group went dark, researchers said. [...]
