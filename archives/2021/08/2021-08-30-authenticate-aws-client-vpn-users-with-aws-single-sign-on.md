Title: Authenticate AWS Client VPN users with AWS Single Sign-On
Date: 2021-08-30T17:27:22+00:00
Author: Drew Marumoto
Category: AWS Security
Tags: AWS Client VPN;AWS Single Sign-On (SSO);Intermediate (200);Networking & Content Delivery;Security, Identity, & Compliance;AWS Single Sign-On;Hybrid connectivity;SAML;Security Blog;VPN
Slug: 2021-08-30-authenticate-aws-client-vpn-users-with-aws-single-sign-on

[Source](https://aws.amazon.com/blogs/security/authenticate-aws-client-vpn-users-with-aws-single-sign-on/){:target="_blank" rel="noopener"}

> AWS Client VPN is a managed client-based VPN service that enables users to use an OpenVPN-based client to securely access their resources in Amazon Web Services (AWS) and in their on-premises network from any location. In this blog post, we show you how you can integrate Client VPN with your existing AWS Single Sign-On via [...]
