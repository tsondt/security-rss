Title: ‘Friends’ Reunion Anchors Video Swindle
Date: 2021-08-11T16:43:37+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Most Recent ThreatLists;Web Security
Slug: 2021-08-11-friends-reunion-anchors-video-swindle

[Source](https://threatpost.com/friends-reunion-video-swindle/168583/){:target="_blank" rel="noopener"}

> Spam was on the rise in Q2, with video fraud and COVID-19-related efforts in the mix. [...]
