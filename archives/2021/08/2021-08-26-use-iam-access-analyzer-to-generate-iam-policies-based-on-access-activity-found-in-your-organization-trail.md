Title: Use IAM Access Analyzer to generate IAM policies based on access activity found in your organization trail
Date: 2021-08-26T20:33:49+00:00
Author: Mathangi Ramesh
Category: AWS Security
Tags: Amazon Simple Storage Services (S3);AWS IAM Access Analyzer;Intermediate (200);Security, Identity, & Compliance;Access management;Amazon CloudTrail;Amazon DynamoDB;Amazon EC2;Amazon S3;AWS IAM;AWS Lambda;least privilege;Policies;Security Blog
Slug: 2021-08-26-use-iam-access-analyzer-to-generate-iam-policies-based-on-access-activity-found-in-your-organization-trail

[Source](https://aws.amazon.com/blogs/security/use-iam-access-analyzer-to-generate-iam-policies-based-on-access-activity-found-in-your-organization-trail/){:target="_blank" rel="noopener"}

> In April 2021, AWS Identity and Access Management (IAM) Access Analyzer added policy generation to help you create fine-grained policies based on AWS CloudTrail activity stored within your account. Now, we’re extending policy generation to enable you to generate policies based on access activity stored in a designated account. For example, you can use AWS [...]
