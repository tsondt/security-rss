Title: The Week in Ransomware - August 13th 2021 - The rise of LockBit
Date: 2021-08-13T16:00:51-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-13-the-week-in-ransomware-august-13th-2021-the-rise-of-lockbit

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-august-13th-2021-the-rise-of-lockbit/){:target="_blank" rel="noopener"}

> This week we saw an existing operation rise in attacks while existing ransomware operations turn to Windows vulnerabilities to elevate their privileges. [...]
