Title: Microsoft Spills 38 Million Sensitive Data Records Via Careless Power App Configs
Date: 2021-08-23T23:18:36+00:00
Author: Tom Spring
Category: Threatpost
Tags: Breach;Cloud Security
Slug: 2021-08-23-microsoft-spills-38-million-sensitive-data-records-via-careless-power-app-configs

[Source](https://threatpost.com/microsoft-38-million-sensitive-records-power-app/168885/){:target="_blank" rel="noopener"}

> Data leaked includes COVID-19 vaccination records, social security numbers and email addresses tied to American Airlines, Ford, Indiana Department of Health and New York City public schools. [...]
