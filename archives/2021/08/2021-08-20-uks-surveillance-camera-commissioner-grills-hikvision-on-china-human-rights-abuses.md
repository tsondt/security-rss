Title: UK's Surveillance Camera Commissioner grills Hikvision on China human rights abuses
Date: 2021-08-20T09:54:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-20-uks-surveillance-camera-commissioner-grills-hikvision-on-china-human-rights-abuses

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/20/hikvision_surveillance_camera_commissioner_uighur_controversy/){:target="_blank" rel="noopener"}

> Eye-catching letter exchange revealed The China-based surveillance equipment manufacturer accused of being linked to the human rights abuse of the Uyghur ethnic minority in Xinjiang has denied any wrongdoing in a heated exchange with the UK's Surveillance Camera Commissioner.... [...]
