Title: F5 Bug Could Lead to Complete System Takeover
Date: 2021-08-26T16:40:38+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-08-26-f5-bug-could-lead-to-complete-system-takeover

[Source](https://threatpost.com/f5-critical-bug-system-takeover/168952/){:target="_blank" rel="noopener"}

> The worst of 13 bugs fixed by the August updates could lead to complete system compromise for users in sensitive sectors running products in Appliance mode. [...]
