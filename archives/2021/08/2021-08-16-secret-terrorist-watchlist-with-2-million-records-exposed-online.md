Title: Secret terrorist watchlist with 2 million records exposed online
Date: 2021-08-16T12:55:11-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-08-16-secret-terrorist-watchlist-with-2-million-records-exposed-online

[Source](https://www.bleepingcomputer.com/news/security/secret-terrorist-watchlist-with-2-million-records-exposed-online/){:target="_blank" rel="noopener"}

> A secret terrorist watchlist with 1.9 million records, including "no-fly" records was exposed on the internet. The list was left accessible on an Elasticsearch cluster that had no password on it. [...]
