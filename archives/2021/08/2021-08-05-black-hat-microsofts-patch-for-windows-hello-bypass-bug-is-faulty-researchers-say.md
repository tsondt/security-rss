Title: Black Hat: Microsoft’s Patch for Windows Hello Bypass Bug is Faulty, Researchers Say
Date: 2021-08-05T14:36:23+00:00
Author: Tom Spring
Category: Threatpost
Tags: Black Hat;Hacks;Vulnerabilities
Slug: black-hat-microsofts-patch-for-windows-hello-bypass-bug-is-faulty-researchers-say

[Source](https://threatpost.com/microsofts-patch-windows-hello-faulty/168392/){:target="_blank" rel="noopener"}

> Researchers show how to circumvent Microsoft’s Windows Hello biometric authentication using a spoofed USB camera. [...]
