Title: After reportedly dragging its feet, BlackBerry admits, yes, QNX in cars, equipment suffers from BadAlloc bug
Date: 2021-08-19T01:35:00+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-19-after-reportedly-dragging-its-feet-blackberry-admits-yes-qnx-in-cars-equipment-suffers-from-badalloc-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/19/blackberry_qnxrtos_badalloc/){:target="_blank" rel="noopener"}

> Four months after Microsoft went public, ex-RIM biz puts its hand up BlackBerry this week issued a critical security advisory for past versions of its QNX Real Time Operating System (RTOS), used in more than 175m cars, medical equipment, and industrial systems.... [...]
