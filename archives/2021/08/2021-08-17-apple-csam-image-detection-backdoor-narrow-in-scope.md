Title: Apple: CSAM Image-Detection Backdoor ‘Narrow’ in Scope
Date: 2021-08-17T13:58:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Privacy;Web Security
Slug: 2021-08-17-apple-csam-image-detection-backdoor-narrow-in-scope

[Source](https://threatpost.com/apple-image-detection-backdoor/168727/){:target="_blank" rel="noopener"}

> Computing giant tries to reassure users that the tool won’t be used for mass surveillance. [...]
