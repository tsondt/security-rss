Title: Legacy EDR. Yes, it’s a thing
Date: 2021-08-04T06:30:11+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: legacy-edr-yes-its-a-thing

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/04/legacy_edr_is_a_thing/){:target="_blank" rel="noopener"}

> Don’t let your endpoints become a sitting target, says Fortinet Sponsored Thirty years ago, the industry birthed networked antivirus (NAV), which later morphed into endpoint protection (EP), managed using endpoint protection platforms (EPPs). More recently, this era has faded as endpoint protection and response (EDR) and managed detection and response (MDR) services become the industry standard.... [...]
