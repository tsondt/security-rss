Title: Worried ransomware merchants know more about file storage than you do? You should be…
Date: 2021-08-23T17:00:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-08-23-worried-ransomware-merchants-know-more-about-file-storage-than-you-do-you-should-be

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/23/nasuni_cloud_bound_21/){:target="_blank" rel="noopener"}

> Find out more at Nasuni CloudBound21 Promo If you’re still not convinced of the need to reexamine your whole approach to file management, perhaps a ransomware attack will change your mind.... [...]
