Title: Data breach at US waste management firm exposes employees’ healthcare details
Date: 2021-08-12T11:10:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-12-data-breach-at-us-waste-management-firm-exposes-employees-healthcare-details

[Source](https://portswigger.net/daily-swig/data-breach-at-us-waste-management-firm-exposes-employees-healthcare-details){:target="_blank" rel="noopener"}

> Waste Management Resources said attacker gained network access in January [...]
