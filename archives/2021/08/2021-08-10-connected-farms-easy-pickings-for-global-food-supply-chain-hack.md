Title: Connected Farms Easy Pickings for Global Food Supply-Chain Hack
Date: 2021-08-10T21:21:44+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;IoT;Privacy;Vulnerabilities
Slug: 2021-08-10-connected-farms-easy-pickings-for-global-food-supply-chain-hack

[Source](https://threatpost.com/connected-farms-food-supply-chain-hack/168547/){:target="_blank" rel="noopener"}

> John Deere security bugs could allow cyberattackers to damage crops, surrounding property or even people; impact harvests; or destroy farmland for years. [...]
