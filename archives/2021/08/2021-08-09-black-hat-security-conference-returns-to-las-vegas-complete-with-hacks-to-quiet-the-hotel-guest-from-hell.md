Title: Black Hat security conference returns to Las Vegas – complete with hacks to quiet the hotel guest from hell
Date: 2021-08-09T04:02:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: black-hat-security-conference-returns-to-las-vegas-complete-with-hacks-to-quiet-the-hotel-guest-from-hell

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/09/in_brief_security/){:target="_blank" rel="noopener"}

> And a very scary story of a job that went from white hat to murky shades of gray in the United Arab Emirates In Brief After a year off due to a certain virus, the Black Hat and DEF CON security conferences returned to Las Vegas last week, just in time for the US government's attempts to foster more collaboration across the infosec industry.... [...]
