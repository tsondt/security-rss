Title: Surveillance tech company sues Police Digital Service over 'flawed' scoring of bids on £18m contract
Date: 2021-08-26T09:27:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-26-surveillance-tech-company-sues-police-digital-service-over-flawed-scoring-of-bids-on-18m-contract

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/26/excession_sues_police_digital_service_surveillance_framework/){:target="_blank" rel="noopener"}

> Excession chief exec testifies in High Court of England and Wales A company is suing the Police Digital Service (PDS) over a framework worth up to £18m after losing a bid to provide a mass surveillance platform, claiming police managers broke laws on the awarding of public contracts.... [...]
