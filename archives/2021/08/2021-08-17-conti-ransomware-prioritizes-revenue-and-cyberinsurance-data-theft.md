Title: Conti ransomware prioritizes revenue and cyberinsurance data theft
Date: 2021-08-17T15:27:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-17-conti-ransomware-prioritizes-revenue-and-cyberinsurance-data-theft

[Source](https://www.bleepingcomputer.com/news/security/conti-ransomware-prioritizes-revenue-and-cyberinsurance-data-theft/){:target="_blank" rel="noopener"}

> Training material used by Conti ransomware affiliates was leaked online this month, allowing an inside look at how attackers abuse legitimate software seek out cyber insurance policies. [...]
