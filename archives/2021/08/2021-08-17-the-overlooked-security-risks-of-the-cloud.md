Title: The Overlooked Security Risks of The Cloud
Date: 2021-08-17T18:56:39+00:00
Author: Nate Warfield
Category: Threatpost
Tags: Cloud Security;InfoSec Insider
Slug: 2021-08-17-the-overlooked-security-risks-of-the-cloud

[Source](https://threatpost.com/security-risks-cloud/168754/){:target="_blank" rel="noopener"}

> Nate Warfield, CTO of Prevaliion, discusses the top security concerns for those embracing virtual machines, public cloud storage and cloud strategies for remote working. [...]
