Title: Zoom Lied about End-to-End Encryption
Date: 2021-08-05T11:25:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: courts;encryption;lies;videoconferencing
Slug: zoom-lied-about-end-to-end-encryption

[Source](https://www.schneier.com/blog/archives/2021/08/zoom-lied-about-end-to-end-encryption.html){:target="_blank" rel="noopener"}

> The facts aren’t news, but Zoom will pay $85M — to the class-action attorneys, and to users — for lying to users about end-to-end encryption, and for giving user data to Facebook and Google without consent. The proposed settlement would generally give Zoom users $15 or $25 each and was filed Saturday at US District Court for the Northern District of California. It came nine months after Zoom agreed to security improvements and a “prohibition on privacy and security misrepresentations” in a settlement with the Federal Trade Commission, but the FTC settlement didn’t include compensation for users. [...]
