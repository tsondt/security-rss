Title: Black Hat USA: Downgrade attack against Let’s Encrypt lowers the bar for printing fraudulent SSL certificates
Date: 2021-08-06T14:51:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: black-hat-usa-downgrade-attack-against-lets-encrypt-lowers-the-bar-for-printing-fraudulent-ssl-certificates

[Source](https://portswigger.net/daily-swig/black-hat-usa-downgrade-attack-against-lets-encrypt-lowers-the-bar-for-printing-fraudulent-ssl-certificates){:target="_blank" rel="noopener"}

> German researchers circumvent key web security mechanism [...]
