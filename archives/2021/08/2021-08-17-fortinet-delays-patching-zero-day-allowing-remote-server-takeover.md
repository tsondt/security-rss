Title: Fortinet delays patching zero-day allowing remote server takeover
Date: 2021-08-17T09:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-17-fortinet-delays-patching-zero-day-allowing-remote-server-takeover

[Source](https://www.bleepingcomputer.com/news/security/fortinet-delays-patching-zero-day-allowing-remote-server-takeover/){:target="_blank" rel="noopener"}

> Fortinet has delayed patching a zero-day command injection vulnerability found in the FortiWeb web application firewall (WAF) until the end of August. [...]
