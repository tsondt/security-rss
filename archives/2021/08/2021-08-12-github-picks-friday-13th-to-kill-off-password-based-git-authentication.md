Title: GitHub picks Friday 13th to kill off password-based Git authentication
Date: 2021-08-12T23:20:42+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: 2021-08-12-github-picks-friday-13th-to-kill-off-password-based-git-authentication

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/12/git_proxyshell_gigabyte/){:target="_blank" rel="noopener"}

> Plus: eBPF Foundation emerges, Exchange severs probed for ProxyShell holes, and more In brief If your Git operations start failing on Friday, August 13 with GitHub, it may well be because you're still using password authentication – and you need to change that.... [...]
