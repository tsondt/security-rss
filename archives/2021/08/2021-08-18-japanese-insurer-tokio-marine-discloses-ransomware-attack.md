Title: Japanese insurer Tokio Marine discloses ransomware attack
Date: 2021-08-18T09:55:32-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-18-japanese-insurer-tokio-marine-discloses-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/japanese-insurer-tokio-marine-discloses-ransomware-attack/){:target="_blank" rel="noopener"}

> Tokio Marine Holdings, a multinational insurance holding company in Japan, announced this week that its Singapore branch, Tokio Marine Insurance Singapore (TMiS), suffered a ransomware attack. [...]
