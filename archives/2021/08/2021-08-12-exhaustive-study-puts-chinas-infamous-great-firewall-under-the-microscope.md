Title: Exhaustive study puts China’s infamous Great Firewall under the microscope
Date: 2021-08-12T14:13:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-12-exhaustive-study-puts-chinas-infamous-great-firewall-under-the-microscope

[Source](https://portswigger.net/daily-swig/exhaustive-study-puts-chinas-infamous-great-firewall-under-the-microscope){:target="_blank" rel="noopener"}

> Censorship platform is distorting some internet lookups, researchers warn [...]
