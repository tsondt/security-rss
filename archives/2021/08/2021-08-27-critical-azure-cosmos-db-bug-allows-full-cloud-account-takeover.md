Title: Critical Azure Cosmos DB Bug Allows Full Cloud Account Takeover
Date: 2021-08-27T16:49:23+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2021-08-27-critical-azure-cosmos-db-bug-allows-full-cloud-account-takeover

[Source](https://threatpost.com/azure-cosmos-db-bug-cloud/168986/){:target="_blank" rel="noopener"}

> It's unclear if Microsoft customers were breached during the months-long period where the #ChaosDB bug in Jupyter Notebooks was exploitable. [...]
