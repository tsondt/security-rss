Title: Microsoft Patch Tuesday bug drought: No, it's not climate change or unexpected code quality improvements
Date: 2021-08-10T19:53:22+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-10-microsoft-patch-tuesday-bug-drought-no-its-not-climate-change-or-unexpected-code-quality-improvements

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/10/microsoft_patch_tuesday/){:target="_blank" rel="noopener"}

> It's just temporary relief from the typical monthly repair routine Now is the winter of our discontent made glorious summer by the fact that it's August and Patch Tuesday brings word of only 44 vulnerabilities in Microsoft's software.... [...]
