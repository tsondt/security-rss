Title: Social account thief goes to prison for stealing, trading nude photos
Date: 2021-08-20T03:29:33-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-20-social-account-thief-goes-to-prison-for-stealing-trading-nude-photos

[Source](https://www.bleepingcomputer.com/news/security/social-account-thief-goes-to-prison-for-stealing-trading-nude-photos/){:target="_blank" rel="noopener"}

> A New York man received a three year sentence in federal prison for hacking social media accounts of dozens of female college students and stealing nude photos and videos of them. [...]
