Title: Microsoft Warns: Another Unpatched PrintNightmare Zero-Day
Date: 2021-08-12T13:19:50+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-08-12-microsoft-warns-another-unpatched-printnightmare-zero-day

[Source](https://threatpost.com/microsoft-unpatched-printnightmare-zero-day/168613/){:target="_blank" rel="noopener"}

> The out-of-band warning pairs with a working proof-of-concept exploit for the issue, circulating since mid-July. [...]
