Title: Black Hat Briefings: Hosted DNS configuration flaws risk leaking corporate network topologies
Date: 2021-08-05T15:43:48+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: black-hat-briefings-hosted-dns-configuration-flaws-risk-leaking-corporate-network-topologies

[Source](https://portswigger.net/daily-swig/black-hat-briefings-hosted-dns-configuration-flaws-risk-leaking-corporate-network-topologies){:target="_blank" rel="noopener"}

> AWS Route 53 plugs security hole, but other managed DNS platforms are potentially vulnerable, researchers warn [...]
