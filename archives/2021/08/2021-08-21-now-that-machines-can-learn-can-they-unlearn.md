Title: Now that machines can learn, can they unlearn?
Date: 2021-08-21T10:55:11+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Science;AI;algorithms;bias;privacy
Slug: 2021-08-21-now-that-machines-can-learn-can-they-unlearn

[Source](https://arstechnica.com/?p=1788910){:target="_blank" rel="noopener"}

> Enlarge (credit: Andriy Onufriyenko | Getty Images) Companies of all kinds use machine learning to analyze people’s desires, dislikes, or faces. Some researchers are now asking a different question: How can we make machines forget? A nascent area of computer science dubbed machine unlearning seeks ways to induce selective amnesia in artificial intelligence software. The goal is to remove all trace of a particular person or data point from a machine learning system, without affecting its performance. If made practical, the concept could give people more control over their data and the value derived from it. Although users can already [...]
