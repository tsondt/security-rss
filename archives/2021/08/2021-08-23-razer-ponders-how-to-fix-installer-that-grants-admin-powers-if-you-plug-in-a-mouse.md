Title: Razer ponders how to fix installer that grants admin powers if you plug in a mouse
Date: 2021-08-23T21:22:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-08-23-razer-ponders-how-to-fix-installer-that-grants-admin-powers-if-you-plug-in-a-mouse

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/23/security_in_brief/){:target="_blank" rel="noopener"}

> Plus: Cloudflare tackles huge DDoS attack, Apple and CSAM, and more In brief Razer is said to be working on an updated installer after it was discovered you can gain admin privileges on Windows by plugging in one of the gaming gear maker's mice or keyboards.... [...]
