Title: Nude hunt: LA phisherman accessed 4,700 iCloud accounts, 620K photos
Date: 2021-08-25T18:39:29+00:00
Author: Jim Salter
Category: Ars Technica
Tags: Biz & IT;Tech;fappening;icloud;infosec;phishing;privacy
Slug: 2021-08-25-nude-hunt-la-phisherman-accessed-4700-icloud-accounts-620k-photos

[Source](https://arstechnica.com/?p=1789674){:target="_blank" rel="noopener"}

> Enlarge / The Internet is unfortunately packed full of criminals seeking to steal sexual (or sexualizable) images from privately held cloud backup accounts. (credit: 1905HKN via Getty Images / Jim Salter ) The LA Times reported this week that Los Angeles man Hao Kuo "David" Chi pled guilty to four federal felonies related to his efforts to steal and share online nude images of young women. Chi collected more than 620,000 private photos and 9,000 videos from an undetermined number of victims across the US, most of whom were young and female. "At least 306" victims Chi's plea agreement with [...]
