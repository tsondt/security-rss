Title: US govt scores a point against Assange in run-up to extradition appeal showdown
Date: 2021-08-12T20:31:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-12-us-govt-scores-a-point-against-assange-in-run-up-to-extradition-appeal-showdown

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/12/assange_extradition_latest/){:target="_blank" rel="noopener"}

> Judge wrong to prevent Uncle Sam from challenging psychiatrist's suicide risk report, says High Court Analysis Julian Assange has lost a legal scrap in court, this time over the US government's attempt to expand its grounds for extraditing him from England to stand trial in America.... [...]
