Title: Think your backups will protect you against ransomware? They’re top of the target list
Date: 2021-08-12T17:30:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-08-12-think-your-backups-will-protect-you-against-ransomware-theyre-top-of-the-target-list

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/12/zero_trust_regcast/){:target="_blank" rel="noopener"}

> Zoom in on zero trust by tuning in and finding out Webcast Being hit by ransomware is gut wrenching enough, but it’ll be ten times worse if it coincides with the realization that your data protection systems just aren’t up the job anymore.... [...]
