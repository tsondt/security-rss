Title: Friday Squid Blogging: Tentacle Doorknob
Date: 2021-08-27T21:14:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-08-27-friday-squid-blogging-tentacle-doorknob

[Source](https://www.schneier.com/blog/archives/2021/08/friday-squid-blogging-tentacle-doorknob.html){:target="_blank" rel="noopener"}

> It’s pretty. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
