Title: Using “Master Faces” to Bypass Face-Recognition Authenticating Systems
Date: 2021-08-06T11:44:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;authentication;face recognition
Slug: using-master-faces-to-bypass-face-recognition-authenticating-systems

[Source](https://www.schneier.com/blog/archives/2021/08/using-master-faces-to-bypass-face-recognition-authenticating-systems.html){:target="_blank" rel="noopener"}

> Fascinating research: “ Generating Master Faces for Dictionary Attacks with a Network-Assisted Latent Space Evolution.” Abstract: A master face is a face image that passes face-based identity-authentication for a large portion of the population. These faces can be used to impersonate, with a high probability of success, any user, without having access to any user-information. We optimize these faces, by using an evolutionary algorithm in the latent embedding space of the StyleGAN face generator. Multiple evolutionary strategies are compared, and we propose a novel approach that employs a neural network in order to direct the search in the direction of [...]
