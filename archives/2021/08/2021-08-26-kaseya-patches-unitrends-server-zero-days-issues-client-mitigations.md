Title: Kaseya patches Unitrends server zero-days, issues client mitigations
Date: 2021-08-26T11:10:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-26-kaseya-patches-unitrends-server-zero-days-issues-client-mitigations

[Source](https://www.bleepingcomputer.com/news/security/kaseya-patches-unitrends-server-zero-days-issues-client-mitigations/){:target="_blank" rel="noopener"}

> American software company Kaseya has issued a security updates to patch server side Kaseya Unitrends vulnerabilities found by security researchers at the Dutch Institute for Vulnerability Disclosure (DIVD). [...]
