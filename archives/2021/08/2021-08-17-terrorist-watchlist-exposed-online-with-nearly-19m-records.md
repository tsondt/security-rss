Title: Terrorist Watchlist Exposed Online with Nearly 1.9M Records
Date: 2021-08-17T14:46:09+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Privacy
Slug: 2021-08-17-terrorist-watchlist-exposed-online-with-nearly-19m-records

[Source](https://threatpost.com/terrorist-watchlist-exposed-online/168737/){:target="_blank" rel="noopener"}

> A researcher discovered a data cache from the FBI’s Terrorist Screening Center left online without a password or authentication requirement. [...]
