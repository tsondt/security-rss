Title: Nokia subsidiary discloses data breach after Conti ransomware attack
Date: 2021-08-23T12:16:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-23-nokia-subsidiary-discloses-data-breach-after-conti-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/nokia-subsidiary-discloses-data-breach-after-conti-ransomware-attack/){:target="_blank" rel="noopener"}

> SAC Wireless, a US-based and independently-operating Nokia company subsidiary, has disclosed a data breach following a ransomware attack where Conti operators were able to successfully breach its network, steal data, and encrypt systems. [...]
