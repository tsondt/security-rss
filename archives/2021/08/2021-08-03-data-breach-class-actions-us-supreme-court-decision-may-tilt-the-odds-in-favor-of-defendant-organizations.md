Title: Data breach class actions: US Supreme Court decision may tilt the odds in favor of defendant organizations
Date: 2021-08-03T11:56:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-class-actions-us-supreme-court-decision-may-tilt-the-odds-in-favor-of-defendant-organizations

[Source](https://portswigger.net/daily-swig/data-breach-class-actions-us-supreme-court-decision-may-tilt-the-odds-in-favor-of-defendant-organizations){:target="_blank" rel="noopener"}

> TransUnion opinion raises bar for bringing federal class action lawsuits – but state courts offer breach victims a more viable alternative route, writes David Oberly [...]
