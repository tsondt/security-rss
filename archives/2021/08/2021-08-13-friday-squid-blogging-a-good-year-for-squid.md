Title: Friday Squid Blogging: A Good Year for Squid?
Date: 2021-08-13T21:28:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-08-13-friday-squid-blogging-a-good-year-for-squid

[Source](https://www.schneier.com/blog/archives/2021/08/friday-squid-blogging-a-good-year-for-squid.html){:target="_blank" rel="noopener"}

> Improved ocean conditions are leading to optimism about this year’s squid catch. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
