Title: Excellent Write-up of the SolarWinds Security Breach
Date: 2021-08-30T11:24:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;breaches;cybersecurity;data breaches;reports
Slug: 2021-08-30-excellent-write-up-of-the-solarwinds-security-breach

[Source](https://www.schneier.com/blog/archives/2021/08/excellent-write-up-of-the-solarwinds-security-breach.html){:target="_blank" rel="noopener"}

> Robert Chesney wrote up the Solar Winds story as a case study, and it’s a really good summary. [...]
