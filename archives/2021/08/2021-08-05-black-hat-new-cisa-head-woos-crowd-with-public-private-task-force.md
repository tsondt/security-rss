Title: Black Hat: New CISA Head Woos Crowd With Public-Private Task Force
Date: 2021-08-05T23:40:42+00:00
Author: Tom Spring
Category: Threatpost
Tags: Black Hat;Cryptography;Government;Hacks
Slug: black-hat-new-cisa-head-woos-crowd-with-public-private-task-force

[Source](https://threatpost.com/cisa-head-woos-security-crowd/168426/){:target="_blank" rel="noopener"}

> Day two Black Hat keynote by CISA Director Jen Easterly includes launch of private-public partnership with Amazon, Google and Microsoft to fight cybercrime. [...]
