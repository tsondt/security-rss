Title: Paragon: Yet Another Cyberweapons Arms Manufacturer
Date: 2021-08-03T11:44:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberweapons;encryption;patching;privacy;smartphones;spyware;surveillance;zero-day
Slug: paragon-yet-another-cyberweapons-arms-manufacturer

[Source](https://www.schneier.com/blog/archives/2021/08/paragon-yet-another-cyberweapons-arms-manufacturer.html){:target="_blank" rel="noopener"}

> Forbes has the story : Paragon’s product will also likely get spyware critics and surveillance experts alike rubbernecking: It claims to give police the power to remotely break into encrypted instant messaging communications, whether that’s WhatsApp, Signal, Facebook Messenger or Gmail, the industry sources said. One other spyware industry executive said it also promises to get longer-lasting access to a device, even when it’s rebooted. [...] Two industry sources said they believed Paragon was trying to set itself apart further by promising to get access to the instant messaging applications on a device, rather than taking complete control of everything [...]
