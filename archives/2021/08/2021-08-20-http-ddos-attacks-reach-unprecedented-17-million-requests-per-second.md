Title: HTTP DDoS attacks reach unprecedented 17 million requests per second
Date: 2021-08-20T12:50:05-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-20-http-ddos-attacks-reach-unprecedented-17-million-requests-per-second

[Source](https://www.bleepingcomputer.com/news/security/http-ddos-attacks-reach-unprecedented-17-million-requests-per-second/){:target="_blank" rel="noopener"}

> A distributed denial-of-service (DDoS) attack earlier this year takes the top spot for the largest such incident, peaking at 17.2 million requests per second (rps). [...]
