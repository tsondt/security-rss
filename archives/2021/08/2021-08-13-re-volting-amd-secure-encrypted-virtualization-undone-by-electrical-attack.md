Title: Re-volting: AMD Secure Encrypted Virtualization undone by electrical attack
Date: 2021-08-13T07:35:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-13-re-volting-amd-secure-encrypted-virtualization-undone-by-electrical-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/13/amd_secure_encrypted_virtualization/){:target="_blank" rel="noopener"}

> Fault injection technique presents risk in cloud environments from rogue admins AMD's Secure Encrypted Virtualization (SEV) scheme is not as secure as its name suggests.... [...]
