Title: Ukraine shuts down money laundering cryptocurrency exchanges
Date: 2021-08-12T12:16:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-08-12-ukraine-shuts-down-money-laundering-cryptocurrency-exchanges

[Source](https://www.bleepingcomputer.com/news/security/ukraine-shuts-down-money-laundering-cryptocurrency-exchanges/){:target="_blank" rel="noopener"}

> The Security Service of Ukraine (SBU) took down a network of cryptocurrency exchanges used to anonymize transactions since the beginning of 2021. [...]
