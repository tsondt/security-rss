Title: Microsoft confirms another Windows print spooler zero-day bug
Date: 2021-08-11T18:10:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-08-11-microsoft-confirms-another-windows-print-spooler-zero-day-bug

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-confirms-another-windows-print-spooler-zero-day-bug/){:target="_blank" rel="noopener"}

> Microsoft has issued an advisory for another zero-day Windows print spooler vulnerability tracked as CVE-2021-36958 that allows local attackers to gain SYSTEM privileges on a computer. [...]
