Title: Microsoft 365 Usage Analytics now anonymizes user info by default
Date: 2021-08-31T17:42:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-08-31-microsoft-365-usage-analytics-now-anonymizes-user-info-by-default

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-365-usage-analytics-now-anonymizes-user-info-by-default/){:target="_blank" rel="noopener"}

> Microsoft has announced that it will start anonymizing​​​​​​​ user-level info by default Microsoft 365 Usage Analytics beginning with September 1, 2021. [...]
