Title: Angry Affiliate Leaks Conti Ransomware Gang Playbook
Date: 2021-08-06T14:44:16+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: angry-affiliate-leaks-conti-ransomware-gang-playbook

[Source](https://threatpost.com/affiliate-leaks-conti-ransomware-playbook/168442/){:target="_blank" rel="noopener"}

> The data includes IP addresses for Cobalt Strike C2 servers as well as an archive including numerous tools and training materials for the group, revealing how it performs attacks. [...]
