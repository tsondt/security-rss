Title: Chinese auto-maker accused of altering data after fatal autonomous car accident
Date: 2021-08-24T08:02:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-08-24-chinese-auto-maker-accused-of-altering-data-after-fatal-autonomous-car-accident

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/24/chinese_police_investigating_fatal_ev_accident/){:target="_blank" rel="noopener"}

> Driver assistance feature was engaged in level-2 autonomous car at time of incident Police are investigating an electrical vehicle company in China following claims that car data was tampered with following a fatal collision.... [...]
