Title: Angry Conti ransomware affiliate leaks gang's attack playbook
Date: 2021-08-05T14:29:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: angry-conti-ransomware-affiliate-leaks-gangs-attack-playbook

[Source](https://www.bleepingcomputer.com/news/security/angry-conti-ransomware-affiliate-leaks-gangs-attack-playbook/){:target="_blank" rel="noopener"}

> A disgruntled Conti affiliate has leaked the gang's training material when conducting attacks, including information about one of the ransomware's operators. [...]
