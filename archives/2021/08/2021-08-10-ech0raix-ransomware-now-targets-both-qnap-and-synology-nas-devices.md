Title: eCh0raix ransomware now targets both QNAP and Synology NAS devices
Date: 2021-08-10T08:10:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ech0raix-ransomware-now-targets-both-qnap-and-synology-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/ech0raix-ransomware-now-targets-both-qnap-and-synology-nas-devices/){:target="_blank" rel="noopener"}

> A newly discovered eCh0raix ransomware variant has added support for encrypting both QNAP and Synology Network-Attached Storage (NAS) devices. [...]
