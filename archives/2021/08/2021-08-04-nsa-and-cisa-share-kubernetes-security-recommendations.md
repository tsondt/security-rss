Title: NSA and CISA share Kubernetes security recommendations
Date: 2021-08-04T01:02:03-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: nsa-and-cisa-share-kubernetes-security-recommendations

[Source](https://www.bleepingcomputer.com/news/security/nsa-and-cisa-share-kubernetes-security-recommendations/){:target="_blank" rel="noopener"}

> The National Security Agency (NSA) and the Cybersecurity and Infrastructure Security Agency (CISA) have published comprehensive recommendations for strengthening the security of an organization's Kubernetes system. [...]
