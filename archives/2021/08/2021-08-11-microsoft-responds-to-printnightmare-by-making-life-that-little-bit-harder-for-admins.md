Title: Microsoft responds to PrintNightmare by making life that little bit harder for admins
Date: 2021-08-11T11:54:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-08-11-microsoft-responds-to-printnightmare-by-making-life-that-little-bit-harder-for-admins

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/11/printnightmare_mitigation/){:target="_blank" rel="noopener"}

> Have they forgotten SysAdmin Appreciation Day so soon? Microsoft appears intent on turning the print spooler remote code execution vulnerability known as "PrintNightmare" into an AdminNightmare, judging by its latest mitigation, which requires administrator privileges for Point and Print driver installation and update.... [...]
