Title: Un-carrier? Definitely Unsecure: T-Mobile US admits 48m customers' details stolen after downplaying reports
Date: 2021-08-18T12:37:03+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-18-un-carrier-definitely-unsecure-t-mobile-us-admits-48m-customers-details-stolen-after-downplaying-reports

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/18/t_mobile_us_admits_hack_48m_users/){:target="_blank" rel="noopener"}

> Affected users to get free McAfee ID protection, so that's OK then T-Mobile US has begun admitting to the theft of 100 million user accounts in stages, confessing overnight that 8 million people's personal details had been stolen from its servers.... [...]
