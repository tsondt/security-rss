Title: How to conduct live network forensics in GCP
Date: 2021-08-18T16:00:00+00:00
Author: Assaf Namer
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-08-18-how-to-conduct-live-network-forensics-in-gcp

[Source](https://cloud.google.com/blog/products/identity-security/how-to-use-live-forensics-to-analyze-a-cyberattack/){:target="_blank" rel="noopener"}

> Forensics is the application of science to criminal and civil laws. It is a proven approach for gathering and processing evidence at a crime scene. An integral step in the forensics process is the isolation of the scene without contaminating or modifying the evidence. The isolation step prevents any further contamination or tampering with possible evidence. The same philosophy can be applied to the investigation of digital events. In this post we will review methods, tactics and architecture designs to isolate an infected VM while still making it accessible to forensic tools. The goal is to allow access so that [...]
