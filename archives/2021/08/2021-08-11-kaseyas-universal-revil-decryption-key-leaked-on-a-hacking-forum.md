Title: Kaseya's universal REvil decryption key leaked on a hacking forum
Date: 2021-08-11T02:01:21-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-11-kaseyas-universal-revil-decryption-key-leaked-on-a-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/kaseyas-universal-revil-decryption-key-leaked-on-a-hacking-forum/){:target="_blank" rel="noopener"}

> The universal decryption key for REvil's attack on Kaseya's customers has been leaked on hacking forums allowing researchers their first glimpse of the mysterious key. [...]
