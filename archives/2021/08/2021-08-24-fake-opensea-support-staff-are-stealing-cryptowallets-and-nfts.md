Title: Fake OpenSea support staff are stealing cryptowallets and NFTs
Date: 2021-08-24T19:46:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-08-24-fake-opensea-support-staff-are-stealing-cryptowallets-and-nfts

[Source](https://www.bleepingcomputer.com/news/security/fake-opensea-support-staff-are-stealing-cryptowallets-and-nfts/){:target="_blank" rel="noopener"}

> OpenSea users are being targeted in an ongoing and aggressive Discord phishing attack to steal cryptocurrency funds and NFTs. [...]
