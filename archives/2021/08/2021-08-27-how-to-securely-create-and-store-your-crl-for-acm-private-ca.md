Title: How to securely create and store your CRL for ACM Private CA
Date: 2021-08-27T21:32:22+00:00
Author: Tracy Pierce
Category: AWS Security
Tags: Amazon Simple Storage Services (S3);AWS Certificate Manager;Intermediate (200);Security, Identity, & Compliance;ACM;ACM Private CA;Amazon S3;Certificate revocation lsit;CRL;Privacy;Security Blog
Slug: 2021-08-27-how-to-securely-create-and-store-your-crl-for-acm-private-ca

[Source](https://aws.amazon.com/blogs/security/how-to-securely-create-and-store-your-crl-for-acm-private-ca/){:target="_blank" rel="noopener"}

> In this blog post, I show you how to protect your Amazon Simple Storage Service (Amazon S3) bucket while still allowing access to your AWS Certificate Manager (ACM) Private Certificate Authority (CA) certificate revocation list (CRL). A CRL is a list of certificates that have been revoked by the CA. Certificates can be revoked because [...]
