Title: More on Apple’s iPhone Backdoor
Date: 2021-08-20T13:54:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;backdoors;hashes;photos;privacy;surveillance
Slug: 2021-08-20-more-on-apples-iphone-backdoor

[Source](https://www.schneier.com/blog/archives/2021/08/more-on-apples-iphone-backdoor.html){:target="_blank" rel="noopener"}

> In this post, I’ll collect links on Apple’s iPhone backdoor for scanning CSAM images. Previous links are here and here. Apple says that hash collisions in its CSAM detection system were expected, and not a concern. I’m not convinced that this secondary system was originally part of the design, since it wasn’t discussed in the original specification. Good op-ed from a group of Princeton researchers who developed a similar system: Our system could be easily repurposed for surveillance and censorship. The design wasn’t restricted to a specific category of content; a service could simply swap in any content-matching database, and [...]
