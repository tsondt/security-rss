Title: Surveillance of the Internet Backbone
Date: 2021-08-25T15:13:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;forensics;Internet;privacy;surveillance;tracking;traffic analysis
Slug: 2021-08-25-surveillance-of-the-internet-backbone

[Source](https://www.schneier.com/blog/archives/2021/08/surveillance-of-the-internet-backbone.html){:target="_blank" rel="noopener"}

> Vice has an article about how data brokers sell access to the Internet backbone. This is netflow data. It’s useful for cybersecurity forensics, but can also be used for things like tracing VPN activity. At a high level, netflow data creates a picture of traffic flow and volume across a network. It can show which server communicated with another, information that may ordinarily only be available to the server owner or the ISP carrying the traffic. Crucially, this data can be used for, among other things, tracking traffic through virtual private networks, which are used to mask where someone is [...]
