Title: Enfilade: Open source tool flags ransomware and bot infections in MongoDB instances
Date: 2021-08-06T13:08:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: enfilade-open-source-tool-flags-ransomware-and-bot-infections-in-mongodb-instances

[Source](https://portswigger.net/daily-swig/enfilade-open-source-tool-flags-ransomware-and-bot-infections-in-mongodb-instances){:target="_blank" rel="noopener"}

> Malware research tool lands on GitHub after Covid-related cancellation of Black Hat presentation [...]
