Title: Critical vulnerabilities in web file manager elFinder leave servers susceptible to takeover
Date: 2021-08-23T14:22:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-23-critical-vulnerabilities-in-web-file-manager-elfinder-leave-servers-susceptible-to-takeover

[Source](https://portswigger.net/daily-swig/critical-vulnerabilities-in-web-file-manager-elfinder-leave-servers-susceptible-to-takeover){:target="_blank" rel="noopener"}

> Immediate triage urged as researchers warn in-the-wild exploitation likely [...]
