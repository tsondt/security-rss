Title: XSS Bug in SEOPress WordPress Plugin Allows Site Takeover
Date: 2021-08-16T18:22:25+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-08-16-xss-bug-in-seopress-wordpress-plugin-allows-site-takeover

[Source](https://threatpost.com/xss-bug-seopress-wordpress-plugin/168702/){:target="_blank" rel="noopener"}

> The bug would allow a number of malicious actions, up to and including full site takeover. The vulnerable plugin is installed on 100,000 websites. [...]
