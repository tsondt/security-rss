Title: Apple's iPhone computer vision has the potential to preserve privacy but also break it completely
Date: 2021-08-16T09:27:09+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2021-08-16-apples-iphone-computer-vision-has-the-potential-to-preserve-privacy-but-also-break-it-completely

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/16/ai_vision_apple/){:target="_blank" rel="noopener"}

> Cupertino can see things you people wouldn't believe Opinion For a company built around helping people communicate, Apple sure has problems talking to folk. It pole-vaulted itself feet first into the minefield of Child Sexual Abuse Material (CSAM), saying that it was going to be checking everybody's images whether they liked it or not.... [...]
