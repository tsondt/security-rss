Title: Critical F5 BIG-IP bug impacts customers in sensitive sectors
Date: 2021-08-25T14:58:35-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-25-critical-f5-big-ip-bug-impacts-customers-in-sensitive-sectors

[Source](https://www.bleepingcomputer.com/news/security/critical-f5-big-ip-bug-impacts-customers-in-sensitive-sectors/){:target="_blank" rel="noopener"}

> BIG-IP application services company F5 has fixed more than a dozen high-severity vulnerabilities in its networking device, one of them being elevated to critical severity under specific conditions. [...]
