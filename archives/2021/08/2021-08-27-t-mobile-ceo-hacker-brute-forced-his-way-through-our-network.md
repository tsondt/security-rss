Title: T-Mobile CEO: Hacker brute-forced his way through our network
Date: 2021-08-27T12:51:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-27-t-mobile-ceo-hacker-brute-forced-his-way-through-our-network

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-ceo-hacker-brute-forced-his-way-through-our-network/){:target="_blank" rel="noopener"}

> Today, T-Mobile's CEO Mike Sievert said that the hacker behind the carrier's latest massive data breach brute forced his way through T-Mobile's network after gaining access to testing environments. [...]
