Title: How AWS can help your US federal agency meet the executive order on improving the nation’s cybersecurity
Date: 2021-08-13T19:28:26+00:00
Author: Michael Cotton
Category: AWS Security
Tags: Announcements;Federal;Foundational (100);Government;Public Sector;Security, Identity, & Compliance;cloud adoption;Cybersecurity EO;Executive order;Security Blog;Zero Trust
Slug: 2021-08-13-how-aws-can-help-your-us-federal-agency-meet-the-executive-order-on-improving-the-nations-cybersecurity

[Source](https://aws.amazon.com/blogs/security/how-aws-can-help-your-us-federal-agency-meet-the-executive-order-on-improving-the-nations-cybersecurity/){:target="_blank" rel="noopener"}

> AWS can support your information security modernization program to meet the President’s Executive Order on Improving the Nation’s Cybersecurity (issued May 12th, 2021). When working with AWS, a US federal agency gains access to resources, expertise, technology, professional services, and our AWS Partner Network (APN), which can help the agency meet the security and compliance [...]
