Title: Australian govt warns of escalating LockBit ransomware attacks
Date: 2021-08-08T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: australian-govt-warns-of-escalating-lockbit-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/australian-govt-warns-of-escalating-lockbit-ransomware-attacks/){:target="_blank" rel="noopener"}

> The Australian Cyber Security Centre (ACSC) warns of an increase of LockBit 2.0 ransomware attacks against Australian organizations starting July 2021. [...]
