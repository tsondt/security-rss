Title: Top 3 API Vulnerabilities: Why Apps are Pwned by Cyberattackers
Date: 2021-08-31T13:29:17+00:00
Author: Jason Kent
Category: Threatpost
Tags: InfoSec Insider;Vulnerabilities;Web Security
Slug: 2021-08-31-top-3-api-vulnerabilities-why-apps-are-pwned-by-cyberattackers

[Source](https://threatpost.com/top-3-api-vulnerabilities-cyberattackers/169048/){:target="_blank" rel="noopener"}

> Jason Kent, hacker-in-residence at Cequence, talks about how cybercriminals target apps and how to thwart them. [...]
