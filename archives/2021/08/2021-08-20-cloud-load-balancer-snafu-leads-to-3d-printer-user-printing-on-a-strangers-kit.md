Title: Cloud load balancer snafu leads to 3D printer user printing on a stranger's kit
Date: 2021-08-20T13:47:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-20-cloud-load-balancer-snafu-leads-to-3d-printer-user-printing-on-a-strangers-kit

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/20/3d_printer_spaghetti_detectives_cloud_config/){:target="_blank" rel="noopener"}

> Founder of The Spaghetti Detective apologises for config blunder A 3D printer remote monitoring company accidentally exposed users' printers to each other after a cloud reconfiguration snafu.... [...]
