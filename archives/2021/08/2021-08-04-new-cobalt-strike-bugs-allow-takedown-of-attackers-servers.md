Title: New Cobalt Strike bugs allow takedown of attackers’ servers
Date: 2021-08-04T09:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: new-cobalt-strike-bugs-allow-takedown-of-attackers-servers

[Source](https://www.bleepingcomputer.com/news/security/new-cobalt-strike-bugs-allow-takedown-of-attackers-servers/){:target="_blank" rel="noopener"}

> Security researchers have discovered Cobalt Strike denial of service (DoS) vulnerabilities that allow blocking beacon command-and-control (C2) communication channels and new deployments. [...]
