Title: Bogus Cryptomining Apps Infest Google Play
Date: 2021-08-18T18:26:25+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2021-08-18-bogus-cryptomining-apps-infest-google-play

[Source](https://threatpost.com/bogus-cryptomining-apps-google-play/168785/){:target="_blank" rel="noopener"}

> The apps attempt to swindle users into buying in-app upgrades or clicking on masses of ads. [...]
