Title: FIN8 Targets US Bank With New ‘Sardonic’ Backdoor
Date: 2021-08-27T17:32:56+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-08-27-fin8-targets-us-bank-with-new-sardonic-backdoor

[Source](https://threatpost.com/fin8-bank-sardonic-backdoor/168982/){:target="_blank" rel="noopener"}

> The latest refinement of the APT's BadHatch backdoor can leverage new malware on the fly without redeployment, making it potent and nimble. [...]
