Title: CISA shares guidance on how to prevent ransomware data breaches
Date: 2021-08-19T08:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-19-cisa-shares-guidance-on-how-to-prevent-ransomware-data-breaches

[Source](https://www.bleepingcomputer.com/news/security/cisa-shares-guidance-on-how-to-prevent-ransomware-data-breaches/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) has released guidance to help government and private sector organizations prevent data breaches resulting from ransomware double extortion schemes. [...]
