Title: Password of three random words better than complex variation, experts say
Date: 2021-08-07T11:29:13+00:00
Author: PA Media
Category: The Guardian
Tags: Data and computer security;UK news;Cybercrime;Internet;GCHQ;Technology
Slug: password-of-three-random-words-better-than-complex-variation-experts-say

[Source](https://www.theguardian.com/technology/2021/aug/07/password-of-three-random-words-better-than-complex-variation-experts-say){:target="_blank" rel="noopener"}

> UK National Cyber Security Centre recommends approach for improved combination of usability and safety It is far better to concoct passwords made up of three random words than to use more complex variations involving streams of letters, numbers and symbols, UK government experts have said. The National Cyber Security Centre (NCSC), part of Government Communications Headquarters, highlighted its “three random words” recommendation in a new blogpost. Continue reading... [...]
