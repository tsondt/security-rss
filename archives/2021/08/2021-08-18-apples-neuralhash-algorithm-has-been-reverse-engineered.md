Title: Apple’s NeuralHash Algorithm Has Been Reverse-Engineered
Date: 2021-08-18T16:51:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;algorithms;Apple;backdoors;cryptography;hashes;iOS;iPhone;reverse engineering
Slug: 2021-08-18-apples-neuralhash-algorithm-has-been-reverse-engineered

[Source](https://www.schneier.com/blog/archives/2021/08/apples-neuralhash-algorithm-has-been-reverse-engineered.html){:target="_blank" rel="noopener"}

> Apple’s NeuralHash algorithm — the one it’s using for client-side scanning on the iPhone — has been reverse-engineered. Turns out it was already in iOS 14.3, and someone noticed : Early tests show that it can tolerate image resizing and compression, but not cropping or rotations. We also have the first collision : two images that hash to the same value. The next step is to generate innocuous images that NeuralHash classifies as prohibited content. This was a bad idea from the start, and Apple never seemed to consider the adversarial context of the system as a whole, and not [...]
