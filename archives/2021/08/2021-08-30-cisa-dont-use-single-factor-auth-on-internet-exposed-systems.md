Title: CISA: Don’t use single-factor auth on Internet-exposed systems
Date: 2021-08-30T13:10:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-30-cisa-dont-use-single-factor-auth-on-internet-exposed-systems

[Source](https://www.bleepingcomputer.com/news/security/cisa-don-t-use-single-factor-auth-on-internet-exposed-systems/){:target="_blank" rel="noopener"}

> Single-factor authentication (SFA) has been added today by the US Cybersecurity and Infrastructure Security Agency (CISA) to a very short list of cybersecurity bad practices it advises against. [...]
