Title: Audiomack music streaming platform launches public bug bounty
Date: 2021-08-17T14:16:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-17-audiomack-music-streaming-platform-launches-public-bug-bounty

[Source](https://portswigger.net/daily-swig/audiomack-music-streaming-platform-launches-public-bug-bounty){:target="_blank" rel="noopener"}

> New program follows a year-long private VDP [...]
