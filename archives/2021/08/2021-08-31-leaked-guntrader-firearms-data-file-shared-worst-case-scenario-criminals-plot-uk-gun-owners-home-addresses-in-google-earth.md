Title: Leaked Guntrader firearms data file shared. Worst case scenario? Criminals plot UK gun owners' home addresses in Google Earth
Date: 2021-08-31T14:19:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-31-leaked-guntrader-firearms-data-file-shared-worst-case-scenario-criminals-plot-uk-gun-owners-home-addresses-in-google-earth

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/31/guntrader_breach_csv_danger/){:target="_blank" rel="noopener"}

> Bang out of order Updated The names and home addresses of 111,000 British firearm owners have been dumped online as a Google Earth-compatible CSV file that pinpoints domestic homes as likely firearm storage locations – a worst-case scenario for victims of the breach.... [...]
