Title: Hacker behind biggest ever cryptocurrency heist returns stolen funds
Date: 2021-08-11T17:21:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency;Security
Slug: 2021-08-11-hacker-behind-biggest-ever-cryptocurrency-heist-returns-stolen-funds

[Source](https://www.bleepingcomputer.com/news/security/hacker-behind-biggest-ever-cryptocurrency-heist-returns-stolen-funds/){:target="_blank" rel="noopener"}

> The threat actor who hacked Poly Network's cross-chain interoperability protocol yesterday to steal over $600 million worth of cryptocurrency assets is now returning the stolen funds. [...]
