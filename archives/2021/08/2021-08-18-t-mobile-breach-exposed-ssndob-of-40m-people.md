Title: T-Mobile: Breach Exposed SSN/DOB of 40M+ People
Date: 2021-08-18T16:24:33+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;T-Mobile breach
Slug: 2021-08-18-t-mobile-breach-exposed-ssndob-of-40m-people

[Source](https://krebsonsecurity.com/2021/08/t-mobile-breach-exposed-ssn-dob-of-40m-people/){:target="_blank" rel="noopener"}

> T-Mobile is warning that a data breach has exposed the names, date of birth, Social Security number and driver’s license/ID information of more than 40 million current, former or prospective customers who applied for credit with the company. The acknowledgment came less than 48 hours after millions of the stolen T-Mobile customer records went up for sale in the cybercrime underground. In a statement Tuesday evening, T-Mobile said a “highly sophisticated” attack against its network led to the breach of data on millions of customers. “Our preliminary analysis is that approximately 7.8 million current T-Mobile postpaid customer accounts’ information appears [...]
