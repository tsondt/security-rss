Title: ‘DeadRinger’ Targeted Exchange Servers Long Before Discovery
Date: 2021-08-03T14:55:56+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: deadringer-targeted-exchange-servers-long-before-discovery

[Source](https://threatpost.com/deadringer-targeted-exchange-servers-before-discovery/168300/){:target="_blank" rel="noopener"}

> Cyberespionage campaigns linked to China attacked telecoms via ProxyLogon bugs, stealing call records and maintaining persistence, as far back as 2017. [...]
