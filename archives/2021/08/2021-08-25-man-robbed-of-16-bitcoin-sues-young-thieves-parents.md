Title: Man Robbed of 16 Bitcoin Sues Young Thieves’ Parents
Date: 2021-08-25T22:20:16+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;SIM Swapping;Web Fraud 2.0;Andrew Schober;Hazel D. Wells;mark rasch
Slug: 2021-08-25-man-robbed-of-16-bitcoin-sues-young-thieves-parents

[Source](https://krebsonsecurity.com/2021/08/man-robbed-of-16-bitcoin-sues-young-thieves-parents/){:target="_blank" rel="noopener"}

> In 2018, Andrew Schober was digitally mugged for approximately $1 million worth of bitcoin. After several years of working with investigators, Schober says he’s confident he has located two young men in the United Kingdom responsible for developing a clever piece of digital clipboard-stealing malware that let them siphon his crypto holdings. Schober is now suing each of their parents in a civil case that seeks to extract what their children would not return voluntarily. In a lawsuit filed in Colorado, Schober said the sudden disappearance of his funds in January 2018 prompted him to spend more than $10,000 hiring [...]
