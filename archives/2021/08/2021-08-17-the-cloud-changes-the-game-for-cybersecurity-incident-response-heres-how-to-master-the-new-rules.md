Title: The cloud changes the game for cybersecurity incident response – here’s how to master the new rules
Date: 2021-08-17T14:00:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-08-17-the-cloud-changes-the-game-for-cybersecurity-incident-response-heres-how-to-master-the-new-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/17/long_live_cloud_forensics/){:target="_blank" rel="noopener"}

> The next chapter in digital forensics: Long live cloud forensics Promo On the face of it, the cloud fundamentally changes how security teams investigate and remediate incidents.... [...]
