Title: Nigerian Threat Actors Solicit Employees to Deploy Ransomware for Cut of Profits
Date: 2021-08-20T14:09:50+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: 2021-08-20-nigerian-threat-actors-solicit-employees-to-deploy-ransomware-for-cut-of-profits

[Source](https://threatpost.com/nigerian-solicits-employees-ransomware-profits/168849/){:target="_blank" rel="noopener"}

> Campaign emails company insiders and initially offers 1 million in Bitcoin if they install DemonWare on an organization’s network. [...]
