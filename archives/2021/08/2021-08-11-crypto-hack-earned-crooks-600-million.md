Title: Crypto Hack Earned Crooks $600 Million
Date: 2021-08-11T15:12:47+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cryptography;Hacks
Slug: 2021-08-11-crypto-hack-earned-crooks-600-million

[Source](https://threatpost.com/crypto-hack-600-million/168554/){:target="_blank" rel="noopener"}

> In one of the largest cryptocurrency hacks to date, cyberattackers reportedly stole millions from the decentralized finance (DeFi) platform Poly Network. [...]
