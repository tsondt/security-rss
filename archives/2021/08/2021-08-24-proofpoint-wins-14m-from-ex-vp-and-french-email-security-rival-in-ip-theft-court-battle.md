Title: Proofpoint wins $14m from ex-VP and French email security rival in IP theft court battle
Date: 2021-08-24T16:57:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-24-proofpoint-wins-14m-from-ex-vp-and-french-email-security-rival-in-ip-theft-court-battle

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/24/proofpoint_wins_14m_vade/){:target="_blank" rel="noopener"}

> Jury finds message-filtering tech misappropriation was 'wilful and malicious' Infosec firm Proofpoint has won $14m from a former vice president and his new employer after a jury found they had unlawfully used its trade secrets when he moved to the other company.... [...]
