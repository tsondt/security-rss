Title: Black Hat: Novel DNS Hack Spills Confidential Corp Data
Date: 2021-08-12T20:30:58+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cloud Security;Hacks;Vulnerabilities
Slug: 2021-08-12-black-hat-novel-dns-hack-spills-confidential-corp-data

[Source](https://threatpost.com/black-hat-novel-dns-hack/168636/){:target="_blank" rel="noopener"}

> Threatpost interviews Wiz CTO about a vulnerability recently patched by Amazon Route53's DNS service and Google Cloud DNS. [...]
