Title: Poly Network says it's got pretty much all of that $610m in stolen crypto-coins back
Date: 2021-08-23T22:37:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-08-23-poly-network-says-its-got-pretty-much-all-of-that-610m-in-stolen-crypto-coins-back

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/23/poly_network_payback/){:target="_blank" rel="noopener"}

> 'I'm quitting the show' says mystery thief Poly Network says virtually all of the crypto-currency funds, valued at $610m, stolen from it by a thief have been returned.... [...]
