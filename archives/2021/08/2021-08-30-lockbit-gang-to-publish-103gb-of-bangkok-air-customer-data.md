Title: LockBit Gang to Publish 103GB of Bangkok Air Customer Data
Date: 2021-08-30T15:14:21+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Cloud Security;Malware;Web Security
Slug: 2021-08-30-lockbit-gang-to-publish-103gb-of-bangkok-air-customer-data

[Source](https://threatpost.com/lockbit-bangkok-airways-breach/169019/){:target="_blank" rel="noopener"}

> The airline announced the breach on Thursday, and the ransomware gang started a countdown clock the next day. [...]
