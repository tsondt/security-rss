Title: SolarWinds urges US judge to toss out crap infosec sueball: We got pwned by actual Russia, give us a break
Date: 2021-08-04T15:34:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: solarwinds-urges-us-judge-to-toss-out-crap-infosec-sueball-we-got-pwned-by-actual-russia-give-us-a-break

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/04/solarwinds_lawsuit_shareholders_motion_dismiss/){:target="_blank" rel="noopener"}

> Company says it didn't skimp on security before everything went wrong SolarWinds is urging a US federal judge to throw out a lawsuit brought against it by aggrieved shareholders who say they were misled about its security posture in advance of the infamous Russian attack on the business.... [...]
