Title: Notorious AlphaBay darknet market comes back to life
Date: 2021-08-12T13:07:54-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-12-notorious-alphabay-darknet-market-comes-back-to-life

[Source](https://www.bleepingcomputer.com/news/security/notorious-alphabay-darknet-market-comes-back-to-life/){:target="_blank" rel="noopener"}

> The AlphaBay darkweb market has come back to life after an administrator of the original project relaunched it over the weekend. [...]
