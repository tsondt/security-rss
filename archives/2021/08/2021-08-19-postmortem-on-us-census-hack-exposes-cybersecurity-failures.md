Title: Postmortem on U.S. Census Hack Exposes Cybersecurity Failures
Date: 2021-08-19T14:35:49+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Vulnerabilities
Slug: 2021-08-19-postmortem-on-us-census-hack-exposes-cybersecurity-failures

[Source](https://threatpost.com/postmortem-on-u-s-census-hack-exposes-cybersecurity-failures/168814/){:target="_blank" rel="noopener"}

> Government says cybersecurity failures were many within failed January hack of U.S. Census Bureau systems. [...]
