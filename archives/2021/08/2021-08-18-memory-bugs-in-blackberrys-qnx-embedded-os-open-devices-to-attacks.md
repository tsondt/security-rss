Title: Memory Bugs in BlackBerry’s QNX Embedded OS Open Devices to Attacks
Date: 2021-08-18T14:30:51+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: IoT;Vulnerabilities
Slug: 2021-08-18-memory-bugs-in-blackberrys-qnx-embedded-os-open-devices-to-attacks

[Source](https://threatpost.com/blackberrys-qnx-devices-attacks/168772/){:target="_blank" rel="noopener"}

> The once-dominant handset maker BlackBerry is busy squashing BadAlloc bugs in its QNX real-time operating system used in cars in medical devices. [...]
