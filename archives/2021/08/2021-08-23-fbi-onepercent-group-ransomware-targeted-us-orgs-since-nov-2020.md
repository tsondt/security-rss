Title: FBI: OnePercent Group Ransomware targeted US orgs since Nov 2020
Date: 2021-08-23T18:17:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-23-fbi-onepercent-group-ransomware-targeted-us-orgs-since-nov-2020

[Source](https://www.bleepingcomputer.com/news/security/fbi-onepercent-group-ransomware-targeted-us-orgs-since-nov-2020/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) has shared info about a threat actor known as OnePercent Group that has been actively targeting US organizations in ransomware attacks since at least November 2020. [...]
