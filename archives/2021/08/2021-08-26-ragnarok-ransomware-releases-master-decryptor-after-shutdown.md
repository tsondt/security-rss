Title: Ragnarok ransomware releases master decryptor after shutdown
Date: 2021-08-26T18:36:35-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-26-ragnarok-ransomware-releases-master-decryptor-after-shutdown

[Source](https://www.bleepingcomputer.com/news/security/ragnarok-ransomware-releases-master-decryptor-after-shutdown/){:target="_blank" rel="noopener"}

> Ragnarok ransomware gang appears to have called it quits and released the master key that can decrypt files locked with their malware. [...]
