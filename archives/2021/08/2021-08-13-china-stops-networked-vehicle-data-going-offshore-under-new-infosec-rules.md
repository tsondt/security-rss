Title: China stops networked vehicle data going offshore under new infosec rules
Date: 2021-08-13T06:58:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-08-13-china-stops-networked-vehicle-data-going-offshore-under-new-infosec-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/13/china_networked_car_rules/){:target="_blank" rel="noopener"}

> Hands-off driving detectors required, over-the-air updates to be strictly regulated China has drafted new rules required of its autonomous and networked vehicle builders.... [...]
