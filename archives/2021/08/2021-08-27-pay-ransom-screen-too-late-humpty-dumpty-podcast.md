Title: ‘Pay Ransom’ Screen? Too Late, Humpty Dumpty – Podcast
Date: 2021-08-27T12:00:38+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;News;Podcasts;Vulnerabilities;Web Security
Slug: 2021-08-27-pay-ransom-screen-too-late-humpty-dumpty-podcast

[Source](https://threatpost.com/ransom-humpty-dumpty-podcast/168962/){:target="_blank" rel="noopener"}

> Splunk’s Ryan Kovar discusses the rise in supply-chain attacks a la Kaseya & how to get ahead of encryption leaving your business a pile of broken shells. [...]
