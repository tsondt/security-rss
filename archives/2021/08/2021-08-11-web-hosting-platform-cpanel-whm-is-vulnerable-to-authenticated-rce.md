Title: Web hosting platform cPanel &amp; WHM is vulnerable to authenticated RCE
Date: 2021-08-11T10:58:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-11-web-hosting-platform-cpanel-whm-is-vulnerable-to-authenticated-rce

[Source](https://portswigger.net/daily-swig/web-hosting-platform-cpanel-amp-whm-is-vulnerable-to-authenticated-rce){:target="_blank" rel="noopener"}

> Pen testers and vendor disagree over appropriate mitigations [...]
