Title: eCh0raix Ransomware Variant Targets QNAP, Synology NAS Devices
Date: 2021-08-10T17:22:28+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Malware;News;Vulnerabilities;Web Security
Slug: 2021-08-10-ech0raix-ransomware-variant-targets-qnap-synology-nas-devices

[Source](https://threatpost.com/ech0raix-ransomware-variant-qnap-synology-nas-devices/168516/){:target="_blank" rel="noopener"}

> Some bad actors are honing tools to go after small fry: This variant was refined to target not one, but two vendors’ devices that are common in SOHO setups. [...]
