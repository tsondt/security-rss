Title: Bangkok Airways hit by LockBit ransomware attack, loses lotsa data after refusing to pay
Date: 2021-08-31T05:15:04+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-08-31-bangkok-airways-hit-by-lockbit-ransomware-attack-loses-lotsa-data-after-refusing-to-pay

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/31/bangkok_airways_hit_by_lockbit/){:target="_blank" rel="noopener"}

> Partial credit card numbers appear and, worse still, passengers' meal preferences Bangkok Airways has revealed it was the victim of a cyberattack from ransomware group LockBit on August 23rd, resulting in the publishing of stolen data.... [...]
