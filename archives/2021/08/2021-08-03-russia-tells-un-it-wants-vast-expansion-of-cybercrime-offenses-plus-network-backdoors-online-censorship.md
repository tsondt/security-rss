Title: Russia tells UN it wants vast expansion of cybercrime offenses, plus network backdoors, online censorship
Date: 2021-08-03T20:15:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: russia-tells-un-it-wants-vast-expansion-of-cybercrime-offenses-plus-network-backdoors-online-censorship

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/03/russia_cybercrime_laws/){:target="_blank" rel="noopener"}

> And said entirely with a straight face, too Russia has put forward a draft convention to the United Nations ostensibly to fight cyber-crime.... [...]
