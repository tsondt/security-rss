Title: How to use ACM Private CA for enabling mTLS in AWS App Mesh
Date: 2021-08-31T17:23:23+00:00
Author: Raj Jain
Category: AWS Security
Tags: AWS App Mesh;AWS Certificate Manager;Expert (400);Security, Identity, & Compliance;ACM Private CA;AWS Lambda;certificates;mTLS;PKI;Security Blog;TLS;X.509
Slug: 2021-08-31-how-to-use-acm-private-ca-for-enabling-mtls-in-aws-app-mesh

[Source](https://aws.amazon.com/blogs/security/how-to-use-acm-private-ca-for-enabling-mtls-in-aws-app-mesh/){:target="_blank" rel="noopener"}

> Securing east-west traffic in service meshes, such as AWS App Mesh, by using mutual Transport Layer Security (mTLS) adds an additional layer of defense beyond perimeter control. mTLS adds bidirectional peer-to-peer authentication on top of the one-way authentication in normal TLS. This is done by adding a client-side certificate during the TLS handshake, through which [...]
