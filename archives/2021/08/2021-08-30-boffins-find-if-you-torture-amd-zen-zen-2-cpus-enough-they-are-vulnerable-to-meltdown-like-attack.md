Title: Boffins find if you torture AMD Zen+, Zen 2 CPUs enough, they are vulnerable to Meltdown-like attack
Date: 2021-08-30T21:49:58+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-30-boffins-find-if-you-torture-amd-zen-zen-2-cpus-enough-they-are-vulnerable-to-meltdown-like-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/30/amd_meltdown_zen/){:target="_blank" rel="noopener"}

> Chip biz's fix involves performance-inhibiting LFENCE, if warranted Computer scientists at TU Dresden in Germany have found that AMD's Zen processor family is vulnerable to a data-bothering Meltdown-like attack after all.... [...]
