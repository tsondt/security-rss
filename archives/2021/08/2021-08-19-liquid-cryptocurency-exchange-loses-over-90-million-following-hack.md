Title: Liquid cryptocurency exchange loses over $90 million following hack
Date: 2021-08-19T05:24:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-08-19-liquid-cryptocurency-exchange-loses-over-90-million-following-hack

[Source](https://www.bleepingcomputer.com/news/security/liquid-cryptocurency-exchange-loses-over-90-million-following-hack/){:target="_blank" rel="noopener"}

> Japan-based cryptocurrency exchange Liquid has suspended deposits and withdrawals after attackers have compromised its warm wallets. [...]
