Title: T-Mobile data breach just got worse — now at 54 million customers
Date: 2021-08-20T12:30:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-20-t-mobile-data-breach-just-got-worse-now-at-54-million-customers

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-data-breach-just-got-worse-now-at-54-million-customers/){:target="_blank" rel="noopener"}

> The T-Mobile data breach keeps getting worse as an update to their investigation now reveals that cyberattack exposed over 54 million individuals' data. [...]
