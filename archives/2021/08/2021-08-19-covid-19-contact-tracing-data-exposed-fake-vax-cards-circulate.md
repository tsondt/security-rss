Title: COVID-19 Contact-Tracing Data Exposed, Fake Vax Cards Circulate
Date: 2021-08-19T16:38:31+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Web Security
Slug: 2021-08-19-covid-19-contact-tracing-data-exposed-fake-vax-cards-circulate

[Source](https://threatpost.com/covid-contact-tracing-exposed-fake-vax-cards/168821/){:target="_blank" rel="noopener"}

> COVID-19-related exploitation and abuse is on the rise as vaccine data opens new frontiers for threat actors. [...]
