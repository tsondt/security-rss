Title: Steam security: Valve promptly resolves ‘unlimited funds’ gaming wallet cheat
Date: 2021-08-13T15:06:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-13-steam-security-valve-promptly-resolves-unlimited-funds-gaming-wallet-cheat

[Source](https://portswigger.net/daily-swig/steam-security-valve-promptly-resolves-unlimited-funds-gaming-wallet-cheat){:target="_blank" rel="noopener"}

> Security researcher earns $7,500 bug bounty after discovering business logic flaw [...]
