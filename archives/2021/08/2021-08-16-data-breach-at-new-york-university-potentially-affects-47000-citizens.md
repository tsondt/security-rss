Title: Data breach at New York university potentially affects 47,000 citizens
Date: 2021-08-16T12:54:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-16-data-breach-at-new-york-university-potentially-affects-47000-citizens

[Source](https://portswigger.net/daily-swig/data-breach-at-new-york-university-potentially-affects-47-000-citizens){:target="_blank" rel="noopener"}

> Thousands of Social Security numbers may have been compromised A data breach at a New York university has potentially exposed the personal information of nearly 47,000 individuals. The Research Founda [...]
