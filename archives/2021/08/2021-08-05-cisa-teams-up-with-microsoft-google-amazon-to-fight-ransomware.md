Title: CISA teams up with Microsoft, Google, Amazon to fight ransomware
Date: 2021-08-05T17:05:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-teams-up-with-microsoft-google-amazon-to-fight-ransomware

[Source](https://www.bleepingcomputer.com/news/security/cisa-teams-up-with-microsoft-google-amazon-to-fight-ransomware/){:target="_blank" rel="noopener"}

> CISA has announced the launch of Joint Cyber Defense Collaborative (JCDC), a partnership across public and private sectors focused on defending US critical infrastructure from ransomware and other cyber threats. [...]
