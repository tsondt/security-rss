Title: US Media, Retailers Targeted by New SparklingGoblin APT
Date: 2021-08-25T15:10:46+00:00
Author: Tom Spring
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: 2021-08-25-us-media-retailers-targeted-by-new-sparklinggoblin-apt

[Source](https://threatpost.com/sparklinggoblin-apt/168928/){:target="_blank" rel="noopener"}

> The new APT uses an undocumented backdoor to infiltrate the education, retail and government sectors. [...]
