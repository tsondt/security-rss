Title: Poly Network Recoups $610M Stolen from DeFi Platform
Date: 2021-08-24T19:35:41+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2021-08-24-poly-network-recoups-610m-stolen-from-defi-platform

[Source](https://threatpost.com/poly-network-recoups-610m-stolen-from-defi-platform/168906/){:target="_blank" rel="noopener"}

> The attacker returned the loot after being offered a gig as chief security advisor with Poly Network. [...]
