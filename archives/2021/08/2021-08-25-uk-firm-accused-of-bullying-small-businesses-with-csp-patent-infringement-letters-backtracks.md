Title: UK firm accused of bullying small businesses with CSP patent infringement letters backtracks
Date: 2021-08-25T16:10:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-25-uk-firm-accused-of-bullying-small-businesses-with-csp-patent-infringement-letters-backtracks

[Source](https://portswigger.net/daily-swig/uk-firm-accused-of-bullying-small-businesses-with-csp-patent-infringement-letters-backtracks){:target="_blank" rel="noopener"}

> Datawing disavows CSP nonce legal offensive [...]
