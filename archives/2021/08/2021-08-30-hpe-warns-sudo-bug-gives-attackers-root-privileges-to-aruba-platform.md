Title: HPE Warns Sudo Bug Gives Attackers Root Privileges to Aruba Platform
Date: 2021-08-30T21:46:56+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-08-30-hpe-warns-sudo-bug-gives-attackers-root-privileges-to-aruba-platform

[Source](https://threatpost.com/hpe-sudo-bug-aruba-platform/169038/){:target="_blank" rel="noopener"}

> HPE joins Apple in warning customers of a high-severity Sudo vulnerability. [...]
