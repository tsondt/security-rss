Title: Canada accepted 7,300 more immigration applications due to technical bug
Date: 2021-08-31T00:02:05-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Government;Technology
Slug: 2021-08-31-canada-accepted-7300-more-immigration-applications-due-to-technical-bug

[Source](https://www.bleepingcomputer.com/news/security/canada-accepted-7-300-more-immigration-applications-due-to-technical-bug/){:target="_blank" rel="noopener"}

> A bug in the Canadian immigration system led to the government accepting an additional 7,307 immigration applications, surpassing the imposed limit. This comprised files from international graduate stream applicants aspiring to change their temporary visa status to permanent residency. [...]
