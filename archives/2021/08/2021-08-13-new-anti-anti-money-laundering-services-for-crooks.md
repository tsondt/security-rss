Title: New Anti Anti-Money Laundering Services for Crooks
Date: 2021-08-13T17:28:21+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Web Fraud 2.0;AMLBot;Antinalysis;Elliptic;Incognito Market;Nick Bax;Tom Robinson
Slug: 2021-08-13-new-anti-anti-money-laundering-services-for-crooks

[Source](https://krebsonsecurity.com/2021/08/new-anti-anti-money-laundering-services-for-crooks/){:target="_blank" rel="noopener"}

> A new dark web service is marketing to cybercriminals who are curious to see how their various cryptocurrency holdings and transactions may be linked to known criminal activity. Dubbed “ Antinalysis,” the service purports to offer a glimpse into how one’s payment activity might be flagged by law enforcement agencies and private companies that try to link suspicious cryptocurrency transactions to real people. Sample provided by Antinalysis. “Worried about dirty funds in your BTC address? Come check out Antinalysis, the new address risk analyzer,” reads the service’s announcement, pointing to a link only accessible via ToR. “This service is dedicated [...]
