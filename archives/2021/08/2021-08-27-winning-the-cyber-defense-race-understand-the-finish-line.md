Title: Winning the Cyber-Defense Race: Understand the Finish Line
Date: 2021-08-27T20:16:34+00:00
Author: Kerry Matre
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Malware;Vulnerabilities;Web Security
Slug: 2021-08-27-winning-the-cyber-defense-race-understand-the-finish-line

[Source](https://threatpost.com/winning-cyber-defense-race/168996/){:target="_blank" rel="noopener"}

> Kerry Matre, Mandiant senior director, clears up misconceptions about the value to business for enterprise cyber-defense. Hint: It's not achieving visibility. [...]
