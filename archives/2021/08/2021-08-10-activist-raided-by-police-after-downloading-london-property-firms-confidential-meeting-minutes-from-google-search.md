Title: Activist raided by police after downloading London property firm's 'confidential' meeting minutes from Google Search
Date: 2021-08-10T10:30:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-10-activist-raided-by-police-after-downloading-london-property-firms-confidential-meeting-minutes-from-google-search

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/10/police_raid_man_for_downloading_google_search_docs/){:target="_blank" rel="noopener"}

> Someone must have broken in and taken docs, said Leathermarket Community Benefit Society A man who viewed documents online for a controversial London property development and shared them on social media was raided by police after developers claimed there had been a break-in to their systems.... [...]
