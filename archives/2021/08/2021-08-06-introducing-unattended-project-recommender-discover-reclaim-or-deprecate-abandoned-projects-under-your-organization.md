Title: Introducing Unattended Project Recommender: discover, reclaim, or deprecate abandoned projects under your organization
Date: 2021-08-06T16:00:00+00:00
Author: Bakh Inamov
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: introducing-unattended-project-recommender-discover-reclaim-or-deprecate-abandoned-projects-under-your-organization

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-launches-unattended-project-recommender/){:target="_blank" rel="noopener"}

> In fast-moving organizations, it's not uncommon for cloud resources, including entire projects, to occasionally be forgotten about. Not only such unattended resources can be difficult to identify, but they also tend to create a lot of headaches for product teams down the road, including unnecessary waste and security risks. To help you prune your idle cloud resources, we’re excited to introduce Unattended Project Recommender. It’s a new feature of Active Assist that provides you with a one-stop shop for discovering, reclaiming, and shutting down unattended projects. With actionable and automatic recommendations, you no longer have to worry about wasting money [...]
