Title: AT&T denies data breach after hacker auctions 70 million user database
Date: 2021-08-20T09:43:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-20-att-denies-data-breach-after-hacker-auctions-70-million-user-database

[Source](https://www.bleepingcomputer.com/news/security/atandt-denies-data-breach-after-hacker-auctions-70-million-user-database/){:target="_blank" rel="noopener"}

> AT&T says that they did not suffer a data breach after a well-known threat actor claimed to be selling a database containing the personal information of 70 million customers. [...]
