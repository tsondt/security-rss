Title: Crytek confirms Egregor ransomware attack, customer data theft
Date: 2021-08-10T15:45:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-10-crytek-confirms-egregor-ransomware-attack-customer-data-theft

[Source](https://www.bleepingcomputer.com/news/security/crytek-confirms-egregor-ransomware-attack-customer-data-theft/){:target="_blank" rel="noopener"}

> Game developer and publisher Crytek has confirmed that the Egregor ransomware gang breached its network in October 2020, encrypting systems and stealing files containing customers' personal info later leaked on the gang's dark web leak site. [...]
