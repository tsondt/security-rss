Title: Education giant Pearson fined $1M for downplaying data breach
Date: 2021-08-16T15:23:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-16-education-giant-pearson-fined-1m-for-downplaying-data-breach

[Source](https://www.bleepingcomputer.com/news/security/education-giant-pearson-fined-1m-for-downplaying-data-breach/){:target="_blank" rel="noopener"}

> The US Securities and Exchange Commission (SEC) announced today that Pearson, a British multinational educational publishing and services company, has settled charges of mishandling the disclosure process for a 2018 data breach discovered in March 2019. [...]
