Title: US officials, experts fear China ransacked Exchange servers for data to train AI systems
Date: 2021-08-31T19:23:05+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-08-31-us-officials-experts-fear-china-ransacked-exchange-servers-for-data-to-train-ai-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/31/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: T-Mobile US apologizes, security holes found in medical pumps, and more In brief The massive attack on Microsoft Exchange servers in March may have been China harvesting information to train AI systems, according to US government officials and computer-security experts who talked to NPR.... [...]
