Title: WooCommerce Pricing Plugin Allows Malicious Code-Injection
Date: 2021-08-31T16:12:26+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-08-31-woocommerce-pricing-plugin-allows-malicious-code-injection

[Source](https://threatpost.com/woocommerce-plugin-malicious/169063/){:target="_blank" rel="noopener"}

> The popular Dynamic Pricing and Discounts plugin from Envato can be exploited by unauthenticated attackers. [...]
