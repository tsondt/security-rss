Title: Firefox adds enhanced cookie clearing, HTTPS by default in private browsing
Date: 2021-08-10T09:54:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-10-firefox-adds-enhanced-cookie-clearing-https-by-default-in-private-browsing

[Source](https://www.bleepingcomputer.com/news/security/firefox-adds-enhanced-cookie-clearing-https-by-default-in-private-browsing/){:target="_blank" rel="noopener"}

> Mozilla says that, starting in Firefox 91 released today, users will be able to fully erase the browser history for all visited websites which prevents privacy violations due to "sneaky third-party cookies sticking around." [...]
