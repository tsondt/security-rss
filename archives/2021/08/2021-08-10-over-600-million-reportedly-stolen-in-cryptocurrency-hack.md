Title: Over $600 million reportedly stolen in cryptocurrency hack
Date: 2021-08-10T12:19:15-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-08-10-over-600-million-reportedly-stolen-in-cryptocurrency-hack

[Source](https://www.bleepingcomputer.com/news/security/over-600-million-reportedly-stolen-in-cryptocurrency-hack/){:target="_blank" rel="noopener"}

> Over $611 million have reportedly been stolen in one of the largest cryptocurrency hacks. Decentralized cross-chain protocol and network, Poly Network announced today that it was attacked with cryptocurrency assets having successfully been transferred into the attackers' wallets. [...]
