Title: Friday Squid Blogging: On Squid Brains
Date: 2021-08-20T21:18:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-08-20-friday-squid-blogging-on-squid-brains

[Source](https://www.schneier.com/blog/archives/2021/08/friday-squid-blogging-on-squid-brains.html){:target="_blank" rel="noopener"}

> Interesting National Geographic article. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
