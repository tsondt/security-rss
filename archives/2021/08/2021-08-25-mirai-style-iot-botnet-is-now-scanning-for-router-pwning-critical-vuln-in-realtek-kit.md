Title: Mirai-style IoT botnet is now scanning for router-pwning critical vuln in Realtek kit
Date: 2021-08-25T17:11:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-25-mirai-style-iot-botnet-is-now-scanning-for-router-pwning-critical-vuln-in-realtek-kit

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/25/mirai_botnet_critical_vuln_realtek_radware/){:target="_blank" rel="noopener"}

> Researchers warn of Dark.IoT's rapidly evolving nasty A denial-of-service vulnerability affecting SDKs for Realtek chipsets used in 65 vendors' IoT devices has been incorporated into a son-of-Mirai botnet, according to new research.... [...]
