Title: FBI, CISA: Ransomware attack risk increases on holidays, weekends
Date: 2021-08-31T13:52:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-31-fbi-cisa-ransomware-attack-risk-increases-on-holidays-weekends

[Source](https://www.bleepingcomputer.com/news/security/fbi-cisa-ransomware-attack-risk-increases-on-holidays-weekends/){:target="_blank" rel="noopener"}

> The FBI and CISA urged organizations not to let down their defenses against ransomware attacks during weekends or holidays to released a joint cybersecurity advisory issued earlier today. [...]
