Title: UK security chiefs issue guidance after hackers target ministers on WhatsApp
Date: 2021-08-13T12:18:44+00:00
Author: Rowena Mason Deputy political editor
Category: The Guardian
Tags: Data and computer security;Politics;Technology;Privacy;Hacking;WhatsApp;Digital media;Media;UK news
Slug: 2021-08-13-uk-security-chiefs-issue-guidance-after-hackers-target-ministers-on-whatsapp

[Source](https://www.theguardian.com/technology/2021/aug/13/uk-security-chiefs-issue-guidance-after-hackers-target-ministers-on-whatsapp){:target="_blank" rel="noopener"}

> Exclusive: civil service chief points to work to improve cybersecurity in response to Labour concerns Ministers and civil servants conducting “government by WhatsApp” have been exposed to hackers, leading to new advice from security chiefs about how to improve their privacy. The cabinet secretary, Simon Case, revealed that the Government Security Group had issued fresh guidance after Labour raised questions about ministers using their personal phones to conduct official business. Related: UK government admits ministers can use self-deleting messages Continue reading... [...]
