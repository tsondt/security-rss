Title: Critical Valve Bug Lets Gamers Add Unlimited Funds to Steam Wallets
Date: 2021-08-16T20:50:23+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cloud Security;Hacks;Vulnerabilities
Slug: 2021-08-16-critical-valve-bug-lets-gamers-add-unlimited-funds-to-steam-wallets

[Source](https://threatpost.com/valve-bug-unlimited-funds/168710/){:target="_blank" rel="noopener"}

> Valve plugs an API bug found in its Steam platform that that abused the Smart2Pay system to add unlimited funds to gamer digital wallets. [...]
