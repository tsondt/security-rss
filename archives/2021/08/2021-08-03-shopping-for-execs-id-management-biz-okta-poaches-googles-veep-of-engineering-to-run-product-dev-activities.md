Title: Shopping for execs: ID management biz Okta poaches Google's veep of engineering to run product dev activities
Date: 2021-08-03T11:19:40+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: shopping-for-execs-id-management-biz-okta-poaches-googles-veep-of-engineering-to-run-product-dev-activities

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/03/okta_hires_google_exec_sagnik_nandy/){:target="_blank" rel="noopener"}

> Head techie for Chocolate Factory's search ad biz departs Mountain View Identity-as-a-service slinger Okta has poached Google veep of engineering Sagnik Nandy to become its president and chief tech officer.... [...]
