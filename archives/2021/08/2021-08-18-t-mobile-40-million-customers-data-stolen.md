Title: T-Mobile: >40 Million Customers’ Data Stolen
Date: 2021-08-18T17:54:05+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;News;Web Security
Slug: 2021-08-18-t-mobile-40-million-customers-data-stolen

[Source](https://threatpost.com/t-mobile-40-million-customers-data-stolen/168778/){:target="_blank" rel="noopener"}

> Attackers stole tens of millions of current, former or prospective customers' personal data, the company confirmed. It's providing 2 years of free ID protection. [...]
