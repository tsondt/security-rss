Title: Golang Cryptomining Worm Offers 15% Speed Boost
Date: 2021-08-06T20:41:40+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: golang-cryptomining-worm-offers-15-speed-boost

[Source](https://threatpost.com/golang-cryptomining-worm-speed-boost/168456/){:target="_blank" rel="noopener"}

> The latest variants of the Monero-mining malware exploit known web server bugs and add efficiency to the mining process. [...]
