Title: South Korea to test grenade-launching drones
Date: 2021-08-06T06:30:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: south-korea-to-test-grenade-launching-drones

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/06/south_korea_grenade_toting_drones/){:target="_blank" rel="noopener"}

> Back on terra firma, ransomware rampage sees elevated security threat levels and giveaways to SMBs South Korea has this week announced two new weapons: grenade-launching drones for its military, and anti-ransomware software for businesses.... [...]
