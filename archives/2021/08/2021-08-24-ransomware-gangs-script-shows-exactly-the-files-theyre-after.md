Title: Ransomware gang's script shows exactly the files they're after
Date: 2021-08-24T14:16:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-24-ransomware-gangs-script-shows-exactly-the-files-theyre-after

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-script-shows-exactly-the-files-theyre-after/){:target="_blank" rel="noopener"}

> A PowerShell script used by the Pysa ransomware operation gives us a sneak peek at the types of data they attempt to steal during a cyberattack. [...]
