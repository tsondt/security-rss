Title: Whistleblowing security researchers deny ‘inappropriate access’ to Indiana Covid-19 survey data
Date: 2021-08-18T15:22:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-18-whistleblowing-security-researchers-deny-inappropriate-access-to-indiana-covid-19-survey-data

[Source](https://portswigger.net/daily-swig/whistleblowing-security-researchers-deny-inappropriate-access-to-indiana-covid-19-survey-data){:target="_blank" rel="noopener"}

> Midwestern state in data leak drama [...]
