Title: ProxyShell Attacks Pummel Unpatched Exchange Servers
Date: 2021-08-23T18:54:29+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Black Hat;Cloud Security;News;Vulnerabilities;Web Security
Slug: 2021-08-23-proxyshell-attacks-pummel-unpatched-exchange-servers

[Source](https://threatpost.com/proxyshell-attacks-unpatched-exchange-servers/168879/){:target="_blank" rel="noopener"}

> CISA is warning about a surge of ProxyShell attacks, as Huntress discovered 140 webshells launched against 1,900 unpatched Microsoft Exchange servers. [...]
