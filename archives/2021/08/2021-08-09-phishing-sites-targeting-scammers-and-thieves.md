Title: Phishing Sites Targeting Scammers and Thieves
Date: 2021-08-09T15:21:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Brian Billionaire;briansclub;Mitch;PinPays
Slug: phishing-sites-targeting-scammers-and-thieves

[Source](https://krebsonsecurity.com/2021/08/phishing-sites-targeting-scammers-and-thieves/){:target="_blank" rel="noopener"}

> I was preparing to knock off work for the week on a recent Friday evening when a curious and annoying email came in via the contact form on this site: “Hello I go by the username Nuclear27 on your site Briansclub[.]com,” wrote “ Mitch,” confusing me with the proprietor of perhaps the underground’s largest bazaar for stolen credit and identity data. “I made a deposit to my wallet on the site but nothing has shown up yet and I would like to know why.” The real BriansClub login page. Several things stood out in Mitch’s message. For starters, that is [...]
