Title: Pakistan government approves new cybersecurity policy, cybercrime agency
Date: 2021-08-05T10:33:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: pakistan-government-approves-new-cybersecurity-policy-cybercrime-agency

[Source](https://portswigger.net/daily-swig/pakistan-government-approves-new-cybersecurity-policy-cybercrime-agency){:target="_blank" rel="noopener"}

> New policy welcomed as much-needed improvement to ‘poorly implemented’ Prevention of Electronic Crime Act [...]
