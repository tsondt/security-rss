Title: Web hosting platform cPanel &amp; WHM is vulnerable to authenticated RCE&nbsp;and privilege escalation
Date: 2021-08-11T10:58:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-11-web-hosting-platform-cpanel-whm-is-vulnerable-to-authenticated-rce-and-privilege-escalation

[Source](https://portswigger.net/daily-swig/web-hosting-platform-cpanel-amp-whm-is-vulnerable-to-authenticated-rce-nbsp-and-privilege-escalation){:target="_blank" rel="noopener"}

> Pen testers and vendor disagree over appropriate mitigations [...]
