Title: ‘Glowworm’ Attack Turns Power Light Flickers into Audio
Date: 2021-08-09T21:06:30+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Hacks;Privacy;Vulnerabilities;Web Security
Slug: glowworm-attack-turns-power-light-flickers-into-audio

[Source](https://threatpost.com/glowworm-attack-light-flickers-audio/168501/){:target="_blank" rel="noopener"}

> Researchers have found an entirely new attack vector for eavesdropping on Zoom and other virtual meetings. [...]
