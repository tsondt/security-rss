Title: US and Singapore sign agreement to bolster cybersecurity across government agencies
Date: 2021-08-24T15:43:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-24-us-and-singapore-sign-agreement-to-bolster-cybersecurity-across-government-agencies

[Source](https://portswigger.net/daily-swig/us-and-singapore-sign-agreement-to-bolster-cybersecurity-across-government-agencies){:target="_blank" rel="noopener"}

> Memorandum of Understanding will strengthen knowledge sharing practices [...]
