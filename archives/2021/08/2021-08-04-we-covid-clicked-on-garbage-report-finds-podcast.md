Title: We COVID-Clicked on Garbage, Report Finds: Podcast
Date: 2021-08-04T04:00:06+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Podcasts;Vulnerabilities;Web Security
Slug: we-covid-clicked-on-garbage-report-finds-podcast

[Source](https://threatpost.com/we-covid-clicked-on-garbage-report-podcast/168340/){:target="_blank" rel="noopener"}

> Were we work-from-home clicking zombies? Steganography attacks snagged three out of eight recipients. Nasty CAPTCHAs suckered 50 times more clicks during 2020. [...]
