Title: How to automate forensic disk collection in AWS
Date: 2021-08-24T17:02:47+00:00
Author: Matt Duda
Category: AWS Security
Tags: Advanced (300);Amazon GuardDuty;AWS Security Hub;Security, Identity, & Compliance;cybersecurity;Incident response;security automation;Security Blog
Slug: 2021-08-24-how-to-automate-forensic-disk-collection-in-aws

[Source](https://aws.amazon.com/blogs/security/how-to-automate-forensic-disk-collection-in-aws/){:target="_blank" rel="noopener"}

> In this blog post you’ll learn about a hands-on solution you can use for automated disk collection across multiple AWS accounts. This solution will help your incident response team set up an automation workflow to capture the disk evidence they need to analyze to determine scope and impact of potential security incidents. This post includes [...]
