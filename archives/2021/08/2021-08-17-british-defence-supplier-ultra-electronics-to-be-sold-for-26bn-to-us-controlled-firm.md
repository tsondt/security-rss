Title: British defence supplier Ultra Electronics to be sold for £2.6bn to US-controlled firm
Date: 2021-08-17T12:25:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-17-british-defence-supplier-ultra-electronics-to-be-sold-for-26bn-to-us-controlled-firm

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/17/ultra_electronics_sold_2_6_bn/){:target="_blank" rel="noopener"}

> UK.gov shrugs at calls for national security intervention British defence tech specialist Ultra Electronics has been bought for £2.6bn by a US private equity firm, through a wholly owned UK subsidiary that was itself once a proud standalone business.... [...]
