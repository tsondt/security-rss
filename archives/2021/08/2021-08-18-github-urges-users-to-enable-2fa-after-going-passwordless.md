Title: GitHub urges users to enable 2FA after going passwordless
Date: 2021-08-18T14:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-18-github-urges-users-to-enable-2fa-after-going-passwordless

[Source](https://www.bleepingcomputer.com/news/security/github-urges-users-to-enable-2fa-after-going-passwordless/){:target="_blank" rel="noopener"}

> GitHub is urging its user base to toggle on two-factor authentication (2FA) after deprecating password-based authentication for Git operations. [...]
