Title: SAP Patches Nine Critical & High-Severity Bugs
Date: 2021-08-11T15:27:02+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Vulnerabilities;Web Security
Slug: 2021-08-11-sap-patches-nine-critical-high-severity-bugs

[Source](https://threatpost.com/sap-patches-critical-bugs/168558/){:target="_blank" rel="noopener"}

> Experts urged enterprises to patch fast: SAP vulnerabilities are being weaponized in a matter of hours. [...]
