Title: Microsoft Patch Tuesday, August 2021 Edition
Date: 2021-08-10T21:12:58+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;AskWoody.com;CVE-2021-26424;CVE-2021-34481;CVE-2021-34535;CVE-2021-36936;CVE-2021-36948;Dustin Childs;Immersive Labs;Kevin Breen;Patch Tuesday August 2021;PrintNightmare;sans internet storm center;Trend Micro Zero Day Initiative;Windows Update Medic
Slug: 2021-08-10-microsoft-patch-tuesday-august-2021-edition

[Source](https://krebsonsecurity.com/2021/08/microsoft-patch-tuesday-august-2021-edition/){:target="_blank" rel="noopener"}

> Microsoft today released software updates to plug at least 44 security vulnerabilities in its Windows operating systems and related products. The software giant warned that attackers already are pouncing on one of the flaws, which ironically enough involves an easy-to-exploit bug in the software component responsible for patching Windows 10 PCs and Windows Server 2019 machines. Microsoft said attackers have seized upon CVE-2021-36948, which is a weakness in the Windows Update Medic service. Update Medic is a new service that lets users repair Windows Update components from a damaged state so that the device can continue to receive updates. Redmond [...]
