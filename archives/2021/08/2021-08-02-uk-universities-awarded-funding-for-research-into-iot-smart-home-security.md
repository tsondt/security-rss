Title: UK universities awarded funding for research into IoT, smart home security
Date: 2021-08-02T10:58:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uk-universities-awarded-funding-for-research-into-iot-smart-home-security

[Source](https://portswigger.net/daily-swig/uk-universities-awarded-funding-for-research-into-iot-smart-home-security){:target="_blank" rel="noopener"}

> Academics say that smart technology is a ‘balancing act’, and that consumers need to be aware of the risks [...]
