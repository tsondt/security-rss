Title: PwnedPiper critical bug set impacts major hospitals in North America
Date: 2021-08-02T06:41:57-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: pwnedpiper-critical-bug-set-impacts-major-hospitals-in-north-america

[Source](https://www.bleepingcomputer.com/news/security/pwnedpiper-critical-bug-set-impacts-major-hospitals-in-north-america/){:target="_blank" rel="noopener"}

> Pneumatic tube system (PTS) stations used in thousands of hospitals worldwide are vulnerable to a set of nine critical security issues collectively referred to as PwnedPiper. [...]
