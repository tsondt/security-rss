Title: International cybercrime gang charged with Covid-19 business email compromise fraud
Date: 2021-08-11T14:08:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-11-international-cybercrime-gang-charged-with-covid-19-business-email-compromise-fraud

[Source](https://portswigger.net/daily-swig/international-gang-charged-with-covid-19-business-email-compromise-fraud){:target="_blank" rel="noopener"}

> Twenty-three nationals involved in €1 million scheme [...]
