Title: Cybercrime triathlete jailed for 11 years over trio of online scams
Date: 2021-08-26T18:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-26-cybercrime-triathlete-jailed-for-11-years-over-trio-of-online-scams

[Source](https://portswigger.net/daily-swig/cybercrime-triathlete-jailed-for-11-years-over-trio-of-online-scams){:target="_blank" rel="noopener"}

> Romanian national sent down after admitting vishing, phishing, and money laundering rackets [...]
