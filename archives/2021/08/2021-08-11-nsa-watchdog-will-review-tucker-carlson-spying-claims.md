Title: NSA Watchdog Will Review Tucker Carlson Spying Claims
Date: 2021-08-11T19:17:13+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Privacy
Slug: 2021-08-11-nsa-watchdog-will-review-tucker-carlson-spying-claims

[Source](https://threatpost.com/nsa-watchdog-review-tucker-carlson-spying/168590/){:target="_blank" rel="noopener"}

> Despite a lack of evidence, the National Security Agency will investigate whether the Fox host was illegally targeted. [...]
