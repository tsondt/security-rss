Title: Hacker gets 500K reward for returning stolen cryptocurrency
Date: 2021-08-23T15:30:59-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-23-hacker-gets-500k-reward-for-returning-stolen-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/security/hacker-gets-500k-reward-for-returning-stolen-cryptocurrency/){:target="_blank" rel="noopener"}

> The saga of what has been dubbed the biggest hack in the world of decentralized finance appears to be over as Poly Network recovered more than $610 million in cryptocurrency assets it lost two weeks ago and the hacker received a $500,000 bounty for returning the money. [...]
