Title: LockBit 2.0 Ransomware Proliferates Globally
Date: 2021-08-17T16:44:58+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Malware
Slug: 2021-08-17-lockbit-20-ransomware-proliferates-globally

[Source](https://threatpost.com/lockbit-ransomware-proliferates-globally/168746/){:target="_blank" rel="noopener"}

> Fresh attacks target companies' employees, promising millions of dollars in exchange for valid account credentials for initial access. [...]
