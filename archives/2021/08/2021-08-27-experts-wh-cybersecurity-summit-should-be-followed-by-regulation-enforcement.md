Title: Experts: WH Cybersecurity Summit Should Be Followed by Regulation, Enforcement
Date: 2021-08-27T20:35:05+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Critical Infrastructure;Government
Slug: 2021-08-27-experts-wh-cybersecurity-summit-should-be-followed-by-regulation-enforcement

[Source](https://threatpost.com/wh-cybersecurity-summit-regulation-enforcement/169002/){:target="_blank" rel="noopener"}

> Amazon, Google, Microsoft etc. making major commitments to shore up nation’s cyber-defenses just won't be enough, researchers say. [...]
