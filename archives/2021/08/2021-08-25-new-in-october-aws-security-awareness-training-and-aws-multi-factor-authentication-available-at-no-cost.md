Title: New in October: AWS Security Awareness Training and AWS Multi-factor Authentication available at no cost
Date: 2021-08-25T22:01:22+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements
Slug: 2021-08-25-new-in-october-aws-security-awareness-training-and-aws-multi-factor-authentication-available-at-no-cost

[Source](https://aws.amazon.com/blogs/security/amazon-security-awareness-training-and-aws-multi-factor-authentication-tokens-to-be-made-available-at-no-cost/){:target="_blank" rel="noopener"}

> You’ve often heard us talk about security being “Job Zero” at Amazon, and today I’m happy to announce two new initiatives that I think will provide quick security wins for customers. The first initiative is the public release of the training we’ve developed and used to ensure our employees are up to date on how [...]
