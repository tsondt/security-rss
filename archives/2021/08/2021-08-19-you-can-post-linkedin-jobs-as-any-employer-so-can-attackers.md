Title: You can post LinkedIn jobs as ANY employer — so can attackers
Date: 2021-08-19T12:52:06-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-08-19-you-can-post-linkedin-jobs-as-any-employer-so-can-attackers

[Source](https://www.bleepingcomputer.com/news/security/you-can-post-linkedin-jobs-as-any-employer-so-can-attackers/){:target="_blank" rel="noopener"}

> Anyone can create a job listing on the leading recruitment platform LinkedIn on behalf of any employer—no verification needed. And worse, the employer cannot easily take these down. [...]
