Title: One million stolen credit cards leaked to promote carding market
Date: 2021-08-09T18:19:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: one-million-stolen-credit-cards-leaked-to-promote-carding-market

[Source](https://www.bleepingcomputer.com/news/security/one-million-stolen-credit-cards-leaked-to-promote-carding-market/){:target="_blank" rel="noopener"}

> A threat actor is promoting a new criminal carding marketplace by releasing one million credit cards stolen between 2018 and 2019 on hacking forums. [...]
