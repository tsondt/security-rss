Title: Understanding Network Access in Windows AppContainers
Date: 2021-08-19T09:37:00-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2021-08-19-understanding-network-access-in-windows-appcontainers

[Source](https://googleprojectzero.blogspot.com/2021/08/understanding-network-access-windows-app.html){:target="_blank" rel="noopener"}

> Posted by James Forshaw, Project Zero Recently I've been delving into the inner workings of the Windows Firewall. This is interesting to me as it's used to enforce various restrictions such as whether AppContainer sandboxed applications can access the network. Being able to bypass network restrictions in AppContainer sandboxes is interesting as it expands the attack surface available to the application, such as being able to access services on localhost, as well as granting access to intranet resources in an Enterprise. I recently discovered a configuration issue with the Windows Firewall which allowed the restrictions to be bypassed and allowed [...]
