Title: Linux version of BlackMatter ransomware targets VMware ESXi servers
Date: 2021-08-05T17:32:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: linux-version-of-blackmatter-ransomware-targets-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-blackmatter-ransomware-targets-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> ​The BlackMatter gang has joined the ranks of ransomware operations to develop a Linux encryptor that targets VMware's ESXi virtual machine platform. [...]
