Title: Botnet targets hundreds of thousands of devices using Realtek SDK
Date: 2021-08-23T16:14:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-23-botnet-targets-hundreds-of-thousands-of-devices-using-realtek-sdk

[Source](https://www.bleepingcomputer.com/news/security/botnet-targets-hundreds-of-thousands-of-devices-using-realtek-sdk/){:target="_blank" rel="noopener"}

> A Mirai-based botnet now targets a critical vulnerability in the software SDK used by hundreds of thousands of Realtek-based devices, encompassing 200 models from at least 65 vendors, including Asus, Belkin, D-Link, Netgear, Tenda, ZTE, and Zyxel. [...]
