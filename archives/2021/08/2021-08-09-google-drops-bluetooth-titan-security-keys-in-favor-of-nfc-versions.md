Title: Google drops Bluetooth Titan Security Keys in favor of NFC versions
Date: 2021-08-09T14:25:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-drops-bluetooth-titan-security-keys-in-favor-of-nfc-versions

[Source](https://www.bleepingcomputer.com/news/security/google-drops-bluetooth-titan-security-keys-in-favor-of-nfc-versions/){:target="_blank" rel="noopener"}

> Google is discontinuing the Bluetooth Titan Security Key to focus on security keys with Near Field Communication (NFC) functionality. [...]
