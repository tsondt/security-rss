Title: Cisco: Firewall manager RCE bug is a zero-day, patch incoming
Date: 2021-08-06T13:16:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisco-firewall-manager-rce-bug-is-a-zero-day-patch-incoming

[Source](https://www.bleepingcomputer.com/news/security/cisco-firewall-manager-rce-bug-is-a-zero-day-patch-incoming/){:target="_blank" rel="noopener"}

> In a Thursday security advisory update, Cisco revealed that a remote code execution (RCE) vulnerability in the Adaptive Security Device Manager (ADSM) Launcher disclosed last month is a zero-day bug that has yet to receive a security update. [...]
