Title: Details of the Recent T-Mobile Breach
Date: 2021-08-27T13:37:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;breaches;cell phones;data breaches;hacking;T-Mobile
Slug: 2021-08-27-details-of-the-recent-t-mobile-breach

[Source](https://www.schneier.com/blog/archives/2021/08/details-of-the-recent-t-mobile-breach.html){:target="_blank" rel="noopener"}

> Seems that 47 million customers were affected. Surprising no one, T-Mobile had awful security. I’ve lost count of how many times T-Mobile has been hacked. [...]
