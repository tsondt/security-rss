Title: How Caribou Coffee uses reCAPTCHA Enterprise to create safe and frictionless digital experiences
Date: 2021-08-30T16:00:00+00:00
Author: Eric Caron
Category: GCP Security
Tags: Identity & Security
Slug: 2021-08-30-how-caribou-coffee-uses-recaptcha-enterprise-to-create-safe-and-frictionless-digital-experiences

[Source](https://cloud.google.com/blog/products/identity-security/caribou-coffee-secures-webpages-with-recaptcha-enterprise/){:target="_blank" rel="noopener"}

> Like most businesses, we are constantly evolving and innovating to keep pace with the digital world. Last year, many experiences we had with our customers shifted to being online. And while our virtual customer interactions increased, so did attempted attacks. We needed to secure our customers’ experiences while simultaneously not disrupting them. Today, we’re going to share with you why Caribou Coffee chose to use reCAPTCHA Enterprise to protect its most important web pages. We started using reCAPTCHA Enterprise to initially protect our Contact Us page, a space where customers could reach out to us with questions or feedback. We [...]
