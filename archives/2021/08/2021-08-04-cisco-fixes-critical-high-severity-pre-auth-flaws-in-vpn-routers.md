Title: Cisco fixes critical, high severity pre-auth flaws in VPN routers
Date: 2021-08-04T15:20:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisco-fixes-critical-high-severity-pre-auth-flaws-in-vpn-routers

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-critical-high-severity-pre-auth-flaws-in-vpn-routers/){:target="_blank" rel="noopener"}

> Cisco has addressed pre-auth security vulnerabilities impacting multiple Small Business VPN routers and allowing remote attackers to trigger a denial of service condition or execute commands and arbitrary code on vulnerable devices. [...]
