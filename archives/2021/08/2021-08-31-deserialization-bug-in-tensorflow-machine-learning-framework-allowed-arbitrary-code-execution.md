Title: Deserialization bug in TensorFlow machine learning framework allowed arbitrary code execution
Date: 2021-08-31T11:05:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-31-deserialization-bug-in-tensorflow-machine-learning-framework-allowed-arbitrary-code-execution

[Source](https://portswigger.net/daily-swig/deserialization-bug-in-tensorflow-machine-learning-framework-allowed-arbitrary-code-execution){:target="_blank" rel="noopener"}

> Developers revoke YAML support to protect against exploitation [...]
