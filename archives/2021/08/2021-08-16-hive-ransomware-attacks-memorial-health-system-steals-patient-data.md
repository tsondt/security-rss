Title: Hive ransomware attacks Memorial Health System, steals patient data
Date: 2021-08-16T19:06:36-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-16-hive-ransomware-attacks-memorial-health-system-steals-patient-data

[Source](https://www.bleepingcomputer.com/news/security/hive-ransomware-attacks-memorial-health-system-steals-patient-data/){:target="_blank" rel="noopener"}

> In what appears to be an attack from the Hive ransomware gang, computers of the non-profit Memorial Health System have been encrypted, forcing staff to work with paper charts. [...]
