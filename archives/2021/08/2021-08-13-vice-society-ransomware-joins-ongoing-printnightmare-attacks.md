Title: Vice Society ransomware joins ongoing PrintNightmare attacks
Date: 2021-08-13T05:42:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-13-vice-society-ransomware-joins-ongoing-printnightmare-attacks

[Source](https://www.bleepingcomputer.com/news/security/vice-society-ransomware-joins-ongoing-printnightmare-attacks/){:target="_blank" rel="noopener"}

> The Vice Society ransomware gang is now also actively exploiting Windows print spooler PrintNightmare vulnerability for lateral movement through their victims' networks. [...]
