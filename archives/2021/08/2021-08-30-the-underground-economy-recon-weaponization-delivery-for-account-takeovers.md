Title: The Underground Economy: Recon, Weaponization & Delivery for Account Takeovers
Date: 2021-08-30T19:44:54+00:00
Author: Tony Lauro
Category: Threatpost
Tags: Breach;InfoSec Insider;Web Security
Slug: 2021-08-30-the-underground-economy-recon-weaponization-delivery-for-account-takeovers

[Source](https://threatpost.com/underground-economy-account-takeovers/169032/){:target="_blank" rel="noopener"}

> In part one of a two-part series, Akamai's director of security technology and strategy, Tony Lauro, lays out what orgs need to know to defend against account takeover attacks. [...]
