Title: Chipotle Emails Serve Up Phishing Lures
Date: 2021-08-02T19:15:42+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Web Security
Slug: chipotle-emails-serve-up-phishing-lures

[Source](https://threatpost.com/chipotle-serves-up-lures/168279/){:target="_blank" rel="noopener"}

> Mass email distribution service compromise mirrors earlier Nobelium attacks. [...]
