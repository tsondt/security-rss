Title: Effective Threat-Hunting Queries in a Redacted World
Date: 2021-08-24T12:00:45+00:00
Author: Chad Anderson
Category: Threatpost
Tags: InfoSec Insider;Malware;Web Security
Slug: 2021-08-24-effective-threat-hunting-queries-in-a-redacted-world

[Source](https://threatpost.com/effective-threat-hunting-queries/168864/){:target="_blank" rel="noopener"}

> Chad Anderson, senior security researcher for DomainTools, demonstrates how seemingly disparate pieces of infrastructure information can form perfect fingerprints for tracking cyberattackers' infrastructure. [...]
