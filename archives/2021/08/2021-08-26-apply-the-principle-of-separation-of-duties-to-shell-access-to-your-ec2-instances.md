Title: Apply the principle of separation of duties to shell access to your EC2 instances
Date: 2021-08-26T19:11:44+00:00
Author: Vesselin Tzvetkov
Category: AWS Security
Tags: Advanced (300);Amazon EC2;AWS Systems Manager;Security, Identity, & Compliance;AWS Systems Manager Change Manager;AWS Systems Manager Session Manager;Interactive shell;Security Blog;Separation of duties
Slug: 2021-08-26-apply-the-principle-of-separation-of-duties-to-shell-access-to-your-ec2-instances

[Source](https://aws.amazon.com/blogs/security/apply-the-principle-of-separation-of-duties-to-shell-access-to-your-ec2-instances/){:target="_blank" rel="noopener"}

> In this blog post, we will show you how you can use AWS Systems Manager Change Manager to control access to Amazon Elastic Compute Cloud (Amazon EC2) instance interactive shell sessions, to enforce separation of duties. Separation of duties is a design principle where more than one person’s approval is required to conclude a critical [...]
