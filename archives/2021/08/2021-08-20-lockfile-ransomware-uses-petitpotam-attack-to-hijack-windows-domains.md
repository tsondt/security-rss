Title: LockFile ransomware uses PetitPotam attack to hijack Windows domains
Date: 2021-08-20T15:07:51-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-20-lockfile-ransomware-uses-petitpotam-attack-to-hijack-windows-domains

[Source](https://www.bleepingcomputer.com/news/security/lockfile-ransomware-uses-petitpotam-attack-to-hijack-windows-domains/){:target="_blank" rel="noopener"}

> At least one ransomware threat actor has started to leverage the recently discovered PetitPotam NTLM relay attack method to take over the Windows domain on various networks worldwide. [...]
