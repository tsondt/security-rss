Title: Pharmacist faces 120 years in prison for selling vaccination cards on eBay
Date: 2021-08-17T12:05:40-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-17-pharmacist-faces-120-years-in-prison-for-selling-vaccination-cards-on-ebay

[Source](https://www.bleepingcomputer.com/news/security/pharmacist-faces-120-years-in-prison-for-selling-vaccination-cards-on-ebay/){:target="_blank" rel="noopener"}

> An Illinois pharmacist arrested today faces 120 years in prison for allegedly selling dozens of authentic COVID-19 vaccination record cards issued by the Center for Disease Control and Prevention (CDC). [...]
