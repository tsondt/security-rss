Title: Fuzz Off: How to Shake Up Code to Get It Right – Podcast
Date: 2021-08-10T14:43:41+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Podcasts;Vulnerabilities;Web Security
Slug: 2021-08-10-fuzz-off-how-to-shake-up-code-to-get-it-right-podcast

[Source](https://threatpost.com/fuzz-off-how-to-shake-up-code-to-get-it-right-podcast/168487/){:target="_blank" rel="noopener"}

> Is fuzzing for the cybersec elite, or should it be accessible to all software developers? FuzzCon panelists say join the party as they share fuzzing wins & fails. [...]
