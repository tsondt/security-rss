Title: Microsoft: Evasive Office 365 phishing campaign active since July 2020
Date: 2021-08-12T14:14:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-08-12-microsoft-evasive-office-365-phishing-campaign-active-since-july-2020

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-evasive-office-365-phishing-campaign-active-since-july-2020/){:target="_blank" rel="noopener"}

> Microsoft says that a year-long and highly evasive spear-phishing campaign has targeted Office 365 customers in multiple waves of attacks starting with July 2020. [...]
