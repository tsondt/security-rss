Title: 38 million records exposed online—including contact-tracing info
Date: 2021-08-24T14:11:31+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;data leak;microsoft;Portal;Power App;privacy
Slug: 2021-08-24-38-million-records-exposed-onlineincluding-contact-tracing-info

[Source](https://arstechnica.com/?p=1789373){:target="_blank" rel="noopener"}

> Enlarge (credit: Jorg Greuel | Getty Images) More than a thousand web apps mistakenly exposed 38 million records on the open Internet, including data from a number of COVID-19 contact-tracing platforms, vaccination sign-ups, job application portals, and employee databases. The data included a range of sensitive information, from people’s phone numbers and home addresses to Social Security numbers and COVID-19 vaccination status. The incident affected major companies and organizations, including American Airlines, Ford, the transportation and logistics company J.B. Hunt, the Maryland Department of Health, the New York City Municipal Transportation Authority, and New York City public schools. And while [...]
