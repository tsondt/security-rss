Title: T-Mobile Data Breach
Date: 2021-08-19T11:17:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;crime;cybercrime;hacking;phishing
Slug: 2021-08-19-t-mobile-data-breach

[Source](https://www.schneier.com/blog/archives/2021/08/t-mobile-data-breach.html){:target="_blank" rel="noopener"}

> It’s a big one : As first reported by Motherboard on Sunday, someone on the dark web claims to have obtained the data of 100 million from T-Mobile’s servers and is selling a portion of it on an underground forum for 6 bitcoin, about $280,000. The trove includes not only names, phone numbers, and physical addresses but also more sensitive data like social security numbers, driver’s license information, and IMEI numbers, unique identifiers tied to each mobile device. Motherboard confirmed that samples of the data “contained accurate information on T-Mobile customers.” [...]
