Title: 38 million records exposed by misconfigured Microsoft Power Apps. Redmond's advice? RTFM
Date: 2021-08-23T20:17:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-23-38-million-records-exposed-by-misconfigured-microsoft-power-apps-redmonds-advice-rtfm

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/23/power_shell_records/){:target="_blank" rel="noopener"}

> Low-code platform comes with high expectations that folks understand security Forty-seven government entities and privacy companies, including Microsoft, exposed 38 million sensitive data records online by misconfiguring the Windows giant's Power Apps, a low-code service that promises an easy way to build professional applications.... [...]
