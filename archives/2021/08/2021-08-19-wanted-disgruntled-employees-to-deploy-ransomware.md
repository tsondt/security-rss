Title: Wanted: Disgruntled Employees to Deploy Ransomware
Date: 2021-08-19T16:27:48+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Abnormal Security;BEC;business email compromise;ceo fraud;Crane Hassold;fbi;Internet Crime Complaint Center;Locbit 2.0 ransomware
Slug: 2021-08-19-wanted-disgruntled-employees-to-deploy-ransomware

[Source](https://krebsonsecurity.com/2021/08/wanted-disgruntled-employees-to-deploy-ransomware/){:target="_blank" rel="noopener"}

> Criminal hackers will try almost anything to get inside a profitable enterprise and secure a million-dollar payday from a ransomware infection. Apparently now that includes emailing employees directly and asking them to unleash the malware inside their employer’s network in exchange for a percentage of any ransom amount paid by the victim company. Image: Abnormal Security. Crane Hassold, director of threat intelligence at Abnormal Security, described what happened after he adopted a fake persona and responded to the proposal in the screenshot above. It offered to pay him 40 percent of a million-dollar ransom demand if he agreed to launch [...]
