Title: The European Space Agency Launches Hackable Satellite
Date: 2021-08-02T11:46:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: the-european-space-agency-launches-hackable-satellite

[Source](https://www.schneier.com/blog/archives/2021/08/the-european-space-agency-launches-hackable-satellite.html){:target="_blank" rel="noopener"}

> Of course this is hackable: A sophisticated telecommunications satellite that can be completely repurposed while in space has launched. [...] Because the satellite can be reprogrammed in orbit, it can respond to changing demands during its lifetime. [...] The satellite can detect and characterise any rogue emissions, enabling it to respond dynamically to accidental interference or intentional jamming. We can assume strong encryption, and good key management. Still, seems like a juicy target for other governments. [...]
