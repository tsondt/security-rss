Title: FISMA's a fizzer, says Cisco, and calls on Congress to get cyber security policy right – pronto
Date: 2021-08-13T06:16:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-08-13-fismas-a-fizzer-says-cisco-and-calls-on-congress-to-get-cyber-security-policy-right-pronto

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/13/cisco_ciso_advisor_calls_on/){:target="_blank" rel="noopener"}

> Organizational structure, piecemeal approach and hiring practices all need to change, says Borg security bigwig A senior Chief Information Security Officer (CISO) advisor at Cisco has penned a commentary on the state of US cybersecurity frameworks, criticizing current government infosec and advocating for more autonomy for CISOs and a better understanding of the task at hand from those creating policies.... [...]
