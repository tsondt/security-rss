Title: $600m in cryptocurrencies swiped from Poly Network servers after security snafu
Date: 2021-08-10T20:51:56+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2021-08-10-600m-in-cryptocurrencies-swiped-from-poly-network-servers-after-security-snafu

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/10/poly_networks_theft/){:target="_blank" rel="noopener"}

> Here's the addresses you need to block Poly Network, a Chinese software biz that processes cryptocurrency transactions across different blockchain platforms, urged hackers to return $600m worth of stolen digital cash in what it called the “biggest [attack] in DeFi history.”... [...]
