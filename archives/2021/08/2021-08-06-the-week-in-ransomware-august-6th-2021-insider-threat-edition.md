Title: The Week in Ransomware - August 6th 2021 - Insider threat edition
Date: 2021-08-06T17:16:56-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-august-6th-2021-insider-threat-edition

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-august-6th-2021-insider-threat-edition/){:target="_blank" rel="noopener"}

> If there is one thing we learned this week, it's that not only are corporations vulnerable to insider threats but so are ransomware operations. [...]
