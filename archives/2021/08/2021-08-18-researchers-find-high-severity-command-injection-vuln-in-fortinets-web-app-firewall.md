Title: Researchers find high-severity command injection vuln in Fortinet's web app firewall
Date: 2021-08-18T16:38:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-18-researchers-find-high-severity-command-injection-vuln-in-fortinets-web-app-firewall

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/18/fortinet_fortiweb_flaw/){:target="_blank" rel="noopener"}

> Mitigation: Don't let randomers from the internet log in to your firewall Updated A command injection vulnerability exists in Fortinet's management interface for its FortiWeb web app firewall, according to infosec firm Rapid7.... [...]
