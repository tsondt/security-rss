Title: Black Hat: Let’s All Help Cyber-Immunize Each Other
Date: 2021-08-04T19:57:35+00:00
Author: Tom Spring
Category: Threatpost
Tags: Black Hat;Web Security
Slug: black-hat-lets-all-help-cyber-immunize-each-other

[Source](https://threatpost.com/black-hat-usa-2021-mitigating-cyber-and-covid/168361/){:target="_blank" rel="noopener"}

> We're selfish if we're only mitigating our own stuff, said Black Hat USA 2021 keynoter Jeff Moss. Let's be like doctors battling COVID and work for herd immunity. [...]
