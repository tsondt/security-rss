Title: Cloud CISO Perspectives: August 2021
Date: 2021-08-26T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-08-26-cloud-ciso-perspectives-august-2021

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-august-2021/){:target="_blank" rel="noopener"}

> We’re more than halfway through 2021 and cybersecurity continues to be one of the most pressing issues facing organizations around the globe. As a major cloud provider, we have the opportunity to help address these challenges by delivering high levels of security in the platforms and services we offer. This remains a top priority for Google Cloud today. In this month's post, I’ll recap how we are working with governments and enterprise customers to scale security defenses, share new product updates across Google Cloud’s security portfolio and provide industry highlights from our financial services and public sector organizations. Thoughts from [...]
