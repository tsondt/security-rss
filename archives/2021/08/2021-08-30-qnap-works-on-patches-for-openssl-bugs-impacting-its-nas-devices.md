Title: QNAP works on patches for OpenSSL bugs impacting its NAS devices
Date: 2021-08-30T14:21:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-30-qnap-works-on-patches-for-openssl-bugs-impacting-its-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-works-on-patches-for-openssl-bugs-impacting-its-nas-devices/){:target="_blank" rel="noopener"}

> Network-attached storage (NAS) maker QNAP is investigating and working on security updates to address remote code execution (RCE) and denial-of-service (DoS) vulnerabilities patched by OpenSSL last week. [...]
