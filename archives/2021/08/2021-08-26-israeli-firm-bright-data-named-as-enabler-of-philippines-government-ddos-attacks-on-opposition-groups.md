Title: Israeli firm Bright Data named as enabler of Philippines government DDOS attacks on opposition groups
Date: 2021-08-26T04:00:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-08-26-israeli-firm-bright-data-named-as-enabler-of-philippines-government-ddos-attacks-on-opposition-groups

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/26/qurium_bright_data_philippines_ddos/){:target="_blank" rel="noopener"}

> Bright denies all in this odd tale of a leaky VPN, creepy proxy networks, 8Chan, clouds hosting wonky workloads, and Swedish digital rights org Qurium Updated Swedish digital rights organisation Qurium has alleged that an Israeli company called Bright Data has helped the government of the Philippines to DDOS local human rights organisation Karapatan.... [...]
