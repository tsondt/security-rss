Title: Apple says its CSAM scan code can be verified by researchers. Corellium starts throwing out dollar bills
Date: 2021-08-17T22:10:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-17-apple-says-its-csam-scan-code-can-be-verified-by-researchers-corellium-starts-throwing-out-dollar-bills

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/17/corellium_apple_bounty/){:target="_blank" rel="noopener"}

> Here's $15,000 to make that third-party inspection happen, says Florida outfit Updated Last week, Apple essentially invited security researchers to probe its forthcoming technology that's supposed to help thwart the spread of known child sexual abuse material (CSAM).... [...]
