Title: Microsoft August 2021 Patch Tuesday fixes 3 zero-days, 44 flaws
Date: 2021-08-10T13:36:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-08-10-microsoft-august-2021-patch-tuesday-fixes-3-zero-days-44-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-august-2021-patch-tuesday-fixes-3-zero-days-44-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's August 2021 Patch Tuesday, and with it comes fixes for three zero-day vulnerabilities and a total of 44 flaws, so please be nice to your Windows admins as they scramble to installed patches. [...]
