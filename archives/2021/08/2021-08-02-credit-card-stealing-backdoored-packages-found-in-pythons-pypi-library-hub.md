Title: Credit-card-stealing, backdoored packages found in Python's PyPI library hub
Date: 2021-08-02T18:58:05+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: credit-card-stealing-backdoored-packages-found-in-pythons-pypi-library-hub

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/02/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: SolarWinds cyber-spies hit US prosecutors' email systems, and more In brief Malicious libraries capable of lifting credit card numbers and opening backdoors on infected machines have been found in PyPI, the official third-party software repository for Python.... [...]
