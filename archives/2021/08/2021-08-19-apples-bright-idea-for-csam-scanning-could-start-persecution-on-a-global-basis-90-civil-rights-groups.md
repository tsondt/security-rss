Title: Apple's bright idea for CSAM scanning could start 'persecution on a global basis' – 90+ civil rights groups
Date: 2021-08-19T19:22:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-19-apples-bright-idea-for-csam-scanning-could-start-persecution-on-a-global-basis-90-civil-rights-groups

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/19/apple_csam_condemned/){:target="_blank" rel="noopener"}

> Letter to Cook & Co warns image-probing tech could also harm kids More than ninety human rights groups from around the world have signed a letter condemning Apple's plans to scan devices for child sexual abuse material (CSAM) – and warned Cupertino could usher in "censorship, surveillance and persecution on a global basis."... [...]
