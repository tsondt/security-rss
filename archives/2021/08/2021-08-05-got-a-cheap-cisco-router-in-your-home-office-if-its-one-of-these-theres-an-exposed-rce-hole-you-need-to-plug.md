Title: Got a cheap Cisco router in your home office? If it's one of these, there's an exposed RCE hole you need to plug
Date: 2021-08-05T13:28:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: got-a-cheap-cisco-router-in-your-home-office-if-its-one-of-these-theres-an-exposed-rce-hole-you-need-to-plug

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/05/cisco_rv340_series_router_vulns_patched/){:target="_blank" rel="noopener"}

> Patches issued for two CVE-rated vulns Cisco has published patches for critical vulns affecting the web management interface for some of its Small Business Dual WAN Gigabit routers – including a 9.8-rated nasty.... [...]
