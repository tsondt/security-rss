Title: Phishing Costs Nearly Quadrupled Over 6 Years
Date: 2021-08-17T04:00:43+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-08-17-phishing-costs-nearly-quadrupled-over-6-years

[Source](https://threatpost.com/phishing-costs-quadrupled/168716/){:target="_blank" rel="noopener"}

> Lost productivity & mopping up after the costly attacks that follow phishing – BEC & ransomware in particular – eat up most costs, not payouts to crooks. [...]
