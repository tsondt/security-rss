Title: Defeating Microsoft’s Trusted Platform Module
Date: 2021-08-09T11:19:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: BitLocker;hacking;Microsoft
Slug: defeating-microsofts-trusted-platform-module

[Source](https://www.schneier.com/blog/archives/2021/08/defeating-microsofts-trusted-platform-module.html){:target="_blank" rel="noopener"}

> This is a really interesting story explaining how to defeat Microsoft’s TPM in 30 minutes — without having to solder anything to the motherboard. Researchers at the security consultancy Dolos Group, hired to test the security of one client’s network, received a new Lenovo computer preconfigured to use the standard security stack for the organization. They received no test credentials, configuration details, or other information about the machine. They were not only able to get into the BitLocker-encrypted computer, but then use the computer to get into the corporate network. It’s the “evil maid attack.” It requires physical access to [...]
