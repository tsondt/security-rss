Title: Worried your data protection strategy is dated? Don’t let a ransomware infection prove you right
Date: 2021-08-04T18:00:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: worried-your-data-protection-strategy-is-dated-dont-let-a-ransomware-infection-prove-you-right

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/04/what_is_your_ransomware_attack_strategy/){:target="_blank" rel="noopener"}

> Trust us – you need to tune into this Webcast Some say the best form of defense is offense. But when it comes to modern ransomware from cyber-crime orgs that are well-funded, possibly have state actor backing, and have your data under their control, just how offensive can you afford to be?... [...]
