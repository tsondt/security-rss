Title: 1M Stolen Credit Cards Hit Dark Web for Free
Date: 2021-08-10T13:47:59+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Hacks;Web Security
Slug: 2021-08-10-1m-stolen-credit-cards-hit-dark-web-for-free

[Source](https://threatpost.com/1m-stolen-credit-cards-dark-web/168514/){:target="_blank" rel="noopener"}

> A dump of hundreds of thousands of active accounts is aimed at promoting AllWorld.Cards, a recently launched cybercriminal site for selling payment credentials online. [...]
