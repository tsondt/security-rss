Title: $600m in cryptocurrencies swiped from Poly Network
Date: 2021-08-10T20:51:56+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2021-08-10-600m-in-cryptocurrencies-swiped-from-poly-network

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/10/poly_networks_cryptocurrency_theft/){:target="_blank" rel="noopener"}

> 'Any country will regard this as a major economic crime and you will be pursued' Poly Network, a Chinese biz that handles transactions across various blockchains, urged thieves to return $600m of digital cash stolen from it in what it called the “biggest [attack] in DeFi history.”... [...]
