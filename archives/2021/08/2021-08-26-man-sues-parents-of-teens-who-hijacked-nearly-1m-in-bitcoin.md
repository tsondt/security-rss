Title: Man Sues Parents of Teens Who Hijacked Nearly $1M in Bitcoin
Date: 2021-08-26T20:50:23+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Malware;News;Web Security
Slug: 2021-08-26-man-sues-parents-of-teens-who-hijacked-nearly-1m-in-bitcoin

[Source](https://threatpost.com/man-sues-parents-teens-hijacked-1m-bitcoin/168964/){:target="_blank" rel="noopener"}

> Now adults, the then-teens apparently used clipboard hijacking malware to steal Bitcoin. [...]
