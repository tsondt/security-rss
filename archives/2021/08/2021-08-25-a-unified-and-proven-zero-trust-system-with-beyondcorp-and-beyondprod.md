Title: A unified and proven Zero Trust system with BeyondCorp and BeyondProd
Date: 2021-08-25T21:30:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Identity & Security
Slug: 2021-08-25-a-unified-and-proven-zero-trust-system-with-beyondcorp-and-beyondprod

[Source](https://cloud.google.com/blog/products/identity-security/applying-zero-trust-to-user-access-and-production-services/){:target="_blank" rel="noopener"}

> One of the most used buzzwords in cybersecurity today is undoubtedly “Zero Trust.” It’s been used to describe a wide range of approaches and products, leading to a fair bit of confusion about the term itself and to what it actually means. Some attempts to explain or simplify zero trust assert that “zero trust means trust nothing” or “zero trust is about delivering secure access without a VPN.” This conventional wisdom is mostly incorrect and limiting. At the core of a Zero Trust approach is the idea that implicit trust in any single component of a complex, interconnected system can [...]
