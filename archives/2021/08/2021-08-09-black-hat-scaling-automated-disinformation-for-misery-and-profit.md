Title: Black Hat: Scaling Automated Disinformation for Misery and Profit
Date: 2021-08-09T19:41:45+00:00
Author: Tom Spring
Category: Threatpost
Tags: Black Hat;Government
Slug: black-hat-scaling-automated-disinformation-for-misery-and-profit

[Source](https://threatpost.com/scaling-automated-disinformation/168484/){:target="_blank" rel="noopener"}

> Researchers demonstrated the power deep neural networks enlisted to create a bot army with the firepower to shape public opinion and spark QAnon 2.0. [...]
