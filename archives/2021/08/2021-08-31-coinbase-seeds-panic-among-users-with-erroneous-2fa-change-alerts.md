Title: Coinbase seeds panic among users with erroneous 2FA change alerts
Date: 2021-08-31T12:02:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-08-31-coinbase-seeds-panic-among-users-with-erroneous-2fa-change-alerts

[Source](https://www.bleepingcomputer.com/news/security/coinbase-seeds-panic-among-users-with-erroneous-2fa-change-alerts/){:target="_blank" rel="noopener"}

> Coinbase, the world's second largest cryptocurrency exchange with approximately 68 million users from over 100 countries, has scared a significant amount of its users with erroneous 2FA warnings. [...]
