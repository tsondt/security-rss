Title: T-Mobile US probes claims of 100m stolen customer records up for sale on dark web
Date: 2021-08-16T19:22:45+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-08-16-t-mobile-us-probes-claims-of-100m-stolen-customer-records-up-for-sale-on-dark-web

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/16/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Signal expands auto-deleting messages, SIM-swap thief pleads guilty, and more In brief T-Mobile US is investigating claims that highly sensitive personal data of 100 million customers has been stolen and peddled via the dark web.... [...]
