Title: Microsoft revives deprecated RDCMan after fixing security flaw
Date: 2021-08-10T18:30:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-08-10-microsoft-revives-deprecated-rdcman-after-fixing-security-flaw

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-revives-deprecated-rdcman-after-fixing-security-flaw/){:target="_blank" rel="noopener"}

> Microsoft has revived the Remote Desktop Connection Manager (RDCMan) app that was deprecated last year due to an important severity information disclosure bug the company decided not to fix. [...]
