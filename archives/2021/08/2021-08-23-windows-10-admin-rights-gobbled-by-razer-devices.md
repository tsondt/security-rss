Title: Windows 10 Admin Rights Gobbled by Razer Devices
Date: 2021-08-23T15:58:04+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-08-23-windows-10-admin-rights-gobbled-by-razer-devices

[Source](https://threatpost.com/windows-10-admin-rights-razer-devices-mouse-peripherals/168855/){:target="_blank" rel="noopener"}

> So much for Windows 10's security: A zero-day in the device installer software grants admin rights just by plugging in a mouse or other compatible device. UPDATE: Microsoft is investigating. [...]
