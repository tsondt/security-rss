Title: Ransomware gang uses PrintNightmare to breach Windows servers
Date: 2021-08-12T05:03:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-08-12-ransomware-gang-uses-printnightmare-to-breach-windows-servers

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-uses-printnightmare-to-breach-windows-servers/){:target="_blank" rel="noopener"}

> Ransomware operators have added PrintNightmare exploits to their arsenal and are targeting Windows servers to deploy Magniber ransomware payloads. [...]
