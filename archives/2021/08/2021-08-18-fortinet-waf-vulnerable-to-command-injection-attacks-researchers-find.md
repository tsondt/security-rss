Title: Fortinet WAF vulnerable to command injection attacks, researchers find
Date: 2021-08-18T11:28:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-18-fortinet-waf-vulnerable-to-command-injection-attacks-researchers-find

[Source](https://portswigger.net/daily-swig/fortinet-waf-vulnerable-to-command-injection-attacks-researchers-find){:target="_blank" rel="noopener"}

> Patch for FortiWeb flaw due over the coming days [...]
