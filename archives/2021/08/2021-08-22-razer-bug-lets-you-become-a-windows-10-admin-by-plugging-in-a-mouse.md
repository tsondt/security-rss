Title: Razer bug lets you become a Windows 10 admin by plugging in a mouse
Date: 2021-08-22T12:40:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-08-22-razer-bug-lets-you-become-a-windows-10-admin-by-plugging-in-a-mouse

[Source](https://www.bleepingcomputer.com/news/security/razer-bug-lets-you-become-a-windows-10-admin-by-plugging-in-a-mouse/){:target="_blank" rel="noopener"}

> A Razer Synapse zero-day vulnerability has been disclosed on Twitter, allowing you to gain Windows admin privileges simply by plugging in a Razer mouse or keyboard. [...]
