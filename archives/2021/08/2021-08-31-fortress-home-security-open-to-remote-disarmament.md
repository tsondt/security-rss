Title: Fortress Home Security Open to Remote Disarmament
Date: 2021-08-31T20:35:18+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;IoT;Vulnerabilities;Web Security
Slug: 2021-08-31-fortress-home-security-open-to-remote-disarmament

[Source](https://threatpost.com/fortress-home-security-remote-disarmament/169069/){:target="_blank" rel="noopener"}

> A pair of unpatched security vulnerabilities can allow unauthenticated cyberattackers to turn off window, door and motion-sensor monitoring. [...]
