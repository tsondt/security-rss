Title: Diavol ransomware sample shows stronger connection to TrickBot gang
Date: 2021-08-18T07:52:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-18-diavol-ransomware-sample-shows-stronger-connection-to-trickbot-gang

[Source](https://www.bleepingcomputer.com/news/security/diavol-ransomware-sample-shows-stronger-connection-to-trickbot-gang/){:target="_blank" rel="noopener"}

> A new analysis of a Diavol ransomware sample shows a more clear connection with the gang behind the TrickBot botnet and the evolution of the malware. [...]
