Title: Access token security for microservice APIs on Amazon EKS
Date: 2021-08-19T18:35:19+00:00
Author: Timothy James Power
Category: AWS Security
Tags: Amazon Cognito;Amazon Elastic Kubernetes Service;Containers;Developer Tools;Intermediate (200);Security, Identity, & Compliance;Amazon EKS;microservices
Slug: 2021-08-19-access-token-security-for-microservice-apis-on-amazon-eks

[Source](https://aws.amazon.com/blogs/security/access-token-security-for-microservice-apis-on-amazon-eks/){:target="_blank" rel="noopener"}

> In this blog post, I demonstrate how to implement service-to-service authorization using OAuth 2.0 access tokens for microservice APIs hosted on Amazon Elastic Kubernetes Service (Amazon EKS). A common use case for OAuth 2.0 access tokens is to facilitate user authorization to a public facing application. Access tokens can also be used to identify and [...]
