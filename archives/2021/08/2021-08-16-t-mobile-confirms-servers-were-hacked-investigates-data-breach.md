Title: T-Mobile confirms servers were hacked, investigates data breach
Date: 2021-08-16T15:52:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2021-08-16-t-mobile-confirms-servers-were-hacked-investigates-data-breach

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-confirms-servers-were-hacked-investigates-data-breach/){:target="_blank" rel="noopener"}

> ​T-Mobile has confirmed that threat actors hacked their servers in a recent cyber attack but still investigate whether customer data was stolen. [...]
