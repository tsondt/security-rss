Title: Hackers siphon $600 million in digital tokens, crypto network says
Date: 2021-08-11T20:06:22+00:00
Author: Eric Bangeman
Category: Ars Technica
Tags: Biz & IT;Policy;black hat;cryptocurrency;hacking
Slug: 2021-08-11-hackers-siphon-600-million-in-digital-tokens-crypto-network-says

[Source](https://arstechnica.com/?p=1786591){:target="_blank" rel="noopener"}

> Enlarge (credit: RobertAx | Getty Images ) A decentralized financial network has claimed hackers absconded with about $600 million worth of cryptocurrencies in one of the largest heists to target the growing digital asset industry. Poly Network, which links some of the world’s most widely used digital ledgers, said on Tuesday that attackers had exploited a vulnerability in its system and taken thousands of crypto tokens. The attack would be one of the largest to date on a crypto venture, on a par with breaches of exchanges Coincheck and Mt. Gox. The alleged hack was a blow to supporters of [...]
