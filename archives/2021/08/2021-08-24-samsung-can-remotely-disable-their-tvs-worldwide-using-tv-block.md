Title: Samsung can remotely disable their TVs worldwide using TV Block
Date: 2021-08-24T16:28:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-24-samsung-can-remotely-disable-their-tvs-worldwide-using-tv-block

[Source](https://www.bleepingcomputer.com/news/security/samsung-can-remotely-disable-their-tvs-worldwide-using-tv-block/){:target="_blank" rel="noopener"}

> Samsung says that it can disable any of its Samsung TV sets remotely using TV Block, a feature built into all television products sold worldwide. [...]
