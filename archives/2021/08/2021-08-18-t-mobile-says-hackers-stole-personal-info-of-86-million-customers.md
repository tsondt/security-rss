Title: T-Mobile says hackers stole personal info of 8.6 million customers
Date: 2021-08-18T07:35:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-18-t-mobile-says-hackers-stole-personal-info-of-86-million-customers

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-says-hackers-stole-personal-info-of-86-million-customers/){:target="_blank" rel="noopener"}

> T-Mobile has confirmed that attackers who recently breached its servers stole files containing the personal information of over 8.6 million current customers. [...]
