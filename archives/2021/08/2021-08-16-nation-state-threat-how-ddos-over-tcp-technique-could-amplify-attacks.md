Title: Nation-state threat: How DDoS-over-TCP technique could amplify attacks
Date: 2021-08-16T16:22:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-16-nation-state-threat-how-ddos-over-tcp-technique-could-amplify-attacks

[Source](https://portswigger.net/daily-swig/nation-state-threat-how-ddos-over-tcp-technique-could-amplify-attacks){:target="_blank" rel="noopener"}

> Researchers detail novel way of increasing cyber-attack surface [...]
