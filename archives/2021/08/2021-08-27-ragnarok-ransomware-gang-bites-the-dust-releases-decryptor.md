Title: Ragnarok Ransomware Gang Bites the Dust, Releases Decryptor
Date: 2021-08-27T13:50:44+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cryptography;Malware
Slug: 2021-08-27-ragnarok-ransomware-gang-bites-the-dust-releases-decryptor

[Source](https://threatpost.com/ragnarok-releases-decryptor/168976/){:target="_blank" rel="noopener"}

> The cybercriminal group, active since late 2019, has closed its doors and released the key to unlocking victims’ files on its dark web portal. [...]
