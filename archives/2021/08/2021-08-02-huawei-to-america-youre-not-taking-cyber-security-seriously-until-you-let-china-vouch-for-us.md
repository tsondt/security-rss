Title: Huawei to America: You're not taking cyber-security seriously until you let China vouch for us
Date: 2021-08-02T06:15:03+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: huawei-to-america-youre-not-taking-cyber-security-seriously-until-you-let-china-vouch-for-us

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/02/huawei_security_advice_to_usa/){:target="_blank" rel="noopener"}

> Slams Biden's Executive Order on improving infosec, calls for multilateral trust framework Huawei has decided to school America on cyber-security, and its lesson is to co-operate with China so its vendors – including Huawei – can be trusted around the world.... [...]
