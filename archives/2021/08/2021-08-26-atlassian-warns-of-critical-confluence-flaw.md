Title: Atlassian warns of critical Confluence flaw
Date: 2021-08-26T06:00:40+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-08-26-atlassian-warns-of-critical-confluence-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/26/atlassian_critical_confluence_flaw/){:target="_blank" rel="noopener"}

> 9.8-rated bug allows arbitrary code execution – possibly without authentication Atlassian has warned users of its Confluence Server that they need to patch the product to remedy a Critical-rated flaw.... [...]
