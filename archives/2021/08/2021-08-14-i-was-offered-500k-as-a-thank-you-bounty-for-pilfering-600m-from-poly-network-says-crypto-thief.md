Title: I was offered $500k as a thank-you bounty for pilfering $600m from Poly Network, says crypto-thief
Date: 2021-08-14T10:03:07+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2021-08-14-i-was-offered-500k-as-a-thank-you-bounty-for-pilfering-600m-from-poly-network-says-crypto-thief

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/14/poly_network_payment/){:target="_blank" rel="noopener"}

> Blockchain exchange biz says it's working to have all the purloined assets returned The mysterious miscreant who exploited a software vulnerability in Poly Network to drain $600m in crypto-assets, claims the Chinese blockchain company offered them $500,000 as a reward for discovering the weakness.... [...]
