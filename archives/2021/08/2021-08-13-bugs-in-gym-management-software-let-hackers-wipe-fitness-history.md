Title: Bugs in gym management software let hackers wipe fitness history
Date: 2021-08-13T10:18:51-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-13-bugs-in-gym-management-software-let-hackers-wipe-fitness-history

[Source](https://www.bleepingcomputer.com/news/security/bugs-in-gym-management-software-let-hackers-wipe-fitness-history/){:target="_blank" rel="noopener"}

> Security researchers found vulnerabilities in the Wodify fitness platform that allows an attacker to view and modify user workouts from any of the more than 5,000 gyms that use the solution worldwide. [...]
