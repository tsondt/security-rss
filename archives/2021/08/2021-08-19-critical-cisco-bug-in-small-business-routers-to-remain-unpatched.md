Title: Critical Cisco Bug in Small Business Routers to Remain Unpatched
Date: 2021-08-19T20:34:42+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-08-19-critical-cisco-bug-in-small-business-routers-to-remain-unpatched

[Source](https://threatpost.com/critical-cisco-bug-routers-unpatched/168831/){:target="_blank" rel="noopener"}

> The issue affects a range of Cisco Wireless-N and Wireless-AC VPN routers that have reached end-of-life. [...]
