Title: SolarWinds 2.0 Could Ignite Financial Crisis – Podcast
Date: 2021-08-13T20:08:25+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Podcasts;Web Security
Slug: 2021-08-13-solarwinds-20-could-ignite-financial-crisis-podcast

[Source](https://threatpost.com/solarwinds-financial-crisis-podcast/168677/){:target="_blank" rel="noopener"}

> That’s what NY State suggests could happen, given the utter lack of cybersec protection at many private equity & hedge fund firms. Can AI help avert it? [...]
