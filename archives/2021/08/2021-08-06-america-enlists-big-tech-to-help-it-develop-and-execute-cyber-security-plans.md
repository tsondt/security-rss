Title: America enlists Big Tech to help it develop and execute cyber security plans
Date: 2021-08-06T03:15:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: america-enlists-big-tech-to-help-it-develop-and-execute-cyber-security-plans

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/06/cisa_convenes_joint_cyber_defense_collaborative/){:target="_blank" rel="noopener"}

> Players in ‘Joint Cyber Defense Collaborative’ include Microsoft, AWS, and Google The United States' Cybersecurity and Infrastructure Security Agency (CISA) has announced the "standup" of a body called the "Joint Cyber Defense Collaborative" (JCDC) that it hopes will spark ideas for new and improved national responses against electronic threats.... [...]
