Title: Black Hat: Security Bugs Allow Takeover of Capsule Hotel Rooms
Date: 2021-08-04T21:14:30+00:00
Author: Tara Seals
Category: Threatpost
Tags: Black Hat;IoT;Mobile Security;Vulnerabilities
Slug: black-hat-security-bugs-allow-takeover-of-capsule-hotel-rooms

[Source](https://threatpost.com/security-bugs-takeover-capsule-hotel/168376/){:target="_blank" rel="noopener"}

> A researcher was able to remotely control the lights, bed and ventilation in "smart" hotel rooms via Nasnos vulnerabilities. [...]
