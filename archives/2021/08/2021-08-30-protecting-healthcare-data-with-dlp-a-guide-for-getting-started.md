Title: Protecting Healthcare data with DLP: A guide for getting started
Date: 2021-08-30T16:00:00+00:00
Author: Nelly Wilson
Category: GCP Security
Tags: Google Cloud;Healthcare & Life Sciences;Identity & Security
Slug: 2021-08-30-protecting-healthcare-data-with-dlp-a-guide-for-getting-started

[Source](https://cloud.google.com/blog/products/identity-security/getting-started-with-dlp-for-healthcare-data/){:target="_blank" rel="noopener"}

> Protecting patient data is of paramount importance to any healthcare provider. This is not only because of the many laws and regulations in different countries around the world requiring this data to be safeguarded, but it is a foundational requirement for trust between a provider and their patient. This does create some tension between patients and their providers. To give patients the best care possible, a significant amount of Protected Health Information (PHI) is shared with providers who in turn share it with other providers, insurers, labs, etc. While sharing data can lead to better quality of care, it also [...]
