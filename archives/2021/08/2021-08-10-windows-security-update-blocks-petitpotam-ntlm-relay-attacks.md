Title: Windows security update blocks PetitPotam NTLM relay attacks
Date: 2021-08-10T15:28:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-08-10-windows-security-update-blocks-petitpotam-ntlm-relay-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-security-update-blocks-petitpotam-ntlm-relay-attacks/){:target="_blank" rel="noopener"}

> Microsoft has released security updates that block the PetitPotam NTLM relay attack that allows a threat actor to take over a Windows domain. [...]
