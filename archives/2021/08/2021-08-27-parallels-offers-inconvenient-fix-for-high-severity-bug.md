Title: Parallels Offers ‘Inconvenient’ Fix for High-Severity Bug
Date: 2021-08-27T20:54:13+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cloud Security;Malware;Vulnerabilities
Slug: 2021-08-27-parallels-offers-inconvenient-fix-for-high-severity-bug

[Source](https://threatpost.com/parallels-inconvenient-fix/168997/){:target="_blank" rel="noopener"}

> Firm offers guidance on how to mitigate a five-months-old privilege escalation bug impacting Parallels Desktop 16 for Mac and all previous versions. [...]
