Title: Bug Bounty Radar // The latest bug bounty programs for September 2021
Date: 2021-08-31T16:19:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-31-bug-bounty-radar-the-latest-bug-bounty-programs-for-september-2021

[Source](https://portswigger.net/daily-swig/bug-bounty-radar-the-latest-bug-bounty-programs-for-september-2021){:target="_blank" rel="noopener"}

> New web targets for the discerning hacker [...]
