Title: Facebook rolls out end-to-end encryption for Messenger calls
Date: 2021-08-13T16:20:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-13-facebook-rolls-out-end-to-end-encryption-for-messenger-calls

[Source](https://www.bleepingcomputer.com/news/security/facebook-rolls-out-end-to-end-encryption-for-messenger-calls/){:target="_blank" rel="noopener"}

> Facebook has announced the roll-out of end-to-end encrypted Messenger voice and video calls five years after making it available in one-on-one text chats. [...]
