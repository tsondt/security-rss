Title: SynAck ransomware decryptor lets victims recover files for free
Date: 2021-08-20T14:02:45-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-20-synack-ransomware-decryptor-lets-victims-recover-files-for-free

[Source](https://www.bleepingcomputer.com/news/security/synack-ransomware-decryptor-lets-victims-recover-files-for-free/){:target="_blank" rel="noopener"}

> Emsisoft has released a decryptor for the SynAck Ransomware, allowing victims to decrypt their encrypted files for free. [...]
