Title: US brokers warned of ongoing phishing attacks impersonating FINRA
Date: 2021-08-14T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-14-us-brokers-warned-of-ongoing-phishing-attacks-impersonating-finra

[Source](https://www.bleepingcomputer.com/news/security/us-brokers-warned-of-ongoing-phishing-attacks-impersonating-finra/){:target="_blank" rel="noopener"}

> The US Financial Industry Regulatory Authority (FINRA) warns US brokerage firms and brokers of an ongoing phishing campaign impersonating FINRA officials and asking them to hand over sensitive information under the threat of penalties. [...]
