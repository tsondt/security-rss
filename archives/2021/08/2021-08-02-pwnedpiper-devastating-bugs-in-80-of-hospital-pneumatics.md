Title: ‘PwnedPiper’: Devastating Bugs in >80% of Hospital Pneumatics
Date: 2021-08-02T20:58:54+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Podcasts;Vulnerabilities;Web Security
Slug: pwnedpiper-devastating-bugs-in-80-of-hospital-pneumatics

[Source](https://threatpost.com/pwnedpiper-bugs-hospital-pneumatics/168277/){:target="_blank" rel="noopener"}

> Podcast: Blood samples aren’t martinis. You can’t shake them. But bugs in pneumatic control systems could lead to that, RCE or ransomware. [...]
