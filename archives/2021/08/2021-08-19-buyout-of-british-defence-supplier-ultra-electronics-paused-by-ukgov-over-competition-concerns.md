Title: Buyout of British defence supplier Ultra Electronics paused by UK.gov over competition concerns
Date: 2021-08-19T13:54:03+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-19-buyout-of-british-defence-supplier-ultra-electronics-paused-by-ukgov-over-competition-concerns

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/19/ultra_electronics_buyout_paused_dcms/){:target="_blank" rel="noopener"}

> Regulator send out one more ping The British government has intervened in the US buyout of defence supplier Ultra Electronics, temporarily halting the acquisition and prohibiting any tech transfer overseas.... [...]
