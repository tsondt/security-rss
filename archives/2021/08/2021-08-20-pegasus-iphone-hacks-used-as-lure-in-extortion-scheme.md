Title: Pegasus iPhone hacks used as lure in extortion scheme
Date: 2021-08-20T11:06:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-20-pegasus-iphone-hacks-used-as-lure-in-extortion-scheme

[Source](https://www.bleepingcomputer.com/news/security/pegasus-iphone-hacks-used-as-lure-in-extortion-scheme/){:target="_blank" rel="noopener"}

> A new extortion scam is underway that attempts to capitalize on the recent Pegasus iOS spyware attacks to scare people into paying a blackmail demand. [...]
