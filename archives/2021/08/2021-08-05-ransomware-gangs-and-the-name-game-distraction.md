Title: Ransomware Gangs and the Name Game Distraction
Date: 2021-08-05T11:38:48+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;Babuk ransomware;BitPaymer;BlackMatter ransomware;Cerber;CrowdStrike;Cryptolocker;DarkSide ransomware;DoppelPaymer;Dridex;Emsisoft;Evgeniy Mikhailovich Bogachev;Evil Corp.;Fabian Wosar;Gameover ZeuS;GandCrab;Grief ransomware;Indrik Spider;Mark Arena;REvil ransomware;TA505;The Business Club;WastedLocker;zeus
Slug: ransomware-gangs-and-the-name-game-distraction

[Source](https://krebsonsecurity.com/2021/08/ransomware-gangs-and-the-name-game-distraction/){:target="_blank" rel="noopener"}

> It’s nice when ransomware gangs have their bitcoin stolen, malware servers shut down, or are otherwise forced to disband. We hang on to these occasional victories because history tells us that most ransomware moneymaking collectives don’t go away so much as reinvent themselves under a new name, with new rules, targets and weaponry. Indeed, some of the most destructive and costly ransomware groups are now in their third incarnation. A rough timeline of major ransomware operations and their reputed links over time. Reinvention is a basic survival skill in the cybercrime business. Among the oldest tricks in the book is [...]
