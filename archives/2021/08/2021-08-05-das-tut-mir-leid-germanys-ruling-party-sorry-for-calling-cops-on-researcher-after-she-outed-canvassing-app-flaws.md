Title: Das tut mir leid! Germany's ruling party sorry for calling cops on researcher after she outed canvassing app flaws
Date: 2021-08-05T10:31:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: das-tut-mir-leid-germanys-ruling-party-sorry-for-calling-cops-on-researcher-after-she-outed-canvassing-app-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/05/germany_responsible_disclosure_cdu/){:target="_blank" rel="noopener"}

> Party denies naming activist to police but apologises anyway A "left-wing" German infosec researcher was this week threatened with criminal prosecution after revealing that an app used by Angela Merkel's political party to canvass voters was secretly collecting personal data.... [...]
