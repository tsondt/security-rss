Title: Microsoft Exchange ProxyToken bug can let hackers steal user email
Date: 2021-08-30T12:28:32-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-30-microsoft-exchange-proxytoken-bug-can-let-hackers-steal-user-email

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-proxytoken-bug-can-let-hackers-steal-user-email/){:target="_blank" rel="noopener"}

> Technical details have emerged on a serious vulnerability in Microsoft Exchange Server dubbed ProxyToken that does not require authentication to access emails from a target account. [...]
