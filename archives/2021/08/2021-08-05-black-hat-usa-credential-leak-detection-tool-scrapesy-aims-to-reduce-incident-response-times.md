Title: Black Hat USA: Credential leak detection tool Scrapesy aims to reduce incident response times
Date: 2021-08-05T13:26:48+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: black-hat-usa-credential-leak-detection-tool-scrapesy-aims-to-reduce-incident-response-times

[Source](https://portswigger.net/daily-swig/black-hat-usa-credential-leak-detection-tool-scrapesy-aims-to-reduce-incident-response){:target="_blank" rel="noopener"}

> Dual-purpose hacking tool was demonstrated at the Arsenal track of the security conference this week [...]
