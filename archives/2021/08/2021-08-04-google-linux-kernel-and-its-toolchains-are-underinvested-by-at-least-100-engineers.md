Title: Google: Linux kernel and its toolchains are underinvested by at least 100 engineers
Date: 2021-08-04T12:29:09+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: google-linux-kernel-and-its-toolchains-are-underinvested-by-at-least-100-engineers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/04/google_linux_kernel_security/){:target="_blank" rel="noopener"}

> Security not good enough, claims Chocolate Factory engineer Google's open security team has claimed the Linux kernel code is not good enough, with nearly 100 new fixes every week, and that at least 100 more engineers are needed to work on it.... [...]
