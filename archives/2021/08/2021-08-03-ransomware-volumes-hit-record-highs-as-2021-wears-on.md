Title: Ransomware Volumes Hit Record Highs as 2021 Wears On
Date: 2021-08-03T20:00:31+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Most Recent ThreatLists
Slug: ransomware-volumes-hit-record-highs-as-2021-wears-on

[Source](https://threatpost.com/ransomware-volumes-record-highs-2021/168327/){:target="_blank" rel="noopener"}

> The second quarter of the year saw the highest volumes of ransomware attacks ever, with Ryuk leading the way. [...]
