Title: What’s Next for T-Mobile and Its Customers? – Podcast
Date: 2021-08-19T22:06:26+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Cloud Security;Mobile Security;Podcasts
Slug: 2021-08-19-whats-next-for-t-mobile-and-its-customers-podcast

[Source](https://threatpost.com/whats-next-for-t-mobile-and-its-customers-podcast/168813/){:target="_blank" rel="noopener"}

> Hopefully not a hacked-up hairball of a “no can do” message when customers rush to change their PINs. In this episode: Corporate resilience vs. the opposite. [...]
