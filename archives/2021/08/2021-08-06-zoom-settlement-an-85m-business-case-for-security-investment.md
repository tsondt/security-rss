Title: Zoom Settlement: An $85M Business Case for Security Investment
Date: 2021-08-06T15:01:49+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Cryptography;Mobile Security;Vulnerabilities;Web Security
Slug: zoom-settlement-an-85m-business-case-for-security-investment

[Source](https://threatpost.com/zoom-settlement-85m-security-investment/168445/){:target="_blank" rel="noopener"}

> Zoom’s security lesson over end-to-end encryption shows the costs of playing cybersecurity catchup. [...]
