Title: Bug in Millions of Flawed IoT Devices Lets Attackers Eavesdrop
Date: 2021-08-17T16:20:30+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;IoT;Vulnerabilities;Web Security
Slug: 2021-08-17-bug-in-millions-of-flawed-iot-devices-lets-attackers-eavesdrop

[Source](https://threatpost.com/bug-iot-millions-devices-attackers-eavesdrop/168729/){:target="_blank" rel="noopener"}

> A remote attacker could exploit a critical vulnerability to eavesdrop on live audio & video or take control. The bug is in ThroughTek’s Kalay network, used in 83m devices. [...]
