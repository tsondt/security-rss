Title: Microsoft wonders if disabling just-in-time compilation of JavaScript improves browser security
Date: 2021-08-06T05:30:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: microsoft-wonders-if-disabling-just-in-time-compilation-of-javascript-improves-browser-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/06/edge_super_duper_security_mode/){:target="_blank" rel="noopener"}

> Edge is getting a 'Super Duper Security Mode' to test the idea. Yes, that is the actual name Microsoft is conducting an experiment it hopes will improve browser security – by making its Edge offering worse at running JavaScript... [...]
