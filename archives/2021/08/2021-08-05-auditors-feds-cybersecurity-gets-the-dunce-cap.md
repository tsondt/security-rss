Title: Auditors: Feds’ Cybersecurity Gets the Dunce Cap
Date: 2021-08-05T21:54:16+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;News;Vulnerabilities;Web Security
Slug: auditors-feds-cybersecurity-gets-the-dunce-cap

[Source](https://threatpost.com/auditors-feds-poor-cybersecurity-dunce-cap/168418/){:target="_blank" rel="noopener"}

> Out of eight agencies, four were given D grades in a report for the Senate, while the Feds overall got a C-. [...]
