Title: Manage data exfiltration risks in Cloud Run with VPC Service Controls
Date: 2021-08-20T16:00:00+00:00
Author: Karolina Netolicka
Category: GCP Security
Tags: Identity & Security;Networking;Google Cloud;Serverless
Slug: 2021-08-20-manage-data-exfiltration-risks-in-cloud-run-with-vpc-service-controls

[Source](https://cloud.google.com/blog/products/serverless/cloud-run-gets-enterprise-grade-network-security-with-vpc-sc/){:target="_blank" rel="noopener"}

> Enterprises looking to take advantage of the scalability and ease-of-use associated with cloud technology have often turned to serverless computing architectures. In these systems, a cloud provider allocates resources on-demand as required by a particular workload, and abstracts much of the management of an application or system for a customer. But to the most security-minded enterprises, a serverless architecture can sometimes be confusing due to the black box nature of the security of a fully-managed cloud deployment. An understanding of the underlying security systems within a serverless offering can alleviate those concerns. Many cloud services include identity and access management [...]
