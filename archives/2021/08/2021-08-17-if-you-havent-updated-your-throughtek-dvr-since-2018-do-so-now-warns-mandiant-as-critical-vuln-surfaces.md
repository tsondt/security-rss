Title: If you haven't updated your ThroughTek DVR since 2018 do so now, warns Mandiant as critical vuln surfaces
Date: 2021-08-17T15:02:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-17-if-you-havent-updated-your-throughtek-dvr-since-2018-do-so-now-warns-mandiant-as-critical-vuln-surfaces

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/17/throughtek_dvr_kalay_protocol_vuln_mandiant_warning/){:target="_blank" rel="noopener"}

> Callooh! Kalay! Outdated SDK component poses threat, says intel firm A critical vulnerability affecting tens of millions of digital video recorders powering baby monitors and CCTV systems across the world has been uncovered by Mandiant, which claims the vuln allows for unauthorised viewing of live camera footage.... [...]
