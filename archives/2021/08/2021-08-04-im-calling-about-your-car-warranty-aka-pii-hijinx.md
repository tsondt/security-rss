Title: ‘I’m Calling About Your Car Warranty’, aka PII Hijinx
Date: 2021-08-04T21:34:27+00:00
Author: Threatpost
Category: Threatpost
Tags: Black Hat;News;Privacy
Slug: im-calling-about-your-car-warranty-aka-pii-hijinx

[Source](https://threatpost.com/im-calling-about-your-car-warranty-aka-pii-hijinx/168375/){:target="_blank" rel="noopener"}

> Black Hat: Researchers created 300 fake identities, signed them up on 185 legit sites, then tracked how much the sites used signup PII to pester the accounts. [...]
