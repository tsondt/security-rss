Title: Managing Privileged Access to Secure the Post-COVID Perimeter
Date: 2021-08-23T14:18:30+00:00
Author: Joseph Carson
Category: Threatpost
Tags: Cloud Security;Hacks;InfoSec Insider;Mobile Security;Web Security
Slug: 2021-08-23-managing-privileged-access-to-secure-the-post-covid-perimeter

[Source](https://threatpost.com/privileged-access-covid-security/168860/){:target="_blank" rel="noopener"}

> Joseph Carson, chief security scientist & advisory CISO at ThycoticCentrify, discusses how to implement advanced privileged-access practices. [...]
