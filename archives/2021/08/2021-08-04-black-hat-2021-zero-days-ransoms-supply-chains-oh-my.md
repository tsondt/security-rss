Title: Black Hat 2021: Zero-days, ransoms, supply chains, oh my!
Date: 2021-08-04T19:06:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: black-hat-2021-zero-days-ransoms-supply-chains-oh-my

[Source](https://portswigger.net/daily-swig/black-hat-2021-zero-days-ransoms-supply-chains-oh-my){:target="_blank" rel="noopener"}

> Stolen zero-days are fueling ‘out-of-control’ supply chain attack problem, warns former cyber-spy [...]
