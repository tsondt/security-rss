Title: What is GDPR and why does the UK want to reshape its data laws?
Date: 2021-08-26T12:47:57+00:00
Author: Alex Hern Technology editor
Category: The Guardian
Tags: GDPR;Data protection;Technology;Data and computer security;UK news;Politics
Slug: 2021-08-26-what-is-gdpr-and-why-does-the-uk-want-to-reshape-its-data-laws

[Source](https://www.theguardian.com/technology/2021/aug/26/what-gdpr-why-does-uk-want-reshape-data-laws){:target="_blank" rel="noopener"}

> The government says an overhaul will boost growth and increase trade – but it must be careful not to go too far The government has announced plans to reshape the UK’s data laws such as GDPR requirements in an effort, it claims, to boost growth and increase trade post-Brexit. The digital, media and culture secretary, Oliver Dowden, says the UK wants to shape data laws based on “common sense, not box-ticking”. Continue reading... [...]
