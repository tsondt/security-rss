Title: Fake Apple rep amasses 620,000+ stolen iCloud pics, vids in hunt for images of nude women to trade
Date: 2021-08-24T21:37:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-24-fake-apple-rep-amasses-620000-stolen-icloud-pics-vids-in-hunt-for-images-of-nude-women-to-trade

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/24/los_angeles_county_man_pretended/){:target="_blank" rel="noopener"}

> Scumbag spent years tricking victims into handing over login details A California man this month admitted he hoarded hundreds of thousands of photos and videos stolen from strangers' Apple iCloud accounts to find and share images of nude young women.... [...]
