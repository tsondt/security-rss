Title: CISA warns admins to urgently patch Exchange ProxyShell bugs
Date: 2021-08-23T10:49:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-08-23-cisa-warns-admins-to-urgently-patch-exchange-proxyshell-bugs

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-admins-to-urgently-patch-exchange-proxyshell-bugs/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) issued its first alert tagged as "urgent," warning admins to patch on-premises Microsoft Exchange servers against actively exploited ProxyShell vulnerabilities. [...]
