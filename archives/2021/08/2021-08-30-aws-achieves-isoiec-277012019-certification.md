Title: AWS achieves ISO/IEC 27701:2019 certification
Date: 2021-08-30T19:16:16+00:00
Author: Anastasia Strebkova
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;27701;AWS ISO;AWS ISO Certificates;AWS ISO27701;AWS Privacy Management System
Slug: 2021-08-30-aws-achieves-isoiec-277012019-certification

[Source](https://aws.amazon.com/blogs/security/aws-achieves-iso-iec-27701-2019-certification/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has achieved ISO/IEC 27701:2019 certification with no findings. This certification is a rigorous third-party independent assessment of the Privacy Information Management System (PIMS) of a cloud service provider. ISO/IEC 27701:2019 specifies requirements and guidelines to establish and continuously improve a PIMS, including processing of Personally Identifiable [...]
