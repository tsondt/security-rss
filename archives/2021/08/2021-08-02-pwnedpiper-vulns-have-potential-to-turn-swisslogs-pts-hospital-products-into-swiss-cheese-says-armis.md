Title: PwnedPiper vulns have potential to turn Swisslog's PTS hospital products into Swiss cheese, says Armis
Date: 2021-08-02T11:36:06+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: pwnedpiper-vulns-have-potential-to-turn-swisslogs-pts-hospital-products-into-swiss-cheese-says-armis

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/02/pwnedpiper_swisslog_pts/){:target="_blank" rel="noopener"}

> Hardcoded passwords, unencrypted connections and unauthenticated firmware updates... patches released Security specialist Armis has discovered vulnerabilities, collectively dubbed PwnedPiper, in pneumatic tube control systems used in thousands of hospitals worldwide – including 80 per cent of the major hospitals found in the US.... [...]
