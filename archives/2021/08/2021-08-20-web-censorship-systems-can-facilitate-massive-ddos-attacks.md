Title: Web Censorship Systems Can Facilitate Massive DDoS Attacks
Date: 2021-08-20T21:11:16+00:00
Author: Tom Spring
Category: Threatpost
Tags: Government;Vulnerabilities
Slug: 2021-08-20-web-censorship-systems-can-facilitate-massive-ddos-attacks

[Source](https://threatpost.com/censorship-systems-ddos-attacks/168853/){:target="_blank" rel="noopener"}

> Systems are ripe for abuse by attackers who can abuse systems to launch DDoS attacks. [...]
