Title: Microsoft Teams will alert users of incoming spam calls
Date: 2021-08-13T14:02:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-08-13-microsoft-teams-will-alert-users-of-incoming-spam-calls

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-teams-will-alert-users-of-incoming-spam-calls/){:target="_blank" rel="noopener"}

> Microsoft is working on adding a spam call notification feature to the Microsoft 365 Teams collaboration platform. [...]
