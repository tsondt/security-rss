Title: Thief hands back at least a third of $600m in crypto-coins stolen from Poly Network
Date: 2021-08-11T23:18:04+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2021-08-11-thief-hands-back-at-least-a-third-of-600m-in-crypto-coins-stolen-from-poly-network

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/11/poly_network_funds_returned/){:target="_blank" rel="noopener"}

> It was just a prank, bro Whoever drained roughly $600m in cryptocurrencies from Poly Network is said to have returned at least $260m so far.... [...]
