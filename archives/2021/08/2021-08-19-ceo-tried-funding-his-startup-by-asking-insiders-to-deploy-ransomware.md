Title: CEO tried funding his startup by asking insiders to deploy ransomware
Date: 2021-08-19T15:32:48-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-19-ceo-tried-funding-his-startup-by-asking-insiders-to-deploy-ransomware

[Source](https://www.bleepingcomputer.com/news/security/ceo-tried-funding-his-startup-by-asking-insiders-to-deploy-ransomware/){:target="_blank" rel="noopener"}

> Likely inspired by the LockBit ransomware gang, a Nigerian threat actor tried their luck with a $1 million payment lure to recruit an insider to detonate a ransomware payload on the company servers. [...]
