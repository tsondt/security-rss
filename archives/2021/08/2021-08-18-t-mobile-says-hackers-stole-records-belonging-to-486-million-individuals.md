Title: T-Mobile says hackers stole records belonging to 48.6 million individuals
Date: 2021-08-18T07:35:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-18-t-mobile-says-hackers-stole-records-belonging-to-486-million-individuals

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-says-hackers-stole-records-belonging-to-486-million-individuals/){:target="_blank" rel="noopener"}

> T-Mobile has confirmed that attackers who recently breached its servers stole files containing the personal information of tens of millions of individuals. [...]
