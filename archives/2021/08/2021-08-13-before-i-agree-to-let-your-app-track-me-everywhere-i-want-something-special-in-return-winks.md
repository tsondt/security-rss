Title: Before I agree to let your app track me everywhere, I want something 'special' in return (winks)…
Date: 2021-08-13T11:35:12+00:00
Author: Alistair Dabbs
Category: The Register
Tags: 
Slug: 2021-08-13-before-i-agree-to-let-your-app-track-me-everywhere-i-want-something-special-in-return-winks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/13/something_for_the_weekend/){:target="_blank" rel="noopener"}

> Help me, officer, I’m lost! 'No problem, sir, you’re right in front of me' Something for the Weekend, Sir? "This website is requesting permission to access your location. Yes/No?" Absolutely not. My personal details are sacred!... [...]
