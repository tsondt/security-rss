Title: Singaporean telco StarHub discloses data leak affecting 57,000 customers
Date: 2021-08-09T15:47:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: singaporean-telco-starhub-discloses-data-leak-affecting-57000-customers

[Source](https://portswigger.net/daily-swig/singaporean-telco-starhub-discloses-data-leak-affecting-57-000-customers){:target="_blank" rel="noopener"}

> Company says data breach unrelated to previous system compromise [...]
