Title: Accenture confirms hack after LockBit ransomware data leak threats
Date: 2021-08-11T12:22:06-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-08-11-accenture-confirms-hack-after-lockbit-ransomware-data-leak-threats

[Source](https://www.bleepingcomputer.com/news/security/accenture-confirms-hack-after-lockbit-ransomware-data-leak-threats/){:target="_blank" rel="noopener"}

> Accenture, a global IT consultancy giant has likely been hit by a ransomware cyberattack. The ransomware group LockBit is threatening to publish data on its leak site within hours, as seen by BleepingComputer. [...]
