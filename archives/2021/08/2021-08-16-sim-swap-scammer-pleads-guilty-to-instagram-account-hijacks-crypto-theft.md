Title: SIM swap scammer pleads guilty to Instagram account hijacks, crypto theft
Date: 2021-08-16T11:27:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-16-sim-swap-scammer-pleads-guilty-to-instagram-account-hijacks-crypto-theft

[Source](https://www.bleepingcomputer.com/news/security/sim-swap-scammer-pleads-guilty-to-instagram-account-hijacks-crypto-theft/){:target="_blank" rel="noopener"}

> Declan Harrington, a Massachusetts man charged two years ago for his alleged involvement in a series of SIM swapping attacks, pleaded guilty to stealing cryptocurrency from multiple victims and hijacking the Instagram account of others. [...]
