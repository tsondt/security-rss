Title: QNAP Is Latest to Get Dinged by OpenSSL Bugs Fallout
Date: 2021-08-31T15:08:46+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-08-31-qnap-is-latest-to-get-dinged-by-openssl-bugs-fallout

[Source](https://threatpost.com/qnap-openssl-bugs/169054/){:target="_blank" rel="noopener"}

> The NAS maker issued two security advisories about the RCE and DoS flaws, adding to a flurry of advisories from the vast array of companies whose products use OpenSSL. [...]
