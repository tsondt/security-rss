Title: Chase bank accidentally leaked customer info to other customers
Date: 2021-08-17T01:37:05-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-08-17-chase-bank-accidentally-leaked-customer-info-to-other-customers

[Source](https://www.bleepingcomputer.com/news/security/chase-bank-accidentally-leaked-customer-info-to-other-customers/){:target="_blank" rel="noopener"}

> Chase Bank has admitted to the presence of a technical bug on its online banking website and app that allowed accidental leakage of customer banking information to other customers. [...]
