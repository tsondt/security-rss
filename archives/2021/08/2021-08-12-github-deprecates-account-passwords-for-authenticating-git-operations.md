Title: GitHub deprecates account passwords for authenticating Git operations
Date: 2021-08-12T18:10:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-12-github-deprecates-account-passwords-for-authenticating-git-operations

[Source](https://www.bleepingcomputer.com/news/security/github-deprecates-account-passwords-for-authenticating-git-operations/){:target="_blank" rel="noopener"}

> GitHub has announced today that account passwords will no longer be accepted for authenticating Git operations starting tomorrow. [...]
