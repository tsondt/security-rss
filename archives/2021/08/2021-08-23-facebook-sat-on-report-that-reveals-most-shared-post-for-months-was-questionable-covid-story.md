Title: Facebook sat on report that reveals most-shared post for months was questionable COVID story
Date: 2021-08-23T03:31:44+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-08-23-facebook-sat-on-report-that-reveals-most-shared-post-for-months-was-questionable-covid-story

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/23/facebook_widely_viewed_content_report/){:target="_blank" rel="noopener"}

> Then published its successor and claimed that was its debut effort Facebook has revealed a report that shows the most-shared link on the platform in the first three months of 2021 described questionable interpretation of a death attributed to a COVID-19 vaccination – but only did so after publishing a later and more flattering report.... [...]
