Title: Boffins propose Pretty Good Phone Privacy to end pretty invasive location data harvesting by telcos
Date: 2021-08-11T00:06:58+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-11-boffins-propose-pretty-good-phone-privacy-to-end-pretty-invasive-location-data-harvesting-by-telcos

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/11/phone_location_masking/){:target="_blank" rel="noopener"}

> Ready to go for telcos, but what's their incentive to lose all that lovely money? Computer science boffins have devised a way to prevent the location of mobile phone users from being snarfed and sold to marketers, though the technique won't affect targeted nation-state surveillance.... [...]
