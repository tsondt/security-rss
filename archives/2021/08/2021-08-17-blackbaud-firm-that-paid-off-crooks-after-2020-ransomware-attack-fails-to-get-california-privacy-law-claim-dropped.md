Title: Blackbaud – firm that paid off crooks after 2020 ransomware attack – fails to get California privacy law claim dropped
Date: 2021-08-17T11:34:08+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2021-08-17-blackbaud-firm-that-paid-off-crooks-after-2020-ransomware-attack-fails-to-get-california-privacy-law-claim-dropped

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/17/ccpa_blackbaud/){:target="_blank" rel="noopener"}

> Suit could net $750 a pop under GDPR-ish rule for complainants who allege info 'unencrypted' A judge in South Carolina has struck out a number of claims in a consolidated class-action suit alleging cloud CRM provider Blackbaud didn't do enough to prevent a 2020 ransomware attack, but allegations under California's Consumer Privacy Act (CCPA) will move forward.... [...]
