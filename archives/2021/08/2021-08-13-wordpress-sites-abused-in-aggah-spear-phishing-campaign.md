Title: WordPress Sites Abused in Aggah Spear-Phishing Campaign
Date: 2021-08-13T13:31:51+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;News;Vulnerabilities;Web Security
Slug: 2021-08-13-wordpress-sites-abused-in-aggah-spear-phishing-campaign

[Source](https://threatpost.com/aggah-wordpress-spearphishing/168657/){:target="_blank" rel="noopener"}

> The Pakistan-linked threat group's campaign uses compromised WordPress sites to deliver the Warzone RAT to manufacturing companies in Taiwan and South Korea. [...]
