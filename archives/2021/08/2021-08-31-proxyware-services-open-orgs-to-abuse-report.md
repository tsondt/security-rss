Title: Proxyware Services Open Orgs to Abuse – Report
Date: 2021-08-31T20:12:46+00:00
Author: Tom Spring
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-08-31-proxyware-services-open-orgs-to-abuse-report

[Source](https://threatpost.com/abuse-of-proxyware-services/169068/){:target="_blank" rel="noopener"}

> Services that let consumers resell their bandwidth for money are ripe for abuse, researchers warn. [...]
