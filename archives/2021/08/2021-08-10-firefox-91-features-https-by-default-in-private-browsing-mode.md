Title: Firefox 91 features HTTPS by default in private browsing mode
Date: 2021-08-10T15:02:48+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-10-firefox-91-features-https-by-default-in-private-browsing-mode

[Source](https://portswigger.net/daily-swig/firefox-91-features-https-by-default-in-private-browsing-mode){:target="_blank" rel="noopener"}

> Web browser will go further to protect against cyber-attacks [...]
