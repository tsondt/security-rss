Title: Data of three million elderly citizens exposed in cloud security oversight
Date: 2021-08-10T11:25:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-of-three-million-elderly-citizens-exposed-in-cloud-security-oversight

[Source](https://portswigger.net/daily-swig/data-of-three-million-elderly-citizens-exposed-in-cloud-security-oversight){:target="_blank" rel="noopener"}

> SeniorAdvisor has now secured the leaky bucket [...]
