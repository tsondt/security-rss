Title: SynAck ransomware releases decryption keys after El_Cometa rebrand
Date: 2021-08-13T11:20:23-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-13-synack-ransomware-releases-decryption-keys-after-el_cometa-rebrand

[Source](https://www.bleepingcomputer.com/news/security/synack-ransomware-releases-decryption-keys-after-el-cometa-rebrand/){:target="_blank" rel="noopener"}

> The SynAck ransomware gang released the master decryption keys for their operation after rebranding as the new El_Cometa group. [...]
