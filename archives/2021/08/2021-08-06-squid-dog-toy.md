Title: Squid Dog Toy
Date: 2021-08-06T21:05:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: squid-dog-toy

[Source](https://www.schneier.com/blog/archives/2021/08/squid-dog-toy.html){:target="_blank" rel="noopener"}

> It’s sold out, but the pictures are cute. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
