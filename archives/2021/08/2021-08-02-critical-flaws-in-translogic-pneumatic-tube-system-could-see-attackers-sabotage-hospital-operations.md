Title: Critical flaws in TransLogic Pneumatic Tube System could see attackers sabotage&nbsp;hospital operations
Date: 2021-08-02T14:34:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: critical-flaws-in-translogic-pneumatic-tube-system-could-see-attackers-sabotage-hospital-operations

[Source](https://portswigger.net/daily-swig/critical-flaws-in-translogic-pneumatic-tube-system-could-see-attackers-sabotage-nbsp-hospital-operations){:target="_blank" rel="noopener"}

> Vendor Swisslog urges more than 3,000 hospitals worldwide to apply patch ASAP [...]
