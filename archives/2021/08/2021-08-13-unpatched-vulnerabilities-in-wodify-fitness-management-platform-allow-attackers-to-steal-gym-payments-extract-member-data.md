Title: ‘Unpatched’ vulnerabilities in Wodify fitness management platform allow attackers to steal gym payments, extract member data
Date: 2021-08-13T12:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-13-unpatched-vulnerabilities-in-wodify-fitness-management-platform-allow-attackers-to-steal-gym-payments-extract-member-data

[Source](https://portswigger.net/daily-swig/unpatched-vulnerabilities-in-wodify-fitness-management-platform-allow-attackers-to-steal-gym-payments-extract-member-data){:target="_blank" rel="noopener"}

> Personal trainers urged to exercise caution over alleged security flaws [...]
