Title: Pegasus Spyware Uses iPhone Zero-Click iMessage Zero-Day
Date: 2021-08-24T17:51:56+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Vulnerabilities;Web Security
Slug: 2021-08-24-pegasus-spyware-uses-iphone-zero-click-imessage-zero-day

[Source](https://threatpost.com/pegasus-spyware-uses-iphone-zero-click-imessage-zero-day/168899/){:target="_blank" rel="noopener"}

> Cybersecurity watchdog Citizen Lab saw the new zero-day FORCEDENTRY exploit successfully deployed against iOS versions 14.4 & 14.6, blowing past Apple's new BlastDoor sandboxing feature to install spyware on the iPhones of Bahraini activists – even one living in London at the time. [...]
