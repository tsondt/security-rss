Title: Pakistan's tax office services go dark after migration project goes awry
Date: 2021-08-17T03:00:01+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-08-17-pakistans-tax-office-services-go-dark-after-migration-project-goes-awry

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/17/pakistan_federal_board_of_revenue_outage/){:target="_blank" rel="noopener"}

> Local reports suggest Microsoft Hyper-V crack was the cause, as rumours swirl of data leak Pakistan's Federal Board of Revenue – the nation's tax office – has experienced a lengthy outage after a migration project went bad, perhaps as the result of a cyber-attack.... [...]
