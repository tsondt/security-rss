Title: Microsoft Exchange ‘ProxyToken’ Bug Allows Email Snooping
Date: 2021-08-30T17:31:06+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-08-30-microsoft-exchange-proxytoken-bug-allows-email-snooping

[Source](https://threatpost.com/microsoft-exchange-proxytoken-email/169030/){:target="_blank" rel="noopener"}

> The bug (CVE-2021-33766) is an information-disclosure issue that could reveal victims' personal information, sensitive company data and more. [...]
