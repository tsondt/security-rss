Title: Brazilian government discloses National Treasury ransomware attack
Date: 2021-08-17T09:36:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-17-brazilian-government-discloses-national-treasury-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/brazilian-government-discloses-national-treasury-ransomware-attack/){:target="_blank" rel="noopener"}

> The Brazilian Ministry of Economy has disclosed a ransomware attack that hit some of National Treasury's computing systems on Friday night, right before the start of the weekend. [...]
