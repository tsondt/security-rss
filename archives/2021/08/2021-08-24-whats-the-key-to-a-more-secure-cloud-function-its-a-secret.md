Title: What's the key to a more secure Cloud Function? It's a secret!
Date: 2021-08-24T16:00:00+00:00
Author: Vinod Ramachandran
Category: GCP Security
Tags: Identity & Security;Google Cloud;Serverless
Slug: 2021-08-24-whats-the-key-to-a-more-secure-cloud-function-its-a-secret

[Source](https://cloud.google.com/blog/products/serverless/cloud-functions-integrates-with-google-secret-manager/){:target="_blank" rel="noopener"}

> Google Cloud Functions provides a simple and intuitive developer experience to execute code from Google Cloud, Firebase, Google Assistant, or any web, mobile, or backend application. Oftentimes that code needs secrets—like API keys, passwords, or certificates—to authenticate to or invoke upstream APIs and services. While Google Secret Manager is a fully-managed, secure, and convenient storage system for such secrets, developers have historically leveraged environment variables or the filesystem for managing secrets in Cloud Functions. This was largely because integrating with Secret Manager required developers to write custom code... until now. We listened to customer feedback and today we are announcing [...]
