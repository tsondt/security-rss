Title: Remote code execution flaws lurk in countless routers, IoT gear, cameras using Realtek Wi-Fi module SDKs
Date: 2021-08-16T20:11:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-08-16-remote-code-execution-flaws-lurk-in-countless-routers-iot-gear-cameras-using-realtek-wi-fi-module-sdks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/16/realtek_wifi_sdk_vulnerabilities/){:target="_blank" rel="noopener"}

> Devices from 60+ manufacturers affected, says infosec outfit Taiwanese chip designer Realtek has warned of four vulnerabilities in three SDKs accompanying its Wi-Fi modules, which are used in almost 200 products made by more than five dozen vendors.... [...]
