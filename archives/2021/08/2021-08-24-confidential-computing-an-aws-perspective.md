Title: Confidential computing: an AWS perspective
Date: 2021-08-24T22:18:33+00:00
Author: David Brown
Category: AWS Security
Tags: Amazon EC2;Foundational (100);Security, Identity, & Compliance;AWS Nitro System;Nitro Enclaves;Security;Security Blog;Trusted computing
Slug: 2021-08-24-confidential-computing-an-aws-perspective

[Source](https://aws.amazon.com/blogs/security/confidential-computing-an-aws-perspective/){:target="_blank" rel="noopener"}

> Customers around the globe—from governments and highly regulated industries to small businesses and start-ups—trust Amazon Web Services (AWS) with their most sensitive data and applications. At AWS, keeping our customers’ workloads secure and confidential, while helping them meet their privacy and data sovereignty requirements, is our highest priority. Our investments in security technologies and rigorous [...]
