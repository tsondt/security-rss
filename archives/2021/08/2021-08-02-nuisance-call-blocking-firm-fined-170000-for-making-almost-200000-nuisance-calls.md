Title: Nuisance call-blocking firm fined £170,000 for making almost 200,000 nuisance calls
Date: 2021-08-02T15:12:46+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2021-08-02-nuisance-call-blocking-firm-fined-170000-for-making-almost-200000-nuisance-calls

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/02/ico_nuisance_call_ycsl/){:target="_blank" rel="noopener"}

> Irony, thy name is Yes Consumer Solutions Ltd A firm that sells nuisance call-blocking systems is itself nursing a £170,000 fine from the UK's data watchdog, ironically for cold calling almost 200,000 people registered with the Telephone Preference Service (TPS).... [...]
