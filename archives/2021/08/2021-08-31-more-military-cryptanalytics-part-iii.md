Title: More Military Cryptanalytics, Part III
Date: 2021-08-31T11:37:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptanalysis;cryptography;FOIA;history of cryptography;military;NSA
Slug: 2021-08-31-more-military-cryptanalytics-part-iii

[Source](https://www.schneier.com/blog/archives/2021/08/more-military-cryptanalytics-part-iii.html){:target="_blank" rel="noopener"}

> Late last year, the NSA declassified and released a redacted version of Lambros D. Callimahos’s Military Cryptanalytics, Part III. We just got most of the index. It’s hard to believe that there are any real secrets left in this 44-year-old volume. [...]
