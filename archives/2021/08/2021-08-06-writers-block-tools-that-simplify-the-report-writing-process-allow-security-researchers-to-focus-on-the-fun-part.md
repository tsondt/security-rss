Title: Writers’ block? Tools that simplify the report-writing process allow security researchers to ‘focus on the fun part’
Date: 2021-08-06T12:24:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: writers-block-tools-that-simplify-the-report-writing-process-allow-security-researchers-to-focus-on-the-fun-part

[Source](https://portswigger.net/daily-swig/writers-block-tools-that-simplify-the-report-writing-process-allow-security-researchers-to-focus-on-the-fun-part){:target="_blank" rel="noopener"}

> Importance of communication brought into focus at Black Hat USA this week [...]
