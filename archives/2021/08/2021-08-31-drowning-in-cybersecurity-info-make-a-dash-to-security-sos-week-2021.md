Title: Drowning in cybersecurity info? Make a dash to Security SOS Week 2021
Date: 2021-08-31T06:30:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-08-31-drowning-in-cybersecurity-info-make-a-dash-to-security-sos-week-2021

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/31/drowning_in_cybersecurity_info_make/){:target="_blank" rel="noopener"}

> Dive deep into key topics – and still have time for lunch Sponsored Tapping into leading edge cyber security knowledge can be like listening to the radio. There’s a lot of great stuff out there, the trick is tuning out the noise.... [...]
