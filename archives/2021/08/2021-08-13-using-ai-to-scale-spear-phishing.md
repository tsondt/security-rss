Title: Using AI to Scale Spear Phishing
Date: 2021-08-13T11:16:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;phishing;social engineering
Slug: 2021-08-13-using-ai-to-scale-spear-phishing

[Source](https://www.schneier.com/blog/archives/2021/08/using-ai-to-scale-spear-phishing.html){:target="_blank" rel="noopener"}

> The problem with spear phishing it that it takes time and creativity to create individualized enticing phishing emails. Researchers are using GPT-3 to attempt to solve that problem: The researchers used OpenAI’s GPT-3 platform in conjunction with other AI-as-a-service products focused on personality analysis to generate phishing emails tailored to their colleagues’ backgrounds and traits. Machine learning focused on personality analysis aims to be predict a person’s proclivities and mentality based on behavioral inputs. By running the outputs through multiple services, the researchers were able to develop a pipeline that groomed and refined the emails before sending them out. They [...]
