Title: Apple responds to critics of CSAM scan plan with FAQs - says it'd block governments subverting its system
Date: 2021-08-09T21:37:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: apple-responds-to-critics-of-csam-scan-plan-with-faqs-says-itd-block-governments-subverting-its-system

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/09/apple_csam_faq/){:target="_blank" rel="noopener"}

> Offer may not apply in China, or anywhere else warn experts Apple's announcement last week that it will soon be scanning photos on iPhones and iPads that sync to iCloud for child sexual abuse material (CSAM) prompted pushback from thousands of security and privacy professionals and a response from the company that attempts to mollify its critics.... [...]
