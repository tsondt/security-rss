Title: Google expects delays in enforcing 2FA for Chrome extension devs
Date: 2021-08-05T09:35:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-expects-delays-in-enforcing-2fa-for-chrome-extension-devs

[Source](https://www.bleepingcomputer.com/news/google/google-expects-delays-in-enforcing-2fa-for-chrome-extension-devs/){:target="_blank" rel="noopener"}

> Google says that enforcing two-step verification on Google accounts of Chrome Web Store developers will take longer than expected. [...]
