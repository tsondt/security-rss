Title: Bow to the USBsamurai: Malicious USB cable leaves air-gapped networks open to attack
Date: 2021-08-06T11:43:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: bow-to-the-usbsamurai-malicious-usb-cable-leaves-air-gapped-networks-open-to-attack

[Source](https://portswigger.net/daily-swig/bow-to-the-usbsamurai-malicious-usb-cable-leaves-air-gapped-networks-open-to-attack){:target="_blank" rel="noopener"}

> Open source hacking tool costs less than $15 to produce [...]
