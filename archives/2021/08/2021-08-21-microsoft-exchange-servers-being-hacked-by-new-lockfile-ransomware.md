Title: Microsoft Exchange servers being hacked by new LockFile ransomware
Date: 2021-08-21T11:05:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-21-microsoft-exchange-servers-being-hacked-by-new-lockfile-ransomware

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-servers-being-hacked-by-new-lockfile-ransomware/){:target="_blank" rel="noopener"}

> A new ransomware gang known as LockFile encrypts Windows domains after hacking into Microsoft Exchange servers using the recently disclosed ProxyShell vulnerabilities. [...]
