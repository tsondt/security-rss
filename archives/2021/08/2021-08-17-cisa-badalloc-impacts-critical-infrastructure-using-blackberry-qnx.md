Title: CISA: BadAlloc impacts critical infrastructure using BlackBerry QNX
Date: 2021-08-17T14:16:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-17-cisa-badalloc-impacts-critical-infrastructure-using-blackberry-qnx

[Source](https://www.bleepingcomputer.com/news/security/cisa-badalloc-impacts-critical-infrastructure-using-blackberry-qnx/){:target="_blank" rel="noopener"}

> CISA today warned that IoT and OT security flaws known as BadAlloc impact BlackBerry's QNX Real Time Operating System (RTOS) used by critical infrastructure organizations. [...]
