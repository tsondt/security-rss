Title: Telegram for Mac bug lets you save self-destructing messages forever
Date: 2021-08-05T09:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: telegram-for-mac-bug-lets-you-save-self-destructing-messages-forever

[Source](https://www.bleepingcomputer.com/news/security/telegram-for-mac-bug-lets-you-save-self-destructing-messages-forever/){:target="_blank" rel="noopener"}

> ​Researchers have discovered a way for users on Telegram for Mac to keep specific self-destructing messages forever or view them without the sender ever knowing. [...]
