Title: New Windows PrintNightmare zero-days get free unofficial patch
Date: 2021-08-05T11:19:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: new-windows-printnightmare-zero-days-get-free-unofficial-patch

[Source](https://www.bleepingcomputer.com/news/microsoft/new-windows-printnightmare-zero-days-get-free-unofficial-patch/){:target="_blank" rel="noopener"}

> A free unofficial patch has been released to protect Windows users from all new PrintNightmare zero-day vulnerabilities discovered since June. [...]
