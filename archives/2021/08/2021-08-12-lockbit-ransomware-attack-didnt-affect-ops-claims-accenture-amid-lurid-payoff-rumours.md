Title: Lockbit ransomware attack didn't affect ops, claims Accenture amid lurid payoff rumours
Date: 2021-08-12T15:24:35+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-12-lockbit-ransomware-attack-didnt-affect-ops-claims-accenture-amid-lurid-payoff-rumours

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/12/accenture_lockbit_ransomware/){:target="_blank" rel="noopener"}

> No word on whether gang got their mitts on data, though Outsourcing and accounting firm Accenture has been struck by Lockbit ransomware.... [...]
