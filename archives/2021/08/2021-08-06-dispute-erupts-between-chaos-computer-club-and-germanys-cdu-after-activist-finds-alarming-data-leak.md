Title: Dispute erupts between Chaos Computer Club and Germany’s CDU after activist finds alarming data leak
Date: 2021-08-06T13:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: dispute-erupts-between-chaos-computer-club-and-germanys-cdu-after-activist-finds-alarming-data-leak

[Source](https://portswigger.net/daily-swig/dispute-erupts-between-chaos-computer-club-and-germanys-cdu-after-activist-finds-alarming-data-leak){:target="_blank" rel="noopener"}

> Christian Democratic Union spokesperson says police report was not directed at security researcher [...]
