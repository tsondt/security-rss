Title: Top Hacks from Black Hat and DEF CON 2021
Date: 2021-08-09T17:04:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: top-hacks-from-black-hat-and-def-con-2021

[Source](https://portswigger.net/daily-swig/top-hacks-from-black-hat-and-def-con-2021){:target="_blank" rel="noopener"}

> Tools, techniques, and (hybrid) procedures [...]
