Title: Amazon Kindle Vulnerable to Malicious EBooks
Date: 2021-08-06T18:54:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Mobile Security;Vulnerabilities
Slug: amazon-kindle-vulnerable-to-malicious-ebooks

[Source](https://threatpost.com/amazon-kindle-malicious-ebooks/168454/){:target="_blank" rel="noopener"}

> Prior to a patch, a serious bug could have allowed attackers to take over Kindles and steal personal data. [...]
