Title: Big tech proud as punch about cameos in Joe Biden's security theatre
Date: 2021-08-26T07:59:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-08-26-big-tech-proud-as-punch-about-cameos-in-joe-bidens-security-theatre

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/26/big_tech_biden_cybersecurity_meeting/){:target="_blank" rel="noopener"}

> After White House summit, AWS promises MFA tokens, Google and Microsoft spray money, IBM 'announces' snapshots against ransomware US President Joe Biden staged a cyber security summit at the White House, and it's produced quick results in the form of big tech making vague promises about stuff they think will improve the nation's security... [...]
