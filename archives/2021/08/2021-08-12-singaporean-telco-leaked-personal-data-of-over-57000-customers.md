Title: Singaporean telco leaked personal data of over 57,000 customers
Date: 2021-08-12T04:28:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-08-12-singaporean-telco-leaked-personal-data-of-over-57000-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/12/singapore_telecom_breach_leaked_personal/){:target="_blank" rel="noopener"}

> StarHub's breach announcement came a month after discovery of customer file on dump site Singapore pay TV, internet and mobile phone provider StarHub is in the process of notifying 57,191 customers via email that they are victims of a cyber attack that leaked national identity card numbers, mobile numbers and email addresses.... [...]
