Title: T-Mobile’s Security Is ‘Awful,’ Says Purported Thief
Date: 2021-08-28T16:58:45+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks;Mobile Security;News;Web Security
Slug: 2021-08-28-t-mobiles-security-is-awful-says-purported-thief

[Source](https://threatpost.com/t-mobile-security-awful-thief/169011/){:target="_blank" rel="noopener"}

> John Binns, claiming to be behind the massive T-Mobile theft of >50m customer records, dissed the security measures of the US's No. 2 wireless biggest carrier. T-Mobile is "humbled," it said, announcing new partnerships with security heavyweights on Friday. [...]
