Title: Rampant misconfigurations in Microsoft Power Apps exposed 38 million records
Date: 2021-08-24T16:32:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-24-rampant-misconfigurations-in-microsoft-power-apps-exposed-38-million-records

[Source](https://portswigger.net/daily-swig/rampant-misconfigurations-in-microsoft-power-apps-exposed-38-million-records){:target="_blank" rel="noopener"}

> Microsoft makes OData APIs privacy-preserving by default after revelations [...]
