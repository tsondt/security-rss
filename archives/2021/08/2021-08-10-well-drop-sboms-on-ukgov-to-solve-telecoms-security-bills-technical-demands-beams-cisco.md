Title: We'll drop SBOMs on UK.gov to solve Telecoms Security Bill's technical demands, beams Cisco
Date: 2021-08-10T09:26:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: well-drop-sboms-on-ukgov-to-solve-telecoms-security-bills-technical-demands-beams-cisco

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/10/telecoms_security_bill_vendor_annex_cisco_doc/){:target="_blank" rel="noopener"}

> Doc reveals more of what's causing industry to tear its hair out Britain's Telecoms Security Bill will be accompanied by a detailed code of practice containing 70 specific security requirements for telcos and their suppliers to meet, The Register can reveal.... [...]
