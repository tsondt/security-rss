Title: Research finds cyber-snoops working for 'Chinese state interests' lurking in SE Asian telco networks since 2017
Date: 2021-08-03T04:01:07+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: research-finds-cyber-snoops-working-for-chinese-state-interests-lurking-in-se-asian-telco-networks-since-2017

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/03/cybereason_deadringer/){:target="_blank" rel="noopener"}

> Handy way to keep tabs on 'activists, politicians, business leaders, and more' Attack protection specialist Cybereason has fingered threat actors working on behalf of "Chinese state interests" as being behind attacks on telcos operating in Southeast Asia – with some having been prowling the penetrated networks for information on high-value targets since 2017.... [...]
