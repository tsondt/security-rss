Title: Energy group ERG reports minor disruptions after ransomware attack
Date: 2021-08-04T17:34:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: energy-group-erg-reports-minor-disruptions-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/energy-group-erg-reports-minor-disruptions-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Italian energy company ERG reports "only a few minor disruptions" affecting its information and communications technology (ICT) infrastructure following a ransomware attack on its systems. [...]
