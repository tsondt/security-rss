Title: How US federal agencies can use AWS to improve logging and log retention
Date: 2021-08-13T19:36:02+00:00
Author: Derek Doerr
Category: AWS Security
Tags: Federal;Government;Intermediate (200);Public Sector;Security, Identity, & Compliance;Compliance;cybersecurity;Cybersecurity EO;Executive order;Incident response;Logging;Observability;Security Blog
Slug: 2021-08-13-how-us-federal-agencies-can-use-aws-to-improve-logging-and-log-retention

[Source](https://aws.amazon.com/blogs/security/how-us-federal-agencies-can-use-aws-to-improve-logging-and-log-retention/){:target="_blank" rel="noopener"}

> This post is part of a series about how Amazon Web Services (AWS) can help your US federal agency meet the requirements of the President’s Executive Order on Improving the Nation’s Cybersecurity. You will learn how you can use AWS information security practices to help meet the requirement to improve logging and log retention practices [...]
