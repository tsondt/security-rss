Title: OK, so you stole $600m-plus from us, how about you be our Chief Security Advisor, Poly Network asks thief
Date: 2021-08-18T20:29:37+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2021-08-18-ok-so-you-stole-600m-plus-from-us-how-about-you-be-our-chief-security-advisor-poly-network-asks-thief

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/18/poly_network_job/){:target="_blank" rel="noopener"}

> Could it be a trap? The mysterious thief who stole $600m-plus in cryptocurrencies from Poly Network has been offered the role of Chief Security Advisor at the Chinese blockchain biz.... [...]
