Title: Windows PetitPotam attacks can be blocked using new method
Date: 2021-08-02T12:10:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: windows-petitpotam-attacks-can-be-blocked-using-new-method

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-petitpotam-attacks-can-be-blocked-using-new-method/){:target="_blank" rel="noopener"}

> Security researchers have devised a way to block the recently disclosed PetitPotam attack vector that allows hackers to take control of a Windows domain controller easily. [...]
