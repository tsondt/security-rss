Title: Phishing Campaign Dangles SharePoint File-Shares
Date: 2021-08-04T14:44:54+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Web Security
Slug: phishing-campaign-dangles-sharepoint-file-shares

[Source](https://threatpost.com/phishing-sharepoint-file-shares/168356/){:target="_blank" rel="noopener"}

> Attackers spoof sender addresses to appear legitimate in a crafty campaign that can slip past numerous detections, Microsoft researchers have discovered. [...]
