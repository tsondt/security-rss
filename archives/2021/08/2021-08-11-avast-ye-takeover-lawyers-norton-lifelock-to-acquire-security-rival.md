Title: Avast, ye takeover lawyers! Norton LifeLock to acquire security rival
Date: 2021-08-11T05:15:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-08-11-avast-ye-takeover-lawyers-norton-lifelock-to-acquire-security-rival

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/11/nortonlifelock_to_acquire_avast/){:target="_blank" rel="noopener"}

> $8B deal will create smallbiz and consumer security monster The discussions between security vendors NortonLifelock and Avast that The Register reported had reached an advanced stage in July have proved fruitful, to the tune of more than $8 billion.... [...]
