Title: LockFile Ransomware Uses Never-Before Seen Encryption to Avoid Detection
Date: 2021-08-31T10:42:18+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2021-08-31-lockfile-ransomware-uses-never-before-seen-encryption-to-avoid-detection

[Source](https://threatpost.com/lockfile-ransomware-avoid-detection/169042/){:target="_blank" rel="noopener"}

> Researchers from Sophos discovered the emerging threat in July, which exploits the ProxyShell vulnerabilities in Microsoft Exchange servers to attack systems. [...]
