Title: Microsoft and Google to invest billions to bolster US cybersecurity
Date: 2021-08-26T11:27:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-08-26-microsoft-and-google-to-invest-billions-to-bolster-us-cybersecurity

[Source](https://www.bleepingcomputer.com/news/security/microsoft-and-google-to-invest-billions-to-bolster-us-cybersecurity/){:target="_blank" rel="noopener"}

> Executives and leaders from big tech, education, the finance sector, and infrastructure have committed to bolstering US interests' security during yesterday's White House cybersecurity summit. [...]
