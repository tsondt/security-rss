Title: Amazon’s Plan to Track Worker Keystrokes: A Sign of Controls to Come?
Date: 2021-08-13T21:19:16+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;Privacy
Slug: 2021-08-13-amazons-plan-to-track-worker-keystrokes-a-sign-of-controls-to-come

[Source](https://threatpost.com/amazons-track-worker-keystrokes/168687/){:target="_blank" rel="noopener"}

> Data theft, insider threats and imposters accessing sensitive customer data have apparently gotten so bad inside Amazon, the company is considering rolling out keyboard-stroke monitoring for its customer-service reps. A confidential memo from inside Amazon explained that customer service credential abuse and data theft was on the rise, according to Motherboard which reviewed the document. [...]
