Title: MacOS Flaw in Telegram Retrieves Deleted Messages
Date: 2021-08-05T15:26:32+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: News
Slug: macos-flaw-in-telegram-retrieves-deleted-messages

[Source](https://threatpost.com/macos-flaw-in-telegram-retrieves-deleted-messages/168412/){:target="_blank" rel="noopener"}

> Telegram declined to fix a scenario in which the flaw can be exploited, spurring a Trustwave researcher to decline a bug bounty and to disclose his findings instead. [...]
