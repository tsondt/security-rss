Title: Critical bug impacting millions of IoT devices lets hackers spy on you
Date: 2021-08-17T09:23:13-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-17-critical-bug-impacting-millions-of-iot-devices-lets-hackers-spy-on-you

[Source](https://www.bleepingcomputer.com/news/security/critical-bug-impacting-millions-of-iot-devices-lets-hackers-spy-on-you/){:target="_blank" rel="noopener"}

> Security researchers are sounding the alarm on a critical vulnerability affecting tens of millions of devices worldwide connected via ThroughTek's Kalay IoT cloud platform. [...]
