Title: Ransomware Payments Explode Amid ‘Quadruple Extortion’
Date: 2021-08-12T16:06:27+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-08-12-ransomware-payments-explode-amid-quadruple-extortion

[Source](https://threatpost.com/ransomware-payments-quadruple-extortion/168622/){:target="_blank" rel="noopener"}

> Unit 42 puts the average payout at over half a million, while Barracuda has tracked a 64 percent year over year spike in the number of attacks. [...]
