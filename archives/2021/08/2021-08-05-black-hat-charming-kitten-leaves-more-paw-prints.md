Title: Black Hat: Charming Kitten  Leaves More Paw Prints
Date: 2021-08-05T14:16:03+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Black Hat;Malware;Mobile Security;News;Web Security
Slug: black-hat-charming-kitten-leaves-more-paw-prints

[Source](https://threatpost.com/black-hat-charming-kitten-opsec-goofs-training-videos/168394/){:target="_blank" rel="noopener"}

> IBM X-Force detailed the custom-made "LittleLooter" data stealer and 4+ hours of ITG18 operator training videos revealed by an opsec goof. [...]
