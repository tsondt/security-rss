Title: Adobe fixes critical preauth vulnerabilities in Magento
Date: 2021-08-10T17:08:46-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Software
Slug: 2021-08-10-adobe-fixes-critical-preauth-vulnerabilities-in-magento

[Source](https://www.bleepingcomputer.com/news/security/adobe-fixes-critical-preauth-vulnerabilities-in-magento/){:target="_blank" rel="noopener"}

> Adobe has released a large Patch Tuesday security update that fixes critical vulnerabilities in Magento and important bugs in Adobe Connect. [...]
