Title: Apple Adds a Backdoor to iMesssage and iCloud Storage
Date: 2021-08-10T11:37:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Apple;backdoors;cloud computing;Edward Snowden;privacy;surveillance
Slug: apple-adds-a-backdoor-to-imesssage-and-icloud-storage

[Source](https://www.schneier.com/blog/archives/2021/08/apple-adds-a-backdoor-to-imesssage-and-icloud-storage.html){:target="_blank" rel="noopener"}

> Apple’s announcement that it’s going to start scanning photos for child abuse material is a big deal. ( Here are five news stories.) I have been following the details, and discussing it in several different email lists. I don’t have time right now to delve into the details, but wanted to post something. EFF writes : There are two main features that the company is planning to install in every Apple device. One is a scanning feature that will scan all photos as they get uploaded into iCloud Photos to see if they match a photo in the database of [...]
