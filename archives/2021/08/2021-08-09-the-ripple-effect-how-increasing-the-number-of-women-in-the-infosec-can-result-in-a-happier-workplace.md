Title: The Ripple Effect: How increasing the number of women in the infosec can result in a happier workplace
Date: 2021-08-09T14:24:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: the-ripple-effect-how-increasing-the-number-of-women-in-the-infosec-can-result-in-a-happier-workplace

[Source](https://portswigger.net/daily-swig/the-ripple-effect-how-increasing-the-number-of-women-in-the-infosec-can-result-in-a-happier-workplace){:target="_blank" rel="noopener"}

> Women in tech was a key community topic at this year’s Black Hat USA conference [...]
