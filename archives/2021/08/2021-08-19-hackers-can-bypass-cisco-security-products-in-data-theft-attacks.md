Title: Hackers can bypass Cisco security products in data theft attacks
Date: 2021-08-19T13:30:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-08-19-hackers-can-bypass-cisco-security-products-in-data-theft-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-can-bypass-cisco-security-products-in-data-theft-attacks/){:target="_blank" rel="noopener"}

> Cisco said that unauthenticated attackers could bypass TLS inspection filtering tech in multiple products to exfiltrate data from previously compromised servers inside customers' networks. [...]
