Title: United Nations calls for moratorium on sale of surveillance tech like NSO Group's Pegasus
Date: 2021-08-13T07:58:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-08-13-united-nations-calls-for-moratorium-on-sale-of-surveillance-tech-like-nso-groups-pegasus

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/13/un_wants_surveillance_tech_sales_moratorium/){:target="_blank" rel="noopener"}

> Suggests the world to sort out a ban to preserve human rights, issues sternly worded 'Please Explain' to Israel The United Nations has called for a moratorium on the sale of "life threatening" surveillance technology and singled out the NSO Group and Israel for criticism.... [...]
