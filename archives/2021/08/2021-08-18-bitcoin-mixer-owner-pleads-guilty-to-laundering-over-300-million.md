Title: Bitcoin mixer owner pleads guilty to laundering over $300 million
Date: 2021-08-18T15:34:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-08-18-bitcoin-mixer-owner-pleads-guilty-to-laundering-over-300-million

[Source](https://www.bleepingcomputer.com/news/security/bitcoin-mixer-owner-pleads-guilty-to-laundering-over-300-million/){:target="_blank" rel="noopener"}

> Larry Dean Harmon, the owner of a dark web cryptocurrency laundering service known as Helix, pleaded guilty today of laundering over $300 million worth of bitcoins between 2014 and 2017. [...]
