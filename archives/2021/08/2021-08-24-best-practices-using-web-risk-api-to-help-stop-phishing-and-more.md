Title: Best practices using Web Risk API to help stop phishing and more
Date: 2021-08-24T16:00:00+00:00
Author: Evan Derheim
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-08-24-best-practices-using-web-risk-api-to-help-stop-phishing-and-more

[Source](https://cloud.google.com/blog/products/identity-security/follow-web-risk-apis-best-practices-to-stop-attacks/){:target="_blank" rel="noopener"}

> Whether you are a social media site, security company, or enterprise email manager, keeping users safe is always the goal. However, the top two threats to users' security, phishing and malware, can make that challenging. The Safe Browsing team at Google has been dedicated to defending the web from malware, phishing, and more threats for more than a decade. Google scans more than a billion links on a daily basis to produce a list of over one million malicious URLs related to phishing, social engineering, malware, and unwanted software. Over the last couple years, we’ve introduced an enterprise-ready tool that [...]
