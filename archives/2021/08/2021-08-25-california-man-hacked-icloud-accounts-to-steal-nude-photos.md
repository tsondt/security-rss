Title: California Man Hacked iCloud Accounts to Steal Nude Photos
Date: 2021-08-25T11:41:31+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy
Slug: 2021-08-25-california-man-hacked-icloud-accounts-to-steal-nude-photos

[Source](https://threatpost.com/man-hacked-icloud/168923/){:target="_blank" rel="noopener"}

> Hao Kou Chi pleaded guilty to four felonies in a hacker-for-hire scam that used socially engineered emails to trick people out of their credentials. [...]
