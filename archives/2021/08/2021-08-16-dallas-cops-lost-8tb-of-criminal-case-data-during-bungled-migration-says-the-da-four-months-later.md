Title: Dallas cops lost 8TB of criminal case data during bungled migration, says the DA... four months later
Date: 2021-08-16T13:19:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-08-16-dallas-cops-lost-8tb-of-criminal-case-data-during-bungled-migration-says-the-da-four-months-later

[Source](https://go.theregister.com/feed/www.theregister.com/2021/08/16/dallas_data_migration_8tb_deletion/){:target="_blank" rel="noopener"}

> Murder trial affected last week A bungled data migration of a network drive caused the deletion of 22 terabytes of information from a US police force's systems – including case files in a murder trial, according to local reports.... [...]
