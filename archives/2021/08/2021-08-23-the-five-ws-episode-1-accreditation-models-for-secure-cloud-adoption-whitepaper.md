Title: The Five Ws episode 1: Accreditation models for secure cloud adoption whitepaper
Date: 2021-08-23T21:02:29+00:00
Author: Jana Kay
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;accreditation;cloud adoption;Security Blog;The Five Ws
Slug: 2021-08-23-the-five-ws-episode-1-accreditation-models-for-secure-cloud-adoption-whitepaper

[Source](https://aws.amazon.com/blogs/security/the-five-ws-episode-1-accreditation-models-for-secure-cloud-adoption-whitepaper/){:target="_blank" rel="noopener"}

> AWS whitepapers are a great way to expand your knowledge of the cloud. Authored by Amazon Web Services (AWS) and the AWS community, they provide in-depth content that often addresses specific customer situations. We’re featuring some of our whitepapers in a new video series, The Five Ws. These short videos outline the who, what, when, [...]
