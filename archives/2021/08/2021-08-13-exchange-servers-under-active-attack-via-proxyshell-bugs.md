Title: Exchange Servers Under Active Attack via ProxyShell Bugs
Date: 2021-08-13T18:56:27+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;News;Vulnerabilities;Web Security
Slug: 2021-08-13-exchange-servers-under-active-attack-via-proxyshell-bugs

[Source](https://threatpost.com/exchange-servers-attack-proxyshell/168661/){:target="_blank" rel="noopener"}

> There’s an entirely new attack surface in Exchange, a researcher revealed at Black Hat, and threat actors are now exploiting servers vulnerable to the RCE bugs. [...]
