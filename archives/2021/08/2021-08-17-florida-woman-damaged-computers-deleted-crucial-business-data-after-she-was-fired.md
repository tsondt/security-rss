Title: Florida woman ‘damaged computers, deleted crucial business data’ after she was fired
Date: 2021-08-17T12:08:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-17-florida-woman-damaged-computers-deleted-crucial-business-data-after-she-was-fired

[Source](https://portswigger.net/daily-swig/florida-woman-damaged-computers-deleted-crucial-business-data-after-she-was-fired){:target="_blank" rel="noopener"}

> A harsh lesson in the importance of access revocation [...]
