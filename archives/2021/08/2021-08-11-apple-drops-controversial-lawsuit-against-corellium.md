Title: Apple drops controversial lawsuit against Corellium
Date: 2021-08-11T16:34:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-08-11-apple-drops-controversial-lawsuit-against-corellium

[Source](https://portswigger.net/daily-swig/apple-drops-controversial-lawsuit-against-corellium){:target="_blank" rel="noopener"}

> Fears of chilling effect on security tool development lifted [...]
