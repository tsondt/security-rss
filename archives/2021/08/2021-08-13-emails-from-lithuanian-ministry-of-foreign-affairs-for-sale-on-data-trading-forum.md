Title: Emails from Lithuanian Ministry of Foreign Affairs for sale on data-trading forum
Date: 2021-08-13T15:04:03-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-08-13-emails-from-lithuanian-ministry-of-foreign-affairs-for-sale-on-data-trading-forum

[Source](https://www.bleepingcomputer.com/news/security/emails-from-lithuanian-ministry-of-foreign-affairs-for-sale-on-data-trading-forum/){:target="_blank" rel="noopener"}

> The Lithuanian Ministry of Foreign Affairs has declined to comment about the authenticity of email files allegedly stolen from its network and offered for sale on a data-trading forum.Lith [...]
