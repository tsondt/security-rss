Title: Apple fixes 2 iOS zero-day vulnerabilities actively used in attacks
Date: 2021-05-03T18:56:56-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Apple
Slug: apple-fixes-2-ios-zero-day-vulnerabilities-actively-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/apple/apple-fixes-2-ios-zero-day-vulnerabilities-actively-used-in-attacks/){:target="_blank" rel="noopener"}

> Today, Apple has released security updates that fix two actively exploited iOS zero-day vulnerabilities in the Webkit engine used by hackers to attack iPhones, iPads, iPods, macOS, and Apple Watch devices. [...]
