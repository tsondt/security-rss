Title: Apple Fixes Zero‑Day Security Bugs Under Active Attack
Date: 2021-05-04T16:16:37+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: apple-fixes-zero-day-security-bugs-under-active-attack

[Source](https://threatpost.com/apple-zero%e2%80%91days-active-attack/165842/){:target="_blank" rel="noopener"}

> On Monday, Apple released a quartet of unscheduled updates for iOS, macOS, and watchOS, slapping security patches on flaws in its WebKit browser engine. [...]
