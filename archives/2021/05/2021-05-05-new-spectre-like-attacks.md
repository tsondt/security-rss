Title: New Spectre-Like Attacks
Date: 2021-05-05T15:35:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;hardware;patching;vulnerabilities
Slug: new-spectre-like-attacks

[Source](https://www.schneier.com/blog/archives/2021/05/new-spectre-like-attacks.html){:target="_blank" rel="noopener"}

> There’s new research that demonstrates security vulnerabilities in all of the AMD and Intel chips with micro-op caches, including the ones that were specifically engineered to be resistant to the Spectre/Meltdown attacks of three years ago. Details : The new line of attacks exploits the micro-op cache: an on-chip structure that speeds up computing by storing simple commands and allowing the processor to fetch them quickly and early in the speculative execution process, as the team explains in a writeup from the University of Virginia. Even though the processor quickly realizes its mistake and does a U-turn to go down [...]
