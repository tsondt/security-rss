Title: Forrester names Google Cloud a Leader in Unstructured Data Security Platforms
Date: 2021-05-20T16:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: forrester-names-google-cloud-a-leader-in-unstructured-data-security-platforms

[Source](https://cloud.google.com/blog/products/identity-security/google-a-leader-in-unstructured-data-security-platforms/){:target="_blank" rel="noopener"}

> As organizations expand their use of cloud computing services, more of their sensitive data inevitably moves to and lives in the cloud. Much of this sensitive data is unstructured and can be challenging to secure. Despite this potential challenge, the usefulness of cloud for data storage and processing is too big for most organizations to ignore and has in turn led to data sprawl, where their sensitive data is spread over many resources, both in the cloud and on-premise. Addressing data sprawl requires solutions that can discover, manage, and secure sensitive data, especially unstructured data, as it spreads. To help [...]
