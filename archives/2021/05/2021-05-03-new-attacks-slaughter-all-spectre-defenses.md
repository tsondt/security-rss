Title: New Attacks Slaughter All Spectre Defenses
Date: 2021-05-03T20:56:03+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities
Slug: new-attacks-slaughter-all-spectre-defenses

[Source](https://threatpost.com/attacks-slaughter-spectre-defenses/165809/){:target="_blank" rel="noopener"}

> The 3+ years computer scientists spent concocting ways to defend against these supply-chain attacks against chip architecture? It's bound for the dustbin. [...]
