Title: Aurelia framework’s default HTML sanitizer opens the door to XSS attacks
Date: 2021-05-13T13:28:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: aurelia-frameworks-default-html-sanitizer-opens-the-door-to-xss-attacks

[Source](https://portswigger.net/daily-swig/aurelia-frameworks-default-html-sanitizer-opens-the-door-to-xss-attacks){:target="_blank" rel="noopener"}

> Contributor slowdown indicates a patch may not be in the works [...]
