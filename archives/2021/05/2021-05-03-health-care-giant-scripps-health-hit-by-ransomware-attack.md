Title: Health care giant Scripps Health hit by ransomware attack
Date: 2021-05-03T19:33:38-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: health-care-giant-scripps-health-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/health-care-giant-scripps-health-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Nonprofit health care provider Scripps Health in San Diego is currently dealing with a ransomware attack that forced the organization to suspend user access to its online portal and switch to alternative methods for patient care operations. [...]
