Title: Eufycam Wi-Fi security cameras streamed video feeds from other people's homes
Date: 2021-05-17T19:58:38+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: eufycam-wi-fi-security-cameras-streamed-video-feeds-from-other-peoples-homes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/17/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Biden's order on security, US govt acquiring data on citizens, and more In brief Unlucky owners of Eufycam security cameras were horrified earlier today when they opened their app for the equipment and saw video streams from strangers' homes instead of their own.... [...]
