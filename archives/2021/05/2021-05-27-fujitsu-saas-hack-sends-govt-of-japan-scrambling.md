Title: Fujitsu SaaS Hack Sends Govt. of Japan Scrambling
Date: 2021-05-27T13:56:20+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Cloud Security;Government
Slug: fujitsu-saas-hack-sends-govt-of-japan-scrambling

[Source](https://threatpost.com/fujitsu-saas-hack-japan-scrambling/166517/){:target="_blank" rel="noopener"}

> Tech giant disables ProjectWEB cloud-based collaboration platform after threat actors gained access and nabbed files belonging to several state entities. [...]
