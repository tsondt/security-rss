Title: US healthcare non-profit reports data breach impacting 200,000 patients, employees
Date: 2021-05-25T12:58:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-healthcare-non-profit-reports-data-breach-impacting-200000-patients-employees

[Source](https://portswigger.net/daily-swig/us-healthcare-non-profit-reports-data-breach-impacting-200-000-patients-employees){:target="_blank" rel="noopener"}

> Unauthorized intrusion detected by Rehoboth McKinley Christian Health Care Services [...]
