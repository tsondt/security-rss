Title: Cuba Ransomware partners with Hancitor for spam-fueled attacks
Date: 2021-05-07T05:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: cuba-ransomware-partners-with-hancitor-for-spam-fueled-attacks

[Source](https://www.bleepingcomputer.com/news/security/cuba-ransomware-partners-with-hancitor-for-spam-fueled-attacks/){:target="_blank" rel="noopener"}

> The Cuba Ransomware gang has teamed up with the spam operators of the Hancitor malware to gain easier access to compromised corporate networks. [...]
