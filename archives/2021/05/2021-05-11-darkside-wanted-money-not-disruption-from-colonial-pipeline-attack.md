Title: DarkSide Wanted Money, Not Disruption from Colonial Pipeline Attack
Date: 2021-05-11T14:45:48+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: darkside-wanted-money-not-disruption-from-colonial-pipeline-attack

[Source](https://threatpost.com/darkside-wanted-money-not-disruption/166016/){:target="_blank" rel="noopener"}

> Statement by the ransomware gang suggests that the incident that crippled a major U.S. oil pipeline may not have exactly gone to plan for overseas threat actors. [...]
