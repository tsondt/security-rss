Title: Anti-Spam WordPress Plugin Could Expose Website User Data
Date: 2021-05-05T20:58:00+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: anti-spam-wordpress-plugin-could-expose-website-user-data

[Source](https://threatpost.com/anti-spam-wordpress-plugin-expose-data/165901/){:target="_blank" rel="noopener"}

> 'Spam protection, AntiSpam, FireWall by CleanTalk' is installed on more than 100,000 sites -- and could offer up sensitive info to attackers that aren't even logged in. [...]
