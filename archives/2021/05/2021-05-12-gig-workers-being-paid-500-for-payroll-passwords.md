Title: Gig Workers Being Paid $500 for Payroll Passwords
Date: 2021-05-12T16:50:43+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Privacy;Web Security
Slug: gig-workers-being-paid-500-for-payroll-passwords

[Source](https://threatpost.com/gig-workers-paid-payroll-passwords/166086/){:target="_blank" rel="noopener"}

> Argyle is paying workers to help hack payroll providers, researchers suspect. [...]
