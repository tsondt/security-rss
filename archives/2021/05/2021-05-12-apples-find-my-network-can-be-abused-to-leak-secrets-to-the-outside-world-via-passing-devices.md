Title: Apple's Find My network can be abused to leak secrets to the outside world via passing devices
Date: 2021-05-12T20:28:31+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: apples-find-my-network-can-be-abused-to-leak-secrets-to-the-outside-world-via-passing-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/12/apples_find_network/){:target="_blank" rel="noopener"}

> You gotta work hard for those three-bytes-a-second transfers, though Apple's Find My network, used to locate iOS and macOS devices – and more recently AirTags and other kit – also turns out to be a potential espionage tool.... [...]
