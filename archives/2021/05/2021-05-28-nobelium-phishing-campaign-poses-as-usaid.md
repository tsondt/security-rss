Title: Nobelium Phishing Campaign Poses as USAID
Date: 2021-05-28T13:13:39+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: nobelium-phishing-campaign-poses-as-usaid

[Source](https://threatpost.com/solarwinds-nobelium-phishing-attack-usaid/166531/){:target="_blank" rel="noopener"}

> Microsoft uncovered the SolarWinds crooks using mass-mail service Constant Contact and posing as a U.S.-based development organization to deliver malicious URLs to more than 150 organizations. [...]
