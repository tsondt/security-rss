Title: Qualcomm Snapdragon 855 modem code flaw exposed Android smartphones to possible snooping
Date: 2021-05-06T16:11:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: qualcomm-snapdragon-855-modem-code-flaw-exposed-android-smartphones-to-possible-snooping

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/06/qualcomm_snapdragon_855_heap_overflow/){:target="_blank" rel="noopener"}

> Good thing researchers spotted it, no evidence of exploit in the wild A heap overflow vulnerability in Qualcomm's Snapdragon 855 system-on-chip modem firmware, used in Android devices, could be exploited by baddies to run arbitrary code on unsuspecting users' devices, according to Check Point.... [...]
