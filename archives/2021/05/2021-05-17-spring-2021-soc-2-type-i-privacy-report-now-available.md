Title: Spring 2021 SOC 2 Type I Privacy report now available
Date: 2021-05-17T10:38:42+00:00
Author: Ninad Naik
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Privacy Report;AWS SOC Reports;Security Blog
Slug: spring-2021-soc-2-type-i-privacy-report-now-available

[Source](https://aws.amazon.com/blogs/security/spring-2021-soc-2-type-i-privacy-report-now-available/){:target="_blank" rel="noopener"}

> Your privacy considerations are at the core of our compliance work at Amazon Web Services, and we are focused on the protection of your content while using AWS. Our Spring 2021 SOC 2 Type I Privacy report is now available to demonstrate our privacy compliance commitment to you. The Spring 2021 SOC 2 Type I [...]
