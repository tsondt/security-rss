Title: Unfixable Apple M1 chip bug enables cross-process chatter, breaking OS security model
Date: 2021-05-27T01:38:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: unfixable-apple-m1-chip-bug-enables-cross-process-chatter-breaking-os-security-model

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/27/apple_m1_chip_bug/){:target="_blank" rel="noopener"}

> M1RACLES flaw looks more embarrassing than dangerous Apple's Arm-based M1 chip, much ballyhooed for its performance, contains a design flaw that can be exploited to allow different processes to quietly communicate with one another, in violation of operating system security principles.... [...]
