Title: All major desktop browsers vulnerable to tracking flaw that can bypass privacy tools – research
Date: 2021-05-14T14:09:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: all-major-desktop-browsers-vulnerable-to-tracking-flaw-that-can-bypass-privacy-tools-research

[Source](https://portswigger.net/daily-swig/all-major-desktop-browsers-vulnerable-to-tracking-flaw-that-can-bypass-privacy-tools-research){:target="_blank" rel="noopener"}

> Chrome, Firefox, Safari, and Tor Browser all affected by ‘scheme flooding’ attacks [...]
