Title: Canada Post reveals supplier data breach involving shipping information of 950,000 parcel recipients
Date: 2021-05-27T13:18:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: canada-post-reveals-supplier-data-breach-involving-shipping-information-of-950000-parcel-recipients

[Source](https://portswigger.net/daily-swig/canada-post-reveals-supplier-data-breach-involving-shipping-information-of-950-000-parcel-recipients){:target="_blank" rel="noopener"}

> Names and postal addresses leak blamed on malware attack [...]
