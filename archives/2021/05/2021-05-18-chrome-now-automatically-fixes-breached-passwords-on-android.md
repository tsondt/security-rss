Title: Chrome now automatically fixes breached passwords on Android
Date: 2021-05-18T16:22:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: chrome-now-automatically-fixes-breached-passwords-on-android

[Source](https://www.bleepingcomputer.com/news/security/chrome-now-automatically-fixes-breached-passwords-on-android/){:target="_blank" rel="noopener"}

> Google is rolling out a new Chrome on Android feature to help users change passwords compromised in data breaches with a single tap. [...]
