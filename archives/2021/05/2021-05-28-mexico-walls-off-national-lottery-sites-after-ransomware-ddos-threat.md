Title: Mexico walls off national lottery sites after ransomware DDoS threat
Date: 2021-05-28T13:14:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: mexico-walls-off-national-lottery-sites-after-ransomware-ddos-threat

[Source](https://www.bleepingcomputer.com/news/security/mexico-walls-off-national-lottery-sites-after-ransomware-ddos-threat/){:target="_blank" rel="noopener"}

> Access to Mexico's Lotería Nacional and Pronósticos lottery websites are now blocked to IP addresses outside of Mexico after a ransomware gang threatened to perform denial of service attacks. [...]
