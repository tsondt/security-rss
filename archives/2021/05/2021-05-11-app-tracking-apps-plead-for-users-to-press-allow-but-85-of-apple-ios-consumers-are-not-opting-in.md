Title: App Tracking: Apps plead for users to press allow, but 85% of Apple iOS consumers are not opting in
Date: 2021-05-11T13:45:12+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: app-tracking-apps-plead-for-users-to-press-allow-but-85-of-apple-ios-consumers-are-not-opting-in

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/11/85_of_apple_ios_users/){:target="_blank" rel="noopener"}

> The data is in: most users do not opt in to third-party tracking Mobile app analytics company Flurry is measuring how many users of iOS 14.5 are opting in to allow apps to request to track them - and so far only 15 per cent worldwide have done so.... [...]
