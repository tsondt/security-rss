Title: Australian Federal Police hiring digital evidence retrieval specialists: Being a very good boy and paws required
Date: 2021-05-19T07:58:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: australian-federal-police-hiring-digital-evidence-retrieval-specialists-being-a-very-good-boy-and-paws-required

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/19/woof_woof_whos_a_good/){:target="_blank" rel="noopener"}

> Hounds can sniff out SIM cards that a human might miss Australia's Federal Police (AFP) is getting more help from some very good boys with four paws, wagging tails, and the ability to sniff out tech equipment with their highly sensitive noses.... [...]
