Title: Business-intelligence-company-turned-Bitcoin-addict MicroStrategy grabs another $10m crypto-coin fix
Date: 2021-05-18T17:01:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: business-intelligence-company-turned-bitcoin-addict-microstrategy-grabs-another-10m-crypto-coin-fix

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/18/microstrategy_bitcoin/){:target="_blank" rel="noopener"}

> Who's recommending investment? The man who already has a $1bn stake Wikipedia says MicroStrategy is a company that provides business intelligence (BI), mobile software, and cloud-based services, but that wouldn't be the first outdated information on the crowdsourced knowledge repository.... [...]
