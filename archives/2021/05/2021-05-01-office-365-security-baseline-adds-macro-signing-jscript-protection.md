Title: Office 365 security baseline adds macro signing, JScript protection
Date: 2021-05-01T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: office-365-security-baseline-adds-macro-signing-jscript-protection

[Source](https://www.bleepingcomputer.com/news/security/office-365-security-baseline-adds-macro-signing-jscript-protection/){:target="_blank" rel="noopener"}

> Microsoft has updated the security baseline for Microsoft 365 Apps for enterprise (formerly Office 365 Professional Plus) to include protection from JScript code execution attacks and unsigned macros. [...]
