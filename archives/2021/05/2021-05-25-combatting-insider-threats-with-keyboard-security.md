Title: Combatting Insider Threats with Keyboard Security
Date: 2021-05-25T15:20:45+00:00
Author: Dale Ludwig
Category: Threatpost
Tags: InfoSec Insider;Vulnerabilities;Web Security
Slug: combatting-insider-threats-with-keyboard-security

[Source](https://threatpost.com/insider-threats-keyboard-security/166449/){:target="_blank" rel="noopener"}

> Dale Ludwig, business development manager at Cherry Americas, discusses advances in hardware-based security that can enhance modern cyber-defenses. [...]
