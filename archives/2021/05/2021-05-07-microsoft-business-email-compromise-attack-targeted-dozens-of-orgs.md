Title: Microsoft: Business email compromise attack targeted dozens of orgs
Date: 2021-05-07T16:56:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-business-email-compromise-attack-targeted-dozens-of-orgs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-business-email-compromise-attack-targeted-dozens-of-orgs/){:target="_blank" rel="noopener"}

> Microsoft detected a large-scale business email compromise (BEC) campaign that targeted more than 120 organization using typo-squatted domains registered days before the attacks began. [...]
