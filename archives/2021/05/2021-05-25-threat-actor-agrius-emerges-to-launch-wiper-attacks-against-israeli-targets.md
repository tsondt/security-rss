Title: Threat Actor ‘Agrius’ Emerges to Launch Wiper Attacks Against Israeli Targets
Date: 2021-05-25T20:26:05+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware
Slug: threat-actor-agrius-emerges-to-launch-wiper-attacks-against-israeli-targets

[Source](https://threatpost.com/agrius-wiper-attacks-israeli-targets/166474/){:target="_blank" rel="noopener"}

> The group is using ransomware intended to make its espionage and destruction efforts appear financially motivated. [...]
