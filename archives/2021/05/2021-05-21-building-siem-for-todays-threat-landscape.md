Title: Building SIEM for Today’s Threat Landscape
Date: 2021-05-21T18:25:13+00:00
Author: Sivan Tehila
Category: Threatpost
Tags: Cloud Security;Hacks;InfoSec Insider;Web Security
Slug: building-siem-for-todays-threat-landscape

[Source](https://threatpost.com/building-siem-threat-landscape/166390/){:target="_blank" rel="noopener"}

> Sivan Tehila, cybersecurity strategist at Perimeter 81, discusses the elements involved in creating a modern SIEM strategy for remote work and cloud-everything. [...]
