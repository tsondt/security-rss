Title: Tech industry quietly patches FragAttacks Wi-Fi flaws that leak data, weaken security
Date: 2021-05-12T00:58:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: tech-industry-quietly-patches-fragattacks-wi-fi-flaws-that-leak-data-weaken-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/12/krack_hack_wifi/){:target="_blank" rel="noopener"}

> Dozen design, implementation blunders date back 24 years A dozen Wi-Fi design and implementation flaws make it possible for miscreants to steal transmitted data and bypass firewalls to attack devices on home networks, according to security researcher Mathy Vanhoef.... [...]
