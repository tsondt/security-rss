Title: Deepfake Attacks Are About to Surge, Experts Warn
Date: 2021-05-03T17:51:14+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: deepfake-attacks-are-about-to-surge-experts-warn

[Source](https://threatpost.com/deepfake-attacks-surge-experts-warn/165798/){:target="_blank" rel="noopener"}

> New deepfake products and services are cropping up across the Dark Web. [...]
