Title: Can Nanotech Secure IoT Devices From the Inside-Out?
Date: 2021-05-19T20:24:50+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: IoT;Malware;Mobile Security;RSAC;Vulnerabilities
Slug: can-nanotech-secure-iot-devices-from-the-inside-out

[Source](https://threatpost.com/nanotech-secure-iot-devices/166324/){:target="_blank" rel="noopener"}

> Work's being done with uber-lightweight nanoagents on every IoT device to stop malicious behavior, such as a scourge of botnet attacks, among other threats. [...]
