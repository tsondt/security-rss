Title: <span>What’s TsuNAME? DDoS attack vector threatens authoritative DNS servers</span>
Date: 2021-05-11T11:44:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: whats-tsuname-ddos-attack-vector-threatens-authoritative-dns-servers

[Source](https://portswigger.net/daily-swig/whats-tsuname-ddos-attack-vector-threatens-authoritative-dns-servers){:target="_blank" rel="noopener"}

> Researchers release open source tool to narrow down cyclic dependencies-related threat [...]
