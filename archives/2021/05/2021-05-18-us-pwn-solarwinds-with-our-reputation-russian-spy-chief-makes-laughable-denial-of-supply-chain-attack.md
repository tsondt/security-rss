Title: Us? Pwn SolarWinds? With our reputation? Russian spy chief makes laughable denial of supply chain attack
Date: 2021-05-18T17:42:17+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: us-pwn-solarwinds-with-our-reputation-russian-spy-chief-makes-laughable-denial-of-supply-chain-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/18/russian_spymaster_solarwinds/){:target="_blank" rel="noopener"}

> Hint: He doesn't care if you personally think it's rubbish, and here's why A Russian spymaster has denied that his agency carried out the infamous SolarWinds supply chain attack in a public relations move worthy of the Internet Research Agency.... [...]
