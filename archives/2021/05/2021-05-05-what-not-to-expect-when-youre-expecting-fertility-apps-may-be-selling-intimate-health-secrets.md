Title: What not to expect when you're expecting: Fertility apps may be selling intimate health secrets
Date: 2021-05-05T07:32:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: what-not-to-expect-when-youre-expecting-fertility-apps-may-be-selling-intimate-health-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/05/fertility_apps_leak_personal_information/){:target="_blank" rel="noopener"}

> Majority aren't GDPR compliant and Google Play categorises them badly, leading to lax practices Hundreds of millions of women turn to fertility apps to conceive or prevent pregnancy, and according to a new study those apps may leak very personal information including miscarriages, abortions, sexual history, potential infertility and pregnancy.... [...]
