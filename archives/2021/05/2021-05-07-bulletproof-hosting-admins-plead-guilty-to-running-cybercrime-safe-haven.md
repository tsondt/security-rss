Title: Bulletproof hosting admins plead guilty to running cybercrime safe haven
Date: 2021-05-07T15:29:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: bulletproof-hosting-admins-plead-guilty-to-running-cybercrime-safe-haven

[Source](https://www.bleepingcomputer.com/news/security/bulletproof-hosting-admins-plead-guilty-to-running-cybercrime-safe-haven/){:target="_blank" rel="noopener"}

> Four individuals from Eastern Europe are facing 20 years in prison for Racketeer Influenced Corrupt Organization (RICO) charges after pleading guilty to running a bulletproof hosting service as a safe haven for cybercrime operations targeting US entities. [...]
