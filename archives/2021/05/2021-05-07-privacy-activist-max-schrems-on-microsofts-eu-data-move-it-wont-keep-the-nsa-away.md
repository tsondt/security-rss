Title: Privacy activist Max Schrems on Microsoft's EU data move: It won't keep the NSA away
Date: 2021-05-07T15:20:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: privacy-activist-max-schrems-on-microsofts-eu-data-move-it-wont-keep-the-nsa-away

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/07/schrems_slams_microsoft_eu_data/){:target="_blank" rel="noopener"}

> Software giant vows data processing of EU cloud services to stay in EU, which means that currently... Microsoft has announced plans to ensure data processing of EU cloud services within the borders of the political bloc in a move that expert observers claim reveals problems with the firm's existing setup.... [...]
