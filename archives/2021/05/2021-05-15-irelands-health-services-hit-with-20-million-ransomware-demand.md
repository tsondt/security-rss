Title: Ireland’s Health Services hit with $20 million ransomware demand
Date: 2021-05-15T13:40:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: irelands-health-services-hit-with-20-million-ransomware-demand

[Source](https://www.bleepingcomputer.com/news/security/ireland-s-health-services-hit-with-20-million-ransomware-demand/){:target="_blank" rel="noopener"}

> Ireland's health service, the HSE, says they are refusing to pay a $20 million ransom demand to the Conti ransomware gang after the hackers encrypted computers and disrupted health care in the country. [...]
