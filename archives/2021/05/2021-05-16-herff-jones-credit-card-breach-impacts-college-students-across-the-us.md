Title: Herff Jones credit card breach impacts college students across the US
Date: 2021-05-16T09:39:02-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: herff-jones-credit-card-breach-impacts-college-students-across-the-us

[Source](https://www.bleepingcomputer.com/news/security/herff-jones-credit-card-breach-impacts-college-students-across-the-us/){:target="_blank" rel="noopener"}

> Graduating students from several universities in the U.S. have been reporting fraudulent transactions after using payment cards at popular cap and gown maker Herff Jones. [...]
