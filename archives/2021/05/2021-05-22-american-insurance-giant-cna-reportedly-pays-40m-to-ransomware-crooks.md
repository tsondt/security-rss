Title: American insurance giant CNA reportedly pays $40m to ransomware crooks
Date: 2021-05-22T10:22:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: american-insurance-giant-cna-reportedly-pays-40m-to-ransomware-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/22/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Stalkerware even more scummy and ExifTool needs patching In brief CNA Finaincial, the US insurance conglomerate, has apparently paid $40m to ransomware operators to gets its files back.... [...]
