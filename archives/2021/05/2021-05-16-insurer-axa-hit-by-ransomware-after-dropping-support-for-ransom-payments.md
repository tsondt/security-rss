Title: Insurer AXA hit by ransomware after dropping support for ransom payments
Date: 2021-05-16T12:24:32-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: insurer-axa-hit-by-ransomware-after-dropping-support-for-ransom-payments

[Source](https://www.bleepingcomputer.com/news/security/insurer-axa-hit-by-ransomware-after-dropping-support-for-ransom-payments/){:target="_blank" rel="noopener"}

> Branches of insurance giant AXA based in Thailand, Malaysia, Hong Kong, and the Philippines have been struck by a ransomware cyber attack. As seen by BleepingComputer yesterday, the Avaddon ransomware group claimed on their leak site that they had stolen over 3 TB of sensitive data from AXA's Asian operations. [...]
