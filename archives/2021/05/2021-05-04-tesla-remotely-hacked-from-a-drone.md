Title: Tesla Remotely Hacked from a Drone
Date: 2021-05-04T14:41:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cars;drones;hacking;vulnerabilities;Wi-Fi
Slug: tesla-remotely-hacked-from-a-drone

[Source](https://www.schneier.com/blog/archives/2021/05/tesla-remotely-hacked-from-a-drone.html){:target="_blank" rel="noopener"}

> This is an impressive hack: Security researchers Ralf-Philipp Weinmann of Kunnamon, Inc. and Benedikt Schmotzle of Comsecuris GmbH have found remote zero-click security vulnerabilities in an open-source software component (ConnMan) used in Tesla automobiles that allowed them to compromise parked cars and control their infotainment systems over WiFi. It would be possible for an attacker to unlock the doors and trunk, change seat positions, both steering and acceleration modes — in short, pretty much what a driver pressing various buttons on the console can do. This attack does not yield drive control of the car though. That last sentence is [...]
