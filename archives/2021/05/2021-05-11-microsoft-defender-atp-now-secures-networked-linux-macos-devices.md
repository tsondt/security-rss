Title: Microsoft Defender ATP now secures networked Linux, macOS devices
Date: 2021-05-11T13:01:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-atp-now-secures-networked-linux-macos-devices

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-atp-now-secures-networked-linux-macos-devices/){:target="_blank" rel="noopener"}

> Microsoft has added support for identifying and assessing the security configurations of Linux and macOS endpoints on enterprise networks using Microsoft Defender for Endpoint (previously Microsoft Defender Advanced Threat Protection). [...]
