Title: Contract killer: Certified PDFs can be secretly tampered with during the signing process, boffins find
Date: 2021-05-26T06:46:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: contract-killer-certified-pdfs-can-be-secretly-tampered-with-during-the-signing-process-boffins-find

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/26/pdf_certificate_flaw/){:target="_blank" rel="noopener"}

> 24 out of 26 tools vulnerable – with bonus JavaScript attack for Adobe A pair of techniques to surreptitiously alter the content of certified PDFs have been detailed by researchers in Germany.... [...]
