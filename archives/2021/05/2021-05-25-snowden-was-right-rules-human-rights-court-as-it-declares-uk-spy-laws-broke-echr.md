Title: Snowden was right, rules human rights court as it declares UK spy laws broke ECHR
Date: 2021-05-25T17:08:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: snowden-was-right-rules-human-rights-court-as-it-declares-uk-spy-laws-broke-echr

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/25/echr_ruling_uk_ripa_surveillance_laws/){:target="_blank" rel="noopener"}

> Says privacy and freedom of expression breached, but upholds sending surveillance product to foreign countries Surveillance laws permitting GCHQ to operate its Tempora dragnet mass surveillance system broke the law, the European Court of Human Rights has ruled.... [...]
