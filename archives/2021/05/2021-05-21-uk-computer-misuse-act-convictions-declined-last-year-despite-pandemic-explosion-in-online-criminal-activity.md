Title: UK Computer Misuse Act convictions declined last year despite pandemic explosion in online criminal activity
Date: 2021-05-21T15:32:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-computer-misuse-act-convictions-declined-last-year-despite-pandemic-explosion-in-online-criminal-activity

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/21/computer_misuse_act_2020_prosecutions/){:target="_blank" rel="noopener"}

> And less than a fifth of CMA crims copped jail terms Prosecutions under the UK's Computer Misuse Act (CMA) dropped by a fifth in 2020 even as conviction rates soared to 95 per cent during the year of the pandemic, new statistics have revealed.... [...]
