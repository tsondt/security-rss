Title: Pulse Secure VPNs Get a Fix for Critical Zero-Day Bugs
Date: 2021-05-04T17:42:30+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Vulnerabilities;Web Security
Slug: pulse-secure-vpns-get-a-fix-for-critical-zero-day-bugs

[Source](https://threatpost.com/pulse-secure-vpns-fix-critical-zero-day-bugs/165850/){:target="_blank" rel="noopener"}

> The security flaw tracked as CVE-2021-22893 is being used by at least two APTs likely linked to China, to attack U.S. defense targets among others. [...]
