Title: PHP package manager flaw left millions of web apps open to abuse
Date: 2021-05-03T14:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: php-package-manager-flaw-left-millions-of-web-apps-open-to-abuse

[Source](https://portswigger.net/daily-swig/php-package-manager-flaw-left-millions-of-web-apps-open-to-abuse){:target="_blank" rel="noopener"}

> Fix released for decade-old supply chain vulnerability impacting Composer [...]
