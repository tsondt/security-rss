Title: Major U.S. Pipeline Crippled in Ransomware Attack
Date: 2021-05-08T19:28:28+00:00
Author: Tom Spring
Category: Threatpost
Tags: Malware
Slug: major-us-pipeline-crippled-in-ransomware-attack

[Source](https://threatpost.com/pipeline-crippled-ransomware/165963/){:target="_blank" rel="noopener"}

> Colonial Pipeline Company says it is the victim of a cyberattack that forced the major provider of liquid fuels to the East Coast to temporarily halted all pipeline operations. [...]
