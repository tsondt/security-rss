Title: U.S. Agency for Global Media data breach caused by a phishing attack
Date: 2021-05-04T19:54:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: us-agency-for-global-media-data-breach-caused-by-a-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/us-agency-for-global-media-data-breach-caused-by-a-phishing-attack/){:target="_blank" rel="noopener"}

> The U.S. Agency for Global Media (USAGM) has disclosed a data breach that exposed the personal information of current and former employees and their beneficiaries. [...]
