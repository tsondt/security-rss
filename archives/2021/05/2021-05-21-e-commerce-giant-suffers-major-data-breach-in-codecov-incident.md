Title: E-commerce giant suffers major data breach in Codecov incident
Date: 2021-05-21T05:26:06-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: e-commerce-giant-suffers-major-data-breach-in-codecov-incident

[Source](https://www.bleepingcomputer.com/news/security/e-commerce-giant-suffers-major-data-breach-in-codecov-incident/){:target="_blank" rel="noopener"}

> E-commerce platform Mercari has disclosed a major data breach incident that occurred due to exposure from the Codecov supply-chain attack. Mercari is a publicly traded Japanese company and an online marketplace that has recently expanded its operations to the United States and the United Kingdom. [...]
