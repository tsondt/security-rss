Title: Indonesia’s national health insurance scheme leaks at least a million citizens' records
Date: 2021-05-24T02:28:52+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: indonesias-national-health-insurance-scheme-leaks-at-least-a-million-citizens-records

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/24/indonesia_health_data_breach/){:target="_blank" rel="noopener"}

> Tech Ministry trying to figure out just how much personal info has made it onto notorious RaidForums data-mart Indonesia’s government has admitted to leaks of personal data from the agency that runs its national health insurance scheme... [...]
