Title: US declares emergency after ransomware shuts oil pipeline that pumps 100 million gallons a day
Date: 2021-05-10T00:15:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: us-declares-emergency-after-ransomware-shuts-oil-pipeline-that-pumps-100-million-gallons-a-day

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/10/colonial_pipeline_ransomware/){:target="_blank" rel="noopener"}

> Oil transport by road allowed after Colonial Pipeline goes down, operator says recovery is under way but offers no recovery date One of the USA’s largest oil pipelines has been shut by ransomware, leading the nation's Federal Motor Carrier Safety Administration to issue a regional emergency declaration permitting the transport of fuel by road.... [...]
