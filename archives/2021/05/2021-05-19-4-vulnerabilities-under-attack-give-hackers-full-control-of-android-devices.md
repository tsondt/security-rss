Title: 4 vulnerabilities under attack give hackers full control of Android devices
Date: 2021-05-19T20:45:58+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;android;exploits;vulnerabilities;zerodays
Slug: 4-vulnerabilities-under-attack-give-hackers-full-control-of-android-devices

[Source](https://arstechnica.com/?p=1766173){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Unknown hackers have been exploiting four Android vulnerabilities that allow the execution of malicious code that can take complete control of devices, Google warned on Wednesday. All four of the vulnerabilities were disclosed two weeks ago in Google’s Android Security Bulletin for May. Google has released security updates to device manufacturers, who are then responsible for distributing the patches to users. Google’s May 3 bulletin initially didn’t report that any of the roughly 50 vulnerabilities it covered were under active exploitation. On Wednesday, Google updated the advisory to say that there are “indications” that four [...]
