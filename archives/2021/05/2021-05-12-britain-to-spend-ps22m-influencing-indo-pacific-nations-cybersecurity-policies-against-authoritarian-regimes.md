Title: Britain to spend £22m influencing Indo-Pacific nations' cybersecurity policies against 'authoritarian regimes'
Date: 2021-05-12T12:15:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: britain-to-spend-ps22m-influencing-indo-pacific-nations-cybersecurity-policies-against-authoritarian-regimes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/12/cyberuk_dominic_raab_22m_indopacific/){:target="_blank" rel="noopener"}

> So says Foreign Secretary in lacklustre speech to NCSC faithful CyberUK 21 Britain is to spend £22m on training African and Indo-Pacific nations to stave off cyber influences from "authoritarian regimes", foreign secretary Dominic Raab said today.... [...]
