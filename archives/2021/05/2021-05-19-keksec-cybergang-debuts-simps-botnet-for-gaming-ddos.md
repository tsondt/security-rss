Title: Keksec Cybergang Debuts Simps Botnet for Gaming DDoS
Date: 2021-05-19T16:53:32+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Malware;Vulnerabilities;Web Security
Slug: keksec-cybergang-debuts-simps-botnet-for-gaming-ddos

[Source](https://threatpost.com/keksec-simps-botnet-gaming-ddos/166306/){:target="_blank" rel="noopener"}

> The newly discovered malware infects IoT devices in tandem with the prolific Gafgyt botnet, using known security vulnerabilities. [...]
