Title: Telegram Fraudsters Ramp Up Forged COVID-19 Vaccine Card Sales
Date: 2021-05-12T19:51:46+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Newsmaker Interviews;Privacy;Web Security
Slug: telegram-fraudsters-ramp-up-forged-covid-19-vaccine-card-sales

[Source](https://threatpost.com/telegram-forged-covid-19-vaccine-cards/166093/){:target="_blank" rel="noopener"}

> A new type of fraud is spiking across the platform: Selling fake vax records to people who want to lie their way into places where proof of vaccine is required. [...]
