Title: US announces new security directive after critical pipeline hack
Date: 2021-05-27T09:48:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-announces-new-security-directive-after-critical-pipeline-hack

[Source](https://www.bleepingcomputer.com/news/security/us-announces-new-security-directive-after-critical-pipeline-hack/){:target="_blank" rel="noopener"}

> The US Department of Homeland Security (DHS) has announced new pipeline cybersecurity requirements after the largest fuel pipeline in the United States was forced to shut down operations in early May following a ransomware attack. [...]
