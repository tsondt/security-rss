Title: Critical 21Nails Exim bugs expose millions of servers to attacks
Date: 2021-05-04T11:46:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Linux
Slug: critical-21nails-exim-bugs-expose-millions-of-servers-to-attacks

[Source](https://www.bleepingcomputer.com/news/security/critical-21nails-exim-bugs-expose-millions-of-servers-to-attacks/){:target="_blank" rel="noopener"}

> Newly discovered critical vulnerabilities in the Exim message transfer agent (MTA) software allow unauthenticated remote attackers to execute arbitrary code and gain root privilege on mail servers with default or common configurations. [...]
