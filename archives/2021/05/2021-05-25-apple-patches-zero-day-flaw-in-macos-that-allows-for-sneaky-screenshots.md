Title: Apple Patches Zero-Day Flaw in MacOS that Allows for Sneaky Screenshots
Date: 2021-05-25T12:25:55+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware;Privacy
Slug: apple-patches-zero-day-flaw-in-macos-that-allows-for-sneaky-screenshots

[Source](https://threatpost.com/apple-patches-zero-day-flaw-in-macos-that-allows-for-sneaky-screenshots/166428/){:target="_blank" rel="noopener"}

> Security researchers at Jamf discovered the XCSSET malware exploiting the vulnerability, patched in Big Sur 11.4, to take photos of people’s computer screens without their knowing. [...]
