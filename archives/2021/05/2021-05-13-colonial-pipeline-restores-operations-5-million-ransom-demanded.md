Title: Colonial Pipeline restores operations, $5 million ransom demanded
Date: 2021-05-13T13:54:54-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: colonial-pipeline-restores-operations-5-million-ransom-demanded

[Source](https://www.bleepingcomputer.com/news/security/colonial-pipeline-restores-operations-5-million-ransom-demanded/){:target="_blank" rel="noopener"}

> Colonial Pipeline Company has recovered quickly from the ransomware attack suffered less than a week ago and expects all its infrastructure to be fully operational today. [...]
