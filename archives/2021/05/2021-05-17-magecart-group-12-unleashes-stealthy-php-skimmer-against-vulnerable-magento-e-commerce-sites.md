Title: Magecart Group 12 unleashes stealthy PHP skimmer against vulnerable Magento e-commerce sites
Date: 2021-05-17T15:32:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: magecart-group-12-unleashes-stealthy-php-skimmer-against-vulnerable-magento-e-commerce-sites

[Source](https://portswigger.net/daily-swig/magecart-group-12-unleashes-stealthy-php-skimmer-against-vulnerable-magento-e-commerce-sites){:target="_blank" rel="noopener"}

> Server-side requests to malicious domain conceal malware from endpoint security tools [...]
