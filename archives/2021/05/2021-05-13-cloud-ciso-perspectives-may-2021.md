Title: Cloud CISO Perspectives: May 2021
Date: 2021-05-13T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud Platform;Perspectives;Identity & Security
Slug: cloud-ciso-perspectives-may-2021

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-may-2021/){:target="_blank" rel="noopener"}

> May is a big month for the security industry. It's been over a year since we gathered for RSA in San Francisco for one of 2020’s last major in-person events. While we likely won’t be together in person this year, it's an important time for the security community to come together and reflect on many accomplishments, and to consider the challenges still ahead of us. As the world focuses on security incidents and all the risks that still need resolving, it is important to stand back, on occasion, and also note that immense progress has been made by large numbers [...]
