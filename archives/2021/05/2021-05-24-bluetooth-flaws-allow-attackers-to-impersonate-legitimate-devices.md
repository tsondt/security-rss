Title: Bluetooth flaws allow attackers to impersonate legitimate devices
Date: 2021-05-24T14:43:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: bluetooth-flaws-allow-attackers-to-impersonate-legitimate-devices

[Source](https://www.bleepingcomputer.com/news/security/bluetooth-flaws-allow-attackers-to-impersonate-legitimate-devices/){:target="_blank" rel="noopener"}

> Attackers could abuse vulnerabilities discovered in the Bluetooth Core and Mesh Profile specifications to impersonate legitimate devices during the pairing process and launch man-in-the-middle (MitM) attacks. [...]
