Title: Nine additional AWS cloud service offerings authorized by DISA
Date: 2021-05-06T16:46:35+00:00
Author: Tyler Harding
Category: AWS Security
Tags: Defense;Foundational (100);Government;Public Sector;Security, Identity, & Compliance;authorization;Cloud security;Department of Defense (DoD);DISA;DoD;DoD CC SRG;IL4;IL5;IL6;Security Blog
Slug: nine-additional-aws-cloud-service-offerings-authorized-by-disa

[Source](https://aws.amazon.com/blogs/security/nine-additional-aws-cloud-service-offerings-authorized-by-disa/){:target="_blank" rel="noopener"}

> I’m excited to share that the Defense Information Systems Agency (DISA) has authorized three additional Amazon Web Services (AWS) services at Impact Level (IL) 4 and IL 5 in the AWS GovCloud (US) Regions, as well as five additional AWS services and one feature at IL 6 in the AWS Secret Region, under the Department [...]
