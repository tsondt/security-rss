Title: Colonial Pipeline paid a $5 million ransom—and kept a vicious cycle turning
Date: 2021-05-15T10:00:05+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;colonial pipeline;ransomware
Slug: colonial-pipeline-paid-a-5-million-ransom-and-kept-a-vicious-cycle-turning

[Source](https://arstechnica.com/?p=1765276){:target="_blank" rel="noopener"}

> Enlarge (credit: Sean Rayford | Getty Images) Nearly a week after a ransomware attack led Colonial Pipeline to halt fuel distribution on the East Coast, reports emerged on Friday that the company paid a 75 bitcoin ransom—worth as much as $5 million, depending on the time of payment—in an attempt to restore service more quickly. And while the company was able to restart operations Wednesday night, the decision to give in to hackers' demands will only embolden other groups going forward. Real progress against the ransomware epidemic, experts say, will require more companies to say no. Not to say that [...]
