Title: Ransomware gangs have leaked the stolen data of 2,100 companies so far
Date: 2021-05-08T10:23:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gangs-have-leaked-the-stolen-data-of-2100-companies-so-far

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-have-leaked-the-stolen-data-of-2-100-companies-so-far/){:target="_blank" rel="noopener"}

> Since 2019, ransomware gangs have leaked the stolen data for 2,103 companies on dark web data leaks sites. [...]
