Title: GitHub Prepares to Move Beyond Passwords
Date: 2021-05-11T19:46:27+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Privacy;Web Security
Slug: github-prepares-to-move-beyond-passwords

[Source](https://threatpost.com/github-security-keys-passwords/166054/){:target="_blank" rel="noopener"}

> GitHub adds support for FIDO2 security keys for Git over SSH to fend off account hijacking and further its plan to stick a fork in the security bane of passwords. [...]
