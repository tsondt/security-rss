Title: US introduces bills to secure critical infrastructure from cyber attacks
Date: 2021-05-19T10:56:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-introduces-bills-to-secure-critical-infrastructure-from-cyber-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-introduces-bills-to-secure-critical-infrastructure-from-cyber-attacks/){:target="_blank" rel="noopener"}

> The US House Committee on Homeland Security has passed five bipartisan bills on Monday to bolster defense capabilities against cyber attacks targeting US organizations and critical infrastructure. [...]
