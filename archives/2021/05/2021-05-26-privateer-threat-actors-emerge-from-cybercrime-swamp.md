Title: ‘Privateer’ Threat Actors Emerge from Cybercrime Swamp
Date: 2021-05-26T12:01:14+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks
Slug: privateer-threat-actors-emerge-from-cybercrime-swamp

[Source](https://threatpost.com/privateer-threat-actors-emerge/166483/){:target="_blank" rel="noopener"}

> ‘Privateers’ aren’t necessarily state-sponsored, but they have some form of government protection while promoting their own financially-motivated criminal agenda, according to Cisco Talos. [...]
