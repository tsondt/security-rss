Title: A student pirating software led to a full-blown Ryuk ransomware attack
Date: 2021-05-06T12:08:56-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: a-student-pirating-software-led-to-a-full-blown-ryuk-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/a-student-pirating-software-led-to-a-full-blown-ryuk-ransomware-attack/){:target="_blank" rel="noopener"}

> A student's attempt to pirate an expensive data visualization software led to a full-blown Ryuk ransomware attack at a European biomolecular research institute. [...]
