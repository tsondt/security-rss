Title: Magecart Goes Server-Side in Latest Tactics Changeup
Date: 2021-05-17T21:46:14+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: magecart-goes-server-side-in-latest-tactics-changeup

[Source](https://threatpost.com/magecart-server-side-itactics-changeup/166242/){:target="_blank" rel="noopener"}

> The latest Magecart iteration is finding success with a new PHP web shell skimmer. [...]
