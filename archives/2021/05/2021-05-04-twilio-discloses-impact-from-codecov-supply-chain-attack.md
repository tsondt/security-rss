Title: Twilio discloses impact from Codecov supply-chain attack
Date: 2021-05-04T12:39:15-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: twilio-discloses-impact-from-codecov-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/twilio-discloses-impact-from-codecov-supply-chain-attack/){:target="_blank" rel="noopener"}

> Cloud communications company Twilio has now disclosed that the recent Codecov supply-chain attack exposed a small number of Twilio's customer email addresses. [...]
