Title: Try This One Weird Trick Russian Hackers Hate
Date: 2021-05-17T14:14:01+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;Security Tools;Allison Nixon;Colonial Pipeline ransomware attack;Cybereason;DarkSide ransomware;fbi;Lance James;Microsoft Windows;rEvil;Sodinokibi;twitter;Unit221B
Slug: try-this-one-weird-trick-russian-hackers-hate

[Source](https://krebsonsecurity.com/2021/05/try-this-one-weird-trick-russian-hackers-hate/){:target="_blank" rel="noopener"}

> In a Twitter discussion last week on ransomware attacks, KrebsOnSecurity noted that virtually all ransomware strains have a built-in failsafe designed to cover the backsides of the malware purveyors: They simply will not install on a Microsoft Windows computer that already has one of many types of virtual keyboards installed — such as Russian or Ukrainian. So many readers had questions in response to the tweet that I thought it was worth a blog post exploring this one weird cyber defense trick. The Commonwealth of Independent States (CIS) more or less matches the exclusion list on an awful lot of [...]
