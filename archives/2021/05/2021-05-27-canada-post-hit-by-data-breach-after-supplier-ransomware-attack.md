Title: Canada Post hit by data breach after supplier ransomware attack
Date: 2021-05-27T14:08:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: canada-post-hit-by-data-breach-after-supplier-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/canada-post-hit-by-data-breach-after-supplier-ransomware-attack/){:target="_blank" rel="noopener"}

> Canada Post has informed 44 of its large commercial customers that a ransomware attack on a third-party service provider exposed shipping information for their customers. [...]
