Title: Uncle Sam wants 'ethical hackers' to crack its planetary defenses, but don't expect a pay-day from this bug bounty
Date: 2021-05-10T11:32:05+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: uncle-sam-wants-ethical-hackers-to-crack-its-planetary-defenses-but-dont-expect-a-pay-day-from-this-bug-bounty

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/10/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Student cripples EU bio lab and IRS goes after cryptocurrency In brief The United States' Department of Defense has opened up all of its publicly facing systems and apps to investigation under a bug bounty program.... [...]
