Title: Fake Chrome App Anchors Rapidly Worming ‘Smish’ Cyberattack
Date: 2021-05-11T18:01:11+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: fake-chrome-app-anchors-rapidly-worming-smish-cyberattack

[Source](https://threatpost.com/fake-chrome-app-worming-smish-cyberattack/166038/){:target="_blank" rel="noopener"}

> An ingenious attack on Android devices self-propagates, with the potential for a range of damage. [...]
