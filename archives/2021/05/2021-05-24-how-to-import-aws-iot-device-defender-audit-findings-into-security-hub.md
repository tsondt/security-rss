Title: How to import AWS IoT Device Defender audit findings into Security Hub
Date: 2021-05-24T16:41:39+00:00
Author: Joaquin Manuel Rinaudo
Category: AWS Security
Tags: Advanced (300);AWS IoT Device Defender;AWS Security Hub;Security, Identity, & Compliance;IoT;IoT Device Defender;IoT Security;Security Blog
Slug: how-to-import-aws-iot-device-defender-audit-findings-into-security-hub

[Source](https://aws.amazon.com/blogs/security/how-to-import-aws-iot-device-defender-audit-findings-into-security-hub/){:target="_blank" rel="noopener"}

> AWS Security Hub provides a comprehensive view of the security alerts and security posture in your accounts. In this blog post, we show how you can import AWS IoT Device Defender audit findings into Security Hub. You can then view and organize Internet of Things (IoT) security findings in Security Hub together with findings from [...]
