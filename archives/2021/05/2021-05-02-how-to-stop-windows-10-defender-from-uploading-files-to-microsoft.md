Title: How to stop Windows 10 Defender from uploading files to Microsoft
Date: 2021-05-02T09:44:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: how-to-stop-windows-10-defender-from-uploading-files-to-microsoft

[Source](https://www.bleepingcomputer.com/news/security/how-to-stop-windows-10-defender-from-uploading-files-to-microsoft/){:target="_blank" rel="noopener"}

> Like other antivirus programs, Microsoft Defender will upload suspicious files to Microsoft to determine if they are malicious. However, some consider this a privacy risk and would rather have their files stay on their computer than being uploaded to a third party. [...]
