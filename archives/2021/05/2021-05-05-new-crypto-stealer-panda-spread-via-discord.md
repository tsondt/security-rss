Title: New Crypto-Stealer ‘Panda’ Spread via Discord
Date: 2021-05-05T21:03:27+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware
Slug: new-crypto-stealer-panda-spread-via-discord

[Source](https://threatpost.com/panda-stealer-crypto-wallets-discord/165898/){:target="_blank" rel="noopener"}

> PandaStealer is delivered in rigged Excel files masquerading as business quotes, bent on stealing victims' cryptocurrency and other info. [...]
