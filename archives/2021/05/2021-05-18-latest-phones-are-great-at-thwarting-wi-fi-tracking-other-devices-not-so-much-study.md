Title: Latest phones are great at thwarting Wi-Fi tracking. Other devices, not so much – study
Date: 2021-05-18T07:29:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: latest-phones-are-great-at-thwarting-wi-fi-tracking-other-devices-not-so-much-study

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/18/wifi_tracking_failures/){:target="_blank" rel="noopener"}

> Boffins find increasing MAC address randomization protection with mobiles In 2017, US Naval Academy researchers found that MAC address randomization in mobile devices was largely worthless as a privacy defense. Three years later, the same research group took another look and found that while there's been meaningful improvement, many phones still fail to effectively prevent MAC address-based tracking.... [...]
