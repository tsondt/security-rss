Title: VMware reveals critical vCenter hole it says ‘needs to be considered at once’
Date: 2021-05-26T02:04:18+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: vmware-reveals-critical-vcenter-hole-it-says-needs-to-be-considered-at-once

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/26/vmware_vcenter_bug/){:target="_blank" rel="noopener"}

> Unauthenticated remote code execution possible thanks to vSphere Client bug VMware has revealed a critical bug that can be exploited to achieve unauthenticated remote code execution in the very core of a virtualised system – vCenter Server.... [...]
