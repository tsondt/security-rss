Title: Fujitsu pulls ProjectWEB tool offline after apparent supply chain attack sees Japanese infosec agency data stolen
Date: 2021-05-27T12:29:39+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: fujitsu-pulls-projectweb-tool-offline-after-apparent-supply-chain-attack-sees-japanese-infosec-agency-data-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/27/fujitsu_projectweb_supply_chain_attack/){:target="_blank" rel="noopener"}

> No sign of ransomware - or attacker's identity, so far A Fujitsu project management suite is causing red faces at the Japanese company’s HQ after “unauthorised access” resulted in data being stolen from government agencies, local reports say.... [...]
