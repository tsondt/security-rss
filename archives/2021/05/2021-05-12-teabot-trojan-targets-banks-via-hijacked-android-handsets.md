Title: TeaBot Trojan Targets Banks via Hijacked Android Handsets
Date: 2021-05-12T12:41:38+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security
Slug: teabot-trojan-targets-banks-via-hijacked-android-handsets

[Source](https://threatpost.com/teabot-trojan-targets-banks-android/166075/){:target="_blank" rel="noopener"}

> Malware first observed in Italy can steal victims’ credentials and SMS messages as well as livestream device screens on demand. [...]
