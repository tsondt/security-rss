Title: 200K Veterans’ Medical Records May Have Been Stolen by Ransomware Gang
Date: 2021-05-11T15:34:38+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Malware;Privacy
Slug: 200k-veterans-medical-records-may-have-been-stolen-by-ransomware-gang

[Source](https://threatpost.com/veterans-medical-records-ransomware/166025/){:target="_blank" rel="noopener"}

> Analyst finds ransomware evidence, despite a contractor's denial of compromise. [...]
