Title: Microsoft: SolarWinds hackers target govt agencies from 24 countries
Date: 2021-05-28T08:08:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-solarwinds-hackers-target-govt-agencies-from-24-countries

[Source](https://www.bleepingcomputer.com/news/security/microsoft-solarwinds-hackers-target-govt-agencies-from-24-countries/){:target="_blank" rel="noopener"}

> The Microsoft Threat Intelligence Center (MSTIC) has discovered that the Russian-based SolarWinds hackers are behind an ongoing phishing campaign targeting government agencies worldwide. [...]
