Title: Hewlett Packard Enterprise Plugs Critical Bug in Edge Platform Tool
Date: 2021-05-03T18:22:23+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: hewlett-packard-enterprise-plugs-critical-bug-in-edge-platform-tool

[Source](https://threatpost.com/hewlett-packard-critical-bug-edge/165797/){:target="_blank" rel="noopener"}

> Researchers warned that unpatched versions of HPE’s Edgeline Infrastructure Manager are open to remote authentication-bypass attacks. [...]
