Title: How to confirm your automated Amazon EBS snapshots are still created after the TLS 1.2 uplift on AWS FIPS endpoints
Date: 2021-05-03T17:29:42+00:00
Author: Janelle Hopper
Category: AWS Security
Tags: Amazon Elastic Block Storage (EBS);Intermediate (200);Security, Identity, & Compliance;Amazon CloudWatch;Amazon CloudWatch Logs;Amazon EC2;Amazon Elastic Block Store;AWS GovCloud (US);AWS service FIPS endpoints;AWS Systems Manager;Compliance;Federal Information Processing Standard;FedRAMP;FIPS;FIPS 140-2;FIPS endpoints;Monitoring;NIST;Security Blog;TLS;Transport Layer Security
Slug: how-to-confirm-your-automated-amazon-ebs-snapshots-are-still-created-after-the-tls-12-uplift-on-aws-fips-endpoints

[Source](https://aws.amazon.com/blogs/security/tls-1-2-confirm-your-connections/){:target="_blank" rel="noopener"}

> We are happy to announce that all AWS Federal Information Processing Standard (FIPS) endpoints have been updated to only accept a minimum of Transport Layer Security (TLS) 1.2 connections. This ensures that our customers who run regulated workloads can meet FedRAMP compliance requirements that mandate a minimum of TLS 1.2 encryption for data in transit. Attempts [...]
