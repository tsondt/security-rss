Title: Largest U.S. pipeline shuts down operations after ransomware attack
Date: 2021-05-08T11:31:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: largest-us-pipeline-shuts-down-operations-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/largest-us-pipeline-shuts-down-operations-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Colonial Pipeline, the largest fuel pipeline in the United States, has shut down operations after suffering what is reported to be a ransomware attack. [...]
