Title: Double-Encrypting Ransomware
Date: 2021-05-21T13:50:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: encryption;extortion;malware;ransomware
Slug: double-encrypting-ransomware

[Source](https://www.schneier.com/blog/archives/2021/05/double-encrypting-ransomware.html){:target="_blank" rel="noopener"}

> This seems to be a new tactic : Emsisoft has identified two distinct tactics. In the first, hackers encrypt data with ransomware A and then re-encrypt that data with ransomware B. The other path involves what Emsisoft calls a “side-by-side encryption” attack, in which attacks encrypt some of an organization’s systems with ransomware A and others with ransomware B. In that case, data is only encrypted once, but a victim would need both decryption keys to unlock everything. The researchers also note that in this side-by-side scenario, attackers take steps to make the two distinct strains of ransomware look as [...]
