Title: US water filter supplier pays $200,000 to settle credit card data leak lawsuit
Date: 2021-05-21T15:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-water-filter-supplier-pays-200000-to-settle-credit-card-data-leak-lawsuit

[Source](https://portswigger.net/daily-swig/us-water-filter-supplier-pays-200-000-to-settle-credit-card-data-leak-lawsuit){:target="_blank" rel="noopener"}

> Filters Fast agrees to pay New York Attorney General [...]
