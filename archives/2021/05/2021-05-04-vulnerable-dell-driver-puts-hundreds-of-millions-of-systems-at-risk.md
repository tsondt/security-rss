Title: Vulnerable Dell driver puts hundreds of millions of systems at risk
Date: 2021-05-04T09:07:58-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: vulnerable-dell-driver-puts-hundreds-of-millions-of-systems-at-risk

[Source](https://www.bleepingcomputer.com/news/security/vulnerable-dell-driver-puts-hundreds-of-millions-of-systems-at-risk/){:target="_blank" rel="noopener"}

> A driver that's been pushed for the past 12 years to Dell computer devices for consumers and enterprises contains multiple vulnerabilities that could lead to increased privileges on the system. [...]
