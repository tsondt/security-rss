Title: Air India data breach impacts 4.5 million customers
Date: 2021-05-21T14:48:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: air-india-data-breach-impacts-45-million-customers

[Source](https://www.bleepingcomputer.com/news/security/air-india-data-breach-impacts-45-million-customers/){:target="_blank" rel="noopener"}

> Air India disclosed a data breach after personal information belonging to roughly 4.5 million of its customers was leaked two months following the hack of Passenger Service System provider SITA in February 2021. [...]
