Title: VMware Sounds Ransomware Alarm Over Critical Severity Bug
Date: 2021-05-26T19:45:50+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: vmware-sounds-ransomware-alarm-over-critical-severity-bug

[Source](https://threatpost.com/vmware-ransomware-alarm-critical-bug/166501/){:target="_blank" rel="noopener"}

> VMware’s virtualization management platform, vCenter Server, has a critical severity bug the company is urging customers to patch “as soon as possible”. [...]
