Title: DarkSide ransomware made $90 million in just nine months
Date: 2021-05-18T12:33:41-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: darkside-ransomware-made-90-million-in-just-nine-months

[Source](https://www.bleepingcomputer.com/news/security/darkside-ransomware-made-90-million-in-just-nine-months/){:target="_blank" rel="noopener"}

> The DarkSide ransomware gang has collected at least $90 million in ransoms paid by its victims over the past nine months to multiple Bitcoin wallets. [...]
