Title: Boss of ATM Skimming Syndicate Arrested in Mexico
Date: 2021-05-28T14:47:46+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: All About Skimmers;Ne'er-Do-Well News;El Universal;Florian Tudor;Intacash;OCCRP;Organized Crime and Corruption Reporting Project;The Riviera Maya Gang;Top Life Servicios
Slug: boss-of-atm-skimming-syndicate-arrested-in-mexico

[Source](https://krebsonsecurity.com/2021/05/boss-of-atm-skimming-syndicate-arrested-in-mexico/){:target="_blank" rel="noopener"}

> Florian “The Shark” Tudor, the alleged ringleader of a prolific ATM skimming gang that siphoned hundreds of millions of dollars from bank accounts of tourists visiting Mexico over the last eight years, was arrested in Mexico City on Thursday in response to an extradition warrant from a Romanian court. Florian Tudor, at a 2020 press conference in Mexico in which he asserted he was a legitimate businessman and not a mafia boss. Image: OCCRP. Tudor, a native of Craiova, Romania, moved to Mexico to set up Top Life Servicios, an ATM servicing company which managed a fleet of relatively new [...]
