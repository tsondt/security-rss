Title: Legacy data protection and modern ransomware? The odds are not in your favor
Date: 2021-05-24T19:00:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: legacy-data-protection-and-modern-ransomware-the-odds-are-not-in-your-favor

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/24/modern_ransomware_regcast/){:target="_blank" rel="noopener"}

> Join us to learn how to frustrate extortionists in the 2020s Webcast On the face of it, blunting a ransomware attack should be straightforward if you’ve got a solid data protection plan in place.... [...]
