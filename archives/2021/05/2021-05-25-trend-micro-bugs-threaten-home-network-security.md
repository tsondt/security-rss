Title: Trend Micro Bugs Threaten Home Network Security
Date: 2021-05-25T16:41:28+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: trend-micro-bugs-threaten-home-network-security

[Source](https://threatpost.com/trend-micro-bugs-home-network-security/166453/){:target="_blank" rel="noopener"}

> The security vendor's network management and threat protection station can open the door to code execution, DoS and potential PC takeovers. [...]
