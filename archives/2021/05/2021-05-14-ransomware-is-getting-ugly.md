Title: Ransomware Is Getting Ugly
Date: 2021-05-14T11:30:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cryptocurrency;cybercrime;doxing;police;ransomware
Slug: ransomware-is-getting-ugly

[Source](https://www.schneier.com/blog/archives/2021/05/ransomware-is-getting-ugly.html){:target="_blank" rel="noopener"}

> Modern ransomware has two dimensions: pay to get your data back, and pay not to have your data dumped on the Internet. The DC police are the victims of this ransomware, and the criminals have just posted personnel records — “including the results of psychological assessments and polygraph tests; driver’s license images; fingerprints; social security numbers; dates of birth; and residential, financial, and marriage histories” — for two dozen police officers. The negotiations don’t seem to be doing well. The criminals want $4M. The DC police offered them $100,000. The Colonial Pipeline is another current high-profile ransomware victim. (Brian Krebs [...]
