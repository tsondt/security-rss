Title: 4 new features to secure your Cloud Run services
Date: 2021-05-13T16:00:00+00:00
Author: Daniel Conde
Category: GCP Security
Tags: Identity & Security;Google Cloud Platform;Serverless
Slug: 4-new-features-to-secure-your-cloud-run-services

[Source](https://cloud.google.com/blog/products/serverless/improving-the-security-of-your-cloud-run-environment/){:target="_blank" rel="noopener"}

> Cloud Run makes developing and deploying containerized applications easier for developers. At the same time, Cloud Run services need to be secure. Today, we’re announcing several new ways for you to secure your Cloud Run environments: Mount secrets from Google Secret Manager Use Binary Authorization to ensure you only deploy trusted container images Use your own encryptions keys Get recommendations based on the principle of least privilege Let’s take a closer look at each of these new features. 1. Mount secrets from Google Secret Manager You might have previously stored API keys, passwords, certificates, and other sensitive data in environment [...]
