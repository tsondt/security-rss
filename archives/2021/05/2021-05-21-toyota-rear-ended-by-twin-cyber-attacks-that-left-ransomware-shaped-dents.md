Title: Toyota rear-ended by twin cyber attacks that left ransomware-shaped dents
Date: 2021-05-21T05:05:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: toyota-rear-ended-by-twin-cyber-attacks-that-left-ransomware-shaped-dents

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/21/toyota_cyber_attacks/){:target="_blank" rel="noopener"}

> Oh what a feeling, and in the same week as automaker announced new production pauses Toyota has admitted to a pair of cyber-attacks.... [...]
