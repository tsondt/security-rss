Title: Iranian hacking group targets Israel with wiper disguised as ransomware
Date: 2021-05-25T11:00:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: iranian-hacking-group-targets-israel-with-wiper-disguised-as-ransomware

[Source](https://www.bleepingcomputer.com/news/security/iranian-hacking-group-targets-israel-with-wiper-disguised-as-ransomware/){:target="_blank" rel="noopener"}

> An Iranian hacking group has been observed camouflaging destructive attacks against Israeli targets as ransomware attacks while maintaining access to victims' networks for months in what looks like an extensive espionage campaign. [...]
