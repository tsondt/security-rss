Title: Microsoft releases free online ‘playbooks’ to help businesses defend against cyber-attacks
Date: 2021-05-17T14:06:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: microsoft-releases-free-online-playbooks-to-help-businesses-defend-against-cyber-attacks

[Source](https://portswigger.net/daily-swig/microsoft-releases-online-playbooks-to-help-businesses-defend-against-cyber-attacks){:target="_blank" rel="noopener"}

> Tech giant provides advice for organizations on how to protect their networks [...]
