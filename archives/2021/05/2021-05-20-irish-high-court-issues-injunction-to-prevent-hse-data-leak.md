Title: Irish High Court issues injunction to prevent HSE data leak
Date: 2021-05-20T18:28:21-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: irish-high-court-issues-injunction-to-prevent-hse-data-leak

[Source](https://www.bleepingcomputer.com/news/security/irish-high-court-issues-injunction-to-prevent-hse-data-leak/){:target="_blank" rel="noopener"}

> The High Court of Ireland has issued an injunction against the Conti Ransomware gang, demanding that stolen HSE data be returned and not sold or published. [...]
