Title: SolarWinds hack: Nation-state attackers could have launched supply chain attack nine months before previously thought
Date: 2021-05-20T14:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: solarwinds-hack-nation-state-attackers-could-have-launched-supply-chain-attack-nine-months-before-previously-thought

[Source](https://portswigger.net/daily-swig/solarwinds-hack-nation-state-attackers-could-have-launched-supply-chain-attack-nine-months-before-previously-thought){:target="_blank" rel="noopener"}

> Company CEO sheds light on high-profile breach at RSA Conference 2021 [...]
