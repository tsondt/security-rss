Title: Malicious Office 365 Apps Are the Ultimate Insiders
Date: 2021-05-05T12:27:50+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Web Fraud 2.0;BEC;business email compromise;malicious office apps;Microsoft Office 365;phishing;proofpoint;Ryan Kalember
Slug: malicious-office-365-apps-are-the-ultimate-insiders

[Source](https://krebsonsecurity.com/2021/05/malicious-office-365-apps-are-the-ultimate-insiders/){:target="_blank" rel="noopener"}

> Phishers targeting Microsoft Office 365 users increasingly are turning to specialized links that take users to their organization’s own email login page. After a user logs in, the link prompts them to install a malicious but innocuously-named app that gives the attacker persistent, password-free access to any of the user’s emails and files, both of which are then plundered to launch malware and phishing scams against others. These attacks begin with an emailed link that when clicked loads not a phishing site but the user’s actual Office 365 login page — whether that be at microsoft.com or their employer’s domain. [...]
