Title: How to implement a hybrid PKI solution on AWS
Date: 2021-05-27T00:57:37+00:00
Author: Max Farnga
Category: AWS Security
Tags: Advanced (300);AWS Certificate Manager;AWS CloudHSM;Security, Identity, & Compliance;ACM Private CA;Certificate Authority;Cloud PKI;Hybrid PKI;PKI;PKI on AWS;Public key infrastructure;Security Blog;Windows CA on AWS
Slug: how-to-implement-a-hybrid-pki-solution-on-aws

[Source](https://aws.amazon.com/blogs/security/how-to-implement-a-hybrid-pki-solution-on-aws/){:target="_blank" rel="noopener"}

> As customers migrate workloads into Amazon Web Services (AWS) they may be running a combination of on-premises and cloud infrastructure. When certificates are issued to this infrastructure, having a common root of trust to the certificate hierarchy allows for consistency and interoperability of the Public Key Infrastructure (PKI) solution. In this blog post, I am [...]
