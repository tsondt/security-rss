Title: DarkSide ransomware servers reportedly seized, operation shuts down
Date: 2021-05-14T10:37:45-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: darkside-ransomware-servers-reportedly-seized-operation-shuts-down

[Source](https://www.bleepingcomputer.com/news/security/darkside-ransomware-servers-reportedly-seized-operation-shuts-down/){:target="_blank" rel="noopener"}

> The DarkSide ransomware operation has allegedly shut down after the threat actors lost access to servers and their cryptocurrency was transferred to an unknown wallet. [...]
