Title: Apple sent my data to the FBI, says boss of controversial research paper trove Sci-Hub
Date: 2021-05-17T07:57:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: apple-sent-my-data-to-the-fbi-says-boss-of-controversial-research-paper-trove-sci-hub

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/17/sci_hub_apple_fbi_claim/){:target="_blank" rel="noopener"}

> Former Sun boss Scott McNealy offers interesting response Alexandra Elbakyan, the creator of controversial research trove Sci-Hub, has claimed that Apple informed her it has handed over information about her account to the FBI.... [...]
