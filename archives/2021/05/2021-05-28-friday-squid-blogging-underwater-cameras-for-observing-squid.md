Title: Friday Squid Blogging: Underwater Cameras for Observing Squid
Date: 2021-05-28T21:09:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;cameras;squid
Slug: friday-squid-blogging-underwater-cameras-for-observing-squid

[Source](https://www.schneier.com/blog/archives/2021/05/friday-squid-blogging-underwater-cameras-for-observing-squid.html){:target="_blank" rel="noopener"}

> Interesting research paper. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
