Title: US declares state of emergency after ransomware hits largest pipeline
Date: 2021-05-10T09:37:47-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: us-declares-state-of-emergency-after-ransomware-hits-largest-pipeline

[Source](https://www.bleepingcomputer.com/news/security/us-declares-state-of-emergency-after-ransomware-hits-largest-pipeline/){:target="_blank" rel="noopener"}

> After a ransomware attack on Colonial Pipeline forced the company to shut down 5,500 miles of fuel pipeline, the Federal Motor Carrier Safety Administration (FMCSA) issued a regional emergency declaration affecting 17 states and the District of Columbia. [...]
