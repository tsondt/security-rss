Title: New Epsilon Red ransomware hunts unpatched Microsoft Exchange servers
Date: 2021-05-29T11:33:44-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: new-epsilon-red-ransomware-hunts-unpatched-microsoft-exchange-servers

[Source](https://www.bleepingcomputer.com/news/security/new-epsilon-red-ransomware-hunts-unpatched-microsoft-exchange-servers/){:target="_blank" rel="noopener"}

> A new ransomware threat calling itself Red Epsilon has been seen leveraging Microsoft Exchange server vulnerabilities to encrypt machines across the network. [...]
