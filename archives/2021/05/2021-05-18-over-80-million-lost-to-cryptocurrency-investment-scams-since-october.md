Title: Over $80 million lost to cryptocurrency investment scams since October
Date: 2021-05-18T11:52:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: over-80-million-lost-to-cryptocurrency-investment-scams-since-october

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/over-80-million-lost-to-cryptocurrency-investment-scams-since-october/){:target="_blank" rel="noopener"}

> The US Federal Trade Commission (FTC) says that over $80 million were lost to cryptocurrency investment scams, according to roughly 7,000 reports received since October 2020. [...]
