Title: Sneakers, Gaming, Nvidia Cards: Retailers Can Stop Shopping Bots
Date: 2021-05-04T14:12:52+00:00
Author: Jason Kent
Category: Threatpost
Tags: InfoSec Insider;Web Security
Slug: sneakers-gaming-nvidia-cards-retailers-can-stop-shopping-bots

[Source](https://threatpost.com/sneakers-gaming-nvidia-retailers-shopping-bots/165838/){:target="_blank" rel="noopener"}

> Jason Kent, hacker in residence at Cequence Security, says most retailers are applying 1970s solutions to the modern (and out-of-control) shopping-bot problem, and offers alternative ideas. [...]
