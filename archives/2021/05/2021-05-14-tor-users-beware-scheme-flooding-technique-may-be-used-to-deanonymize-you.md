Title: Tor users, beware: 'Scheme flooding' technique may be used to deanonymize you
Date: 2021-05-14T13:32:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: tor-users-beware-scheme-flooding-technique-may-be-used-to-deanonymize-you

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/14/browser_fingerprinting_flaw/){:target="_blank" rel="noopener"}

> By probing for installed apps with custom URL schemes, it's possible to build a 32-bit unique fingerprint FingerprintJS, maker of a browser-fingerprinting library for fraud prevention, on Thursday said it has identified a more dubious fingerprinting technique capable of generating a consistent identifier across different desktop browsers, including the Tor Browser.... [...]
