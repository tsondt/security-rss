Title: Biden issues executive order to increase U.S. cybersecurity defenses
Date: 2021-05-12T20:02:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Government
Slug: biden-issues-executive-order-to-increase-us-cybersecurity-defenses

[Source](https://www.bleepingcomputer.com/news/security/biden-issues-executive-order-to-increase-us-cybersecurity-defenses/){:target="_blank" rel="noopener"}

> President Biden signed an executive order Wednesday to modernize the country's defenses against cyberattacks and give more timely access to information necessary for law enforcement to conduct investigations. [...]
