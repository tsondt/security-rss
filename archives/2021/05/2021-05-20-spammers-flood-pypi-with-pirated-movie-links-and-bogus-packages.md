Title: Spammers flood PyPI with pirated movie links and bogus packages
Date: 2021-05-20T12:02:40-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: spammers-flood-pypi-with-pirated-movie-links-and-bogus-packages

[Source](https://www.bleepingcomputer.com/news/security/spammers-flood-pypi-with-pirated-movie-links-and-bogus-packages/){:target="_blank" rel="noopener"}

> The official Python software package repository, PyPI, is getting flooded with spam packages, as seen by BleepingComputer. These packages are named after different movies in a style that is commonly associated with torrents and "warez" sites hosting pirated content. [...]
