Title: Insider PhD: Hacking education channel suspended from YouTube for ‘severe’ guideline violations
Date: 2021-05-24T14:59:25+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: insider-phd-hacking-education-channel-suspended-from-youtube-for-severe-guideline-violations

[Source](https://portswigger.net/daily-swig/insider-phd-hacking-education-channel-suspended-from-youtube-for-severe-guideline-violations){:target="_blank" rel="noopener"}

> Katie Paxton-Fear tells The Daily Swig she has appealed the decision [...]
