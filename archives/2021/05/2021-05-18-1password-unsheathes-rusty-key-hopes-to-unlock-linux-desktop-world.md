Title: 1Password unsheathes Rusty key, hopes to unlock Linux Desktop world
Date: 2021-05-18T13:30:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 1password-unsheathes-rusty-key-hopes-to-unlock-linux-desktop-world

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/18/1password/){:target="_blank" rel="noopener"}

> Tries to tempt penguins with Ring Crypto 1Password has unveiled a full-featured desktop app for Linux, written in Rust and using the ring crypto library for end-to-end encryption.... [...]
