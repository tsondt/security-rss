Title: Wormable Windows Bug Opens Door to DoS, RCE
Date: 2021-05-11T20:05:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: wormable-windows-bug-opens-door-to-dos-rce

[Source](https://threatpost.com/wormable-windows-bug-dos-rce/166057/){:target="_blank" rel="noopener"}

> Microsoft's May 2021 Patch Tuesday updates include fixes for four critical security vulnerabilities. [...]
