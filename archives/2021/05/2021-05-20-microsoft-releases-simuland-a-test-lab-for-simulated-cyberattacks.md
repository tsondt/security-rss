Title: Microsoft releases SimuLand, a test lab for simulated cyberattacks
Date: 2021-05-20T14:59:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-releases-simuland-a-test-lab-for-simulated-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-releases-simuland-a-test-lab-for-simulated-cyberattacks/){:target="_blank" rel="noopener"}

> Microsoft has released SimuLand, an open-source lab environment to help test and improve Microsoft 365 Defender, Azure Defender, and Azure Sentinel defenses against real attack scenarios. [...]
