Title: What the FLoC? Everything you need to know about Google’s new ad tech that aims to replace third-party cookies
Date: 2021-05-07T15:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: what-the-floc-everything-you-need-to-know-about-googles-new-ad-tech-that-aims-to-replace-third-party-cookies

[Source](https://portswigger.net/daily-swig/what-the-floc-everything-you-need-to-know-about-googles-new-ad-tech-that-aims-to-replace-third-party-cookies){:target="_blank" rel="noopener"}

> Will ‘Federated Learning of Cohorts’ preserve user privacy? The jury is still out [...]
