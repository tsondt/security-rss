Title: Hospitals cancel outpatient appointments as Irish health service struck by ransomware
Date: 2021-05-14T11:45:15+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: hospitals-cancel-outpatient-appointments-as-irish-health-service-struck-by-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/14/ireland_hse_ransomware_hospital_conti_wizardspider/){:target="_blank" rel="noopener"}

> Russia-based criminals pick soft target in hope of easy gains Ireland's nationalised health service has shut down its IT systems following a "human-operated" Conti ransomware attack, causing a Dublin hospital to cancel outpatient appointments.... [...]
