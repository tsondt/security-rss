Title: Feds Shut Down Fake COVID-19 Vaccine Phishing Website
Date: 2021-05-05T13:24:30+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Web Security
Slug: feds-shut-down-fake-covid-19-vaccine-phishing-website

[Source](https://threatpost.com/feds-fake-covid-19-vaccine-phishing-website/165872/){:target="_blank" rel="noopener"}

> ‘Freevaccinecovax.org’ claimed to be that of a biotech company but instead was stealing info from visitors to use for nefarious purposes. [...]
