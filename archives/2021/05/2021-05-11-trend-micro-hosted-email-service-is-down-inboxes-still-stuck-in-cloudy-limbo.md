Title: Trend Micro hosted email service is down, inboxes still stuck in cloudy limbo
Date: 2021-05-11T01:13:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: trend-micro-hosted-email-service-is-down-inboxes-still-stuck-in-cloudy-limbo

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/11/trend_email_outage/){:target="_blank" rel="noopener"}

> Blames spam filters for brownout, warns fix could be 'disruptive' Trend Micro’s hosted email security product is experiencing a global brownout.... [...]
