Title: Audio maker Bose discloses data breach after ransomware attack
Date: 2021-05-24T19:47:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: audio-maker-bose-discloses-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/audio-maker-bose-discloses-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Bose Corporation (Bose) has disclosed a data breach following a ransomware attack that hit the company's systems in early March. [...]
