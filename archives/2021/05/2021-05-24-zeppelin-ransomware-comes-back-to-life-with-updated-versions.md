Title: Zeppelin ransomware comes back to life with updated versions
Date: 2021-05-24T03:22:23-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: zeppelin-ransomware-comes-back-to-life-with-updated-versions

[Source](https://www.bleepingcomputer.com/news/security/zeppelin-ransomware-comes-back-to-life-with-updated-versions/){:target="_blank" rel="noopener"}

> The developers of Zeppelin ransomware have resumed their activity after a period of relative silence that started last Fall and started to advertise new versions of the malware. [...]
