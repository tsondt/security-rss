Title: Chrome on Windows turns on Intel, AMD chip-level defenses against malicious websites
Date: 2021-05-06T07:23:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: chrome-on-windows-turns-on-intel-amd-chip-level-defenses-against-malicious-websites

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/06/chrome_code_protection/){:target="_blank" rel="noopener"}

> Terms and conditions apply Version 90 of Google's Chrome browser includes a bit of extra security for users of recent versions of Windows and the latest x86 processors, in the form of hardware-enforced stack protection.... [...]
