Title: Unsuccessful Conti Ransomware Attack Still Packs Costly Punch
Date: 2021-05-18T14:57:33+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware
Slug: unsuccessful-conti-ransomware-attack-still-packs-costly-punch

[Source](https://threatpost.com/conti-ransomware-fail-costly/166263/){:target="_blank" rel="noopener"}

> Separate attacks last week on the country’s Department of Health and Health Service Executive forced the shutdown of networks and services that still haven’t been fully restored. [...]
