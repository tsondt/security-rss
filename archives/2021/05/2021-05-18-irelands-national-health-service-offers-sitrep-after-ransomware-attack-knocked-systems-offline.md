Title: Ireland’s national health service offers sitrep after ransomware attack knocked systems offline
Date: 2021-05-18T14:21:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: irelands-national-health-service-offers-sitrep-after-ransomware-attack-knocked-systems-offline

[Source](https://portswigger.net/daily-swig/irelands-national-health-service-offers-sitrep-after-ransomware-attack-knocked-systems-offline){:target="_blank" rel="noopener"}

> The Health Service Executive is working to restore servers and devices [...]
