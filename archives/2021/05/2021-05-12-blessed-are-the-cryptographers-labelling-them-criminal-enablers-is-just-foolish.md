Title: Blessed are the cryptographers, labelling them criminal enablers is just foolish
Date: 2021-05-12T07:31:08+00:00
Author: Mark Pesce
Category: The Register
Tags: 
Slug: blessed-are-the-cryptographers-labelling-them-criminal-enablers-is-just-foolish

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/12/blessed_are_the_cryptographers/){:target="_blank" rel="noopener"}

> Preserving privacy is hard. I know because when I tried, I quickly learned not to play with weapons Column Nearly a decade ago I decided to try my hand as a cryptographer. It went about as well as you might expect. I’d gotten the crazy idea to write a tool that would encrypt Twitter’s direct messages - sent in the clear - so that your private communications would truly be private, visible to no one, including Twitter.... [...]
