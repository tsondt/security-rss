Title: Newly Unclassified NSA Document on Cryptography in the 1970s
Date: 2021-05-10T11:21:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cryptography;FOIA;history of cryptography;NSA
Slug: newly-unclassified-nsa-document-on-cryptography-in-the-1970s

[Source](https://www.schneier.com/blog/archives/2021/05/newly-unclassified-nsa-document-on-cryptography-in-the-1970s.html){:target="_blank" rel="noopener"}

> This is a newly unclassified NSA history of its reaction to academic cryptography in the 1970s: “ New Comes Out of the Closet: The Debate over Public Cryptography in the Inman Era,” Cryptographic Quarterly, Spring 1996, author still classified. [...]
