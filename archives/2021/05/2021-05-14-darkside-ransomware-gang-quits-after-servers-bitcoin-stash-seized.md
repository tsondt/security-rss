Title: DarkSide Ransomware Gang Quits After Servers, Bitcoin Stash Seized
Date: 2021-05-14T15:44:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;Avaddon;BitMix;Colonial Pipeline ransomware attack;DarkSide ransomware;Intel 471;rEvil;XSS
Slug: darkside-ransomware-gang-quits-after-servers-bitcoin-stash-seized

[Source](https://krebsonsecurity.com/2021/05/darkside-ransomware-gang-quits-after-servers-bitcoin-stash-seized/){:target="_blank" rel="noopener"}

> The DarkSide ransomware affiliate program responsible for the six-day outage at Colonial Pipeline this week that led to fuel shortages and price spikes across the country is running for the hills. The crime gang announced it was closing up shop after its servers were seized and someone drained the cryptocurrency from an account the group uses to pay affiliates. “Servers were seized (country not named), money of advertisers and founders was transferred to an unknown account,” reads a message from a cybercrime forum reposted to the Russian OSINT Telegram channel. “A few hours ago, we lost access to the public [...]
