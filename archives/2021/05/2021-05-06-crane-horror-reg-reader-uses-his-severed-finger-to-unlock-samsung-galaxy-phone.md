Title: Crane horror <i>Reg</i> reader uses his severed finger to unlock Samsung Galaxy phone
Date: 2021-05-06T09:15:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: crane-horror-reg-reader-uses-his-severed-finger-to-unlock-samsung-galaxy-phone

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/06/samsung_galaxy/){:target="_blank" rel="noopener"}

> On the other hand he was fine Graphic images Everyone knows the trope. The baddies smash their way in and gun down the guard standing in front of the vault. "Dammit," says the lead bad guy, "it's a biometric scanner, we'll never get in!" His most grizzled henchman turns round, holding up the dead guard's lifeless arm. "Oh yes we will..."... [...]
