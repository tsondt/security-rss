Title: ‘Soft skills are the most under-researched area of the bug bounty industry’ – ‘Reconless’ YouTubers on filling a gap in infosec education
Date: 2021-05-21T12:23:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: soft-skills-are-the-most-under-researched-area-of-the-bug-bounty-industry-reconless-youtubers-on-filling-a-gap-in-infosec-education

[Source](https://portswigger.net/daily-swig/soft-skills-are-the-most-under-researched-area-of-the-bug-bounty-industry-reconless-youtubers-on-filling-a-gap-in-infosec-education){:target="_blank" rel="noopener"}

> One year after the launch of their ethical hacking video channel, Ron Chan, ‘FileDescriptor’, and ‘EdOverflow’ tell The Daily Swig about their approach towards inspiring and educating the hacker community [...]
