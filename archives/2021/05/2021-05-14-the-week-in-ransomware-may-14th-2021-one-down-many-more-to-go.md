Title: The Week in Ransomware - May 14th 2021 - One down, many more to go
Date: 2021-05-14T14:39:33-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-may-14th-2021-one-down-many-more-to-go

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-14th-2021-one-down-many-more-to-go/){:target="_blank" rel="noopener"}

> Ransomware took the media spotlight this week after a ransomware gang known as DarkSide targeted critical infrastructure in the USA. [...]
