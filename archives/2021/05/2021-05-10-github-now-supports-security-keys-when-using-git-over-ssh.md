Title: GitHub now supports security keys when using Git over SSH
Date: 2021-05-10T16:09:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: github-now-supports-security-keys-when-using-git-over-ssh

[Source](https://www.bleepingcomputer.com/news/security/github-now-supports-security-keys-when-using-git-over-ssh/){:target="_blank" rel="noopener"}

> GitHub has added support for securing SSH Git operations using FIDO2 security keys for added protection from account takeover attempts. [...]
