Title: Free SANS Cyber Security Summits: Sign up now, learn online, keep your network safe
Date: 2021-05-14T15:30:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: free-sans-cyber-security-summits-sign-up-now-learn-online-keep-your-network-safe

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/14/sans_cyber_security_summits/){:target="_blank" rel="noopener"}

> Sometimes you need to lift yourself out of the cybersec trenches and look up to the summit Promo Keeping your organization safe from cybercriminals and other ne’er do wells requires constant honing and refining of your own skills and knowledge.... [...]
