Title: Building fine-grained authorization using Amazon Cognito, API Gateway, and IAM
Date: 2021-05-21T19:22:59+00:00
Author: Artem Lovan
Category: AWS Security
Tags: Advanced (300);Amazon API Gateway;Amazon Cognito;AWS Identity and Access Management (IAM);Security, Identity, & Compliance;Amazon DynamoDB;API;authorization;AWS IAM;custom authorizer;JWT;OAuth;RBAC;SAML;Security Blog;Token
Slug: building-fine-grained-authorization-using-amazon-cognito-api-gateway-and-iam

[Source](https://aws.amazon.com/blogs/security/building-fine-grained-authorization-using-amazon-cognito-api-gateway-and-iam/){:target="_blank" rel="noopener"}

> Authorizing functionality of an application based on group membership is a best practice. If you’re building APIs with Amazon API Gateway and you need fine-grained access control for your users, you can use Amazon Cognito. Amazon Cognito allows you to use groups to create a collection of users, which is often done to set the [...]
