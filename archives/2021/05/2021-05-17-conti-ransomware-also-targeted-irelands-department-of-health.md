Title: Conti ransomware also targeted Ireland's Department of Health
Date: 2021-05-17T18:13:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: conti-ransomware-also-targeted-irelands-department-of-health

[Source](https://www.bleepingcomputer.com/news/security/conti-ransomware-also-targeted-irelands-department-of-health/){:target="_blank" rel="noopener"}

> The Conti ransomware gang failed to encrypt the systems of Ireland's Department of Health (DoH) despite breaching its network and dropping Cobalt Strike beacons to deploy their malware across the network. [...]
