Title: DOD expands bug disclosure program to all publicly accessible systems
Date: 2021-05-04T16:20:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: dod-expands-bug-disclosure-program-to-all-publicly-accessible-systems

[Source](https://www.bleepingcomputer.com/news/security/dod-expands-bug-disclosure-program-to-all-publicly-accessible-systems/){:target="_blank" rel="noopener"}

> US Department of Defense (DOD) officials today announced that the department's Vulnerability Disclosure Program (VDP) has been expanded to include all publicly accessible DOD websites and applications. [...]
