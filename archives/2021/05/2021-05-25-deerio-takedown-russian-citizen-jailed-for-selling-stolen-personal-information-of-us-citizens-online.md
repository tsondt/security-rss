Title: Deer.io takedown: Russian citizen jailed for selling stolen personal information of US citizens online
Date: 2021-05-25T14:16:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: deerio-takedown-russian-citizen-jailed-for-selling-stolen-personal-information-of-us-citizens-online

[Source](https://portswigger.net/daily-swig/deer-io-takedown-russian-citizen-jailed-for-selling-stolen-personal-information-of-us-citizens-online){:target="_blank" rel="noopener"}

> High-value credit card and Social Security numbers were among datasets available on now-defunct website [...]
