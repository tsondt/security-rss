Title: China signals dissatisfaction with gig economy impact on ride-share drivers
Date: 2021-05-17T01:00:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: china-signals-dissatisfaction-with-gig-economy-impact-on-ride-share-drivers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/17/china_transport_data_crackdown/){:target="_blank" rel="noopener"}

> Also puts brakes on data collection by carmakers China has signaled that ride-sharing companies can expect the same scrutiny as its web giants, and laid out regulations that will stop cars from collecting unnecessary data.... [...]
