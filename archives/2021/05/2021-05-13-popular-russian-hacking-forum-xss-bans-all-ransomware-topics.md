Title: Popular Russian hacking forum XSS bans all ransomware topics
Date: 2021-05-13T21:48:23-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: popular-russian-hacking-forum-xss-bans-all-ransomware-topics

[Source](https://www.bleepingcomputer.com/news/security/popular-russian-hacking-forum-xss-bans-all-ransomware-topics/){:target="_blank" rel="noopener"}

> One of the most popular Russian-speaking hacker forums, XSS, has banned all topics promoting ransomware to prevent unwanted attention. [...]
