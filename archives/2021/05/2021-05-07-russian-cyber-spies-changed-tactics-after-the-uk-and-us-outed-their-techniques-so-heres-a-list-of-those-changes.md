Title: Russian cyber-spies changed tactics after the UK and US outed their techniques – so here's a list of those changes
Date: 2021-05-07T18:49:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: russian-cyber-spies-changed-tactics-after-the-uk-and-us-outed-their-techniques-so-heres-a-list-of-those-changes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/07/ncsc_russia_vulns_smart_cities_china_warning/){:target="_blank" rel="noopener"}

> Plus: NCSC warns of how hostile powers may exploit smart city infrastructure Russian spies from APT29 responded to Western agencies outing their tactics by adopting a red-teaming tool to blend into targets' networks as a legitimate pentesting exercise.... [...]
