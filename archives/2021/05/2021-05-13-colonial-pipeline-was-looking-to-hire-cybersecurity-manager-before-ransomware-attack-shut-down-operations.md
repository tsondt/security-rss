Title: Colonial Pipeline was looking to hire cybersecurity manager before ransomware attack shut down operations
Date: 2021-05-13T12:35:39+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: colonial-pipeline-was-looking-to-hire-cybersecurity-manager-before-ransomware-attack-shut-down-operations

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/13/colonial_pipeline_hiring_cybersecurity_manager/){:target="_blank" rel="noopener"}

> Good luck to whoever got that gig Stricken US bulk hydrocarbon conveyor Colonial Pipeline advertised for a new cybersecurity manager a month before that ransomware attack forced operators to shut down the pipeline as a pre-emptive safety measure.... [...]
