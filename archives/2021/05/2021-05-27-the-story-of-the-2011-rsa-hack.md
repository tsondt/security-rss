Title: The Story of the 2011 RSA Hack
Date: 2021-05-27T11:41:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: China;cybersecurity;hacking;RSA;supply chain
Slug: the-story-of-the-2011-rsa-hack

[Source](https://www.schneier.com/blog/archives/2021/05/the-story-of-the-2011-rsa-hack.html){:target="_blank" rel="noopener"}

> Really good long article about the Chinese hacking of RSA, Inc. They were able to get copies of the seed values to the SecurID authentication token, a harbinger of supply-chain attacks to come. [...]
