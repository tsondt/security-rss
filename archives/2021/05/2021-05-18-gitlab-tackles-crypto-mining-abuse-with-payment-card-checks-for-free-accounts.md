Title: GitLab tackles crypto-mining abuse with payment card checks for free accounts
Date: 2021-05-18T13:31:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: gitlab-tackles-crypto-mining-abuse-with-payment-card-checks-for-free-accounts

[Source](https://portswigger.net/daily-swig/gitlab-tackles-crypto-mining-abuse-with-payment-card-checks-for-free-accounts){:target="_blank" rel="noopener"}

> Security control could be rolled out more widely if it fails to halt rise in abuse [...]
