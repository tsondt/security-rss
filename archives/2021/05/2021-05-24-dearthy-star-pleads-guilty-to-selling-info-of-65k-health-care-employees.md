Title: 'Dearthy Star' pleads guilty to selling info of 65K health care employees
Date: 2021-05-24T18:12:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: dearthy-star-pleads-guilty-to-selling-info-of-65k-health-care-employees

[Source](https://www.bleepingcomputer.com/news/security/dearthy-star-pleads-guilty-to-selling-info-of-65k-health-care-employees/){:target="_blank" rel="noopener"}

> Justin Sean Johnson, a 30-year-old from Detroit, Michigan, has pleaded guilty to stealing the personally identifiable information (PII) of 65,000 employees of health care provider and insurer University of Pittsburgh Medical Center (UPMC) and selling it on the dark web. [...]
