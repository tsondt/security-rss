Title: Identifying the Person Behind Bitcoin Fog
Date: 2021-05-03T14:36:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: anonymity;bitcoin;crime;cryptocurrency;dark web;de-anonymization;FBI;identification;tracking
Slug: identifying-the-person-behind-bitcoin-fog

[Source](https://www.schneier.com/blog/archives/2021/05/identifying-the-person-behind-bitcoin-fog.html){:target="_blank" rel="noopener"}

> The person behind the Bitcoin Fog was identified and arrested. Bitcoin Fog was an anonymization service: for a fee, it mixed a bunch of people’s bitcoins up so that it was hard to figure out where any individual coins came from. It ran for ten years. Identifying the person behind Bitcoin Fog serves as an illustrative example of how hard it is to be anonymous online in the face of a competent police investigation: Most remarkable, however, is the IRS’s account of tracking down Sterlingov using the very same sort of blockchain analysis that his own service was meant to [...]
