Title: Apple Censorship and Surveillance in China
Date: 2021-05-19T11:31:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Apple;censorship;China;privacy;surveillance
Slug: apple-censorship-and-surveillance-in-china

[Source](https://www.schneier.com/blog/archives/2021/05/apple-censorship-and-surveillance-in-china.html){:target="_blank" rel="noopener"}

> Good investigative reporting on how Apple is participating in and assisting with Chinese censorship and surveillance. [...]
