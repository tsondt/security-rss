Title: Google will make you use two-step verification to login
Date: 2021-05-07T00:52:59+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: google-will-make-you-use-two-step-verification-to-login

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/07/google_password_purge/){:target="_blank" rel="noopener"}

> World Password Day returns to remind us how much passwords suck Google has marked World Password Day by declaring "passwords are the single biggest threat to your online security," and announcing plans to automatically add multi-step authentication to its users' accounts.... [...]
