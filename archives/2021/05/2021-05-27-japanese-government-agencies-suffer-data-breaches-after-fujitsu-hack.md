Title: Japanese government agencies suffer data breaches after Fujitsu hack
Date: 2021-05-27T03:21:17-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: japanese-government-agencies-suffer-data-breaches-after-fujitsu-hack

[Source](https://www.bleepingcomputer.com/news/security/japanese-government-agencies-suffer-data-breaches-after-fujitsu-hack/){:target="_blank" rel="noopener"}

> Offices of multiple Japanese agencies were breached via Fujitsu's "ProjectWEB" information sharing tool. Fujitsu states that attackers gained unauthorized access to projects that used ProjectWEB, and stole some customer data. [...]
