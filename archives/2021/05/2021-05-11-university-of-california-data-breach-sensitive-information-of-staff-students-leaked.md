Title: University of California data breach: Sensitive information of staff, students leaked
Date: 2021-05-11T13:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: university-of-california-data-breach-sensitive-information-of-staff-students-leaked

[Source](https://portswigger.net/daily-swig/university-of-california-data-breach-sensitive-information-of-staff-students-leaked){:target="_blank" rel="noopener"}

> Education institution releases more details after third-party Accellion hack [...]
