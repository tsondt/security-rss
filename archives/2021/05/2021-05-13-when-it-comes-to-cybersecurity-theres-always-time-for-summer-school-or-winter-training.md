Title: When it comes to cybersecurity, there's always time for summer school or winter training
Date: 2021-05-13T00:22:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: when-it-comes-to-cybersecurity-theres-always-time-for-summer-school-or-winter-training

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/13/sans_asia_pac_training_event/){:target="_blank" rel="noopener"}

> Get ready for SANS Institute's biggest ever Asia-Pacific training event Promo Whatever your plans for the third quarter of 2021, an emerging security vulnerability or a network security breach has the potential to throw them into disarray. Unless, of course, you’ve made the effort to hone your existing skills or expand your knowledge into new areas ahead of time.... [...]
