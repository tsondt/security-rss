Title: Stalkerware Apps Riddled with Security Bugs
Date: 2021-05-18T18:36:38+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Malware;Mobile Security;Privacy
Slug: stalkerware-apps-riddled-with-security-bugs

[Source](https://threatpost.com/stalkerware-apps-security-bugs/166274/){:target="_blank" rel="noopener"}

> Attackers can take advantage of the fact these apps access, gather, store and transmit more information than any other app their victims have installed. [...]
