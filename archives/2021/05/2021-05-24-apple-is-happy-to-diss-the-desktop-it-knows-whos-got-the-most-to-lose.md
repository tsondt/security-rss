Title: Apple is happy to diss the desktop – it knows who's got the most to lose
Date: 2021-05-24T09:01:06+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: apple-is-happy-to-diss-the-desktop-it-knows-whos-got-the-most-to-lose

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/24/desktop_os/){:target="_blank" rel="noopener"}

> Also: The basic utility of the general purpose OS cannot be sanitised into total safety. Nor should it Column You will have noticed that Apple just pushed MacOS under the wolves, thrown it to the bus and left it hanging out to dry like a post-Brexit fishing net.... [...]
