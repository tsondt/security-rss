Title: Domino's India discloses data breach after hackers sell data online
Date: 2021-05-25T14:37:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: dominos-india-discloses-data-breach-after-hackers-sell-data-online

[Source](https://www.bleepingcomputer.com/news/security/dominos-india-discloses-data-breach-after-hackers-sell-data-online/){:target="_blank" rel="noopener"}

> Domino's India has disclosed a data breach after a threat actor hacked their systems and sold their stolen data on a hacking forum. [...]
