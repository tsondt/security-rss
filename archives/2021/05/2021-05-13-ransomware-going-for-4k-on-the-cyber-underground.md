Title: Ransomware Going for $4K on the Cyber-Underground
Date: 2021-05-13T19:52:33+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Most Recent ThreatLists;Vulnerabilities;Web Security
Slug: ransomware-going-for-4k-on-the-cyber-underground

[Source](https://threatpost.com/ransomware-4k-cyber-underground/166145/){:target="_blank" rel="noopener"}

> An analysis of three popular forums used by ransomware operators reveals a complex ecosystem with many partnerships. [...]
