Title: 80% of Net Neutrality Comments to FCC Were Fudged
Date: 2021-05-07T13:56:53+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government
Slug: 80-of-net-neutrality-comments-to-fcc-were-fudged

[Source](https://threatpost.com/net-neutrality-comments-fcc-fudged/165943/){:target="_blank" rel="noopener"}

> NY's AG: Millions of fake comments – in favor and against – came from a secret broadband-funded campaign or from a 19-year-old's fake identities. [...]
