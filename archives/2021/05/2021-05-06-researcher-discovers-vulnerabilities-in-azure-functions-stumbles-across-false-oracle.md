Title: Researcher discovers vulnerabilities in Azure Functions, stumbles across false oracle
Date: 2021-05-06T14:09:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: researcher-discovers-vulnerabilities-in-azure-functions-stumbles-across-false-oracle

[Source](https://portswigger.net/daily-swig/researcher-discovers-vulnerabilities-in-azure-functions-stumbles-across-false-oracle){:target="_blank" rel="noopener"}

> Potential catastrophe averted due to an implementation bug in Microsoft cryptography [...]
