Title: AIs and Fake Comments
Date: 2021-05-24T11:20:05+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: artificial intelligence;essays;fake news;social media
Slug: ais-and-fake-comments

[Source](https://www.schneier.com/blog/archives/2021/05/ais-and-fake-comments.html){:target="_blank" rel="noopener"}

> This month, the New York state attorney general issued a report on a scheme by “U.S. Companies and Partisans [to] Hack Democracy.” This wasn’t another attempt by Republicans to make it harder for Black people and urban residents to vote. It was a concerted attack on another core element of US democracy ­– the ability of citizens to express their voice to their political representatives. And it was carried out by generating millions of fake comments and fake emails purporting to come from real citizens. This attack was detected because it was relatively crude. But artificial intelligence technologies are making [...]
