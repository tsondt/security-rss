Title: XSS in the wild: JavaScript-stuffed orders used to compromise Japanese e-commerce sites
Date: 2021-05-04T14:20:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: xss-in-the-wild-javascript-stuffed-orders-used-to-compromise-japanese-e-commerce-sites

[Source](https://portswigger.net/daily-swig/xss-in-the-wild-javascript-stuffed-orders-used-to-compromise-japanese-e-commerce-sites){:target="_blank" rel="noopener"}

> Website vulnerabilities abused in new hacking campaign [...]
