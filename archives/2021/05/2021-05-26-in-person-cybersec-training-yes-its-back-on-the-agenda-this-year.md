Title: In-person cybersec training? Yes, it’s back on the agenda this year
Date: 2021-05-26T07:30:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: in-person-cybersec-training-yes-its-back-on-the-agenda-this-year

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/26/in_person_cybersec_training/){:target="_blank" rel="noopener"}

> SANS brings you face to face with students and instructors – or virtually if you prefer Promo The last year has taught us that online training can absolutely deliver the same learning experience as traditional in-person training. But it has also demonstrated how some of us thrive on human interaction, whether it’s with an instructor or simply with your fellow students.... [...]
