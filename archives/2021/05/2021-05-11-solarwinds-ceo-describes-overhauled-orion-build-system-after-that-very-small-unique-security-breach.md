Title: SolarWinds CEO describes overhauled Orion build system after that 'very small, unique' security breach
Date: 2021-05-11T19:59:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: solarwinds-ceo-describes-overhauled-orion-build-system-after-that-very-small-unique-security-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/11/solarwinds_ceo_orion_build_system/){:target="_blank" rel="noopener"}

> 'This can happen to anybody. There's always learning in any crisis. And we were no exception' CyberUK 21 SolarWinds’ chief exec has described the 18,000 customers who downloaded backdoored versions of its Orion software as a “very small” number while giving a speech to an infosec event.... [...]
