Title: Insurance giant CNA fully restores systems after ransomware attack
Date: 2021-05-13T12:14:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: insurance-giant-cna-fully-restores-systems-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/insurance-giant-cna-fully-restores-systems-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Leading US-based insurance company CNA Financial has fully restored systems following a Phoenix CryptoLocker ransomware attack that disrupted its online services and business operations during late March. [...]
