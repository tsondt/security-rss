Title: 13 best practices for user account, authentication, and password management, 2021 edition
Date: 2021-05-06T16:00:00+00:00
Author: Ian Maddox
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: 13-best-practices-for-user-account-authentication-and-password-management-2021-edition

[Source](https://cloud.google.com/blog/products/identity-security/account-authentication-and-password-management-best-practices/){:target="_blank" rel="noopener"}

> Updated for 2021 : This post includes updated best practices including the latest from Google's Best Practices for Password Management whitepapers for both users and system designers. Account management, authentication and password management can be tricky. Often, account management is a dark corner that isn't a top priority for developers or product managers. The resulting experience often falls short of what some of your users would expect for data security and user experience. Fortunately, Google Cloud brings several tools to help you make good decisions around the creation, secure handling and authentication of user accounts (in this context, anyone who [...]
