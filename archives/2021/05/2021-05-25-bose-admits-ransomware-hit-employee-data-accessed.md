Title: Bose Admits Ransomware Hit: Employee Data Accessed
Date: 2021-05-25T15:06:13+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Malware
Slug: bose-admits-ransomware-hit-employee-data-accessed

[Source](https://threatpost.com/bose-ransomware-employee-data/166443/){:target="_blank" rel="noopener"}

> The consumer-electronics stalwart was able to recover without paying a ransom, it said. [...]
