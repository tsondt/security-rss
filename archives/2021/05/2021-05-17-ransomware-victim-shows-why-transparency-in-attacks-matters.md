Title: Ransomware victim shows why transparency in attacks matters
Date: 2021-05-17T15:42:31-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-victim-shows-why-transparency-in-attacks-matters

[Source](https://www.bleepingcomputer.com/news/security/ransomware-victim-shows-why-transparency-in-attacks-matters/){:target="_blank" rel="noopener"}

> As devastating ransomware attacks continue to have far-reaching consequences, companies still try to hide the attacks rather than be transparent. Below we highlight a company's response to an attack that should be used as a model for all future disclosures. [...]
