Title: Colonial Pipeline cyber-attack: DarkSide ransomware details emerge as US urges critical infrastructure operators to be vigilant
Date: 2021-05-12T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: colonial-pipeline-cyber-attack-darkside-ransomware-details-emerge-as-us-urges-critical-infrastructure-operators-to-be-vigilant

[Source](https://portswigger.net/daily-swig/colonial-pipeline-cyber-attack-darkside-ransomware-details-emerge-as-us-urges-critical-infrastructure-operators){:target="_blank" rel="noopener"}

> The malware strain shut down operations at the US fuel provider last week [...]
