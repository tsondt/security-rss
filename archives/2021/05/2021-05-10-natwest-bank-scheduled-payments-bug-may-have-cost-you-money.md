Title: NatWest Bank scheduled payments bug may have cost you money
Date: 2021-05-10T08:13:16-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: natwest-bank-scheduled-payments-bug-may-have-cost-you-money

[Source](https://www.bleepingcomputer.com/news/technology/natwest-bank-scheduled-payments-bug-may-have-cost-you-money/){:target="_blank" rel="noopener"}

> Today, UK-based NatWest Bank has alerted multiple customers of a system error that may have caused many more payments to be debited from customer accounts than the originally agreed-upon amount. The issue impacts standing orders set up between 23rd March 2020 and 24th February 2021. [...]
