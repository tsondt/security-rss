Title: The Story of Colossus
Date: 2021-05-06T11:11:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cryptanalysis;history of computing;history of cryptography;video
Slug: the-story-of-colossus

[Source](https://www.schneier.com/blog/archives/2021/05/the-story-of-colossus.html){:target="_blank" rel="noopener"}

> Nice video of a talk by Chris Shore on the history of Colossus. [...]
