Title: A Russian Doll review – a data-disinformation troll keeps us hooked
Date: 2021-05-21T14:27:55+00:00
Author: Arifa Akbar
Category: The Guardian
Tags: Theatre;Stage;Culture;Data and computer security;Technology;Data protection;Arcola theatre
Slug: a-russian-doll-review-a-data-disinformation-troll-keeps-us-hooked

[Source](https://www.theguardian.com/stage/2021/may/21/a-russian-doll-review-barn-arcola-theatre){:target="_blank" rel="noopener"}

> Barn theatre, Cirencester Rachel Redford shines as a student recruited to mess with British heads, in a joint production with the Arcola theatre ‘Who owns your data?” asks Masha, the tortured voice at the centre of this monologue. Issues of data privacy and misuse are being increasingly raised but rarely show the inner workings of a Russian web brigade that orchestrates disinformation campaigns through anonymous online commentary. That is what Masha (Rachel Redford) does in Cat Goscovitch’s illuminating drama, based on a true story and co-produced by the Barn and Arcola theatres. She is an English literature student turned troll [...]
