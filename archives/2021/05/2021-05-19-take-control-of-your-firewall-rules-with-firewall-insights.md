Title: Take control of your firewall rules with Firewall Insights
Date: 2021-05-19T16:00:00+00:00
Author: Tracy Jiang
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: take-control-of-your-firewall-rules-with-firewall-insights

[Source](https://cloud.google.com/blog/products/identity-security/eliminate-firewall-misconfigurations-with-firewall-insights/){:target="_blank" rel="noopener"}

> Corporate firewalls typically include a massive number of rules, which accumulate over time as new workloads are added. When rules stack up piecemeal like this, misconfigurations occur that, at best, create headaches for security administrators, and at worst, create vulnerabilities that lead to security breaches. To address this, we have introduced the Firewall Insights module in our Network Intelligence Center, which provides a single console for managing Google Cloud network visibility, monitoring and troubleshooting. What are Firewall insights? Historically, there hasn’t been an easy way to deal with the accumulation of complicated firewall rules. That was until we created Firewall [...]
