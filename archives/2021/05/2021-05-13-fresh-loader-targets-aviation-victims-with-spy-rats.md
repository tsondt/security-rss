Title: Fresh Loader Targets Aviation Victims with Spy RATs
Date: 2021-05-13T14:55:53+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: fresh-loader-targets-aviation-victims-with-spy-rats

[Source](https://threatpost.com/loader-aviation-spy-rats/166133/){:target="_blank" rel="noopener"}

> The campaign is harvesting screenshots, keystrokes, credentials, webcam feeds, browser and clipboard data and more, with RevengeRAT or AsyncRAT payloads. [...]
