Title: New blueprint helps secure confidential data in AI Platform Notebooks
Date: 2021-05-03T16:00:00+00:00
Author: Matthieu Mayran
Category: GCP Security
Tags: AI & Machine Learning;Google Cloud Platform;Identity & Security
Slug: new-blueprint-helps-secure-confidential-data-in-ai-platform-notebooks

[Source](https://cloud.google.com/blog/products/ai-machine-learning/ai-platform-notebooks-security-blueprint/){:target="_blank" rel="noopener"}

> Core to Google Cloud’s efforts to be the industry’s most Trusted Cloud is our belief in shared fate - taking an active stake to help customers achieve better security outcomes on our platforms. To make it easier to build security into deployments, we provide opinionated guidance for customers in the form of security blueprints. We recently released our updated Google Cloud security foundations guide and deployable blueprint to help our customers build security into their starting point on Google Cloud. Today, we’re adding to our portfolio of blueprints with the publication of our Protecting confidential data in AI Platform Notebooks [...]
