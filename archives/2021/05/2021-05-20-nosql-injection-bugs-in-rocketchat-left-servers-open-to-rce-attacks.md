Title: NoSQL injection bugs in Rocket.Chat left servers open to RCE attacks
Date: 2021-05-20T12:25:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nosql-injection-bugs-in-rocketchat-left-servers-open-to-rce-attacks

[Source](https://portswigger.net/daily-swig/nosql-injection-bugs-in-rocket-chat-left-servers-open-to-rce-attacks){:target="_blank" rel="noopener"}

> Enterprise messaging platform forced to spill secrets [...]
