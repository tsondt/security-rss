Title: Russian gang behind SolarWinds hack returns with phishing attack disguised as mail from US aid agency
Date: 2021-05-28T07:57:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: russian-gang-behind-solarwinds-hack-returns-with-phishing-attack-disguised-as-mail-from-us-aid-agency

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/28/solar_winds_attacker_nobelium_returns/){:target="_blank" rel="noopener"}

> Microsoft says Nobelium scored access to Constant Contact email marketing tool Nobelium, the Russia-aligned gang identified as the perpetrators of the supply chain attack on SolarWinds' Orion software, has struck again, Microsoft vice president Tom Burt in a blogpost Thursday.... [...]
