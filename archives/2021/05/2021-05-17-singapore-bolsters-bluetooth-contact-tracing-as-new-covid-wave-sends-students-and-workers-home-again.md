Title: Singapore bolsters Bluetooth contact-tracing as new COVID wave sends students and workers home again
Date: 2021-05-17T06:02:18+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: singapore-bolsters-bluetooth-contact-tracing-as-new-covid-wave-sends-students-and-workers-home-again

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/17/singapore_changes_covid_tracking_tech/){:target="_blank" rel="noopener"}

> TraceTogether app becomes primary tracking tool and compulsory in many settings Singapore has made its Bluetooth-powered "TraceTogether" contact-tracing app its preferred means of recording movements in public spaces across the island.... [...]
