Title: Red Hat open-sources StackRox Kubernetes security product
Date: 2021-05-04T18:24:11+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: red-hat-open-sources-stackrox-kubernetes-security-product

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/04/red_hat_stackrox_kubernetes/){:target="_blank" rel="noopener"}

> More goodies for OpenShift, plus Konveyor to Kubernetes in association with IBM Kubecon Europe As Kubecon Europe gets under way, Red Hat has pushed out StackRox, the Kubernetes security product it acquired earlier this year, as an open-source project which will be the upstream for its Advanced Cluster Security for OpenShift.... [...]
