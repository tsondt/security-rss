Title: Four plead guilty to providing ‘bulletproof’ hosting services for cybercriminals
Date: 2021-05-10T14:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: four-plead-guilty-to-providing-bulletproof-hosting-services-for-cybercriminals

[Source](https://portswigger.net/daily-swig/four-plead-guilty-to-providing-bulletproof-hosting-services-for-cybercriminals){:target="_blank" rel="noopener"}

> Eastern European nationals admit to renting out resources to launch malware attacks and conduct other fraudulent activities [...]
