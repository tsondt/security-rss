Title: It’s Time to Prepare for a Rise in Insider Threats
Date: 2021-05-18T16:01:11+00:00
Author: Anurag Kahol
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;InfoSec Insider;Malware;Mobile Security;Web Security
Slug: its-time-to-prepare-for-a-rise-in-insider-threats

[Source](https://threatpost.com/prepare-rise-insider-threats/166272/){:target="_blank" rel="noopener"}

> Anurag Kahol, CTO at Bitglass, discusses options for detecting malicious or dangerous activity from within an organization. [...]
