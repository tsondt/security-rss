Title: Happy to pay out to ransomware masterminds? Yup, we thought so
Date: 2021-05-12T16:00:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: happy-to-pay-out-to-ransomware-masterminds-yup-we-thought-so

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/12/learn_to_frustrate_modern_ransomware/){:target="_blank" rel="noopener"}

> Join us online and learn about modern extortionware and how to frustrate it Webcast It’s shocking how blasé ransomware-toting criminals can be about freezing the operations of any organisation they can insinuate themselves into, including critical utilities or medical facilities.... [...]
