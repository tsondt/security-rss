Title: Recycle Your Phone, Sure, But Maybe Not Your Number
Date: 2021-05-19T15:13:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Security Tools;Web Fraud 2.0;Google Voice;number parking services;Princeton University;SMS;T-Mobile;Verizon
Slug: recycle-your-phone-sure-but-maybe-not-your-number

[Source](https://krebsonsecurity.com/2021/05/recycle-your-phone-sure-but-maybe-not-your-number/){:target="_blank" rel="noopener"}

> Many online services allow users to reset their passwords by clicking a link sent via SMS, and this unfortunately widespread practice has turned mobile phone numbers into de facto identity documents. Which means losing control over one thanks to a divorce, job termination or financial crisis can be devastating. Even so, plenty of people willingly abandon a mobile number without considering the potential fallout to their digital identities when those digits invariably get reassigned to someone else. New research shows how fraudsters can abuse wireless provider websites to identify available, recycled mobile numbers that allow password resets at a range [...]
