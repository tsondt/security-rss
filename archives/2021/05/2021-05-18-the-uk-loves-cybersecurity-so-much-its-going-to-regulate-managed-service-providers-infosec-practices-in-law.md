Title: The UK loves cybersecurity so much, it's going to regulate managed service providers' infosec practices in law
Date: 2021-05-18T15:03:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: the-uk-loves-cybersecurity-so-much-its-going-to-regulate-managed-service-providers-infosec-practices-in-law

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/18/ukgov_cybersecurity_reviews_supply_chain_cma/){:target="_blank" rel="noopener"}

> And you're invited to speak your brains on Computer Misuse Act changes +Comment The British government has vowed to create a legally binding cybersecurity framework for managed service providers (MSPs) – and if you want to tell gov.UK what you think, you've only got a few weeks to act.... [...]
