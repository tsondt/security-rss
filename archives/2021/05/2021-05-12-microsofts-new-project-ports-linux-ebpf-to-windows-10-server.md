Title: Microsoft's new project ports Linux eBPF to Windows 10, Server
Date: 2021-05-12T11:09:26-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: microsofts-new-project-ports-linux-ebpf-to-windows-10-server

[Source](https://www.bleepingcomputer.com/news/security/microsofts-new-project-ports-linux-ebpf-to-windows-10-server/){:target="_blank" rel="noopener"}

> Microsoft has launched a new open-source project that aims to add to Windows the benefits of eBPF, a technology first implemented in Linux that allows attaching programs in both kernel and user applications. [...]
