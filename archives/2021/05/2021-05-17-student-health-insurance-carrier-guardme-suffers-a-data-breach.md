Title: Student health insurance carrier Guard.me suffers a data breach
Date: 2021-05-17T20:57:51-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: student-health-insurance-carrier-guardme-suffers-a-data-breach

[Source](https://www.bleepingcomputer.com/news/security/student-health-insurance-carrier-guardme-suffers-a-data-breach/){:target="_blank" rel="noopener"}

> Student health insurance carrier guard.me has taken their website offline after a vulnerability allowed a threat actor to access policyholders' personal information. [...]
