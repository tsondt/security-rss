Title: Use EC2 Instance Connect to provide secure SSH access to EC2 instances with private IP addresses
Date: 2021-05-13T23:26:25+00:00
Author: Jason Nicholls
Category: AWS Security
Tags: Amazon EC2;Intermediate (200);Security, Identity, & Compliance;EC2;EC2 Instance Connect;Enterprise;Security Blog;SSH
Slug: use-ec2-instance-connect-to-provide-secure-ssh-access-to-ec2-instances-with-private-ip-addresses

[Source](https://aws.amazon.com/blogs/security/use-ec2-instance-connect-to-provide-secure-ssh-access-to-ec2-instances-with-private-ip-addresses/){:target="_blank" rel="noopener"}

> In this post, I show you how to use Amazon EC2 Instance Connect to use Secure Shell (SSH) to securely access your Amazon Elastic Compute Cloud (Amazon EC2) instances running on private subnets within an Amazon Virtual Private Cloud (Amazon VPC). EC2 Instance Connect provides a simple and secure way to connect to your EC2 [...]
