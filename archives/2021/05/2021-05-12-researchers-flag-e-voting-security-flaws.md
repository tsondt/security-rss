Title: Researchers Flag e-Voting Security Flaws
Date: 2021-05-12T20:43:52+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Hacks;Vulnerabilities;Web Security
Slug: researchers-flag-e-voting-security-flaws

[Source](https://threatpost.com/e-voting-security-flaws/166110/){:target="_blank" rel="noopener"}

> Paper ballots and source-code transparency are recommended to improve election security. [...]
