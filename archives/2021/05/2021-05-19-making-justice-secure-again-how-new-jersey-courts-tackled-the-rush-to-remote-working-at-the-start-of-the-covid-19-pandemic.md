Title: Making justice secure again: How New Jersey Courts tackled the rush to remote working at the start of the Covid-19 pandemic
Date: 2021-05-19T15:53:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: making-justice-secure-again-how-new-jersey-courts-tackled-the-rush-to-remote-working-at-the-start-of-the-covid-19-pandemic

[Source](https://portswigger.net/daily-swig/making-justice-secure-again-how-new-jersey-courts-tackled-the-rush-to-remote-working-at-the-start-of-the-covid-19-pandemic){:target="_blank" rel="noopener"}

> Security and data handling were at the top of the IT agenda [...]
