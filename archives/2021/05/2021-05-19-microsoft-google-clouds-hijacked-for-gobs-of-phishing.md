Title: Microsoft, Google Clouds Hijacked for Gobs of Phishing
Date: 2021-05-19T20:16:31+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Web Security
Slug: microsoft-google-clouds-hijacked-for-gobs-of-phishing

[Source](https://threatpost.com/microsoft-google-clouds-hijacked-phishing/166329/){:target="_blank" rel="noopener"}

> Attackers sent 52M malicious messages leveraging the likes of Office 365, Azure, OneDrive, SharePoint, G-Suite and Firebase storage in Q1 2021. [...]
