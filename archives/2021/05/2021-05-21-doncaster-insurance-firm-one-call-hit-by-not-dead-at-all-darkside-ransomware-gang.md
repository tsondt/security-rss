Title: Doncaster insurance firm One Call hit by not-dead-at-all Darkside ransomware gang
Date: 2021-05-21T09:15:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: doncaster-insurance-firm-one-call-hit-by-not-dead-at-all-darkside-ransomware-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/21/darkside_ransomware_doncaster/){:target="_blank" rel="noopener"}

> Local paper reports £15m heist demand amid Colonial Pipeline chaos A Doncaster insurance company has been hit by ransomware from the Darkside crew – whose "press release" declaring it was shutting down its operations last week was taken at face value by some pundits.... [...]
