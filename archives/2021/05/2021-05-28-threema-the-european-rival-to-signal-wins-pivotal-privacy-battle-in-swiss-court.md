Title: Threema, the European rival to Signal, wins pivotal privacy battle in Swiss Court
Date: 2021-05-28T11:56:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: threema-the-european-rival-to-signal-wins-pivotal-privacy-battle-in-swiss-court

[Source](https://portswigger.net/daily-swig/threema-the-european-rival-to-signal-wins-pivotal-privacy-battle-in-swiss-court){:target="_blank" rel="noopener"}

> Supreme Court judges affirm lower court decision to uphold privacy of its nine million users [...]
