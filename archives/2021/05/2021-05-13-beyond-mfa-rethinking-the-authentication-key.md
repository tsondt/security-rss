Title: Beyond MFA: Rethinking the Authentication Key
Date: 2021-05-13T15:39:01+00:00
Author: Tony Lauro
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Mobile Security;Web Security
Slug: beyond-mfa-rethinking-the-authentication-key

[Source](https://threatpost.com/mfa-rethinking-authentication-key/166136/){:target="_blank" rel="noopener"}

> Tony Lauro, director of security technology and strategy at Akamai, discusses hardware security dongles and using phones to act as surrogates for them. [...]
