Title: Qualcomm Chip Bug Opens Android Fans to Eavesdropping
Date: 2021-05-06T19:55:31+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Privacy;Vulnerabilities
Slug: qualcomm-chip-bug-opens-android-fans-to-eavesdropping

[Source](https://threatpost.com/qualcomm-chip-bug-android-eavesdropping/165934/){:target="_blank" rel="noopener"}

> A malicious app can exploit the issue, which could affect up to 30 percent of Android phones. [...]
