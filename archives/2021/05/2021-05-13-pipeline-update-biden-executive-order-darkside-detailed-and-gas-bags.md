Title: Pipeline Update: Biden Executive Order, DarkSide Detailed and Gas Bags
Date: 2021-05-13T11:39:56+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: pipeline-update-biden-executive-order-darkside-detailed-and-gas-bags

[Source](https://threatpost.com/pipeline-biden-darkside-gas-bags/166112/){:target="_blank" rel="noopener"}

> FBI/CISA warn about the RaaS network behind the Colonial hack, Colonial restarts operations, and researchers details groups that rent the ransomware. [...]
