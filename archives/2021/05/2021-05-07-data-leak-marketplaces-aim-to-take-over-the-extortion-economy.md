Title: Data leak marketplaces aim to take over the extortion economy
Date: 2021-05-07T08:16:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: data-leak-marketplaces-aim-to-take-over-the-extortion-economy

[Source](https://www.bleepingcomputer.com/news/security/data-leak-marketplaces-aim-to-take-over-the-extortion-economy/){:target="_blank" rel="noopener"}

> Cybercriminals are embracing data-theft extortion by creating dark web marketplaces that exist solely to sell stolen data. [...]
