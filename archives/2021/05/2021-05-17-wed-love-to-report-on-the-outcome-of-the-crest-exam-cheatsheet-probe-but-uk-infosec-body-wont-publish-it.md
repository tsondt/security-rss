Title: We'd love to report on the outcome of the CREST exam cheatsheet probe, but UK infosec body won't publish it
Date: 2021-05-17T10:47:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: wed-love-to-report-on-the-outcome-of-the-crest-exam-cheatsheet-probe-but-uk-infosec-body-wont-publish-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/17/crest_not_publishing_cert_exam_cheat_report/){:target="_blank" rel="noopener"}

> Why? It might reveal whistleblowers' names... British infosec accreditation body CREST has declared that it will not be publishing its full report into last year's exam-cheating scandal after all, triggering anger from the cybersecurity community.... [...]
