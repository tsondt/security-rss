Title: Pulse Secure VPNs Get Quick Fix for Critical RCE
Date: 2021-05-25T14:57:53+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: pulse-secure-vpns-get-quick-fix-for-critical-rce

[Source](https://threatpost.com/pulse-secure-vpns-critical-rce/166437/){:target="_blank" rel="noopener"}

> One of the workaround XML files automatically deactivates protection from an earlier workaround: a potential path to older vulnerabilities being opened again. [...]
