Title: New TsuNAME DNS bug allows attackers to DDoS authoritative DNS servers
Date: 2021-05-06T13:40:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: new-tsuname-dns-bug-allows-attackers-to-ddos-authoritative-dns-servers

[Source](https://www.bleepingcomputer.com/news/security/new-tsuname-dns-bug-allows-attackers-to-ddos-authoritative-dns-servers/){:target="_blank" rel="noopener"}

> Attackers can use a newly disclosed domain name server (DNS) vulnerability publicly known as TsuNAME as an amplification vector in large-scale reflection-based distributed denial of service (DDoS) attacks targeting authoritative DNS servers. [...]
