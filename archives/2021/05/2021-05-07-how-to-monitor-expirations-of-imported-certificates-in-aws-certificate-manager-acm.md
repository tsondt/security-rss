Title: How to monitor expirations of imported certificates in AWS Certificate Manager (ACM)
Date: 2021-05-07T18:04:37+00:00
Author: Dmitry Kagansky
Category: AWS Security
Tags: Advanced (300);AWS Certificate Manager;Security, Identity, & Compliance;ACM;ACM Private CA;Certificate;Certificate expiration;certificate renewal;Imported certificate;Security Blog
Slug: how-to-monitor-expirations-of-imported-certificates-in-aws-certificate-manager-acm

[Source](https://aws.amazon.com/blogs/security/how-to-monitor-expirations-of-imported-certificates-in-aws-certificate-manager-acm/){:target="_blank" rel="noopener"}

> Certificates are vital to maintaining trust and providing encryption to internal or external facing infrastructure and applications. AWS Certificate Manager (ACM) provides certificate services to any workload that requires them. Although ACM provides managed renewals that automatically renew certificates in most cases, there are exceptions, such as imported certs, where an automatic renewal isn’t possible. [...]
