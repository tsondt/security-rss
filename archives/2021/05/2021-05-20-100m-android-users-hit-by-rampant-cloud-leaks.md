Title: 100M Android Users Hit By Rampant Cloud Leaks
Date: 2021-05-20T20:45:01+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Mobile Security
Slug: 100m-android-users-hit-by-rampant-cloud-leaks

[Source](https://threatpost.com/100m-android-users-cloud-leaks/166372/){:target="_blank" rel="noopener"}

> Several mobile apps, some with 10 million downloads, have opened up personal data of users to the public internet – and most aren't fixed. [...]
