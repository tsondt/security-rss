Title: Deadshot: Open source DevOps tool stops sensitive data from being uploaded to GitHub
Date: 2021-05-31T12:26:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: deadshot-open-source-devops-tool-stops-sensitive-data-from-being-uploaded-to-github

[Source](https://portswigger.net/daily-swig/deadshot-open-source-devops-tool-stops-sensitive-data-from-being-uploaded-to-github){:target="_blank" rel="noopener"}

> Twilio security pros take aim at leaky commits [...]
