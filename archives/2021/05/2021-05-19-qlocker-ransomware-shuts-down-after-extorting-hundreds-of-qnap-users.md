Title: Qlocker ransomware shuts down after extorting hundreds of QNAP users
Date: 2021-05-19T13:59:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: qlocker-ransomware-shuts-down-after-extorting-hundreds-of-qnap-users

[Source](https://www.bleepingcomputer.com/news/security/qlocker-ransomware-shuts-down-after-extorting-hundreds-of-qnap-users/){:target="_blank" rel="noopener"}

> The Qlocker ransomware gang has shut down their operation after earning $350,000 in a month by exploiting vulnerabilities in QNAP NAS devices. [...]
