Title: Is 85% of US Critical Infrastructure in Private Hands?
Date: 2021-05-17T11:00:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;infrastructure;national security policy
Slug: is-85-of-us-critical-infrastructure-in-private-hands

[Source](https://www.schneier.com/blog/archives/2021/05/is-85-of-us-critical-infrastructure-in-private-hands.html){:target="_blank" rel="noopener"}

> Most US critical infrastructure is run by private corporations. This has major security implications, because it’s putting a random power company in — say — Ohio — up against the Russian cybercommand, which isn’t a fair fight. When this problem is discussed, people regularly quote the statistic that 85% of US critical infrastructure is in private hands. It’s a handy number, and matches our intuition. Still, I have never been able to find a factual basis, or anyone who knows where the number comes from. Paul Rosenzweig investigates, and reaches the same conclusion. So we don’t know the percentage, but [...]
