Title: Microsoft Patch Tuesday, May 2021 Edition
Date: 2021-05-11T20:28:19+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;CVE-2020-24587;CVE-2021-26419;CVE-2021-31166;DEVCORE;Dustin Childs;Immersive Labs;Internet Explorer 11;Kevin Breen;Microsoft Patch Tuesday May 2021;Orange Tsai;Satnam Narang;Tenable;trend micro;zdi
Slug: microsoft-patch-tuesday-may-2021-edition

[Source](https://krebsonsecurity.com/2021/05/microsoft-patch-tuesday-may-2021-edition/){:target="_blank" rel="noopener"}

> Microsoft today released fixes to plug at least 55 security holes in its Windows operating systems and other software. Four of these weaknesses can be exploited by malware and malcontents to seize complete, remote control over vulnerable systems without any help from users. On deck this month are patches to quash a wormable flaw, a creepy wireless bug, and yet another reason to call for the death of Microsoft’s Internet Explorer (IE) web browser. While May brings about half the normal volume of updates from Microsoft, there are some notable weaknesses that deserve prompt attention, particularly from enterprises. By all [...]
