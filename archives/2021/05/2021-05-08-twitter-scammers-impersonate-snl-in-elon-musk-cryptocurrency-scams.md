Title: Twitter scammers impersonate SNL in Elon Musk cryptocurrency scams
Date: 2021-05-08T18:54:31-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: twitter-scammers-impersonate-snl-in-elon-musk-cryptocurrency-scams

[Source](https://www.bleepingcomputer.com/news/security/twitter-scammers-impersonate-snl-in-elon-musk-cryptocurrency-scams/){:target="_blank" rel="noopener"}

> Twitter scammers are jumping on Elon Musk's hosting of Saturday Night Live to push cryptocurrency scams to steal people's Bitcoin, Ethereum, and Dogecoin. [...]
