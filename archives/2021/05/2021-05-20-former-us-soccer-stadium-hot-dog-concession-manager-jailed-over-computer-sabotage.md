Title: Former US soccer stadium hot dog concession manager jailed over computer sabotage
Date: 2021-05-20T15:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: former-us-soccer-stadium-hot-dog-concession-manager-jailed-over-computer-sabotage

[Source](https://portswigger.net/daily-swig/former-us-soccer-stadium-hot-dog-concession-manager-jailed-over-computer-sabotage){:target="_blank" rel="noopener"}

> Earthquakes shaken by hack that relied on unrevoked admin credentials [...]
