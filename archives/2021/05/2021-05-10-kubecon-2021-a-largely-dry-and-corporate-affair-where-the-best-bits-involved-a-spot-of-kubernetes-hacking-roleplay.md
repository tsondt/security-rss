Title: Kubecon 2021: A largely dry and corporate affair where the best bits involved a spot of Kubernetes-hacking roleplay
Date: 2021-05-10T19:12:11+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: kubecon-2021-a-largely-dry-and-corporate-affair-where-the-best-bits-involved-a-spot-of-kubernetes-hacking-roleplay

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/10/kubecon_2021_highlights/){:target="_blank" rel="noopener"}

> But we heard the message loud and clear – it's pretty much the standard runtime platform now Kubecon A session on how to hack into a Kubernetes cluster was among the highlights of a Kubecon where the main events were generally bland and corporate affairs, perhaps indicative of the technology now being a de facto infrastructure standard among enterprises.... [...]
