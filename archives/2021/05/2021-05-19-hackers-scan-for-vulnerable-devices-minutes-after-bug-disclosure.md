Title: Hackers scan for vulnerable devices minutes after bug disclosure
Date: 2021-05-19T08:57:01-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hackers-scan-for-vulnerable-devices-minutes-after-bug-disclosure

[Source](https://www.bleepingcomputer.com/news/security/hackers-scan-for-vulnerable-devices-minutes-after-bug-disclosure/){:target="_blank" rel="noopener"}

> Every hour, a threat actor starts a new scan on the public web for vulnerable systems, moving at a quicker pace than global enterprises when trying to identify serious vulnerabilities on their networks. [...]
