Title: Book Sale: Beyond Fear
Date: 2021-05-12T12:48:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Beyond Fear;books
Slug: book-sale-beyond-fear

[Source](https://www.schneier.com/blog/archives/2021/05/book-sale-beyond-fear.html){:target="_blank" rel="noopener"}

> I have 80 copies of my 2000 book Beyond Fear available at the very cheap price of $5 plus shipping. Note that there is a 20% chance that your book will have a “BT Counterpane” sticker on the front cover. Order your signed copy here. [...]
