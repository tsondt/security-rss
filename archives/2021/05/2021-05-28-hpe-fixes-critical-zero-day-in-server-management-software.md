Title: HPE Fixes Critical Zero-Day in Server Management Software
Date: 2021-05-28T15:11:25+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: hpe-fixes-critical-zero-day-in-server-management-software

[Source](https://threatpost.com/hpe-fixes-critical-zero-day-sim/166543/){:target="_blank" rel="noopener"}

> The bug in HPE SIM makes it easy as pie for attackers to remotely trigger code, no user interaction necessary. [...]
