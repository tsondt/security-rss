Title: Google Play to require privacy labels on apps in 2022, almost two years after Apple
Date: 2021-05-07T02:57:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: google-play-to-require-privacy-labels-on-apps-in-2022-almost-two-years-after-apple

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/07/google_play_security_privacy_disclosure_requirement/){:target="_blank" rel="noopener"}

> Developers want to do this, says Google. Ummm... guys, you do remember the thousands of malware nightmares you’ve hosted and sold? Google has decided the time has come to require app developers to disclose the data their wares collect, and their security practices, in their Play Store listings.... [...]
