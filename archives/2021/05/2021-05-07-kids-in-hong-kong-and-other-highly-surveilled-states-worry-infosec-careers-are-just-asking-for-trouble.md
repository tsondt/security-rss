Title: Kids in Hong Kong and other highly surveilled states worry infosec careers are just asking for trouble
Date: 2021-05-07T05:11:19+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: kids-in-hong-kong-and-other-highly-surveilled-states-worry-infosec-careers-are-just-asking-for-trouble

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/07/asia_ethical_hacking/){:target="_blank" rel="noopener"}

> Asia is already short millions of trainees; expert warns talent pipeline will dry up in response to government snooping Black Hat Asia Asian nations in which governments are keen on citizen surveillance struggle to develop ethical hackers, as prospective workers fear their activities may be misunderstood, according to security specialist Mika Devonshire.... [...]
