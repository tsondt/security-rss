Title: North Korean hackers behind CryptoCore multi-million dollar heists
Date: 2021-05-24T10:02:03-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: north-korean-hackers-behind-cryptocore-multi-million-dollar-heists

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-behind-cryptocore-multi-million-dollar-heists/){:target="_blank" rel="noopener"}

> Security researchers piecing together evidence from multiple attacks on cryptocurrency exchanges, attributed to a threat actor they named CryptoCore have established a strong connection to the North Korean state-sponsored group Lazarus. [...]
