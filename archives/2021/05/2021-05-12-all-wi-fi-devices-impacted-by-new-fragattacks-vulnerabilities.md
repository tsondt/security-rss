Title: All Wi-Fi devices impacted by new FragAttacks vulnerabilities
Date: 2021-05-12T09:24:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: all-wi-fi-devices-impacted-by-new-fragattacks-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/all-wi-fi-devices-impacted-by-new-fragattacks-vulnerabilities/){:target="_blank" rel="noopener"}

> Newly discovered Wi-Fi security vulnerabilities collectively known as FragAttacks (fragmentation and aggregation attacks) are impacting all Wi-Fi devices (including computers, smartphones, and smart devices) going back as far as 1997. [...]
