Title: FBI: Conti ransomware attacked 16 US healthcare, first responder orgs
Date: 2021-05-21T12:24:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-conti-ransomware-attacked-16-us-healthcare-first-responder-orgs

[Source](https://www.bleepingcomputer.com/news/security/fbi-conti-ransomware-attacked-16-us-healthcare-first-responder-orgs/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) says the Conti ransomware gang has attempted to breach the networks of over a dozen US healthcare and first responder organizations. [...]
