Title: Bait Boost: Phishers Delivering Increasingly Convincing Lures
Date: 2021-05-04T13:46:19+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Most Recent ThreatLists;Web Security
Slug: bait-boost-phishers-delivering-increasingly-convincing-lures

[Source](https://threatpost.com/bait-phishers-convincing-lures/165834/){:target="_blank" rel="noopener"}

> An intense hunt for corporate account credentials will continue into next quarter, researchers predict. [...]
