Title: Twilio's private GitHub repositories cloned by Codecov attacker, cloud comms platform confirms
Date: 2021-05-05T12:27:14+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: twilios-private-github-repositories-cloned-by-codecov-attacker-cloud-comms-platform-confirms

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/05/twilio_codecov_attack/){:target="_blank" rel="noopener"}

> Used the GitHub Codecov Action? Credentials may have been pilfered Cloud comms platform Twilio has confirmed its private GitHub repositories were cloned after it became the latest casualty of the compromised credential-stealing Codecov script.... [...]
