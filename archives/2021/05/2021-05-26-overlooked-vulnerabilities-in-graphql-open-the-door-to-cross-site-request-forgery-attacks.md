Title: Overlooked vulnerabilities in GraphQL open the door to cross-site request forgery attacks
Date: 2021-05-26T10:14:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: overlooked-vulnerabilities-in-graphql-open-the-door-to-cross-site-request-forgery-attacks

[Source](https://portswigger.net/daily-swig/overlooked-vulnerabilities-in-graphql-open-the-door-to-cross-site-request-forgery-attacks){:target="_blank" rel="noopener"}

> CSRF risk factors are often hidden, and misunderstood, in GraphQL implementations [...]
