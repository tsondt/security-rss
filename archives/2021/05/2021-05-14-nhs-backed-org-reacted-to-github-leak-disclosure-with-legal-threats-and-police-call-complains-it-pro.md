Title: NHS-backed org reacted to GitHub leak disclosure with legal threats and police call, complains IT pro
Date: 2021-05-14T10:02:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: nhs-backed-org-reacted-to-github-leak-disclosure-with-legal-threats-and-police-call-complains-it-pro

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/14/apperta_rob_dyke_disclosure_brouhaha/){:target="_blank" rel="noopener"}

> Retention of now-deleted security breach evidence sparks spat +Comment IT pro Rob Dyke says an NHS-backed company not only threatened him with legal action after he flagged up an exposed GitHub repository containing credentials and insecure code, it even called the police on him.... [...]
