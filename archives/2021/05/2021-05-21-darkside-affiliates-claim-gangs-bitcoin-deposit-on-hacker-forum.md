Title: DarkSide affiliates claim gang's bitcoin deposit on hacker forum
Date: 2021-05-21T03:29:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: darkside-affiliates-claim-gangs-bitcoin-deposit-on-hacker-forum

[Source](https://www.bleepingcomputer.com/news/security/darkside-affiliates-claim-gangs-bitcoin-deposit-on-hacker-forum/){:target="_blank" rel="noopener"}

> Since the DarkSide ransomware operation shut down a week ago, multiple affiliates have complained about not getting paid for past services and issued a claim for bitcoins in escrow at a hacker forum. [...]
