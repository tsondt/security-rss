Title: On the Taxonomy and Evolution of Ransomware
Date: 2021-05-31T13:41:37+00:00
Author: Oliver Tavakoli
Category: Threatpost
Tags: InfoSec Insider;Malware
Slug: on-the-taxonomy-and-evolution-of-ransomware

[Source](https://threatpost.com/taxonomy-evolution-ransomware/166462/){:target="_blank" rel="noopener"}

> Not all ransomware is the same! Oliver Tavakoli, CTO at Vectra AI, discusses the different species of this growing scourge. [...]
