Title: CISOs Struggle to Cope with Mounting Job Stress
Date: 2021-05-17T16:09:51+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Critical Infrastructure;Hacks;IoT;Malware;Most Recent ThreatLists;Vulnerabilities;Web Security
Slug: cisos-struggle-to-cope-with-mounting-job-stress

[Source](https://threatpost.com/cisos-struggle-job-stress/166221/){:target="_blank" rel="noopener"}

> Pandemic and evolving IT demands are having a major, negative impact on CISOs' mental health, a survey found. [...]
