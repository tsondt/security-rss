Title: Hard cheese: Stilton snap shared via EncroChat leads to drug dealer's downfall
Date: 2021-05-25T06:30:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: hard-cheese-stilton-snap-shared-via-encrochat-leads-to-drug-dealers-downfall

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/25/cheese_fingerprint_prison/){:target="_blank" rel="noopener"}

> Brit thrown in the clink for 13 years after 'palm-print' lifted from internet photo A drug dealer's ham-handed OPSEC allowed British police to identify him from a picture of him holding a block of cheese, which led to his arrest, guilty plea, and a sentence of 13 years and six months in prison.... [...]
