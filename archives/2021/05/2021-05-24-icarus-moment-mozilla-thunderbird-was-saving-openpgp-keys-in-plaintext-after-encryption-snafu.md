Title: Icarus moment: Mozilla Thunderbird was saving OpenPGP keys in plaintext after encryption snafu
Date: 2021-05-24T17:15:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: icarus-moment-mozilla-thunderbird-was-saving-openpgp-keys-in-plaintext-after-encryption-snafu

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/24/mozilla_thunderbird_openpgp_plaintext_keys/){:target="_blank" rel="noopener"}

> Cockup has since been patched in latest release Mozilla Thunderbird spent the last couple of months saving some users’ OpenPGP keys in plain text – but that’s now been patched, the author of both the bug and the patch fixing it has told The Register.... [...]
