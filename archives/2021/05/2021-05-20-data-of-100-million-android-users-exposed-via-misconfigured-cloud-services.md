Title: Data of 100+ million Android users exposed via misconfigured cloud services
Date: 2021-05-20T07:50:03-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: data-of-100-million-android-users-exposed-via-misconfigured-cloud-services

[Source](https://www.bleepingcomputer.com/news/security/data-of-100-plus-million-android-users-exposed-via-misconfigured-cloud-services/){:target="_blank" rel="noopener"}

> A banking trojan named Bizarro that originates from Brazil has crossed the borders and started to target customers of 70 banks in Europe and South America. [...]
