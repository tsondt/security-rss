Title: UK govt seeks advice on defending against supply-chain cyberattacks
Date: 2021-05-17T12:48:47-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: uk-govt-seeks-advice-on-defending-against-supply-chain-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/uk-govt-seeks-advice-on-defending-against-supply-chain-cyberattacks/){:target="_blank" rel="noopener"}

> Today, the UK government has announced a call for advice on defending against software supply-chain attacks and ways to strengthen IT Managed Service Providers (MSPs) across the country. The move comes after last week when President Biden had issued an executive order to increase cybersecurity defenses across the U.S. [...]
