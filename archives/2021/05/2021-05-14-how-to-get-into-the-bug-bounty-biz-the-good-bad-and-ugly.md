Title: How to Get into the Bug-Bounty Biz: The Good, Bad and Ugly
Date: 2021-05-14T12:00:27+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Bug Bounty;Threatpost Webinar Series;Vulnerabilities
Slug: how-to-get-into-the-bug-bounty-biz-the-good-bad-and-ugly

[Source](https://threatpost.com/how-to-bug-bounties/165657/){:target="_blank" rel="noopener"}

> Experts from Intel, GitHub and KnowBe4 weigh in on what you need to succeed at security bug-hunting. [...]
