Title: Irish healthcare shuts down IT systems after ransomware attack
Date: 2021-05-14T07:44:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: irish-healthcare-shuts-down-it-systems-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/irish-healthcare-shuts-down-it-systems-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Ireland's Health Service Executive(HSE), the country's publicly funded healthcare system, has shut down all IT systems after its network was breached in a ransomware attack. [...]
