Title: Microsoft emits more fixes for Exchange Server plus patches for remote-code exec holes in HTTP stack, Visual Studio
Date: 2021-05-11T19:08:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: microsoft-emits-more-fixes-for-exchange-server-plus-patches-for-remote-code-exec-holes-in-http-stack-visual-studio

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/11/microsoft_patch_tuesday_exchange_hyperv/){:target="_blank" rel="noopener"}

> Plus: Grab your updates for Adobe, SAP, Android, Intel Patch Tuesday Microsoft's May Patch Tuesday brought a lighter-than-usual load of 55 fixes for 32 of the Windows giant's applications and services, which is about half what was served up in April.... [...]
