Title: Crypto exchange glitch causes duplicate purchases, delayed credits
Date: 2021-05-13T06:36:08-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: CryptoCurrency
Slug: crypto-exchange-glitch-causes-duplicate-purchases-delayed-credits

[Source](https://www.bleepingcomputer.com/news/security/crypto-exchange-glitch-causes-duplicate-purchases-delayed-credits/){:target="_blank" rel="noopener"}

> This week, recurring glitches on the popular cryptocurrency exchange Crypto.com caused multi-day delays for users in receiving their purchased assets. Moreover, those reattempting "declined" or "expired" transactions were charged multiple times for duplicate purchases. [...]
