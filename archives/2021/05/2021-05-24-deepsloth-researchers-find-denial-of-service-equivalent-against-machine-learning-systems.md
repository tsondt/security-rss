Title: DeepSloth: Researchers find denial-of-service equivalent against machine learning systems
Date: 2021-05-24T10:48:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: deepsloth-researchers-find-denial-of-service-equivalent-against-machine-learning-systems

[Source](https://portswigger.net/daily-swig/deepsloth-researchers-find-denial-of-service-equivalent-against-machine-learning-systems){:target="_blank" rel="noopener"}

> DoS-style attacks can slow deep neural networks to a crawl [...]
