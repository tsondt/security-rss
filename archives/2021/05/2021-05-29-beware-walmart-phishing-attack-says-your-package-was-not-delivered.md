Title: Beware: Walmart phishing attack says your package was not delivered
Date: 2021-05-29T14:41:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: beware-walmart-phishing-attack-says-your-package-was-not-delivered

[Source](https://www.bleepingcomputer.com/news/security/beware-walmart-phishing-attack-says-your-package-was-not-delivered/){:target="_blank" rel="noopener"}

> A Walmart phishing campaign is underway that attempts to steal your personal information and verifies your email for further phishing attacks. [...]
