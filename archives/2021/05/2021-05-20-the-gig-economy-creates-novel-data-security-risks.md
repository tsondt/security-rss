Title: The Gig Economy Creates Novel Data-Security Risks
Date: 2021-05-20T17:59:30+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Mobile Security;RSAC;Web Security
Slug: the-gig-economy-creates-novel-data-security-risks

[Source](https://threatpost.com/the-gig-economy-data-security-risks/166359/){:target="_blank" rel="noopener"}

> Enterprises are embracing on-demand freelance help – but the practice, while growing, opens up entirely new avenues of cyber-risk. [...]
