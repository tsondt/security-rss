Title: FBI to share compromised passwords with Have I Been Pwned
Date: 2021-05-28T00:05:55-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fbi-to-share-compromised-passwords-with-have-i-been-pwned

[Source](https://www.bleepingcomputer.com/news/security/fbi-to-share-compromised-passwords-with-have-i-been-pwned/){:target="_blank" rel="noopener"}

> The FBI will soon begin to share compromised passwords with Have I Been Pwned's 'Password Pwned' service that were discovered during law enforcement investigations. [...]
