Title: South Korea orders urgent review of energy infrastructure cybersecurity
Date: 2021-05-12T03:38:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: south-korea-orders-urgent-review-of-energy-infrastructure-cybersecurity

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/12/south_korea_security_review/){:target="_blank" rel="noopener"}

> No prizes for guessing why, as Colonial Pipeline outage stretches patience and looks like lasting a week South Korea’s Ministry of Trade, Energy and Infrastructure has ordered a review of the cybersecurity preparedness of the nation’s energy infrastructure.... [...]
