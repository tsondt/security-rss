Title: Biden’s Cybersecurity Executive Order Puts Emphasis on the Wrong Issues
Date: 2021-05-27T12:00:01+00:00
Author: David “moose” Wolpoff
Category: Threatpost
Tags: Cloud Security;Government;InfoSec Insider;Malware;Vulnerabilities;Web Security
Slug: bidens-cybersecurity-executive-order-puts-emphasis-on-the-wrong-issues

[Source](https://threatpost.com/bidens-cybersecurity-executive-order-wrong-issues/166479/){:target="_blank" rel="noopener"}

> David Wolpoff, CTO at Randori, argues that the call for rapid cloud transition Is a dangerous proposition: "Mistakes will be made, creating opportunities for our adversaries. [...]
