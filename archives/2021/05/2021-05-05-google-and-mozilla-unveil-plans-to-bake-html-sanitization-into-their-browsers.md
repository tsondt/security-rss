Title: Google and Mozilla unveil plans to bake HTML sanitization into their browsers
Date: 2021-05-05T13:37:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-and-mozilla-unveil-plans-to-bake-html-sanitization-into-their-browsers

[Source](https://portswigger.net/daily-swig/google-and-mozilla-unveil-plans-to-bake-html-sanitization-into-their-browsers){:target="_blank" rel="noopener"}

> Tech giants take aim at client-side scripting attacks [...]
