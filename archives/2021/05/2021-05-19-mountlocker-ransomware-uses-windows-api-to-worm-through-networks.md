Title: MountLocker ransomware uses Windows API to worm through networks
Date: 2021-05-19T03:31:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: mountlocker-ransomware-uses-windows-api-to-worm-through-networks

[Source](https://www.bleepingcomputer.com/news/security/mountlocker-ransomware-uses-windows-api-to-worm-through-networks/){:target="_blank" rel="noopener"}

> The MountLocker ransomware operation now uses enterprise Windows Active Directory APIs to worm through networks. [...]
