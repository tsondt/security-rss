Title: Investment Scammer John Davies Reinvents Himself?
Date: 2021-05-07T13:15:27+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;ABA Group & Associates LTD;binary options;Euro Forex Investments Ltd.;Harper & Partners Ltd.;Hempton Business Management LLP;hemptonllp.com;Inside Knowledge;James Donahoe;Joaquim Magro Almeida;John Bernard;John Cavendish;John Clifton Davies;Jonathan Bibi;Mariya Kulikova;Mariya Kulykova;pyramid scheme;shelf corporation;TBA & Associates;The Private Office of John Bernard;Transparency International
Slug: investment-scammer-john-davies-reinvents-himself

[Source](https://krebsonsecurity.com/2021/05/investment-scammer-john-davies-reinvents-himself/){:target="_blank" rel="noopener"}

> John Bernard, a pseudonym used by a convicted thief and con artist named John Clifton Davies who’s fleeced dozens of technology startups out of an estimated $30 million, appears to have reinvented himself again after being exposed in a recent investigative series published here. Sources tell KrebsOnSecurity that Davies/Bernard is now posing as John Cavendish and head of a new “private office” called Hempton Business Management LLP. John Davies is a U.K. man who absconded from justice before being convicted on multiple counts of fraud in 2015. Prior to his conviction, Davies served 16 months in jail before being cleared [...]
