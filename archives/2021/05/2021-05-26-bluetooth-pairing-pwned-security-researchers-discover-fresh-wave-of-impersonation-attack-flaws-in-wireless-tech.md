Title: Bluetooth pairing, pwned: Security researchers discover fresh wave of ‘impersonation attack’ flaws in wireless tech
Date: 2021-05-26T12:25:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: bluetooth-pairing-pwned-security-researchers-discover-fresh-wave-of-impersonation-attack-flaws-in-wireless-tech

[Source](https://portswigger.net/daily-swig/bluetooth-pairing-pwned-security-researchers-discover-fresh-wave-of-impersonation-attack-flaws-in-wireless-tech){:target="_blank" rel="noopener"}

> Inherent weaknesses in short-range radio technology laid bare [...]
