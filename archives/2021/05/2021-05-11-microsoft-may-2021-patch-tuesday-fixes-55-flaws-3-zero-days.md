Title: Microsoft May 2021 Patch Tuesday fixes 55 flaws, 3 zero-days
Date: 2021-05-11T13:28:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-may-2021-patch-tuesday-fixes-55-flaws-3-zero-days

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-may-2021-patch-tuesday-fixes-55-flaws-3-zero-days/){:target="_blank" rel="noopener"}

> Today is Microsoft's May 2021 Patch Tuesday, and with it comes three zero-day vulnerabilities, so Windows admins will be rushing to apply updates. [...]
