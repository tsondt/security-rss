Title: Email Campaign Spreads StrRAT Fake-Ransomware RAT
Date: 2021-05-21T13:27:14+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: email-campaign-spreads-strrat-fake-ransomware-rat

[Source](https://threatpost.com/email-campaign-fake-ransomware-rat/166378/){:target="_blank" rel="noopener"}

> Microsoft Security discovered malicious PDFs that download Java-based StrRAT, which can steal credentials and change file names but doesn't actually encrypt. [...]
