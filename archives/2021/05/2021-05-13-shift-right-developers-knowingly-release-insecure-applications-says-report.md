Title: Shift right: Developers knowingly release insecure applications, says report
Date: 2021-05-13T14:42:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: shift-right-developers-knowingly-release-insecure-applications-says-report

[Source](https://portswigger.net/daily-swig/shift-right-developers-knowingly-release-insecure-applications-says-report){:target="_blank" rel="noopener"}

> Concerns surface about resource constraints, information sharing, and buck-passing in the workplace [...]
