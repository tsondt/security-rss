Title: How to Tell a Job Offer from an ID Theft Trap
Date: 2021-05-21T17:41:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Employment Fraud;Latest Warnings;Web Fraud 2.0;Erica Siegel;fbi;Geosyntec Consultants;LinkedIn;LinkedIn job scam;Troy Gwin
Slug: how-to-tell-a-job-offer-from-an-id-theft-trap

[Source](https://krebsonsecurity.com/2021/05/how-to-tell-a-job-offer-from-an-id-theft-trap/){:target="_blank" rel="noopener"}

> One of the oldest scams around — the fake job interview that seeks only to harvest your personal and financial data — is on the rise, the FBI warns. Here’s the story of a recent LinkedIn impersonation scam that led to more than 100 people getting duped, and one almost-victim who decided the job offer was too-good-to-be-true. Last week, someone began began posting classified notices on LinkedIn for different design consulting jobs at Geosyntec Consultants, an environmental engineering firm based in the Washington, D.C. area. Those who responded were told their application for employment was being reviewed and that they [...]
