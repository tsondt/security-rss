Title: New Zealand hospitals infected by ransomware, cancel some surgeries
Date: 2021-05-19T04:59:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: new-zealand-hospitals-infected-by-ransomware-cancel-some-surgeries

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/19/new_zealand_hospitals_taken_down/){:target="_blank" rel="noopener"}

> Intrusion believed to have entered through email New Zealand's Waikato District Health Board (DHB) has been hit with a strain of ransomware that took down most IT services Tuesday morning and drastically reduced services at six of its affiliate hospitals.... [...]
