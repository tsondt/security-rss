Title: US and Australia warn of escalating Avaddon ransomware attacks
Date: 2021-05-10T12:19:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-and-australia-warn-of-escalating-avaddon-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-and-australia-warn-of-escalating-avaddon-ransomware-attacks/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) and the Australian Cyber Security Centre (ACSC) are warning of an ongoing Avaddon ransomware campaign targeting organizations from an extensive array of sectors in the US and worldwide. [...]
