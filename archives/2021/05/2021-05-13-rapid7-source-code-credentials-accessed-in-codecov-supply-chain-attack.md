Title: Rapid7 source code, credentials accessed in Codecov supply-chain attack
Date: 2021-05-13T15:56:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: rapid7-source-code-credentials-accessed-in-codecov-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/rapid7-source-code-credentials-accessed-in-codecov-supply-chain-attack/){:target="_blank" rel="noopener"}

> US cybersecurity firm Rapid7 has disclosed that some source code repositories were accessed in a security incident linked to the supply-chain attack that recently impacted customers of the popular Codecov code coverage tool. [...]
