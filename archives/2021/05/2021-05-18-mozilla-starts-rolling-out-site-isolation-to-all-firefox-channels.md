Title: Mozilla starts rolling out Site Isolation to all Firefox channels
Date: 2021-05-18T14:31:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Software
Slug: mozilla-starts-rolling-out-site-isolation-to-all-firefox-channels

[Source](https://www.bleepingcomputer.com/news/security/mozilla-starts-rolling-out-site-isolation-to-all-firefox-channels/){:target="_blank" rel="noopener"}

> Mozilla has started rolling out the Site Isolation security feature to all Firefox channels, now also protecting users in the Beta and Release channels from attacks launched via malicious websites. [...]
