Title: Beijing twirls ban-hammer at 84 more apps it says need to stop slurping excess data
Date: 2021-05-12T05:19:37+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: beijing-twirls-ban-hammer-at-84-more-apps-it-says-need-to-stop-slurping-excess-data

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/12/central_cyberspace_affairs_commission_warns_84_apps/){:target="_blank" rel="noopener"}

> Online lending apps and more given fifteen days to ‘rectify’ behaviour China’s Central Cyberspace Affairs Commission has named 84 apps it says breach local privacy laws and given their developers 15 days to “rectify” their code.... [...]
