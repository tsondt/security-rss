Title: Friday Squid Blogging: Picking up Squid on the Beach
Date: 2021-05-21T21:02:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-picking-up-squid-on-the-beach

[Source](https://www.schneier.com/blog/archives/2021/05/friday-squid-blogging-picking-up-squid-on-the-beach.html){:target="_blank" rel="noopener"}

> Make sure they’re dead. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
