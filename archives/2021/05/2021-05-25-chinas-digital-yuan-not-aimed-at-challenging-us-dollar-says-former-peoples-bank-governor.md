Title: China's Digital Yuan not aimed at challenging US dollar, says former People’s Bank governor
Date: 2021-05-25T05:59:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: chinas-digital-yuan-not-aimed-at-challenging-us-dollar-says-former-peoples-bank-governor

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/25/zhou_xiaochuan_digital_yuan_speech/){:target="_blank" rel="noopener"}

> It’s all about domestic efficiency, and if that helps China to become a bigger player then so be it A former governor of the People's Bank of China has given a speech in which he suggested that China's Digital Yuan is not intended to increase China's influence over global financial systems.... [...]
