Title: New US Executive Order on Cybersecurity
Date: 2021-05-13T14:39:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;national security policy;security standards
Slug: new-us-executive-order-on-cybersecurity

[Source](https://www.schneier.com/blog/archives/2021/05/new-us-executive-order-on-cybersecurity.html){:target="_blank" rel="noopener"}

> President Biden signed an executive order to improve government cybersecurity, setting new security standards for software sold to the federal government. For the first time, the United States will require all software purchased by the federal government to meet, within six months, a series of new cybersecurity standards. Although the companies would have to “self-certify,” violators would be removed from federal procurement lists, which could kill their chances of selling their products on the commercial market. I’m a big fan of these sorts of measures. The US government is a big enough market that vendors will try to comply with [...]
