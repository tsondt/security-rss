Title: American Express Fined for Sending Millions of Spam Messages
Date: 2021-05-24T20:53:10+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Web Security
Slug: american-express-fined-for-sending-millions-of-spam-messages

[Source](https://threatpost.com/american-express-fined-spam/166412/){:target="_blank" rel="noopener"}

> British regulators ruled that Amex sent 4 million nuisance emails to opted-out customers. [...]
