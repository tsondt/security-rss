Title: Indonesian govt blocks access to RaidForums hacking forum after data leak
Date: 2021-05-24T11:21:31-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: indonesian-govt-blocks-access-to-raidforums-hacking-forum-after-data-leak

[Source](https://www.bleepingcomputer.com/news/security/indonesian-govt-blocks-access-to-raidforums-hacking-forum-after-data-leak/){:target="_blank" rel="noopener"}

> The Indonesian government is blocking access to the RaidForums hacking forum after the alleged personal information of Indonesian citizens was posted online. [...]
