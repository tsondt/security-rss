Title: Troy Hunt at Black Hat Asia: ‘We’re making it very difficult for people to make good security decisions’
Date: 2021-05-06T15:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: troy-hunt-at-black-hat-asia-were-making-it-very-difficult-for-people-to-make-good-security-decisions

[Source](https://portswigger.net/daily-swig/troy-hunt-at-black-hat-asia-were-making-it-very-difficult-for-people-to-make-good-security-decisions){:target="_blank" rel="noopener"}

> Have I Been Pwned founder’s keynote offered a sobering counterpoint to the well-meaning ‘World Password Day’ [...]
