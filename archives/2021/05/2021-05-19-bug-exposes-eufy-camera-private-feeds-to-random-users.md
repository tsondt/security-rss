Title: Bug Exposes Eufy Camera Private Feeds to Random Users
Date: 2021-05-19T13:28:38+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: IoT;Privacy
Slug: bug-exposes-eufy-camera-private-feeds-to-random-users

[Source](https://threatpost.com/eufy-cam-private-feeds/166288/){:target="_blank" rel="noopener"}

> Customers panic and question parent company Anker’s security and privacy practices after learning their home videos could be accessed and even controlled by strangers due to a server-upgrade glitch. [...]
