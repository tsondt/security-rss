Title: Critical Cisco SD-WAN, HyperFlex Bugs Threaten Corporate Networks
Date: 2021-05-06T17:54:33+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: critical-cisco-sd-wan-hyperflex-bugs-threaten-corporate-networks

[Source](https://threatpost.com/critical-cisco-sd-wan-hyperflex-bugs/165923/){:target="_blank" rel="noopener"}

> The networking giant has rolled out patches for remote code-execution and command-injection security holes that could give attackers keys to the kingdom. [...]
