Title: Ransomware victim Colonial Pipeline paid $5m to get oil pumping again, restored from backups anyway – report
Date: 2021-05-13T17:44:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ransomware-victim-colonial-pipeline-paid-5m-to-get-oil-pumping-again-restored-from-backups-anyway-report

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/13/colonial_pipeline_ransom/){:target="_blank" rel="noopener"}

> Anonymous sources get into war-by-media counterbriefing Colonial Pipeline's operators reportedly paid $5m to regain control of their digital systems and get the pipeline pumping oil following last week's ransomware infection.... [...]
