Title: AI Security Risk Assessment Tool
Date: 2021-05-11T14:53:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: artificial intelligence;automation;machine learning;risk assessment
Slug: ai-security-risk-assessment-tool

[Source](https://www.schneier.com/blog/archives/2021/05/ai-security-risk-assessment-tool.html){:target="_blank" rel="noopener"}

> Microsoft researchers just released an open-source automation tool for security testing AI systems: “ Counterfit.” Details on their blog. [...]
