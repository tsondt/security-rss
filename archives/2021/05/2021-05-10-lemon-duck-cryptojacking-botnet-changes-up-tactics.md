Title: Lemon Duck Cryptojacking Botnet Changes Up Tactics
Date: 2021-05-10T17:37:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: lemon-duck-cryptojacking-botnet-changes-up-tactics

[Source](https://threatpost.com/lemon-duck-cryptojacking-botnet-tactics/165986/){:target="_blank" rel="noopener"}

> The sophisticated threat is targeting Microsoft Exchange servers via ProxyLogon in a wave of fresh attacks against North American targets. [...]
