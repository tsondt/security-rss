Title: Codecov hackers gained access to Monday.com source code
Date: 2021-05-18T02:33:35-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: codecov-hackers-gained-access-to-mondaycom-source-code

[Source](https://www.bleepingcomputer.com/news/security/codecov-hackers-gained-access-to-mondaycom-source-code/){:target="_blank" rel="noopener"}

> Monday.com has recently disclosed the impact of the Codecov supply-chain attack that affected multiple companies. As reported by BleepingComputer last month, popular code coverage tool Codecov had been a victim of a supply-chain attack that lasted for two months. [...]
