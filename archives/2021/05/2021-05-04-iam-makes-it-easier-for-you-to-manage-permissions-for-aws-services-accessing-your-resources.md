Title: IAM makes it easier for you to manage permissions for AWS services accessing your resources
Date: 2021-05-04T16:05:19+00:00
Author: Ilya Epshteyn
Category: AWS Security
Tags: AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;AWS IAM;data access;Data perimeters;Identity;least privilege;Security Blog
Slug: iam-makes-it-easier-for-you-to-manage-permissions-for-aws-services-accessing-your-resources

[Source](https://aws.amazon.com/blogs/security/iam-makes-it-easier-to-manage-permissions-for-aws-services-accessing-resources/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) customers are storing an unprecedented amount of data on AWS for a range of use cases, including data lakes and analytics, machine learning, and enterprise applications. Customers secure their data by implementing data security controls including identity and access management, network security, and encryption. For non-public, sensitive data, customers want to [...]
