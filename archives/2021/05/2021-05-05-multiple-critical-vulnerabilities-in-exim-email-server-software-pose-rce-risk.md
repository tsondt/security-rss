Title: Multiple critical vulnerabilities in Exim email server software pose RCE risk
Date: 2021-05-05T14:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: multiple-critical-vulnerabilities-in-exim-email-server-software-pose-rce-risk

[Source](https://portswigger.net/daily-swig/multiple-critical-vulnerabilities-in-exim-email-server-software-pose-rce-risk){:target="_blank" rel="noopener"}

> Msg spool attack threat [...]
