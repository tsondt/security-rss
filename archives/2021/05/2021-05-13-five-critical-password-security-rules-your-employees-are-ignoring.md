Title: Five Critical Password Security Rules Your Employees Are Ignoring
Date: 2021-05-13T13:00:19+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Sponsored;Web Security
Slug: five-critical-password-security-rules-your-employees-are-ignoring

[Source](https://threatpost.com/5-password-security-rules-employees-ignoring/165686/){:target="_blank" rel="noopener"}

> According to Keeper Security’s Workplace Password Malpractice Report, many remote workers aren’t following best practices for password security. [...]
