Title: 21 nails in Exim mail server: Vulnerabilities enable 'full remote unauthenticated code execution', millions of boxes at risk
Date: 2021-05-05T17:20:06+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: 21-nails-in-exim-mail-server-vulnerabilities-enable-full-remote-unauthenticated-code-execution-millions-of-boxes-at-risk

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/05/21_nails_in_exim_mail/){:target="_blank" rel="noopener"}

> Nearly 4 million to be exact, say researchers Researchers at security biz Qualys discovered 21 vulnerabilities in Exim, a popular mail server, which can be chained to obtain "a full remote unauthenticated code execution and gain root privileges on the Exim Server."... [...]
