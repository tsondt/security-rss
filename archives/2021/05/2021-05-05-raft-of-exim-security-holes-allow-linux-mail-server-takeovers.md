Title: Raft of Exim Security Holes Allow Linux Mail Server Takeovers
Date: 2021-05-05T18:15:39+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: raft-of-exim-security-holes-allow-linux-mail-server-takeovers

[Source](https://threatpost.com/exim-security-linux-mail-server-takeovers/165894/){:target="_blank" rel="noopener"}

> Remote code execution, privilege escalation to root and lateral movement through a victim's environment are all on offer for the unpatched or unaware. [...]
