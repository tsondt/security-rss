Title: AWS Verified episode 5: A conversation with Eric Rosenbach of Harvard University’s Belfer Center
Date: 2021-05-20T18:37:31+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Belfer Center;Cloud security;cybersecurity;enterprise IT security;Eric Rosenbach;Security Blog;Verified;Zero Trust
Slug: aws-verified-episode-5-a-conversation-with-eric-rosenbach-of-harvard-universitys-belfer-center

[Source](https://aws.amazon.com/blogs/security/aws-verified-episode-5-a-conversation-with-eric-rosenbach-of-harvard-universitys-belfer-center/){:target="_blank" rel="noopener"}

> I am pleased to share the latest episode of AWS Verified, where we bring you conversations with global cybersecurity leaders about important issues, such as how to create a culture of security, cyber resiliency, Zero Trust, and other emerging security trends. Recently, I got the opportunity to experience distance learning when I took the AWS [...]
