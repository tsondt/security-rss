Title: Microsoft Exchange admin portal blocked by expired SSL certificate
Date: 2021-05-23T15:21:39-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-exchange-admin-portal-blocked-by-expired-ssl-certificate

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-exchange-admin-portal-blocked-by-expired-ssl-certificate/){:target="_blank" rel="noopener"}

> The Microsoft Exchange admin portal is currently inaccessible from some browsers after Microsoft forgot to renew the SSL certificate for the website. [...]
