Title: Russian state hackers switch targets after US joint advisories
Date: 2021-05-07T11:29:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: russian-state-hackers-switch-targets-after-us-joint-advisories

[Source](https://www.bleepingcomputer.com/news/security/russian-state-hackers-switch-targets-after-us-joint-advisories/){:target="_blank" rel="noopener"}

> Russian Foreign Intelligence Service (SVR) operators have switched their attacks to target new vulnerabilities in reaction to US govt advisories published last month with info on SVR tactics, tools, techniques, and capabilities used in ongoing attacks. [...]
