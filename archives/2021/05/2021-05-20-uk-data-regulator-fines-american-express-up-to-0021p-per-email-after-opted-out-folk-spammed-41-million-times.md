Title: UK data regulator fines American Express up to 0.021p per email after opted-out folk spammed 4.1 million times
Date: 2021-05-20T13:45:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-data-regulator-fines-american-express-up-to-0021p-per-email-after-opted-out-folk-spammed-41-million-times

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/20/amex_fine_50m_spam/){:target="_blank" rel="noopener"}

> Bank made $1.4bn in profits alone last quarter American Express has been fined 0.009 per cent of its annual profits by the Information Commissioner's Office (ICO) after spamming people who opted out of its marketing emails with 4.1 million unwanted messages.... [...]
