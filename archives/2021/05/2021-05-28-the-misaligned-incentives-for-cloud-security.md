Title: The Misaligned Incentives for Cloud Security
Date: 2021-05-28T11:20:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: certificates;cloud computing;cyberattack;cyberespionage;essays;externalities;hacking;Russia
Slug: the-misaligned-incentives-for-cloud-security

[Source](https://www.schneier.com/blog/archives/2021/05/the-misaligned-incentives-for-cloud-security.html){:target="_blank" rel="noopener"}

> Russia’s Sunburst cyberespionage campaign, discovered late last year, impacted more than 100 large companies and US federal agencies, including the Treasury, Energy, Justice, and Homeland Security departments. A crucial part of the Russians’ success was their ability to move through these organizations by compromising cloud and local network identity systems to then access cloud accounts and pilfer emails and files. Hackers said by the US government to have been working for the Kremlin targeted a widely used Microsoft cloud service that synchronizes user identities. The hackers stole security certificates to create their own identities, which allowed them to bypass safeguards [...]
