Title: Klarna privacy clanger blamed on buggy software update
Date: 2021-05-28T15:04:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: klarna-privacy-clanger-blamed-on-buggy-software-update

[Source](https://portswigger.net/daily-swig/klarna-privacy-clanger-blamed-on-buggy-software-update){:target="_blank" rel="noopener"}

> Fintech firm rules out external attack [...]
