Title: Microsoft: Russian SVR hackers target govt agencies from 24 countries
Date: 2021-05-28T08:08:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-russian-svr-hackers-target-govt-agencies-from-24-countries

[Source](https://www.bleepingcomputer.com/news/security/microsoft-russian-svr-hackers-target-govt-agencies-from-24-countries/){:target="_blank" rel="noopener"}

> The Microsoft Threat Intelligence Center (MSTIC) has discovered that the Russian-backed hackers behind the SolarWinds supply-chain attack are now coordinating an ongoing phishing campaign targeting government agencies worldwide. [...]
