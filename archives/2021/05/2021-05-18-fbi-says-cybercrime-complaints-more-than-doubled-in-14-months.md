Title: FBI says cybercrime complaints more than doubled in 14 months
Date: 2021-05-18T12:20:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fbi-says-cybercrime-complaints-more-than-doubled-in-14-months

[Source](https://www.bleepingcomputer.com/news/security/fbi-says-cybercrime-complaints-more-than-doubled-in-14-months/){:target="_blank" rel="noopener"}

> The FBI's Internet Crime Complaint Center (IC3) has seen a massive 100% in cybercrime complaints over the past 14 months. [...]
