Title: N3TW0RM ransomware emerges in wave of cyberattacks in Israel
Date: 2021-05-03T17:46:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: n3tw0rm-ransomware-emerges-in-wave-of-cyberattacks-in-israel

[Source](https://www.bleepingcomputer.com/news/security/n3tw0rm-ransomware-emerges-in-wave-of-cyberattacks-in-israel/){:target="_blank" rel="noopener"}

> A new ransomware gang known as 'N3TW0RM' is targeting Israeli companies in a wave of cyberattacks starting last week. [...]
