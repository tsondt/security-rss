Title: Scripps Health Cyberattack Causes Widespread Hospital Outages
Date: 2021-05-03T21:04:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Hacks;Malware
Slug: scripps-health-cyberattack-causes-widespread-hospital-outages

[Source](https://threatpost.com/scripps-health-cyberattack-hospital-outages/165817/){:target="_blank" rel="noopener"}

> The San Diego-based hospital system diverted ambulances to other medical centers after a suspected ransomware attack. [...]
