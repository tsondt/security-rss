Title: US nuclear weapon bunker security secrets spill from online flashcards since 2013
Date: 2021-05-28T18:51:27+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: us-nuclear-weapon-bunker-security-secrets-spill-from-online-flashcards-since-2013

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/28/flashcards_military_nuclear/){:target="_blank" rel="noopener"}

> Leaked data proves very educational Details of some US nuclear missile bunkers in Europe, which contain live warheads, along with secret codewords used by guards to signal that they’re being threatened by enemies, were exposed for nearly a decade through online flashcards used for education, but which were left publicly available.... [...]
