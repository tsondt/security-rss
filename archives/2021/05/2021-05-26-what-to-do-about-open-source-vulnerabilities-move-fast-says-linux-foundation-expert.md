Title: What to do about open source vulnerabilities? Move fast, says Linux Foundation expert
Date: 2021-05-26T11:34:05+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: what-to-do-about-open-source-vulnerabilities-move-fast-says-linux-foundation-expert

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/26/open_source_vluns_talk_qcon/){:target="_blank" rel="noopener"}

> The CIO does not decide how soon you need to respond. 'The person who decides is the attacker' QCon Plus Automated testing and rapid deployment are critical to defending against vulnerabilities in open source software, said David Wheeler, director of Open Source Supply Chain Security at the Linux Foundation.... [...]
