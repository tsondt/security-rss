Title: Conti ransomware gives HSE Ireland free decryptor, still selling data
Date: 2021-05-20T10:46:35-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: conti-ransomware-gives-hse-ireland-free-decryptor-still-selling-data

[Source](https://www.bleepingcomputer.com/news/security/conti-ransomware-gives-hse-ireland-free-decryptor-still-selling-data/){:target="_blank" rel="noopener"}

> The Conti ransomware gang has released a free decryptor for Ireland's health service, the HSE, but warns that they will still sell or release the stolen data. [...]
