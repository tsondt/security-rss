Title: Deliver zero trust on unmanaged devices with new BeyondCorp Enterprise protected profiles
Date: 2021-05-12T16:00:00+00:00
Author: Jian Zhen
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: deliver-zero-trust-on-unmanaged-devices-with-new-beyondcorp-enterprise-protected-profiles

[Source](https://cloud.google.com/blog/products/identity-security/new-protected-profiles-enable-remote-work-with-zero-trust/){:target="_blank" rel="noopener"}

> Modern enterprises rely on vast and complex networks of technologies and skillsets to accomplish their goals. Markets are global, workers are remote, and information needs to be accessible anywhere, while remaining secure. This increasing complexity has led many enterprises to adopt a zero trust approach to security and deploy Google's BeyondCorp Enterprise, which provides customers with simple and secure access to applications and cloud resources with integrated threat and data protection. To help BeyondCorp Enterprise customers account for the global, mobile nature of work, we're excited to unveil a new feature, the protected profile. Protected profiles enable users to securely [...]
