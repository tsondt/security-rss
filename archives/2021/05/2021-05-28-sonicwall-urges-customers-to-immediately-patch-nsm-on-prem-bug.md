Title: SonicWall urges customers to 'immediately' patch NSM On-Prem bug
Date: 2021-05-28T09:46:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: sonicwall-urges-customers-to-immediately-patch-nsm-on-prem-bug

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-urges-customers-to-immediately-patch-nsm-on-prem-bug/){:target="_blank" rel="noopener"}

> SonicWall urges customers to 'immediately' patch a post-authentication vulnerability impacting on-premises versions of the Network Security Manager (NSM) multi-tenant firewall management solution. [...]
