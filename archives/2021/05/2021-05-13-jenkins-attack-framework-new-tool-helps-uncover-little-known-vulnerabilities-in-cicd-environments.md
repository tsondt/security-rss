Title: Jenkins Attack Framework: New tool helps uncover little-known vulnerabilities in CI/CD environments
Date: 2021-05-13T10:51:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: jenkins-attack-framework-new-tool-helps-uncover-little-known-vulnerabilities-in-cicd-environments

[Source](https://portswigger.net/daily-swig/jenkins-attack-framework-new-tool-helps-uncover-little-known-vulnerabilities-in-ci-cd-environments){:target="_blank" rel="noopener"}

> Open source utility automates and simplifies testing for known Jenkins exploits [...]
