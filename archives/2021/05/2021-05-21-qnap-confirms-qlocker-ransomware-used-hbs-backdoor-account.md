Title: QNAP confirms Qlocker ransomware used HBS backdoor account
Date: 2021-05-21T11:27:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qnap-confirms-qlocker-ransomware-used-hbs-backdoor-account

[Source](https://www.bleepingcomputer.com/news/security/qnap-confirms-qlocker-ransomware-used-hbs-backdoor-account/){:target="_blank" rel="noopener"}

> QNAP is advising customers to update the HBS 3 disaster recovery app to block Qlocker ransomware attacks targeting their Internet-exposed Network Attached Storage (NAS) devices. [...]
