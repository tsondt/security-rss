Title: Shifting Threats in a Changed World: Edge, IoT and Vaccine Fraud
Date: 2021-05-11T16:08:19+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;IoT;Malware;Web Security
Slug: shifting-threats-in-a-changed-world-edge-iot-and-vaccine-fraud

[Source](https://threatpost.com/threats-edge-iot-vaccine-fraud/166029/){:target="_blank" rel="noopener"}

> Aamir Lakhani, researcher at FortiGuard Labs, discusses leading-edge threats related to edge access/browsers/IoT, and the COVID-19 vaccine, as a way of getting into larger organizations. [...]
