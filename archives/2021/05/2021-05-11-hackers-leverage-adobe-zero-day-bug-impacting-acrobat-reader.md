Title: Hackers Leverage Adobe Zero-Day Bug Impacting Acrobat Reader
Date: 2021-05-11T18:38:36+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: hackers-leverage-adobe-zero-day-bug-impacting-acrobat-reader

[Source](https://threatpost.com/adobe-zero-day-bug-acrobat-reader/166044/){:target="_blank" rel="noopener"}

> A patch for Adobe Acrobat, the world’s leading PDF reader, fixes a vulnerability under active attack affecting both Windows and macOS systems that could lead to arbitrary code execution. [...]
