Title: Trust Wallet, MetaMask crypto wallets targeted by new support scam
Date: 2021-05-12T12:36:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency
Slug: trust-wallet-metamask-crypto-wallets-targeted-by-new-support-scam

[Source](https://www.bleepingcomputer.com/news/security/trust-wallet-metamask-crypto-wallets-targeted-by-new-support-scam/){:target="_blank" rel="noopener"}

> Trust Wallet and MetaMask wallet users are being targeted in ongoing and aggressive Twitter phishing attacks to steal cryptocurrency funds. [...]
