Title: Colonial Pipeline’s Ransomware Attack Sparks Emergency Declaration
Date: 2021-05-10T17:42:22+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Critical Infrastructure;Malware
Slug: colonial-pipelines-ransomware-attack-sparks-emergency-declaration

[Source](https://threatpost.com/colonial-pipeline-ransomware-emergency-declaration/165977/){:target="_blank" rel="noopener"}

> Security researchers mull possible perpetrators of the attack, and warned that the incident could be a harbinger of things to come. [...]
