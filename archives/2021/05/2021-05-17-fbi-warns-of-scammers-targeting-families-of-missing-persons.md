Title: FBI warns of scammers targeting families of missing persons
Date: 2021-05-17T12:27:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-scammers-targeting-families-of-missing-persons

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-scammers-targeting-families-of-missing-persons/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned that scammers actively target the vulnerable families of missing persons attempting to extort them using information shared on social media. [...]
