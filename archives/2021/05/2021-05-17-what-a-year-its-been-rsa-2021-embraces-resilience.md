Title: What a Year It’s Been: RSA 2021 Embraces ‘Resilience’
Date: 2021-05-17T19:40:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;IoT;Malware;Mobile Security;RSAC;Web Security
Slug: what-a-year-its-been-rsa-2021-embraces-resilience

[Source](https://threatpost.com/rsa-2021-embraces-resilience/166233/){:target="_blank" rel="noopener"}

> Keynoters from Cisco, Netflix and RSA highlighted lessons from the last year, and cybersecurity's new mandate in the post-pandemic world: Bounce back stronger. [...]
