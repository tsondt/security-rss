Title: iPhone Hack Allegedly Used to Spy on China’s Uyghurs
Date: 2021-05-07T20:28:41+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Mobile Security;Vulnerabilities
Slug: iphone-hack-allegedly-used-to-spy-on-chinas-uyghurs

[Source](https://threatpost.com/iphone-hack-spying-china-uyghurs/165950/){:target="_blank" rel="noopener"}

> U.S. intelligence said that the Chaos iPhone remote takeover exploit was used against the minority ethnic group before Apple could patch the problem. [...]
