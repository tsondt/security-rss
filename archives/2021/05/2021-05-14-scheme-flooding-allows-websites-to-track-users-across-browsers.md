Title: ‘Scheme Flooding’ Allows Websites to Track Users Across Browsers
Date: 2021-05-14T14:03:01+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy;Vulnerabilities;Web Security
Slug: scheme-flooding-allows-websites-to-track-users-across-browsers

[Source](https://threatpost.com/scheme-flooding-website-tracking/166185/){:target="_blank" rel="noopener"}

> A flaw that allows browsers to enumerate applications on a machine threatens cross-browser anonymity in Chrome, Firefox, Microsoft Edge, Safari and even Tor. [...]
