Title: Opera security team discloses multiple flaws in open source web proxy, Privoxy
Date: 2021-05-19T11:09:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: opera-security-team-discloses-multiple-flaws-in-open-source-web-proxy-privoxy

[Source](https://portswigger.net/daily-swig/opera-security-team-discloses-multiple-flaws-in-open-source-web-proxy-privoxy){:target="_blank" rel="noopener"}

> First chapter in security audit series released [...]
