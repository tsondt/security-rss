Title: 2021 Attacker Dwell Time Trends and Best Defenses
Date: 2021-05-20T15:07:15+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Sponsored;Web Security
Slug: 2021-attacker-dwell-time-trends-and-best-defenses

[Source](https://threatpost.com/2021-attacker-dwell-time-trends-and-best-defenses/166116/){:target="_blank" rel="noopener"}

> The time that attackers stay hidden inside an organization’s networks is shifting, putting pressure on defenders and upping the need to detect and respond to threats in real-time. [...]
