Title: Foxit Reader bug lets attackers run malicious code via PDFs
Date: 2021-05-07T13:46:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: foxit-reader-bug-lets-attackers-run-malicious-code-via-pdfs

[Source](https://www.bleepingcomputer.com/news/security/foxit-reader-bug-lets-attackers-run-malicious-code-via-pdfs/){:target="_blank" rel="noopener"}

> Foxit Software, the company behind the highly popular Foxit Reader, has published security updates to fix a high severity remote code execution (RCE) vulnerability affecting the PDF reader. [...]
