Title: Ransomware’s New Swindle: Triple Extortion
Date: 2021-05-14T12:30:43+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware
Slug: ransomwares-new-swindle-triple-extortion

[Source](https://threatpost.com/ransomwares-swindle-triple-extortion/166149/){:target="_blank" rel="noopener"}

> Ransomware attackers are now demanding cash from the customers of victims too. [...]
