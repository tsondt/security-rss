Title: UK government to review country's aging Computer Misuse Act – official
Date: 2021-05-11T15:40:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uk-government-to-review-countrys-aging-computer-misuse-act-official

[Source](https://portswigger.net/daily-swig/uk-government-to-review-countrys-aging-computer-misuse-act-official){:target="_blank" rel="noopener"}

> Security legislation, ransomware, and supply chain attacks top the agenda at this year’s CyberUK [...]
