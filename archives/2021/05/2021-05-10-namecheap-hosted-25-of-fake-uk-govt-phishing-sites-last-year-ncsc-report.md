Title: Namecheap hosted 25%+ of fake UK govt phishing sites last year – NCSC report
Date: 2021-05-10T08:30:14+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: namecheap-hosted-25-of-fake-uk-govt-phishing-sites-last-year-ncsc-report

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/10/ncsc_active_cyber_defence_report/){:target="_blank" rel="noopener"}

> Also we fixed SS7 use by British telcos. How? Why? Not saying Domains'n'hosting outfit Namecheap harboured more than a quarter of all known phishing sites that falsely posed as UK government web presences during 2020, according to the National Cyber Security Centre today.... [...]
