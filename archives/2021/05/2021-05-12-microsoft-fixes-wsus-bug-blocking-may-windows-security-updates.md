Title: Microsoft fixes WSUS bug blocking May Windows security updates
Date: 2021-05-12T13:52:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-fixes-wsus-bug-blocking-may-windows-security-updates

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-wsus-bug-blocking-may-windows-security-updates/){:target="_blank" rel="noopener"}

> Microsoft has resolved a known issue preventing managed devices from receiving the May 2021 Patch Tuesday security updates. [...]
