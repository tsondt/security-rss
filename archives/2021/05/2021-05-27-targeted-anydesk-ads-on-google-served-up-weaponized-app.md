Title: Targeted AnyDesk Ads on Google Served Up Weaponized App
Date: 2021-05-27T21:43:33+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: targeted-anydesk-ads-on-google-served-up-weaponized-app

[Source](https://threatpost.com/ad-malicious-version-anydesk/166525/){:target="_blank" rel="noopener"}

> Malicious ad campaign was able to rank higher in searches than legitimate AnyDesk ads. [...]
