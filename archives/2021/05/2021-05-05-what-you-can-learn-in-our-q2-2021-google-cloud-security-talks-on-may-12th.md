Title: What you can learn in our Q2 2021 Google Cloud Security Talks on May 12th
Date: 2021-05-05T16:00:00+00:00
Author: Rob Sadowski
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: what-you-can-learn-in-our-q2-2021-google-cloud-security-talks-on-may-12th

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-security-talks-q2-2021/){:target="_blank" rel="noopener"}

> Join us for our second Google Cloud Security Talks of 2021, a live online event on May 12th where we’ll help you navigate the latest developments in cloud security. We’ll share expert insights into how we’re working to be your most trusted cloud by covering the following topics: Sunil Potti and Rob Sadowski will kick off Security Talks on May 12th. Following this will be a panel discussion on how organizations are using Confidential Computing with Harold Giménez (HashiCorp), Morgan Akers (JP Morgan Chase), Nelly Porter & Sam Lugani. Andy Wen and Neil Kumaran will discuss our use of machine [...]
