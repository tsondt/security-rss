Title: Fintech Startup Offers $500 for Payroll Passwords
Date: 2021-05-10T14:25:37+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Argyle;argyle.com;Bain Capital;Billy Mardsen;CommonGrounds;Earnin;Equifax;J.P. Morgan Chase;Kevin Beaumont;Shmulik Fishman;Steve Friedl;T-Mobile;The Work Number;UniteAtWork;WageCompete;Workers Research Alliances LLC;Workers United
Slug: fintech-startup-offers-500-for-payroll-passwords

[Source](https://krebsonsecurity.com/2021/05/fintech-startup-offers-500-for-payroll-passwords/){:target="_blank" rel="noopener"}

> How much is your payroll data worth? Probably a lot more than you think. One financial startup that’s targeting the gig worker market is offering up to $500 to anyone willing to hand over the payroll account username and password given to them by their employer, plus a regular payment for each month afterwards in which those credentials still work. This ad, from workplaceunited[.]com, promised up to $500 for people who provided their payroll passwords, plus $25 a month for each month those credentials kept working. New York-based Argyle.com says it’s building a platform where people who work multiple jobs [...]
