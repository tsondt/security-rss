Title: Teaching Cybersecurity to Children
Date: 2021-05-07T13:36:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: children;cybersecurity;generations;privacy;security education
Slug: teaching-cybersecurity-to-children

[Source](https://www.schneier.com/blog/archives/2021/05/teaching-cybersecurity-to-children.html){:target="_blank" rel="noopener"}

> A new draft of an Australian educational curriculum proposes teaching children as young as five cybersecurity: The proposed curriculum aims to teach five-year-old children — an age at which Australian kids first attend school — not to share information such as date of birth or full names with strangers, and that they should consult parents or guardians before entering personal information online. Six-and-seven-year-olds will be taught how to use usernames and passwords, and the pitfalls of clicking on pop-up links to competitions. By the time kids are in third and fourth grade, they’ll be taught how to identify the personal [...]
