Title: Amazon devices will soon automatically share your Internet with neighbors
Date: 2021-05-29T19:10:00+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;amazon sidewalk;privacy;wireless
Slug: amazon-devices-will-soon-automatically-share-your-internet-with-neighbors

[Source](https://arstechnica.com/?p=1768704){:target="_blank" rel="noopener"}

> Enlarge (credit: Amazon ) If you use Alexa, Echo, or any other Amazon device, you have only 10 days to opt out of an experiment that leaves your personal privacy and security hanging in the balance. On June 8, the merchant, Web host, and entertainment behemoth will automatically enroll the devices in Amazon Sidewalk. The new wireless mesh service will share a small slice of your Internet bandwidth with nearby neighbors who don’t have connectivity and help you to their bandwidth when you don’t have a connection. By default, Amazon devices including Alexa, Echo, Ring, security cams, outdoor lights, motion [...]
