Title: Watch out: These unsubscribe emails only lead to further spam
Date: 2021-05-30T14:55:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: watch-out-these-unsubscribe-emails-only-lead-to-further-spam

[Source](https://www.bleepingcomputer.com/news/security/watch-out-these-unsubscribe-emails-only-lead-to-further-spam/){:target="_blank" rel="noopener"}

> Scammers use fake 'unsubscribe' spam emails to confirm valid email accounts to be used in future phishing and spam campaigns. [...]
