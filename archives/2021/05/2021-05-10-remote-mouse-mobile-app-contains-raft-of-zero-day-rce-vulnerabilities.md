Title: Remote Mouse mobile app contains raft of zero-day RCE vulnerabilities
Date: 2021-05-10T12:55:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: remote-mouse-mobile-app-contains-raft-of-zero-day-rce-vulnerabilities

[Source](https://portswigger.net/daily-swig/remote-mouse-mobile-app-contains-raft-of-zero-day-rce-vulnerabilities){:target="_blank" rel="noopener"}

> Remote control app for desktop PCs has surpassed 20m users, says vendor [...]
