Title: The Wages of Password Re-use: Your Money or Your Life
Date: 2021-05-04T17:22:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News
Slug: the-wages-of-password-re-use-your-money-or-your-life

[Source](https://krebsonsecurity.com/2021/05/the-wages-of-password-re-use-your-money-or-your-life/){:target="_blank" rel="noopener"}

> When normal computer users fall into the nasty habit of recycling passwords, the result is most often some type of financial loss. When cybercriminals develop the same habit, it can eventually cost them their freedom. Our passwords can say a lot about us, and much of what they have to say is unflattering. In a world in which all databases — including hacker forums — are eventually compromised and leaked online, it can be tough for cybercriminals to maintain their anonymity if they’re in the habit of re-using the same unusual passwords across multiple accounts associated with different email addresses. [...]
