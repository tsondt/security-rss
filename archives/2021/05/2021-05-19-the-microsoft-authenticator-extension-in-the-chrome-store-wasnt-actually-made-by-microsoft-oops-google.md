Title: The Microsoft Authenticator extension in the Chrome store wasn't actually made by Microsoft. Oops, Google
Date: 2021-05-19T07:02:04+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: the-microsoft-authenticator-extension-in-the-chrome-store-wasnt-actually-made-by-microsoft-oops-google

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/19/chrome_extension_microsoft_authenticator_fake/){:target="_blank" rel="noopener"}

> Guess they'll let anyone in here The trustworthiness of Google's Chrome Store was again called into question after an extension billing itself as Microsoft Authenticator was published by the software souk without the simplest of checks.... [...]
