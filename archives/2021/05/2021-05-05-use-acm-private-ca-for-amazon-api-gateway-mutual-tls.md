Title: Use ACM Private CA for Amazon API Gateway Mutual TLS
Date: 2021-05-05T22:28:57+00:00
Author: Tracy Pierce
Category: AWS Security
Tags: Amazon API Gateway;AWS Certificate Manager;Intermediate (200);Security, Identity, & Compliance;ACM;ACM Private CA;API Gateway;Security Blog;SSL;TLS
Slug: use-acm-private-ca-for-amazon-api-gateway-mutual-tls

[Source](https://aws.amazon.com/blogs/security/use-acm-private-ca-for-amazon-api-gateway-mutual-tls/){:target="_blank" rel="noopener"}

> Last year Amazon API Gateway announced certificate-based mutual Transport Layer Security (TLS) authentication. Mutual TLS (mTLS) authenticates the server to the client, and requests the client to send an X.509 certificate to prove its identity as well. This way, both parties are authenticated to each other. In a previous post, you can learn how to [...]
