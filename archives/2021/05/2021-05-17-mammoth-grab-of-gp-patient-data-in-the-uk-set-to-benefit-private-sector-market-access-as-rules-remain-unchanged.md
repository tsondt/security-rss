Title: Mammoth grab of GP patient data in the UK set to benefit private-sector market access as rules remain unchanged
Date: 2021-05-17T09:18:10+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: mammoth-grab-of-gp-patient-data-in-the-uk-set-to-benefit-private-sector-market-access-as-rules-remain-unchanged

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/17/nhs_data_market_access/){:target="_blank" rel="noopener"}

> No policy shakeup to deal with snatch of info from primary physicians Evidence from NHS Digital's website suggests that patient data held by GPs in England will be available to private-sector companies to help them understand market opportunities in the UK's health service.... [...]
