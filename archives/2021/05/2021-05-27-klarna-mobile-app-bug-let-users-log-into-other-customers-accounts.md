Title: Klarna mobile app bug let users log into other customers' accounts
Date: 2021-05-27T11:22:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: klarna-mobile-app-bug-let-users-log-into-other-customers-accounts

[Source](https://www.bleepingcomputer.com/news/security/klarna-mobile-app-bug-let-users-log-into-other-customers-accounts/){:target="_blank" rel="noopener"}

> Klarna Bank suffered a severe technical issue this morning that allowed mobile app users to log into other customers' accounts and see their stored information. [...]
