Title: Swedish Health Agency shuts down SmiNet after hacking attempts
Date: 2021-05-31T13:36:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: swedish-health-agency-shuts-down-sminet-after-hacking-attempts

[Source](https://www.bleepingcomputer.com/news/security/swedish-health-agency-shuts-down-sminet-after-hacking-attempts/){:target="_blank" rel="noopener"}

> The Swedish Public Health Agency (Folkhälsomyndigheten) has shut down SmiNet, the country's infectious diseases database, on Thursday after it was targeted in several hacking attempts. [...]
