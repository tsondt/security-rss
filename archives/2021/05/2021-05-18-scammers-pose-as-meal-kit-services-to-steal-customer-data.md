Title: Scammers Pose as Meal-Kit Services to Steal Customer Data
Date: 2021-05-18T20:48:13+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: scammers-pose-as-meal-kit-services-to-steal-customer-data

[Source](https://threatpost.com/scammers-meal-kit-services-customer-data/166282/){:target="_blank" rel="noopener"}

> Attackers are sending messages disguised as offers from meal-kit services, like HelloFresh. [...]
