Title: Amex fined £90,000 for sending 4 million spam emails in a year
Date: 2021-05-23T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: amex-fined-ps90000-for-sending-4-million-spam-emails-in-a-year

[Source](https://www.bleepingcomputer.com/news/security/amex-fined-90-000-for-sending-4-million-spam-emails-in-a-year/){:target="_blank" rel="noopener"}

> The UK data regulator has fined American Express (Amex) £90,000 for sending over 4 million spam emails to customers within one year. [...]
