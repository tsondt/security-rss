Title: How reCAPTCHA Enterprise protects unemployment and COVID-19 vaccination portals
Date: 2021-05-11T16:00:00+00:00
Author: John Chirhart
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: how-recaptcha-enterprise-protects-unemployment-and-covid-19-vaccination-portals

[Source](https://cloud.google.com/blog/products/identity-security/recaptcha-protects-public-services/){:target="_blank" rel="noopener"}

> More people than ever have been conducting more of their lives online due to the COVID-19 pandemic. This creates a new landscape for fraudsters to create and release new attacks. Research commissioned by Forrester Consulting showed 84% of companies have seen an increase in bot attacks. 71% of organizations have seen an increase in the amount of successful attacks. 65% of businesses have experienced more frequent attacks and greater revenue loss due to bot attacks. With so many people visiting government websites to learn more about the COVID-19 vaccine, make vaccine appointments, or file for unemployment, these web pages have [...]
