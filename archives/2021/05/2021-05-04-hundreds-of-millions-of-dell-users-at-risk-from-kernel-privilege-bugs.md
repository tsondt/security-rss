Title: Hundreds of Millions of Dell Users at Risk from Kernel-Privilege Bugs
Date: 2021-05-04T16:07:10+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: hundreds-of-millions-of-dell-users-at-risk-from-kernel-privilege-bugs

[Source](https://threatpost.com/dell-kernel-privilege-bugs/165843/){:target="_blank" rel="noopener"}

> The privilege-escalation bug remained hidden for 12 years and has been present in all Dell PCs, tablets and notebooks shipped since 2009. [...]
