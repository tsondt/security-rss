Title: How Apple’s AirTag turns us into unwitting spies in a vast surveillance network
Date: 2021-05-17T01:33:16+00:00
Author: Paul Haskell-Dowland for the Conversation
Category: The Guardian
Tags: Apple;Data and computer security;Data protection;Surveillance;Bluetooth;Technology
Slug: how-apples-airtag-turns-us-into-unwitting-spies-in-a-vast-surveillance-network

[Source](https://www.theguardian.com/technology/2021/may/17/how-apples-airtag-turns-us-into-unwitting-spies-in-a-vast-surveillance-network){:target="_blank" rel="noopener"}

> The tech giant says it has security safeguards in place. But these tracking devices can be hacked and put to other nefarious purposes Apple has launched the latest version of its operating system, iOS 14.5, which features the much-anticipated app tracking transparency function, bolstering the tech giant’s privacy credentials. But iOS 14.5 also introduced support for the new Apple AirTag, which risks doing the opposite. Related: Apple launches new iMac, iPad Pro, AirTags and Podcast subscriptions Being around someone with an AirTag is *very* annoying pic.twitter.com/GZj8ZeTCck A security researcher has found out the microcontroller inside Apple's #AirTag can be reprogrammed, [...]
