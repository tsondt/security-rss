Title: Computer Misuse Act: Tell the Home Office infosec needs a public interest defence in law, says CyberUp campaign
Date: 2021-05-26T09:17:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: computer-misuse-act-tell-the-home-office-infosec-needs-a-public-interest-defence-in-law-says-cyberup-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/26/cyberup_techuk_public_interest_call/){:target="_blank" rel="noopener"}

> Bug-hunting industry wants to know a bit more before doing that, though Businesses operating in the word of infosec have been urged to write to the Home Office and support a public interest defence being added to the Computer Misuse Act.... [...]
