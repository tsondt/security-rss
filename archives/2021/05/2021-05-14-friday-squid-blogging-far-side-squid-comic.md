Title: Friday Squid Blogging: Far Side Squid Comic
Date: 2021-05-14T21:06:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-far-side-squid-comic

[Source](https://www.schneier.com/blog/archives/2021/05/friday-squid-blogging-far-side-squid-comic.html){:target="_blank" rel="noopener"}

> A classic. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
