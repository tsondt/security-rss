Title: Cloudflare launches campaign to ‘end the madness’ of CAPTCHAs
Date: 2021-05-14T03:29:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: cloudflare-launches-campaign-to-end-the-madness-of-captchas

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/14/cloudflare_cryptographic_attestation_of_personhood_captcha_killer/){:target="_blank" rel="noopener"}

> Testing dongle-driven ‘Cryptographic Attestation of Personhood’ and WebAuthn as alternative Poll Cloudflare has called on the world to “end this madness” by consigning Completely Automated Public Turing test to tell Computers and Humans Apart (CAPTCHAS) to the dustbin of history.... [...]
