Title: Google Cloud’s contribution to an environment of trust and transparency in Europe
Date: 2021-05-20T13:00:00+00:00
Author: Marc Crandall
Category: GCP Security
Tags: Identity & Security;Google Cloud Platform;Google Cloud in Europe;Compliance
Slug: google-clouds-contribution-to-an-environment-of-trust-and-transparency-in-europe

[Source](https://cloud.google.com/blog/products/compliance/google-cloud-adopts-eu-gdpr-cloud-code-of-conduct/){:target="_blank" rel="noopener"}

> Google Cloud's industry-leading controls, contractual commitments, and accountability tools have helped organizations across Europe meet stringent data protection regulatory requirements for years. This commitment to supporting the compliance efforts of European companies has earned us the trust of businesses like retailers, manufacturers and financial services providers. As part of our continued efforts to uphold that trust, Google Cloud was one of the first cloud providers to support and adopt the EU GDPR Cloud Code of Conduct (CoC). The CoC is a mechanism for cloud providers to demonstrate how they offer sufficient guarantees to implement appropriate technical and organizational measures as [...]
