Title: Open source ecosystem ripe for dependency confusion attacks, research finds
Date: 2021-05-24T15:38:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: open-source-ecosystem-ripe-for-dependency-confusion-attacks-research-finds

[Source](https://portswigger.net/daily-swig/open-source-ecosystem-ripe-for-dependency-confusion-attacks-research-finds){:target="_blank" rel="noopener"}

> Attacks could put ‘millions of users’ at risk [...]
