Title: ‘FragAttacks’: Wi-Fi Bugs Affect Millions of Devices
Date: 2021-05-12T15:48:05+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;IoT;Mobile Security;Vulnerabilities;Web Security
Slug: fragattacks-wi-fi-bugs-affect-millions-of-devices

[Source](https://threatpost.com/fragattacks-wifi-bugs-millions-devices/166080/){:target="_blank" rel="noopener"}

> Wi-Fi devices going back to 1997 are vulnerable to attackers who can steal your data if they're in range. [...]
