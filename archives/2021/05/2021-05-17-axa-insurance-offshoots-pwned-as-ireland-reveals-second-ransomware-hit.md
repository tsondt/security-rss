Title: Axa insurance offshoots pwned as Ireland reveals second ransomware hit
Date: 2021-05-17T15:37:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: axa-insurance-offshoots-pwned-as-ireland-reveals-second-ransomware-hit

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/17/ransomware_roundup/){:target="_blank" rel="noopener"}

> Dept of Health unsuccessfully targeted in same attack against hospitals In brief The murky world of ransomware criminals is all aflutter after it was revealed that Ireland's health services were hit by a second attack hot on the heels of one that took out its hospitals, while ransomware insurance refusenik Axa was itself hit with ransomware after its French branch vowed to stop buying off criminals on behalf of its customers.... [...]
