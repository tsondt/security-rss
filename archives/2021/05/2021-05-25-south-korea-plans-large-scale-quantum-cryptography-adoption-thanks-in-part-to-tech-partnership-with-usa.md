Title: South Korea plans large scale quantum cryptography adoption, thanks in part to tech partnership with USA
Date: 2021-05-25T08:33:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: south-korea-plans-large-scale-quantum-cryptography-adoption-thanks-in-part-to-tech-partnership-with-usa

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/25/south_korea_quantum_encryption/){:target="_blank" rel="noopener"}

> Also steps into future by allowing plug to be pulled on 2G networks The Republic of Korea took two bold steps into the future on Tuesday, by announcing that the last of its 2G networks will go offline in June and that it will initiate large-scale adoption of communications protected by quantum encryption.... [...]
