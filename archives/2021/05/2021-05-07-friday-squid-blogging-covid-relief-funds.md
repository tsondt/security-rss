Title: Friday Squid Blogging: COVID Relief Funds
Date: 2021-05-07T21:13:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-covid-relief-funds

[Source](https://www.schneier.com/blog/archives/2021/05/friday-squid-blogging-covid-relief-funds.html){:target="_blank" rel="noopener"}

> A town in Japan built a giant squid statue with its COVID relief grant. One local told the Chunichi Shimbun newspaper that while the statue may be effective in the long run, the money could have been used for “urgent support,” such as for medical staff and long-term care facilities. But a spokesperson for the town told Fuji News Network that the statue would be a tourist attraction and part of a long term strategy to help promote Noto’s famous flying squid. I am impressed by the town’s sense of priorities. As usual, you can also use this squid post [...]
