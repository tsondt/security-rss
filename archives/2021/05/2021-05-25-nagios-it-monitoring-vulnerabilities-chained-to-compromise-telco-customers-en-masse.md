Title: Nagios IT monitoring vulnerabilities chained to compromise telco customers en masse
Date: 2021-05-25T09:57:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nagios-it-monitoring-vulnerabilities-chained-to-compromise-telco-customers-en-masse

[Source](https://portswigger.net/daily-swig/nagios-it-monitoring-vulnerabilities-chained-to-compromise-telco-customers-en-masse){:target="_blank" rel="noopener"}

> Medium-impact flaws combined to create ‘upstream attack platform’ [...]
