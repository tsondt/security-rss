Title: Air India admits to data breach impacting 4.5m customers, sat on the news for five weeks
Date: 2021-05-24T05:58:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: air-india-admits-to-data-breach-impacting-45m-customers-sat-on-the-news-for-five-weeks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/24/air_india_sita_data_breach/){:target="_blank" rel="noopener"}

> While my SITA gently leaks, customers were unaware their credit card numbers had flown away India’s flag carrier, Air India, has admitted it fell foul of the data breach at aviation information services provider SITA, and that its disclosure comes five weeks after it was notified of the situation.... [...]
