Title: Uptime funk: Microsoft has lifted availability of Azure Key Vault to 99.99%
Date: 2021-05-19T10:01:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: uptime-funk-microsoft-has-lifted-availability-of-azure-key-vault-to-9999

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/19/azure_key_vault/){:target="_blank" rel="noopener"}

> But beware the SLA: Just how much would an outage actually cost you? Microsoft has added another 9 to its availability guarantee for Azure Key Vault, taking the service to 99.99 per cent availability.... [...]
