Title: Analysis: Colonial Pipeline’s $5m ransomware payment risks perpetuating cybercrime ‘feedback loop’
Date: 2021-05-14T15:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: analysis-colonial-pipelines-5m-ransomware-payment-risks-perpetuating-cybercrime-feedback-loop

[Source](https://portswigger.net/daily-swig/analysis-colonial-pipelines-5m-ransomware-payment-risks-perpetuating-cybercrime-feedback-loop){:target="_blank" rel="noopener"}

> Will the colossal payout further embolden financially-motivated cybercrooks? [...]
