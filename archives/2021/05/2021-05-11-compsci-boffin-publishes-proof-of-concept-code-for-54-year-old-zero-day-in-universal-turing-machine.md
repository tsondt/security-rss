Title: Compsci boffin publishes proof-of-concept code for 54-year-old zero-day in Universal Turing Machine
Date: 2021-05-11T09:15:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: compsci-boffin-publishes-proof-of-concept-code-for-54-year-old-zero-day-in-universal-turing-machine

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/11/turing_machine_0day_no_patch_available/){:target="_blank" rel="noopener"}

> Patch your devi... oh, hang on a sec A computer science professor from Sweden has discovered an arbitrary code execution vuln in the Universal Turing Machine, one of the earliest computer designs in history – though he admits it has "no real-world implications".... [...]
