Title: JET engine flaws can crash Microsoft's IIS, SQL Server, say Palo Alto researchers
Date: 2021-05-06T04:59:48+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: jet-engine-flaws-can-crash-microsofts-iis-sql-server-say-palo-alto-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/06/jet_engine_flaws_attack_iis_sql/){:target="_blank" rel="noopener"}

> Trio claim database queries can lead to remote code execution Black Hat Asia A trio of researchers at Palo Alto Networks has detailed vulnerabilities in the JET database engine, and demonstrated how those flaws can be exploited to ultimately execute malicious code on systems running Microsoft’s SQL Server and Internet Information Services web server.... [...]
