Title: The Week in Ransomware - May 7th 2021 - Attacking healthcare
Date: 2021-05-07T18:13:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-may-7th-2021-attacking-healthcare

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-7th-2021-attacking-healthcare/){:target="_blank" rel="noopener"}

> While ransomware attacks continued throughout the week, for the most part, it has been quieter than usual, with only a few new variants released. [...]
