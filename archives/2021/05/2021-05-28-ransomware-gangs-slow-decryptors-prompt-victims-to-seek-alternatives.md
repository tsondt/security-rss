Title: Ransomware gangs' slow decryptors prompt victims to seek alternatives
Date: 2021-05-28T08:35:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gangs-slow-decryptors-prompt-victims-to-seek-alternatives

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-slow-decryptors-prompt-victims-to-seek-alternatives/){:target="_blank" rel="noopener"}

> Recently, two highly publicized ransomware victims received a decryptor that was too slow to make it effective in quickly restoring the victim's network. [...]
