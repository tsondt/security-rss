Title: Microsoft reveals final plan to remove Flash Player in Windows 10
Date: 2021-05-03T10:10:35-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Software
Slug: microsoft-reveals-final-plan-to-remove-flash-player-in-windows-10

[Source](https://www.bleepingcomputer.com/news/security/microsoft-reveals-final-plan-to-remove-flash-player-in-windows-10/){:target="_blank" rel="noopener"}

> Microsoft quietly revealed its plans to remove the Adobe Flash plugin from Windows 10, with mandatory removal starting in July 2021. [...]
