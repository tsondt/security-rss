Title: Massive DDoS Attack Disrupts Belgium Parliament
Date: 2021-05-06T15:48:13+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks
Slug: massive-ddos-attack-disrupts-belgium-parliament

[Source](https://threatpost.com/ddos-disrupts-belgium/165911/){:target="_blank" rel="noopener"}

> A large-scale incident earlier this week against Belnet and other ISPs has sent a wave of internet disruption across numerous Belgian government, scientific and educational institutions. [...]
