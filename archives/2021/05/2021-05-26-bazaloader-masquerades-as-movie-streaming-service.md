Title: BazaLoader Masquerades as Movie-Streaming Service
Date: 2021-05-26T17:44:46+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: bazaloader-masquerades-as-movie-streaming-service

[Source](https://threatpost.com/bazaloader-fake-movie-streaming-service/166489/){:target="_blank" rel="noopener"}

> The website for “BravoMovies” features fake movie posters and a FAQ with a rigged Excel spreadsheet for “cancelling” the service, but all it downloads is malware. [...]
