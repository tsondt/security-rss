Title: Miscreants started scanning for Exchange Hafnium vulns five minutes after Microsoft told world about zero-days
Date: 2021-05-19T13:02:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: miscreants-started-scanning-for-exchange-hafnium-vulns-five-minutes-after-microsoft-told-world-about-zero-days

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/19/hafnium_scans_5_mins_post_disclosure/){:target="_blank" rel="noopener"}

> Being slow to patch just means you'll get pwned faster Attackers began scanning for vulnerabilities just five minutes after Microsoft announced there were four zero-days in Exchange Server, according to Palo Alto Networks.... [...]
