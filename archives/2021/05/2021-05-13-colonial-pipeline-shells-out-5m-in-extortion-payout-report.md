Title: Colonial Pipeline Shells Out $5M in Extortion Payout, Report
Date: 2021-05-13T20:22:15+00:00
Author: Tom Spring
Category: Threatpost
Tags: Critical Infrastructure;Malware;Vulnerabilities;Web Security
Slug: colonial-pipeline-shells-out-5m-in-extortion-payout-report

[Source](https://threatpost.com/colonial-pays-5m/166147/){:target="_blank" rel="noopener"}

> According to news reports, Colonial Pipeline paid the cybergang known as DarkSide the ransom it demanded in return for a decryption key. [...]
