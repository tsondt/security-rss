Title: QNAP warns of eCh0raix ransomware attacks, Roon Server zero-day
Date: 2021-05-14T08:49:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qnap-warns-of-ech0raix-ransomware-attacks-roon-server-zero-day

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-of-ech0raix-ransomware-attacks-roon-server-zero-day/){:target="_blank" rel="noopener"}

> QNAP warns customers of an actively exploited Roon Server zero-day bug and eCh0raix ransomware attacks targeting their Network Attached Storage (NAS) devices, just two weeks after alerting them of an ongoing AgeLocker ransomware outbreak. [...]
