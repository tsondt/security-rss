Title: Interpol intercepts $83 million fighting financial cyber crime
Date: 2021-05-30T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: interpol-intercepts-83-million-fighting-financial-cyber-crime

[Source](https://www.bleepingcomputer.com/news/security/interpol-intercepts-83-million-fighting-financial-cyber-crime/){:target="_blank" rel="noopener"}

> The INTERPOL (short for International Criminal Police Organisation) has intercepted $83 million belonging to victims of online financial crime from being transferred to the accounts of their attackers. [...]
