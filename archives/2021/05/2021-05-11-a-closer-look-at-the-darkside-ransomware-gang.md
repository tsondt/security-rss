Title: A Closer Look at the DarkSide Ransomware Gang
Date: 2021-05-11T16:37:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ransomware;Colonial Pipeline ransomware attack;Coveware;DarkSide ransomware;Flashpoint;Intel 471
Slug: a-closer-look-at-the-darkside-ransomware-gang

[Source](https://krebsonsecurity.com/2021/05/a-closer-look-at-the-darkside-ransomware-gang/){:target="_blank" rel="noopener"}

> The FBI confirmed this week that a relatively new ransomware group known as DarkSide is responsible for an attack that caused Colonial Pipeline to shut down 5,550 miles of pipe, stranding countless barrels of gasoline, diesel and jet fuel on the Gulf Coast. Here’s a closer look at the DarkSide cybercrime gang, as seen through their negotiations with a recent U.S. victim that earns $15 billion in annual revenue. Colonial Pipeline has shut down 5,500 miles of fuel pipe in response to a ransomware incident. Image: colpipe.com New York City-based cyber intelligence firm Flashpoint said its analysts assess with a [...]
