Title: Fuzzing iOS code on macOS at native speed
Date: 2021-05-20T10:07:00-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: fuzzing-ios-code-on-macos-at-native-speed

[Source](https://googleprojectzero.blogspot.com/2021/05/fuzzing-ios-code-on-macos-at-native.html){:target="_blank" rel="noopener"}

> Or how iOS apps on macOS work under the hood Posted by Samuel Groß, Project Zero This short post explains how code compiled for iOS can be run natively on Apple Silicon Macs. With the introduction of Apple Silicon Macs, Apple also made it possible to run iOS apps natively on these Macs. This is fundamentally possible due to (1) iPhones and Apple Silicon Macs both using the arm64 instruction set architecture (ISA) and (2) macOS using a mostly compatible set of runtime libraries and frameworks while also providing /System/iOSSupport which contains the parts of the iOS runtime that do [...]
