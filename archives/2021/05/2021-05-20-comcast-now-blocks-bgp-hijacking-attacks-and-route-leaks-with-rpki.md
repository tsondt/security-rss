Title: Comcast now blocks BGP hijacking attacks and route leaks with RPKI
Date: 2021-05-20T15:16:03-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: comcast-now-blocks-bgp-hijacking-attacks-and-route-leaks-with-rpki

[Source](https://www.bleepingcomputer.com/news/security/comcast-now-blocks-bgp-hijacking-attacks-and-route-leaks-with-rpki/){:target="_blank" rel="noopener"}

> Comcast, one of America's largest broadband providers, has now deployed RPKI on its network to defend against BGP route hijacks and leaks. Left unchecked, a BGP route hijack or leak can cause a drastic surge in internet traffic that now gets misdirected or stuck, leading to global congestion and a Denial of Service (DoS). [...]
