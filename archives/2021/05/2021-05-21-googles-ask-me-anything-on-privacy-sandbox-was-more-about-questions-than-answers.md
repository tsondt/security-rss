Title: Google's 'Ask me anything' on Privacy Sandbox was more about questions than answers
Date: 2021-05-21T10:59:08+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: googles-ask-me-anything-on-privacy-sandbox-was-more-about-questions-than-answers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/21/google_privacy_sandbox_ask_me_anything/){:target="_blank" rel="noopener"}

> FLoC is not for our benefit, says Chocolate Factory, it's for everyone else Google conducted an "Ask me anything" panel on its controversial Privacy Sandbox proposals at its online I/O event.... [...]
