Title: Verizon: Pandemic Ushers in ⅓ More Cyber-Misery
Date: 2021-05-14T13:26:48+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks;Malware;Most Recent ThreatLists;Vulnerabilities;Web Security
Slug: verizon-pandemic-ushers-in-13-more-cyber-misery

[Source](https://threatpost.com/verizon-pandemic-cyber-misery/166168/){:target="_blank" rel="noopener"}

> The DBIR – Verizon’s 2021 data breach report – shows spikes in sophisticated phishing, financially motivated cyberattacks and a criminal focus on web-application servers. [...]
