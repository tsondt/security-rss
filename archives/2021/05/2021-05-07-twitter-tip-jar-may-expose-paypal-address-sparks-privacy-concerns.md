Title: Twitter Tip Jar may expose PayPal address, sparks privacy concerns
Date: 2021-05-07T09:11:19-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: twitter-tip-jar-may-expose-paypal-address-sparks-privacy-concerns

[Source](https://www.bleepingcomputer.com/news/security/twitter-tip-jar-may-expose-paypal-address-sparks-privacy-concerns/){:target="_blank" rel="noopener"}

> This week Twitter has begun experimenting with a new feature called 'Tip Jar,' which lets Twitter users tip select profiles to support their work.. But the feature has sparked multiple concerns among Twitter users: from the sender's PayPal shipping address getting exposed, to how are disputes handled. [...]
