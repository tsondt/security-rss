Title: East London council blurts thousands of residents' email addresses in To field blunder
Date: 2021-05-05T14:01:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: east-london-council-blurts-thousands-of-residents-email-addresses-in-to-field-blunder

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/05/tower_hamlets_email_fail/){:target="_blank" rel="noopener"}

> 'Was a Mailchimp sub too hard?!' asks Reg reader A local authority in East London has committed a classic privacy blunder by emailing what appear to be thousands of residents – while forgetting to use the BCC field and exposing all of the email addresseses to each recipient.... [...]
