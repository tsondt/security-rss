Title: City of Tulsa's online services disrupted in ransomware incident
Date: 2021-05-10T17:27:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: city-of-tulsas-online-services-disrupted-in-ransomware-incident

[Source](https://www.bleepingcomputer.com/news/security/city-of-tulsas-online-services-disrupted-in-ransomware-incident/){:target="_blank" rel="noopener"}

> The City of Tulsa, Oklahoma, has suffered a ransomware attack that forced the City to shut down its systems to prevent the further spread of the malware. [...]
