Title: Arming Google Workspace users and admins with advanced counter-abuse and threat-analysis capabilities
Date: 2021-05-18T17:00:00+00:00
Author: Sarah Zimmel
Category: GCP Security
Tags: Identity & Security;Google Workspace
Slug: arming-google-workspace-users-and-admins-with-advanced-counter-abuse-and-threat-analysis-capabilities

[Source](https://cloud.google.com/blog/products/workspace/new-security-capabilities-for-google-workspace/){:target="_blank" rel="noopener"}

> Like everything we build at Google, we created Google Workspace with security at its core to defend against threats and combat abuse, helping all of our users stay safe. With these protections, we enable IT admins to defend against attackers who are always looking for new vectors to exploit. To ensure we’re giving admins the controls and capabilities that help them protect their users and organizations against security threats and abuse, we’re adding new advanced security features to Google Workspace. Alert Center enrichment with VirusTotal threat context Google Workspace’s Alert Center provides IT admins with actionable, real-time alerts and security [...]
