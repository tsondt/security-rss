Title: Packaging vendor Ardagh admits cyber-attack disrupted operations
Date: 2021-05-19T14:08:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: packaging-vendor-ardagh-admits-cyber-attack-disrupted-operations

[Source](https://portswigger.net/daily-swig/packaging-vendor-ardagh-admits-cyber-attack-disrupted-operations){:target="_blank" rel="noopener"}

> Mystery assault contained [...]
