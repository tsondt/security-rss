Title: Ransomware Shuts Down US Pipeline
Date: 2021-05-10T19:17:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybercrime;ransomware;Russia
Slug: ransomware-shuts-down-us-pipeline

[Source](https://www.schneier.com/blog/archives/2021/05/ransomware-shuts-down-us-pipeline.html){:target="_blank" rel="noopener"}

> This is a major story : a probably Russian cybercrime group called DarkSide shut down the Colonial Pipeline in a ransomware attack. The pipeline supplies much of the East Coast. This is the new and improved ransomware attack: the hackers stole nearly 100 gig of data, and are threatening to publish it. The White House has declared a state of emergency and has created a task force to deal with the problem, but it’s unclear what they can do. This is bad; our supply chains are so tightly coupled that this kind of thing can have disproportionate effects. [...]
