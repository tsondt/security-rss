Title: Ryuk Ransomware Attack Sprung by Frugal Student
Date: 2021-05-06T17:26:53+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: ryuk-ransomware-attack-sprung-by-frugal-student

[Source](https://threatpost.com/ryuk-ransomware-attack-student/165918/){:target="_blank" rel="noopener"}

> The student opted for “free” software packed with a keylogger that grabbed credentials later used by "Totoro" to get into a biomolecular institute. [...]
