Title: Researcher calls out privacy flaw in Twitter’s new ‘Tip Jar’ donation feature
Date: 2021-05-07T13:53:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: researcher-calls-out-privacy-flaw-in-twitters-new-tip-jar-donation-feature

[Source](https://portswigger.net/daily-swig/researcher-calls-out-privacy-flaw-in-twitters-new-tip-jar-donation-feature){:target="_blank" rel="noopener"}

> Social media platform promises to warn donors that their address is leaked by PayPal when tipping other users [...]
