Title: AWS Shield threat landscape review: 2020 year-in-review
Date: 2021-05-20T21:51:21+00:00
Author: Mário Pinho
Category: AWS Security
Tags: AWS Shield;Foundational (100);Security, Identity, & Compliance;AWS Threat Research Team;AWS WAF;DDoS;Gaming;Security Blog;threats;Web application
Slug: aws-shield-threat-landscape-review-2020-year-in-review

[Source](https://aws.amazon.com/blogs/security/aws-shield-threat-landscape-review-2020-year-in-review/){:target="_blank" rel="noopener"}

> AWS Shield is a managed service that protects applications that are running on Amazon Web Services (AWS) against external threats, such as bots and distributed denial of service (DDoS) attacks. Shield detects network and web application-layer volumetric events that may indicate a DDoS attack, web content scraping, or other unauthorized non-human traffic that is interacting [...]
