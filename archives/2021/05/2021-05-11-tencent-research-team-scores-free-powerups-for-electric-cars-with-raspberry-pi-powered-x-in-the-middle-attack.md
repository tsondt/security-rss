Title: Tencent research team scores free powerups for electric cars with Raspberry Pi-powered X-in-the-middle attack
Date: 2021-05-11T04:04:26+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: tencent-research-team-scores-free-powerups-for-electric-cars-with-raspberry-pi-powered-x-in-the-middle-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/11/black_hat_asia_car_hacking/){:target="_blank" rel="noopener"}

> Another auto-exploit saw rPi push Telegram messages over CAN bus to brick a car Black Hat Asia Researchers have used the Black Hat Asia conference to demonstrate the awesome power of the Raspberry Pi as a car-p0wning platform.... [...]
