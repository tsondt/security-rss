Title: Oops, says Manchester City Council after thousands of number plates exposed in parking ticket spreadsheet
Date: 2021-05-13T10:01:14+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: oops-says-manchester-city-council-after-thousands-of-number-plates-exposed-in-parking-ticket-spreadsheet

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/13/manchester_numberplate_blunder_open_data/){:target="_blank" rel="noopener"}

> They are personal data, you know. Wait – you did know that, right? Exclusive Manchester City Council exposed online the number plates of more than 60,000 cars slapped with parking tickets, breaking data protection laws as it did so.... [...]
