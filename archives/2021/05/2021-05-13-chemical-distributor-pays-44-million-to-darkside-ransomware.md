Title: Chemical distributor pays $4.4 million to DarkSide ransomware
Date: 2021-05-13T18:24:29-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: chemical-distributor-pays-44-million-to-darkside-ransomware

[Source](https://www.bleepingcomputer.com/news/security/chemical-distributor-pays-44-million-to-darkside-ransomware/){:target="_blank" rel="noopener"}

> Chemical distribution company Brenntag paid a $4.4 million ransom in Bitcoin to the DarkSide ransomware gang to receive a decryptor for encrypted files and prevent the threat actors from publicly leaking stolen data. [...]
