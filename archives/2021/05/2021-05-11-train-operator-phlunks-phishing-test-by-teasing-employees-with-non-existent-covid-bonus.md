Title: Train operator phlunks phishing test by teasing employees with non-existent COVID bonus
Date: 2021-05-11T07:58:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: train-operator-phlunks-phishing-test-by-teasing-employees-with-non-existent-covid-bonus

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/11/west_midlands_trains_phishing_drill_goes_off/){:target="_blank" rel="noopener"}

> Someone at West Midlands Trains approved nasty cybersecurity drill UK rail operator West Midlands Trains sent an email to 2,500 employees to thank them for hard work during COVID and promised a one-time bonus as a reward, but that lovely news turned out to be phishing training. Needless to say, it did not go over well.... [...]
