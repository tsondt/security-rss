Title: Using Fake Reviews to Find Dangerous Extensions
Date: 2021-05-29T16:14:47+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;adobe;Amazon;brookice100@gmail.com;CapCut;chrome-stats.com;Facebook;google;Google Chrome Store;Hao Nguyen;HBO;microsoft;Microsoft Authenticator;Roku;Verizon
Slug: using-fake-reviews-to-find-dangerous-extensions

[Source](https://krebsonsecurity.com/2021/05/using-fake-reviews-to-find-dangerous-extensions/){:target="_blank" rel="noopener"}

> Fake, positive reviews have infiltrated nearly every corner of life online these days, confusing consumers while offering an unwelcome advantage to fraudsters and sub-par products everywhere. Happily, identifying and tracking these fake reviewer accounts is often the easiest way to spot scams. Here’s the story of how bogus reviews on a counterfeit Microsoft Authenticator browser extension exposed dozens of other extensions that siphoned personal and financial data. Comments on the fake Microsoft Authenticator browser extension show the reviews for these applications are either positive or very negative — basically calling it out as a scam. Image: chrome-stats.com. After hearing from [...]
