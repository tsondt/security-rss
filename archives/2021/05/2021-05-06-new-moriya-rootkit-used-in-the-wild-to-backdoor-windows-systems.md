Title: New Moriya rootkit used in the wild to backdoor Windows systems
Date: 2021-05-06T10:31:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: new-moriya-rootkit-used-in-the-wild-to-backdoor-windows-systems

[Source](https://www.bleepingcomputer.com/news/security/new-moriya-rootkit-used-in-the-wild-to-backdoor-windows-systems/){:target="_blank" rel="noopener"}

> A new stealthy rootkit was used by an unknown threat actor to backdoor targeted Windows systems in a likely ongoing espionage campaign dubbed TunnelSnake and going back to at least 2018. [...]
