Title: FBI Analyst Indicted for Theft of Osama bin Laden Threat Intel
Date: 2021-05-24T16:23:14+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government
Slug: fbi-analyst-indicted-for-theft-of-osama-bin-laden-threat-intel

[Source](https://threatpost.com/fbi-analyst-cyber-threat-bin-laden-data/166405/){:target="_blank" rel="noopener"}

> An FBI employee allegedly made off with top-secret documents, keeping them in her home for more than a decade. [...]
