Title: Brit watchdog shows some teeth over McAfee antivirus auto-renewals
Date: 2021-05-25T15:46:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: brit-watchdog-shows-some-teeth-over-mcafee-antivirus-auto-renewals

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/25/uk_cma_mcafee_refunds/){:target="_blank" rel="noopener"}

> Refund rights for customers The UK's Competition and Markets Authority (CMA) has reached agreement with antivirus vendor McAfee that means some customers whose software subscription was automatically renewed will be able to get a refund.... [...]
