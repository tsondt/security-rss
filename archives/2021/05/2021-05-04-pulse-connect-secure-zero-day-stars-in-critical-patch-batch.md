Title: Pulse Connect Secure zero-day stars in critical patch batch
Date: 2021-05-04T15:33:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: pulse-connect-secure-zero-day-stars-in-critical-patch-batch

[Source](https://portswigger.net/daily-swig/pulse-connect-secure-zero-day-stars-in-critical-patch-batch){:target="_blank" rel="noopener"}

> System updates urgent amid exploitation by nation-state attackers [...]
