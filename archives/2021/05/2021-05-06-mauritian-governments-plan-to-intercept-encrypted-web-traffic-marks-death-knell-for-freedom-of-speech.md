Title: Mauritian government’s plan to intercept encrypted web traffic marks ‘death knell for freedom of speech’
Date: 2021-05-06T11:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: mauritian-governments-plan-to-intercept-encrypted-web-traffic-marks-death-knell-for-freedom-of-speech

[Source](https://portswigger.net/daily-swig/mauritian-governments-plan-to-intercept-encrypted-web-traffic-marks-death-knell-for-freedom-of-speech){:target="_blank" rel="noopener"}

> Privacy and security advocates slam proposal that would allow authorities to ‘inspect’ all social media traffic [...]
