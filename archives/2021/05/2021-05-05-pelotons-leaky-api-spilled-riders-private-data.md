Title: Peloton’s Leaky API Spilled Riders’ Private Data
Date: 2021-05-05T16:03:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Privacy
Slug: pelotons-leaky-api-spilled-riders-private-data

[Source](https://threatpost.com/pelotons-spilled-riders-data/165880/){:target="_blank" rel="noopener"}

> On top of the privacy spill, Peloton is also recalling all treadmills after the equipment was linked to 70 injuries and the death of one child. [...]
