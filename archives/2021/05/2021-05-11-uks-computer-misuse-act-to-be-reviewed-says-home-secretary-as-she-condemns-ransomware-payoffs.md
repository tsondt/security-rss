Title: UK's Computer Misuse Act to be reviewed, says Home Secretary as she condemns ransomware payoffs
Date: 2021-05-11T16:00:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uks-computer-misuse-act-to-be-reviewed-says-home-secretary-as-she-condemns-ransomware-payoffs

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/11/computer_misuse_act_review_priti_patel/){:target="_blank" rel="noopener"}

> Priti Patel doesn't say a word about encryption, though CyberUK 21 Priti Patel has promised a government review of the UK's 30-year-old Computer Misuse Act "this year" as well as condemning companies that buy off ransomware criminals.... [...]
