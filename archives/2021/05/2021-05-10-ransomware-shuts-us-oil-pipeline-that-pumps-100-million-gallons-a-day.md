Title: Ransomware shuts US oil pipeline that pumps 100 million gallons a day
Date: 2021-05-10T00:15:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: ransomware-shuts-us-oil-pipeline-that-pumps-100-million-gallons-a-day

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/10/colonial_pipeline_ransomware/){:target="_blank" rel="noopener"}

> Colonial Pipeline says damage contained, some smaller lines already back, but has no timetable for resumption One of the USA’s largest oil pipelines has been shut by ransomware.... [...]
