Title: Cisco bugs allow creating admin accounts, executing commands as root
Date: 2021-05-05T14:51:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisco-bugs-allow-creating-admin-accounts-executing-commands-as-root

[Source](https://www.bleepingcomputer.com/news/security/cisco-bugs-allow-creating-admin-accounts-executing-commands-as-root/){:target="_blank" rel="noopener"}

> Cisco has fixed critical SD-WAN vManage and HyperFlex HX software security flaws that could enable remote attackers to execute commands as root or create rogue admin accounts. [...]
