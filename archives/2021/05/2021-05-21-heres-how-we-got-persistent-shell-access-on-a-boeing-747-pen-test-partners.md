Title: Here's how we got persistent shell access on a Boeing 747 – Pen Test Partners
Date: 2021-05-21T11:50:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: heres-how-we-got-persistent-shell-access-on-a-boeing-747-pen-test-partners

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/21/boeing_747_ife_windows_nt4_shell_access/){:target="_blank" rel="noopener"}

> In-flight entertainment system ran Windows NT4 – and almost defied access attempts Researchers from infosec biz Pen Test Partners established a persistent shell on an in-flight entertainment (IFE) system from a Boeing 747 airliner after using a vulnerability dating back to 1999.... [...]
