Title: FIN7 Backdoor Masquerades as Ethical Hacking Tool
Date: 2021-05-14T17:36:33+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: fin7-backdoor-masquerades-as-ethical-hacking-tool

[Source](https://threatpost.com/fin7-backdoor-ethical-hacking-tool/166194/){:target="_blank" rel="noopener"}

> The financially motivated cybercrime gang behind the Carbanak RAT is back with the Lizar malware, which can harvest all kinds of info from Windows machines. [...]
