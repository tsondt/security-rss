Title: Bizarro Banking Trojan Sports Sophisticated Backdoor
Date: 2021-05-17T16:19:34+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware
Slug: bizarro-banking-trojan-sports-sophisticated-backdoor

[Source](https://threatpost.com/bizarro-banking-trojan-backdoor/166211/){:target="_blank" rel="noopener"}

> The advanced Brazilian malware has gone global, harvesting bank logins from Android mobile users. [...]
