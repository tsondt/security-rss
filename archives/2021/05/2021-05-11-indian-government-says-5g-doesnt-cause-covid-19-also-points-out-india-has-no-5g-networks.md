Title: Indian government says 5G doesn’t cause COVID-19. Also points out India has no 5G networks
Date: 2021-05-11T02:58:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: indian-government-says-5g-doesnt-cause-covid-19-also-points-out-india-has-no-5g-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/11/india_5g_covid_19/){:target="_blank" rel="noopener"}

> But won’t reveal who it wants banned from social media over less obvious disinformation As COVID-19 continues to ravage India, the nation’s government has told it populace that 5G signals have nothing to do with the spread of the virus – if only because no 5G networks operate in India.... [...]
