Title: Enhance DDoS protection & get predictable pricing with new Cloud Armor service
Date: 2021-05-12T16:00:00+00:00
Author: Emil Kiner
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: enhance-ddos-protection-get-predictable-pricing-with-new-cloud-armor-service

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-armor-managed-protection-plus-is-now-ga/){:target="_blank" rel="noopener"}

> Securing websites and applications is a constant challenge for most organizations. To make it easier, we have introduced new capabilities within Cloud Armor over the past year that can help protect your applications. Today, we are announcing the general availability of Google Cloud Armor Managed Protection Plus. Cloud Armor, our Distributed Denial of Service (DDoS) protection and Web-Application Firewall (WAF) service on Google Cloud, leverages the same infrastructure, network, and technology that has protected Google’s internet-facing properties from some of the largest attacks ever reported. These same tools protect customers’ infrastructure from DDoS attacks, which are increasing in both magnitude [...]
