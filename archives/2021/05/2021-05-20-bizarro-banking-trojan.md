Title: Bizarro Banking Trojan
Date: 2021-05-20T14:13:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;banking;credentials;cryptography;malware;reports
Slug: bizarro-banking-trojan

[Source](https://www.schneier.com/blog/archives/2021/05/bizarro-banking-trojan.html){:target="_blank" rel="noopener"}

> Bizarro is a new banking trojan that is stealing financial information and crypto wallets....the program can be delivered in a couple of ways­ — either via malicious links contained within spam emails, or through a trojanized app. Using these sneaky methods, trojan operators will implant the malware onto a target device, where it will install a sophisticated backdoor that “contains more than 100 commands and allows the attackers to steal online banking account credentials,” the researchers write. The backdoor has numerous commands built in to allow manipulation of a targeted individual, including keystroke loggers that allow for harvesting of personal [...]
