Title: Pulse Secure fixes VPN zero-day used to hack high-value targets
Date: 2021-05-03T11:42:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: pulse-secure-fixes-vpn-zero-day-used-to-hack-high-value-targets

[Source](https://www.bleepingcomputer.com/news/security/pulse-secure-fixes-vpn-zero-day-used-to-hack-high-value-targets/){:target="_blank" rel="noopener"}

> Pulse Secure has fixed a zero-day vulnerability in the Pulse Connect Secure (PCS) SSL VPN appliance that is being actively exploited to compromise the internal networks of defense firms and govt agencies. [...]
