Title: NHS App gets go-ahead for vaccine passport use despite protest from privacy groups
Date: 2021-05-11T15:15:05+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: nhs-app-gets-go-ahead-for-vaccine-passport-use-despite-protest-from-privacy-groups

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/11/nhs_app_vaccine_passport_england/){:target="_blank" rel="noopener"}

> Big Brother Watch warns app contains too much sensitive medical information Folks in England can from next week use the NHS App to confer their vaccination status, in the face of warnings that the technology could lead to identifiable medical information being exposed.... [...]
