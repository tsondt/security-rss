Title: DarkSide ransomware will now vet targets after pipeline cyberattack
Date: 2021-05-10T11:40:13-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: darkside-ransomware-will-now-vet-targets-after-pipeline-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/darkside-ransomware-will-now-vet-targets-after-pipeline-cyberattack/){:target="_blank" rel="noopener"}

> The DarkSide ransomware gang posted a new "press release" today stating that they are apolitical and will vet all targets before they are attacked. [...]
