Title: The Week in Ransomware - May 21st 2021 - Healthcare under attack
Date: 2021-05-21T15:18:19-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-may-21st-2021-healthcare-under-attack

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-21st-2021-healthcare-under-attack/){:target="_blank" rel="noopener"}

> This week's ransomware news has been dominated by the attack on Ireland's Health Service Executive (HSE) that has severely disrupted Ireland's healthcare system. [...]
