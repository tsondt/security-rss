Title: WP Statistics Bug Allows Attackers to Lift Data from WordPress Sites
Date: 2021-05-21T17:30:52+00:00
Author: Tara Seals
Category: Threatpost
Tags: Privacy;Vulnerabilities;Web Security
Slug: wp-statistics-bug-allows-attackers-to-lift-data-from-wordpress-sites

[Source](https://threatpost.com/wp-statistics-attackers-data-wordpress/166386/){:target="_blank" rel="noopener"}

> The plugin, installed on hundreds of thousands of sites, allows anyone to filch database info without having to be logged in. [...]
