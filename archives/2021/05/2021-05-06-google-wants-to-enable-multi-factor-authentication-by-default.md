Title: Google wants to enable multi-factor authentication by default
Date: 2021-05-06T17:05:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-wants-to-enable-multi-factor-authentication-by-default

[Source](https://www.bleepingcomputer.com/news/security/google-wants-to-enable-multi-factor-authentication-by-default/){:target="_blank" rel="noopener"}

> Google strives to push all its users to start using two-factor authentication (2FA), which can block attackers from taking control of their accounts using compromised credentials or guessing their passwords. [...]
