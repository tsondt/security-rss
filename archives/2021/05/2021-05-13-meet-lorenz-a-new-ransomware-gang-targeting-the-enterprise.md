Title: Meet Lorenz — A new ransomware gang targeting the enterprise
Date: 2021-05-13T12:54:13-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: meet-lorenz-a-new-ransomware-gang-targeting-the-enterprise

[Source](https://www.bleepingcomputer.com/news/security/meet-lorenz-a-new-ransomware-gang-targeting-the-enterprise/){:target="_blank" rel="noopener"}

> A new ransomware operation known as Lorenz targets organizations worldwide with customized attacks demanding hundreds of thousands of dollars in ransoms. [...]
