Title: AXA ransomware attack comes just days after insurer pulled coverage for cyber-attack class in France
Date: 2021-05-18T11:37:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: axa-ransomware-attack-comes-just-days-after-insurer-pulled-coverage-for-cyber-attack-class-in-france

[Source](https://portswigger.net/daily-swig/axa-ransomware-attack-comes-just-days-after-insurer-pulled-coverage-for-cyber-attack-class-in-france){:target="_blank" rel="noopener"}

> Third party, fire, and (ransomware) theft [...]
