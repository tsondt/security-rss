Title: Food giant JBS Foods shuts down production after cyberattack
Date: 2021-05-31T10:57:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: food-giant-jbs-foods-shuts-down-production-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/food-giant-jbs-foods-shuts-down-production-after-cyberattack/){:target="_blank" rel="noopener"}

> JBS Foods, a leading food company and the largest meat producer globally, was forced to shut down production at multiple sites worldwide following a cyberattack. [...]
