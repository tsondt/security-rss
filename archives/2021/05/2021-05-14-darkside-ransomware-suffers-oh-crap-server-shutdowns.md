Title: DarkSide Ransomware Suffers ‘Oh, Crap!’ Server Shutdowns
Date: 2021-05-14T16:05:13+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: darkside-ransomware-suffers-oh-crap-server-shutdowns

[Source](https://threatpost.com/darksides-servers-shutdown/166187/){:target="_blank" rel="noopener"}

> The RaaS that crippled Colonial Pipeline lost the servers it uses to pull off ransomware attacks, while REvil’s gonads shrank in response. [...]
