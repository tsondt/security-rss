Title: DarkSide Getting Taken to ‘Hackers’ Court’ For Not Paying Affiliates
Date: 2021-05-21T18:41:37+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Web Security
Slug: darkside-getting-taken-to-hackers-court-for-not-paying-affiliates

[Source](https://threatpost.com/darkside-hackers-court-paying-affiliates/166393/){:target="_blank" rel="noopener"}

> A shadow court system for hackers shows how professional ransomware gangs have become. [...]
