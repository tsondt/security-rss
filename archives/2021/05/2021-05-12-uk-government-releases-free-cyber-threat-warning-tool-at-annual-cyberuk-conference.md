Title: UK government releases free cyber-threat warning tool at annual CyberUK conference
Date: 2021-05-12T15:03:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uk-government-releases-free-cyber-threat-warning-tool-at-annual-cyberuk-conference

[Source](https://portswigger.net/daily-swig/uk-government-releases-free-cyber-threat-warning-tool-at-annual-cyberuk-conference){:target="_blank" rel="noopener"}

> Announcement comes as GCHQ boss emphasizes need to confront Russia and China on cybersecurity [...]
