Title: Chrome Insider: Managing BeyondCorp Enterprise's threat and data protection capabilities in Chrome
Date: 2021-05-06T17:00:00+00:00
Author: Fletcher Oliver
Category: GCP Security
Tags: Chrome Insider;Productivity & Collaboration;Identity & Security;Chrome Enterprise
Slug: chrome-insider-managing-beyondcorp-enterprises-threat-and-data-protection-capabilities-in-chrome

[Source](https://cloud.google.com/blog/products/chrome-enterprise/managing-beyondcorp-enterprises-threat-and-data-protection/){:target="_blank" rel="noopener"}

> Google's BeyondCorp Enterprise recently launched, offering organizations a zero trust solution that enables secure access to applications and cloud resources with integrated threat and data protection. These threat and data protection capabilities are delivered directly through Chrome, so organizations can easily take advantage of our web-based protections. Due to BeyondCorp Enterprise’s agentless approach utilizing the Chrome browser, these capabilities are extremely easy to adopt and deploy. The solution is delivered as a non-disruptive overlay to your existing architecture, with no need to install additional software, clients, or agents. Threat and data protection features in BeyondCorp Enterprise help prevent web-based threats [...]
