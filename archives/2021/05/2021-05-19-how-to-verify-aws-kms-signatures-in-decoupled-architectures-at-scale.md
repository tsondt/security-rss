Title: How to verify AWS KMS signatures in decoupled architectures at scale
Date: 2021-05-19T20:11:18+00:00
Author: Raj Jain
Category: AWS Security
Tags: AWS Key Management Service;Expert (400);Security, Identity, & Compliance;AWS KMS;digital signature;ECSDA;Security Blog
Slug: how-to-verify-aws-kms-signatures-in-decoupled-architectures-at-scale

[Source](https://aws.amazon.com/blogs/security/how-to-verify-aws-kms-signatures-in-decoupled-architectures-at-scale/){:target="_blank" rel="noopener"}

> AWS Key Management Service (AWS KMS) makes it easy to create and manage cryptographic keys in your applications. The service supports both symmetric and asymmetric customer master keys (CMKs). The asymmetric CMKs offer digital signature capability, which data consumers can use to verify that data is from a trusted producer and is unaltered in transit. [...]
