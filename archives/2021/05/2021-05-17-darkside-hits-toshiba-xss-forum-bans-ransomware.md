Title: DarkSide Hits Toshiba; XSS Forum Bans Ransomware
Date: 2021-05-17T16:23:35+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Malware;Web Security
Slug: darkside-hits-toshiba-xss-forum-bans-ransomware

[Source](https://threatpost.com/darkside-toshiba-xss-bans-ransomware/166210/){:target="_blank" rel="noopener"}

> The criminal forum washed its hands of ransomware after DarkSide's pipeline attack & alleged shutdown: A "loss of servers" that didn't stop another attack. [...]
