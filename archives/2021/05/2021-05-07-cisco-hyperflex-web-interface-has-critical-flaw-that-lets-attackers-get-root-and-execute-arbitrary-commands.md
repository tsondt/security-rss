Title: Cisco HyperFlex web interface has critical flaw that lets attackers get <code>root</code> and execute arbitrary commands
Date: 2021-05-07T05:52:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: cisco-hyperflex-web-interface-has-critical-flaw-that-lets-attackers-get-root-and-execute-arbitrary-commands

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/07/cisco_hyperflex_critical_flaw/){:target="_blank" rel="noopener"}

> You know the drill: shake your head in disbelief, then figure out if patching will wipe out a weekend or be merely inconvenient Cisco has revealed a pair of critical bugs in its HyperFlex hyperconverged infrastructure product.... [...]
