Title: Ransomware gang leaks data from Metropolitan Police Department
Date: 2021-05-11T11:29:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-leaks-data-from-metropolitan-police-department

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-leaks-data-from-metropolitan-police-department/){:target="_blank" rel="noopener"}

> Babuk ransomware operators have leaked what they claim are personal files belongin to police officers from the Metropolitan Police Department after negotiations went stale. [...]
