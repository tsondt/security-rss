Title: PDF Feature ‘Certified’ Widely Vulnerable to Attack
Date: 2021-05-26T20:14:30+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities
Slug: pdf-feature-certified-widely-vulnerable-to-attack

[Source](https://threatpost.com/pdf-certified-widely-vulnerable-to-attack/166505/){:target="_blank" rel="noopener"}

> Researchers found flaws most of the ‘popular’ PDF applications tested. [...]
