Title: Hong Kong recorded phishing surge in 2020 as scum sought to cash in on viral worries
Date: 2021-05-28T04:56:44+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: hong-kong-recorded-phishing-surge-in-2020-as-scum-sought-to-cash-in-on-viral-worries

[Source](https://go.theregister.com/feed/www.theregister.com/2021/05/28/hong_kong_cybercrime_stats/){:target="_blank" rel="noopener"}

> Special Administrative Region recorded plunge in ransomware attacks Criminals tried to exploit Hong Kong residents' COVID-related anxiety, according to new security data released yesterday by the Special Administrative Region's secretary for innovation and technology Alfred Sit.... [...]
