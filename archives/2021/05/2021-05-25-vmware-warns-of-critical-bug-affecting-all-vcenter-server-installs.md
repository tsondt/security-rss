Title: VMware warns of critical bug affecting all vCenter Server installs
Date: 2021-05-25T14:21:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: vmware-warns-of-critical-bug-affecting-all-vcenter-server-installs

[Source](https://www.bleepingcomputer.com/news/security/vmware-warns-of-critical-bug-affecting-all-vcenter-server-installs/){:target="_blank" rel="noopener"}

> VMware urges customers to patch a critical remote code execution (RCE) vulnerability in the Virtual SAN Health Check plug-in and impacting all vCenter Server deployments. [...]
