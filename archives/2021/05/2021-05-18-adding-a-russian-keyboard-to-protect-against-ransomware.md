Title: Adding a Russian Keyboard to Protect against Ransomware
Date: 2021-05-18T15:31:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: law enforcement;malware;ransomware;Russia
Slug: adding-a-russian-keyboard-to-protect-against-ransomware

[Source](https://www.schneier.com/blog/archives/2021/05/adding-a-russian-keyboard-to-protect-against-ransomware.html){:target="_blank" rel="noopener"}

> A lot of Russian malware — the malware that targeted the Colonial Pipeline, for example — won’t install on computers with a Cyrillic keyboard installed. Brian Krebs wonders if this could be a useful defense: In Russia, for example, authorities there generally will not initiate a cybercrime investigation against one of their own unless a company or individual within the country’s borders files an official complaint as a victim. Ensuring that no affiliates can produce victims in their own countries is the easiest way for these criminals to stay off the radar of domestic law enforcement agencies. [...] DarkSide, like [...]
