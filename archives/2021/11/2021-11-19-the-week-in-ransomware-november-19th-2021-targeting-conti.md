Title: The Week in Ransomware - November 19th 2021 - Targeting Conti
Date: 2021-11-19T19:19:16-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-19-the-week-in-ransomware-november-19th-2021-targeting-conti

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-19th-2021-targeting-conti/){:target="_blank" rel="noopener"}

> While last week was full of arrests and law enforcement actions, this week has been much quieter, with mostly new research released. [...]
