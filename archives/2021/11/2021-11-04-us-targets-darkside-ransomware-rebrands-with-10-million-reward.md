Title: US targets DarkSide ransomware, rebrands with $10 million reward
Date: 2021-11-04T17:00:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-11-04-us-targets-darkside-ransomware-rebrands-with-10-million-reward

[Source](https://www.bleepingcomputer.com/news/security/us-targets-darkside-ransomware-rebrands-with-10-million-reward/){:target="_blank" rel="noopener"}

> The US government is targeting the DarkSide ransomware and its rebrands with up to a $10,000,000 reward for information leading to the identification or arrest of members of the operation. [...]
