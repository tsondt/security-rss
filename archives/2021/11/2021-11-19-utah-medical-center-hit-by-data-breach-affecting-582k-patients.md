Title: Utah medical center hit by data breach affecting 582k patients
Date: 2021-11-19T10:35:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2021-11-19-utah-medical-center-hit-by-data-breach-affecting-582k-patients

[Source](https://www.bleepingcomputer.com/news/security/utah-medical-center-hit-by-data-breach-affecting-582k-patients/){:target="_blank" rel="noopener"}

> Utah Imaging Associates (UIA), a Utah-based radiology center, has announced a data breach affecting 582,170 people after their personal information was exposed. [...]
