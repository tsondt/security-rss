Title: GoDaddy managed WordPress hosting service breach exposed 1.2m user profiles
Date: 2021-11-23T13:56:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-23-godaddy-managed-wordpress-hosting-service-breach-exposed-12m-user-profiles

[Source](https://portswigger.net/daily-swig/godaddy-managed-wordpress-hosting-service-breach-exposed-1-2m-user-profiles){:target="_blank" rel="noopener"}

> External investigation finds breach dates back more than two months [...]
