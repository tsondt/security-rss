Title: Alleged Twitter hacker charged with theft of $784K in crypto via SIM swaps
Date: 2021-11-03T18:55:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-03-alleged-twitter-hacker-charged-with-theft-of-784k-in-crypto-via-sim-swaps

[Source](https://www.bleepingcomputer.com/news/security/alleged-twitter-hacker-charged-with-theft-of-784k-in-crypto-via-sim-swaps/){:target="_blank" rel="noopener"}

> The US Department of Justice has indicted a suspected Twitter hacker known as 'PlugWalkJoe' for also stealing $784,000 worth of cryptocurrency using SIM swap attacks. [...]
