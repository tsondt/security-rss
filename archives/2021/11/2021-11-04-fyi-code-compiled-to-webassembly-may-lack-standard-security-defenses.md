Title: FYI: Code compiled to WebAssembly may lack standard security defenses
Date: 2021-11-04T12:14:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-04-fyi-code-compiled-to-webassembly-may-lack-standard-security-defenses

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/04/webassembly_stack_canaries/){:target="_blank" rel="noopener"}

> Mechanisms taken for granted on x86 vanish in WASM land, says trio WebAssembly has been promoted for its security benefits, though researchers in Belgium and New Zealand contend applications built in this binary format lack important protections.... [...]
