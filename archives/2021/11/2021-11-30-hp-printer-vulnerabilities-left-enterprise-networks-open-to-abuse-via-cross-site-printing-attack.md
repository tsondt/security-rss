Title: HP printer vulnerabilities left enterprise networks open to abuse via ‘cross-site printing’&nbsp;attack
Date: 2021-11-30T13:17:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-30-hp-printer-vulnerabilities-left-enterprise-networks-open-to-abuse-via-cross-site-printing-attack

[Source](https://portswigger.net/daily-swig/hp-printer-vulnerabilities-left-enterprise-networks-open-to-abuse-via-cross-site-printing-nbsp-attack){:target="_blank" rel="noopener"}

> Hardware hacking technique gets points for innovation, although some degree of social engineering is required [...]
