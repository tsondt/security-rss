Title: Fall 2021 SOC reports now available with 141 services in scope
Date: 2021-11-15T18:49:34+00:00
Author: Ninad Naik
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC 1;AWS SOC 2;AWS SOC 3;AWS SOC Reports
Slug: 2021-11-15-fall-2021-soc-reports-now-available-with-141-services-in-scope

[Source](https://aws.amazon.com/blogs/security/fall-2021-soc-reports-now-available-with-141-services-in-scope/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we’re committed to providing our customers with continued assurance over the security, availability and confidentiality of the AWS control environment. We’re proud to deliver the System and Organizational (SOC) 1, 2, and 3 reports to enable our AWS customers to maintain confidence in AWS services. For the Fall 2021 SOC [...]
