Title: How Veolia protects its cloud environment across 31 countries with Security Command Center
Date: 2021-11-01T16:00:00+00:00
Author: Thomas Meriadec
Category: GCP Security
Tags: Google Cloud;Sustainability;Google Cloud in Europe;Compliance;Identity & Security
Slug: 2021-11-01-how-veolia-protects-its-cloud-environment-across-31-countries-with-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/how-veolia-uses-security-command-center/){:target="_blank" rel="noopener"}

> The world’s resources are increasingly scarce and yet too often they are wasted. At Veolia, we use waste to produce new resources, helping to build a circular economy that redefines growth with a focus on sustainability. Our sustainability mission transcends borders, and nearly 179,000 employees work in dozens of profit centers worldwide to bring it to life. It’s a massive operation that requires an IT architecture to match, which is why we’ve streamlined critical IT work across our global operations with Google Cloud. As Technical Lead and Product Manager for Veolia’s Google Cloud, it’s my team’s responsibility to standardize processes [...]
