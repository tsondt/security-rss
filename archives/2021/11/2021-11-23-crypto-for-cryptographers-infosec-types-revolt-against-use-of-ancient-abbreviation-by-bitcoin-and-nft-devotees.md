Title: Crypto for cryptographers! Infosec types revolt against use of ancient abbreviation by Bitcoin and NFT devotees
Date: 2021-11-23T18:45:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-23-crypto-for-cryptographers-infosec-types-revolt-against-use-of-ancient-abbreviation-by-bitcoin-and-nft-devotees

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/23/crypto_graphy_or_currency/){:target="_blank" rel="noopener"}

> Complaints abound that yoof use it to mean 'digital currency' Poll Infosec must "reclaim" the word crypto from people who trade in Bitcoins and other digital currencies, according to industry veteran Bruce Schneier – and it seems some Reg readers agree while others disagree.... [...]
