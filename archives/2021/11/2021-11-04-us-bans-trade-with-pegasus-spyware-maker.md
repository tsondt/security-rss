Title: US Bans Trade With Pegasus Spyware Maker
Date: 2021-11-04T18:03:41+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Mobile Security;Vulnerabilities
Slug: 2021-11-04-us-bans-trade-with-pegasus-spyware-maker

[Source](https://threatpost.com/pegasus-spyware-blacklisted-us/175999/){:target="_blank" rel="noopener"}

> NSO Group plans to fight the trade ban, saying it's "dismayed" and clinging to the mantra that its tools actually help to prevent terrorism and crime. [...]
