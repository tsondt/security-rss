Title: FBI spams thousands with fake infosec advice after 'software misconfiguration'
Date: 2021-11-15T02:30:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-15-fbi-spams-thousands-with-fake-infosec-advice-after-software-misconfiguration

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/15/fbi_fake_emails/){:target="_blank" rel="noopener"}

> Looks like feuding hackers wanted to expose Feds' failings as a public service. We want to believe The United States Federal Bureau of Investigation has admitted that a software misconfiguration let parties unknown send email from its servers.... [...]
