Title: Achieving Autonomic Security Operations: Reducing toil
Date: 2021-11-29T17:00:00+00:00
Author: Anton Chuvakin
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-11-29-achieving-autonomic-security-operations-reducing-toil

[Source](https://cloud.google.com/blog/products/identity-security/achieving-autonomic-security-operations-reducing-toil/){:target="_blank" rel="noopener"}

> Almost two decades of Site Reliability Engineering (SRE) has proved the value of incorporating software engineering practices into traditional infrastructure and operations management. In a parallel world, we’re finding that similar principles can radically improve outcomes for the Security Operations Center (SOC), a domain plagued with infrastructure and operational challenges. As more organizations go through digital transformation, the importance of building a highly effective threat management function rises to be one of their top priorities. In our paper, “Autonomic Security Operations — 10X Transformation of the Security Operations Center”, we’ve outlined our approach to modernizing Security Operations. One of the [...]
