Title: US govt warns of increased ransomware risks during holidays
Date: 2021-11-22T13:45:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-22-us-govt-warns-of-increased-ransomware-risks-during-holidays

[Source](https://www.bleepingcomputer.com/news/security/us-govt-warns-of-increased-ransomware-risks-during-holidays/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) and the FBI warned critical infrastructure partners and public/private sector organizations not to let down their defenses against ransomware attacks during the holiday season. [...]
