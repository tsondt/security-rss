Title: Russian ransomware gangs start collaborating with Chinese hackers
Date: 2021-11-17T13:31:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-17-russian-ransomware-gangs-start-collaborating-with-chinese-hackers

[Source](https://www.bleepingcomputer.com/news/security/russian-ransomware-gangs-start-collaborating-with-chinese-hackers/){:target="_blank" rel="noopener"}

> ​There's some unusual activity brewing on Russian-speaking cybercrime forums, where hackers appear to be reaching out to Chinese counterparts for collaboration. [...]
