Title: Friday Squid Blogging: Giant Squid Art
Date: 2021-11-12T22:16:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-11-12-friday-squid-blogging-giant-squid-art

[Source](https://www.schneier.com/blog/archives/2021/11/friday-squid-blogging-giant-squid-art.html){:target="_blank" rel="noopener"}

> Images of giant squid (and octopi) attacking. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
