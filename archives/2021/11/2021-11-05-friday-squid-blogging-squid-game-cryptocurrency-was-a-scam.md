Title: Friday Squid Blogging: Squid Game Cryptocurrency Was a Scam
Date: 2021-11-05T21:11:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;fraud;scams;squid
Slug: 2021-11-05-friday-squid-blogging-squid-game-cryptocurrency-was-a-scam

[Source](https://www.schneier.com/blog/archives/2021/11/friday-squid-blogging-squid-game-cryptocurrency-was-a-scam.html){:target="_blank" rel="noopener"}

> The Squid Game cryptocurrency was a complete scam : The SQUID cryptocurrency peaked at a price of $2,861 before plummeting to $0 around 5:40 a.m. ET., according to the website CoinMarketCap. This kind of theft, commonly called a “rug pull” by crypto investors, happens when the creators of the crypto quickly cash out their coins for real money, draining the liquidity pool from the exchange. I don’t know why anyone would trust an investment — any investment — that you could buy but not sell. Wired story. As usual, you can also use this squid post to talk about the [...]
