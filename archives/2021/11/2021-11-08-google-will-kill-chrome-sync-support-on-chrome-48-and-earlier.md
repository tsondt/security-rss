Title: Google will kill Chrome sync support on Chrome 48 and earlier
Date: 2021-11-08T07:58:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2021-11-08-google-will-kill-chrome-sync-support-on-chrome-48-and-earlier

[Source](https://www.bleepingcomputer.com/news/google/google-will-kill-chrome-sync-support-on-chrome-48-and-earlier/){:target="_blank" rel="noopener"}

> Google will end support for the Chrome sync feature for all users still running Google Chrome 48 and earlier after Chrome 96 reaches the stable channel. [...]
