Title: VMware addresses SSRF, arbitrary file read flaws in vCenter Server
Date: 2021-11-24T13:33:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-24-vmware-addresses-ssrf-arbitrary-file-read-flaws-in-vcenter-server

[Source](https://portswigger.net/daily-swig/vmware-addresses-ssrf-arbitrary-file-read-flaws-in-vcenter-server){:target="_blank" rel="noopener"}

> ‘Important’ severity flaws both reside in the vSphere Web Client [...]
