Title: Former Broadcom engineer accused of pinching chip tech to share with new Chinese employer
Date: 2021-11-10T05:56:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-11-10-former-broadcom-engineer-accused-of-pinching-chip-tech-to-share-with-new-chinese-employer

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/10/broadcom_engineer_trade_secrets_theft_allegation/){:target="_blank" rel="noopener"}

> We're guessing it wasn't Fiber Channel, but it would be weirdly cool if it was A federal grand jury has charged a former Broadcom engineer with stealing trade secrets and using them while working at a new employer – a Chinese chip start-up.... [...]
