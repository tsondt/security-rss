Title: Pirate Sports Streamer Gets Busted, Pivots to MLB Extortion
Date: 2021-11-01T20:22:33+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-11-01-pirate-sports-streamer-gets-busted-pivots-to-mlb-extortion

[Source](https://threatpost.com/pirate-sports-streamer-mlb-extortion/175898/){:target="_blank" rel="noopener"}

> An alleged sports content pirate is accused of not only hijacking leagues' streams but also threatening to tell reporters how he accessed their systems. [...]
