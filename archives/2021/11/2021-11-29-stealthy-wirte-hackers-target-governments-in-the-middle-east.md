Title: Stealthy WIRTE hackers target governments in the Middle East
Date: 2021-11-29T11:30:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-29-stealthy-wirte-hackers-target-governments-in-the-middle-east

[Source](https://www.bleepingcomputer.com/news/security/stealthy-wirte-hackers-target-governments-in-the-middle-east/){:target="_blank" rel="noopener"}

> A stealthy hacking group named WIRTE has been linked to a government-targeting campaign conducting attacks since at least 2019 using malicious Excel 4.0 macros. [...]
