Title: US regulators order banks to report cyberattacks within 36 hours
Date: 2021-11-19T08:05:29-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-19-us-regulators-order-banks-to-report-cyberattacks-within-36-hours

[Source](https://www.bleepingcomputer.com/news/security/us-regulators-order-banks-to-report-cyberattacks-within-36-hours/){:target="_blank" rel="noopener"}

> US federal bank regulatory agencies have approved a new rule requiring banks to notify their primary federal regulators of significant computer-security incidents within 36 hours. [...]
