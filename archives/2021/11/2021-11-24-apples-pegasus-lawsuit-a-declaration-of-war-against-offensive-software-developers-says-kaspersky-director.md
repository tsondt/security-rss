Title: Apple's Pegasus lawsuit a 'declaration of war' against offensive software developers, says Kaspersky director
Date: 2021-11-24T13:12:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-11-24-apples-pegasus-lawsuit-a-declaration-of-war-against-offensive-software-developers-says-kaspersky-director

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/24/apples_pegasus_lawsuit/){:target="_blank" rel="noopener"}

> Regional exec says Apple wants offensive researchers out of the field because they are harmful to the reputation of the company Kaspersky's APAC director of Global Research and Analysis, Vitaly Kamlyuk, has called Apple's lawsuit against Pegasus maker NSO a "declaration of war against software developers."... [...]
