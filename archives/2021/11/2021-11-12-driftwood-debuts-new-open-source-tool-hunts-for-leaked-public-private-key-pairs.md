Title: Driftwood debuts: New open source tool hunts for leaked public-private key pairs
Date: 2021-11-12T12:12:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-12-driftwood-debuts-new-open-source-tool-hunts-for-leaked-public-private-key-pairs

[Source](https://portswigger.net/daily-swig/driftwood-debuts-new-open-source-tool-hunts-for-leaked-public-private-key-pairs){:target="_blank" rel="noopener"}

> The tool will help security professionals find compromised TLS keys and sensitive keys tied to GitHub accounts [...]
