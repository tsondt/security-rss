Title: REvil ransomware affiliates arrested in Romania and Kuwait
Date: 2021-11-08T09:51:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-08-revil-ransomware-affiliates-arrested-in-romania-and-kuwait

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-affiliates-arrested-in-romania-and-kuwait/){:target="_blank" rel="noopener"}

> Romanian law enforcement authorities have arrested two suspects believed to be Sodinokibi/REvil ransomware affiliates, allegedly responsible for infecting thousands of victims. [...]
