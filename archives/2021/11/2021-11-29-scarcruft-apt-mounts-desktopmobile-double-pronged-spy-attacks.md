Title: ScarCruft APT Mounts Desktop/Mobile Double-Pronged Spy Attacks
Date: 2021-11-29T19:08:06+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: 2021-11-29-scarcruft-apt-mounts-desktopmobile-double-pronged-spy-attacks

[Source](https://threatpost.com/scarcruft-apt-desktop-mobile-attacks/176620/){:target="_blank" rel="noopener"}

> The North Korea-linked group is deploying the Chinotto spyware backdoor against dissidents, journalists and other politically relevant individuals in South Korea. [...]
