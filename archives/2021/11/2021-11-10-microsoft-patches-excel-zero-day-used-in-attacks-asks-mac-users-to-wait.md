Title: Microsoft patches Excel zero-day used in attacks, asks Mac users to wait
Date: 2021-11-10T10:36:47-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Apple;Security
Slug: 2021-11-10-microsoft-patches-excel-zero-day-used-in-attacks-asks-mac-users-to-wait

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-patches-excel-zero-day-used-in-attacks-asks-mac-users-to-wait/){:target="_blank" rel="noopener"}

> During this month's Patch Tuesday, Microsoft has patched an Excel zero-day vulnerability exploited in the wild by threat actors. [...]
