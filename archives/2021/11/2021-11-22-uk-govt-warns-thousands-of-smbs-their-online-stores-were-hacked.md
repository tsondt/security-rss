Title: UK govt warns thousands of SMBs their online stores were hacked
Date: 2021-11-22T15:05:16-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-22-uk-govt-warns-thousands-of-smbs-their-online-stores-were-hacked

[Source](https://www.bleepingcomputer.com/news/security/uk-govt-warns-thousands-of-smbs-their-online-stores-were-hacked/){:target="_blank" rel="noopener"}

> The UK's National Cyber Security Centre (NCSC) says it warned the owners of more than 4,000 online stores that their sites were compromised in Magecart attacks to steal the payment info of customers. [...]
