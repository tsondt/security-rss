Title: UK Ministry of Justice secures HVAC systems 'protected' by passwordless Wi-Fi after Register tipoff
Date: 2021-11-23T10:15:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-23-uk-ministry-of-justice-secures-hvac-systems-protected-by-passwordless-wi-fi-after-register-tipoff

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/23/unsecured_rcj_hvac_wifi_routers/){:target="_blank" rel="noopener"}

> There's a default admin password online too The Ministry of Justice has secured a set of Wi-Fi access points that potentially gave admin access to industrial control equipment after a tipoff by The Register.... [...]
