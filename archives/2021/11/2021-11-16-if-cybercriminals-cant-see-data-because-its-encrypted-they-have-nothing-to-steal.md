Title: If cybercriminals can’t see data because it’s encrypted, they have nothing to steal
Date: 2021-11-16T07:30:05+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2021-11-16-if-cybercriminals-cant-see-data-because-its-encrypted-they-have-nothing-to-steal

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/16/encrypted_data_ransomware_defence/){:target="_blank" rel="noopener"}

> This is a no-brainer, says Thales Paid feature Here’s the irony of ransomware data breach stories that gets surprisingly little attention: cybercriminals enthusiastically encrypt and steal sensitive data to extort money and yet their victims rarely bother to defend themselves using the same obviously highly effective concept.... [...]
