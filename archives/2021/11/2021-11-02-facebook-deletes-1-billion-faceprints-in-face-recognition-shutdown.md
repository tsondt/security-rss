Title: Facebook deletes 1 billion faceprints in Face Recognition shutdown
Date: 2021-11-02T16:08:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Technology;Security
Slug: 2021-11-02-facebook-deletes-1-billion-faceprints-in-face-recognition-shutdown

[Source](https://www.bleepingcomputer.com/news/technology/facebook-deletes-1-billion-faceprints-in-face-recognition-shutdown/){:target="_blank" rel="noopener"}

> Facebook announced today that they will no longer use the Face Recognition system on their platform and will be deleting over 1 billion people's facial recognition profiles. [...]
