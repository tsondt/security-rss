Title: BlackMatter ransomware gang says it's disbanding – again – after Ukraine arrests
Date: 2021-11-03T16:15:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-03-blackmatter-ransomware-gang-says-its-disbanding-again-after-ukraine-arrests

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/03/blackmatter_ransomware_disbanding_claim/){:target="_blank" rel="noopener"}

> Just like the last time. Don't get your hopes up A member of the BlackMatter (aka Darkside) ransomware gang has publicly claimed the extortionists are shutting down, causing much excitement within the infosec world.... [...]
