Title: Marine services provider Swire Pacific Offshore hit by ransomware
Date: 2021-11-26T10:31:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-26-marine-services-provider-swire-pacific-offshore-hit-by-ransomware

[Source](https://www.bleepingcomputer.com/news/security/marine-services-provider-swire-pacific-offshore-hit-by-ransomware/){:target="_blank" rel="noopener"}

> Swire Pacific Offshore (SPO) has discovered an unauthorized network infiltration onto its IT systems, resulting in the compromise of some employee data. [...]
