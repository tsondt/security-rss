Title: HTML smuggling: Fresh attack technique increasingly being used to target banking sector
Date: 2021-11-12T15:08:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-12-html-smuggling-fresh-attack-technique-increasingly-being-used-to-target-banking-sector

[Source](https://portswigger.net/daily-swig/html-smuggling-fresh-attack-technique-increasingly-being-used-to-target-banking-sector){:target="_blank" rel="noopener"}

> Evasive malware is being spread via email in campaigns similar to those of nation-state actors [...]
