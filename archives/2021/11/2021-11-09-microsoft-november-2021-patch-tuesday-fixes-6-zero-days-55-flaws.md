Title: Microsoft November 2021 Patch Tuesday fixes 6 zero-days, 55 flaws
Date: 2021-11-09T13:30:03-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-09-microsoft-november-2021-patch-tuesday-fixes-6-zero-days-55-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-november-2021-patch-tuesday-fixes-6-zero-days-55-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's November 2021 Patch Tuesday, and with it comes fixes for six zero-day vulnerabilities and a total of 55 flaws. The actively exploited vulnerabilities are for Microsoft Exchange and Excel, with the Exchange zero-day used as part of the Tianfu hacking contest. [...]
