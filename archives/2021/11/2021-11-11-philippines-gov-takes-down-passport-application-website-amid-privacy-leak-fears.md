Title: Philippines gov takes down passport application website amid privacy leak fears
Date: 2021-11-11T23:52:26+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-11-11-philippines-gov-takes-down-passport-application-website-amid-privacy-leak-fears

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/11/philippines_passport_portal_data_leak/){:target="_blank" rel="noopener"}

> Google searches reportedly produce applicants' personal information The Philippines' Department of Foreign Affairs (DFA) has disabled its online passport application tracker, citing a "data privacy issue" and hinting that information could have leaked.... [...]
