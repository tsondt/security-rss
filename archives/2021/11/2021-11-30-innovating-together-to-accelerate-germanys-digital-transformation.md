Title: Innovating together to accelerate Germany’s digital transformation
Date: 2021-11-30T08:00:00+00:00
Author: Wieland Holfelder
Category: GCP Security
Tags: Google Cloud;Google Cloud in Europe;Compliance;Identity & Security
Slug: 2021-11-30-innovating-together-to-accelerate-germanys-digital-transformation

[Source](https://cloud.google.com/blog/products/identity-security/co-innovation-for-sustainable-cloud-transformation-in-germany/){:target="_blank" rel="noopener"}

> At Google Cloud, we are committed to supporting the next wave of growth for Europe’s businesses and organizations. Germany is one of the largest and most connected global economies, and it is undergoing digital transformation enabled by the use of cloud services. To further support that transformation, we announced plans to invest approximately EUR 1 billion in cloud infrastructure and green energy in Germany by 2030. Organizations in Germany and across Europe need cloud solutions that meet their requirements for security, privacy, and digital sovereignty, without compromising on functionality or innovation. To help meet these requirements, we launched ‘ Cloud. [...]
