Title: Ukrainian cuffed, faces extradition to US for allegedly orchestrating Kaseya ransomware infection
Date: 2021-11-08T22:04:20+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-08-ukrainian-cuffed-faces-extradition-to-us-for-allegedly-orchestrating-kaseya-ransomware-infection

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/08/revil_ransomware_operators/){:target="_blank" rel="noopener"}

> American, European officials announce raft of arrests, indictments, sanctions, rewards In a major ransomware bust US and European authorities on Monday announced separate but related indictments and arrests linked to extortionware attacks on IT service provider Kaseya and other firms.... [...]
