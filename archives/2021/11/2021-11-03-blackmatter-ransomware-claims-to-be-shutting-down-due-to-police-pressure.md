Title: BlackMatter ransomware claims to be shutting down due to police pressure
Date: 2021-11-03T01:59:46-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-03-blackmatter-ransomware-claims-to-be-shutting-down-due-to-police-pressure

[Source](https://www.bleepingcomputer.com/news/security/blackmatter-ransomware-claims-to-be-shutting-down-due-to-police-pressure/){:target="_blank" rel="noopener"}

> The BlackMatter ransomware is allegedly shutting down its operation due to pressure from the authorities and recent law enforcement operations. [...]
