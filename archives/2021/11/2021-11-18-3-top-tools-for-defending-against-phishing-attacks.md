Title: 3 Top Tools for Defending Against Phishing Attacks
Date: 2021-11-18T18:49:37+00:00
Author: Justin Jett
Category: Threatpost
Tags: InfoSec Insider;Malware;Web Security
Slug: 2021-11-18-3-top-tools-for-defending-against-phishing-attacks

[Source](https://threatpost.com/tools-defending-phishing-attacks/176463/){:target="_blank" rel="noopener"}

> Phishing emails are now skating past traditional defenses. Justin Jett, director of audit and compliance at Plixer, discusses what to do about it. [...]
