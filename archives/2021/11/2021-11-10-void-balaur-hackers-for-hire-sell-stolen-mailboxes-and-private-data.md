Title: Void Balaur hackers-for-hire sell stolen mailboxes and private data
Date: 2021-11-10T19:31:12-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-10-void-balaur-hackers-for-hire-sell-stolen-mailboxes-and-private-data

[Source](https://www.bleepingcomputer.com/news/security/void-balaur-hackers-for-hire-sell-stolen-mailboxes-and-private-data/){:target="_blank" rel="noopener"}

> A hacker-for-hire group called Void Balaur has been stealing emails and highly-sensitive information for more than five years, selling it to customers with both financial and espionage goals [...]
