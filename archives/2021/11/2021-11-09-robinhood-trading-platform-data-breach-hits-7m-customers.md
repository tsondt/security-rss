Title: Robinhood Trading Platform Data Breach Hits 7M Customers
Date: 2021-11-09T14:43:08+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Web Security
Slug: 2021-11-09-robinhood-trading-platform-data-breach-hits-7m-customers

[Source](https://threatpost.com/robinhood-trading-platform-data-breach/176106/){:target="_blank" rel="noopener"}

> The cyberattacker attempted to extort the company after socially engineering a customer service employee to gain access to email addresses and more. [...]
