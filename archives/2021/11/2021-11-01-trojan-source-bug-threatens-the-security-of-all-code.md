Title: ‘Trojan Source’ Bug Threatens the Security of All Code
Date: 2021-11-01T04:23:36+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Time to Patch;Bidi override;Cambridge University;CVE-2021-42574;CVE-2021-42694;Johns Hopkins Information Security Institute;Matthew Green;Nicholas Weaver;Ross Anderson;Rust;Trojan Source bug;University of California Berkeley
Slug: 2021-11-01-trojan-source-bug-threatens-the-security-of-all-code

[Source](https://krebsonsecurity.com/2021/11/trojan-source-bug-threatens-the-security-of-all-code/){:target="_blank" rel="noopener"}

> Virtually all compilers — programs that transform human-readable source code into computer-executable machine code — are vulnerable to an insidious attack in which an adversary can introduce targeted vulnerabilities into any software without being detected, new research released today warns. The vulnerability disclosure was coordinated with multiple organizations, some of whom are now releasing updates to address the security weakness. Researchers with the University of Cambridge discovered a bug that affects most computer code compilers and many software development environments. At issue is a component of the digital text encoding standard Unicode, which allows computers to exchange information regardless of [...]
