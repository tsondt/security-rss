Title: Max Schrems hits Irish Data Protection Commissioner with corruption complaint
Date: 2021-11-24T15:05:14+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2021-11-24-max-schrems-hits-irish-data-protection-commissioner-with-corruption-complaint

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/24/max_schrems_files_corruption_complaint/){:target="_blank" rel="noopener"}

> Watchdog argues 'fairness' in process should keep some documents confidential Data privacy campaign group noyb, founded by Austrian lawyer Max Schrems, has filed a complaint with the Austrian Office for the Prosecution of Corruption ( WKStA ) for a potential violation of Austrian criminal laws by the Irish Data Protection Commission.... [...]
