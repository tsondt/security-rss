Title: Tor Project unveils plans to route device traffic through Tor anonymity network with new VPN-like service
Date: 2021-11-18T16:20:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-18-tor-project-unveils-plans-to-route-device-traffic-through-tor-anonymity-network-with-new-vpn-like-service

[Source](https://portswigger.net/daily-swig/tor-project-unveils-plans-to-route-device-traffic-through-tor-anonymity-network-with-new-vpn-like-service){:target="_blank" rel="noopener"}

> VPN+ tech detailed during annual ‘State of the Onion’ update [...]
