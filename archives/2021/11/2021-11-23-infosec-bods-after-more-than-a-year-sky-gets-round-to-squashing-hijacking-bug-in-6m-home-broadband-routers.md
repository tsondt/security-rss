Title: Infosec bods: After more than a year, Sky gets round to squashing hijacking bug in 6m home broadband routers
Date: 2021-11-23T07:31:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-11-23-infosec-bods-after-more-than-a-year-sky-gets-round-to-squashing-hijacking-bug-in-6m-home-broadband-routers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/23/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: DNS cache poisoning again, cops probe property conveyancing group's IT outage, Azure hole addressed, and more In brief Sky has fixed a flaw in six million of its home broadband routers, and it only took the British broadcaster'n'telecoms giant a year to do so, infosec researchers have said.... [...]
