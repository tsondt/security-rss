Title: SMS About Bank Fraud as a Pretext for Voice Phishing
Date: 2021-11-10T21:12:03+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Kris Stevens;smishing;voice phishing
Slug: 2021-11-10-sms-about-bank-fraud-as-a-pretext-for-voice-phishing

[Source](https://krebsonsecurity.com/2021/11/sms-about-bank-fraud-as-a-pretext-for-voice-phishing/){:target="_blank" rel="noopener"}

> Most of us have probably heard the term “smishing” — which is a portmanteau for traditional ph ishing scams sent through SMS text messages. Smishing messages usually include a link to a site that spoofs a popular bank and tries to siphon personal information. But increasingly, phishers are turning to a hybrid form of smishing — blasting out linkless text messages about suspicious bank transfers as a pretext for immediately calling and scamming anyone who responds via text. KrebsOnSecurity recently heard from a reader who said his daughter received an SMS that said it was from her bank, and inquired [...]
