Title: Web trust dies in darkness: Hidden Certificate Authorities undermine public crypto infrastructure
Date: 2021-11-19T04:00:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-19-web-trust-dies-in-darkness-hidden-certificate-authorities-undermine-public-crypto-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/19/web_trust_certificates/){:target="_blank" rel="noopener"}

> Boffins measure the black hole of dubious certs and find it troubling Security researchers have checked the web's public key infrastructure and have measured a long-known but little-analyzed security threat: hidden root Certificate Authorities.... [...]
