Title: Yes, ransomware is your number one security nightmare. But here’s how to sleep easy
Date: 2021-11-24T07:30:04+00:00
Author: Keith Hale, Regional Director Nordics for Encryption products at Thales
Category: The Register
Tags: 
Slug: 2021-11-24-yes-ransomware-is-your-number-one-security-nightmare-but-heres-how-to-sleep-easy

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/24/ransomware_is_your_security_nightmare/){:target="_blank" rel="noopener"}

> Here’s a clue... it involves encryption Advertorial It may have escaped your notice, but last month was Cybersecurity Awareness month, and this year’s theme is “Do Your Part. Be #CyberSmart”.... [...]
