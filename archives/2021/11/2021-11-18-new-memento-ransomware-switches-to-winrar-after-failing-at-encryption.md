Title: New Memento ransomware switches to WinRar after failing at encryption
Date: 2021-11-18T11:42:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-18-new-memento-ransomware-switches-to-winrar-after-failing-at-encryption

[Source](https://www.bleepingcomputer.com/news/security/new-memento-ransomware-switches-to-winrar-after-failing-at-encryption/){:target="_blank" rel="noopener"}

> A new ransomware group called Memento takes the unusual approach of locking files inside password-protected archives after their encryption method kept being detected by security software. [...]
