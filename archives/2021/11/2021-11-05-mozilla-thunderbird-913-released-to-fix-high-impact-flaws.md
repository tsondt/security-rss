Title: Mozilla Thunderbird 91.3 released to fix high impact flaws
Date: 2021-11-05T09:47:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-05-mozilla-thunderbird-913-released-to-fix-high-impact-flaws

[Source](https://www.bleepingcomputer.com/news/security/mozilla-thunderbird-913-released-to-fix-high-impact-flaws/){:target="_blank" rel="noopener"}

> ​Mozilla released Thunderbird 91.3 to fix several high-impact vulnerabilities that can cause a denial of service, spoof the origin, bypass security policies, and allow arbitrary code execution. [...]
