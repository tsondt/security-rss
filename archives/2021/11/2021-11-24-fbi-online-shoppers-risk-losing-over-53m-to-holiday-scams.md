Title: FBI: Online shoppers risk losing over $53M to holiday scams
Date: 2021-11-24T12:13:04-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-24-fbi-online-shoppers-risk-losing-over-53m-to-holiday-scams

[Source](https://www.bleepingcomputer.com/news/security/fbi-online-shoppers-risk-losing-over-53m-to-holiday-scams/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned today that online shoppers risk losing more than $53 million during this year's holiday season to scams promising bargains and hard-to-find gifts. [...]
