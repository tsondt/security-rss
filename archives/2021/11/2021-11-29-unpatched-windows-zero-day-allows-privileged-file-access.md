Title: Unpatched Windows Zero-Day Allows Privileged File Access
Date: 2021-11-29T17:47:10+00:00
Author: Tara Seals
Category: Threatpost
Tags: Privacy;Vulnerabilities
Slug: 2021-11-29-unpatched-windows-zero-day-allows-privileged-file-access

[Source](https://threatpost.com/unpatched-windows-zero-day-privileged-file-access/176609/){:target="_blank" rel="noopener"}

> A temporary fix has been issued for CVE-2021-24084, which can be exploited using the LPE exploitation approach for the HiveNightmare/SeriousSAM bug. [...]
