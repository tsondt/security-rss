Title: Security Tool Guts: How Much Should Customers See?
Date: 2021-11-09T15:52:51+00:00
Author: Yaron Kassner
Category: Threatpost
Tags: InfoSec Insider;Malware;Web Security
Slug: 2021-11-09-security-tool-guts-how-much-should-customers-see

[Source](https://threatpost.com/security-tool-transparency/176113/){:target="_blank" rel="noopener"}

> Yaron Kassner, CTO of Silverfort, delves into the pros and cons of transparency when it comes to cybersecurity tools’ algorithms. [...]
