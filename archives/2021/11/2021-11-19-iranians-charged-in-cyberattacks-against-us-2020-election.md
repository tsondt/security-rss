Title: Iranians Charged in Cyberattacks Against U.S. 2020 Election
Date: 2021-11-19T19:49:31+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government
Slug: 2021-11-19-iranians-charged-in-cyberattacks-against-us-2020-election

[Source](https://threatpost.com/iranians-charged-cyberattacks-2020-election/176488/){:target="_blank" rel="noopener"}

> The State Department has offered a $10M reward for tips on the two Iran-based threat actors accused of voter intimidation and disinformation. [...]
