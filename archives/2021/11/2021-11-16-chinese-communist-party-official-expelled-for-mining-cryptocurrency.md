Title: Chinese Communist Party official expelled for mining cryptocurrency
Date: 2021-11-16T00:49:34+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-16-chinese-communist-party-official-expelled-for-mining-cryptocurrency

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/16/china_cryptomining_expulsion/){:target="_blank" rel="noopener"}

> Middle Kingdom floats fresh data security rules, too, with eight-hour privacy breach notification requirement China’s Central Commission for Discipline Inspection has expelled a communist party member for allowing cryptocurrency mining to happen, corruption, and other infractions.... [...]
