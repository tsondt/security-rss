Title: Implement OAuth 2.0 device grant flow by using Amazon Cognito and AWS Lambda
Date: 2021-11-02T17:49:00+00:00
Author: Jeff Lombardo
Category: AWS Security
Tags: Amazon Cognito;Amazon DynamoDB;AWS Lambda;Expert (400);Security, Identity, & Compliance;AWS IoT;Device grant;Identity;OAuth2;OIDC;Security Blog
Slug: 2021-11-02-implement-oauth-20-device-grant-flow-by-using-amazon-cognito-and-aws-lambda

[Source](https://aws.amazon.com/blogs/security/implement-oauth-2-0-device-grant-flow-by-using-amazon-cognito-and-aws-lambda/){:target="_blank" rel="noopener"}

> In this blog post, you’ll learn how to implement the OAuth 2.0 device authorization grant flow for Amazon Cognito by using AWS Lambda and Amazon DynamoDB. When you implement the OAuth 2.0 authorization framework (RFC 6749) for internet-connected devices with limited input capabilities or that lack a user-friendly browser—such as wearables, smart assistants, video-streaming devices, [...]
