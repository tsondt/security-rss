Title: Beijing lashes USA's China Telecom ban – but quite gently
Date: 2021-11-04T01:57:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-04-beijing-lashes-usas-china-telecom-ban-but-quite-gently

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/04/beijing_lashes_usas_china_telecom/){:target="_blank" rel="noopener"}

> Rolls out usual lines about national security being pretext for competitive action, more strident voices keep quiet China's Ministry of Industry and Information Technology has responded with mild indignation to the USA's decision to revoke the operating licence that allowed China Telcom to operate in the land of the free.... [...]
