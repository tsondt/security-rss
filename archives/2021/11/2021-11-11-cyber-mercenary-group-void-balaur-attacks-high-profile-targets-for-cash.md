Title: Cyber-Mercenary Group Void Balaur Attacks High-Profile Targets for Cash
Date: 2021-11-11T18:48:06+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Government;Hacks;Malware
Slug: 2021-11-11-cyber-mercenary-group-void-balaur-attacks-high-profile-targets-for-cash

[Source](https://threatpost.com/cyber-mercenary-void-balaur/176230/){:target="_blank" rel="noopener"}

> A Russian-language threat group is available for hire, to steal data on journalists, political leaders, activists and from organizations in every sector. [...]
