Title: FBI warns of Iranian hackers looking to buy US orgs’ stolen data
Date: 2021-11-10T16:30:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-10-fbi-warns-of-iranian-hackers-looking-to-buy-us-orgs-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-iranian-hackers-looking-to-buy-us-orgs-stolen-data/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned private industry partners of attempts by an Iranian threat actor to buy stolen information regarding US and worldwide organizations. [...]
