Title: NIST unveils draft criteria for ‘seal of approval’ scheme on consumer software security
Date: 2021-11-02T16:47:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-02-nist-unveils-draft-criteria-for-seal-of-approval-scheme-on-consumer-software-security

[Source](https://portswigger.net/daily-swig/nist-unveils-draft-criteria-for-seal-of-approval-scheme-on-consumer-software-security){:target="_blank" rel="noopener"}

> Baseline standards proposed for secure development, handling vulnerabilities, and protecting sensitive data [...]
