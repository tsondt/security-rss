Title: New Rowhammer Technique
Date: 2021-11-19T14:31:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hardware;side-channel attacks
Slug: 2021-11-19-new-rowhammer-technique

[Source](https://www.schneier.com/blog/archives/2021/11/new-rowhammer-technique.html){:target="_blank" rel="noopener"}

> Rowhammer is an attack technique involving accessing — that’s “hammering” — rows of bits in memory, millions of times per second, with the intent of causing bits in neighboring rows to flip. This is a side-channel attack, and the result can be all sorts of mayhem. Well, there is a new enhancement: All previous Rowhammer attacks have hammered rows with uniform patterns, such as single-sided, double-sided, or n-sided. In all three cases, these “aggressor” rows — meaning those that cause bitflips in nearby “victim” rows — are accessed the same number of times. Research published on Monday presented a new [...]
