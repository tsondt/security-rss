Title: FBI seized $2.2M from affiliate of REvil, Gandcrab ransomware gangs
Date: 2021-11-30T16:46:32-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-30-fbi-seized-22m-from-affiliate-of-revil-gandcrab-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/fbi-seized-22m-from-affiliate-of-revil-gandcrab-ransomware-gangs/){:target="_blank" rel="noopener"}

> The FBI seized $2.2 million in August from a well-known REvil and GandCrab ransomware affiliate, according to court documents seen by BleepingComputer. [...]
