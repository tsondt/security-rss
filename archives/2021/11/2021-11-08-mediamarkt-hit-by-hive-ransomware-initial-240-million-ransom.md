Title: MediaMarkt hit by Hive ransomware, initial $240 million ransom
Date: 2021-11-08T09:27:49-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-08-mediamarkt-hit-by-hive-ransomware-initial-240-million-ransom

[Source](https://www.bleepingcomputer.com/news/security/mediamarkt-hit-by-hive-ransomware-initial-240-million-ransom/){:target="_blank" rel="noopener"}

> Electronics retail giant MediaMarkt has suffered a Hive ransomware with an initial ransom demand of $240 million, causing IT systems to shut down and store operations to be disrupted in Netherlands and Germany. [...]
