Title: The Five Ws episode 2: Data Classification whitepaper
Date: 2021-11-01T20:14:49+00:00
Author: Jana Kay
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;cloud adoption;data classification;Security Blog;The Five Ws;Whitepaper
Slug: 2021-11-01-the-five-ws-episode-2-data-classification-whitepaper

[Source](https://aws.amazon.com/blogs/security/the-five-ws-episode-2-data-classification-whitepaper/){:target="_blank" rel="noopener"}

> AWS whitepapers are a great way to expand your knowledge of the cloud. Authored by Amazon Web Services (AWS) and the AWS community, they provide in-depth content that often addresses specific customer situations. We’re featuring some of our whitepapers in a new video series, The Five Ws. These short videos outline the who, what, when, [...]
