Title: Campaigning lawyers launch counter-offensive against software patent trolls
Date: 2021-11-08T14:34:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-08-campaigning-lawyers-launch-counter-offensive-against-software-patent-trolls

[Source](https://portswigger.net/daily-swig/campaigning-lawyers-launch-counter-offensive-against-software-patent-trolls){:target="_blank" rel="noopener"}

> Stemming the tide of ‘stupid software patents and the trolls they feed’ [...]
