Title: BlackMatter ransomware moves victims to LockBit after shutdown
Date: 2021-11-03T12:47:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-03-blackmatter-ransomware-moves-victims-to-lockbit-after-shutdown

[Source](https://www.bleepingcomputer.com/news/security/blackmatter-ransomware-moves-victims-to-lockbit-after-shutdown/){:target="_blank" rel="noopener"}

> With the BlackMatter ransomware operation shutting down, existing affiliates are moving their victims to the competing LockBit ransomware site for continued extortion. [...]
