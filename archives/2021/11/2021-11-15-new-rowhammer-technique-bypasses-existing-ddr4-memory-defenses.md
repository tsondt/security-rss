Title: New Rowhammer technique bypasses existing DDR4 memory defenses
Date: 2021-11-15T17:27:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2021-11-15-new-rowhammer-technique-bypasses-existing-ddr4-memory-defenses

[Source](https://www.bleepingcomputer.com/news/security/new-rowhammer-technique-bypasses-existing-ddr4-memory-defenses/){:target="_blank" rel="noopener"}

> Researchers have developed a new fuzzing-based technique called 'Blacksmith' that revives Rowhammer vulnerability attacks against modern DRAM devices that bypasses existing mitigations. [...]
