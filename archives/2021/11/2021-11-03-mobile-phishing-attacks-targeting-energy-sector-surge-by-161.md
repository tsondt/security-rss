Title: Mobile phishing attacks targeting energy sector surge by 161%
Date: 2021-11-03T10:28:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-11-03-mobile-phishing-attacks-targeting-energy-sector-surge-by-161

[Source](https://www.bleepingcomputer.com/news/security/mobile-phishing-attacks-targeting-energy-sector-surge-by-161-percent/){:target="_blank" rel="noopener"}

> Mobile phishing attacks targeting employees in the energy industry have risen by 161% compared to last year's (H2 2020) data, and the trend is showing no signs of slowing down. [...]
