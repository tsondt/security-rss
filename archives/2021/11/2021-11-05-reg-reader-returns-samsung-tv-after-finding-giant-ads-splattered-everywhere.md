Title: <i>Reg</i> reader returns Samsung TV after finding giant ads splattered everywhere
Date: 2021-11-05T20:10:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-05-reg-reader-returns-samsung-tv-after-finding-giant-ads-splattered-everywhere

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/05/samsung_tv_ads/){:target="_blank" rel="noopener"}

> Even your telly is now a moneymaking gadget for someone else A Register reader triggered a kerfuffle for Samsung after asking the electronics biz if he could disable large and intrusive adverts splattered across his new smart TV's programme guide.... [...]
