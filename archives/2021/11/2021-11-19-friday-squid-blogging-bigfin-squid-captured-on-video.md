Title: Friday Squid Blogging: Bigfin Squid Captured on Video
Date: 2021-11-19T22:12:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-11-19-friday-squid-blogging-bigfin-squid-captured-on-video

[Source](https://www.schneier.com/blog/archives/2021/11/friday-squid-blogging-bigfin-squid-captured-on-video.html){:target="_blank" rel="noopener"}

> “ Eerie video captures elusive, alien-like squid gliding in the Gulf of Mexico.” As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
