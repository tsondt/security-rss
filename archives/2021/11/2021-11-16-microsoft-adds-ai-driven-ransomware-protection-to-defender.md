Title: Microsoft adds AI-driven ransomware protection to Defender
Date: 2021-11-16T10:31:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-16-microsoft-adds-ai-driven-ransomware-protection-to-defender

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-adds-ai-driven-ransomware-protection-to-defender/){:target="_blank" rel="noopener"}

> Microsoft has introduced an AI-driven ransomware attack detection system for Microsoft Defender for Endpoint customers that complements existing cloud protection by evaluating risks and blocking actors at the perimeter. [...]
