Title: NUCLEUS:13 TCP security bugs impact critical healthcare devices
Date: 2021-11-09T19:46:29-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-09-nucleus13-tcp-security-bugs-impact-critical-healthcare-devices

[Source](https://www.bleepingcomputer.com/news/security/nucleus-13-tcp-security-bugs-impact-critical-healthcare-devices/){:target="_blank" rel="noopener"}

> Researchers today published details about a suite of 13 vulnerabilities in the Nucleus real-time operating system (RTOS) from Siemens that powers devices used in the medical, industrial, automotive, and aerospace sectors. [...]
