Title: Computer misuse crimes in UK surge to high not seen since 2017 even as prosecutions slump 20%
Date: 2021-11-08T07:31:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-08-computer-misuse-crimes-in-uk-surge-to-high-not-seen-since-2017-even-as-prosecutions-slump-20

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/08/computer_misuse_crimes_ons_crime_survey/){:target="_blank" rel="noopener"}

> COVID didn't stop crooks, but law enforcement doesn't seem to have realised Public reports of computer-linked crimes are soaring thanks to a huge rise in data breaches, even as prosecutions against Computer Misuse Act offenders slump.... [...]
