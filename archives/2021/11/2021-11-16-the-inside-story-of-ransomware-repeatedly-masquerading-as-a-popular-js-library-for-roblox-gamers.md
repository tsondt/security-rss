Title: The inside story of ransomware repeatedly masquerading as a popular JS library for Roblox gamers
Date: 2021-11-16T21:46:54+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-16-the-inside-story-of-ransomware-repeatedly-masquerading-as-a-popular-js-library-for-roblox-gamers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/16/nobloxjs_typosquatting_discord/){:target="_blank" rel="noopener"}

> Ongoing typosquatting attacks target kids as Discord drags its feet Since early September, Josh Muir and five other maintainers of the noblox.js package, have been trying to prevent cybercriminals from distributing ransomware through similarly named code libraries.... [...]
