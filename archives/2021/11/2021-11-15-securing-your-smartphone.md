Title: Securing Your Smartphone
Date: 2021-11-15T14:18:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;phishing;risk assessment;security analysis;smartphones;threat models
Slug: 2021-11-15-securing-your-smartphone

[Source](https://www.schneier.com/blog/archives/2021/11/securing-your-smartphone.html){:target="_blank" rel="noopener"}

> This is part 3 of Sean Gallagher’s advice for “securing your digital life.” [...]
