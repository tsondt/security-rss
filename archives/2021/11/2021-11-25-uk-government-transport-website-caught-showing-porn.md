Title: UK government transport website caught showing porn
Date: 2021-11-25T14:33:23-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-11-25-uk-government-transport-website-caught-showing-porn

[Source](https://www.bleepingcomputer.com/news/security/uk-government-transport-website-caught-showing-porn/){:target="_blank" rel="noopener"}

> A UK Department for Transport (DfT) website was caught serving porn earlier today. The particular DfT subdomain behind the mishap, on most days, provides vital DfT statistics for the public and the department's business plan. [...]
