Title: The New Frontier of Enterprise Risk: Nth Parties
Date: 2021-11-09T16:01:59+00:00
Author: Threatpost
Category: Threatpost
Tags: Cloud Security;Sponsored;Web Security
Slug: 2021-11-09-the-new-frontier-of-enterprise-risk-nth-parties

[Source](https://threatpost.com/enterprise-risk-nth-parties/176114/){:target="_blank" rel="noopener"}

> The average number of vulnerabilities discovered in a Cyberpion scan of external Fortune 500 networks (such as cloud systems) was 296, many critical (with the top of the scale weighing in at a staggering 7,500). [...]
