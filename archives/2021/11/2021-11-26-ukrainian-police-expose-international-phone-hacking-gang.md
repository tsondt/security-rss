Title: Ukrainian police expose international phone-hacking gang
Date: 2021-11-26T15:36:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-26-ukrainian-police-expose-international-phone-hacking-gang

[Source](https://portswigger.net/daily-swig/ukrainian-police-expose-international-phone-hacking-gang){:target="_blank" rel="noopener"}

> ‘Phoenix’ group laid low following seizure of computing equipment and stolen devices [...]
