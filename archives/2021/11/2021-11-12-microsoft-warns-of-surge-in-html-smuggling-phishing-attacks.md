Title: Microsoft warns of surge in HTML smuggling phishing attacks
Date: 2021-11-12T10:27:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-12-microsoft-warns-of-surge-in-html-smuggling-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-surge-in-html-smuggling-phishing-attacks/){:target="_blank" rel="noopener"}

> Microsoft has seen a surge in malware campaigns using HTML smuggling to distribute banking malware and remote access trojans (RAT). [...]
