Title: EU needs more cybersecurity graduates, says ENISA infosec agency – pointing at growing list of master's degree courses
Date: 2021-11-26T16:37:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-26-eu-needs-more-cybersecurity-graduates-says-enisa-infosec-agency-pointing-at-growing-list-of-masters-degree-courses

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/26/enisa_cybersecurity_degrees_report/){:target="_blank" rel="noopener"}

> Skills gap needs filling somehow The EU needs more cybersecurity graduates to plug the political bloc's shortage of skilled infosec bods, according to a report from the ENISA online security agency.... [...]
