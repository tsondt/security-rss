Title: Magecart Credit Card Skimmer Avoids VMs to Fly Under the Radar
Date: 2021-11-04T12:51:48+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-11-04-magecart-credit-card-skimmer-avoids-vms-to-fly-under-the-radar

[Source](https://threatpost.com/magecart-credit-card-skimmer-avoids-vms-to-fly-under-the-radar/175993/){:target="_blank" rel="noopener"}

> The Magecart threat actor uses a browser script to evade detection by researchers and sandboxes so it targets only victims’ machines to steal credentials and personal info. [...]
