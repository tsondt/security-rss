Title: Is Microsoft Stealing People’s Bookmarks?
Date: 2021-11-17T13:53:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;browsers;Microsoft;privacy;web privacy
Slug: 2021-11-17-is-microsoft-stealing-peoples-bookmarks

[Source](https://www.schneier.com/blog/archives/2021/11/is-microsoft-stealing-peoples-bookmarks.html){:target="_blank" rel="noopener"}

> I received email from two people who told me that Microsoft Edge enabled synching without warning or consent, which means that Microsoft sucked up all of their bookmarks. Of course they can turn synching off, but it’s too late. Has this happened to anyone else, or was this user error of some sort? If this is real, can some reporter write about it? (Not that “user error” is a good justification. Any system where making a simple mistake means that you’ve forever lost your privacy isn’t a good one. We see this same situation with sharing contact lists with apps [...]
