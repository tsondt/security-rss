Title: Vulnerabilities in NPM allowed threat actors to publish new version of any package
Date: 2021-11-17T14:32:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-17-vulnerabilities-in-npm-allowed-threat-actors-to-publish-new-version-of-any-package

[Source](https://portswigger.net/daily-swig/vulnerabilities-in-npm-allowed-threat-actors-to-publish-new-version-of-any-package){:target="_blank" rel="noopener"}

> Details of flaws were made public this week [...]
