Title: Google advises passwords are good, spear phishing is bad, and free clouds get attacked
Date: 2021-11-25T06:59:22+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-25-google-advises-passwords-are-good-spear-phishing-is-bad-and-free-clouds-get-attacked

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/25/google_cybersecurity_action_team_threat_horizons/){:target="_blank" rel="noopener"}

> Ad giant's first stab at providing the 'world's premier security advisory' starts with the obvious Google's Cybersecurity Action Team has released its first "threat horizon" report on the scary things it's found on the internet.... [...]
