Title: Fake TSA PreCheck sites scam US travelers with fake renewals
Date: 2021-11-19T11:32:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-19-fake-tsa-precheck-sites-scam-us-travelers-with-fake-renewals

[Source](https://www.bleepingcomputer.com/news/security/fake-tsa-precheck-sites-scam-us-travelers-with-fake-renewals/){:target="_blank" rel="noopener"}

> There has been a surge in reports of people getting scammed after visiting TSA PreCheck, Global Entry, and NEXUS application service sites, being charged $140 only to get nothing in return. [...]
