Title: PhoneSpy: Android spyware campaign targeting South Korean users
Date: 2021-11-10T09:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-11-10-phonespy-android-spyware-campaign-targeting-south-korean-users

[Source](https://www.bleepingcomputer.com/news/security/phonespy-android-spyware-campaign-targeting-south-korean-users/){:target="_blank" rel="noopener"}

> An ongoing spyware campaign dubbed 'PhoneSpy' targets South Korean users via a range of lifestyle apps that nest in the device and silently exfiltrate data. [...]
