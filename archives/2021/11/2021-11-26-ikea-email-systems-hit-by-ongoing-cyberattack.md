Title: IKEA email systems hit by ongoing cyberattack
Date: 2021-11-26T15:41:42-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-26-ikea-email-systems-hit-by-ongoing-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/ikea-email-systems-hit-by-ongoing-cyberattack/){:target="_blank" rel="noopener"}

> IKEA is battling an ongoing cyberattack where threat actors are targeting employees in internal phishing attacks using stolen reply-chain emails. [...]
