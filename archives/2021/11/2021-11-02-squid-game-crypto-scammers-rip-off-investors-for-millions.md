Title: Squid Game Crypto Scammers Rip Off Investors for Millions
Date: 2021-11-02T20:55:33+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cryptography;Web Security
Slug: 2021-11-02-squid-game-crypto-scammers-rip-off-investors-for-millions

[Source](https://threatpost.com/squid-game-crypto-scammers-investors/175951/){:target="_blank" rel="noopener"}

> Anti-dumping code kept investors from selling SQUID while fraudsters cashed out. [...]
