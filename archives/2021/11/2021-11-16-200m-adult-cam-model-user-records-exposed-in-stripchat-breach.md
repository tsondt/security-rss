Title: 200M Adult Cam Model, User Records Exposed in Stripchat Breach
Date: 2021-11-16T20:32:16+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Privacy;Web Security
Slug: 2021-11-16-200m-adult-cam-model-user-records-exposed-in-stripchat-breach

[Source](https://threatpost.com/adult-cam-model-user-records-exposed-stripchat-breach/176372/){:target="_blank" rel="noopener"}

> The leak included model information, chat messages and payment details. [...]
