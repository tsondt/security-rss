Title: AMD fixes dozens of Windows 10 graphics driver security bugs
Date: 2021-11-11T13:13:41-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-11-amd-fixes-dozens-of-windows-10-graphics-driver-security-bugs

[Source](https://www.bleepingcomputer.com/news/security/amd-fixes-dozens-of-windows-10-graphics-driver-security-bugs/){:target="_blank" rel="noopener"}

> AMD has fixed a long list of security vulnerabilities found in its graphics driver for Windows 10 devices, allowing attackers to execute arbitrary code and elevate privileges on vulnerable systems. [...]
