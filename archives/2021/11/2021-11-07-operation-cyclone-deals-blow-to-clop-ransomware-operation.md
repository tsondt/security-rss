Title: Operation Cyclone deals blow to Clop ransomware operation
Date: 2021-11-07T11:46:27-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-07-operation-cyclone-deals-blow-to-clop-ransomware-operation

[Source](https://www.bleepingcomputer.com/news/security/operation-cyclone-deals-blow-to-clop-ransomware-operation/){:target="_blank" rel="noopener"}

> A thirty-month international law enforcement operation codenamed 'Operation Cyclone' targeted the Clop ransomware gang, leading to the previously reported arrests of six members in Ukraine. [...]
