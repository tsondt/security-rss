Title: Lockean multi-ransomware affiliates linked to attacks on French orgs
Date: 2021-11-04T07:22:01-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-04-lockean-multi-ransomware-affiliates-linked-to-attacks-on-french-orgs

[Source](https://www.bleepingcomputer.com/news/security/lockean-multi-ransomware-affiliates-linked-to-attacks-on-french-orgs/){:target="_blank" rel="noopener"}

> Details about the tools and tactics used by a ransomware affiliate group, now tracked as Lockean, have emerged today in a report from France's Computer Emergency Response Team (CERT). [...]
