Title: Ransomware cybercriminals linked to Norsk Hydro attack fall prey to Europol swoop
Date: 2021-11-01T14:17:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-01-ransomware-cybercriminals-linked-to-norsk-hydro-attack-fall-prey-to-europol-swoop

[Source](https://portswigger.net/daily-swig/ransomware-cybercriminals-linked-to-norsk-hydro-attack-fall-prey-to-europol-swoop){:target="_blank" rel="noopener"}

> Two-year investigation results in raids targeting ‘high-value’ suspects and seizure of cash and computers [...]
