Title: Panasonic’s Data Breach Leaves Open Questions
Date: 2021-11-30T17:56:03+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;Privacy
Slug: 2021-11-30-panasonics-data-breach-leaves-open-questions

[Source](https://threatpost.com/panasonic-data-breach-questions/176660/){:target="_blank" rel="noopener"}

> Cyberattackers had unfettered access to the technology giant's file server for four months. [...]
