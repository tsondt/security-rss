Title: UK Department for Transport caught inadvertently serving pornographic content to site visitors
Date: 2021-11-29T12:32:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-29-uk-department-for-transport-caught-inadvertently-serving-pornographic-content-to-site-visitors

[Source](https://portswigger.net/daily-swig/uk-department-for-transport-caught-inadvertently-serving-pornographic-content-to-site-visitors){:target="_blank" rel="noopener"}

> ‘The page has since been permanently deleted’, a government spokesperson told The Daily Swig [...]
