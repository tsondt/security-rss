Title: FBI warns of increased use of cryptocurrency ATMs, QR codes for fraud
Date: 2021-11-05T09:55:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-11-05-fbi-warns-of-increased-use-of-cryptocurrency-atms-qr-codes-for-fraud

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-increased-use-of-cryptocurrency-atms-qr-codes-for-fraud/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warns that victims of various fraud schemes are increasingly asked by criminals to use cryptocurrency ATMs and Quick Response (QR) codes, making it harder to recover their financial losses. [...]
