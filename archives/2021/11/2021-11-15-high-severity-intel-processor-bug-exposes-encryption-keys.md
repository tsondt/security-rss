Title: High-Severity Intel Processor Bug Exposes Encryption Keys
Date: 2021-11-15T20:52:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-11-15-high-severity-intel-processor-bug-exposes-encryption-keys

[Source](https://threatpost.com/intel-processor-bug-encryption-keys/176355/){:target="_blank" rel="noopener"}

> CVE-2021-0146, arising from a debugging functionality with excessive privileges, allows attackers to read encrypted files. [...]
