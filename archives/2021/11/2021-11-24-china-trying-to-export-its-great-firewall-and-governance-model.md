Title: China trying to export its Great Firewall and governance model
Date: 2021-11-24T02:56:03+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-24-china-trying-to-export-its-great-firewall-and-governance-model

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/24/aspi_chinese_internet_governance_report/){:target="_blank" rel="noopener"}

> Beware of Communists bearing internet governance proposals, says Australian Strategic Policy Institute China is actively trying to export its internal internet governance model, according to a paper from the International Cyber Policy Centre at the Australian Strategic Policy Institute.... [...]
