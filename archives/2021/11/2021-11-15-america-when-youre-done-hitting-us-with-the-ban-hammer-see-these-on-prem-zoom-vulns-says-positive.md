Title: America, when you're done hitting us with the ban hammer, see these on-prem Zoom vulns, says Positive
Date: 2021-11-15T20:27:33+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-15-america-when-youre-done-hitting-us-with-the-ban-hammer-see-these-on-prem-zoom-vulns-says-positive

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/15/positive_zoom_flaw/){:target="_blank" rel="noopener"}

> Now would be a good idea to check you're up-to-date US-sanctioned Positive Technologies has pointed out three vulnerabilities in Zoom that can be exploited to crash or hijack on-prem instances of the videoconferencing system.... [...]
