Title: Fake end-to-end encrypted chat app distributes Android spyware
Date: 2021-11-13T11:12:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-13-fake-end-to-end-encrypted-chat-app-distributes-android-spyware

[Source](https://www.bleepingcomputer.com/news/security/fake-end-to-end-encrypted-chat-app-distributes-android-spyware/){:target="_blank" rel="noopener"}

> The GravityRAT remote access trojan is being distributed in the wild again, this time under the guise of an end-to-end encrypted chat application called SoSafe Chat. [...]
