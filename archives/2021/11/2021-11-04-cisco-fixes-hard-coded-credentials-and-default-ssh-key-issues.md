Title: Cisco fixes hard-coded credentials and default SSH key issues
Date: 2021-11-04T13:24:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-04-cisco-fixes-hard-coded-credentials-and-default-ssh-key-issues

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-hard-coded-credentials-and-default-ssh-key-issues/){:target="_blank" rel="noopener"}

> Cisco has released security updates to address critical security flaws allowing unauthenticated attackers to log in using hard-coded credentials or default SSH keys to take over unpatched devices. [...]
