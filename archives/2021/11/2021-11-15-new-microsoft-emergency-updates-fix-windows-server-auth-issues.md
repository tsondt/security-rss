Title: New Microsoft emergency updates fix Windows Server auth issues
Date: 2021-11-15T04:35:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-15-new-microsoft-emergency-updates-fix-windows-server-auth-issues

[Source](https://www.bleepingcomputer.com/news/microsoft/new-microsoft-emergency-updates-fix-windows-server-auth-issues/){:target="_blank" rel="noopener"}

> Microsoft has released out-of-band updates to address authentication failures related to Kerberos delegation scenarios impacting Domain Controllers (DC) running supported versions of Windows Server. [...]
