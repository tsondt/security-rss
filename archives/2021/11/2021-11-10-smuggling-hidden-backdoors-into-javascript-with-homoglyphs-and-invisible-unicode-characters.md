Title: Smuggling hidden backdoors into JavaScript with homoglyphs and invisible Unicode characters
Date: 2021-11-10T16:31:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-10-smuggling-hidden-backdoors-into-javascript-with-homoglyphs-and-invisible-unicode-characters

[Source](https://portswigger.net/daily-swig/smuggling-hidden-backdoors-into-javascript-with-homoglyphs-and-invisible-unicode-characters){:target="_blank" rel="noopener"}

> Researchers urge developers to secure code by disallowing non-ASCII characters [...]
