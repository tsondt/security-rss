Title: NPM fixes private package names leak, serious authorization bug
Date: 2021-11-16T07:43:30-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-11-16-npm-fixes-private-package-names-leak-serious-authorization-bug

[Source](https://www.bleepingcomputer.com/news/security/npm-fixes-private-package-names-leak-serious-authorization-bug/){:target="_blank" rel="noopener"}

> The largest software registry of Node.js packages, npm, has disclosed fixing multiple security flaws. The first flaw concerns leak of names of private npm packages on the npmjs.com's "replica" server. Whereas, the second flaw allows attackers to publish new versions of any existing npm package that they do not own or have rights to. [...]
