Title: US Dept of Commerce sanctions NSO Group, Positive Technologies, other makers of snoopware
Date: 2021-11-03T20:50:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-03-us-dept-of-commerce-sanctions-nso-group-positive-technologies-other-makers-of-snoopware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/03/us_sanctions_spyware/){:target="_blank" rel="noopener"}

> Yeah, that ought to do the trick The US government's Dept of Commerce on Wednesday sanctioned four companies in Israel, Russia, and Singapore for selling software used to break into computer systems and by foreign governments to suppress dissent.... [...]
