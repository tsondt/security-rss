Title: New Windows 10 zero-day gives admin rights, gets unofficial patch
Date: 2021-11-27T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-11-27-new-windows-10-zero-day-gives-admin-rights-gets-unofficial-patch

[Source](https://www.bleepingcomputer.com/news/security/new-windows-10-zero-day-gives-admin-rights-gets-unofficial-patch/){:target="_blank" rel="noopener"}

> Free unofficial patches have been released to protect Windows users from a local privilege escalation (LPE) zero-day vulnerability in the Mobile Device Management Service impacting all Windows 10 versions from v1809 to v21H1. [...]
