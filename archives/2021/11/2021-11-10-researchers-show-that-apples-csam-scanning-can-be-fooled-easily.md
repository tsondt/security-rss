Title: Researchers show that Apple’s CSAM scanning can be fooled easily
Date: 2021-11-10T14:44:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Technology;Security
Slug: 2021-11-10-researchers-show-that-apples-csam-scanning-can-be-fooled-easily

[Source](https://www.bleepingcomputer.com/news/technology/researchers-show-that-apple-s-csam-scanning-can-be-fooled-easily/){:target="_blank" rel="noopener"}

> A team of researchers at the Imperial College in London have presented a simple method to evade detection by image content scanning mechanisms, such as Apple's CSAM. [...]
