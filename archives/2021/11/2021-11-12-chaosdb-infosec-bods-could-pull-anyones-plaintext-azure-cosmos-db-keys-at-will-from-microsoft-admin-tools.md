Title: ChaosDB: Infosec bods could pull anyone's plaintext Azure Cosmos DB keys at will from Microsoft admin tools
Date: 2021-11-12T19:19:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-12-chaosdb-infosec-bods-could-pull-anyones-plaintext-azure-cosmos-db-keys-at-will-from-microsoft-admin-tools

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/12/chaos_db_wiz_azure_cosmos_research_pwnage/){:target="_blank" rel="noopener"}

> And they had a wildcard cert too. Still feeling secure? Black Hat Europe An astonishing piece of vulnerability probing gave infosec researchers a way into to Microsoft's management controls for Azure Cosmos DB – with full read and write privileges over customer databases.... [...]
