Title: Anti-virus firm McAfee seems to be sending junk emails
Date: 2021-11-30T07:00:10+00:00
Author: Miles Brignall
Category: The Guardian
Tags: Money;Consumer rights;Consumer affairs;Viruses;Scams;Data and computer security;Malware;Technology;UK news
Slug: 2021-11-30-anti-virus-firm-mcafee-seems-to-be-sending-junk-emails

[Source](https://www.theguardian.com/money/2021/nov/30/anti-virus-firm-mcafee-seems-to-be-sending-junk-emails){:target="_blank" rel="noopener"}

> I received a flood of renewal demands and unsubscribing doesn’t work I cancelled my McAfee anti-virus subscription earlier this year when I discovered it had been double charging me. It refunded only the current year and led me on a wild goose chase to recover the previous two years ’ money; I eventually gave up. Now that my subscription period has ended, it is bombarding me with renewal demand emails several times a day. This weekend I received 15. Continue reading... [...]
