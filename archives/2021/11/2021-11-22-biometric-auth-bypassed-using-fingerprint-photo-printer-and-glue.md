Title: Biometric auth bypassed using fingerprint photo, printer, and glue
Date: 2021-11-22T12:10:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2021-11-22-biometric-auth-bypassed-using-fingerprint-photo-printer-and-glue

[Source](https://www.bleepingcomputer.com/news/security/biometric-auth-bypassed-using-fingerprint-photo-printer-and-glue/){:target="_blank" rel="noopener"}

> Researchers demonstrated that fingerprints could be cloned for biometric authentication for as little as $5 without using any sophisticated or uncommon tools. [...]
