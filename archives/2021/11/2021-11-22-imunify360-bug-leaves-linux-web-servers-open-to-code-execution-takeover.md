Title: Imunify360 Bug Leaves Linux Web Servers Open to Code Execution, Takeover
Date: 2021-11-22T19:14:11+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-11-22-imunify360-bug-leaves-linux-web-servers-open-to-code-execution-takeover

[Source](https://threatpost.com/linux-web-servers-imunify360-bug/176508/){:target="_blank" rel="noopener"}

> CloudLinux's security platform for Linux-based websites and web servers contains a high-severity PHP deserialization bug. [...]
