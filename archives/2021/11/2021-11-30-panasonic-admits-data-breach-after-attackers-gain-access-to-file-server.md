Title: Panasonic admits data breach after attackers gain access to file server
Date: 2021-11-30T14:31:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-30-panasonic-admits-data-breach-after-attackers-gain-access-to-file-server

[Source](https://portswigger.net/daily-swig/panasonic-admits-data-breach-after-attackers-gain-access-to-file-server){:target="_blank" rel="noopener"}

> Reports suggest that intrusion may have persisted for months [...]
