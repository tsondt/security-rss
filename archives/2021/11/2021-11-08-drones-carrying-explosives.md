Title: Drones Carrying Explosives
Date: 2021-11-08T12:03:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;assassinations;drones;Iran
Slug: 2021-11-08-drones-carrying-explosives

[Source](https://www.schneier.com/blog/archives/2021/11/drones-carrying-explosives.html){:target="_blank" rel="noopener"}

> We’ve now had an (unsuccessful) assassination attempt by explosive-laden drones. [...]
