Title: US Blacklists NSO Group
Date: 2021-11-04T11:52:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberweapons;national security policy;spyware;zero-day
Slug: 2021-11-04-us-blacklists-nso-group

[Source](https://www.schneier.com/blog/archives/2021/11/us-blacklists-nso-group.html){:target="_blank" rel="noopener"}

> The Israeli cyberweapons arms manufacturer — and human rights violator, and probably war criminal — NSO Group has been added to the US Department of Commerce’s trade blacklist. US companies and individuals cannot sell to them. Aside from the obvious difficulties this causes, it’ll make it harder for them to buy zero-day vulnerabilities on the open market. This is another step in the ongoing US actions against the company. [...]
