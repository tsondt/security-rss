Title: Critical Citrix DDoS Bug Shuts Down Network, Cloud App Access
Date: 2021-11-10T18:24:50+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: 2021-11-10-critical-citrix-ddos-bug-shuts-down-network-cloud-app-access

[Source](https://threatpost.com/critical-citrix-bug-etwork-cloud-app-access/176183/){:target="_blank" rel="noopener"}

> The distributed computing vendor patched the flaw, affecting Citrix ADC and Gateway, along with another flaw impacting availability for SD-WAN appliances. [...]
