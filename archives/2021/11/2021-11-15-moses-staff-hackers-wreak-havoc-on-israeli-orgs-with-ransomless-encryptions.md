Title: Moses Staff hackers wreak havoc on Israeli orgs with ransomless encryptions
Date: 2021-11-15T10:01:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-15-moses-staff-hackers-wreak-havoc-on-israeli-orgs-with-ransomless-encryptions

[Source](https://www.bleepingcomputer.com/news/security/moses-staff-hackers-wreak-havoc-on-israeli-orgs-with-ransomless-encryptions/){:target="_blank" rel="noopener"}

> A new hacker group named Moses Staff has recently claimed responsibility for numerous attacks against Israeli entities, which appear politically motivated as they do not make any ransom payment demands. [...]
