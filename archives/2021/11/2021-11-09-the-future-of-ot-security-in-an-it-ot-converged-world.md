Title: The future of OT security in an IT-OT converged world
Date: 2021-11-09T11:30:06+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2021-11-09-the-future-of-ot-security-in-an-it-ot-converged-world

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/09/securing_ics_in_the_cloud/){:target="_blank" rel="noopener"}

> Securing ICS in the cloud requires 'fundamentally different' approach Paid Feature If you thought the industrial internet of things (IIoT) was the cutting edge of industrial control systems, think again. Companies have been busy allowing external access to sensors and controllers in factories and utilities for a while now, but forward-thinking firms are now exploring a new development; operating their industrial control systems (ICS) entirely from the cloud. That raises a critical question: who's going to protect it all?... [...]
