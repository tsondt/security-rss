Title: <span>Interpol issues arrest warrants for members of Clop ransomware gang</span>
Date: 2021-11-08T13:47:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-08-interpol-issues-arrest-warrants-for-members-of-clop-ransomware-gang

[Source](https://portswigger.net/daily-swig/interpol-issues-arrest-warrants-for-members-of-clop-ransomware-gang){:target="_blank" rel="noopener"}

> Wanted: cybercriminals behind global malware campaign [...]
