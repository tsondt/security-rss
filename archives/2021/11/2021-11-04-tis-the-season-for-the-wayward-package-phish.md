Title: ‘Tis the Season for the Wayward Package Phish
Date: 2021-11-04T16:49:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;FedEex;FedEx;Louis Morton;smishing
Slug: 2021-11-04-tis-the-season-for-the-wayward-package-phish

[Source](https://krebsonsecurity.com/2021/11/tis-the-season-for-the-wayward-package-phish/){:target="_blank" rel="noopener"}

> The holiday shopping season always means big business for phishers, who tend to find increased success this time of year with a lure about a wayward package that needs redelivery. Here’s a look at a fairly elaborate SMS-based phishing scam that spoofs FedEx in a bid to extract personal and financial information from unwary recipients. One of dozens of FedEx-themed phishing sites currently being advertised via SMS spam. Louis Morton, a security professional based in Fort Worth, Texas, forwarded an SMS phishing or “smishing” message sent to his wife’s mobile device that indicated a package couldn’t be delivered. “It is [...]
