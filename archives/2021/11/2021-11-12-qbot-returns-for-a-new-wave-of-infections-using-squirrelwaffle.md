Title: QBot returns for a new wave of infections using Squirrelwaffle
Date: 2021-11-12T12:45:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-12-qbot-returns-for-a-new-wave-of-infections-using-squirrelwaffle

[Source](https://www.bleepingcomputer.com/news/security/qbot-returns-for-a-new-wave-of-infections-using-squirrelwaffle/){:target="_blank" rel="noopener"}

> The activity of the QBot (also known as Quakbot) banking trojan is spiking again, and analysts from multiple security research firms attribute this to the rise of Squirrelwaffle. [...]
