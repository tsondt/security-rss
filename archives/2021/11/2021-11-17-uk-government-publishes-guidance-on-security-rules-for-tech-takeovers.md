Title: UK government publishes guidance on security rules for tech takeovers
Date: 2021-11-17T11:46:05+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2021-11-17-uk-government-publishes-guidance-on-security-rules-for-tech-takeovers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/17/uk_government_publishes_guidance_national/){:target="_blank" rel="noopener"}

> National Security and Investment Act 2021 give ministers power to halt M&As The UK government has published guidance describing what technologies may be caught within the National Security and Investment Act 2021, which is set to give ministers the power to halt mergers and acquisitions.... [...]
