Title: SSL keys, sFTP passwords and more exposed after someone broke into GoDaddy Managed WordPress using 'compromised password'
Date: 2021-11-22T20:37:16+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-11-22-ssl-keys-sftp-passwords-and-more-exposed-after-someone-broke-into-godaddy-managed-wordpress-using-compromised-password

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/22/godaddy_managed_wordpress_ssl_keys/){:target="_blank" rel="noopener"}

> Yikes: Up to 1.2 million customers affected GoDaddy has admitted to America's financial watchdog that one or more miscreants broke into its systems and potentially accessed a huge amount of customer data, from email addresses to SSL private keys.... [...]
