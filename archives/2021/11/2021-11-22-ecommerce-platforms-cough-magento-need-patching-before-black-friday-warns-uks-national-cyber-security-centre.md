Title: Ecommerce platforms (cough, Magento) need patching before Black Friday, warns UK's National Cyber Security Centre
Date: 2021-11-22T17:14:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-22-ecommerce-platforms-cough-magento-need-patching-before-black-friday-warns-uks-national-cyber-security-centre

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/22/ncsc_magento_updates_black_friday_reminder/){:target="_blank" rel="noopener"}

> You're your own security team, remember? If you run a small online business powered by the Magento ecommerce platform, Britain's National Cyber Security Centre (NCSC) is begging you to make sure it's fully patched ahead of Black Friday.... [...]
