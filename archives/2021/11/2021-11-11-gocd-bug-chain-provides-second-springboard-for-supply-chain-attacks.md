Title: GoCD bug chain provides second springboard for supply chain attacks
Date: 2021-11-11T16:38:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-11-gocd-bug-chain-provides-second-springboard-for-supply-chain-attacks

[Source](https://portswigger.net/daily-swig/gocd-bug-chain-provides-second-springboard-for-supply-chain-attacks){:target="_blank" rel="noopener"}

> Follow-up to recent GoCD disclosure provides additional path to infiltrating build environments [...]
