Title: US seizes $6 million from REvil ransomware, arrest Kaseya hacker
Date: 2021-11-08T13:18:02-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-08-us-seizes-6-million-from-revil-ransomware-arrest-kaseya-hacker

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-6-million-from-revil-ransomware-arrest-kaseya-hacker/){:target="_blank" rel="noopener"}

> The United States Department of Justice today has announced charges against a REvil ransomware affiliate responsible for the attack against the Kaseya MSP platform on July 2nd and seizing more than $6 million from another REvil partner. [...]
