Title: Maritime giant Swire Pacific Offshore suffers data breach following cyber-attack
Date: 2021-11-26T11:18:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-26-maritime-giant-swire-pacific-offshore-suffers-data-breach-following-cyber-attack

[Source](https://portswigger.net/daily-swig/maritime-giant-swire-pacific-offshore-suffers-data-breach-following-cyber-attack){:target="_blank" rel="noopener"}

> Organization said it suffered ‘unauthorized access’ to systems [...]
