Title: No 10 accused of failing to act against states accused of NSO spyware abuses
Date: 2021-11-11T18:14:17+00:00
Author: Stephanie Kirchgaessner in Washington
Category: The Guardian
Tags: Surveillance;Boris Johnson;Conservatives;World news;Politics;UK news;International trade;Labour;Scottish National party (SNP);Liberal Democrats;Foreign policy;Espionage;Privacy;Data and computer security
Slug: 2021-11-11-no-10-accused-of-failing-to-act-against-states-accused-of-nso-spyware-abuses

[Source](https://www.theguardian.com/world/2021/nov/11/no-10-accused-failing-act-states-accused-nso-pegasus-spyware-abuses-boris-johnson){:target="_blank" rel="noopener"}

> Group of 10 MPs and peers say Boris Johnson’s government has prioritised trade over national security Boris Johnson’s government has been accused by MPs of prioritising trade agreements over national security in its handling of surveillance abuses on British soil by governments using spyware made by the Israeli company NSO Group. A letter to the British prime minister signed by 10 MPs and peers has called on the government to end its cybersecurity programmes with countries that are known to have used NSO spyware to target dissidents, journalists and lawyers, among others, and to impose sanctions on NSO, “if they [...]
