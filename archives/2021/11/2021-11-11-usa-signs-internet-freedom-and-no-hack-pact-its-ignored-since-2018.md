Title: USA signs internet freedom and no-hack pact it's ignored since 2018
Date: 2021-11-11T05:31:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-11-usa-signs-internet-freedom-and-no-hack-pact-its-ignored-since-2018

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/11/usa_supports_paris_call/){:target="_blank" rel="noopener"}

> Joins 79 nations supporting Paris Call for Trust and Security in Cyberspace – China and Russia aren't on the list The United States has signed up for The Paris Call for Trust and Security in Cyberspace – an international effort to ensure the internet remains free and open, and an agreement to put critical infrastructure off limits to electronic attack by sovereign states and other actors.... [...]
