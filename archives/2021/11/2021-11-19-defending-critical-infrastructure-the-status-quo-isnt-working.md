Title: Defending critical infrastructure: The status quo isn’t working
Date: 2021-11-19T07:30:04+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2021-11-19-defending-critical-infrastructure-the-status-quo-isnt-working

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/19/defending_critical_infrastructure_against_ransomware/){:target="_blank" rel="noopener"}

> AI can help thwart attacks before they affect operations Paid Feature Cyber-attacks aren't just about siphoning bank accounts. They're also targeting critical national infrastructure, warn experts – and we're not doing a very good job of preventing them. How can we stop the rot and protect the systems that funnel our oil, carry our electricity, and manage our water, among other things?... [...]
