Title: Dangerous XSS bug in Google Chrome’s ‘New Tab’ page bypassed security features
Date: 2021-11-03T15:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-03-dangerous-xss-bug-in-google-chromes-new-tab-page-bypassed-security-features

[Source](https://portswigger.net/daily-swig/dangerous-xss-bug-in-google-chromes-new-tab-page-bypassed-security-features){:target="_blank" rel="noopener"}

> ‘Chrome’s NTP only has a really weak CSP that doesn’t mitigate XSS’ [...]
