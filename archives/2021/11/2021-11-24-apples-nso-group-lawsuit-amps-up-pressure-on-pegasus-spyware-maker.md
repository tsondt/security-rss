Title: Apple’s NSO Group Lawsuit Amps Up Pressure on Pegasus Spyware-Maker
Date: 2021-11-24T15:55:50+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware;Mobile Security;Privacy;Vulnerabilities
Slug: 2021-11-24-apples-nso-group-lawsuit-amps-up-pressure-on-pegasus-spyware-maker

[Source](https://threatpost.com/apple-nso-lawsuit-pegasus-spyware/176565/){:target="_blank" rel="noopener"}

> Just weeks after a judge ruled that NSO Group did not have immunity in a suit brought by Facebook subsidiary WhatsApp, Apple is adding significant weight to the company's woes. [...]
