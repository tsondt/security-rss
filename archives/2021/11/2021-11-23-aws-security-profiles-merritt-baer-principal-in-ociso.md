Title: AWS Security Profiles: Merritt Baer, Principal in OCISO
Date: 2021-11-23T18:18:06+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: AWS re:Invent;Foundational (100);Security, Identity, & Compliance;ransomware;reInvent
Slug: 2021-11-23-aws-security-profiles-merritt-baer-principal-in-ociso

[Source](https://aws.amazon.com/blogs/security/aws-security-profiles-merritt-baer-principal-in-ociso/){:target="_blank" rel="noopener"}

> In the week leading up AWS re:Invent 2021, we’ll share conversations we’ve had with people at AWS who will be presenting, and get a sneak peek at their work. How long have you been at Amazon Web Services (AWS), and what do you do in your current role? I’m a Principal in the Office of [...]
