Title: Microsoft Edge adds Super Duper Secure Mode to Stable channel
Date: 2021-11-23T12:09:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-23-microsoft-edge-adds-super-duper-secure-mode-to-stable-channel

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-edge-adds-super-duper-secure-mode-to-stable-channel/){:target="_blank" rel="noopener"}

> Microsoft has quietly added a 'Super Duper Secure Mode' to the Microsoft Edge web browser, a new feature that brings security improvements without significant performance losses. [...]
