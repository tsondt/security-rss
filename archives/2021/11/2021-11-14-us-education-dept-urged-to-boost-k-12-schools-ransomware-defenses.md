Title: US Education Dept urged to boost K-12 schools' ransomware defenses
Date: 2021-11-14T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-14-us-education-dept-urged-to-boost-k-12-schools-ransomware-defenses

[Source](https://www.bleepingcomputer.com/news/security/us-education-dept-urged-to-boost-k-12-schools-ransomware-defenses/){:target="_blank" rel="noopener"}

> The US Department of Education and Department of Homeland Security (DHS) were urged this week to more aggressively strengthen cybersecurity protections at K-12 schools across the nation to keep up with a massive wave of attacks. [...]
