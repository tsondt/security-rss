Title: Data transfers between the EU and the US: Still unclear on what you're supposed to do? Here's an explainer
Date: 2021-11-01T11:30:08+00:00
Author: Rafi Azim-Khan and Steve Farmer
Category: The Register
Tags: 
Slug: 2021-11-01-data-transfers-between-the-eu-and-the-us-still-unclear-on-what-youre-supposed-to-do-heres-an-explainer

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/01/data_transfers_europe/){:target="_blank" rel="noopener"}

> This applies to British businesses too... for now Lightning does not strike twice – except, it would seem, in the land of data privacy. Having struck down Safe Harbor – the agreement governing EU-US data transfers – in 2015, the Court of Justice of the European Union (CJEU) went on to condemn its replacement, the beleaguered EU-US Privacy Shield, to a similar fate just over a year ago.... [...]
