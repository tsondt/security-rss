Title: 2021 PCI 3DS report now available
Date: 2021-11-23T19:05:06+00:00
Author: Michael Oyeniya
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;Compliance reports;PCI;PCI DSS;Security Blog
Slug: 2021-11-23-2021-pci-3ds-report-now-available

[Source](https://aws.amazon.com/blogs/security/2021-pci-3ds-report-now-available/){:target="_blank" rel="noopener"}

> We are excited to announce that Amazon Web Services (AWS) has released the latest 2021 PCI 3-D Secure (3DS) attestation to support our customers implementing EMV® 3-D Secure services on AWS. Although AWS doesn’t directly perform the functions of 3DS Server (3DSS), 3DS Directory Server (DS), or 3DS Access Control Server (ACS), AWS customers can [...]
