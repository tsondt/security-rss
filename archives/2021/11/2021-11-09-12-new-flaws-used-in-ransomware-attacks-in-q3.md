Title: 12 New Flaws Used in Ransomware Attacks in Q3
Date: 2021-11-09T18:06:33+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-11-09-12-new-flaws-used-in-ransomware-attacks-in-q3

[Source](https://threatpost.com/12-new-flaws-used-in-ransomware-attacks-in-q3/176137/){:target="_blank" rel="noopener"}

> The Q3 2021 report revealed a 4.5% increase in CVEs associated with ransomware and a 3.4% increase in ransomware families compared with Q2 2021. [...]
