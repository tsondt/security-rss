Title: The ‘Zelle Fraud’ Scam: How it Works, How to Fight Back
Date: 2021-11-19T21:36:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;Bob Sullivan;Consumer Financial Protection Bureau;CUNA Mutual Insurance;Ken Otsuka;Zelle scam
Slug: 2021-11-19-the-zelle-fraud-scam-how-it-works-how-to-fight-back

[Source](https://krebsonsecurity.com/2021/11/the-zelle-fraud-scam-how-it-works-how-to-fight-back/){:target="_blank" rel="noopener"}

> One of the more common ways cybercriminals cash out access to bank accounts involves draining the victim’s funds via Zelle, a “peer-to-peer” (P2P) payment service used by many financial institutions that allows customers to quickly send cash to friends and family. Naturally, a great deal of phishing schemes that precede these bank account takeovers begin with a spoofed text message from the target’s bank warning about a suspicious Zelle transfer. What follows is a deep dive into how this increasingly clever Zelle fraud scam typically works, and what victims can do about it. Last week’s story warned that scammers are [...]
