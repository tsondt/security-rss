Title: Criminal group dismantled after forcing victims to be money mules
Date: 2021-11-08T10:40:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2021-11-08-criminal-group-dismantled-after-forcing-victims-to-be-money-mules

[Source](https://www.bleepingcomputer.com/news/security/criminal-group-dismantled-after-forcing-victims-to-be-money-mules/){:target="_blank" rel="noopener"}

> The Spanish police have arrested 45 people who are believed to be members of an online fraud group that operated twenty websites to defraud at least 200 people of 1,500,000 Euros ($1.73 million). [...]
