Title: WordPress sites are being hacked in fake ransomware attacks
Date: 2021-11-16T12:35:50-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-16-wordpress-sites-are-being-hacked-in-fake-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/wordpress-sites-are-being-hacked-in-fake-ransomware-attacks/){:target="_blank" rel="noopener"}

> A new wave of attacks starting late last week has hacked close to 300 WordPress sites to display fake encryption notices, trying to trick the site owners into paying 0.1 bitcoin for restoration. [...]
