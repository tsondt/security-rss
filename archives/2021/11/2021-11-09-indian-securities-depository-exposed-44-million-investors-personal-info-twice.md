Title: Indian securities depository exposed 44 million investors' personal info – twice
Date: 2021-11-09T04:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-09-indian-securities-depository-exposed-44-million-investors-personal-info-twice

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/09/cdsl_data_leak/){:target="_blank" rel="noopener"}

> Didn't act until CERT stepped in and pointed out problems Indian infosec consultancy CyberX9 claims it twice found records of 43.9 million shareholders exposed by systems operated by Central Depository Services Limited (CDSL) – and that the depository company responded slowly to its alerts of significant vulnerabilities.... [...]
