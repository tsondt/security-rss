Title: FBI: Ransomware gangs hit several tribal-owned casinos in the last year
Date: 2021-11-05T12:03:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-05-fbi-ransomware-gangs-hit-several-tribal-owned-casinos-in-the-last-year

[Source](https://www.bleepingcomputer.com/news/security/fbi-ransomware-gangs-hit-several-tribal-owned-casinos-in-the-last-year/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) says that multiple ransomware gangs have hit tribal entities over the last year, taking down their systems and impacting businesses and public services. [...]
