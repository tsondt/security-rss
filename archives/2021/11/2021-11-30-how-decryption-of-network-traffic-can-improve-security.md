Title: How Decryption of Network Traffic Can Improve Security
Date: 2021-11-30T20:58:59+00:00
Author: Jeff Costlow
Category: Threatpost
Tags: InfoSec Insider;Vulnerabilities;Web Security
Slug: 2021-11-30-how-decryption-of-network-traffic-can-improve-security

[Source](https://threatpost.com/decryption-improve-security/176613/){:target="_blank" rel="noopener"}

> Most industry analyst firms conclude that between 80-90 percent of network traffic is encrypted today. Jeff Costlow, CISO at ExtraHop, explains why this might not be a good thing. [...]
