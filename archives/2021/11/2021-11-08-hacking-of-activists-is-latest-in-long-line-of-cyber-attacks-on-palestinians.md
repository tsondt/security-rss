Title: Hacking of activists is latest in long line of cyber-attacks on Palestinians
Date: 2021-11-08T16:22:34+00:00
Author: Peter Beaumont
Category: The Guardian
Tags: Surveillance;Israel;Palestinian territories;Human rights;Malware;Espionage;Middle East and North Africa;Technology;Data and computer security;World news;Privacy
Slug: 2021-11-08-hacking-of-activists-is-latest-in-long-line-of-cyber-attacks-on-palestinians

[Source](https://www.theguardian.com/world/2021/nov/08/hacking-activists-latest-long-line-cyber-attacks-palestinians-nso-group-pegasus-spyware){:target="_blank" rel="noopener"}

> Analysis: while identity of hackers is not known in this case, Palestinians have long been spied on by Israeli military The disclosure that Palestinian human rights defenders were reportedly hacked using NSO’s Pegasus spyware will come as little surprise to two groups of people: Palestinians themselves and the Israeli military and intelligence cyber operatives who have long spied on Palestinians. While it is not known who was responsible for the hacking in this instance, what is very well documented is the role of the Israeli military’s 8200 cyberwarfare unit – known in Hebrew as the Yehida Shmoneh-Matayim – in the [...]
