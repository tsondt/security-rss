Title: There's something to be said for delayed gratification when Windows 11 is this full of bugs
Date: 2021-11-15T13:15:54+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: 2021-11-15-theres-something-to-be-said-for-delayed-gratification-when-windows-11-is-this-full-of-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/15/windows_11_insiders_bug_fixes/){:target="_blank" rel="noopener"}

> Also: Emergency patch for Windows Server after Patch Tuesday broke single sign-on for some users An update to the Insiders version of Windows 11 includes a massive list of bug fixes, many of them serious, showing the wisdom of holding back on an early upgrade from Windows 10.... [...]
