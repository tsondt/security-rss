Title: Israeli spyware company NSO Group placed on US blacklist
Date: 2021-11-03T19:53:21+00:00
Author: Stephanie Kirchgaessner in Washington
Category: The Guardian
Tags: US foreign policy;Surveillance;US news;Biden administration;World news;Israel;Technology;Malware;Data and computer security
Slug: 2021-11-03-israeli-spyware-company-nso-group-placed-on-us-blacklist

[Source](https://www.theguardian.com/us-news/2021/nov/03/nso-group-pegasus-spyware-us-blacklist){:target="_blank" rel="noopener"}

> Decision against company at heart of Pegasus project reflects deep concern about impact of spyware on US national security interests NSO Group has been placed on a US blacklist by the Biden administration after it determined the Israeli spyware maker has acted “contrary to the foreign policy and national security interests of the US”. The finding by the commerce department represents a major blow to the Israeli company and reveals a deep undercurrent of concern by the US about the impact of spyware on national security. Continue reading... [...]
