Title: Microsoft Defender for Endpoint fails to start on Windows Server
Date: 2021-11-25T03:51:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-25-microsoft-defender-for-endpoint-fails-to-start-on-windows-server

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-for-endpoint-fails-to-start-on-windows-server/){:target="_blank" rel="noopener"}

> Microsoft has confirmed a new issue impacting Windows Server devices preventing the Microsoft Defender for Endpoint security solution from launching on some systems. [...]
