Title: Crypto investors lose $500,000 to Google Ads pushing fake wallets
Date: 2021-11-04T09:23:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-11-04-crypto-investors-lose-500000-to-google-ads-pushing-fake-wallets

[Source](https://www.bleepingcomputer.com/news/security/crypto-investors-lose-500-000-to-google-ads-pushing-fake-wallets/){:target="_blank" rel="noopener"}

> ​Threat actors are using advertisements in Google Search to promote fake cryptocurrency wallets and DEX platforms to steal user's cryptocurrency. [...]
