Title: EwDoor botnet targets AT&T network edge devices at US firms
Date: 2021-11-30T12:26:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-30-ewdoor-botnet-targets-att-network-edge-devices-at-us-firms

[Source](https://www.bleepingcomputer.com/news/security/ewdoor-botnet-targets-atandt-network-edge-devices-at-us-firms/){:target="_blank" rel="noopener"}

> A recently discovered botnet is attacking unpatched AT&T enterprise network edge devices using exploits for a four-year-old critical severity Blind Command Injection security flaw. [...]
