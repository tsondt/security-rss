Title: Mozilla sprinkles Firefox Relay with Premium fairy dust
Date: 2021-11-16T16:22:40+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-11-16-mozilla-sprinkles-firefox-relay-with-premium-fairy-dust

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/16/mozilla_firefox_relay_premium/){:target="_blank" rel="noopener"}

> You want more than five email aliases? Sure, but it'll cost you Mozilla hopes to ramp up the monetisation machine with a paid premium version of its Firefox Relay service, upping the current limit of five email aliases to a near-unlimited number.... [...]
