Title: AWS Security Profiles: Megan O’Neil, Sr. Security Solutions Architect
Date: 2021-11-24T18:39:42+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: AWS re:Invent;Security, Identity, & Compliance;Uncategorized;ransomeware;reInvent;threat detection
Slug: 2021-11-24-aws-security-profiles-megan-oneil-sr-security-solutions-architect

[Source](https://aws.amazon.com/blogs/security/aws-security-profiles-megan-oneil-sr-security-solutions-architect/){:target="_blank" rel="noopener"}

> In the week leading up to AWS re:Invent 2021, we’ll share conversations we’ve had with people at AWS who will be presenting, and get a sneak peek at their work. How long have you been at Amazon Web Services (AWS), and what do you do in your current role? I’ve been at AWS nearly 4 [...]
