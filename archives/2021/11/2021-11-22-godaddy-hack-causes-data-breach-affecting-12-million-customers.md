Title: GoDaddy hack causes data breach affecting 1.2 million customers
Date: 2021-11-22T11:43:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-22-godaddy-hack-causes-data-breach-affecting-12-million-customers

[Source](https://www.bleepingcomputer.com/news/security/godaddy-hack-causes-data-breach-affecting-12-million-customers/){:target="_blank" rel="noopener"}

> GoDaddy said in a data breach notification published today that the data of up to 1.2 million of its customers was exposed after hackers gained access to the company's Managed WordPress hosting environment. [...]
