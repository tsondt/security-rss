Title: Fall 2021 SOC 2 Type I Privacy report now available
Date: 2021-11-15T18:00:11+00:00
Author: Ninad Naik
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Privacy Report;AWS SOC Reports;Security Blog
Slug: 2021-11-15-fall-2021-soc-2-type-i-privacy-report-now-available

[Source](https://aws.amazon.com/blogs/security/fall-2021-soc-2-type-i-privacy-report-now-available/){:target="_blank" rel="noopener"}

> Your privacy considerations are at the core of our compliance work, and at Amazon Web Services (AWS), we are focused on the protection of your content while using AWS services. Our Fall 2021 SOC 2 Type I Privacy report is now available, demonstrating the privacy compliance commitments we made to you. The Fall 2021 SOC [...]
