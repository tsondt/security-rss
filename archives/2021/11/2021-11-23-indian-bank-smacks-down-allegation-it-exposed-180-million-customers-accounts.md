Title: Indian bank smacks down allegation it exposed 180 million customers' accounts
Date: 2021-11-23T01:58:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-23-indian-bank-smacks-down-allegation-it-exposed-180-million-customers-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/23/punjab_national_bank_cyberx9_exchange_allegation/){:target="_blank" rel="noopener"}

> Infosec firm says it found unpatched software, Bank admits Exchange may not have been in the best shape India's Punjab National Bank has smacked down a security firm's allegation that it exposed personal and financial data of its 180 million customers – but appears to have admitted its Exchange Server implementation wasn't in tip-top shape.... [...]
