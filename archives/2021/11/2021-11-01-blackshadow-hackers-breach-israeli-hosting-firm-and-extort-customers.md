Title: BlackShadow hackers breach Israeli hosting firm and extort customers
Date: 2021-11-01T10:37:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-01-blackshadow-hackers-breach-israeli-hosting-firm-and-extort-customers

[Source](https://www.bleepingcomputer.com/news/security/blackshadow-hackers-breach-israeli-hosting-firm-and-extort-customers/){:target="_blank" rel="noopener"}

> The BlackShadow hacking group attacked the Israeli hosting provider Cyberserve to steal client databases and disrupt the company's services. [...]
