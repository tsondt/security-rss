Title: China Telecom's US arm sues in last-ditch bid to retain license
Date: 2021-11-16T06:15:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-11-16-china-telecoms-us-arm-sues-in-last-ditch-bid-to-retain-license

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/16/china_telecom_us_emergency_lawsuit/){:target="_blank" rel="noopener"}

> Company claims it poses no threat, yet regs want China influence out The US subsidiary of China Telecom has filed an emergency appeal it hopes will prevent the impending revocation of the company's license to operate in the USA, which the The Federal Communications Commission (FCC) terminated in October on grounds the carrier is a national security threat.... [...]
