Title: US indicts Iranian hackers for Proud Boys voter intimidation emails
Date: 2021-11-18T15:19:37-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-18-us-indicts-iranian-hackers-for-proud-boys-voter-intimidation-emails

[Source](https://www.bleepingcomputer.com/news/security/us-indicts-iranian-hackers-for-proud-boys-voter-intimidation-emails/){:target="_blank" rel="noopener"}

> The U.S. Department of State is offering a $10 million reward for information about the activities of two Iranian nationals charged for cyber activity intended to "intimidate and influence" American voters during the 2020 U.S. presidential campaign. [...]
