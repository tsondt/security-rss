Title: Malicious Chrome extensions are bad. But what about nice ones that can be hijacked? This new tool spots them
Date: 2021-11-11T08:36:41+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-11-malicious-chrome-extensions-are-bad-but-what-about-nice-ones-that-can-be-hijacked-this-new-tool-spots-them

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/11/chrome_extension_analyzer/){:target="_blank" rel="noopener"}

> DoubleX static analyzer is doubleplusgood Security researchers from Germany's CISPA Helmholtz Center for Information Security have developed software to help identify Chrome extensions that are vulnerable to exploitation by malicious webpages and other extensions.... [...]
