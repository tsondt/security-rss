Title: New differential fuzzing tool reveals novel HTTP request smuggling techniques
Date: 2021-11-25T16:55:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-25-new-differential-fuzzing-tool-reveals-novel-http-request-smuggling-techniques

[Source](https://portswigger.net/daily-swig/new-differential-fuzzing-tool-reveals-novel-http-request-smuggling-techniques){:target="_blank" rel="noopener"}

> White paper systematically examines the attack while showcasing a ‘laundry list’ of new flaws [...]
