Title: Emotet botnet comeback orchestrated by Conti ransomware gang
Date: 2021-11-19T14:05:11-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-19-emotet-botnet-comeback-orchestrated-by-conti-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/emotet-botnet-comeback-orchestrated-by-conti-ransomware-gang/){:target="_blank" rel="noopener"}

> The Emotet botnet is back by popular demand, resurrected by its former operator, who was convinced by members of the Conti ransomware gang. [...]
