Title: NUCLEUS:13 – Host of vulnerabilities shatter Nucelus TCP/IP stack defenses
Date: 2021-11-22T11:09:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-22-nucleus13-host-of-vulnerabilities-shatter-nucelus-tcpip-stack-defenses

[Source](https://portswigger.net/daily-swig/nucleus-13-host-of-vulnerabilities-shatter-nucelus-tcp-ip-stack-defenses){:target="_blank" rel="noopener"}

> Worst security flaw can lead to remote code execution [...]
