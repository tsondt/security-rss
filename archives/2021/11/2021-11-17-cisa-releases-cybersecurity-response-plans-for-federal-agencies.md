Title: CISA releases cybersecurity response plans for federal agencies
Date: 2021-11-17T08:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-17-cisa-releases-cybersecurity-response-plans-for-federal-agencies

[Source](https://www.bleepingcomputer.com/news/security/cisa-releases-cybersecurity-response-plans-for-federal-agencies/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has released new cybersecurity response plans (known as playbooks) for federal civilian executive branch (FCEB) agencies. [...]
