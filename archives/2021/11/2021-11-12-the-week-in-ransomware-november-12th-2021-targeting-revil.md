Title: The Week in Ransomware - November 12th 2021 - Targeting REvil
Date: 2021-11-12T16:07:06-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-12-the-week-in-ransomware-november-12th-2021-targeting-revil

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-12th-2021-targeting-revil/){:target="_blank" rel="noopener"}

> This week, law enforcement struck a massive blow against the REvil ransomware operation, with multiple arrests announced and the seizure of cryptocurrency. [...]
