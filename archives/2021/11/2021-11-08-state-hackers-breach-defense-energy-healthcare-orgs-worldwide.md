Title: State hackers breach defense, energy, healthcare orgs worldwide
Date: 2021-11-08T03:34:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-08-state-hackers-breach-defense-energy-healthcare-orgs-worldwide

[Source](https://www.bleepingcomputer.com/news/security/state-hackers-breach-defense-energy-healthcare-orgs-worldwide/){:target="_blank" rel="noopener"}

> Cybersecurity firm Palo Alto Networks warned over the weekend of an ongoing hacking campaign that has already resulted in the compromise of at least nine organizations worldwide from critical sectors, including defense, healthcare, energy, technology, and education. [...]
