Title: Data breach at New Mexico healthcare business impacts 62,000 state residents
Date: 2021-11-25T12:13:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-25-data-breach-at-new-mexico-healthcare-business-impacts-62000-state-residents

[Source](https://portswigger.net/daily-swig/data-breach-at-new-mexico-healthcare-business-impacts-62-000-state-residents){:target="_blank" rel="noopener"}

> True Health New Mexico was hit by a cyber-attack in October [...]
