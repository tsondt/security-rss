Title: Critical Linux Kernel Bug Allows Remote Takeover
Date: 2021-11-04T15:50:42+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-11-04-critical-linux-kernel-bug-allows-remote-takeover

[Source](https://threatpost.com/critical-linux-kernel-bug/176000/){:target="_blank" rel="noopener"}

> The bug (CVE-2021-43267) exists in a TIPC message type that allows Linux nodes to send cryptographic keys to each other. [...]
