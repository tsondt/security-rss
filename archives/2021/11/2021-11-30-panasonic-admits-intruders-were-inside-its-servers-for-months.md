Title: Panasonic admits intruders were inside its servers for months
Date: 2021-11-30T04:27:18+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-30-panasonic-admits-intruders-were-inside-its-servers-for-months

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/30/panasonic_breach/){:target="_blank" rel="noopener"}

> Spotted the crack after it ended – still not sure what was lost Japanese industrial giant Panasonic has admitted it's been popped, and badly.... [...]
