Title: Human rights activists condemn mass denial of service as Sudan’s nationwide internet shutdown enters second week
Date: 2021-11-04T12:55:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-04-human-rights-activists-condemn-mass-denial-of-service-as-sudans-nationwide-internet-shutdown-enters-second-week

[Source](https://portswigger.net/daily-swig/human-rights-activists-condemn-mass-denial-of-service-as-sudans-nationwide-internet-shutdown-enters-second-week){:target="_blank" rel="noopener"}

> ‘All mobile internet networks are completely cut off,’ one journalist on the ground tells The Daily Swig [...]
