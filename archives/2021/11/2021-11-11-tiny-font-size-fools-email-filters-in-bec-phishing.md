Title: Tiny Font Size Fools Email Filters in BEC Phishing
Date: 2021-11-11T14:00:04+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-11-11-tiny-font-size-fools-email-filters-in-bec-phishing

[Source](https://threatpost.com/tiny-font-size-email-filters-bec-phishing/176198/){:target="_blank" rel="noopener"}

> The One Font BEC campaign targets Microsoft 365 users and uses sophisticated obfuscation tactics to slip past security protections to harvest credentials. [...]
