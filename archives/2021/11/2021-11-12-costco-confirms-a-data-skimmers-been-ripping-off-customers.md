Title: Costco Confirms: A Data Skimmer’s Been Ripping Off Customers
Date: 2021-11-12T23:19:17+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks
Slug: 2021-11-12-costco-confirms-a-data-skimmers-been-ripping-off-customers

[Source](https://threatpost.com/costco-data-skimmer-customers-notification/176320/){:target="_blank" rel="noopener"}

> Big-box behemoth retailer Costco is offering victims 12 months of credit monitoring, a $1 million insurance reimbursement policy and ID theft recovery services. [...]
