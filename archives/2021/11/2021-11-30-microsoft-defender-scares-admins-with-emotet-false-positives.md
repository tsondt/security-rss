Title: Microsoft Defender scares admins with Emotet false positives
Date: 2021-11-30T18:04:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-30-microsoft-defender-scares-admins-with-emotet-false-positives

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-scares-admins-with-emotet-false-positives/){:target="_blank" rel="noopener"}

> Microsoft Defender for Endpoint is currently blocking Office documents from being opened and some executables from launching due to a false positive tagging the files as potentially bundling an Emotet malware payload. [...]
