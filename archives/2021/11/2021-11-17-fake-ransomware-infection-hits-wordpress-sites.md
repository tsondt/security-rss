Title: Fake Ransomware Infection Hits WordPress Sites
Date: 2021-11-17T22:06:26+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-11-17-fake-ransomware-infection-hits-wordpress-sites

[Source](https://threatpost.com/fake-ransomware-infection-wordpress/176410/){:target="_blank" rel="noopener"}

> WordPress sites have been splashed with ransomware warnings that are as real as dime-store cobwebs made out of spun polyester. [...]
