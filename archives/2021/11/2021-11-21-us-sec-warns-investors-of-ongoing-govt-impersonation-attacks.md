Title: US SEC warns investors of ongoing govt impersonation attacks
Date: 2021-11-21T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-21-us-sec-warns-investors-of-ongoing-govt-impersonation-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-sec-warns-investors-of-ongoing-govt-impersonation-attacks/){:target="_blank" rel="noopener"}

> The Securities and Exchange Commission (SEC) has warned US investors of scammers impersonating SEC officials in government impersonator schemes via phone calls, voicemails, emails, and letters. [...]
