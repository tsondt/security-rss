Title: TrickBot phishing checks screen resolution to evade researchers
Date: 2021-11-26T13:02:16-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-26-trickbot-phishing-checks-screen-resolution-to-evade-researchers

[Source](https://www.bleepingcomputer.com/news/security/trickbot-phishing-checks-screen-resolution-to-evade-researchers/){:target="_blank" rel="noopener"}

> The TrickBot malware operators have been using a new method to check the screen resolution of a victim system to evade detection of security software and analysis by researchers. [...]
