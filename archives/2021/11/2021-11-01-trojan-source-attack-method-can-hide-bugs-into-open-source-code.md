Title: 'Trojan Source' attack method can hide bugs into open-source code
Date: 2021-11-01T20:07:48-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-01-trojan-source-attack-method-can-hide-bugs-into-open-source-code

[Source](https://www.bleepingcomputer.com/news/security/trojan-source-attack-method-can-hide-bugs-into-open-source-code/){:target="_blank" rel="noopener"}

> Academic researchers have released details about a new attack method they call "Trojan Source" that allows injecting vulnerabilities into the source code of a software project in a way that human reviewers can't detect. [...]
