Title: If even tech leaders struggle with email encryption, are we all doomed?
Date: 2021-11-11T18:00:09+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-11-11-if-even-tech-leaders-struggle-with-email-encryption-are-we-all-doomed

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/11/email_encryption/){:target="_blank" rel="noopener"}

> And the answer is... well, tune in next week and we'll reveal all Webinar Email is fundamental to the operation of most businesses. Unfortunately, it’s also fundamental to how most cyber-attackers ply their trade too.... [...]
