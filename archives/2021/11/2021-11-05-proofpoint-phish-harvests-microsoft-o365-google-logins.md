Title: Proofpoint Phish Harvests Microsoft O365, Google Logins
Date: 2021-11-05T15:12:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Web Security
Slug: 2021-11-05-proofpoint-phish-harvests-microsoft-o365-google-logins

[Source](https://threatpost.com/proofpoint-phish-microsoft-o365-google-logins/176038/){:target="_blank" rel="noopener"}

> A savvy campaign impersonating the cybersecurity company skated past Microsoft email security. [...]
