Title: DuckDuckGo wants to stop apps tracking you on Android
Date: 2021-11-20T11:22:18+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Tech;android;duckduckgo;privacy;search;smartphones
Slug: 2021-11-20-duckduckgo-wants-to-stop-apps-tracking-you-on-android

[Source](https://arstechnica.com/?p=1814467){:target="_blank" rel="noopener"}

> Enlarge / Gabriel Weinberg, creator of DuckDuckGo. (credit: Washington Post | Getty Images) At the end of April, Apple’s introduction of App Tracking Transparency tools shook the advertising industry to its core. iPhone and iPad owners could now stop apps from tracking their behavior and using their data for personalized advertising. Since the new privacy controls launched, almost $10 billion has been wiped from the revenues of Snap, Meta Platform’s Facebook, Twitter, and YouTube. Now, a similar tool is coming to Google’s Android operating system—although not from Google itself. Privacy-focused tech company DuckDuckGo, which started life as a private search [...]
