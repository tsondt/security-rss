Title: It’s about the survival of the fittest – CISOs must be brave enough to throw away their security playbook, or suffer the consequences
Date: 2021-11-25T17:00:08+00:00
Author: masked CISO
Category: The Register
Tags: 
Slug: 2021-11-25-its-about-the-survival-of-the-fittest-cisos-must-be-brave-enough-to-throw-away-their-security-playbook-or-suffer-the-consequences

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/25/vectra_masked_ciso_series/){:target="_blank" rel="noopener"}

> The Vectra Masked CISO series gives security leaders a place to expose the biggest issues in security and advise peers on how to overcome them. Advertorial I’m always asked what keeps me awake at night. Being targeted by APT groups? New ransomware strains?... [...]
