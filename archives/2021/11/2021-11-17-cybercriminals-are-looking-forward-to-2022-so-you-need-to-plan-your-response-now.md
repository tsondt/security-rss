Title: Cybercriminals are looking forward to 2022, so you need to plan your response now
Date: 2021-11-17T18:00:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-11-17-cybercriminals-are-looking-forward-to-2022-so-you-need-to-plan-your-response-now

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/17/rapid7_security_defense_planning/){:target="_blank" rel="noopener"}

> Rapid7's 2022 Planning webinars are here to help Paid Post Is it too early to be thinking about cybersecurity in 2022? That’s an easy one... of course not.... [...]
