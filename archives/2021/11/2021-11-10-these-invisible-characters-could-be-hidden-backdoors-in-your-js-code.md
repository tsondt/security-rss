Title: These invisible characters could be hidden backdoors in your JS code
Date: 2021-11-10T08:18:44-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-11-10-these-invisible-characters-could-be-hidden-backdoors-in-your-js-code

[Source](https://www.bleepingcomputer.com/news/security/these-invisible-characters-could-be-hidden-backdoors-in-your-js-code/){:target="_blank" rel="noopener"}

> Could malicious backdoors be hiding in your code, that otherwise appears perfectly clean to the human eye and text editors alike? A security researcher has shed light on how invisible characters can be snuck into JavaScript code to introduce security risks, like backdoors, into your software. [...]
