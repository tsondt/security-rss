Title: Native Tribal Casinos Taking Millions in Ransomware Losses
Date: 2021-11-05T19:55:19+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware
Slug: 2021-11-05-native-tribal-casinos-taking-millions-in-ransomware-losses

[Source](https://threatpost.com/native-tribal-casinos-ransomware-losses/176060/){:target="_blank" rel="noopener"}

> An FBI notification is warning of an uptick in attacks against tribal casinos. [...]
