Title: California Pizza Kitchen Serves Up Employee SSNs in Data Breach
Date: 2021-11-19T13:31:28+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Web Security
Slug: 2021-11-19-california-pizza-kitchen-serves-up-employee-ssns-in-data-breach

[Source](https://threatpost.com/california-pizza-kitchen-employee-ssns-data-breach/176478/){:target="_blank" rel="noopener"}

> A hefty slice of data – that of 100K+ current and former employees – was spilled in an “external system breach,” the pizza chain said. [...]
