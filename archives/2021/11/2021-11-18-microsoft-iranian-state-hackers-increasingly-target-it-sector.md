Title: Microsoft: Iranian state hackers increasingly target IT sector
Date: 2021-11-18T11:57:31-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-11-18-microsoft-iranian-state-hackers-increasingly-target-it-sector

[Source](https://www.bleepingcomputer.com/news/security/microsoft-iranian-state-hackers-increasingly-target-it-sector/){:target="_blank" rel="noopener"}

> Microsoft says Iranian-backed hacking groups have increasingly attempted to compromise IT services companies this year to steal credentials they could use to breach the systems of downstream clients. [...]
