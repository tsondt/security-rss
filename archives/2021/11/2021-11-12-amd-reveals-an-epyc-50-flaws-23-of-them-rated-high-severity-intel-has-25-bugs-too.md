Title: AMD reveals an Epyc 50 flaws – 23 of them rated high severity. Intel has 25 bugs, too
Date: 2021-11-12T06:02:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-12-amd-reveals-an-epyc-50-flaws-23-of-them-rated-high-severity-intel-has-25-bugs-too

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/12/amd_and_intel_flaws/){:target="_blank" rel="noopener"}

> Think of an attack – DoS, arbitrary code execution, memory corruption – and one of these vulns allows it Microsoft may have given us a mere 55 CVEs to worry about on November's Patch Tuesday, but AMD and Intel have together topped that number with fixes for their products.... [...]
