Title: Leaked footage shows British F-35B falling off HMS Queen Elizabeth and pilot's death-defying ejection
Date: 2021-11-30T14:55:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-30-leaked-footage-shows-british-f-35b-falling-off-hms-queen-elizabeth-and-pilots-death-defying-ejection

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/30/f35b_ejection_hms_queen_elizabeth_video/){:target="_blank" rel="noopener"}

> Parachute snagged on ship's bows Video Video footage has emerged of a British F-35B fighter jet falling off the front of aircraft carrier HMS Queen Elizabeth after a botched takeoff.... [...]
