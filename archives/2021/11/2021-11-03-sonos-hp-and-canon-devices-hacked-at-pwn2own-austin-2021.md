Title: Sonos, HP, and Canon devices hacked at Pwn2Own Austin 2021
Date: 2021-11-03T09:58:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-03-sonos-hp-and-canon-devices-hacked-at-pwn2own-austin-2021

[Source](https://www.bleepingcomputer.com/news/security/sonos-hp-and-canon-devices-hacked-at-pwn2own-austin-2021/){:target="_blank" rel="noopener"}

> During the first day of Pwn2Own Austin 2021, contestants won $362,500 after exploiting previously unknown security flaws to hack printers, routers, NAS devices, and speakers from Canon, HP, Western Digital, Cisco, Sonos, TP-Link, and NETGEAR. [...]
