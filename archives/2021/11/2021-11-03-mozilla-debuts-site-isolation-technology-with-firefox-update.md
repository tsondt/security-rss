Title: Mozilla debuts Site Isolation technology with Firefox update
Date: 2021-11-03T16:22:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-03-mozilla-debuts-site-isolation-technology-with-firefox-update

[Source](https://portswigger.net/daily-swig/mozilla-debuts-site-isolation-technology-with-firefox-update){:target="_blank" rel="noopener"}

> Sandboxing technology levels up browser security [...]
