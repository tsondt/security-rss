Title: Researchers Flag 300K Banking Trojan Infections from Google Play in 4 Months
Date: 2021-11-29T21:15:35+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2021-11-29-researchers-flag-300k-banking-trojan-infections-from-google-play-in-4-months

[Source](https://threatpost.com/banking-trojan-infections-google-play/176630/){:target="_blank" rel="noopener"}

> Attackers are honing Google Play dropper campaigns, overcoming app store restrictions. [...]
