Title: Feds Offer $10 Million Bounty for DarkSide Info
Date: 2021-11-05T13:03:07+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Critical Infrastructure;Malware
Slug: 2021-11-05-feds-offer-10-million-bounty-for-darkside-info

[Source](https://threatpost.com/feds-offer-10-million-bounty-on-darkside-info/176030/){:target="_blank" rel="noopener"}

> The U.S. State Department ups the ante in its hunt for the ransomware perpetrators by offering a sizeable cash sum for locating and arresting leaders of the cybercriminal group. [...]
