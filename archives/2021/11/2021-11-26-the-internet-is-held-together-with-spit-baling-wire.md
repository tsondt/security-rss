Title: The Internet is Held Together With Spit & Baling Wire
Date: 2021-11-26T19:03:53+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Adam Korab;BGP;Border Gateway Protocol;CenturyLink;Internet Routing Registry;IRR;kc claffy;Level3 Communications;Lumen Technologies
Slug: 2021-11-26-the-internet-is-held-together-with-spit-baling-wire

[Source](https://krebsonsecurity.com/2021/11/the-internet-is-held-together-with-spit-baling-wire/){:target="_blank" rel="noopener"}

> A visualization of the Internet made using network routing data. Image: Barrett Lyon, opte.org. Imagine being able to disconnect or redirect Internet traffic destined for some of the world’s biggest companies — just by spoofing an email. This is the nature of a threat vector recently removed by a Fortune 500 firm that operates one of the largest Internet backbones. Based in Monroe, La., Lumen Technologies Inc. [ NYSE: LUMN ] (formerly CenturyLink ) is one of more than two dozen entities that operate what’s known as an Internet Routing Registry (IRR). These IRRs maintain routing databases used by network [...]
