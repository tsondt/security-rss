Title: 7 million Robinhood user email addresses for sale on hacker forum
Date: 2021-11-15T10:52:48-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-15-7-million-robinhood-user-email-addresses-for-sale-on-hacker-forum

[Source](https://www.bleepingcomputer.com/news/security/7-million-robinhood-user-email-addresses-for-sale-on-hacker-forum/){:target="_blank" rel="noopener"}

> The data for approximately 7 million Robinhood customers stolen in a recent data breach are being sold on a popular hacking forum and marketplace. [...]
