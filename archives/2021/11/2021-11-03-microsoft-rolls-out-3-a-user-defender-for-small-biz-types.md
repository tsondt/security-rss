Title: Microsoft rolls out $3-a-user Defender for small biz types
Date: 2021-11-03T19:45:10+00:00
Author: Andrew Rose
Category: The Register
Tags: 
Slug: 2021-11-03-microsoft-rolls-out-3-a-user-defender-for-small-biz-types

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/03/microsoft_defender_for_biz/){:target="_blank" rel="noopener"}

> Endpoint security for firms with under 300 staffers Ignite Sniffing the wind after the large uptick in ransomware attacks across the corporate world, Microsoft said it plans to roll out an SMB version of Defender for Biz.... [...]
