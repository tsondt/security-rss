Title: Ukraine arrests ‘Phoenix’ hackers behind Apple phishing attacks
Date: 2021-11-24T09:57:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Mobile
Slug: 2021-11-24-ukraine-arrests-phoenix-hackers-behind-apple-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/ukraine-arrests-phoenix-hackers-behind-apple-phishing-attacks/){:target="_blank" rel="noopener"}

> The Security Service of Ukraine (SSU) has arrested five members of the international 'Phoenix' hacking group who specialize in the remote hacking of mobile devices. [...]
