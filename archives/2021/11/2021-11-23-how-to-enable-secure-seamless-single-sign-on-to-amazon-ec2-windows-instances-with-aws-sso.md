Title: How to enable secure seamless single sign-on to Amazon EC2 Windows instances with AWS SSO
Date: 2021-11-23T22:03:23+00:00
Author: Todd Rowe
Category: AWS Security
Tags: Amazon EC2;Announcements;AWS Single Sign-On (SSO);Security, Identity, & Compliance;Amazon EC2 instances;AWS SSO;AWS System Manager Fleet Manager
Slug: 2021-11-23-how-to-enable-secure-seamless-single-sign-on-to-amazon-ec2-windows-instances-with-aws-sso

[Source](https://aws.amazon.com/blogs/security/how-to-enable-secure-seamless-single-sign-on-to-amazon-ec2-windows-instances-with-aws-sso/){:target="_blank" rel="noopener"}

> Today, we’re launching new functionality that simplifies the experience to securely access your AWS compute instances running Microsoft Windows. We took on this update to respond to customer feedback around creating a more streamlined experience for administrators and users to more securely access their EC2 Windows instances. The new experience utilizes your existing identity solutions [...]
