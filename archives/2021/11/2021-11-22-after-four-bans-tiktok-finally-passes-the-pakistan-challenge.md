Title: After four bans, TikTok finally passes the Pakistan challenge
Date: 2021-11-22T04:59:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-22-after-four-bans-tiktok-finally-passes-the-pakistan-challenge

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/22/ibm_power8_eol1/){:target="_blank" rel="noopener"}

> Video app promises not to let naughty content cross the border, and to ban those who try Pakistan has allowed TikTok to resume operations on its soil.... [...]
