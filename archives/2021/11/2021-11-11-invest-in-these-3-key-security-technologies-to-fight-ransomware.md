Title: Invest in These 3 Key Security Technologies to Fight Ransomware
Date: 2021-11-11T20:32:39+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: InfoSec Insider;Malware
Slug: 2021-11-11-invest-in-these-3-key-security-technologies-to-fight-ransomware

[Source](https://threatpost.com/invest-3-key-security-technologies-ransomware/176246/){:target="_blank" rel="noopener"}

> Ransomware volumes are up 1000%. Aamir Lakhani, cybersecurity researcher and practitioner at FortiGuard Labs, discusses secure email, network segmentation and sandboxing for defense. [...]
