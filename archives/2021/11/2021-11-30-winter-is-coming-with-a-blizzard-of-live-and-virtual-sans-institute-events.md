Title: Winter is coming … with a blizzard of live and virtual SANS Institute events
Date: 2021-11-30T07:30:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-11-30-winter-is-coming-with-a-blizzard-of-live-and-virtual-sans-institute-events

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/30/sans_training/){:target="_blank" rel="noopener"}

> Or a long, hot summer of cybersec training, depending where you are Paid Post As security pros survey the months ahead, they may be resigning themselves to long winter nights or hot summer days spent fighting off cyber-attackers exploiting the end-of-year lull, or final rush of custom, depending on the business involved.... [...]
