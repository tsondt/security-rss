Title: “Crypto” Means “Cryptography,” not “Cryptocurrency”
Date: 2021-11-22T14:40:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;cryptography
Slug: 2021-11-22-crypto-means-cryptography-not-cryptocurrency

[Source](https://www.schneier.com/blog/archives/2021/11/crypto-means-cryptography-not-cryptocurrency.html){:target="_blank" rel="noopener"}

> I have long been annoyed that the word “crypto” has been co-opted by the blockchain people, and no longer refers to “cryptography.” I’m not the only one. [...]
