Title: WordPress security plugin Hide My WP addresses SQL injection, deactivation flaws
Date: 2021-11-25T14:02:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-25-wordpress-security-plugin-hide-my-wp-addresses-sql-injection-deactivation-flaws

[Source](https://portswigger.net/daily-swig/wordpress-security-plugin-hide-my-wp-addresses-sql-injection-deactivation-flaws){:target="_blank" rel="noopener"}

> Bugs deemed ‘very easy to exploit as they require no prerequisites’ [...]
