Title: UK.gov emits draft IoT and smartphone security law for Parliamentary scrutiny
Date: 2021-11-25T09:30:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-25-ukgov-emits-draft-iot-and-smartphone-security-law-for-parliamentary-scrutiny

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/25/product_security_telecoms_bill_parliament/){:target="_blank" rel="noopener"}

> Mandatory vuln reporting, hefty fines for non-compliance A new British IoT product security law is racing through the House of Commons, with the government boasting it will outlaw default admin passwords and more.... [...]
