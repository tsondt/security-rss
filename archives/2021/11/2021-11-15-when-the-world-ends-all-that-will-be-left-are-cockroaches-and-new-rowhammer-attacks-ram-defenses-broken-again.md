Title: When the world ends, all that will be left are cockroaches and new Rowhammer attacks: RAM defenses broken again
Date: 2021-11-15T21:46:49+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-15-when-the-world-ends-all-that-will-be-left-are-cockroaches-and-new-rowhammer-attacks-ram-defenses-broken-again

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/15/rowhammer_blacksmith_memory/){:target="_blank" rel="noopener"}

> Blacksmith is latest hammer horror Boffins at ETH Zurich, Vrije Universiteit Amsterdam, and Qualcomm Technologies have found that varying the order, regularity, and intensity of rowhammer attacks on memory chips can defeat defenses, thereby compromising security on any device with DRAM.... [...]
