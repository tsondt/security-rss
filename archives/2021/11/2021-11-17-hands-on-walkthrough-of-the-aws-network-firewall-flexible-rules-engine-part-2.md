Title: Hands-on walkthrough of the AWS Network Firewall flexible rules engine – Part 2
Date: 2021-11-17T23:12:18+00:00
Author: Shiva Vaidyanathan
Category: AWS Security
Tags: Amazon CloudWatch;AWS Network Firewall;Security, Identity, & Compliance;AWS Cloudwatch;AWS Lambda;Security;Simple Storage Service
Slug: 2021-11-17-hands-on-walkthrough-of-the-aws-network-firewall-flexible-rules-engine-part-2

[Source](https://aws.amazon.com/blogs/security/hands-on-walkthrough-of-the-aws-network-firewall-flexible-rules-engine-part-2/){:target="_blank" rel="noopener"}

> This blog post is Part 2 of Hands-on walkthrough of the AWS Network Firewall flexible rules engine – Part 1. To recap, AWS Network Firewall is a managed service that offers a flexible rules engine that gives you the ability to write firewall rules for granular policy enforcement. In Part 1, we shared how to [...]
