Title: GoDaddy’s Latest Breach Affects 1.2M Customers
Date: 2021-11-22T22:03:30+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Privacy;Web Security
Slug: 2021-11-22-godaddys-latest-breach-affects-12m-customers

[Source](https://threatpost.com/godaddys-latest-breach-customers/176530/){:target="_blank" rel="noopener"}

> The kingpin domain registrar has logged its fifth cyber-incident since 2018, after an attacker with a compromised password stole email addresses, SSH keys and database logins. [...]
