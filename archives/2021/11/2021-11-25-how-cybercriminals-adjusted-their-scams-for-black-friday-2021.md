Title: How cybercriminals adjusted their scams for Black Friday 2021
Date: 2021-11-25T14:30:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-25-how-cybercriminals-adjusted-their-scams-for-black-friday-2021

[Source](https://www.bleepingcomputer.com/news/security/how-cybercriminals-adjusted-their-scams-for-black-friday-2021/){:target="_blank" rel="noopener"}

> Black Friday is approaching, and while shoppers prepare to open their wallets, cybercriminals hone their malware droppers, phishing lures, and fake sites. [...]
