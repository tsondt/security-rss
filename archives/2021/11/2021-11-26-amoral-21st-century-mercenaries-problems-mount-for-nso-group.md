Title: ‘Amoral 21st-century mercenaries’: problems mount for NSO Group
Date: 2021-11-26T15:37:53+00:00
Author: Stephanie Kirchgaessner in Washington DC
Category: The Guardian
Tags: Surveillance;Israel;Malware;Data and computer security;Middle East and North Africa;US foreign policy;Technology;World news;US news
Slug: 2021-11-26-amoral-21st-century-mercenaries-problems-mount-for-nso-group

[Source](https://www.theguardian.com/world/2021/nov/26/amoral-21st-century-mercenaries-problems-mount-nso-group){:target="_blank" rel="noopener"}

> Israeli spyware firm’s problems go from bad to worse as scathing Apple lawsuit follows US blacklisting Shalev Hulio, the co-founder of Israel’s NSO Group, was in Washington DC on a mission to try to resuscitate the surveillance company’s battered reputation on Capitol Hill shortly before the news broke that he had probably arrived too late to make a difference. With little advance warning to its allies in Israel, the Biden administration announced on 3 November that it was putting the spyware maker – one of the most sophisticated cyber-weapons companies in the world – on a US blacklist, citing use [...]
