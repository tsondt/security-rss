Title: US sanctions Chatex cryptoexchange used by ransomware gangs
Date: 2021-11-08T13:26:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-08-us-sanctions-chatex-cryptoexchange-used-by-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-chatex-cryptoexchange-used-by-ransomware-gangs/){:target="_blank" rel="noopener"}

> The US Treasury Department announced today sanctions against the Chatex cryptocurrency exchange for helping ransomware gangs evade sanctions and facilitating ransom transactions. [...]
