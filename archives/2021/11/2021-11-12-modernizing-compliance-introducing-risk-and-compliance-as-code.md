Title: Modernizing compliance: Introducing Risk and Compliance as Code
Date: 2021-11-12T17:00:00+00:00
Author: Anton Chuvakin
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-11-12-modernizing-compliance-introducing-risk-and-compliance-as-code

[Source](https://cloud.google.com/blog/products/identity-security/risk-and-compliance-as-code/){:target="_blank" rel="noopener"}

> Almost all publicly reported breaches in the cloud stem from misconfigurations, rather than from attacks that compromise underlying cloud infrastructure. Misconfigurations continue to be a source of security risk because most security and compliance practices play catchup - teams are involved later in the CI/CD process and misconfigurations are identified at runtime, instead of during the build process. Reliance on runtime security also creates friction between developers and security professionals because runtime tools, by their nature, are deployed at the end of the CI/CD process, and are therefore often seen as the final gate or blocker to production. To prevent [...]
