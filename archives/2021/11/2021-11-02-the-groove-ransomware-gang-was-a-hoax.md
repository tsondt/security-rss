Title: The ‘Groove’ Ransomware Gang Was a Hoax
Date: 2021-11-02T15:34:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Babuk;BlackMatter;Boriselcin;Catalin Cimpanu;Flashpoint;Fortinet VPN;Groove ransom;mcafee;RAMP forum;The Record;Tom Hoffman;XSS
Slug: 2021-11-02-the-groove-ransomware-gang-was-a-hoax

[Source](https://krebsonsecurity.com/2021/11/the-groove-ransomware-gang-was-a-hoax/){:target="_blank" rel="noopener"}

> A number of publications in September warned about the emergence of “ Groove,” a new ransomware group that called on competing extortion gangs to unite in attacking U.S. government interests online. It now appears that Groove was all a big hoax designed to toy with security firms and journalists. “An appeal to business brothers!” reads the Oct. 22 post from Groove calling for attacks on the United States government sector. Groove was first announced Aug. 22 on RAMP, a new and fairly exclusive Russian-language darknet cybercrime forum. “GROOVE is first and foremost an aggressive financially motivated criminal organization dealing in [...]
