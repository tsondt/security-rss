Title: Costco discloses data breach after finding credit card skimmer
Date: 2021-11-12T10:11:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-12-costco-discloses-data-breach-after-finding-credit-card-skimmer

[Source](https://www.bleepingcomputer.com/news/security/costco-discloses-data-breach-after-finding-credit-card-skimmer/){:target="_blank" rel="noopener"}

> Costco Wholesale Corporation has warned customers in notification letters sent this month that their payment card information might have been stolen while recently shopping at one of its stores. [...]
