Title: US defense contractor Electronic Warfare hit by data breach
Date: 2021-11-05T10:59:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-05-us-defense-contractor-electronic-warfare-hit-by-data-breach

[Source](https://www.bleepingcomputer.com/news/security/us-defense-contractor-electronic-warfare-hit-by-data-breach/){:target="_blank" rel="noopener"}

> US defense contractor Electronic Warfare Associates (EWA) has disclosed a data breach after threat actors hacked their email system and stole files containing personal information. [...]
