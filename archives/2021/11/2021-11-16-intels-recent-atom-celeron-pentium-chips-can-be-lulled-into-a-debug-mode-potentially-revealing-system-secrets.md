Title: Intel's recent Atom, Celeron, Pentium chips can be lulled into a debug mode, potentially revealing system secrets
Date: 2021-11-16T08:29:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-16-intels-recent-atom-celeron-pentium-chips-can-be-lulled-into-a-debug-mode-potentially-revealing-system-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/16/intels_chip_flaw/){:target="_blank" rel="noopener"}

> Testing times for Chipzilla as it emits patches to protect PCs, equipment Certain Intel processors can be slipped into a test mode, granting access to low-level keys that can be used to, say, unlock encrypted data stored in a stolen laptop or some other device.... [...]
