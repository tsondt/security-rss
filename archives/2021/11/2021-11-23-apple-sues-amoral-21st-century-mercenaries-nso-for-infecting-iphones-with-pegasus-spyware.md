Title: Apple sues 'amoral 21st century mercenaries' NSO for infecting iPhones with Pegasus spyware
Date: 2021-11-23T20:58:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-11-23-apple-sues-amoral-21st-century-mercenaries-nso-for-infecting-iphones-with-pegasus-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/23/apple_nso_group/){:target="_blank" rel="noopener"}

> iGiant pledges any damages plus $10m to anti-cybersurveillance groups Apple today sued NSO Group, which sells spyware to governments and other organizations, for infecting and snooping on people's iPhones.... [...]
