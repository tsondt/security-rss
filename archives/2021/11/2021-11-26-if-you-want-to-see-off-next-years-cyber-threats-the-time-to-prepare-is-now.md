Title: If you want to see off next year’s cyber-threats, the time to prepare is … now
Date: 2021-11-26T07:25:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-11-26-if-you-want-to-see-off-next-years-cyber-threats-the-time-to-prepare-is-now

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/26/sophos_cybersecurity_summit_2021/){:target="_blank" rel="noopener"}

> Fast forward into 2022 with Sophos’ Cybersecurity Summit 2021 Paid Post Whatever sector you’re in, 2022 is likely to mean more and nastier cyber-threats.... [...]
