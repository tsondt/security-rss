Title: Congress Mulls Ban on Big Ransom Payouts Unless Victims Get Official Say-So
Date: 2021-11-11T17:54:28+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Critical Infrastructure;Government;Malware;Web Security
Slug: 2021-11-11-congress-mulls-ban-on-big-ransom-payouts-unless-victims-get-official-say-so

[Source](https://threatpost.com/congress-ban-ransomware-payouts/176213/){:target="_blank" rel="noopener"}

> A bill introduced this week would regulate ransomware response by the country's critical financial sector. [...]
