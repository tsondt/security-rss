Title: Kaspersky's stolen Amazon SES token used in Office 365 phishing
Date: 2021-11-01T13:25:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-01-kasperskys-stolen-amazon-ses-token-used-in-office-365-phishing

[Source](https://www.bleepingcomputer.com/news/security/kasperskys-stolen-amazon-ses-token-used-in-office-365-phishing/){:target="_blank" rel="noopener"}

> Kaspersky said today that a legitimate Amazon Simple Email Service (SES) token issued to a third-party contractor was recently used by threat actors behind a spear-phishing campaign targeting Office 365 users. [...]
