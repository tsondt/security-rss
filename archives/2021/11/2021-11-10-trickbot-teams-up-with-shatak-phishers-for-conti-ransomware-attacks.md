Title: TrickBot teams up with Shatak phishers for Conti ransomware attacks
Date: 2021-11-10T10:52:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-10-trickbot-teams-up-with-shatak-phishers-for-conti-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/trickbot-teams-up-with-shatak-phishers-for-conti-ransomware-attacks/){:target="_blank" rel="noopener"}

> A threat actor tracked as Shatak (TA551) recently partnered with the ITG23 gang (aka TrickBot and Wizard Spider) to deploy Conti ransomware on targeted systems. [...]
