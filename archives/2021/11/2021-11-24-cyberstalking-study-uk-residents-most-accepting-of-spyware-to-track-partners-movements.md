Title: Cyberstalking study: UK residents most accepting of spyware to track partners’ movements
Date: 2021-11-24T14:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-24-cyberstalking-study-uk-residents-most-accepting-of-spyware-to-track-partners-movements

[Source](https://portswigger.net/daily-swig/cyberstalking-study-uk-residents-most-accepting-of-spyware-to-track-partners-movements){:target="_blank" rel="noopener"}

> Report from cybersecurity firm Kaspersky reveals worrying attitudes towards spyware usage [...]
