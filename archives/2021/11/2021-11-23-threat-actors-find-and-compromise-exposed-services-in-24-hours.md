Title: Threat actors find and compromise exposed services in 24 hours
Date: 2021-11-23T16:35:51-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-23-threat-actors-find-and-compromise-exposed-services-in-24-hours

[Source](https://www.bleepingcomputer.com/news/security/threat-actors-find-and-compromise-exposed-services-in-24-hours/){:target="_blank" rel="noopener"}

> Researchers set up 320 honeypots to see how quickly threat actors would target exposed cloud services and report that 80% of them were compromised in under 24 hours. [...]
