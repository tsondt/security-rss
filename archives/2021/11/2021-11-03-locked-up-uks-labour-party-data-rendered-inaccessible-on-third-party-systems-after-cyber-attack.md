Title: Locked up: UK's Labour Party data 'rendered inaccessible' on third-party systems after cyber attack
Date: 2021-11-03T15:06:09+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2021-11-03-locked-up-uks-labour-party-data-rendered-inaccessible-on-third-party-systems-after-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/03/labour_party_data_rendered_inaccessible/){:target="_blank" rel="noopener"}

> As membership website goes TITSUP* The UK's Labour Party, the official opposition to the country's ruling Conservatives, has suffered a humiliating data breach.... [...]
