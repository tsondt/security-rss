Title: Back-to-Back PlayStation 5 Hacks Hit on the Same Day
Date: 2021-11-11T20:06:21+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;IoT;Vulnerabilities
Slug: 2021-11-11-back-to-back-playstation-5-hacks-hit-on-the-same-day

[Source](https://threatpost.com/playstation-5-hacks-same-day/176240/){:target="_blank" rel="noopener"}

> Cyberattackers stole PS5 root keys and exploited the kernel, revealing rampant insecurity in gaming devices. [...]
