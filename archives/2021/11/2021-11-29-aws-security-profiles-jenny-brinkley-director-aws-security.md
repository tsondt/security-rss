Title: AWS Security Profiles: Jenny Brinkley, Director, AWS Security
Date: 2021-11-29T18:23:21+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: AWS re:Invent;Foundational (100);Security, Identity, & Compliance;AWS Security and Compliance Podcast;reInvent
Slug: 2021-11-29-aws-security-profiles-jenny-brinkley-director-aws-security

[Source](https://aws.amazon.com/blogs/security/aws-security-profiles-jenny-brinkley-director-aws-security/){:target="_blank" rel="noopener"}

> In the week leading up to AWS re:Invent 2021, we’ll share conversations we’ve had with people at AWS who will be presenting, and get a sneak peek at their work. How long have you been at AWS, and what do you do in your current role? I’ve been at AWS for 51⁄2 years. I get [...]
