Title: Cisco patches critical bug trio in Policy Suite and ONT networking devices
Date: 2021-11-05T14:37:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-05-cisco-patches-critical-bug-trio-in-policy-suite-and-ont-networking-devices

[Source](https://portswigger.net/daily-swig/cisco-patches-critical-bug-trio-in-policy-suite-and-ont-networking-devices){:target="_blank" rel="noopener"}

> Critical severity bugs disclosed by networking titan [...]
