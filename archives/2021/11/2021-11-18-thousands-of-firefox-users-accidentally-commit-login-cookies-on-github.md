Title: Thousands of Firefox users accidentally commit login cookies on GitHub
Date: 2021-11-18T20:04:30+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-18-thousands-of-firefox-users-accidentally-commit-login-cookies-on-github

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/18/firefox_cookies_github/){:target="_blank" rel="noopener"}

> GitHub: 'Credentials exposed by our users are not in scope' Thousands of Firefox cookie databases containing sensitive data are available on request from GitHub repositories, data potentially usable for hijacking authenticated sessions.... [...]
