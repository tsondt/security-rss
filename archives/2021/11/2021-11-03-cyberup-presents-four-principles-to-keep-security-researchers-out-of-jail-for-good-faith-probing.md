Title: CyberUp presents four principles to keep security researchers out of jail for good-faith probing
Date: 2021-11-03T09:33:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-03-cyberup-presents-four-principles-to-keep-security-researchers-out-of-jail-for-good-faith-probing

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/03/computer_misuse_act_defence_principles_cyberup/){:target="_blank" rel="noopener"}

> Computer Misuse Act campaign gets down to brass tacks Campaigners want a new code of practice alongside a proposed public interest defence for the Computer Misuse Act 1990, in the hope it will protect infosec pros from false threats of prosecution.... [...]
