Title: Hiding Vulnerabilities in Source Code
Date: 2021-11-01T15:58:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;operating systems;security engineering;steganography;usability;vulnerabilities
Slug: 2021-11-01-hiding-vulnerabilities-in-source-code

[Source](https://www.schneier.com/blog/archives/2021/11/hiding-vulnerabilities-in-source-code.html){:target="_blank" rel="noopener"}

> Really interesting research demonstrating how to hide vulnerabilities in source code by manipulating how Unicode text is displayed. It’s really clever, and not the sort of attack one would normally think about. From Ross Anderson’s blog : We have discovered ways of manipulating the encoding of source code files so that human viewers and compilers see different logic. One particularly pernicious method uses Unicode directionality override characters to display code as an anagram of its true logic. We’ve verified that this attack works against C, C++, C#, JavaScript, Java, Rust, Go, and Python, and suspect that it will work against [...]
