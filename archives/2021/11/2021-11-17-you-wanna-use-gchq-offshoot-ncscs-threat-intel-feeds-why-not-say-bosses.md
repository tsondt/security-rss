Title: You wanna use GCHQ offshoot NCSC's threat intel feeds? Why not, say bosses
Date: 2021-11-17T11:15:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-17-you-wanna-use-gchq-offshoot-ncscs-threat-intel-feeds-why-not-say-bosses

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/17/ncsc_annual_review/){:target="_blank" rel="noopener"}

> Annual review boasts of fending off health org attacks Britain's National Cyber Security Centre is prepared to share its cyber defence tech and threat intel feeds with British organisations in need of extra help, it said at the launch of its annual review today.... [...]
