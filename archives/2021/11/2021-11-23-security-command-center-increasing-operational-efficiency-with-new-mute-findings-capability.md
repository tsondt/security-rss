Title: Security Command Center - Increasing operational efficiency with new mute findings capability
Date: 2021-11-23T17:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-11-23-security-command-center-increasing-operational-efficiency-with-new-mute-findings-capability

[Source](https://cloud.google.com/blog/products/identity-security/announcing-mute-findings-capability-security-command-center/){:target="_blank" rel="noopener"}

> Security Command Center (SCC) is Google Cloud’s security and risk management platform that helps manage and improve your cloud security and risk posture. It is used by organizations globally to protect their environments providing visibility into cloud assets, discovering misconfigurations and vulnerabilities, detecting threats, and helping to maintain compliance with industry standards and benchmarks. SCC is constantly evolving, adding new capabilities to make your security operations and management processes more efficient. To help, we’re excited to announce a new “Mute Findings” capability in SCC that helps you more effectively manage findings based on your organization’s policies and requirements. SCC presents [...]
