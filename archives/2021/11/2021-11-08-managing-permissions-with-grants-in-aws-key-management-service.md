Title: Managing permissions with grants in AWS Key Management Service
Date: 2021-11-08T22:07:30+00:00
Author: Rick Yin
Category: AWS Security
Tags: Advanced (300);AWS Key Management Service;Security, Identity, & Compliance;authorization;AWS KMS;Grants;Performance;Scaling;Security Blog
Slug: 2021-11-08-managing-permissions-with-grants-in-aws-key-management-service

[Source](https://aws.amazon.com/blogs/security/managing-permissions-with-grants-in-aws-key-management-service/){:target="_blank" rel="noopener"}

> AWS Key Management Service (AWS KMS) helps customers to use encryption to secure their data. When creating a new encrypted Amazon Web Services (AWS) resource, such as an Amazon Relational Database Service (Amazon RDS) database or an Amazon Simple Storage Service (Amazon S3) bucket, all you have to do is provide an AWS KMS key [...]
