Title: Keeping an eye on critical infrastructure and industrial systems? So are legions of cyber-criminals
Date: 2021-11-03T06:30:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-11-03-keeping-an-eye-on-critical-infrastructure-and-industrial-systems-so-are-legions-of-cyber-criminals

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/03/critical_infrastructure_security/){:target="_blank" rel="noopener"}

> If you want to keep ahead of them, watch our broadcast on-demand now or whenever you want Webinar Ransomware attacks on industrial environments have increased by 500 per cent in three years, and it’s unlikely the criminals responsible are going to slow down anytime soon.... [...]
