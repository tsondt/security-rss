Title: Investment app Robinhood: Extortionist tricked our support desk and made off with customer information
Date: 2021-11-09T01:50:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-09-investment-app-robinhood-extortionist-tricked-our-support-desk-and-made-off-with-customer-information

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/09/robinhood_breach/){:target="_blank" rel="noopener"}

> Robinhood, Robinhood, had a data leak. Robinhood, Robinhood, infosec was weak Investment app Robinhood has revealed an extortionist accessed its internal systems and siphoned off customer data after tricking a support desk worker.... [...]
