Title: Removing need to unlock mobile wallets for contactless payments has eroded security protections, researchers warn
Date: 2021-11-15T14:52:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-15-removing-need-to-unlock-mobile-wallets-for-contactless-payments-has-eroded-security-protections-researchers-warn

[Source](https://portswigger.net/daily-swig/removing-need-to-unlock-mobile-wallets-for-contactless-payments-has-eroded-security-protections-researchers-warn){:target="_blank" rel="noopener"}

> Mind the gap [...]
