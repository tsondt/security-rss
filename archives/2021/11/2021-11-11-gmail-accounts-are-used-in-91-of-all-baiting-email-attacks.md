Title: Gmail accounts are used in 91% of all baiting email attacks
Date: 2021-11-11T03:32:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-11-gmail-accounts-are-used-in-91-of-all-baiting-email-attacks

[Source](https://www.bleepingcomputer.com/news/security/gmail-accounts-are-used-in-91-percent-of-all-baiting-email-attacks/){:target="_blank" rel="noopener"}

> Bait attacks are on the rise, and it appears that actors who distribute this special kind of phishing emails prefer to use Gmail accounts to conduct their attacks. [...]
