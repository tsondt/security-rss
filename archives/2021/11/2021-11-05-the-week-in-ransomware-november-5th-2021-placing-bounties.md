Title: The Week in Ransomware - November 5th 2021 - Placing bounties
Date: 2021-11-05T18:05:04-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-05-the-week-in-ransomware-november-5th-2021-placing-bounties

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-5th-2021-placing-bounties/){:target="_blank" rel="noopener"}

> Law enforcement continues to keep up the pressure on ransomware operations with infrastructure hacks and million-dollar rewards, leading to the shut down of criminal operations. [...]
