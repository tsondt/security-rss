Title: Not Punny: Angling Direct Breach Cripples Retailer for Days
Date: 2021-11-09T20:26:09+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Web Security
Slug: 2021-11-09-not-punny-angling-direct-breach-cripples-retailer-for-days

[Source](https://threatpost.com/angling-direct-breach-cripples-retailer/176144/){:target="_blank" rel="noopener"}

> A U.K. fishing retailer’s site has been hijacked and redirected to Pornhub. [...]
