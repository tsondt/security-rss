Title: Server-side vulnerabilities in Concrete CMS put thousands of websites under threat
Date: 2021-11-16T14:11:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-16-server-side-vulnerabilities-in-concrete-cms-put-thousands-of-websites-under-threat

[Source](https://portswigger.net/daily-swig/server-side-vulnerabilities-in-concrete-cms-put-thousands-of-websites-under-threat){:target="_blank" rel="noopener"}

> Web admins urged to apply patches now [...]
