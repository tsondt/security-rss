Title: Office 365 Phishing Campaign Uses Kaspersky’s Amazon SES Token
Date: 2021-11-02T00:29:17+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2021-11-02-office-365-phishing-campaign-uses-kasperskys-amazon-ses-token

[Source](https://threatpost.com/office-365-phishing-campaign-kasperskys-amazon-ses-token/175915/){:target="_blank" rel="noopener"}

> It's a legitimate access token, stolen from a third-party contractor, that lets the attackers send phishing emails from kaspersky.com email addresses. [...]
