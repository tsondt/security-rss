Title: Interpol arrests over 1,000 suspects linked to cyber crime
Date: 2021-11-26T09:21:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2021-11-26-interpol-arrests-over-1000-suspects-linked-to-cyber-crime

[Source](https://www.bleepingcomputer.com/news/legal/interpol-arrests-over-1-000-suspects-linked-to-cyber-crime/){:target="_blank" rel="noopener"}

> Interpol has coordinated the arrest of 1,003 individuals linked to various cyber-crimes such as romance scams, investment frauds, online money laundering, and illegal online gambling. [...]
