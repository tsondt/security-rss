Title: These are the cryptomixers hackers use to clean their ransoms
Date: 2021-11-16T12:01:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-16-these-are-the-cryptomixers-hackers-use-to-clean-their-ransoms

[Source](https://www.bleepingcomputer.com/news/security/these-are-the-cryptomixers-hackers-use-to-clean-their-ransoms/){:target="_blank" rel="noopener"}

> Cryptomixers have always been at the epicenter of cybercrime activity, allowing hackers to "clean" cryptocurrency stolen from victims and making it hard for law enforcement to track them. [...]
