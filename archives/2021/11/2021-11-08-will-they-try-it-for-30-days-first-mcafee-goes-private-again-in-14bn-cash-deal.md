Title: Will they try it for 30 days first? McAfee goes private again in $14bn cash deal
Date: 2021-11-08T20:59:49+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-11-08-will-they-try-it-for-30-days-first-mcafee-goes-private-again-in-14bn-cash-deal

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/08/in_brief_security_mcafee/){:target="_blank" rel="noopener"}

> Plus: Uncle Sam gets tough on patching, NIST needs you, and more In brief A consortium of private equity types have stumped up $12bn in cash to acquire what's left of McAfee the company plus another couple of billion to pay off its debts.... [...]
