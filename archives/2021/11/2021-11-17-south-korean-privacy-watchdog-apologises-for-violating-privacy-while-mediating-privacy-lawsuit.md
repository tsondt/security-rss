Title: South Korean privacy watchdog apologises for violating privacy while mediating privacy lawsuit
Date: 2021-11-17T05:15:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-11-17-south-korean-privacy-watchdog-apologises-for-violating-privacy-while-mediating-privacy-lawsuit

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/17/oops_south_korean_privacy_watchdog/){:target="_blank" rel="noopener"}

> You had one job... South Korea's privacy watchdog leaked personal information relating to participants in a case that sought to probe Facebook's leak of personal information.... [...]
