Title: Wind turbine giant Vestas' data compromised in cyberattack
Date: 2021-11-22T09:56:39-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-22-wind-turbine-giant-vestas-data-compromised-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/wind-turbine-giant-vestas-data-compromised-in-cyberattack/){:target="_blank" rel="noopener"}

> Vestas Wind Systems, a leader in wind turbine manufacturing, has shut down its IT systems after suffering a cyberattack. [...]
