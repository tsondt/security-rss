Title: Free Discord Nitro Offer Used to Steal Steam Credentials
Date: 2021-11-04T16:18:43+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2021-11-04-free-discord-nitro-offer-used-to-steal-steam-credentials

[Source](https://threatpost.com/free-discord-nitro-offer-steam-credentials/176011/){:target="_blank" rel="noopener"}

> A fake Steam pop-up prompts users to ‘link’ Discord account for free Nitro subs. [...]
