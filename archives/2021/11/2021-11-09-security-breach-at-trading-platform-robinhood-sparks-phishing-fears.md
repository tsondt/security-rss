Title: Security breach at trading platform Robinhood sparks phishing fears
Date: 2021-11-09T14:36:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-09-security-breach-at-trading-platform-robinhood-sparks-phishing-fears

[Source](https://portswigger.net/daily-swig/security-breach-at-trading-platform-robinhood-sparks-phishing-fears){:target="_blank" rel="noopener"}

> Social engineering attack exposes email addresses of five million investors [...]
