Title: Alleged Brit SIM-swapper will kill himself if extradited to US for trial, London court told
Date: 2021-11-23T16:10:49+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-23-alleged-brit-sim-swapper-will-kill-himself-if-extradited-to-us-for-trial-london-court-told

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/23/corey_de_rose_us_extradition_hearing/){:target="_blank" rel="noopener"}

> 'Exceptional' case involves 100 BTC payoff, judge told A Briton accused of playing a pivotal role in an $8.5m SIM-swapping attack shouldn't be extradited to the US because he might commit suicide, making his an "exceptional" case, a court was told.... [...]
