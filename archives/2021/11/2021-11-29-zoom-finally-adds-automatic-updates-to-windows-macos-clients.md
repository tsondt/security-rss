Title: Zoom finally adds automatic updates to Windows, macOS clients
Date: 2021-11-29T10:45:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-29-zoom-finally-adds-automatic-updates-to-windows-macos-clients

[Source](https://www.bleepingcomputer.com/news/security/zoom-finally-adds-automatic-updates-to-windows-macos-clients/){:target="_blank" rel="noopener"}

> Zoom has announced today the launch of an automatic update feature designed to streamline the update process for desktop clients. [...]
