Title: REvil Ransom Arrest, $6M Seizure, and $10M Reward
Date: 2021-11-09T02:05:21+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;Ransomware;damnating@yandex.ru;ransomware;rEvil;U.S. Department of State;vkontakte;yarik45@gmail.com;Yevgeniy Igorevich Polyanin
Slug: 2021-11-09-revil-ransom-arrest-6m-seizure-and-10m-reward

[Source](https://krebsonsecurity.com/2021/11/revil-ransom-arrest-6m-seizure-and-10m-reward/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice today announced the arrest of Ukrainian man accused of deploying ransomware on behalf of the REvil ransomware gang, a Russian-speaking cybercriminal collective that has extorted hundreds of millions from victim organizations. The DOJ also said it had seized $6.1 million in cryptocurrency sent to another REvil affiliate, and that the U.S. Department of State is now offering up to $10 million for the name or location any key REvil leaders, and up to $5 million for information on REvil affiliates. If it sounds unlikely that a normal Internet user could make millions of dollars unmasking [...]
