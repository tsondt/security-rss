Title: Apple Sues NSO Group
Date: 2021-11-24T15:29:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;courts;exploits;spyware
Slug: 2021-11-24-apple-sues-nso-group

[Source](https://www.schneier.com/blog/archives/2021/11/apple-sues-nso-group.html){:target="_blank" rel="noopener"}

> Piling more on NSO Group’s legal troubles, Apple is suing it : The complaint provides new information on how NSO Group infected victims’ devices with its Pegasus spyware. To prevent further abuse and harm to its users, Apple is also seeking a permanent injunction to ban NSO Group from using any Apple software, services, or devices. NSO Group’s Pegasus spyware is favored by totalitarian governments around the world, who use it to hack Apple phones and computers. More news : Apple’s legal complaint provides new information on NSO Group’s FORCEDENTRY, an exploit for a now-patched vulnerability previously used to break [...]
