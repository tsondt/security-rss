Title: FBI warns of phishing targeting high-profile brands' customers
Date: 2021-11-23T14:52:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-23-fbi-warns-of-phishing-targeting-high-profile-brands-customers

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-phishing-targeting-high-profile-brands-customers/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned today of recently detected spear-phishing email campaigns targeting customers of "brand-name companies" in attacks known as brand phishing. [...]
