Title: Labour Party supplier ransomware attack: Who holds ex-members' data and on what legal basis?
Date: 2021-11-05T13:00:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-05-labour-party-supplier-ransomware-attack-who-holds-ex-members-data-and-on-what-legal-basis

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/05/labour_party_ransomware_data_breach_questions/){:target="_blank" rel="noopener"}

> 'Anon firm lost your data, don't worry' just makes people more fearful Mystery surrounds the Labour Party ransomware attack, with former party members who left years ago saying their data was caught up in the hack – while official sources refuse to say what really happened.... [...]
