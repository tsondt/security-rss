Title: Microsoft Authenticator gets new enterprise security features
Date: 2021-11-19T10:49:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-19-microsoft-authenticator-gets-new-enterprise-security-features

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-authenticator-gets-new-enterprise-security-features/){:target="_blank" rel="noopener"}

> Microsoft has added new security features for Microsoft Authenticator users that further secure the app and make it easier to roll out in enterprise environments. [...]
