Title: These are the top-level domains threat actors like the most
Date: 2021-11-12T11:04:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-12-these-are-the-top-level-domains-threat-actors-like-the-most

[Source](https://www.bleepingcomputer.com/news/security/these-are-the-top-level-domains-threat-actors-like-the-most/){:target="_blank" rel="noopener"}

> ​Out of over a thousand top-level domain choices, cyber-criminals and threat actors prefer a small set of 25, which accounts for 90% of all malicious sites. [...]
