Title: Philips healthcare infomatics solution vulnerable to SQL injection
Date: 2021-11-05T11:23:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2021-11-05-philips-healthcare-infomatics-solution-vulnerable-to-sql-injection

[Source](https://www.bleepingcomputer.com/news/security/philips-healthcare-infomatics-solution-vulnerable-to-sql-injection/){:target="_blank" rel="noopener"}

> The Philips Tasy EMR, used by hundreds of hospitals as a medical record solution and healthcare management system, is vulnerable to two critical SQL injection flaws. [...]
