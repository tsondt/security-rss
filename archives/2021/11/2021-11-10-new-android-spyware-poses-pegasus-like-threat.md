Title: New Android Spyware Poses Pegasus-Like Threat
Date: 2021-11-10T14:00:26+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2021-11-10-new-android-spyware-poses-pegasus-like-threat

[Source](https://threatpost.com/new-android-spyware-poses-pegasus-like-threat/176155/){:target="_blank" rel="noopener"}

> PhoneSpy already has stolen data and tracked the activity of targets in South Korea, disguising itself as legitimate lifestyle apps. [...]
