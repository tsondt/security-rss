Title: Windows 10 Privilege-Escalation Zero-Day Gets an Unofficial Fix
Date: 2021-11-12T19:49:05+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-11-12-windows-10-privilege-escalation-zero-day-gets-an-unofficial-fix

[Source](https://threatpost.com/windows-10-privilege-escalation-zero-day-unofficial-fix/176313/){:target="_blank" rel="noopener"}

> Researchers warn that CVE-2021-34484 can be exploited with a patch bypass for a bug originally addressed in August by Microsoft. [...]
