Title: Popular 'coa' NPM library hijacked to steal user passwords
Date: 2021-11-04T14:06:01-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-11-04-popular-coa-npm-library-hijacked-to-steal-user-passwords

[Source](https://www.bleepingcomputer.com/news/security/popular-coa-npm-library-hijacked-to-steal-user-passwords/){:target="_blank" rel="noopener"}

> Popular npm library 'coa' was hijacked today with malicious code injected into it, ephemerally impacting React pipelines around the world. The 'coa' library, short for Command-Option-Argument, receives about 9 million weekly downloads on npm, and is used by almost 5 million open source repositories on GitHub. [...]
