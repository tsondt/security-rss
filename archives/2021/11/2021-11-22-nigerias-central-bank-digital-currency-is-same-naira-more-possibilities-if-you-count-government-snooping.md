Title: Nigeria's central bank digital currency is 'same Naira, more possibilities' – if you count government snooping
Date: 2021-11-22T11:00:09+00:00
Author: Dr Kemi Omotubora and Dr Subhajit Basu
Category: The Register
Tags: 
Slug: 2021-11-22-nigerias-central-bank-digital-currency-is-same-naira-more-possibilities-if-you-count-government-snooping

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/22/e_naira_legal_privacy/){:target="_blank" rel="noopener"}

> Privacy challenges and rushed implementation should make this cash alternative much less attractive Opinion Nigeria recently became the first African country to launch its central bank digital currency (CBDC), the eNaira. However, there are significant privacy challenges that could make eNaira a lot less attractive.... [...]
