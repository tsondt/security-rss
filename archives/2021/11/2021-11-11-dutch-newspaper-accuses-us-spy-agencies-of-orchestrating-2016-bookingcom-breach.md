Title: Dutch newspaper accuses US spy agencies of orchestrating 2016 Booking.com breach
Date: 2021-11-11T20:07:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-11-dutch-newspaper-accuses-us-spy-agencies-of-orchestrating-2016-bookingcom-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/11/booking_com_hacked_by_us_allegations/){:target="_blank" rel="noopener"}

> Journalists' book claims company was targeted for Middle Eastern data Jointly US-Dutch owned Booking.com was illegally accessed by an American attacker in 2016 – and the company failed to tell anyone when it became aware of what happened, according to explosive revelations.... [...]
