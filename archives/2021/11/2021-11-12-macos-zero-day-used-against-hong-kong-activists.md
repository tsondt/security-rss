Title: MacOS Zero-Day Used against Hong Kong Activists
Date: 2021-11-12T15:07:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;activism;Apple;China;cybersecurity;exploits;Google;hacking;zero-day
Slug: 2021-11-12-macos-zero-day-used-against-hong-kong-activists

[Source](https://www.schneier.com/blog/archives/2021/11/macos-zero-day-used-against-hong-kong-activists.html){:target="_blank" rel="noopener"}

> Google researchers discovered a MacOS zero-day exploit being used against Hong Kong activists. It was a “watering hole” attack, which means the malware was hidden in a legitimate website. Users visiting that website would get infected. From an article : Google’s researchers were able to trigger the exploits and study them by visiting the websites compromised by the hackers. The sites served both iOS and MacOS exploit chains, but the researchers were only able to retrieve the MacOS one. The zero-day exploit was similar to another in-the-wild vulnerability analyzed by another Google researcher in the past, according to the report. [...]
