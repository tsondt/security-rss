Title: Apple macOS Flaw Allows Kernel-Level Compromise
Date: 2021-11-02T15:50:51+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-11-02-apple-macos-flaw-allows-kernel-level-compromise

[Source](https://threatpost.com/apple-macos-flaw-kernel-compromise/175927/){:target="_blank" rel="noopener"}

> ‘Shrootless’ allows bypass of System Integrity Protection IT security measures to install a malicious rootkit that goes undetected and performs arbitrary device operations. [...]
