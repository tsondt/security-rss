Title: There's no Huawei back now: Biden signs law that forbids US buyers acquiring kit on naughty list
Date: 2021-11-12T04:26:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-11-12-theres-no-huawei-back-now-biden-signs-law-that-forbids-us-buyers-acquiring-kit-on-naughty-list

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/12/biden_signs_the_secure_equipment/){:target="_blank" rel="noopener"}

> FCC Commissioner said the act closes the 'Huawei loophole' US President Joe Biden signed The Secure Equipment Act on Thursday. The legislation prevents US regulators from even considering the issuance of new telecom equipment licenses for companies deemed security threats – which means the likes of China's Huawei and ZTE.... [...]
