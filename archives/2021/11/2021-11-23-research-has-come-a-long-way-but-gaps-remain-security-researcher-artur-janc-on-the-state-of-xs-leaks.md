Title: Research has come a long way, but gaps remain – security researcher Artur Janc on the state of XS-Leaks
Date: 2021-11-23T15:34:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-23-research-has-come-a-long-way-but-gaps-remain-security-researcher-artur-janc-on-the-state-of-xs-leaks

[Source](https://portswigger.net/daily-swig/research-has-come-a-long-way-but-gaps-remain-security-researcher-artur-janc-on-the-state-of-xs-leaks){:target="_blank" rel="noopener"}

> ‘By focusing on XS-Leaks as a fundamental vulnerability class, we help raise their profile and make it easier for developers to understand their impact’ [...]
