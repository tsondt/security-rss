Title: Threat from Organized Cybercrime Syndicates Is Rising
Date: 2021-11-13T00:46:28+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Malware
Slug: 2021-11-13-threat-from-organized-cybercrime-syndicates-is-rising

[Source](https://threatpost.com/organized-cybercrime-syndicates-europol/176326/){:target="_blank" rel="noopener"}

> Europol reports that criminal groups are undermining the EU’s economy and its society, offering everything from murder-for-hire to kidnapping, torture and mutilation. [...]
