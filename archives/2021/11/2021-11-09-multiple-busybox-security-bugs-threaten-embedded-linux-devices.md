Title: Multiple BusyBox Security Bugs Threaten Embedded Linux Devices
Date: 2021-11-09T14:00:36+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;IoT;Vulnerabilities
Slug: 2021-11-09-multiple-busybox-security-bugs-threaten-embedded-linux-devices

[Source](https://threatpost.com/busybox-security-bugs-linux-devices/176098/){:target="_blank" rel="noopener"}

> Researchers discovered 14 vulnerabilities in the ‘Swiss Army Knife’ of the embedded OS used in many OT and IoT environments. They allow RCE, denial of service and data leaks. [...]
