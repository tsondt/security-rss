Title: Predicting the Next OWASP API Security Top 10
Date: 2021-11-03T17:05:37+00:00
Author: Jason Kent
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Vulnerabilities;Web Security
Slug: 2021-11-03-predicting-the-next-owasp-api-security-top-10

[Source](https://threatpost.com/owasp-api-security-top-10/175961/){:target="_blank" rel="noopener"}

> API security risk has dramatically evolved in the last two years. Jason Kent, Hacker-in-Residence at Cequence Security, discusses the top API security concerns today and how to address them. [...]
