Title: Google Ads for Faux Cryptowallets Net Scammers At Least $500K
Date: 2021-11-05T15:51:25+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cryptography;Web Security
Slug: 2021-11-05-google-ads-for-faux-cryptowallets-net-scammers-at-least-500k

[Source](https://threatpost.com/google-ads-cryptowallets-scammers/176047/){:target="_blank" rel="noopener"}

> Malicious Phantom, MetaMask cryptowallets are on the prowl to drain victim funds. [...]
