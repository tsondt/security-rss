Title: Microsoft announces new endpoint security solution for SMBs
Date: 2021-11-02T11:37:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-02-microsoft-announces-new-endpoint-security-solution-for-smbs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-announces-new-endpoint-security-solution-for-smbs/){:target="_blank" rel="noopener"}

> Microsoft today announced a new endpoint security solution dubbed Microsoft Defender for Business, specially built for small and medium-sized businesses. [...]
