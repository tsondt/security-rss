Title: On Cell Phone Metadata
Date: 2021-11-02T11:28:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;identification;tracking
Slug: 2021-11-02-on-cell-phone-metadata

[Source](https://www.schneier.com/blog/archives/2021/11/on-cell-phone-metadata.html){:target="_blank" rel="noopener"}

> Interesting Twitter thread on how cell phone metadata can be used to identify and track people who don’t want to be identified and tracked. [...]
