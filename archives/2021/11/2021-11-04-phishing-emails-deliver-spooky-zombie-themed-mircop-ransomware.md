Title: Phishing emails deliver spooky zombie-themed MirCop ransomware
Date: 2021-11-04T15:03:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-04-phishing-emails-deliver-spooky-zombie-themed-mircop-ransomware

[Source](https://www.bleepingcomputer.com/news/security/phishing-emails-deliver-spooky-zombie-themed-mircop-ransomware/){:target="_blank" rel="noopener"}

> A new phishing campaign pretending to be supply lists infects users with the MirCop ransomware that encrypts a target system in under fifteen minutes. [...]
