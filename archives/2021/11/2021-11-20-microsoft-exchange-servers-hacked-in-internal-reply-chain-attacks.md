Title: Microsoft Exchange servers hacked in internal reply-chain attacks
Date: 2021-11-20T12:55:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-20-microsoft-exchange-servers-hacked-in-internal-reply-chain-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-servers-hacked-in-internal-reply-chain-attacks/){:target="_blank" rel="noopener"}

> Threat actors are hacking Microsoft Exchange servers using ProxyShell and ProxyLogon exploits to distribute malware and bypass detection using stolen internal reply-chain emails. [...]
