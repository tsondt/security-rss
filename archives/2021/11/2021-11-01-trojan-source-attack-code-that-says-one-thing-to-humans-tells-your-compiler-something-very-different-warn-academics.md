Title: Trojan Source attack: Code that says one thing to humans tells your compiler something very different, warn academics
Date: 2021-11-01T17:18:36+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-01-trojan-source-attack-code-that-says-one-thing-to-humans-tells-your-compiler-something-very-different-warn-academics

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/01/trojan_source_language_reversal_unicode/){:target="_blank" rel="noopener"}

> Bidirectional character attack – simple and nightmarish Updated The way Unicode's UTF-8 text encoding handles different languages could be misused to write malicious code that says one thing to humans and another to compilers, academics are warning.... [...]
