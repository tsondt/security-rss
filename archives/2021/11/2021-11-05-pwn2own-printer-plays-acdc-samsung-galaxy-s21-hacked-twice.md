Title: Pwn2Own: Printer plays AC/DC, Samsung Galaxy S21 hacked twice
Date: 2021-11-05T13:25:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-05-pwn2own-printer-plays-acdc-samsung-galaxy-s21-hacked-twice

[Source](https://www.bleepingcomputer.com/news/security/pwn2own-printer-plays-ac-dc-samsung-galaxy-s21-hacked-twice/){:target="_blank" rel="noopener"}

> Trend Micro's ZDI has awarded $1,081,250 for 61 zero-days exploited at Pwn2Own Austin 2021, with competitors successfully pwning the Samsung Galaxy S21 again and hacking an HP LaserJet printer to play AC/DC's Thunderstruck on the contest's third day. [...]
