Title: Belarusian hackers claim to have accessed full database of those crossing the country’s borders
Date: 2021-11-17T11:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-17-belarusian-hackers-claim-to-have-accessed-full-database-of-those-crossing-the-countrys-borders

[Source](https://portswigger.net/daily-swig/belarusian-hackers-claim-to-have-accessed-full-database-of-those-crossing-the-countrys-borders){:target="_blank" rel="noopener"}

> ‘Belarus Cyber-Partisans’ say they gained access to all entries in and out of the country over the past 15 years [...]
