Title: Not only MSPs: All cloudy firms are in line for UK security law crackdown
Date: 2021-11-16T15:15:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-16-not-only-msps-all-cloudy-firms-are-in-line-for-uk-security-law-crackdown

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/16/ukgov_dcms_msp_cyber_security_crackdown_widens/){:target="_blank" rel="noopener"}

> Now's a good time to read up on Cyber Essentials Plus A government crackdown on British MSPs' security practices is drawing ever closer after the Department for Digital, Culture, Media and Sport (DCMS) floated plans to make Cyber Assessment Framework compliance mandatory.... [...]
