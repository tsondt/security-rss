Title: Hacking the Sony Playstation 5
Date: 2021-11-10T12:17:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;exploits;gaming consoles;hacking;reverse engineering
Slug: 2021-11-10-hacking-the-sony-playstation-5

[Source](https://www.schneier.com/blog/archives/2021/11/hacking-the-sony-playstation-5.html){:target="_blank" rel="noopener"}

> I just don’t think it’s possible to create a hack-proof computer system, especially when the system is physically in the hands of the hackers. The Sony Playstation 5 is the latest example: Hackers may have just made some big strides towards possibly jailbreaking the PlayStation 5 over the weekend, with the hacking group Fail0verflow claiming to have managed to obtain PS5 root keys allowing them to decrypt the console’s firmware. [...] The two exploits are particularly notable due to the level of access they theoretically give to the PS5’s software. Decrypted firmware ­ which is possible through Fail0verflow’s keys ­ [...]
