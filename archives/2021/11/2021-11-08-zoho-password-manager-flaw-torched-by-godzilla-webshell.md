Title: Zoho Password Manager Flaw Torched by Godzilla Webshell
Date: 2021-11-08T16:38:05+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-11-08-zoho-password-manager-flaw-torched-by-godzilla-webshell

[Source](https://threatpost.com/zoho-password-manager-flaw-godzilla-webshell/176063/){:target="_blank" rel="noopener"}

> Researchers have spotted a second, worldwide campaign exploiting the Zoho zero-day: one that’s breached defense, energy and healthcare organizations. [...]
