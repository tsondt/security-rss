Title: AWS achieves GSMA Security Certification for Europe (Paris) Region
Date: 2021-11-10T19:21:23+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;GSM;Security Blog;Telecom
Slug: 2021-11-10-aws-achieves-gsma-security-certification-for-europe-paris-region

[Source](https://aws.amazon.com/blogs/security/aws-achieves-gsma-security-certification-for-europe-paris-region/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that our Europe (Paris) Region is now certified by the GSM Association (GSMA) under its Security Accreditation Scheme Subscription Management (SAS-SM) with scope Data Center Operations and Management (DCOM). This is an addition to our [...]
