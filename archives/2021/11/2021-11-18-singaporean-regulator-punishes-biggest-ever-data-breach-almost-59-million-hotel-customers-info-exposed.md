Title: Singaporean regulator punishes biggest-ever data breach: almost 5.9 million hotel customers' info exposed
Date: 2021-11-18T04:01:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-11-18-singaporean-regulator-punishes-biggest-ever-data-breach-almost-59-million-hotel-customers-info-exposed

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/18/redoorz_fined_for_massive_data_leak/){:target="_blank" rel="noopener"}

> RedDoorz.com left red-faced after leaving AWS access key in an APK Singapore's Personal Data Protection Commission (PDPC) has issued a fine of SG$74,000 ($54,456) on travel company Commeasure, which operates a travel booking website named RedDoorz that exposed 5.9 million customers' data – the largest data breach handled by the Commission since its inception.... [...]
