Title: Phishing Scam Aims to Hijack TikTok ‘Influencer’ Accounts
Date: 2021-11-17T13:44:29+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks
Slug: 2021-11-17-phishing-scam-aims-to-hijack-tiktok-influencer-accounts

[Source](https://threatpost.com/phishing-scam-tiktok-influencer/176391/){:target="_blank" rel="noopener"}

> Threat actors used malicious emails to target more than 125 people with high-profile TikTok accounts in an attempt to steal info and lock them out. [...]
