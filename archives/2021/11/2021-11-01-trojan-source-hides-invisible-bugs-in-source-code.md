Title: ‘Trojan Source’ Hides Invisible Bugs in Source Code
Date: 2021-11-01T16:28:56+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-11-01-trojan-source-hides-invisible-bugs-in-source-code

[Source](https://threatpost.com/trojan-source-invisible-bugs-source-code/175891/){:target="_blank" rel="noopener"}

> The old RLO trick of exploiting how Unicode handles script ordering and a related homoglyph attack can imperceptibly switch the real name of malware. [...]
