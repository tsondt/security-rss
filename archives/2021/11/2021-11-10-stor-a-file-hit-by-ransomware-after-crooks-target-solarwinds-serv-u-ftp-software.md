Title: Stor-a-File hit by ransomware after crooks target SolarWinds Serv-U FTP software
Date: 2021-11-10T12:28:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-10-stor-a-file-hit-by-ransomware-after-crooks-target-solarwinds-serv-u-ftp-software

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/10/stor_a_file_ransomware_attack_solarwinds_serv_u/){:target="_blank" rel="noopener"}

> New research says it's Clop's favourite attack method du jour Stor-a-File, a British data capture and storage company, suffered a ransomware attack in August that exploited an unpatched instance of SolarWinds' Serv-U FTP software.... [...]
