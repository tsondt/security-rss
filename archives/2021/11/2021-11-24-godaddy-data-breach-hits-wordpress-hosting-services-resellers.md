Title: GoDaddy data breach hits WordPress hosting services resellers
Date: 2021-11-24T10:47:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-24-godaddy-data-breach-hits-wordpress-hosting-services-resellers

[Source](https://www.bleepingcomputer.com/news/security/godaddy-data-breach-hits-wordpress-hosting-services-resellers/){:target="_blank" rel="noopener"}

> GoDaddy says the recently disclosed data breach affecting roughly 1.2 million customers has also hit multiple Managed WordPress services resellers. [...]
