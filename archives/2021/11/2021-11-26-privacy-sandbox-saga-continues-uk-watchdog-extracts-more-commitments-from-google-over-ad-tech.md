Title: Privacy Sandbox saga continues: UK watchdog extracts more commitments from Google over ad tech
Date: 2021-11-26T13:33:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-11-26-privacy-sandbox-saga-continues-uk-watchdog-extracts-more-commitments-from-google-over-ad-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/26/cma_google/){:target="_blank" rel="noopener"}

> Roll up, roll up. Come and be the CMA-approved trustee to keep an eye on the Chocolate Factory's antics The torrid tale of Google's Privacy Sandbox took another turn today with the UK's Competitions and Markets Authority (CMA) saying it has "secured improved commitments" from the ad giant over the cookie crushing tech.... [...]
