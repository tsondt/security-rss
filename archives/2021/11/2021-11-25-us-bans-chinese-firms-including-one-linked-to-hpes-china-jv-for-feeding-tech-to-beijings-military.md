Title: US bans Chinese firms – including one linked to HPE’s China JV – for feeding tech to Beijing's military
Date: 2021-11-25T01:11:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-25-us-bans-chinese-firms-including-one-linked-to-hpes-china-jv-for-feeding-tech-to-beijings-military

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/25/us_chinese_ban/){:target="_blank" rel="noopener"}

> Other additions to Entity List are accused of helping Pakistan, North Korea make nukes, missiles The US Dept of Commerce's Bureau of Industry and Security has added 27 companies to its list of entities prohibited from doing business with the USA on grounds they threaten national security – and one of the firms is associated with HPE’s Chinese joint venture H3C.... [...]
