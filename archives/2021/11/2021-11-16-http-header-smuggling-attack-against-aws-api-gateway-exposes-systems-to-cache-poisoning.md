Title: HTTP header smuggling attack against AWS API Gateway exposes systems to cache poisoning
Date: 2021-11-16T11:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-16-http-header-smuggling-attack-against-aws-api-gateway-exposes-systems-to-cache-poisoning

[Source](https://portswigger.net/daily-swig/http-header-smuggling-attack-against-aws-api-gateway-exposes-systems-to-cache-poisoning){:target="_blank" rel="noopener"}

> New hacking technique may pave the way for other serious attacks [...]
