Title: Wind turbine maker Vestas confirms recent security incident <i>was</i> ransomware
Date: 2021-11-29T14:29:13+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2021-11-29-wind-turbine-maker-vestas-confirms-recent-security-incident-was-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/29/wind_turbine_maker_vestas_confirms/){:target="_blank" rel="noopener"}

> 10 days after attack 'almost all systems' up and running, refuses to say if ransom was paid Wind turbine maker Vestas says "almost all" of its IT systems are finally up and running 10 days after a security attack by criminals, confirming that it had indeed fallen victim to ransomware.... [...]
