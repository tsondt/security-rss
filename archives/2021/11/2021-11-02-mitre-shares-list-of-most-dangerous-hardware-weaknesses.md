Title: MITRE shares list of most dangerous hardware weaknesses
Date: 2021-11-02T08:27:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-02-mitre-shares-list-of-most-dangerous-hardware-weaknesses

[Source](https://www.bleepingcomputer.com/news/security/mitre-shares-list-of-most-dangerous-hardware-weaknesses/){:target="_blank" rel="noopener"}

> MITRE shared a list of the topmost dangerous programming, design, and architecture security flaws plaguing hardware this year. [...]
