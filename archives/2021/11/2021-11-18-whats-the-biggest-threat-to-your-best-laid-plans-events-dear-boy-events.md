Title: What’s the biggest threat to your best-laid plans? Events, dear boy. Events
Date: 2021-11-18T14:00:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-11-18-whats-the-biggest-threat-to-your-best-laid-plans-events-dear-boy-events

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/18/webinar_resileient_response_plan/){:target="_blank" rel="noopener"}

> Learn how to build a resilient disaster response plan Webinar You can’t predict when a disaster will strike your organisation, whether it’s extreme weather, workplace violence, or a cyber attack.... [...]
