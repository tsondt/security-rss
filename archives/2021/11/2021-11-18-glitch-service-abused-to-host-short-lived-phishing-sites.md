Title: Glitch service abused to host short-lived phishing sites
Date: 2021-11-18T10:38:55-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-18-glitch-service-abused-to-host-short-lived-phishing-sites

[Source](https://www.bleepingcomputer.com/news/security/glitch-service-abused-to-host-short-lived-phishing-sites/){:target="_blank" rel="noopener"}

> Phishing actors are now actively abusing the Glitch platform to host short-lived credential-stealing URLs for free while evading detection and takedowns. [...]
