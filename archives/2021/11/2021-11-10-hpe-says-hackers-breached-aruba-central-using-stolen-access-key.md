Title: HPE says hackers breached Aruba Central using stolen access key
Date: 2021-11-10T17:19:06-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-10-hpe-says-hackers-breached-aruba-central-using-stolen-access-key

[Source](https://www.bleepingcomputer.com/news/security/hpe-says-hackers-breached-aruba-central-using-stolen-access-key/){:target="_blank" rel="noopener"}

> HPE has disclosed that data repositories for their Aruba Central network monitoring platform were compromised, allowing a threat actor to access collected data about monitored devices and their locations. [...]
