Title: Dark web market Cannazon shuts down after massive DDoS attack
Date: 2021-11-29T13:26:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-29-dark-web-market-cannazon-shuts-down-after-massive-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/dark-web-market-cannazon-shuts-down-after-massive-ddos-attack/){:target="_blank" rel="noopener"}

> Cannazon, one of the largest dark web marketplaces for buying marijuana products, shut down last week after suffering a debilitating distributed denial of service attack. [...]
