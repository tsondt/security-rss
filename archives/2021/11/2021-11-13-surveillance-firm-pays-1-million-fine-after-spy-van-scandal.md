Title: Surveillance firm pays $1 million fine after 'spy van' scandal
Date: 2021-11-13T10:01:02-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-13-surveillance-firm-pays-1-million-fine-after-spy-van-scandal

[Source](https://www.bleepingcomputer.com/news/security/surveillance-firm-pays-1-million-fine-after-spy-van-scandal/){:target="_blank" rel="noopener"}

> The Office of the Commissioner for Personal Data Protection in Cyprus has collected a $1 million fine from intelligence company WiSpear for gathering mobile data from various individuals arriving at the airport in Larnaca. [...]
