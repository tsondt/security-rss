Title: How to Defend Against Mobile App Impersonation
Date: 2021-11-23T14:00:01+00:00
Author: David Stewart
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Malware;Mobile Security;Privacy
Slug: 2021-11-23-how-to-defend-against-mobile-app-impersonation

[Source](https://threatpost.com/defend-app-impersonation/176519/){:target="_blank" rel="noopener"}

> Despite tight security measures by Google/Apple, cybercriminals still find ways to bypass fake app checks to plant malware on mobile devices. Dave Stewart, CEO of Approov, discusses technical approaches to defense against this. [...]
