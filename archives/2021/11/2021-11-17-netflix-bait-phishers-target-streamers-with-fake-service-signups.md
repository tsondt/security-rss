Title: Netflix Bait: Phishers Target Streamers with Fake Service Signups
Date: 2021-11-17T21:56:26+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2021-11-17-netflix-bait-phishers-target-streamers-with-fake-service-signups

[Source](https://threatpost.com/netflix-bait-phishers-fake-signups/176422/){:target="_blank" rel="noopener"}

> Lures dressed up to look like movie and TV streaming offers are swiping payment data. [...]
