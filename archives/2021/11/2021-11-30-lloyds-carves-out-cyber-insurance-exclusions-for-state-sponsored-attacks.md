Title: Lloyd’s Carves Out Cyber-Insurance Exclusions for State-Sponsored Attacks
Date: 2021-11-30T20:41:17+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Government;Hacks;Malware;Vulnerabilities;Web Security
Slug: 2021-11-30-lloyds-carves-out-cyber-insurance-exclusions-for-state-sponsored-attacks

[Source](https://threatpost.com/lloyds-cyber-insurance-exclusions/176669/){:target="_blank" rel="noopener"}

> The insurer won’t pay for 'acts of cyber-war' or nation-state retaliation attacks. [...]
