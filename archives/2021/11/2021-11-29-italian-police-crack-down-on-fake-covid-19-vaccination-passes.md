Title: Italian police crack down on fake Covid-19 vaccination passes
Date: 2021-11-29T16:48:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-29-italian-police-crack-down-on-fake-covid-19-vaccination-passes

[Source](https://portswigger.net/daily-swig/italian-police-crack-down-on-fake-covid-19-vaccination-passes){:target="_blank" rel="noopener"}

> Underground trade conducted over Telegram [...]
