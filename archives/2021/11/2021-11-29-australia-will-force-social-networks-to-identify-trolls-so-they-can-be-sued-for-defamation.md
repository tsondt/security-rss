Title: Australia will force social networks to identify trolls, so they can be sued for defamation
Date: 2021-11-29T01:15:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-29-australia-will-force-social-networks-to-identify-trolls-so-they-can-be-sued-for-defamation

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/29/australia_troll_bill/){:target="_blank" rel="noopener"}

> At the same time, will overrule court decision that traditional publishers are liable for comments on social media Australia's government has announced it will compel social media companies to reveal the identities of users who post material considered defamatory.... [...]
