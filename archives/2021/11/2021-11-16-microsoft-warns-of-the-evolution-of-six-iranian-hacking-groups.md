Title: Microsoft warns of the evolution of six Iranian hacking groups
Date: 2021-11-16T13:11:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-11-16-microsoft-warns-of-the-evolution-of-six-iranian-hacking-groups

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-the-evolution-of-six-iranian-hacking-groups/){:target="_blank" rel="noopener"}

> The Microsoft Threat Intelligence Center (MSTIC) has presented an analysis of the evolution of several Iranian threat actors at the CyberWarCon 2021, and their findings show increasingly sophisticated attacks. [...]
