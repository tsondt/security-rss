Title: Attackers Will Flock to Crypto Wallets, Linux in 2022:  Podcast
Date: 2021-11-23T21:09:04+00:00
Author: Threatpost
Category: Threatpost
Tags: Podcasts;Sponsored;Vulnerabilities;Web Security
Slug: 2021-11-23-attackers-will-flock-to-crypto-wallets-linux-in-2022-podcast

[Source](https://threatpost.com/attackers-will-flock-to-crypto-wallets-linux-in-2022-podcast/176546/){:target="_blank" rel="noopener"}

> That’s just the start of what cyberattackers will zero in on as they pick up APT techniques to hurl more destructive ransomware & supply-chain attacks, says Fortinet’s Derek Manky. [...]
