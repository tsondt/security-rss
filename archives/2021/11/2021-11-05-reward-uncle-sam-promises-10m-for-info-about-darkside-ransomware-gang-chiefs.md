Title: Reward! Uncle Sam promises $10m for info about DarkSide ransomware gang chiefs
Date: 2021-11-05T14:30:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-05-reward-uncle-sam-promises-10m-for-info-about-darkside-ransomware-gang-chiefs

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/05/us_darkside_ransomware_10m_bounty/){:target="_blank" rel="noopener"}

> Plus: Interpol boasts of infosec companies' help nabbing Cl0p suspects US authorities are dangling a $10m reward for information on the DarkSide gang, while Interpol says half a dozen people were arrested in Ukraine on suspicion of being part of the Cl0p extortionist crew.... [...]
