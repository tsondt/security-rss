Title: CKEditor vulnerabilities pose XSS threat to Drupal and other downstream applications
Date: 2021-11-19T14:38:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-19-ckeditor-vulnerabilities-pose-xss-threat-to-drupal-and-other-downstream-applications

[Source](https://portswigger.net/daily-swig/ckeditor-vulnerabilities-pose-xss-threat-to-drupal-and-other-downstream-applications){:target="_blank" rel="noopener"}

> Attackers could bypass content sanitization with malformed HTML [...]
