Title: Signal now lets you report and block spam messages
Date: 2021-11-01T17:55:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-01-signal-now-lets-you-report-and-block-spam-messages

[Source](https://www.bleepingcomputer.com/news/security/signal-now-lets-you-report-and-block-spam-messages/){:target="_blank" rel="noopener"}

> Signal has added an easy way for users to report and block spam straight from message request screens with a single mouse click. [...]
