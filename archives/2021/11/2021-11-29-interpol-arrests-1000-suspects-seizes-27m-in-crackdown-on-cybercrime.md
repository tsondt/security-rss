Title: Interpol arrests 1,000 suspects, seizes $27m in crackdown on cybercrime
Date: 2021-11-29T14:35:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-29-interpol-arrests-1000-suspects-seizes-27m-in-crackdown-on-cybercrime

[Source](https://portswigger.net/daily-swig/interpol-arrests-1-000-suspects-seizes-27m-in-crackdown-on-cybercrime){:target="_blank" rel="noopener"}

> Worldwide law enforcement operation targets online crime surge [...]
