Title: AWS Security Profiles: J.D. Bean, Sr. Security Solutions Architect
Date: 2021-11-22T20:12:41+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Learning Levels;Uncategorized;AWS Nitro Enclaves;reInvent
Slug: 2021-11-22-aws-security-profiles-jd-bean-sr-security-solutions-architect

[Source](https://aws.amazon.com/blogs/security/aws-security-profiles-j-d-bean-sr-security-solutions-architect/){:target="_blank" rel="noopener"}

> In the week leading up to AWS re:Invent 2021, we’ll share conversations we’ve had with people at AWS who will be presenting, and get a sneak peek at their work. How long have you been at AWS, and what do you do in your current role? I’m coming up on my three-year anniversary at AWS. [...]
