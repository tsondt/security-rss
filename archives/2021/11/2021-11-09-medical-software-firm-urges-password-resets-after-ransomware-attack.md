Title: Medical software firm urges password resets after ransomware attack
Date: 2021-11-09T09:15:45-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2021-11-09-medical-software-firm-urges-password-resets-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/medical-software-firm-urges-password-resets-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Medatixx, a German medical software vendor whose products are used in over 21,000 health institutions, urges customers to change their application passwords following a ransomware attack that has severely impaired its entire operations. [...]
