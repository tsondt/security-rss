Title: Friday Squid Blogging: Bobtail Squid and Vibrio Bacteria
Date: 2021-11-26T22:05:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-11-26-friday-squid-blogging-bobtail-squid-and-vibrio-bacteria

[Source](https://www.schneier.com/blog/archives/2021/11/friday-squid-blogging-bobtail-squid-and-vibrio-bacteria.html){:target="_blank" rel="noopener"}

> Research on the Vibrio bacteria and its co-evolution with its bobtail squid hosts. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
