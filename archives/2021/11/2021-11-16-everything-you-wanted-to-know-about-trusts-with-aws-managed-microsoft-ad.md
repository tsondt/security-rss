Title: Everything you wanted to know about trusts with AWS Managed Microsoft AD
Date: 2021-11-16T20:02:07+00:00
Author: Jeremy Girven
Category: AWS Security
Tags: AWS Directory Service;Intermediate (200);Security, Identity, & Compliance;Active Directory;AWS Managed Microsoft AD;on-premises;Security Blog;Trusts
Slug: 2021-11-16-everything-you-wanted-to-know-about-trusts-with-aws-managed-microsoft-ad

[Source](https://aws.amazon.com/blogs/security/everything-you-wanted-to-know-about-trusts-with-aws-managed-microsoft-ad/){:target="_blank" rel="noopener"}

> Many Amazon Web Services (AWS) customers use Active Directory to centralize user authentication and authorization for a variety of applications and services. For these customers, Active Directory is a critical piece of their IT infrastructure. AWS offers AWS Directory Service for Microsoft Active Directory, also known as AWS Managed Microsoft AD, to provide a highly [...]
