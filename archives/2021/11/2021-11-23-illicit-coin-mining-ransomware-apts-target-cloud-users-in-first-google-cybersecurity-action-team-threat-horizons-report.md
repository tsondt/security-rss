Title: Illicit coin mining, ransomware, APTs target cloud users in first Google Cybersecurity Action Team Threat Horizons report
Date: 2021-11-23T18:30:00+00:00
Author: Seth Rosenblatt
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-11-23-illicit-coin-mining-ransomware-apts-target-cloud-users-in-first-google-cybersecurity-action-team-threat-horizons-report

[Source](https://cloud.google.com/blog/products/identity-security/coin-mining-ransomware-apts-target-cloud-gcat-report/){:target="_blank" rel="noopener"}

> At Google we have an immense aperture into the global cybersecurity threat landscape and the means to mitigate risks that stem from those threats. With our recently launched Google Cybersecurity Action Team, we are bringing more of our security abilities and advisory services to our customers to increase their defenses. A big part of this is to bridge our collective threat intelligence to yield specific insights, such as when malicious hackers exploit improperly-secured cloud instances to download cryptocurrency mining software to the system—sometimes within 22 seconds of being compromised. This is one of several observations that we have published in [...]
