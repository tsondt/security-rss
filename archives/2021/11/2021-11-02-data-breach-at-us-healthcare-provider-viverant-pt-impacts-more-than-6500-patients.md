Title: Data breach at US healthcare provider Viverant PT impacts more than 6,500 patients
Date: 2021-11-02T14:18:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-02-data-breach-at-us-healthcare-provider-viverant-pt-impacts-more-than-6500-patients

[Source](https://portswigger.net/daily-swig/data-breach-at-us-healthcare-provider-viverant-pt-impacts-more-than-6-500-patients){:target="_blank" rel="noopener"}

> Minnesota healthcare provider hit by cyber-attack A data breach at a physical therapy center based in the US has breached the personal data of more than 6,500 patients. Viverant PT, based in Minneapol [...]
