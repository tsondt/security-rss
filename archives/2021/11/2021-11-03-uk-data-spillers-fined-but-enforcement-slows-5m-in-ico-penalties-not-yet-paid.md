Title: UK data spillers fined, but enforcement slows: £5m in ICO penalties not yet paid
Date: 2021-11-03T13:06:46+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-03-uk-data-spillers-fined-but-enforcement-slows-5m-in-ico-penalties-not-yet-paid

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/03/ico_fines_5m_pounds_outstanding/){:target="_blank" rel="noopener"}

> Nuisance call companies... and others... are quite the nuisance More than half of data protection fines issued by the Information Commissioner's Office over the last two years, totalling more than £5m, have not been paid.... [...]
