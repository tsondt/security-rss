Title: DNA testing firm discloses data breach affecting 2.1 million people
Date: 2021-11-30T08:26:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2021-11-30-dna-testing-firm-discloses-data-breach-affecting-21-million-people

[Source](https://www.bleepingcomputer.com/news/security/dna-testing-firm-discloses-data-breach-affecting-21-million-people/){:target="_blank" rel="noopener"}

> DNA Diagnostics Center (DDC), an Ohio-based DNA testing company, has disclosed a hacking incident that affects 2,102,436 persons. [...]
