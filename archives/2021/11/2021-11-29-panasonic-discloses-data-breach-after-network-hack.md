Title: Panasonic discloses data breach after network hack
Date: 2021-11-29T09:40:21-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-29-panasonic-discloses-data-breach-after-network-hack

[Source](https://www.bleepingcomputer.com/news/security/panasonic-discloses-data-breach-after-network-hack/){:target="_blank" rel="noopener"}

> Japanese multinational conglomerate Panasonic disclosed a security breach after unknown threat actors gained access to servers on its network this month. [...]
