Title: Alan Paller: Infosec world pays homage after SANS founder and infosec luminary dies
Date: 2021-11-12T13:13:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-12-alan-paller-infosec-world-pays-homage-after-sans-founder-and-infosec-luminary-dies

[Source](https://portswigger.net/daily-swig/alan-paller-infosec-world-pays-homage-after-sans-founder-and-infosec-luminary-dies){:target="_blank" rel="noopener"}

> ‘His vision has changed the lives of hundreds of thousands of security practitioners’ [...]
