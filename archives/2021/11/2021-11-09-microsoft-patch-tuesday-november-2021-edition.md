Title: Microsoft Patch Tuesday, November 2021 Edition
Date: 2021-11-09T20:39:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Security Tools;Time to Patch;Allan Liska;CVE-2021-41371;CVE-2021-42292;CVE-2021-42321;CVE-20213-38631;Dustin Childs;Microsoft Excel;Microsoft Exchange Server zero-day;Recorded Future;remote desktop protocol;Trend Micro Zero Day Initiative
Slug: 2021-11-09-microsoft-patch-tuesday-november-2021-edition

[Source](https://krebsonsecurity.com/2021/11/microsoft-patch-tuesday-november-2021-edition/){:target="_blank" rel="noopener"}

> Microsoft Corp. today released updates to quash at least 55 security bugs in its Windows operating systems and other software. Two of the patches address vulnerabilities that are already being used in active attacks online, and four of the flaws were disclosed publicly before today — potentially giving adversaries a head start in figuring out how to exploit them. Among the zero-day bugs is CVE-2021-42292, a “security feature bypass” problem with Microsoft Excel versions 2013-2021 that could allow attackers to install malicious code just by convincing someone to open a booby-trapped Excel file (Microsoft says Mac versions of Office are [...]
