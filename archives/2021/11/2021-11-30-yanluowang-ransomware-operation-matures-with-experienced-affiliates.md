Title: Yanluowang ransomware operation matures with experienced affiliates
Date: 2021-11-30T06:56:06-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-30-yanluowang-ransomware-operation-matures-with-experienced-affiliates

[Source](https://www.bleepingcomputer.com/news/security/yanluowang-ransomware-operation-matures-with-experienced-affiliates/){:target="_blank" rel="noopener"}

> An affiliate of the recently discovered Yanluowang ransomware operation is focusing its attacks on U.S. organizations in the financial sector using BazarLoader malware in the reconnaissance stage. [...]
