Title: Lock up your Office macros: Emotet botnet back from the dead with Trickbot links
Date: 2021-11-16T19:57:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-16-lock-up-your-office-macros-emotet-botnet-back-from-the-dead-with-trickbot-links

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/16/emotet_botnet_rappears/){:target="_blank" rel="noopener"}

> Nice to have nearly a year off from that malspam threat, but now it's returned The Emotet malware delivery botnet is back, almost a year after law enforcement agencies bragged about shutting it down and arresting the operators.... [...]
