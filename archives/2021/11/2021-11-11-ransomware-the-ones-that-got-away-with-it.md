Title: Ransomware: The ones that got away (with it)
Date: 2021-11-11T06:45:14+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2021-11-11-ransomware-the-ones-that-got-away-with-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/11/ransomware/){:target="_blank" rel="noopener"}

> We're talking victims here, not the bad guys Paid feature Well-known ransomware families like Ryuk or REvil make the news every day. Security teams track their development and document the criminal groups behind them. But there are other ransomware groups and tools, constantly evolving as they grapple for a foothold on your systems.... [...]
