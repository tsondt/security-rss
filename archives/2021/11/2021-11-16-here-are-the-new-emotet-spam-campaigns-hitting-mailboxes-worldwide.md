Title: Here are the new Emotet spam campaigns hitting mailboxes worldwide
Date: 2021-11-16T18:07:17-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-16-here-are-the-new-emotet-spam-campaigns-hitting-mailboxes-worldwide

[Source](https://www.bleepingcomputer.com/news/security/here-are-the-new-emotet-spam-campaigns-hitting-mailboxes-worldwide/){:target="_blank" rel="noopener"}

> The Emotet malware kicked into action yesterday after a ten-month hiatus with multiple spam campaigns delivering malicious documents to mailboxes worldwide. [...]
