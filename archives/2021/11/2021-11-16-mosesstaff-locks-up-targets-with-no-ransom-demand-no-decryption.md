Title: MosesStaff Locks Up Targets, with No Ransom Demand, No Decryption
Date: 2021-11-16T18:29:46+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2021-11-16-mosesstaff-locks-up-targets-with-no-ransom-demand-no-decryption

[Source](https://threatpost.com/mosesstaff-locks-targets-ransom-decryption/176366/){:target="_blank" rel="noopener"}

> A politically motivated group is paralyzing Israeli entities with no financial goal – and no intention of handing over decryption keys. [...]
