Title: No day in court: US Foreign Intelligence Surveillance Court rulings will stay a secret
Date: 2021-11-05T16:15:12+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2021-11-05-no-day-in-court-us-foreign-intelligence-surveillance-court-rulings-will-stay-a-secret

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/05/fisc_secrecy_ruling/){:target="_blank" rel="noopener"}

> Eight years after Snowden, you'll never know how much they spy on you... The US Supreme Court this week refused [PDF] to hear a case that would have forced the country's hush-hush Foreign Intelligence Surveillance Court (FISC) to explain its justifications for giving the Feds the right to help themselves to bulk amounts of the public's data.... [...]
