Title: Turbine maker Vestas Wind Systems admits to cyber incident, refuses to confirm if ransomware is at play
Date: 2021-11-22T14:10:14+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2021-11-22-turbine-maker-vestas-wind-systems-admits-to-cyber-incident-refuses-to-confirm-if-ransomware-is-at-play

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/22/vestas_wind_systems/){:target="_blank" rel="noopener"}

> Company data compromised but not systems containing customer or supplier information Vestas Wind Systems, one of the world's largest makers of wind turbines, today confirmed company data has been compromised in a "cyber security incident" that forced the firm to isolate parts of its IT infrastructure.... [...]
