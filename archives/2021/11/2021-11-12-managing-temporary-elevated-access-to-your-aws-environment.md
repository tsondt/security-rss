Title: Managing temporary elevated access to your AWS environment
Date: 2021-11-12T20:14:47+00:00
Author: James Greenwood
Category: AWS Security
Tags: Advanced (300);AWS Identity and Access Management (IAM);Security, Identity, & Compliance;AWS CLI;AWS IAM;AWS Management Console;Identity broker;Privileged access;Security Blog;Temporary elevated access
Slug: 2021-11-12-managing-temporary-elevated-access-to-your-aws-environment

[Source](https://aws.amazon.com/blogs/security/managing-temporary-elevated-access-to-your-aws-environment/){:target="_blank" rel="noopener"}

> In this post you’ll learn about temporary elevated access and how it can mitigate risks relating to human access to your AWS environment. You’ll also be able to download a minimal reference implementation and use it as a starting point to build a temporary elevated access solution tailored for your organization. Introduction While many modern [...]
