Title: Apache Storm maintainers patch two pre-auth RCE vulnerabilities
Date: 2021-11-10T11:46:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-10-apache-storm-maintainers-patch-two-pre-auth-rce-vulnerabilities

[Source](https://portswigger.net/daily-swig/apache-storm-maintainers-patch-two-pre-auth-rce-vulnerabilities){:target="_blank" rel="noopener"}

> High-risk issues were discovered by GitHub’s in-house security team [...]
