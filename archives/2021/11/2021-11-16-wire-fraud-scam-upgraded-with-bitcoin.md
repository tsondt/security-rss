Title: Wire Fraud Scam Upgraded with Bitcoin
Date: 2021-11-16T12:18:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bitcoin;cryptocurrency;FBI;fraud;scams
Slug: 2021-11-16-wire-fraud-scam-upgraded-with-bitcoin

[Source](https://www.schneier.com/blog/archives/2021/11/wire-fraud-scam-upgraded-with-bitcoin.html){:target="_blank" rel="noopener"}

> The FBI has issued a bulletin describing a bitcoin variant of a wire fraud scam : As the agency describes it, the scammer will contact their victim and somehow convince them that they need to send money, either with promises of love, further riches, or by impersonating an actual institution like a bank or utility company. After the mark is convinced, the scammer will have them get cash (sometimes out of investment or retirement accounts), and head to an ATM that sells cryptocurrencies and supports reading QR codes. Once the victim’s there, they’ll scan a QR code that the scammer [...]
