Title: Angling (re)Direct: Criminals net website of Brit fishing tackle retailer, send users straight to smut site
Date: 2021-11-08T14:09:42+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2021-11-08-angling-redirect-criminals-net-website-of-brit-fishing-tackle-retailer-send-users-straight-to-smut-site

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/08/angling_direct/){:target="_blank" rel="noopener"}

> We've signed everyone up for PornHub Premium, crow immature attackers Miscreants have hijacked the systems of Angling Direct, diverting traffic from its websites to Pornhub and threatening to wipe its internal data.... [...]
