Title: High severity BIOS flaws affect numerous Intel processors
Date: 2021-11-15T12:15:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2021-11-15-high-severity-bios-flaws-affect-numerous-intel-processors

[Source](https://www.bleepingcomputer.com/news/security/high-severity-bios-flaws-affect-numerous-intel-processors/){:target="_blank" rel="noopener"}

> Intel has released an advisory to confirm the existence of two high-severity vulnerabilities that affect a wide range of Intel processor families. [...]
