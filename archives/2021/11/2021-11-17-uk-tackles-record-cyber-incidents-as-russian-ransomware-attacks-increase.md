Title: UK tackles record cyber incidents as Russian ransomware attacks increase
Date: 2021-11-17T06:00:29+00:00
Author: Dan Sabbagh Defence and security editor
Category: The Guardian
Tags: Cybercrime;Technology;Hacking;GCHQ;UK news;Malware;Data and computer security
Slug: 2021-11-17-uk-tackles-record-cyber-incidents-as-russian-ransomware-attacks-increase

[Source](https://www.theguardian.com/technology/2021/nov/17/uk-tackles-record-cyber-incidents-as-russian-ransomware-attacks-increase){:target="_blank" rel="noopener"}

> National Cyber Security Centre says cyberattacks at record high and urges businesses not to pay up The National Cyber Security Centre (NCSC) said it tackled a record number of cyber incidents in the UK over the last year, with ransomware attacks originating from Russia dominating its activities. The cybersecurity agency said it had helped deal with a 7.5% increase in cases in the year to August, fuelled by the surge of criminal hackers seizing control of corporate data and demanding payment in cryptocurrency for its return. Continue reading... [...]
