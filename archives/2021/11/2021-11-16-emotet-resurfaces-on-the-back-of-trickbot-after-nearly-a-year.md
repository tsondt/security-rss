Title: Emotet Resurfaces on the Back of TrickBot After Nearly a Year
Date: 2021-11-16T13:57:04+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: 2021-11-16-emotet-resurfaces-on-the-back-of-trickbot-after-nearly-a-year

[Source](https://threatpost.com/emotet-resurfaces-trickbot/176362/){:target="_blank" rel="noopener"}

> Researchers observed what looks like the Emotet botnet – the "world’s most dangerous malware" – reborn and distributed by the trojan it used to deliver. [...]
