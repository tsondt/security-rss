Title: Hoax Email Blast Abused Poor Coding in FBI Website
Date: 2021-11-13T22:46:53+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Criminal Justice Information Services division;Department of Justice;eims@ic.fbi.gov;fbi;FBI email hack;Law Enforcement Enterprise Portal;LEEP;NightLion;pompompurin;Shadowbyte;Vinny Troia
Slug: 2021-11-13-hoax-email-blast-abused-poor-coding-in-fbi-website

[Source](https://krebsonsecurity.com/2021/11/hoax-email-blast-abused-poor-coding-in-fbi-website/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) confirmed today that its fbi.gov domain name and Internet address were used to blast out thousands of fake emails about a cybercrime investigation. According to an interview with the person who claimed responsibility for the hoax, the spam messages were sent by abusing insecure code in an FBI online portal designed to share information with state and local law enforcement authorities. The phony message sent late Thursday evening via the FBI’s email system. Image: Spamhaus.org Late in the evening of Nov. 12 ET, tens of thousands of emails began flooding out from the FBI [...]
