Title: Decrypting diversity: One in five UK infosec professionals say they’ve experienced discrimination at work
Date: 2021-11-24T15:41:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-24-decrypting-diversity-one-in-five-uk-infosec-professionals-say-theyve-experienced-discrimination-at-work

[Source](https://portswigger.net/daily-swig/decrypting-diversity-one-in-five-uk-infosec-professionals-say-theyve-experienced-discrimination-at-work){:target="_blank" rel="noopener"}

> Report states diversity and inclusion within the industry is lagging behind [...]
