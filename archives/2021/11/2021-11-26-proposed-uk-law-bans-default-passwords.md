Title: Proposed UK Law Bans Default Passwords
Date: 2021-11-26T13:43:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2021-11-26-proposed-uk-law-bans-default-passwords

[Source](https://www.schneier.com/blog/archives/2021/11/proposed-uk-law-bans-default-passwords.html){:target="_blank" rel="noopener"}

> Following California’s lead, a new UK law would ban default passwords in IoT devices. [...]
