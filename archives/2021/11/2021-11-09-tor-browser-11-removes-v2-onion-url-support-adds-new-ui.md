Title: Tor Browser 11 removes V2 Onion URL support, adds new UI
Date: 2021-11-09T11:13:14-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software;Security
Slug: 2021-11-09-tor-browser-11-removes-v2-onion-url-support-adds-new-ui

[Source](https://www.bleepingcomputer.com/news/software/tor-browser-11-removes-v2-onion-url-support-adds-new-ui/){:target="_blank" rel="noopener"}

> The Tor Project has released Tor Browser 11.0 with a new user interface design and the removal of support for V2 onion services. [...]
