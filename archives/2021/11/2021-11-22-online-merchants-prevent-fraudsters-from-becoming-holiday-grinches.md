Title: Online Merchants: Prevent Fraudsters from Becoming Holiday Grinches
Date: 2021-11-22T20:13:09+00:00
Author: Saryu Nayyar
Category: Threatpost
Tags: InfoSec Insider;Web Security
Slug: 2021-11-22-online-merchants-prevent-fraudsters-from-becoming-holiday-grinches

[Source](https://threatpost.com/online-merchants-fraudsters-holiday-grinches/176513/){:target="_blank" rel="noopener"}

> Black Friday and Cyber Monday approach! Saryu Nayyar, CEO at Gurucul, discusses concerning statistics about skyrocketing online fraud during the festive season. [...]
