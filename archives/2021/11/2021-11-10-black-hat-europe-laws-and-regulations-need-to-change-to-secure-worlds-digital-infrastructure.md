Title: Black Hat Europe: Laws and regulations need to change to secure world’s digital infrastructure
Date: 2021-11-10T13:59:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-10-black-hat-europe-laws-and-regulations-need-to-change-to-secure-worlds-digital-infrastructure

[Source](https://portswigger.net/daily-swig/black-hat-europe-laws-and-regulations-need-to-change-to-secure-worlds-digital-infrastructure){:target="_blank" rel="noopener"}

> Better incentives to build secure products needed, former MEP tells conference [...]
