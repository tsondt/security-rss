Title: ‘Focus on brilliance at the basics’ – GitHub CSO Mike Hanley on shifting left and securing the software supply chain
Date: 2021-11-05T11:55:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-05-focus-on-brilliance-at-the-basics-github-cso-mike-hanley-on-shifting-left-and-securing-the-software-supply-chain

[Source](https://portswigger.net/daily-swig/focus-on-brilliance-at-the-basics-github-cso-mike-hanley-on-shifting-left-and-securing-the-software-supply-chain){:target="_blank" rel="noopener"}

> Security fundamentals often overlooked in favor of eye-catching initiatives, says infosec pro [...]
