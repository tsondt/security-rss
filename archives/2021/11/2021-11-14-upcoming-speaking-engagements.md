Title: Upcoming Speaking Engagements
Date: 2021-11-14T18:01:14+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2021-11-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2021/11/upcoming-speaking-engagements-14.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking on “Securing a World of Physically Capable Computers” at @Hack on November 29, 2021. The list is maintained on this page. [...]
