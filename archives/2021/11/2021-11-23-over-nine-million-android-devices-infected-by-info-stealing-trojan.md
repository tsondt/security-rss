Title: Over nine million Android devices infected by info-stealing trojan
Date: 2021-11-23T11:00:29-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-11-23-over-nine-million-android-devices-infected-by-info-stealing-trojan

[Source](https://www.bleepingcomputer.com/news/security/over-nine-million-android-devices-infected-by-info-stealing-trojan/){:target="_blank" rel="noopener"}

> A large-scale malware campaign on Huawei's AppGallery has led to approximately 9,300,000 installs of Android trojans masquerading as over 190 different apps [...]
