Title: Beware: Free Discord Nitro phishing targets Steam gamers
Date: 2021-11-03T14:22:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-03-beware-free-discord-nitro-phishing-targets-steam-gamers

[Source](https://www.bleepingcomputer.com/news/security/beware-free-discord-nitro-phishing-targets-steam-gamers/){:target="_blank" rel="noopener"}

> ​A new Steam phishing promoted via Discord messages promises a free Nitro subscription if a user links their Steam account, which the hackers then use to steal game items or promote other scams. [...]
