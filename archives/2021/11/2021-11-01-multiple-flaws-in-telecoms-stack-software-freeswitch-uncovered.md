Title: Multiple flaws in telecoms stack software FreeSwitch uncovered
Date: 2021-11-01T16:42:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-01-multiple-flaws-in-telecoms-stack-software-freeswitch-uncovered

[Source](https://portswigger.net/daily-swig/multiple-flaws-in-telecoms-stack-software-freeswitch-uncovered){:target="_blank" rel="noopener"}

> Authentication and denial of service risks for DIY PBX tech patched [...]
