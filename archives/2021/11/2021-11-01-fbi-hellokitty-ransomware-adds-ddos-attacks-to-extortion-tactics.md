Title: FBI: HelloKitty ransomware adds DDoS attacks to extortion tactics
Date: 2021-11-01T10:13:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-01-fbi-hellokitty-ransomware-adds-ddos-attacks-to-extortion-tactics

[Source](https://www.bleepingcomputer.com/news/security/fbi-hellokitty-ransomware-adds-ddos-attacks-to-extortion-tactics/){:target="_blank" rel="noopener"}

> The U.S. Federal Bureau of Investigation (FBI) has sent out a flash alert warning private industry partners that the HelloKitty ransomware gang (aka FiveHands) has added distributed denial-of-service (DDoS) attacks to their arsenal of extortion tactics. [...]
