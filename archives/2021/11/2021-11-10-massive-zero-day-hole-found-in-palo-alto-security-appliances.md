Title: Massive Zero-Day Hole Found in Palo Alto Security Appliances
Date: 2021-11-10T17:00:35+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-11-10-massive-zero-day-hole-found-in-palo-alto-security-appliances

[Source](https://threatpost.com/massive-zero-day-hole-found-in-palo-alto-security-appliances/176170/){:target="_blank" rel="noopener"}

> UPDATE: Researchers have a working exploit for the vulnerability (now patched), which allows for unauthenticated RCE and affects what Palo Alto clarified is an estimated 10,000 VPN/firewalls. [...]
