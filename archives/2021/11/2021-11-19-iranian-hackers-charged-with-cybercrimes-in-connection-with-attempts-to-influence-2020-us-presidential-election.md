Title: Iranian hackers charged with cybercrimes in connection with attempts to influence 2020 US Presidential Election
Date: 2021-11-19T13:22:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-19-iranian-hackers-charged-with-cybercrimes-in-connection-with-attempts-to-influence-2020-us-presidential-election

[Source](https://portswigger.net/daily-swig/iranian-hackers-charged-with-cybercrimes-in-connection-with-attempts-to-influence-2020-us-presidential-election){:target="_blank" rel="noopener"}

> Pair were affiliated with group that tried to secure a win for Donald Trump [...]
