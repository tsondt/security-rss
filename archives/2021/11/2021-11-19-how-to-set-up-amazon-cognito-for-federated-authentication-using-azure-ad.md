Title: How to set up Amazon Cognito for federated authentication using Azure AD
Date: 2021-11-19T18:06:50+00:00
Author: Ratan Kumar
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;Security, Identity, & Compliance;Identity;OAuth2;OIDC;Security Blog
Slug: 2021-11-19-how-to-set-up-amazon-cognito-for-federated-authentication-using-azure-ad

[Source](https://aws.amazon.com/blogs/security/how-to-set-up-amazon-cognito-for-federated-authentication-using-azure-ad/){:target="_blank" rel="noopener"}

> In this blog post, I’ll walk you through the steps to integrate Azure AD as a federated identity provider in Amazon Cognito user pool. A user pool is a user directory in Amazon Cognito that provides sign-up and sign-in options for your app users. Identity management and authentication flow can be challenging when you need [...]
