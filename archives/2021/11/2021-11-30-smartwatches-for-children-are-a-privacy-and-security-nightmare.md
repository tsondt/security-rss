Title: Smartwatches for children are a privacy and security nightmare
Date: 2021-11-30T13:55:57-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-30-smartwatches-for-children-are-a-privacy-and-security-nightmare

[Source](https://www.bleepingcomputer.com/news/security/smartwatches-for-children-are-a-privacy-and-security-nightmare/){:target="_blank" rel="noopener"}

> Researchers analyzed the security of four popular smartwatches for children and found pre-installed downloaders, weak passwords, and unencrypted data transmissions. [...]
