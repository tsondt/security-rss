Title: REvil Affiliates Arrested; DOJ Seizes $6.1M in Ransom
Date: 2021-11-09T00:01:05+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;News;Web Security
Slug: 2021-11-09-revil-affiliates-arrested-doj-seizes-61m-in-ransom

[Source](https://threatpost.com/revil-affiliates-arrested-doj-europol/176087/){:target="_blank" rel="noopener"}

> The U.S. is seeking the extradition of a Ukrainian man, Yaroslav Vasinskyi, whom they suspect is behind the Kaseya supply-chain attacks and other REvil attacks. [...]
