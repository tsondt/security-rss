Title: Researchers warn of severe risks from ‘Printjack’ printer attacks
Date: 2021-11-23T13:05:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-23-researchers-warn-of-severe-risks-from-printjack-printer-attacks

[Source](https://www.bleepingcomputer.com/news/security/researchers-warn-of-severe-risks-from-printjack-printer-attacks/){:target="_blank" rel="noopener"}

> A team of Italian researchers has compiled a set of three attacks called 'Printjack,' warning users of the significant consequences of over-trusting their printer. [...]
