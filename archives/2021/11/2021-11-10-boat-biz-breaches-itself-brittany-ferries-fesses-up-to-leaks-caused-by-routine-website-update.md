Title: Boat biz breaches itself: Brittany Ferries 'fesses up to leaks caused by routine website update
Date: 2021-11-10T15:29:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2021-11-10-boat-biz-breaches-itself-brittany-ferries-fesses-up-to-leaks-caused-by-routine-website-update

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/10/brittany_ferries/){:target="_blank" rel="noopener"}

> Customers' passport data potentially exposed, says company, promises to carry out password testing It's never good when a boat operator talks of a breach, even if in this case it's a figurative one.... [...]
