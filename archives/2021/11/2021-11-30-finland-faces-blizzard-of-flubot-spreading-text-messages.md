Title: Finland Faces Blizzard of Flubot-Spreading Text Messages
Date: 2021-11-30T18:11:16+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-11-30-finland-faces-blizzard-of-flubot-spreading-text-messages

[Source](https://threatpost.com/finland-flubot-text-messages/176649/){:target="_blank" rel="noopener"}

> Millions of texts leading to the Flubot spyware/banking trojan are targeting everyone who uses Androids in the country, in an "exceptional" attack. [...]
