Title: Government-favoured child safety app warned it could violate the UK's Investigatory Powers Act with message-scanning tech
Date: 2021-11-26T12:23:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-26-government-favoured-child-safety-app-warned-it-could-violate-the-uks-investigatory-powers-act-with-message-scanning-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/26/safetonet_message_scanning_legal_warning/){:target="_blank" rel="noopener"}

> Redesigned SafeToNet feature highlights tech law mess A company repeatedly endorsed by ministers backing the UK's Online Safety Bill was warned by its lawyers that its technology could breach the Investigatory Powers Act's ban on unlawful interception of communications, The Register can reveal.... [...]
