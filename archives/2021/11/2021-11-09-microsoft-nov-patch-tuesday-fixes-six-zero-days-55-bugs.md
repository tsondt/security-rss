Title: Microsoft Nov. Patch Tuesday Fixes Six Zero-Days, 55 Bugs
Date: 2021-11-09T21:41:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-11-09-microsoft-nov-patch-tuesday-fixes-six-zero-days-55-bugs

[Source](https://threatpost.com/microsoft-nov-patch-tuesday-fixes-six-zero-days-55-bugs/176143/){:target="_blank" rel="noopener"}

> Experts urged users to prioritize patches for Microsoft Exchange and Excel, those favorite platforms so frequently targeted by cybercriminals and nation-state actors. [...]
