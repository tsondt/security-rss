Title: FTC shares ransomware defense tips for small US businesses
Date: 2021-11-12T12:14:17-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-12-ftc-shares-ransomware-defense-tips-for-small-us-businesses

[Source](https://www.bleepingcomputer.com/news/security/ftc-shares-ransomware-defense-tips-for-small-us-businesses/){:target="_blank" rel="noopener"}

> The US Federal Trade Commission (FTC) has shared guidance for small businesses on how to secure their networks from ransomware attacks by blocking threat actors' attempts to exploit vulnerabilities using social engineering or exploits targeting technology. [...]
