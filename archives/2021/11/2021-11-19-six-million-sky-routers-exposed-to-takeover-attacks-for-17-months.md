Title: Six million Sky routers exposed to takeover attacks for 17 months
Date: 2021-11-19T09:57:57-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-19-six-million-sky-routers-exposed-to-takeover-attacks-for-17-months

[Source](https://www.bleepingcomputer.com/news/security/six-million-sky-routers-exposed-to-takeover-attacks-for-17-months/){:target="_blank" rel="noopener"}

> Around six million Sky Broadband customer routers in the UK were affected by a critical vulnerability that took over 17 months to roll out a fix to customers. [...]
