Title: Beijing fingers foreign spies for data mischief, with help from consulting firm
Date: 2021-11-05T05:45:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-11-05-beijing-fingers-foreign-spies-for-data-mischief-with-help-from-consulting-firm

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/05/china_claims_foreign_spies_stole_data/){:target="_blank" rel="noopener"}

> Chinese media wonders why it hasn't been reported in the West - hang on, you're reading this... China's Ministry of State Security released details this week of three alleged security breaches that saw sensitive data illegally transferred abroad.... [...]
