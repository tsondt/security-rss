Title: Amazon India execs charged after sellers allegedly use site to smuggle marijuana
Date: 2021-11-22T03:58:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-11-22-amazon-india-execs-charged-after-sellers-allegedly-use-site-to-smuggle-marijuana

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/22/amazon_india_marijuana/){:target="_blank" rel="noopener"}

> Ganja believe it? Seller claimed to sell 'Stevia leaves', but shifted a tonne of wacky 'baccy before being busted Updated Police in the Indian state of Madhya Pradesh have charged Amazon India executives under narcotics laws, after uncovering a marijuana smuggling operation centered around the e-commerce website.... [...]
