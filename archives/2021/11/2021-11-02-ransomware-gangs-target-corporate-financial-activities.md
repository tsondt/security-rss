Title: Ransomware Gangs Target Corporate Financial Activities
Date: 2021-11-02T20:17:29+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware
Slug: 2021-11-02-ransomware-gangs-target-corporate-financial-activities

[Source](https://threatpost.com/ransomware-corporate-financial/175940/){:target="_blank" rel="noopener"}

> The FBI is warning about a fresh extortion tactic: threatening to tank share prices for publicly held companies. [...]
