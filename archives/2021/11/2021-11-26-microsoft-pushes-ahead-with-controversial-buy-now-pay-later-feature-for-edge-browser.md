Title: Microsoft pushes ahead with controversial ‘buy now, pay later’ feature for Edge browser
Date: 2021-11-26T13:54:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-26-microsoft-pushes-ahead-with-controversial-buy-now-pay-later-feature-for-edge-browser

[Source](https://portswigger.net/daily-swig/microsoft-pushes-ahead-with-controversial-buy-now-pay-later-feature-for-edge-browser){:target="_blank" rel="noopener"}

> ‘It’s like you’re recapitulating the worst IE browser extensions and installing them by default’, grumbles one user [...]
