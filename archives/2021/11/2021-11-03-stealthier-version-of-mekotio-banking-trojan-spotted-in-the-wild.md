Title: Stealthier version of Mekotio banking trojan spotted in the wild
Date: 2021-11-03T12:29:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-03-stealthier-version-of-mekotio-banking-trojan-spotted-in-the-wild

[Source](https://www.bleepingcomputer.com/news/security/stealthier-version-of-mekotio-banking-trojan-spotted-in-the-wild/){:target="_blank" rel="noopener"}

> A new version of a banking trojan known as Mekotio is being deployed in the wild, with malware analysts reporting that it's using a new, stealthier infection flow. [...]
