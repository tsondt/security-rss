Title: Mekotio Banking Trojan Resurges with Tweaked Code, Stealthy Campaign
Date: 2021-11-03T19:47:38+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2021-11-03-mekotio-banking-trojan-resurges-with-tweaked-code-stealthy-campaign

[Source](https://threatpost.com/mekotio-banking-trojan-campaign/175981/){:target="_blank" rel="noopener"}

> The banker, aka Metamorfo, is roaring back after Spanish police arrested more than a dozen gang members. [...]
