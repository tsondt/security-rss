Title: Telnyx is the latest VoIP provider hit with DDoS attacks
Date: 2021-11-10T16:18:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-10-telnyx-is-the-latest-voip-provider-hit-with-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/telnyx-is-the-latest-voip-provider-hit-with-ddos-attacks/){:target="_blank" rel="noopener"}

> Telnyx is the latest VoIP telephony provider targeted with distributed denial-of-service (DDoS) attacks, causing worldwide outages since yesterday. [...]
