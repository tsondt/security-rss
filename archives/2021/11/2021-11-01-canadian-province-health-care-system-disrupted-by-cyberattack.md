Title: Canadian province health care system disrupted by cyberattack
Date: 2021-11-01T13:51:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-01-canadian-province-health-care-system-disrupted-by-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/canadian-province-health-care-system-disrupted-by-cyberattack/){:target="_blank" rel="noopener"}

> The Canadian provinces of Newfoundland and Labrador have suffered a cyberattack that has led to severe disruption to healthcare providers and hospitals. [...]
