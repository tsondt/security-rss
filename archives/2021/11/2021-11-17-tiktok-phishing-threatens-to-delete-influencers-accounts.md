Title: TikTok phishing threatens to delete influencers’ accounts
Date: 2021-11-17T12:07:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-17-tiktok-phishing-threatens-to-delete-influencers-accounts

[Source](https://www.bleepingcomputer.com/news/security/tiktok-phishing-threatens-to-delete-influencers-accounts/){:target="_blank" rel="noopener"}

> Researchers have observed a new phishing campaign primarily targeting high-profile TikTok accounts belonging to influencers, brand consultants, production studios, and influencers' managers. [...]
