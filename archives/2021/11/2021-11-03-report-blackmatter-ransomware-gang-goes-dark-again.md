Title: Report: BlackMatter Ransomware Gang Goes Dark, Again
Date: 2021-11-03T12:33:30+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: News
Slug: 2021-11-03-report-blackmatter-ransomware-gang-goes-dark-again

[Source](https://threatpost.com/blackmatter-ransomware-dark/175955/){:target="_blank" rel="noopener"}

> The former DarkSide cybercriminal group will shut down due to increased pressure from authorities, who may have nabbed a key team member. [...]
