Title: Researcher finds SSRF bug in internal Google Cloud project, nabs $10,000 bounty
Date: 2021-11-19T15:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-19-researcher-finds-ssrf-bug-in-internal-google-cloud-project-nabs-10000-bounty

[Source](https://portswigger.net/daily-swig/researcher-finds-ssrf-bug-in-internal-google-cloud-project-nabs-10-000-bounty){:target="_blank" rel="noopener"}

> Now-patched API vulnerability allowed attacker to access sensitive resources [...]
