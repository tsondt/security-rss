Title: Over 30,000 GitLab servers still unpatched against critical bug
Date: 2021-11-02T13:46:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-02-over-30000-gitlab-servers-still-unpatched-against-critical-bug

[Source](https://www.bleepingcomputer.com/news/security/over-30-000-gitlab-servers-still-unpatched-against-critical-bug/){:target="_blank" rel="noopener"}

> A critical unauthenticated, remote code execution GitLab flaw fixed on April 14, 2021, remains exploitable, with over 50% of deployments remaining unpatched. [...]
