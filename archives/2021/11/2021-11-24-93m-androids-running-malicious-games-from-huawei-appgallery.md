Title: 9.3M+ Androids Running ‘Malicious’ Games from Huawei AppGallery
Date: 2021-11-24T17:28:37+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2021-11-24-93m-androids-running-malicious-games-from-huawei-appgallery

[Source](https://threatpost.com/9m-androids-malware-games-huawei-appgallery/176581/){:target="_blank" rel="noopener"}

> A new trojan called Android.Cynos.7.origin, designed to collect Android users’ device data and phone numbers, was found in 190 games installed on over 9M Android devices. [...]
