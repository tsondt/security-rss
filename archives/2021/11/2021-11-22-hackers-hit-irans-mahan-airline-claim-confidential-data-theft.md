Title: Hackers hit Iran's Mahan airline, claim confidential data theft
Date: 2021-11-22T13:30:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-22-hackers-hit-irans-mahan-airline-claim-confidential-data-theft

[Source](https://www.bleepingcomputer.com/news/security/hackers-hit-irans-mahan-airline-claim-confidential-data-theft/){:target="_blank" rel="noopener"}

> One of Iran's largest privately-owned airlines, Mahan Air, has announced a cybersecurity incident that has resulted in its website going offline and potentially data loss. [...]
