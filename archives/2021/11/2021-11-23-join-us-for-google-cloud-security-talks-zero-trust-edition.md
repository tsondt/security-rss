Title: Join us for Google Cloud Security Talks: Zero Trust edition
Date: 2021-11-23T17:00:00+00:00
Author: Jessica Davlin
Category: GCP Security
Tags: Events;Identity & Security
Slug: 2021-11-23-join-us-for-google-cloud-security-talks-zero-trust-edition

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-security-talks-event-focused-on-zero-trust-and-beyondcorp/){:target="_blank" rel="noopener"}

> Join us for our final Google Cloud Security Talks of 2021, a live online event on December 15th, where we’ll focus on all things zero trust. During this multi-session digital event, learn how you can leverage a zero trust approach to protect your employees, users, and critical information, with sessions including the following: A keynote from Sunil Potti and Rob Sadowski to kick off Security Talks on December 15th. They will provide an update across the product portfolio and highlight how you can build a more secure future with Google Cloud. Following this will be a panel discussion on our [...]
