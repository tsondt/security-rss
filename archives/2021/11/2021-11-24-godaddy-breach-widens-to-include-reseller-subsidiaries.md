Title: GoDaddy Breach Widens to Include Reseller Subsidiaries
Date: 2021-11-24T16:16:12+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Privacy;Web Security
Slug: 2021-11-24-godaddy-breach-widens-to-include-reseller-subsidiaries

[Source](https://threatpost.com/godaddy-breach-widens-reseller-subsidiaries/176575/){:target="_blank" rel="noopener"}

> Customers of several brands that resell GoDaddy Managed WordPress have also been caught up in the big breach, in which millions of emails, passwords and more were stolen. [...]
