Title: Russian 'King of Fraud' sentenced to 10 years for Methbot scheme
Date: 2021-11-11T09:24:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2021-11-11-russian-king-of-fraud-sentenced-to-10-years-for-methbot-scheme

[Source](https://www.bleepingcomputer.com/news/legal/russian-king-of-fraud-sentenced-to-10-years-for-methbot-scheme/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DOJ) sentenced a Russian man for operating a large-scale digital advertising fraud scheme called 'Methbot' ('3ve') that stole at least $7 million from American companies. [...]
