Title: 6M Sky Routers Left Exposed to Attack for Nearly 1.5 Years
Date: 2021-11-19T17:39:18+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-11-19-6m-sky-routers-left-exposed-to-attack-for-nearly-15-years

[Source](https://threatpost.com/6m-sky-routers-exposed-18-months/176483/){:target="_blank" rel="noopener"}

> Pen Test Partners didn't disclose the vulnerability after 90 days because it knew ISPs were struggling with a pandemic-increased network load as work from home became the new norm. [...]
