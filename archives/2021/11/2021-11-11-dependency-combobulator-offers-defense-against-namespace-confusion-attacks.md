Title: Dependency Combobulator offers defense against namespace confusion attacks
Date: 2021-11-11T14:12:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-11-dependency-combobulator-offers-defense-against-namespace-confusion-attacks

[Source](https://portswigger.net/daily-swig/dependency-combobulator-offers-defense-against-namespace-confusion-attacks){:target="_blank" rel="noopener"}

> Toolkit ‘tackles common scenarios’ and can evolve to detect emerging attack variants [...]
