Title: FBI system hacked to email 'urgent' warning about fake cyberattacks
Date: 2021-11-13T13:36:16-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-13-fbi-system-hacked-to-email-urgent-warning-about-fake-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-system-hacked-to-email-urgent-warning-about-fake-cyberattacks/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) email servers were hacked to distribute spam email impersonating FBI warnings that the recipients' network was breached and data was stolen. [...]
