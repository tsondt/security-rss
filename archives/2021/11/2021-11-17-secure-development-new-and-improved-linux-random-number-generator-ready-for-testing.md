Title: Secure development: New and improved Linux Random Number Generator ready for testing
Date: 2021-11-17T16:59:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-17-secure-development-new-and-improved-linux-random-number-generator-ready-for-testing

[Source](https://portswigger.net/daily-swig/secure-development-new-and-improved-linux-random-number-generator-ready-for-testing){:target="_blank" rel="noopener"}

> Proposed replacement for /dev/random promises to double performance and add flexibility [...]
