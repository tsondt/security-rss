Title: Tech CEO Pleads to Wire Fraud in IP Address Scheme
Date: 2021-11-17T23:56:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;African Network Information Centre;AFRINIC;American Registry for Internet Numbers;Amir Golestan;ARIN;Micfo LLC;Ron Guilmette;Sherri Lydon;spamhaus;Stephen Ryan
Slug: 2021-11-17-tech-ceo-pleads-to-wire-fraud-in-ip-address-scheme

[Source](https://krebsonsecurity.com/2021/11/tech-ceo-pleads-to-wire-fraud-in-ip-address-scheme/){:target="_blank" rel="noopener"}

> The CEO of a South Carolina technology firm has pleaded guilty to 20 counts of wire fraud in connection with an elaborate network of phony companies set up to obtain more than 735,000 Internet Protocol (IP) addresses from the nonprofit organization that leases the digital real estate to entities in North America. In 2018, the American Registry for Internet Numbers (ARIN), which oversees IP addresses assigned to entities in the U.S., Canada, and parts of the Caribbean, notified Charleston, S.C. based Micfo LLC that it intended to revoke 735,000 addresses. ARIN said they wanted the addresses back because the company [...]
