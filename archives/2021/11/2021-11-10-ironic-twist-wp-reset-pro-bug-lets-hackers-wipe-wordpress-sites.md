Title: Ironic twist: WP Reset PRO bug lets hackers wipe WordPress sites
Date: 2021-11-10T12:00:07-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-10-ironic-twist-wp-reset-pro-bug-lets-hackers-wipe-wordpress-sites

[Source](https://www.bleepingcomputer.com/news/security/ironic-twist-wp-reset-pro-bug-lets-hackers-wipe-wordpress-sites/){:target="_blank" rel="noopener"}

> A high severity security flaw in the WP Reset PRO WordPress plugin can let authenticated attackers wipe vulnerable websites, as revealed by Patchstack security researchers. [...]
