Title: Intel is Maintaining Legacy Technology for Security Research
Date: 2021-11-30T07:28:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2021-11-30-intel-is-maintaining-legacy-technology-for-security-research

[Source](https://www.schneier.com/blog/archives/2021/11/intel-is-maintaining-legacy-technology-for-security-research.html){:target="_blank" rel="noopener"}

> Interesting : Intel’s issue reflects a wider concern: Legacy technology can introduce cybersecurity weaknesses. Tech makers constantly improve their products to take advantage of speed and power increases, but customers don’t always upgrade at the same pace. This creates a long tail of old products that remain in widespread use, vulnerable to attacks. Intel’s answer to this conundrum was to create a warehouse and laboratory in Costa Rica, where the company already had a research-and-development lab, to store the breadth of its technology and make the devices available for remote testing. After planning began in mid-2018, the Long-Term Retention Lab [...]
