Title: New Zealand spooks say satellite snooping is obsolete – better intel is found elsewhere
Date: 2021-11-11T04:16:23+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-11-11-new-zealand-spooks-say-satellite-snooping-is-obsolete-better-intel-is-found-elsewhere

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/11/new_zealand_spooks_say_satellite/){:target="_blank" rel="noopener"}

> Kiwis are done with dishes, and the Five Eyes alliance is cool with it New Zealand's Government Communications Security Bureau (GCSB) – the nation's signals intelligence and infosec agency – will retire its Waihopai satellite communications interception station because it's no longer needed.... [...]
