Title: Zero tolerance: How infosec’s online ‘cancel culture’ is stunting industry growth
Date: 2021-11-11T15:55:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-11-zero-tolerance-how-infosecs-online-cancel-culture-is-stunting-industry-growth

[Source](https://portswigger.net/daily-swig/zero-tolerance-how-infosecs-online-cancel-culture-is-stunting-industry-growth){:target="_blank" rel="noopener"}

> Fear of Twitter fallout is stopping vital information from being shared Social media backlash and online squabbling is stopping the information security industry from learning from its mistakes, Black [...]
