Title: Hackers undetected on Queensland water supplier server for 9 months
Date: 2021-11-11T11:44:50-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-11-11-hackers-undetected-on-queensland-water-supplier-server-for-9-months

[Source](https://www.bleepingcomputer.com/news/security/hackers-undetected-on-queensland-water-supplier-server-for-9-months/){:target="_blank" rel="noopener"}

> Hackers stayed hidden for nine months on a server holding customer information for a Queensland water supplier, illustrating the need of better cyberdefenses for critical infrastructure. [...]
