Title: FBI: Ransomware targets companies during mergers and acquisitions
Date: 2021-11-02T07:59:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-02-fbi-ransomware-targets-companies-during-mergers-and-acquisitions

[Source](https://www.bleepingcomputer.com/news/security/fbi-ransomware-targets-companies-during-mergers-and-acquisitions/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warns that ransomware gangs are targeting companies involved in "time-sensitive financial events" such as corporate mergers and acquisitions to make it easier to extort their victims. [...]
