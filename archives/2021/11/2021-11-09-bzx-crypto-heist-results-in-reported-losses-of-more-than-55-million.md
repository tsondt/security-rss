Title: bZx crypto heist results in reported losses of more than $55 million
Date: 2021-11-09T15:46:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-09-bzx-crypto-heist-results-in-reported-losses-of-more-than-55-million

[Source](https://portswigger.net/daily-swig/bzx-crypto-heist-results-in-reported-losses-of-more-than-55-million){:target="_blank" rel="noopener"}

> BSC and Polygon funds drained – but Ethereum contracts ‘safe’ – following phishing attack [...]
