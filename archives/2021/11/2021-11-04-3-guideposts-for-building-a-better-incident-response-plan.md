Title: 3 Guideposts for Building a Better Incident-Response Plan
Date: 2021-11-04T17:50:29+00:00
Author: Grant Oviatt
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;InfoSec Insider;Mobile Security;Web Security
Slug: 2021-11-04-3-guideposts-for-building-a-better-incident-response-plan

[Source](https://threatpost.com/3-guideposts-incident-response-plan/176019/){:target="_blank" rel="noopener"}

> Invest and practice: Grant Oviatt, director of incident-response engagements at Red Canary, lays out the key building blocks for effective IR. [...]
