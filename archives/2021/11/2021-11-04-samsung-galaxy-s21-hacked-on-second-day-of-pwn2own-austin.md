Title: Samsung Galaxy S21 hacked on second day of Pwn2Own Austin
Date: 2021-11-04T12:03:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-04-samsung-galaxy-s21-hacked-on-second-day-of-pwn2own-austin

[Source](https://www.bleepingcomputer.com/news/security/samsung-galaxy-s21-hacked-on-second-day-of-pwn2own-austin/){:target="_blank" rel="noopener"}

> Contestants hacked the Samsung Galaxy S21 smartphone during the second day of the Pwn2Own Austin 2021 competition, as well as routers, NAS devices, speakers, and printers from Cisco, TP-Link, Western Digital, Sonos, Canon, Lexmark, and HP. [...]
