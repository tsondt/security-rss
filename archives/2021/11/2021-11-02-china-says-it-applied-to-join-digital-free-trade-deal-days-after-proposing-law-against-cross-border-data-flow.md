Title: China says it applied to join digital free trade deal days after proposing law against cross-border data flow
Date: 2021-11-02T11:30:04+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-11-02-china-says-it-applied-to-join-digital-free-trade-deal-days-after-proposing-law-against-cross-border-data-flow

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/02/china_free_trade/){:target="_blank" rel="noopener"}

> Xi Jinping announces to G20 he wants in on 3-nation agreement China's Ministry of Commerce said on Monday the country has officially applied for entry into the Digital Economy Partnership Agreement (DEPA).... [...]
