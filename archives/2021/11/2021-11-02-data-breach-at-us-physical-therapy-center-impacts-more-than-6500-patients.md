Title: Data breach at US physical therapy center impacts more than 6,500 patients
Date: 2021-11-02T14:18:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-02-data-breach-at-us-physical-therapy-center-impacts-more-than-6500-patients

[Source](https://portswigger.net/daily-swig/data-breach-at-us-physical-therapy-center-impacts-more-than-6-500-patients){:target="_blank" rel="noopener"}

> Minnesota healthcare provider hit by cyber-attack A US physical therapy center has announced that the personal data of more than 6,500 patients has been breached in a security incident. Viverant PT, b [...]
