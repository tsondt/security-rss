Title: Microsoft unveils ‘Super Duper Secure Mode’ in latest version of Edge
Date: 2021-11-23T17:22:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-23-microsoft-unveils-super-duper-secure-mode-in-latest-version-of-edge

[Source](https://portswigger.net/daily-swig/microsoft-unveils-super-duper-secure-mode-in-latest-version-of-edge){:target="_blank" rel="noopener"}

> Browser goes further to protect against bugs by disabling JIT [...]
