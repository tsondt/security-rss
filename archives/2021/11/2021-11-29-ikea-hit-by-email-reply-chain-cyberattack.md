Title: IKEA Hit by Email Reply-Chain Cyberattack
Date: 2021-11-29T21:22:12+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-11-29-ikea-hit-by-email-reply-chain-cyberattack

[Source](https://threatpost.com/ikea-email-reply-chain-attack/176625/){:target="_blank" rel="noopener"}

> IKEA, king of furniture-in-a-flat-box, warned employees on Friday that an ongoing cyberattack was using internal emails to malspam malicious links in active email threads. [...]
