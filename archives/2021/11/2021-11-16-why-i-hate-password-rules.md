Title: Why I Hate Password Rules
Date: 2021-11-16T11:33:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2021-11-16-why-i-hate-password-rules

[Source](https://www.schneier.com/blog/archives/2021/11/why-i-hate-password-rules.html){:target="_blank" rel="noopener"}

> The other day I was creating a new account on the web. It was financial in nature, which means it gets one of my most secure passwords. I used PasswordSafe to generate this 16-character alphanumeric password: :s^Twd.J;3hzg=Q~ Which was rejected by the site, because it didn’t meet their password security rules. It took me a minute to figure out what was wrong with it. They wanted at least two numbers. Sheesh. Okay, that’s not really why I don’t like password rules. I don’t like them because they’re all different. If someone has a strong password generation system, it is likely [...]
