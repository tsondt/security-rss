Title: ‘Tortilla’ Wraps Exchange Servers in ProxyShell Attacks
Date: 2021-11-03T18:16:37+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2021-11-03-tortilla-wraps-exchange-servers-in-proxyshell-attacks

[Source](https://threatpost.com/tortilla-exchange-servers-proxyshell/175967/){:target="_blank" rel="noopener"}

> The Microsoft Exchange ProxyShell vulnerabilities are being exploited yet again for ransomware, this time with Babuk from the new "Tortilla" threat actor. [...]
