Title: DDoS Attacks Shatter Records in Q3, Report Finds
Date: 2021-11-08T20:48:59+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2021-11-08-ddos-attacks-shatter-records-in-q3-report-finds

[Source](https://threatpost.com/ddos-attacks-records-q3/176082/){:target="_blank" rel="noopener"}

> Q3 DDoS attacks topped thousands daily, with more growth expected. [...]
