Title: Two men charged with deploying REvil ransomware attacks, targeting US government and businesses
Date: 2021-11-09T12:23:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-09-two-men-charged-with-deploying-revil-ransomware-attacks-targeting-us-government-and-businesses

[Source](https://portswigger.net/daily-swig/two-men-charged-with-deploying-revil-ransomware-attacks-targeting-us-government-and-businesses){:target="_blank" rel="noopener"}

> Individuals face up to 145 years in prison if convicted [...]
