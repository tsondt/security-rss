Title: Yanluowang Ransomware Tied to Thieflock Threat Actor
Date: 2021-11-30T13:56:45+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-11-30-yanluowang-ransomware-tied-to-thieflock-threat-actor

[Source](https://threatpost.com/yanluowang-ransomware-thieflock-threat-actor/176640/){:target="_blank" rel="noopener"}

> Links between the tactics and tools demonstrated in attacks suggest a former affiliate has switched loyalties, according to new research. [...]
