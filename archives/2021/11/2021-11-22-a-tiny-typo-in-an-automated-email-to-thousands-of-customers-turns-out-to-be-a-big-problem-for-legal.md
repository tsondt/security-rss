Title: A tiny typo in an automated email to thousands of customers turns out to be a big problem for legal
Date: 2021-11-22T08:30:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-11-22-a-tiny-typo-in-an-automated-email-to-thousands-of-customers-turns-out-to-be-a-big-problem-for-legal

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/22/who_me/){:target="_blank" rel="noopener"}

> Unexpected consequences of the SQL Slammer worm Who, Me? Do you check your emails before sending them? Re-read a dozen times but still that typo sneaks through? Welcome to a Who, Me? in which a reader learns that one mistyped letter can result in a visit from the legal department.... [...]
