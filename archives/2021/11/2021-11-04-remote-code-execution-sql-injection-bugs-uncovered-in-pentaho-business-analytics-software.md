Title: Remote code execution, SQL injection bugs uncovered in Pentaho Business Analytics software
Date: 2021-11-04T14:14:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-04-remote-code-execution-sql-injection-bugs-uncovered-in-pentaho-business-analytics-software

[Source](https://portswigger.net/daily-swig/remote-code-execution-sql-injection-bugs-uncovered-in-pentaho-business-analytics-software){:target="_blank" rel="noopener"}

> Penetration test reveals severe issues in Hitachi Vantara’s business solution [...]
