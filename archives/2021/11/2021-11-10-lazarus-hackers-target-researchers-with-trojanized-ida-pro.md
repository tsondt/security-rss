Title: Lazarus hackers target researchers with trojanized IDA Pro
Date: 2021-11-10T12:08:04-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-10-lazarus-hackers-target-researchers-with-trojanized-ida-pro

[Source](https://www.bleepingcomputer.com/news/security/lazarus-hackers-target-researchers-with-trojanized-ida-pro/){:target="_blank" rel="noopener"}

> A North Korean state-sponsored hacking group known as Lazarus is again trying to hack security researchers, this time with a trojanized pirated version of the popular IDA Pro reverse engineering application. [...]
