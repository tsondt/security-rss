Title: Zero trust workload security with GKE, Traffic Director, and CA Service
Date: 2021-11-02T16:00:00+00:00
Author: Sanjay Pujare
Category: GCP Security
Tags: Containers & Kubernetes;Google Cloud;Identity & Security
Slug: 2021-11-02-zero-trust-workload-security-with-gke-traffic-director-and-ca-service

[Source](https://cloud.google.com/blog/products/identity-security/workload-credentials-for-gke-via-cas/){:target="_blank" rel="noopener"}

> At the core of a zero trust approach to security is the idea that trust needs to be established via multiple mechanisms and continuously verified. Internally, Google has applied this thinking to the end-to-end process of running production systems and protecting workloads on cloud-native infrastructure, an approach we call BeyondProd. Establishing and verifying trust in such a system requires: 1) that each workload has a unique workload identity and credentials for authentication, and 2) an authorization layer that determines which components of the system can communicate with other components. Consider a cloud-native architecture where apps are broken into microservices. In-process [...]
