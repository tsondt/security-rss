Title: UK Labour Party discloses data breach after ransomware attack
Date: 2021-11-03T13:22:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-03-uk-labour-party-discloses-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/uk-labour-party-discloses-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> The UK Labour Party notified members that some of their information was impacted in a data breach after a ransomware attack hit a third-party organization that was managing the party's data. [...]
