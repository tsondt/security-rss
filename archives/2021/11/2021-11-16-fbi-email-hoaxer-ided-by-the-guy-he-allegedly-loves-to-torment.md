Title: FBI Email Hoaxer ID’ed by the Guy He Allegedly Loves to Torment
Date: 2021-11-16T22:33:59+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Government;Hacks
Slug: 2021-11-16-fbi-email-hoaxer-ided-by-the-guy-he-allegedly-loves-to-torment

[Source](https://threatpost.com/fbi-email-hoaxer-ided-vinny-troia/176377/){:target="_blank" rel="noopener"}

> Vinny Troia, the cybersecurity researcher mentioned in a fake alert gushed out of the FBI’s email system, says it's just one of a string of jabs from a childish but cybercriminally talented tormentor. [...]
