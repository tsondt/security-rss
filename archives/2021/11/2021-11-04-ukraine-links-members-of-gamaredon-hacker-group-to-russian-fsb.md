Title: Ukraine links members of Gamaredon hacker group to Russian FSB
Date: 2021-11-04T09:54:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-11-04-ukraine-links-members-of-gamaredon-hacker-group-to-russian-fsb

[Source](https://www.bleepingcomputer.com/news/security/ukraine-links-members-of-gamaredon-hacker-group-to-russian-fsb/){:target="_blank" rel="noopener"}

> SSU and the Ukrainian secret service say they have identified five members of the Gamaredon hacking group, a Russian state-sponsored operation known for targeting Ukraine since 2014. [...]
