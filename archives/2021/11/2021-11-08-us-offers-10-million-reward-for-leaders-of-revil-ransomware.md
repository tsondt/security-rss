Title: U.S. offers $10 million reward for leaders of REvil ransomware
Date: 2021-11-08T19:11:32-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-11-08-us-offers-10-million-reward-for-leaders-of-revil-ransomware

[Source](https://www.bleepingcomputer.com/news/security/us-offers-10-million-reward-for-leaders-of-revil-ransomware/){:target="_blank" rel="noopener"}

> The U.S. is offering up to $10 million for identifying or locating leaders in the REvil (Sodinokibi) ransomware operation, including $5 million leading to the arrest of affiliates. [...]
