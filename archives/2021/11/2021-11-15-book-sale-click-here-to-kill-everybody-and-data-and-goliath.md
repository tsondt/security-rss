Title: Book Sale: Click Here to Kill Everybody and Data and Goliath
Date: 2021-11-15T20:34:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;books;CH2KE;Data and Goliath;Schneier news
Slug: 2021-11-15-book-sale-click-here-to-kill-everybody-and-data-and-goliath

[Source](https://www.schneier.com/blog/archives/2021/11/book-sale-click-here-to-kill-everybody-and-data-and-goliath.html){:target="_blank" rel="noopener"}

> For a limited time, I am selling signed copies of Click Here to Kill Everybody and Data and Goliath, both in paperback, for just $6 each plus shipping. I have 500 copies of each book available. When they’re gone, the sale is over and the price will revert to normal. Order here and here. Please be patient on delivery. It’s a lot of work to sign and mail hundreds of books. And the pandemic is causing mail slowdowns all over the world. I’ll send them out as quickly as I can, but I can’t guarantee any particular delivery date. Also, [...]
