Title: Google Play apps downloaded 300,000 times stole bank credentials
Date: 2021-11-29T21:25:36+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;bank fraud;google play;malware
Slug: 2021-11-29-google-play-apps-downloaded-300000-times-stole-bank-credentials

[Source](https://arstechnica.com/?p=1816768){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Researchers said they’ve discovered a batch of apps downloaded from Google Play more than 300,000 times before the apps were revealed to be banking trojans that surreptitiously siphoned user passwords and two-factor authentication codes, logged keystrokes, and took screenshots. The apps—posing as QR scanners, PDF scanners, and cryptocurrency wallets—belonged to four separate Android malware families that were distributed over four months. They used several tricks to sidestep restrictions that Google has devised in an attempt to rein in the unending distribution of fraudulent apps in its official marketplace. Those limitations include restricting the use of [...]
