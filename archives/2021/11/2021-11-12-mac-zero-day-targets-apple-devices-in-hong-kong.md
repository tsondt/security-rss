Title: Mac Zero Day Targets Apple Devices in Hong Kong
Date: 2021-11-12T18:05:02+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2021-11-12-mac-zero-day-targets-apple-devices-in-hong-kong

[Source](https://threatpost.com/mac-zero-day-apple-hong-kong/176300/){:target="_blank" rel="noopener"}

> Google researchers have detailed a widespread watering-hole attack that installed a backdoor on Apple devices that visited Hong Kong-based media and pro-democracy sites. [...]
