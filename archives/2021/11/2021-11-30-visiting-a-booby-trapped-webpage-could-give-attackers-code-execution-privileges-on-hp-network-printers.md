Title: Visiting a booby-trapped webpage could give attackers code execution privileges on HP network printers
Date: 2021-11-30T15:59:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-30-visiting-a-booby-trapped-webpage-could-give-attackers-code-execution-privileges-on-hp-network-printers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/30/exploitable_hp_enterprise_printers_f_secure/){:target="_blank" rel="noopener"}

> Patches available for 150 affected products Tricking users into visiting a malicious webpage could allow malicious people to compromise 150 models of HP multi-function printers, according to F-Secure researchers.... [...]
