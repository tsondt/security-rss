Title: Attackers Hijack Email Threads Using ProxyLogon/ProxyShell Flaws
Date: 2021-11-22T19:26:25+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-11-22-attackers-hijack-email-threads-using-proxylogonproxyshell-flaws

[Source](https://threatpost.com/attackers-hijack-email-threads-proxylogon-proxyshell/176496/){:target="_blank" rel="noopener"}

> Exploiting Microsoft Exchange ProxyLogon & ProxyShell vulnerabilities, attackers are malspamming replies in existing threads and slipping past malicious-email filters. [...]
