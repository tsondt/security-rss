Title: Clubhouse launches bug bounty program with $3,000 on offer for critical vulnerabilities
Date: 2021-11-18T13:40:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-18-clubhouse-launches-bug-bounty-program-with-3000-on-offer-for-critical-vulnerabilities

[Source](https://portswigger.net/daily-swig/clubhouse-launches-bug-bounty-program-with-3-000-on-offer-for-critical-vulnerabilities){:target="_blank" rel="noopener"}

> Audio-based social media platform prioritizes access control bypasses and information disclosure flaws [...]
