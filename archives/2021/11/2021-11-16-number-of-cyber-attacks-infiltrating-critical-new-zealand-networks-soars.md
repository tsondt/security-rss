Title: <span>Number of cyber-attacks infiltrating critical New Zealand networks soars</span>
Date: 2021-11-16T15:26:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-16-number-of-cyber-attacks-infiltrating-critical-new-zealand-networks-soars

[Source](https://portswigger.net/daily-swig/number-of-cyber-attacks-infiltrating-critical-new-zealand-networks-soars){:target="_blank" rel="noopener"}

> National cybersecurity agency also observes rise in automated probing for web security flaws [...]
