Title: New bill sets ransomware attack response rules for US financial orgs
Date: 2021-11-11T08:54:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-11-11-new-bill-sets-ransomware-attack-response-rules-for-us-financial-orgs

[Source](https://www.bleepingcomputer.com/news/security/new-bill-sets-ransomware-attack-response-rules-for-us-financial-orgs/){:target="_blank" rel="noopener"}

> New legislation introduced this week by US lawmakers aims to set ransomware attack response "rules of road" for US financial institutions. [...]
