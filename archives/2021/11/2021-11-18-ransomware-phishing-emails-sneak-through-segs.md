Title: Ransomware Phishing Emails Sneak Through SEGs
Date: 2021-11-18T21:45:54+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware
Slug: 2021-11-18-ransomware-phishing-emails-sneak-through-segs

[Source](https://threatpost.com/ransomware-phishing-emails-segs/176470/){:target="_blank" rel="noopener"}

> The MICROP ransomware spreads via Google Drive and locally stored passwords. [...]
