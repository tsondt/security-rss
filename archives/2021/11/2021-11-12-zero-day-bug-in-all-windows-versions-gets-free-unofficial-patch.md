Title: Zero-day bug in all Windows versions gets free unofficial patch
Date: 2021-11-12T07:28:40-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-12-zero-day-bug-in-all-windows-versions-gets-free-unofficial-patch

[Source](https://www.bleepingcomputer.com/news/microsoft/zero-day-bug-in-all-windows-versions-gets-free-unofficial-patch/){:target="_blank" rel="noopener"}

> A free and unofficial patch is now available for a zero-day local privilege escalation vulnerability in the Windows User Profile Service that lets attackers gain SYSTEM privileges under certain conditions. [...]
