Title: Robinhood discloses data breach impacting 7 million customers
Date: 2021-11-08T16:40:29-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-11-08-robinhood-discloses-data-breach-impacting-7-million-customers

[Source](https://www.bleepingcomputer.com/news/security/robinhood-discloses-data-breach-impacting-7-million-customers/){:target="_blank" rel="noopener"}

> Stock trading platform Robinhood has disclosed a data breach after their systems were hacked and a threat actor gained access to the personal information of approximately 7 million customers. [...]
