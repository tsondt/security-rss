Title: New Twists on Gift-Card Scams Flourish on Black Friday
Date: 2021-11-25T16:02:38+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-11-25-new-twists-on-gift-card-scams-flourish-on-black-friday

[Source](https://threatpost.com/new-twists-on-gift-card-scams-flourish-on-black-friday/176593/){:target="_blank" rel="noopener"}

> Fake merchandise and crypto jacking are among the new ways cybercriminals will try to defraud people flocking online for Black Friday and Cyber Monday. [...]
