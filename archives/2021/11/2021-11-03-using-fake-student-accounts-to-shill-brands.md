Title: Using Fake Student Accounts to Shill Brands
Date: 2021-11-03T11:10:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;e-mail;fraud;reputation;scams
Slug: 2021-11-03-using-fake-student-accounts-to-shill-brands

[Source](https://www.schneier.com/blog/archives/2021/11/using-fake-student-accounts-to-shill-brands.html){:target="_blank" rel="noopener"}

> It turns out that it’s surprisingly easy to create a fake Harvard student and get a harvard.edu email account. Scammers are using that prestigious domain name to shill brands : Basically, it appears that anyone with $300 to spare can ­– or could, depending on whether Harvard successfully shuts down the practice — advertise nearly anything they wanted on Harvard.edu, in posts that borrow the university’s domain and prestige while making no mention of the fact that it in reality they constitute paid advertising.... A Harvard spokesperson said that the university is working to crack down on the fake students [...]
