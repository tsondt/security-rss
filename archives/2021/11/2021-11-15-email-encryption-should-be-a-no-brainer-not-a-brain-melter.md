Title: Email encryption should be a no-brainer, not a brain melter
Date: 2021-11-15T18:00:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-11-15-email-encryption-should-be-a-no-brainer-not-a-brain-melter

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/15/email_encryption_solutions/){:target="_blank" rel="noopener"}

> Warm up your email security smarts with this online broadcast Webinar Email has always been one of the weaker links when it comes to cybersecurity. But the risks to your enterprise are only greater now after large chunks of your workforce have spent almost two years working at home, often using devices and infrastructure that are less than enterprise grade.... [...]
