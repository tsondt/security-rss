Title: Lloyd's of London suggests insurers should not cover 'retaliatory cyber operations' between nation states
Date: 2021-11-30T14:02:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-11-30-lloyds-of-london-suggests-insurers-should-not-cover-retaliatory-cyber-operations-between-nation-states

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/30/lloyds_london_cyber_insurance_clauses/){:target="_blank" rel="noopener"}

> And they might attribute cyber attacks if governments won't Lloyd’s of London may no longer extend insurance cover to companies affected by acts of war, and new clauses drafted for providers of so-called "cyber" insurance are raising the spectre of organisations caught in tit-for-tat nation state-backed attacks being left high and dry.... [...]
