Title: How a malicious Android app could covertly turn the DSP in your MediaTek-powered phone into an eavesdropping bug
Date: 2021-11-24T11:00:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-11-24-how-a-malicious-android-app-could-covertly-turn-the-dsp-in-your-mediatek-powered-phone-into-an-eavesdropping-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/24/mediatek_audio_vulnerabilty/){:target="_blank" rel="noopener"}

> Millions of devices potentially vulnerable, we're told Check Point Research will today spill the beans on security holes it found within the audio processor firmware in millions of smartphones, which can be potentially exploited by malicious apps to secretly eavesdrop on people.... [...]
