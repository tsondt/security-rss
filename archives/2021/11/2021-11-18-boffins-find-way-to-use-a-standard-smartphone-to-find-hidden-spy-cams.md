Title: Boffins find way to use a standard smartphone to find hidden spy cams
Date: 2021-11-18T22:43:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-18-boffins-find-way-to-use-a-standard-smartphone-to-find-hidden-spy-cams

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/18/smartphone_camera_detection/){:target="_blank" rel="noopener"}

> Smartphones now have lasers so we're gonna use them to find voyeurs Recent model smartphones can be smarter still about finding hidden cameras in their vicinity, if they take advantage of time-of-flight (ToF) sensors.... [...]
