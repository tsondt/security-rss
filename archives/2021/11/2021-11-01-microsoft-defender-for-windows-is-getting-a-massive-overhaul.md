Title: Microsoft Defender for Windows is getting a massive overhaul
Date: 2021-11-01T17:35:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-01-microsoft-defender-for-windows-is-getting-a-massive-overhaul

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-for-windows-is-getting-a-massive-overhaul/){:target="_blank" rel="noopener"}

> Microsoft Defender for Windows is getting a massive overhaul allowing home network admins to deploy Android, iOS, and Mac clients to monitor antivirus, phishing, compromised passwords, and identity theft alerts from a single security dashboard. [...]
