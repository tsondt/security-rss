Title: Arrest in ‘Ransom Your Employer’ Email Scheme
Date: 2021-11-22T21:57:18+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;Abnormal Security;Agari;Crane Hassold;CyberLab;Oluwaseun Medayedupin;ransomware;Ronnie Tokazowski;Sociogram
Slug: 2021-11-22-arrest-in-ransom-your-employer-email-scheme

[Source](https://krebsonsecurity.com/2021/11/arrest-in-ransom-your-employer-email-scheme/){:target="_blank" rel="noopener"}

> In August, KrebsOnSecurity warned that scammers were contacting people and asking them to unleash ransomware inside their employer’s network, in exchange for a percentage of any ransom amount paid by the victim company. This week, authorities in Nigeria arrested a suspect in connection with the scheme — a young man who said he was trying to save up money to help fund a new social network. Image: Abnormal Security. The brazen approach targeting disgruntled employees was first spotted by threat intelligence firm Abnormal Security, which described what happened after they adopted a fake persona and responded to the proposal in [...]
