Title: Attackers Actively Target Windows Installer Zero-Day
Date: 2021-11-24T14:09:18+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2021-11-24-attackers-actively-target-windows-installer-zero-day

[Source](https://threatpost.com/attackers-target-windows-installer-bug/176558/){:target="_blank" rel="noopener"}

> Researcher discovered a “more powerful” variant of an elevation-of-privilege flaw for which Microsoft released a botched patch earlier this month. [...]
