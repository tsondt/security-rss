Title: NSO Group’s CEO-designate quits after US blacklists spyware firm
Date: 2021-11-11T11:54:33+00:00
Author: Agencies
Category: The Guardian
Tags: Israel;Surveillance;US foreign policy;Malware;Data and computer security;Middle East and North Africa;Technology;US news;World news
Slug: 2021-11-11-nso-groups-ceo-designate-quits-after-us-blacklists-spyware-firm

[Source](https://www.theguardian.com/world/2021/nov/11/nso-group-ceo-designate-quits-after-us-blacklists-spyware-firm){:target="_blank" rel="noopener"}

> Move reported by Israeli media comes after Biden administration said firm acted contrary to US security interests The chief executive officer-designate of NSO Group has resigned citing the Israeli spyware company’s blacklisting by the US Department of Commerce last week, Israeli media said on Thursday. NSO Group declined to comment. Continue reading... [...]
