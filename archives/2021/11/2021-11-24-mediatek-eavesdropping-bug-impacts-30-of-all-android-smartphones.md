Title: Mediatek eavesdropping bug impacts 30% of all Android smartphones
Date: 2021-11-24T09:23:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-11-24-mediatek-eavesdropping-bug-impacts-30-of-all-android-smartphones

[Source](https://www.bleepingcomputer.com/news/security/mediatek-eavesdropping-bug-impacts-30-percent-of-all-android-smartphones/){:target="_blank" rel="noopener"}

> MediaTek fixed security vulnerabilities that could have allowed attackers to eavesdrop on Android phone calls, execute commands, or elevate their privileges to a higher level. [...]
