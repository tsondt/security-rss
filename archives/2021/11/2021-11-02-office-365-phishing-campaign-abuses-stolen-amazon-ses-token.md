Title: Office 365 Phishing Campaign Abuses Stolen Amazon SES Token
Date: 2021-11-02T00:29:17+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2021-11-02-office-365-phishing-campaign-abuses-stolen-amazon-ses-token

[Source](https://threatpost.com/office-365-phishing-campaign-kasperskys-amazon-ses-token/175915/){:target="_blank" rel="noopener"}

> Stolen access token leveraged in phishing campaign that spoofs brand name email addresses. [...]
