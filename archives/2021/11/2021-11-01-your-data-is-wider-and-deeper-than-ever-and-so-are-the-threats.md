Title: Your data is wider and deeper than ever – and so are the threats
Date: 2021-11-01T19:30:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-11-01-your-data-is-wider-and-deeper-than-ever-and-so-are-the-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/01/data_threat/){:target="_blank" rel="noopener"}

> Looking for answers on how to protect your backups from attack? Join us and we'll share all we know Webinar Modern cybercriminals know that choking off an organization’s production data will often be enough to force it to the negotiating table, because recovering the data from backups will be simply too time consuming.... [...]
