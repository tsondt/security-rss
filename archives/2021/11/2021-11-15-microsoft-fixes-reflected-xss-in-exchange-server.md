Title: Microsoft fixes reflected XSS in Exchange Server
Date: 2021-11-15T16:28:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-15-microsoft-fixes-reflected-xss-in-exchange-server

[Source](https://portswigger.net/daily-swig/microsoft-fixes-reflected-xss-in-exchange-server){:target="_blank" rel="noopener"}

> Researchers’ bid to reproduce ProxyShell yields something entirely new [...]
