Title: Canadian teen nabbed in $36.5M crypto heist – possibly the biggest haul yet by a single individual
Date: 2021-11-18T23:04:32+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2021-11-18-canadian-teen-nabbed-in-365m-crypto-heist-possibly-the-biggest-haul-yet-by-a-single-individual

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/18/canadian_cryptocurrency_heist/){:target="_blank" rel="noopener"}

> Plus, US gov to sell off $56M of Bitcoin – the largest single sum recovered so far from a cryptocurrency fraud A Canadian teenager has been arrested for allegedly stealing $37 million worth of cryptocurrency ($46M Canadian) via a SIM swap scam, making it the largest virtual cash heist affecting a single person yet, according to police.... [...]
