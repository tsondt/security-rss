Title: NSO fails once again to claim foreign sovereign immunity in WhatsApp spying lawsuit
Date: 2021-11-09T00:53:01+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-11-09-nso-fails-once-again-to-claim-foreign-sovereign-immunity-in-whatsapp-spying-lawsuit

[Source](https://go.theregister.com/feed/www.theregister.com/2021/11/09/nso_foreign_immunity_whatsapp_decision/){:target="_blank" rel="noopener"}

> US appeals court allows legal battle to resume, says it will be an 'easy case' Spyware maker NSO Group cannot use its government clients to shield itself from litigation, a US appeals court ruled on Monday, a decision that allows WhatsApp's lawsuit against the Israel-based firm to resume.... [...]
