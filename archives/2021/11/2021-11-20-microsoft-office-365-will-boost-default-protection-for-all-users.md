Title: Microsoft: Office 365 will boost default protection for all users
Date: 2021-11-20T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-11-20-microsoft-office-365-will-boost-default-protection-for-all-users

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-office-365-will-boost-default-protection-for-all-users/){:target="_blank" rel="noopener"}

> Microsoft is rolling out Built-In Protection to Defender for Office 365, a new feature that would automatically enable recommended settings and policies to make sure all new and existing users get at least a basic level of protection. [...]
