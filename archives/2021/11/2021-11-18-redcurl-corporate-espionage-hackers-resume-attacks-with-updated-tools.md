Title: RedCurl corporate espionage hackers resume attacks with updated tools
Date: 2021-11-18T06:32:33-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-11-18-redcurl-corporate-espionage-hackers-resume-attacks-with-updated-tools

[Source](https://www.bleepingcomputer.com/news/security/redcurl-corporate-espionage-hackers-resume-attacks-with-updated-tools/){:target="_blank" rel="noopener"}

> A crew of highly-skilled hackers specialized in corporate espionage has resumed activity, one of their victims this year being a large wholesale company in Russia. [...]
