Title: Wind turbine giant Vestas confirms data breach following ‘cybersecurity incident’
Date: 2021-11-22T13:20:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-11-22-wind-turbine-giant-vestas-confirms-data-breach-following-cybersecurity-incident

[Source](https://portswigger.net/daily-swig/wind-turbine-giant-vestas-confirms-data-breach-following-cybersecurity-incident){:target="_blank" rel="noopener"}

> Danish company has also ‘initiated a gradual and controlled reopening of all IT systems’ [...]
