Title: How MikroTik Routers Became a Cybercriminal Target
Date: 2021-12-09T15:56:16+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: IoT;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2021-12-09-how-mikrotik-routers-became-a-cybercriminal-target

[Source](https://threatpost.com/mikrotik-routers-cybercriminal-target/176894/){:target="_blank" rel="noopener"}

> The powerful devices leveraged by the Meris botnet have weaknesses that make them easy to exploit, yet complex for organizations to track and secure, researchers said. [...]
