Title: 4-Year-Old Microsoft Azure Zero-Day Exposes Web App Source Code
Date: 2021-12-23T19:04:13+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: 2021-12-23-4-year-old-microsoft-azure-zero-day-exposes-web-app-source-code

[Source](https://threatpost.com/microsoft-azure-zero-day-source-code/177270/){:target="_blank" rel="noopener"}

> The security vulnerability could expose passwords and access tokens, along with blueprints for internal infrastructure and finding software vulnerabilities. [...]
