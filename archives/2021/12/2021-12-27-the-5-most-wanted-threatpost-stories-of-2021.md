Title: The 5 Most-Wanted Threatpost Stories of 2021
Date: 2021-12-27T18:57:24+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;Malware;Mobile Security;Privacy;Vulnerabilities;Web Security
Slug: 2021-12-27-the-5-most-wanted-threatpost-stories-of-2021

[Source](https://threatpost.com/5-top-threatpost-stories-2021/177278/){:target="_blank" rel="noopener"}

> A look back at what was hot with readers in this second year of the pandemic. [...]
