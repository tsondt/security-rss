Title: Log4j attackers switch to injecting Monero miners via RMI
Date: 2021-12-16T16:12:45-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-16-log4j-attackers-switch-to-injecting-monero-miners-via-rmi

[Source](https://www.bleepingcomputer.com/news/security/log4j-attackers-switch-to-injecting-monero-miners-via-rmi/){:target="_blank" rel="noopener"}

> Some threat actors exploiting the Apache Log4j vulnerability have switched from LDAP callback URLs to RMI or even used both in a single request for maximum chances of success. [...]
