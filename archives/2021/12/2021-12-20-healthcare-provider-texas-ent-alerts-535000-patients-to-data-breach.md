Title: Healthcare provider Texas ENT alerts 535,000 patients to data breach
Date: 2021-12-20T14:44:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-20-healthcare-provider-texas-ent-alerts-535000-patients-to-data-breach

[Source](https://portswigger.net/daily-swig/healthcare-provider-texas-ent-alerts-535-000-patients-to-data-breach){:target="_blank" rel="noopener"}

> Unauthorized intruder exfiltrated personal data over a six-day period [...]
