Title: Oz Feds reveal distribution model behind backdoored 'An0m' chat app spread by crims
Date: 2021-12-09T03:43:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-09-oz-feds-reveal-distribution-model-behind-backdoored-an0m-chat-app-spread-by-crims

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/09/feds_reveal_distribution_model_behind/){:target="_blank" rel="noopener"}

> Resellers were given exclusive territories to target, and offered tech support Australia's Federal Police force has revealed more about how it distributed a backdoored chat app to criminals.... [...]
