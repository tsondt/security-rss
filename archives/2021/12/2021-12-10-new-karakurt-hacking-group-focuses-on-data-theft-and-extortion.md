Title: New 'Karakurt' hacking group focuses on data theft and extortion
Date: 2021-12-10T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-10-new-karakurt-hacking-group-focuses-on-data-theft-and-extortion

[Source](https://www.bleepingcomputer.com/news/security/new-karakurt-hacking-group-focuses-on-data-theft-and-extortion/){:target="_blank" rel="noopener"}

> A sophisticated cybercrime group known as 'Karakurt' who has been quietly working from the shadows has had its tactics and procedures exposed by researchers who tracked recent cyberattacks conducted by the hackers. [...]
