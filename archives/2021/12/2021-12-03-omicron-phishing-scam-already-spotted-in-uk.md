Title: Omicron Phishing Scam Already Spotted in UK
Date: 2021-12-03T19:46:27+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2021-12-03-omicron-phishing-scam-already-spotted-in-uk

[Source](https://threatpost.com/omicron-phishing-scam-uk/176771/){:target="_blank" rel="noopener"}

> Omicron COVID-19 variant anxiety inspires new phishing scam offering fake NHS tests to steal data. [...]
