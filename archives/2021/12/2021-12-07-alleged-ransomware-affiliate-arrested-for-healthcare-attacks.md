Title: Alleged ransomware affiliate arrested for healthcare attacks
Date: 2021-12-07T15:37:57-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2021-12-07-alleged-ransomware-affiliate-arrested-for-healthcare-attacks

[Source](https://www.bleepingcomputer.com/news/security/alleged-ransomware-affiliate-arrested-for-healthcare-attacks/){:target="_blank" rel="noopener"}

> A 31-year old Canadian national has been charged in connection to ransomware attacks against organizations in the United States and Canada, a federal indictment unsealed today shows. [...]
