Title: Pandemic-Influenced Car Shopping: Just Use the Manufacturer API
Date: 2021-12-03T20:09:24+00:00
Author: Jason Kent
Category: Threatpost
Tags: InfoSec Insider;Web Security
Slug: 2021-12-03-pandemic-influenced-car-shopping-just-use-the-manufacturer-api

[Source](https://threatpost.com/pandemic-car-shopping-manufacturer-api/176740/){:target="_blank" rel="noopener"}

> Jason Kent, hacker-in-residence at Cequence, found a way to exploit a Toyota API to get around the hassle of car shopping in the age of supply-chain woes. [...]
