Title: Malicious KMSPico installers steal your cryptocurrency wallets
Date: 2021-12-04T12:06:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-12-04-malicious-kmspico-installers-steal-your-cryptocurrency-wallets

[Source](https://www.bleepingcomputer.com/news/security/malicious-kmspico-installers-steal-your-cryptocurrency-wallets/){:target="_blank" rel="noopener"}

> Threat actors are distributing altered KMSpico installers to infect Windows devices with malware that steals cryptocurrency wallets. [...]
