Title: Kronos ransomware attack may cause weeks of HR solutions downtime
Date: 2021-12-13T12:57:29-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-13-kronos-ransomware-attack-may-cause-weeks-of-hr-solutions-downtime

[Source](https://www.bleepingcomputer.com/news/security/kronos-ransomware-attack-may-cause-weeks-of-hr-solutions-downtime/){:target="_blank" rel="noopener"}

> Workforce management solutions provider Kronos has suffered a ransomware attack that will likely disrupt many of their cloud-based solutions for weeks. [...]
