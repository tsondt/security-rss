Title: Conti Gang Suspected of Ransomware Attack on McMenamins
Date: 2021-12-17T13:57:02+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-12-17-conti-gang-suspected-of-ransomware-attack-on-mcmenamins

[Source](https://threatpost.com/conti-gang-ransomware-attack-mcmenamins/177119/){:target="_blank" rel="noopener"}

> The incident occurred last weekend at the popular chain of restaurants, hotels and breweries, which is still facing disruptions. [...]
