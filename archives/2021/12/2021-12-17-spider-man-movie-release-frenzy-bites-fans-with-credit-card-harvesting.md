Title: Spider-Man Movie Release Frenzy Bites Fans with Credit-Card Harvesting
Date: 2021-12-17T19:49:15+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2021-12-17-spider-man-movie-release-frenzy-bites-fans-with-credit-card-harvesting

[Source](https://threatpost.com/spider-man-movie-credit-card-harvesting/177146/){:target="_blank" rel="noopener"}

> Attackers are using the excitement over the new Spider-Man movie to steal bank information and spread malware. [...]
