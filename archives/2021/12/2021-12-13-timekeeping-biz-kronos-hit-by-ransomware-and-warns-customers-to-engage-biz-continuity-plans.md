Title: Timekeeping biz Kronos hit by ransomware and warns customers to engage biz continuity plans
Date: 2021-12-13T15:07:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-13-timekeeping-biz-kronos-hit-by-ransomware-and-warns-customers-to-engage-biz-continuity-plans

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/13/ultimate_kronos_group_ransomware_attack/){:target="_blank" rel="noopener"}

> Big implications for millions of staffers' Christmas pay packets Kronos Private Cloud has been hit by a ransomware attack. The company, also known as Ultimate Kronos Group (UKG), provides timekeeping services to companies employing millions across worldwide.... [...]
