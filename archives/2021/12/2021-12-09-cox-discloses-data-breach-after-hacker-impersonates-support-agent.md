Title: Cox discloses data breach after hacker impersonates support agent
Date: 2021-12-09T08:58:50-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-09-cox-discloses-data-breach-after-hacker-impersonates-support-agent

[Source](https://www.bleepingcomputer.com/news/security/cox-discloses-data-breach-after-hacker-impersonates-support-agent/){:target="_blank" rel="noopener"}

> Cox Communications has disclosed a data breach after a hacker impersonated a support agent to gain access to customers' personal information. [...]
