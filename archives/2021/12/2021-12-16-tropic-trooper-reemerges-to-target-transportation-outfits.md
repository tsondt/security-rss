Title: ‘Tropic Trooper’ Reemerges to Target Transportation Outfits
Date: 2021-12-16T19:16:06+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware;Web Security
Slug: 2021-12-16-tropic-trooper-reemerges-to-target-transportation-outfits

[Source](https://threatpost.com/tropic-trooper-transportation/177106/){:target="_blank" rel="noopener"}

> Analysts warn that the attack group, now known as 'Earth Centaur,' is honing its attacks to go after transportation and government agencies. [...]
