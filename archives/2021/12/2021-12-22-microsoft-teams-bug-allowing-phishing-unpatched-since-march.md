Title: Microsoft Teams bug allowing phishing unpatched since March
Date: 2021-12-22T12:47:28-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-12-22-microsoft-teams-bug-allowing-phishing-unpatched-since-march

[Source](https://www.bleepingcomputer.com/news/security/microsoft-teams-bug-allowing-phishing-unpatched-since-march/){:target="_blank" rel="noopener"}

> Microsoft said it won't fix or is delaying patches for several security flaws impacting Microsoft Teams' link preview feature reported since March 2021. [...]
