Title: Human error bugs increasingly making a splash in hacker-powered pen tests –&nbsp;report
Date: 2021-12-10T12:12:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-10-human-error-bugs-increasingly-making-a-splash-in-hacker-powered-pen-tests-report

[Source](https://portswigger.net/daily-swig/human-error-bugs-increasingly-making-a-splash-in-hacker-powered-pen-tests-nbsp-report){:target="_blank" rel="noopener"}

> HackerOne study charts effects of digital transformation and cloud migration [...]
