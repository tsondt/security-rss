Title: Top 10 healthcare breaches in the U.S. exposed data of 19 million
Date: 2021-12-31T08:13:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-12-31-top-10-healthcare-breaches-in-the-us-exposed-data-of-19-million

[Source](https://www.bleepingcomputer.com/news/security/top-10-healthcare-breaches-in-the-us-exposed-data-of-19-million/){:target="_blank" rel="noopener"}

> The healthcare sector has been the target of hundreds of cyberattacks this year. A tally of public data breach reports so far shows that tens of millions of healthcare records have been exposed to unauthorized parties. [...]
