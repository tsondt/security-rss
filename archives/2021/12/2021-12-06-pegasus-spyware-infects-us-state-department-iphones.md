Title: Pegasus Spyware Infects U.S. State Department iPhones
Date: 2021-12-06T16:25:02+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2021-12-06-pegasus-spyware-infects-us-state-department-iphones

[Source](https://threatpost.com/pegasus-spyware-state-department-iphones/176779/){:target="_blank" rel="noopener"}

> It's unknown who's behind the cyberattacks against at least nine employees' iPhones, who are all involved in Ugandan diplomacy. [...]
