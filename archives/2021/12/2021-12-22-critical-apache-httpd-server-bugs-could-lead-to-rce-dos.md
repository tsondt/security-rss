Title: Critical Apache HTTPD Server Bugs Could Lead to RCE, DoS
Date: 2021-12-22T17:59:55+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-12-22-critical-apache-httpd-server-bugs-could-lead-to-rce-dos

[Source](https://threatpost.com/apache-httpd-server-bugs-rce-dos/177234/){:target="_blank" rel="noopener"}

> Don't freak: It's got nothing to do with Log4Shell, except it may be just as far-reaching as Log4j, given HTTPD's tendency to tiptoe into software projects. [...]
