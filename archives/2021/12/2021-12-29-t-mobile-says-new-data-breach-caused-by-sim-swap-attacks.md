Title: T-Mobile says new data breach caused by SIM swap attacks
Date: 2021-12-29T12:03:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-29-t-mobile-says-new-data-breach-caused-by-sim-swap-attacks

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-says-new-data-breach-caused-by-sim-swap-attacks/){:target="_blank" rel="noopener"}

> T-Mobile confirmed that recent reports of a new data breach are linked to notifications sent to a "very small number of customers" that they fell victim to SIM swap attacks. [...]
