Title: Google warns that NSO hacking is on par with elite nation-state spies
Date: 2021-12-18T12:30:47+00:00
Author: Ars Staff
Category: Ars Technica
Tags: Biz & IT;forced entry;wired
Slug: 2021-12-18-google-warns-that-nso-hacking-is-on-par-with-elite-nation-state-spies

[Source](https://arstechnica.com/?p=1821631){:target="_blank" rel="noopener"}

> Enlarge / A man walks by the building entrance of Israeli cyber company NSO Group at one of its branches in the Arava Desert on November 11, 2021, in Sapir, Israel. (credit: Amir Levy | Getty Images) The Israeli spyware developer NSO Group has shocked the global security community for years with aggressive and effective hacking tools that can target both Android and iOS devices. The company's products have been so abused by its customers around the world that NSO Group now faces sanctions, high-profile lawsuits, and an uncertain future. But a new analysis of the spyware maker's ForcedEntry iOS [...]
