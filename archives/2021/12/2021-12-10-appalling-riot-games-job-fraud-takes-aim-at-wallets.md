Title: ‘Appalling’ Riot Games Job Fraud Takes Aim at Wallets
Date: 2021-12-10T19:00:36+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2021-12-10-appalling-riot-games-job-fraud-takes-aim-at-wallets

[Source](https://threatpost.com/riot-games-job-fraud/176950/){:target="_blank" rel="noopener"}

> Scammers are using fake job listings to empty the wallets of young, hopeful victims looking to break into the gaming industry. [...]
