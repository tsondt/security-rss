Title: Google Shuts Down Glupteba Botnet, Sues Operators
Date: 2021-12-09T15:36:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bitcoin;blockchain;botnets;cybersecurity;Google
Slug: 2021-12-09-google-shuts-down-glupteba-botnet-sues-operators

[Source](https://www.schneier.com/blog/archives/2021/12/google-shuts-down-glupteba-botnet-sues-operators.html){:target="_blank" rel="noopener"}

> Google took steps to shut down the Glupteba botnet, at least for now. (The botnet uses the bitcoin blockchain as a backup command-and-control mechanism, making it hard to get rid of it permanently.) So Google is also suing the botnet’s operators. It’s an interesting strategy. Let’s see if it’s successful. [...]
