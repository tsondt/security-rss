Title: Safe browsing: Google fixes Chrome Site Isolation bypass bug
Date: 2021-12-21T15:20:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-21-safe-browsing-google-fixes-chrome-site-isolation-bypass-bug

[Source](https://portswigger.net/daily-swig/safe-browsing-google-fixes-chrome-site-isolation-bypass-bug){:target="_blank" rel="noopener"}

> Vulnerability in Chrome’s service worker feature created chink in browser’s armor [...]
