Title: What the Rise in Cyber-Recon Means for Your Security Strategy
Date: 2021-12-30T18:01:59+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;InfoSec Insider;IoT;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2021-12-30-what-the-rise-in-cyber-recon-means-for-your-security-strategy

[Source](https://threatpost.com/rise-cyber-recon-security-strategy/177317/){:target="_blank" rel="noopener"}

> Expect many more zero-day exploits in 2022, and cyberattacks using them being launched at a significantly higher rate, warns Aamir Lakhani, researcher at FortiGuard Labs. [...]
