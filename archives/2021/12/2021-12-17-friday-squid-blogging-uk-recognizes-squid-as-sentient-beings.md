Title: Friday Squid Blogging: UK Recognizes Squid as Sentient Beings
Date: 2021-12-17T22:01:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-12-17-friday-squid-blogging-uk-recognizes-squid-as-sentient-beings

[Source](https://www.schneier.com/blog/archives/2021/12/friday-squid-blogging-uk-recognizes-squid-as-sentient-beings.html){:target="_blank" rel="noopener"}

> This seems big : The UK government has officially included decapod crustaceans–including crabs, lobsters, and crayfish–and cephalopod mollusks–including octopuses, squid, and cuttlefish–in its Animal Welfare (Sentience) Bill. This means they are now recognized as “sentient beings” in the UK. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
