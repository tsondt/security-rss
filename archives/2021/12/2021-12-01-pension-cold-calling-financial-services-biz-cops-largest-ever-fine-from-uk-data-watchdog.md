Title: Pension cold-calling financial services biz cops largest ever fine from UK data watchdog
Date: 2021-12-01T13:50:38+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2021-12-01-pension-cold-calling-financial-services-biz-cops-largest-ever-fine-from-uk-data-watchdog

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/01/ico_issues_biggest_ever_fine/){:target="_blank" rel="noopener"}

> EB Associates facing £140k penalty for 107,000 illegal calls EB Associates, a London-based financial advisory business, is facing a £140,000 fine from the UK's data watchdog after it instigated 107,000 illegal cold calls to people about their pensions.... [...]
