Title: HCL Technologies patches serious vulnerabilities in HCL DX
Date: 2021-12-30T16:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-30-hcl-technologies-patches-serious-vulnerabilities-in-hcl-dx

[Source](https://portswigger.net/daily-swig/hcl-technologies-patches-serious-vulnerabilities-in-hcl-dx){:target="_blank" rel="noopener"}

> Disclosure process for bugs in HCL DX – formerly WebSphere Portal – initially went awry [...]
