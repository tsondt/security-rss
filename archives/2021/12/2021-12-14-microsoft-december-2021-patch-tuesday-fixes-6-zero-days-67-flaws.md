Title: Microsoft December 2021 Patch Tuesday fixes 6 zero-days, 67 flaws
Date: 2021-12-14T13:41:43-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-12-14-microsoft-december-2021-patch-tuesday-fixes-6-zero-days-67-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-december-2021-patch-tuesday-fixes-6-zero-days-67-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's December 2021 Patch Tuesday, and with it comes fixes for six zero-day vulnerabilities and a total of 67 flaws. These updates include a fix for an actively exploited Windows Installer vulnerability used in malware distribution campaigns. [...]
