Title: 2021 Wants Another Chance (A Lighter-Side Year in Review)
Date: 2021-12-28T11:00:24+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Cloud Security;Malware;Vulnerabilities;Web Security
Slug: 2021-12-28-2021-wants-another-chance-a-lighter-side-year-in-review

[Source](https://threatpost.com/2021-log4j-year-review-funny-cybersecurity/177215/){:target="_blank" rel="noopener"}

> The year wasn't ALL bad news. These sometimes cringe-worthy/sometimes laughable cybersecurity and other technology stories offer schadenfreude and WTF opportunities, and some giggles. [...]
