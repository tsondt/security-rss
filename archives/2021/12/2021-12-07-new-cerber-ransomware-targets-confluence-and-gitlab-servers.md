Title: New Cerber ransomware targets Confluence and GitLab servers
Date: 2021-12-07T13:19:53-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-07-new-cerber-ransomware-targets-confluence-and-gitlab-servers

[Source](https://www.bleepingcomputer.com/news/security/new-cerber-ransomware-targets-confluence-and-gitlab-servers/){:target="_blank" rel="noopener"}

> Cerber ransomware is back, as a new ransomware family adopts the old name and targets Atlassian Confluence and GitLab servers using remote code execution vulnerabilities. [...]
