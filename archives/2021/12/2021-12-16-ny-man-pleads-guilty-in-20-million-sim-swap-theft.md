Title: NY Man Pleads Guilty in $20 Million SIM Swap Theft
Date: 2021-12-16T17:52:03+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;SIM Swapping;Ellis Pinsky;Erin West;Michael Terpin;Nicholas Truglia;REACT Task Force;Robert Ross;SIM swapping;stopsimcrime.com
Slug: 2021-12-16-ny-man-pleads-guilty-in-20-million-sim-swap-theft

[Source](https://krebsonsecurity.com/2021/12/ny-man-pleads-guilty-in-20-million-sim-swap-theft/){:target="_blank" rel="noopener"}

> A 24-year-old New York man who bragged about helping to steal more than $20 million worth of cryptocurrency from a technology executive has pleaded guilty to conspiracy to commit wire fraud. Nicholas Truglia was part of a group alleged to have stolen more than $100 million from cryptocurrency investors using fraudulent “SIM swaps,” scams in which identity thieves hijack a target’s mobile phone number and use that to wrest control over the victim’s online identities. Truglia admitted to a New York federal court that he let a friend use his account at crypto-trading platform Binance in 2018 to launder more [...]
