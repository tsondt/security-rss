Title: Western Digital warns customers to update their My Cloud devices
Date: 2021-12-18T10:36:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-18-western-digital-warns-customers-to-update-their-my-cloud-devices

[Source](https://www.bleepingcomputer.com/news/security/western-digital-warns-customers-to-update-their-my-cloud-devices/){:target="_blank" rel="noopener"}

> Western Digital is urging customers to update their WD My Cloud devices to the latest available firmware to keep receiving security updates on My Cloud OS firmware reaching the end of support. [...]
