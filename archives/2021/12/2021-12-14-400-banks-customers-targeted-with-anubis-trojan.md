Title: 400 Banks’ Customers Targeted with Anubis Trojan
Date: 2021-12-14T20:23:48+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: 2021-12-14-400-banks-customers-targeted-with-anubis-trojan

[Source](https://threatpost.com/400-banks-targeted-anubis-trojan/177038/){:target="_blank" rel="noopener"}

> The new campaign masqueraded as an Orange Telecom account management app to deliver the latest iteration of Anubis banking malware. [...]
