Title: Four security trends for ‘22—and what to do about them
Date: 2021-12-16T17:30:00+00:00
Author: Seth Rosenblatt
Category: GCP Security
Tags: Identity & Security
Slug: 2021-12-16-four-security-trends-for-22and-what-to-do-about-them

[Source](https://cloud.google.com/blog/products/identity-security/four-security-trends-for-2022-and-what-to-do-about-them/){:target="_blank" rel="noopener"}

> When it comes to cloud security, 2022 will be the year that the past catches up with the future. Trends that businesses have been ignoring for too long will force organizations large and small to confront and control their security debt. That’s according to Google Cloud’s own cybersecurity experts, who have identified four security trends that organizations need to watch out for—and get ahead of. We have predictions on what to expect in the coming year from MK Palmore, director of the Office of the CISO; Brian Roddy, vice president of engineering for Cloud Security; Tim Dierks, engineering director for [...]
