Title: Emotet now drops Cobalt Strike, fast forwards ransomware attacks
Date: 2021-12-07T18:21:46-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-07-emotet-now-drops-cobalt-strike-fast-forwards-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/emotet-now-drops-cobalt-strike-fast-forwards-ransomware-attacks/){:target="_blank" rel="noopener"}

> In a concerning development, the notorious Emotet malware now installs Cobalt Strike beacons directly, giving immediate network access to threat actors and making ransomware attacks imminent. [...]
