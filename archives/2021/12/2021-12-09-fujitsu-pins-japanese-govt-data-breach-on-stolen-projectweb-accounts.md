Title: Fujitsu pins Japanese govt data breach on stolen ProjectWEB accounts
Date: 2021-12-09T07:47:15-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-12-09-fujitsu-pins-japanese-govt-data-breach-on-stolen-projectweb-accounts

[Source](https://www.bleepingcomputer.com/news/security/fujitsu-pins-japanese-govt-data-breach-on-stolen-projectweb-accounts/){:target="_blank" rel="noopener"}

> Fujitsu says the attackers behind the May data breach used a vulnerability in the company's ProjectWEB information-sharing tool to steal accounts from legitimate users and access proprietary data belonging to multiple Japanese government agencies. [...]
