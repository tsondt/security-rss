Title: UK government reveals plans to become ‘global cyber power’ in 2022
Date: 2021-12-16T14:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-16-uk-government-reveals-plans-to-become-global-cyber-power-in-2022

[Source](https://portswigger.net/daily-swig/uk-government-reveals-plans-to-become-global-cyber-power-in-2022){:target="_blank" rel="noopener"}

> National Cyber Security Centre leads scheme to increase capabilities [...]
