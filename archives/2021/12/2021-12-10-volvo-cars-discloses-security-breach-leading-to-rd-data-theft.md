Title: Volvo Cars discloses security breach leading to R&D data theft
Date: 2021-12-10T13:07:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-10-volvo-cars-discloses-security-breach-leading-to-rd-data-theft

[Source](https://www.bleepingcomputer.com/news/security/volvo-cars-discloses-security-breach-leading-to-randd-data-theft/){:target="_blank" rel="noopener"}

> Swedish carmaker Volvo Cars has disclosed that unknown attackers have stolen research and development information after hacking some of its servers. [...]
