Title: ALPHV BlackCat - This year's most sophisticated ransomware
Date: 2021-12-09T16:47:28-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-09-alphv-blackcat-this-years-most-sophisticated-ransomware

[Source](https://www.bleepingcomputer.com/news/security/alphv-blackcat-this-years-most-sophisticated-ransomware/){:target="_blank" rel="noopener"}

> The new ALPHV ransomware operation, aka BlackCat, launched last month and could be the most sophisticated ransomware of the year, with a highly-customizable feature set allowing for attacks on a wide range of corporate environments. [...]
