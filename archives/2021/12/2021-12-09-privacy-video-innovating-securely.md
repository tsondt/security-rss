Title: Privacy video: Innovating securely
Date: 2021-12-09T20:47:00+00:00
Author: Chad Woolf
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS security;Privacy;Security Blog
Slug: 2021-12-09-privacy-video-innovating-securely

[Source](https://aws.amazon.com/blogs/security/privacy-video-innovating-securely/){:target="_blank" rel="noopener"}

> I’m pleased to share a video of a conversation about privacy I had with my colleague Laura Dawson, the North American Lead at the AWS Institute. Privacy is becoming more of a strategic issue for our customers, similar to how security is today. We discussed how, while the two topics are similar in some ways, [...]
