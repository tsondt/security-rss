Title: Threat Group Takes Aim Again at Cloud Platform Provider Zoho
Date: 2021-12-03T13:17:47+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-12-03-threat-group-takes-aim-again-at-cloud-platform-provider-zoho

[Source](https://threatpost.com/threat-group-takes-aim-again-at-cloud-platform-provider-zoho/176732/){:target="_blank" rel="noopener"}

> Attackers that previously targeted the cloud platform provider have shifted their focus to additional products in the company’s portfolio. [...]
