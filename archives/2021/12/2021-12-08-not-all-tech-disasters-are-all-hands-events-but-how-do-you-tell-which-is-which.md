Title: Not all tech disasters are ‘all hands’ events. But how do you tell which is which?
Date: 2021-12-08T18:00:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-12-08-not-all-tech-disasters-are-all-hands-events-but-how-do-you-tell-which-is-which

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/08/preventing_cyber_disaster/){:target="_blank" rel="noopener"}

> This webinar shows you how to measure the blast radius Webinar This isn’t surprising. The prospect of having all your data and applications compromised, whether due to ransomware or other cyberattacks, or any of the more traditional disaster scenarios is so horrifying, that it’s natural to throw everything you have at it.... [...]
