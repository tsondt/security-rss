Title: In case you missed it: Google Cloud Security Talks, Zero Trust Edition
Date: 2021-12-16T17:00:00+00:00
Author: Jessica Davlin
Category: GCP Security
Tags: Events;Identity & Security
Slug: 2021-12-16-in-case-you-missed-it-google-cloud-security-talks-zero-trust-edition

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-zero-trust-security-talks-available-on-demand/){:target="_blank" rel="noopener"}

> Yesterday we held our final Google Cloud Security Talks event of 2021. Our event focused on zero trust and covered everything from Google’s history with BeyondCorp to our strategic thinking when it comes to applying zero trust principles to production environments. We shared product updates across the portfolio and talked about how zero trust fits into our invisible security vision. In case you missed the event, we’ve put together a brief recap below. Of course, we’d also encourage you to check out the sessions on-demand for all the details! Our opening keynote highlighted many recent security launches, including: the general [...]
