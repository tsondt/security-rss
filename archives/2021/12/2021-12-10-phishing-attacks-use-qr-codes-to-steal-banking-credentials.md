Title: Phishing attacks use QR codes to steal banking credentials
Date: 2021-12-10T14:10:05-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-10-phishing-attacks-use-qr-codes-to-steal-banking-credentials

[Source](https://www.bleepingcomputer.com/news/security/phishing-attacks-use-qr-codes-to-steal-banking-credentials/){:target="_blank" rel="noopener"}

> A new phishing campaign that targets German e-banking users has been underway in the last couple of weeks, involving QR codes in the credential-snatching process. [...]
