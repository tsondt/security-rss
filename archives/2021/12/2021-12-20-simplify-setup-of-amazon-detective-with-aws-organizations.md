Title: Simplify setup of Amazon Detective with AWS Organizations
Date: 2021-12-20T18:48:18+00:00
Author: Karthik Ram
Category: AWS Security
Tags: Amazon Detective;Best Practices;AWS Organizations;AWS security;Detective delegated Administrator;Detective GuardDuty support;Detective resource focused view;Detective Splunk Integration;Organization behavior graph;Splunk;Splunk Trumpet
Slug: 2021-12-20-simplify-setup-of-amazon-detective-with-aws-organizations

[Source](https://aws.amazon.com/blogs/security/simplify-setup-of-amazon-detective-with-aws-organizations/){:target="_blank" rel="noopener"}

> Amazon Detective makes it easy to analyze, investigate, and quickly identify the root cause of potential security issues or suspicious activities by collecting log data from your AWS resources. Amazon Detective simplifies the process of a deep dive into a security finding from other AWS security services, such as Amazon GuardDuty and AWS SecurityHub. Detective [...]
