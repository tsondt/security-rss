Title: Resistance is ... cheap? Cloudflare, Mandiant, and pals form incident response 'n' cyber insurance borg
Date: 2021-12-09T14:32:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-09-resistance-is-cheap-cloudflare-mandiant-and-pals-form-incident-response-n-cyber-insurance-borg

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/09/cloudflare_mandiant_cyber_protection_borg/){:target="_blank" rel="noopener"}

> Trust us with everything, croons septuple-strong partnership Cyber insurance premiums are increasing and so is infosec's determination to get a slice of that pie: Cloudflare is partnering with Mandiant, Secureworks, and Crowdstrike in a "rapid referral" partnership for under-attack companies.... [...]
