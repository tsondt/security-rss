Title: More on NSO Group and Cytrox: Two Cyberweapons Arms Manufacturers
Date: 2021-12-20T15:17:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Citizen Lab;cyberweapons;exploits;spyware
Slug: 2021-12-20-more-on-nso-group-and-cytrox-two-cyberweapons-arms-manufacturers

[Source](https://www.schneier.com/blog/archives/2021/12/more-on-nso-group-and-cytrox-two-cyberweapons-arms-manufacturers.html){:target="_blank" rel="noopener"}

> Citizen Lab published another report on the spyware used against two Egyptian nationals. One was hacked by NSO Group’s Pegasus spyware. The other was hacked both by Pegasus and by the spyware from another cyberweapons arms manufacturer: Cytrox. We haven’t heard a lot about Cytrox and its Predator spyware. According to Citzen Lab: We conducted Internet scanning for Predator spyware servers and found likely Predator customers in Armenia, Egypt, Greece, Indonesia, Madagascar, Oman, Saudi Arabia, and Serbia. Cytrox was reported to be part of Intellexa, the so-called “Star Alliance of spyware,” which was formed to compete with NSO Group, and [...]
