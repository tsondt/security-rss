Title: AWS attained MTCS Level 3 certification under the new SS584:2020 standard
Date: 2021-12-03T20:28:23+00:00
Author: Clara Lim
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Banking Regulations;FSI;Korea;MTCS;Seoul;Singapore
Slug: 2021-12-03-aws-attained-mtcs-level-3-certification-under-the-new-ss5842020-standard

[Source](https://aws.amazon.com/blogs/security/aws-attained-mtcs-level-3-certification-under-the-new-ss5842020-standard/){:target="_blank" rel="noopener"}

> We’re excited to announce the completion of the Multi-Tier Cloud Security (MTCS) Level 3 certification under the new SS584:2020 standard in November 2021 for three Amazon Web Services (AWS) Regions: Singapore, Korea, and United States, excluding AWS GovCloud (US) Regions. The new standard, released in October 2020, includes more stringent controls for greater assurance as [...]
