Title: Hardening the security of your AWS Elastic Beanstalk Application the Well-Architected way
Date: 2021-12-09T00:11:27+00:00
Author: Laurens Brinker
Category: AWS Security
Tags: AWS Elastic Beanstalk;Best Practices;Foundational (100);Data protection;default configuration;Encryption;highly available;private subnets;Security;Security Blog;Security groups;Security pillar;Well-Architected
Slug: 2021-12-09-hardening-the-security-of-your-aws-elastic-beanstalk-application-the-well-architected-way

[Source](https://aws.amazon.com/blogs/security/hardening-the-security-of-your-aws-elastic-beanstalk-application-the-well-architected-way/){:target="_blank" rel="noopener"}

> Launching an application in AWS Elastic Beanstalk is straightforward. You define a name for your application, select the platform you want to run it on (for example, Ruby), and upload the source code. The default Elastic Beanstalk configuration is intended to be a starting point which prioritizes simplicity and ease of setup. This allows you to quickly [...]
