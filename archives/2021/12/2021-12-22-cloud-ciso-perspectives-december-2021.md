Title: Cloud CISO Perspectives: December 2021
Date: 2021-12-22T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-12-22-cloud-ciso-perspectives-december-2021

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-december-2021/){:target="_blank" rel="noopener"}

> This is our last Cloud CISO Perspectives of 2021. It's been an eventful year for the cybersecurity industry, both good and bad, and I welcome the opportunities and challenges we will continue to address together in 2022. In this final post, I’ll share the latest updates from the Google Cybersecurity Action Team, new reports from Google’s security research teams and more information on Google Cloud’s Log4j impact and assessment. Update on Log4j vulnerability Google Cloud continues to actively follow the evolving security vulnerabilities in the open-source Apache “Log4j” utility and we are providing regular updates to our security advisory page. [...]
