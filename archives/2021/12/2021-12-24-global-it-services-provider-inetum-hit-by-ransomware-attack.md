Title: Global IT services provider Inetum hit by ransomware attack
Date: 2021-12-24T11:00:32-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-12-24-global-it-services-provider-inetum-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/global-it-services-provider-inetum-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Less than a week before the Christmas holiday, French IT services company Inetum Group was hit by a ransomware attack that had a limited impact on the business and its customers. [...]
