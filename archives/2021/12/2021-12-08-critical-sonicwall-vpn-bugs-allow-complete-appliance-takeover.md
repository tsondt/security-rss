Title: Critical SonicWall VPN Bugs Allow Complete Appliance Takeover
Date: 2021-12-08T19:16:54+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: 2021-12-08-critical-sonicwall-vpn-bugs-allow-complete-appliance-takeover

[Source](https://threatpost.com/critical-sonicwall-vpn-bugs-appliance-takeover/176869/){:target="_blank" rel="noopener"}

> Unauthenticated, remote attackers can achieve root-level RCE on SMA 100-series appliances. [...]
