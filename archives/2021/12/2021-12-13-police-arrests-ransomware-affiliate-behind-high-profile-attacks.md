Title: Police arrests ransomware affiliate behind high-profile attacks
Date: 2021-12-13T07:51:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-13-police-arrests-ransomware-affiliate-behind-high-profile-attacks

[Source](https://www.bleepingcomputer.com/news/security/police-arrests-ransomware-affiliate-behind-high-profile-attacks/){:target="_blank" rel="noopener"}

> Romanian law enforcement authorities arrested a ransomware affiliate suspected of hacking and stealing sensitive info from the networks of multiple high-profile companies worldwide, including a large Romanian IT company with clients from the retail, energy, and utilities sectors. [...]
