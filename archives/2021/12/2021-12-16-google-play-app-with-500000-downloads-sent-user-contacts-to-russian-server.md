Title: Google Play app with 500,000 downloads sent user contacts to Russian server
Date: 2021-12-16T22:21:55+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;android;google play;Joker;malware
Slug: 2021-12-16-google-play-app-with-500000-downloads-sent-user-contacts-to-russian-server

[Source](https://arstechnica.com/?p=1821282){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) An Android app with more than 500,000 downloads from Google Play has been caught hosting malware that surreptitiously sends users’ contacts to an attacker-controlled server and signs up users to pricey subscriptions, a security firm reported. The app, named Color Message, was still available on Google servers at the time this post was being prepared. Google removed it more than three hours after I asked the company for comment. Ostensibly, Color Message enhances text messaging by doing things such as adding emojis and blocking junk texts. But according to researchers at Pradeo Security said on [...]
