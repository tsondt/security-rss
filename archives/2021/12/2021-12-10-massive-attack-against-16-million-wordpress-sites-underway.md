Title: Massive attack against 1.6 million WordPress sites underway
Date: 2021-12-10T03:29:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-10-massive-attack-against-16-million-wordpress-sites-underway

[Source](https://www.bleepingcomputer.com/news/security/massive-attack-against-16-million-wordpress-sites-underway/){:target="_blank" rel="noopener"}

> Wordfence analysts report having detected a massive wave of attacks in the last couple of days, originating from 16,000 IPs and targeting over 1.6 million WordPress sites. [...]
