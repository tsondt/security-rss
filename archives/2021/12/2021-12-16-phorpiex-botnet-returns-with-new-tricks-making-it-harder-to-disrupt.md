Title: Phorpiex botnet returns with new tricks making it harder to disrupt
Date: 2021-12-16T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-12-16-phorpiex-botnet-returns-with-new-tricks-making-it-harder-to-disrupt

[Source](https://www.bleepingcomputer.com/news/security/phorpiex-botnet-returns-with-new-tricks-making-it-harder-to-disrupt/){:target="_blank" rel="noopener"}

> The previously shutdown Phorpiex botnet has re-emerged with new peer-to-peer command and control infrastructure, making the malware more difficult to disrupt. [...]
