Title: Swig Security Review 2021 – Part I
Date: 2021-12-29T16:11:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-29-swig-security-review-2021-part-i

[Source](https://portswigger.net/daily-swig/swig-security-review-2021-part-i){:target="_blank" rel="noopener"}

> Key thinkers on the biggest security stories and trends in 2021 [...]
