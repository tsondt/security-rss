Title: CISA warns critical infrastructure to stay vigilant for ongoing threats
Date: 2021-12-15T13:47:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-15-cisa-warns-critical-infrastructure-to-stay-vigilant-for-ongoing-threats

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-critical-infrastructure-to-stay-vigilant-for-ongoing-threats/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) warned critical infrastructure organizations today to strengthen their cybersecurity defenses against potential and ongoing threats. [...]
