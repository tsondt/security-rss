Title: European Cybercrime Centre confident it's kicked credit card crims – again
Date: 2021-12-02T08:21:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-02-european-cybercrime-centre-confident-its-kicked-credit-card-crims-again

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/02/european_cybercrime_centre_carding_action_2021/){:target="_blank" rel="noopener"}

> Poised to reveal similar haul to 2020's €40M loss prevention total The European Cybercrime Centre has again acted against credit card fraud and is poised to reveal success on a similar scale to its 2020 campaign that prevented €40 million of losses.... [...]
