Title: Spar shops across northern UK shut after cyber attack hits payment processing abilities
Date: 2021-12-06T18:41:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-06-spar-shops-across-northern-uk-shut-after-cyber-attack-hits-payment-processing-abilities

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/06/spar_cyber_attack/){:target="_blank" rel="noopener"}

> Franchisees' closures also affect petrol stations The British arm of Dutch supermarket chain Spar has shut hundreds of shops after suffering an "online attack," the company has confirmed to The Register.... [...]
