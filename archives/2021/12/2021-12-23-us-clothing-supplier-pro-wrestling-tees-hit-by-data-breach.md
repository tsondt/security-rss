Title: US clothing supplier Pro Wrestling Tees hit by data breach
Date: 2021-12-23T14:22:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-23-us-clothing-supplier-pro-wrestling-tees-hit-by-data-breach

[Source](https://portswigger.net/daily-swig/us-clothing-supplier-pro-wrestling-tees-hit-by-data-breach){:target="_blank" rel="noopener"}

> Law enforcement alerted company to compromise of payment card info [...]
