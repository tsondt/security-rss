Title: Threat actors steal $80 million per month with fake giveaways, surveys
Date: 2021-12-21T12:51:27-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-21-threat-actors-steal-80-million-per-month-with-fake-giveaways-surveys

[Source](https://www.bleepingcomputer.com/news/security/threat-actors-steal-80-million-per-month-with-fake-giveaways-surveys/){:target="_blank" rel="noopener"}

> Scammers are estimated to have made $80 million per month by impersonating popular brands asking people to participate in fake surveys or giveaways. [...]
