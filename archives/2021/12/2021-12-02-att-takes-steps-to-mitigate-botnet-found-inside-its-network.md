Title: AT&T Takes Steps to Mitigate Botnet Found Inside Its Network
Date: 2021-12-02T17:35:06+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Malware;Mobile Security
Slug: 2021-12-02-att-takes-steps-to-mitigate-botnet-found-inside-its-network

[Source](https://threatpost.com/att-botnet-network/176711/){:target="_blank" rel="noopener"}

> AT&T is battling a modular malware called EwDoor on 5,700 VoIP servers, but it could have a larger wildcard certificate problem. [...]
