Title: BadgerDAO DeFi defunded as hackers apparently nab millions in crypto tokens
Date: 2021-12-02T22:58:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-12-02-badgerdao-defi-defunded-as-hackers-apparently-nab-millions-in-crypto-tokens

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/02/badgerdao_coin_theft/){:target="_blank" rel="noopener"}

> Badger, badger, badger, coin theft, coin theft! BadgerDAO, maker of a decentralized finance (DeFi) protocol, said on Wednesday that it is investigating reports that millions in user funds have been stolen.... [...]
