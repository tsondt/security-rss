Title: Flaws in Tonga’s top-level domain left Google, Amazon, Tether web services vulnerable to takeover
Date: 2021-12-07T16:43:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-07-flaws-in-tongas-top-level-domain-left-google-amazon-tether-web-services-vulnerable-to-takeover

[Source](https://portswigger.net/daily-swig/flaws-in-tongas-top-level-domain-left-google-amazon-tether-web-services-vulnerable-to-takeover){:target="_blank" rel="noopener"}

> Misaligned incentives are undermining efforts to tackle TLD bugs with ‘mass-scale impact’ [...]
