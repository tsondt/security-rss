Title: Cabinet Office fined £500,000 over New Year honours list data breach
Date: 2021-12-02T12:46:01+00:00
Author: Dan Milmo Technology editor
Category: The Guardian
Tags: Data protection;Information commissioner;Politics;New Year honours list;Honours system;UK news;Data and computer security;Privacy;Technology;Elton John;Ben Stokes;Iain Duncan Smith;Simon Stevens
Slug: 2021-12-02-cabinet-office-fined-500000-over-new-year-honours-list-data-breach

[Source](https://www.theguardian.com/technology/2021/dec/02/cabinet-office-fined-new-year-honours-list-data-breach){:target="_blank" rel="noopener"}

> Regulator says safety of hundreds of individuals was jeopardised after their addresses were posted online The Cabinet Office has been fined £500,000 by the UK’s data watchdog after the postal addresses of the 2020 New Year honours recipients were disclosed online. The Information Commissioner’s Office (ICO) found officials failed to put in place “appropriate technical and organisational measures” to prevent the unauthorised disclosure of personal information in breach of data protection law. Continue reading... [...]
