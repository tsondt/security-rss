Title: East Londoners nicked under Computer Misuse Act after NHS vaccine passport app sprouted clump of fake entries
Date: 2021-12-16T16:04:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-16-east-londoners-nicked-under-computer-misuse-act-after-nhs-vaccine-passport-app-sprouted-clump-of-fake-entries

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/16/fake_nhs_vaccine_passport_arrests_met_police/){:target="_blank" rel="noopener"}

> App runs off a database, and databases are run by humans British police have made a series of arrests over the past few months after people with apparent access to NHS databases allegedly sold fake vaccination status entries on the NHS vaccine passport app.... [...]
