Title: Data breach at Florida school district impacts 50,000 students and employees
Date: 2021-12-02T12:50:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-02-data-breach-at-florida-school-district-impacts-50000-students-and-employees

[Source](https://portswigger.net/daily-swig/data-breach-at-florida-school-district-impacts-50-000-students-and-employees){:target="_blank" rel="noopener"}

> Broward County School District backtracks after ransomware attack [...]
