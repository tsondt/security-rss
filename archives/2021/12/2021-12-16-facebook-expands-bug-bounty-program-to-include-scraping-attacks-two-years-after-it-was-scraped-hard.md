Title: Facebook expands bug bounty program to include scraping attacks, two years after it was scraped – hard
Date: 2021-12-16T01:33:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-16-facebook-expands-bug-bounty-program-to-include-scraping-attacks-two-years-after-it-was-scraped-hard

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/16/facebook_scraping_bug_bounties/){:target="_blank" rel="noopener"}

> But still allows limited harvesting Meta has expanded its bug bounty program to include payouts for reports of scraping attacks on Facebook – but hold your applause.... [...]
