Title: Log4j: List of vulnerable products and vendor advisories
Date: 2021-12-14T02:46:48-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-12-14-log4j-list-of-vulnerable-products-and-vendor-advisories

[Source](https://www.bleepingcomputer.com/news/security/log4j-list-of-vulnerable-products-and-vendor-advisories/){:target="_blank" rel="noopener"}

> News about a critical vulnerability in the Apache Log4j logging library broke last week when proof-of-concept exploits started to emerge on Thursday. [...]
