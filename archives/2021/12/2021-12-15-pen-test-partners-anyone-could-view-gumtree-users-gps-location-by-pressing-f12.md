Title: Pen Test Partners: Anyone could view Gumtree users' GPS location by pressing F12
Date: 2021-12-15T15:31:17+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-15-pen-test-partners-anyone-could-view-gumtree-users-gps-location-by-pressing-f12

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/15/gumtree_data_breach_idor_f12_badness/){:target="_blank" rel="noopener"}

> And online flea market had IDOR in an iOS-focused API UK online used goods bazaar Gumtree exposed its users' home addresses in the source code of its webpages, and then tried to squirm out of a bug bounty after infosec bods alerted it to the flaw.... [...]
