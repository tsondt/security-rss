Title: Windows 'InstallerFileTakeOver' zero-day bug gets free micropatch
Date: 2021-12-09T03:22:11-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-12-09-windows-installerfiletakeover-zero-day-bug-gets-free-micropatch

[Source](https://www.bleepingcomputer.com/news/security/windows-installerfiletakeover-zero-day-bug-gets-free-micropatch/){:target="_blank" rel="noopener"}

> An unofficial patch is available for a zero-day vulnerability that is actively exploited in the wild to gain administrator privileges. [...]
