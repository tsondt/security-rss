Title: Nine WiFi routers used by millions were vulnerable to 226 flaws
Date: 2021-12-02T09:30:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2021-12-02-nine-wifi-routers-used-by-millions-were-vulnerable-to-226-flaws

[Source](https://www.bleepingcomputer.com/news/security/nine-wifi-routers-used-by-millions-were-vulnerable-to-226-flaws/){:target="_blank" rel="noopener"}

> Security researchers analyzed nine popular WiFi routers and found a total of 226 potential vulnerabilities in them, even when running the latest firmware. [...]
