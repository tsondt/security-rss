Title: ‘Over-permissive’ authentication checks left 190 Australian organizations vulnerable to business email compromise attacks
Date: 2021-12-01T15:32:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-01-over-permissive-authentication-checks-left-190-australian-organizations-vulnerable-to-business-email-compromise-attacks

[Source](https://portswigger.net/daily-swig/over-permissive-authentication-checks-left-190-australian-organizations-vulnerable-to-business-email-compromise-attacks){:target="_blank" rel="noopener"}

> Mail servers readily hijacked due to MSP oversight [...]
