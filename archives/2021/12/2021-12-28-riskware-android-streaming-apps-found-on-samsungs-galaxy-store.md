Title: Riskware Android streaming apps found on Samsung's Galaxy store
Date: 2021-12-28T11:38:39-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-12-28-riskware-android-streaming-apps-found-on-samsungs-galaxy-store

[Source](https://www.bleepingcomputer.com/news/security/riskware-android-streaming-apps-found-on-samsungs-galaxy-store/){:target="_blank" rel="noopener"}

> Samsung's official Android app store, called the Galaxy Store, has had an infiltration of riskware apps that triggered multiple Play Protect warnings on people's devices. [...]
