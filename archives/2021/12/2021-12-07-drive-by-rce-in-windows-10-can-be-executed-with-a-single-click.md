Title: Drive-by RCE in Windows 10 ‘can be executed with a single click’
Date: 2021-12-07T16:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-07-drive-by-rce-in-windows-10-can-be-executed-with-a-single-click

[Source](https://portswigger.net/daily-swig/drive-by-rce-in-windows-10-can-be-executed-with-a-single-click){:target="_blank" rel="noopener"}

> Underlying security vulnerability is still present in popular OS, researchers warn [...]
