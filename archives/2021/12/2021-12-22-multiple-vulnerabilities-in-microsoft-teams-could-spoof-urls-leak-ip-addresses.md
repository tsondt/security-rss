Title: Multiple vulnerabilities in Microsoft Teams could spoof URLs, leak IP addresses
Date: 2021-12-22T13:21:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-22-multiple-vulnerabilities-in-microsoft-teams-could-spoof-urls-leak-ip-addresses

[Source](https://portswigger.net/daily-swig/multiple-vulnerabilities-in-microsoft-teams-could-spoof-urls-leak-ip-addresses){:target="_blank" rel="noopener"}

> Only one of the issues has so far been patched [...]
