Title: Malicious Joker App Scores Half-Million Downloads on Google Play
Date: 2021-12-17T19:23:09+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2021-12-17-malicious-joker-app-scores-half-million-downloads-on-google-play

[Source](https://threatpost.com/malicious-joker-app-downloads-google-play/177139/){:target="_blank" rel="noopener"}

> Joker malware was found lurking in the Color Message app, ready to fleece unsuspecting users with premium SMS charges. [...]
