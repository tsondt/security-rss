Title: Planned Parenthood Breach Opens Patients to Follow-On Attacks
Date: 2021-12-02T19:29:18+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Privacy
Slug: 2021-12-02-planned-parenthood-breach-opens-patients-to-follow-on-attacks

[Source](https://threatpost.com/planned-parenthood-breach-attacks/176718/){:target="_blank" rel="noopener"}

> Cyberattackers made off with addresses, insurance information, dates of birth, and most worryingly, clinical information, such as diagnosis, procedures, and/or prescription information. [...]
