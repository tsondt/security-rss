Title: Apple fixes macOS security flaw behind Gatekeeper bypass
Date: 2021-12-23T17:09:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2021-12-23-apple-fixes-macos-security-flaw-behind-gatekeeper-bypass

[Source](https://www.bleepingcomputer.com/news/apple/apple-fixes-macos-security-flaw-behind-gatekeeper-bypass/){:target="_blank" rel="noopener"}

> Apple has addressed a macOS vulnerability that unsigned and unnotarized script-based apps could exploit to bypass all macOS security protection mechanisms even on fully patched systems. [...]
