Title: Canadian Ransomware Arrest Is a Meaningful Flex, Experts Say
Date: 2021-12-09T21:09:49+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware
Slug: 2021-12-09-canadian-ransomware-arrest-is-a-meaningful-flex-experts-say

[Source](https://threatpost.com/canadian-ransomware-arrest/176905/){:target="_blank" rel="noopener"}

> U.S. and Canada charge Ottawa man for ransomware attacks, signaling that North America is no cybercriminal haven. [...]
