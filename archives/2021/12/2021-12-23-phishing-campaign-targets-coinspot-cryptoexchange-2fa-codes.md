Title: Phishing campaign targets CoinSpot cryptoexchange 2FA codes
Date: 2021-12-23T13:31:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-12-23-phishing-campaign-targets-coinspot-cryptoexchange-2fa-codes

[Source](https://www.bleepingcomputer.com/news/security/phishing-campaign-targets-coinspot-cryptoexchange-2fa-codes/){:target="_blank" rel="noopener"}

> A new phishing campaign that targets users of the CoinSpot cryptocurrency exchange employs a new theme that revolves around withdrawal confirmations. [...]
