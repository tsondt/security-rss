Title: Apple AirTags Are Being Used to Track People and Cars
Date: 2021-12-31T15:52:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;cars;geolocation;privacy;surveillance;tracking
Slug: 2021-12-31-apple-airtags-are-being-used-to-track-people-and-cars

[Source](https://www.schneier.com/blog/archives/2021/12/apple-airtags-are-being-used-to-track-people-and-cars.html){:target="_blank" rel="noopener"}

> This development suprises no one who has been paying attention: Researchers now believe AirTags, which are equipped with Bluetooth technology, could be revealing a more widespread problem of tech-enabled tracking. They emit a digital signal that can be detected by devices running Apple’s mobile operating system. Those devices then report where an AirTag has last been seen. Unlike similar tracking products from competitors such as Tile, Apple added features to prevent abuse, including notifications like the one Ms. Estrada received and automatic beeping. (Tile plans to release a feature to prevent the tracking of people next year, a spokeswoman for [...]
