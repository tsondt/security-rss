Title: VirusTotal Collections feature helps keep neat IoC lists
Date: 2021-12-01T05:33:22-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-12-01-virustotal-collections-feature-helps-keep-neat-ioc-lists

[Source](https://www.bleepingcomputer.com/news/security/virustotal-collections-feature-helps-keep-neat-ioc-lists/){:target="_blank" rel="noopener"}

> Scanning service VirusTotal announced today a new feature called Collections that lets researchers create and share reports with indicators of compromise observed in security incidents. [...]
