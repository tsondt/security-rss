Title: Conti ransomware uses Log4j bug to hack VMware vCenter servers
Date: 2021-12-17T10:00:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-12-17-conti-ransomware-uses-log4j-bug-to-hack-vmware-vcenter-servers

[Source](https://www.bleepingcomputer.com/news/security/conti-ransomware-uses-log4j-bug-to-hack-vmware-vcenter-servers/){:target="_blank" rel="noopener"}

> Conti ransomware operation is using the critical Log4Shell exploit to gain rapid access to internal VMware vCenter Server instances and encrypt virtual machines. [...]
