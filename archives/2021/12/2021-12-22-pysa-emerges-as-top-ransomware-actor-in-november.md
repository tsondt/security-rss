Title: PYSA Emerges as Top Ransomware Actor in November
Date: 2021-12-22T18:39:08+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Most Recent ThreatLists
Slug: 2021-12-22-pysa-emerges-as-top-ransomware-actor-in-november

[Source](https://threatpost.com/pysa-top-ransomware-november/177242/){:target="_blank" rel="noopener"}

> Overtaking the Conti ransomware gang, PYSA finds success with government-sector attacks. [...]
