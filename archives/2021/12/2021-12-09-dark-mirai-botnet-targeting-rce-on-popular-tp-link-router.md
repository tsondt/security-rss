Title: Dark Mirai botnet targeting RCE on popular TP-Link router
Date: 2021-12-09T12:14:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-09-dark-mirai-botnet-targeting-rce-on-popular-tp-link-router

[Source](https://www.bleepingcomputer.com/news/security/dark-mirai-botnet-targeting-rce-on-popular-tp-link-router/){:target="_blank" rel="noopener"}

> The botnet known as Dark Mirai (aka MANGA) has been observed exploiting a new vulnerability on the TP-Link TL-WR840N EU V5, a popular inexpensive home router released in 2017. [...]
