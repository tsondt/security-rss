Title: The Week in Ransomware - December 3rd 2021 - Seizing Bitcoin
Date: 2021-12-03T18:34:06-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-03-the-week-in-ransomware-december-3rd-2021-seizing-bitcoin

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-3rd-2021-seizing-bitcoin/){:target="_blank" rel="noopener"}

> For this week's 'Week in Ransomware' article we have included the latest ransomware news over the past two weeks. [...]
