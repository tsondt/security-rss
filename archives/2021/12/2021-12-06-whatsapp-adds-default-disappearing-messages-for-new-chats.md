Title: WhatsApp adds default disappearing messages for new chats
Date: 2021-12-06T11:17:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-06-whatsapp-adds-default-disappearing-messages-for-new-chats

[Source](https://www.bleepingcomputer.com/news/security/whatsapp-adds-default-disappearing-messages-for-new-chats/){:target="_blank" rel="noopener"}

> WhatsApp announced today that it had expanded the privacy control features with the addition of default disappearing messages for all newly initiated chats. [...]
