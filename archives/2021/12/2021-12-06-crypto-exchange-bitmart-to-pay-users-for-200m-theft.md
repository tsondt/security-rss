Title: Crypto-Exchange BitMart to Pay Users for $200M Theft
Date: 2021-12-06T22:09:50+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;News;Web Security
Slug: 2021-12-06-crypto-exchange-bitmart-to-pay-users-for-200m-theft

[Source](https://threatpost.com/crypto-exchange-bitmart-theft/176805/){:target="_blank" rel="noopener"}

> BitMart confirmed it had been drained of ~$150 million in cryptocurrency assets, but a blockchain security firm said it's closer to $200 million. [...]
