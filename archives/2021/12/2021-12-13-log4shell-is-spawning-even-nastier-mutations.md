Title: Log4Shell Is Spawning Even Nastier Mutations
Date: 2021-12-13T18:14:46+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Malware;Vulnerabilities;Web Security
Slug: 2021-12-13-log4shell-is-spawning-even-nastier-mutations

[Source](https://threatpost.com/apache-log4j-log4shell-mutations/176962/){:target="_blank" rel="noopener"}

> The cybersecurity Hiroshima of the year – the Apache Log4j logging library exploit – has spun off 60 bigger mutations in less than a day, researchers said. [...]
