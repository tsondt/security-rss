Title: Two Active Directory Bugs Lead to Easy Windows Domain Takeover
Date: 2021-12-21T16:46:02+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-12-21-two-active-directory-bugs-lead-to-easy-windows-domain-takeover

[Source](https://threatpost.com/active-directory-bugs-windows-domain-takeover/177185/){:target="_blank" rel="noopener"}

> Microsoft is urging customers to patch two Active Directory domain controller bugs after a PoC tool was publicly released on Dec. 12. [...]
