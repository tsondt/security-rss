Title: Who Is the Network Access Broker ‘Babam’?
Date: 2021-12-03T21:53:44+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;Ransomware;Babam;bo3dom bo3domster;Bureau Veritas;Constella Intelligence;domaintools;Flashpoint;LockBit;ransomware;Rekvizitai.vz
Slug: 2021-12-03-who-is-the-network-access-broker-babam

[Source](https://krebsonsecurity.com/2021/12/who-is-the-network-access-broker-babam/){:target="_blank" rel="noopener"}

> Rarely do cybercriminal gangs that deploy ransomware gain the initial access to the target themselves. More commonly, that access is purchased from a cybercriminal broker who specializes in acquiring remote access credentials — such as usernames and passwords needed to remotely connect to the target’s network. In this post we’ll look at the clues left behind by “ Babam,” the handle chosen by a cybercriminal who has sold such access to ransomware groups on many occasions over the past few years. Since the beginning of 2020, Babam has set up numerous auctions on the Russian-language cybercrime forum Exploit, mainly selling [...]
