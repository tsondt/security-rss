Title: Grafana urges web devs to update following path traversal bug disclosure
Date: 2021-12-09T13:43:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-09-grafana-urges-web-devs-to-update-following-path-traversal-bug-disclosure

[Source](https://portswigger.net/daily-swig/grafana-urges-web-devs-to-update-following-path-traversal-bug-disclosure){:target="_blank" rel="noopener"}

> ‘With all good intentions, I had placed the Grafana team in a bit of a stressful situation,’ researcher admits [...]
