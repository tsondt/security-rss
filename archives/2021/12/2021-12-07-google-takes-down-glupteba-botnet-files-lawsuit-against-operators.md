Title: Google Takes Down Glupteba Botnet; Files Lawsuit Against Operators
Date: 2021-12-07T17:13:51+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-12-07-google-takes-down-glupteba-botnet-files-lawsuit-against-operators

[Source](https://threatpost.com/google-glupteba-botnet-lawsuit/176826/){:target="_blank" rel="noopener"}

> The malware's unique blockchain-enabled backup C2 scheme makes it difficult to eliminate completely. [...]
