Title: Cyber Command Publicly Joins Fight Against Ransomware Groups
Date: 2021-12-06T20:45:19+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware
Slug: 2021-12-06-cyber-command-publicly-joins-fight-against-ransomware-groups

[Source](https://threatpost.com/cyber-command-ransomware-groups/176801/){:target="_blank" rel="noopener"}

> U.S. military acknowledges targeting cybercriminals who launch attacks on U.S. companies. [...]
