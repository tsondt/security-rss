Title: Really stupid “smart contract” bug let hackers steal $31 million in digital coin
Date: 2021-12-01T23:41:36+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;cryptocurrency;defi;hacking
Slug: 2021-12-01-really-stupid-smart-contract-bug-let-hackers-steal-31-million-in-digital-coin

[Source](https://arstechnica.com/?p=1817374){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Blockchain startup MonoX Finance said on Wednesday that a hacker stole $31 million by exploiting a bug in software the service uses to draft smart contracts. The company uses a decentralized finance protocol known as MonoX that lets users trade digital currency tokens without some of the requirements of traditional exchanges. “Project owners can list their tokens without the burden of capital requirements and focus on using funds for building the project instead of providing liquidity,” MonoX company representatives say here. “It works by grouping deposited tokens into a virtual pair with vCASH, to offer [...]
