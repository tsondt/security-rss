Title: Upgraded to log4j 2.16? Surprise, there's a 2.17 fixing DoS
Date: 2021-12-18T05:29:24-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-12-18-upgraded-to-log4j-216-surprise-theres-a-217-fixing-dos

[Source](https://www.bleepingcomputer.com/news/security/upgraded-to-log4j-216-surprise-theres-a-217-fixing-dos/){:target="_blank" rel="noopener"}

> Yesterday, BleepingComputer summed up all the log4j and logback CVEs known thus far. Ever since the critical log4j zero-day saga began last week, security experts have time and time again recommended version 2.16 as the safest release to be on. That changes today with version 2.17.0 out that fixes CVE-2021-45105, a DoS vulnerability. [...]
