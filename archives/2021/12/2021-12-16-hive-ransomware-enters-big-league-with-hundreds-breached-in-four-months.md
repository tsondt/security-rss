Title: Hive ransomware enters big league with hundreds breached in four months
Date: 2021-12-16T10:14:50-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-12-16-hive-ransomware-enters-big-league-with-hundreds-breached-in-four-months

[Source](https://www.bleepingcomputer.com/news/security/hive-ransomware-enters-big-league-with-hundreds-breached-in-four-months/){:target="_blank" rel="noopener"}

> The Hive ransomware gang is more active and aggressive than its leak site shows, with affiliates attacking an average of three companies every day since the operation became known in late June. [...]
