Title: Blackmagic fixes critical DaVinci Resolve code execution flaws
Date: 2021-12-24T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-24-blackmagic-fixes-critical-davinci-resolve-code-execution-flaws

[Source](https://www.bleepingcomputer.com/news/security/blackmagic-fixes-critical-davinci-resolve-code-execution-flaws/){:target="_blank" rel="noopener"}

> Blackmagic Software has recently addressed two security vulnerabilities in the highly popular DaVinci Resolve software that would allow attackers to gain code execution on unpatched systems. [...]
