Title: Telegram Abused to Steal Crypto-Wallet Credentials
Date: 2021-12-23T16:00:22+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Malware;Web Security
Slug: 2021-12-23-telegram-abused-to-steal-crypto-wallet-credentials

[Source](https://threatpost.com/telegram-steal-crypto-wallet-credentials/177266/){:target="_blank" rel="noopener"}

> Attackers use the Telegram handle “Smokes Night” to spread the malicious Echelon infostealer, which steals credentials for cryptocurrency and other user accounts, researchers said. [...]
