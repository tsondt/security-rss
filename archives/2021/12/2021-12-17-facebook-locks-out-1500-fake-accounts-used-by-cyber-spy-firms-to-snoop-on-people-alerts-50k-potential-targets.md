Title: Facebook locks out 1,500 fake accounts used by cyber-spy firms to snoop on people, alerts 50k potential targets
Date: 2021-12-17T01:41:32+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-12-17-facebook-locks-out-1500-fake-accounts-used-by-cyber-spy-firms-to-snoop-on-people-alerts-50k-potential-targets

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/17/cyber_spying_firms_facebook_meta/){:target="_blank" rel="noopener"}

> Meta adverse to internet mercenaries using its social networks to help governments violate human rights Facebook successor Meta on Thursday said it canceled 1,500 social media accounts used by seven surveillance-for-hire firms to conduct online attacks against government critics and members of civil society.... [...]
