Title: FTC: Americans lost $148 million to gift card scams this year
Date: 2021-12-12T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-12-ftc-americans-lost-148-million-to-gift-card-scams-this-year

[Source](https://www.bleepingcomputer.com/news/security/ftc-americans-lost-148-million-to-gift-card-scams-this-year/){:target="_blank" rel="noopener"}

> The US Federal Trade Commission (FTC) said Americans reported losing $148 million to gift card scams during the first nine months of 2021 following a major increase compared to last year. [...]
