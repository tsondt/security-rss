Title: Four years: that’s how long Azure’s App Service had a source code leak bug
Date: 2021-12-24T06:01:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-24-four-years-thats-how-long-azures-app-service-had-a-source-code-leak-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/24/azure_app_service_not_legit_source_code_leak/){:target="_blank" rel="noopener"}

> Firm that found the flaw also spotted ChaosDB and OMIGOD, confident this one’s been exploited Microsoft has revealed a vulnerability in its Azure App Service for Linux allowed the download of files that users almost certainly did not intend to be made public.... [...]
