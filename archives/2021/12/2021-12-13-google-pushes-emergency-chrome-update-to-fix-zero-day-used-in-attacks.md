Title: Google pushes emergency Chrome update to fix zero-day used in attacks
Date: 2021-12-13T17:31:38-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2021-12-13-google-pushes-emergency-chrome-update-to-fix-zero-day-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-pushes-emergency-chrome-update-to-fix-zero-day-used-in-attacks/){:target="_blank" rel="noopener"}

> Google has released Chrome 96.0.4664.110 for Windows, Mac, and Linux, to address a high-severity zero-day vulnerability exploited in the wild. [...]
