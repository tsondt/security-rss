Title: Not with a Bang but a Whisper: The Shift to Stealthy C2
Date: 2021-12-08T19:28:35+00:00
Author: Nate Warfield
Category: Threatpost
Tags: InfoSec Insider;Vulnerabilities;Web Security
Slug: 2021-12-08-not-with-a-bang-but-a-whisper-the-shift-to-stealthy-c2

[Source](https://threatpost.com/tactics-attackers-stealthy-c2/176853/){:target="_blank" rel="noopener"}

> DoH! Nate Warfield, CTO of Prevailion, discusses new stealth tactics threat actors are using for C2, including Malleable C2 from Cobalt Strike's arsenal. [...]
