Title: University loses 77TB of research data due to backup error
Date: 2021-12-30T11:02:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-30-university-loses-77tb-of-research-data-due-to-backup-error

[Source](https://www.bleepingcomputer.com/news/security/university-loses-77tb-of-research-data-due-to-backup-error/){:target="_blank" rel="noopener"}

> The Kyoto University in Japan has lost about 77TB of research data due to an error in the backup system of its Hewlett-Packard supercomputer. [...]
