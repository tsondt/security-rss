Title: Global Cyberattacks from Nation-State Actors Posing Greater Threats
Date: 2021-12-27T19:34:19+00:00
Author: Casey Ellis
Category: Threatpost
Tags: Breach;Cloud Security;Critical Infrastructure;Government;InfoSec Insider;IoT;Malware;Vulnerabilities;Web Security
Slug: 2021-12-27-global-cyberattacks-from-nation-state-actors-posing-greater-threats

[Source](https://threatpost.com/global-cyberattacks-nation-state-threats/177253/){:target="_blank" rel="noopener"}

> Casey Ellis, CTO at Bugcrowd, outlines how international relations have deteriorated into a new sort of Cold War, with espionage playing out in the cyber-domain. [...]
