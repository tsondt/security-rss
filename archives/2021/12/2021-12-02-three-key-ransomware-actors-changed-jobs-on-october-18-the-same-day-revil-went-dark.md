Title: Three key ransomware actors changed jobs on October 18 – the same day REvil went dark
Date: 2021-12-02T05:58:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-02-three-key-ransomware-actors-changed-jobs-on-october-18-the-same-day-revil-went-dark

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/02/ransomware_forums_revealed/){:target="_blank" rel="noopener"}

> Underground industry grows in complexity and sophistication, says Santander Group researcher October 18, 2021, was a tricky day for the ransomware industry. First, the gang that ran the REvil ransomware had its servers compromised, and then three individuals with key roles changed jobs.... [...]
