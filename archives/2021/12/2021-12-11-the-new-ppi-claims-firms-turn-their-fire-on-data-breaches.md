Title: The new PPI? Claims firms turn their fire on data breaches
Date: 2021-12-11T07:00:04+00:00
Author: Rupert Jones
Category: The Guardian
Tags: Consumer affairs;Money;Data and computer security;UK news;Privacy;Business
Slug: 2021-12-11-the-new-ppi-claims-firms-turn-their-fire-on-data-breaches

[Source](https://www.theguardian.com/money/2021/dec/11/the-new-ppi-claims-firms-turn-their-fire-on-data-breaches){:target="_blank" rel="noopener"}

> People are being told they are entitled to compensation as more companies move into the industry Claims companies and law firms looking for the next bonanza in payouts are targeting people who have been the victim of a data breach, with some telling those affected they could be entitled to thousands of pounds in compensation. A Google search for the term “data breach claim” results in a long list of firms – the vast majority of them no-win, no-fee solicitors – and there are more moving into this space all the time. Meanwhile, adverts for firms are increasingly appearing in [...]
