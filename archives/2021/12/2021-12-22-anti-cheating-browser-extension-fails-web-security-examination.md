Title: Anti-cheating browser extension fails web security examination
Date: 2021-12-22T15:24:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-22-anti-cheating-browser-extension-fails-web-security-examination

[Source](https://portswigger.net/daily-swig/anti-cheating-browser-extension-fails-web-security-examination){:target="_blank" rel="noopener"}

> XSS flaw in Proctorio gets resolved [...]
