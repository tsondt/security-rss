Title: As Twitter removes blue badges for many, phishing targets verified accounts
Date: 2021-12-05T04:50:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-12-05-as-twitter-removes-blue-badges-for-many-phishing-targets-verified-accounts

[Source](https://www.bleepingcomputer.com/news/security/as-twitter-removes-blue-badges-for-many-phishing-targets-verified-accounts/){:target="_blank" rel="noopener"}

> A new phishing campaign has been targeting verified Twitter accounts, as seen by BleepingComputer. The phishing campaign follows Twitter's recent removal of the checkmark from a number of verified accounts, citing that these were ineligible for the legendary status, and verified in error. [...]
