Title: Friday Squid Blogging: Deep-Dwelling Squid
Date: 2021-12-31T22:03:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2021-12-31-friday-squid-blogging-deep-dwelling-squid

[Source](https://www.schneier.com/blog/archives/2021/12/friday-squid-blogging-deep-dwelling-squid.html){:target="_blank" rel="noopener"}

> We have discovered a squid — (Oegopsida, Magnapinnidae, Magnapinna sp.) — that lives at 6,000 meters deep. :They’re really weird,” says Vecchione. “They drift along with their arms spread out and these really long, skinny, spaghetti-like extensions dangling down underneath them.” Microscopic suckers on those filaments enable the squid to capture their prey. But the squid that Jamieson and Vecchione saw in the footage captured 6,212 meters below the ocean’s surface is a small one. They estimate that its mantle measured 10 centimeters long — ­about a third the size of the largest-known magnapinnid. And the characteristically long extensions observed [...]
