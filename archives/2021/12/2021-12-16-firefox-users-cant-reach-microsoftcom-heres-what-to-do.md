Title: Firefox users can't reach Microsoft.com — here's what to do
Date: 2021-12-16T03:15:13-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-12-16-firefox-users-cant-reach-microsoftcom-heres-what-to-do

[Source](https://www.bleepingcomputer.com/news/security/firefox-users-cant-reach-microsoftcom-heres-what-to-do/){:target="_blank" rel="noopener"}

> Those using the Mozilla Firefox web browser are left unable to access Microsoft.com domain. Tests by BleepingComputer confirm the issue relates to SSL certificate validation errors. Below we explain what can you do to remedy the issue. [...]
