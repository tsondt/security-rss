Title: Ransomware gang coughs up decryptor after realizing they hit the police
Date: 2021-12-29T14:01:07-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-29-ransomware-gang-coughs-up-decryptor-after-realizing-they-hit-the-police

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-coughs-up-decryptor-after-realizing-they-hit-the-police/){:target="_blank" rel="noopener"}

> The AvosLocker ransomware operation provided a free decryptor after learning they encrypted a US government agency. [...]
