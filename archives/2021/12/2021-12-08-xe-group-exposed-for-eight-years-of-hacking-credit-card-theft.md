Title: XE Group exposed for eight years of hacking, credit card theft
Date: 2021-12-08T10:12:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-08-xe-group-exposed-for-eight-years-of-hacking-credit-card-theft

[Source](https://www.bleepingcomputer.com/news/security/xe-group-exposed-for-eight-years-of-hacking-credit-card-theft/){:target="_blank" rel="noopener"}

> A relatively unknown group of Vietnamese hackers calling themselves 'XE Group' has been linked to eight years of for-profit hacking and credit card skimming. [...]
