Title: Popular WordPress platform Flywheel vulnerable to subdomain takeover, researcher claims
Date: 2021-12-23T16:33:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-23-popular-wordpress-platform-flywheel-vulnerable-to-subdomain-takeover-researcher-claims

[Source](https://portswigger.net/daily-swig/popular-wordpress-platform-flywheel-vulnerable-to-subdomain-takeover-researcher-claims){:target="_blank" rel="noopener"}

> Malicious actors could wreak havoc by impersonating legitimate websites [...]
