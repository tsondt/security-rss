Title: FTC implements tougher data protection rules to safeguard customer information
Date: 2021-12-03T12:47:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-03-ftc-implements-tougher-data-protection-rules-to-safeguard-customer-information

[Source](https://portswigger.net/daily-swig/ftc-implements-tougher-data-protection-rules-to-safeguard-customer-information){:target="_blank" rel="noopener"}

> New requirements for financial institutions include vulnerability assessments, employee training [...]
