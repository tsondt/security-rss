Title: Pro Wrestling Tees discloses data breach after credit cards stolen
Date: 2021-12-23T10:49:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-23-pro-wrestling-tees-discloses-data-breach-after-credit-cards-stolen

[Source](https://www.bleepingcomputer.com/news/security/pro-wrestling-tees-discloses-data-breach-after-credit-cards-stolen/){:target="_blank" rel="noopener"}

> Popular wrestling t-shirt site Pro Wrestling Tees has disclosed a data breach incident that has resulted in the compromise of the financial details of tens of thousands of its customers. [...]
