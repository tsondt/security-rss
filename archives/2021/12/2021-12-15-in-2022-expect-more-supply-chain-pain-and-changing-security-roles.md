Title: In 2022, Expect More Supply Chain Pain and Changing Security Roles
Date: 2021-12-15T00:27:24+00:00
Author: Sounil Yu
Category: Threatpost
Tags: InfoSec Insider
Slug: 2021-12-15-in-2022-expect-more-supply-chain-pain-and-changing-security-roles

[Source](https://threatpost.com/supply-chain-pain-and-changing-security-roles/177058/){:target="_blank" rel="noopener"}

> If 2021 was the Year of Supply Chain Pain, 2022 will be the Year of Supply Chain Chronic Pain (or something worse than pain). This past year, the pain was felt in two significant ways: through the supply chain disruptions caused by COVID-19, and through the many security breaches that we saw in our key [...]
