Title: Stealthy ‘WIRTE’ Gang Targets Middle Eastern Governments
Date: 2021-12-01T17:11:04+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Web Security
Slug: 2021-12-01-stealthy-wirte-gang-targets-middle-eastern-governments

[Source](https://threatpost.com/wirte-middle-eastern-governments/176688/){:target="_blank" rel="noopener"}

> Kaspersky researchers suspect that the cyberattackers may be a subgroup of the politically motivated, Palestine-focused Gaza Cybergang. [...]
