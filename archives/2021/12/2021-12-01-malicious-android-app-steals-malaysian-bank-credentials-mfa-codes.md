Title: Malicious Android app steals Malaysian bank credentials, MFA codes
Date: 2021-12-01T13:33:17-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-12-01-malicious-android-app-steals-malaysian-bank-credentials-mfa-codes

[Source](https://www.bleepingcomputer.com/news/security/malicious-android-app-steals-malaysian-bank-credentials-mfa-codes/){:target="_blank" rel="noopener"}

> A fake Android app is masquerading as a housekeeping service to steal online banking credentials from the customers of eight Malaysian banks. [...]
