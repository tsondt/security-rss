Title: Intel adds payout bonuses as it migrates bug bounty program to Intigriti
Date: 2021-12-13T16:28:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-13-intel-adds-payout-bonuses-as-it-migrates-bug-bounty-program-to-intigriti

[Source](https://portswigger.net/daily-swig/intel-adds-payout-bonuses-as-it-migrates-bug-bounty-program-to-intigriti){:target="_blank" rel="noopener"}

> Payout ceiling lifted from $100,000 to $150,000 for 12-month bonus period [...]
