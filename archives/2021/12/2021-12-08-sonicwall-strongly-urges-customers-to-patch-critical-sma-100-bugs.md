Title: SonicWall ‘strongly urges’ customers to patch critical SMA 100 bugs
Date: 2021-12-08T08:11:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-08-sonicwall-strongly-urges-customers-to-patch-critical-sma-100-bugs

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-strongly-urges-customers-to-patch-critical-sma-100-bugs/){:target="_blank" rel="noopener"}

> SonicWall 'strongly urges' organizations using SMA 100 series appliances to immediately patch them against multiple security flaws rated with CVSS scores ranging from medium to critical. [...]
