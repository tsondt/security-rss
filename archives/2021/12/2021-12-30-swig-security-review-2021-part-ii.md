Title: Swig Security Review 2021 – Part II
Date: 2021-12-30T15:32:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-30-swig-security-review-2021-part-ii

[Source](https://portswigger.net/daily-swig/swig-security-review-2021-part-ii){:target="_blank" rel="noopener"}

> Key thinkers on the biggest security stories and trends in 2021 [...]
