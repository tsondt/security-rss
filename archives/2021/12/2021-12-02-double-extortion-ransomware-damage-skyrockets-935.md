Title: ‘Double-Extortion’ Ransomware Damage Skyrockets 935%
Date: 2021-12-02T19:53:02+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Malware
Slug: 2021-12-02-double-extortion-ransomware-damage-skyrockets-935

[Source](https://threatpost.com/double-extortion-ransomware-data-leaks/176723/){:target="_blank" rel="noopener"}

> Startling triple-digit growth is fueled by easy criminal access to corporate networks and RaaS tools, an analysis found. [...]
