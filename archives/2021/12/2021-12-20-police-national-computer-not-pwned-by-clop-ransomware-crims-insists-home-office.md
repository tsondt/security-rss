Title: Police National Computer not pwned by Clop ransomware crims, insists Home Office
Date: 2021-12-20T15:51:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-20-police-national-computer-not-pwned-by-clop-ransomware-crims-insists-home-office

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/20/dacoll_ransomware_clop_pnc_claims/){:target="_blank" rel="noopener"}

> Scottish MSP Dacoll was hit, however The Clop ransomware gang pwned a managed service provider with access to the UK's Police National Computer, dumping data on its dark web leaks site – but officials deny that police data was compromised.... [...]
