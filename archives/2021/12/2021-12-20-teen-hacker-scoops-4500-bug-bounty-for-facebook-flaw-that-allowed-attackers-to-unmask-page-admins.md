Title: Teen hacker scoops $4,500 bug bounty for Facebook flaw that allowed attackers to unmask page admins
Date: 2021-12-20T12:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-20-teen-hacker-scoops-4500-bug-bounty-for-facebook-flaw-that-allowed-attackers-to-unmask-page-admins

[Source](https://portswigger.net/daily-swig/teen-hacker-scoops-4-500-bug-bounty-for-facebook-flaw-that-allowed-attackers-to-unmask-page-admins){:target="_blank" rel="noopener"}

> High-impact privacy bug in Facebook’s Android app now fixed [...]
