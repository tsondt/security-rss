Title: Log4j 2.17.1 out now, fixes new remote code execution bug
Date: 2021-12-28T15:12:01-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-12-28-log4j-2171-out-now-fixes-new-remote-code-execution-bug

[Source](https://www.bleepingcomputer.com/news/security/log4j-2171-out-now-fixes-new-remote-code-execution-bug/){:target="_blank" rel="noopener"}

> Apache has released another Log4j version, 2.17.1 fixing a newly discovered remote code execution (RCE) vulnerability in 2.17.0, tracked as CVE-2021-44832. Prior to today, 2.17.0 was the most recent version of Log4j and deemed the safest release to upgrade to, but that advice has now evolved. [...]
