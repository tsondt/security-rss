Title: Facebook disrupts operations of seven surveillance-for-hire firms
Date: 2021-12-16T15:52:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-16-facebook-disrupts-operations-of-seven-surveillance-for-hire-firms

[Source](https://www.bleepingcomputer.com/news/security/facebook-disrupts-operations-of-seven-surveillance-for-hire-firms/){:target="_blank" rel="noopener"}

> Facebook has disrupted the operations of seven different spyware-making companies, blocking their Internet infrastructure, sending cease and desist letters, and banning them from its platform. [...]
