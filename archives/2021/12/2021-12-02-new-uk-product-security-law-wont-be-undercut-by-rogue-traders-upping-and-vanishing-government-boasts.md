Title: New UK product security law won't be undercut by rogue traders upping and vanishing, government boasts
Date: 2021-12-02T09:15:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-02-new-uk-product-security-law-wont-be-undercut-by-rogue-traders-upping-and-vanishing-government-boasts

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/02/psti_bill_phoenixing_dcms_response/){:target="_blank" rel="noopener"}

> El Reg asks about phoenixing – but will answer convince world+dog? Britain's plans to force internet-connected device vendors to declare legally binding product lifespans won't be easily evaded by shell companies, the government has told The Register.... [...]
