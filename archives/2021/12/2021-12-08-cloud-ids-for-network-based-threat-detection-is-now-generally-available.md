Title: Cloud IDS for network-based threat detection is now generally available
Date: 2021-12-08T16:55:00+00:00
Author: Gregory M. Lebovitz
Category: GCP Security
Tags: Networking;Google Cloud;Identity & Security
Slug: 2021-12-08-cloud-ids-for-network-based-threat-detection-is-now-generally-available

[Source](https://cloud.google.com/blog/products/identity-security/announcing-general-availability-of-google-cloud-ids/){:target="_blank" rel="noopener"}

> As more and more applications move to the cloud, cloud network security teams have to keep them secure against an ever-evolving threat landscape. Shielding applications against network threats is also one of the most important criteria for regulatory compliance. For example, effective intrusion detection is a requirement of the Payment Card Industry Data Security Standard - PCI DSS 3.2.1. To address these challenges, many cloud network security teams build their own complex network threat detection solutions based on open source or third-party IDS components. These bespoke solutions can be difficult and costly to operate, and they often lack the scalability [...]
