Title: How to customize behavior of AWS Managed Rules for AWS WAF
Date: 2021-12-10T17:50:12+00:00
Author: Madhu Kondur
Category: AWS Security
Tags: Announcements;AWS WAF;Best Practices;Security, Identity, & Compliance;Technical How-to;AWS Managed Rules;AWS Threat Research Team
Slug: 2021-12-10-how-to-customize-behavior-of-aws-managed-rules-for-aws-waf

[Source](https://aws.amazon.com/blogs/security/how-to-customize-behavior-of-aws-managed-rules-for-aws-waf/){:target="_blank" rel="noopener"}

> AWS Managed Rules for AWS WAF provides a group of rules created by AWS that can be used help protect you against common application vulnerabilities and other unwanted access to your systems without having to write your own rules. AWS Threat Research Team updates AWS Managed Rules to respond to an ever-changing threat landscape in order [...]
