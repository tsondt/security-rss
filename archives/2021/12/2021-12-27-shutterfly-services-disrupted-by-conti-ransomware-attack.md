Title: Shutterfly services disrupted by Conti ransomware attack
Date: 2021-12-27T02:56:34-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-27-shutterfly-services-disrupted-by-conti-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/shutterfly-services-disrupted-by-conti-ransomware-attack/){:target="_blank" rel="noopener"}

> Photography and personalized photo giant Shutterfly has suffered a Conti ransomware attack that allegedly encrypted thousands of devices and stole corporate data. [...]
