Title: HCL DX vendor ‘could not reproduce’ allegedly critical vulnerabilities
Date: 2021-12-30T16:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-30-hcl-dx-vendor-could-not-reproduce-allegedly-critical-vulnerabilities

[Source](https://portswigger.net/daily-swig/hcl-dx-vendor-could-not-reproduce-allegedly-critical-vulnerabilities){:target="_blank" rel="noopener"}

> Disclosure process for bugs in HCL DX – formerly WebSphere Portal – seemingly went awry [...]
