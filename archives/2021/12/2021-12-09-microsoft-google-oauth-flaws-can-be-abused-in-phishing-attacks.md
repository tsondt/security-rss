Title: Microsoft, Google OAuth flaws can be abused in phishing attacks
Date: 2021-12-09T11:21:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-09-microsoft-google-oauth-flaws-can-be-abused-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-google-oauth-flaws-can-be-abused-in-phishing-attacks/){:target="_blank" rel="noopener"}

> Researchers have discovered a set of previously unknown methods to launch URL redirection attacks against weak OAuth 2.0 implementations. [...]
