Title: Japan draws a LINE: web giants must reveal where they store user data
Date: 2021-12-16T06:46:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-12-16-japan-draws-a-line-web-giants-must-reveal-where-they-store-user-data

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/16/japan_data_location_requirement/){:target="_blank" rel="noopener"}

> Looks a lot like a response to messaging services passing data through China Social media and search engine operators in Japan will be required to specify the countries in which users' data is physically stored, under a planned tweak to local laws.... [...]
