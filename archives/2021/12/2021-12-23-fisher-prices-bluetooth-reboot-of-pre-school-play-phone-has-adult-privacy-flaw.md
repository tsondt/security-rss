Title: Fisher Price's Bluetooth reboot of pre-school play phone has adult privacy flaw
Date: 2021-12-23T08:02:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-23-fisher-prices-bluetooth-reboot-of-pre-school-play-phone-has-adult-privacy-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/23/fisher_prices_bluetooth_reboot_of/){:target="_blank" rel="noopener"}

> ‘Chatter’ can be bugged thanks to kindergarten-grade security A Bluetooth phone designed to evoke the carefree days of early childhood has been found to instead threaten the very adult prospect of being surveilled in your home.... [...]
