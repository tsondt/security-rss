Title: Apache takes off, nukes insecure feature at the heart of Log4j from orbit with v2.16
Date: 2021-12-14T23:30:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-14-apache-takes-off-nukes-insecure-feature-at-the-heart-of-log4j-from-orbit-with-v216

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/14/apache_log4j_v2_16_jndi_disabled_default/){:target="_blank" rel="noopener"}

> Now open-source logging library's JNDI disabled entirely by default, message lookups removed Last week, version 2.15 of the widely used open-source logging library Log4j was released to tackle a critical security hole, dubbed Log4Shell, which could be trivially abused by miscreants to hijack servers and apps over the internet.... [...]
