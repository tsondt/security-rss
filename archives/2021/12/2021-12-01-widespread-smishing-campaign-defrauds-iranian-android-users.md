Title: Widespread ‘Smishing’ Campaign Defrauds Iranian Android Users
Date: 2021-12-01T12:15:28+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2021-12-01-widespread-smishing-campaign-defrauds-iranian-android-users

[Source](https://threatpost.com/smishing-campaign-iranian-android-users/176679/){:target="_blank" rel="noopener"}

> Attackers use socially engineered SMS messages and malware to compromise tens of thousands of devices and drain user bank accounts. [...]
