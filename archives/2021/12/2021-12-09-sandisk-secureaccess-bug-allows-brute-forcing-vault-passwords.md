Title: SanDisk SecureAccess bug allows brute forcing vault passwords
Date: 2021-12-09T08:40:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-09-sandisk-secureaccess-bug-allows-brute-forcing-vault-passwords

[Source](https://www.bleepingcomputer.com/news/security/sandisk-secureaccess-bug-allows-brute-forcing-vault-passwords/){:target="_blank" rel="noopener"}

> Western Digital has fixed a security vulnerability that enabled attackers to brute force SanDisk SecureAccess passwords and access the users' protected files. [...]
