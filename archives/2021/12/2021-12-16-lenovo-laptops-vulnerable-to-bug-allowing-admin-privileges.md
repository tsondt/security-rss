Title: Lenovo laptops vulnerable to bug allowing admin privileges
Date: 2021-12-16T10:56:29-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-16-lenovo-laptops-vulnerable-to-bug-allowing-admin-privileges

[Source](https://www.bleepingcomputer.com/news/security/lenovo-laptops-vulnerable-to-bug-allowing-admin-privileges/){:target="_blank" rel="noopener"}

> Lenovo laptops, including ThinkPad and Yoga models, are vulnerable to a privilege elevation bug in the ImControllerService service allowing attackers to execute commands with admin privileges. [...]
