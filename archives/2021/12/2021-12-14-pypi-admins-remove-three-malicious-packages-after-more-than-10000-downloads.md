Title: <span>PyPI admins remove three malicious packages after more than 10,000 downloads</span>
Date: 2021-12-14T16:09:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-14-pypi-admins-remove-three-malicious-packages-after-more-than-10000-downloads

[Source](https://portswigger.net/daily-swig/pypi-admins-remove-three-malicious-packages-after-more-than-10-000-downloads){:target="_blank" rel="noopener"}

> Two packages lay undiscovered for 10 months [...]
