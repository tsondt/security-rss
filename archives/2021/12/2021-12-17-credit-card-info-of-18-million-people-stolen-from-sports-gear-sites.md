Title: Credit card info of 1.8 million people stolen from sports gear sites
Date: 2021-12-17T14:06:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-17-credit-card-info-of-18-million-people-stolen-from-sports-gear-sites

[Source](https://www.bleepingcomputer.com/news/security/credit-card-info-of-18-million-people-stolen-from-sports-gear-sites/){:target="_blank" rel="noopener"}

> Four affiliated online sports gear sites have disclosed a cyberattack where threat actors stole credit cards for 1,813,224 customers. [...]
