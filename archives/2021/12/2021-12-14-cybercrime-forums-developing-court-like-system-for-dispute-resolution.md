Title: Cybercrime forums developing court-like system for dispute resolution
Date: 2021-12-14T11:53:44+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-14-cybercrime-forums-developing-court-like-system-for-dispute-resolution

[Source](https://portswigger.net/daily-swig/cybercrime-forums-developing-court-like-system-for-dispute-resolution){:target="_blank" rel="noopener"}

> Too hot-to-handle ransomware excluded from scheme [...]
