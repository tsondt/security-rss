Title: Ukraine hosts large-scale simulation of cyber-attack against energy grid
Date: 2021-12-17T15:57:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-17-ukraine-hosts-large-scale-simulation-of-cyber-attack-against-energy-grid

[Source](https://portswigger.net/daily-swig/ukraine-hosts-large-scale-simulation-of-cyber-attack-against-energy-grid){:target="_blank" rel="noopener"}

> SANS Institute’s latest Grid NetWars competition involved 250 security pros from Ukraine [...]
