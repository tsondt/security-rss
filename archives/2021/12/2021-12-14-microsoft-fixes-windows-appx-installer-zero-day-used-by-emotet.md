Title: Microsoft fixes Windows AppX Installer zero-day used by Emotet
Date: 2021-12-14T14:09:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-12-14-microsoft-fixes-windows-appx-installer-zero-day-used-by-emotet

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-windows-appx-installer-zero-day-used-by-emotet/){:target="_blank" rel="noopener"}

> Microsoft has patched a high severity Windows zero-day vulnerability exploited in the wild to deliver Emotet malware payloads. [...]
