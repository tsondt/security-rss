Title: CISA releases Apache Log4j scanner to find vulnerable apps
Date: 2021-12-22T10:23:40-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-22-cisa-releases-apache-log4j-scanner-to-find-vulnerable-apps

[Source](https://www.bleepingcomputer.com/news/security/cisa-releases-apache-log4j-scanner-to-find-vulnerable-apps/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has announced the release of a scanner for identifying web services impacted by& two Apache Log4j remote code execution vulnerabilities, tracked as CVE-2021-44228 and CVE-2021-45046. [...]
