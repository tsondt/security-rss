Title: Windows 10 21H2 adds ransomware protection to security baseline
Date: 2021-12-21T08:06:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-12-21-windows-10-21h2-adds-ransomware-protection-to-security-baseline

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-10-21h2-adds-ransomware-protection-to-security-baseline/){:target="_blank" rel="noopener"}

> Microsoft has released the final version of security configuration baseline settings for Windows 10, version 21H2, available today from the Microsoft Security Compliance Toolkit. [...]
