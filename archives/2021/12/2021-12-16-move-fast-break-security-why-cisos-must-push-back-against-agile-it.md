Title: Move fast, break security: Why CISOs must push back against Agile IT
Date: 2021-12-16T08:30:06+00:00
Author: The Masked CISO
Category: The Register
Tags: 
Slug: 2021-12-16-move-fast-break-security-why-cisos-must-push-back-against-agile-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/16/move_fast_break_security_why/){:target="_blank" rel="noopener"}

> The Vectra Masked CISO series gives security leaders a place to expose the biggest issues in security and advise peers on how to overcome them Advertorial The Vectra Masked CISO series gives security leaders a place to expose the biggest issues in security and advise peers on how to overcome them.... [...]
