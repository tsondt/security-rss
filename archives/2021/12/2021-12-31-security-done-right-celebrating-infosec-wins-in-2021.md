Title: Security done right: Celebrating infosec wins in 2021
Date: 2021-12-31T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-31-security-done-right-celebrating-infosec-wins-in-2021

[Source](https://portswigger.net/daily-swig/security-done-right-celebrating-infosec-wins-in-2021){:target="_blank" rel="noopener"}

> Kudos to Tonga’s ccTLD, the US Supreme Court, and others... [...]
