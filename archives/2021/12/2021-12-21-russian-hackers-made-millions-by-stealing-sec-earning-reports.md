Title: Russian hackers made millions by stealing SEC earning reports
Date: 2021-12-21T12:18:07-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-12-21-russian-hackers-made-millions-by-stealing-sec-earning-reports

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-made-millions-by-stealing-sec-earning-reports/){:target="_blank" rel="noopener"}

> A Russian national working for a cybersecurity company has been extradited to the U.S. where he is being charged for hacking into computer networks of two U.S.-based filing agents used by multiple companies to file quarterly and annual earnings through the Securities and Exchange Commissions (SEC) system. [...]
