Title: The Week in Ransomware - December 17th 2021 - Enter Log4j
Date: 2021-12-17T18:37:23-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-17-the-week-in-ransomware-december-17th-2021-enter-log4j

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-17th-2021-enter-log4j/){:target="_blank" rel="noopener"}

> A critical Apache Log4j vulnerability took the world by storm this week, and now it is being used by threat actors as part of their ransomware attacks. [...]
