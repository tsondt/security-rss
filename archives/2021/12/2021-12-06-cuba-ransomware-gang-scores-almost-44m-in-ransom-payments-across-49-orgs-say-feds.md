Title: Cuba ransomware gang scores almost $44m in ransom payments across 49 orgs, say Feds
Date: 2021-12-06T13:02:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-12-06-cuba-ransomware-gang-scores-almost-44m-in-ransom-payments-across-49-orgs-say-feds

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/06/cuba_ransomware_gang_scores_almost/){:target="_blank" rel="noopener"}

> Hancitor is at play The US Federal Bureau of Investigation (FBI) says 49 organisations, including some in government, were hit by Cuba ransomware as of early November this year.... [...]
