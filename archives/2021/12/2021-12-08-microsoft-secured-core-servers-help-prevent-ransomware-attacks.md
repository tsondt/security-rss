Title: Microsoft: Secured-core servers help prevent ransomware attacks
Date: 2021-12-08T14:25:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-12-08-microsoft-secured-core-servers-help-prevent-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-secured-core-servers-help-prevent-ransomware-attacks/){:target="_blank" rel="noopener"}

> Microsoft says the first Secured-core certified Windows Server and Microsoft Azure Stack HCI devices are now available to protect customers' networks from security threats, including ransomware attacks. [...]
