Title: Bugs in billions of WiFi, Bluetooth chips allow password, data theft
Date: 2021-12-13T11:04:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-12-13-bugs-in-billions-of-wifi-bluetooth-chips-allow-password-data-theft

[Source](https://www.bleepingcomputer.com/news/security/bugs-in-billions-of-wifi-bluetooth-chips-allow-password-data-theft/){:target="_blank" rel="noopener"}

> Researchers at the University of Darmstadt, Brescia, CNIT, and the Secure Mobile Networking Lab, have published a paper that proves it's possible to extract passwords and manipulate traffic on a WiFi chip by targeting a device's Bluetooth component. [...]
