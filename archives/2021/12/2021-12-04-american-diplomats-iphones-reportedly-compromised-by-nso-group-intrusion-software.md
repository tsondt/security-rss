Title: American diplomats' iPhones reportedly compromised by NSO Group intrusion software
Date: 2021-12-04T01:54:34+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-12-04-american-diplomats-iphones-reportedly-compromised-by-nso-group-intrusion-software

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/04/american_diplomats_nso/){:target="_blank" rel="noopener"}

> Reuters claims nine State Department employees outside the US had their devices hacked The Apple iPhones of at least nine US State Department officials were compromised by an unidentified entity using NSO Group's Pegasus spyware, according to a report published Friday by Reuters.... [...]
