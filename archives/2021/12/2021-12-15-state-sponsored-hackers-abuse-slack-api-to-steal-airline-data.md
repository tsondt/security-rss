Title: State-sponsored hackers abuse Slack API to steal airline data
Date: 2021-12-15T12:32:55-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-15-state-sponsored-hackers-abuse-slack-api-to-steal-airline-data

[Source](https://www.bleepingcomputer.com/news/security/state-sponsored-hackers-abuse-slack-api-to-steal-airline-data/){:target="_blank" rel="noopener"}

> A suspected Iranian state-supported threat actor is deploying a newly discovered backdoor named 'Aclip' that abuses the Slack API for covert communications. [...]
