Title: Microsoft extends Secured-core concept to servers
Date: 2021-12-08T05:15:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-08-microsoft-extends-secured-core-concept-to-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/08/microsoft_extends_securedcore_concept_to_servers/){:target="_blank" rel="noopener"}

> Certifies hardware with malware-crimping spec, already common in PCs, for Azure Stack and Windows Server Microsoft has extended the Secured-core concept it applied to PCs in 2019 to servers, and to Windows Server and Azure Stack HCI.... [...]
