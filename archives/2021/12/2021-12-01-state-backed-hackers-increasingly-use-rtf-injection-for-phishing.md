Title: State-backed hackers increasingly use RTF injection for phishing
Date: 2021-12-01T05:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-01-state-backed-hackers-increasingly-use-rtf-injection-for-phishing

[Source](https://www.bleepingcomputer.com/news/security/state-backed-hackers-increasingly-use-rtf-injection-for-phishing/){:target="_blank" rel="noopener"}

> Three APT hacking groups from India, Russia, and China, were observed using a novel RTF (rich text format) template injection technique in their recent phishing campaigns. [...]
