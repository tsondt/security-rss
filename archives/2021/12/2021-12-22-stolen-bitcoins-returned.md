Title: Stolen Bitcoins Returned
Date: 2021-12-22T16:20:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bitcoin;cryptocurrency;cybercrime;FBI;law enforcement;theft
Slug: 2021-12-22-stolen-bitcoins-returned

[Source](https://www.schneier.com/blog/archives/2021/12/stolen-bitcoins-returned.html){:target="_blank" rel="noopener"}

> The US has returned $154 million in bitcoins stolen by a Sony employee. However, on December 1, following an investigation in collaboration with Japanese law enforcement authorities, the FBI seized the 3879.16242937 BTC in Ishii’s wallet after obtaining the private key, which made it possible to transfer all the bitcoins to the FBI’s bitcoin wallet. [...]
