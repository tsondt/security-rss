Title: Apache Kafka Cloud Clusters Expose Sensitive Data for Large Companies
Date: 2021-12-06T16:14:54+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Web Security
Slug: 2021-12-06-apache-kafka-cloud-clusters-expose-sensitive-data-for-large-companies

[Source](https://threatpost.com/apache-kafka-cloud-clusters-expose-data/176778/){:target="_blank" rel="noopener"}

> The culprit is misconfigured Kafdrop interfaces, used for centralized management of the open-source platform. [...]
