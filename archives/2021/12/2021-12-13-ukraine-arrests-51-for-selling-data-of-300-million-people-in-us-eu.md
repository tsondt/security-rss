Title: Ukraine arrests 51 for selling data of 300 million people in US, EU
Date: 2021-12-13T09:09:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-13-ukraine-arrests-51-for-selling-data-of-300-million-people-in-us-eu

[Source](https://www.bleepingcomputer.com/news/security/ukraine-arrests-51-for-selling-data-of-300-million-people-in-us-eu/){:target="_blank" rel="noopener"}

> Ukrainian law enforcement arrested 51 suspects believed to have been selling stolen personal data on hacking forums belonging to hundreds of millions worldwide, including Ukraine, the US, and Europe. [...]
