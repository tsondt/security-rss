Title: Pip-audit: Google-backed tool probes Python environments for vulnerable packages
Date: 2021-12-03T15:36:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-03-pip-audit-google-backed-tool-probes-python-environments-for-vulnerable-packages

[Source](https://portswigger.net/daily-swig/pip-audit-google-backed-tool-probes-python-environments-for-vulnerable-packages){:target="_blank" rel="noopener"}

> ‘Good initial results’, says one early adopter [...]
