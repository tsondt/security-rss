Title: Cuba Ransomware Gang Hauls in $44M in Payouts
Date: 2021-12-06T18:29:59+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-12-06-cuba-ransomware-gang-hauls-in-44m-in-payouts

[Source](https://threatpost.com/cuba-ransomware-gang-44m-payouts/176790/){:target="_blank" rel="noopener"}

> The gang is using a variety of tools and malware to carry out attacks in volume on critical sectors, the FBI warned. [...]
