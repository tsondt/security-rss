Title: Backdoor gives hackers complete control over federal agency network
Date: 2021-12-16T20:15:00+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;backdoors;malware;US government
Slug: 2021-12-16-backdoor-gives-hackers-complete-control-over-federal-agency-network

[Source](https://arstechnica.com/?p=1821223){:target="_blank" rel="noopener"}

> Enlarge (credit: Jeremy Brooks / Flickr ) A US federal agency has been hosting a backdoor that can provide total visibility into and complete control over the agency network, and the researchers who discovered it have been unable to engage with the administrators responsible, security firm Avast said on Thursday. The US Commission on International Religious Freedom, associated with international rights, regularly communicates with other US agencies and international governmental and nongovernmental organizations. The security firm published a blog post after multiple attempts failed to report the findings directly and through channels the US government has in place. The post [...]
