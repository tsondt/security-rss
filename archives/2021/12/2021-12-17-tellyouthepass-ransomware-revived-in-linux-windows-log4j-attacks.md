Title: TellYouThePass ransomware revived in Linux, Windows Log4j attacks
Date: 2021-12-17T15:25:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux
Slug: 2021-12-17-tellyouthepass-ransomware-revived-in-linux-windows-log4j-attacks

[Source](https://www.bleepingcomputer.com/news/security/tellyouthepass-ransomware-revived-in-linux-windows-log4j-attacks/){:target="_blank" rel="noopener"}

> Threat actors have revived an old and relatively inactive ransomware family known as TellYouThePass, deploying it in attacks against Windows and Linux devices targeting a critical remote code execution bug in the Apache Log4j library. [...]
