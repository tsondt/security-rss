Title: Policy Troubleshooter for BeyondCorp Enterprise is now GA!
Date: 2021-12-14T17:00:00+00:00
Author: Tanisha Rai
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-12-14-policy-troubleshooter-for-beyondcorp-enterprise-is-now-ga

[Source](https://cloud.google.com/blog/products/identity-security/unblock-bce-users-for-easy-zero-trust-access/){:target="_blank" rel="noopener"}

> Having the ability to access corporate resources and information remotely and securely has been crucial for countless organizations during the course of the COVID-19 pandemic. Yet, many employees may agree that this process is not always seamless, especially if they were blocked from getting to an app or a resource they should be able to access. Adding to this frustration is the challenge of getting in touch with IT support to figure out what was happening and why, which can be even more difficult in a remote environment. Our aim with BeyondCorp Enterprise, Google Cloud’s zero trust access solution, is [...]
