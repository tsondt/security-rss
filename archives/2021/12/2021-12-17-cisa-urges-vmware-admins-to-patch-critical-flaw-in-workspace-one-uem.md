Title: CISA urges VMware admins to patch critical flaw in Workspace ONE UEM
Date: 2021-12-17T13:32:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-17-cisa-urges-vmware-admins-to-patch-critical-flaw-in-workspace-one-uem

[Source](https://www.bleepingcomputer.com/news/security/cisa-urges-vmware-admins-to-patch-critical-flaw-in-workspace-one-uem/){:target="_blank" rel="noopener"}

> CISA has asked VMware admins and users today to patch a critical security vulnerability found in the Workspace ONE UEM console that threat actors could abuse to gain access to sensitive information. [...]
