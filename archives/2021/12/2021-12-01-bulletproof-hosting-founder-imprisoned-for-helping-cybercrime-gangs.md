Title: Bulletproof hosting founder imprisoned for helping cybercrime gangs
Date: 2021-12-01T16:23:56-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-01-bulletproof-hosting-founder-imprisoned-for-helping-cybercrime-gangs

[Source](https://www.bleepingcomputer.com/news/security/bulletproof-hosting-founder-imprisoned-for-helping-cybercrime-gangs/){:target="_blank" rel="noopener"}

> 34-year-old Russian Aleksandr Grichishkin, the founder of a bulletproof hosting service, was sentenced to 60 months in prison for allowing cybercrime gangs to use the platform in attacks targeting US financial institutions between 2008 to 2015. [...]
