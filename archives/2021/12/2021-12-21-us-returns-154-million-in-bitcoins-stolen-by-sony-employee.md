Title: US returns $154 Million in bitcoins stolen by Sony employee
Date: 2021-12-21T12:03:15-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-12-21-us-returns-154-million-in-bitcoins-stolen-by-sony-employee

[Source](https://www.bleepingcomputer.com/news/security/us-returns-154-million-in-bitcoins-stolen-by-sony-employee/){:target="_blank" rel="noopener"}

> The United States has taken legal action to seize and return over $154 million purportedly stolen from Sony Life Insurance Company Ltd, a SONY subsidiary, by an employee in a textbook business email compromise (BEC) attack. [...]
