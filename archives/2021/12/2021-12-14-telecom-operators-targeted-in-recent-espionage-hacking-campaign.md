Title: Telecom operators targeted in recent espionage hacking campaign
Date: 2021-12-14T18:32:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-14-telecom-operators-targeted-in-recent-espionage-hacking-campaign

[Source](https://www.bleepingcomputer.com/news/security/telecom-operators-targeted-in-recent-espionage-hacking-campaign/){:target="_blank" rel="noopener"}

> Researchers have spotted a new espionage campaign targeting telecommunication and IT service providers in the Middle East and Asia. [...]
