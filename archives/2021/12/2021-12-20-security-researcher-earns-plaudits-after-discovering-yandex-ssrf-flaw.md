Title: Security researcher earns plaudits after discovering Yandex SSRF flaw
Date: 2021-12-20T15:39:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-20-security-researcher-earns-plaudits-after-discovering-yandex-ssrf-flaw

[Source](https://portswigger.net/daily-swig/security-researcher-earns-plaudits-after-discovering-yandex-ssrf-flaw){:target="_blank" rel="noopener"}

> Russian language search engine has secured its backend infrastructure [...]
