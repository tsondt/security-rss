Title: Insider threat: Tech firm was hacked and extorted by its own employee, says FBI
Date: 2021-12-02T14:15:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-02-insider-threat-tech-firm-was-hacked-and-extorted-by-its-own-employee-says-fbi

[Source](https://portswigger.net/daily-swig/insider-threat-tech-firm-was-hacked-and-extorted-by-its-own-employee-says-fbi){:target="_blank" rel="noopener"}

> Senior developer also accused of posing as anonymous whistleblower [...]
