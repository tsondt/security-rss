Title: New German Government is Pro-Encryption and Anti-Backdoors
Date: 2021-12-08T19:19:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;encryption;Germany;national security policy;privacy
Slug: 2021-12-08-new-german-government-is-pro-encryption-and-anti-backdoors

[Source](https://www.schneier.com/blog/archives/2021/12/new-german-government-is-pro-encryption-and-anti-backdoors.html){:target="_blank" rel="noopener"}

> I hope this is true: According to Jens Zimmermann, the German coalition negotiations had made it “quite clear” that the incoming government of the Social Democrats (SPD), the Greens and the business-friendly liberal FDP would reject “the weakening of encryption, which is being attempted under the guise of the fight against child abuse” by the coalition partners. Such regulations, which are already enshrined in the interim solution of the ePrivacy Regulation, for example, “diametrically contradict the character of the coalition agreement” because secure end-to-end encryption is guaranteed there, Zimmermann said. Introducing backdoors would undermine this goal of the coalition agreement, [...]
