Title: US distrust of Huawei linked in part to malicious software update in 2012
Date: 2021-12-18T11:01:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-12-18-us-distrust-of-huawei-linked-in-part-to-malicious-software-update-in-2012

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/18/us_huawei_malware/){:target="_blank" rel="noopener"}

> Report claims Huawei techs working for Chinese intelligence compromised Australian telco Suspicions about the integrity of Huawei products among US government officials can be attributed in part to a 2012 incident involving a Huawei software update that compromised the network of a major Australian telecom company with malicious code, according to a report published by Bloomberg.... [...]
