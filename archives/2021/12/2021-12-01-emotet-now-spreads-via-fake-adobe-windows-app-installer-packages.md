Title: Emotet now spreads via fake Adobe Windows App Installer packages
Date: 2021-12-01T18:43:10-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-12-01-emotet-now-spreads-via-fake-adobe-windows-app-installer-packages

[Source](https://www.bleepingcomputer.com/news/security/emotet-now-spreads-via-fake-adobe-windows-app-installer-packages/){:target="_blank" rel="noopener"}

> The notorious Emotet malware is now distributed through malicious Windows App Installer packages that pretend to be Adobe PDF software. [...]
