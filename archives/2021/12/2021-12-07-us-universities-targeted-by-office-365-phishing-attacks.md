Title: US universities targeted by Office 365 phishing attacks
Date: 2021-12-07T15:23:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-07-us-universities-targeted-by-office-365-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-universities-targeted-by-office-365-phishing-attacks/){:target="_blank" rel="noopener"}

> US universities are being targeted in multiple phishing attacks designed to impersonate college login portals to steal valuable Office 365 credentials. [...]
