Title: 2easy now a significant dark web marketplace for stolen data
Date: 2021-12-21T15:02:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-21-2easy-now-a-significant-dark-web-marketplace-for-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/2easy-now-a-significant-dark-web-marketplace-for-stolen-data/){:target="_blank" rel="noopener"}

> A dark web marketplace named '2easy' is becoming a significant player in the sale of stolen data "Logs" harvested from roughly 600,000 devices infected with information-stealing malware. [...]
