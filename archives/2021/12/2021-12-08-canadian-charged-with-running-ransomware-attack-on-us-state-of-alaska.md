Title: Canadian charged with running ransomware attack on US state of Alaska
Date: 2021-12-08T19:02:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-08-canadian-charged-with-running-ransomware-attack-on-us-state-of-alaska

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/08/canadian_man_ransomware_alaska_charged/){:target="_blank" rel="noopener"}

> Cross-border op nabbed our man, boast cops and prosecutors A Canadian man is accused of masterminding ransomware attacks that caused "damage" to systems belonging to the US state of Alaska.... [...]
