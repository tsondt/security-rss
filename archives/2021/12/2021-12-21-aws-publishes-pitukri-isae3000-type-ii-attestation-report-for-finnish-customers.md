Title: AWS publishes PiTuKri ISAE3000 Type II Attestation Report for Finnish customers
Date: 2021-12-21T19:50:02+00:00
Author: Niyaz Noor
Category: AWS Security
Tags: Announcements;Foundational (100);AWS Compliance;AWS PiTuKri;Finland;Finnish Public Sector;PiTuKri Compliance;Security Blog
Slug: 2021-12-21-aws-publishes-pitukri-isae3000-type-ii-attestation-report-for-finnish-customers

[Source](https://aws.amazon.com/blogs/security/aws-publishes-pitukri-isae3000-type-ii-attestation-report-for-finnish-customers/){:target="_blank" rel="noopener"}

> Gaining and maintaining customer trust is an ongoing commitment at Amazon Web Services (AWS). Our customers’ industry security requirements drive the scope and portfolio of compliance reports, attestations, and certifications we pursue. AWS is pleased to announce the issuance of the Criteria to Assess the Information Security of Cloud Services (PiTuKri) ISAE 3000 Type 2 [...]
