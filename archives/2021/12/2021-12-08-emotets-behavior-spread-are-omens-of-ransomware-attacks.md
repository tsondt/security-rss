Title: Emotet’s Behavior & Spread Are Omens of Ransomware Attacks
Date: 2021-12-08T14:47:59+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-12-08-emotets-behavior-spread-are-omens-of-ransomware-attacks

[Source](https://threatpost.com/emotets-behavior-spread-are-omens-of-ransomware-attacks/176845/){:target="_blank" rel="noopener"}

> The botnet, which resurfaced last month on the back of TrickBot, can now directly install Cobalt Strike on infected devices, giving threat actors direct access to targets. [...]
