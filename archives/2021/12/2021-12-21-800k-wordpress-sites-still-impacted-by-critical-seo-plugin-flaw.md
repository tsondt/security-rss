Title: 800K WordPress sites still impacted by critical SEO plugin flaw
Date: 2021-12-21T14:25:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-21-800k-wordpress-sites-still-impacted-by-critical-seo-plugin-flaw

[Source](https://www.bleepingcomputer.com/news/security/800k-wordpress-sites-still-impacted-by-critical-seo-plugin-flaw/){:target="_blank" rel="noopener"}

> Two critical and high severity security vulnerabilities in the highly popular "All in One" SEO WordPress plugin exposed over 3 million websites to takeover attacks. [...]
