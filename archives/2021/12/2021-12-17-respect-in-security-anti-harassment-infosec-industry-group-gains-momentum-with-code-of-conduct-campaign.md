Title: Respect in Security: Anti-harassment infosec industry group gains momentum with code of conduct campaign
Date: 2021-12-17T13:36:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-17-respect-in-security-anti-harassment-infosec-industry-group-gains-momentum-with-code-of-conduct-campaign

[Source](https://portswigger.net/daily-swig/respect-in-security-anti-harassment-infosec-industry-group-gains-momentum-with-code-of-conduct-campaign){:target="_blank" rel="noopener"}

> Take the pledge, companies are urged [...]
