Title: Kronos Ransomware Outage Drives Widespread Payroll Chaos
Date: 2021-12-13T23:17:45+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Critical Infrastructure;Government;Hacks;Malware
Slug: 2021-12-13-kronos-ransomware-outage-drives-widespread-payroll-chaos

[Source](https://threatpost.com/kronos-ransomware-outage-payroll-chaos/176984/){:target="_blank" rel="noopener"}

> Kronos, the workforce-management provider, said a weeks-long outage of its cloud services is in the offing, just in time to hamstring end-of-year HR activities like bonuses and vacation tracking. [...]
