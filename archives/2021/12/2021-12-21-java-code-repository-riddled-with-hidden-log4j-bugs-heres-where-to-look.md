Title: Java Code Repository Riddled with Hidden Log4j Bugs; Here’s Where to Look
Date: 2021-12-21T20:46:35+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-12-21-java-code-repository-riddled-with-hidden-log4j-bugs-heres-where-to-look

[Source](https://threatpost.com/java-supply-chain-log4j-bug/177211/){:target="_blank" rel="noopener"}

> There are 17,000npatched Log4j packages in the Maven Central ecosystem, leaving massive supply-chain risk on the table from Log4Shell exploits. [...]
