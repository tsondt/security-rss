Title: Robocalls More Than Doubled in 2021, Cost Victims $30B
Date: 2021-12-20T19:48:25+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Mobile Security
Slug: 2021-12-20-robocalls-more-than-doubled-in-2021-cost-victims-30b

[Source](https://threatpost.com/robocalls-doubled-2021-30b/177165/){:target="_blank" rel="noopener"}

> T-Mobile reported blocking 21 billion scam calls during a record-smashing year for robocalls. [...]
