Title: The secret Uganda deal that has brought NSO to the brink of collapse
Date: 2021-12-21T14:51:19+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;Features;Policy;0day;black hat;hacking;Israel;nso group
Slug: 2021-12-21-the-secret-uganda-deal-that-has-brought-nso-to-the-brink-of-collapse

[Source](https://arstechnica.com/?p=1822047){:target="_blank" rel="noopener"}

> Enlarge / A man walks by the building entrance of Israeli cyber company NSO Group at one of its branches in the Arava Desert on November 11, 2021, in Sapir, Israel. (credit: Amir Levy | Getty Images) In February 2019, an Israeli woman sat across from the son of Uganda’s president and made an audacious pitch—would he want to secretly hack any phone in the world? Lt. General Muhoozi Kainerugaba, in charge of his father’s security and a long-whispered successor to Yoweri Museveni, was keen, said two people familiar with the sales pitch. After all, the woman, who had ties [...]
