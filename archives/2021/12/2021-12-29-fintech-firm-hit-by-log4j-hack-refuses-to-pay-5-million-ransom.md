Title: Fintech firm hit by log4j hack refuses to pay $5 million ransom
Date: 2021-12-29T07:07:07-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-12-29-fintech-firm-hit-by-log4j-hack-refuses-to-pay-5-million-ransom

[Source](https://www.bleepingcomputer.com/news/security/fintech-firm-hit-by-log4j-hack-refuses-to-pay-5-million-ransom/){:target="_blank" rel="noopener"}

> One of the largest Vietnamese crypto trading platforms, ONUS, recently suffered a cyber attack on its payment system running a vulnerable Log4j version. Soon enough, threat actors approached ONUS to extort $5 million and threatened to publish the customer data should ONUS refuse to comply. [...]
