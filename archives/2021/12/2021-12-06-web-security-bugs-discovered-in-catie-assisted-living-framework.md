Title: Web security bugs discovered in CATIE assisted living framework
Date: 2021-12-06T17:00:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-06-web-security-bugs-discovered-in-catie-assisted-living-framework

[Source](https://portswigger.net/daily-swig/web-security-bugs-discovered-in-catie-assisted-living-framework){:target="_blank" rel="noopener"}

> Care home communications tool conundrum [...]
