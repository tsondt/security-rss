Title: Microsoft seizes sites used by APT15 Chinese state hackers
Date: 2021-12-06T16:53:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-12-06-microsoft-seizes-sites-used-by-apt15-chinese-state-hackers

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-seizes-sites-used-by-apt15-chinese-state-hackers/){:target="_blank" rel="noopener"}

> Microsoft seized today dozens of malicious sites used by the Nickel China-based hacking group to target organizations in the US and 28 other countries worldwide. [...]
