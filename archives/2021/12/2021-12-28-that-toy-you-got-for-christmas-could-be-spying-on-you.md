Title: That Toy You Got for Christmas Could Be Spying on You
Date: 2021-12-28T16:31:41+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: IoT;Mobile Security;Privacy;Vulnerabilities
Slug: 2021-12-28-that-toy-you-got-for-christmas-could-be-spying-on-you

[Source](https://threatpost.com/toy-christmas-spying/177288/){:target="_blank" rel="noopener"}

> Security flaws in the recently released Fisher-Price Chatter Bluetooth telephone can allow nearby attackers to spy on calls or communicate with children using the device. [...]
