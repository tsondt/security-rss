Title: How to tackle hybrid cloud security and DevSecOps
Date: 2021-12-21T20:29:26+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2021-12-21-how-to-tackle-hybrid-cloud-security-and-devsecops

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/21/devsecops_hybrid_cloud_security/){:target="_blank" rel="noopener"}

> Putting the Sec into DevOps is key, says Red Hat Paid Feature Of all the ideas to surface in the 20-year history of cloud computing, few have proved as compelling as the hybrid cloud. Organizations understand on-premises data centers and how computing power can be rented through public clouds or accessed through dedicated private clouds.... [...]
