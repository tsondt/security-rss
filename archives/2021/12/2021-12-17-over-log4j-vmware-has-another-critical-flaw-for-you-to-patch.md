Title: Over Log4j? VMware has another critical flaw for you to patch
Date: 2021-12-17T02:28:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-17-over-log4j-vmware-has-another-critical-flaw-for-you-to-patch

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/17/vmware_criticial_uem_flaw/){:target="_blank" rel="noopener"}

> Workspace ONE Unified Endpoint Management can leak info via server-side request forgery VMware customers have probably had a busy week because more than 100 of the IT giant's products are impacted by the Log4j bug.... [...]
