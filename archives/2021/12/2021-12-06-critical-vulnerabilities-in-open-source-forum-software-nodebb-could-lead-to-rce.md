Title: Critical vulnerabilities in open source forum software NodeBB could lead to RCE
Date: 2021-12-06T15:10:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-06-critical-vulnerabilities-in-open-source-forum-software-nodebb-could-lead-to-rce

[Source](https://portswigger.net/daily-swig/critical-vulnerabilities-in-open-source-forum-software-nodebb-could-lead-to-rce){:target="_blank" rel="noopener"}

> Personal data, account access is at risk [...]
