Title: How Vuclip safeguards its cloud environment across 100+ projects with Security Command Center
Date: 2021-12-07T17:00:00+00:00
Author: Daniel Li
Category: GCP Security
Tags: Customers;Google Cloud;Media & Entertainment;Identity & Security
Slug: 2021-12-07-how-vuclip-safeguards-its-cloud-environment-across-100-projects-with-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/how-vuclip-uses-security-command-center/){:target="_blank" rel="noopener"}

> Entertainment has never been more accessible. As our phones are now an inextricable part of our lives, there’s an increasing appetite for mobile video content, and that is what Vuclip delivers. Vuclip is a leading video-on-demand service for mobile devices with more than 41 million monthly active users across more than 22 countries. Speed is critical to the viewing experience, and delivering crisp, no-buffer video streaming was one of the reasons we decided to migrate to Google Cloud in 2017. Now we have replaced our monolithic on-prem infrastructure with a microservices-based production environment that’s almost fully on Google Cloud. Most [...]
