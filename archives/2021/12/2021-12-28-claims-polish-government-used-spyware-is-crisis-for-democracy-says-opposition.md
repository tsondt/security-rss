Title: Claims Polish government used spyware is ‘crisis for democracy’, says opposition
Date: 2021-12-28T16:19:49+00:00
Author: Agence France-Presse in Warsaw
Category: The Guardian
Tags: Poland;Surveillance;Europe;Malware;Technology;Data and computer security
Slug: 2021-12-28-claims-polish-government-used-spyware-is-crisis-for-democracy-says-opposition

[Source](https://www.theguardian.com/world/2021/dec/28/poland-pegasus-spyware-donald-tusk){:target="_blank" rel="noopener"}

> Opposition leader Donald Tusk calls for inquiry after watchdog says government’s rivals were targeted by Pegasus spyware Polish opposition leader Donald Tusk said on Tuesday reports that the government spied on its opponents represented the country’s biggest “crisis for democracy” since the end of communism. A cybersecurity watchdog last week said the Pegasus spyware had been used to target prominent opposition figures, with Polish media dubbing the scandal a “Polish Watergate”. Continue reading... [...]
