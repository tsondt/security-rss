Title: Mozilla fixes critical bug in cross-platform cryptography library
Date: 2021-12-01T12:39:15-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-01-mozilla-fixes-critical-bug-in-cross-platform-cryptography-library

[Source](https://www.bleepingcomputer.com/news/security/mozilla-fixes-critical-bug-in-cross-platform-cryptography-library/){:target="_blank" rel="noopener"}

> Mozilla has addressed a critical memory corruption vulnerability affecting its cross-platform Network Security Services (NSS) set of cryptography libraries. [...]
