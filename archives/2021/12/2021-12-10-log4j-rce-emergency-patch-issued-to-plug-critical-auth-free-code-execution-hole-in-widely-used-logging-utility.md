Title: Log4j RCE: Emergency patch issued to plug critical auth-free code execution hole in widely-used logging utility
Date: 2021-12-10T16:04:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-10-log4j-rce-emergency-patch-issued-to-plug-critical-auth-free-code-execution-hole-in-widely-used-logging-utility

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/10/log4j_remote_code_execution_vuln_patch_issued/){:target="_blank" rel="noopener"}

> Prepare to have a very busy weekend of mitigating and patching An unauthenticated remote code execution vulnerability in Apache's Log4j Java-based logging tool is being actively exploited, researchers have warned after it was used to execute code on Minecraft servers.... [...]
