Title: Cloud Security podcast by Google turns 46 - Reflections and lessons!
Date: 2021-12-07T17:00:00+00:00
Author: Timothy Peacock
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-12-07-cloud-security-podcast-by-google-turns-46-reflections-and-lessons

[Source](https://cloud.google.com/blog/products/identity-security/cloud-security-podcast-by-google-turns-46/){:target="_blank" rel="noopener"}

> Time flies when you’re having fun! We’ve produced 46 episodes of the Cloud Security Podcast by Google since our launch in February 2021. Looking back, we’d like to share some cloud security lessons and insights we picked up along the way. Over the course of 2021, the following themes emerged as the most popular with our audience. Zero trust security Cloud threat detection Making Cloud migrations more secure Data security in the cloud Let's explore each of these while highlighting some of the more interesting episodes: Zero trust On the zero trust side, we had a great episode where we [...]
