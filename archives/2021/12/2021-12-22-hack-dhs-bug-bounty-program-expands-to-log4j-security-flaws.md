Title: ‘Hack DHS’ bug bounty program expands to Log4j security flaws
Date: 2021-12-22T15:30:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-22-hack-dhs-bug-bounty-program-expands-to-log4j-security-flaws

[Source](https://www.bleepingcomputer.com/news/security/hack-dhs-bug-bounty-program-expands-to-log4j-security-flaws/){:target="_blank" rel="noopener"}

> The Department of Homeland Security (DHS) has announced that the 'Hack DHS' program is now also open to bug bounty hunters willing to track down DHS systems impacted by Log4j vulnerabilities. [...]
