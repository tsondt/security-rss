Title: Sixth member of notorious SIM-swapping cybercrime gang sentenced
Date: 2021-12-01T13:29:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-01-sixth-member-of-notorious-sim-swapping-cybercrime-gang-sentenced

[Source](https://portswigger.net/daily-swig/sixth-member-of-notorious-sim-swapping-cybercrime-gang-sentenced){:target="_blank" rel="noopener"}

> US crime syndicate ‘The Community’ stole millions of dollars’ worth of cryptocurrency [...]
