Title: Utility biz Delta-Montrose Electric Association loses billing capability and two decades of records after cyber attack
Date: 2021-12-03T22:06:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-03-utility-biz-delta-montrose-electric-association-loses-billing-capability-and-two-decades-of-records-after-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/03/dmea_colorado_cyber_attack_billing_systems/){:target="_blank" rel="noopener"}

> All together now - R, A, N, S, O... A US utility company based in Colorado was hit by a ransomware attack in November that wiped out two decades' worth of records and knocked out billing systems that won't be restored until next week at the earliest.... [...]
