Title: How expired web domains help criminal hackers unlock enterprise defenses
Date: 2021-12-16T12:59:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-16-how-expired-web-domains-help-criminal-hackers-unlock-enterprise-defenses

[Source](https://portswigger.net/daily-swig/how-expired-web-domains-help-criminal-hackers-unlock-enterprise-defenses){:target="_blank" rel="noopener"}

> Allow domains to ‘drop’ and you’re increasing the effectiveness of a variety of attacks [...]
