Title: Twitter bots pose as support staff to steal your cryptocurrency
Date: 2021-12-07T04:04:02-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-07-twitter-bots-pose-as-support-staff-to-steal-your-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/security/twitter-bots-pose-as-support-staff-to-steal-your-cryptocurrency/){:target="_blank" rel="noopener"}

> Scammers monitor every tweet containing requests for support on MetaMask, TrustWallet, and other popular crypto wallets, and respond to them with scam links in just seconds. [...]
