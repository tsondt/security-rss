Title: ‘Karakurt’ Extortion Threat Emerges, But Says No to Ransomware
Date: 2021-12-10T13:16:43+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;Malware;Vulnerabilities;Web Security
Slug: 2021-12-10-karakurt-extortion-threat-emerges-but-says-no-to-ransomware

[Source](https://threatpost.com/extortion-karakurt-threat-ransomware/176911/){:target="_blank" rel="noopener"}

> The threat group, first identified in June, focuses solely on data exfiltration and subsequent extortion, and has already targeted 40 victims since September. [...]
