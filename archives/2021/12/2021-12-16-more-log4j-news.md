Title: More Log4j News
Date: 2021-12-16T15:50:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data protection;patching;vulnerabilities;zero-day
Slug: 2021-12-16-more-log4j-news

[Source](https://www.schneier.com/blog/archives/2021/12/more-log4j-news.html){:target="_blank" rel="noopener"}

> Log4j is being exploited by all sorts of attackers, all over the Internet: At that point it was reported that there were over 100 attempts to exploit the vulnerability every minute. “Since we started to implement our protection we prevented over 1,272,000 attempts to allocate the vulnerability, over 46% of those attempts were made by known malicious groups,” said cybersecurity company Check Point. And according to Check Point, attackers have now attempted to exploit the flaw on over 40% of global networks. And a second vulnerability was found, in the patch for the first vulnerability. This is likely not to [...]
