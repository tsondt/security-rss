Title: Phishing campaign uses PowerPoint macros to drop Agent Tesla
Date: 2021-12-13T15:49:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-13-phishing-campaign-uses-powerpoint-macros-to-drop-agent-tesla

[Source](https://www.bleepingcomputer.com/news/security/phishing-campaign-uses-powerpoint-macros-to-drop-agent-tesla/){:target="_blank" rel="noopener"}

> A new variant of the Agent Tesla malware has been spotted in an ongoing phishing campaign that relies on Microsoft PowerPoint documents laced with malicious macro code. [...]
