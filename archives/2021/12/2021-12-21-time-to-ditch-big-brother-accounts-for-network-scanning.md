Title: Time to Ditch Big-Brother Accounts for Network Scanning
Date: 2021-12-21T22:08:01+00:00
Author: Yaron Kassner
Category: Threatpost
Tags: InfoSec Insider;Vulnerabilities;Web Security
Slug: 2021-12-21-time-to-ditch-big-brother-accounts-for-network-scanning

[Source](https://threatpost.com/domain-admin-accounts-scan-network/177194/){:target="_blank" rel="noopener"}

> Yaron Kassner, CTO and co-founder of Silverfort, discusses why using all-seeing privileged accounts for monitoring is bad practice. [...]
