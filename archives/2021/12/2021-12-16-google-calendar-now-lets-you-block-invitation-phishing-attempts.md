Title: Google Calendar now lets you block invitation phishing attempts
Date: 2021-12-16T13:39:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-16-google-calendar-now-lets-you-block-invitation-phishing-attempts

[Source](https://www.bleepingcomputer.com/news/security/google-calendar-now-lets-you-block-invitation-phishing-attempts/){:target="_blank" rel="noopener"}

> Google now makes it easy to block unwanted calendar invitations, commonly used by threat actors in phishing and malicious campaigns, from being added to your Google Calendar. [...]
