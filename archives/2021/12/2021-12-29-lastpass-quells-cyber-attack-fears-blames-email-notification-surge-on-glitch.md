Title: LastPass quells cyber-attack fears, blames email notification surge on ‘glitch’
Date: 2021-12-29T14:44:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-29-lastpass-quells-cyber-attack-fears-blames-email-notification-surge-on-glitch

[Source](https://portswigger.net/daily-swig/lastpass-quells-cyber-attack-fears-blames-email-notification-surge-on-glitch){:target="_blank" rel="noopener"}

> Password vault investigation reveals no evidence of credential stuffing activity [...]
