Title: Threat Advisory: E-commerce Bots Use Domain Registration Services for Mass Account Fraud
Date: 2021-12-29T19:13:45+00:00
Author: Jason Kent
Category: Threatpost
Tags: InfoSec Insider;Web Security
Slug: 2021-12-29-threat-advisory-e-commerce-bots-use-domain-registration-services-for-mass-account-fraud

[Source](https://threatpost.com/ecommerce-bots-domain-registration-account-fraud/177305/){:target="_blank" rel="noopener"}

> Jason Kent, hacker-in-residence at Cequence Security, discusses sneaky shopping bot tactics (i.e., domain parking) seen in a mass campaign, and what retail security teams can do about them. [...]
