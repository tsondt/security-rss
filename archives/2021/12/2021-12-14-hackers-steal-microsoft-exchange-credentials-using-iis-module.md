Title: Hackers steal Microsoft Exchange credentials using IIS module
Date: 2021-12-14T12:16:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-14-hackers-steal-microsoft-exchange-credentials-using-iis-module

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-microsoft-exchange-credentials-using-iis-module/){:target="_blank" rel="noopener"}

> Threat actors are installing a malicious IIS web server module named 'Owowa' on Microsoft Exchange Outlook Web Access servers to steal credentials and execute commands on the server remotely. [...]
