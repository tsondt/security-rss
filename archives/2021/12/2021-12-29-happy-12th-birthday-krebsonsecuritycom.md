Title: Happy 12th Birthday, KrebsOnSecurity.com!
Date: 2021-12-29T21:32:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other
Slug: 2021-12-29-happy-12th-birthday-krebsonsecuritycom

[Source](https://krebsonsecurity.com/2021/12/happy-12th-birthday-krebsonsecurity-com/){:target="_blank" rel="noopener"}

> KrebsOnSecurity.com celebrates its 12th anniversary today! Maybe “celebrate” is too indelicate a word for a year wracked by the global pandemics of COVID-19 and ransomware. Especially since stories about both have helped to grow the audience here tremendously in 2021. But this site’s birthday also is a welcome opportunity to thank you all for your continued readership and support, which helps keep the content here free to everyone. More than seven million unique visitors came to KrebsOnSecurity.com in 2021, generating some 12 million+ pageviews and leaving almost 8,000 comments. We also now have nearly 50,000 subscribers to our email newsletter, [...]
