Title: ‘Seedworm’ Attackers Target Telcos in Asia, Middle East
Date: 2021-12-14T13:21:57+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: 2021-12-14-seedworm-attackers-target-telcos-in-asia-middle-east

[Source](https://threatpost.com/seedworm-attackers-telcos-asia-middle-east/176992/){:target="_blank" rel="noopener"}

> The focused attacks aimed at cyberespionage and lateral movement appear to hint at further ambitions by the group, including supply-chain threats. [...]
