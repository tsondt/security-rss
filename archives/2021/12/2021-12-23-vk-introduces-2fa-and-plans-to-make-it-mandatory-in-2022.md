Title: VK introduces 2FA and plans to make it mandatory in 2022
Date: 2021-12-23T10:01:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-23-vk-introduces-2fa-and-plans-to-make-it-mandatory-in-2022

[Source](https://www.bleepingcomputer.com/news/security/vk-introduces-2fa-and-plans-to-make-it-mandatory-in-2022/){:target="_blank" rel="noopener"}

> VK, Russia's most popular social media platform with 650 million users, is finally introducing two-factor authentication on all its services and plans to make it mandatory in February 2022 for administrators of large communities. [...]
