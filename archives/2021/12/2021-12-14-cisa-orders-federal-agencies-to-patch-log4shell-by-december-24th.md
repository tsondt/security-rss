Title: CISA orders federal agencies to patch Log4Shell by December 24th
Date: 2021-12-14T09:46:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-14-cisa-orders-federal-agencies-to-patch-log4shell-by-december-24th

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-federal-agencies-to-patch-log4shell-by-december-24th/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has ordered federal agencies to patch systems against the critical Log4Shell remote code execution vulnerability and released mitigation guidance in response to active exploitation. [...]
