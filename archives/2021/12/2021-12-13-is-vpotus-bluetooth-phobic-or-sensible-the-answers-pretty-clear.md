Title: Is VPOTUS Bluetooth-phobic or sensible? The answer's pretty clear
Date: 2021-12-13T17:01:05+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-12-13-is-vpotus-bluetooth-phobic-or-sensible-the-answers-pretty-clear

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/13/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: bugs found on Mars! Of the software kind, of course In Brief The vice-president of the United States, Kamala Harris, was mocked by commentators this week for her aversion to Bluetooth on security grounds. Security professionals think she has a point – given her position.... [...]
