Title: AWS Among 12 Cloud Services Affected by Flaws in Eltima SDK
Date: 2021-12-08T18:54:12+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: 2021-12-08-aws-among-12-cloud-services-affected-by-flaws-in-eltima-sdk

[Source](https://threatpost.com/aws-cloud-services-flaws-eltima/176852/){:target="_blank" rel="noopener"}

> The flaws, which could enable attackers to disable security and gain kernel-level privileges, affect Amazon WorkSpaces and other cloud services that use USB over Ethernet. [...]
