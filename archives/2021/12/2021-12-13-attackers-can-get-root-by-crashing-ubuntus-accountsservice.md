Title: Attackers can get root by crashing Ubuntu’s AccountsService
Date: 2021-12-13T12:05:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Linux;Security
Slug: 2021-12-13-attackers-can-get-root-by-crashing-ubuntus-accountsservice

[Source](https://www.bleepingcomputer.com/news/security/attackers-can-get-root-by-crashing-ubuntu-s-accountsservice/){:target="_blank" rel="noopener"}

> A local privilege escalation security vulnerability could allow attackers to gain root access on Ubuntu systems by exploiting a double-free memory corruption bug in GNOME's AccountsService component. [...]
