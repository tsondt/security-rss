Title: ‘PseudoManuscrypt’ Mass Spyware Campaign Targets 35K Systems
Date: 2021-12-16T18:36:40+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Critical Infrastructure;Government;Hacks;Malware;Web Security
Slug: 2021-12-16-pseudomanuscrypt-mass-spyware-campaign-targets-35k-systems

[Source](https://threatpost.com/pseudomanuscrypt-mass-spyware-campaign/177097/){:target="_blank" rel="noopener"}

> It’s similar to Lazarus’s Manuscrypt malware, but the new spyware is splattering itself onto government organizations and ICS in a non-Lazarus-like, untargeted wave of attacks. [...]
