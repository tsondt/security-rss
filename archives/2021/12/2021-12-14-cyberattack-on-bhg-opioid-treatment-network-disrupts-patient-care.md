Title: Cyberattack on BHG opioid treatment network disrupts patient care
Date: 2021-12-14T10:35:32-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-14-cyberattack-on-bhg-opioid-treatment-network-disrupts-patient-care

[Source](https://www.bleepingcomputer.com/news/security/cyberattack-on-bhg-opioid-treatment-network-disrupts-patient-care/){:target="_blank" rel="noopener"}

> Opioid treatment network Behavioral Health Group suffered a cyberattack that led to an almost week-long disruption of IT systems and patient care. [...]
