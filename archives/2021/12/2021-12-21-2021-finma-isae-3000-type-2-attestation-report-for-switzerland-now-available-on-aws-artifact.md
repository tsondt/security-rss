Title: 2021 FINMA ISAE 3000 Type 2 attestation report for Switzerland now available on AWS Artifact
Date: 2021-12-21T18:47:53+00:00
Author: Niyaz Noor
Category: AWS Security
Tags: Financial Services;Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS FINMA;Outsourcing Guidelines;Security Blog;Swiss banking regulations;Switzerland
Slug: 2021-12-21-2021-finma-isae-3000-type-2-attestation-report-for-switzerland-now-available-on-aws-artifact

[Source](https://aws.amazon.com/blogs/security/2021-finma-isae-3000-type-2-attestation-report-for-switzerland-now-available-on-aws-artifact/){:target="_blank" rel="noopener"}

> AWS is pleased to announce the issuance of a second Swiss Financial Market Supervisory Authority (FINMA) ISAE 3000 Type 2 attestation report. The latest report covers the period from October 1, 2020 to September 30, 2021, with a total of 141 AWS services and 23 global AWS Regions included in the scope. A full list of certified services [...]
