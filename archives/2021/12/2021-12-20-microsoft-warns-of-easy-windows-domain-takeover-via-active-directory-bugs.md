Title: Microsoft warns of easy Windows domain takeover via Active Directory bugs
Date: 2021-12-20T14:51:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-12-20-microsoft-warns-of-easy-windows-domain-takeover-via-active-directory-bugs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-warns-of-easy-windows-domain-takeover-via-active-directory-bugs/){:target="_blank" rel="noopener"}

> Microsoft warned customers today to patch two Active Directory domain service privilege escalation security flaws that, when combined, allow attackers to easily takeover Windows domains. [...]
