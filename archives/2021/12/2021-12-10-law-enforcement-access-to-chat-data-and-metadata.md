Title: Law Enforcement Access to Chat Data and Metadata
Date: 2021-12-10T12:37:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;FBI;law enforcement;metadata;privacy
Slug: 2021-12-10-law-enforcement-access-to-chat-data-and-metadata

[Source](https://www.schneier.com/blog/archives/2021/12/law-enforcement-access-to-chat-data-and-metadata.html){:target="_blank" rel="noopener"}

> A January 2021 FBI document outlines what types of data and metadata can be lawfully obtained by the FBI from messaging apps. Rolling Stone broke the story and it’s been written about elsewhere. I don’t see a lot of surprises in the document. Lots of apps leak all sorts of metadata: iMessage and WhatsApp seem to be the worst. Signal protects the most metadata. End-to-end encrypted message content can be available if the user uploads it to an unencrypted backup server. [...]
