Title: Friday Squid Blogging: Squeeze the Squid
Date: 2021-12-03T20:18:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-12-03-friday-squid-blogging-squeeze-the-squid

[Source](https://www.schneier.com/blog/archives/2021/12/friday-squid-blogging-squeeze-the-squid.html){:target="_blank" rel="noopener"}

> Squeeze the Squid is a band. It just released its second album. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
