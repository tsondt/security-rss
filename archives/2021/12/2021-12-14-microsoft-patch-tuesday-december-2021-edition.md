Title: Microsoft Patch Tuesday, December 2021 Edition
Date: 2021-12-14T22:23:44+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;adobe;chrome;CVE-2021-41379;CVE-2021-43883;CVE-2021-43890;CVE-2021-43905;Dustin Childs;google;Immersive Labs;Johannes Ullrich;Kevin Beaumont;Kevin Breen;microsoft;Microsoft Patch Tuesday December 2021;sans internet storm center;Satnam Narang;Tenable;trend micro
Slug: 2021-12-14-microsoft-patch-tuesday-december-2021-edition

[Source](https://krebsonsecurity.com/2021/12/microsoft-patch-tuesday-december-2021-edition/){:target="_blank" rel="noopener"}

> Microsoft, Adobe, and Google all issued security updates to their products today. The Microsoft patches include six previously disclosed security flaws, and one that is already being actively exploited. But this month’s Patch Tuesday is overshadowed by the “ Log4Shell ” 0-day exploit in a popular Java library that web server administrators are now racing to find and patch amid widespread exploitation of the flaw. Log4Shell is the name picked for a critical flaw disclosed Dec. 9 in the popular logging library for Java called “ log4j,” which is included in a huge number of Java applications. Publicly released exploit [...]
