Title: Desjardins data breach: Class action lawsuit agreement reaches $201 million
Date: 2021-12-17T12:24:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-17-desjardins-data-breach-class-action-lawsuit-agreement-reaches-201-million

[Source](https://portswigger.net/daily-swig/desjardins-data-breach-class-action-lawsuit-agreement-reaches-201-million){:target="_blank" rel="noopener"}

> Final amount to be confirmed in 2022 [...]
