Title: Severe Chrome bug allowed RCE on devices running remote headless interface
Date: 2021-12-14T13:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-14-severe-chrome-bug-allowed-rce-on-devices-running-remote-headless-interface

[Source](https://portswigger.net/daily-swig/severe-chrome-bug-allowed-rce-on-devices-running-remote-headless-interface){:target="_blank" rel="noopener"}

> Attackers could read and write arbitrary files to a device’s hard drive [...]
