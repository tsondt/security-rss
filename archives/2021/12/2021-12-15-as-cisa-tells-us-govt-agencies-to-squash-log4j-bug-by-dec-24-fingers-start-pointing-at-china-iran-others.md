Title: As CISA tells US govt agencies to squash Log4j bug by Dec 24, fingers start pointing at China, Iran, others
Date: 2021-12-15T23:31:07+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: 2021-12-15-as-cisa-tells-us-govt-agencies-to-squash-log4j-bug-by-dec-24-fingers-start-pointing-at-china-iran-others

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/15/log4j_latest_cisa/){:target="_blank" rel="noopener"}

> Microsoft says cyber-spies linked to Beijing, Tehran are getting busy with security flaw along with world + dog Microsoft reckons government cyber-spies in China, Iran, North Korea, and Turkey are actively exploiting the Log4j 2.x remote-code execution hole.... [...]
