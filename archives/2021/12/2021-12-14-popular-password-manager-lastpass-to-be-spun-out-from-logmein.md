Title: Popular password manager LastPass to be spun out from LogMeIn
Date: 2021-12-14T17:11:06+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2021-12-14-popular-password-manager-lastpass-to-be-spun-out-from-logmein

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/14/lastpass_spinout/){:target="_blank" rel="noopener"}

> Private equity owners play pass the parcel One of the biggest beasts in the password management world, LastPass, is being spun out from parent LogMeIn as a "standalone cloud security" organisation.... [...]
