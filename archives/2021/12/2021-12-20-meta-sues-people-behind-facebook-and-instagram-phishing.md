Title: Meta sues people behind Facebook and Instagram phishing
Date: 2021-12-20T13:37:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-20-meta-sues-people-behind-facebook-and-instagram-phishing

[Source](https://www.bleepingcomputer.com/news/security/meta-sues-people-behind-facebook-and-instagram-phishing/){:target="_blank" rel="noopener"}

> Meta (formerly known as Facebook) has filed a federal lawsuit in California court to disrupt phishing attacks targeting Facebook, Messenger, Instagram, and WhatsApp users. [...]
