Title: Relentless Log4j Attacks Include State Actors, Possible Worm
Date: 2021-12-15T23:18:44+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-12-15-relentless-log4j-attacks-include-state-actors-possible-worm

[Source](https://threatpost.com/log4j-attacks-state-actors-worm/177088/){:target="_blank" rel="noopener"}

> More than 1.8 million attacks, against half of all corporate networks, have already launched to exploit Log4Shell. [...]
