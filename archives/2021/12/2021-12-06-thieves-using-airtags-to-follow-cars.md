Title: Thieves Using AirTags to “Follow” Cars
Date: 2021-12-06T16:25:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;cars;geolocation;theft;tracking
Slug: 2021-12-06-thieves-using-airtags-to-follow-cars

[Source](https://www.schneier.com/blog/archives/2021/12/thieves-using-airtags-to-follow-cars.html){:target="_blank" rel="noopener"}

> From Ontario and not surprising : Since September 2021, officers have investigated five incidents where suspects have placed small tracking devices on high-end vehicles so they can later locate and steal them. Brand name “air tags” are placed in out-of-sight areas of the target vehicles when they are parked in public places like malls or parking lots. Thieves then track the targeted vehicles to the victim’s residence, where they are stolen from the driveway. Thieves typically use tools like screwdrivers to enter the vehicles through the driver or passenger door, while ensuring not to set off alarms. Once inside, an [...]
