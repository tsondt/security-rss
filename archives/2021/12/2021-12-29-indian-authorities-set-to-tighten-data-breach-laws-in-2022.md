Title: Indian authorities set to tighten data breach laws in 2022
Date: 2021-12-29T11:50:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-29-indian-authorities-set-to-tighten-data-breach-laws-in-2022

[Source](https://portswigger.net/daily-swig/indian-authorities-set-to-tighten-data-breach-laws-in-2022){:target="_blank" rel="noopener"}

> Credit card storage rules and 72-hour breach notification deadline due to come into play next year [...]
