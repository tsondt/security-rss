Title: Google disrupts massive Glupteba botnet, sues Russian operators
Date: 2021-12-07T11:57:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-07-google-disrupts-massive-glupteba-botnet-sues-russian-operators

[Source](https://www.bleepingcomputer.com/news/security/google-disrupts-massive-glupteba-botnet-sues-russian-operators/){:target="_blank" rel="noopener"}

> Google announced today that it has taken action to disrupt the Glupteba botnet that now controls more than 1 million Windows PCs around the world, growing by thousands of new infected devices each day. [...]
