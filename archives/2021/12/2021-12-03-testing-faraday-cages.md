Title: Testing Faraday Cages
Date: 2021-12-03T12:13:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2021-12-03-testing-faraday-cages

[Source](https://www.schneier.com/blog/archives/2021/12/testing-faraday-cages.html){:target="_blank" rel="noopener"}

> Matt Blaze tested a variety of Faraday cages for phones, both commercial and homemade. The bottom line: A quick and likely reliable “go/no go test” can be done with an Apple AirTag and an iPhone: drop the AirTag in the bag under test, and see if the phone can locate it and activate its alarm (beware of caching in the FindMy app when doing this). This test won’t tell you the exact attenuation level, of course, but it will tell you if the attenuation is sufficient for most practical purposes. It can also detect whether an otherwise good bag has [...]
