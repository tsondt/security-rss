Title: Twitter account of FBI's fake chat app, ANOM seen trolling today
Date: 2021-12-30T07:20:58-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-12-30-twitter-account-of-fbis-fake-chat-app-anom-seen-trolling-today

[Source](https://www.bleepingcomputer.com/news/security/twitter-account-of-fbis-fake-chat-app-anom-seen-trolling-today/){:target="_blank" rel="noopener"}

> The Twitter account previously associated with the ANOM chat app is posting frivolous tweets this week. ANOM was a fake encrypted messaging platform created as part of a global sting operation led by the U.S. FBI, Australian Federal Police (AFP), and other law enforcement agencies to catch criminals. [...]
