Title: SAP squashes SQL injection, XSS bugs in December patch round
Date: 2021-12-16T16:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-16-sap-squashes-sql-injection-xss-bugs-in-december-patch-round

[Source](https://portswigger.net/daily-swig/sap-squashes-sql-injection-xss-bugs-in-december-patch-round){:target="_blank" rel="noopener"}

> CVSS severity scores range from 2.4 to 9.9 [...]
