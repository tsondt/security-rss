Title: Nordic Choice Hotels hit by Conti ransomware, no ransom demand yet
Date: 2021-12-07T02:39:46-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-12-07-nordic-choice-hotels-hit-by-conti-ransomware-no-ransom-demand-yet

[Source](https://www.bleepingcomputer.com/news/security/nordic-choice-hotels-hit-by-conti-ransomware-no-ransom-demand-yet/){:target="_blank" rel="noopener"}

> Nordic Choice Hotels has now confirmed a cyber attack on its systems from the Conti ransomware group. Although there is no indication of card or payment information being affected, information pertaining to guest bookings was potentially leaked. [...]
