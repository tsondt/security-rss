Title: McMenamins breweries hit by a Conti ransomware attack
Date: 2021-12-16T15:48:04-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-16-mcmenamins-breweries-hit-by-a-conti-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/mcmenamins-breweries-hit-by-a-conti-ransomware-attack/){:target="_blank" rel="noopener"}

> Portland brewery and hotel chain McMenamins suffered a Conti ransomware attack over the weekend that disrupted the company's operations. [...]
