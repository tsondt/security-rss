Title: Gumtree classifieds site leaked personal info via the F12 key
Date: 2021-12-16T11:20:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-16-gumtree-classifieds-site-leaked-personal-info-via-the-f12-key

[Source](https://www.bleepingcomputer.com/news/security/gumtree-classifieds-site-leaked-personal-info-via-the-f12-key/){:target="_blank" rel="noopener"}

> British classifieds site Gumtree.com suffered a data leak after a security researcher revealed that he could access sensitive personally identifiable data of advertisers simply by pressing F12 on the keyboard. [...]
