Title: Critical web security flaws in Kaseya Unitrends&nbsp;backup appliances remediated after researchers’ disclosure
Date: 2021-12-07T15:22:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-07-critical-web-security-flaws-in-kaseya-unitrends-backup-appliances-remediated-after-researchers-disclosure

[Source](https://portswigger.net/daily-swig/critical-web-security-flaws-in-kaseya-unitrends-nbsp-backup-appliances-remediated-after-researchers-disclosure){:target="_blank" rel="noopener"}

> Two critical flaws addressed in cloud storage patch batch [...]
