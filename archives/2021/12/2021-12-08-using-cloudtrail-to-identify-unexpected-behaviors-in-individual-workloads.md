Title: Using CloudTrail to identify unexpected behaviors in individual workloads
Date: 2021-12-08T20:09:53+00:00
Author: Volker Rath
Category: AWS Security
Tags: AWS CloudTrail;Security, Identity, & Compliance;Security Blog
Slug: 2021-12-08-using-cloudtrail-to-identify-unexpected-behaviors-in-individual-workloads

[Source](https://aws.amazon.com/blogs/security/using-cloudtrail-to-identify-unexpected-behaviors-in-individual-workloads/){:target="_blank" rel="noopener"}

> In this post, we describe a practical approach that you can use to detect anomalous behaviors within Amazon Web Services (AWS) cloud workloads by using behavioral analysis techniques that can be used to augment existing threat detection solutions. Anomaly detection is an advanced threat detection technique that should be considered when a mature security baseline [...]
