Title: US government launches ‘Hack the DHS’ bug bounty program
Date: 2021-12-15T15:39:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-15-us-government-launches-hack-the-dhs-bug-bounty-program

[Source](https://portswigger.net/daily-swig/us-government-launches-hack-the-dhs-bug-bounty-program){:target="_blank" rel="noopener"}

> Initiative will also invite selected security researchers to a live hacking event [...]
