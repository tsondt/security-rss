Title: You may have cracked serverless development, but it’s almost certain you haven’t solved serverless security
Date: 2021-12-14T18:00:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-12-14-you-may-have-cracked-serverless-development-but-its-almost-certain-you-havent-solved-serverless-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/14/you_may_have_cracked_serverless/){:target="_blank" rel="noopener"}

> Here’s how to secure that ever-expanding attack surface Paid Post Serverless is revolutionizing software development, allowing organizations to produce applications which consume cloud resources only when they need to. Developing applications this way also dramatically reduces the amount of code to write while increasing the velocity of completed applications.... [...]
