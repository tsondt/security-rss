Title: Fueled by Pandemic Realities, Grinchbots Aggressively Surge in Activity
Date: 2021-12-09T19:54:39+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Mobile Security;Web Security
Slug: 2021-12-09-fueled-by-pandemic-realities-grinchbots-aggressively-surge-in-activity

[Source](https://threatpost.com/pandemic-grinchbots-surge-activity/176898/){:target="_blank" rel="noopener"}

> E-commerce's proverbial Who-ville is under siege, with a rise in bots bent on ruining gift cards and snapping up coveted gifts for outrageously priced resale. [...]
