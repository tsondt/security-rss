Title: GOautodial vulnerabilities put call center network security on the line
Date: 2021-12-08T19:59:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-08-goautodial-vulnerabilities-put-call-center-network-security-on-the-line

[Source](https://portswigger.net/daily-swig/goautodial-vulnerabilities-put-call-center-network-security-on-the-line){:target="_blank" rel="noopener"}

> Now-patched bugs were easy to exploit, but required prior authentication/network access [...]
