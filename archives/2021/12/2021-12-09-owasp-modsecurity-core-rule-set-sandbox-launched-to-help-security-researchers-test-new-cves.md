Title: OWASP ModSecurity Core Rule Set sandbox launched to help security researchers test new CVEs
Date: 2021-12-09T15:55:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-09-owasp-modsecurity-core-rule-set-sandbox-launched-to-help-security-researchers-test-new-cves

[Source](https://portswigger.net/daily-swig/owasp-modsecurity-core-rule-set-sandbox-launched-to-help-security-researchers-test-new-cves){:target="_blank" rel="noopener"}

> Free-to-use feature can help users gauge whether the CRS protects against payloads [...]
