Title: Data breach impacts 80,000 South Australian govt employees
Date: 2021-12-10T05:17:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-10-data-breach-impacts-80000-south-australian-govt-employees

[Source](https://www.bleepingcomputer.com/news/security/data-breach-impacts-80-000-south-australian-govt-employees/){:target="_blank" rel="noopener"}

> The South Australian government has admitted that the personal details of tens of thousands of its employees were compromised following a cyber-attack on an external payroll software provider. [...]
