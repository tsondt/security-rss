Title: Singapore and UK ink digital trade agreements at Future Tech Forum in London
Date: 2021-12-01T14:30:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-12-01-singapore-and-uk-ink-digital-trade-agreements-at-future-tech-forum-in-london

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/01/singapore_uk_digital_trade_mous/){:target="_blank" rel="noopener"}

> Such memorandums of understanding are all the rage Singapore and the UK signed three memorandums of understanding (MoUs) this week, hoping to strengthen digital connectivity between the two island nations.... [...]
