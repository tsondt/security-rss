Title: How to automate AWS Managed Microsoft AD scaling based on utilization metrics
Date: 2021-12-01T23:43:13+00:00
Author: Dennis Rothmel
Category: AWS Security
Tags: Announcements;Customer Solutions;Active Directory;Kerberos;LDAP;Monitoring;Performance;Resilience;Scaling
Slug: 2021-12-01-how-to-automate-aws-managed-microsoft-ad-scaling-based-on-utilization-metrics

[Source](https://aws.amazon.com/blogs/security/how-to-automate-aws-managed-microsoft-ad-scaling-based-on-utilization-metrics/){:target="_blank" rel="noopener"}

> AWS Directory Service for Microsoft Active Directory (AWS Managed Microsoft AD), provides a fully managed service for Microsoft Active Directory (AD) in the AWS cloud. When you create your directory, AWS deploys two domain controllers in separate Availability Zones that are exclusively yours for high availability. For use cases requiring even higher resilience and performance, [...]
