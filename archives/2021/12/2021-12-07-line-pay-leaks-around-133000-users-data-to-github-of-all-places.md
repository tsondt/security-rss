Title: LINE Pay leaks around 133,000 users' data to GitHub, of all places
Date: 2021-12-07T04:03:45+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-12-07-line-pay-leaks-around-133000-users-data-to-github-of-all-places

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/07/line_pay_leaks_around_133000/){:target="_blank" rel="noopener"}

> Someone just accidentally put it there, says the messaging service company Smartphone payment provider LINE Pay announced yesterday that around 133,000 users' payment details were mistakenly published on GitHub between September and November of this year.... [...]
