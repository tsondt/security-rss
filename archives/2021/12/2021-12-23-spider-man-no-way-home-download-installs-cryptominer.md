Title: ‘Spider-Man: No Way Home’ Download Installs Cryptominer
Date: 2021-12-23T15:00:19+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-12-23-spider-man-no-way-home-download-installs-cryptominer

[Source](https://threatpost.com/spider-man-no-way-home-download-installs-cryptominer/177254/){:target="_blank" rel="noopener"}

> The origin of the Monero cryptominer file has been traced to a Russian torrent website, researchers report. [...]
