Title: Bad things come in threes: Apache reveals <i>another</i> Log4J bug
Date: 2021-12-19T22:57:26+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-19-bad-things-come-in-threes-apache-reveals-another-log4j-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/19/log4j_new_flaw_cve_2021_45105/){:target="_blank" rel="noopener"}

> Third major fix in ten days is an infinite recursion flaw rated 7.5/10 The Apache Software Foundation (ASF) has revealed a third bug in its Log4 Java-based open-source logging library Log4j.... [...]
