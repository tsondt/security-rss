Title: Malicious Exchange Server Module Hoovers Up Outlook Credentials
Date: 2021-12-15T19:34:04+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Privacy;Web Security
Slug: 2021-12-15-malicious-exchange-server-module-hoovers-up-outlook-credentials

[Source](https://threatpost.com/malicious-exchange-server-module-outlook-credentials/177077/){:target="_blank" rel="noopener"}

> "Owowa" stealthily lurks on IIS servers, waiting to harvest successful logins when an Outlook Web Access (OWA) authentication request is made. [...]
