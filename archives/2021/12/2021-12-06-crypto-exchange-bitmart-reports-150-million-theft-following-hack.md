Title: Crypto-exchange BitMart reports $150 million theft following hack
Date: 2021-12-06T12:43:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-06-crypto-exchange-bitmart-reports-150-million-theft-following-hack

[Source](https://portswigger.net/daily-swig/crypto-exchange-bitmart-reports-150-million-theft-following-hack){:target="_blank" rel="noopener"}

> Security firm said attackers executed a ‘transfer-out, swap, and wash’ [...]
