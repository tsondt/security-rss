Title: Microsoft Azure App Service flaw exposed customer source code
Date: 2021-12-22T14:15:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-12-22-microsoft-azure-app-service-flaw-exposed-customer-source-code

[Source](https://www.bleepingcomputer.com/news/security/microsoft-azure-app-service-flaw-exposed-customer-source-code/){:target="_blank" rel="noopener"}

> A security flaw found in Azure App Service, a Microsoft-managed platform for building and hosting web apps, led to the exposure of PHP, Node, Python, Ruby, or Java customer source code for at least four years, since 2017. [...]
