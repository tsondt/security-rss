Title: Ransomwared payroll provider leaks data on 38,000 Australian government workers
Date: 2021-12-10T05:58:17+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-10-ransomwared-payroll-provider-leaks-data-on-38000-australian-government-workers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/10/frontier_software_ransomware_incindent/){:target="_blank" rel="noopener"}

> Frontier Software admitted attack three weeks ago, said data was safe... now it's on the dark web Personal information describing names, addresses, bank account details, and taxation IDs of 38,000 Australian government employees has been leaked to the dark web after a ransomware attack.... [...]
