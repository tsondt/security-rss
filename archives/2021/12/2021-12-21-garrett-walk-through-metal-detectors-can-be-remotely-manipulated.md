Title: Garrett walk-through metal detectors can be remotely manipulated
Date: 2021-12-21T10:23:54-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-21-garrett-walk-through-metal-detectors-can-be-remotely-manipulated

[Source](https://www.bleepingcomputer.com/news/security/garrett-walk-through-metal-detectors-can-be-remotely-manipulated/){:target="_blank" rel="noopener"}

> Two widely used walk-through metal detectors made by Garrett are vulnerable to many remotely exploitable flaws that could severely impair their functionality, thus rendering security checkpoints deficient. [...]
