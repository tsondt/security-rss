Title: National Cyber Strategy will lead to BritChip for mobile devices by 2025, claims UK.gov
Date: 2021-12-16T07:29:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-16-national-cyber-strategy-will-lead-to-britchip-for-mobile-devices-by-2025-claims-ukgov

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/16/national_cyber_strategy_uk_launched/){:target="_blank" rel="noopener"}

> And potentially an increase in UK state-backed hacks The British government has launched a £2.6bn National Cyber Strategy, intended to steer the state's thinking on cyber attack, defence and technology for the next three years – and there's some good news if you run a tech company.... [...]
