Title: RAF shoots down 'terrorist drone' over US-owned special ops base in Syria
Date: 2021-12-17T15:29:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-17-raf-shoots-down-terrorist-drone-over-us-owned-special-ops-base-in-syria

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/17/raf_shoots_down_drone_syria/){:target="_blank" rel="noopener"}

> £200k Anglo-French heat-seeking missile does its thing The RAF has scored its first air-to-air "kill" – where an aircraft downs an enemy aircraft – for almost 40 years after shooting down a drone over Syria.... [...]
