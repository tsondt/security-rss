Title: Phishing attacks impersonate Pfizer in fake requests for quotation
Date: 2021-12-20T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-20-phishing-attacks-impersonate-pfizer-in-fake-requests-for-quotation

[Source](https://www.bleepingcomputer.com/news/security/phishing-attacks-impersonate-pfizer-in-fake-requests-for-quotation/){:target="_blank" rel="noopener"}

> Threat actors are conducting a highly targeted phishing campaign impersonating Pfizer to steal business and financial information from victims. [...]
