Title: New ransomware now being deployed in Log4Shell attacks
Date: 2021-12-14T17:02:25-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-14-new-ransomware-now-being-deployed-in-log4shell-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-ransomware-now-being-deployed-in-log4shell-attacks/){:target="_blank" rel="noopener"}

> The first public case of the Log4j Log4Shell vulnerability used to download and install ransomware has been discovered by researchers. [...]
