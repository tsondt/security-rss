Title: Four Bugs in Microsoft Teams Left Platform Vulnerable Since March
Date: 2021-12-22T14:03:05+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-12-22-four-bugs-in-microsoft-teams-left-platform-vulnerable-since-march

[Source](https://threatpost.com/microsoft-teams-bugs-vulnerable-march/177225/){:target="_blank" rel="noopener"}

> Attackers exploiting bugs in the “link preview” feature in Microsoft Teams could abuse the flaws to spoof links, leak an Android user’s IP address and launch a DoS attack. [...]
