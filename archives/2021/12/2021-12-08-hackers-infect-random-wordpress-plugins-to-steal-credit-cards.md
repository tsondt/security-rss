Title: Hackers infect random WordPress plugins to steal credit cards
Date: 2021-12-08T13:11:55-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-08-hackers-infect-random-wordpress-plugins-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/hackers-infect-random-wordpress-plugins-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> Credit card swipers are being injected into random plugins of e-commerce WordPress sites, hiding from detection while stealing customer payment details. [...]
