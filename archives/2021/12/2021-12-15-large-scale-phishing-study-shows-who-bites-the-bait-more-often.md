Title: Large-scale phishing study shows who bites the bait more often
Date: 2021-12-15T14:24:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-15-large-scale-phishing-study-shows-who-bites-the-bait-more-often

[Source](https://www.bleepingcomputer.com/news/security/large-scale-phishing-study-shows-who-bites-the-bait-more-often/){:target="_blank" rel="noopener"}

> A large-scale phishing study involving 14,733 participants over a 15-month experiment has produced some surprising findings that contradict previous research results that formed the basis for popular industry practices. [...]
