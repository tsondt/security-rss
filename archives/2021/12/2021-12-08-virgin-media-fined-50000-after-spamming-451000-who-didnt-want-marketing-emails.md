Title: Virgin Media fined £50,000 after spamming 451,000 who didn't want marketing emails
Date: 2021-12-08T16:37:51+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-08-virgin-media-fined-50000-after-spamming-451000-who-didnt-want-marketing-emails

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/08/virgin_media_pecr_fine_415000_customers_spammed/){:target="_blank" rel="noopener"}

> Data watchdog shows it's keeping its PECR up British telco Virgin Media is facing a £50k financial penalty after spamming more than 400,000 opted-out customers urging them to sign back up to receive marketing bumf.... [...]
