Title: Of course a Bluetooth-using home COVID test was cracked to fake results
Date: 2021-12-22T03:58:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-22-of-course-a-bluetooth-using-home-covid-test-was-cracked-to-fake-results

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/22/ellume_home_covid_test_cracked/){:target="_blank" rel="noopener"}

> The Ellume COVID-19 Home Test was connected to the internet of woefully insecure things for a while Security vendor F-Secure has faked a COVID test result on a Bluetooth-equipped home COVID Test. Thankfully the vendor’s since fixed the device.... [...]
