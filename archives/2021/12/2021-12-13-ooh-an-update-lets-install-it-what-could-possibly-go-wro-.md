Title: Ooh, an update. Let's install it. What could possibly go wro-
Date: 2021-12-13T08:30:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-12-13-ooh-an-update-lets-install-it-what-could-possibly-go-wro-

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/13/who_me/){:target="_blank" rel="noopener"}

> Patching the patch Who, Me? Welcome to another Who, Me? confession from the Register readership, and a reminder of the unexpected side effects of software updates.... [...]
