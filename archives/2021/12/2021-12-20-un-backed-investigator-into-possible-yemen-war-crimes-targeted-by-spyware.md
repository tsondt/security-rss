Title: UN-backed investigator into possible Yemen war crimes targeted by spyware
Date: 2021-12-20T05:00:45+00:00
Author: Stephanie Kirchgaessner in Washington
Category: The Guardian
Tags: United Nations;World news;Yemen;Tunisia;Technology;Israel;Malware;Data and computer security;Surveillance
Slug: 2021-12-20-un-backed-investigator-into-possible-yemen-war-crimes-targeted-by-spyware

[Source](https://www.theguardian.com/world/2021/dec/20/un-backed-investigator-into-possible-yemen-war-crimes-targeted-by-spyware){:target="_blank" rel="noopener"}

> Exclusive: Analysis of Kamel Jendoubi’s mobile phone reveals he was targeted in August 2019 The mobile phone of a UN-backed investigator who was examining possible war crimes in Yemen was targeted with spyware made by Israel’s NSO Group, a new forensic analysis of the device has revealed. Kamel Jendoubi, a Tunisian who served as the chairman of the now defunct Group of Eminent Experts in Yemen (GEE)– a panel mandated by the UN to investigate possible war crimes – was targeted in August 2019, according to an analysis of his mobile phone by experts at Amnesty International and the Citizen [...]
