Title: NSO Group’s Pegasus Spyware Used Against US State Department Officials
Date: 2021-12-13T12:16:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberespionage;hacking;Israel;spyware
Slug: 2021-12-13-nso-groups-pegasus-spyware-used-against-us-state-department-officials

[Source](https://www.schneier.com/blog/archives/2021/12/nso-groups-pegasus-spyware-used-against-us-state-department-officials.html){:target="_blank" rel="noopener"}

> NSO Group’s descent into Internet pariah status continues. Its Pegasus spyware was used against nine US State Department employees. We don’t know which NSO Group customer trained the spyware on the US. But the company does: NSO Group said in a statement on Thursday that it did not have any indication their tools were used but canceled access for the relevant customers and would investigate based on the Reuters inquiry. “If our investigation shall show these actions indeed happened with NSO’s tools, such customer will be terminated permanently and legal actions will take place,” said an NSO spokesperson, who added [...]
