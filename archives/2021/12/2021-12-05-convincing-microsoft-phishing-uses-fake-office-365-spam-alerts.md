Title: Convincing Microsoft phishing uses fake Office 365 spam alerts
Date: 2021-12-05T11:07:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-12-05-convincing-microsoft-phishing-uses-fake-office-365-spam-alerts

[Source](https://www.bleepingcomputer.com/news/security/convincing-microsoft-phishing-uses-fake-office-365-spam-alerts/){:target="_blank" rel="noopener"}

> A persuasive and ongoing series of phishing attacks are using fake Office 365 notifications asking the recipients to review blocked spam messages, with the end goal of stealing their Microsoft credentials. [...]
