Title: QNAP NAS devices hit in surge of ech0raix ransomware attacks
Date: 2021-12-27T11:19:45-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-12-27-qnap-nas-devices-hit-in-surge-of-ech0raix-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/qnap-nas-devices-hit-in-surge-of-ech0raix-ransomware-attacks/){:target="_blank" rel="noopener"}

> Users of QNAP network-attached storage (NAS) devices are reporting attacks on their systems with the eCh0raix ransomware, also known as QNAPCrypt. [...]
