Title: Logistics giant warns of BEC emails following ransomware attack
Date: 2021-12-17T11:28:39-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-17-logistics-giant-warns-of-bec-emails-following-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/logistics-giant-warns-of-bec-emails-following-ransomware-attack/){:target="_blank" rel="noopener"}

> Hellmann Worldwide is warning customers of an increase in fraudulent calls and emails regarding payment transfer and bank account changes after a recent ransomware attack. [...]
