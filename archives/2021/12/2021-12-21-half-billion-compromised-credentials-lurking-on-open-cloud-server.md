Title: Half-Billion Compromised Credentials Lurking on Open Cloud Server
Date: 2021-12-21T20:08:42+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Web Security
Slug: 2021-12-21-half-billion-compromised-credentials-lurking-on-open-cloud-server

[Source](https://threatpost.com/half-billion-compromised-credentials-cloud-server/177202/){:target="_blank" rel="noopener"}

> A quarter-billion of those passwords were not seen in previous breaches that have been added to Have I Been Pwned. [...]
