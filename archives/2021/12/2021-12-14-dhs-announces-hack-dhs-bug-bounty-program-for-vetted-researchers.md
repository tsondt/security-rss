Title: DHS announces 'Hack DHS' bug bounty program for vetted researchers
Date: 2021-12-14T15:38:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-14-dhs-announces-hack-dhs-bug-bounty-program-for-vetted-researchers

[Source](https://www.bleepingcomputer.com/news/security/dhs-announces-hack-dhs-bug-bounty-program-for-vetted-researchers/){:target="_blank" rel="noopener"}

> The Department of Homeland Security (DHS) has launched a new bug bounty program dubbed "Hack DHS" that allows vetted cybersecurity researchers to find and report security vulnerabilities in external DHS systems. [...]
