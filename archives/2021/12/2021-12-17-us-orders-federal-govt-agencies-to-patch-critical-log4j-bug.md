Title: US orders federal govt agencies to patch critical Log4j bug
Date: 2021-12-17T12:35:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-17-us-orders-federal-govt-agencies-to-patch-critical-log4j-bug

[Source](https://www.bleepingcomputer.com/news/security/us-orders-federal-govt-agencies-to-patch-critical-log4j-bug/){:target="_blank" rel="noopener"}

> US Federal Civilian Executive Branch agencies have been ordered to patch the critical and actively exploited Log4Shell security vulnerability in the Apache Log4j library within the next six days. [...]
