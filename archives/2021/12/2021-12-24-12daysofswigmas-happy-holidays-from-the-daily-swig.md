Title: #12DaysofSwigmas – Happy Holidays from The Daily Swig
Date: 2021-12-24T15:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-24-12daysofswigmas-happy-holidays-from-the-daily-swig

[Source](https://portswigger.net/daily-swig/12daysofswigmas-happy-holidays-from-the-daily-swig){:target="_blank" rel="noopener"}

> On the 12th Day of Swigmas, The Daily Swig gave to me... [...]
