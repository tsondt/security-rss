Title: STOP Ransomware vaccine released to block encryption
Date: 2021-12-07T10:06:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-07-stop-ransomware-vaccine-released-to-block-encryption

[Source](https://www.bleepingcomputer.com/news/security/stop-ransomware-vaccine-released-to-block-encryption/){:target="_blank" rel="noopener"}

> German security software company G DATA has released a vaccine that will block STOP Ransomware from encrypting victims' files after infection. [...]
