Title: Canada Charges Its “Most Prolific Cybercriminal”
Date: 2021-12-08T23:27:40+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Ransomware;darkcloudowner;Darkode;DCReavers2;fbi;fubar;Iserdo;Matt Watson;Matthew Philbert;Ontario Provincial Police;Project CODA
Slug: 2021-12-08-canada-charges-its-most-prolific-cybercriminal

[Source](https://krebsonsecurity.com/2021/12/canada-charges-its-most-prolific-cybercriminal/){:target="_blank" rel="noopener"}

> A 31-year-old Canadian man has been arrested and charged with fraud in connection with numerous ransomware attacks against businesses, government agencies and private citizens throughout Canada and the United States. Canadian authorities describe him as “the most prolific cybercriminal we’ve identified in Canada,” but so far they’ve released few other details about the investigation or the defendant. Helpfully, an email address and nickname apparently connected to the accused offer some additional clues. Matthew Filbert, in 2016. Matthew Philbert of Ottawa, Ontario was charged with fraud and conspiracy in a joint law enforcement action by Canadian and U.S. authorities dubbed “Project [...]
