Title: All in One SEO Plugin Bug Threatens 3M Websites with Takeovers
Date: 2021-12-22T18:24:07+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-12-22-all-in-one-seo-plugin-bug-threatens-3m-websites-with-takeovers

[Source](https://threatpost.com/all-in-one-seo-plugin-bug-threatens-3m-wordpress-websites-takeovers/177240/){:target="_blank" rel="noopener"}

> A critical privilege-escalation vulnerability could lead to backdoors for admin access nesting in web servers. [...]
