Title: Software-Defined community cloud - a new way to “Government Cloud”
Date: 2021-12-09T17:00:00+00:00
Author: Jason Callaway
Category: GCP Security
Tags: Public Sector;Compliance;Google Cloud;Identity & Security
Slug: 2021-12-09-software-defined-community-cloud-a-new-way-to-government-cloud

[Source](https://cloud.google.com/blog/products/identity-security/software-defined-community-cloud-new-way-government-cloud/){:target="_blank" rel="noopener"}

> Google has a long history and deep commitment to innovation in the public sector and regulated markets including healthcare, financial services, and telecommunications, to name a few. Recently, we’ve made significant advances in our security and compliance offerings and capabilities in order to better enable government and government supply chain customers to adopt Google Cloud. Specifically, our Assured Workloads product implements a novel approach to help customers meet compliance and sovereignty requirements: a software-defined community cloud. What is a community cloud? A community cloud is defined by NIST SP 800-145 as: Cloud infrastructure [that] is provisioned for exclusive use by [...]
