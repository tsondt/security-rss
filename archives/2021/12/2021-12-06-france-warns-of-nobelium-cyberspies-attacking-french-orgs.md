Title: France warns of Nobelium cyberspies attacking French orgs
Date: 2021-12-06T13:46:47-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-06-france-warns-of-nobelium-cyberspies-attacking-french-orgs

[Source](https://www.bleepingcomputer.com/news/security/france-warns-of-nobelium-cyberspies-attacking-french-orgs/){:target="_blank" rel="noopener"}

> The French national cyber-security agency ANSSI said today that the Russian-backed Nobelium hacking group behind last year's SolarWinds hack has been targeting French organizations since February 2021. [...]
