Title: When Scammers Get Scammed, They Take It to Cybercrime Court
Date: 2021-12-07T20:01:45+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Hacks;Malware;Web Security
Slug: 2021-12-07-when-scammers-get-scammed-they-take-it-to-cybercrime-court

[Source](https://threatpost.com/scammers-cybercrime-court/176834/){:target="_blank" rel="noopener"}

> Underground arbitration system settles disputes between cybercriminals. [...]
