Title: Emotet starts dropping Cobalt Strike again for faster attacks
Date: 2021-12-15T16:59:27-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-15-emotet-starts-dropping-cobalt-strike-again-for-faster-attacks

[Source](https://www.bleepingcomputer.com/news/security/emotet-starts-dropping-cobalt-strike-again-for-faster-attacks/){:target="_blank" rel="noopener"}

> Right in time for the holidays, the notorious Emotet malware is once again directly installing Cobalt Strike beacons for rapid cyberattacks. [...]
