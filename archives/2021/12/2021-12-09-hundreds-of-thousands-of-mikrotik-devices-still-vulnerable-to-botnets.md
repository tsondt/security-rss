Title: Hundreds of thousands of MikroTik devices still vulnerable to botnets
Date: 2021-12-09T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2021-12-09-hundreds-of-thousands-of-mikrotik-devices-still-vulnerable-to-botnets

[Source](https://www.bleepingcomputer.com/news/security/hundreds-of-thousands-of-mikrotik-devices-still-vulnerable-to-botnets/){:target="_blank" rel="noopener"}

> Approximately 300,000 MikroTik routers are vulnerable to critical vulnerabilities that malware botnets can exploit for cryptomining and DDoS attacks. [...]
