Title: What’s the right amount of trust to build into your network? Less than Zero
Date: 2021-12-08T07:30:08+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: 2021-12-08-whats-the-right-amount-of-trust-to-build-into-your-network-less-than-zero

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/08/iomart_zero_trust/){:target="_blank" rel="noopener"}

> It’s tricky but manageable, says Iomart Paid Feature “The trust of the innocent is the liar's most useful tool,” Stephen King wrote. At least that’s what the internet claims.... [...]
