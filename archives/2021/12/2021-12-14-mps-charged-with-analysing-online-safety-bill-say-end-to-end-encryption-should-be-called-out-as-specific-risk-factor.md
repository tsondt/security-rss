Title: MPs charged with analysing Online Safety Bill say end-to-end encryption should be called out as 'specific risk factor'
Date: 2021-12-14T16:00:40+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-14-mps-charged-with-analysing-online-safety-bill-say-end-to-end-encryption-should-be-called-out-as-specific-risk-factor

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/14/online_safety_bill_report/){:target="_blank" rel="noopener"}

> Too far? Committee thinks it doesn't go far enough Britain's Online Safety Bill is being enthusiastically endorsed in a "manifesto" issued today by MPs who were tasked with scrutinising its controversial contents.... [...]
