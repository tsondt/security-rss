Title: All Log4j, logback bugs we know so far, and why you MUST ditch 2.15
Date: 2021-12-17T07:20:07-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-12-17-all-log4j-logback-bugs-we-know-so-far-and-why-you-must-ditch-215

[Source](https://www.bleepingcomputer.com/news/security/all-log4j-logback-bugs-we-know-so-far-and-why-you-must-ditch-215/){:target="_blank" rel="noopener"}

> Everyone's heard of the critical log4j zero-day by now. Dubbed 'Log4Shell' and 'Logjam,' the vulnerability has set the internet on fire. Below we summarize the four or more CVEs identified thus far, and pretty good reasons to ditch log4j version 2.15.0 for 2.16.0. [...]
