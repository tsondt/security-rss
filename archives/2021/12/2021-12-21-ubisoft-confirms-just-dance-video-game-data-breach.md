Title: Ubisoft confirms Just Dance video game data breach
Date: 2021-12-21T13:56:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-21-ubisoft-confirms-just-dance-video-game-data-breach

[Source](https://portswigger.net/daily-swig/ubisoft-confirms-just-dance-video-game-data-breach){:target="_blank" rel="noopener"}

> Developer said no accounts had been improperly accessed [...]
