Title: LastPass users warned their master passwords are compromised
Date: 2021-12-28T12:27:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-28-lastpass-users-warned-their-master-passwords-are-compromised

[Source](https://www.bleepingcomputer.com/news/security/lastpass-users-warned-their-master-passwords-are-compromised/){:target="_blank" rel="noopener"}

> Many LastPass users report that their master passwords have been compromised after receiving email warnings that someone tried to use them to log into their accounts from unknown locations. [...]
