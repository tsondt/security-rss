Title: Former Ubiquiti dev charged for trying to extort his employer
Date: 2021-12-01T18:03:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-01-former-ubiquiti-dev-charged-for-trying-to-extort-his-employer

[Source](https://www.bleepingcomputer.com/news/security/former-ubiquiti-dev-charged-for-trying-to-extort-his-employer/){:target="_blank" rel="noopener"}

> Nickolas Sharp, a former employee of networking device maker Ubiquiti, was arrested and charged today with data theft and attempting to extort his employer while posing as a whistleblower and an anonymous hacker. [...]
