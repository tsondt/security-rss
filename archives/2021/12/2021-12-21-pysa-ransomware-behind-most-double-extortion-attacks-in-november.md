Title: PYSA ransomware behind most double extortion attacks in November
Date: 2021-12-21T17:37:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-21-pysa-ransomware-behind-most-double-extortion-attacks-in-november

[Source](https://www.bleepingcomputer.com/news/security/pysa-ransomware-behind-most-double-extortion-attacks-in-november/){:target="_blank" rel="noopener"}

> Security analysts from NCC Group report that ransomware attacks in November 2021 increased over the past month, with double-extortion continuing to be a powerful tool in threat actors' arsenal. [...]
