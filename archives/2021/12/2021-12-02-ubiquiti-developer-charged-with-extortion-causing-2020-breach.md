Title: Ubiquiti Developer Charged With Extortion, Causing 2020 “Breach”
Date: 2021-12-02T16:11:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Nickolas Sharp;Surfshark;Ubiquiti
Slug: 2021-12-02-ubiquiti-developer-charged-with-extortion-causing-2020-breach

[Source](https://krebsonsecurity.com/2021/12/ubiquiti-developer-charged-with-extortion-causing-2020-breach/){:target="_blank" rel="noopener"}

> In January 2021, technology vendor Ubiquiti Inc. [NYSE:UI] disclosed that a breach at a third party cloud provider had exposed customer account credentials. In March, a Ubiquiti employee warned that the company had drastically understated the scope of the incident, and that the third-party cloud provider claim was a fabrication. On Wednesday, a former Ubiquiti developer was arrested and charged with stealing data and trying to extort his employer while pretending to be a whistleblower. Federal prosecutors say Nickolas Sharp, a senior developer at Ubiquiti, actually caused the “breach” that forced Ubiquiti to disclose a cybersecurity incident in January. They [...]
