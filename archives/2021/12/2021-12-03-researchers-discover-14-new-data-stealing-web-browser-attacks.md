Title: Researchers discover 14 new data-stealing web browser attacks
Date: 2021-12-03T10:34:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-03-researchers-discover-14-new-data-stealing-web-browser-attacks

[Source](https://www.bleepingcomputer.com/news/security/researchers-discover-14-new-data-stealing-web-browser-attacks/){:target="_blank" rel="noopener"}

> IT security researchers from Ruhr-Universität Bochum (RUB) and the Niederrhein University of Applied Sciences have discovered 14 new types of 'XS-Leak' cross-site leak attacks against modern web browsers, including Google Chrome, Microsoft Edge, Safari, and Mozilla Firefox. [...]
