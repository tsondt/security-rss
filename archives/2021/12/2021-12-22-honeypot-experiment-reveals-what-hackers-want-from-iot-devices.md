Title: Honeypot experiment reveals what hackers want from IoT devices
Date: 2021-12-22T16:46:05-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-22-honeypot-experiment-reveals-what-hackers-want-from-iot-devices

[Source](https://www.bleepingcomputer.com/news/security/honeypot-experiment-reveals-what-hackers-want-from-iot-devices/){:target="_blank" rel="noopener"}

> ​A three-year-long honeypot experiment featuring simulated low-interaction IoT devices of various types and locations gives a clear idea of why actors target specific devices. [...]
