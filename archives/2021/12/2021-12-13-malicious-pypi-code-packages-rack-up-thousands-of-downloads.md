Title: Malicious PyPI Code Packages Rack Up Thousands of Downloads
Date: 2021-12-13T18:46:34+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware
Slug: 2021-12-13-malicious-pypi-code-packages-rack-up-thousands-of-downloads

[Source](https://threatpost.com/malicious-pypi-code-packages/176971/){:target="_blank" rel="noopener"}

> The Python code repository was infiltrated by malware bent on data exfiltration from developer apps and more. [...]
