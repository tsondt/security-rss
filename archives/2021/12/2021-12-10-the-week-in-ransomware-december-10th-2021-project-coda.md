Title: The Week in Ransomware - December 10th 2021 - Project CODA
Date: 2021-12-10T19:37:35-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-10-the-week-in-ransomware-december-10th-2021-project-coda

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-10th-2021-project-coda/){:target="_blank" rel="noopener"}

> This week has quite a bit of ransomware news, including arrests, a new and sophisticated ransomware, and an attack bringing down 300 supermarkets in England. [...]
