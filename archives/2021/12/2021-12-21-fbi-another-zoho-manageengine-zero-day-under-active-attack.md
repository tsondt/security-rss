Title: FBI: Another Zoho ManageEngine Zero-Day Under Active Attack
Date: 2021-12-21T14:42:02+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-12-21-fbi-another-zoho-manageengine-zero-day-under-active-attack

[Source](https://threatpost.com/zoho-zero-day-manageengine-active-attack/177178/){:target="_blank" rel="noopener"}

> APT attackers are using a security vulnerability in ManageEngine Desktop Central to take over servers, deliver malware and establish network persistence. [...]
