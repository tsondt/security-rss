Title: Continuous runtime security monitoring with AWS Security Hub and Falco
Date: 2021-12-17T17:35:38+00:00
Author: Rajarshi Das
Category: AWS Security
Tags: Advanced (300);Best Practices;Security;Amazon ECS;Amazon EKS;AWS Security Hub;Containers;DevSecOps;Falco;Integration;Kubernetes;Runtime Security;Security Blog;Vulnerability management
Slug: 2021-12-17-continuous-runtime-security-monitoring-with-aws-security-hub-and-falco

[Source](https://aws.amazon.com/blogs/security/continuous-runtime-security-monitoring-with-aws-security-hub-and-falco/){:target="_blank" rel="noopener"}

> Customers want a single and comprehensive view of the security posture of their workloads. Runtime security event monitoring is important to building secure, operationally excellent, and reliable workloads, especially in environments that run containers and container orchestration platforms. In this blog post, we show you how to use services such as AWS Security Hub and [...]
