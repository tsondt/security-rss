Title: Feds charge two men with claiming ownership of others' songs to steal YouTube royalty payments
Date: 2021-12-03T21:54:30+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-12-03-feds-charge-two-men-with-claiming-ownership-of-others-songs-to-steal-youtube-royalty-payments

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/03/feds_youtube_theft/){:target="_blank" rel="noopener"}

> Alleged scheme said to have netted $20m since 2017 The US Attorney's Office of Arizona on Wednesday announced the indictment of two men on charges that they defrauded musicians and associated companies by claiming more than $20m in royalty payments for songs played on YouTube.... [...]
