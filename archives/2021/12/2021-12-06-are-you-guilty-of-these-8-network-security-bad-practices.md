Title: Are You Guilty of These 8 Network-Security Bad Practices?
Date: 2021-12-06T21:47:45+00:00
Author: Tony Lauro
Category: Threatpost
Tags: Breach;InfoSec Insider
Slug: 2021-12-06-are-you-guilty-of-these-8-network-security-bad-practices

[Source](https://threatpost.com/bad-practices-network-security/176798/){:target="_blank" rel="noopener"}

> Tony Lauro, director of Security Technology & Strategy at Akamai, discusses VPNs, RDP, flat networks, BYOD and other network-security bugbears. [...]
