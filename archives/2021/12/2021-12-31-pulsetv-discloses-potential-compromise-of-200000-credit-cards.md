Title: PulseTV discloses potential compromise of 200,000 credit cards
Date: 2021-12-31T12:35:50-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-31-pulsetv-discloses-potential-compromise-of-200000-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/pulsetv-discloses-potential-compromise-of-200-000-credit-cards/){:target="_blank" rel="noopener"}

> PulseTV (pulsetv.com), an American e-store that uses TV as a medium to reach customers, has disclosed a large-scale customer credit card compromise. [...]
