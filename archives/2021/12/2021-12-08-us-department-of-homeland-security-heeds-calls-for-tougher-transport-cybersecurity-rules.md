Title: <span>US Department of Homeland Security heeds calls for tougher transport cybersecurity rules</span>
Date: 2021-12-08T16:23:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-08-us-department-of-homeland-security-heeds-calls-for-tougher-transport-cybersecurity-rules

[Source](https://portswigger.net/daily-swig/us-department-of-homeland-security-heeds-calls-for-tougher-transport-cybersecurity-rules){:target="_blank" rel="noopener"}

> TSA issues mandatory requirements for ‘high-risk’ rail infrastructure [...]
