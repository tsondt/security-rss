Title: Cryptominers aren't just a headache – they're a big neon sign that Bad Things are on your network
Date: 2021-12-07T16:14:14+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-07-cryptominers-arent-just-a-headache-theyre-a-big-neon-sign-that-bad-things-are-on-your-network

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/07/sophos_tor2mine_research_cryptominer_warning/){:target="_blank" rel="noopener"}

> So says Sophos in warning about Tor2Mine Monero malware Cryptominer malware removal is a routine piece of the cybersecurity landscape these days. Yet if criminals are hijacking your compute cycles to mine cryptocurrencies, chances are there's something worse lurking on your network too.... [...]
