Title: AvosLocker ransomware reboots in Safe Mode to bypass security tools
Date: 2021-12-23T12:47:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-23-avoslocker-ransomware-reboots-in-safe-mode-to-bypass-security-tools

[Source](https://www.bleepingcomputer.com/news/security/avoslocker-ransomware-reboots-in-safe-mode-to-bypass-security-tools/){:target="_blank" rel="noopener"}

> Recent AvosLocker ransomware attacks are characterized by a focus on disabling endpoint security solutions that stand in the way of threat actors. [...]
