Title: US State Dept employees’ phones hacked using NSO spyware
Date: 2021-12-03T12:55:33-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-03-us-state-dept-employees-phones-hacked-using-nso-spyware

[Source](https://www.bleepingcomputer.com/news/security/us-state-dept-employees-phones-hacked-using-nso-spyware/){:target="_blank" rel="noopener"}

> Apple has warned US Department of State employees that their iPhones have been hacked by unknown attackers using an iOS exploit dubbed ForcedEntry to deploy Pegasus spyware developed by Israeli surveillance firm NSO Group. [...]
