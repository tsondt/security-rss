Title: Microsoft Defender Log4j scanner triggers false positive alerts
Date: 2021-12-29T09:15:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-12-29-microsoft-defender-log4j-scanner-triggers-false-positive-alerts

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-log4j-scanner-triggers-false-positive-alerts/){:target="_blank" rel="noopener"}

> Microsoft Defender for Endpoint is currently showing "sensor tampering" alerts linked to the company's newly deployed Microsoft 365 Defender scanner for Log4j processes. [...]
