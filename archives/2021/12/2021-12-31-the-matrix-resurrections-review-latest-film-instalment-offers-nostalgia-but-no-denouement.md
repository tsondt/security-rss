Title: The Matrix Resurrections review: Latest film instalment offers nostalgia but no denouement
Date: 2021-12-31T15:23:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-31-the-matrix-resurrections-review-latest-film-instalment-offers-nostalgia-but-no-denouement

[Source](https://portswigger.net/daily-swig/the-matrix-resurrections-review-latest-film-instalment-offers-nostalgia-but-no-denouement){:target="_blank" rel="noopener"}

> Déjà vu isn't what it used to be [...]
