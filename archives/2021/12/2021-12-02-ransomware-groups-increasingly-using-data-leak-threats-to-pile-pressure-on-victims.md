Title: Ransomware groups increasingly using data leak threats to pile pressure on victims
Date: 2021-12-02T16:58:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-02-ransomware-groups-increasingly-using-data-leak-threats-to-pile-pressure-on-victims

[Source](https://portswigger.net/daily-swig/ransomware-groups-increasingly-using-data-leak-threats-to-pile-pressure-on-victims){:target="_blank" rel="noopener"}

> Nearly one in three victims succumb to extortion, estimates Group-IB [...]
