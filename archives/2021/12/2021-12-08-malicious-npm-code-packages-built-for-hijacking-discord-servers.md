Title: Malicious npm Code Packages Built for Hijacking Discord Servers
Date: 2021-12-08T22:30:04+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-12-08-malicious-npm-code-packages-built-for-hijacking-discord-servers

[Source](https://threatpost.com/malicious-npm-code-packages-discord/176886/){:target="_blank" rel="noopener"}

> The lurking code-bombs lift Discord tokens from users of any applications that pulled the packages into their code bases. [...]
