Title: Inside Ireland’s Public Healthcare Ransomware Scare
Date: 2021-12-14T02:13:34+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ransomware;Conti ransomware;H-ISAC;Health Service Executive;HSE;Ireland;PriceWaterhouseCoopers;ransomware;Ryuk
Slug: 2021-12-14-inside-irelands-public-healthcare-ransomware-scare

[Source](https://krebsonsecurity.com/2021/12/inside-irelands-public-healthcare-ransomware-scare/){:target="_blank" rel="noopener"}

> The consulting firm PricewaterhouseCoopers recently published lessons learned from the disruptive and costly ransomware attack in May 2021 on Ireland’s public health system. The unusually candid post-mortem found that nearly two months elapsed between the initial intrusion and the launching of the ransomware. It also found affected hospitals had tens of thousands of outdated Windows 7 systems, and that the health system’s IT administrators failed to respond to multiple warning signs that a massive attack was imminent. PWC’s timeline of the days leading up to the deployment of Conti ransomware on May 14. Ireland’s Health Service Executive (HSE), which operates [...]
