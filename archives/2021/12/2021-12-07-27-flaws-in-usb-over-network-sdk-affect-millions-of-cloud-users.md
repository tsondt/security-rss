Title: 27 flaws in USB-over-network SDK affect millions of cloud users
Date: 2021-12-07T10:15:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-07-27-flaws-in-usb-over-network-sdk-affect-millions-of-cloud-users

[Source](https://www.bleepingcomputer.com/news/security/27-flaws-in-usb-over-network-sdk-affect-millions-of-cloud-users/){:target="_blank" rel="noopener"}

> Researchers have discovered 27 vulnerabilities in Eltima SDK, a library used by numerous cloud providers to remotely mount a local USB device. [...]
