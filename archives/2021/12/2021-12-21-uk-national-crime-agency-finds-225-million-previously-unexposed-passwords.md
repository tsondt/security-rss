Title: UK National Crime Agency finds 225 million previously unexposed passwords
Date: 2021-12-21T07:10:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-21-uk-national-crime-agency-finds-225-million-previously-unexposed-passwords

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/21/nca_finds_255m_fresh_stolen_passwords/){:target="_blank" rel="noopener"}

> Shares them with Troy Hunt’s Have I Been Pwned after sweeping them up from ‘compromised cloud storage’ The United Kingdom’s National Crime Agency and National Cyber Crime Unit have uncovered a colossal trove of stolen passwords.... [...]
