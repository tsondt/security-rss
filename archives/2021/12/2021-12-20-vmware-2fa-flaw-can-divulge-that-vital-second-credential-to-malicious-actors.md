Title: VMware 2FA flaw can divulge that vital second credential to malicious actors
Date: 2021-12-20T07:02:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-12-20-vmware-2fa-flaw-can-divulge-that-vital-second-credential-to-malicious-actors

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/20/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Deep dive into the NSO Group's zero-click exploit and 'Hack the DHS!' In Brief VMware has warned users a flaw in its VMware Verify two-factor authentication product could allow a malicious actor with a first-factor authentication credential to obtain a second factor from its VMware Verify product.... [...]
