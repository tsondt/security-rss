Title: Revealed: Remember the Sony rootkit rumpus? It was almost oh so much worse
Date: 2021-12-10T14:02:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-12-10-revealed-remember-the-sony-rootkit-rumpus-it-was-almost-oh-so-much-worse

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/10/autorunning_away/){:target="_blank" rel="noopener"}

> That time Rootkitting for Dummies might as well have been in Microsoft's Plus! Pack Retired Microsoft engineer, Dave Plummer, offered a blast from the past last week with a look back at the infamous Sony Windows "rootkit" scandal.... [...]
