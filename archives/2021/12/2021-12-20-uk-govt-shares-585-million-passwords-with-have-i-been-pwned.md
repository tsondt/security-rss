Title: UK govt shares 585 million passwords with Have I Been Pwned
Date: 2021-12-20T12:49:11-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-12-20-uk-govt-shares-585-million-passwords-with-have-i-been-pwned

[Source](https://www.bleepingcomputer.com/news/security/uk-govt-shares-585-million-passwords-with-have-i-been-pwned/){:target="_blank" rel="noopener"}

> The United Kingdom's National Crime Agency has contributed more than 585 million passwords to the Have I Been Pwned service that lets users check if their login information has leaked online. [...]
