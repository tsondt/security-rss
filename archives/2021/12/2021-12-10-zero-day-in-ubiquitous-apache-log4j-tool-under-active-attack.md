Title: Zero Day in Ubiquitous Apache Log4j Tool Under Active Attack
Date: 2021-12-10T17:58:04+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Government;Hacks;News;Vulnerabilities;Web Security
Slug: 2021-12-10-zero-day-in-ubiquitous-apache-log4j-tool-under-active-attack

[Source](https://threatpost.com/zero-day-in-ubiquitous-apache-log4j-tool-under-active-attack/176937/){:target="_blank" rel="noopener"}

> The Log4Shell vulnerability critically threatens anybody using the popular open-source Apache Struts framework and could lead to a “Mini internet meltdown soonish.” [...]
