Title: Netgear leaves vulnerabilities unpatched in Nighthawk router
Date: 2021-12-31T07:15:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-31-netgear-leaves-vulnerabilities-unpatched-in-nighthawk-router

[Source](https://www.bleepingcomputer.com/news/security/netgear-leaves-vulnerabilities-unpatched-in-nighthawk-router/){:target="_blank" rel="noopener"}

> Researchers have found half a dozen high-risk vulnerabilities in the latest firmware version for the Netgear Nighthawk R6700v3 router. At publishing time the flaws remain unpatched. [...]
