Title: Friday Squid Blogging: Squid-Headed Statue Appears in Dallas
Date: 2021-12-24T22:17:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-12-24-friday-squid-blogging-squid-headed-statue-appears-in-dallas

[Source](https://www.schneier.com/blog/archives/2021/12/friday-squid-blogging-squid-headed-statue-appears-in-dallas.html){:target="_blank" rel="noopener"}

> Someone left it in a cemetery. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
