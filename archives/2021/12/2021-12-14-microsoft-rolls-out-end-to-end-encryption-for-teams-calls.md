Title: Microsoft rolls out end-to-end encryption for Teams calls
Date: 2021-12-14T13:01:29-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-12-14-microsoft-rolls-out-end-to-end-encryption-for-teams-calls

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-rolls-out-end-to-end-encryption-for-teams-calls/){:target="_blank" rel="noopener"}

> Microsoft announced today the general availability of end-to-end encryption (E2EE) support for one-to-one Microsoft Teams calls. [...]
