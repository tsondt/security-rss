Title: Compliance Engineering - Continuous Compliance GCP case studies
Date: 2021-12-15T17:00:00+00:00
Author: Jo Hellwig
Category: GCP Security
Tags: Identity & Security;Compliance
Slug: 2021-12-15-compliance-engineering-continuous-compliance-gcp-case-studies

[Source](https://cloud.google.com/blog/products/compliance/continuous-compliance-engineering-gcp-case-studies/){:target="_blank" rel="noopener"}

> Our previous article provided tools and techniques to transform your productionalization process and make it ready for Cloud workloads. In this post, we will cover technical examples of GCP controls and how it can help your organization maintain your security and compliance posture in GCP. In comparison to on-prem infrastructure, GCP is a highly integrated environment and provides out of the box capabilities to evidence a large variety of controls. The following cornerstones build the foundation of an effective control attestation: Inventory Management - On-prem workloads frequently have discovery tools installed to understand what infrastructure components are actually deployed in [...]
