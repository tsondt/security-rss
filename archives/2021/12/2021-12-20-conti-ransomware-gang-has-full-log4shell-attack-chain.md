Title: Conti Ransomware Gang Has Full Log4Shell Attack Chain
Date: 2021-12-20T22:11:30+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-12-20-conti-ransomware-gang-has-full-log4shell-attack-chain

[Source](https://threatpost.com/conti-ransomware-gang-has-full-log4shell-attack-chain/177173/){:target="_blank" rel="noopener"}

> Conti has become the first professional-grade, sophisticated ransomware group to weaponize Log4j2, now with a full attack chain. [...]
