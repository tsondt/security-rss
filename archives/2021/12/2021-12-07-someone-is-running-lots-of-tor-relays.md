Title: Someone Is Running Lots of Tor Relays
Date: 2021-12-07T12:25:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;de-anonymization;privacy;Tor
Slug: 2021-12-07-someone-is-running-lots-of-tor-relays

[Source](https://www.schneier.com/blog/archives/2021/12/someone-is-running-lots-of-tor-relays.html){:target="_blank" rel="noopener"}

> Since 2017, someone is running about a thousand — 10% of the total — Tor servers in an attempt to deanonymize the network: Grouping these servers under the KAX17 umbrella, Nusenu says this threat actor has constantly added servers with no contact details to the Tor network in industrial quantities, operating servers in the realm of hundreds at any given point. The actor’s servers are typically located in data centers spread all over the world and are typically configured as entry and middle points primarily, although KAX17 also operates a small number of exit points. Nusenu said this is strange [...]
