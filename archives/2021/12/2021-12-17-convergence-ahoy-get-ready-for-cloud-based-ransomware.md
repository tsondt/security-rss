Title: Convergence Ahoy: Get Ready for Cloud-Based Ransomware
Date: 2021-12-17T15:45:36+00:00
Author: Oliver Tavakoli
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Malware
Slug: 2021-12-17-convergence-ahoy-get-ready-for-cloud-based-ransomware

[Source](https://threatpost.com/cloud-ransomware-convergence/177112/){:target="_blank" rel="noopener"}

> Oliver Tavakoli, CTO at Vectra AI, takes us inside the coming nexus of ransomware, supply-chain attacks and cloud deployments. [...]
