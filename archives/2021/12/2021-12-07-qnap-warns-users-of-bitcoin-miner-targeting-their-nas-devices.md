Title: QNAP warns users of bitcoin miner targeting their NAS devices
Date: 2021-12-07T08:53:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-12-07-qnap-warns-users-of-bitcoin-miner-targeting-their-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-users-of-bitcoin-miner-targeting-their-nas-devices/){:target="_blank" rel="noopener"}

> QNAP warned customers today of ongoing attacks targeting their NAS (network-attached storage) devices with cryptomining malware, urging them to take measures to protect them immediately. [...]
