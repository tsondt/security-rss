Title: Where the Latest Log4Shell Attacks Are Coming From
Date: 2021-12-13T19:00:01+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-12-13-where-the-latest-log4shell-attacks-are-coming-from

[Source](https://threatpost.com/log4shell-attacks-origin-botnet/176977/){:target="_blank" rel="noopener"}

> Analysts find at least 10 Linux botnets actively exploiting Log4Shell flaw. [...]
