Title: 80K Retail WooCommerce Sites Exposed by Plugin XSS Bug
Date: 2021-12-01T19:34:53+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-12-01-80k-retail-woocommerce-sites-exposed-by-plugin-xss-bug

[Source](https://threatpost.com/retail-woocommerce-sites-plugin-xss-bug/176704/){:target="_blank" rel="noopener"}

> The Variation Swatches plugin security flaw lets attackers with low-level permissions tweak important settings on e-commerce sites to inject malicious scripts. [...]
