Title: UK watchdog's punishment for Blackbaud, Easyjet, other big privacy lawbreakers was slap on the wrist in private
Date: 2021-12-01T07:28:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-01-uk-watchdogs-punishment-for-blackbaud-easyjet-other-big-privacy-lawbreakers-was-slap-on-the-wrist-in-private

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/01/ico_reprimands_large_organisations/){:target="_blank" rel="noopener"}

> Is this what they call light-touch regulation? Blackbaud was given a private slap on the wrist by the UK's Information Commissioner's Office (ICO) after paying off criminals who stole users' financial data from the cloud CRM biz's servers.... [...]
