Title: 5 Cybersecurity Trends to Watch in 2022
Date: 2021-12-29T13:00:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Government;IoT;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2021-12-29-5-cybersecurity-trends-to-watch-in-2022

[Source](https://threatpost.com/5-cybersecurity-trends-2022/177273/){:target="_blank" rel="noopener"}

> Here’s what cybersecurity watchers want infosec pros to know heading into 2022. [...]
