Title: When disaster strikes, data recovery really is a race against time
Date: 2021-12-13T18:00:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-12-13-when-disaster-strikes-data-recovery-really-is-a-race-against-time

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/13/race_for_data_recovery/){:target="_blank" rel="noopener"}

> But exactly how much time are we talking about? Webinar When it comes to recovering after a catastrophic event such as a ransomware attack or data center failure, time is necessarily of the essence.... [...]
