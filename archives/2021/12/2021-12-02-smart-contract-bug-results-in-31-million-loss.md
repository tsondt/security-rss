Title: Smart Contract Bug Results in $31 Million Loss
Date: 2021-12-02T14:32:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2021-12-02-smart-contract-bug-results-in-31-million-loss

[Source](https://www.schneier.com/blog/archives/2021/12/smart-contract-bug-results-in-31-million-loss.html){:target="_blank" rel="noopener"}

> A hacker stole $31 million from the blockchain company MonoX Finance, by exploiting a bug in software the service uses to draft smart contracts. Specifically, the hack used the same token as both the tokenIn and tokenOut, which are methods for exchanging the value of one token for another. MonoX updates prices after each swap by calculating new prices for both tokens. When the swap is completed, the price of tokenIn­that is, the token sent by the user­decreases and the price of tokenOut­or the token received by the user­increases. By using the same token for both tokenIn and tokenOut, the [...]
