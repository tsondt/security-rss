Title: US bags Russian accused of bagging millions after stealing pre-release financial filings
Date: 2021-12-20T22:23:45+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-12-20-us-bags-russian-accused-of-bagging-millions-after-stealing-pre-release-financial-filings

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/20/russian_insider_trading/){:target="_blank" rel="noopener"}

> Swiss cough up accused crim while Russia is 'deeply disappointed' The US Attorney's Office of Massachusetts on Monday announced the extradition of Vladislav Klyushin, a Russian business executive with ties to the Kremlin, on charges of hacking US computer networks and committing securities fraud by trading on undisclosed financial data.... [...]
