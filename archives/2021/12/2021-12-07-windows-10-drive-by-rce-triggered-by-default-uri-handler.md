Title: Windows 10 Drive-By RCE Triggered by Default URI Handler
Date: 2021-12-07T20:24:02+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: 2021-12-07-windows-10-drive-by-rce-triggered-by-default-uri-handler

[Source](https://threatpost.com/windows-10-rce-url-handler/176830/){:target="_blank" rel="noopener"}

> There's an argument injection weakness in the Windows 10/11 default handler, researchers said: an issue that Microsoft has only partially fixed. [...]
