Title: Android banking trojan spreads via fake Google Play Store page
Date: 2021-12-24T10:27:54-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-12-24-android-banking-trojan-spreads-via-fake-google-play-store-page

[Source](https://www.bleepingcomputer.com/news/security/android-banking-trojan-spreads-via-fake-google-play-store-page/){:target="_blank" rel="noopener"}

> An Android banking trojan targeting Itaú Unibanco, a large financial services provider in Brazil with 55 million customers globally, is using a fake Google Play store to spread to devices. [...]
