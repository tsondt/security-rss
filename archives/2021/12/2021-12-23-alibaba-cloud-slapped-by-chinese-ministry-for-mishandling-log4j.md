Title: Alibaba Cloud slapped by Chinese ministry for mishandling Log4j
Date: 2021-12-23T05:58:04+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-12-23-alibaba-cloud-slapped-by-chinese-ministry-for-mishandling-log4j

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/23/alibaba_cloud_in_trouble_with/){:target="_blank" rel="noopener"}

> Beijing's not saying what cloudy contender did wrong China's Ministry of Industry and Information Technology has suspended Alibaba Cloud's membership of an influential security board to protest its handling of the Log4j flaw.... [...]
