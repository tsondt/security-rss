Title: Sites hacked with credit card stealers undetected for months
Date: 2021-12-15T10:28:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-15-sites-hacked-with-credit-card-stealers-undetected-for-months

[Source](https://www.bleepingcomputer.com/news/security/sites-hacked-with-credit-card-stealers-undetected-for-months/){:target="_blank" rel="noopener"}

> Threat actors are gearing up for the holidays with credit card skimming attacks remaining undetected for months as payment information is stolen from customers. [...]
