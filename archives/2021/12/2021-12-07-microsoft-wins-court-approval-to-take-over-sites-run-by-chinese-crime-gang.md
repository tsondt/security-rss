Title: Microsoft wins court approval to take over sites run by Chinese crime gang
Date: 2021-12-07T05:31:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-07-microsoft-wins-court-approval-to-take-over-sites-run-by-chinese-crime-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/07/microsoft_nickel_takeover/){:target="_blank" rel="noopener"}

> 'Nickel' back in trouble for trying to lift secrets, often by exploiting Microsoft snafus Microsoft has revealed its Digital Crimes Unit (DCU) won court approval to take control of websites a Chinese gang was using to attack targets across the world – often by exploiting vulnerabilities in Microsoft products.... [...]
