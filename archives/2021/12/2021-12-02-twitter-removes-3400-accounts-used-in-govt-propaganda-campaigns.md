Title: Twitter removes 3,400 accounts used in govt propaganda campaigns
Date: 2021-12-02T15:28:25-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-12-02-twitter-removes-3400-accounts-used-in-govt-propaganda-campaigns

[Source](https://www.bleepingcomputer.com/news/security/twitter-removes-3-400-accounts-used-in-govt-propaganda-campaigns/){:target="_blank" rel="noopener"}

> Twitter today announced the permanent removal of more than 3,400 accounts linked to governments of six countries running manipulation or spam campaigns. [...]
