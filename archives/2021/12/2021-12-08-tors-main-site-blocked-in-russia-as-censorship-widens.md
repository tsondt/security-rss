Title: Tor’s main site blocked in Russia as censorship widens
Date: 2021-12-08T08:57:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-12-08-tors-main-site-blocked-in-russia-as-censorship-widens

[Source](https://www.bleepingcomputer.com/news/security/tor-s-main-site-blocked-in-russia-as-censorship-widens/){:target="_blank" rel="noopener"}

> The Tor Project's main website, torproject.org, is actively blocked by Russia's largest internet service providers, and sources from the country claim that the government is getting ready to conduct an extensive block of the project. [...]
