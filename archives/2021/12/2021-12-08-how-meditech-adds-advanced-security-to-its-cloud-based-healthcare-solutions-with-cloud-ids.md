Title: How MEDITECH adds advanced security to its cloud-based healthcare solutions with Cloud IDS
Date: 2021-12-08T16:55:00+00:00
Author: Tom Moriarty
Category: GCP Security
Tags: Networking;Google Cloud;Identity & Security
Slug: 2021-12-08-how-meditech-adds-advanced-security-to-its-cloud-based-healthcare-solutions-with-cloud-ids

[Source](https://cloud.google.com/blog/products/identity-security/how-meditech-uses-google-cloud-ids/){:target="_blank" rel="noopener"}

> MEDITECH develops electronic health record (EHR) systems solutions that enhance the interactions of physicians and clinicians with patients. The company empowers healthcare organizations large and small to deliver secure, cost-effective patient care. MEDITECH's intuitive and mobile offerings include software for health information management, patient care and patient safety, emergency department management, oncology, genomics, population health, laboratories, blood banks, revenue cycle management, home health, virtual care, and many other areas of healthcare. Proficiency with cloud technology is a competitive advantage and major selling point for MEDITECH. On its website the company describes its MEDITECH Cloud Platform as a way to "[g]ive [...]
