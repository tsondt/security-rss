Title: How cut-and-pasted programming is putting the internet and society at risk | John Naughton
Date: 2021-12-18T16:00:02+00:00
Author: John Naughton
Category: The Guardian
Tags: Games;Data and computer security;Malware;Technology;Culture;Internet
Slug: 2021-12-18-how-cut-and-pasted-programming-is-putting-the-internet-and-society-at-risk-john-naughton

[Source](https://www.theguardian.com/commentisfree/2021/dec/18/how-cut-and-pasted-programming-is-putting-the-internet-and-society-at-risk){:target="_blank" rel="noopener"}

> A vulnerability has been exposed in Minecraft, the bestselling video game of all time – and the security implications outside the world of gaming are vast In one of those delicious coincidences that warm the cockles of every tech columnist’s heart, in the same week that the entire internet community was scrambling to patch a glaring vulnerability that affects countless millions of web servers across the world, the UK government announced a grand new National Cyber Security Strategy that, even if actually implemented, would have been largely irrelevant to the crisis at hand. Initially, it looked like a prank in [...]
