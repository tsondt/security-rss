Title: A third of you slackers out there still aren't using HTTPS by default
Date: 2021-12-09T19:46:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-12-09-a-third-of-you-slackers-out-there-still-arent-using-https-by-default

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/09/top_1_million_report_scott_helme/){:target="_blank" rel="noopener"}

> And it's really bad news for EV cert vendors in Top 1 Million report Almost a third of the world wide web's top million sites are still not using HTTPS by default, according to infosec researcher Scott Helme's analysis.... [...]
