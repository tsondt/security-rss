Title: Foreign Office IT chaos: Shocking testimony reveals poor tech support hindered Afghan evac attempts
Date: 2021-12-07T13:49:32+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2021-12-07-foreign-office-it-chaos-shocking-testimony-reveals-poor-tech-support-hindered-afghan-evac-attempts

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/07/foreign_office_it_chaos_testimony/){:target="_blank" rel="noopener"}

> Contributed to dysfunction as diplomats and soldiers struggled to get Afghan helpers out of reach of Taliban Diplomats and soldiers were left grappling with appallingly inadequate IT and secure communications support as thousands of Afghans struggled to get help from the UK during the fall of the capital Kabul in August.... [...]
