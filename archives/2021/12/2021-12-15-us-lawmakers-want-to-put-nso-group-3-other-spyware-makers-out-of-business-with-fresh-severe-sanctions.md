Title: US lawmakers want to put NSO Group, 3 other spyware makers out of business with fresh severe sanctions
Date: 2021-12-15T20:50:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-12-15-us-lawmakers-want-to-put-nso-group-3-other-spyware-makers-out-of-business-with-fresh-severe-sanctions

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/15/magnitsky_act_nso_group/){:target="_blank" rel="noopener"}

> Export controls aren't enough, Dems say: Bring on the Global Magnitsky Act Eighteen US Democratic lawmakers have asked the Treasury Department and State Department to punish Israel-based spyware maker NSO Group and three other surveillance software firms for enabling human rights abuses.... [...]
