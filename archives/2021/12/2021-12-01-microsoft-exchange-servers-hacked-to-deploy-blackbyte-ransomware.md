Title: Microsoft Exchange servers hacked to deploy BlackByte ransomware
Date: 2021-12-01T11:21:48-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-12-01-microsoft-exchange-servers-hacked-to-deploy-blackbyte-ransomware

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-servers-hacked-to-deploy-blackbyte-ransomware/){:target="_blank" rel="noopener"}

> BlackByte ransomware actors were observed exploiting the ProxyShell set of vulnerabilities (CVE-2021-34473, CVE-2021-34523, CVE-2021-31207) to compromise Microsoft Exchange servers. [...]
