Title: US identity thieves jailed over $130,000 scam that targeted the elderly
Date: 2021-12-03T14:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-03-us-identity-thieves-jailed-over-130000-scam-that-targeted-the-elderly

[Source](https://portswigger.net/daily-swig/us-identity-thieves-jailed-over-130-000-scam-that-targeted-the-elderly){:target="_blank" rel="noopener"}

> Dark web fraudsters caught after stealing the identities of murder victims [...]
