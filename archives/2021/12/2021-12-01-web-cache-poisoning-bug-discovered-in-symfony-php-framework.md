Title: Web cache poisoning bug discovered in Symfony PHP framework
Date: 2021-12-01T10:38:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-01-web-cache-poisoning-bug-discovered-in-symfony-php-framework

[Source](https://portswigger.net/daily-swig/web-cache-poisoning-bug-discovered-in-symfony-php-framework){:target="_blank" rel="noopener"}

> Vulnerability in open source project has since been patched [...]
