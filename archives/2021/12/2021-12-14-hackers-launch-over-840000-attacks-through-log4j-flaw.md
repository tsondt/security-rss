Title: Hackers launch over 840,000 attacks through Log4J flaw
Date: 2021-12-14T15:32:27+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;china;hacking;log4j
Slug: 2021-12-14-hackers-launch-over-840000-attacks-through-log4j-flaw

[Source](https://arstechnica.com/?p=1820509){:target="_blank" rel="noopener"}

> Enlarge (credit: Matejmo | Getty Images) Hackers including Chinese state-backed groups have launched more than 840,000 attacks on companies globally since last Friday, according to researchers, through a previously unnoticed vulnerability in a widely used piece of open-source software called Log4J. Cyber security group Check Point said the attacks relating to the vulnerability had accelerated in the 72 hours since Friday, and that at some points its researchers were seeing more than 100 attacks a minute. Perpetrators include “Chinese government attackers,” according to Charles Carmakal, chief technology officer of cyber company Mandiant. Read 11 remaining paragraphs | Comments [...]
