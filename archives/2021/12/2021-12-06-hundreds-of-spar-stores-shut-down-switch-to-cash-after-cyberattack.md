Title: Hundreds of SPAR stores shut down, switch to cash after cyberattack
Date: 2021-12-06T12:22:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-06-hundreds-of-spar-stores-shut-down-switch-to-cash-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/hundreds-of-spar-stores-shut-down-switch-to-cash-after-cyberattack/){:target="_blank" rel="noopener"}

> Approximately 330 SPAR shops in northern England face severe operational problems following a weekend cyberattack, forcing many stores to close or switch to cash-only payments. [...]
