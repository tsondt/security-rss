Title: Protecting your critical infrastructure is one thing…protecting your backups is the same thing
Date: 2021-12-03T19:43:55+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: 2021-12-03-protecting-your-critical-infrastructure-is-one-thingprotecting-your-backups-is-the-same-thing

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/03/protecting_backups_ransomware/){:target="_blank" rel="noopener"}

> Do you know what your recovery position really is? Paid Feature Normally, when we have more of something, we tend to think of it as less valuable. We might even become less protective of it.... [...]
