Title: Australian govt raises alarm over Conti ransomware attacks
Date: 2021-12-10T09:12:25-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-10-australian-govt-raises-alarm-over-conti-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/australian-govt-raises-alarm-over-conti-ransomware-attacks/){:target="_blank" rel="noopener"}

> The Australian Cyber Security Centre (ACSC) says Conti ransomware attacks have targeted multiple Australian organizations from various industry verticals since November. [...]
