Title: Third Log4J Bug Can Trigger DoS; Apache Issues Patch
Date: 2021-12-20T16:01:57+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-12-20-third-log4j-bug-can-trigger-dos-apache-issues-patch

[Source](https://threatpost.com/third-log4j-bug-dos-apache-patch/177159/){:target="_blank" rel="noopener"}

> The new Log4j vulnerability is similar to Log4Shell in that it also affects the logging library, but this DoS flaw has to do with Context Map lookups, not JNDI. [...]
