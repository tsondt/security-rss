Title: Miscreants make off with $150m of digital assets in BitMart security breach
Date: 2021-12-06T17:01:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-12-06-miscreants-make-off-with-150m-of-digital-assets-in-bitmart-security-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/06/bitmart_breach/){:target="_blank" rel="noopener"}

> Or it might be nearer $200m. Even the amounts stolen seem to be volatile in the crypto world Cryptocurrency exchange BitMart has coughed to a large-scale security breach relating to ETH and BSC hot wallets. The company reckons that hackers made off with approximately $150m in assets.... [...]
