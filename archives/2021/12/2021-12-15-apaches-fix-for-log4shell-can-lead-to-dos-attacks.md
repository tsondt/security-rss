Title: Apache’s Fix for Log4Shell Can Lead to DoS Attacks
Date: 2021-12-15T14:04:19+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;Web Security
Slug: 2021-12-15-apaches-fix-for-log4shell-can-lead-to-dos-attacks

[Source](https://threatpost.com/apache-patch-log4shell-log4j-dos-attacks/177064/){:target="_blank" rel="noopener"}

> Not only is the jaw-dropping flaw in the Apache Log4j logging library ubiquitous; Apache’s blanket of a quickly baked patch for Log4Shell also has holes. [...]
