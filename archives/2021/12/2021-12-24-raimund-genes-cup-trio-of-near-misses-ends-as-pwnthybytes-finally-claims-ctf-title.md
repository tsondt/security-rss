Title: Raimund Genes Cup: Trio of near misses ends as PwnThyBytes finally claims CTF title&nbsp;
Date: 2021-12-24T12:16:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-24-raimund-genes-cup-trio-of-near-misses-ends-as-pwnthybytes-finally-claims-ctf-title

[Source](https://portswigger.net/daily-swig/raimund-genes-cup-trio-of-near-misses-ends-as-pwnthybytes-finally-claims-ctf-title-nbsp){:target="_blank" rel="noopener"}

> A dozen teams competed in cloud, IoT, OSINT, forensics, and machine learning challenges [...]
