Title: Rideshare account hacker faces up to 22 years in prison
Date: 2021-12-22T14:51:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2021-12-22-rideshare-account-hacker-faces-up-to-22-years-in-prison

[Source](https://www.bleepingcomputer.com/news/legal/rideshare-account-hacker-faces-up-to-22-years-in-prison/){:target="_blank" rel="noopener"}

> A man pleaded guilty to fraudulently opening rideshare and delivery service accounts using stolen identity information sold on dark web marketplaces. [...]
