Title: T-Mobile says it blocked 21 billion scam calls this year
Date: 2021-12-20T10:46:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-20-t-mobile-says-it-blocked-21-billion-scam-calls-this-year

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-says-it-blocked-21-billion-scam-calls-this-year/){:target="_blank" rel="noopener"}

> T-Mobile says it blocked 21 billion scam, spam, and unwanted robocalls this year through its free Scam Shield robocall and scam protection service, amounting to an average of 1.8 billion scam calls identified or blocked every month. [...]
