Title: The Week in Ransomware - December 24th 2021 - No rest for the weary
Date: 2021-12-24T16:34:18-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-24-the-week-in-ransomware-december-24th-2021-no-rest-for-the-weary

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-24th-2021-no-rest-for-the-weary/){:target="_blank" rel="noopener"}

> The holiday season is here, but there is no rest for our weary admins as ransomware gangs are still conducting attacks over the Christmas and New Years breaks. [...]
