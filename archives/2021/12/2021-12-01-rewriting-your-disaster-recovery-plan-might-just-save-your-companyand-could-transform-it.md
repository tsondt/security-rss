Title: Rewriting your disaster recovery plan might just save your company…and could transform it
Date: 2021-12-01T21:00:10+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: 2021-12-01-rewriting-your-disaster-recovery-plan-might-just-save-your-companyand-could-transform-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/01/time_to_review_disaster_recovery_plan/){:target="_blank" rel="noopener"}

> Just be sure it actually works Paid Feature Disaster recovery (DR) used to be thought of as a form of corporate hygiene, but it’s becoming increasingly clear it has to be considered a matter of corporate survival.... [...]
