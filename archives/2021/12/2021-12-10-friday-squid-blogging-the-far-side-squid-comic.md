Title: Friday Squid Blogging: The Far Side Squid Comic
Date: 2021-12-10T22:05:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-12-10-friday-squid-blogging-the-far-side-squid-comic

[Source](https://www.schneier.com/blog/archives/2021/12/friday-squid-blogging-the-far-side-squid-comic.html){:target="_blank" rel="noopener"}

> The Far Side is always good for a squid reference. Here’s a recent one. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
