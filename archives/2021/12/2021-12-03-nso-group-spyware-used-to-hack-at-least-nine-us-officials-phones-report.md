Title: NSO Group spyware used to hack at least nine US officials’ phones – report
Date: 2021-12-03T18:36:58+00:00
Author: Stephanie Kirchgaessner in Washington
Category: The Guardian
Tags: Surveillance;Hacking;US news;US national security;Data and computer security;Biden administration;Technology;World news
Slug: 2021-12-03-nso-group-spyware-used-to-hack-at-least-nine-us-officials-phones-report

[Source](https://www.theguardian.com/world/2021/dec/03/us-state-department-officials-iphones-hacked-nso-group-spyware){:target="_blank" rel="noopener"}

> Revelation comes just weeks after the Biden administration placed NSO on a US blacklist The iPhones of at least nine US state department officials were recently hacked by a government using NSO Group spyware, according to a new report that raised serious questions about the use of Israeli surveillance tools against US government officials around the world. The claim, which was reported by Reuters, comes just weeks after the Biden administration placed NSO on a US blacklist and said the surveillance company acted “contrary to the foreign policy and national security interests of the US”. Continue reading... [...]
