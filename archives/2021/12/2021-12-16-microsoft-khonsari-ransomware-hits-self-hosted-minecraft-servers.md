Title: Microsoft: Khonsari ransomware hits self-hosted Minecraft servers
Date: 2021-12-16T12:20:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-12-16-microsoft-khonsari-ransomware-hits-self-hosted-minecraft-servers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-khonsari-ransomware-hits-self-hosted-minecraft-servers/){:target="_blank" rel="noopener"}

> Microsoft urges admins of self-hosted Minecraft servers to upgrade to the latest release to defend against Khonsari ransomware attacks exploiting the critical Log4Shell security vulnerability. [...]
