Title: 2022: Supply-Chain Chronic Pain & SaaS Security Meltdowns
Date: 2021-12-15T00:27:24+00:00
Author: Sounil Yu
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Malware;Vulnerabilities;Web Security
Slug: 2021-12-15-2022-supply-chain-chronic-pain-saas-security-meltdowns

[Source](https://threatpost.com/supply-chain-pain-and-changing-security-roles/177058/){:target="_blank" rel="noopener"}

> Sounil Yu, CISO at JupiterOne, discusses the growing mesh of integrations between SaaS applications, which enables automated business workflows - and rampant lateral movement by attackers, well outside IT's purview. [...]
