Title: Propane distributor Superior Plus admits ransomware breach
Date: 2021-12-15T17:14:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-15-propane-distributor-superior-plus-admits-ransomware-breach

[Source](https://portswigger.net/daily-swig/propane-distributor-superior-plus-admits-ransomware-breach){:target="_blank" rel="noopener"}

> Clean up and damage assessment underway [...]
