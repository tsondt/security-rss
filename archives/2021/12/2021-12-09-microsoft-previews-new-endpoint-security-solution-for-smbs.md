Title: Microsoft previews new endpoint security solution for SMBs
Date: 2021-12-09T10:36:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-12-09-microsoft-previews-new-endpoint-security-solution-for-smbs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-previews-new-endpoint-security-solution-for-smbs/){:target="_blank" rel="noopener"}

> Microsoft Defender for Business, a new endpoint security solution specially built for small and medium-sized businesses (SMBs), is now rolling out in preview worldwide. [...]
