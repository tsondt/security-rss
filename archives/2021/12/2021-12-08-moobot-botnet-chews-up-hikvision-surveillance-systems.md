Title: Moobot Botnet Chews Up Hikvision Surveillance Systems
Date: 2021-12-08T20:13:18+00:00
Author: Becky Bracken
Category: Threatpost
Tags: IoT;Malware;Vulnerabilities;Web Security
Slug: 2021-12-08-moobot-botnet-chews-up-hikvision-surveillance-systems

[Source](https://threatpost.com/moobot-botnet-hikvision-surveillance-systems/176879/){:target="_blank" rel="noopener"}

> Attackers are milking unpatched Hikvision video systems to drop a DDoS botnet, researchers warned. [...]
