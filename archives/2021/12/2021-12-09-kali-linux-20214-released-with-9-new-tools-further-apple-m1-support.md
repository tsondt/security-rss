Title: Kali Linux 2021.4 released with 9 new tools, further Apple M1 support
Date: 2021-12-09T17:58:02-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Linux
Slug: 2021-12-09-kali-linux-20214-released-with-9-new-tools-further-apple-m1-support

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20214-released-with-9-new-tools-further-apple-m1-support/){:target="_blank" rel="noopener"}

> ​Kali Linux 2021.4 was released today by Offensive Security and includes further Apple M1 support, increased Samba compatibility, nine new tools, and an update for all three main desktop. [...]
