Title: Enabling keyless authentication from GitHub Actions
Date: 2021-12-06T17:00:00+00:00
Author: Bharath Baiju
Category: GCP Security
Tags: Solutions and How-to's;Hybrid & Multicloud;Partners;Google Cloud;Identity & Security
Slug: 2021-12-06-enabling-keyless-authentication-from-github-actions

[Source](https://cloud.google.com/blog/products/identity-security/enabling-keyless-authentication-from-github-actions/){:target="_blank" rel="noopener"}

> GitHub Actions is a third-party CI/CD solution popular among many Google Cloud customers and developers. When a GitHub Actions Workflow needs to read or mutate resources on Google Cloud – such as publishing a container to Artifact Registry or deploying a new service with Cloud Run – it must first authenticate. Traditionally, authenticating from GitHub Actions to Google Cloud required exporting and storing a long-lived JSON service account key, turning an identity management problem into a secrets management problem. Not only did this introduce additional security risks if the service account key were to leak, but it also meant developers [...]
