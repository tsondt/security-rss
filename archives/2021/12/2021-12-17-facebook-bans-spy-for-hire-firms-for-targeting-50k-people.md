Title: Facebook Bans Spy-for-Hire Firms for Targeting 50K People
Date: 2021-12-17T20:17:45+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Facebook;Government;Hacks;Malware;Vulnerabilities;Web Security
Slug: 2021-12-17-facebook-bans-spy-for-hire-firms-for-targeting-50k-people

[Source](https://threatpost.com/facebook-bans-spy-hire/177149/){:target="_blank" rel="noopener"}

> Meta, Facebook’s parent company, said that the seven banned actors run fake accounts on its platforms to deceive users and plant malware on targets’ phones. [...]
