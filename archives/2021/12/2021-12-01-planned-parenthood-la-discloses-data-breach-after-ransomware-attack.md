Title: Planned Parenthood LA discloses data breach after ransomware attack
Date: 2021-12-01T20:18:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2021-12-01-planned-parenthood-la-discloses-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/planned-parenthood-la-discloses-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> ​Planned Parenthood Los Angeles has disclosed a data breach after suffering a ransomware attack in October that exposed the personal information of approximately 400,000 patients. [...]
