Title: Opera browser working on clipboard anti-hijacking feature
Date: 2021-12-22T14:00:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2021-12-22-opera-browser-working-on-clipboard-anti-hijacking-feature

[Source](https://www.bleepingcomputer.com/news/security/opera-browser-working-on-clipboard-anti-hijacking-feature/){:target="_blank" rel="noopener"}

> The Opera browser team is working on a new clipboard monitoring and protection system called Paste Protection, which aims to prevent content hijacking and snooping. [...]
