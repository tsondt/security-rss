Title: US food importer Atalanta admits ransomware attack
Date: 2021-12-09T14:48:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-09-us-food-importer-atalanta-admits-ransomware-attack

[Source](https://portswigger.net/daily-swig/us-food-importer-atalanta-admits-ransomware-attack){:target="_blank" rel="noopener"}

> Multiple questions remain about July data breach [...]
