Title: What the Log4Shell Bug Means for SMBs: Experts Weigh In
Date: 2021-12-14T17:54:47+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-12-14-what-the-log4shell-bug-means-for-smbs-experts-weigh-in

[Source](https://threatpost.com/log4shell-bug-smbs-experts/177021/){:target="_blank" rel="noopener"}

> An exclusive roundtable of security researchers discuss the specific implications of CVE-2021-44228 for smaller businesses, including what's vulnerable, what an attack looks like and to how to remediate. [...]
