Title: Investigating the usage of GCP Service Accounts
Date: 2021-12-13T17:00:00+00:00
Author: Garrett Wong
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-12-13-investigating-the-usage-of-gcp-service-accounts

[Source](https://cloud.google.com/blog/products/identity-security/three-services-to-investigate-gcp-service-account-usage/){:target="_blank" rel="noopener"}

> Service accounts on Google Cloud are used when a workload needs to access resources or conduct actions without end-user involvement. There are multiple methods of authenticating using service accounts, including using service accounts as part of Google Compute Engine instances, impersonating service accounts, or using service accounts with a key file -- an option which should be carefully considered. A common objective is to achieve keyless service account architectures on Google Cloud, but this can be difficult across an entire organization. There are a number of reasons why teams may opt to generate service account keys, ranging from developer validation [...]
