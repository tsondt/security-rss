Title: Dell driver fix still allows Windows Kernel-level attacks
Date: 2021-12-13T15:21:59-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-13-dell-driver-fix-still-allows-windows-kernel-level-attacks

[Source](https://www.bleepingcomputer.com/news/security/dell-driver-fix-still-allows-windows-kernel-level-attacks/){:target="_blank" rel="noopener"}

> Dell's driver fix of the CVE-2021-21551 vulnerability leaves margin for catastrophic BYOVD attacks resulting in Windows kernel driver code execution. [...]
