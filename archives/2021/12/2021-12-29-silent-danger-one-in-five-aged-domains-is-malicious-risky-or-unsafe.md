Title: Silent danger: One in five aged domains is malicious, risky, or unsafe
Date: 2021-12-29T15:42:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-29-silent-danger-one-in-five-aged-domains-is-malicious-risky-or-unsafe

[Source](https://www.bleepingcomputer.com/news/security/silent-danger-one-in-five-aged-domains-is-malicious-risky-or-unsafe/){:target="_blank" rel="noopener"}

> The number of malicious dormant domains is on the rise, and as researchers warn, roughly 22.3% of strategically aged domains pose some form of danger. [...]
