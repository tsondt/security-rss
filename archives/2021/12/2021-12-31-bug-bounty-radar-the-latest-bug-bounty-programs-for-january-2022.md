Title: Bug Bounty Radar // The latest bug bounty programs for January 2022
Date: 2021-12-31T14:38:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-12-31-bug-bounty-radar-the-latest-bug-bounty-programs-for-january-2022

[Source](https://portswigger.net/daily-swig/bug-bounty-radar-the-latest-bug-bounty-programs-for-january-2022){:target="_blank" rel="noopener"}

> New web targets for the discerning hacker [...]
