Title: Brand-New Log4Shell Attack Vector Threatens Local Hosts
Date: 2021-12-17T17:43:43+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-12-17-brand-new-log4shell-attack-vector-threatens-local-hosts

[Source](https://threatpost.com/new-log4shell-attack-vector-local-hosts/177128/){:target="_blank" rel="noopener"}

> The discovery, which affects services running as localhost that aren't exposed to any network or the internet, vastly widens the scope of attack possibilities. [...]
