Title: Sprawling Active Attack Aims to Take Over 1.6M WordPress Sites
Date: 2021-12-10T16:19:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-12-10-sprawling-active-attack-aims-to-take-over-16m-wordpress-sites

[Source](https://threatpost.com/active-attack-takeover-wordpress/176933/){:target="_blank" rel="noopener"}

> Cyberattackers are targeting security vulnerabilities in four plugins plus Epsilon themes, to assign themselves administrative accounts. [...]
