Title: Rook ransomware is yet another spawn of the leaked Babuk code
Date: 2021-12-24T11:26:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-12-24-rook-ransomware-is-yet-another-spawn-of-the-leaked-babuk-code

[Source](https://www.bleepingcomputer.com/news/security/rook-ransomware-is-yet-another-spawn-of-the-leaked-babuk-code/){:target="_blank" rel="noopener"}

> A new ransomware operation named Rook has appeared recently on the cyber-crime space, declaring a desperate need to make "a lot of money" by breaching corporate networks and encrypting devices. [...]
