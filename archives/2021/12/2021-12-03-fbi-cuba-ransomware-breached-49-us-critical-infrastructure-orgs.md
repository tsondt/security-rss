Title: FBI: Cuba ransomware breached 49 US critical infrastructure orgs
Date: 2021-12-03T12:16:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-12-03-fbi-cuba-ransomware-breached-49-us-critical-infrastructure-orgs

[Source](https://www.bleepingcomputer.com/news/security/fbi-cuba-ransomware-breached-49-us-critical-infrastructure-orgs/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) has revealed that the Cuba ransomware gang has compromised the networks of at least 49 organizations from US critical infrastructure sectors. [...]
