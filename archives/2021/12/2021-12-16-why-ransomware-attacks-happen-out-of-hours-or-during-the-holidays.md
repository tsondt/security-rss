Title: Why ransomware attacks happen out of hours or during the holidays
Date: 2021-12-16T18:00:05+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2021-12-16-why-ransomware-attacks-happen-out-of-hours-or-during-the-holidays

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/16/out_of_hours_ransomware_attacks/){:target="_blank" rel="noopener"}

> Security teams have a choice to make – and doing nothing is not an option Paid Feature Time waits for no one. But ransomware attackers do. Increasingly, cybercriminals are timing their attacks, detonating them when their victims are out of the office. This gives them the chance to inflict maximum damage, and explains why ransomware attacks surge on public holidays like Thanksgiving and Christmas. How do they do it, and what can under-staffed security teams do about it?... [...]
