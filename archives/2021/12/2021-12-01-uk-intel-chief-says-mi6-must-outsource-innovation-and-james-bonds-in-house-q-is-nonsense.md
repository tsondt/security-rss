Title: UK intel chief says MI6 must outsource innovation – and James Bond's in-house 'Q' is nonsense
Date: 2021-12-01T03:56:27+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-12-01-uk-intel-chief-says-mi6-must-outsource-innovation-and-james-bonds-in-house-q-is-nonsense

[Source](https://go.theregister.com/feed/www.theregister.com/2021/12/01/mi6_chief_richard_moore_speech/){:target="_blank" rel="noopener"}

> China is on the march, Russia loves to destabilise, no intelligence agency can stop 'em without help The head of the UK's secretive Military Intelligence Section 6 agency – popularly known as MI6 – has delivered a rare speech in which he has warned that China, Iran, and Russia use information technology to destabilise rivals, and that the agency he leads can no longer rely on in-house innovation to develop the technologies the UK needs to defend itself.... [...]
