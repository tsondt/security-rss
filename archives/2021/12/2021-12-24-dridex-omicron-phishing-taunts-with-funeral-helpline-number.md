Title: Dridex Omicron phishing taunts with funeral helpline number
Date: 2021-12-24T08:11:22-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-12-24-dridex-omicron-phishing-taunts-with-funeral-helpline-number

[Source](https://www.bleepingcomputer.com/news/security/dridex-omicron-phishing-taunts-with-funeral-helpline-number/){:target="_blank" rel="noopener"}

> A malware distributor for the Dridex banking malware has been toying with victims and researchers over the last few weeks. The latest example is a phishing campaign that taunts victims with a COVID-19 funeral assistance helpline number. [...]
