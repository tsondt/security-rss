Title: Malicious PyPI packages with over 10,000 downloads taken down
Date: 2021-12-13T06:54:54-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-12-13-malicious-pypi-packages-with-over-10000-downloads-taken-down

[Source](https://www.bleepingcomputer.com/news/security/malicious-pypi-packages-with-over-10-000-downloads-taken-down/){:target="_blank" rel="noopener"}

> The Python Package Index (PyPI) registry has removed three malicious Python packages aimed at exfiltrating environment variables and dropping trojans on the infected machines. These malicious packages are estimated to have generated over 10,000 downloads and mirrors put together, according to the researchers' report. [...]
