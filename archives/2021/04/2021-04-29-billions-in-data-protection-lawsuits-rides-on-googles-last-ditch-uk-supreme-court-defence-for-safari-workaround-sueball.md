Title: Billions in data protection lawsuits rides on Google's last-ditch UK Supreme Court defence for Safari Workaround sueball
Date: 2021-04-29T11:30:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: billions-in-data-protection-lawsuits-rides-on-googles-last-ditch-uk-supreme-court-defence-for-safari-workaround-sueball

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/29/google_safari_workaround_supreme_court/){:target="_blank" rel="noopener"}

> Biggest data protection case for years teeters on brink Google has urged the UK's Supreme Court to throw out a £3bn lawsuit brought by an ex-Which director over secretly planted tracking cookies on devices running Safari, on the grounds that local law doesn’t allow for opt-out class action lawsuits.... [...]
