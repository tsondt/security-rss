Title: It was Russia wot did it: SolarWinds hack was done by Kremlin's APT29 crew, say UK and US
Date: 2021-04-15T15:49:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: it-was-russia-wot-did-it-solarwinds-hack-was-done-by-kremlins-apt29-crew-say-uk-and-us

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/15/solarwinds_hack_russia_apt29_positive_technologies_sanctions/){:target="_blank" rel="noopener"}

> And Positive Technologies has been slapped with American sanctions Russia’s infamous APT 29, aka Cozy Bear, was behind the SolarWinds Orion attack, the US and UK governments said today as America slapped sanctions on Russian infosec companies as well as expelling diplomats from that country’s US embassy.... [...]
