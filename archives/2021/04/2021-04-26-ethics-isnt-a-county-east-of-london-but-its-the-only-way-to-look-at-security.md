Title: Ethics isn't a county east of London, but it's the only way to look at security
Date: 2021-04-26T09:15:11+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: ethics-isnt-a-county-east-of-london-but-its-the-only-way-to-look-at-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/26/column_security_ethics/){:target="_blank" rel="noopener"}

> We are all human beings, we live in a community, and everything we do affects others Column The trouble with good ideas is that, taken together, they can be very bad. It's a good idea to worry about supply chain malware injection – ask SolarWinds – and a good idea to come up with ways to stop it. It's even a good idea to look at major open-source software projects, such as the Linux kernel, with their very open supply chain, and ask – is this particularly vulnerable? After all, a poisoned Linux kernel would be bad enough to make [...]
