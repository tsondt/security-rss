Title: Google Project Zero Cuts Bug Disclosure Timeline to a 30-Day Grace Period
Date: 2021-04-16T12:57:36+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Bug Bounty;Vulnerabilities
Slug: google-project-zero-cuts-bug-disclosure-timeline-to-a-30-day-grace-period

[Source](https://threatpost.com/google-project-zero-cuts-bug-disclosure-timeline-to-a-30-day-grace-period/165432/){:target="_blank" rel="noopener"}

> The zero-day flaw research group has revised its disclosure of the technical details of vulnerabilities in the hopes of speeding up the release and adoption of fixes. [...]
