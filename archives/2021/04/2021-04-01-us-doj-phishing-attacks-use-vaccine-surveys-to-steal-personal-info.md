Title: US DOJ: Phishing attacks use vaccine surveys to steal personal info
Date: 2021-04-01T14:15:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-doj-phishing-attacks-use-vaccine-surveys-to-steal-personal-info

[Source](https://www.bleepingcomputer.com/news/security/us-doj-phishing-attacks-use-vaccine-surveys-to-steal-personal-info/){:target="_blank" rel="noopener"}

> The US Department of Justice warns of phishing attacks using fake post-vaccine surveys to steal money from people or tricking them into handing over their personal information. [...]
