Title: Build security into Google Cloud deployments with our updated security foundations blueprint
Date: 2021-04-26T16:00:00+00:00
Author: Andy Chang
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: build-security-into-google-cloud-deployments-with-our-updated-security-foundations-blueprint

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-security-foundations-guide/){:target="_blank" rel="noopener"}

> At Google, we’re committed to delivering the industry’s most trusted cloud. To earn customer trust, we strive to operate in a shared-fate model for risk management in conjunction with our customers. We believe that it's our responsibility to be active partners as our customers securely deploy on our platform, not simply delineate where our responsibility ends. Toward this goal, we have launched an updated version of our Google Cloud security foundations guide and corresponding Terraform blueprint scripts. In these resources, we provide opinionated step-by-step guidance for creating a secured landing zone into which you can configure and deploy your Google [...]
