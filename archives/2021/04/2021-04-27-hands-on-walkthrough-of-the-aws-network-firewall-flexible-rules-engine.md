Title: Hands-on walkthrough of the AWS Network Firewall flexible rules engine
Date: 2021-04-27T21:21:29+00:00
Author: Joel Desaulniers
Category: AWS Security
Tags: AWS Network Firewall;Intermediate (200);Security, Identity, & Compliance;Inspection;Security Blog
Slug: hands-on-walkthrough-of-the-aws-network-firewall-flexible-rules-engine

[Source](https://aws.amazon.com/blogs/security/hands-on-walkthrough-of-the-aws-network-firewall-flexible-rules-engine/){:target="_blank" rel="noopener"}

> AWS Network Firewall is a managed service that makes it easy to provide fine-grained network protections for all of your Amazon Virtual Private Clouds (Amazon VPCs) to ensure that your traffic is inspected, monitored, and logged. The firewall scales automatically with your network traffic, and offers built-in redundancies designed to provide high availability. AWS Network [...]
