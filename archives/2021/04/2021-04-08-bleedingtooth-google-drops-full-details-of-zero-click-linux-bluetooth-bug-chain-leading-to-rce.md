Title: BleedingTooth: Google drops full details of zero-click Linux Bluetooth bug chain leading to RCE
Date: 2021-04-08T15:38:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: bleedingtooth-google-drops-full-details-of-zero-click-linux-bluetooth-bug-chain-leading-to-rce

[Source](https://portswigger.net/daily-swig/bleedingtooth-google-drops-full-details-of-zero-click-linux-bluetooth-bug-chain-leading-to-rce){:target="_blank" rel="noopener"}

> Researcher says his findings ultimately led to a safer, more stable kernel [...]
