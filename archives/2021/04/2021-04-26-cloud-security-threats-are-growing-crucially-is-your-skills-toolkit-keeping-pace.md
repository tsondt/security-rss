Title: Cloud security threats are growing – crucially, is your skills toolkit keeping pace?
Date: 2021-04-26T06:30:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: cloud-security-threats-are-growing-crucially-is-your-skills-toolkit-keeping-pace

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/26/cloud_security_threats_are_growing/){:target="_blank" rel="noopener"}

> Here’s a flight plan to map your journey Promo Whatever unit of measurement you use, it’s clear that more and more enterprise computing is happening in the cloud - which also means the cloud is an ever-growing target for cyber attackers.... [...]
