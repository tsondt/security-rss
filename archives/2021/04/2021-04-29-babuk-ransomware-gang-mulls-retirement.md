Title: Babuk Ransomware Gang Mulls Retirement
Date: 2021-04-29T22:44:21+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware
Slug: babuk-ransomware-gang-mulls-retirement

[Source](https://threatpost.com/babuk-ransomware-gang-mulls-retirement/165742/){:target="_blank" rel="noopener"}

> The RaaS operators have been posting, tweaking and taking down a goodbye note, saying that they'll be open-sourcing their data encryption malware for other crooks to use. [...]
