Title: Signal founder: I hacked police phone-cracking tool Cellebrite
Date: 2021-04-22T16:33:42+00:00
Author: Alex Hern Technology editor
Category: The Guardian
Tags: Hacking;Chat and messaging apps;Computing;Mobile phones;Technology;Israel;US news;World news;Data and computer security
Slug: signal-founder-i-hacked-police-phone-cracking-tool-cellebrite

[Source](https://www.theguardian.com/technology/2021/apr/22/signal-founder-i-hacked-police-phone-cracking-tool-cellebrite){:target="_blank" rel="noopener"}

> Moxie Marlinspike accuses surveillance firm of being ‘linked to persecution’ around the world The CEO of the messaging app Signal claims to have hacked the phone-cracking tools used by police in Britain and around the world to extract information from seized devices. In an online post, Moxie Marlinspike, the security researcher who founded Signal in 2013, detailed a series of vulnerabilities in the surveillance devices, made by the Israeli company Cellebrite. Continue reading... [...]
