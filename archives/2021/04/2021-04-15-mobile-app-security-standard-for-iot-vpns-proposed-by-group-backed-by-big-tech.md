Title: Mobile app security standard for IoT, VPNs proposed by group backed by Big Tech
Date: 2021-04-15T21:42:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: mobile-app-security-standard-for-iot-vpns-proposed-by-group-backed-by-big-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/15/mobile_app_security/){:target="_blank" rel="noopener"}

> ioXt Alliance aims to bring 'transparency and visibility' On Thursday the ioXt Alliance, an Internet of Things (IoT) security trade group backed by some of the biggest names in the business, introduced a set of baseline standards for mobile apps, in the hope that IoT security may someday be a bit less of a dumpster fire.... [...]
