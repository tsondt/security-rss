Title: European Commission, other EU orgs recently hit by cyber-attack
Date: 2021-04-06T13:08:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Deals
Slug: european-commission-other-eu-orgs-recently-hit-by-cyber-attack

[Source](https://www.bleepingcomputer.com/news/security/european-commission-other-eu-orgs-recently-hit-by-cyber-attack/){:target="_blank" rel="noopener"}

> The European Commission and several other European Union organizations were hit by a cyberattack in March according to a European Commission spokesperson. [...]
