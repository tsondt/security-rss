Title: A Post-Data Privacy World and Data-Rights Management
Date: 2021-04-14T17:21:54+00:00
Author: Joseph Carson
Category: Threatpost
Tags: Government;InfoSec Insider;Privacy
Slug: a-post-data-privacy-world-and-data-rights-management

[Source](https://threatpost.com/data-privacy-data-rights-management/165402/){:target="_blank" rel="noopener"}

> Joseph Carson, chief security scientist at Thycotic, discusses the death of data privacy and what comes next. [...]
