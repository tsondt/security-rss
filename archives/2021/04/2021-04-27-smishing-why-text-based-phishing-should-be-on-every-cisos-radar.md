Title: Smishing: Why Text-Based Phishing Should Be on Every CISO’s Radar
Date: 2021-04-27T16:49:26+00:00
Author: Phil Richards
Category: Threatpost
Tags: InfoSec Insider;Mobile Security;Web Security
Slug: smishing-why-text-based-phishing-should-be-on-every-cisos-radar

[Source](https://threatpost.com/smishing-text-phishing-ciso-radar/165634/){:target="_blank" rel="noopener"}

> Phil Richards, Chief Security Officer at Ivanti, discusses dramatic growth in smishing and what to do about it. [...]
