Title: Cybersecurity Experts to Follow on Twitter
Date: 2021-04-16T19:13:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;Schneier news;Twitter
Slug: cybersecurity-experts-to-follow-on-twitter

[Source](https://www.schneier.com/blog/archives/2021/04/cybersecurity-experts-to-follow-on-twitter.html){:target="_blank" rel="noopener"}

> Security Boulevard recently listed the “Top-21 Cybersecurity Experts You Must Follow on Twitter in 2021.” I came in at #7. I thought that was pretty good, especially since I never tweet. My Twitter feed just mirrors my blog. (If you are one of the 134K people who read me from Twitter, “hi.”) [...]
