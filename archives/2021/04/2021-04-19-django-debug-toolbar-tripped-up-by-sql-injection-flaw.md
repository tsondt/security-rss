Title: Django Debug Toolbar tripped up by SQL injection flaw
Date: 2021-04-19T14:44:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: django-debug-toolbar-tripped-up-by-sql-injection-flaw

[Source](https://portswigger.net/daily-swig/django-debug-toolbar-tripped-up-by-sql-injection-flaw){:target="_blank" rel="noopener"}

> Doctored forms pose threat to web framework DevOps plugin [...]
