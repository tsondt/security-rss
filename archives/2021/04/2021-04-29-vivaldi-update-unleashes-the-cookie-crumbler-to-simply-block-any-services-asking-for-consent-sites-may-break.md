Title: Vivaldi update unleashes the 'Cookie Crumbler' to simply block any services asking for consent (sites may break)
Date: 2021-04-29T12:26:26+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: vivaldi-update-unleashes-the-cookie-crumbler-to-simply-block-any-services-asking-for-consent-sites-may-break

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/29/vivaldi_3_8/){:target="_blank" rel="noopener"}

> Plus: Browser sends Google's FLoC straight to the blacklist The latest release of Chromium-based browser Vivaldi has extended ad blocking to handle cookie warning dialogs and sent a shot across the bows of Google's ad technology, FLoC.... [...]
