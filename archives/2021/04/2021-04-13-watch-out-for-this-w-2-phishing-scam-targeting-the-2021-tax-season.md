Title: Watch out for this W-2 phishing scam targeting the 2021 tax season
Date: 2021-04-13T09:04:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: watch-out-for-this-w-2-phishing-scam-targeting-the-2021-tax-season

[Source](https://www.bleepingcomputer.com/news/security/watch-out-for-this-w-2-phishing-scam-targeting-the-2021-tax-season/){:target="_blank" rel="noopener"}

> With the United State tax season in high gear, threat actors have sprung into action with a recent tax document phishing scam that abuses TypeForm forms to steal your login credentials. [...]
