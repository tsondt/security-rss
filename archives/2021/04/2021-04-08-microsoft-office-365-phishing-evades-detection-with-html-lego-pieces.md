Title: Microsoft Office 365 phishing evades detection with HTML Lego pieces
Date: 2021-04-08T09:12:50-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: microsoft-office-365-phishing-evades-detection-with-html-lego-pieces

[Source](https://www.bleepingcomputer.com/news/security/microsoft-office-365-phishing-evades-detection-with-html-lego-pieces/){:target="_blank" rel="noopener"}

> A recent phishing campaign used a clever trick to deliver the fraudulent web page that collects Microsoft Office 365 credentials by building it from chunks of HTML code stored locally and remotely. [...]
