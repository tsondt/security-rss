Title: Network Detection & Response: The Next Frontier in Fighting the Human Problem
Date: 2021-04-09T15:40:59+00:00
Author: Justin Jett
Category: Threatpost
Tags: InfoSec Insider;Vulnerabilities;Web Security
Slug: network-detection-response-the-next-frontier-in-fighting-the-human-problem

[Source](https://threatpost.com/network-detection-response-human-problem/165332/){:target="_blank" rel="noopener"}

> Justin Jett, director of audit and compliance for Plixer, discusses the transformation of network-traffic analytics and what it means for cybersecurity now. [...]
