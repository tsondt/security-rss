Title: ParkMobile Breach Exposes License Plate Data, Mobile Numbers of 21M Users
Date: 2021-04-12T22:18:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Gemini Advisory;Jeff Perkins;ParkMobile breach
Slug: parkmobile-breach-exposes-license-plate-data-mobile-numbers-of-21m-users

[Source](https://krebsonsecurity.com/2021/04/parkmobile-breach-exposes-license-plate-data-mobile-numbers-of-21m-users/){:target="_blank" rel="noopener"}

> Someone is selling account information for 21 million customers of ParkMobile, a mobile parking app that’s popular in North America. The stolen data includes customer email addresses, dates of birth, phone numbers, license plate numbers, hashed passwords and mailing addresses. KrebsOnSecurity first heard about the breach from Gemini Advisory, a New York City based threat intelligence firm that keeps a close eye on the cybercrime forums. Gemini shared a new sales thread on a Russian-language crime forum that included my ParkMobile account information in the accompanying screenshot of the stolen data. Included in the data were my email address and [...]
