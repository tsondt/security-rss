Title: Ubiquiti All But Confirms Breach Response Iniquity
Date: 2021-04-04T19:22:03+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Ubiquiti breach
Slug: ubiquiti-all-but-confirms-breach-response-iniquity

[Source](https://krebsonsecurity.com/2021/04/ubiquiti-all-but-confirms-breach-response-iniquity/){:target="_blank" rel="noopener"}

> For four days this past week, Internet-of-Things giant Ubiquiti did not respond to requests for comment on a whistleblower’s allegations the company had massively downplayed a “catastrophic” two-month breach ending in January to save its stock price, and that Ubiquiti’s insinuation that a third-party was to blame was a fabrication. I was happy to add their eventual public response to the top of Tuesday’s story on the whistleblower’s claims, but their statement deserves a post of its own because it actually confirms and reinforces those claims. Ubiquiti’s IoT gear includes things like WiFi routers, security cameras, and network video recorders. [...]
