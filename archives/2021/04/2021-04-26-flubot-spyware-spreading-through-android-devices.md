Title: Flubot Spyware Spreading Through Android Devices
Date: 2021-04-26T20:28:20+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security
Slug: flubot-spyware-spreading-through-android-devices

[Source](https://threatpost.com/flubot-spyware-android-devices/165607/){:target="_blank" rel="noopener"}

> The malware is spreading rapidly through ‘missed package delivery’ SMS texts, prompting urgent scam warnings from mobile carriers. [...]
