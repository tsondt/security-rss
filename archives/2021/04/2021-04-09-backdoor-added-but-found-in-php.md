Title: Backdoor Added — But Found — in PHP
Date: 2021-04-09T13:54:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: authentication;backdoors;hacking;open source;supply chain
Slug: backdoor-added-but-found-in-php

[Source](https://www.schneier.com/blog/archives/2021/04/backdoor-added-but-found-in-php.html){:target="_blank" rel="noopener"}

> Unknown hackers attempted to add a backdoor to the PHP source code. It was two malicious commits, with the subject “fix typo” and the names of known PHP developers and maintainers. They were discovered and removed before being pushed out to any users. But since 79% of the Internet’s websites use PHP, it’s scary. Developers have moved PHP to GitHub, which has better authentication. Hopefully it will be enough — PHP is a juicy target. [...]
