Title: Data from 500M LinkedIn Users Posted for Sale Online
Date: 2021-04-09T14:06:24+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Web Security
Slug: data-from-500m-linkedin-users-posted-for-sale-online

[Source](https://threatpost.com/data-500m-linkedin-users-online/165329/){:target="_blank" rel="noopener"}

> Like the Facebook incident earlier this week, the information — including user profile IDs, email addresses and other PII -- was scraped from the social-media platform. [...]
