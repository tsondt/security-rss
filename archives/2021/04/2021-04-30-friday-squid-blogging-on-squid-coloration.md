Title: Friday Squid Blogging: On Squid Coloration
Date: 2021-04-30T21:14:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-on-squid-coloration

[Source](https://www.schneier.com/blog/archives/2021/04/friday-squid-blogging-on-squid-coloration.html){:target="_blank" rel="noopener"}

> Nice excerpt from Martin Wallin’s book Squid. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
