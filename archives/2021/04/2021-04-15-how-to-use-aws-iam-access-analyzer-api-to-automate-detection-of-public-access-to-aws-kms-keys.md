Title: How to use AWS IAM Access Analyzer API to automate detection of public access to AWS KMS keys
Date: 2021-04-15T19:53:24+00:00
Author: Yevgeniy Ilyin
Category: AWS Security
Tags: AWS IAM Access Analyzer;AWS Key Management Service;Intermediate (200);Security, Identity, & Compliance;Amazon S3;IAM Access Analyzer;S3 public access;Security Blog
Slug: how-to-use-aws-iam-access-analyzer-api-to-automate-detection-of-public-access-to-aws-kms-keys

[Source](https://aws.amazon.com/blogs/security/how-to-use-aws-iam-access-analyzer-api-to-automate-detection-of-public-access-to-aws-kms-keys/){:target="_blank" rel="noopener"}

> In this blog post, I show you how to use AWS IAM Access Analyzer programmatically to automate the detection of public access to your resources in an AWS account. I also show you how to work with the Access Analyzer API, create an analyzer on your account and call specific API functions from your code. [...]
