Title: Facebook attributes 533 million users' data leak to "scraping" not hacking
Date: 2021-04-07T06:27:53-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: facebook-attributes-533-million-users-data-leak-to-scraping-not-hacking

[Source](https://www.bleepingcomputer.com/news/security/facebook-attributes-533-million-users-data-leak-to-scraping-not-hacking/){:target="_blank" rel="noopener"}

> Facebook has now released a public statement clarifying the cause of and addressing some of the concerns related to the recent data leak. As reported last week, information of about 533 million Facebook profiles surfaced on a hacker forum. [...]
