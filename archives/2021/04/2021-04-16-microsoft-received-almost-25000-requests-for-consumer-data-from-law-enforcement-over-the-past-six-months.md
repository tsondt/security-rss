Title: Microsoft received almost 25,000 requests for consumer data from law enforcement over the past six months
Date: 2021-04-16T14:41:47+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: microsoft-received-almost-25000-requests-for-consumer-data-from-law-enforcement-over-the-past-six-months

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/16/microsoft_digital_transparency/){:target="_blank" rel="noopener"}

> 25% were rejected, and it's less than 2013's figure... but be wary of what Redmond does with your information Microsoft has had a busy six months if its latest biannual digital trust report is anything to go by as law enforcement agencies crept closer to making 25,000 legal requests.... [...]
