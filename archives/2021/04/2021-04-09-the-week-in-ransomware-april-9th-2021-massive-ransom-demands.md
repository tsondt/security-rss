Title: The Week in Ransomware - April 9th 2021 - Massive ransom demands
Date: 2021-04-09T16:47:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-april-9th-2021-massive-ransom-demands

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-9th-2021-massive-ransom-demands/){:target="_blank" rel="noopener"}

> Ransomware attacks continue over the past two weeks with a continuation of the massive initial ransom demands we have seen recently. [...]
