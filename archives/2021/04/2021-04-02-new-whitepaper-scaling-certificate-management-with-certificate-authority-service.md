Title: New whitepaper: Scaling certificate management with Certificate Authority Service
Date: 2021-04-02T18:00:00+00:00
Author: Anoosh Saboori
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: new-whitepaper-scaling-certificate-management-with-certificate-authority-service

[Source](https://cloud.google.com/blog/products/identity-security/how-to-scale-certificate-management-with-google-cas-whitepaper/){:target="_blank" rel="noopener"}

> As Google Cloud’s Certificate Authority Service (CAS) approaches general availability, we want to help customers understand the service better. Customers have asked us how CAS fits into our larger security story and how CAS works for various use cases; today we are releasing a white paper about CAS to answer those questions and more. “Scaling certificate management with Google Certificate Authority Service”, written by Andrew Lance of Sidechain and Anton Chuvakin and Anoosh Saboori of Google Cloud, focuses on CAS as a modern certificate authority service and showcases key use cases for CAS. The digital world has experienced unprecedented growth [...]
