Title: Whistler resort municipality hit by new ransomware operation
Date: 2021-04-29T12:01:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: whistler-resort-municipality-hit-by-new-ransomware-operation

[Source](https://www.bleepingcomputer.com/news/security/whistler-resort-municipality-hit-by-new-ransomware-operation/){:target="_blank" rel="noopener"}

> The Whistler municipality in British Columbia, Canada, has suffered a cyberattack at the hands of a new ransomware operation. [...]
