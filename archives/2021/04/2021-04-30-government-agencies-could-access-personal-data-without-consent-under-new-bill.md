Title: Government agencies could access personal data without consent under new bill
Date: 2021-04-30T20:00:45+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Australian politics;Privacy;Technology;Australia news;Centrelink;Data and computer security
Slug: government-agencies-could-access-personal-data-without-consent-under-new-bill

[Source](https://www.theguardian.com/australia-news/2021/may/01/government-agencies-could-access-personal-data-without-consent-under-new-bill){:target="_blank" rel="noopener"}

> Privacy advocates fear Coalition’s proposed data-sharing law could allow for robodebt-style tactics Australians’ personal information could be accessed by government agencies and researchers without their consent under proposed data-sharing legislation that critics say could pave the way for more robodebt-style tactics. In a speech at an Australian Financial Review conference this week, the former government services minister Stuart Robert said it wasn’t his job to make government “sexy”, but make it simple. Related: Facebook data leak: Australians urged to check and secure social media accounts Related: Government investigates data breach revealing details of 774,000 migrants Continue reading... [...]
