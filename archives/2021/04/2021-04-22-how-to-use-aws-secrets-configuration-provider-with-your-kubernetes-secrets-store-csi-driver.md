Title: How to use AWS Secrets & Configuration Provider with your Kubernetes Secrets Store CSI driver
Date: 2021-04-22T22:58:26+00:00
Author: Tracy Pierce
Category: AWS Security
Tags: Advanced (300);Amazon Elastic Kubernetes Service;AWS Secrets Manager;Security, Identity, & Compliance;ASCP;CSI;Kubernetes;secrets;Security Blog
Slug: how-to-use-aws-secrets-configuration-provider-with-your-kubernetes-secrets-store-csi-driver

[Source](https://aws.amazon.com/blogs/security/how-to-use-aws-secrets-configuration-provider-with-kubernetes-secrets-store-csi-driver/){:target="_blank" rel="noopener"}

> AWS Secrets Manager now enables you to securely retrieve secrets from AWS Secrets Manager for use in your Amazon Elastic Kubernetes Service (Amazon EKS) Kubernetes pods. With the launch of AWS Secrets and Config Provider (ASCP), you now have an easy-to-use plugin for the industry-standard Kubernetes Secrets Store and Container Storage Interface (CSI) driver, used [...]
