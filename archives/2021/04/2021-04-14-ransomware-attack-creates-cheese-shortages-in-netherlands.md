Title: Ransomware Attack Creates Cheese Shortages in Netherlands
Date: 2021-04-14T19:55:41+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities
Slug: ransomware-attack-creates-cheese-shortages-in-netherlands

[Source](https://threatpost.com/ransomware-cheese-shortages-netherlands/165407/){:target="_blank" rel="noopener"}

> Not a Gouda situation: An attack on a logistics firm is suspected to be related to Microsoft Exchange server flaw. [...]
