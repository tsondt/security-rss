Title: What is operations-centric security?
Date: 2021-04-06T07:30:10+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: what-is-operations-centric-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/06/what_is_operations_centric_security/){:target="_blank" rel="noopener"}

> Let’s find out... with Cybereason CEO Lior Div Sponsored The SolarWinds attacks compromised tens of thousands of systems across US federal government agencies and private sector companies alike. The US will feel its effects for years, and it was largely avoidable. In fact, according to Lior Div, CEO and co-founder of Cybereason, if those systems had been using a concept called operation-centric security, they could have spotted it immediately.... [...]
