Title: Security vendor Proofpoint snapped up by private equity for $12.3bn but still in search of profit
Date: 2021-04-26T16:15:09+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: security-vendor-proofpoint-snapped-up-by-private-equity-for-123bn-but-still-in-search-of-profit

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/26/proofpoint_thoma_bravo/){:target="_blank" rel="noopener"}

> Thoma Bravo follows Sophos purchase with further infosec landgrab Proofpoint has become the latest sizable tech vendor to succumb to private equity after Thoma Bravo succeeded in its $12.3bn grasp for the infosec giant.... [...]
