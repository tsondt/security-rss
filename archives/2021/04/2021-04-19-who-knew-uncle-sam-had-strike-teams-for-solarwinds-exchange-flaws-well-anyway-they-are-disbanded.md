Title: Who knew Uncle Sam had strike teams for SolarWinds, Exchange flaws? Well, anyway, they are disbanded
Date: 2021-04-19T22:28:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: who-knew-uncle-sam-had-strike-teams-for-solarwinds-exchange-flaws-well-anyway-they-are-disbanded

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/19/federal_solarwinds_investigation/){:target="_blank" rel="noopener"}

> Lessons learned and mission accomplished, apparently The US government's response groups for dealing with recent SolarWinds and Microsoft Exchange vulnerabilities have reached the end of the road.... [...]
