Title: Malicious cheats for Call of Duty: Warzone are circulating online
Date: 2021-04-03T14:09:25+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Gaming & Culture;Tech;Activision;Call of Duty;cheats;gaming;malware
Slug: malicious-cheats-for-call-of-duty-warzone-are-circulating-online

[Source](https://arstechnica.com/?p=1754269){:target="_blank" rel="noopener"}

> Enlarge (credit: CHUYN / Getty Images ) Criminals have been hiding malware inside publicly available software that purports to be a cheat for Activision’s Call of Duty: Warzone, researchers with the game maker warned earlier this week. Cheats are programs that tamper with in-game events or player interactions so that users gain an unfair advantage over their opponents. The software typically works by accessing computer memory during gameplay and changing health, ammo, score, lives, inventories, or other information. Cheats are almost always forbidden by game makers. On Wednesday, Activision said that a popular cheating site was circulating a fake cheat [...]
