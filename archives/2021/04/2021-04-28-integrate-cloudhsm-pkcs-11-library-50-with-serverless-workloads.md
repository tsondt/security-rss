Title: Integrate CloudHSM PKCS #11 Library 5.0 with serverless workloads
Date: 2021-04-28T19:28:08+00:00
Author: Nikolai Danylchyk
Category: AWS Security
Tags: Advanced (300);AWS CloudHSM;Security, Identity, & Compliance;CloudHSM;CloudHSM client;Security Blog;Serverless
Slug: integrate-cloudhsm-pkcs-11-library-50-with-serverless-workloads

[Source](https://aws.amazon.com/blogs/security/integrate-cloudhsm-pkcs-11-library-5-0-with-serverless-workloads/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) recently released PCKS #11 Library version 5.0 for AWS CloudHSM. This blog post describes the changes implemented in the new library. We also cover a simple encryption example with the Advanced Encryption Standard (AES) algorithm in Galois/Counter Mode (GCM), dockerized, running on AWS Fargate. The primary change from the previous SDK [...]
