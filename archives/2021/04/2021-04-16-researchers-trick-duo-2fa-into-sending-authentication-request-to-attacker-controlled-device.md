Title: Researchers trick Duo 2FA into sending authentication request to attacker-controlled device
Date: 2021-04-16T15:26:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: researchers-trick-duo-2fa-into-sending-authentication-request-to-attacker-controlled-device

[Source](https://portswigger.net/daily-swig/researchers-trick-duo-2fa-into-sending-authentication-request-to-attacker-controlled-device){:target="_blank" rel="noopener"}

> Something you know, something you hack [...]
