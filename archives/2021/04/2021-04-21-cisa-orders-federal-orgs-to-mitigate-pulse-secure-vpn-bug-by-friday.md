Title: CISA orders federal orgs to mitigate Pulse Secure VPN bug by Friday
Date: 2021-04-21T11:53:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-orders-federal-orgs-to-mitigate-pulse-secure-vpn-bug-by-friday

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-federal-orgs-to-mitigate-pulse-secure-vpn-bug-by-friday/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) has issued a new emergency directive ordering federal agencies to mitigate an actively exploited vulnerability in Pulse Connect Secure (PCS) VPN appliances on their networks by Friday. [...]
