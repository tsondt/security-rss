Title: Fugitive Identified on YouTube By His Distinctive Tattoos
Date: 2021-04-01T14:39:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: identification;Italy;operational security;videos
Slug: fugitive-identified-on-youtube-by-his-distinctive-tattoos

[Source](https://www.schneier.com/blog/archives/2021/04/fugitive-identified-on-youtube-by-his-distinctive-tattoos.html){:target="_blank" rel="noopener"}

> A mafia fugitive hiding out in the Dominican Republic was arrested when investigators found his YouTube cooking channel and identified him by his distinctive arm tattoos. [...]
