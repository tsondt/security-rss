Title: Babuk Ransomware Gang Targets Washington D.C. Police
Date: 2021-04-27T15:35:17+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Government;Malware;Vulnerabilities
Slug: babuk-ransomware-gang-targets-washington-dc-police

[Source](https://threatpost.com/babuk-ransomware-washington-dc-police/165616/){:target="_blank" rel="noopener"}

> The RaaS developers thumbed their noses at police, saying “We find 0 day before you.” [...]
