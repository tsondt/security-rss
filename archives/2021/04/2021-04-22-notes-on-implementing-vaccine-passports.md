Title: Notes on Implementing Vaccine Passports
Date: 2021-04-22T15:45:14+00:00
Author: Eric Rescorla
Category: Mozilla Security
Tags: Privacy
Slug: notes-on-implementing-vaccine-passports

[Source](https://blog.mozilla.org/blog/2021/04/22/notes-on-implementing-vaccine-passports/){:target="_blank" rel="noopener"}

> Now that we’re starting to get widespread COVID vaccination “vaccine passports” have started to become more relevant. The idea behind a vaccine passport is that you would have some kind... Read more The post Notes on Implementing Vaccine Passports appeared first on The Mozilla Blog. [...]
