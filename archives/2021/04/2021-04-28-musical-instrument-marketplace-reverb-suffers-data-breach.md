Title: Musical instrument marketplace Reverb suffers data breach
Date: 2021-04-28T12:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: musical-instrument-marketplace-reverb-suffers-data-breach

[Source](https://portswigger.net/daily-swig/musical-instrument-marketplace-reverb-suffers-data-breach){:target="_blank" rel="noopener"}

> Chicago-based company confirms security incident, asks users to reset passwords [...]
