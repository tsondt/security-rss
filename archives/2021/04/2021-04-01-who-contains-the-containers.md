Title: Who Contains the Containers?
Date: 2021-04-01T09:06:00-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: who-contains-the-containers

[Source](https://googleprojectzero.blogspot.com/2021/04/who-contains-containers.html){:target="_blank" rel="noopener"}

> Posted by James Forshaw, Project Zero This is a short blog post about a research project I conducted on Windows Server Containers that resulted in four privilege escalations which Microsoft fixed in March 2021. In the post, I describe what led to this research, my research process, and insights into what to look for if you’re researching this area. Windows Containers Background Windows 10 and its server counterparts added support for application containerization. The implementation in Windows is similar in concept to Linux containers, but of course wildly different. The well-known Docker platform supports Windows containers which leads to the [...]
