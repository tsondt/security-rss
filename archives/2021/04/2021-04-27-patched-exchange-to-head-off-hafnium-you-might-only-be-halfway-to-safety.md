Title: Patched Exchange to head off Hafnium? You might only be halfway to safety
Date: 2021-04-27T07:00:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: patched-exchange-to-head-off-hafnium-you-might-only-be-halfway-to-safety

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/27/exchange_hafnium_sophos/){:target="_blank" rel="noopener"}

> Office 365 shop? You may be exposed too. Here’s why – according to Sophos Promo If you’re running Microsoft Exchange anywhere in your organisation and you’re not extremely concerned about the threat from Hafnium, you haven’t been paying attention this year.... [...]
