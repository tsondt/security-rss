Title: Zero-Day Bug Impacts Problem-Plagued Cisco SOHO Routers
Date: 2021-04-08T21:07:47+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: zero-day-bug-impacts-problem-plagued-cisco-soho-routers

[Source](https://threatpost.com/zero-day-bug-soho-routers/165321/){:target="_blank" rel="noopener"}

> Cisco says it will not patch three small business router models and one VPN firewall device with critical vulnerabilities. [...]
