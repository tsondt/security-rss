Title: Multi-Gov Task Force Plans to Take Down the Ransomware Economy
Date: 2021-04-29T17:39:37+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware;Vulnerabilities;Web Security
Slug: multi-gov-task-force-plans-to-take-down-the-ransomware-economy

[Source](https://threatpost.com/gov-task-force-ransomware-economy/165715/){:target="_blank" rel="noopener"}

> A coalition of 60 global entities (including the DoJ) has proposed a sweeping plan to hunt down and disrupt ransomware gangs by going after their financial operations. [...]
