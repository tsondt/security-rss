Title: Microsoft Office SharePoint Targeted With High-Risk Phish, Ransomware Attacks
Date: 2021-04-28T19:00:55+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: microsoft-office-sharepoint-targeted-with-high-risk-phish-ransomware-attacks

[Source](https://threatpost.com/sharepoint-phish-ransomware-attacks/165671/){:target="_blank" rel="noopener"}

> SharePoint servers are being picked at with high-risk, legitimate-looking, branded phish messages and preyed on by a ransomware gang using an old bug. [...]
