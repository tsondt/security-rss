Title: Over 600,000 stolen credit cards leaked after Swarmshop hack
Date: 2021-04-08T15:58:44-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: over-600000-stolen-credit-cards-leaked-after-swarmshop-hack

[Source](https://www.bleepingcomputer.com/news/security/over-600-000-stolen-credit-cards-leaked-after-swarmshop-hack/){:target="_blank" rel="noopener"}

> The hacking spree targeting underground marketplaces has claimed another victim as a database from card shop Swarmshop emerged on another forum. [...]
