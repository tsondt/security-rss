Title: Security Bug Allows Attackers to Brick Kubernetes Clusters
Date: 2021-04-14T20:56:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: security-bug-allows-attackers-to-brick-kubernetes-clusters

[Source](https://threatpost.com/security-bug-brick-kubernetes-clusters/165413/){:target="_blank" rel="noopener"}

> The vulnerability is triggered when a cloud container pulls a malicious image from a registry. [...]
