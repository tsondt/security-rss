Title: Have I Been Pwned adds search for leaked Facebook phone numbers
Date: 2021-04-06T13:50:58-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: have-i-been-pwned-adds-search-for-leaked-facebook-phone-numbers

[Source](https://www.bleepingcomputer.com/news/security/have-i-been-pwned-adds-search-for-leaked-facebook-phone-numbers/){:target="_blank" rel="noopener"}

> Facebook users can now use the Have I Been Pwned data breach notification site to check if their phone number was exposed in the social site's recent data leak. [...]
