Title: Biden Administration Imposes Sanctions on Russia for SolarWinds
Date: 2021-04-20T11:19:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberespionage;espionage;hacking;national security policy;Russia
Slug: biden-administration-imposes-sanctions-on-russia-for-solarwinds

[Source](https://www.schneier.com/blog/archives/2021/04/biden-administration-imposes-sanctions-on-russia-for-solarwinds.html){:target="_blank" rel="noopener"}

> On April 15, the Biden administration both formally attributed the SolarWinds espionage campaign to the Russian Foreign Intelligence Service (SVR), and imposed a series of sanctions designed to punish the country for the attack and deter future attacks. I will leave it to those with experience in foreign relations to convince me that the response is sufficient to deter future operations. To me, it feels like too little. The New York Times reports that “the sanctions will be among what President Biden’s aides say are ‘seen and unseen steps in response to the hacking,” which implies that there’s more we [...]
