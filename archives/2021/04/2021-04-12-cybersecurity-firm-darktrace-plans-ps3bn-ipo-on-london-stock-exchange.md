Title: Cybersecurity firm Darktrace plans £3bn IPO on London Stock Exchange
Date: 2021-04-12T09:38:47+00:00
Author: Kalyeena Makortoff
Category: The Guardian
Tags: IPOs;Data and computer security;London Stock Exchange;Stock markets;FTSE;Business;Technology;UK news;US news
Slug: cybersecurity-firm-darktrace-plans-ps3bn-ipo-on-london-stock-exchange

[Source](https://www.theguardian.com/business/2021/apr/12/cybersecurity-darktrace-ipo-london-stock-exchange){:target="_blank" rel="noopener"}

> Cambridge-based company claims to be first to use AI to detect cybersecurity threats on a large scale The cybersecurity firm Darktrace has announced plans to float on the London Stock Exchange, in a move that will reportedly value the Cambridge-based company at £3bn. It is the first big company to have chosen the City for its initial public offering (IPO) since Deliveroo’s disappointing stock market debut last month. Continue reading... [...]
