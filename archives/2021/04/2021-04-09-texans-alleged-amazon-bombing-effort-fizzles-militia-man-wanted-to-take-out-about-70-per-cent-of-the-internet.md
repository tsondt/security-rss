Title: Texan's alleged Amazon bombing effort fizzles: Militia man wanted to take out 'about 70 per cent of the internet'
Date: 2021-04-09T21:57:50+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: texans-alleged-amazon-bombing-effort-fizzles-militia-man-wanted-to-take-out-about-70-per-cent-of-the-internet

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/09/amazon_bombing_plot/){:target="_blank" rel="noopener"}

> Someone hasn't heard of redundancy The US Justice Department on Friday announced the arrest of Seth Aaron Pendley, 28, for allegedly planning to blow up a single Amazon data center in Ashburn, Virginia, which he thought would knock out around 70 per cent of the internet.... [...]
