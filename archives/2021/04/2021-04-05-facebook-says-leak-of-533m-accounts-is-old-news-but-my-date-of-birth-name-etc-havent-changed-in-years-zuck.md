Title: Facebook says leak of 533m accounts is old news. But my date of birth, name, etc haven't changed in years, Zuck
Date: 2021-04-05T21:05:13+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: facebook-says-leak-of-533m-accounts-is-old-news-but-my-date-of-birth-name-etc-havent-changed-in-years-zuck

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/05/facebook_data_dump/){:target="_blank" rel="noopener"}

> Account info swiped in 2019 via security hole, sold online, now given away for free Reams of personal data – including phone numbers, email addresses, and birthdays – obtained from 533 million Facebook accounts was offered to all for free on a cyber-crime forum over the weekend.... [...]
