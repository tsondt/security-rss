Title: How To Defend the Extended Network Against Web Risks
Date: 2021-04-05T17:28:13+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: Hacks;InfoSec Insider;Vulnerabilities;Web Security
Slug: how-to-defend-the-extended-network-against-web-risks

[Source](https://threatpost.com/how-to-defend-the-extended-network-against-web-risks/165236/){:target="_blank" rel="noopener"}

> Aamir Lakhani, cybersecurity researcher for Fortinet’s FortiGuard Labs, discusses criminals flocking to web server and browser attacks, and what to do about it. [...]
