Title: Asteelflash electronics maker hit by REvil ransomware attack
Date: 2021-04-02T14:17:55-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: asteelflash-electronics-maker-hit-by-revil-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/asteelflash-electronics-maker-hit-by-revil-ransomware-attack/){:target="_blank" rel="noopener"}

> Asteelflash, a leading French electronics manufacturing services company, has suffered a cyberattack by the REvil ransomware gang who is demanding a $24 million ransom. [...]
