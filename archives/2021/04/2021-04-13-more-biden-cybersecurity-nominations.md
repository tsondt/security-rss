Title: More Biden Cybersecurity Nominations
Date: 2021-04-13T14:13:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;national security policy;NSA
Slug: more-biden-cybersecurity-nominations

[Source](https://www.schneier.com/blog/archives/2021/04/more-biden-cybersecurity-nominations.html){:target="_blank" rel="noopener"}

> News : President Biden announced key cybersecurity leadership nominations Monday, proposing Jen Easterly as the next head of the Cybersecurity and Infrastructure Security Agency and John “Chris” Inglis as the first ever national cyber director (NCD). I know them both, and think they’re both good choices. More news. [...]
