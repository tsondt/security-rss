Title: Ransomware gang leaks data from Stanford, Maryland universities
Date: 2021-04-03T09:31:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-leaks-data-from-stanford-maryland-universities

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-leaks-data-from-stanford-maryland-universities/){:target="_blank" rel="noopener"}

> Stolen personal and financial information of students at Stanford Medicine, University of Maryland Baltimore (UMB), and the University of California was leaked online by the Clop ransomware group earlier this week. [...]
