Title: There’s a whole wide world of web application firewall options – so how do you choose the right one?
Date: 2021-04-08T07:30:04+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: theres-a-whole-wide-world-of-web-application-firewall-options-so-how-do-you-choose-the-right-one

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/08/choosing_the_right_waf_/){:target="_blank" rel="noopener"}

> Take the heat out of your firewall deployment Webcast If you’ve got an application which faces the web, no one would dispute that you should probably have a web application firewall sitting in front of it.... [...]
