Title: Are You One of the 533M People Who Got Facebooked?
Date: 2021-04-06T18:55:53+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;The Coming Storm;Alon Gal;Facebook breach;HaveIBeenPwned.com;SIM swapping;Under the Breach
Slug: are-you-one-of-the-533m-people-who-got-facebooked

[Source](https://krebsonsecurity.com/2021/04/are-you-one-of-the-533m-people-who-got-facebooked/){:target="_blank" rel="noopener"}

> Ne’er-do-wells leaked personal data — including phone numbers — for some 553 million Facebook users this week. Facebook says the data was collected before 2020 when it changed things to prevent such information from being scraped from profiles. To my mind, this just reinforces the need to remove mobile phone numbers from all of your online accounts wherever feasible. Meanwhile, if you’re a Facebook product user and want to learn if your data was leaked, there are easy ways to find out. The HaveIBeenPwned project, which collects and analyzes hundreds of database dumps containing information about billions of leaked accounts, [...]
