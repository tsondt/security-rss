Title: Mike Lynch-backed Darktrace to file for London IPO in aftermath of Deliveroo flop
Date: 2021-04-12T16:15:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: mike-lynch-backed-darktrace-to-file-for-london-ipo-in-aftermath-of-deliveroo-flop

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/12/darktrace_ipo_confirmed/){:target="_blank" rel="noopener"}

> LSE document confirms AI infosec company's plans British AI-powered infosec biz Darktrace is to go public in England's capital city, the company told the London Stock Exchange this morning.... [...]
