Title: Time to update DNS servers to defend against brace of serious BIND vulnerabilities
Date: 2021-04-29T13:56:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: time-to-update-dns-servers-to-defend-against-brace-of-serious-bind-vulnerabilities

[Source](https://portswigger.net/daily-swig/time-to-update-dns-servers-to-defend-against-brace-of-serious-bind-vulnerabilities){:target="_blank" rel="noopener"}

> Denial of service and buffer overflow bugs addressed in latest security release [...]
