Title: Russian infosec firm Positive Technologies trying to stay positive after US sanctions
Date: 2021-04-16T17:25:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: russian-infosec-firm-positive-technologies-trying-to-stay-positive-after-us-sanctions

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/16/positive_technologies_us_sanctions_groundless/){:target="_blank" rel="noopener"}

> Company insists it's a legit operator that's here to help Positive Technologies has hit back at the US government's "groundless accusations" that it helped the Russian state carry out cyber attacks against the West – by highlighting how "government agencies of different countries" use its products.... [...]
