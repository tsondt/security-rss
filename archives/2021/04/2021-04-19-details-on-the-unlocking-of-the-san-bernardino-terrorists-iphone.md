Title: Details on the Unlocking of the San Bernardino Terrorist’s iPhone
Date: 2021-04-19T11:08:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Apple;exploits;FBI;hacking;iPhone;terrorism
Slug: details-on-the-unlocking-of-the-san-bernardino-terrorists-iphone

[Source](https://www.schneier.com/blog/archives/2021/04/details-on-the-unlocking-of-the-san-bernardino-terrorists-iphone.html){:target="_blank" rel="noopener"}

> The Washington Post has published a long story on the unlocking of the San Bernardino Terrorist’s iPhone 5C in 2016. We all thought it was an Israeli company called Cellebrite. It was actually an Australian company called Azimuth Security. Azimuth specialized in finding significant vulnerabilities. Dowd, a former IBM X-Force researcher whom one peer called “the Mozart of exploit design,” had found one in open-source code from Mozilla that Apple used to permit accessories to be plugged into an iPhone’s lightning port, according to the person. [...] Using the flaw Dowd found, Wang, based in Portland, Ore., created an exploit [...]
