Title: MacKenzie Scott Grant scam more widespread than initially thought
Date: 2021-04-02T03:19:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: mackenzie-scott-grant-scam-more-widespread-than-initially-thought

[Source](https://www.bleepingcomputer.com/news/security/mackenzie-scott-grant-scam-more-widespread-than-initially-thought/){:target="_blank" rel="noopener"}

> A massive phishing campaign reaching tens of thousands of inboxes impersonated the MacKenzie Bezos-Scott grant foundation promising financial benefits to recipients in exchange of a processing fee. [...]
