Title: Telecoms industry facing increased DDoS attacks, report warns
Date: 2021-04-21T10:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: telecoms-industry-facing-increased-ddos-attacks-report-warns

[Source](https://portswigger.net/daily-swig/telecoms-industry-facing-increased-ddos-attacks-report-warns){:target="_blank" rel="noopener"}

> New research from Cloudflare details cyber-attack trends of 2021 so far [...]
