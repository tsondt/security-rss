Title: North Korean hackers return, target infosec researchers in new operation
Date: 2021-04-01T11:56:38+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;hacking;lazarus;North Korea;security researchers
Slug: north-korean-hackers-return-target-infosec-researchers-in-new-operation

[Source](https://arstechnica.com/?p=1753650){:target="_blank" rel="noopener"}

> Enlarge In January, Google and Microsoft outed what they said was North Korean government-sponsored hackers targeting security researchers. The hackers spent weeks using fake Twitter profiles—purportedly belonging to vulnerability researchers—before unleashing an Internet Explorer zero-day and a malicious Visual Studio Project, both of which installed custom malware. Now, the same hackers are back, a Google researcher said on Wednesday, this time with a new batch of social media profiles and a fake company that claims to offer offensive security services, including penetration testing, software security assessments, and software exploits. Once more with feeling The homepage for the fake company is [...]
