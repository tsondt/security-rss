Title: 533M Facebook Accounts Leaked Online: Check if You Are Exposed
Date: 2021-04-05T21:07:42+00:00
Author: Tom Spring
Category: Threatpost
Tags: Breach;Facebook;Privacy;Web Security
Slug: 533m-facebook-accounts-leaked-online-check-if-you-are-exposed

[Source](https://threatpost.com/facebook-accounts-leaked-check-exposed/165245/){:target="_blank" rel="noopener"}

> An estimated 32 million, of the half-billion of Facebook account details posted online, were tied to US-based accounts. [...]
