Title: Second Click Here to Kill Everybody Sale
Date: 2021-04-28T01:22:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: second-click-here-to-kill-everybody-sale

[Source](https://www.schneier.com/blog/archives/2021/04/second-click-here-to-kill-everybody-sale.html){:target="_blank" rel="noopener"}

> For a limited time, I am selling signed copies of Click Here to Kill Everybody in hardcover for just $6, plus shipping. I have 600 copies of the book available. When they’re gone, the sale is over and the price will revert to normal. Order here. Please be patient on delivery. It’s a lot of work to sign and mail hundreds of books. I try to do some each day, but sometimes I can’t. And the pandemic can cause mail slowdowns all over the world. [...]
