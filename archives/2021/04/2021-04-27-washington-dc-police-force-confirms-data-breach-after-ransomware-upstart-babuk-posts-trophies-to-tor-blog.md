Title: Washington DC police force confirms data breach after ransomware upstart Babuk posts trophies to Tor blog
Date: 2021-04-27T12:25:15+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: washington-dc-police-force-confirms-data-breach-after-ransomware-upstart-babuk-posts-trophies-to-tor-blog

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/27/washington_dc_police_ransomware/){:target="_blank" rel="noopener"}

> Newish criminal gang 'trying to make a name for themselves' Ransomware criminals have posted trophy pictures on their Tor blog after attacking the police force for US capital Washington DC.... [...]
