Title: Average British computer criminal is young, male and not highly skilled, researcher finds
Date: 2021-04-13T09:27:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: average-british-computer-criminal-is-young-male-and-not-highly-skilled-researcher-finds

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/13/computer_misuse_act_convictions_analysis/){:target="_blank" rel="noopener"}

> Analysis of Computer Misuse Act cases also draws heavily on El Reg archives An academic researcher has analysed more than 100 Computer Misuse Act cases to paint a picture of the sort of computer-enabled criminals who plague Great Britain’s digital doings in the 21st Century.... [...]
