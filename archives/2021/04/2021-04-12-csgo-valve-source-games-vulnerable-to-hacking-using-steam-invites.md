Title: CS:GO, Valve Source games vulnerable to hacking using Steam invites
Date: 2021-04-12T16:44:06-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: csgo-valve-source-games-vulnerable-to-hacking-using-steam-invites

[Source](https://www.bleepingcomputer.com/news/security/cs-go-valve-source-games-vulnerable-to-hacking-using-steam-invites/){:target="_blank" rel="noopener"}

> A group of security researchers known as the Secret Club took it to Twitter to report a remote code execution bug in the Source 3D game engine developed by Valve and used for building games with tens of millions of unique players. [...]
