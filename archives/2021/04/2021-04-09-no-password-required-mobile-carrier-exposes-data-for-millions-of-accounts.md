Title: No password required: Mobile carrier exposes data for millions of accounts
Date: 2021-04-09T19:23:36+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;data leak;home mobile;my mobile account;personal information;privacy;q link wireless
Slug: no-password-required-mobile-carrier-exposes-data-for-millions-of-accounts

[Source](https://arstechnica.com/?p=1755853){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Q Link Wireless, a provider of low-cost mobile phone and data services to 2 million US-based customers, has been making sensitive account data available to anyone who knows a valid phone number on the carrier’s network, an analysis of the company’s account management app shows. Dania, Florida-based Q Link Wireless is what’s known as a Mobile Virtual Network Operator, meaning it doesn’t operate its own wireless network but rather buys services in bulk from other carriers and resells them. It provides government-subsidized phones and service to low-income consumers through the FCC’s Lifeline Program. It also offers [...]
