Title: Codecov dev tool warns of stolen credentials from compromised script, undiscovered for two months
Date: 2021-04-19T16:03:10+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: codecov-dev-tool-warns-of-stolen-credentials-from-compromised-script-undiscovered-for-two-months

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/19/codecov_warns_of_stolen_credentials/){:target="_blank" rel="noopener"}

> Environment variables full of secrets uploaded to attacker server Codecov, makers of a code coverage tool used by over 29,000 customers, has warned that a compromised script may have stolen credentials over a period of two months, before it was discovered a few weeks ago.... [...]
