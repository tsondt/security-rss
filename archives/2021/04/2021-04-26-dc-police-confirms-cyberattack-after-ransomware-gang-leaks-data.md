Title: DC Police confirms cyberattack after ransomware gang leaks data
Date: 2021-04-26T22:35:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: dc-police-confirms-cyberattack-after-ransomware-gang-leaks-data

[Source](https://www.bleepingcomputer.com/news/security/dc-police-confirms-cyberattack-after-ransomware-gang-leaks-data/){:target="_blank" rel="noopener"}

> The Metropolitan Police Department has confirmed that they suffered a cyberattack after the Babuk ransomware gang leaked screenshots of stolen data. [...]
