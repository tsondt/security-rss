Title: 'Anomalous surge in DNS queries' knocked Microsoft's cloud off the web last week
Date: 2021-04-06T02:41:34+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: anomalous-surge-in-dns-queries-knocked-microsofts-cloud-off-the-web-last-week

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/06/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Top universities hit by data-stealing extortionists in Brief It was a tsunami of DNS queries that ultimately took out a host of Microsoft services, from Xbox Live to Teams, for some netizens about an hour on April Fools' Day, Redmond has said.... [...]
