Title: QNAP fixes critical RCE vulnerabilities in NAS devices
Date: 2021-04-20T15:42:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: qnap-fixes-critical-rce-vulnerabilities-in-nas-devices

[Source](https://portswigger.net/daily-swig/qnap-fixes-critical-rce-vulnerabilities-in-nas-devices){:target="_blank" rel="noopener"}

> Taiwanese vendor also issues mitigations for quartet of other serious flaws [...]
