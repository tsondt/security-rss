Title: Whitepaper available: Classic intrusion analysis frameworks for AWS environments
Date: 2021-04-21T20:13:37+00:00
Author: Tim Rains
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Enterprise;Security Blog;Security controls;Security strategy
Slug: whitepaper-available-classic-intrusion-analysis-frameworks-for-aws-environments

[Source](https://aws.amazon.com/blogs/security/whitepaper-available-classic-intrusion-analysis-frameworks-for-aws-environments/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has released a new whitepaper, Classic intrusion analysis frameworks for AWS environments, to help organizations plan and implement a classic intrusion analysis framework for AWS environments. This whitepaper provides context that will help you understand how such frameworks are used and shows you, in detail, how to mitigate advanced attack tactics [...]
