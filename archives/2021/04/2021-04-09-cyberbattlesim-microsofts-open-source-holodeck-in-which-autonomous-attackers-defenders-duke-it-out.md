Title: CyberBattleSim: Microsoft's open-source Holodeck in which autonomous attackers, defenders duke it out
Date: 2021-04-09T11:06:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: cyberbattlesim-microsofts-open-source-holodeck-in-which-autonomous-attackers-defenders-duke-it-out

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/09/cyberbattlesim_not_a_poorly_translated/){:target="_blank" rel="noopener"}

> Very 2021 to have AI bots fight in simulated networks for our entertainment (and science) Microsoft has open-sourced software that pits machine-learning-powered network intruders against automated defenders inside virtual networks.... [...]
