Title: NSA workflow application Emissary vulnerable to malicious takeover
Date: 2021-04-07T14:28:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nsa-workflow-application-emissary-vulnerable-to-malicious-takeover

[Source](https://portswigger.net/daily-swig/nsa-workflow-application-emissary-vulnerable-to-malicious-takeover){:target="_blank" rel="noopener"}

> Users urged to update their systems after disclosure of serious vulnerabilities [...]
