Title: Nigerian email scammer sent down for 40 months in the US, ordered to pay back $2.7m to victims
Date: 2021-04-14T22:32:01+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: nigerian-email-scammer-sent-down-for-40-months-in-the-us-ordered-to-pay-back-27m-to-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/14/email_scammer_sentenced/){:target="_blank" rel="noopener"}

> Among the victims: the United Nations A Nigerian email scammer based in New York was on Tuesday sentenced to 40 months in prison, and ordered to pay back $2.7m in stolen money.... [...]
