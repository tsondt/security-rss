Title: People with dyslexia have skills that we need, says GCHQ
Date: 2021-04-29T18:00:29+00:00
Author: Rachel Hall
Category: The Guardian
Tags: GCHQ;Dyslexia;Apprenticeships;Data and computer security;Education;Vocational education;UK security and counter-terrorism;Technology;UK news
Slug: people-with-dyslexia-have-skills-that-we-need-says-gchq

[Source](https://www.theguardian.com/uk-news/2021/apr/29/people-with-dyslexia-have-skills-that-we-need-says-gchq){:target="_blank" rel="noopener"}

> UK surveillance agency says it has long valued neuro-diverse analysts – including Alan Turing Apprentices on GCHQ’s scheme are four times more likely to have dyslexia than those on other organisations’ programmes, the agency has said, the result of a drive to recruit those whose brains process information differently. GCHQ says those with dyslexia have valuable skills spotting patterns that others miss – a key area the spy agency wants to encourage as it pivots away from dead letter drops and bugging towards high-tech cybersecurity and data analysis. Related: GCHQ releases 'most difficult puzzle ever' in honour of Alan Turing [...]
