Title: Apple, you've AirDrop'd the ball: Academics detail ways to leak contact info of nearby iThings for spear-phishing
Date: 2021-04-22T08:16:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: apple-youve-airdropd-the-ball-academics-detail-ways-to-leak-contact-info-of-nearby-ithings-for-spear-phishing

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/22/airdrop_contact_leaks/){:target="_blank" rel="noopener"}

> Too bad there's no suggested solution... oh, wait Apple's AirDrop has a couple of potentially annoying privacy weaknesses that Cupertino is so far refusing to address even though a solution has been offered.... [...]
