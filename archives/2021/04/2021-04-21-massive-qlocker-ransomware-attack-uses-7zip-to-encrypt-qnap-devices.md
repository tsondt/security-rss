Title: Massive Qlocker ransomware attack uses 7zip to encrypt QNAP devices
Date: 2021-04-21T13:44:57-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Hardware
Slug: massive-qlocker-ransomware-attack-uses-7zip-to-encrypt-qnap-devices

[Source](https://www.bleepingcomputer.com/news/security/massive-qlocker-ransomware-attack-uses-7zip-to-encrypt-qnap-devices/){:target="_blank" rel="noopener"}

> A massive ransomware campaign targeting QNAP devices worldwide is underway, and users are finding their files now stored in password-protected 7zip archives. [...]
