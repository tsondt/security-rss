Title: HashiCorp is the latest victim of Codecov supply-chain attack
Date: 2021-04-24T02:16:37-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: hashicorp-is-the-latest-victim-of-codecov-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/hashicorp-is-the-latest-victim-of-codecov-supply-chain-attack/){:target="_blank" rel="noopener"}

> Open-source software tools and Vault maker HashiCorp has disclosed a security incident that occurred due to the recent Codecov attack. HashiCorp, a Codecov customer, has stated that the recent Codecov supply-chain attack aimed at collecting developer credentials led to the exposure of HashiCorp's GPG signing key. [...]
