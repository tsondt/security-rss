Title: Atheists appeal to higher power for intercession over alleged sins against privacy
Date: 2021-04-07T06:58:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: atheists-appeal-to-higher-power-for-intercession-over-alleged-sins-against-privacy

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/07/atheists_data_leak/){:target="_blank" rel="noopener"}

> Membership data row ascends to desk of California attorney-general The Atheist Alliance International, an organisation that works to demystify atheism and advocate for secular governance, has taken legal action it hopes will prove that members’ personal data does not remain in the possession of the rival International Association of Atheists.... [...]
