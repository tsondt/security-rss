Title: UC Berkeley confirms data breach, becomes latest victim of Accellion cyber-attack
Date: 2021-04-02T13:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uc-berkeley-confirms-data-breach-becomes-latest-victim-of-accellion-cyber-attack

[Source](https://portswigger.net/daily-swig/uc-berkeley-confirms-data-breach-becomes-latest-victim-of-accellion-cyber-attack){:target="_blank" rel="noopener"}

> File transfer vendor suffered a cyber intrusion in January [...]
