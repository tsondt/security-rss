Title: Raising the bar: Tiki app hands ownership of personal data back to the individual
Date: 2021-04-29T17:26:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: raising-the-bar-tiki-app-hands-ownership-of-personal-data-back-to-the-individual

[Source](https://portswigger.net/daily-swig/raising-the-bar-tiki-app-hands-ownership-of-personal-data-back-to-the-individual){:target="_blank" rel="noopener"}

> More than 10,000 users have already signed up for access prior to Tiki’s launch in June [...]
