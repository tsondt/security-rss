Title: Biden Races to Shore Up Power Grid Against Hacks
Date: 2021-04-15T20:09:21+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Critical Infrastructure;Government;Hacks;Malware
Slug: biden-races-to-shore-up-power-grid-against-hacks

[Source](https://threatpost.com/biden-power-grid-hacks/165428/){:target="_blank" rel="noopener"}

> A 100-day race to boost cybersecurity will rely on incentives rather than regulation, the White House said. [...]
