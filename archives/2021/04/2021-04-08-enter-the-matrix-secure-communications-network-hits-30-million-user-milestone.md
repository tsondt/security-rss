Title: Enter the Matrix: Secure communications network hits 30 million user milestone
Date: 2021-04-08T14:43:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: enter-the-matrix-secure-communications-network-hits-30-million-user-milestone

[Source](https://portswigger.net/daily-swig/enter-the-matrix-secure-communications-network-hits-30-million-user-milestone){:target="_blank" rel="noopener"}

> We caught up with Matrix co-founders to find out how the project is developing and why the future of internet freedom may depend on decentralization [...]
