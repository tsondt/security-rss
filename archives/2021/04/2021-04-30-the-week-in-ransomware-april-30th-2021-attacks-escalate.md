Title: The Week in Ransomware - April 30th 2021 - Attacks Escalate
Date: 2021-04-30T17:46:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-april-30th-2021-attacks-escalate

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-30th-2021-attacks-escalate/){:target="_blank" rel="noopener"}

> Ransomware gangs continue to target organizations large and small, including a brazen attack on the Washington DC police department. [...]
