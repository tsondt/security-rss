Title: Facebook: Stolen Data Scraped from Platform in 2019
Date: 2021-04-07T13:00:27+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy;Vulnerabilities
Slug: facebook-stolen-data-scraped-from-platform-in-2019

[Source](https://threatpost.com/facebook-stolen-data-scraped/165285/){:target="_blank" rel="noopener"}

> The flaw that caused the leak of personal data of more than 533 million users over the weekend no longer exists; however, the social media giant still faces an investigation by EU regulators. [...]
