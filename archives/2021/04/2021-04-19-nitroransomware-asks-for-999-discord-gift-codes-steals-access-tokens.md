Title: NitroRansomware Asks for $9.99 Discord Gift Codes, Steals Access Tokens
Date: 2021-04-19T19:23:07+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: nitroransomware-asks-for-999-discord-gift-codes-steals-access-tokens

[Source](https://threatpost.com/nitroransomware-discord-gift-codes/165488/){:target="_blank" rel="noopener"}

> The malware seems like a silly coding lark at first, but further exploration shows it can wreak serious damage in follow-on attacks. [...]
