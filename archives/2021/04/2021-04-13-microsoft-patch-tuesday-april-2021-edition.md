Title: Microsoft Patch Tuesday, April 2021 Edition
Date: 2021-04-13T23:12:19+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;CVE-2021-28310;CVE-2021-28480;CVE-2021-28481;CVE-2021-28482;CVE-2021-28483;Kaspersky Lab;Microsoft Exchange server vulnerabilities;Microsoft Patch Tuesday April 2021;U.S. National Security Agency;windows
Slug: microsoft-patch-tuesday-april-2021-edition

[Source](https://krebsonsecurity.com/2021/04/microsoft-patch-tuesday-april-2021-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to plug at least 110 security holes in its Windows operating systems and other products. The patches include four security fixes for Microsoft Exchange Server — the same systems that have been besieged by attacks on four separate (and zero-day) bugs in the email software over the past month. Redmond also patched a Windows flaw that is actively being exploited in the wild. Nineteen of the vulnerabilities fixed this month earned Microsoft’s most-dire “Critical” label, meaning they could be used by malware or malcontents to seize remote control over vulnerable Windows systems without any help from [...]
