Title: Pressure grows on Valve to unplug Steam gaming platform vulnerabilities
Date: 2021-04-13T16:14:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: pressure-grows-on-valve-to-unplug-steam-gaming-platform-vulnerabilities

[Source](https://portswigger.net/daily-swig/pressure-grows-on-valve-to-unplug-steam-gaming-platform-vulnerabilities){:target="_blank" rel="noopener"}

> Two-year-old RCE flaws still unpatched, bounty hunters claim [...]
