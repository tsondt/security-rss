Title: 100,000 Google Sites Used to Install SolarMarker RAT
Date: 2021-04-14T14:48:27+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 100000-google-sites-used-to-install-solarmarker-rat

[Source](https://threatpost.com/google-sites-solarmarket-rat/165396/){:target="_blank" rel="noopener"}

> Search-engine optimization (SEO) tactics direct users searching for common business forms such as invoices, receipts or other templates to hacker-controlled Google-hosted domains. [...]
