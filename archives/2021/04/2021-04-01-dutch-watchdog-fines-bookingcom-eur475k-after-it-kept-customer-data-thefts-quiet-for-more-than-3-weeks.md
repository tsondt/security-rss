Title: Dutch watchdog fines Booking.com €475k after it kept customer data thefts quiet for more than 3 weeks
Date: 2021-04-01T16:12:11+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: dutch-watchdog-fines-bookingcom-eur475k-after-it-kept-customer-data-thefts-quiet-for-more-than-3-weeks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/01/booking_dot_com_fine/){:target="_blank" rel="noopener"}

> Thousands of people's personal information purloined after UAE hotels compromised The Netherlands Data Protection Authority has fined Booking.com €475,000 for notifying it too late that criminals had accessed the data of 4,109 people who booked a hotel room via the website.... [...]
