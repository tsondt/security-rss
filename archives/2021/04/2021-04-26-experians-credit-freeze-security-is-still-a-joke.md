Title: Experian’s Credit Freeze Security is Still a Joke
Date: 2021-04-26T21:58:24+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;CreditLock;Dune Thomas;Equifax;Experian;security freeze;TransUnion
Slug: experians-credit-freeze-security-is-still-a-joke

[Source](https://krebsonsecurity.com/2021/04/experians-credit-freeze-security-is-still-a-joke/){:target="_blank" rel="noopener"}

> In 2017, KrebsOnSecurity showed how easy it is for identity thieves to undo a consumer’s request to freeze their credit file at Experian, one of the big three consumer credit bureaus in the United States. Last week, KrebsOnSecurity heard from a reader who had his freeze thawed without authorization through Experian’s website, and it reminded me of how truly broken authentication and security remains in the credit bureau space. Experian’s page for retrieving someone’s credit freeze PIN requires little more information than has already been leaked by big-three bureau Equifax and a myriad other breaches. Dune Thomas is a software [...]
