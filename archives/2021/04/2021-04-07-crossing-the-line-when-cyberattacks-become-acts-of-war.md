Title: Crossing the Line: When Cyberattacks Become Acts of War
Date: 2021-04-07T17:57:20+00:00
Author: Saryu Nayyar
Category: Threatpost
Tags: Government;Hacks;InfoSec Insider;Malware;Web Security
Slug: crossing-the-line-when-cyberattacks-become-acts-of-war

[Source](https://threatpost.com/crossing-line-cyberattack-act-war/165290/){:target="_blank" rel="noopener"}

> Saryu Nayyar, CEO at Gurucul, discusses the new Cold War and the potential for a cyberattack to prompt military action. [...]
