Title: Report: Aussie biz Azimuth cracked San Bernardino shooter’s iPhone, ending Apple-FBI privacy standoff
Date: 2021-04-14T21:37:08+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: report-aussie-biz-azimuth-cracked-san-bernardino-shooters-iphone-ending-apple-fbi-privacy-standoff

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/14/azimuth_fbi_iphone/){:target="_blank" rel="noopener"}

> Mozilla-authored code in iOS exploited, since patched, it is claimed Australian security firm Azimuth has been identified as the experts who managed to crack a mass shooter's iPhone that was at the center of an encryption standoff between the FBI and Apple.... [...]
