Title: DigitalOcean data breach exposes customer billing information
Date: 2021-04-28T16:09:13-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: digitalocean-data-breach-exposes-customer-billing-information

[Source](https://www.bleepingcomputer.com/news/security/digitalocean-data-breach-exposes-customer-billing-information/){:target="_blank" rel="noopener"}

> Cloud hosting provider DigitalOcean has disclosed a data breach after a flaw exposed customers' billing information. [...]
