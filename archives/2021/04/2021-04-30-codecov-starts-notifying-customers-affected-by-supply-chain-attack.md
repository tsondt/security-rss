Title: Codecov starts notifying customers affected by supply-chain attack
Date: 2021-04-30T02:43:43-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: codecov-starts-notifying-customers-affected-by-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/codecov-starts-notifying-customers-affected-by-supply-chain-attack/){:target="_blank" rel="noopener"}

> Codecov has now started notifying the maintainers of software repositories affected by the recent supply-chain attack. These notifications, delivered via both email and the Codecov application interface, state that the company believes the affected repositories were downloaded by threat actors. [...]
