Title: REvil gang tries to extort Apple, threatens to sell stolen blueprints
Date: 2021-04-20T16:39:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: revil-gang-tries-to-extort-apple-threatens-to-sell-stolen-blueprints

[Source](https://www.bleepingcomputer.com/news/security/revil-gang-tries-to-extort-apple-threatens-to-sell-stolen-blueprints/){:target="_blank" rel="noopener"}

> The REvil ransomware gang asked Apple to "buy back" stolen product blueprints to avoid having them leaked on REvil's leak site before today's Apple Spring Loaded event where the new iMac was introduced. [...]
