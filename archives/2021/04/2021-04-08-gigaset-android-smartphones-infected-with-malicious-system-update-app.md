Title: Gigaset Android smartphones infected with malicious system update app
Date: 2021-04-08T10:42:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: gigaset-android-smartphones-infected-with-malicious-system-update-app

[Source](https://portswigger.net/daily-swig/gigaset-android-smartphones-infected-with-malicious-system-update-app){:target="_blank" rel="noopener"}

> Vendor has confirmed the security fracas following what may have been a supply chain attack [...]
