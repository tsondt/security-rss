Title: Hacker leaks 20 million alleged BigBasket user records for free
Date: 2021-04-25T16:28:55-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hacker-leaks-20-million-alleged-bigbasket-user-records-for-free

[Source](https://www.bleepingcomputer.com/news/security/hacker-leaks-20-million-alleged-bigbasket-user-records-for-free/){:target="_blank" rel="noopener"}

> A threat actor has leaked approximately 20 million BigBasket user records containing personal information and hashed passwords on a popular hacking forum. [...]
