Title: Swiss Post launches public bug bounty program with YesWeHack
Date: 2021-04-16T13:22:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: swiss-post-launches-public-bug-bounty-program-with-yeswehack

[Source](https://portswigger.net/daily-swig/swiss-post-launches-public-bug-bounty-program-with-yeswehack){:target="_blank" rel="noopener"}

> Switzerland’s national mail carrier asks more security researchers to test its networks [...]
