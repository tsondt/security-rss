Title: Indian defense chief admits China’s cyber-weapons would ‘disrupt large number of systems’ whenever Beijing presses the button
Date: 2021-04-08T04:14:28+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: indian-defense-chief-admits-chinas-cyber-weapons-would-disrupt-large-number-of-systems-whenever-beijing-presses-the-button

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/08/india_admits_china_outmatches_cyber_defences/){:target="_blank" rel="noopener"}

> Working to improve 'cyberwalls', but for now swift recovery is main strategy Video The highest-ranked officer in India’s armed forces has admitted that China has cyber-war capabilities that can overwhelm his nation’s defenses and suggested that only cross-forces collaboration will get India to parity with its giant neighbor.... [...]
