Title: A Tale of Two Hacks: From SolarWinds to Microsoft Exchange
Date: 2021-04-30T17:03:51+00:00
Author: Oliver Tavakoli
Category: Threatpost
Tags: Government;Hacks;InfoSec Insider;Malware
Slug: a-tale-of-two-hacks-from-solarwinds-to-microsoft-exchange

[Source](https://threatpost.com/solarwinds-hack-seismic-shift/165758/){:target="_blank" rel="noopener"}

> Oliver Tavakoli, CTO of Vectra AI, discusses the differences between the massive supply-chain hack and the Exchange zero-day attacks, and their legacy and ramifications for security professionals. [...]
