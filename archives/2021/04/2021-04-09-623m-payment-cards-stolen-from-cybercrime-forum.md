Title: 623M Payment Cards Stolen from Cybercrime Forum
Date: 2021-04-09T18:40:48+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Web Security
Slug: 623m-payment-cards-stolen-from-cybercrime-forum

[Source](https://threatpost.com/623m-payment-cards-stolen-from-cybercrime-forum/165336/){:target="_blank" rel="noopener"}

> The database was subsequently leaked elsewhere, imperiling consumers from the U.S. and around the world. [...]
