Title: Computer security world in mourning over death of Dan Kaminsky, aged 42
Date: 2021-04-25T04:10:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: computer-security-world-in-mourning-over-death-of-dan-kaminsky-aged-42

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/25/dan_kaminsky_obituary/){:target="_blank" rel="noopener"}

> DEF CON hails 'an icon in all the positive ways' Obit Celebrated information security researcher Dan Kaminsky, known not just for his technical ability but also for his compassion and support for those in his industry, has died. He was 42.... [...]
