Title: REvil ransomware now changes password to auto-login in Safe Mode
Date: 2021-04-07T16:06:13-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: revil-ransomware-now-changes-password-to-auto-login-in-safe-mode

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-now-changes-password-to-auto-login-in-safe-mode/){:target="_blank" rel="noopener"}

> A recent change to the REvil ransomware allows the threat actors to automate file encryption via Safe Mode after changing Windows passwords. [...]
