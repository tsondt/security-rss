Title: Adult content from hundreds of OnlyFans creators leaked online
Date: 2021-04-05T18:28:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: adult-content-from-hundreds-of-onlyfans-creators-leaked-online

[Source](https://www.bleepingcomputer.com/news/security/adult-content-from-hundreds-of-onlyfans-creators-leaked-online/){:target="_blank" rel="noopener"}

> After a shared Google Drive was posted online containing the private videos and images from many OnlyFans accounts, a researcher has created a tool allowing content creators to check if they are part of the leak. [...]
