Title: UK's National Cyber Security Centre recommends password generation idea suggested by El Reg commenter
Date: 2021-04-09T15:58:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uks-national-cyber-security-centre-recommends-password-generation-idea-suggested-by-el-reg-commenter

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/09/ncsc_secure_passwords_three_words_advice/){:target="_blank" rel="noopener"}

> Who says everything below the line is a cesspit of useless filth? Nearly a third of Britons use the name of their pet or a family member as a password, the National Cyber Security Centre has said as it advised folk to adopt what looks very much like a Register forum user's suggestion for secure password generation.... [...]
