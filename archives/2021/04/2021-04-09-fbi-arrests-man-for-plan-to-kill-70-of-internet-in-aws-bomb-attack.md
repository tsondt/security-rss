Title: FBI arrests man for plan to kill 70% of Internet in AWS bomb attack
Date: 2021-04-09T16:29:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-arrests-man-for-plan-to-kill-70-of-internet-in-aws-bomb-attack

[Source](https://www.bleepingcomputer.com/news/security/fbi-arrests-man-for-plan-to-kill-70-percent-of-internet-in-aws-bomb-attack/){:target="_blank" rel="noopener"}

> The FBI arrested a Texas man on Thursday for allegedly planning to "kill of about 70% of the internet" in a bomb attack targeting an Amazon Web Services (AWS) data center on Smith Switch Road in Ashburn, Virginia. [...]
