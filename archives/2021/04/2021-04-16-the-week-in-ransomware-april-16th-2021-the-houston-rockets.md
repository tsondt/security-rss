Title: The Week in Ransomware - April 16th 2021 - The Houston Rockets
Date: 2021-04-16T19:14:46-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-april-16th-2021-the-houston-rockets

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-16th-2021-the-houston-rockets/){:target="_blank" rel="noopener"}

> It has been a pretty quiet week with only a few large attacks disclosed and only a few new ransomware variants released. The highest-profile attack this week is the NBA's Houston Rockets who were transparent about their ransomware attack. [...]
