Title: Botnet backdoors Microsoft Exchange servers, mines cryptocurrency
Date: 2021-04-22T15:30:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: botnet-backdoors-microsoft-exchange-servers-mines-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/security/botnet-backdoors-microsoft-exchange-servers-mines-cryptocurrency/){:target="_blank" rel="noopener"}

> Unpatched Microsoft Exchange servers are being targeted by the Prometei botnet and added to its operators' army of Monero (XMR) cryptocurrency mining bots. [...]
