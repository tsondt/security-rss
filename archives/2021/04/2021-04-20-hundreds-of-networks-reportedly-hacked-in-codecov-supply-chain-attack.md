Title: Hundreds of networks reportedly hacked in Codecov supply-chain attack
Date: 2021-04-20T03:49:39-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: hundreds-of-networks-reportedly-hacked-in-codecov-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/hundreds-of-networks-reportedly-hacked-in-codecov-supply-chain-attack/){:target="_blank" rel="noopener"}

> More details have emerged on the recent Codecov system breach which is being likened to the SolarWinds hack. In new reporting, investigators have stated that hundreds of customer networks have been breached in the incident, expanding the scope of this system breach beyond just Codecov's systems. [...]
