Title: Passwordstate credentials potentially ‘harvested’ after malicious software update injected into password manager
Date: 2021-04-26T15:57:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: passwordstate-credentials-potentially-harvested-after-malicious-software-update-injected-into-password-manager

[Source](https://portswigger.net/daily-swig/passwordstate-credentials-potentially-harvested-after-malicious-software-update-injected-into-password-manager){:target="_blank" rel="noopener"}

> ‘Number of affected customers still appears to be very low’, says latest vendor update [...]
