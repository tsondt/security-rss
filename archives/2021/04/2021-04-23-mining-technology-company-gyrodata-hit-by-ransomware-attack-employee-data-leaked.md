Title: Mining technology company Gyrodata hit by ransomware attack – employee data leaked
Date: 2021-04-23T14:46:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: mining-technology-company-gyrodata-hit-by-ransomware-attack-employee-data-leaked

[Source](https://portswigger.net/daily-swig/mining-technology-company-gyrodata-hit-by-ransomware-attack-employee-data-leaked){:target="_blank" rel="noopener"}

> Sensitive personal details possibly stolen [...]
