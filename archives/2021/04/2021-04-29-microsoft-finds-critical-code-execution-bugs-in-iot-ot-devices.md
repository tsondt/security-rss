Title: Microsoft finds critical code execution bugs in IoT, OT devices
Date: 2021-04-29T18:05:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-finds-critical-code-execution-bugs-in-iot-ot-devices

[Source](https://www.bleepingcomputer.com/news/security/microsoft-finds-critical-code-execution-bugs-in-iot-ot-devices/){:target="_blank" rel="noopener"}

> Microsoft security researchers have discovered over two dozen critical remote code execution (RCE) vulnerabilities in Internet of Things (IoT) devices and Operational Technology (OT) industrial systems. [...]
