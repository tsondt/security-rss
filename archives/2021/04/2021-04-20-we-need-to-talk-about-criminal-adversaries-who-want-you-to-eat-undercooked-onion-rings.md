Title: We need to talk about criminal adversaries who want you to eat undercooked onion rings
Date: 2021-04-20T11:37:05+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: we-need-to-talk-about-criminal-adversaries-who-want-you-to-eat-undercooked-onion-rings

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/20/cisco_talos_corosi_flaws/){:target="_blank" rel="noopener"}

> Cisco Talos discovers flaws in air fryer, connected chip cooker firm fails to fix Bad news for lockdown slimmers who've ignored advice about not needing to connect every friggin' appliance in their home to the internet: Talos researchers have sniffed out security flaws allowing attackers to hijack your air fryer.... [...]
