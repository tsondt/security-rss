Title: COVID-19 Results for 25% of Wyoming Accidentally Posted Online
Date: 2021-04-29T16:17:38+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Privacy
Slug: covid-19-results-for-25-of-wyoming-accidentally-posted-online

[Source](https://threatpost.com/covid-19-results-accidentally-exposed/165709/){:target="_blank" rel="noopener"}

> Sorry, we’ve upchucked your COVID test results and other medical and personal data into public GitHub storage buckets, the Wyoming Department of Health said. [...]
