Title: The Week in Ransomware - April 23rd 2021 - A brutal week
Date: 2021-04-23T18:20:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-april-23rd-2021-a-brutal-week

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-23rd-2021-a-brutal-week/){:target="_blank" rel="noopener"}

> This week has been brutal, not because of many ransomware variants released but due to a single ransomware campaign that affected thousands of people. [...]
