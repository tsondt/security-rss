Title: MangaDex discloses data breach after stolen database shared online
Date: 2021-04-27T10:46:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: mangadex-discloses-data-breach-after-stolen-database-shared-online

[Source](https://www.bleepingcomputer.com/news/security/mangadex-discloses-data-breach-after-stolen-database-shared-online/){:target="_blank" rel="noopener"}

> Manga scanlation site MangaDex disclosed a data breach last week after learning that the site's user database was privately circulating among threat actors. [...]
