Title: Would be so cool if everyone normalized these pesky data leaks, says data-leaking Facebook in leaked memo
Date: 2021-04-20T19:51:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: would-be-so-cool-if-everyone-normalized-these-pesky-data-leaks-says-data-leaking-facebook-in-leaked-memo

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/20/facebook_data_breach/){:target="_blank" rel="noopener"}

> Blundering mouthpiece sent arrogant line to journalist by accident Facebook wants you to believe that the scraping of 533 million people’s personal data from its platform, and the dumping of that data online by nefarious people, is something to be “normalised.”... [...]
