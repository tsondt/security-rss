Title: QNAP removes backdoor account in NAS backup, disaster recovery app
Date: 2021-04-22T11:08:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qnap-removes-backdoor-account-in-nas-backup-disaster-recovery-app

[Source](https://www.bleepingcomputer.com/news/security/qnap-removes-backdoor-account-in-nas-backup-disaster-recovery-app/){:target="_blank" rel="noopener"}

> QNAP has addressed a critical vulnerability allowing attackers to log into QNAP NAS (network-attached storage) devices using hardcoded credentials. [...]
