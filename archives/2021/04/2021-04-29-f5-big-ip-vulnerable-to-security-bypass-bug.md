Title: F5 Big-IP Vulnerable to Security-Bypass Bug
Date: 2021-04-29T20:04:55+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: f5-big-ip-vulnerable-to-security-bypass-bug

[Source](https://threatpost.com/f5-big-ip-security-bypass/165735/){:target="_blank" rel="noopener"}

> The KDC-spoofing flaw tracked as CVE-2021-23008 can be used to bypass Kerberos security and sign into the Big-IP Access Policy Manager or admin console. [...]
