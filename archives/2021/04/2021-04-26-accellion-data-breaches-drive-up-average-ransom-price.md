Title: Accellion data breaches drive up average ransom price
Date: 2021-04-26T15:26:25-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: accellion-data-breaches-drive-up-average-ransom-price

[Source](https://www.bleepingcomputer.com/news/security/accellion-data-breaches-drive-up-average-ransom-price/){:target="_blank" rel="noopener"}

> The data breaches caused by the Clop ransomware gang exploiting a zero-day vulnerability have led to a sharp increase in the average ransom payment calculated for the first three months of the year. [...]
