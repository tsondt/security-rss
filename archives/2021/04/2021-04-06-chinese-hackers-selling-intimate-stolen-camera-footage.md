Title: Chinese Hackers Selling Intimate Stolen Camera Footage
Date: 2021-04-06T20:54:54+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;IoT;Privacy
Slug: chinese-hackers-selling-intimate-stolen-camera-footage

[Source](https://threatpost.com/chinese-hackers-intimate-camera-footage/165281/){:target="_blank" rel="noopener"}

> A massive operation offers access to hacked camera feeds in bedrooms and at hotels. [...]
