Title: QNAP caught napping as disclosure delay expires, critical NAS bugs revealed
Date: 2021-04-02T23:07:48+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: qnap-caught-napping-as-disclosure-delay-expires-critical-nas-bugs-revealed

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/02/qnap_bug_nas/){:target="_blank" rel="noopener"}

> Remote code execution hole, arbitrary file writing flaw could make a mess of stored files Some QNAP network attached storage devices are vulnerable to attack because of two critical vulnerabilities, one that enables unauthenticated remote code execution and another that provides the ability to write to arbitrary files.... [...]
