Title: Risk governance of digital transformation: guide for risk, compliance & audit teams
Date: 2021-04-29T16:00:00+00:00
Author: Nick Godfrey
Category: GCP Security
Tags: Compliance;Google Cloud Platform;Research;Identity & Security
Slug: risk-governance-of-digital-transformation-guide-for-risk-compliance-audit-teams

[Source](https://cloud.google.com/blog/products/identity-security/new-whitepaper-managing-risk-governance-in-digital-transformation/){:target="_blank" rel="noopener"}

> The ongoing shift toward cloud technologies has transformed industries and continues to accelerate. This has created new challenges and opportunities for Chief Risk Officers, Chief Compliance Officers, Heads of Internal Audit and their teams. As their organizations pursue newfound agility, quality improvements to their products and services, and relevance in the marketplace, executives and their teams rightfully prioritize a safe, secure and compliant adoption process for this new technological environment. All technological transformations this broad in scope require an adjustment to the risk, compliance and audit practices that ensure they’re safely managed. But we shouldn’t assume that adopting cloud computing [...]
