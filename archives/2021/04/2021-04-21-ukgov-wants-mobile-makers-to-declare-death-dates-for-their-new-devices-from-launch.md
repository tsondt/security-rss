Title: UK.gov wants mobile makers to declare death dates for their new devices from launch
Date: 2021-04-21T15:05:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ukgov-wants-mobile-makers-to-declare-death-dates-for-their-new-devices-from-launch

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/21/ukgov_death_dates_smartphones_iot_security/){:target="_blank" rel="noopener"}

> IoT security plan suddenly thrusts into the mainstream Phone, tablet, and IoT gadget makers will have to state when they'll stop providing security updates for new devices entering the market, the UK's Department for Culture, Media and Sport (DCMS) vowed this morning.... [...]
