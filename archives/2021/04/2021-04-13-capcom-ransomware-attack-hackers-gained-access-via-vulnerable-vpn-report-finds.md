Title: Capcom ransomware attack: Hackers gained access via vulnerable VPN, report finds
Date: 2021-04-13T15:06:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: capcom-ransomware-attack-hackers-gained-access-via-vulnerable-vpn-report-finds

[Source](https://portswigger.net/daily-swig/capcom-ransomware-attack-hackers-gained-access-via-vulnerable-vpn-report-finds){:target="_blank" rel="noopener"}

> Japanese gaming company fell victim to cyber-attack in November 2020 [...]
