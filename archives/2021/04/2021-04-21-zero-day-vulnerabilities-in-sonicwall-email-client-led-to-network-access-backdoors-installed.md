Title: Zero-day vulnerabilities in SonicWall email client led to network access, backdoors installed
Date: 2021-04-21T13:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: zero-day-vulnerabilities-in-sonicwall-email-client-led-to-network-access-backdoors-installed

[Source](https://portswigger.net/daily-swig/zero-day-vulnerabilities-in-sonicwall-email-client-led-to-network-access-backdoors-installed){:target="_blank" rel="noopener"}

> Unknown adversary found to be exploiting security flaws in the wild [...]
