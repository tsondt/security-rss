Title: Introducing Cloud CISO perspectives
Date: 2021-04-12T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud Platform;Perspectives;Identity & Security
Slug: introducing-cloud-ciso-perspectives

[Source](https://cloud.google.com/blog/products/identity-security/ciso-perspectives-april-2021/){:target="_blank" rel="noopener"}

> Since I joined Google Cloud as Chief Information Security Officer three short months ago, I’ve seen firsthand the unique point of view we have to improve security for our customers and society at large through the cloud. I started in this new role as the security industry was rattled by a major breach impacting the software supply chain, and I was reminded of one of the reasons I joined Google - the opportunity to push the industry forward in addressing challenging security issues and helping lay the foundation for a more secure future. Today, I’m excited to begin a new [...]
