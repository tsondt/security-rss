Title: Feds zap Exchange Server backdoors as Microsoft offers patches for further flaws
Date: 2021-04-14T14:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: feds-zap-exchange-server-backdoors-as-microsoft-offers-patches-for-further-flaws

[Source](https://portswigger.net/daily-swig/feds-zap-exchange-server-backdoors-as-microsoft-offers-patches-for-further-flaws){:target="_blank" rel="noopener"}

> Trouble comes in twos [...]
