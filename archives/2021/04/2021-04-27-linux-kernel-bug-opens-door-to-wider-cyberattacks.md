Title: Linux Kernel Bug Opens Door to Wider Cyberattacks
Date: 2021-04-27T19:43:51+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: linux-kernel-bug-opens-door-to-wider-cyberattacks

[Source](https://threatpost.com/linux-kernel-bug-wider-cyberattacks/165640/){:target="_blank" rel="noopener"}

> The information-disclosure flaw allows KASLR bypass and the discovery of additional, unpatched vulnerabilities in ARM devices. [...]
