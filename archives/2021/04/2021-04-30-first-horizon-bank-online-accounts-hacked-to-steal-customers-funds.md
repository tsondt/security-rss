Title: First Horizon bank online accounts hacked to steal customers’ funds
Date: 2021-04-30T16:04:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: first-horizon-bank-online-accounts-hacked-to-steal-customers-funds

[Source](https://www.bleepingcomputer.com/news/security/first-horizon-bank-online-accounts-hacked-to-steal-customers-funds/){:target="_blank" rel="noopener"}

> Bank holding company First Horizon Corporation disclosed the some of its customers had their online banking accounts breached by unknown attackers earlier this month. [...]
