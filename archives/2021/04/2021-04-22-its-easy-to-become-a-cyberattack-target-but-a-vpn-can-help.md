Title: It’s Easy to Become a Cyberattack Target, but a VPN Can Help
Date: 2021-04-22T13:00:14+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Web Security
Slug: its-easy-to-become-a-cyberattack-target-but-a-vpn-can-help

[Source](https://threatpost.com/cyberattack-target-vpn-help/165504/){:target="_blank" rel="noopener"}

> You might think that cybercrime is more prevalent in less digitally literate countries. However, NordVPN's Cyber Risk Index puts North American and Northern European countries at the top of the target list. [...]
