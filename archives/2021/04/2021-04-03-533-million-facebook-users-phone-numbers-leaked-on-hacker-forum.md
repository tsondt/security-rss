Title: 533 million Facebook users’ phone numbers leaked on hacker forum
Date: 2021-04-03T14:48:04-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: 533-million-facebook-users-phone-numbers-leaked-on-hacker-forum

[Source](https://www.bleepingcomputer.com/news/security/533-million-facebook-users-phone-numbers-leaked-on-hacker-forum/){:target="_blank" rel="noopener"}

> The mobile phone numbers and other personal information for approximately 533 million Facebook users worldwide has been leaked on a popular hacker forum for free. [...]
