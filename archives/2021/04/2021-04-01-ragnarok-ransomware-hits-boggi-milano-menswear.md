Title: Ragnarok Ransomware Hits Boggi Milano Menswear
Date: 2021-04-01T18:07:13+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware
Slug: ragnarok-ransomware-hits-boggi-milano-menswear

[Source](https://threatpost.com/ragnarok-ransomware-boggi-milano-menswear/165161/){:target="_blank" rel="noopener"}

> The ransomware gang exfiltrated 40 gigabytes of data from the fashion house, including HR and salary details. [...]
