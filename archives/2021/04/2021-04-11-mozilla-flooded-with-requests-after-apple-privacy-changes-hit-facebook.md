Title: Mozilla flooded with requests after Apple privacy changes hit Facebook
Date: 2021-04-11T11:34:32-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Apple;Technology
Slug: mozilla-flooded-with-requests-after-apple-privacy-changes-hit-facebook

[Source](https://www.bleepingcomputer.com/news/security/mozilla-flooded-with-requests-after-apple-privacy-changes-hit-facebook/){:target="_blank" rel="noopener"}

> Mozilla volunteers have recently been flooded with requests by online merchants and marketers for their domains to be added to what's called a Public Suffix List (PSL) due to recent privacy changes brought forth by Apple's iOS 14.5. [...]
