Title: Babuk quits ransomware encryption, focuses on data-theft extortion
Date: 2021-04-30T15:28:39-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: babuk-quits-ransomware-encryption-focuses-on-data-theft-extortion

[Source](https://www.bleepingcomputer.com/news/security/babuk-quits-ransomware-encryption-focuses-on-data-theft-extortion/){:target="_blank" rel="noopener"}

> A new message today from the operators of Babuk ransomware clarifies that the gang has decided to close the affiliate program and move to an extortion model that does not rely on encrypting victim computers. [...]
