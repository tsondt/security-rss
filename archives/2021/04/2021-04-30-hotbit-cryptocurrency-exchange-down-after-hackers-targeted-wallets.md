Title: Hotbit cryptocurrency exchange down after hackers targeted wallets
Date: 2021-04-30T11:32:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: hotbit-cryptocurrency-exchange-down-after-hackers-targeted-wallets

[Source](https://www.bleepingcomputer.com/news/security/hotbit-cryptocurrency-exchange-down-after-hackers-targeted-wallets/){:target="_blank" rel="noopener"}

> Cryptocurrency trading platform Hotbit has shut down all services for at least a week after a cyberattack that down several of its services on Thursday evening. [...]
