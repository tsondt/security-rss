Title: Adobe fixes critical vulnerabilities in Photoshop and Digital Editions
Date: 2021-04-13T11:20:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: adobe-fixes-critical-vulnerabilities-in-photoshop-and-digital-editions

[Source](https://www.bleepingcomputer.com/news/security/adobe-fixes-critical-vulnerabilities-in-photoshop-and-digital-editions/){:target="_blank" rel="noopener"}

> Adobe has released security updates that address security vulnerabilities in Adobe Photoshop, Adobe Digital Editions, Adobe Bridge, and RoboHelp. [...]
