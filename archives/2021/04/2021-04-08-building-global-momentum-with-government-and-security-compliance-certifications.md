Title: Building global momentum with government and security compliance certifications
Date: 2021-04-08T15:30:00+00:00
Author: Mike Daniels
Category: GCP Security
Tags: Google Cloud Platform;Compliance;Public Sector;Identity & Security
Slug: building-global-momentum-with-government-and-security-compliance-certifications

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-adds-compliance-certifications-and-resources/){:target="_blank" rel="noopener"}

> Over the course of the COVID-19 pandemic, it’s proven more important than ever for public sector agencies to embrace digital services to transform how they work and serve their communities. Operating virtually has only heightened the importance of organizational security and compliance for public sector agencies around the world— who must still meet strict regulatory requirements while adjusting to new ways of connecting to their citizens. We recently made a public commitment to act as your security transformation partner, and to be the most Trusted Cloud. To deliver on this promise, we’ve been significantly expanding our list of compliance certifications [...]
