Title: Whitepaper: Hold your own key with Google Cloud External Key Manager
Date: 2021-04-23T16:00:00+00:00
Author: Il-Sung Lee
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: whitepaper-hold-your-own-key-with-google-cloud-external-key-manager

[Source](https://cloud.google.com/blog/products/identity-security/hold-your-own-key-with-google-cloud-external-key-manager/){:target="_blank" rel="noopener"}

> We want to help customers use Google Cloud while trusting us less. This is the philosophy behind a lot of our security work, as we’ve described before. It’s part of our vision for a trusted cloud, our plan to build technologies and strategies that earn greater trust. To that end, we are releasing a new resource to help customers understand Google Cloud External Key Manager (Cloud EKM), our technology for Hold Your Own Key (HYOK). This whitepaper explains the origin of the idea, the functionality, architecture and use cases for EKM. It is written by Andrew Lance of Sidechain, and [...]
