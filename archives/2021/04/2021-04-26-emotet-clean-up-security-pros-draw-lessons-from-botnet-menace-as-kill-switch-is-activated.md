Title: Emotet clean-up: Security pros draw lessons from botnet menace as kill switch is activated
Date: 2021-04-26T14:57:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: emotet-clean-up-security-pros-draw-lessons-from-botnet-menace-as-kill-switch-is-activated

[Source](https://portswigger.net/daily-swig/emotet-clean-up-security-pros-draw-lessons-from-botnet-menace-as-kill-switch-is-activated){:target="_blank" rel="noopener"}

> Malware-as-a-Service trailblazer finally zapped out of existence [...]
