Title: NSA Discloses Vulnerabilities in Microsoft Exchange
Date: 2021-04-16T11:23:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: disclosure;Microsoft;NSA;patching;vulnerabilities
Slug: nsa-discloses-vulnerabilities-in-microsoft-exchange

[Source](https://www.schneier.com/blog/archives/2021/04/nsa-discloses-vulnerabilities-in-microsoft-exchange.html){:target="_blank" rel="noopener"}

> Amongst the 100+ vulnerabilities patch in this month’s Patch Tuesday, there are four in Microsoft Exchange that were disclosed by the NSA. [...]
