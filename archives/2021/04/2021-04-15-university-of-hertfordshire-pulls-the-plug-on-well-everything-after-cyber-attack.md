Title: University of Hertfordshire pulls the plug on, well, everything after cyber attack
Date: 2021-04-15T15:04:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: university-of-hertfordshire-pulls-the-plug-on-well-everything-after-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/15/university_hertfordshire_cyber_attack/){:target="_blank" rel="noopener"}

> Another UK institution topples at the hands of miscreants The University of Hertfordshire has fallen victim to a cyber attack that has resulted in the establishment pulling all its systems offline to deal with the situation.... [...]
