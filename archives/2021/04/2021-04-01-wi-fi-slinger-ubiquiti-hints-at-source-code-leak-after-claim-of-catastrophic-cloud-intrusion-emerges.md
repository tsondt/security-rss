Title: Wi-Fi slinger Ubiquiti hints at source code leak after claim of ‘catastrophic’ cloud intrusion emerges
Date: 2021-04-01T04:58:56+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: wi-fi-slinger-ubiquiti-hints-at-source-code-leak-after-claim-of-catastrophic-cloud-intrusion-emerges

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/01/ubiquiti_data_breach/){:target="_blank" rel="noopener"}

> Says customer data wasn't touched, doesn't say much about being rooted Wi-Fi kit-slinger Ubiquiti has suggested the attacker that accessed some of its cloud-hosted systems in January 2021 may have made off with source code and employee logins, not the customer data it initially warned could be in peril.... [...]
