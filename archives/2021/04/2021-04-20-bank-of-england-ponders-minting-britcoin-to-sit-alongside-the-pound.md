Title: Bank of England ponders minting 'Britcoin' to sit alongside the Pound
Date: 2021-04-20T06:56:02+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: bank-of-england-ponders-minting-britcoin-to-sit-alongside-the-pound

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/20/britcoin_taskforce/){:target="_blank" rel="noopener"}

> Taskforce and two forums to consider Central Bank Digital Currency The Bank of England and HM Treasury have formed a Taskforce to "coordinate the exploration of a potential UK Central Bank Digital Currency" (CBDC).... [...]
