Title: Friday Squid Blogging: Jurassic Squid and Prey
Date: 2021-04-09T21:08:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-jurassic-squid-and-prey

[Source](https://www.schneier.com/blog/archives/2021/04/friday-squid-blogging-jurassic-squid-and-prey.html){:target="_blank" rel="noopener"}

> A 180-million-year-old Vampire squid ancestor was fossilized along with its prey. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
