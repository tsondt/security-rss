Title: Nintendo Sues Video-Game Pirates
Date: 2021-04-27T20:46:37+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Web Security
Slug: nintendo-sues-video-game-pirates

[Source](https://threatpost.com/nintendo-sues-video-game-pirates/165646/){:target="_blank" rel="noopener"}

> Nintendo is questing after its third successful lawsuit against circumvention-device sellers, this time against Team Xecuter. [...]
