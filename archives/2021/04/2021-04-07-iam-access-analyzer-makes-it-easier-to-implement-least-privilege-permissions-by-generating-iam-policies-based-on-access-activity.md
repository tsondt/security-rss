Title: IAM Access Analyzer makes it easier to implement least privilege permissions by generating IAM policies based on access activity
Date: 2021-04-07T20:22:38+00:00
Author: Mathangi Ramesh
Category: AWS Security
Tags: AWS IAM Access Analyzer;Foundational (100);Security, Identity, & Compliance;Access management;Amazon DynamoDB;Amazon EC2;Amazon S3;AWS CloudTrail;AWS IAM;AWS Lambda;IAM;IAM Access Analyzer;least privilege;Policies;Security Blog
Slug: iam-access-analyzer-makes-it-easier-to-implement-least-privilege-permissions-by-generating-iam-policies-based-on-access-activity

[Source](https://aws.amazon.com/blogs/security/iam-access-analyzer-makes-it-easier-to-implement-least-privilege-permissions-by-generating-iam-policies-based-on-access-activity/){:target="_blank" rel="noopener"}

> In 2019, AWS Identity and Access Management (IAM) Access Analyzer was launched to help you remove unintended public and cross account access by analyzing your existing permissions. In March 2021, IAM Access Analyzer added policy validation to help you set secure and functional permissions during policy authoring. Now, IAM Access Analyzer takes that a step [...]
