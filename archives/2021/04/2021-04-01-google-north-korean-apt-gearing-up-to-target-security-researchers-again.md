Title: Google: North Korean APT Gearing Up to Target Security Researchers Again
Date: 2021-04-01T14:51:02+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Web Security
Slug: google-north-korean-apt-gearing-up-to-target-security-researchers-again

[Source](https://threatpost.com/north-korean-apt-security-researchers/165155/){:target="_blank" rel="noopener"}

> Cyberattackers have set up a website for a fake company called SecuriElite, as well as associated Twitter and LinkedIn accounts. [...]
