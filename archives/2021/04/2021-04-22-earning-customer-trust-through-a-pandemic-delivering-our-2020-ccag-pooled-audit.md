Title: Earning customer trust through a pandemic: delivering our 2020 CCAG pooled audit
Date: 2021-04-22T16:00:00+00:00
Author: Rani Urbas
Category: GCP Security
Tags: Identity & Security;Google Cloud Platform;Compliance
Slug: earning-customer-trust-through-a-pandemic-delivering-our-2020-ccag-pooled-audit

[Source](https://cloud.google.com/blog/products/compliance/google-completes-annual-pooled-audit-with-ccag-completely-remote/){:target="_blank" rel="noopener"}

> At Google Cloud, we work closely with customers who want to assess and verify the security of our platform. Take as an example our recent collaboration with the Collaborative Cloud Audit Group (CCAG). As our customers increased their use of cloud services to meet the demands of teleworking and aid in COVID-19 recovery, we’ve worked hard to meet our commitment to being the industry’s most trusted cloud, despite the global pandemic. That’s why we are proud to announce that Google Cloud completed an annual pooled audit with the CCAG in a completely remote setting, and was the only cloud service [...]
