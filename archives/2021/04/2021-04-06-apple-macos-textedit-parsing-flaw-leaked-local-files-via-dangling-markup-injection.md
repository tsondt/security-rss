Title: Apple macOS TextEdit parsing flaw leaked local files via dangling markup injection
Date: 2021-04-06T14:28:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: apple-macos-textedit-parsing-flaw-leaked-local-files-via-dangling-markup-injection

[Source](https://portswigger.net/daily-swig/apple-macos-textedit-parsing-flaw-leaked-local-files-via-dangling-markup-injection){:target="_blank" rel="noopener"}

> Flaw allowed attacker to leak victim’s IP address and gain access to local files [...]
