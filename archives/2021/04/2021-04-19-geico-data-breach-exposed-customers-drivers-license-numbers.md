Title: Geico data breach exposed customers' driver's license numbers
Date: 2021-04-19T18:27:46-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: geico-data-breach-exposed-customers-drivers-license-numbers

[Source](https://www.bleepingcomputer.com/news/security/geico-data-breach-exposed-customers-drivers-license-numbers/){:target="_blank" rel="noopener"}

> Car insurance provider Geico has suffered a data breach where threat actors stole the driver's licenses for policyholders for over a month. [...]
