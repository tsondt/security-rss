Title: US government confirms Russian SVR behind the SolarWinds hack
Date: 2021-04-15T10:54:57-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: us-government-confirms-russian-svr-behind-the-solarwinds-hack

[Source](https://www.bleepingcomputer.com/news/security/us-government-confirms-russian-svr-behind-the-solarwinds-hack/){:target="_blank" rel="noopener"}

> The United States government is formally accusing the Russian government of the SolarWinds supply-chain attack that gave hackers access to the network of multiple U.S. agencies and private tech sector companies. [...]
