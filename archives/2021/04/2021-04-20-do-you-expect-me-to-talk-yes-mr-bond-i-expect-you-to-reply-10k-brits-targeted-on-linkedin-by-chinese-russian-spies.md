Title: Do you expect me to talk? Yes, Mr Bond, I expect you to reply: 10k Brits targeted on LinkedIn by Chinese, Russian spies
Date: 2021-04-20T15:48:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: do-you-expect-me-to-talk-yes-mr-bond-i-expect-you-to-reply-10k-brits-targeted-on-linkedin-by-chinese-russian-spies

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/20/linkedin_10000_uk_russia_china/){:target="_blank" rel="noopener"}

> Campaign launched to alert public sector staff that not everyone on the internet is nice Ten thousand Britons have been targeted on LinkedIn by recruiters for the Chinese and Russian intelligence services, according to an awareness campaign launched by domestic spy agency MI5 this morning.... [...]
