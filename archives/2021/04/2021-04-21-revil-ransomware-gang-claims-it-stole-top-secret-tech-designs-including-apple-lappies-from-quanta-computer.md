Title: REvil ransomware gang claims it stole top-secret tech designs – including Apple lappies – from Quanta Computer
Date: 2021-04-21T07:57:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: revil-ransomware-gang-claims-it-stole-top-secret-tech-designs-including-apple-lappies-from-quanta-computer

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/21/ransomware_gang_extorts_apple/){:target="_blank" rel="noopener"}

> Threatens to release designs and data if not paid. But dangles 2005-vintage ThinkPad as proof it's serious An entity claiming to represent ransomware gang REvil says it has accessed "large quantities of confidential drawings and gigabytes of personal data" from Quanta Computer Incorporated, a Taiwanese manufacturer that builds laptops and other gadgets for the likes of Apple, Dell, HPE, Lenovo, Cisco, and plenty of other top-tier tech companies.... [...]
