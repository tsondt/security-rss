Title: Microsoft Edge to add automatic HTTPS option for all domains
Date: 2021-04-27T15:51:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-edge-to-add-automatic-https-option-for-all-domains

[Source](https://www.bleepingcomputer.com/news/security/microsoft-edge-to-add-automatic-https-option-for-all-domains/){:target="_blank" rel="noopener"}

> Microsoft Edge will automatically redirect users to a secure HTTPS connection when visiting websites using the HTTP protocol, starting with version 92, coming in late July. [...]
