Title: Signal CEO gives mobile-hacking firm a taste of being hacked
Date: 2021-04-21T19:13:50-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: signal-ceo-gives-mobile-hacking-firm-a-taste-of-being-hacked

[Source](https://www.bleepingcomputer.com/news/security/signal-ceo-gives-mobile-hacking-firm-a-taste-of-being-hacked/){:target="_blank" rel="noopener"}

> Software developed by data extraction company Cellebrite contains vulnerabilities that allow arbitrary code execution on the device, claims Moxie Marlinspike, the creator of the encrypted messaging app Signal. [...]
