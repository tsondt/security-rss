Title: SAP Bugs Under Active Cyberattack, Causing Widespread Compromise
Date: 2021-04-06T18:47:57+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: sap-bugs-under-active-cyberattack-causing-widespread-compromise

[Source](https://threatpost.com/sap-bugs-cyberattack-compromise/165265/){:target="_blank" rel="noopener"}

> Cyberattackers are actively exploiting known security vulnerabilities in widely deployed, mission-critical SAP applications, allowing for full takeover and the ability to infest an organization further. [...]
