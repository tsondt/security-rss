Title: If you have a QNAP NAS, stop what you're doing right now and install latest updates. Do it before Qlocker gets you
Date: 2021-04-22T21:57:54+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: if-you-have-a-qnap-nas-stop-what-youre-doing-right-now-and-install-latest-updates-do-it-before-qlocker-gets-you

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/22/qnap_nas_ransomware_qlocker_ech0raix/){:target="_blank" rel="noopener"}

> Storage box maker puts customers on red alert after outbreak in ransomware infections QNAP has urged its customers to install and run its latest firmware and malware removal tools on their NAS boxes amid a surge in ransomware infections.... [...]
