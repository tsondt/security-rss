Title: Capital One notifies more clients of SSNs exposed in 2019 data breach
Date: 2021-04-02T11:46:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: capital-one-notifies-more-clients-of-ssns-exposed-in-2019-data-breach

[Source](https://www.bleepingcomputer.com/news/security/capital-one-notifies-more-clients-of-ssns-exposed-in-2019-data-breach/){:target="_blank" rel="noopener"}

> US bank Capital One notified additional customers that their Social Security numbers were exposed in a data breach announced in July 2019. [...]
