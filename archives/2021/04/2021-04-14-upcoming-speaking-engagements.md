Title: Upcoming Speaking Engagements
Date: 2021-04-14T17:30:19+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: 
Slug: upcoming-speaking-engagements-7

[Source](https://www.schneier.com/blog/archives/2021/04/upcoming-speaking-engagements-7.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m keynoting the (all-virtual) RSA Conference 2021, May 17-20, 2021. I’m keynoting the 5th International Symposium on Cyber Security Cryptology and Machine Learning (via Zoom), July 8-9, 2021. I’ll be speaking at an Informa event on September 14, 2021. Details to come. The list is maintained on this page. [...]
