Title: Ryuk ransomware operation updates hacking techniques
Date: 2021-04-17T10:15:42-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: ryuk-ransomware-operation-updates-hacking-techniques

[Source](https://www.bleepingcomputer.com/news/security/ryuk-ransomware-operation-updates-hacking-techniques/){:target="_blank" rel="noopener"}

> Recent attacks from Ryuk ransomware operators show that the actors have a new preference when it comes to gaining initial access to the victim network. [...]
