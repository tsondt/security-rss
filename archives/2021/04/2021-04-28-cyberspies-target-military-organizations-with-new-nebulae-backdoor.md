Title: Cyberspies target military organizations with new Nebulae backdoor
Date: 2021-04-28T09:00:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cyberspies-target-military-organizations-with-new-nebulae-backdoor

[Source](https://www.bleepingcomputer.com/news/security/cyberspies-target-military-organizations-with-new-nebulae-backdoor/){:target="_blank" rel="noopener"}

> A Chinese-speaking threat actor has deployed a new backdoor in multiple cyber-espionage operations spanning roughly two years and targeting military organizations from Southeast Asia. [...]
