Title: Cisco fixes bug allowing remote code execution with root privileges
Date: 2021-04-07T15:38:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisco-fixes-bug-allowing-remote-code-execution-with-root-privileges

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-bug-allowing-remote-code-execution-with-root-privileges/){:target="_blank" rel="noopener"}

> Cisco has released security updates to address a pre-authentication remote code execution (RCE) vulnerability affecting SD-WAN vManage Software's user management function. [...]
