Title: Sierra Wireless resumes production after ransomware attack
Date: 2021-04-04T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: sierra-wireless-resumes-production-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/sierra-wireless-resumes-production-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Canadian IoT solutions provider Sierra Wireless announced that it resumed production at its manufacturing sites after it halted it due to a ransomware attack that hit its internal network and corporate website on March 20. [...]
