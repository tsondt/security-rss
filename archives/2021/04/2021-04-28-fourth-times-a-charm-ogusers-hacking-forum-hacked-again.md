Title: Fourth time's a charm - OGUsers hacking forum hacked again
Date: 2021-04-28T14:35:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fourth-times-a-charm-ogusers-hacking-forum-hacked-again

[Source](https://www.bleepingcomputer.com/news/security/fourth-times-a-charm-ogusers-hacking-forum-hacked-again/){:target="_blank" rel="noopener"}

> Popular hacking forum OGUsers has been hacked for its fourth time in two years, with hackers now selling the site's database containing user records and private messages. [...]
