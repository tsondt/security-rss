Title: Brit MPs and campaigners come together to oppose COVID status certificates as 'divisive and discriminatory'
Date: 2021-04-28T14:32:13+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: brit-mps-and-campaigners-come-together-to-oppose-covid-status-certificates-as-divisive-and-discriminatory

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/28/covid_status_certificates_uk/){:target="_blank" rel="noopener"}

> Transport minister confirms use of the NHS app for just that when citizens travel abroad With Minister for the Cabinet Office Michael Gove expected to announce app-based "COVID status certificates," the UK's post-lockdown plan looks set to come under fierce attack.... [...]
