Title: Brazil's Rio Grande do Sul court system hit by REvil ransomware
Date: 2021-04-29T19:18:35-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: brazils-rio-grande-do-sul-court-system-hit-by-revil-ransomware

[Source](https://www.bleepingcomputer.com/news/security/brazils-rio-grande-do-sul-court-system-hit-by-revil-ransomware/){:target="_blank" rel="noopener"}

> Brazil's Tribunal de Justiça do Estado do Rio Grande do Sul was hit with an REvil ransomware attack yesterday that encrypted employee's files and forced the courts to shut down their network. [...]
