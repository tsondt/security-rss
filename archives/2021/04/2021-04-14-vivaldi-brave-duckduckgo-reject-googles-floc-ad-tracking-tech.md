Title: Vivaldi, Brave, DuckDuckGo reject Google's FLoC ad tracking tech
Date: 2021-04-14T11:59:54-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Google
Slug: vivaldi-brave-duckduckgo-reject-googles-floc-ad-tracking-tech

[Source](https://www.bleepingcomputer.com/news/security/vivaldi-brave-duckduckgo-reject-googles-floc-ad-tracking-tech/){:target="_blank" rel="noopener"}

> Makers of Vivaldi and Brave web browsers have rejected Google's new privacy-preserving proposal called FLoC, which is meant to replace third-party tracking cookies across websites on browsers, including Chrome. [...]
