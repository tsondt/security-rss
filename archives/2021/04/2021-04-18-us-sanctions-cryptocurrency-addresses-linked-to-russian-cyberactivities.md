Title: US sanctions cryptocurrency addresses linked to Russian cyberactivities
Date: 2021-04-18T12:07:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency
Slug: us-sanctions-cryptocurrency-addresses-linked-to-russian-cyberactivities

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-cryptocurrency-addresses-linked-to-russian-cyberactivities/){:target="_blank" rel="noopener"}

> The US government sanctioned this week twenty-eight cryptocurrency addresses allegedly associated with entities or individuals linked to Russian cyberattacks or election interference. [...]
