Title: Note to Self: Create Non-Exhaustive List of Competitors
Date: 2021-04-20T21:46:52+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Gartner Inc.;Magic Quadrant;Medium;Patreon;Substack
Slug: note-to-self-create-non-exhaustive-list-of-competitors

[Source](https://krebsonsecurity.com/2021/04/note-to-self-create-non-exhaustive-list-of-competitors/){:target="_blank" rel="noopener"}

> What was the best news you heard so far this month? Mine was learning that KrebsOnSecurity is listed as a restricted competitor by Gartner Inc. [ NYSE:IT ] — a $4 billion technology goliath whose analyst reports can move markets and shape the IT industry. Earlier this month, a reader pointed my attention to the following notice from Gartner to clients who are seeking to promote Gartner reports about technology products and services: What that notice says is that KrebsOnSecurity is somehow on Gartner’s “non exhaustive list of competitors,” i.e., online venues where technology companies are not allowed to promote [...]
