Title: Hacker sells $38M worth of gift cards from thousands of shops
Date: 2021-04-06T12:49:58-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hacker-sells-38m-worth-of-gift-cards-from-thousands-of-shops

[Source](https://www.bleepingcomputer.com/news/security/hacker-sells-38m-worth-of-gift-cards-from-thousands-of-shops/){:target="_blank" rel="noopener"}

> A Russian hacker has sold on a top-tier underground forum close to 900,000 gift cards with a total value estimated at $38 million. [...]
