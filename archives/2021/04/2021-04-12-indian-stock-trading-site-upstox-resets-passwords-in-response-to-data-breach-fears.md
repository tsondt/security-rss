Title: Indian stock trading site Upstox resets passwords in response to data breach fears
Date: 2021-04-12T14:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: indian-stock-trading-site-upstox-resets-passwords-in-response-to-data-breach-fears

[Source](https://portswigger.net/daily-swig/indian-stock-trading-site-upstox-resets-passwords-in-response-to-data-breach-fears){:target="_blank" rel="noopener"}

> Company calls in experts and tightens security amid reports of data warehouse leak [...]
