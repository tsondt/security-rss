Title: Robinhood Warns Customers of Tax-Season Phishing Scams
Date: 2021-04-02T13:09:25+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks
Slug: robinhood-warns-customers-of-tax-season-phishing-scams

[Source](https://threatpost.com/robinhood-warns-customers-of-tax-season-phishing-scams/165180/){:target="_blank" rel="noopener"}

> Attackers are impersonating the stock-trading broker using fake websites to steal credentials as well as sending emails with malicious tax files. [...]
