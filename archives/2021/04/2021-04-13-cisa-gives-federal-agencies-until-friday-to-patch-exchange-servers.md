Title: CISA gives federal agencies until Friday to patch Exchange servers
Date: 2021-04-13T17:59:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: cisa-gives-federal-agencies-until-friday-to-patch-exchange-servers

[Source](https://www.bleepingcomputer.com/news/security/cisa-gives-federal-agencies-until-friday-to-patch-exchange-servers/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) has ordered federal agencies to install newly released Microsoft Exchange security updates by Friday. [...]
