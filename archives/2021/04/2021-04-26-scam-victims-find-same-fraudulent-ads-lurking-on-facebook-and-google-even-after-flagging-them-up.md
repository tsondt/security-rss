Title: Scam victims find same fraudulent ads lurking on Facebook and Google even after flagging them up
Date: 2021-04-26T13:45:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: scam-victims-find-same-fraudulent-ads-lurking-on-facebook-and-google-even-after-flagging-them-up

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/26/which_facebook_google/){:target="_blank" rel="noopener"}

> Consumer watchdog blasts platforms for onerous reporting mechanisms UK consumer watchdog Which? has found that ad giants Google and Facebook are failing to remove online scam ads even after victims report them.... [...]
