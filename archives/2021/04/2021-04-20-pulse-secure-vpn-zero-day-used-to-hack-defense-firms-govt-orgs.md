Title: Pulse Secure VPN zero-day used to hack defense firms, govt orgs
Date: 2021-04-20T11:03:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: pulse-secure-vpn-zero-day-used-to-hack-defense-firms-govt-orgs

[Source](https://www.bleepingcomputer.com/news/security/pulse-secure-vpn-zero-day-used-to-hack-defense-firms-govt-orgs/){:target="_blank" rel="noopener"}

> Pulse Secure has shared mitigation measures for a zero-day authentication bypass vulnerability in the Pulse Connect Secure (PCS) SSL VPN appliance actively exploited in attacks against worldwide organizations and focused on US Defense Industrial base (DIB) networks. [...]
