Title: Apple supplier Quanta Computer confirms it's fallen victim to ransomware attack
Date: 2021-04-21T17:58:10+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: apple-supplier-quanta-computer-confirms-its-fallen-victim-to-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/21/quanta_confirms_ransomware/){:target="_blank" rel="noopener"}

> REvil gang starts publishing designs of what appear to be unreleased products Quanta Computer, an ODM laptop manufacturer and prolific Apple supplier, has now confirmed that digital burglars broke into its systems.... [...]
