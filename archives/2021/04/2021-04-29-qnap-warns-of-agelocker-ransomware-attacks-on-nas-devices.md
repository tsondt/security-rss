Title: QNAP warns of AgeLocker ransomware attacks on NAS devices
Date: 2021-04-29T14:26:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qnap-warns-of-agelocker-ransomware-attacks-on-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-of-agelocker-ransomware-attacks-on-nas-devices/){:target="_blank" rel="noopener"}

> QNAP customers are once again urged to secure their Network Attached Storage (NAS) devices to defend against Agelocker ransomware attacks targeting their data. [...]
