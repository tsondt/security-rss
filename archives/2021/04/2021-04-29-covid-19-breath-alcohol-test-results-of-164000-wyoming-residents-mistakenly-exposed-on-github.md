Title: Covid-19, breath alcohol test results of 164,000 Wyoming residents mistakenly exposed on GitHub
Date: 2021-04-29T12:49:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: covid-19-breath-alcohol-test-results-of-164000-wyoming-residents-mistakenly-exposed-on-github

[Source](https://portswigger.net/daily-swig/covid-19-breath-alcohol-test-results-of-164-000-wyoming-residents-mistakenly-exposed-on-github){:target="_blank" rel="noopener"}

> Wyoming health department employee uploaded sensitive data to software code repositories [...]
