Title: Microsoft Warns 25 Critical Vulnerabilities in IoT, Industrial Devices
Date: 2021-04-30T11:49:34+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: IoT;Vulnerabilities
Slug: microsoft-warns-25-critical-vulnerabilities-in-iot-industrial-devices

[Source](https://threatpost.com/microsoft-warns-25-critical-iot-industrial-devices/165752/){:target="_blank" rel="noopener"}

> Azure Defender security team discovers that memory allocation is a systemic problem that can allow threat actors to execute malicious code remotely or cause entire systems to crash. [...]
