Title: IcedID Circulates Via Web Forms, Google URLs
Date: 2021-04-12T18:12:04+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: icedid-circulates-via-web-forms-google-urls

[Source](https://threatpost.com/icedid-web-forms-google-urls/165347/){:target="_blank" rel="noopener"}

> Attackers are filling out and submitting web-based "contact us" forms, thus evading email spam filters. [...]
