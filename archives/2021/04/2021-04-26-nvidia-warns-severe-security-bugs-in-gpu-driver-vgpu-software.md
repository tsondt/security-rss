Title: Nvidia Warns: Severe Security Bugs in GPU Driver, vGPU Software
Date: 2021-04-26T18:12:03+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: nvidia-warns-severe-security-bugs-in-gpu-driver-vgpu-software

[Source](https://threatpost.com/nvidia-security-bugs-gpu-vgpu/165597/){:target="_blank" rel="noopener"}

> The gaming- and AI-friendly graphics accelerators can open the door to a range of cyberattacks. [...]
