Title: Ransomware gang wanted $40 million in Florida schools cyberattack
Date: 2021-04-02T07:03:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-wanted-40-million-in-florida-schools-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-wanted-40-million-in-florida-schools-cyberattack/){:target="_blank" rel="noopener"}

> Fueled by large payments from victims, ransomware gangs have started to demand ridiculous ransoms from organizations that can not afford them. An example of this is a recently revealed ransomware attack on the Broward County Public Schools district where threat actors demanded a $40,000,000 payment. [...]
