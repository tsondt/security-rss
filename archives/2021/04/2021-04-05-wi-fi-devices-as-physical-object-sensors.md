Title: Wi-Fi Devices as Physical Object Sensors
Date: 2021-04-05T11:15:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;sensors;Wi-Fi;wireless
Slug: wi-fi-devices-as-physical-object-sensors

[Source](https://www.schneier.com/blog/archives/2021/04/wi-fi-devices-as-physical-object-sensors.html){:target="_blank" rel="noopener"}

> The new 802.11bf standard will turn Wi-Fi devices into object sensors: In three years or so, the Wi-Fi specification is scheduled to get an upgrade that will turn wireless devices into sensors capable of gathering data about the people and objects bathed in their signals. “When 802.11bf will be finalized and introduced as an IEEE standard in September 2024, Wi-Fi will cease to be a communication-only standard and will legitimately become a full-fledged sensing paradigm,” explains Francesco Restuccia, assistant professor of electrical and computer engineering at Northeastern University, in a paper summarizing the state of the Wi-Fi Sensing project ( [...]
