Title: Zero Trust: The Mobile Dimension
Date: 2021-04-12T18:14:35+00:00
Author: Hank Schless
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Mobile Security
Slug: zero-trust-the-mobile-dimension

[Source](https://threatpost.com/zero-trust-mobile-dimension/165349/){:target="_blank" rel="noopener"}

> Hank Schless, senior security solutions manager at Lookout, discusses how to secure remote working via mobile devices. [...]
