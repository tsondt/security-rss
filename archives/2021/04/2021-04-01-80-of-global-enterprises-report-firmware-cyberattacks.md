Title: 80% of Global Enterprises Report Firmware Cyberattacks
Date: 2021-04-01T20:58:32+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Most Recent ThreatLists;Vulnerabilities
Slug: 80-of-global-enterprises-report-firmware-cyberattacks

[Source](https://threatpost.com/enterprises-firmware-cyberattacks/165174/){:target="_blank" rel="noopener"}

> A vast majority of companies in a global survey from Microsoft report being a victim of a firmware-focused cyberattack, but defense spending lags, but defense spending lags. [...]
