Title: 48 ways you can avoid file-scrambling, data-stealing miscreants – or so says the Ransomware Task Force
Date: 2021-04-29T10:00:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 48-ways-you-can-avoid-file-scrambling-data-stealing-miscreants-or-so-says-the-ransomware-task-force

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/29/ransomware_task_force_offers_48/){:target="_blank" rel="noopener"}

> No, not the US government's task force... the other one The Institute for Security and Technology's Ransomware Task Force (RTF) on Thursday published an 81-page report presenting policy makers with 48 recommendations to disrupt the ransomware business and mitigate the effect of such attacks.... [...]
