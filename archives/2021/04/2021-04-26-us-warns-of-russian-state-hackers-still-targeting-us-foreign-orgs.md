Title: US warns of Russian state hackers still targeting US, foreign orgs
Date: 2021-04-26T11:16:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-warns-of-russian-state-hackers-still-targeting-us-foreign-orgs

[Source](https://www.bleepingcomputer.com/news/security/us-warns-of-russian-state-hackers-still-targeting-us-foreign-orgs/){:target="_blank" rel="noopener"}

> The FBI, the US Department of Homeland Security (DHS), and the Cybersecurity and Infrastructure Security Agency (CISA) warned today of continued attacks coordinated by the Russian-backed APT 29 hacking group against US and foreign organizations. [...]
