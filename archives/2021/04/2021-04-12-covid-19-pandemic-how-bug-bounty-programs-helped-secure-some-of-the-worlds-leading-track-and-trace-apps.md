Title: Covid-19 pandemic: How bug bounty programs helped secure some of the world’s leading track and trace apps
Date: 2021-04-12T13:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: covid-19-pandemic-how-bug-bounty-programs-helped-secure-some-of-the-worlds-leading-track-and-trace-apps

[Source](https://portswigger.net/daily-swig/covid-19-pandemic-how-bug-bounty-programs-helped-secure-some-of-the-worlds-leading-track-and-trace-apps){:target="_blank" rel="noopener"}

> Crowdsourced security was a key tool in securing some countries’ efforts, while others missed the mark [...]
