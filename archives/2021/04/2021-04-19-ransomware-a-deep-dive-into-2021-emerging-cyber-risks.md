Title: Ransomware: A Deep Dive into 2021 Emerging Cyber-Risks
Date: 2021-04-19T18:01:23+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;eBook;Government;IoT;Malware;Web Security
Slug: ransomware-a-deep-dive-into-2021-emerging-cyber-risks

[Source](https://threatpost.com/ebook-2021-ransomware-emerging-risks/165477/){:target="_blank" rel="noopener"}

> Our new eBook goes beyond the status quo to take a look at the evolution of ransomware and what to prepare for next. [...]
