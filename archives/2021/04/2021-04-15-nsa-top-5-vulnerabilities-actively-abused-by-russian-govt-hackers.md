Title: NSA: Top 5 vulnerabilities actively abused by Russian govt hackers
Date: 2021-04-15T09:29:08-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: nsa-top-5-vulnerabilities-actively-abused-by-russian-govt-hackers

[Source](https://www.bleepingcomputer.com/news/security/nsa-top-5-vulnerabilities-actively-abused-by-russian-govt-hackers/){:target="_blank" rel="noopener"}

> A joint advisory from the U.S. National Security Agency (NSA), the Cybersecurity and Infrastructure Security Agency (CISA), and the Federal Bureau of Investigation (FBI) warn that the Russian Foreign Intelligence Service (SVR) is exploiting five vulnerabilities in attacks against U.S. organizations and interests. [...]
