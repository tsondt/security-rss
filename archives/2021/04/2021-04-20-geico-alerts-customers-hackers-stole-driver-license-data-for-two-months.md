Title: GEICO Alerts Customers Hackers Stole Driver License Data for Two Months
Date: 2021-04-20T15:59:56+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Privacy;Web Security
Slug: geico-alerts-customers-hackers-stole-driver-license-data-for-two-months

[Source](https://threatpost.com/geico-alerts-hackers-stole-driver-license-data/165493/){:target="_blank" rel="noopener"}

> The second-largest auto insurance provider in the U.S. has since fixed the vulnerability that exposed information from its website. [...]
