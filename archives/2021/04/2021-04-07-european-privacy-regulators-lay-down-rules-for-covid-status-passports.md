Title: European privacy regulators lay down rules for Covid-status passports
Date: 2021-04-07T15:40:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: european-privacy-regulators-lay-down-rules-for-covid-status-passports

[Source](https://portswigger.net/daily-swig/european-privacy-regulators-lay-down-rules-for-covid-status-passports){:target="_blank" rel="noopener"}

> Coronavirus travel pass plans must prioritize data privacy, says European Data Protection Board [...]
