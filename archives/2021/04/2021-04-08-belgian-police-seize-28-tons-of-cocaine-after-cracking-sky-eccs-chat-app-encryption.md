Title: Belgian police seize 28 tons of cocaine after 'cracking' Sky ECC's chat app encryption
Date: 2021-04-08T10:39:14+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: belgian-police-seize-28-tons-of-cocaine-after-cracking-sky-eccs-chat-app-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/08/sky_ecc_drugs/){:target="_blank" rel="noopener"}

> Euro cops take $1.65bn of blow off the streets after poring over messages The Belgian plod says it seized 27.64 tons of cocaine worth €1.4bn (£1.2bn, $1.65bn) from shipments into Antwerp in the past six weeks after defeating the encryption in the Sky ECC chat app to read drug smugglers' messages.... [...]
