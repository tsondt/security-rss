Title: Cisco router flaws left small business networks open to abuse
Date: 2021-04-14T15:12:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cisco-router-flaws-left-small-business-networks-open-to-abuse

[Source](https://portswigger.net/daily-swig/cisco-router-flaws-left-small-business-networks-open-to-abuse){:target="_blank" rel="noopener"}

> Complexity to exploit authentication bypass bug ‘very low’ [...]
