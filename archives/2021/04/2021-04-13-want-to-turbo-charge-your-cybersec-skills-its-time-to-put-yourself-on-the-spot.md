Title: Want to turbo-charge your cybersec skills? It’s time to put yourself on the SPOT
Date: 2021-04-13T02:00:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: want-to-turbo-charge-your-cybersec-skills-its-time-to-put-yourself-on-the-spot

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/13/turbo_charge_your_cybersec_skills/){:target="_blank" rel="noopener"}

> That’s Self-Paced Online Training, says SANS Institute Promo Working in cybersecurity means always keeping your skills bang up to date. But what are your options when the challenges of blocking out time for traditional in person training are compounded by pandemic-related restrictions?... [...]
