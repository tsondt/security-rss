Title: Software developer charged with sabotaging employer’s systems through denial-of-service attack
Date: 2021-04-15T17:01:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: software-developer-charged-with-sabotaging-employers-systems-through-denial-of-service-attack

[Source](https://portswigger.net/daily-swig/software-developer-charged-with-sabotaging-employers-systems-through-denial-of-service-attack){:target="_blank" rel="noopener"}

> Crash of production server leads to FBI case, despite modest losses [...]
