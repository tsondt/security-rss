Title: REvil’s Big Apple Ransomware Gambit Looks to Pay Off
Date: 2021-04-23T13:00:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: revils-big-apple-ransomware-gambit-looks-to-pay-off

[Source](https://threatpost.com/revil-apple-ransomware-pay-off/165570/){:target="_blank" rel="noopener"}

> The notorious cybercrime gang could make out whether or not Apple pays the $50 million ransom by May 1 as demanded. [...]
