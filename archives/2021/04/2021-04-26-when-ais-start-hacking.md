Title: When AIs Start Hacking
Date: 2021-04-26T11:06:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: artificial intelligence;essays;hacking;vulnerabilities
Slug: when-ais-start-hacking

[Source](https://www.schneier.com/blog/archives/2021/04/when-ais-start-hacking.html){:target="_blank" rel="noopener"}

> If you don’t have enough to worry about already, consider a world where AIs are hackers. Hacking is as old as humanity. We are creative problem solvers. We exploit loopholes, manipulate systems, and strive for more influence, power, and wealth. To date, hacking has exclusively been a human activity. Not for long. As I lay out in a report I just published, artificial intelligence will eventually find vulnerabilities in all sorts of social, economic, and political systems, and then exploit them at unprecedented speed, scale, and scope. After hacking humanity, AI systems will then hack other AI systems, and humans [...]
