Title: Apple’s ransomware mess is the future of online extortion
Date: 2021-04-24T11:01:50+00:00
Author: Eric Bangeman
Category: Ars Technica
Tags: Biz & IT;Tech;apple;corporate espionage;hacking;iphone;ransomware
Slug: apples-ransomware-mess-is-the-future-of-online-extortion

[Source](https://arstechnica.com/?p=1759821){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson) On the day Apple was set to announce a slew of new products at its Spring Loaded event, a leak appeared from an unexpected quarter. The notorious ransomware gang REvil said they had stolen data and schematics from Apple supplier Quanta Computer about unreleased products and that they would sell the data to the highest bidder if they didn’t get a $50 million payment. As proof, they released a cache of documents about upcoming, unreleased MacBook Pros. They've since added iMac schematics to the pile. The connection to Apple and dramatic timing generated buzz about the [...]
