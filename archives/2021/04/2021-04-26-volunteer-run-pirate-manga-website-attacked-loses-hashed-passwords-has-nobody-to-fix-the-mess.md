Title: Volunteer-run pirate Manga website attacked, loses hashed passwords, has ‘nobody’ to fix the mess
Date: 2021-04-26T02:28:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: volunteer-run-pirate-manga-website-attacked-loses-hashed-passwords-has-nobody-to-fix-the-mess

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/26/mangadex_data_breach/){:target="_blank" rel="noopener"}

> Dot-org has been offline for a month, says ‘people who have ill intentions’ behind crack A “scanlation” website for Manga has admitted that its members credentials have been stolen and are now being shared online.... [...]
