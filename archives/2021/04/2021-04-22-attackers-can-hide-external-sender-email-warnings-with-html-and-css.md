Title: Attackers can hide 'external sender' email warnings with HTML and CSS
Date: 2021-04-22T06:18:43-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Microsoft;Technology
Slug: attackers-can-hide-external-sender-email-warnings-with-html-and-css

[Source](https://www.bleepingcomputer.com/news/security/attackers-can-hide-external-sender-email-warnings-with-html-and-css/){:target="_blank" rel="noopener"}

> The "external sender" warnings shown to email recipients by clients like Microsoft Outlook can be hidden by the sender, as demonstrated by a researcher. Turns out, all it takes for attackers to alter the "external sender" warning, or remove it altogether from emails is just a few lines of HTML and CSS code. [...]
