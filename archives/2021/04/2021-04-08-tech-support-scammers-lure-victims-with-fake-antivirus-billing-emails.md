Title: Tech support scammers lure victims with fake antivirus billing emails
Date: 2021-04-08T09:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: tech-support-scammers-lure-victims-with-fake-antivirus-billing-emails

[Source](https://www.bleepingcomputer.com/news/security/tech-support-scammers-lure-victims-with-fake-antivirus-billing-emails/){:target="_blank" rel="noopener"}

> Tech support scammers are pretending to be from Microsoft, McAfee, and Norton to target users with fake antivirus billing renewals in a large-scale email campaign. [...]
