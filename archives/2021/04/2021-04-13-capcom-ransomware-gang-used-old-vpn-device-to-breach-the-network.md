Title: Capcom: Ransomware gang used old VPN device to breach the network
Date: 2021-04-13T18:40:48-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: capcom-ransomware-gang-used-old-vpn-device-to-breach-the-network

[Source](https://www.bleepingcomputer.com/news/security/capcom-ransomware-gang-used-old-vpn-device-to-breach-the-network/){:target="_blank" rel="noopener"}

> Capcom has released a new update about the ransomware attack it suffered last year, detailing how the hackers gained access to the network, compromised devices, and stole personal information belonging to thousands of individuals. [...]
