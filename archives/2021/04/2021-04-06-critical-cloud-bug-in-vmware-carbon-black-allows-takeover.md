Title: Critical Cloud Bug in VMWare Carbon Black Allows Takeover
Date: 2021-04-06T20:55:47+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: critical-cloud-bug-in-vmware-carbon-black-allows-takeover

[Source](https://threatpost.com/critical-cloud-bug-vmware-carbon-black/165278/){:target="_blank" rel="noopener"}

> CVE-2021-21982 affects a platform designed to secure private clouds, and the virtual servers and workloads that they contain. [...]
