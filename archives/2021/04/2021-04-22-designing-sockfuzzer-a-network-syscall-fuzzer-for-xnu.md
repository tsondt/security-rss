Title: Designing sockfuzzer, a network syscall fuzzer for XNU
Date: 2021-04-22T11:05:00.002000-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: designing-sockfuzzer-a-network-syscall-fuzzer-for-xnu

[Source](https://googleprojectzero.blogspot.com/2021/04/designing-sockfuzzer-network-syscall.html){:target="_blank" rel="noopener"}

> Posted by Ned Williams on, Project Zero Introduction When I started my 20% project – an initiative where employees are allocated twenty-percent of their paid work time to pursue personal projects – with Project Zero, I wanted to see if I could apply the techniques I had learned fuzzing Chrome to XNU, the kernel used in iOS and macOS. My interest was sparked after learning some prominent members of the iOS research community believed the kernel was “fuzzed to death,” and my understanding was that most of the top researchers used auditing for vulnerability research. This meant finding new bugs [...]
