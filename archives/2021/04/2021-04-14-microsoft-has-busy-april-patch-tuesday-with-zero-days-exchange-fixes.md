Title: Microsoft Has Busy April Patch Tuesday with Zero-Days, Exchange Fixes
Date: 2021-04-14T12:46:33+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: microsoft-has-busy-april-patch-tuesday-with-zero-days-exchange-fixes

[Source](https://threatpost.com/microsoft-april-patch-tuesday-zero-days/165393/){:target="_blank" rel="noopener"}

> Microsoft fixes 110 vulnerabilities, with 19 classified as critical and another flaw under active attack. [...]
