Title: Prometei Botnet Could Fire Up APT-Style Attacks
Date: 2021-04-23T17:15:23+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: prometei-botnet-could-fire-up-apt-style-attacks

[Source](https://threatpost.com/prometei-botnet-apt-attacks/165574/){:target="_blank" rel="noopener"}

> The malware is for now using exploits for the Microsoft Exchange "ProxyLogon" security bugs to install Monero-mining malware on targets. [...]
