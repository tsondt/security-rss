Title: Cybercrooks targeting UK organisations started 2020 strong only for attacks to whither away by Christmas
Date: 2021-04-07T13:58:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: cybercrooks-targeting-uk-organisations-started-2020-strong-only-for-attacks-to-whither-away-by-christmas

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/07/bitdefender_cyber_crims_burnout/){:target="_blank" rel="noopener"}

> Aww, did the big bad criminals get a little lockdown burnout too? Compromising every web-connected server and service you can find gets tiring after a while – and by the end of 2021 internet criminals targeting British companies were as fatigued as the rest of us, according to Bitdefender.... [...]
