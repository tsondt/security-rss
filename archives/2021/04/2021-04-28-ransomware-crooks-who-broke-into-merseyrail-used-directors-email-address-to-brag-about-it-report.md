Title: Ransomware crooks who broke into Merseyrail used director's email address to brag about it – report
Date: 2021-04-28T16:45:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ransomware-crooks-who-broke-into-merseyrail-used-directors-email-address-to-brag-about-it-report

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/28/merseyrail_ransomware_claim/){:target="_blank" rel="noopener"}

> Hasn't stopped the trains, though Brit railway company Merseyrail is understood to have suffered a ransomware attack – and the crooks responsible reportedly pwned a director's Office 365 account to email employees and journalists about it.... [...]
