Title: Ransomware hits TU Dublin and National College of Ireland
Date: 2021-04-06T12:17:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ransomware-hits-tu-dublin-and-national-college-of-ireland

[Source](https://www.bleepingcomputer.com/news/security/ransomware-hits-tu-dublin-and-national-college-of-ireland/){:target="_blank" rel="noopener"}

> The National College of Ireland is working on restoring IT services after being hit by a ransomware attack over the weekend that forced the college to take IT systems offline. [...]
