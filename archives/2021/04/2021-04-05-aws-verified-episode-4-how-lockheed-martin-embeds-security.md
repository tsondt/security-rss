Title: AWS Verified, episode 4: How Lockheed Martin embeds security
Date: 2021-04-05T19:45:52+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Cloud security;Cyber Resiliency Framework;cybersecurity;enterprise IT security;Lockheed Martin;Lockheed Martin Cyber Kill Chain;Mike Gordon;Security Blog;Supply chain;Verified;Zero Trust
Slug: aws-verified-episode-4-how-lockheed-martin-embeds-security

[Source](https://aws.amazon.com/blogs/security/aws-verified-episode-4-how-lockheed-martin-embeds-security/){:target="_blank" rel="noopener"}

> Last year Amazon Web Services (AWS) launched a new video series, AWS Verified, where we talk to global cybersecurity leaders about important issues, such as how the pandemic is impacting cloud security, how to create a culture of security, and emerging security trends. Today I’m happy to share the latest episode of AWS Verified, an [...]
