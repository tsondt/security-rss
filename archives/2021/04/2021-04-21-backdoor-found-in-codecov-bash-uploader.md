Title: Backdoor Found in Codecov Bash Uploader
Date: 2021-04-21T16:12:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;credentials;hacking;supply chain
Slug: backdoor-found-in-codecov-bash-uploader

[Source](https://www.schneier.com/blog/archives/2021/04/backdoor-found-in-codecov-bash-uploader.html){:target="_blank" rel="noopener"}

> Developers have discovered a backdoor in the Codecov bash uploader. It’s been there for four months. We don’t know who put it there. Codecov said the breach allowed the attackers to export information stored in its users’ continuous integration (CI) environments. This information was then sent to a third-party server outside of Codecov’s infrastructure,” the company warned. Codecov’s Bash Uploader is also used in several uploaders — Codecov-actions uploader for Github, the Codecov CircleCl Orb, and the Codecov Bitrise Step — and the company says these uploaders were also impacted by the breach. According to Codecov, the altered version of [...]
