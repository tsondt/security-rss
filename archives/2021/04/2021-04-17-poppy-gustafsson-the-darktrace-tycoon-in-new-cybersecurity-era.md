Title: Poppy Gustafsson: the Darktrace tycoon in new cybersecurity era
Date: 2021-04-17T07:00:27+00:00
Author: Mark Sweney and Alex Hern
Category: The Guardian
Tags: IPOs;Data and computer security;Internet;Technology;Business;UK news;Gender;World news;Artificial intelligence (AI);Computing;Cybercrime;Hacking;Stock markets
Slug: poppy-gustafsson-the-darktrace-tycoon-in-new-cybersecurity-era

[Source](https://www.theguardian.com/business/2021/apr/17/poppy-gustafsson-the-darktrace-tycoon-in-new-cybersecurity-era){:target="_blank" rel="noopener"}

> Gustafsson’s firm, founded when she was 30, is marketed as a digital parallel of a human body fighting illness Poppy Gustafsson runs a cutting-edge and gender-diverse cybersecurity firm on the brink of a £3bn stock market debut, but she is happy to reference pop culture classic the Terminator to help describe what Darktrace actually does. Launched in Cambridge eight years ago by an unlikely alliance of mathematicians, former spies from GCHQ and the US and artificial intelligence (AI) experts, Darktrace provides protection, enabling businesses to stay one step ahead of increasingly smarter and dangerous hackers and viruses. Related: Huge rise [...]
