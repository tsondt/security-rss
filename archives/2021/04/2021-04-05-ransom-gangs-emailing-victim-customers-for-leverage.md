Title: Ransom Gangs Emailing Victim Customers for Leverage
Date: 2021-04-05T21:38:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;The Coming Storm;Bleeping Computer;Clop;Emsisoft;Fabian Wosar;Lawrence Abrams;Racetrac;ransomware;rEvil
Slug: ransom-gangs-emailing-victim-customers-for-leverage

[Source](https://krebsonsecurity.com/2021/04/ransom-gangs-emailing-victim-customers-for-leverage/){:target="_blank" rel="noopener"}

> Some of the top ransomware gangs are deploying a new pressure tactic to push more victim organizations into paying an extortion demand: Emailing the victim’s customers and partners directly, warning that their data will be leaked to the dark web unless they can convince the victim firm to pay up. This letter is from the Clop ransomware gang, putting pressure on a recent victim named on Clop’s dark web shaming site. “Good day! If you received this letter, you are a customer, buyer, partner or employee of [victim],” the missive reads. “The company has been hacked, data has been stolen [...]
