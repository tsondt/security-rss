Title: Machine learning security vulnerabilities are a growing threat to the web, report highlights
Date: 2021-04-27T12:57:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: machine-learning-security-vulnerabilities-are-a-growing-threat-to-the-web-report-highlights

[Source](https://portswigger.net/daily-swig/machine-learning-security-vulnerabilities-are-a-growing-threat-to-the-web-report-highlights){:target="_blank" rel="noopener"}

> Security industry needs to tackle nascent AI threats before it’s too late [...]
