Title: Optimizing cloud governance on AWS: Integrating the NIST Cybersecurity Framework, AWS Cloud Adoption Framework, and AWS Well-Architected
Date: 2021-04-12T21:07:27+00:00
Author: Min Hyun
Category: AWS Security
Tags: AWS Well-Architected;Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS CAF;AWS Cloud Adoption Framework;CSF;Cybersecurity Framework;governance;GRC;NIST;Security Blog;Strategy;Well-Architected
Slug: optimizing-cloud-governance-on-aws-integrating-the-nist-cybersecurity-framework-aws-cloud-adoption-framework-and-aws-well-architected

[Source](https://aws.amazon.com/blogs/security/optimizing-cloud-governance-on-aws-integrating-the-nist-cybersecurity-framework-aws-cloud-adoption-framework-and-aws-well-architected/){:target="_blank" rel="noopener"}

> Your approach to security governance, risk management, and compliance can be an enabler to digital transformation and business agility. As more organizations progress in their digital transformation journey—empowered by cloud computing—security organizations and processes cannot simply participate, they must lead in that transformation. Today, many customers establish a security foundation using technology-agnostic risk management frameworks—such [...]
