Title: Google Forms and Telegram abused to collect phished credentials
Date: 2021-04-07T12:10:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: google-forms-and-telegram-abused-to-collect-phished-credentials

[Source](https://www.bleepingcomputer.com/news/security/google-forms-and-telegram-abused-to-collect-phished-credentials/){:target="_blank" rel="noopener"}

> Security researchers note an increase in alternative methods to steal data from phishing attacks, as scammers obtain the stolen info through Google Forms or private Telegram bots. [...]
