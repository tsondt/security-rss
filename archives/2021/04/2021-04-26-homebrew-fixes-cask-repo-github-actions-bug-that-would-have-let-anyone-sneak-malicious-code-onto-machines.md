Title: Homebrew fixes Cask repo GitHub Actions bug that would have let anyone sneak malicious code onto machines
Date: 2021-04-26T04:39:04+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: homebrew-fixes-cask-repo-github-actions-bug-that-would-have-let-anyone-sneak-malicious-code-onto-machines

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/26/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: America creates task force to tackle ransomware crims In Brief The Homebrew package manager for macOS and Linux has fixed an issue that could have been exploited by miscreants to run malicious code on people's computers.... [...]
