Title: FBI and CISA warn of state hackers attacking Fortinet FortiOS servers
Date: 2021-04-02T13:04:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-and-cisa-warn-of-state-hackers-attacking-fortinet-fortios-servers

[Source](https://www.bleepingcomputer.com/news/security/fbi-and-cisa-warn-of-state-hackers-attacking-fortinet-fortios-servers/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) and the Cybersecurity and Infrastructure Security Agency (CISA) warn of advanced persistent threat (APT) actors targeting Fortinet FortiOS servers using multiple exploits. [...]
