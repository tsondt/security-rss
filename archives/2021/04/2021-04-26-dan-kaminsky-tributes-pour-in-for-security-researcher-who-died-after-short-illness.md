Title: Dan Kaminsky: Tributes pour in for security researcher who died after short illness
Date: 2021-04-26T11:58:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: dan-kaminsky-tributes-pour-in-for-security-researcher-who-died-after-short-illness

[Source](https://portswigger.net/daily-swig/dan-kaminsky-tributes-pour-in-for-security-researcher-who-died-after-short-illness){:target="_blank" rel="noopener"}

> ‘We owe him so much... People who will never know his name owe Dan’ [...]
