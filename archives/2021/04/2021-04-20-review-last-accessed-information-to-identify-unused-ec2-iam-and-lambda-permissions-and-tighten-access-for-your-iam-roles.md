Title: Review last accessed information to identify unused EC2, IAM, and Lambda permissions and tighten access for your IAM roles
Date: 2021-04-20T23:06:59+00:00
Author: Mathangi Ramesh
Category: AWS Security
Tags: Amazon EC2;AWS Identity and Access Management (IAM);AWS Lambda;Foundational (100);Security, Identity, & Compliance;Access management;Amazon DynamoDB;Amazon S3;AWS CloudTrail;AWS IAM;least privilege;Policies;Security Blog
Slug: review-last-accessed-information-to-identify-unused-ec2-iam-and-lambda-permissions-and-tighten-access-for-your-iam-roles

[Source](https://aws.amazon.com/blogs/security/review-last-accessed-information-to-identify-unused-ec2-iam-and-lambda-permissions-and-tighten-access-for-iam-roles/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) helps customers analyze access and achieve least privilege. When you are working on new permissions for your team, you can use IAM Access Analyzer policy generation to create a policy based on your access activity and set fine-grained permissions. To analyze and refine existing permissions, you can use last [...]
