Title: Choose the best way to use and authenticate service accounts on Google Cloud
Date: 2021-04-27T20:00:00+00:00
Author: Vignesh Rajamani
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: choose-the-best-way-to-use-and-authenticate-service-accounts-on-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/how-to-authenticate-service-accounts-to-help-keep-applications-secure/){:target="_blank" rel="noopener"}

> A fundamental security premise is to verify the identity of a user before determining if they are permitted to access a resource or service. This process is known as authentication. But authentication is necessary for more than just human users. When one application needs to talk to another, we need to authenticate its identity as well. In the cloud, this is most frequently accomplished through a service account. Service accounts represent non-human users and on Google Cloud are managed by Cloud Identity and Access Management (IAM). They are intended for scenarios where an application needs to access resources or perform [...]
