Title: Identifying People Through Lack of Cell Phone Use
Date: 2021-04-29T11:07:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cell phones;crime;France;prison escapes;prisons
Slug: identifying-people-through-lack-of-cell-phone-use

[Source](https://www.schneier.com/blog/archives/2021/04/identifying-people-through-lack-of-cell-phone-use.html){:target="_blank" rel="noopener"}

> In this entertaining story of French serial criminal Rédoine Faïd and his jailbreaking ways, there’s this bit about cell phone surveillance: After Faïd’s helicopter breakout, 3,000 police officers took part in the manhunt. According to the 2019 documentary La Traque de Rédoine Faïd, detective units scoured records of cell phones used during his escape, isolating a handful of numbers active at the time that went silent shortly thereafter. [...]
