Title: Windows XP makes ransomware gangs work harder for their money
Date: 2021-04-06T18:00:33-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: windows-xp-makes-ransomware-gangs-work-harder-for-their-money

[Source](https://www.bleepingcomputer.com/news/security/windows-xp-makes-ransomware-gangs-work-harder-for-their-money/){:target="_blank" rel="noopener"}

> A recently created ransomware decryptor illustrates how threat actors have to support Windows XP, even when Microsoft dropped supporting it seven years ago. [...]
