Title: Friday Squid Blogging: 500-Million-Year-Old Cephalopod
Date: 2021-04-02T21:10:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-500-million-year-old-cephalopod

[Source](https://www.schneier.com/blog/archives/2021/04/friday-squid-blogging-500-million-year-old-cephalopod.html){:target="_blank" rel="noopener"}

> The oldest known cephalopod — the ancestor of all modern octopuses, squid, cuttlefish and nautiluses — is 500 million years old. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
