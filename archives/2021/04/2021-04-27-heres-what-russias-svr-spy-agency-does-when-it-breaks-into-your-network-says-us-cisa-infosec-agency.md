Title: Here's what Russia's SVR spy agency does when it breaks into your network, says US CISA infosec agency
Date: 2021-04-27T17:03:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: heres-what-russias-svr-spy-agency-does-when-it-breaks-into-your-network-says-us-cisa-infosec-agency

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/27/apt29_russia_svr_tactics_cisa/){:target="_blank" rel="noopener"}

> Email provider cock.li called out for harbouring snooping personas Following attribution of the SolarWinds supply chain attack to Russia's APT29, the US CISA infosec agency has published a list of the spies' known tactics – including a penchant for using a naughtily named email provider.... [...]
