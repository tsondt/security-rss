Title: How to set up a two-way integration between AWS Security Hub and ServiceNow
Date: 2021-04-09T21:10:28+00:00
Author: Ramesh Venkataraman
Category: AWS Security
Tags: AWS Security Hub;Intermediate (200);Security, Identity, & Compliance;Security Blog;ServiceNow
Slug: how-to-set-up-a-two-way-integration-between-aws-security-hub-and-servicenow

[Source](https://aws.amazon.com/blogs/security/how-to-set-up-two-way-integration-between-aws-security-hub-and-servicenow/){:target="_blank" rel="noopener"}

> If you use both AWS Security Hub and ServiceNow, the new AWS Service Management Connector for ServiceNow integration enables you to provision, manage, and operate your AWS resources natively through ServiceNow. In this blog post, I’ll show you how to set up the new two-way integration of Security Hub and ServiceNow by using the AWS [...]
