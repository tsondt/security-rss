Title: 1.3M Clubhouse Users’ Data Dumped in Hacker Forum for Free
Date: 2021-04-12T20:18:18+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Privacy;Web Security
Slug: 13m-clubhouse-users-data-dumped-in-hacker-forum-for-free

[Source](https://threatpost.com/clubhouse-users-data-hacker-forum/165354/){:target="_blank" rel="noopener"}

> Clubhouse denies it was ‘breached’ and says the data is out there for anyone to grab. [...]
