Title: NAME:WRECK DNS vulnerabilities affect over 100 million devices
Date: 2021-04-13T00:01:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: namewreck-dns-vulnerabilities-affect-over-100-million-devices

[Source](https://www.bleepingcomputer.com/news/security/name-wreck-dns-vulnerabilities-affect-over-100-million-devices/){:target="_blank" rel="noopener"}

> Security researchers today disclosed nine vulnerabilities affecting implementations of the Domain Name System protocol in popular TCP/IP network communication stacks running on at least 100 million devices. [...]
