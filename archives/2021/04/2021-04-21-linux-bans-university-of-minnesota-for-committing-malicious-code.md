Title: Linux bans University of Minnesota for committing malicious code
Date: 2021-04-21T13:08:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Linux;Software
Slug: linux-bans-university-of-minnesota-for-committing-malicious-code

[Source](https://www.bleepingcomputer.com/news/security/linux-bans-university-of-minnesota-for-committing-malicious-code/){:target="_blank" rel="noopener"}

> Linux kernel project maintainers have imposed a ban on the University of Minnesota (UMN) from contributing to the open-source Linux project after a group of UMN researchers were caught submitting a series of malicious code commits, or patches that deliberately introduced security vulnerabilities in the official Linux project. [...]
