Title: Twitter accidentally sends suspicious emails asking to confirm accounts
Date: 2021-04-23T01:44:12-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Technology
Slug: twitter-accidentally-sends-suspicious-emails-asking-to-confirm-accounts

[Source](https://www.bleepingcomputer.com/news/technology/twitter-accidentally-sends-suspicious-emails-asking-to-confirm-accounts/){:target="_blank" rel="noopener"}

> Twitter caused quite the panic Thursday night when they accidentally sent emails asking users to confirm their accounts, which looked suspiciously like a phishing attack. [...]
