Title: Qualys says Accellion hackers did not breach production systems
Date: 2021-04-02T12:28:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qualys-says-accellion-hackers-did-not-breach-production-systems

[Source](https://www.bleepingcomputer.com/news/security/qualys-says-accellion-hackers-did-not-breach-production-systems/){:target="_blank" rel="noopener"}

> Cybersecurity firm Qualys said today that the attackers who breached its Accellion FTA server didn't infiltrate the company's production and corporate environments. [...]
