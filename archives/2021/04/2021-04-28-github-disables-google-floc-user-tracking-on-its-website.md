Title: GitHub disables Google FloC user tracking on its website
Date: 2021-04-28T01:25:45-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Google
Slug: github-disables-google-floc-user-tracking-on-its-website

[Source](https://www.bleepingcomputer.com/news/security/github-disables-google-floc-user-tracking-on-its-website/){:target="_blank" rel="noopener"}

> GitHub has announced rolling out a mysterious HTTP header on all GitHub Pages sites to block Google FLoC tracking. [...]
