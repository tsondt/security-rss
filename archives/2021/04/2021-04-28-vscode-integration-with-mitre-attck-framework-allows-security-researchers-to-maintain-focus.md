Title: VSCode integration with Mitre ATT&amp;CK framework allows security researchers to maintain focus
Date: 2021-04-28T11:05:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: vscode-integration-with-mitre-attck-framework-allows-security-researchers-to-maintain-focus

[Source](https://portswigger.net/daily-swig/vscode-integration-with-mitre-att-amp-ck-framework-allows-security-researchers-to-maintain-focus){:target="_blank" rel="noopener"}

> Introducing VSCode-ATT&CK [...]
