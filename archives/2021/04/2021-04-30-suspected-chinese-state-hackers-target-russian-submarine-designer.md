Title: Suspected Chinese state hackers target Russian submarine designer
Date: 2021-04-30T10:09:32-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: suspected-chinese-state-hackers-target-russian-submarine-designer

[Source](https://www.bleepingcomputer.com/news/security/suspected-chinese-state-hackers-target-russian-submarine-designer/){:target="_blank" rel="noopener"}

> Hackers suspected to work for the Chinese government have used a new malware called PortDoor to infiltrate the systems of an engineering company that designs submarines for the Russian Navy. [...]
