Title: Spotlight on Cybercriminal Supply Chains
Date: 2021-04-22T19:06:45+00:00
Author: Threatpost
Category: Threatpost
Tags: Hacks;Newsmaker Interviews;Podcasts
Slug: spotlight-on-cybercriminal-supply-chains

[Source](https://threatpost.com/spotlight-on-the-cybercriminal-supply-chains/165552/){:target="_blank" rel="noopener"}

> In this Threatpost podcast Fortinet’s top researcher outlines what a cybercriminal supply chain is and how much the illicit market is worth. [...]
