Title: Man Arrested for AWS Bomb Plot
Date: 2021-04-12T20:01:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Government;Privacy;Web Security
Slug: man-arrested-for-aws-bomb-plot

[Source](https://threatpost.com/man-arrested-for-aws-bomb-plot/165351/){:target="_blank" rel="noopener"}

> A man caught in an FBI sting allegedly said he wanted to destroy "70 percent of the internet" by going after the tech giant's data centers. [...]
