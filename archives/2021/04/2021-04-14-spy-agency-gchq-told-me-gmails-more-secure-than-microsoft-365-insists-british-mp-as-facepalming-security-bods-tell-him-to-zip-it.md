Title: Spy agency GCHQ told me Gmail's more secure than Microsoft 365, insists British MP as facepalming security bods tell him to zip it
Date: 2021-04-14T09:16:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: spy-agency-gchq-told-me-gmails-more-secure-than-microsoft-365-insists-british-mp-as-facepalming-security-bods-tell-him-to-zip-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/14/tom_tugendhat_email_security_outburst/){:target="_blank" rel="noopener"}

> Keep using the provided tools, NCSC says Conservative MP Tom Tugendhat has publicly claimed GCHQ sources told him Gmail was more secure than Parliament’s own Microsoft Office 365 deployment – but both Parliament and a GCHQ offshoot have told him to stop being silly.... [...]
