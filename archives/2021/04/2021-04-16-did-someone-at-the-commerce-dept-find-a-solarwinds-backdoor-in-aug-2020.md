Title: Did Someone at the Commerce Dept. Find a SolarWinds Backdoor in Aug. 2020?
Date: 2021-04-16T12:57:19+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;CVE-2020-4006;Cybersecurity Infrastructure Security Administration;Democratic National Committee;fbi;FireEye;GoldMax;Lexicon.exe;microsoft;national security agency;National Telecommunications and Information Administration;NTIA;SolarWinds hack;Sunshuttle;The Wall Street Journal;U.S. Commerce Department;U.S. Treasury Department;virustotal;VMware
Slug: did-someone-at-the-commerce-dept-find-a-solarwinds-backdoor-in-aug-2020

[Source](https://krebsonsecurity.com/2021/04/did-someone-at-the-commerce-dept-find-a-solarwinds-backdoor-in-aug-2020/){:target="_blank" rel="noopener"}

> On Aug. 13, 2020, someone uploaded a suspected malicious file to VirusTotal, a service that scans submitted files against more than five dozen antivirus and security products. Last month, Microsoft and FireEye identified that file as a newly-discovered fourth malware backdoor used in the sprawling SolarWinds supply chain hack. An analysis of the malicious file and other submissions by the same VirusTotal user suggest the account that initially flagged the backdoor as suspicious belongs to IT personnel at the National Telecommunications and Information Administration (NTIA), a division of the U.S. Commerce Department that handles telecommunications and Internet policy. Both Microsoft [...]
