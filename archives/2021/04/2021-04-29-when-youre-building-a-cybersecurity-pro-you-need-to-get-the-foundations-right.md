Title: When you’re building a cybersecurity pro, you need to get the foundations right
Date: 2021-04-29T08:00:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: when-youre-building-a-cybersecurity-pro-you-need-to-get-the-foundations-right

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/29/new_starter_or_mid_career_switcher/){:target="_blank" rel="noopener"}

> New starter or mid-career switcher? Here’s where to start Promo Cyber attackers are a diverse lot. They can strike from anywhere in the world, and may be motivated by greed, politics, status, or pure malevolence. And their techniques range from the dazzlingly sophisticated to the frankly crude, technically speaking.... [...]
