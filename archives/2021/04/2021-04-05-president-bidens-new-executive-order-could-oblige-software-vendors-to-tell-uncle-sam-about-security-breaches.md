Title: President Biden’s new executive order could oblige software vendors to tell Uncle Sam about security breaches
Date: 2021-04-05T14:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: president-bidens-new-executive-order-could-oblige-software-vendors-to-tell-uncle-sam-about-security-breaches

[Source](https://portswigger.net/daily-swig/president-bidens-new-executive-order-could-oblige-software-vendors-to-tell-uncle-sam-about-security-breaches){:target="_blank" rel="noopener"}

> Prompt disclosure shake-up follows SolarWinds calamity [...]
