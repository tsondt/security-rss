Title: How do we stamp out the ransomware business model? Ban insurance payouts for one, says ex-GCHQ director
Date: 2021-04-09T10:02:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: how-do-we-stamp-out-the-ransomware-business-model-ban-insurance-payouts-for-one-says-ex-gchq-director

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/09/ban_cyber_insurance_payouts/){:target="_blank" rel="noopener"}

> New laws needed to cut off incentive to crooks, argues Marcus Willett Increasing numbers of senior ex-GCHQ people have called for laws preventing businesses using cyber insurance to buy off ransomware attackers – with the money merely perpetuating the criminals' business model.... [...]
