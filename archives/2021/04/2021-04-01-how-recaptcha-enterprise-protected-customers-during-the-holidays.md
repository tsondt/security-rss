Title: How reCAPTCHA Enterprise protected customers during the holidays
Date: 2021-04-01T17:30:00+00:00
Author: Kelly Anderson
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: how-recaptcha-enterprise-protected-customers-during-the-holidays

[Source](https://cloud.google.com/blog/products/identity-security/customers-protected-over-the-2020-holidays-with-recaptcha-enterprise/){:target="_blank" rel="noopener"}

> Every business had to adapt to a new reality in 2020, and make online business their primary channel. But as online business increased, so did web-based attacks. In research commissioned by Forrester consulting, 84% of companies have seen an increase in bot attacks. 71% of organizations have seen an increase in the amount of successful attacks. 65% of businesses have experienced more frequent attacks and greater revenue loss due to bot attacks. With online fraud expected to only increase, the security of web pages has never been more important. Online fraud and abuse impacts various industries differently, ranging from inventory [...]
