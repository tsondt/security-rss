Title: Keyless API authentication—Better cloud security through workload identity federation, no service account keys necessary
Date: 2021-04-08T16:00:00+00:00
Author: Vignesh Rajamani
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: keyless-api-authentication-better-cloud-security-through-workload-identity-federation-no-service-account-keys-necessary

[Source](https://cloud.google.com/blog/products/identity-security/enable-keyless-access-to-gcp-with-workload-identity-federation/){:target="_blank" rel="noopener"}

> Organizations often have applications that run on multiple platforms, on-premises or cloud. For such applications that call Google Cloud Platform (GCP) APIs, a common challenge admins face is securing long-lived service account keys used to authenticate to GCP. Examples of such applications might include: Analytics workloads running on AWS or Azure that access sensitive datasets stored in Google Cloud Storage CI/CD pipelines that use external tools such as Terraform to provision projects and VMs on GCP Microservice-based apps running on GKE that connect to one or more GCP services. Since these applications rely on service account keys to access GCP [...]
