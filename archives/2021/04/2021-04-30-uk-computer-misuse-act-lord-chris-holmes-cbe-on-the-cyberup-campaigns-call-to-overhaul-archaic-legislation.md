Title: <span>UK Computer Misuse Act: Lord Chris Holmes CBE on the CyberUp campaign’s call to overhaul ‘archaic’ legislation</span>
Date: 2021-04-30T15:37:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uk-computer-misuse-act-lord-chris-holmes-cbe-on-the-cyberup-campaigns-call-to-overhaul-archaic-legislation

[Source](https://portswigger.net/daily-swig/uk-computer-misuse-act-lord-chris-holmes-cbe-on-the-cyberup-campaigns-call-to-overhaul-archaic-legislation){:target="_blank" rel="noopener"}

> Government peer tells The Daily Swig that legislators should aim to future-proof UK cybercrime laws [...]
