Title: Popular Codecov code coverage tool hacked to steal dev credentials
Date: 2021-04-16T10:44:37-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: popular-codecov-code-coverage-tool-hacked-to-steal-dev-credentials

[Source](https://www.bleepingcomputer.com/news/security/popular-codecov-code-coverage-tool-hacked-to-steal-dev-credentials/){:target="_blank" rel="noopener"}

> Codecov online platform for hosted code testing reports and statistics announced on Thursday that a threat actor had modified its Bash Uploader script, exposing sensitive information in customers' continuous integration (CI) environment. [...]
