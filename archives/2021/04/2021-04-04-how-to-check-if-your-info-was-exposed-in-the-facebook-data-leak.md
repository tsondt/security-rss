Title: How to check if your info was exposed in the Facebook data leak
Date: 2021-04-04T14:28:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: how-to-check-if-your-info-was-exposed-in-the-facebook-data-leak

[Source](https://www.bleepingcomputer.com/news/security/how-to-check-if-your-info-was-exposed-in-the-facebook-data-leak/){:target="_blank" rel="noopener"}

> Data breach notification service Have I Been Pwned can now be used to check if your personal information was exposed in yesterday's Facebook data leak that contains the phone numbers and information for over 500 million users. [...]
