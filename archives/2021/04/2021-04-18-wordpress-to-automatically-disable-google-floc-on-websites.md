Title: WordPress to automatically disable Google FLoC on websites
Date: 2021-04-18T15:12:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Technology
Slug: wordpress-to-automatically-disable-google-floc-on-websites

[Source](https://www.bleepingcomputer.com/news/security/wordpress-to-automatically-disable-google-floc-on-websites/){:target="_blank" rel="noopener"}

> WordPress announced today that they plan on treating Google's new FLoC tracking technology as a security concern and plans to block it by default on WordPress sites. [...]
