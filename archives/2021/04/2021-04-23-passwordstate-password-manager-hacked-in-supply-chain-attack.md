Title: Passwordstate password manager hacked in supply chain attack
Date: 2021-04-23T16:18:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: passwordstate-password-manager-hacked-in-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/passwordstate-password-manager-hacked-in-supply-chain-attack/){:target="_blank" rel="noopener"}

> Click Studios, the company behind the Passwordstate password manager, notified customers that attackers compromised the app's update mechanism to deliver malware in a supply-chain attack after breaching its networks. [...]
