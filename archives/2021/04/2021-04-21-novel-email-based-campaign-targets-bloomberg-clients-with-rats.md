Title: Novel Email-Based Campaign Targets Bloomberg Clients with RATs
Date: 2021-04-21T12:00:41+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: novel-email-based-campaign-targets-bloomberg-clients-with-rats

[Source](https://threatpost.com/email-campaign-targets-bloomberg-clients/165514/){:target="_blank" rel="noopener"}

> Attacks dubbed ‘Fajan’ by researchers are specifically targeted and appear to be testing various threat techniques to find ones with the greatest impact. [...]
