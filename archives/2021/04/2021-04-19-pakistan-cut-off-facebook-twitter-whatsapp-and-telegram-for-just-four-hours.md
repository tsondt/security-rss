Title: Pakistan cut off Facebook, Twitter, WhatsApp, and Telegram – for just four hours
Date: 2021-04-19T05:58:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: pakistan-cut-off-facebook-twitter-whatsapp-and-telegram-for-just-four-hours

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/19/pakistan_brief_social_media_ban/){:target="_blank" rel="noopener"}

> To stop protests by far-right party that wants France’s ambassador expelled Pakistan shut down several social networks within its borders on Friday but lifted the ban after around four hours.... [...]
