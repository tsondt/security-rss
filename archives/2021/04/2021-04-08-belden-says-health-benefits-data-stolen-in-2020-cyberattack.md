Title: Belden says health benefits data stolen in 2020 cyberattack
Date: 2021-04-08T13:25:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: belden-says-health-benefits-data-stolen-in-2020-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/belden-says-health-benefits-data-stolen-in-2020-cyberattack/){:target="_blank" rel="noopener"}

> Belden has disclosed that additional data was accessed and copied during their November 2020 cyberattack related to employees' healthcare benefits and family members covered under their plan. [...]
