Title: Inference attacks: How much information can machine learning models leak?
Date: 2021-04-14T12:48:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: inference-attacks-how-much-information-can-machine-learning-models-leak

[Source](https://portswigger.net/daily-swig/inference-attacks-how-much-information-can-machine-learning-models-leak){:target="_blank" rel="noopener"}

> Academics warn that user privacy may fall at the hands of little-known attack vector [...]
