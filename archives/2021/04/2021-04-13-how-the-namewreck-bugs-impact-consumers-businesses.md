Title: How the NAME:WRECK Bugs Impact Consumers, Businesses
Date: 2021-04-13T21:03:41+00:00
Author: Tom Spring
Category: Threatpost
Tags: Critical Infrastructure;IoT;Vulnerabilities
Slug: how-the-namewreck-bugs-impact-consumers-businesses

[Source](https://threatpost.com/namewreck-bugs-businesses/165385/){:target="_blank" rel="noopener"}

> How this class of vulnerabilities will impact millions connected devices and potentially wreck the day of IT security professionals. [...]
