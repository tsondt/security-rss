Title: 4 Innovative Ways Cyberattackers Hunt for Security Bugs
Date: 2021-04-21T20:49:29+00:00
Author: David “moose” Wolpoff
Category: Threatpost
Tags: Hacks;InfoSec Insider;Vulnerabilities;Web Security
Slug: 4-innovative-ways-cyberattackers-hunt-for-security-bugs

[Source](https://threatpost.com/4-ways-attackers-hunt-bugs/165536/){:target="_blank" rel="noopener"}

> David “moose” Wolpoff, co-founder and CTO at Randori, talks lesser-known hacking paths, including unresolved "fixme" flags in developer support groups. [...]
