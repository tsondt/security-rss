Title: VMware fixes authentication bypass in data center security software
Date: 2021-04-01T12:58:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Linux
Slug: vmware-fixes-authentication-bypass-in-data-center-security-software

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-authentication-bypass-in-data-center-security-software/){:target="_blank" rel="noopener"}

> VMware has addressed a critical vulnerability in the VMware Carbon Black Cloud Workload appliance that could allow attackers to bypass authentication after exploiting vulnerable servers. [...]
