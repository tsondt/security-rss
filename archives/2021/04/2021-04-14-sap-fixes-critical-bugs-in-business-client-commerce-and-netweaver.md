Title: SAP fixes critical bugs in Business Client, Commerce, and NetWeaver
Date: 2021-04-14T14:39:17-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: sap-fixes-critical-bugs-in-business-client-commerce-and-netweaver

[Source](https://www.bleepingcomputer.com/news/security/sap-fixes-critical-bugs-in-business-client-commerce-and-netweaver/){:target="_blank" rel="noopener"}

> SAP's security updates for this month address multiple critical vulnerabilities. The most serious of them, rated with the highest severity score, affects the company's Business Client product. [...]
