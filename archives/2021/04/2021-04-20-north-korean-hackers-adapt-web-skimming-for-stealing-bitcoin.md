Title: North Korean hackers adapt web skimming for stealing Bitcoin
Date: 2021-04-20T03:23:19-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: north-korean-hackers-adapt-web-skimming-for-stealing-bitcoin

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-adapt-web-skimming-for-stealing-bitcoin/){:target="_blank" rel="noopener"}

> Hackers linked with the North Korean government applied the web skimming technique to steal cryptocurrency in a previously undocumented campaign that started early last year, researchers say. [...]
