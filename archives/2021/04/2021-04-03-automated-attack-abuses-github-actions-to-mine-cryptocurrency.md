Title: Automated attack abuses GitHub Actions to mine cryptocurrency
Date: 2021-04-03T05:49:56-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: automated-attack-abuses-github-actions-to-mine-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/security/automated-attack-abuses-github-actions-to-mine-cryptocurrency/){:target="_blank" rel="noopener"}

> GitHub Actions has been abused by attackers to mine cryptocurrency using GitHub's servers, automatically.The particular attack adds malicious GitHub Actions code to repositories forked from legitimate ones, and further creates a Pull Request for the original repository maintainers to merge the code back, to alter the original code. [...]
