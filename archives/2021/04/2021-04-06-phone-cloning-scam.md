Title: Phone Cloning Scam
Date: 2021-04-06T11:05:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cell phones;cloning;scams;Uber
Slug: phone-cloning-scam

[Source](https://www.schneier.com/blog/archives/2021/04/phone-cloning-scam.html){:target="_blank" rel="noopener"}

> A newspaper in Malaysia is reporting on a cell phone cloning scam. The scammer convinces the victim to lend them their cell phone, and the scammer quickly clones it. What’s clever about this scam is that the victim is an Uber driver and the scammer is the passenger, so the driver is naturally busy and can’t see what the scammer is doing. [...]
