Title: Google Sites blight: Over 100,000 web pages for business form searches overrun with backdoor RATs
Date: 2021-04-14T01:22:34+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: google-sites-blight-over-100000-web-pages-for-business-form-searches-overrun-with-backdoor-rats

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/14/google_sites_malware/){:target="_blank" rel="noopener"}

> eSentire warns of remote-access trojans masquerading as PDFs More than 100,000 web pages hosted by Google Sites are being used to trick netizens into opening business documents booby-trapped with a remote-access trojan (RAT) that takes over victims' PCs and hands control to miscreants.... [...]
