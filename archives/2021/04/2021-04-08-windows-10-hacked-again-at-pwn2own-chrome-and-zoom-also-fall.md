Title: Windows 10 hacked again at Pwn2Own, Chrome and Zoom also fall
Date: 2021-04-08T10:33:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: windows-10-hacked-again-at-pwn2own-chrome-and-zoom-also-fall

[Source](https://www.bleepingcomputer.com/news/security/windows-10-hacked-again-at-pwn2own-chrome-and-zoom-also-fall/){:target="_blank" rel="noopener"}

> Contestants hacked Microsoft's Windows 10 OS twice during the second day of the Pwn2Own 2021 competition, together with the Google Chrome web browser and the Zoom video communication platform. [...]
