Title: Happy Friday? Darktrace gets 40 per cent boost on London IPO debut
Date: 2021-04-30T15:10:06+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: happy-friday-darktrace-gets-40-per-cent-boost-on-london-ipo-debut

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/30/darktrace_ipo/){:target="_blank" rel="noopener"}

> AI infosec start-up avoids same opening day peril as Deliveroo British AI-powered security startup Darktrace has enjoyed a bumper IPO Friday as its shares climbed 40 per cent on its London Stock Exchange debut.... [...]
