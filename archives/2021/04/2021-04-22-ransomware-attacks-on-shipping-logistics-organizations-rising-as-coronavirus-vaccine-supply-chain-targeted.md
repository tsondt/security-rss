Title: Ransomware attacks on shipping, logistics organizations rising as coronavirus vaccine supply chain targeted
Date: 2021-04-22T13:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ransomware-attacks-on-shipping-logistics-organizations-rising-as-coronavirus-vaccine-supply-chain-targeted

[Source](https://portswigger.net/daily-swig/ransomware-attacks-on-shipping-logistics-organizations-rising-as-coronavirus-vaccine-supply-chain-targeted){:target="_blank" rel="noopener"}

> No honor among thieves [...]
