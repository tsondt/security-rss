Title: How to deploy Certificate Authority Service
Date: 2021-04-09T16:00:00+00:00
Author: Anoosh Saboori
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: how-to-deploy-certificate-authority-service

[Source](https://cloud.google.com/blog/products/identity-security/how-to-deploy-google-cas-whitepaper/){:target="_blank" rel="noopener"}

> Certificate Authority Service (CAS) is a highly available, scalable Google Cloud service that enables you to simplify, automate, and customize the deployment, management, and security of private certificate authorities (CA). As it nears general availability, we want to provide guidance on how to deploy the service in real world scenarios. Today we’re releasing a whitepaper about CAS that explains exactly that. And if you want to learn more about how and why CAS was built, we have a paper on that too. “How to deploy a secure and reliable public key infrastructure with Google Cloud Certificate Authority Service” (written by [...]
