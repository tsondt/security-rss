Title: What COVID-19 Taught Us: Prepping Cybersecurity for the Next Crisis
Date: 2021-04-19T15:27:38+00:00
Author: Sivan Tehila
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;InfoSec Insider;IoT;Web Security
Slug: what-covid-19-taught-us-prepping-cybersecurity-for-the-next-crisis

[Source](https://threatpost.com/covid-19-prepping-cybersecurity-crisis/165472/){:target="_blank" rel="noopener"}

> Sivan Tehila, cybersecurity strategist at Perimeter 81, discusses climate change and the cyber-resilience lessons companies should take away from dealing with the pandemic. [...]
