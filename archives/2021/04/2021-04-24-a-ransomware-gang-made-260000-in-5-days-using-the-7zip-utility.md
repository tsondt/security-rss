Title: A ransomware gang made $260,000 in 5 days using the 7zip utility
Date: 2021-04-24T12:06:51-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: a-ransomware-gang-made-260000-in-5-days-using-the-7zip-utility

[Source](https://www.bleepingcomputer.com/news/security/a-ransomware-gang-made-260-000-in-5-days-using-the-7zip-utility/){:target="_blank" rel="noopener"}

> A ransomware gang has made $260,000 in just five days simply by remotely encrypting files on QNAP devices using the 7zip archive program. [...]
