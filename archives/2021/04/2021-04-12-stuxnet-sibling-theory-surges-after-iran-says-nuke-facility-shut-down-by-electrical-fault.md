Title: Stuxnet sibling theory surges after Iran says nuke facility shut down by electrical fault
Date: 2021-04-12T06:57:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: stuxnet-sibling-theory-surges-after-iran-says-nuke-facility-shut-down-by-electrical-fault

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/12/iran_cyber_attack_theory/){:target="_blank" rel="noopener"}

> Evidence is thin, but Natanz enrichment facility is offline Iran has admitted that one of its nuclear facilities went offline over the weekend, and a single report claiming Israeli cyber-weapons were the cause has been widely accepted as a credible explanation for the incident.... [...]
