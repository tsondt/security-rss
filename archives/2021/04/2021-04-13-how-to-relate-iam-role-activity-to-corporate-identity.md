Title: How to relate IAM role activity to corporate identity
Date: 2021-04-13T23:18:03+00:00
Author: Tracy Pierce
Category: AWS Security
Tags: AWS Identity and Access Management (IAM);Security, Identity, & Compliance;AWS STS;Federation;Identity;Security Blog;Security token service;Sessions;Tracing
Slug: how-to-relate-iam-role-activity-to-corporate-identity

[Source](https://aws.amazon.com/blogs/security/how-to-relate-iam-role-activity-to-corporate-identity/){:target="_blank" rel="noopener"}

> AWS Security Token Service (AWS STS) now offers customers the ability to specify a unique identity attribute for their workforce identities and applications when they assume an AWS Identity and Access Management (IAM) role. This new SourceIdentity attribute makes it easier for you, as an Amazon Web Services (AWS) administrator, to determine the identity that [...]
