Title: Gafgyt Botnet Lifts DDoS Tricks from Mirai
Date: 2021-04-15T16:35:53+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Malware;Vulnerabilities;Web Security
Slug: gafgyt-botnet-lifts-ddos-tricks-from-mirai

[Source](https://threatpost.com/gafgyt-botnet-ddos-mirai/165424/){:target="_blank" rel="noopener"}

> The IoT-targeted malware has also added new exploits for initial compromise, for Huawei, Realtek and Dasan GPON devices. [...]
