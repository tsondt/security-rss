Title: What the FLoC? Browser makers queue up to decry Google's latest ad-targeting initiative as invasive tracking
Date: 2021-04-14T19:33:43+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: what-the-floc-browser-makers-queue-up-to-decry-googles-latest-ad-targeting-initiative-as-invasive-tracking

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/14/browser_makers_reject_google_floc/){:target="_blank" rel="noopener"}

> 'Federated Learning of Cohorts' groups users together and is already being tested in Chrome Updated Google's FLoC (Federated Learning of Cohorts) mechanism for ad personalisation, currently being trialled in the Chrome browser, has been rejected as privacy-invasive tracking by other browser makers including Vivaldi and Brave.... [...]
