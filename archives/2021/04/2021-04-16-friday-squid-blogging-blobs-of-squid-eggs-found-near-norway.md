Title: Friday Squid Blogging: Blobs of Squid Eggs Found Near Norway
Date: 2021-04-16T21:09:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-blobs-of-squid-eggs-found-near-norway

[Source](https://www.schneier.com/blog/archives/2021/04/friday-squid-blogging-blobs-of-squid-eggs-found-near-norway.html){:target="_blank" rel="noopener"}

> Divers find three-foot “blobs” — egg sacs of the squid Illex coindetii — off the coast of Norway. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
