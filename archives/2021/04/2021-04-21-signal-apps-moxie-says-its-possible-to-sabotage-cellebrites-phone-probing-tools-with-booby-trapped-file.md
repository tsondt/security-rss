Title: Signal app's Moxie says it's possible to sabotage Cellebrite's phone-probing tools with booby-trapped file
Date: 2021-04-21T22:04:01+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: signal-apps-moxie-says-its-possible-to-sabotage-cellebrites-phone-probing-tools-with-booby-trapped-file

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/21/signal_cellebrite/){:target="_blank" rel="noopener"}

> More a declaration of war than turning the tables Updated It is possible to hijack and manipulate Cellebrite's phone-probing software tools by placing a specially crafted file on your handset, it is claimed.... [...]
