Title: Conti Gang Demands $40M Ransom from Florida School District
Date: 2021-04-06T13:59:11+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: conti-gang-demands-40m-ransom-from-florida-school-district

[Source](https://threatpost.com/conti-40m-ransom-florida-school/165258/){:target="_blank" rel="noopener"}

> New details of negotiation between attackers and officials from Broward County Public Schools emerge after a ransomware attack early last month. [...]
