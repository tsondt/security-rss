Title: The FBI Is Now Securing Networks Without Their Owners’ Permission
Date: 2021-04-14T14:56:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: China;courts;cyberespionage;espionage;FBI;Microsoft;zero-day
Slug: the-fbi-is-now-securing-networks-without-their-owners-permission

[Source](https://www.schneier.com/blog/archives/2021/04/the-fbi-is-now-securing-networks-without-their-owners-permission.html){:target="_blank" rel="noopener"}

> In January, we learned about a Chinese espionage campaign that exploited four zero-days in Microsoft Exchange. One of the characteristics of the campaign, in the later days when the Chinese probably realized that the vulnerabilities would soon be fixed, was to install a web shell in compromised networks that would give them subsequent remote access. Even if the vulnerabilities were patched, the shell would remain until the network operators removed it. Now, months later, many of those shells are still in place. And they’re being used by criminal hackers as well. On Tuesday, the FBI announced that it successfully received [...]
