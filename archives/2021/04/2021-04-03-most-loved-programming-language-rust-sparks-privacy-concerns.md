Title: Most loved programming language Rust sparks privacy concerns
Date: 2021-04-03T10:42:51-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: most-loved-programming-language-rust-sparks-privacy-concerns

[Source](https://www.bleepingcomputer.com/news/security/most-loved-programming-language-rust-sparks-privacy-concerns/){:target="_blank" rel="noopener"}

> Rust developers have repeatedly raised concerned about a privacy issue over the last few years. Rust has rapidly gained momentum among developers, for its focus on performance, safety, safe concurrency, and for having a similar syntax to C++. However, developers have been bothered by their Rust production binaries leaking usernames. [...]
