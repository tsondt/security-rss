Title: New Cring ransomware hits unpatched Fortinet VPN devices
Date: 2021-04-07T13:12:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: new-cring-ransomware-hits-unpatched-fortinet-vpn-devices

[Source](https://www.bleepingcomputer.com/news/security/new-cring-ransomware-hits-unpatched-fortinet-vpn-devices/){:target="_blank" rel="noopener"}

> A vulnerability impacting Fortinet VPNs is being exploited by a new human-operated ransomware strain known as Cring to breach and encrypt industrial sector companies' networks. [...]
