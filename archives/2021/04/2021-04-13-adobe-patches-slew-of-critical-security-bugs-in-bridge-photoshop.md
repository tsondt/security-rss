Title: Adobe Patches Slew of Critical Security Bugs in Bridge, Photoshop
Date: 2021-04-13T16:40:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: adobe-patches-slew-of-critical-security-bugs-in-bridge-photoshop

[Source](https://threatpost.com/adobe-patches-critical-security-holes-bridge-photoshop/165371/){:target="_blank" rel="noopener"}

> The security bugs could open the door for arbitrary code-execution and full takeover of targeted machines. [...]
