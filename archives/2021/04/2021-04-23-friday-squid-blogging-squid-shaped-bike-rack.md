Title: Friday Squid Blogging: Squid-Shaped Bike Rack
Date: 2021-04-23T21:01:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-squid-shaped-bike-rack

[Source](https://www.schneier.com/blog/archives/2021/04/friday-squid-blogging-squid-shaped-bike-rack.html){:target="_blank" rel="noopener"}

> There’s a new squid-shaped bike rack in Ballard, WA. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
