Title: SMS phishing scam lures Rogers customers with outage refunds
Date: 2021-04-28T11:56:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: sms-phishing-scam-lures-rogers-customers-with-outage-refunds

[Source](https://www.bleepingcomputer.com/news/security/sms-phishing-scam-lures-rogers-customers-with-outage-refunds/){:target="_blank" rel="noopener"}

> Cybercriminals target Rogers customers with a new SMS phishing campaign pretending to be refunds for last week's Canada-wide wireless outage. [...]
