Title: FBI shares 4 million email addresses used by Emotet with Have I Been Pwned
Date: 2021-04-27T12:18:57-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: fbi-shares-4-million-email-addresses-used-by-emotet-with-have-i-been-pwned

[Source](https://www.bleepingcomputer.com/news/security/fbi-shares-4-million-email-addresses-used-by-emotet-with-have-i-been-pwned/){:target="_blank" rel="noopener"}

> Millions of email addresses collected by Emotet botnet for malware distribution campaigns have been shared by the Federal Bureau of Investigation (FBI) as part of the agency's effort to clean infected computers. [...]
