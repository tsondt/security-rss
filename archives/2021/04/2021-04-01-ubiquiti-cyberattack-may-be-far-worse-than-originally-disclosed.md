Title: Ubiquiti cyberattack may be far worse than originally disclosed
Date: 2021-04-01T03:04:05-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: ubiquiti-cyberattack-may-be-far-worse-than-originally-disclosed

[Source](https://www.bleepingcomputer.com/news/security/ubiquiti-cyberattack-may-be-far-worse-than-originally-disclosed/){:target="_blank" rel="noopener"}

> The data breach report from Ubiquiti in January is allegedly a cover-up of a massive incident that put at risk customer data and devices deployed on corporate and home networks. [...]
