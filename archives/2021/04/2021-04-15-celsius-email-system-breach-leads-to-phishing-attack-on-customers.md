Title: Celsius email system breach leads to phishing attack on customers
Date: 2021-04-15T16:05:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: celsius-email-system-breach-leads-to-phishing-attack-on-customers

[Source](https://www.bleepingcomputer.com/news/security/celsius-email-system-breach-leads-to-phishing-attack-on-customers/){:target="_blank" rel="noopener"}

> Cryptocurrency rewards platform Celsius Network has disclosed a security breach exposing customer information that led to a phishing attack. [...]
