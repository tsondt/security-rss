Title: ‘We are not motivated by profits’ – Open Bug Bounty maintainers on finding a niche in the crowdsourced AppSec market
Date: 2021-04-22T13:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: we-are-not-motivated-by-profits-open-bug-bounty-maintainers-on-finding-a-niche-in-the-crowdsourced-appsec-market

[Source](https://portswigger.net/daily-swig/we-are-not-motivated-by-profits-open-bug-bounty-maintainers-on-finding-a-niche-in-the-crowdsourced-appsec-market){:target="_blank" rel="noopener"}

> Vulnerability disclosure platform driven by ‘transparency and fairness’, with over 500,000 bugs fixed since 2014 [...]
