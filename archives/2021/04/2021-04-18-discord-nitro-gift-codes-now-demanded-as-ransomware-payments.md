Title: Discord Nitro gift codes now demanded as ransomware payments
Date: 2021-04-18T14:10:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: discord-nitro-gift-codes-now-demanded-as-ransomware-payments

[Source](https://www.bleepingcomputer.com/news/security/discord-nitro-gift-codes-now-demanded-as-ransomware-payments/){:target="_blank" rel="noopener"}

> In a novel approach to ransom demands, a new ransomware calling itself 'NitroRansomware' encrypts victim's files and then demands a Discord Nitro gift code to decrypt files. [...]
