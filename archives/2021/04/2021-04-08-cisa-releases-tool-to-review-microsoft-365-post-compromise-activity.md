Title: CISA releases tool to review Microsoft 365 post-compromise activity
Date: 2021-04-08T17:39:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-releases-tool-to-review-microsoft-365-post-compromise-activity

[Source](https://www.bleepingcomputer.com/news/security/cisa-releases-tool-to-review-microsoft-365-post-compromise-activity/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has released a companion Splunk-based dashboard that helps review post-compromise activity in Microsoft Azure Active Directory (AD), Office 365 (O365), and Microsoft 365 (M365) environments. [...]
