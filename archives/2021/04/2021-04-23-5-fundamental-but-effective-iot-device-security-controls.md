Title: 5 Fundamental But Effective IoT Device Security Controls
Date: 2021-04-23T17:13:00+00:00
Author: Matt Dunn
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;IoT;Malware;Vulnerabilities;Web Security
Slug: 5-fundamental-but-effective-iot-device-security-controls

[Source](https://threatpost.com/5-fundamental-iot-device-security-controls/165577/){:target="_blank" rel="noopener"}

> Matt Dunn, the associate managing director for cyber-risk at Kroll, discusses how to keep networks safe from insecure IoT devices. [...]
