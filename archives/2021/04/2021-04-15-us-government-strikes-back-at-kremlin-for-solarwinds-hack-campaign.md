Title: US government strikes back at Kremlin for SolarWinds hack campaign
Date: 2021-04-15T20:17:39+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;hacking;russia;sanctions;solarwinds;Treasury Department
Slug: us-government-strikes-back-at-kremlin-for-solarwinds-hack-campaign

[Source](https://arstechnica.com/?p=1757269){:target="_blank" rel="noopener"}

> Enlarge (credit: Matt Anderson Photography/Getty Images) US officials on Thursday formally blamed Russia for backing one of the worst espionage hacks in recent US history and imposed sanctions designed to mete out punishments for that and other recent actions. In a joint advisory, the National Security Agency, FBI, and Cybersecurity and Information Security Agency said that Russia’s Foreign Intelligence Service, abbreviated as the SVR, carried out the supply-chain attack on customers of the network management software from Austin, Texas-based SolarWinds. The operation infected SolarWinds’ software build and distribution system and used it to push backdoored updates to about 18,000 customers. [...]
