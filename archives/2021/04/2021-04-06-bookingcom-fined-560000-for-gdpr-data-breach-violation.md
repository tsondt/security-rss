Title: Booking.com fined $560,000 for GDPR data breach violation
Date: 2021-04-06T16:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: bookingcom-fined-560000-for-gdpr-data-breach-violation

[Source](https://portswigger.net/daily-swig/booking-com-fined-560-000-for-gdpr-data-breach-violation){:target="_blank" rel="noopener"}

> Netherlands-based company failed to act quickly enough, says regulator [...]
