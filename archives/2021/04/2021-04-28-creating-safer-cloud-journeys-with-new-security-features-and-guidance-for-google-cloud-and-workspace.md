Title: Creating safer cloud journeys with new security features and guidance for Google Cloud and Workspace
Date: 2021-04-28T16:00:00+00:00
Author: Sam Lugani
Category: GCP Security
Tags: Google Cloud Platform;Google Workspace;Identity & Security
Slug: creating-safer-cloud-journeys-with-new-security-features-and-guidance-for-google-cloud-and-workspace

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-security-feature-rollout-for-spring-2021/){:target="_blank" rel="noopener"}

> One of the core benefits of using cloud technology to help modernize your security program is the ever-growing set of provider capabilities that you can use to protect your users, applications, and data. As part of our commitment to be our customers’ most Trusted Cloud, we’re constantly adding new security features to Google Cloud and Google Workspace, as well as helpful guidance on how to solve security challenges and improve your security posture with the help of our tools. We've got a bundle of new security features, whitepapers and demos to announce today, which can all help to create safer [...]
