Title: Popular Twitch AdBlock shuts down after Twitch breaks extension
Date: 2021-04-02T12:54:07-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Google
Slug: popular-twitch-adblock-shuts-down-after-twitch-breaks-extension

[Source](https://www.bleepingcomputer.com/news/security/popular-twitch-adblock-shuts-down-after-twitch-breaks-extension/){:target="_blank" rel="noopener"}

> The popular Twitch AdBlock extension has been removed from both Chrome Web Store and Firefox Addons. Twitch AdBlock was the choice of extension among Twitch users for restricting ads when using Twitch. The extension's author stated before its removal, the ad blocker had over 150,000 users and 6 million daily views. [...]
