Title: Task Force Seeks to Disrupt Ransomware Payments
Date: 2021-04-29T12:26:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;The Coming Storm;Amazon;Cisco;Department of Homeland Security;disrupting ransomware payments;Emsisoft;Europol;fbi;FireEye;Institute for Security and Technology;mcafee;microsoft;Philip Reiner;ransomware;The Wall Street Journal;U.K. National Crime Agency;U.S. Justice Department;U.S. Treasury Department
Slug: task-force-seeks-to-disrupt-ransomware-payments

[Source](https://krebsonsecurity.com/2021/04/task-force-seeks-to-disrupt-ransomware-payments/){:target="_blank" rel="noopener"}

> Some of the world’s top tech firms are backing a new industry task force focused on disrupting cybercriminal ransomware gangs by limiting their ability to get paid, and targeting the individuals and finances of the organized thieves behind these crimes. In a 81-page report delivered to the Biden administration this week, top executives from Amazon, Cisco, FireEye, McAfee, Microsoft and dozens of other firms joined the U.S. Department of Justice (DOJ), Europol and the U.K. National Crime Agency in calling for an international coalition to combat ransomware criminals, and for a global network of ransomware investigation hubs. The Ransomware Task [...]
