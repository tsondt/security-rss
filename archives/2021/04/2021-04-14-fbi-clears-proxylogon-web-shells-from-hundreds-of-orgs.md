Title: FBI Clears ProxyLogon Web Shells from Hundreds of Orgs
Date: 2021-04-14T17:31:13+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware;Vulnerabilities;Web Security
Slug: fbi-clears-proxylogon-web-shells-from-hundreds-of-orgs

[Source](https://threatpost.com/fbi-proxylogon-web-shells/165400/){:target="_blank" rel="noopener"}

> In a veritable cyber-SWAT action, the Feds remotely removed the infections without warning businesses beforehand. [...]
