Title: New KrebsOnSecurity Mobile-Friendly Site
Date: 2021-04-01T20:19:23+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other
Slug: new-krebsonsecurity-mobile-friendly-site

[Source](https://krebsonsecurity.com/2021/04/new-krebsonsecurity-mobile-friendly-site/){:target="_blank" rel="noopener"}

> Dear Readers, this has been long overdue, but at last I give you a more responsive, mobile-friendly version of KrebsOnSecurity. We tried to keep the visual changes to a minimum and focus on a simple theme that presents information in a straightforward, easy-to-read format. Please bear with us over the next few days as we hunt down the gremlins in the gears. We were shooting for responsive (fast) and uncluttered. Hopefully, we achieved that and this new design will render well in whatever device you use to view it. If something looks amiss, please don’t hesitate to drop a note [...]
