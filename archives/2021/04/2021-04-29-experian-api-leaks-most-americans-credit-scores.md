Title: Experian API Leaks Most Americans’ Credit Scores
Date: 2021-04-29T18:42:59+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Web Security
Slug: experian-api-leaks-most-americans-credit-scores

[Source](https://threatpost.com/experian-api-leaks-american-credit-scores/165731/){:target="_blank" rel="noopener"}

> Researchers fear wider exposure, amidst a tepid response from Experian. [...]
