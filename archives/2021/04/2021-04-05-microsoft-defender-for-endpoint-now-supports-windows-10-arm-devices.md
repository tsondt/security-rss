Title: Microsoft Defender for Endpoint now supports Windows 10 Arm devices
Date: 2021-04-05T15:41:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-for-endpoint-now-supports-windows-10-arm-devices

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-for-endpoint-now-supports-windows-10-arm-devices/){:target="_blank" rel="noopener"}

> Microsoft today announced that Microsoft Defender for Endpoint, the enterprise version of its Defender antivirus, now comes with support for Windows 10 on Arm devices. [...]
