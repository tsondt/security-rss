Title: Codecov users warned after backdoor discovered in DevOps tool
Date: 2021-04-19T13:25:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: codecov-users-warned-after-backdoor-discovered-in-devops-tool

[Source](https://portswigger.net/daily-swig/codecov-users-warned-after-backdoor-discovered-in-devops-tool){:target="_blank" rel="noopener"}

> Credential-slurping code lingered in Bash Uploader script for months [...]
