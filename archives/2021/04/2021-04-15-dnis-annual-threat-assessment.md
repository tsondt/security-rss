Title: DNI’s Annual Threat Assessment
Date: 2021-04-15T11:13:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;national security policy;supply chain;threat models
Slug: dnis-annual-threat-assessment

[Source](https://www.schneier.com/blog/archives/2021/04/dnis-annual-threat-assessment.html){:target="_blank" rel="noopener"}

> The office of the Director of National Intelligence released its “ Annual Threat Assessment of the U.S. Intelligence Community.” Cybersecurity is covered on pages 20-21. Nothing surprising: Cyber threats from nation states and their surrogates will remain acute. States’ increasing use of cyber operations as a tool of national power, including increasing use by militaries around the world, raises the prospect of more destructive and disruptive cyber activity. Authoritarian and illiberal regimes around the world will increasingly exploit digital tools to surveil their citizens, control free expression, and censor and manipulate information to maintain control over their populations. During the [...]
