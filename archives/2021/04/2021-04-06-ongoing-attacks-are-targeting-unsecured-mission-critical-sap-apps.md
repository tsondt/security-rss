Title: Ongoing attacks are targeting unsecured mission-critical SAP apps
Date: 2021-04-06T09:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ongoing-attacks-are-targeting-unsecured-mission-critical-sap-apps

[Source](https://www.bleepingcomputer.com/news/security/ongoing-attacks-are-targeting-unsecured-mission-critical-sap-apps/){:target="_blank" rel="noopener"}

> Threat actors are targeting mission-critical SAP enterprise applications unsecured against already patched vulnerabilities, exposing the networks of commercial and government organizations to attacks. [...]
