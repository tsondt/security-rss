Title: Dutch supermarkets run out of cheese after ransomware attack
Date: 2021-04-12T12:54:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: dutch-supermarkets-run-out-of-cheese-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/dutch-supermarkets-run-out-of-cheese-after-ransomware-attack/){:target="_blank" rel="noopener"}

> A ransomware attack against conditioned warehousing and transportation provider Bakker Logistiek has caused a cheese shortage in Dutch supermarkets. [...]
