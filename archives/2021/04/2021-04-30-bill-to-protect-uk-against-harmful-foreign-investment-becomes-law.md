Title: Bill to protect UK against harmful foreign investment becomes law
Date: 2021-04-30T16:52:15+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: bill-to-protect-uk-against-harmful-foreign-investment-becomes-law

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/30/national_security_investment_act/){:target="_blank" rel="noopener"}

> Act gives government powers to scrutinise, alter, and block transactions where there is a risk to national security In a move akin to calling the fire brigade after your house has burned down, the UK government today announced the passage of a bill that would afford it powers to intervene in potentially hostile direct investment.... [...]
