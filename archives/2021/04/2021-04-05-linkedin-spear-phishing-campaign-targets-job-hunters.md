Title: LinkedIn Spear-Phishing Campaign Targets Job Hunters
Date: 2021-04-05T19:46:18+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Web Security
Slug: linkedin-spear-phishing-campaign-targets-job-hunters

[Source](https://threatpost.com/linkedin-spear-phishing-job-hunters/165240/){:target="_blank" rel="noopener"}

> Fake job offers lure professionals into downloading the more_eggs backdoor trojan. [...]
