Title: FBI nuked web shells from hacked Exchange Servers without telling owners
Date: 2021-04-13T20:57:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fbi-nuked-web-shells-from-hacked-exchange-servers-without-telling-owners

[Source](https://www.bleepingcomputer.com/news/security/fbi-nuked-web-shells-from-hacked-exchange-servers-without-telling-owners/){:target="_blank" rel="noopener"}

> A court-approved FBI operation was conducted to remove web shells from compromised US-based Microsoft Exchange servers without first notifying the servers' owners. [...]
