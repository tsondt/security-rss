Title: A Clubhouse bug let people lurk in rooms invisibly
Date: 2021-04-23T14:26:11+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Clubhouse;social media;startups;white hat
Slug: a-clubhouse-bug-let-people-lurk-in-rooms-invisibly

[Source](https://arstechnica.com/?p=1759717){:target="_blank" rel="noopener"}

> Enlarge (credit: Sam Whitney | Wired | Getty Images) “Basically, I'm going to keep talking to you, but I’m going to disappear," longtime security researcher Katie Moussouris told me in a private Clubhouse room in February. “We'll still be talking, but I'll be gone.” And then her avatar vanished. I was alone, or at least that's how it seemed. “That’s it," she said from the digital beyond. "That's the bug. I am a fucking ghost.” It's been more than a year since the audio social network Clubhouse debuted. In that time, its explosive growth has come with a panoply of [...]
