Title: Don’t panic! DEF CON warrant canary confusion blamed on ‘CMS mistake’
Date: 2021-04-21T14:45:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: dont-panic-def-con-warrant-canary-confusion-blamed-on-cms-mistake

[Source](https://portswigger.net/daily-swig/dont-panic-def-con-warrant-canary-confusion-blamed-on-cms-mistake){:target="_blank" rel="noopener"}

> It ain’t the feds, just gremlins in the system [...]
