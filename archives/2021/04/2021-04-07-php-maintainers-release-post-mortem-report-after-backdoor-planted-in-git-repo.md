Title: PHP maintainers release post-mortem report after backdoor planted in Git repo
Date: 2021-04-07T13:03:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: php-maintainers-release-post-mortem-report-after-backdoor-planted-in-git-repo

[Source](https://portswigger.net/daily-swig/php-maintainers-release-post-mortem-report-after-backdoor-planted-in-git-repo){:target="_blank" rel="noopener"}

> More details released about the incident, though the attacker remains unidentified [...]
