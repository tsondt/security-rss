Title: Millions of web surfers are being targeted by a single malvertising group
Date: 2021-04-19T20:08:14+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;android;iphone;malvertising;malware;scam
Slug: millions-of-web-surfers-are-being-targeted-by-a-single-malvertising-group

[Source](https://arstechnica.com/?p=1758113){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Hackers have compromised more than 120 ad servers over the past year in an ongoing campaign that displays malicious advertisements on tens of millions, if not hundreds of millions, of devices as they visit sites that, by all outward appearances, are benign. Malvertising is the practice of delivering ads to people as they visit trusted websites. The ads embed JavaScript that surreptitiously exploits software flaws or tries to trick visitors into installing an unsafe app, paying fraudulent computer support fees, or taking other harmful actions. Typically, the scammers behind this Internet scourge pose as buyers [...]
