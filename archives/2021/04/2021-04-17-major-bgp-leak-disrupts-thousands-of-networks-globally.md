Title: Major BGP leak disrupts thousands of networks globally
Date: 2021-04-17T03:33:23-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: major-bgp-leak-disrupts-thousands-of-networks-globally

[Source](https://www.bleepingcomputer.com/news/security/major-bgp-leak-disrupts-thousands-of-networks-globally/){:target="_blank" rel="noopener"}

> A large BGP routing leak that occurred last night disrupted the connectivity for thousands of major networks and websites around the world. Although the BGP routing leak occurred in Vodafone's autonomous network (AS55410) based in India, it has impacted U.S. companies, including Google, according to sources. [...]
