Title: India orders takedowns of social media posts it claims harm fight against raging COVID-19 outbreak
Date: 2021-04-26T05:34:59+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: india-orders-takedowns-of-social-media-posts-it-claims-harm-fight-against-raging-covid-19-outbreak

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/26/india_social_media_takedown_orders/){:target="_blank" rel="noopener"}

> Many banned posts were made by opposition politicians and appear to be criticism of the government As India battles a surging second wave of COVID-19 cases and severe shortages of medical supplies to fight it, the nation's government has told Facebook, Instagram and Twitter to remove social media posts it says may panic its populace with misinformation.... [...]
