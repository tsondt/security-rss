Title: Brown University hit by cyberattack, some systems still offline
Date: 2021-04-02T16:01:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: brown-university-hit-by-cyberattack-some-systems-still-offline

[Source](https://www.bleepingcomputer.com/news/security/brown-university-hit-by-cyberattack-some-systems-still-offline/){:target="_blank" rel="noopener"}

> Brown University, a private US research university, had to disable systems and cut connections to the data center after suffering a cyberattack on Tuesday. [...]
