Title: Ill-advised research on Linux kernel lands computer scientists in hot water
Date: 2021-04-22T15:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ill-advised-research-on-linux-kernel-lands-computer-scientists-in-hot-water

[Source](https://portswigger.net/daily-swig/ill-advised-research-on-linux-kernel-lands-computer-scientists-in-hot-water){:target="_blank" rel="noopener"}

> University of Minnesota banned from Linux kernel contributions in fallout over buggy commits experiment [...]
