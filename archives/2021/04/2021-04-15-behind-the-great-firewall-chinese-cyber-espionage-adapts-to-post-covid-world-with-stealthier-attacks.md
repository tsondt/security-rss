Title: Behind the Great Firewall: Chinese cyber-espionage adapts to post-Covid world with stealthier attacks
Date: 2021-04-15T11:19:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: behind-the-great-firewall-chinese-cyber-espionage-adapts-to-post-covid-world-with-stealthier-attacks

[Source](https://portswigger.net/daily-swig/behind-the-great-firewall-chinese-cyber-espionage-adapts-to-post-covid-world-with-stealthier-attacks){:target="_blank" rel="noopener"}

> Beijing adopting supply chain tactics and greater sharing of resources between spying groups, experts warn [...]
