Title: Ubiquiti confirms extortion attempt following security breach
Date: 2021-04-01T09:31:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ubiquiti-confirms-extortion-attempt-following-security-breach

[Source](https://www.bleepingcomputer.com/news/security/ubiquiti-confirms-extortion-attempt-following-security-breach/){:target="_blank" rel="noopener"}

> Networking device maker Ubiquiti has confirmed that it was the target of an extortion attempt following a January security breach, as revealed by a whistleblower earlier this week. [...]
