Title: Building a Fortress: 3 Key Strategies for Optimized IT Security
Date: 2021-04-01T16:22:38+00:00
Author: Chris Haas
Category: Threatpost
Tags: InfoSec Insider;Malware;Vulnerabilities;Web Security
Slug: building-a-fortress-3-key-strategies-for-optimized-it-security

[Source](https://threatpost.com/key-strategies-optimized-it-security/165157/){:target="_blank" rel="noopener"}

> Chris Hass, director of information security and research at Automox, discusses how to shore up cybersecurity defenses and what to prioritize. [...]
