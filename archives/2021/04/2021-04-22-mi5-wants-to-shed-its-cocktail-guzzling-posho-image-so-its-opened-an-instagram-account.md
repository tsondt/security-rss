Title: MI5 wants to shed its cocktail-guzzling posho image – so it's opened an Instagram account
Date: 2021-04-22T13:45:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: mi5-wants-to-shed-its-cocktail-guzzling-posho-image-so-its-opened-an-instagram-account

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/22/mi5_instagram_cocktail_quaffing_poshos/){:target="_blank" rel="noopener"}

> Lockdown's getting to everyone – even the social media monkeys British domestic spy agency MI5 wants to dispel the idea it is staffed by martini-quaffing layabouts who spend implausible amounts of time lounging around top-end bars and hotels. It has therefore opened an Instagram account.... [...]
