Title: Spy Operations Target Vietnam with Sophisticated RAT
Date: 2021-04-05T21:04:26+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: spy-operations-target-vietnam-with-sophisticated-rat

[Source](https://threatpost.com/spy-operations-vietnam-rat/165243/){:target="_blank" rel="noopener"}

> Researchers said the FoundCore malware represents a big step forward when it comes to evasion. [...]
