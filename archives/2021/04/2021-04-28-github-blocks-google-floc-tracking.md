Title: GitHub blocks Google FLoC tracking
Date: 2021-04-28T01:25:45-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Google
Slug: github-blocks-google-floc-tracking

[Source](https://www.bleepingcomputer.com/news/security/github-blocks-google-floc-tracking/){:target="_blank" rel="noopener"}

> GitHub has announced rolling out a mysterious HTTP header on all GitHub Pages sites to block Google FLoC tracking. [...]
