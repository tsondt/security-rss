Title: Facebook data leak: Australians urged to check and secure social media accounts
Date: 2021-04-05T08:18:06+00:00
Author: Mostafa Rachwani
Category: The Guardian
Tags: Facebook;Australia news;Social networking;Technology;Data and computer security;Social media
Slug: facebook-data-leak-australians-urged-to-check-and-secure-social-media-accounts

[Source](https://www.theguardian.com/technology/2021/apr/05/facebook-data-leak-2021-breach-check-australia-users){:target="_blank" rel="noopener"}

> Experts urge users to secure accounts and passwords after breach exposes personal details of more than 500 million people Australians are being urged to secure their social media accounts after the details of more than 500 million global Facebook users were found online in a massive data breach. The details published freely online included names, phone numbers, email addresses, account IDs and bios. Related: Australia’s move to tame Facebook and Google is just the start of a global battle | Michelle Meagher Continue reading... [...]
