Title: Eversource Energy data breach caused by unsecured cloud storage
Date: 2021-04-20T13:45:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: eversource-energy-data-breach-caused-by-unsecured-cloud-storage

[Source](https://www.bleepingcomputer.com/news/security/eversource-energy-data-breach-caused-by-unsecured-cloud-storage/){:target="_blank" rel="noopener"}

> Eversource, the largest power supplier in New England, has suffered a data breach after customers' personal information was exposed on an unsecured cloud server. [...]
