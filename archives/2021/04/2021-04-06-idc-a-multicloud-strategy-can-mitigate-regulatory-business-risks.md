Title: IDC: A multicloud strategy can mitigate regulatory, business risks
Date: 2021-04-06T16:00:00+00:00
Author: Nick Godfrey
Category: GCP Security
Tags: Anthos;Identity & Security;Google Cloud Platform;Research;Hybrid & Multi-Cloud
Slug: idc-a-multicloud-strategy-can-mitigate-regulatory-business-risks

[Source](https://cloud.google.com/blog/topics/hybrid-cloud/idc-whitepaper-assesses-multicloud-as-risk-mitigation-strategy/){:target="_blank" rel="noopener"}

> In recent years, cloud has become an important driver for innovation, enhancing security and operational resilience, and the COVID-19 pandemic has only accelerated the trend. But cloud adoption has been slower in regulated sectors including government agencies and financial institutions, due to the complexity of their legacy systems, cultural perceptions, and certain compliance challenges. In particular, the industry is focused on reducing vendor lock-in, concentration risk and over-reliance on third-party providers. To address these concerns, many regulated organizations are looking to implement innovative multi-vendor strategies. A multicloud approach lets them pick and choose cloud services from multiple cloud providers, helping [...]
