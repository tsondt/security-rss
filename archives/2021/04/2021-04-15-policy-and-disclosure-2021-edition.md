Title: Policy and Disclosure: 2021 Edition
Date: 2021-04-15T09:02:00-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: policy-and-disclosure-2021-edition

[Source](https://googleprojectzero.blogspot.com/2021/04/policy-and-disclosure-2021-edition.html){:target="_blank" rel="noopener"}

> Posted by Tim Willis, Project Zero At Project Zero, we spend a lot of time discussing and evaluating vulnerability disclosure policies and their consequences for users, vendors, fellow security researchers, and software security norms of the broader industry. We aim to be a vulnerability research team that benefits everyone, working across the entire ecosystem to help make 0-day hard. We remain committed to adapting our policies and practices to best achieve our mission, demonstrating this commitment at the beginning of last year with our 2020 Policy and Disclosure Trial. As part of our annual year-end review, we evaluated our policy [...]
