Title: United States' plan to beat China includes dominating tech standards groups, especially for 5G
Date: 2021-04-12T01:40:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: united-states-plan-to-beat-china-includes-dominating-tech-standards-groups-especially-for-5g

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/12/us_strategic_competition_act_tech_sections/){:target="_blank" rel="noopener"}

> 'Strategic Competition Act' calls for appointment of a new ambassador-at-large for tech America's plan to compete with China includes a call for the land of the free to dominate tech standards bodies, especially for 5G, and to appoint an ambassador level official to lead a new “Technology Partnership Office” that Washington will use to drive tech collaboration among like-minded nations.... [...]
