Title: Leading cosmetics group Pierre Fabre hit with $25 million ransomware attack
Date: 2021-04-09T14:52:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: leading-cosmetics-group-pierre-fabre-hit-with-25-million-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/leading-cosmetics-group-pierre-fabre-hit-with-25-million-ransomware-attack/){:target="_blank" rel="noopener"}

> Leading French pharmaceutical group Pierre Fabre suffered a REvil ransomware attack where the threat actors initially demanded a $25 million ransom, BleepingComputer learned today. [...]
