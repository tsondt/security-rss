Title: GCHQ boss warns China can rewrite 'the global operating system' in its own authoritarian image
Date: 2021-04-26T06:58:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: gchq-boss-warns-china-can-rewrite-the-global-operating-system-in-its-own-authoritarian-image

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/26/jeremy_fleming_gchq_china_warning/){:target="_blank" rel="noopener"}

> Brit spymaster calls for renewed attention to UK tech capabilities – and expanded role for his agency The director of the UK's signals intelligence agency has delivered a speech in which he contemplated power in the digital age, observing that "China's size and technological weight means that it has the potential to control the global operating system," and hinting at an expanded role for the agency he leads as one way to fight back.... [...]
