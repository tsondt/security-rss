Title: Security expert coalition shares actions to disrupt ransomware
Date: 2021-04-29T06:57:31-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: security-expert-coalition-shares-actions-to-disrupt-ransomware

[Source](https://www.bleepingcomputer.com/news/security/security-expert-coalition-shares-actions-to-disrupt-ransomware/){:target="_blank" rel="noopener"}

> The Ransomware Task Force, a public-party coalition of more than 50 experts, has shared a framework of actions to disrupt the ransomware business model. [...]
