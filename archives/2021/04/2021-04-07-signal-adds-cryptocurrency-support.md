Title: Signal Adds Cryptocurrency Support
Date: 2021-04-07T11:24:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cryptocurrency;encryption;privacy;Signal
Slug: signal-adds-cryptocurrency-support

[Source](https://www.schneier.com/blog/archives/2021/04/wtf-signal-adds-cryptocurrency-support.html){:target="_blank" rel="noopener"}

> According to Wired, Signal is adding support for the cryptocurrency MobileCoin, “a form of digital cash designed to work efficiently on mobile devices while protecting users’ privacy and even their anonymity.” Moxie Marlinspike, the creator of Signal and CEO of the nonprofit that runs it, describes the new payments feature as an attempt to extend Signal’s privacy protections to payments with the same seamless experience that Signal has offered for encrypted conversations. “There’s a palpable difference in the feeling of what it’s like to communicate over Signal, knowing you’re not being watched or listened to, versus other communication platforms,” Marlinspike [...]
