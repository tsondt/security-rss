Title: Mozilla drops Firefox support on Amazon Fire TV
Date: 2021-04-15T15:34:52-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: mozilla-drops-firefox-support-on-amazon-fire-tv

[Source](https://www.bleepingcomputer.com/news/security/mozilla-drops-firefox-support-on-amazon-fire-tv/){:target="_blank" rel="noopener"}

> This month, Amazon has announced plans to phase out support for the Mozilla Firefox web browser app on its Fire TV product line. The company has decided to no longer support the Mozilla Firefox browser in a bid to promote its Amazon Silk web browser app to customers, effective at the end of this month. [...]
