Title: Mount Locker Ransomware Aggressively Changes Up Tactics
Date: 2021-04-22T19:33:45+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware
Slug: mount-locker-ransomware-aggressively-changes-up-tactics

[Source](https://threatpost.com/mount-locker-ransomware-changes-tactics/165559/){:target="_blank" rel="noopener"}

> The ransomware is upping its danger quotient with new features while signaling a rebranding to "AstroLocker." [...]
