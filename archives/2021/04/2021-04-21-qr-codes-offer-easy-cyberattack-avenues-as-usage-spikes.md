Title: QR Codes Offer Easy Cyberattack Avenues as Usage Spikes
Date: 2021-04-21T19:39:45+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Most Recent ThreatLists;Web Security
Slug: qr-codes-offer-easy-cyberattack-avenues-as-usage-spikes

[Source](https://threatpost.com/qr-codes-cyberattack-usage-spikes/165526/){:target="_blank" rel="noopener"}

> Usage is way up, but so are cyberattacks: Mobile phishing, malware, banking heists and more can come from just one wrong scan. [...]
