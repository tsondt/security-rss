Title: Your stolen ParkMobile data is now free for wannabe scammers
Date: 2021-04-30T11:26:29-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: your-stolen-parkmobile-data-is-now-free-for-wannabe-scammers

[Source](https://www.bleepingcomputer.com/news/security/your-stolen-parkmobile-data-is-now-free-for-wannabe-scammers/){:target="_blank" rel="noopener"}

> The account information for almost 22 million ParkMobile customers is now in the hands of hackers and scammers after the data was released for free on a hacking forum. [...]
