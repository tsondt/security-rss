Title: Drinks giant C&amp;C Group subsidiary shuts down IT systems following security incident
Date: 2021-04-19T12:33:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: drinks-giant-cc-group-subsidiary-shuts-down-it-systems-following-security-incident

[Source](https://portswigger.net/daily-swig/drinks-giant-c-amp-c-group-subsidiary-shuts-down-it-systems-following-security-incident){:target="_blank" rel="noopener"}

> Staff at Matthew Clark Bibendum are ‘fulfilling orders manually’ while they restore systems [...]
