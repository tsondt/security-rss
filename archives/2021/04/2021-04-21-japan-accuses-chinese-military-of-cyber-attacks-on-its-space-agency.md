Title: Japan accuses Chinese military of cyber-attacks on its space agency
Date: 2021-04-21T03:30:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: japan-accuses-chinese-military-of-cyber-attacks-on-its-space-agency

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/21/japan_accuses_china_of_attacking_jaxa/){:target="_blank" rel="noopener"}

> 200 other companies also targeted, but no data lost Japan has accused a member of the Chinese Communist Party of conducting cyber-attacks on its space agency and 200 other local entities.... [...]
