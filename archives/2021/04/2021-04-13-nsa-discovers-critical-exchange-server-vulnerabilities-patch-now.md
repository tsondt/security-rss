Title: NSA discovers critical Exchange Server vulnerabilities, patch now
Date: 2021-04-13T15:15:50-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: nsa-discovers-critical-exchange-server-vulnerabilities-patch-now

[Source](https://www.bleepingcomputer.com/news/security/nsa-discovers-critical-exchange-server-vulnerabilities-patch-now/){:target="_blank" rel="noopener"}

> Microsoft today has released security updates for Exchange Server that address a set of four vulnerabilities with severity scores ranging from high to critical. [...]
