Title: Mozilla Fixes Firefox Flaw That Allowed Spoofing of HTTPS Browser Padlock
Date: 2021-04-20T20:40:09+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: mozilla-fixes-firefox-flaw-that-allowed-spoofing-of-https-browser-padlock

[Source](https://threatpost.com/mozilla-fixes-firefox-flaw/165501/){:target="_blank" rel="noopener"}

> The Mozilla Foundation releases Firefox 88, fixing 13 bugs ranging from high to low severity. [...]
