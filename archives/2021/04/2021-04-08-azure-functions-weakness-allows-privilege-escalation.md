Title: Azure Functions Weakness Allows Privilege Escalation
Date: 2021-04-08T14:12:46+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: azure-functions-weakness-allows-privilege-escalation

[Source](https://threatpost.com/azure-functions-privilege-escalation/165307/){:target="_blank" rel="noopener"}

> Microsoft's cloud-container technology allows attackers to directly write to files, researchers said. [...]
