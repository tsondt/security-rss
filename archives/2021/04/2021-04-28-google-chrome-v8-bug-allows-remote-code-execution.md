Title: Google Chrome V8 Bug Allows Remote Code-Execution
Date: 2021-04-28T17:48:16+00:00
Author: Tara Seals
Category: Threatpost
Tags: 
Slug: google-chrome-v8-bug-allows-remote-code-execution

[Source](https://threatpost.com/google-chrome-v8-bug-remote-code-execution/165662/){:target="_blank" rel="noopener"}

> The internet behemoth rolled out the Chrome 90 stable channel release to address this and eight other security vulnerabilities. [...]
