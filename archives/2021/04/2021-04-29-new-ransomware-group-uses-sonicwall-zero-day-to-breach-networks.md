Title: New ransomware group uses SonicWall zero-day to breach networks
Date: 2021-04-29T18:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: new-ransomware-group-uses-sonicwall-zero-day-to-breach-networks

[Source](https://www.bleepingcomputer.com/news/security/new-ransomware-group-uses-sonicwall-zero-day-to-breach-networks/){:target="_blank" rel="noopener"}

> A financially motivated threat actor exploited a zero-day bug in Sonicwall SMA 100 Series VPN appliances to deploy new ransomware known as FiveHands on the networks of North American and European targets. [...]
