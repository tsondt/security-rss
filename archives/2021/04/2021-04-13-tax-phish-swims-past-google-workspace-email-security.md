Title: Tax Phish Swims Past Google Workspace Email Security
Date: 2021-04-13T18:29:33+00:00
Author: Tara Seals
Category: Threatpost
Tags: Web Security
Slug: tax-phish-swims-past-google-workspace-email-security

[Source](https://threatpost.com/tax-phish-google-workspace-email-security/165376/){:target="_blank" rel="noopener"}

> Crooks are looking to harvest email credentials with a savvy campaign that uses the Typeform service to host the phishing page. [...]
