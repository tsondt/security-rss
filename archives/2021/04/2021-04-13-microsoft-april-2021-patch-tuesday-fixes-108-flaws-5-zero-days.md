Title: Microsoft April 2021 Patch Tuesday fixes 108 flaws, 5 zero-days
Date: 2021-04-13T13:39:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-april-2021-patch-tuesday-fixes-108-flaws-5-zero-days

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-april-2021-patch-tuesday-fixes-108-flaws-5-zero-days/){:target="_blank" rel="noopener"}

> Today is Microsoft's April 2021 Patch Tuesday, and with it comes five zero-day vulnerabilities and more Critical Microsoft Exchange vulnerabilities. It has been a tough couple of months for Windows and Microsoft Exchange admins, and it looks like April won't be any easier, so please be nice to your IT staff today. [...]
