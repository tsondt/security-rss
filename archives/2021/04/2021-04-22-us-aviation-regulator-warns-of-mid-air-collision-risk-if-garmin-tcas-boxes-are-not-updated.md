Title: US aviation regulator warns of mid-air collision risk if Garmin TCAS boxes are not updated
Date: 2021-04-22T19:30:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: us-aviation-regulator-warns-of-mid-air-collision-risk-if-garmin-tcas-boxes-are-not-updated

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/22/garmin_tcas_software_collision_risks_faa/){:target="_blank" rel="noopener"}

> Software fixes available, says FAA American aviation regulators have ordered private jet operators to install software updates for Garmin collision avoidance units after multiple reports of false alarms – raising the risk of a mid-air crash.... [...]
