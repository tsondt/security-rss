Title: Cracked copies of Microsoft Office and Adobe Photoshop steal your session cookies, browser history, crypto-coins
Date: 2021-04-13T17:12:45+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: cracked-copies-of-microsoft-office-and-adobe-photoshop-steal-your-session-cookies-browser-history-crypto-coins

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/13/cracked_copies_of_microsoft_office/){:target="_blank" rel="noopener"}

> It's like the 2000s all over again, sighs Bitdefender Cracked copies of Microsoft Office and Adobe Photoshop are stealing browser session cookies and Monero cryptocurrency wallets from tightwads who install the pirated software, Bitdefender has warned.... [...]
