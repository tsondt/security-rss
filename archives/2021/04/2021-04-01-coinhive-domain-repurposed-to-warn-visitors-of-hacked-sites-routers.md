Title: Coinhive domain repurposed to warn visitors of hacked sites, routers
Date: 2021-04-01T13:24:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: coinhive-domain-repurposed-to-warn-visitors-of-hacked-sites-routers

[Source](https://www.bleepingcomputer.com/news/security/coinhive-domain-repurposed-to-warn-visitors-of-hacked-sites-routers/){:target="_blank" rel="noopener"}

> After taking over the domains for the notorious Coinhive in-browsing Monero mining service, a researcher is now displaying alerts on hacked websites that are still injecting the mining service's JavaScript. [...]
