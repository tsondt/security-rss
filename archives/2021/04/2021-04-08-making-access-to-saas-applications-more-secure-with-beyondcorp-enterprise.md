Title: Making access to SaaS applications more secure with BeyondCorp Enterprise
Date: 2021-04-08T16:00:00+00:00
Author: Jian Zhen
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: making-access-to-saas-applications-more-secure-with-beyondcorp-enterprise

[Source](https://cloud.google.com/blog/products/identity-security/beyondcorp-enterprise-helps-zero-trust-organizations-stay-secure/){:target="_blank" rel="noopener"}

> An explosion of SaaS applications over the last decade has fundamentally changed the security landscape of modern enterprises. According to the Cloud Security Threat Report 1, the average organization uses hundreds, possibly upwards of 1,000 of SaaS applications, many of these unsanctioned by IT departments, and this number is only forecasted to increase. Today, we see many organizations trying to secure modern SaaS applications with a legacy, network-based approach, where access might only be given if a user is on the corporate network or connecting through a VPN. But conventional castle-and-moat security strategies are no longer adequate; the network from [...]
