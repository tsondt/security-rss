Title: Update on PHP source code compromise: User database leak suspected
Date: 2021-04-07T14:38:10+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: update-on-php-source-code-compromise-user-database-leak-suspected

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/07/update_on_php_source_code/){:target="_blank" rel="noopener"}

> Possible culprit: Ancient code running in production. A vuln 'would not be terribly surprising' says maintainer PHP maintainer Nikita Popov has posted an update concerning how the source code was compromised and malicious code inserted – blaming a user database leak rather than a problem with the server itself.... [...]
