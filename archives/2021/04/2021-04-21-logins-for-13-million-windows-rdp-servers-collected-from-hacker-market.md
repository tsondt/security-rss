Title: Logins for 1.3 million Windows RDP servers collected from hacker market
Date: 2021-04-21T11:15:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: logins-for-13-million-windows-rdp-servers-collected-from-hacker-market

[Source](https://www.bleepingcomputer.com/news/security/logins-for-13-million-windows-rdp-servers-collected-from-hacker-market/){:target="_blank" rel="noopener"}

> ​The login names and passwords for 1.3 million current and historically compromised Windows Remote Desktop servers have been leaked by UAS, the largest hacker marketplace for stolen RDP credentials. [...]
