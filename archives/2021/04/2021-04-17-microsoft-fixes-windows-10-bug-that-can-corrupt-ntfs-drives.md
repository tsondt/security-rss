Title: Microsoft fixes Windows 10 bug that can corrupt NTFS drives
Date: 2021-04-17T11:08:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: microsoft-fixes-windows-10-bug-that-can-corrupt-ntfs-drives

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-windows-10-bug-that-can-corrupt-ntfs-drives/){:target="_blank" rel="noopener"}

> Microsoft has fixed a bug that could allow a threat actor to create specially crafted downloads that crash Windows 10 simply by opening the folder where they are downloaded. [...]
