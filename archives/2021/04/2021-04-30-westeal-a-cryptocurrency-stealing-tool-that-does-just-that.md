Title: WeSteal: A Cryptocurrency-Stealing Tool That Does Just That
Date: 2021-04-30T19:01:05+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware
Slug: westeal-a-cryptocurrency-stealing-tool-that-does-just-that

[Source](https://threatpost.com/westeal-cryptocurrency-stealing-tool/165762/){:target="_blank" rel="noopener"}

> The developer of the WeSteal cryptocurrency stealer can’t be bothered with fancy talk: they say flat-out that it’s “the leading way to make money in 2021”. [...]
