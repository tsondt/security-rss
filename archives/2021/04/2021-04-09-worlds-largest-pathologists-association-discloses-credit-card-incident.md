Title: World's largest pathologists association discloses credit card incident
Date: 2021-04-09T12:31:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: worlds-largest-pathologists-association-discloses-credit-card-incident

[Source](https://www.bleepingcomputer.com/news/security/worlds-largest-pathologists-association-discloses-credit-card-incident/){:target="_blank" rel="noopener"}

> The American Society for Clinical Pathology (ASCP) disclosed a payment card incident that impacted customers who entered payment info on its e-commerce website. [...]
