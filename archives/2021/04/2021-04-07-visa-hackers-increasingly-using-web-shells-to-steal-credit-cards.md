Title: VISA: Hackers increasingly using web shells to steal credit cards
Date: 2021-04-07T17:18:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: visa-hackers-increasingly-using-web-shells-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/visa-hackers-increasingly-using-web-shells-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> Global payments processor VISA warns that threat actors are increasingly deploying web shells on compromised servers to exfiltrate credit card information stolen from online store customers. [...]
