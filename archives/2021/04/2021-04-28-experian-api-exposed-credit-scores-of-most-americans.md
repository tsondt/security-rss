Title: Experian API Exposed Credit Scores of Most Americans
Date: 2021-04-28T20:47:02+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Bill Demirkapi;credit score lookup tool;Experian;Geico;Rochester Institute of Technology
Slug: experian-api-exposed-credit-scores-of-most-americans

[Source](https://krebsonsecurity.com/2021/04/experian-api-exposed-credit-scores-of-most-americans/){:target="_blank" rel="noopener"}

> Big-three consumer credit bureau Experian just fixed a weakness with a partner website that let anyone look up the credit score of tens of millions of Americans just by supplying their name and mailing address, KrebsOnSecurity has learned. Experian says it has plugged the data leak, but the researcher who reported the finding says he fears the same weakness may be present at countless other lending websites that work with the credit bureau. Bill Demirkapi, an independent security researcher who’s currently a sophomore at the Rochester Institute of Technology, said he discovered the data exposure while shopping around for student [...]
