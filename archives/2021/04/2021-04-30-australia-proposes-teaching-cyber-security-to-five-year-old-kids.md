Title: Australia proposes teaching cyber-security to five-year-old kids
Date: 2021-04-30T02:33:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: australia-proposes-teaching-cyber-security-to-five-year-old-kids

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/30/eaching_cybersecurity_to_five_year_olds/){:target="_blank" rel="noopener"}

> By eight they should be telling you not to upload geo-tagged photos of them in school uniform Australia has decided that six-year-old children need education on cyber-security, even as it removes other material from the national curriculum.... [...]
