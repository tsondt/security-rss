Title: Security Vulnerabilities in Cellebrite
Date: 2021-04-27T11:57:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: intelligence;police;Signal;smartphones;vulnerabilities
Slug: security-vulnerabilities-in-cellebrite

[Source](https://www.schneier.com/blog/archives/2021/04/security-vulnerabilities-in-cellebrite.html){:target="_blank" rel="noopener"}

> Moxie Marlinspike has an intriguing blog post about Cellebrite, a tool used by police and others to break into smartphones. Moxie got his hands on one of the devices, which seems to be a pair of Windows software packages and a whole lot of connecting cables. According to Moxie, the software is riddled with vulnerabilities. (The one example he gives is that it uses FFmpeg DLLs from 2012, and have not been patched with the 100+ security updates since then.)...we found that it’s possible to execute arbitrary code on a Cellebrite machine simply by including a specially formatted but otherwise [...]
