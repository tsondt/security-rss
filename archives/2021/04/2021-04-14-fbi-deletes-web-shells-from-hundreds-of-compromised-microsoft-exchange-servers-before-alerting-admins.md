Title: FBI deletes web shells from hundreds of compromised Microsoft Exchange servers before alerting admins
Date: 2021-04-14T02:26:52+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: fbi-deletes-web-shells-from-hundreds-of-compromised-microsoft-exchange-servers-before-alerting-admins

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/14/fbi_exchange_server_malware_deletion/){:target="_blank" rel="noopener"}

> Remote-control malware wiped, deployments must still be patched The FBI deleted web shells installed by criminals on hundreds of Microsoft Exchange servers across the United States, it was revealed on Tuesday.... [...]
