Title: India uses controversial Aadhaar facial biometrics to identify COVID vaccination recipients
Date: 2021-04-09T05:01:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: india-uses-controversial-aadhaar-facial-biometrics-to-identify-covid-vaccination-recipients

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/09/india_facial_id_covid_vaccinations/){:target="_blank" rel="noopener"}

> Safer than eyeballs or fingerprints, apparently India’s National Health Authority has commenced a pilot of facial recognition software as a means of identifying people as they queue in the nation's COVID-19 vaccine centres.... [...]
