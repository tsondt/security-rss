Title: GitHub Actions being actively abused to mine cryptocurrency on GitHub servers
Date: 2021-04-03T05:49:56-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: github-actions-being-actively-abused-to-mine-cryptocurrency-on-github-servers

[Source](https://www.bleepingcomputer.com/news/security/github-actions-being-actively-abused-to-mine-cryptocurrency-on-github-servers/){:target="_blank" rel="noopener"}

> GitHub Actions has been abused by attackers to mine cryptocurrency using GitHub's servers, automatically.The particular attack adds malicious GitHub Actions code to repositories forked from legitimate ones, and further creates a Pull Request for the original repository maintainers to merge the code back, to alter the original code. [...]
