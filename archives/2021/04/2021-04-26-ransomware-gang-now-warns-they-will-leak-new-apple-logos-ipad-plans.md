Title: Ransomware gang now warns they will leak new Apple logos, iPad plans
Date: 2021-04-26T15:48:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-now-warns-they-will-leak-new-apple-logos-ipad-plans

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-now-warns-they-will-leak-new-apple-logos-ipad-plans/){:target="_blank" rel="noopener"}

> The REvil ransomware gang has mysteriously removed Apple's schematics from their data leak site after privately warning Quanta that they would leak drawings for the new iPad and new Apple logos. [...]
