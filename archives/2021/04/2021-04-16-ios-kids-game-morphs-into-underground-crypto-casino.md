Title: iOS Kids Game Morphs into Underground Crypto Casino
Date: 2021-04-16T19:19:55+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Mobile Security
Slug: ios-kids-game-morphs-into-underground-crypto-casino

[Source](https://threatpost.com/ios-kids-game-crypto-casino/165450/){:target="_blank" rel="noopener"}

> A malicious ‘Jungle Run’ app tricked security protections to make it into the Apple App Store, scamming users out of money with a casino-like functionality. [...]
