Title: NSA: 5 Security Bugs Under Active Nation-State Cyberattack
Date: 2021-04-16T18:10:09+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware;Vulnerabilities
Slug: nsa-5-security-bugs-under-active-nation-state-cyberattack

[Source](https://threatpost.com/nsa-security-bugs-active-nation-state-cyberattack/165446/){:target="_blank" rel="noopener"}

> Widely deployed platforms from Citrix, Fortinet, Pulse Secure, Synacor and VMware are all in the crosshairs of APT29, bent on stealing credentials and more. [...]
