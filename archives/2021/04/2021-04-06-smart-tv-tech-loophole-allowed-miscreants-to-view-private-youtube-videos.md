Title: Smart TV tech loophole allowed miscreants to view private YouTube videos
Date: 2021-04-06T15:18:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: smart-tv-tech-loophole-allowed-miscreants-to-view-private-youtube-videos

[Source](https://portswigger.net/daily-swig/smart-tv-tech-loophole-allowed-miscreants-to-view-private-youtube-videos){:target="_blank" rel="noopener"}

> Security researcher earns $6,000 bug bounty for thinking outside of the box [...]
