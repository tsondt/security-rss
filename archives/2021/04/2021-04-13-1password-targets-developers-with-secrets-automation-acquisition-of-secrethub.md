Title: 1Password targets developers with Secrets Automation, acquisition of SecretHub
Date: 2021-04-13T20:53:08+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: 1password-targets-developers-with-secrets-automation-acquisition-of-secrethub

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/13/1password_secrethub_automation/){:target="_blank" rel="noopener"}

> Existing users covered until 2022 Password specialist 1Password has acquired SecretHub, a secrets management platform aimed at IT engineers, and made a new service called Secrets Automation, previously in beta, generally available.... [...]
