Title: Their 'next job could be in cyber': UK Cyber Security Council launches itself by pointing world+dog to domain it doesn't own
Date: 2021-04-06T13:50:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: their-next-job-could-be-in-cyber-uk-cyber-security-council-launches-itself-by-pointing-worlddog-to-domain-it-doesnt-own

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/06/uk_cybersecurity_council_domain_fail_launch/){:target="_blank" rel="noopener"}

> Shouting cyber cyber cyber, mega mega fail thing The UK Cyber Security Council announced itself to the public realm last week by touting a domain it doesn't own. Helpfully, internet jokesters then bought up variations on the official address.... [...]
