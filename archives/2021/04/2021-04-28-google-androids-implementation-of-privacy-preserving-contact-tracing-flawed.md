Title: Google Android’s implementation of privacy-preserving contact tracing ‘flawed’
Date: 2021-04-28T16:00:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-androids-implementation-of-privacy-preserving-contact-tracing-flawed

[Source](https://portswigger.net/daily-swig/google-androids-implementation-of-privacy-preserving-contact-tracing-flawed){:target="_blank" rel="noopener"}

> Tech giant downplays location and Covid-states leakage concerns [...]
