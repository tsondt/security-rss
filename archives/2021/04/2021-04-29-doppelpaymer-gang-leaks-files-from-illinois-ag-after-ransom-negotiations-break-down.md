Title: DoppelPaymer Gang Leaks Files from Illinois AG After Ransom Negotiations Break Down
Date: 2021-04-29T11:51:17+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: doppelpaymer-gang-leaks-files-from-illinois-ag-after-ransom-negotiations-break-down

[Source](https://threatpost.com/doppelpaymer-leaks-illinois-ag/165694/){:target="_blank" rel="noopener"}

> Information stolen in April 10 ransomware attack was posted on a dark web portal and includes private documents not published as part of public records. [...]
