Title: Reverb discloses data breach exposing musicians' personal info
Date: 2021-04-26T17:10:48-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: reverb-discloses-data-breach-exposing-musicians-personal-info

[Source](https://www.bleepingcomputer.com/news/security/reverb-discloses-data-breach-exposing-musicians-personal-info/){:target="_blank" rel="noopener"}

> Popular musical instrument marketplace Reverb has suffered a data breach after an unsecured database containing customer information was exposed online. [...]
