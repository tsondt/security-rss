Title: Anti-Vaxxer Hijacks QR Codes at COVID-19 Check-In Sites
Date: 2021-04-29T13:58:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: anti-vaxxer-hijacks-qr-codes-at-covid-19-check-in-sites

[Source](https://threatpost.com/anti-vaxxer-hijacks-qr-codes-covid19/165701/){:target="_blank" rel="noopener"}

> The perp faces jail time, but the incident highlights the growing cyber-abuse of QR codes. [...]
