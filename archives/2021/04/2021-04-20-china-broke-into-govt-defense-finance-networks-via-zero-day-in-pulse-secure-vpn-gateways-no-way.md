Title: China broke into govt, defense, finance networks via zero-day in Pulse Secure VPN gateways? No way
Date: 2021-04-20T22:20:53+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: china-broke-into-govt-defense-finance-networks-via-zero-day-in-pulse-secure-vpn-gateways-no-way

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/20/china_pulse_connect_secure_vpn/){:target="_blank" rel="noopener"}

> Crucial flaw won't be fixed until next month Dozens of defense companies, government agencies, and financial organizations in America and abroad appear to have been compromised by China via vulnerabilities in their Pulse Connect Secure VPN appliances – including a zero-day flaw that won't be patched until next month.... [...]
