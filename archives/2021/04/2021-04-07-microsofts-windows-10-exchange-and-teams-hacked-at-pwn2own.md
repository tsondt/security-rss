Title: Microsoft's Windows 10, Exchange, and Teams hacked at Pwn2Own
Date: 2021-04-07T09:51:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsofts-windows-10-exchange-and-teams-hacked-at-pwn2own

[Source](https://www.bleepingcomputer.com/news/security/microsofts-windows-10-exchange-and-teams-hacked-at-pwn2own/){:target="_blank" rel="noopener"}

> During the first day of Pwn2Own 2021, contestants won $440,000 after successfully exploiting previously unknown vulnerabilities to hack Microsoft's Windows 10 OS, the Exchange mail server, and the Teams communication platform. [...]
