Title: Watchdog thinks Google tricked Australians into giving up data, sues. Judge semi-agrees
Date: 2021-04-16T11:30:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: watchdog-thinks-google-tricked-australians-into-giving-up-data-sues-judge-semi-agrees

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/16/watchdog_thinks_google_tricked_australians/){:target="_blank" rel="noopener"}

> Google employees called the meeting to discuss AP's data privacy reveal the 'Oh Shit' meeting Australian federal court sent a message to Big Tech about its willingness to act on privacy violations when it ruled today that Google had "partially" misled consumers about collecting mobile phone personal location data.... [...]
