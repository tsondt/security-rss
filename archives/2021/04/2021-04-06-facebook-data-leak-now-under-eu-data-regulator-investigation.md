Title: Facebook data leak now under EU data regulator investigation
Date: 2021-04-06T10:22:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: facebook-data-leak-now-under-eu-data-regulator-investigation

[Source](https://www.bleepingcomputer.com/news/security/facebook-data-leak-now-under-eu-data-regulator-investigation/){:target="_blank" rel="noopener"}

> Ireland's Data Protection Commission (DPC) is investigating a massive data leak concerning a database containing personal information belonging to more than 530 million Facebook users. [...]
