Title: HashiCorp reveals exposure of private code-signing key after Codecov compromise
Date: 2021-04-26T19:35:08+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: hashicorp-reveals-exposure-of-private-code-signing-key-after-codecov-compromise

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/26/hashicorp_reveals_exposure_of_private/){:target="_blank" rel="noopener"}

> Among the first of many? Software tools biz reports internal use of credential-stealing script HashiCorp, an open-source company whose Terraform product is widely used for automated cloud deployments, has revealed a private code-signing key was exposed thanks to the compromised Codecov script discovered earlier this month.... [...]
