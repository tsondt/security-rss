Title: IcedID Banking Trojan Surges: The New Emotet?
Date: 2021-04-08T20:00:17+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: icedid-banking-trojan-surges-the-new-emotet

[Source](https://threatpost.com/icedid-banking-trojan-surges-emotet/165314/){:target="_blank" rel="noopener"}

> A widespread email campaign using malicious Microsoft Excel attachments and Excel 4 macros is delivering IcedID at high volumes, suggesting it's filling the Emotet void. [...]
