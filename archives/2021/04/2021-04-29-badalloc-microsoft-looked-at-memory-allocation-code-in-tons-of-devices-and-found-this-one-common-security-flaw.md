Title: BadAlloc: Microsoft looked at memory allocation code in tons of devices and found this one common security flaw
Date: 2021-04-29T22:03:52+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: badalloc-microsoft-looked-at-memory-allocation-code-in-tons-of-devices-and-found-this-one-common-security-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/29/microsoft_badalloc_iot/){:target="_blank" rel="noopener"}

> Integer overflows leave IoT, OT, medical gear vulnerable to heap-seeking missiles Microsoft has taken a look at memory management code used in a wide range of equipment, from industrial control systems to healthcare gear, and found it can be potentially exploited to hijack devices.... [...]
