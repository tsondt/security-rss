Title: Windows and Linux devices are under attack by a new cryptomining worm
Date: 2021-04-09T11:59:59+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;cryptocurrency;cryptomining;exploits;malware;vulnerabilities
Slug: windows-and-linux-devices-are-under-attack-by-a-new-cryptomining-worm

[Source](https://arstechnica.com/?p=1755573){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A newly discovered cryptomining worm is stepping up its targeting of Windows and Linux devices with a batch of new exploits and capabilities, a researcher said. Research company Juniper started monitoring what it’s calling the Sysrv botnet in December. One of the botnet’s malware components was a worm that spread from one vulnerable device to another without requiring any user action. It did this by scanning the Internet for vulnerable devices and, when found, infecting them using a list of exploits that has increased over time. The malware also included a cryptominer that uses infected devices [...]
