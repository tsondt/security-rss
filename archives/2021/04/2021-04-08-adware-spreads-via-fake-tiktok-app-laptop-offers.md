Title: Adware Spreads via Fake TikTok App, Laptop Offers
Date: 2021-04-08T21:17:02+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: adware-spreads-via-fake-tiktok-app-laptop-offers

[Source](https://threatpost.com/adware-tiktok-laptop-offers/165318/){:target="_blank" rel="noopener"}

> Cybercriminals are encouraging users to send the "offers" via WhatsApp to their friends as well. [...]
