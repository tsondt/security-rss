Title: Google Chrome blocks port 10080 to stop NAT Slipstreaming attacks
Date: 2021-04-08T16:50:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Software
Slug: google-chrome-blocks-port-10080-to-stop-nat-slipstreaming-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-blocks-port-10080-to-stop-nat-slipstreaming-attacks/){:target="_blank" rel="noopener"}

> Google Chrome is now blocking HTTP, HTTPS, and FTP access to TCP port 10080 to prevent the ports from being abused in NAT Slipstreaming 2.0 attacks. [...]
