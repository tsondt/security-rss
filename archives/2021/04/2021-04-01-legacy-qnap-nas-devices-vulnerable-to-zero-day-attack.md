Title: Legacy QNAP NAS Devices Vulnerable to Zero-Day Attack
Date: 2021-04-01T19:53:04+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cloud Security;Hacks;Vulnerabilities
Slug: legacy-qnap-nas-devices-vulnerable-to-zero-day-attack

[Source](https://threatpost.com/qnap-nas-devices-zero-day-attack/165165/){:target="_blank" rel="noopener"}

> Some legacy models of QNAP network attached storage devices are vulnerable to remote unauthenticated attacks because of two unpatched vulnerabilities. [...]
