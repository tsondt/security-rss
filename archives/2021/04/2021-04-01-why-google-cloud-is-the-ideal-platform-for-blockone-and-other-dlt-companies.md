Title: Why Google Cloud is the ideal platform for Block.one and other DLT companies
Date: 2021-04-01T16:00:00+00:00
Author: Allen Day
Category: GCP Security
Tags: Google Cloud Platform;Data Analytics;Identity & Security;AI & Machine Learning;Inside Google Cloud
Slug: why-google-cloud-is-the-ideal-platform-for-blockone-and-other-dlt-companies

[Source](https://cloud.google.com/blog/topics/inside-google-cloud/why-google-cloud-is-the-ideal-platform-for-blockone-and-other-dlt-companies/){:target="_blank" rel="noopener"}

> Late last year, Google Cloud joined the EOS community, a leading open-source platform for blockchain innovation and performance, and is taking steps to support the EOS Public Blockchain by becoming a block producer (BP). At the time, we outlined how our planned participation underscores the importance of blockchain to the future of business, government, and society. Today, I want to outline why Google Cloud is uniquely positioned to be an excellent partner for Block.one and other distributed ledger technology (DLT) companies. We've recently seen an unprecedented rate of digital transformation across all industries, as a huge proportion of the economy [...]
