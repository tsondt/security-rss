Title: GitHub Arctic Vault likely contains leaked MedData patient records
Date: 2021-04-02T04:26:23-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: github-arctic-vault-likely-contains-leaked-meddata-patient-records

[Source](https://www.bleepingcomputer.com/news/security/github-arctic-vault-likely-contains-leaked-meddata-patient-records/){:target="_blank" rel="noopener"}

> GitHub Arctic Code Vault has likely inadvertently captured sensitive patient medical records from multiple healthcare facilities. The private data was leaked on GitHub repositories last year that are now part of a collection of open-source contributions bound to last a 1,000 years. [...]
