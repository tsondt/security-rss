Title: Pulse Secure VPN users can't login due to expired certificate
Date: 2021-04-12T11:05:57-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software;Technology
Slug: pulse-secure-vpn-users-cant-login-due-to-expired-certificate

[Source](https://www.bleepingcomputer.com/news/security/pulse-secure-vpn-users-cant-login-due-to-expired-certificate/){:target="_blank" rel="noopener"}

> Users worldwide cannot connect to Pulse Secure VPN devices after a code signing certificate used to digitally sign and verify software components has expired. [...]
