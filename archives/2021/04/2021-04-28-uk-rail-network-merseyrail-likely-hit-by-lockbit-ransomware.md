Title: UK rail network Merseyrail likely hit by Lockbit ransomware
Date: 2021-04-28T04:15:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: uk-rail-network-merseyrail-likely-hit-by-lockbit-ransomware

[Source](https://www.bleepingcomputer.com/news/security/uk-rail-network-merseyrail-likely-hit-by-lockbit-ransomware/){:target="_blank" rel="noopener"}

> UK rail network Merseyrail has confirmed a cyberattack after a ransomware gang used their email system to email employees and journalists about the attack. [...]
