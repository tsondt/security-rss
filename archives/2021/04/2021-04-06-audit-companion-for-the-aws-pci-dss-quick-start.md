Title: Audit companion for the AWS PCI DSS Quick Start
Date: 2021-04-06T18:45:12+00:00
Author: Avik Mukherjee
Category: AWS Security
Tags: AWS Professional Services;Compliance;Intermediate (200);Security, Identity, & Compliance;Auditing;AWS SOC2;PCI DSS;QSA;Security Blog;SOC1
Slug: audit-companion-for-the-aws-pci-dss-quick-start

[Source](https://aws.amazon.com/blogs/security/audit-companion-for-the-aws-pci-dss-quick-start/){:target="_blank" rel="noopener"}

> If you’ve supported a Payment Card Industry Data Security Standard (PCI DSS) assessment as a Qualified Security Assessor (QSA) or as a technical team facing an assessment, it’s likely that you spent a lot of time collecting and analyzing evidence against PCI DSS requirements. In this blog post, I show you how to use automation [...]
