Title: DeepDotWeb operator pleads guilty for role in $8.4m darknet marketplace kickback scheme
Date: 2021-04-01T12:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: deepdotweb-operator-pleads-guilty-for-role-in-84m-darknet-marketplace-kickback-scheme

[Source](https://portswigger.net/daily-swig/deepdotweb-operator-pleads-guilty-for-role-in-8-4m-darknet-marketplace-kickback-scheme){:target="_blank" rel="noopener"}

> Internet portal directed users to dark web sites selling malware, hacking tools, firearms, and drugs [...]
