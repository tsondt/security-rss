Title: Google’s Project Zero Finds a Nation-State Zero-Day Operation
Date: 2021-04-08T11:06:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberattack;Google;terrorism;zero-day
Slug: googles-project-zero-finds-a-nation-state-zero-day-operation

[Source](https://www.schneier.com/blog/archives/2021/04/googles-project-zero-finds-a-nation-state-zero-day-operation.html){:target="_blank" rel="noopener"}

> Google’s Project Zero discovered, and caused to be patched, eleven zero-day exploits against Chrome, Safari, Microsoft Windows, and iOS. This seems to have been exploited by “Western government operatives actively conducting a counterterrorism operation”: The exploits, which went back to early 2020 and used never-before-seen techniques, were “watering hole” attacks that used infected websites to deliver malware to visitors. They caught the attention of cybersecurity experts thanks to their scale, sophistication, and speed. [...] It’s true that Project Zero does not formally attribute hacking to specific groups. But the Threat Analysis Group, which also worked on the project, does perform [...]
