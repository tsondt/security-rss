Title: Microsoft releases a cyberattack simulator - Shall we play a game?
Date: 2021-04-08T12:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-releases-a-cyberattack-simulator-shall-we-play-a-game

[Source](https://www.bleepingcomputer.com/news/security/microsoft-releases-a-cyberattack-simulator-shall-we-play-a-game/){:target="_blank" rel="noopener"}

> Microsoft has released an open-source cyberattack simulator that allows security researchers and data scientists to create simulated network environments and see how they fare against AI-controlled cyber agents. [...]
