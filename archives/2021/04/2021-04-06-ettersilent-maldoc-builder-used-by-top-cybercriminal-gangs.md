Title: EtterSilent maldoc builder used by top cybercriminal gangs
Date: 2021-04-06T07:29:04-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: ettersilent-maldoc-builder-used-by-top-cybercriminal-gangs

[Source](https://www.bleepingcomputer.com/news/security/ettersilent-maldoc-builder-used-by-top-cybercriminal-gangs/){:target="_blank" rel="noopener"}

> A malicious document builder named EtterSilent is gaining more attention on underground forums, security researchers note. As its popularity increased, the developer kept improving it to avoid detection from security solutions. [...]
