Title: Asian buyers set for security spending spree to catch up on shabby strategies
Date: 2021-04-22T04:18:38+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: asian-buyers-set-for-security-spending-spree-to-catch-up-on-shabby-strategies

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/22/idc_semiannual_security_spending_guide_apac_2021/){:target="_blank" rel="noopener"}

> China already growing even faster than 13% regional acceleration Asian businesses are set for a security spending spree, according to analyst firm IDC.... [...]
