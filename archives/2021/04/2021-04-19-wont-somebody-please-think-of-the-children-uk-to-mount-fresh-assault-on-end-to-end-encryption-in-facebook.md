Title: Won't somebody please think of the children!!! UK to mount fresh assault on end-to-end encryption in Facebook
Date: 2021-04-19T18:45:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: wont-somebody-please-think-of-the-children-uk-to-mount-fresh-assault-on-end-to-end-encryption-in-facebook

[Source](https://go.theregister.com/feed/www.theregister.com/2021/04/19/uk_anti_encryption/){:target="_blank" rel="noopener"}

> Change the record, nobody's fooled by this now UK Home Secretary Priti Patel will badmouth Facebook's use of end-to-end encryption on Monday evening as she links the security technology with paedophilia, terrorism, organised crime, and so on.... [...]
