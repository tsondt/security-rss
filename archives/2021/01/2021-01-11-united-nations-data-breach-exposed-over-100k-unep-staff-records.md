Title: United Nations data breach exposed over 100k UNEP staff records
Date: 2021-01-11T01:52:09
Author: Ax Sharma
Category: BleepingComputer
Tags: Government
Slug: united-nations-data-breach-exposed-over-100k-unep-staff-records

[Source](https://www.bleepingcomputer.com/news/security/united-nations-data-breach-exposed-over-100k-unep-staff-records/){:target="_blank" rel="noopener"}

> This week, researchers have responsibly disclosed a vulnerability by exploiting which they could access over 100K private records of United Nations Environmental Programme (UNEP). The data breach stemmed from exposed Git directories which let researchers clone Git repositories and gather PII of a large number of employees. [...]
