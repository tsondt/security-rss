Title: Utah Ponders Making Online ‘Catfishing’ a Crime
Date: 2021-01-28T18:01:24+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government
Slug: utah-ponders-making-online-catfishing-a-crime

[Source](https://threatpost.com/utah-ponders-making-online-catfishing-a-crime/163456/){:target="_blank" rel="noopener"}

> Pretending to be someone else online could become a criminal offense, setting a precedent for other states to follow. [...]
