Title: Coming in at number 5, it's a blast from the past! Tenable's 2020 security flaw chart show features hits of yesteryear
Date: 2021-01-14T18:37:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: coming-in-at-number-5-its-a-blast-from-the-past-tenables-2020-security-flaw-chart-show-features-hits-of-yesteryear

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/14/tenable_threat_report_2020/){:target="_blank" rel="noopener"}

> You know that update thing? JFDI Out of the top five vulnerabilities for 2020 three dated back to 2019 or earlier, according to infosec firm Tenable's annual threat report.... [...]
