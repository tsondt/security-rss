Title: Bonobos clothing store suffers a data breach, hacker leaks 70GB database
Date: 2021-01-22T14:11:38
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: bonobos-clothing-store-suffers-a-data-breach-hacker-leaks-70gb-database

[Source](https://www.bleepingcomputer.com/news/security/bonobos-clothing-store-suffers-a-data-breach-hacker-leaks-70gb-database/){:target="_blank" rel="noopener"}

> Bonobos men's clothing store has suffered a massive data breach exposing millions of customers' personal information after a cloud backup was downloaded by a threat actor. The corporate systems were not breached by the attacker. [...]
