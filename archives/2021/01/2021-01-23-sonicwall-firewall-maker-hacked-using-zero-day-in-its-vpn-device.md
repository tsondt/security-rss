Title: SonicWall firewall maker hacked using zero-day in its VPN device
Date: 2021-01-23T12:14:25-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: sonicwall-firewall-maker-hacked-using-zero-day-in-its-vpn-device

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-firewall-maker-hacked-using-zero-day-in-its-vpn-device/){:target="_blank" rel="noopener"}

> Security hardware manufacturer SonicWall has issued an urgent security notice about threat actors exploiting a zero-day vulnerability in their VPN products to perform attacks on their internal systems. [...]
