Title: UK Cabinet Office spokesman tells House of Lords: We're not being complacent about impact of SolarWinds hack
Date: 2021-01-26T09:30:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-cabinet-office-spokesman-tells-house-of-lords-were-not-being-complacent-about-impact-of-solarwinds-hack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/26/solarwinds_uk_gov_house_lords_questions/){:target="_blank" rel="noopener"}

> Lib Dem blows raspberry at Sir Humphrey-style non-answer The British government has denied being "complacent" over the Solarwinds hack as a fed-up peer of the realm urged a minister to "answer the question".... [...]
