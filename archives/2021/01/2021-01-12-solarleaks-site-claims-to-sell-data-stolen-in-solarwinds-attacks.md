Title: SolarLeaks site claims to sell data stolen in SolarWinds attacks
Date: 2021-01-12T18:57:52
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: solarleaks-site-claims-to-sell-data-stolen-in-solarwinds-attacks

[Source](https://www.bleepingcomputer.com/news/security/solarleaks-site-claims-to-sell-data-stolen-in-solarwinds-attacks/){:target="_blank" rel="noopener"}

> A website named 'SolarLeaks' is selling data they claim was stolen from companies confirmed to have been breached in the SolarWinds attack. [...]
