Title: Mimecast Certificate Hacked in Microsoft Email Supply-Chain Attack
Date: 2021-01-12T18:35:41+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Web Security
Slug: mimecast-certificate-hacked-in-microsoft-email-supply-chain-attack

[Source](https://threatpost.com/mimecast-certificate-microsoft-supply-chain-attack/162965/){:target="_blank" rel="noopener"}

> A sophisticated threat actor has hijacked email security connections to spy on targets. [...]
