Title: FBI warns of vishing attacks stealing corporate accounts
Date: 2021-01-18T10:00:06
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-vishing-attacks-stealing-corporate-accounts

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-vishing-attacks-stealing-corporate-accounts/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) has issued a notification warning of ongoing vishing attacks attempting to steal corporate accounts and credentials for network access and privilege escalation from US and international-based employees. [...]
