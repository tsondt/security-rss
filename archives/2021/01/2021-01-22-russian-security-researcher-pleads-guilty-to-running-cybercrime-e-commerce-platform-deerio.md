Title: Russian ‘security researcher’ pleads guilty to running cybercrime e-commerce platform Deer.io
Date: 2021-01-22T15:38:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: russian-security-researcher-pleads-guilty-to-running-cybercrime-e-commerce-platform-deerio

[Source](https://portswigger.net/daily-swig/russian-security-researcher-pleads-guilty-to-running-cybercrime-e-commerce-platform-deer-io){:target="_blank" rel="noopener"}

> ‘One-stop shopping for criminals’ [...]
