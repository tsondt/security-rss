Title: Microsoft shares how SolarWinds hackers evaded detection
Date: 2021-01-20T15:54:57
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-shares-how-solarwinds-hackers-evaded-detection

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-how-solarwinds-hackers-evaded-detection/){:target="_blank" rel="noopener"}

> Microsoft today shared details on how the SolarWinds hackers were able to remain undetected by hiding their malicious activity inside the networks of breached companies. [...]
