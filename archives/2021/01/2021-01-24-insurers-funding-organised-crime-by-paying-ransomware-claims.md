Title: Insurers 'funding organised crime' by paying ransomware claims
Date: 2021-01-24T17:31:16+00:00
Author: Dan Sabbagh Defence and security editor
Category: The Guardian
Tags: Malware;Data and computer security;Cybercrime;Bitcoin;Cryptocurrencies;Organised crime;Insurance industry
Slug: insurers-funding-organised-crime-by-paying-ransomware-claims

[Source](https://www.theguardian.com/technology/2021/jan/24/insurers-funding-organised-by-paying-ransomware-claims){:target="_blank" rel="noopener"}

> Exclusive: former cybersecurity chief calls for law change and warns situation is ‘close to getting out of control’ Insurers are inadvertently funding organised crime by paying out claims from companies who have paid ransoms to regain access to data and systems after a hacking attack, Britain’s former top cybersecurity official has warned. Ciaran Martin, who ran the National Cyber Security Centre until last August, said he feared that so-called ransomware was “close to getting out of control” and that there was a risk that NHS systems could be hit during the pandemic. Continue reading... [...]
