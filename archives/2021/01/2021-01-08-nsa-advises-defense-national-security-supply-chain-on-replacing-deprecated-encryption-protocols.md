Title: NSA advises defense, national security supply chain on replacing deprecated encryption protocols
Date: 2021-01-08T13:19:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nsa-advises-defense-national-security-supply-chain-on-replacing-deprecated-encryption-protocols

[Source](https://portswigger.net/daily-swig/nsa-advises-defense-national-security-supply-chain-on-replacing-deprecated-encryption-protocols){:target="_blank" rel="noopener"}

> But guidance should be heeded by all sysadmins, says agency [...]
