Title: Rob Joyce to Take Over as NSA Cybersecurity Director
Date: 2021-01-19T20:20:03+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: rob-joyce-to-take-over-as-nsa-cybersecurity-director

[Source](https://threatpost.com/rob-joyce-nsa-cybersecurity-director/163160/){:target="_blank" rel="noopener"}

> Joyce will replace Anne Neuberger, who is now deputy national security advisor for the incoming Biden administration. [...]
