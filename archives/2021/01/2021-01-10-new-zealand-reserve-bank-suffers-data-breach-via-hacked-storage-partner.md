Title: New Zealand Reserve Bank suffers data breach via hacked storage partner
Date: 2021-01-10T15:43:43
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: new-zealand-reserve-bank-suffers-data-breach-via-hacked-storage-partner

[Source](https://www.bleepingcomputer.com/news/security/new-zealand-reserve-bank-suffers-data-breach-via-hacked-storage-partner/){:target="_blank" rel="noopener"}

> The Reserve Bank of New Zealand, known as Te Pūtea Matua, has suffered a data breach after threat actors hacked a third-party hosting partner. [...]
