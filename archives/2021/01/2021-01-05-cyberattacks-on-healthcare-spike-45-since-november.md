Title: Cyberattacks on Healthcare Spike 45% Since November
Date: 2021-01-05T21:33:22+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Malware;Vulnerabilities;Web Security;BazarLoader;Botnets;CISA advisory;Cobalt Strike. DDoS;conti;COVID-19;health care;healthcare cybersecurity;Hospitals;ransomware;ransomware as a service;ryuk;Sodinokibi;Software as a Service;TrickBot
Slug: cyberattacks-on-healthcare-spike-45-since-november

[Source](https://threatpost.com/cyberattacks-healthcare-spike-ransomware/162770/){:target="_blank" rel="noopener"}

> The relentless rise in COVID-19 cases is battering already frayed healthcare systems — and ransomware criminals are using the opportunity to strike. [...]
