Title: Critical WordPress-Plugin Bug Found in ‘Orbit Fox’ Allows Site Takeover
Date: 2021-01-13T19:41:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security;Cross Site Scripting;Orbit Fox;plugin;privilege escalation;Security Vulnerabilities;takeover;website;wordpress
Slug: critical-wordpress-plugin-bug-found-in-orbit-fox-allows-site-takeover

[Source](https://threatpost.com/orbit-fox-wordpress-plugin-bugs/163020/){:target="_blank" rel="noopener"}

> Two security vulnerabilities -- one a privilege-escalation problem and the other a stored XSS bug -- afflict a WordPress plugin with 40,000 installs. [...]
