Title: ADT techie admits he peeked into women's home security cams thousands of times to watch them undress, have sex
Date: 2021-01-23T08:36:04+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: adt-techie-admits-he-peeked-into-womens-home-security-cams-thousands-of-times-to-watch-them-undress-have-sex

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/23/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: SonicWall hacked, Qualcomm security wobble, warrantless cellphone monitoring by US snoops revealed In brief One-time ADT security engineer Telesforo Aviles, 35, pleaded guilty to computer fraud in the US after spying on women through their home surveillance cameras.... [...]
