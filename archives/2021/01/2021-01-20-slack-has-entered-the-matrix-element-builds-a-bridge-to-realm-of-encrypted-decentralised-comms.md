Title: Slack has entered the Matrix: Element builds a bridge to realm of encrypted, decentralised comms
Date: 2021-01-20T12:29:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: slack-has-entered-the-matrix-element-builds-a-bridge-to-realm-of-encrypted-decentralised-comms

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/20/matrix_slack/){:target="_blank" rel="noopener"}

> Element Matrix Services adds to the messaging interoperability toolbox Element Matrix Services is adding a bridge between hipster chat platform Slack and the open-source world of Matrix messaging.... [...]
