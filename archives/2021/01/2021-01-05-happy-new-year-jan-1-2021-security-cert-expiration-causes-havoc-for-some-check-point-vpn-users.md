Title: Happy New Year: Jan 1, 2021 security cert expiration causes havoc for some Check Point VPN users
Date: 2021-01-05T00:54:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: happy-new-year-jan-1-2021-security-cert-expiration-causes-havoc-for-some-check-point-vpn-users

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/05/certificate_check_point/){:target="_blank" rel="noopener"}

> Outdated clients stop working, organizations with thousands of end-points told to switch out.sys files It wasn't the best of New Year's Day mornings for some Check Point customers; in addition to possible hangovers, those who lagged with their patching had been left with inoperable systems and a tough fix ahead for some.... [...]
