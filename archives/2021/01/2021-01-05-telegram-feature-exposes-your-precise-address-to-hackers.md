Title: Telegram feature exposes your precise address to hackers
Date: 2021-01-05T21:40:31+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;messengers;privacy;telegram
Slug: telegram-feature-exposes-your-precise-address-to-hackers

[Source](https://arstechnica.com/?p=1733117){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) If you’re using an Android device—or in some cases an iPhone—the Telegram messenger app makes it easy for hackers to find your precise location when you enable a feature that allows users who are geographically close to you to connect. The researcher who discovered the disclosure vulnerability and privately reported it to Telegram developers said they have no plans to fix it. The problem stems from a feature called People Nearby. By default, it’s turned off. When users enable it, their geographic distance is shown to other people who have it turned on and are [...]
