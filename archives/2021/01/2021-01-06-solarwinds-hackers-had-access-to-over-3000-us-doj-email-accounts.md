Title: SolarWinds hackers had access to over 3,000 US DOJ email accounts
Date: 2021-01-06T14:05:01
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: solarwinds-hackers-had-access-to-over-3000-us-doj-email-accounts

[Source](https://www.bleepingcomputer.com/news/security/solarwinds-hackers-had-access-to-over-3-000-us-doj-email-accounts/){:target="_blank" rel="noopener"}

> The US Department of Justice said that the attackers behind the SolarWinds supply chain attacks have gained access to roughly 3% of the department's Office 365 email inboxes. [...]
