Title: Incoming Biden administration looks to shake up US cybersecurity policy
Date: 2021-01-19T14:56:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: incoming-biden-administration-looks-to-shake-up-us-cybersecurity-policy

[Source](https://portswigger.net/daily-swig/incoming-biden-administration-looks-to-shake-up-us-cybersecurity-policy){:target="_blank" rel="noopener"}

> ‘I would like to see a pivot from cyber warfare back to risk mitigation and personal privacy’ [...]
