Title: Digital burglars break into the Australian Securities and Investments Commission
Date: 2021-01-25T18:01:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: digital-burglars-break-into-the-australian-securities-and-investments-commission

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/25/asic_accellion_breach/){:target="_blank" rel="noopener"}

> Miscreant fingered server that held docs related to credit applications down under The Australian Securities and Investments Commission (ASIC) has admitted one of its servers was accessed without sanction and may have been digitally pawed by miscreants.... [...]
