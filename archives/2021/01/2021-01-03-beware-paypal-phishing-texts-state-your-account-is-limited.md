Title: Beware: PayPal phishing texts state your account is 'limited'
Date: 2021-01-03T12:58:41
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: beware-paypal-phishing-texts-state-your-account-is-limited

[Source](https://www.bleepingcomputer.com/news/security/beware-paypal-phishing-texts-state-your-account-is-limited/){:target="_blank" rel="noopener"}

> A PayPal text message phishing campaign is underway that attempts to steal your account credentials and other sensitive information that can be used for identity theft. [...]
