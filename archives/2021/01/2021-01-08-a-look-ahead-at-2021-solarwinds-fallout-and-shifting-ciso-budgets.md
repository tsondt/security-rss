Title: A Look Ahead at 2021: SolarWinds Fallout and Shifting CISO Budgets
Date: 2021-01-08T20:44:59+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;Podcasts;Vulnerabilities;COVID-19;cyberattack;Healthcare;ransomware;solarwinds
Slug: a-look-ahead-at-2021-solarwinds-fallout-and-shifting-ciso-budgets

[Source](https://threatpost.com/2021-solarwinds-fallout-shifting-ciso-budgets/162897/){:target="_blank" rel="noopener"}

> Threatpost editors discuss the SolarWinds hack, healthcare ransomware attacks and other threats that will plague enterprises in 2021. [...]
