Title: CrackQ tool adds GUI, analysis features to Hashcat password-cracking platform
Date: 2021-01-11T14:39:48+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: crackq-tool-adds-gui-analysis-features-to-hashcat-password-cracking-platform

[Source](https://portswigger.net/daily-swig/crackq-tool-adds-gui-analysis-features-to-hashcat-password-cracking-platform){:target="_blank" rel="noopener"}

> Currently in alpha release, the tool will have a raft of new features added in the coming months [...]
