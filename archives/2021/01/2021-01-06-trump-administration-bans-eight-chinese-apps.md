Title: Trump administration bans eight Chinese apps
Date: 2021-01-06T04:57:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: trump-administration-bans-eight-chinese-apps

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/06/trump_administration_bans_chinese_apps/){:target="_blank" rel="noopener"}

> Alipay, WeChat and friends ‘threaten national security, foreign policy, and economy of the United States’ United States president Donald Trump has signed an executive order banning the use of eight Chinese apps, namely Alipay, CamScanner, QQ Wallet, SHAREit, Tencent QQ, VMate, WeChat Pay, and WPS Office.... [...]
