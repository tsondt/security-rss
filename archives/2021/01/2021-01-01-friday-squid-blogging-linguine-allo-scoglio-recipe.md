Title: Friday Squid Blogging: Linguine allo Scoglio Recipe
Date: 2021-01-01T22:00:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: friday-squid-blogging-linguine-allo-scoglio-recipe

[Source](https://www.schneier.com/blog/archives/2021/01/friday-squid-blogging-linguine-allo-scoglio-recipe.html){:target="_blank" rel="noopener"}

> Delicious seafood pasta dish — includes squid — from America’s Test Kitchen. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
