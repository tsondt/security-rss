Title: Ryuk Rakes in $150M in Ransom Payments
Date: 2021-01-08T20:19:54+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Bitcoin;malware;payments;ransomware;ryuk
Slug: ryuk-rakes-in-150m-in-ransom-payments

[Source](https://threatpost.com/ryuk-150m-ransom-payments/162905/){:target="_blank" rel="noopener"}

> An examination of the malware gang's payments reveals insights into its economic operations. [...]
