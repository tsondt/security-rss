Title: Oculus, Facebook account takeovers net security researcher $30,000 bug bounty
Date: 2021-01-05T15:28:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: oculus-facebook-account-takeovers-net-security-researcher-30000-bug-bounty

[Source](https://portswigger.net/daily-swig/oculus-facebook-account-takeovers-net-security-researcher-30-000-bug-bounty){:target="_blank" rel="noopener"}

> XSS in virtual reality forum one of three flaws chained to land bumper payout [...]
