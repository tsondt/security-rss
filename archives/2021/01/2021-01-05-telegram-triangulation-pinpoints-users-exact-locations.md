Title: Telegram Triangulation Pinpoints Users’ Exact Locations
Date: 2021-01-05T20:33:00+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Privacy;location;people nearby feature;precise location;telegram;triangulation
Slug: telegram-triangulation-pinpoints-users-exact-locations

[Source](https://threatpost.com/telegram-triangulation-users-locations/162762/){:target="_blank" rel="noopener"}

> The "People Nearby" feature in the secure messaging app can be abused to unmask a user's precise location, a researcher said. [...]
