Title: Interpol: Trading scammers lure love-struck victims via dating apps
Date: 2021-01-19T13:10:54
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: interpol-trading-scammers-lure-love-struck-victims-via-dating-apps

[Source](https://www.bleepingcomputer.com/news/security/interpol-trading-scammers-lure-love-struck-victims-via-dating-apps/){:target="_blank" rel="noopener"}

> The Interpol (International Criminal Police Organisation) warns of fraudsters targeting dating app users and attempting to trick them into investing through fake trading apps. [...]
