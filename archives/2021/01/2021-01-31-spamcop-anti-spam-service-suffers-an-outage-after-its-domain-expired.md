Title: SpamCop anti-spam service suffers an outage after its domain expired
Date: 2021-01-31T18:29:44-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: spamcop-anti-spam-service-suffers-an-outage-after-its-domain-expired

[Source](https://www.bleepingcomputer.com/news/security/spamcop-anti-spam-service-suffers-an-outage-after-its-domain-expired/){:target="_blank" rel="noopener"}

> Cisco's SpamCop anti-spam service suffered an outage Sunday after a its domain mistakenly was allowed to expire. [...]
