Title: Masking field values with Amazon Elasticsearch Service
Date: 2021-01-11T16:52:27+00:00
Author: Prashant Agrawal
Category: AWS Security
Tags: Amazon Elasticsearch Service;Intermediate (200);Security, Identity, & Compliance;Access Control;Amazon ES;Elasticsearch;Kibana;Security Blog
Slug: masking-field-values-with-amazon-elasticsearch-service

[Source](https://aws.amazon.com/blogs/security/masking-field-values-with-amazon-elasticsearch-service/){:target="_blank" rel="noopener"}

> Amazon Elasticsearch Service (Amazon ES) is a fully managed service that you can use to deploy, secure, and run Elasticsearch cost-effectively at scale. The service provides support for open-source Elasticsearch APIs, managed Kibana, and integration with Logstash and other AWS services. Amazon ES provides a deep security model that spans many layers of interaction and [...]
