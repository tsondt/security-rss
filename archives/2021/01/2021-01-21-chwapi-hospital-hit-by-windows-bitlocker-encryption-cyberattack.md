Title: CHwapi hospital hit by Windows BitLocker encryption cyberattack
Date: 2021-01-21T11:22:05
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: chwapi-hospital-hit-by-windows-bitlocker-encryption-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/chwapi-hospital-hit-by-windows-bitlocker-encryption-cyberattack/){:target="_blank" rel="noopener"}

> The CHwapi hospital in Belgium is suffering from a cyberattack where threat actors claim to have encrypted 40 servers and 100 TB of data using Windows Bitlocker. [...]
