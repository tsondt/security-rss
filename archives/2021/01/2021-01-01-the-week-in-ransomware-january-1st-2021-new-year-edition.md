Title: The Week in Ransomware - January 1st 2021 - New Year Edition
Date: 2021-01-01T16:04:53
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-january-1st-2021-new-year-edition

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-1st-2021-new-year-edition/){:target="_blank" rel="noopener"}

> This holiday edition cover the latest ransomware news from the past two weeks, including known ransomware attacks and law enforcement takedowns. [...]
