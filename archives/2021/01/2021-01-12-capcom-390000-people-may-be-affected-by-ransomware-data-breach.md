Title: Capcom: 390,000 people may be affected by ransomware data breach
Date: 2021-01-12T16:37:31
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: capcom-390000-people-may-be-affected-by-ransomware-data-breach

[Source](https://www.bleepingcomputer.com/news/security/capcom-390-000-people-may-be-affected-by-ransomware-data-breach/){:target="_blank" rel="noopener"}

> Capcom has released a new update for their data breach investigation and state that up to 390,000 people may now be affected by their November ransomware attack. [...]
