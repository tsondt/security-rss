Title: CISA: Hackers bypassed MFA to access cloud service accounts
Date: 2021-01-13T16:24:52
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-hackers-bypassed-mfa-to-access-cloud-service-accounts

[Source](https://www.bleepingcomputer.com/news/security/cisa-hackers-bypassed-mfa-to-access-cloud-service-accounts/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) said today that threat actors bypassed multi-factor authentication (MFA) authentication protocols to compromise cloud service accounts. [...]
