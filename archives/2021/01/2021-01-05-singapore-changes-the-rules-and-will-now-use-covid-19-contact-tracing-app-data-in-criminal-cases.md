Title: Singapore changes the rules and will now use COVID-19 contact-tracing app data in criminal cases
Date: 2021-01-05T03:58:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: singapore-changes-the-rules-and-will-now-use-covid-19-contact-tracing-app-data-in-criminal-cases

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/05/singapore_covid_tracing/){:target="_blank" rel="noopener"}

> Privacy policy re-written, which is somewhat scary given Singapore has made trackers just-about-mandatory The Singapore government has decided to use data gathered by its TraceTogether COVID-19-coronavirus contact-tracing app in criminal investigations.... [...]
