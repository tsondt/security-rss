Title: Intel wheels out new face authentication product that works a lot like Apple's FaceID
Date: 2021-01-07T15:45:09+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: intel-wheels-out-new-face-authentication-product-that-works-a-lot-like-apples-faceid

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/07/intel_realsense_id/){:target="_blank" rel="noopener"}

> Chipzilla joins facial-recog race Intel has gingerly dipped a toe into the face-based authentication market with the launch of its RealSense ID product.... [...]
