Title: A Look at iMessage in iOS 14
Date: 2021-01-28T11:47:00-08:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: a-look-at-imessage-in-ios-14

[Source](https://googleprojectzero.blogspot.com/2021/01/a-look-at-imessage-in-ios-14.html){:target="_blank" rel="noopener"}

> Posted By Samuel Groß, Project Zero On December 20, Citizenlab published “ The Great iPwn ”, detailing how “Journalists [were] Hacked with Suspected NSO Group iMessage ‘Zero-Click’ Exploit”. Of particular interest is the following note: “We do not believe that [the exploit] works against iOS 14 and above, which includes new security protections''. Given that it is also now almost exactly one year ago since we published the Remote iPhone Exploitation blog post series, in which we described how an iMessage 0-click exploit can work in practice and gave a number of suggestions on how similar attacks could be prevented [...]
