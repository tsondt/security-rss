Title: Deploy an automated ChatOps solution for remediating Amazon Macie findings
Date: 2021-01-05T18:17:22+00:00
Author: Nick Cuneo
Category: AWS Security
Tags: Advanced (300);Amazon Macie;Security, Identity, & Compliance;automated incident response;chatops;Compliance;EventBridge;Incident response;Security Blog;Slack
Slug: deploy-an-automated-chatops-solution-for-remediating-amazon-macie-findings

[Source](https://aws.amazon.com/blogs/security/deploy-an-automated-chatops-solution-for-remediating-amazon-macie-findings/){:target="_blank" rel="noopener"}

> The amount of data being collected, stored, and processed by Amazon Web Services (AWS) customers is growing at an exponential rate. In order to keep pace with this growth, customers are turning to scalable cloud storage services like Amazon Simple Storage Service (Amazon S3) to build data lakes at the petabyte scale. Customers are looking [...]
