Title: 23M Gamer Records Exposed in VIPGames Leak
Date: 2021-01-26T19:35:44+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Privacy;Web Security
Slug: 23m-gamer-records-exposed-in-vipgames-leak

[Source](https://threatpost.com/gamer-records-exposed-vipgames-leak/163352/){:target="_blank" rel="noopener"}

> The personal data of 66,000 users was left wide open on a misconfigured Elasticsearch server, joining a growing list of companies with leaky clouds. [...]
