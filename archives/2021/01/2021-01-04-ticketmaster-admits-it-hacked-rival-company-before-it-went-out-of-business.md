Title: Ticketmaster admits it hacked rival company before it went out of business
Date: 2021-01-04T18:57:08+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;criminal cases;hacking;Ticketmaster
Slug: ticketmaster-admits-it-hacked-rival-company-before-it-went-out-of-business

[Source](https://arstechnica.com/?p=1732823){:target="_blank" rel="noopener"}

> (credit: Pixy ) Ticketmaster has agreed to pay a $10 million criminal fine after admitting its employees repeatedly used stolen passwords and other means to hack a rival ticket sales company. The fine, which is part of a deferred prosecution agreement Ticketmaster entered with federal prosecutors, resolves criminal charges filed last week in federal court in the eastern district of New York. Charges include violations of the Computer Fraud and Abuse Act, computer intrusion for commercial advantage or private financial gain, computer intrusion in furtherance of fraud, conspiracy to commit wire fraud, and wire fraud. In the settlement, Ticketmaster admitted [...]
