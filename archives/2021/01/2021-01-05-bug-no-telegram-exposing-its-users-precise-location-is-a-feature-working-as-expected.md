Title: Bug? No, Telegram exposing its users' precise location is a feature working as 'expected'
Date: 2021-01-05T17:14:07+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: bug-no-telegram-exposing-its-users-precise-location-is-a-feature-working-as-expected

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/05/telegram_location_people_nearby/){:target="_blank" rel="noopener"}

> Messaging app makes inadvertent oversharing too easy A researcher who noted that using the "People Nearby" feature of popular messaging app Telegram exposed the exact location of the user has been told that it's working as expected.... [...]
