Title: United States Congress stormed by violent followers of defeated president, Biden win confirmation halted
Date: 2021-01-06T21:38:31+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: united-states-congress-stormed-by-violent-followers-of-defeated-president-biden-win-confirmation-halted

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/06/us_congress_trump_stormed/){:target="_blank" rel="noopener"}

> Images of evacuated and invaded offices, Senate PCs still left switched on shared online Supporters of defeated American president Donald Trump this morning stormed the capital’s legislative halls, shutting down the process to confirm his replacement.... [...]
