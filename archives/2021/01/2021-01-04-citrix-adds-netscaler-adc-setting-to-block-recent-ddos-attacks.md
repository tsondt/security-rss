Title: Citrix adds NetScaler ADC setting to block recent DDoS attacks
Date: 2021-01-04T11:36:20
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: citrix-adds-netscaler-adc-setting-to-block-recent-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/citrix-adds-netscaler-adc-setting-to-block-recent-ddos-attacks/){:target="_blank" rel="noopener"}

> Citrix has released a feature enhancement designed to block attackers from using the Datagram Transport Layer Security (DTLS) feature of NetScaler ADC devices as an amplification vector in DDoS attacks. [...]
