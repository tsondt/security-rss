Title: Windows Remote Desktop servers now used to amplify DDoS attacks
Date: 2021-01-21T14:18:26
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: windows-remote-desktop-servers-now-used-to-amplify-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/windows-remote-desktop-servers-now-used-to-amplify-ddos-attacks/){:target="_blank" rel="noopener"}

> Windows Remote Desktop Protocol (RDP) servers are now being abused by DDoS-for-hire services to amplify Distributed Denial of Service (DDoS) attacks. [...]
