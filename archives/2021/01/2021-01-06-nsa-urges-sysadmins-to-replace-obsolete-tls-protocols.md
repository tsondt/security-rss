Title: NSA Urges SysAdmins to Replace Obsolete TLS Protocols
Date: 2021-01-06T22:16:51+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Vulnerabilities;and Defense Industrial Base;CloudFlare;Department of Defense (DoD);government;Heartbleed;National Security System (NSS);NSA;SSL;TLS 1.0;TLS 1.1;TLS 1.2;TLS 1.3;transport layer security protocol
Slug: nsa-urges-sysadmins-to-replace-obsolete-tls-protocols

[Source](https://threatpost.com/nsa-urges-sysadmins-to-replace-obsolete-tls-protocols/162814/){:target="_blank" rel="noopener"}

> The NSA released new guidance providing system administrators with the tools to update outdated TLS protocols. [...]
