Title: North Korea infected infosec bods with backdoors via dodgy blog pages, Visual Studio files – Google
Date: 2021-01-26T04:45:18+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: north-korea-infected-infosec-bods-with-backdoors-via-dodgy-blog-pages-visual-studio-files-google

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/26/norks_hack_researchers/){:target="_blank" rel="noopener"}

> Security eggheads discover their PCs chatting with Kim Jong Un's hackers North Korea's hackers homed in on specific infosec researchers and infected their systems with a backdoor after luring them to a suspicious website, Google revealed on Monday.... [...]
