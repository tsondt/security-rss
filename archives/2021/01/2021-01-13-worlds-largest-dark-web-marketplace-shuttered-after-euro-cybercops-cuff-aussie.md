Title: World’s largest dark-web marketplace shuttered after Euro cybercops cuff Aussie
Date: 2021-01-13T08:26:06+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: worlds-largest-dark-web-marketplace-shuttered-after-euro-cybercops-cuff-aussie

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/13/darkmarket_europol_shutdown/){:target="_blank" rel="noopener"}

> 20 DarkMarket servers siezed and probed in international raids Europol cops have taken down dark-web souk DarkMarket, after arresting an Australian citizen living in Germany who they claim was operating the world's biggest online bazaar of its kind.... [...]
