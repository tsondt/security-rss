Title: Critical zero-day RCE in Microsoft Office 365 awaits third security patch
Date: 2021-01-13T16:11:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: critical-zero-day-rce-in-microsoft-office-365-awaits-third-security-patch

[Source](https://portswigger.net/daily-swig/critical-zero-day-rce-in-microsoft-office-365-awaits-third-security-patch){:target="_blank" rel="noopener"}

> ‘Not all code execution bugs in.net are deserialization related’ [...]
