Title: Ryuk ransomware is the top threat for the healthcare sector
Date: 2021-01-05T07:10:51
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: ryuk-ransomware-is-the-top-threat-for-the-healthcare-sector

[Source](https://www.bleepingcomputer.com/news/security/ryuk-ransomware-is-the-top-threat-for-the-healthcare-sector/){:target="_blank" rel="noopener"}

> Healthcare organizations continue to be a prime target for cyberattacks of all kinds, with ransomware incidents, Ryuk in particular, being more prevalent. [...]
