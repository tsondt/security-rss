Title: DarkSide ransomware decryptor recovers victims' files for free
Date: 2021-01-11T12:11:42
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: darkside-ransomware-decryptor-recovers-victims-files-for-free

[Source](https://www.bleepingcomputer.com/news/security/darkside-ransomware-decryptor-recovers-victims-files-for-free/){:target="_blank" rel="noopener"}

> Romanian cybersecurity firm Bitdefender has released a free decryptor for the DarkSide ransomware to allow victims to recover their files without paying a ransom. [...]
