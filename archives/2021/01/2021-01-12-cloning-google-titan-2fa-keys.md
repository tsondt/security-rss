Title: Cloning Google Titan 2FA keys
Date: 2021-01-12T12:16:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cloning;cryptography;Google;hardware;side-channel attacks;two-factor authentication
Slug: cloning-google-titan-2fa-keys

[Source](https://www.schneier.com/blog/archives/2021/01/cloning-google-titan-2fa-keys.html){:target="_blank" rel="noopener"}

> This is a clever side-channel attack: The cloning works by using a hot air gun and a scalpel to remove the plastic key casing and expose the NXP A700X chip, which acts as a secure element that stores the cryptographic secrets. Next, an attacker connects the chip to hardware and software that take measurements as the key is being used to authenticate on an existing account. Once the measurement-taking is finished, the attacker seals the chip in a new casing and returns it to the victim. Extracting and later resealing the chip takes about four hours. It takes another six [...]
