Title: Facebook flaw meant attackers could create invisible posts on any verified page
Date: 2021-01-11T16:18:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: facebook-flaw-meant-attackers-could-create-invisible-posts-on-any-verified-page

[Source](https://portswigger.net/daily-swig/facebook-flaw-meant-attackers-could-create-invisible-posts-on-any-verified-page){:target="_blank" rel="noopener"}

> Discovery and post-fix bypass earn bug hunter two $15k payouts [...]
