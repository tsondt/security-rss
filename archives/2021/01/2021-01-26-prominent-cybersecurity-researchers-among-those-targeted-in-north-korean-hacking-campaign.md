Title: Prominent cybersecurity researchers among those targeted in North Korean hacking campaign
Date: 2021-01-26T15:49:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: prominent-cybersecurity-researchers-among-those-targeted-in-north-korean-hacking-campaign

[Source](https://portswigger.net/daily-swig/prominent-cybersecurity-researchers-among-those-targeted-in-north-korean-hacking-campaign){:target="_blank" rel="noopener"}

> Industry rocked by APT-linked attacks leveraging zero-day browser and OS vulnerabilities [...]
