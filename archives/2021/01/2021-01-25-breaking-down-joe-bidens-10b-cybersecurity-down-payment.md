Title: Breaking Down Joe Biden’s $10B Cybersecurity ‘Down Payment’
Date: 2021-01-25T21:51:13+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;Newsmaker Interviews;Podcasts
Slug: breaking-down-joe-bidens-10b-cybersecurity-down-payment

[Source](https://threatpost.com/breaking-down-joe-bidens-10b-cybersecurity-down-payment/163304/){:target="_blank" rel="noopener"}

> Tom Kellermann, head of cybersecurity strategy for VMware Carbon Black, talks about the top security challenges facing the US government as a new presidential administration steps in. [...]
