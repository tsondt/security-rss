Title: TikTok fixes flaws allowing theft of private user information
Date: 2021-01-26T06:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: tiktok-fixes-flaws-allowing-theft-of-private-user-information

[Source](https://www.bleepingcomputer.com/news/security/tiktok-fixes-flaws-allowing-theft-of-private-user-information/){:target="_blank" rel="noopener"}

> ByteDance, the tech firm behind TikTok, has fixed a security vulnerability in the video-sharing social networking service which could have allowed attackers to steal users' private information. [...]
