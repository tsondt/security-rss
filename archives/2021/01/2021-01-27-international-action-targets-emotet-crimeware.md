Title: International Action Targets Emotet Crimeware
Date: 2021-01-27T14:20:16+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;Emotet;Europol;Operation Ladybird;Ryuk;trickbot
Slug: international-action-targets-emotet-crimeware

[Source](https://krebsonsecurity.com/2021/01/international-action-targets-emotet-crimeware/){:target="_blank" rel="noopener"}

> Authorities across Europe on Tuesday said they’d seized control over Emotet, a prolific malware strain and cybercrime-as-service operation. Investigators say the action could help quarantine more than a million Microsoft Windows systems currently compromised with malware tied to Emotet infections. First surfacing in 2014, Emotet began as a banking trojan, but over the years it has evolved into one of the more aggressive platforms for spreading malware that lays the groundwork for ransomware attacks. In a statement published Wednesday morning on an action dubbed “ Operation Ladybird,” the European police agency Europol said the investigation involved authorities in the Netherlands, [...]
