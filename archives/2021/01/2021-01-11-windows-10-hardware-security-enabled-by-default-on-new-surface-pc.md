Title: Windows 10 hardware security enabled by default on new Surface PC
Date: 2021-01-11T13:46:03
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: windows-10-hardware-security-enabled-by-default-on-new-surface-pc

[Source](https://www.bleepingcomputer.com/news/security/windows-10-hardware-security-enabled-by-default-on-new-surface-pc/){:target="_blank" rel="noopener"}

> Microsoft has unveiled today the new Surface Pro 7+ for enterprise and educational customers, an ultra-light 2-in-1 device which comes with Windows Enhanced Hardware Security features enabled by default. [...]
