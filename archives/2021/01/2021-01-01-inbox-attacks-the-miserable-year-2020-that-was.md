Title: Inbox Attacks: The Miserable Year (2020) That Was
Date: 2021-01-01T11:00:04+00:00
Author: Tom Spring
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;2020 review;2021;Business Email Compromise;COVID-19;DMARC;human factor;maware;Microsoft 365;Phishing;predictions;Spam;work from home
Slug: inbox-attacks-the-miserable-year-2020-that-was

[Source](https://threatpost.com/miserable-spam-year-2020/162566/){:target="_blank" rel="noopener"}

> Reflecting on 2020's record-breaking year of spam and inbox threats. [...]
