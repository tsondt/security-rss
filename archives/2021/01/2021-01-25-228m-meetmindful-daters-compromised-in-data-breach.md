Title: 2.28M MeetMindful Daters Compromised in Data Breach
Date: 2021-01-25T21:08:02+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;Privacy;Web Security
Slug: 228m-meetmindful-daters-compromised-in-data-breach

[Source](https://threatpost.com/meetmindful-daters-compromised-data-breach/163313/){:target="_blank" rel="noopener"}

> The ShinyHunters hacking group offer a raft of information, from location and contact info to dating preferences and bodily descriptions, as a free download. [...]
