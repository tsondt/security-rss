Title: Misconfigurations in Spring Data projects could leave web apps open to abuse
Date: 2021-01-13T12:20:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: misconfigurations-in-spring-data-projects-could-leave-web-apps-open-to-abuse

[Source](https://portswigger.net/daily-swig/misconfigurations-in-spring-data-projects-could-leave-web-apps-open-to-abuse){:target="_blank" rel="noopener"}

> Flaw allowed attacker to view, modify, and delete data [...]
