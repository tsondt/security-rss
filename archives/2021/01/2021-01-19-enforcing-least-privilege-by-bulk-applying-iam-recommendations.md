Title: Enforcing least privilege by bulk-applying IAM recommendations
Date: 2021-01-19T17:00:00+00:00
Author: Abhishek Mishra
Category: GCP Security
Tags: Management Tools;Google Cloud Platform;Identity & Security
Slug: enforcing-least-privilege-by-bulk-applying-iam-recommendations

[Source](https://cloud.google.com/blog/products/identity-security/using-iam-recommender-to-bulk-apply-least-privilege-principles/){:target="_blank" rel="noopener"}

> Imagine this scenario: Your company has been using Google Cloud for a little while now. Things are going pretty well—no outages, no security breaches, and no unexpected costs. You've just begun to feel comfortable when an email comes in from a developer. She noticed that the project she works on has a service account with a Project Owner role, even though this service account was created solely to access the Cloud Storage API. She’s uncomfortable with these elevated permissions, so you begin investigating. As you dig deeper and start looking at a few projects in your organization, you notice multiple [...]
