Title: Amazon Has Trucks Filled with Hard Drives and an Armed Guard
Date: 2021-01-04T12:11:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Amazon;business of security;databases
Slug: amazon-has-trucks-filled-with-hard-drives-and-an-armed-guard

[Source](https://www.schneier.com/blog/archives/2021/01/amazon-has-trucks-filled-with-hard-drives-and-an-armed-guard.html){:target="_blank" rel="noopener"}

> From an interview with an Amazon Web Services security engineer: So when you use AWS, part of what you’re paying for is security. Right; it’s part of what we sell. Let’s say a prospective customer comes to AWS. They say, “I like pay-as-you-go pricing. Tell me more about that.” We say, “Okay, here’s how much you can use at peak capacity. Here are the savings we can see in your case.” Then the company says, “How do I know that I’m secure on AWS?” And this is where the heat turns up. This is where we get them. We say, [...]
