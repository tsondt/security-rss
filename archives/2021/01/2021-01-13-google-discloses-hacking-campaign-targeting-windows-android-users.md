Title: Google discloses hacking campaign targeting Windows, Android users
Date: 2021-01-13T08:51:07
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Microsoft
Slug: google-discloses-hacking-campaign-targeting-windows-android-users

[Source](https://www.bleepingcomputer.com/news/security/google-discloses-hacking-campaign-targeting-windows-android-users/){:target="_blank" rel="noopener"}

> Project Zero, Google's 0day bug-hunting team, revealed a hacking campaign coordinated by "a highly sophisticated actor" and targeting Windows and Android users with zero-day and n-day exploits. [...]
