Title: Biden to Appoint Cybersecurity Advisor to NSC – Report
Date: 2021-01-07T22:21:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cybersecurity;Federal government;Joe biden;NSA;White House
Slug: biden-to-appoint-cybersecurity-advisor-to-nsc-report

[Source](https://threatpost.com/biden-cybersecurity-advisor-nsc/162867/){:target="_blank" rel="noopener"}

> Anne Neuberger will join the National Security Council, according to sources. [...]
