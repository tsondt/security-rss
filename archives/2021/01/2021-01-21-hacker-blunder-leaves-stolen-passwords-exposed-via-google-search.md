Title: Hacker blunder leaves stolen passwords exposed via Google search
Date: 2021-01-21T07:12:51
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hacker-blunder-leaves-stolen-passwords-exposed-via-google-search

[Source](https://www.bleepingcomputer.com/news/security/hacker-blunder-leaves-stolen-passwords-exposed-via-google-search/){:target="_blank" rel="noopener"}

> Hackers hitting thousands of organizations worldwide in a massive phishing campaign forgot to protect their loot and let Google the stolen passwords for public searches. [...]
