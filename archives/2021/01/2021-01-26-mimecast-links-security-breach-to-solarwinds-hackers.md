Title: Mimecast links security breach to SolarWinds hackers
Date: 2021-01-26T10:01:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: mimecast-links-security-breach-to-solarwinds-hackers

[Source](https://www.bleepingcomputer.com/news/security/mimecast-links-security-breach-to-solarwinds-hackers/){:target="_blank" rel="noopener"}

> Email security company Mimecast has confirmed today that the threat actor behind the SolarWinds supply-chain attack is behind the security breach it disclosed earlier this month. [...]
