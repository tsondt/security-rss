Title: Massive Brazilian Data Breach
Date: 2021-01-26T12:15:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: massive-brazilian-data-breach

[Source](https://www.schneier.com/blog/archives/2021/01/massive-brazilian-data-breach.html){:target="_blank" rel="noopener"}

> I think this is the largest data breach of all time: 220 million people. ( Lots more stories are in Portuguese.) [...]
