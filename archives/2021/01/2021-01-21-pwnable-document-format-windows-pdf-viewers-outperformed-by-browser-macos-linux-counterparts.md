Title: Pwnable Document Format: Windows PDF viewers outperformed by browser, macOS, Linux counterparts
Date: 2021-01-21T15:13:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: pwnable-document-format-windows-pdf-viewers-outperformed-by-browser-macos-linux-counterparts

[Source](https://portswigger.net/daily-swig/pwnable-document-format-windows-pdf-viewers-outperformed-by-browser-macos-linux-counterparts){:target="_blank" rel="noopener"}

> Security researchers document their exploits in picking apart dozens of PDF software brands [...]
