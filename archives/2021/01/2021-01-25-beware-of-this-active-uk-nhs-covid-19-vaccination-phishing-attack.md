Title: Beware of this active UK NHS COVID-19 vaccination phishing attack
Date: 2021-01-25T18:27:59-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: beware-of-this-active-uk-nhs-covid-19-vaccination-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/beware-of-this-active-uk-nhs-covid-19-vaccination-phishing-attack/){:target="_blank" rel="noopener"}

> A very active phishing campaign is underway pretending to be from the UK's National Health Service (NHS), alerting recipients that they are eligible to receive the COVID-19 vaccine. [...]
