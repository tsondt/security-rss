Title: Online gaming platform VIP Games exposes 23 million data records on misconfigured server
Date: 2021-01-28T15:12:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: online-gaming-platform-vip-games-exposes-23-million-data-records-on-misconfigured-server

[Source](https://portswigger.net/daily-swig/online-gaming-platform-vip-games-exposes-23-million-data-records-on-misconfigured-server){:target="_blank" rel="noopener"}

> Popular website leaks personal information belonging to 66,000 players [...]
