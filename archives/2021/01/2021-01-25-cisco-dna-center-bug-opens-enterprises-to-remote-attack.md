Title: Cisco DNA Center Bug Opens Enterprises to Remote Attack
Date: 2021-01-25T17:53:51+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: cisco-dna-center-bug-opens-enterprises-to-remote-attack

[Source](https://threatpost.com/cisco-dna-center-bug-remote-attack/163302/){:target="_blank" rel="noopener"}

> The high-severity security vulnerability (CVE-2021-1257) allows cross-site request forgery (CSRF) attacks. [...]
