Title: Data breach at Buyucoin crypto exchange leaks user info, trades
Date: 2021-01-24T13:16:39-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: data-breach-at-buyucoin-crypto-exchange-leaks-user-info-trades

[Source](https://www.bleepingcomputer.com/news/security/data-breach-at-buyucoin-crypto-exchange-leaks-user-info-trades/){:target="_blank" rel="noopener"}

> A threat actor has leaked the stolen database for Indian cryptocurrency exchange Buyucoin on a hacking forum for free. [...]
