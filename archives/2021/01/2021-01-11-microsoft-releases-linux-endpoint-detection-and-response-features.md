Title: Microsoft releases Linux endpoint detection and response features
Date: 2021-01-11T16:40:06
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Linux;Microsoft
Slug: microsoft-releases-linux-endpoint-detection-and-response-features

[Source](https://www.bleepingcomputer.com/news/security/microsoft-releases-linux-endpoint-detection-and-response-features/){:target="_blank" rel="noopener"}

> Microsoft announced today that Microsoft Defender for Endpoint's detection and response (EDR) capabilities are now generally available on Linux servers. [...]
