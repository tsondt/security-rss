Title: FBI Warns of Egregor Attacks on Businesses Worldwide
Date: 2021-01-08T14:15:47+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware;barnes & noble;cyberattack;egregor;Encryption;FBI;malware;ransomware;ransomware as a service;threat actors
Slug: fbi-warns-of-egregor-attacks-on-businesses-worldwide

[Source](https://threatpost.com/fbi-egregor-attacks-businesses-worldwide/162885/){:target="_blank" rel="noopener"}

> The agency said the malware has already compromised more than 150 organizations and provided insight into its ransomware-as-a-service behavior. [...]
