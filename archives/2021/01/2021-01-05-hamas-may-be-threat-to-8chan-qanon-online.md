Title: Hamas May Be Threat to 8chan, QAnon Online
Date: 2021-01-05T19:27:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;8chan;CoreSite;ddos-guard;Hamas;Kobre & Kim;Nick Lim;QAnon;Ron Guilmette;Sean Buckley;SpartanHost;VanwaTech
Slug: hamas-may-be-threat-to-8chan-qanon-online

[Source](https://krebsonsecurity.com/2021/01/hamas-may-be-threat-to-8chan-qanon-online/){:target="_blank" rel="noopener"}

> In October 2020, KrebsOnSecurity looked at how a web of sites connected to conspiracy theory movements QAnon and 8chan were being kept online by DDoS-Guard, a dodgy Russian firm that also hosts the official site for the terrorist group Hamas. New research shows DDoS-Guard relies on data centers provided by a U.S.-based publicly traded company, which experts say could be exposed to civil and criminal liabilities as a result of DDoS-Guard’s business with Hamas. Many of the IP address ranges in in this map of QAnon and 8Chan-related sites — are assigned to VanwaTech. Source: twitter.com/Redrum_of_Crows Last year’s story examined [...]
