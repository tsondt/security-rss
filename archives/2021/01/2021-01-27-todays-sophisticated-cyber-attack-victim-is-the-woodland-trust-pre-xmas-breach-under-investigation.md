Title: Today's 'sophisticated cyber attack' victim is the Woodland Trust: Pre-Xmas breach under investigation
Date: 2021-01-27T15:30:56+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: todays-sophisticated-cyber-attack-victim-is-the-woodland-trust-pre-xmas-breach-under-investigation

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/27/woodland_trust_cyber_attack/){:target="_blank" rel="noopener"}

> Potentially 250,000 reasons UK nature conservation charity was targeted The Woodland Trust, a peaceful British charity that looks after trees, was struck by a “cyber attack” before Christmas.... [...]
