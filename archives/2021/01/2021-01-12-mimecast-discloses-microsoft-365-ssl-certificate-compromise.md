Title: Mimecast discloses Microsoft 365 SSL certificate compromise
Date: 2021-01-12T10:33:25
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: mimecast-discloses-microsoft-365-ssl-certificate-compromise

[Source](https://www.bleepingcomputer.com/news/security/mimecast-discloses-microsoft-365-ssl-certificate-compromise/){:target="_blank" rel="noopener"}

> Email security company Mimecast has disclosed today that a "sophisticated threat actor" compromised one of the certificates the company issues for customers to securely connect Microsoft 365 Exchange to their services. [...]
