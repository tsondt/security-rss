Title: FBI warns of Egregor ransomware extorting businesses worldwide
Date: 2021-01-07T11:37:44
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-egregor-ransomware-extorting-businesses-worldwide

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-egregor-ransomware-extorting-businesses-worldwide/){:target="_blank" rel="noopener"}

> The US Federal Bureau of Investigation (FBI) has sent a security alert warning private sector companies that the Egregor ransomware operation is actively targeting and extorting businesses worldwide. [...]
