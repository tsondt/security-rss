Title: ‘Train the basics’ – Bug bounty hunter ‘Xel’ on forging a lucrative career in ethical hacking
Date: 2021-01-21T14:06:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: train-the-basics-bug-bounty-hunter-xel-on-forging-a-lucrative-career-in-ethical-hacking

[Source](https://portswigger.net/daily-swig/train-the-basics-bug-bounty-hunter-xel-on-forging-a-lucrative-career-in-ethical-hacking){:target="_blank" rel="noopener"}

> Switzerland-based security researcher shares the secrets of his success [...]
