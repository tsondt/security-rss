Title: Microsoft January 2021 Patch Tuesday fixes 83 flaws, 1 zero-day
Date: 2021-01-12T13:27:35
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-january-2021-patch-tuesday-fixes-83-flaws-1-zero-day

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-january-2021-patch-tuesday-fixes-83-flaws-1-zero-day/){:target="_blank" rel="noopener"}

> With the January 2021 Patch Tuesday security updates release, Microsoft has released fixes for 83 vulnerabilities, with ten classified as Critical and 73 as Important. There is also one zero-day and one previously disclosed vulnerabilities fixed as part of the January 2021 updates. [...]
