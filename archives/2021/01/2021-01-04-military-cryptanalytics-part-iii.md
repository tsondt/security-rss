Title: Military Cryptanalytics, Part III
Date: 2021-01-04T20:34:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cryptanalysis;FOIA;history of cryptography;military;NSA
Slug: military-cryptanalytics-part-iii

[Source](https://www.schneier.com/blog/archives/2021/01/military-cryptanalytics-part-iii.html){:target="_blank" rel="noopener"}

> The NSA has just declassified and released a redacted version of Military Cryptanalytics, Part III, by Lambros D. Callimahos, October 1977. Parts I and II, by Lambros D. Callimahos and William F. Friedman, were released decades ago — I believe repeatedly, in increasingly unredacted form — and published by the late Wayne Griswold Barker’s Agean Park Press. I own them in hardcover. Like Parts I and II, Part III is primarily concerned with pre-computer ciphers. At this point, the document only has historical interest. If there is any lesson for today, it’s that modern cryptanalysis is possible primarily because people [...]
