Title: Privacy-focused search engine DuckDuckGo grew by 62% in 2020
Date: 2021-01-17T11:39:59
Author: Mayank Parmar
Category: BleepingComputer
Tags: Technology;Software
Slug: privacy-focused-search-engine-duckduckgo-grew-by-62-in-2020

[Source](https://www.bleepingcomputer.com/news/technology/privacy-focused-search-engine-duckduckgo-grew-by-62-percent-in-2020/){:target="_blank" rel="noopener"}

> The privacy-focused search engine DuckDuckGo continues to grow rapidly as the company reached 102M daily search queries for the first time in January. [...]
