Title: Indian government sites leaking patient COVID-19 test results
Date: 2021-01-05T04:45:40
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: indian-government-sites-leaking-patient-covid-19-test-results

[Source](https://www.bleepingcomputer.com/news/security/indian-government-sites-leaking-patient-covid-19-test-results/){:target="_blank" rel="noopener"}

> Multiple Indian government department websites are leaking COVID-19 lab test results for patients online. These reports uploaded by testing labs across the country as part of the national 'test, trace, isolate' efforts, expose patient's details, test site location, COVID-19 test results, dates, and the healthcare provider's info. [...]
