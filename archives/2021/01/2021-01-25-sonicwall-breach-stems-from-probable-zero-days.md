Title: SonicWall Breach Stems from ‘Probable’ Zero-Days
Date: 2021-01-25T17:04:19+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: sonicwall-breach-stems-from-probable-zero-days

[Source](https://threatpost.com/sonicwall-breach-zero-days-in-remote-access/163290/){:target="_blank" rel="noopener"}

> The security vendor is investigating potential zero-day vulnerabilities in its Secure Mobile Access (SMA) 100 series. [...]
