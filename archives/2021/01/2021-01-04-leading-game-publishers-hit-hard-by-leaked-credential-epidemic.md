Title: Leading Game Publishers Hit Hard by Leaked-Credential Epidemic
Date: 2021-01-04T20:14:52+00:00
Author: Tom Spring
Category: Threatpost
Tags: Breach;Hacks;Malware;Privacy;azorult;COVID-19;gamers;ID theft;password manager;ransomware;Slack;ubisoft;Video gamers;VPN
Slug: leading-game-publishers-hit-hard-by-leaked-credential-epidemic

[Source](https://threatpost.com/game-publishers-hit-by-leaked-credentials/162725/){:target="_blank" rel="noopener"}

> Over 500,000 leaked credentials tied to the top two dozen leading gaming companies are for sale online. [...]
