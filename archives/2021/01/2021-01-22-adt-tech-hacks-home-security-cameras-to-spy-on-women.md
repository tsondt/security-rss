Title: ADT Tech Hacks Home-Security Cameras to Spy on Women
Date: 2021-01-22T19:08:00+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Privacy;Web Security
Slug: adt-tech-hacks-home-security-cameras-to-spy-on-women

[Source](https://threatpost.com/adt-hacks-home-security-cameras/163271/){:target="_blank" rel="noopener"}

> A former ADT employee pleads guilty of accessing customers’ cameras so he could spy on them. [...]
