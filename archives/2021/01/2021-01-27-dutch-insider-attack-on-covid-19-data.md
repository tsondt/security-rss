Title: Dutch Insider Attack on COVID-19 Data
Date: 2021-01-27T14:59:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybercrime;databases;insiders;Netherlands;risks;theft
Slug: dutch-insider-attack-on-covid-19-data

[Source](https://www.schneier.com/blog/archives/2021/01/dutch-insider-attack-on-covid-19-data.html){:target="_blank" rel="noopener"}

> Insider data theft : Dutch police have arrested two individuals on Friday for allegedly selling data from the Dutch health ministry’s COVID-19 systems on the criminal underground. [...] According to Verlaan, the two suspects worked in DDG call centers, where they had access to official Dutch government COVID-19 systems and databases. They were working from home: “Because people are working from home, they can easily take photos of their screens. This is one of the issues when your administrative staff is working from home,” Victor Gevers, Chair of the Dutch Institute for Vulnerability Disclosure, told ZDNet in an interview today. [...]
