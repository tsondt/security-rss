Title: SolarWinds takes a leaf out of Zoom's book, hires A-Team of Stamos and Krebs to sort out its security woes
Date: 2021-01-11T12:36:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: solarwinds-takes-a-leaf-out-of-zooms-book-hires-a-team-of-stamos-and-krebs-to-sort-out-its-security-woes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/11/security_in_brief/){:target="_blank" rel="noopener"}

> The week's other security news In Brief Embattled and embarrassed network management shop SolarWinds has reportedly hired two of the highest profile security bods in the biz to sort out its woes.... [...]
