Title: Cloud Attacks Are Bypassing MFA, Feds Warn
Date: 2021-01-14T16:45:04+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Government;Vulnerabilities;Web Security
Slug: cloud-attacks-are-bypassing-mfa-feds-warn

[Source](https://threatpost.com/cloud-attacks-bypass-mfa-feds/163056/){:target="_blank" rel="noopener"}

> CISA has issued an alert warning that cloud services at U.S. organizations are being actively and successfully targeted. [...]
