Title: On US Capitol Security — By Someone Who Manages Arena-Rock-Concert Security
Date: 2021-01-13T12:06:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: operational security;physical security;police
Slug: on-us-capitol-security-by-someone-who-manages-arena-rock-concert-security

[Source](https://www.schneier.com/blog/archives/2021/01/on-us-capitol-security-by-someone-who-manages-arena-rock-concert-security.html){:target="_blank" rel="noopener"}

> Smart commentary :...I was floored on Wednesday when, glued to my television, I saw police in some areas of the U.S. Capitol using little more than those same mobile gates I had ­ the ones that look like bike racks that can hook together ­ to try to keep the crowds away from sensitive areas and, later, push back people intent on accessing the grounds. (A new fence that appears to be made of sturdier material was being erected on Thursday.) That’s the same equipment and approximately the same amount of force I was able to use when a group [...]
