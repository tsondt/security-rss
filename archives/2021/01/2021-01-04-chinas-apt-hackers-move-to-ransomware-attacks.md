Title: China's APT hackers move to ransomware attacks
Date: 2021-01-04T09:36:27
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: chinas-apt-hackers-move-to-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/chinas-apt-hackers-move-to-ransomware-attacks/){:target="_blank" rel="noopener"}

> Security researchers investigating a set of ransomware incidents at multiple companies discovered malware indicating that the attacks may be the work of a hacker group believed to operate on behalf of China. [...]
