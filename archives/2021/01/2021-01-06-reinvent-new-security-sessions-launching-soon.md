Title: re:Invent – New security sessions launching soon
Date: 2021-01-06T18:15:15+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;Data protection;Identity;Privacy;re:Invent 2020;Security Blog
Slug: reinvent-new-security-sessions-launching-soon

[Source](https://aws.amazon.com/blogs/security/reinvent-new-security-sessions-launching-soon/){:target="_blank" rel="noopener"}

> Where did the last month go? Were you able to catch all of the sessions in the Security, Identity, and Compliance track you hoped to see at AWS re:Invent? If you missed any, don’t worry—you can stream all the sessions released in 2020 via the AWS re:Invent website. Additionally, we’re starting 2021 with all new [...]
