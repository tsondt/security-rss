Title: Verified episode 3: In conversation with Noopur Davis from Comcast
Date: 2021-01-28T19:23:18+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Comcast;cybersecurity;Noopur Davis;secure WFH;Security Blog;Verified;Zero Trust Access
Slug: verified-episode-3-in-conversation-with-noopur-davis-from-comcast

[Source](https://aws.amazon.com/blogs/security/verified-episode-3-in-conversation-with-noopur-davis-from-comcast/){:target="_blank" rel="noopener"}

> 2020 emphasized the value of staying connected with our customers. On that front, I’m proud to bring you the third episode of our new video series, Verified. The series showcases conversations with security leaders discussing trends and lessons learned in cybersecurity, privacy, and the cloud. In episode three, I’m talking to Noopur Davis, Executive Vice [...]
