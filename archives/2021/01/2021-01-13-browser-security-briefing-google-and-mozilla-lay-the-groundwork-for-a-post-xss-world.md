Title: Browser security briefing: Google and Mozilla lay the groundwork for a ‘post-XSS world’
Date: 2021-01-13T14:44:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: browser-security-briefing-google-and-mozilla-lay-the-groundwork-for-a-post-xss-world

[Source](https://portswigger.net/daily-swig/browser-security-briefing-google-and-mozilla-lay-the-groundwork-for-a-post-xss-world){:target="_blank" rel="noopener"}

> The Firefox and Chrome development teams share their progress in minimizing the impact of classic web attacks [...]
