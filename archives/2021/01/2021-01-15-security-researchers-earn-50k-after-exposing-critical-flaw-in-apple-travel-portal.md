Title: Security researchers earn $50k after exposing critical flaw in Apple travel portal
Date: 2021-01-15T16:40:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-researchers-earn-50k-after-exposing-critical-flaw-in-apple-travel-portal

[Source](https://portswigger.net/daily-swig/security-researchers-earn-50k-after-exposing-critical-flaw-in-apple-travel-portal){:target="_blank" rel="noopener"}

> Chained exploit leads to shell access [...]
