Title: Changes in WhatsApp’s Privacy Policy
Date: 2021-01-11T12:17:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: data collection;Facebook;privacy;WhatsApp
Slug: changes-in-whatsapps-privacy-policy

[Source](https://www.schneier.com/blog/archives/2021/01/changes-in-whatsapps-privacy-policy.html){:target="_blank" rel="noopener"}

> If you’re a WhatsApp user, pay attention to the changes in the privacy policy that you’re being forced to agree with. In 2016, WhatsApp gave users a one-time ability to opt out of having account data turned over to Facebook. Now, an updated privacy policy is changing that. Come next month, users will no longer have that choice. Some of the data that WhatsApp collects includes: User phone numbers Other people’s phone numbers stored in address books Profile names Profile pictures and Status message including when a user was last online Diagnostic data collected from app logs Under the new [...]
