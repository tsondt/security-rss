Title: Police Have Disrupted the Emotet Botnet
Date: 2021-01-28T12:02:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;botnets;cybercrime;law enforcement;malware;phishing;ransomware
Slug: police-have-disrupted-the-emotet-botnet

[Source](https://www.schneier.com/blog/archives/2021/01/police-have-disrupted-the-emotet-botnet.html){:target="_blank" rel="noopener"}

> A coordinated effort has captured the command-and-control servers of the Emotet botnet: Emotet establishes a backdoor onto Windows computer systems via automated phishing emails that distribute Word documents compromised with malware. Subjects of emails and documents in Emotet campaigns are regularly altered to provide the best chance of luring victims into opening emails and installing malware ­ regular themes include invoices, shipping notices and information about COVID-19. Those behind the Emotet lease their army of infected machines out to other cyber criminals as a gateway for additional malware attacks, including remote access tools (RATs) and ransomware. [...] A week of [...]
