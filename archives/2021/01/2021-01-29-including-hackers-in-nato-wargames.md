Title: Including Hackers in NATO Wargames
Date: 2021-01-29T18:03:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: control;cyberwar;games;hacking;infrastructure;physical security
Slug: including-hackers-in-nato-wargames

[Source](https://www.schneier.com/blog/archives/2021/01/including-hackers-in-nato-wargames.html){:target="_blank" rel="noopener"}

> This essay makes the point that actual computer hackers would be a useful addition to NATO wargames: The international information security community is filled with smart people who are not in a military structure, many of whom would be excited to pose as independent actors in any upcoming wargames. Including them would increase the reality of the game and the skills of the soldiers building and training on these networks. Hackers and cyberwar experts would demonstrate how industrial control systems such as power supply for refrigeration and temperature monitoring in vaccine production facilities are critical infrastructure; they’re easy targets and [...]
