Title: Backdoor in Zyxel Firewalls and Gateways
Date: 2021-01-06T11:44:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;data protection;firewall;hardware;passwords;patching;VPN
Slug: backdoor-in-zyxel-firewalls-and-gateways

[Source](https://www.schneier.com/blog/archives/2021/01/backdoor-in-zyxel-firewalls-and-gateways.html){:target="_blank" rel="noopener"}

> This is bad : More than 100,000 Zyxel firewalls, VPN gateways, and access point controllers contain a hardcoded admin-level backdoor account that can grant attackers root access to devices via either the SSH interface or the web administration panel. [...] Installing patches removes the backdoor account, which, according to Eye Control researchers, uses the “zyfwp” username and the “PrOw!aN_fXp” password. “The plaintext password was visible in one of the binaries on the system,” the Dutch researchers said in a report published before the Christmas 2020 holiday. [...]
