Title: Microsoft warns of incoming Windows Zerologon patch enforcement
Date: 2021-01-15T09:51:59
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-warns-of-incoming-windows-zerologon-patch-enforcement

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-incoming-windows-zerologon-patch-enforcement/){:target="_blank" rel="noopener"}

> Microsoft today warned admins that updates addressing the Windows Zerologon vulnerability will transition into the enforcement phase starting next month. [...]
