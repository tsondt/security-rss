Title: Microsoft rolls out Application Guard for Office to all customers
Date: 2021-01-27T15:40:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-rolls-out-application-guard-for-office-to-all-customers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-rolls-out-application-guard-for-office-to-all-customers/){:target="_blank" rel="noopener"}

> Microsoft has announced that Application Guard for Office is now generally available for all Microsoft 365 users with supported licenses. [...]
