Title: Critical Cisco SD-WAN Bugs Allow RCE Attacks
Date: 2021-01-20T21:47:54+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: critical-cisco-sd-wan-bugs-allow-rce-attacks

[Source](https://threatpost.com/critical-cisco-sd-wan-bugs-rce-attacks/163204/){:target="_blank" rel="noopener"}

> Cisco is stoppering critical holes in its SD-WAN solutions and its smart software manager satellite. [...]
