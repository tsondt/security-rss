Title: Microsoft Defender for Office 365 to allow testing without setup
Date: 2021-01-04T17:00:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-for-office-365-to-allow-testing-without-setup

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-for-office-365-to-allow-testing-without-setup/){:target="_blank" rel="noopener"}

> Microsoft wants to add a new Office 365 feature to allow customers to test Microsoft Defender email protection without actually having to configure the environment and devices for your organization. [...]
