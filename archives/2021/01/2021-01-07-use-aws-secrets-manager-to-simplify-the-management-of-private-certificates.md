Title: Use AWS Secrets Manager to simplify the management of private certificates
Date: 2021-01-07T20:00:36+00:00
Author: Maitreya Ranganath
Category: AWS Security
Tags: AWS Certificate Manager;AWS Secrets Manager;Intermediate (200);Security, Identity, & Compliance;ACM;ACM Private CA;AWS KMS;Security Blog;systems manager
Slug: use-aws-secrets-manager-to-simplify-the-management-of-private-certificates

[Source](https://aws.amazon.com/blogs/security/use-aws-secrets-manager-to-simplify-the-management-of-private-certificates/){:target="_blank" rel="noopener"}

> AWS Certificate Manager (ACM) lets you easily provision, manage, and deploy public and private Secure Sockets Layer/Transport Layer Security (SSL/TLS) certificates for use with Amazon Web Services (AWS) services and your internal connected resources. For private certificates, AWS Certificate Manager Private Certificate Authority (ACM PCA) can be used to create private CA hierarchies, including root [...]
