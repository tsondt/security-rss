Title: Post-Backlash, WhatsApp Spells Out Privacy Policy Updates
Date: 2021-01-12T10:30:36+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Mobile Security;Privacy
Slug: post-backlash-whatsapp-spells-out-privacy-policy-updates

[Source](https://threatpost.com/post-backlash-whatsapp-spells-out-privacy-policy-updates/162996/){:target="_blank" rel="noopener"}

> WhatsApp aimed to clear the air about its updated privacy policy after reports of mandatory data sharing with Facebook drove users to Signal and Telegram in troves. [...]
