Title: Fired Healthcare Exec Stalls Critical PPE Shipment for Months
Date: 2021-01-07T19:36:31+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Cybersecurity;data tampering;Healthcare;insider threat;PPE
Slug: fired-healthcare-exec-stalls-critical-ppe-shipment-for-months

[Source](https://threatpost.com/healthcare-exec-stalls-critical-ppe-shipment/162855/){:target="_blank" rel="noopener"}

> A fired Stradis Healthcare employee sought revenge by tampering with shipping data for desperately needed healthcare PPE. [...]
