Title: Global cyber-espionage campaign linked to Russian spying tools
Date: 2021-01-11T13:31:30+00:00
Author: Andrew Roth in Moscow
Category: The Guardian
Tags: Espionage;Hacking;Cybercrime;Russia;US news;Europe;Internet;Technology;World news;Data and computer security;US foreign policy
Slug: global-cyber-espionage-campaign-linked-to-russian-spying-tools

[Source](https://www.theguardian.com/world/2021/jan/11/solarwinds-hack-russian-spying-tools-hackers-malware-fsb){:target="_blank" rel="noopener"}

> Kaspersky investigators uncover evidence that may support US claims Moscow was behind attack A Moscow-based cybersecurity company has reported that some of the malicious code employed against the US government in a cyber-attack last month overlaps with code previously used by suspected Russian hackers. The findings by Kaspersky investigators may provide the first public evidence to support accusations from Washington that Moscow was behind the biggest cyber-raid against the government in years, affecting 18,000 users of software produced by SolarWinds, including US government agencies. Related: What you need to know about the biggest hack of the US government in years [...]
