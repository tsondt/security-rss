Title: It's 2021 and you can hijack a Cisco SD-WAN deployment with malicious IP traffic and a buffer overflow. Patch now
Date: 2021-01-22T07:04:04+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: its-2021-and-you-can-hijack-a-cisco-sd-wan-deployment-with-malicious-ip-traffic-and-a-buffer-overflow-patch-now

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/22/cisco_critical_vulnerabilities/){:target="_blank" rel="noopener"}

> And also fix up these other holes that can be exploited via HTTP requests, SQL injection, etc Cisco this week emitted patches for four sets of critical-severity security holes in its products along with other fixes.... [...]
