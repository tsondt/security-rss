Title: Ransomware Attackers Publish 4K Private Scottish Gov Agency Files
Date: 2021-01-22T17:30:52+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware
Slug: ransomware-attackers-publish-4k-private-scottish-gov-agency-files

[Source](https://threatpost.com/attackers-publish-private-scottish-gov-files/163254/){:target="_blank" rel="noopener"}

> Up to 4,000 stolen files have been released by hackers who launched a ransomware attack against the Scottish Environmental Protection Agency on Christmas Eve. [...]
