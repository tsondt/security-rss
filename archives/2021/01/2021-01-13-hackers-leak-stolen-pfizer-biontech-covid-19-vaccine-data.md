Title: Hackers Leak Stolen Pfizer-BioNTech COVID-19 Vaccine Data
Date: 2021-01-13T17:15:17+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities
Slug: hackers-leak-stolen-pfizer-biontech-covid-19-vaccine-data

[Source](https://threatpost.com/hackers-leak-pfizer-covid-19-vaccine-data/163008/){:target="_blank" rel="noopener"}

> On the heels of a cyberattack on the EMA, cybercriminals have now leaked Pfizer and BioNTech COVID-19 vaccine data on the internet. [...]
