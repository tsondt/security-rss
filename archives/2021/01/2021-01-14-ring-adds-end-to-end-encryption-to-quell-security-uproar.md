Title: Ring Adds End-to-End Encryption to Quell Security Uproar
Date: 2021-01-14T13:28:22+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cryptography;Hacks;Privacy;Vulnerabilities
Slug: ring-adds-end-to-end-encryption-to-quell-security-uproar

[Source](https://threatpost.com/ring-adds-end-to-end-encryption-to-quell-security-uproar/163042/){:target="_blank" rel="noopener"}

> The optional feature was released free to users in a technical preview this week, adding a new layer of security to service, which has been plagued by privacy concerns. [...]
