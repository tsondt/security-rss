Title: Friday Squid Blogging: China Launches Six New Squid Jigging Vessels
Date: 2021-01-15T22:03:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-china-launches-six-new-squid-jigging-vessels

[Source](https://www.schneier.com/blog/archives/2021/01/friday-squid-blogging-china-launches-six-new-squid-jigging-vessels.html){:target="_blank" rel="noopener"}

> From Pingtan Marine Enterprise : The 6 large-scale squid jigging vessels are normally operating vessels that returned to China earlier this year from the waters of Southwest Atlantic Ocean for maintenance and repair. These vessels left the port of Mawei on December 17, 2020 and are sailing to the fishing grounds in the international waters of the Southeast Pacific Ocean for operation. I wonder if the company will include this blog post in its PR roundup. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my [...]
