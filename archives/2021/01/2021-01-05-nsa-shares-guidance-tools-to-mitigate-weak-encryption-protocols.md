Title: NSA shares guidance, tools to mitigate weak encryption protocols
Date: 2021-01-05T15:15:24
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: nsa-shares-guidance-tools-to-mitigate-weak-encryption-protocols

[Source](https://www.bleepingcomputer.com/news/security/nsa-shares-guidance-tools-to-mitigate-weak-encryption-protocols/){:target="_blank" rel="noopener"}

> The National Security Agency has shared guidance on how to detect and replace outdated Transport Layer Security (TLS) protocol versions with up to date and secure variants. [...]
