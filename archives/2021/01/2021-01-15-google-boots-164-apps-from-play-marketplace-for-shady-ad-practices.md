Title: Google Boots 164 Apps from Play Marketplace for Shady Ad Practices
Date: 2021-01-15T16:19:40+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security
Slug: google-boots-164-apps-from-play-marketplace-for-shady-ad-practices

[Source](https://threatpost.com/google-boots-164-apps-from-play/163091/){:target="_blank" rel="noopener"}

> The tech giant removes 164 more offending Android apps after banning software showing this type of behavior from the store last year. [...]
