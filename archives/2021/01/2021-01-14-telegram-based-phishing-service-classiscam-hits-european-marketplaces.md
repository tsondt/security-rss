Title: Telegram-based phishing service Classiscam hits European marketplaces
Date: 2021-01-14T07:06:02
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: telegram-based-phishing-service-classiscam-hits-european-marketplaces

[Source](https://www.bleepingcomputer.com/news/security/telegram-based-phishing-service-classiscam-hits-european-marketplaces/){:target="_blank" rel="noopener"}

> Dozens of cybercriminal gangs are publishing fake ads on popular online marketplaces to lure interested users to fraudulent merchant sites or to phishing pages that steal payment data. [...]
