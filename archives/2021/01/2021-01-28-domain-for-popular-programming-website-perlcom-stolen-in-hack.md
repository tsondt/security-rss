Title: Domain for popular programming website Perl.com stolen in ‘hack’
Date: 2021-01-28T16:35:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: domain-for-popular-programming-website-perlcom-stolen-in-hack

[Source](https://portswigger.net/daily-swig/domain-for-popular-programming-website-perl-com-stolen-in-hack){:target="_blank" rel="noopener"}

> Hack rumored to be related to other recent domain takeovers [...]
