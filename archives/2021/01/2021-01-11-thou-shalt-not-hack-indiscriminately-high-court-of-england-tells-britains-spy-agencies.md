Title: Thou shalt not hack indiscriminately, High Court of England tells Britain's spy agencies
Date: 2021-01-11T16:16:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: thou-shalt-not-hack-indiscriminately-high-court-of-england-tells-britains-spy-agencies

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/11/equipment_interference_privacy_international_judgment/){:target="_blank" rel="noopener"}

> Choke chain tightened on 'general warrants' after Privacy International wins judicial review A landmark High Court ruling has struck down Britain's ability to hack millions of people at a time through so-called "general warrants" in what privacy campaigners are hailing as a major victory.... [...]
