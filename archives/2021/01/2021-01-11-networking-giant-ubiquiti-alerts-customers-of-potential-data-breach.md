Title: Networking giant Ubiquiti alerts customers of potential data breach
Date: 2021-01-11T15:41:51
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: networking-giant-ubiquiti-alerts-customers-of-potential-data-breach

[Source](https://www.bleepingcomputer.com/news/security/networking-giant-ubiquiti-alerts-customers-of-potential-data-breach/){:target="_blank" rel="noopener"}

> Networking device maker Ubiquiti has announced a security incident that may have exposed its customers' data. [...]
