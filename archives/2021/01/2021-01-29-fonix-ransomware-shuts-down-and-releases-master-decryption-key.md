Title: Fonix ransomware shuts down and releases master decryption key
Date: 2021-01-29T21:20:42-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fonix-ransomware-shuts-down-and-releases-master-decryption-key

[Source](https://www.bleepingcomputer.com/news/security/fonix-ransomware-shuts-down-and-releases-master-decryption-key/){:target="_blank" rel="noopener"}

> The Fonix Ransomware operators have shut down their operation and released the master decryption allowing victims to recover their files for free. [...]
