Title: SolarWinds hackers used 7-Zip code to hide Raindrop Cobalt Strike loader
Date: 2021-01-19T14:09:38
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: solarwinds-hackers-used-7-zip-code-to-hide-raindrop-cobalt-strike-loader

[Source](https://www.bleepingcomputer.com/news/security/solarwinds-hackers-used-7-zip-code-to-hide-raindrop-cobalt-strike-loader/){:target="_blank" rel="noopener"}

> The ongoing analysis of the SolarWinds supply-chain attack uncovered a fourth malicious tool that researchers call Raindrop and was used for distribution across computers on the victim network. [...]
