Title: BeyondCorp Enterprise: Introducing a safer era of computing
Date: 2021-01-26T17:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: beyondcorp-enterprise-introducing-a-safer-era-of-computing

[Source](https://cloud.google.com/blog/products/identity-security/introducing-beyondcorp-enterprise/){:target="_blank" rel="noopener"}

> Security issues continue to disrupt the status quo for global enterprises. Recent incidents highlight the need to re-think our security plans and operations; attackers are getting smarter, attacks are more sophisticated, and assumptions about what is and isn’t locked down no longer hold. The challenge, however, is to enable disruptive innovation in security without disrupting security operations. Today, we’re excited to announce the general availability of Google’s comprehensive zero trust product offering, BeyondCorp Enterprise, which extends and replaces BeyondCorp Remote Access. Google is no stranger to zero trust—we’ve been on this journey for over a decade with our own implementation [...]
