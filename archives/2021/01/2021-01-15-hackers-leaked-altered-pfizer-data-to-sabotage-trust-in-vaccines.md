Title: Hackers leaked altered Pfizer data to sabotage trust in vaccines
Date: 2021-01-15T13:43:34
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hackers-leaked-altered-pfizer-data-to-sabotage-trust-in-vaccines

[Source](https://www.bleepingcomputer.com/news/security/hackers-leaked-altered-pfizer-data-to-sabotage-trust-in-vaccines/){:target="_blank" rel="noopener"}

> The European Medicines Agency (EMA) today revealed that some of the stolen Pfizer/BioNTech vaccine candidate data was doctored by threat actors before being leaked online with the end goal of undermining the public's trust in COVID-19 vaccines. [...]
