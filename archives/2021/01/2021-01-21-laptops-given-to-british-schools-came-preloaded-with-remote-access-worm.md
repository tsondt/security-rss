Title: Laptops given to British schools came preloaded with remote-access worm
Date: 2021-01-21T17:32:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: laptops-given-to-british-schools-came-preloaded-with-remote-access-worm

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/21/dept_education_school_laptops_malware/){:target="_blank" rel="noopener"}

> Department for Education says: 'We believe this is not widespread' Updated A shipment of laptops supplied to British schools by the Department for Education to help kids learn under lockdown came preloaded with malware, The Register can reveal.... [...]
