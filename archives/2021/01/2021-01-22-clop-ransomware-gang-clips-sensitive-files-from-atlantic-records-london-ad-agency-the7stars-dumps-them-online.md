Title: Clop ransomware gang clips sensitive files from Atlantic Records' London ad agency The7stars, dumps them online
Date: 2021-01-22T17:23:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: clop-ransomware-gang-clips-sensitive-files-from-atlantic-records-london-ad-agency-the7stars-dumps-them-online

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/22/the7stars_ransomware_attack_clop/){:target="_blank" rel="noopener"}

> Medium-sized firm, big revenues, big target Updated A London ad agency that counts Atlantic Records, Suzuki, and Penguin Random House among its clients has had its files dumped online by a ransomware gang, The Register can reveal.... [...]
