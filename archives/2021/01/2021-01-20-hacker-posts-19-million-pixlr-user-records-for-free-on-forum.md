Title: Hacker posts 1.9 million Pixlr user records for free on forum
Date: 2021-01-20T05:05:05
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hacker-posts-19-million-pixlr-user-records-for-free-on-forum

[Source](https://www.bleepingcomputer.com/news/security/hacker-posts-19-million-pixlr-user-records-for-free-on-forum/){:target="_blank" rel="noopener"}

> A hacker has leaked 1.9 million Pixlr user records containing information that could be used to perform targeted phishing and credential stuffing attacks. [...]
