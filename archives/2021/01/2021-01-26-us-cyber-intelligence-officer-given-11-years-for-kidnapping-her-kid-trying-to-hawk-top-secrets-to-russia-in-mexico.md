Title: US cyber intelligence officer given 11 years for kidnapping her kid, trying to hawk top secrets to Russia in Mexico
Date: 2021-01-26T22:59:07+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: us-cyber-intelligence-officer-given-11-years-for-kidnapping-her-kid-trying-to-hawk-top-secrets-to-russia-in-mexico

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/26/us_spy_russia_kidnapping/){:target="_blank" rel="noopener"}

> How's your year going? A US Air Force intelligence officer who kidnapped her daughter to Mexico and attempted to defect to Russia with information labelled top secret has been jailed for 11 years.... [...]
