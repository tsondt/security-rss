Title: DNSpooq Flaws Allow DNS Hijacking of Millions of Devices
Date: 2021-01-19T21:25:10+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: dnspooq-flaws-allow-dns-hijacking-of-millions-of-devices

[Source](https://threatpost.com/dnspooq-flaws-allow-dns-hijacking-of-millions-of-devices/163163/){:target="_blank" rel="noopener"}

> Seven flaws in open-source software Dnsmasq could allow DNS cache poisoning attacks and remote code execution. [...]
