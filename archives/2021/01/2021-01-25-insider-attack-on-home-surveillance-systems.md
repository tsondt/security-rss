Title: Insider Attack on Home Surveillance Systems
Date: 2021-01-25T15:33:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: hacking;insiders;privacy;surveillance
Slug: insider-attack-on-home-surveillance-systems

[Source](https://www.schneier.com/blog/archives/2021/01/insider-attack-on-home-surveillance-systems.html){:target="_blank" rel="noopener"}

> No one who reads this blog regularly will be surprised : A former employee of prominent home security company ADT has admitted that he hacked into the surveillance feeds of dozens of customer homes, doing so primarily to spy on naked women or to leer at unsuspecting couples while they had sex. [...] Authorities say that the IT technician “took note of which homes had attractive women, then repeatedly logged into these customers’ accounts in order to view their footage for sexual gratification.” He did this by adding his personal email address to customer accounts, which ultimately hooked him into [...]
