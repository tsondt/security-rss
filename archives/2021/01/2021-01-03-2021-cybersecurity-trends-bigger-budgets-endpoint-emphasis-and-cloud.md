Title: 2021 Cybersecurity Trends: Bigger Budgets, Endpoint Emphasis and Cloud
Date: 2021-01-03T15:00:52+00:00
Author: Threatpost
Category: Threatpost
Tags: Hacks;IoT;Malware;Mobile Security;Privacy;Vulnerabilities;Web Security;2021 Cybersecurity Trends;5G;artificial intelligence;Cloud Security;Cloud Security Posture Management;inbox attacks;Insider threats;machine learning;mobile threats
Slug: 2021-cybersecurity-trends-bigger-budgets-endpoint-emphasis-and-cloud

[Source](https://threatpost.com/2021-cybersecurity-trends/162629/){:target="_blank" rel="noopener"}

> Insider threats are redefined in 2021, the work-from-home trend will continue define the threat landscape and mobile endpoints become the attack vector of choice, according 2021 forecasts. [...]
