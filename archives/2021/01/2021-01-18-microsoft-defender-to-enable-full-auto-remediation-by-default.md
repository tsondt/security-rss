Title: Microsoft Defender to enable full auto-remediation by default
Date: 2021-01-18T13:30:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-to-enable-full-auto-remediation-by-default

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-to-enable-full-auto-remediation-by-default/){:target="_blank" rel="noopener"}

> Microsoft will enable fully automated threat remediation by default for Microsoft Defender for Endpoint customers who have opted into public previews starting next month, on February 16, 2021. [...]
