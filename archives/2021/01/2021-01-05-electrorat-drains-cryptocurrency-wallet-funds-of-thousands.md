Title: ElectroRAT Drains Cryptocurrency Wallet Funds of Thousands
Date: 2021-01-05T15:00:21+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware;crypto private keys;Cryptocurrency;DaoPoker;electroRAT;ETRADE;Golang programming language;Jamm;Linux;macOS;malware;RAT;Windows
Slug: electrorat-drains-cryptocurrency-wallet-funds-of-thousands

[Source](https://threatpost.com/electrorat-drains-cryptocurrency-wallet-funds-of-thousands/162705/){:target="_blank" rel="noopener"}

> At least 6,500 cryptocurrency users have been infected by new, 'extremely intrusive' malware that's spread via trojanized macOS, Windows and Linux apps. [...]
