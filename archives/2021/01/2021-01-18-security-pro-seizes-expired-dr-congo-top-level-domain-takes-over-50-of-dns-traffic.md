Title: Security pro seizes expired DR Congo top-level domain, takes over 50% of DNS traffic
Date: 2021-01-18T12:43:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-pro-seizes-expired-dr-congo-top-level-domain-takes-over-50-of-dns-traffic

[Source](https://portswigger.net/daily-swig/security-pro-seizes-expired-dr-congo-top-level-domain-takes-over-50-of-dns-traffic){:target="_blank" rel="noopener"}

> The researcher purchased the domain to stop it from falling into the wrong hands [...]
