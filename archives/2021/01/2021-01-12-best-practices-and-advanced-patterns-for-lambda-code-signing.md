Title: Best practices and advanced patterns for Lambda code signing
Date: 2021-01-12T18:53:01+00:00
Author: Cassia Martin
Category: AWS Security
Tags: Advanced (300);AWS Lambda;Best Practices;Security, Identity, & Compliance;code signing;code-signing certificates;cryptography;digital signing;Security Blog
Slug: best-practices-and-advanced-patterns-for-lambda-code-signing

[Source](https://aws.amazon.com/blogs/security/best-practices-and-advanced-patterns-for-lambda-code-signing/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) recently released Code Signing for AWS Lambda. By using this feature, you can help enforce the integrity of your code artifacts and make sure that only trusted developers can deploy code to your AWS Lambda functions. Today, let’s review a basic use case along with best practices for lambda code signing. [...]
