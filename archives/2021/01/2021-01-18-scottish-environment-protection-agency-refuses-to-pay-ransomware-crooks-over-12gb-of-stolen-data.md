Title: Scottish Environment Protection Agency refuses to pay ransomware crooks over 1.2GB of stolen data
Date: 2021-01-18T18:35:56+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: scottish-environment-protection-agency-refuses-to-pay-ransomware-crooks-over-12gb-of-stolen-data

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/18/scottish_environment_protection_agency_refuses_to_pay_ransom/){:target="_blank" rel="noopener"}

> Which is exactly what you should do Scotland's environmental watchdog has confirmed it is dealing with an "ongoing ransomware attack" likely masterminded by international "serious and organised" criminals during the last week of 2020.... [...]
