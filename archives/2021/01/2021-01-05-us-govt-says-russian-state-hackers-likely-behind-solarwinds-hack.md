Title: US govt says Russian state hackers likely behind SolarWinds hack
Date: 2021-01-05T15:56:56
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-govt-says-russian-state-hackers-likely-behind-solarwinds-hack

[Source](https://www.bleepingcomputer.com/news/security/us-govt-says-russian-state-hackers-likely-behind-solarwinds-hack/){:target="_blank" rel="noopener"}

> The Cyber Unified Coordination Group (UCG) said today that a Russian-backed Advanced Persistent Threat (APT) group is likely behind the SolarWinds hack. [...]
