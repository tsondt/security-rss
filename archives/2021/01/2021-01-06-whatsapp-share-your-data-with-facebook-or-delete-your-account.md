Title: WhatsApp: Share your data with Facebook or delete your account
Date: 2021-01-06T11:43:23
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: whatsapp-share-your-data-with-facebook-or-delete-your-account

[Source](https://www.bleepingcomputer.com/news/security/whatsapp-share-your-data-with-facebook-or-delete-your-account/){:target="_blank" rel="noopener"}

> After WhatsApp updated its Privacy Policy and Terms of Service on Monday with additional info on how it handles users' data, the company is now notifying users through the mobile app that, starting February, they will be required to share their data with Facebook. [...]
