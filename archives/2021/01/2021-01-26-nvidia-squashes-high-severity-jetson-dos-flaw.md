Title: Nvidia Squashes High-Severity Jetson DoS Flaw
Date: 2021-01-26T22:11:54+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: IoT;Vulnerabilities
Slug: nvidia-squashes-high-severity-jetson-dos-flaw

[Source](https://threatpost.com/nvidia-squashes-high-severity-jetson-dos-flaw/163360/){:target="_blank" rel="noopener"}

> If exploited, the most serious of these flaws could lead to a denial-of-service condition for Jetson products. [...]
