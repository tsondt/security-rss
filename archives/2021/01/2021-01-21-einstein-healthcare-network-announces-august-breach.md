Title: Einstein Healthcare Network Announces August Breach
Date: 2021-01-21T20:00:13+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks
Slug: einstein-healthcare-network-announces-august-breach

[Source](https://threatpost.com/einstein-healthcare-network-announces-august-breach/163237/){:target="_blank" rel="noopener"}

> Einstein is in violation of the the HHS 60-day breach notification rule, but unlikely to face penalty. [...]
