Title: Click Here to Kill Everybody Sale
Date: 2021-01-15T18:26:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: click-here-to-kill-everybody-sale

[Source](https://www.schneier.com/blog/archives/2021/01/click-here-to-kill-everybody-sale.html){:target="_blank" rel="noopener"}

> For a limited time, I am selling signed copies of Click Here to Kill Everybody in hardcover for just $6, plus shipping. Note that I have had occasional problems with international shipping. The book just disappears somewhere in the process. At this price, international orders are at the buyer’s risk. Also, the USPS keeps reminding us that shipping — both US and international — may be delayed during the pandemic. I have 500 copies of the book available. When they’re gone, the sale is over and the price will revert to normal. Order here. EDITED TO ADD: I was able [...]
