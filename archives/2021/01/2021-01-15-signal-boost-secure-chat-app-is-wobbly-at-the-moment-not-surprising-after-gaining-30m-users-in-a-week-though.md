Title: Signal boost: Secure chat app is wobbly at the moment. Not surprising after gaining 30m+ users in a week, though
Date: 2021-01-15T19:30:47+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: signal-boost-secure-chat-app-is-wobbly-at-the-moment-not-surprising-after-gaining-30m-users-in-a-week-though

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/15/signal_app_down/){:target="_blank" rel="noopener"}

> Install base explodes following WhatsApp 'privacy' update, Musk endorsement Updated Signal is experiencing a partial outage as tens of millions of netizens flood the free secure messaging service.... [...]
