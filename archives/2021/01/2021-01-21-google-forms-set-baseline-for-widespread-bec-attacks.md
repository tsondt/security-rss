Title: Google Forms Set Baseline For Widespread BEC Attacks
Date: 2021-01-21T15:02:34+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security
Slug: google-forms-set-baseline-for-widespread-bec-attacks

[Source](https://threatpost.com/google-forms-set-baseline-for-widespread-bec-attacks/163223/){:target="_blank" rel="noopener"}

> Researchers warn that attackers are collecting reconnaissance for future business email compromise attacks using Google Forms. [...]
