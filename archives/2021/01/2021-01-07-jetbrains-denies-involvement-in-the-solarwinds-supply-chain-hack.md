Title: JetBrains denies involvement in the SolarWinds supply-chain hack
Date: 2021-01-07T09:20:46
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: jetbrains-denies-involvement-in-the-solarwinds-supply-chain-hack

[Source](https://www.bleepingcomputer.com/news/security/jetbrains-denies-involvement-in-the-solarwinds-supply-chain-hack/){:target="_blank" rel="noopener"}

> JetBrains' CEO, Maxim Shafirov, denied reports from multiple news outlets that the company played a role in the SolarWinds supply chain attack. [...]
