Title: AnyVan confirms digital break-in, says customer names, emails and hashed passwords exposed
Date: 2021-01-19T08:45:09+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: anyvan-confirms-digital-break-in-says-customer-names-emails-and-hashed-passwords-exposed

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/19/anyvan_confirms_digital_breakin_says/){:target="_blank" rel="noopener"}

> Burglary took place 3 months before biz discovered unauthorised entry Anyvan, the European online marketplace that lets users buy delivery, transport or removal services from a network of providers, has confirmed it was the victim of a digital burglary that involved the theft of customers' personal data.... [...]
