Title: Pan-Asian retail giant Dairy Farm suffers REvil ransomware attack
Date: 2021-01-26T13:36:11-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: pan-asian-retail-giant-dairy-farm-suffers-revil-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/pan-asian-retail-giant-dairy-farm-suffers-revil-ransomware-attack/){:target="_blank" rel="noopener"}

> Massive pan-Asian retail chain operator Dairy Farm Group was attacked this month by the REvil ransomware operation, demanding a $30 million ransom. [...]
