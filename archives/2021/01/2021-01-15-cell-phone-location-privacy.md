Title: Cell Phone Location Privacy
Date: 2021-01-15T12:36:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cell phones;geolocation;privacy;surveillance
Slug: cell-phone-location-privacy

[Source](https://www.schneier.com/blog/archives/2021/01/cell-phone-location-privacy.html){:target="_blank" rel="noopener"}

> We all know that our cell phones constantly give our location away to our mobile network operators; that’s how they work. A group of researchers has figured out a way to fix that. “Pretty Good Phone Privacy” (PGPP) protects both user identity and user location using the existing cellular networks. It protects users from fake cell phone towers (IMSI-catchers) and surveillance by cell providers. It’s a clever system. The players are the user, a traditional mobile network operator (MNO) like AT&T or Verizon, and a new mobile virtual network operator (MVNO). MVNOs aren’t new. They’re intermediaries like Cricket and Boost. [...]
