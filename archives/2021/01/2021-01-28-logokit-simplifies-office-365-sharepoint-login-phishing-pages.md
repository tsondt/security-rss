Title: LogoKit Simplifies Office 365, SharePoint ‘Login’ Phishing Pages
Date: 2021-01-28T16:46:01+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: logokit-simplifies-office-365-sharepoint-login-phishing-pages

[Source](https://threatpost.com/logokit-simplifies-office-365-sharepoint-login-phishing-pages/163430/){:target="_blank" rel="noopener"}

> A phishing kit has been found running on at least 700 domains - and mimicking services via false SharePoint, OneDrive and Office 365 login portals. [...]
