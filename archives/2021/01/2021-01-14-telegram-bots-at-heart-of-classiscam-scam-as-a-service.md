Title: Telegram Bots at Heart of Classiscam Scam-as-a-Service
Date: 2021-01-14T17:20:34+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security
Slug: telegram-bots-at-heart-of-classiscam-scam-as-a-service

[Source](https://threatpost.com/telegram-bots-classiscam-scam/163061/){:target="_blank" rel="noopener"}

> The cybercriminal service has scammed victims out of $6.5 million and continues to spread on Telegram. [...]
