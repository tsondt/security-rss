Title: Microsoft SolarWinds analysis: Attackers hid inside Windows systems by wearing the skins of legit processes
Date: 2021-01-21T16:58:57+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: microsoft-solarwinds-analysis-attackers-hid-inside-windows-systems-by-wearing-the-skins-of-legit-processes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/21/microsoft_solarwinds_deep_dive/){:target="_blank" rel="noopener"}

> Thorough counter-detection methods laid bare by Redmond The SolarWinds hackers triggered one of their Cobalt Strike implants in the firm's network through a cunning VBScript that was activated by a routine system process, Microsoft has said.... [...]
