Title: OpenWRT Forum user data stolen in weekend data breach
Date: 2021-01-18T13:23:34
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: openwrt-forum-user-data-stolen-in-weekend-data-breach

[Source](https://www.bleepingcomputer.com/news/security/openwrt-forum-user-data-stolen-in-weekend-data-breach/){:target="_blank" rel="noopener"}

> The administrators of the OpenWRT forum, a large community of enthusiasts of alternative, open-source operating systems for routers, announced a data breach. [...]
