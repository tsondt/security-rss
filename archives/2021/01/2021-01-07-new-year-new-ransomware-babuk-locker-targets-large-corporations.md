Title: New Year, New Ransomware: Babuk Locker Targets Large Corporations
Date: 2021-01-07T18:08:14+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Malware;Babuk Locker;conti;Microsoft;ransomware;revil;Windows Restart Manager
Slug: new-year-new-ransomware-babuk-locker-targets-large-corporations

[Source](https://threatpost.com/ransomware-babuk-locker-large-corporations/162836/){:target="_blank" rel="noopener"}

> Despite being a mostly run-of-the-mill ransomware strain, Babuk Locker's encryption mechanisms and abuse of Windows Restart Manager sets it apart. [...]
