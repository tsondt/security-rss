Title: Extracting Personal Information from Large Language Models Like GPT-2
Date: 2021-01-07T12:14:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;cryptography;cyberattack;cybersecurity;data protection
Slug: extracting-personal-information-from-large-language-models-like-gpt-2

[Source](https://www.schneier.com/blog/archives/2021/01/extracting-personal-information-from-large-language-models-like-gpt-2.html){:target="_blank" rel="noopener"}

> Researchers have been able to find all sorts of personal information within GPT-2. This information was part of the training data, and can be extracted with the right sorts of queries. Paper: “ Extracting Training Data from Large Language Models.” Abstract: It has become common to publish large (billion parameter) language models that have been trained on private datasets. This paper demonstrates that in such settings, an adversary can perform a training data extraction attack to recover individual training examples by querying the language model. We demonstrate our attack on GPT-2, a language model trained on scrapes of the public [...]
