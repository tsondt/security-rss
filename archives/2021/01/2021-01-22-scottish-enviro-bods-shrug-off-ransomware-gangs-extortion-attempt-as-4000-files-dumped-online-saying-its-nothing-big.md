Title: Scottish enviro bods shrug off ransomware gang's extortion attempt as 4,000 files dumped online, saying it's nothing big
Date: 2021-01-22T19:00:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: scottish-enviro-bods-shrug-off-ransomware-gangs-extortion-attempt-as-4000-files-dumped-online-saying-its-nothing-big

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/22/sepa_ransomware_failure/){:target="_blank" rel="noopener"}

> Awa' an bile yer heid, SEPA tells ransomware scum About 4,000 stolen files from the Scottish Environmental Protection Agency (SEPA) have been dumped online by frustrated ransomware criminals after the public sector body refused to pay out.... [...]
