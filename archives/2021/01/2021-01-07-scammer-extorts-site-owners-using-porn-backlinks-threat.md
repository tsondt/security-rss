Title: Scammer extorts site owners using porn backlinks threat
Date: 2021-01-07T15:05:47
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: scammer-extorts-site-owners-using-porn-backlinks-threat

[Source](https://www.bleepingcomputer.com/news/security/scammer-extorts-site-owners-using-porn-backlinks-threat/){:target="_blank" rel="noopener"}

> Website owners are receiving emails threatening to ruin their reputation if they do not post a five-star review for a cryptocurrency exchange. [...]
