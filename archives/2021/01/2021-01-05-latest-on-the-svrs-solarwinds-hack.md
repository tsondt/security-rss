Title: Latest on the SVR’s SolarWinds Hack
Date: 2021-01-05T12:42:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: hacking;intelligence;Russia;supply chain
Slug: latest-on-the-svrs-solarwinds-hack

[Source](https://www.schneier.com/blog/archives/2021/01/latest-on-the-svrs-solarwinds-hack.html){:target="_blank" rel="noopener"}

> The New York Times has an in-depth article on the latest information about the SolarWinds hack (not a great name, since it’s much more far-reaching than that). Interviews with key players investigating what intelligence agencies believe to be an operation by Russia’s S.V.R. intelligence service revealed these points: The breach is far broader than first believed. Initial estimates were that Russia sent its probes only into a few dozen of the 18,000 government and private networks they gained access to when they inserted code into network management software made by a Texas company named SolarWinds. But as businesses like Amazon [...]
