Title: Windows 10 bug corrupts your hard drive on seeing this file's icon
Date: 2021-01-14T05:05:05
Author: Ax Sharma
Category: BleepingComputer
Tags: Microsoft;Technology
Slug: windows-10-bug-corrupts-your-hard-drive-on-seeing-this-files-icon

[Source](https://www.bleepingcomputer.com/news/security/windows-10-bug-corrupts-your-hard-drive-on-seeing-this-files-icon/){:target="_blank" rel="noopener"}

> An unpatched zero-day in Microsoft Windows 10 allows attackers to corrupt an NTFS-formatted hard drive with a one-line command. [...]
