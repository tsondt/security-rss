Title: Mind the (skills) gap: Cybersecurity talent pool must expand to take advantage of quantum computing opportunities
Date: 2021-01-18T16:01:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: mind-the-skills-gap-cybersecurity-talent-pool-must-expand-to-take-advantage-of-quantum-computing-opportunities

[Source](https://portswigger.net/daily-swig/mind-the-skills-gap-cybersecurity-talent-pool-must-expand-to-take-advantage-of-quantum-computing-opportunities){:target="_blank" rel="noopener"}

> Experts at the CES 2021 conference stress importance of security education [...]
