Title: Tractors, Pod Ice Cream and Lipstick Awarded CES 2021 Worst in Show
Date: 2021-01-15T22:04:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: IoT;Privacy;Vulnerabilities
Slug: tractors-pod-ice-cream-and-lipstick-awarded-ces-2021-worst-in-show

[Source](https://threatpost.com/tractors-pod-ice-cream-lipstick-ces-2021-worst/163117/){:target="_blank" rel="noopener"}

> Expert panel awards dubious honors to 2021 Consumer Electronics Show’s biggest flops, including security and privacy failures. [...]
