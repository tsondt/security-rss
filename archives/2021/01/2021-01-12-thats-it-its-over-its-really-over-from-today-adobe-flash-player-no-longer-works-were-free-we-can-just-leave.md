Title: That's it. It's over. It's really over. From today, Adobe Flash Player no longer works. We're free. We can just leave
Date: 2021-01-12T01:41:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: thats-it-its-over-its-really-over-from-today-adobe-flash-player-no-longer-works-were-free-we-can-just-leave

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/12/flash_is_dead/){:target="_blank" rel="noopener"}

> Post-Flashpocalypse, we stumble outside, hoping no one ever creates software as insecure as that ever again Adobe has finally and formally killed Flash.... [...]
