Title: Microsoft Patch Tuesday, January 2021 Edition
Date: 2021-01-13T01:32:20+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;Allan Liska;AskWoody.com;CVE-2018-8514;CVE-2019-1409;CVE-2019-1458;CVE-2020-1660;CVE-2021-1647;CVE-2021-1648;CVE-2021-1709;Dustin Childs;Immersive Labs;Kevin Breen;Recorded Future;Trend Micro's ZDI Initiative;Windows Defender
Slug: microsoft-patch-tuesday-january-2021-edition

[Source](https://krebsonsecurity.com/2021/01/microsoft-patch-tuesday-january-2021-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to plug more than 80 security holes in its Windows operating systems and other software, including one that is actively being exploited and another which was disclosed prior to today. Ten of the flaws earned Microsoft’s most-dire “critical” rating, meaning they could be exploited by malware or miscreants to seize remote control over unpatched systems with little or no interaction from Windows users. Most concerning of this month’s batch is probably a critical bug ( CVE-2021-1647 ) in Microsoft’s default anti-malware suite — Windows Defender — that is seeing active exploitation. Microsoft recently stopped providing a [...]
