Title: Researcher Builds Parler Archive Amid Amazon Suspension
Date: 2021-01-11T20:54:43+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Privacy;amazon;Data Scraping;Parler;social media;Web Archive
Slug: researcher-builds-parler-archive-amid-amazon-suspension

[Source](https://threatpost.com/parler-archive-amazon-suspension/162928/){:target="_blank" rel="noopener"}

> A researcher scraped and archived public Parler posts before the conservative social networking service was taken down by Amazon, Apple and Google. [...]
