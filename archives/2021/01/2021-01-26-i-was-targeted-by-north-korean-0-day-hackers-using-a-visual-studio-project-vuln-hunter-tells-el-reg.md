Title: I was targeted by North Korean 0-day hackers using a Visual Studio project, vuln hunter tells El Reg
Date: 2021-01-26T13:30:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: i-was-targeted-by-north-korean-0-day-hackers-using-a-visual-studio-project-vuln-hunter-tells-el-reg

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/26/north_korea_targeted_me_0_day/){:target="_blank" rel="noopener"}

> Hyperion Gray founder relates 'holy f**k' moment when he realised A zero-day hunter has told The Register of the “holy f**k” moment when he realised he'd been targeted by a North Korean campaign aimed at stealing Western researchers' vulns.... [...]
