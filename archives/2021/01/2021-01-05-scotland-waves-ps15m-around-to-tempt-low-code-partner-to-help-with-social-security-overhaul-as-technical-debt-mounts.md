Title: Scotland waves £15m around to tempt low-code partner to help with social security overhaul as technical debt mounts
Date: 2021-01-05T11:27:10+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: scotland-waves-ps15m-around-to-tempt-low-code-partner-to-help-with-social-security-overhaul-as-technical-debt-mounts

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/05/scotland_lowcode_partner/){:target="_blank" rel="noopener"}

> Though that might not be the workaround it needs The Scottish government is sizing up the market for suppliers to develop on its low-code technology platform and support its social security overhaul.... [...]
