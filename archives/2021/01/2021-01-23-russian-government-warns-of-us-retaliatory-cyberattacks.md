Title: Russian government warns of US retaliatory cyberattacks
Date: 2021-01-23T09:41:41-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: russian-government-warns-of-us-retaliatory-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/russian-government-warns-of-us-retaliatory-cyberattacks/){:target="_blank" rel="noopener"}

> The Russian government has issued a security warning to organizations in Russia about possible retaliatory cyberattacks by the USA for the SolarWinds breach. [...]
