Title: TransLink confirms ransomware data theft, still restoring systems
Date: 2021-01-04T13:11:55
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: translink-confirms-ransomware-data-theft-still-restoring-systems

[Source](https://www.bleepingcomputer.com/news/security/translink-confirms-ransomware-data-theft-still-restoring-systems/){:target="_blank" rel="noopener"}

> Metro Vancouver's transportation agency TransLink has confirmed that the Egregor ransomware operators who breached its network at the beginning of December 2020 also accessed and potentially stole employees' banking and social security information. [...]
