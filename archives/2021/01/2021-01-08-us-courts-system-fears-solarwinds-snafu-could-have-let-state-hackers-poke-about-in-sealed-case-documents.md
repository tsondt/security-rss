Title: US courts system fears SolarWinds snafu could have let state hackers poke about in sealed case documents
Date: 2021-01-08T19:30:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: us-courts-system-fears-solarwinds-snafu-could-have-let-state-hackers-poke-about-in-sealed-case-documents

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/08/solarwinds_court_docs/){:target="_blank" rel="noopener"}

> Problems for charging spies in future? Probably not, says ex-NCSC chief The SolarWinds hack exposed sealed US court documents – which could have a serious effect on Western sanctions against state-backed hackers.... [...]
