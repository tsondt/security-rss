Title: Is a remote workforce making your organisation less secure?
Date: 2021-01-14T07:00:11+00:00
Author: Elyse Silverberg
Category: The Register
Tags: 
Slug: is-a-remote-workforce-making-your-organisation-less-secure

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/14/regcast_can_sase_save_us/){:target="_blank" rel="noopener"}

> And can SASE save us? Webcast Last year your bosses embraced remote working because, let’s face it, none of us had a choice.... [...]
