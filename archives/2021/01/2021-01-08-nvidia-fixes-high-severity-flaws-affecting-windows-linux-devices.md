Title: NVIDIA fixes high severity flaws affecting Windows, Linux devices
Date: 2021-01-08T08:11:43
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: nvidia-fixes-high-severity-flaws-affecting-windows-linux-devices

[Source](https://www.bleepingcomputer.com/news/security/nvidia-fixes-high-severity-flaws-affecting-windows-linux-devices/){:target="_blank" rel="noopener"}

> NVIDIA has released security updates to address six security vulnerabilities found in Windows and Linux GPU display drivers, as well as ten additional flaws affecting the NVIDIA Virtual GPU (vGPU) management software. [...]
