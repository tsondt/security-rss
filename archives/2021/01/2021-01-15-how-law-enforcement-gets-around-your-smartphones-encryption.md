Title: How law enforcement gets around your smartphone’s encryption
Date: 2021-01-15T17:54:24+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Tech;android;encryption;iOS;law enforcement;smartphones
Slug: how-law-enforcement-gets-around-your-smartphones-encryption

[Source](https://arstechnica.com/?p=1735453){:target="_blank" rel="noopener"}

> Enlarge / Uberwachung, Symbolbild, Datensicherheit, Datenhoheit (credit: Westend61 | Getty Images) Lawmakers and law enforcement agencies around the world, including in the United States, have increasingly called for backdoors in the encryption schemes that protect your data, arguing that national security is at stake. But new research indicates governments already have methods and tools that, for better or worse, let them access locked smartphones thanks to weaknesses in the security schemes of Android and iOS. Cryptographers at Johns Hopkins University used publicly available documentation from Apple and Google as well as their own analysis to assess the robustness of Android [...]
