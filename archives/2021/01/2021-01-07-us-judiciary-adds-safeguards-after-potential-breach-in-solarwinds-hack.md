Title: US Judiciary adds safeguards after potential breach in SolarWinds hack
Date: 2021-01-07T14:03:50
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-judiciary-adds-safeguards-after-potential-breach-in-solarwinds-hack

[Source](https://www.bleepingcomputer.com/news/security/us-judiciary-adds-safeguards-after-potential-breach-in-solarwinds-hack/){:target="_blank" rel="noopener"}

> The Administrative Office of the U.S. Courts is investigating a potential compromise of the federal courts' case management and electronic case files system which stores millions of highly sensitive and confidential judiciary records. [...]
