Title: Another ransomware now uses DDoS attacks to force victims to pay
Date: 2021-01-24T10:01:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: another-ransomware-now-uses-ddos-attacks-to-force-victims-to-pay

[Source](https://www.bleepingcomputer.com/news/security/another-ransomware-now-uses-ddos-attacks-to-force-victims-to-pay/){:target="_blank" rel="noopener"}

> Another ransomware gang is now using DDoS attacks to force a victim to contact them and negotiate a ransom. [...]
