Title: Sudo Bug Gives Root Access to Mass Numbers of Linux Systems
Date: 2021-01-27T19:16:41+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities
Slug: sudo-bug-gives-root-access-to-mass-numbers-of-linux-systems

[Source](https://threatpost.com/sudo-bug-root-access-linux-2/163395/){:target="_blank" rel="noopener"}

> Qualys said the vuln gives any local user root access to systems running the most popular version of Sudo. [...]
