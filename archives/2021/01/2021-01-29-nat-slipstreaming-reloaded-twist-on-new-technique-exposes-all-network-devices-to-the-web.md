Title: NAT slipstreaming reloaded: Twist on new technique exposes all network devices to the web
Date: 2021-01-29T16:51:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nat-slipstreaming-reloaded-twist-on-new-technique-exposes-all-network-devices-to-the-web

[Source](https://portswigger.net/daily-swig/nat-slipstreaming-reloaded-twist-on-new-technique-exposes-all-network-devices-to-the-web){:target="_blank" rel="noopener"}

> Researchers develop new approach to original technique that exposed only the victim’s device [...]
