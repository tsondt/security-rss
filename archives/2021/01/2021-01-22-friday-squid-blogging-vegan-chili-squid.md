Title: Friday Squid Blogging: Vegan Chili Squid
Date: 2021-01-22T22:19:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-vegan-chili-squid

[Source](https://www.schneier.com/blog/archives/2021/01/friday-squid-blogging-vegan-chili-squid.html){:target="_blank" rel="noopener"}

> The restaurant chain Wagamama is selling a vegan version of its Chilli Squid side dish made from king oyster mushrooms. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
