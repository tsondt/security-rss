Title: The Taxman Cometh for ID Theft Victims
Date: 2021-01-29T18:56:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Tax Refund Fraud;The Coming Storm;1099-G;Karl Fava;Taxpayer Advocate Service;U.S. Internal Revenue Service
Slug: the-taxman-cometh-for-id-theft-victims

[Source](https://krebsonsecurity.com/2021/01/the-taxman-cometh-for-id-theft-victims/){:target="_blank" rel="noopener"}

> The unprecedented volume of unemployment insurance fraud witnessed in 2020 hasn’t abated, although news coverage of the issue has largely been pushed off the front pages by other events. But the ID theft problem is coming to the fore once again: Countless Americans will soon be receiving notices from state regulators saying they owe thousands of dollars in taxes on benefits they never received last year. One state’s experience offers a window into the potential scope of the problem. Hackers, identity thieves and overseas criminal rings stole over $11 billion in unemployment benefits from California last year, or roughly 10 [...]
