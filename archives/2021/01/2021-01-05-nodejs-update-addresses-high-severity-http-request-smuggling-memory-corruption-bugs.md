Title: Node.js update addresses high severity HTTP request smuggling, memory corruption bugs
Date: 2021-01-05T14:03:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nodejs-update-addresses-high-severity-http-request-smuggling-memory-corruption-bugs

[Source](https://portswigger.net/daily-swig/node-js-update-addresses-high-severity-http-request-smuggling-memory-corruption-bugs){:target="_blank" rel="noopener"}

> Developers urged to update now to protect their server-side applications [...]
