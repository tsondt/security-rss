Title: Nmap project becomes latest victim of Google’s ‘wrongful blocking’ of cybersecurity resources
Date: 2021-01-26T12:10:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nmap-project-becomes-latest-victim-of-googles-wrongful-blocking-of-cybersecurity-resources

[Source](https://portswigger.net/daily-swig/nmap-project-becomes-latest-victim-of-googles-wrongful-blocking-of-cybersecurity-resources){:target="_blank" rel="noopener"}

> Open source tool was incorrectly labeled as a threat by Chrome’s Safe Browsing program last week [...]
