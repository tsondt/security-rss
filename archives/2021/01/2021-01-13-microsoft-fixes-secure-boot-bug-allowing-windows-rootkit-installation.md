Title: Microsoft fixes Secure Boot bug allowing Windows rootkit installation
Date: 2021-01-13T11:24:33
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-fixes-secure-boot-bug-allowing-windows-rootkit-installation

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-secure-boot-bug-allowing-windows-rootkit-installation/){:target="_blank" rel="noopener"}

> Microsoft has fixed a security feature bypass vulnerability in Secure Boot that allows attackers to compromise the operating system's booting process even when Secure Boot is enabled. [...]
