Title: RCE ‘Bug’ Found and Disputed in Popular PHP Scripting Framework
Date: 2021-01-05T22:28:17+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities;Web Security;Bug;github;Laminas project;PHP;RCE;remote code execution;scripting framework;Zend Framework
Slug: rce-bug-found-and-disputed-in-popular-php-scripting-framework

[Source](https://threatpost.com/rce-bug-php-scripting-framework/162773/){:target="_blank" rel="noopener"}

> Impacted are PHP-based websites running a vulnerable version of the web-app creation tool Zend Framework and some Laminas Project releases. [...]
