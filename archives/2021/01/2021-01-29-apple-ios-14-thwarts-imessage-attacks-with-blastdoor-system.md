Title: Apple iOS 14 Thwarts iMessage Attacks With BlastDoor System
Date: 2021-01-29T16:52:30+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: apple-ios-14-thwarts-imessage-attacks-with-blastdoor-system

[Source](https://threatpost.com/apple-ios-imessage-blastdoor/163479/){:target="_blank" rel="noopener"}

> Apple has made structural improvements in iOS 14 to block message-based, zero-click exploits. [...]
