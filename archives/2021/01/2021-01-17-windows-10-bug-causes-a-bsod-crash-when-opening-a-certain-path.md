Title: Windows 10 bug causes a BSOD crash when opening a certain path
Date: 2021-01-17T15:21:27
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: windows-10-bug-causes-a-bsod-crash-when-opening-a-certain-path

[Source](https://www.bleepingcomputer.com/news/security/windows-10-bug-causes-a-bsod-crash-when-opening-a-certain-path/){:target="_blank" rel="noopener"}

> A bug in Windows 10 causes the operating system to crash with a Blue Screen of Death simply by opening a certain path in a browser's address bar or using other Windows commands. [...]
