Title: Joker’s Stash Carding Market to Call it Quits
Date: 2021-01-18T19:50:01+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Gemini Advisory;Intel 471;Joker's stash
Slug: jokers-stash-carding-market-to-call-it-quits

[Source](https://krebsonsecurity.com/2021/01/jokers-stash-carding-market-to-call-it-quits/){:target="_blank" rel="noopener"}

> Joker’s Stash, by some accounts the largest underground shop for selling stolen credit card and identity data, says it’s closing up shop effective mid-February 2021. The announcement came on the heels of a turbulent year for the major cybercrime store, and just weeks after U.S. and European authorities seized a number of its servers. A farewell message posted by Joker’s Stash admin on Jan. 15, 2021. The Russian and English language carding store first opened in October 2014, and quickly became a major source of “dumps” — information stolen from compromised payment cards that thieves can buy and use to [...]
