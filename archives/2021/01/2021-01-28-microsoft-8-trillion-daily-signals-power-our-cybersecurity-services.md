Title: Microsoft: 8 trillion daily signals power our cybersecurity services
Date: 2021-01-28T16:37:36-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-8-trillion-daily-signals-power-our-cybersecurity-services

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-8-trillion-daily-signals-power-our-cybersecurity-services/){:target="_blank" rel="noopener"}

> Microsoft's security services grew by $10 billion in 2020, as more companies began utilizing their cloud-based security services. [...]
