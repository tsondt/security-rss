Title: Netwalker ransomware dark web sites seized by law enforcement
Date: 2021-01-27T11:15:36-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: netwalker-ransomware-dark-web-sites-seized-by-law-enforcement

[Source](https://www.bleepingcomputer.com/news/security/netwalker-ransomware-dark-web-sites-seized-by-law-enforcement/){:target="_blank" rel="noopener"}

> The dark web websites associated with the Netwalker ransomware operation have been seized by law enforcement from the USA and Bulgaria. [...]
