Title: Microsoft Edge gets a password generator, leaked credentials monitor
Date: 2021-01-21T13:05:22
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-edge-gets-a-password-generator-leaked-credentials-monitor

[Source](https://www.bleepingcomputer.com/news/security/microsoft-edge-gets-a-password-generator-leaked-credentials-monitor/){:target="_blank" rel="noopener"}

> Microsoft is rolling out a built-in password generator and a leaked credentials monitoring feature on Windows and macOS systems running the latest Microsoft Edge version. [...]
