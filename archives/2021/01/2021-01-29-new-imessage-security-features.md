Title: New iMessage Security Features
Date: 2021-01-29T15:20:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Apple;cybersecurity;hacking;iPhone;mitigation
Slug: new-imessage-security-features

[Source](https://www.schneier.com/blog/archives/2021/01/new-imessage-security-features.html){:target="_blank" rel="noopener"}

> Apple has added added security features to mitigate the risk of zero-click iMessage attacks. Apple did not document the changes but Groß said he fiddled around with the newest iOS 14 and found that Apple shipped a “significant refactoring of iMessage processing” that severely cripples the usual ways exploits are chained together for zero-click attacks. Groß notes that memory corruption based zero-click exploits typically require exploitation of multiple vulnerabilities to create exploit chains. In most observed attacks, these could include a memory corruption vulnerability, reachable without user interaction and ideally without triggering any user notifications; a way to break ASLR [...]
