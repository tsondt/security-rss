Title: Facebook: Malicious Chrome Extension Developers Scraped Profile Data
Date: 2021-01-14T22:30:52+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Facebook
Slug: facebook-malicious-chrome-extension-developers-scraped-profile-data

[Source](https://threatpost.com/facebook-malicious-chrome-extension-developers-scraped-profile-data/163079/){:target="_blank" rel="noopener"}

> Facebook has sued two Chrome devs for scraping user profile data - including names, user IDs and more. [...]
