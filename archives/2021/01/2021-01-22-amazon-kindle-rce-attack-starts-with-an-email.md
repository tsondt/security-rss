Title: Amazon Kindle RCE Attack Starts with an Email
Date: 2021-01-22T21:55:34+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Vulnerabilities;Web Security
Slug: amazon-kindle-rce-attack-starts-with-an-email

[Source](https://threatpost.com/amazon-kindle-attack-email/163282/){:target="_blank" rel="noopener"}

> The "KindleDrip" attack would have allowed attackers to siphon money from unsuspecting victims. [...]
