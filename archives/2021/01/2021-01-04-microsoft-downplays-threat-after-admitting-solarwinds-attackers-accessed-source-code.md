Title: Microsoft downplays threat after admitting SolarWinds attackers accessed source code
Date: 2021-01-04T14:49:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: microsoft-downplays-threat-after-admitting-solarwinds-attackers-accessed-source-code

[Source](https://portswigger.net/daily-swig/microsoft-downplays-threat-after-admitting-solarwinds-attackers-accessed-source-code){:target="_blank" rel="noopener"}

> Software blueprints acquired but not altered [...]
