Title: Google Searches Expose Stolen Corporate Credentials
Date: 2021-01-21T14:00:41+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Privacy;Web Security
Slug: google-searches-expose-stolen-corporate-credentials

[Source](https://threatpost.com/attackers-leave-stolen-credentials-google-searches/163220/){:target="_blank" rel="noopener"}

> A phishing campaign spoofs Xerox notifications to lure victims into clicking on malicious HTML attachments. [...]
