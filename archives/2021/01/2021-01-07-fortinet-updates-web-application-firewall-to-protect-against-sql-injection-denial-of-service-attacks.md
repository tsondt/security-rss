Title: Fortinet updates web application firewall to protect against SQL injection, denial-of-service attacks
Date: 2021-01-07T14:24:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: fortinet-updates-web-application-firewall-to-protect-against-sql-injection-denial-of-service-attacks

[Source](https://portswigger.net/daily-swig/fortinet-updates-web-application-firewall-to-protect-against-sql-injection-denial-of-service-attacks){:target="_blank" rel="noopener"}

> Security module heals itself [...]
