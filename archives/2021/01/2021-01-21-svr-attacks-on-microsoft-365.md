Title: SVR Attacks on Microsoft 365
Date: 2021-01-21T12:31:05+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;hacking;Microsoft;Russia
Slug: svr-attacks-on-microsoft-365

[Source](https://www.schneier.com/blog/archives/2021/01/svr-attacks-on-microsoft-365.html){:target="_blank" rel="noopener"}

> FireEye is reporting the current known tactics that the SVR used to compromise Microsoft 365 cloud data as part of its SolarWinds operation: Mandiant has observed UNC2452 and other threat actors moving laterally to the Microsoft 365 cloud using a combination of four primary techniques: Steal the Active Directory Federation Services (AD FS) token-signing certificate and use it to forge tokens for arbitrary users (sometimes described as Golden SAML ). This would allow the attacker to authenticate into a federated resource provider (such as Microsoft 365) as any user, without the need for that user’s password or their corresponding multi-factor [...]
