Title: Sealed U.S. Court Records Exposed in SolarWinds Breach
Date: 2021-01-07T23:48:25+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Administrative Office of the U.S. Courts;Nicholas Weaver;Orion;PACER;SolarWinds breach;U.S. Justice Department
Slug: sealed-us-court-records-exposed-in-solarwinds-breach

[Source](https://krebsonsecurity.com/2021/01/sealed-u-s-court-records-exposed-in-solarwinds-breach/){:target="_blank" rel="noopener"}

> The ongoing breach affecting thousands of organizations that relied on backdoored products by network software firm SolarWinds may have jeopardized the privacy of countless sealed court documents on file with the U.S. federal court system, according to a memo released Wednesday by the Administrative Office (AO) of the U.S. Courts. The judicial branch agency said it will be deploying more stringent controls for receiving and storing sensitive documents filed with the federal courts, following a discovery that its own systems were compromised as part of the SolarWinds supply chain attack. That intrusion involved malicious code being surreptitiously inserted into updates [...]
