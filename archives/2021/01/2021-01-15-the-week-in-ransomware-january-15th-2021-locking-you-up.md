Title: The Week in Ransomware - January 15th 2021 - Locking you up
Date: 2021-01-15T17:37:47
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-january-15th-2021-locking-you-up

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-15th-2021-locking-you-up/){:target="_blank" rel="noopener"}

> It has been another quiet week for ransomware, though we did have some interesting stories come out this week. [...]
