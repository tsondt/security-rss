Title: Google Chrome blocks 7 more ports to stop NAT Slipstreaming attacks
Date: 2021-01-28T11:11:19-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google
Slug: google-chrome-blocks-7-more-ports-to-stop-nat-slipstreaming-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-blocks-7-more-ports-to-stop-nat-slipstreaming-attacks/){:target="_blank" rel="noopener"}

> Google Chrome now blocks access to websites on an additional seven TCP ports to protect against the NAT Slipstreaming 2.0 vulnerability. [...]
