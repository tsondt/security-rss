Title: Intigriti launches EU-backed bug bounty program for Matrix secure communications tool
Date: 2021-01-14T16:17:48+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: intigriti-launches-eu-backed-bug-bounty-program-for-matrix-secure-communications-tool

[Source](https://portswigger.net/daily-swig/intigriti-launches-eu-backed-bug-bounty-program-for-matrix-secure-communications-tool){:target="_blank" rel="noopener"}

> New scheme comes in wake of successful EU-FOSSA campaign [...]
