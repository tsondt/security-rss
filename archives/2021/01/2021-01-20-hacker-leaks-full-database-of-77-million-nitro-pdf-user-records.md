Title: Hacker leaks full database of 77 million Nitro PDF user records
Date: 2021-01-20T12:17:04
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hacker-leaks-full-database-of-77-million-nitro-pdf-user-records

[Source](https://www.bleepingcomputer.com/news/security/hacker-leaks-full-database-of-77-million-nitro-pdf-user-records/){:target="_blank" rel="noopener"}

> A stolen database containing the email addresses, names, and passwords of more than 77 million records of Nitro PDF service users was leaked today for free. [...]
