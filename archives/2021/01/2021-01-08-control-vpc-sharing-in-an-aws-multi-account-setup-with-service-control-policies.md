Title: Control VPC sharing in an AWS multi-account setup with service control policies
Date: 2021-01-08T16:27:21+00:00
Author: Anandprasanna Gaitonde
Category: AWS Security
Tags: Intermediate (200);Resource Access Manager (RAM);Security, Identity, & Compliance;AWS Organizations;AWS RAM;Management and Governance;Networking;Security Blog;service control policies;VPC sharing
Slug: control-vpc-sharing-in-an-aws-multi-account-setup-with-service-control-policies

[Source](https://aws.amazon.com/blogs/security/control-vpc-sharing-in-an-aws-multi-account-setup-with-service-control-policies/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) customers who establish shared infrastructure services in a multi-account environment through AWS Organizations and AWS Resource Access Manager (RAM) may find that the default permissions assigned to the management account are too broad. This may allow organizational accounts to share virtual private clouds (VPCs) with other accounts that shouldn’t have access. [...]
