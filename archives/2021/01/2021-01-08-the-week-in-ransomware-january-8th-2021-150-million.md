Title: The Week in Ransomware - January 8th 2021 - $150 million
Date: 2021-01-08T18:17:48
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-january-8th-2021-150-million

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-8th-2021-150-million/){:target="_blank" rel="noopener"}

> Even though the holidays are over in many countries, it has been a very quiet week for ransomware. Unfortunately, ransomware activity will likely pick up shortly. [...]
