Title: All Aboard the Pequod!
Date: 2021-01-07T20:18:49+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;8chan;Hartford Courant;Herman Melville;Jake Angeli;Jim Watkins;Moby Dick;President Trump;Q Shaman;QAnon;Queequeg;Rachel E. Greenspan;Ron Guilmette;Ron Watkins
Slug: all-aboard-the-pequod

[Source](https://krebsonsecurity.com/2021/01/all-aboard-the-pequod/){:target="_blank" rel="noopener"}

> Like countless others, I frittered away the better part of Jan. 6 doomscrolling and watching television coverage of the horrifying events unfolding in our nation’s capital, where a mob of President Trump supporters and QAnon conspiracy theorists was incited to lay siege to the U.S. Capitol. For those trying to draw meaning from the experience, might I suggest consulting the literary classic Moby Dick, which simultaneously holds clues about QAnon’s origins and offers an apt allegory about a modern-day Captain Ahab and his ill-fated obsessions. Many have speculated that Jim Watkins, the administrator of the online message board 8chan (a.k.a. [...]
