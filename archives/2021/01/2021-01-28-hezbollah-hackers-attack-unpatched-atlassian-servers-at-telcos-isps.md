Title: Hezbollah hackers attack unpatched Atlassian servers at telcos, ISPs
Date: 2021-01-28T13:42:16-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hezbollah-hackers-attack-unpatched-atlassian-servers-at-telcos-isps

[Source](https://www.bleepingcomputer.com/news/security/hezbollah-hackers-attack-unpatched-atlassian-servers-at-telcos-isps/){:target="_blank" rel="noopener"}

> Volatile Cedar, an advanced hacker group believed to be connected to the Lebanese Hezbollah Cyber Unit, has been silently attacking companies around the world in espionage operations. [...]
