Title: Bye bye, said Trump admin to Huawei: You give a cheque-ie to our techies, but there's no licence to ply
Date: 2021-01-18T17:00:04+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: bye-bye-said-trump-admin-to-huawei-you-give-a-cheque-ie-to-our-techies-but-theres-no-licence-to-ply

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/18/huawei_licence/){:target="_blank" rel="noopener"}

> And them good ol' boys revokin' sanction to buy, singin', 'Soon will come the day that we fly' As parting gifts go, this one ranks pretty low. With less than three days until the inauguration of Joe Biden, the Trump Administration has reportedly revoked several licences that would allow Huawei to buy US-made tech, and plans to deny over 150 pending requests.... [...]
