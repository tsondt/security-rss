Title: Google Chrome now checks for weak passwords, helps fix them
Date: 2021-01-20T13:22:33
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-chrome-now-checks-for-weak-passwords-helps-fix-them

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-now-checks-for-weak-passwords-helps-fix-them/){:target="_blank" rel="noopener"}

> Google has added a new feature to the Chrome web browser that will make it easier for users to check if their stored passwords are weak and easy to guess. [...]
