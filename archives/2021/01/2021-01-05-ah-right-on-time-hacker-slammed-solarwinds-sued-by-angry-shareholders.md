Title: Ah, right on time: Hacker-slammed SolarWinds sued by angry shareholders
Date: 2021-01-05T23:03:31+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: ah-right-on-time-hacker-slammed-solarwinds-sued-by-angry-shareholders

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/05/solarwinds_sued/){:target="_blank" rel="noopener"}

> Plus: US intelligence names and shames Russia as probable culprit SolarWinds – the network monitoring biz thoroughly hacked as part of a wider espionage operation – has been sued by its shareholders who claim bosses failed to tell them about its numerous security woes.... [...]
