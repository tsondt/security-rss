Title: Phishing scam had all the bells and whistles—except for one
Date: 2021-01-21T21:00:36+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;account credentials;phishing;privacy;scams
Slug: phishing-scam-had-all-the-bells-and-whistles-except-for-one

[Source](https://arstechnica.com/?p=1736622){:target="_blank" rel="noopener"}

> Enlarge / The query window for username and password on a webpage can be seen on the monitor of a laptop. (credit: Jens Büttner/picture alliance via Getty Images ) Criminals behind a recent phishing scam had assembled all the important pieces. Malware that bypassed antivirus—check. An email template that got around Microsoft Office 365 Advanced Threat Protection—check. A supply of email accounts with strong reputations from which to send scam mails—check. It was a recipe that allowed the scammers to steal more than 1,000 corporate employee credentials. There was just one problem: the scammers stashed their hard-won passwords on public [...]
