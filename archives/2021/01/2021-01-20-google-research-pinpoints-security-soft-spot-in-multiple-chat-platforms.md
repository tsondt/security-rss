Title: Google Research Pinpoints Security Soft Spot in Multiple Chat Platforms
Date: 2021-01-20T15:21:46+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy;Vulnerabilities
Slug: google-research-pinpoints-security-soft-spot-in-multiple-chat-platforms

[Source](https://threatpost.com/google-research-pinpoints-security-soft-spot-in-multiple-chat-platforms/163175/){:target="_blank" rel="noopener"}

> Mystery of spying using popular chat apps uncovered by Google Project Zero researcher. [...]
