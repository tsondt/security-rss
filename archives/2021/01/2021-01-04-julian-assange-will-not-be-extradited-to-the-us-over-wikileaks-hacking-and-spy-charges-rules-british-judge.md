Title: Julian Assange will NOT be extradited to the US over WikiLeaks hacking and spy charges, rules British judge
Date: 2021-01-04T12:43:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: julian-assange-will-not-be-extradited-to-the-us-over-wikileaks-hacking-and-spy-charges-rules-british-judge

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/04/julian_assange_extradition_judgment/){:target="_blank" rel="noopener"}

> But it's not over yet: Next step is Uncle Sam's appeal to London's High Court Accused hacker and WikiLeaks founder Julian Assange should not be extradited to the US to stand trial, Westminster Magistrates' Court has ruled.... [...]
