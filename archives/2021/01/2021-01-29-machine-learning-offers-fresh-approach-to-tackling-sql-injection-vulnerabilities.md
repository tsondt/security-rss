Title: Machine learning offers fresh approach to tackling SQL injection vulnerabilities
Date: 2021-01-29T11:43:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: machine-learning-offers-fresh-approach-to-tackling-sql-injection-vulnerabilities

[Source](https://portswigger.net/daily-swig/machine-learning-offers-fresh-approach-to-tackling-sql-injection-vulnerabilities){:target="_blank" rel="noopener"}

> Academics use reinforcement learning to automate the SQLi-exploitation process [...]
