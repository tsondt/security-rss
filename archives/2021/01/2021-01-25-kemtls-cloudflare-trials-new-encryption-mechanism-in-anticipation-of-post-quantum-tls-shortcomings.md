Title: KEMTLS: Cloudflare trials new encryption mechanism in anticipation of post-quantum TLS shortcomings
Date: 2021-01-25T12:58:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: kemtls-cloudflare-trials-new-encryption-mechanism-in-anticipation-of-post-quantum-tls-shortcomings

[Source](https://portswigger.net/daily-swig/kemtls-cloudflare-trials-new-encryption-mechanism-in-anticipation-of-post-quantum-tls-shortcomings){:target="_blank" rel="noopener"}

> Devised by a global team of academics, KEMTLS uses key encapsulation instead of signatures for authentication [...]
