Title: Intel: Hackers stole unpublished earnings info from corporate site
Date: 2021-01-22T12:47:33
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: intel-hackers-stole-unpublished-earnings-info-from-corporate-site

[Source](https://www.bleepingcomputer.com/news/security/intel-hackers-stole-unpublished-earnings-info-from-corporate-site/){:target="_blank" rel="noopener"}

> Intel disclosed on Thursday that unknown threat actors stole an infographic containing info on the company's fourth-quarter and full-year 2020 financial results. [...]
