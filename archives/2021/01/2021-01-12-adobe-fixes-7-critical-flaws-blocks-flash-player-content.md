Title: Adobe Fixes 7 Critical Flaws, Blocks Flash Player Content
Date: 2021-01-12T17:13:28+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: adobe-fixes-7-critical-flaws-blocks-flash-player-content

[Source](https://threatpost.com/adobe-critical-flaws-flash-player/162958/){:target="_blank" rel="noopener"}

> Adobe issued patches for seven critical arbitrary-code-execution flaws plaguing Windows and MacOS users. [...]
