Title: Attackers Steal E-Mails, Info from OpenWrt Forum
Date: 2021-01-19T14:45:27+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks
Slug: attackers-steal-e-mails-info-from-openwrt-forum

[Source](https://threatpost.com/attackers-e-mails-openwrt-forum/163136/){:target="_blank" rel="noopener"}

> Users of the Linux-based open-source firmware—which include developers from commercial router companies--may be targeted by phishing campaigns, administrators warn. [...]
