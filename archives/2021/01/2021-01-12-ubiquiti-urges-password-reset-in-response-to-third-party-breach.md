Title: Ubiquiti urges password reset in response to third-party breach
Date: 2021-01-12T15:45:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ubiquiti-urges-password-reset-in-response-to-third-party-breach

[Source](https://portswigger.net/daily-swig/ubiquiti-urges-password-reset-in-response-to-third-party-breach){:target="_blank" rel="noopener"}

> Cloud computing vendor advises customers to take precautionary measures [...]
