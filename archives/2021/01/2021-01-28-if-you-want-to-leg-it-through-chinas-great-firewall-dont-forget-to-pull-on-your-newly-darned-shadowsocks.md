Title: If you want to leg it through China's Great Firewall, don't forget to pull on your newly darned Shadowsocks
Date: 2021-01-28T02:22:20+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: if-you-want-to-leg-it-through-chinas-great-firewall-dont-forget-to-pull-on-your-newly-darned-shadowsocks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/28/great_firewall_circumvention_plans/){:target="_blank" rel="noopener"}

> Censorship-busting tool updated, advice on how to use it to evade blockades published China's recent upgrades to its content-blocking Great Firewall can be circumvented, according to censorship fighters from the Great Firewall Report.... [...]
