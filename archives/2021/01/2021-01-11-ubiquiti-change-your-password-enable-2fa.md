Title: Ubiquiti: Change Your Password, Enable 2FA
Date: 2021-01-11T21:33:26+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Ubiquiti breach
Slug: ubiquiti-change-your-password-enable-2fa

[Source](https://krebsonsecurity.com/2021/01/ubiquiti-change-your-password-enable-2fa/){:target="_blank" rel="noopener"}

> Ubiquiti, a major vendor of cloud-enabled Internet of Things (IoT) devices such as routers, network video recorders, security cameras and access control systems, is urging customers to change their passwords and enable multi-factor authentication. The company says an incident at a third-party cloud provider may have exposed customer account information and credentials used to remotely manage Ubiquiti gear. In an email sent to customers today, Ubiquiti Inc. [ NYSE: UI ] said it recently became aware of “unauthorized access to certain of our information technology systems hosted by a third party cloud provider,” although it declined to name that provider. [...]
