Title: Microsoft Implements Windows Zerologon Flaw ‘Enforcement Mode’
Date: 2021-01-15T21:47:20+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: microsoft-implements-windows-zerologon-flaw-enforcement-mode

[Source](https://threatpost.com/microsoft-implements-windows-zerologon-flaw-enforcement-mode/163104/){:target="_blank" rel="noopener"}

> Starting Feb. 9, Microsoft will enable Domain Controller “enforcement mode” by default to address CVE-2020-1472. [...]
