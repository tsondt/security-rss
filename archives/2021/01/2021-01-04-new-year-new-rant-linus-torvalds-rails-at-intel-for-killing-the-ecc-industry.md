Title: New year, new rant: Linus Torvalds rails at Intel for 'killing' the ECC industry
Date: 2021-01-04T14:00:13+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: new-year-new-rant-linus-torvalds-rails-at-intel-for-killing-the-ecc-industry

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/04/linus_torvalds_intel_killed_ecc/){:target="_blank" rel="noopener"}

> Why do most PCs not support error-correcting code? Chipzilla is to blame, says Linux don Linux creator Linus Torvalds has accused Intel of preventing widespread use of error-correcting memory and being "instrumental in killing the whole ECC industry with its horribly bad market segmentation."... [...]
