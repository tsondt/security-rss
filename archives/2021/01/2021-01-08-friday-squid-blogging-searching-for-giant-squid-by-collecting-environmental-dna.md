Title: Friday Squid Blogging: Searching for Giant Squid by Collecting Environmental DNA
Date: 2021-01-08T22:02:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-searching-for-giant-squid-by-collecting-environmental-dna

[Source](https://www.schneier.com/blog/archives/2021/01/friday-squid-blogging-searching-for-giant-squid-by-collecting-environmental-dna.html){:target="_blank" rel="noopener"}

> The idea is to collect and analyze random DNA floating around the ocean, and using that to figure out where the giant squid are. No one is sure if this will actually work. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
