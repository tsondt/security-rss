Title: Malicious Software Infrastructure Easier to Get and Deploy Than Ever
Date: 2021-01-08T21:31:52+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Web Security;advanced persistent threat;apt;C2;cobalt strike;command and control server;malware;Metasploit;Open Source;PupyRAT
Slug: malicious-software-infrastructure-easier-to-get-and-deploy-than-ever

[Source](https://threatpost.com/malicious-software-infrastructure-easier-deploy/162913/){:target="_blank" rel="noopener"}

> Researchers at Recorded Future report a rise in cracked Cobalt Strike and other open-source adversarial tools with easy-to-use interfaces. [...]
