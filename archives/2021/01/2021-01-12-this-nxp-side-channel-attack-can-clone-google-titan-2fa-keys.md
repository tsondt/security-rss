Title: This NXP side-channel attack can clone Google Titan 2FA keys
Date: 2021-01-12T13:28:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: this-nxp-side-channel-attack-can-clone-google-titan-2fa-keys

[Source](https://portswigger.net/daily-swig/this-nxp-side-channel-attack-can-clone-google-titan-2fa-keys){:target="_blank" rel="noopener"}

> However, Google says attack doesn’t materially undermine Titan’s remote access protection [...]
