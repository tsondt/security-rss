Title: Encrypted Client Hello: Upcoming Firefox 85 rollout builds momentum for ESNI successor
Date: 2021-01-08T17:07:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: encrypted-client-hello-upcoming-firefox-85-rollout-builds-momentum-for-esni-successor

[Source](https://portswigger.net/daily-swig/encrypted-client-hello-upcoming-firefox-85-rollout-builds-momentum-for-esni-successor){:target="_blank" rel="noopener"}

> ECH hopes to remedy the interoperability and deployment challenges of its predecessor [...]
