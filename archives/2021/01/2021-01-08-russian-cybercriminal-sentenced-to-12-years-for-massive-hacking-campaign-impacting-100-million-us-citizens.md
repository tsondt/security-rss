Title: Russian cybercriminal sentenced to 12 years for ‘massive hacking campaign’ impacting 100 million US citizens
Date: 2021-01-08T14:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: russian-cybercriminal-sentenced-to-12-years-for-massive-hacking-campaign-impacting-100-million-us-citizens

[Source](https://portswigger.net/daily-swig/russian-cybercriminal-sentenced-to-12-years-for-massive-hacking-campaign-impacting-100-million-us-citizens){:target="_blank" rel="noopener"}

> Andrei Tyurin played ‘major’ role in operation targeting JP Morgan Chase Bank, Wall Street Journal, and others [...]
