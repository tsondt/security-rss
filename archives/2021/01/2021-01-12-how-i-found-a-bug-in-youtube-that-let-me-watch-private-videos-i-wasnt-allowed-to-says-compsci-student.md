Title: How I found a bug in YouTube that let me watch private videos I wasn't allowed to, says compsci student
Date: 2021-01-12T05:55:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: how-i-found-a-bug-in-youtube-that-let-me-watch-private-videos-i-wasnt-allowed-to-says-compsci-student

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/12/youtube_video_vulnerability/){:target="_blank" rel="noopener"}

> Theft-by-a-thousand-cuts flaw fixed Until early last year, Google's YouTube had a security flaw that made private videos visible at reduced resolution, though not audible, to anyone who knew or guessed the video identifier and possessed the technical knowledge to take advantage of the snafu.... [...]
