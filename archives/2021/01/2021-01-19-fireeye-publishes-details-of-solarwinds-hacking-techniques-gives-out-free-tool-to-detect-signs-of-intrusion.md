Title: FireEye publishes details of SolarWinds hacking techniques, gives out free tool to detect signs of intrusion
Date: 2021-01-19T20:42:01+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: fireeye-publishes-details-of-solarwinds-hacking-techniques-gives-out-free-tool-to-detect-signs-of-intrusion

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/19/fireeye_solarwinds_code/){:target="_blank" rel="noopener"}

> Instructions for spotting and keeping suspected Russians out of systems Any organizations that used the backdoored SolarWinds network-monitoring software should take another look at their logs for signs of intrusion in light of new guidance and tooling.... [...]
