Title: Here's how a researcher broke into Microsoft VS Code's GitHub
Date: 2021-01-27T05:05:05-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Microsoft;Software
Slug: heres-how-a-researcher-broke-into-microsoft-vs-codes-github

[Source](https://www.bleepingcomputer.com/news/security/heres-how-a-researcher-broke-into-microsoft-vs-codes-github/){:target="_blank" rel="noopener"}

> This month a researcher was awarded a bug bounty award of an undisclosed amount after he broke into the official GitHub repository of Microsoft Visual Studio Code. A vulnerability in VS Code's issue management function and a lack of authentication checks enabled the researcher to obtain push access, and write to the repository. [...]
