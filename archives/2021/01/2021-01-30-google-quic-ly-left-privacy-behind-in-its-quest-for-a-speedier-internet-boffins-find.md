Title: Google QUIC-ly left privacy behind in its quest for a speedier internet, boffins find
Date: 2021-01-30T00:10:32+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: google-quic-ly-left-privacy-behind-in-its-quest-for-a-speedier-internet-boffins-find

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/30/quic_fingerprinting_flaw/){:target="_blank" rel="noopener"}

> Promising protocol much easier to fingerprint than HTTPS Google's QUIC (Quick UDP Internet Connections) protocol, announced in 2013 as a way to make the web faster, waited seven years before being implemented in the ad giant's Chrome browser. But it still arrived before privacy could get there.... [...]
