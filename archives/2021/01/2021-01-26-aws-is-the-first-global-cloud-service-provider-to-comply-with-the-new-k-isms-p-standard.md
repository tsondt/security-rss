Title: AWS is the first global cloud service provider to comply with the new K-ISMS-P standard
Date: 2021-01-26T00:24:40+00:00
Author: Seulun Sung
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;Auditing;Certification;Conformation pack;ISMS;K-ISMS;K-ISMS-P;KISA;Korea certification;Korea Internet and Security Agency;Korea-Information Security Management System Certification;Korean Ministry of Science and ICT;MSIT;Quickstart;Security Blog
Slug: aws-is-the-first-global-cloud-service-provider-to-comply-with-the-new-k-isms-p-standard

[Source](https://aws.amazon.com/blogs/security/aws-is-the-first-global-cloud-service-provider-to-comply-with-the-new-k-isms-p-standard/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has achieved certification under the Korea-Personal Information & Information Security Management System (K-ISMS-P) standard (effective from December 16, 2020 to December 15, 2023). The assessment by the Korea Internet & Security Agency (KISA) covered the operation of infrastructure (including compute, storage, networking, databases, and security) in [...]
