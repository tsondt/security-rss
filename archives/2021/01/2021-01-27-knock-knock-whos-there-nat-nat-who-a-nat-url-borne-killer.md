Title: Knock, knock. Who's there? NAT. Nat who? A NAT URL-borne killer
Date: 2021-01-27T20:26:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: knock-knock-whos-there-nat-nat-who-a-nat-url-borne-killer

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/27/nat_slipstream_bypass/){:target="_blank" rel="noopener"}

> Last year's slipstream technique revived to pierce vulnerable firewalls – browsers patched to thwart bypass attempts Video Ben Seri and Gregory Vishnepolsky, threat researchers at Armis, have found a way to expand upon the NAT Slipstream attack disclosed last year by Samy Kamkar, CSO of Openpath Security.... [...]
