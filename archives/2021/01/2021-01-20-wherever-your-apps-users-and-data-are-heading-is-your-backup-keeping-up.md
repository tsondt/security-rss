Title: Wherever your apps, users and data are heading, is your backup keeping up?
Date: 2021-01-20T17:00:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: wherever-your-apps-users-and-data-are-heading-is-your-backup-keeping-up

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/20/data_management_in_the_future/){:target="_blank" rel="noopener"}

> Let’s think about what data management should look like in the future – tune in here next week Webcast What’s your tech infrastructure going to look like in the next few months or years? You know, once we get past the current situation and find time to start innovating again. All of which may come sooner than you think.... [...]
