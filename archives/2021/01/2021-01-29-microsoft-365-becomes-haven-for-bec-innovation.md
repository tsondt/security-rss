Title: Microsoft 365 Becomes Haven for BEC Innovation
Date: 2021-01-29T21:54:24+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Web Security
Slug: microsoft-365-becomes-haven-for-bec-innovation

[Source](https://threatpost.com/microsoft-365-bec-innovation/163508/){:target="_blank" rel="noopener"}

> Two new phishing tactics use the platform's automated responses to evade email filters. [...]
