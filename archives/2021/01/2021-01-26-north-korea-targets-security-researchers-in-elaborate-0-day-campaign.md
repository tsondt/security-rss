Title: North Korea Targets Security Researchers in Elaborate 0-Day Campaign
Date: 2021-01-26T14:49:03+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks;Vulnerabilities;Web Security
Slug: north-korea-targets-security-researchers-in-elaborate-0-day-campaign

[Source](https://threatpost.com/north-korea-security-researchers-0-day/163333/){:target="_blank" rel="noopener"}

> Hackers masquerade as security researchers to befriend analysts and eventually infect fully patched systems at multiple firms with a malicious backdoor. [...]
