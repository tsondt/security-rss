Title: Google Docs bug allowed cyber-spies to screenshot private documents
Date: 2021-01-05T11:50:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-docs-bug-allowed-cyber-spies-to-screenshot-private-documents

[Source](https://portswigger.net/daily-swig/google-docs-bug-allowed-cyber-spies-to-screenshot-private-documents){:target="_blank" rel="noopener"}

> ‘Send Feedback’ flaw earns security researcher $3k bug bounty payout [...]
