Title: Sophisticated Watering Hole Attack
Date: 2021-01-20T12:00:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberattack;exploits;Google;hacking;zero-day
Slug: sophisticated-watering-hole-attack

[Source](https://www.schneier.com/blog/archives/2021/01/sophisticated-watering-hole-attack.html){:target="_blank" rel="noopener"}

> Google’s Project Zero has exposed a sophisticated watering-hole attack targeting both Windows and Android: Some of the exploits were zero-days, meaning they targeted vulnerabilities that at the time were unknown to Google, Microsoft, and most outside researchers (both companies have since patched the security flaws). The hackers delivered the exploits through watering-hole attacks, which compromise sites frequented by the targets of interest and lace the sites with code that installs malware on visitors’ devices. The boobytrapped sites made use of two exploit servers, one for Windows users and the other for users of Android The use of zero-days and complex [...]
