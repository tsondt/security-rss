Title: Sophisticated Hacks Against Android, Windows Reveal Zero-Day Trove
Date: 2021-01-13T16:57:39+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: sophisticated-hacks-against-android-windows-reveal-zero-day-trove

[Source](https://threatpost.com/hacks-android-windows-zero-day/163007/){:target="_blank" rel="noopener"}

> Watering-hole attacks executed by ‘experts’ exploited Chrome, Windows and Android flaws and were carried out on two servers. [...]
