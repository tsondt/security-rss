Title: Facebook’s Mandatory Data-Sharing Rules for WhatsApp Spark Ire
Date: 2021-01-07T12:53:49+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Facebook;Privacy
Slug: facebooks-mandatory-data-sharing-rules-for-whatsapp-spark-ire

[Source](https://threatpost.com/facebooks-mandatory-data-sharing-whatsapp-ire/162828/){:target="_blank" rel="noopener"}

> The messaging platform will update its privacy platform on Feb. 8 to integrate further with its parent company, prompting users to cry foul over privacy issues. [...]
