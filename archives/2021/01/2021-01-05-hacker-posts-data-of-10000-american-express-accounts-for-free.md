Title: Hacker posts data of 10,000 American Express accounts for free
Date: 2021-01-05T09:05:51
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: hacker-posts-data-of-10000-american-express-accounts-for-free

[Source](https://www.bleepingcomputer.com/news/security/hacker-posts-data-of-10-000-american-express-accounts-for-free/){:target="_blank" rel="noopener"}

> A threat actor has posted data of 10,000 American Express credit card holders on a hacker forum for free. In the same forum post, the actor is also claiming to sell more data of Mexican banking customers of American Express, Santander, and Banamex. [...]
