Title: Internet industry group i2Coalition throws weight behind illegal VPN crackdown
Date: 2021-01-22T14:21:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: internet-industry-group-i2coalition-throws-weight-behind-illegal-vpn-crackdown

[Source](https://portswigger.net/daily-swig/internet-industry-group-i2coalition-throws-weight-behind-illegal-vpn-crackdown){:target="_blank" rel="noopener"}

> Advocacy group supports US authorities in deterring cybercrime [...]
