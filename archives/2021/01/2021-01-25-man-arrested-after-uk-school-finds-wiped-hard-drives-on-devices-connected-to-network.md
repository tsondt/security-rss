Title: Man arrested after UK school finds wiped hard drives on devices connected to network
Date: 2021-01-25T13:20:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: man-arrested-after-uk-school-finds-wiped-hard-drives-on-devices-connected-to-network

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/25/leicestershire_school_computer_misuse/){:target="_blank" rel="noopener"}

> Police pull out classic 'sophisticated cyber attack' line A 28-year-old has been arrested after allegedly carrying out what police have labelled a "sophisticated cyber attack" on a school.... [...]
