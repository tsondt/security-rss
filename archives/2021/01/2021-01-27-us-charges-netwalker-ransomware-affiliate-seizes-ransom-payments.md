Title: US charges NetWalker ransomware affiliate, seizes ransom payments
Date: 2021-01-27T15:32:56-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: us-charges-netwalker-ransomware-affiliate-seizes-ransom-payments

[Source](https://www.bleepingcomputer.com/news/security/us-charges-netwalker-ransomware-affiliate-seizes-ransom-payments/){:target="_blank" rel="noopener"}

> The U.S. Justice Department announced today the disruption of the Netwalker ransomware operation and the indictment of a Canadian national for alleged involvement in the file-encrypting extortion attacks. [...]
