Title: How we’re helping to reshape the software supply chain ecosystem securely
Date: 2021-01-15T17:00:00+00:00
Author: Heather Adkins
Category: GCP Security
Tags: Google Cloud Platform;Inside Google Cloud;Identity & Security
Slug: how-were-helping-to-reshape-the-software-supply-chain-ecosystem-securely

[Source](https://cloud.google.com/blog/products/identity-security/how-were-helping-reshape-software-supply-chain-ecosystem-securely/){:target="_blank" rel="noopener"}

> As we start the new year, we see ongoing revelations about an attack involving SolarWinds and others, that in turn led to the compromise of numerous other organizations. Software supply chain attacks like this pose a serious threat to governments, companies, non-profits, and individuals alike. At Google, we work around the clock to protect our users and customers. Based on what is known about the attack today, we are confident that no Google systems were affected by the SolarWinds event. We make very limited use of the affected software and services, and our approach to mitigating supply chain security risks [...]
