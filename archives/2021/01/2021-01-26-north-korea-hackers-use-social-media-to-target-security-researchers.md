Title: North Korea hackers use social media to target security researchers
Date: 2021-01-26T16:04:59+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;hackers;Identity theft;malware;North Korea
Slug: north-korea-hackers-use-social-media-to-target-security-researchers

[Source](https://arstechnica.com/?p=1737393){:target="_blank" rel="noopener"}

> Enlarge (credit: Dmitry Nogaev | Getty Images) Google has warned it has uncovered an “ongoing” state-backed hacking campaign run by North Korea targeting cyber security researchers. The Silicon Valley group said its threat analysis team found that cyber attackers posing as researchers had created numerous fake social media profiles on platforms such as Twitter and LinkedIn. To gain credibility, they also had set up a fake blog for which they would get unwitting targets to write guest posts about actual software bugs. After establishing communication with an actual researcher, the attackers would ask the target to work together on cyber [...]
