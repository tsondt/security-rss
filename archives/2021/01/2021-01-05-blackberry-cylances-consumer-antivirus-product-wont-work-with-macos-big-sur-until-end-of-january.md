Title: Blackberry Cylance's consumer antivirus product won't work with macOS Big Sur until end of January
Date: 2021-01-05T21:25:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: blackberry-cylances-consumer-antivirus-product-wont-work-with-macos-big-sur-until-end-of-january

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/05/blackberry_cylance_antivirus_macos_big_sur/){:target="_blank" rel="noopener"}

> Only three months after latest OS version released Blackberry Cylance's consumer antivirus product will not support macOS Big Sur until the end of January – three months after the Apple operating system’s latest version was released.... [...]
