Title: Microsoft Edge goes homomorphic: Nobody will see your credentials... but you'll need to sign in to use it
Date: 2021-01-22T15:07:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: microsoft-edge-goes-homomorphic-nobody-will-see-your-credentials-but-youll-need-to-sign-in-to-use-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/22/edge_password_monitor/){:target="_blank" rel="noopener"}

> Has your password been pwned? MS browser will tell you Microsoft has detailed how the Password Monitor feature in Edge works after it pushed version 88 of the browser into the Stable channel.... [...]
