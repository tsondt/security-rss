Title: Feds Pinpoint Russia as ‘Likely’ Culprit Behind SolarWinds Attack
Date: 2021-01-06T15:05:32+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Government;Hacks;Malware;Vulnerabilities;Web Security;apt;China;cyber attack;Cybersecurity;Cybersecurity an Infrastructure Security Agency (CISA);Department of Homeland Security;espionage;FBI;federal agencies;National Security Agency;orion;President Trump;russia;solarwinds;state sponsored attack;supply chain;threat actors
Slug: feds-pinpoint-russia-as-likely-culprit-behind-solarwinds-attack

[Source](https://threatpost.com/feds-russia-culprit-solarwinds/162785/){:target="_blank" rel="noopener"}

> The widespread compromise affecting key government agencies is ongoing, according to the U.S. government. [...]
