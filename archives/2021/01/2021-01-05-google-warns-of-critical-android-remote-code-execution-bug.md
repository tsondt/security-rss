Title: Google Warns of Critical Android Remote Code Execution Bug
Date: 2021-01-05T20:21:40+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;Android;android framework;android security update;CVE-2021-0313;CVE-2021-0316;google;Kernel;MediaTek;Qualcomm;remote code execution;Samsung
Slug: google-warns-of-critical-android-remote-code-execution-bug

[Source](https://threatpost.com/google-warns-of-critical-android-remote-code-execution-bug/162756/){:target="_blank" rel="noopener"}

> Google's Android security update addressed 43 bugs overall affecting Android handsets, including Samsung phones. [...]
