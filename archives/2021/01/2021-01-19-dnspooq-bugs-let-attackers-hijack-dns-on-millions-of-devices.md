Title: DNSpooq bugs let attackers hijack DNS on millions of devices
Date: 2021-01-19T11:27:26
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: dnspooq-bugs-let-attackers-hijack-dns-on-millions-of-devices

[Source](https://www.bleepingcomputer.com/news/security/dnspooq-bugs-let-attackers-hijack-dns-on-millions-of-devices/){:target="_blank" rel="noopener"}

> Israel-based security consultancy firm JSOF disclosed today seven Dnsmasq vulnerabilities, collectively known as DNSpooq, that can be exploited to launch DNS cache poisoning, remote code execution, and denial-of-service attacks against millions of affected devices. [...]
