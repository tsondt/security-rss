Title: Decade-old bug in Linux world's sudo can be abused by any logged-in user to gain root privileges
Date: 2021-01-26T21:12:28+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: decade-old-bug-in-linux-worlds-sudo-can-be-abused-by-any-logged-in-user-to-gain-root-privileges

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/26/qualys_sudo_bug/){:target="_blank" rel="noopener"}

> Sudo, make me a heap overflow! Done, this system is now yours Security researchers from Qualys have identified a critical heap buffer overflow vulnerability in sudo that can be exploited by rogue users to take over the host system.... [...]
