Title: What happens when a Chrome extension with 2m+ users changes hands, raises red flags, doesn't document updates? Let's find out
Date: 2021-01-07T07:55:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: what-happens-when-a-chrome-extension-with-2m-users-changes-hands-raises-red-flags-doesnt-document-updates-lets-find-out

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/07/great_suspender_malware/){:target="_blank" rel="noopener"}

> Oh yes, I'm the Great Suspender, pretending I'm doing well Analysis Back in November, 2020, netizens warned that a Chrome extension called The Great Suspender may be malicious. Around that time, Google was made aware of these concerns and looked into the situation.... [...]
