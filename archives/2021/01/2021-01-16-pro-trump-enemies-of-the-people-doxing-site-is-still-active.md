Title: Pro-Trump 'Enemies of the People' doxing site is still active
Date: 2021-01-16T15:46:03
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: pro-trump-enemies-of-the-people-doxing-site-is-still-active

[Source](https://www.bleepingcomputer.com/news/security/pro-trump-enemies-of-the-people-doxing-site-is-still-active/){:target="_blank" rel="noopener"}

> Enemies of the People, the website inciting violence against U.S. officials who refused to support the President's claims to voter fraud, is still active and continues to expose personal details from more individuals. [...]
