Title: Stolen credit card shop Joker's Stash closes after making a fortune
Date: 2021-01-16T13:40:48
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: stolen-credit-card-shop-jokers-stash-closes-after-making-a-fortune

[Source](https://www.bleepingcomputer.com/news/security/stolen-credit-card-shop-jokers-stash-closes-after-making-a-fortune/){:target="_blank" rel="noopener"}

> The administrator of Joker's Stash, one of the longest-running marketplace for stolen credit cards, announced on Friday that they would permanently shut down the operation next month. [...]
