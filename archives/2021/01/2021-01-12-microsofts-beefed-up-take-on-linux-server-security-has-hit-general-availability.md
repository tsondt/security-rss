Title: Microsoft's beefed-up take on Linux server security has hit general availability
Date: 2021-01-12T13:00:05+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: microsofts-beefed-up-take-on-linux-server-security-has-hit-general-availability

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/12/microsoft_linux_edr/){:target="_blank" rel="noopener"}

> Endpoint Detection and Response added. For servers, not standalone Linux desktops, mind After a few months in preview, Microsoft has made Defender Endpoint Detection and Response (EDR) generally available for Linux servers.... [...]
