Title: Reserve Bank of New Zealand apologizes for ‘significant’ data breach
Date: 2021-01-15T12:28:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: reserve-bank-of-new-zealand-apologizes-for-significant-data-breach

[Source](https://portswigger.net/daily-swig/reserve-bank-of-new-zealand-apologizes-for-significant-data-breach){:target="_blank" rel="noopener"}

> Investigation continues into breach that resulted from attack against file-sharing service [...]
