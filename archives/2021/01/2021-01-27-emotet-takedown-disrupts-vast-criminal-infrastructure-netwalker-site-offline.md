Title: Emotet Takedown Disrupts Vast Criminal Infrastructure; NetWalker Site Offline
Date: 2021-01-27T18:04:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware
Slug: emotet-takedown-disrupts-vast-criminal-infrastructure-netwalker-site-offline

[Source](https://threatpost.com/emotet-takedown-infrastructure-netwalker-offline/163389/){:target="_blank" rel="noopener"}

> Hundreds of servers and 1 million Emotet infections have been dismantled globally, while authorities have taken NetWalker's Dark Web leaks site offline and charged a suspect. [...]
