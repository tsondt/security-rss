Title: Verified Twitter accounts hacked in $580k ‘Elon Musk’ crypto scam
Date: 2021-01-14T16:47:21
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency
Slug: verified-twitter-accounts-hacked-in-580k-elon-musk-crypto-scam

[Source](https://www.bleepingcomputer.com/news/security/verified-twitter-accounts-hacked-in-580k-elon-musk-crypto-scam/){:target="_blank" rel="noopener"}

> Threat actors are hacking verified Twitter accounts in an Elon Musk cryptocurrency giveaway scam that has recently become widely active. [...]
