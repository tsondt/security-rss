Title: National security concerns raised as Trump loyalists storm US Capitol
Date: 2021-01-07T16:06:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: national-security-concerns-raised-as-trump-loyalists-storm-us-capitol

[Source](https://portswigger.net/daily-swig/national-security-concerns-raised-as-trump-loyalists-storm-us-capitol){:target="_blank" rel="noopener"}

> ‘I don’t think I’d sleep well until the networks were rebuilt from scratch’ [...]
