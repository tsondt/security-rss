Title: Google Chrome fixes antivirus 'file locking' bug on Windows 10
Date: 2021-01-03T08:00:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Google;Microsoft
Slug: google-chrome-fixes-antivirus-file-locking-bug-on-windows-10

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-fixes-antivirus-file-locking-bug-on-windows-10/){:target="_blank" rel="noopener"}

> Google has fixed a Chromium bug to prevent antivirus programs running on Windows 10 from blocking new files and bookmarks. [...]
