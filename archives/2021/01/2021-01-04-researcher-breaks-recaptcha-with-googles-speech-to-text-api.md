Title: Researcher Breaks reCAPTCHA With Google’s Speech-to-Text API
Date: 2021-01-04T21:45:55+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cryptography;Web Security;artificial intelligence;bot;break;captcha;google;hack;Nikolai Tschacher;Proof of Concept;recaptcha;speech to text api;Turing test;unCaptcha
Slug: researcher-breaks-recaptcha-with-googles-speech-to-text-api

[Source](https://threatpost.com/researcher-breaks-recaptcha-speech-to-text-api/162734/){:target="_blank" rel="noopener"}

> Researcher uses an old unCAPTCHA trick against latest the audio version of reCAPTCHA, with a 97 percent success rate. [...]
