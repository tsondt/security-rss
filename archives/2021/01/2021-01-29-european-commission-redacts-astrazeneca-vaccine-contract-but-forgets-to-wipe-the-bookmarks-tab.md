Title: European Commission redacts AstraZeneca vaccine contract – but forgets to wipe the bookmarks tab
Date: 2021-01-29T17:09:01+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: european-commission-redacts-astrazeneca-vaccine-contract-but-forgets-to-wipe-the-bookmarks-tab

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/29/eu_commission_vaccine_contract_redaction_fail/){:target="_blank" rel="noopener"}

> Open that little box and bingo, clear text of the whole PDF Exclusive The European Commission's war of words against pharma company AstraZeneca over COVID-19 virus vaccines has descended into farce after Brussels accidentally published an unredacted version of a disputed supply contract.... [...]
