Title: Florida Ethics Officer Charged with Cyberstalking
Date: 2021-01-14T19:33:49+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Privacy;Web Security
Slug: florida-ethics-officer-charged-with-cyberstalking

[Source](https://threatpost.com/florida-ethics-officer-charged-cyberstalking/163074/){:target="_blank" rel="noopener"}

> Judge bars former Tallahassee city ethics officer from internet-connected devices after her arrest for cyberstalking. [...]
