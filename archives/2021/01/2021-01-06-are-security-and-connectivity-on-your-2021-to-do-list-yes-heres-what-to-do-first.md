Title: Are security and connectivity on your 2021 to do list, yes? Here’s what to do first
Date: 2021-01-06T18:00:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: are-security-and-connectivity-on-your-2021-to-do-list-yes-heres-what-to-do-first

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/06/security_and_connectivity_on_your/){:target="_blank" rel="noopener"}

> With Fortinet, we'll help you weigh up your SASE options Webcast If you looked back at your list of 2020 New Year’s resolutions, would any of them make still sense for 2021?... [...]
