Title: Call for feedback – Daily Swig reader survey
Date: 2021-01-27T12:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: call-for-feedback-daily-swig-reader-survey

[Source](https://portswigger.net/daily-swig/call-for-feedback-daily-swig-reader-survey){:target="_blank" rel="noopener"}

> Help us shape our content and direction as we look ahead to 2021 and beyond [...]
