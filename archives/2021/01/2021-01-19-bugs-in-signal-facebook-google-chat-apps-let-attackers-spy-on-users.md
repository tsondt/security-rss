Title: Bugs in Signal, Facebook, Google chat apps let attackers spy on users
Date: 2021-01-19T16:45:08
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Mobile
Slug: bugs-in-signal-facebook-google-chat-apps-let-attackers-spy-on-users

[Source](https://www.bleepingcomputer.com/news/security/bugs-in-signal-facebook-google-chat-apps-let-attackers-spy-on-users/){:target="_blank" rel="noopener"}

> Vulnerabilities found in multiple video conferencing mobile applications allowed attackers to listen to users' surroundings without permission before the person on the other end picked up the calls. [...]
