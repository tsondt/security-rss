Title: French cybersecurity non-profit Luatix strengthens its open source development capabilities with ANSSI partnership
Date: 2021-01-14T12:58:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: french-cybersecurity-non-profit-luatix-strengthens-its-open-source-development-capabilities-with-anssi-partnership

[Source](https://portswigger.net/daily-swig/french-cybersecurity-non-profit-luatix-strengthens-its-open-source-development-capabilities-with-anssi-partnership){:target="_blank" rel="noopener"}

> Freshly inked partnership follows the development of two open source security tools [...]
