Title: Lazarus Affiliate ‘ZINC’ Blamed for Campaign Against Security Researcher
Date: 2021-01-29T13:29:10+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: lazarus-affiliate-zinc-blamed-for-campaign-against-security-researcher

[Source](https://threatpost.com/lazarus-affiliate-zinc-blamed-for-campaign-against-security-researcher/163474/){:target="_blank" rel="noopener"}

> New details emerge of how North Korean-linked APT won trust of experts and exploited Visual Studio to infect systems with ‘Comebacker’ malware. [...]
