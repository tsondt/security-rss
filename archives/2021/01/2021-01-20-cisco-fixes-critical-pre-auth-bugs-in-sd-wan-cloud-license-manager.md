Title: Cisco fixes critical pre-auth bugs in SD-WAN, cloud license manager
Date: 2021-01-20T14:25:44
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisco-fixes-critical-pre-auth-bugs-in-sd-wan-cloud-license-manager

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-critical-pre-auth-bugs-in-sd-wan-cloud-license-manager/){:target="_blank" rel="noopener"}

> Cisco has released security updates to address pre-auth remote code execution (RCE) vulnerabilities affecting multiple SD-WAN products and the Cisco Smart Software Manager software. [...]
