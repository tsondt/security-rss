Title: Hacker used ransomware to lock victims in their IoT chastity belt
Date: 2021-01-09T10:24:45
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hacker-used-ransomware-to-lock-victims-in-their-iot-chastity-belt

[Source](https://www.bleepingcomputer.com/news/security/hacker-used-ransomware-to-lock-victims-in-their-iot-chastity-belt/){:target="_blank" rel="noopener"}

> The source code for the ChastityLock ransomware that targeted male users of a specific adult toy is now publicly available for research purposes. [...]
