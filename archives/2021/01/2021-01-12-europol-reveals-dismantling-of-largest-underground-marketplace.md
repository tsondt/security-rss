Title: Europol Reveals Dismantling of ‘Largest’ Underground Marketplace
Date: 2021-01-12T16:26:58+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;Web Security
Slug: europol-reveals-dismantling-of-largest-underground-marketplace

[Source](https://threatpost.com/europol-dismantling-underground-marketplace/162949/){:target="_blank" rel="noopener"}

> Europol announced a wide-ranging investigation that led to the arrest of the alleged DarkMarket operator and the seizure of the marketplace's infrastructure, including more than 20 servers. [...]
