Title: MyFreeCams site hacked to steal info of 2 million paying users
Date: 2021-01-22T03:33:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: myfreecams-site-hacked-to-steal-info-of-2-million-paying-users

[Source](https://www.bleepingcomputer.com/news/security/myfreecams-site-hacked-to-steal-info-of-2-million-paying-users/){:target="_blank" rel="noopener"}

> A hacker is selling a database with login details for two million high-paying users of the MyFreeCams adult video streaming and chat service. [...]
