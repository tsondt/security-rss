Title: US spinal care practice among first to issue healthcare data breach warning in 2021
Date: 2021-01-20T12:41:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-spinal-care-practice-among-first-to-issue-healthcare-data-breach-warning-in-2021

[Source](https://portswigger.net/daily-swig/us-spinal-care-practice-among-first-to-issue-healthcare-data-breach-warning-in-2021){:target="_blank" rel="noopener"}

> Precision Spine Care said hackers attempted to siphon company funds after accessing employee email account [...]
