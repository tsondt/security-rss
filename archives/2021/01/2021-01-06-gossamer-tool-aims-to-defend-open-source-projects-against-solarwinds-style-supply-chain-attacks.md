Title: Gossamer tool aims to defend open source projects against SolarWinds-style supply chain attacks
Date: 2021-01-06T15:34:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: gossamer-tool-aims-to-defend-open-source-projects-against-solarwinds-style-supply-chain-attacks

[Source](https://portswigger.net/daily-swig/gossamer-tool-aims-to-defend-open-source-projects-against-solarwinds-style-supply-chain-attacks){:target="_blank" rel="noopener"}

> Efforts to secure WordPress and Composer tracked on new website [...]
