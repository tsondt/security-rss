Title: Data Breach at ‘Resident Evil’ Gaming Company Widens
Date: 2021-01-12T18:45:14+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Malware;Web Security
Slug: data-breach-at-resident-evil-gaming-company-widens

[Source](https://threatpost.com/data-breach-resident-evil-gaming/162977/){:target="_blank" rel="noopener"}

> Capcom, the game developer behind Resident Evil, Street Fighter and Dark Stalkers, now says its recent attack compromised the personal data of up to 400,000 gamers. [...]
