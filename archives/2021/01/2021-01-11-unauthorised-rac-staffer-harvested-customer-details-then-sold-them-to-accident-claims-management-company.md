Title: Unauthorised RAC staffer harvested customer details then sold them to accident claims management company
Date: 2021-01-11T14:45:05+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: unauthorised-rac-staffer-harvested-customer-details-then-sold-them-to-accident-claims-management-company

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/11/rac_staffer_unauthorised_computer_access/){:target="_blank" rel="noopener"}

> 8-month suspended sentence for conspiracy to secure unauthorised access to computer data An employee at emergency roadside rescue biz RAC has received an eight-month suspended prison sentence for unsanctioned access to computer systems that saw her sell customers' data to an accident claims management company.... [...]
