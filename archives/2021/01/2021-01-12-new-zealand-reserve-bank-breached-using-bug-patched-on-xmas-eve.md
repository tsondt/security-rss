Title: New Zealand Reserve Bank breached using bug patched on Xmas Eve
Date: 2021-01-12T12:28:43
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: new-zealand-reserve-bank-breached-using-bug-patched-on-xmas-eve

[Source](https://www.bleepingcomputer.com/news/security/new-zealand-reserve-bank-breached-using-bug-patched-on-xmas-eve/){:target="_blank" rel="noopener"}

> A recent data breach at the Reserve Bank of New Zealand, known as Te Pūtea Matua, was caused by attackers exploiting a critical vulnerability patched the same day. [...]
