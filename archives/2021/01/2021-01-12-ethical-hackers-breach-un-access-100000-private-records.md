Title: Ethical Hackers Breach U.N., Access 100,000 Private Records
Date: 2021-01-12T15:00:19+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Government;Hacks
Slug: ethical-hackers-breach-un-access-100000-private-records

[Source](https://threatpost.com/hackers-breach-un-access-records/162944/){:target="_blank" rel="noopener"}

> Researchers informed organization of a flaw that exposed GitHub credentials through the organization’s vulnerability disclosure program. [...]
