Title: Criminal, Domestic Violence Case Info Exposed in Cook County Leak
Date: 2021-01-26T17:24:00+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Privacy;Web Security
Slug: criminal-domestic-violence-case-info-exposed-in-cook-county-leak

[Source](https://threatpost.com/criminal-domestic-case-cook-county-leak23k-sensitive-court-records/163336/){:target="_blank" rel="noopener"}

> Cook County, Ill., home to Chicago, has left a database exposed since at least September that contained sensitive criminal and family-court records. [...]
