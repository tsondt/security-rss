Title: Bugs in Firefox, Chrome, Edge Allow Remote System Hijacking
Date: 2021-01-08T06:00:28+00:00
Author: Tom Spring
Category: Threatpost
Tags: Bug Bounty;Vulnerabilities;Web Security
Slug: bugs-in-firefox-chrome-edge-allow-remote-system-hijacking

[Source](https://threatpost.com/firefox-chrome-edge-bugs-system-hijacking/162873/){:target="_blank" rel="noopener"}

> Major browsers get an update to fix separate bugs that both allow for remote attacks, which could potentially allow hackers to takeover targeted devices. [...]
