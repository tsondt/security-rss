Title: SolarWinds Hires Chris Krebs, Alex Stamos in Wake of Hack
Date: 2021-01-08T17:19:09+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;Malware;Vulnerabilities;Alex Stamos;Chris Krebs;cyberattack;hack;solarwinds
Slug: solarwinds-hires-chris-krebs-alex-stamos-in-wake-of-hack

[Source](https://threatpost.com/solarwinds-chris-krebs-alex-stamos-hack/162889/){:target="_blank" rel="noopener"}

> Former CISA director Chris Krebs and former Facebook security exec Alex Stamos have teamed up to create a new consulting group - and have been hired by SolarWinds. [...]
