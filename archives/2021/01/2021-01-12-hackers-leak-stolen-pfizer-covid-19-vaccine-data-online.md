Title: Hackers leak stolen Pfizer COVID-19 vaccine data online
Date: 2021-01-12T11:46:53
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hackers-leak-stolen-pfizer-covid-19-vaccine-data-online

[Source](https://www.bleepingcomputer.com/news/security/hackers-leak-stolen-pfizer-covid-19-vaccine-data-online/){:target="_blank" rel="noopener"}

> The European Medicines Agency (EMA) today revealed that some of the Pfizer/BioNTech COVID-19 vaccine data stolen from its servers in December was leaked online. [...]
