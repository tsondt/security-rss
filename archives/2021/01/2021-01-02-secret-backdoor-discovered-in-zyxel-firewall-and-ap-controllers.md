Title: Secret backdoor discovered in Zyxel firewall and AP controllers
Date: 2021-01-02T13:46:11
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: secret-backdoor-discovered-in-zyxel-firewall-and-ap-controllers

[Source](https://www.bleepingcomputer.com/news/security/secret-backdoor-discovered-in-zyxel-firewall-and-ap-controllers/){:target="_blank" rel="noopener"}

> Over 100,000 Zyxel devices are potentially vulnerable to a secret backdoor caused by hardcoded credentials used to update firewall and AP controllers' firmware. [...]
