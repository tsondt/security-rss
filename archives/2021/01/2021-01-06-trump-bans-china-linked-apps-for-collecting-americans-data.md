Title: Trump bans China-linked apps for collecting Americans’ data
Date: 2021-01-06T09:44:47
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: trump-bans-china-linked-apps-for-collecting-americans-data

[Source](https://www.bleepingcomputer.com/news/security/trump-bans-china-linked-apps-for-collecting-americans-data/){:target="_blank" rel="noopener"}

> United States President Donald Trump has signed an executive order banning eight Chinese apps considered to be a threat to US national security, economy, and foreign policy. [...]
