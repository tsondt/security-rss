Title: Vodafone's ho. Mobile admits data breach, 2.5m users impacted
Date: 2021-01-05T18:37:39
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: vodafones-ho-mobile-admits-data-breach-25m-users-impacted

[Source](https://www.bleepingcomputer.com/news/security/vodafones-ho-mobile-admits-data-breach-25m-users-impacted/){:target="_blank" rel="noopener"}

> Vodafone Group's low-cost operator ho. Mobile announced that hackers stole part of its customer database thus obtaining personal user information and SIM technical data. [...]
