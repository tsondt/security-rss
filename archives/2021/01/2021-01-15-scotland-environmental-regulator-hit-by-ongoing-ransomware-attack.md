Title: Scotland environmental regulator hit by ‘ongoing’ ransomware attack
Date: 2021-01-15T11:22:56
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: scotland-environmental-regulator-hit-by-ongoing-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/scotland-environmental-regulator-hit-by-ongoing-ransomware-attack/){:target="_blank" rel="noopener"}

> The Scottish Environment Protection Agency confirmed on Thursday that some of its contact center, internal systems, processes and internal communications were affected following a ransomware attack that took place on Christmas Eve. [...]
