Title: New whitepaper: Designing and deploying a data security strategy with Google Cloud
Date: 2021-01-22T17:00:00+00:00
Author: Matt Driscoll
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: new-whitepaper-designing-and-deploying-a-data-security-strategy-with-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/start-a-data-security-program-in-a-cloud-native-way-on-google-cloud/){:target="_blank" rel="noopener"}

> William Gibson said it best: “The future is already here—it’s just not evenly distributed.” The cloud has arrived. Data security in the cloud is too often a novel problem for our customers. Well-worn paths to security are lacking. We often see customers struggling to adapt their data security posture to this new reality. There is an understanding that data security is critical, but a lack of well understood principles to drive an effective data security program. Thus, we are excited to share a view of how to deploy a modern and effective data security program. Today, we are releasing a [...]
