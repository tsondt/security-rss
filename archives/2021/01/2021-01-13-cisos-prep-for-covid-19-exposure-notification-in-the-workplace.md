Title: CISOs Prep For COVID-19 Exposure Notification in the Workplace
Date: 2021-01-13T14:00:07+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Newsmaker Interviews;Podcasts;Privacy
Slug: cisos-prep-for-covid-19-exposure-notification-in-the-workplace

[Source](https://threatpost.com/cisos-prep-for-covid-19-exposure-notification-in-the-workplace/162988/){:target="_blank" rel="noopener"}

> Security teams are preparing for the inevitable return to the workplace - and the privacy implications of exposure notification apps that companies may need to adopt. [...]
