Title: Remote Attackers Can Now Reach Protected Network Devices via NAT Slipstreaming
Date: 2021-01-27T20:32:55+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Hacks;Vulnerabilities;Web Security
Slug: remote-attackers-can-now-reach-protected-network-devices-via-nat-slipstreaming

[Source](https://threatpost.com/remote-attackers-internal-network-devices-nat-slipstreaming/163400/){:target="_blank" rel="noopener"}

> A new version of NAT slipstreaming allows cybercriminals an easy path to devices that aren't connected to the internet. [...]
