Title: Intel issues earnings report early after sensitive data was ‘inadvertently made publicly accessible’
Date: 2021-01-25T14:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: intel-issues-earnings-report-early-after-sensitive-data-was-inadvertently-made-publicly-accessible

[Source](https://portswigger.net/daily-swig/intel-issues-earnings-report-early-after-sensitive-data-was-inadvertently-made-publicly-accessible){:target="_blank" rel="noopener"}

> Chipmaker initially thought its website had been hacked [...]
