Title: Aliens and UFOs: A Final Frontier for Social Engineers
Date: 2021-01-11T22:21:35+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: aliens-and-ufos-a-final-frontier-for-social-engineers

[Source](https://threatpost.com/aliens-ufos-frontier-social-engineers/162939/){:target="_blank" rel="noopener"}

> The release of a CIA archive on UFOs is exactly the kind of headline-making event that phishing and scam actors long for. [...]
