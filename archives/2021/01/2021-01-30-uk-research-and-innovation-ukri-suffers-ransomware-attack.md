Title: UK Research and Innovation (UKRI) suffers ransomware attack
Date: 2021-01-30T10:12:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: uk-research-and-innovation-ukri-suffers-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/uk-research-and-innovation-ukri-suffers-ransomware-attack/){:target="_blank" rel="noopener"}

> The UK Research and Innovation (UKRI) is dealing with a ransomware incident that encrypted data and impacted two of its services that offer information to subscribers and the platform for peer review of various parts of the agency. [...]
