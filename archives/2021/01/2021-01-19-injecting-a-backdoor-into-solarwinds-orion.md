Title: Injecting a Backdoor into SolarWinds Orion
Date: 2021-01-19T12:16:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;hacking;implants
Slug: injecting-a-backdoor-into-solarwinds-orion

[Source](https://www.schneier.com/blog/archives/2021/01/injecting-a-backdoor-into-solarwinds-orion.html){:target="_blank" rel="noopener"}

> Crowdstrike is reporting on a sophisticated piece of malware that was able to inject malware into the SolarWinds build process: Key Points SUNSPOT is StellarParticle’s malware used to insert the SUNBURST backdoor into software builds of the SolarWinds Orion IT management product. SUNSPOT monitors running processes for those involved in compilation of the Orion product and replaces one of the source files to include the SUNBURST backdoor code. Several safeguards were added to SUNSPOT to avoid the Orion builds from failing, potentially alerting developers to the adversary’s presence. Analysis of a SolarWinds software build server provided insights into how the [...]
