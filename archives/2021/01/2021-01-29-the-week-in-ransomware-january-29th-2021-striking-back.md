Title: The Week in Ransomware - January 29th 2021 - Striking back
Date: 2021-01-29T17:27:19-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-january-29th-2021-striking-back

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-29th-2021-striking-back/){:target="_blank" rel="noopener"}

> It has been a hectic week, with law enforcement conducting two successful law enforcement operations that will significantly impact ransomware. [...]
