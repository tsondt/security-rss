Title: Windows 10 bug crashes your PC when you access this location
Date: 2021-01-17T15:21:27
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: windows-10-bug-crashes-your-pc-when-you-access-this-location

[Source](https://www.bleepingcomputer.com/news/security/windows-10-bug-crashes-your-pc-when-you-access-this-location/){:target="_blank" rel="noopener"}

> A bug in Windows 10 causes the operating system to crash with a Blue Screen of Death simply by opening a certain path in a browser's address bar or using other Windows commands. [...]
