Title: Blind TCP/IP hijacking is resurrected for Windows 7
Date: 2021-01-28T11:39:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: blind-tcpip-hijacking-is-resurrected-for-windows-7

[Source](https://portswigger.net/daily-swig/blind-tcp-ip-hijacking-is-resurrected-for-windows-7){:target="_blank" rel="noopener"}

> Retro cyber-attack returns to haunt widely used, end-of-life operating system [...]
