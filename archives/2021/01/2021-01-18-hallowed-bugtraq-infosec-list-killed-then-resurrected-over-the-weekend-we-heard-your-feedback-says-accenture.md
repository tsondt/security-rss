Title: Hallowed Bugtraq infosec list killed then resurrected over the weekend: We heard your feedback, says Accenture
Date: 2021-01-18T07:05:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: hallowed-bugtraq-infosec-list-killed-then-resurrected-over-the-weekend-we-heard-your-feedback-says-accenture

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/18/security_in_brief/){:target="_blank" rel="noopener"}

> Plus: Watch out for NTFS-corrupting folder, Mimecast hack, and more In brief Last week ended with news that the venerable infosec mailing list Bugtraq was being shutdown at the end of the month.... [...]
