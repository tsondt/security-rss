Title: Substandard software costs US economy $2tn through security flaws, legacy systems, abandoned projects
Date: 2021-01-06T16:32:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: substandard-software-costs-us-economy-2tn-through-security-flaws-legacy-systems-abandoned-projects

[Source](https://portswigger.net/daily-swig/substandard-software-costs-us-economy-2tn-through-security-flaws-legacy-systems-abandoned-projects){:target="_blank" rel="noopener"}

> Tech consortium urges adoption of ‘DevQualOps’ model [...]
