Title: Ransomware gang taunts IObit with repeated forum hacks
Date: 2021-01-25T11:37:44-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-taunts-iobit-with-repeated-forum-hacks

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-taunts-iobit-with-repeated-forum-hacks/){:target="_blank" rel="noopener"}

> A ransomware gang continues to taunt Windows software developer IObit by hacking its forums to display a ransom demand. [...]
