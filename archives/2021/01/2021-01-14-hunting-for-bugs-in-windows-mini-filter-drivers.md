Title: Hunting for Bugs in Windows Mini-Filter Drivers
Date: 2021-01-14T09:04:00-08:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: hunting-for-bugs-in-windows-mini-filter-drivers

[Source](https://googleprojectzero.blogspot.com/2021/01/hunting-for-bugs-in-windows-mini-filter.html){:target="_blank" rel="noopener"}

> Posted by James Forshaw, Project Zero In December Microsoft fixed 4 issues in Windows in the Cloud Filter and Windows Overlay Filter (WOF) drivers ( CVE-2020-17103, CVE-2020-17134, CVE-2020-17136, CVE-2020-17139 ). These 4 issues were 3 local privilege escalations and a security feature bypass, and they were all present in Windows file system filter drivers. I’ve found a number of issues in filter drivers previously, including 6 in the LUAFV driver which implements UAC file virtualization. The purpose of a file system filter driver according to Microsoft is: “A file system filter driver can filter I/O operations for one or more [...]
