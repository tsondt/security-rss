Title: TikTok Takes Teen Accounts Private
Date: 2021-01-13T22:03:32+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Privacy;Web Security
Slug: tiktok-takes-teen-accounts-private

[Source](https://threatpost.com/tiktok-teen-accounts-private/163040/){:target="_blank" rel="noopener"}

> The company announced accounts for ages 13-15 will default to privacy setting, among other safety measures. [...]
