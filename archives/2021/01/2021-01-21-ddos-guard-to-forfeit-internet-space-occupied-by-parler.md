Title: DDoS-Guard To Forfeit Internet Space Occupied by Parler
Date: 2021-01-21T15:48:01+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;8chan;8kun;AFRINIC;ARIN;Cognitive Cloud LLP;DDoS-Guard Corp.;Evgeniy Marchenko;fbi;LACNIC;Parler;QAnon;Ron Guilmette
Slug: ddos-guard-to-forfeit-internet-space-occupied-by-parler

[Source](https://krebsonsecurity.com/2021/01/ddos-guard-to-forfeit-internet-space-occupied-by-parler/){:target="_blank" rel="noopener"}

> Parler, the beleaguered social network advertised as a “free speech” alternative to Facebook and Twitter, has had a tough month. Apple and Google removed the Parler app from their stores, and Amazon blocked the platform from using its hosting services. Parler has since found a home in DDoS-Guard, a Russian digital infrastructure company. But now it appears DDoS-Guard is about to be relieved of more than two-thirds of the Internet address space the company leases to clients — including the Internet addresses currently occupied by Parler. The pending disruption for DDoS-Guard and Parler comes compliments of Ron Guilmette, a researcher [...]
