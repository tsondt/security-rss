Title: Finding the Location of Telegram Users
Date: 2021-01-14T12:08:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Android;geolocation;spoofing;Telegram
Slug: finding-the-location-of-telegram-users

[Source](https://www.schneier.com/blog/archives/2021/01/finding-the-location-of-telegram-users.html){:target="_blank" rel="noopener"}

> Security researcher Ahmed Hassan has shown that spoofing the Android’s “People Nearby” feature allows him to pinpoint the physical location of Telegram users: Using readily available software and a rooted Android device, he’s able to spoof the location his device reports to Telegram servers. By using just three different locations and measuring the corresponding distance reported by People Nearby, he is able to pinpoint a user’s precise location. [...] A proof-of-concept video the researcher sent to Telegram showed how he could discern the address of a People Nearby user when he used a free GPS spoofing app to make his [...]
