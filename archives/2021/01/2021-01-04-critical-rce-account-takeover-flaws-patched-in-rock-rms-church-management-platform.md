Title: Critical RCE, account takeover flaws patched in Rock RMS church management platform
Date: 2021-01-04T16:24:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: critical-rce-account-takeover-flaws-patched-in-rock-rms-church-management-platform

[Source](https://portswigger.net/daily-swig/critical-rce-account-takeover-flaws-patched-in-rock-rms-church-management-platform){:target="_blank" rel="noopener"}

> Open source CRM software is used by at least 500 churches globally [...]
