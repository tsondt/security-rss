Title: Major Gaming Companies Hit with Ransomware Linked to APT27
Date: 2021-01-05T15:26:12+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Malware;apt;APT27;bronze union;COVID-19;Cyberattacks;DRBControl;dropbox;gaming companies;ransomware;Ransomware Attack;shell code;supply chain;videogames;Winnti
Slug: major-gaming-companies-hit-with-ransomware-linked-to-apt27

[Source](https://threatpost.com/ransomware-major-gaming-companies-apt27/162735/){:target="_blank" rel="noopener"}

> Researchers say a recent attack targeting videogaming developers has 'strong links' to the infamous APT27 threat group. [...]
