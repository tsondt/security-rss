Title: How good are you at scoring security vulnerabilities, really? Boffins seek infosec pros to take rating skill survey
Date: 2021-01-08T09:30:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: how-good-are-you-at-scoring-security-vulnerabilities-really-boffins-seek-infosec-pros-to-take-rating-skill-survey

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/08/cvss_scoring_survey/){:target="_blank" rel="noopener"}

> Real-world CVSS figures are a little variable, or so these folks reckon A German academic is running a study into the effectiveness of vulnerability scores – and is hoping the research will shed more light on the occasionally controversial system.... [...]
