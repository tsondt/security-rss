Title: Millions of Social Profiles Leaked by Chinese Data-Scrapers
Date: 2021-01-11T21:54:43+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Facebook;Privacy
Slug: millions-of-social-profiles-leaked-by-chinese-data-scrapers

[Source](https://threatpost.com/social-profiles-leaked-chinese-data-scrapers/162936/){:target="_blank" rel="noopener"}

> A cloud misconfig by SocialArks exposed 318 million records gleaned from Facebook, Instagram and LinkedIn. [...]
