Title: GitLab addresses numerous vulnerabilities in latest security release
Date: 2021-01-12T12:17:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: gitlab-addresses-numerous-vulnerabilities-in-latest-security-release

[Source](https://portswigger.net/daily-swig/gitlab-addresses-numerous-vulnerabilities-in-latest-security-release){:target="_blank" rel="noopener"}

> Details withheld on security release to offer software developers an update window [...]
