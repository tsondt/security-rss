Title: Babuk Locker is the first new enterprise ransomware of 2021
Date: 2021-01-05T14:31:55
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: babuk-locker-is-the-first-new-enterprise-ransomware-of-2021

[Source](https://www.bleepingcomputer.com/news/security/babuk-locker-is-the-first-new-enterprise-ransomware-of-2021/){:target="_blank" rel="noopener"}

> It's a new year, and with it comes a new ransomware called Babuk Locker that targets corporate victims in human-operated attacks. [...]
