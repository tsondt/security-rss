Title: Leading crane maker Palfinger hit in global cyberattack
Date: 2021-01-25T13:37:18-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: leading-crane-maker-palfinger-hit-in-global-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/leading-crane-maker-palfinger-hit-in-global-cyberattack/){:target="_blank" rel="noopener"}

> Leading crane and lifting manufacturer Palfinger is targeted in an ongoing cyberattack that has disrupted IT systems and business operations. [...]
