Title: SolarWinds: What Hit Us Could Hit Others
Date: 2021-01-12T20:50:50+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other;CrowdStrike;FireEye;Orion;SolarWinds breach;Sudhakar Ramakrishna;Sunburst malware;Sunspot malware;Teardrop malware
Slug: solarwinds-what-hit-us-could-hit-others

[Source](https://krebsonsecurity.com/2021/01/solarwinds-what-hit-us-could-hit-others/){:target="_blank" rel="noopener"}

> New research into the malware that set the stage for the megabreach at IT vendor SolarWinds shows the perpetrators spent months inside the company’s software development labs honing their attack before inserting malicious code into updates that SolarWinds then shipped to thousands of customers. More worrisome, the research suggests the insidious methods used by the intruders to subvert the company’s software development pipeline could be repurposed against many other major software providers. In a blog post published Jan. 11, SolarWinds said the attackers first compromised its development environment on Sept. 4, 2019. Soon after, the attackers began testing code designed [...]
