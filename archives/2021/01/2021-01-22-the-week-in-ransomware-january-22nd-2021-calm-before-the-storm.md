Title: The Week in Ransomware - January 22nd 2021 - Calm before the storm
Date: 2021-01-22T19:28:39
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-january-22nd-2021-calm-before-the-storm

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-22nd-2021-calm-before-the-storm/){:target="_blank" rel="noopener"}

> Ransomware news is slow this week, with mostly small ransomware variants being released and a small number of attacks reported. [...]
