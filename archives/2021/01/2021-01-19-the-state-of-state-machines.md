Title: The State of State Machines
Date: 2021-01-19T09:28:00.003000-08:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: the-state-of-state-machines

[Source](https://googleprojectzero.blogspot.com/2021/01/the-state-of-state-machines.html){:target="_blank" rel="noopener"}

> Posted by Natalie Silvanovich, Project Zero On January 29, 2019, a serious vulnerability was discovered in Group FaceTime which allowed an attacker to call a target and force the call to connect without user interaction from the target, allowing the attacker to listen to the target’s surroundings without their knowledge or consent. The bug was remarkable in both its impact and mechanism. The ability to force a target device to transmit audio to an attacker device without gaining code execution was an unusual and possibly unprecedented impact of a vulnerability. Moreover, the vulnerability was a logic bug in the FaceTime [...]
