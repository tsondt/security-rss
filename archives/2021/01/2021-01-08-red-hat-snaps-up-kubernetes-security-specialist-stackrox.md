Title: Red Hat snaps up Kubernetes security specialist StackRox
Date: 2021-01-08T16:30:04+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: red-hat-snaps-up-kubernetes-security-specialist-stackrox

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/08/redhat_stackrox/){:target="_blank" rel="noopener"}

> Onward to OpenShift IBM-owned Red Hat is to snaffle container security outfit StackRox and plans to fold the company's tech into its OpenShift platform.... [...]
