Title: North Korean software supply chain attack targets stock investors
Date: 2021-01-05T11:55:57
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: north-korean-software-supply-chain-attack-targets-stock-investors

[Source](https://www.bleepingcomputer.com/news/security/north-korean-software-supply-chain-attack-targets-stock-investors/){:target="_blank" rel="noopener"}

> North Korean hacking group Thallium aka APT37 has been targeting a private stock investment messenger service in a supply chain attack, as reported this week. [...]
