Title: New Linux SUDO flaw lets local users gain root privileges
Date: 2021-01-26T14:39:16-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Linux
Slug: new-linux-sudo-flaw-lets-local-users-gain-root-privileges

[Source](https://www.bleepingcomputer.com/news/security/new-linux-sudo-flaw-lets-local-users-gain-root-privileges/){:target="_blank" rel="noopener"}

> A now-fixed Sudo vulnerability allowed any local user to gain root privileges on Unix-like operating systems without requiring authentication. [...]
