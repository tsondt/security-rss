Title: Office January security updates fix remote code execution bugs
Date: 2021-01-14T09:32:57
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: office-january-security-updates-fix-remote-code-execution-bugs

[Source](https://www.bleepingcomputer.com/news/security/office-january-security-updates-fix-remote-code-execution-bugs/){:target="_blank" rel="noopener"}

> Microsoft addresses important severity remote code execution vulnerabilities affecting multiple Office products in the January 2021 Office security updates released during this month's Patch Tuesday. [...]
