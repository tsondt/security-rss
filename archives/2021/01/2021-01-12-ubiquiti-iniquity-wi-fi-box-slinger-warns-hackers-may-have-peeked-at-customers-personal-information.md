Title: Ubiquiti iniquity: Wi-Fi box slinger warns hackers may have peeked at customers' personal information
Date: 2021-01-12T02:42:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: ubiquiti-iniquity-wi-fi-box-slinger-warns-hackers-may-have-peeked-at-customers-personal-information

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/12/ubiquiti_data_leak/){:target="_blank" rel="noopener"}

> Salted password hashes, addresses, phone numbers may have been exposed in cloud security snafu Networking vendor Ubiquiti has written to its customers to advise them of a possible leak of their personal information.... [...]
