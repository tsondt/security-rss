Title: Mimecast Confirms SolarWinds Hack as List of Security Vendor Victims Snowball
Date: 2021-01-28T15:52:29+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Malware;Vulnerabilities;Web Security
Slug: mimecast-confirms-solarwinds-hack-as-list-of-security-vendor-victims-snowball

[Source](https://threatpost.com/mimecast-solarwinds-hack-security-vendor-victims/163431/){:target="_blank" rel="noopener"}

> A growing number of cybersecurity vendors like CrowdStrike, Fidelis, FireEye, Malwarebytes, Palo Alto Networks and Qualys are confirming being targeted in the espionage attack. [...]
