Title: Vulnerabilities in open source streaming platforms YouPHPTube and AVideo could lead to RCE
Date: 2021-01-29T13:44:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: vulnerabilities-in-open-source-streaming-platforms-youphptube-and-avideo-could-lead-to-rce

[Source](https://portswigger.net/daily-swig/vulnerabilities-in-open-source-streaming-platforms-youphptube-and-avideo-could-lead-to-rce){:target="_blank" rel="noopener"}

> SQL injection, XSS flaws among issues reported to developers [...]
