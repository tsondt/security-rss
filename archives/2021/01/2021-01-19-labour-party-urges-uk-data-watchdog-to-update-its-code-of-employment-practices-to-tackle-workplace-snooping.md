Title: Labour Party urges UK data watchdog to update its Code of Employment Practices to tackle workplace snooping
Date: 2021-01-19T14:21:50+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: labour-party-urges-uk-data-watchdog-to-update-its-code-of-employment-practices-to-tackle-workplace-snooping

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/19/workplace_snooping_labour_prospect_ico/){:target="_blank" rel="noopener"}

> Key doc hasn't been updated since 2018, warn politicos and trade union The UK's Information Commissioner's Office needs to update its Code of Employment Practices to tackle workplace spying by bosses, the Prospect trade union and the Labour Party have said.... [...]
