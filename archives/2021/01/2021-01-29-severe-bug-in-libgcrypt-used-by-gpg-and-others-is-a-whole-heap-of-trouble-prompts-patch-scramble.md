Title: Severe bug in Libgcrypt – used by GPG and others – is a whole heap of trouble, prompts patch scramble
Date: 2021-01-29T20:21:46+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: severe-bug-in-libgcrypt-used-by-gpg-and-others-is-a-whole-heap-of-trouble-prompts-patch-scramble

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/29/severe_libgcrypt_bug/){:target="_blank" rel="noopener"}

> Recently released cryptography code easily undone by trivial buffer overflow Google Project Zero researcher Tavis Ormandy on Thursday reported a severe flaw in Libgcrypt 1.9.0, an update to the widely used cryptographic library that was released ten days ago.... [...]
