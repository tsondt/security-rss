Title: T-Mobile Faces Yet Another Data Breach
Date: 2021-01-04T17:09:53+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Mobile Security;CPNI;customer account information;customer proprietary network information;cyberattack;data breach;mobile;phone numbers;t-mobile usa;Wireless
Slug: t-mobile-faces-yet-another-data-breach

[Source](https://threatpost.com/t-mobile-another-data-breach/162703/){:target="_blank" rel="noopener"}

> The cyberattack incident is the wireless carrier's fourth in three years. [...]
