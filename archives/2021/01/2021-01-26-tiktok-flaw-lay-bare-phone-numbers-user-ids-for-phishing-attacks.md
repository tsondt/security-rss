Title: TikTok Flaw Lay Bare Phone Numbers, User IDs For Phishing Attacks
Date: 2021-01-26T11:00:07+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Cloud Security;Mobile Security;Vulnerabilities
Slug: tiktok-flaw-lay-bare-phone-numbers-user-ids-for-phishing-attacks

[Source](https://threatpost.com/tiktok-flaw-phishing-attacks/163322/){:target="_blank" rel="noopener"}

> A security flaw in TikTok could have allowed attackers to query query the platform's database – potentially opening up for privacy violations. [...]
