Title: Ministry of Defence's cyber warfare drive is helping burn a hole through its budget, warns UK's National Audit Office
Date: 2021-01-14T14:56:39+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ministry-of-defences-cyber-warfare-drive-is-helping-burn-a-hole-through-its-budget-warns-uks-national-audit-office

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/14/nao_ministry_defence_budget_report_cyber/){:target="_blank" rel="noopener"}

> All that counter-China stuff costs a pretty penny, y'know The Ministry of Defence's multibillion budget overrun has been caused in part because of its spending splurge on flashy new "cyber" capabilities, according to the National Audit Office.... [...]
