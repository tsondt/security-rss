Title: Russia’s SolarWinds Attack and Software Security
Date: 2021-01-08T12:27:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberattack;cyberespionage;essays;hacking;national security policy;Russia
Slug: russias-solarwinds-attack-and-software-security

[Source](https://www.schneier.com/blog/archives/2021/01/russias-solarwinds-attack-and-software-security.html){:target="_blank" rel="noopener"}

> The information that is emerging about Russia’s extensive cyberintelligence operation against the United States and other countries should be increasingly alarming to the public. The magnitude of the hacking, now believed to have affected more than 250 federal agencies and businesses — ­primarily through a malicious update of the SolarWinds network management software — ­may have slipped under most people’s radar during the holiday season, but its implications are stunning. According to a Washington Post report, this is a massive intelligence coup by Russia’s foreign intelligence service (SVR). And a massive security failure on the part of the United States [...]
