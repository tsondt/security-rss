Title: Firefox 85 protects against supercookie tracking, removes Adobe Flash Player
Date: 2021-01-27T15:19:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: firefox-85-protects-against-supercookie-tracking-removes-adobe-flash-player

[Source](https://portswigger.net/daily-swig/firefox-85-protects-against-supercookie-tracking-removes-adobe-flash-player){:target="_blank" rel="noopener"}

> Updates to Mozilla browser include protections against hard-to-block cross-site trackers [...]
