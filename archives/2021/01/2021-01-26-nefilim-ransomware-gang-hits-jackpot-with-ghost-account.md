Title: Nefilim Ransomware Gang Hits Jackpot with Ghost Account
Date: 2021-01-26T17:15:33+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: nefilim-ransomware-gang-hits-jackpot-with-ghost-account

[Source](https://threatpost.com/nefilim-ransomware-ghost-account/163341/){:target="_blank" rel="noopener"}

> An unmonitored account belonging to a deceased employee allowed Nefilim to exfiltrate data and infiltrate systems for a month, without being noticed. [...]
