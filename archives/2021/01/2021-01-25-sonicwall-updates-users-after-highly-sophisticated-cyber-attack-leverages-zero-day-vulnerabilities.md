Title: SonicWall updates users after ‘highly sophisticated’ cyber-attack leverages zero-day vulnerabilities
Date: 2021-01-25T15:29:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: sonicwall-updates-users-after-highly-sophisticated-cyber-attack-leverages-zero-day-vulnerabilities

[Source](https://portswigger.net/daily-swig/sonicwall-updates-users-after-highly-sophisticated-cyber-attack-leverages-zero-day-vulnerabilities){:target="_blank" rel="noopener"}

> Network security vendor releases further details of ‘coordinated’ assault [...]
