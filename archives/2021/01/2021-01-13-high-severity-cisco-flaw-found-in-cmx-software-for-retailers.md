Title: High-Severity Cisco Flaw Found in CMX Software For Retailers
Date: 2021-01-13T21:22:01+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: high-severity-cisco-flaw-found-in-cmx-software-for-retailers

[Source](https://threatpost.com/cisco-flaw-cmx-software-retailers/163027/){:target="_blank" rel="noopener"}

> Cisco fixed high-severity flaws tied to 67 CVEs overall, including ones found inits AnyConnect Secure Mobility Client and in its RV110W, RV130, RV130W, and RV215W small business routers. [...]
