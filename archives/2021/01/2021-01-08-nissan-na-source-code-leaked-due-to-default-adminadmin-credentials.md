Title: Nissan NA source code leaked due to default admin:admin credentials
Date: 2021-01-08T03:36:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: nissan-na-source-code-leaked-due-to-default-adminadmin-credentials

[Source](https://www.bleepingcomputer.com/news/security/nissan-na-source-code-leaked-due-to-default-admin-admin-credentials/){:target="_blank" rel="noopener"}

> Multiple code repositories from Nissan North America became public this week after the company left an exposed Git server protected with default access credentials. [...]
