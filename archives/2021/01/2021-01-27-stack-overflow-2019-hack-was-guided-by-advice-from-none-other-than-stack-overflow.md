Title: Stack Overflow 2019 hack was guided by advice from none other than... Stack Overflow
Date: 2021-01-27T19:31:09+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: stack-overflow-2019-hack-was-guided-by-advice-from-none-other-than-stack-overflow

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/27/stack_overflow_2019_hack_was/){:target="_blank" rel="noopener"}

> Vulnerabilities in build systems, secrets in source code: developer environments are an attack target Developer site Stack Overflow has published details of a breach dating back to May 2019, finding evidence that an intruder in its systems made extensive use of Stack Overflow itself to determine how to make the next move.... [...]
