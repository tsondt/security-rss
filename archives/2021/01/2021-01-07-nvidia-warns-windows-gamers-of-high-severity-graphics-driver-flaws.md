Title: Nvidia Warns Windows Gamers of High-Severity Graphics Driver Flaws
Date: 2021-01-07T21:14:36+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;graphics drivers;Nvidia;Windows
Slug: nvidia-warns-windows-gamers-of-high-severity-graphics-driver-flaws

[Source](https://threatpost.com/nvidia-windows-gamers-graphics-driver-flaws/162857/){:target="_blank" rel="noopener"}

> In all, Nvidia patched flaws tied to 16 CVEs across its graphics drivers and vGPU software, in its first security update of 2021. [...]
