Title: NVIDIA Gamers Face DoS, Data Loss from Shield TV Bugs
Date: 2021-01-20T20:45:43+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: nvidia-gamers-face-dos-data-loss-from-shield-tv-bugs

[Source](https://threatpost.com/nvidia-gamers-dos-data-loss-shield-tv-bugs/163200/){:target="_blank" rel="noopener"}

> The company also issued patches for Tesla-based GPUs as part of an updated, separate security advisory. [...]
