Title: Command 'n' control botnet of notorious Emotet Windows ransomware shut down in multinational police raid
Date: 2021-01-27T17:13:30+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: command-n-control-botnet-of-notorious-emotet-windows-ransomware-shut-down-in-multinational-police-raid

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/27/emotet_botnet_taken_down_europol/){:target="_blank" rel="noopener"}

> Europol-led op knocks offline 700 servers used to infect 'millions of computers' EU police agency Europol has boasted of taking down the main botnet powering the Emotet trojan-cum-malware dropper, as part of a multinational police operation that included raids on the alleged operators’ homes in the Ukraine.... [...]
