Title: NSA advises companies to avoid third party DNS resolvers
Date: 2021-01-14T13:05:44
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: nsa-advises-companies-to-avoid-third-party-dns-resolvers

[Source](https://www.bleepingcomputer.com/news/security/nsa-advises-companies-to-avoid-third-party-dns-resolvers/){:target="_blank" rel="noopener"}

> The US National Security Agency (NSA) says that companies should avoid using third party DNS resolvers to block threat actors' DNS traffic eavesdropping and manipulation attempts and to block access to internal network information. [...]
