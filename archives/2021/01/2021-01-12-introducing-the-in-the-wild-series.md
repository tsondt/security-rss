Title: Introducing the In-the-Wild Series
Date: 2021-01-12T09:34:00.011000-08:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: introducing-the-in-the-wild-series

[Source](https://googleprojectzero.blogspot.com/2021/01/introducing-in-wild-series.html){:target="_blank" rel="noopener"}

> This is part 1 of a 6-part series detailing a set of vulnerabilities found by Project Zero being exploited in the wild. To read the other parts of the series, head to the bottom of this post. At Project Zero we often refer to our goal simply as “make 0-day hard”. Members of the team approach this challenge mainly through the lens of offensive security research. And while we experiment a lot with new targets and methodologies in order to remain at the forefront of the field, it is important that the team doesn’t stray too far from the current [...]
