Title: Ransomware Gang Collects Data from Blood Testing Lab
Date: 2021-01-04T23:23:13+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Malware;apex laboratory;blood testing;data exfiltration;data theft;double extortion;Healthcare;patient data;ransomware
Slug: ransomware-gang-collects-data-from-blood-testing-lab

[Source](https://threatpost.com/ransomware-gang-data-blood-testing-lab/162721/){:target="_blank" rel="noopener"}

> Apex Laboratory patient data was lifted and posted on a leak site. [...]
