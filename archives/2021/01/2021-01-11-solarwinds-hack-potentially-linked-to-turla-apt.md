Title: SolarWinds Hack Potentially Linked to Turla APT
Date: 2021-01-11T17:53:21+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Hacks;Malware;advanced persistent threat;apt;attribution;backdoor;cyberattack;malware;solarwinds;sunburst;supply chain;Turla
Slug: solarwinds-hack-potentially-linked-to-turla-apt

[Source](https://threatpost.com/solarwinds-hack-linked-turla-apt/162918/){:target="_blank" rel="noopener"}

> Researchers have spotted notable code overlap between the Sunburst backdoor and a known Turla weapon. [...]
