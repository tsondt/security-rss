Title: Ticketmaster Coughs Up $10 Million Fine After Hacking Rival Business
Date: 2021-01-04T15:26:15+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security;artist toolbox;Department of Justice;DOJ;FBI;former employee data retention;hack;illegal data access;insider threat;live nation;songkick;Stolen Credentials;stolen passwords;ticket company;ticketmaster
Slug: ticketmaster-coughs-up-10-million-fine-after-hacking-rival-business

[Source](https://threatpost.com/ticketmaster-10-million-fine-hacking-rival/162695/){:target="_blank" rel="noopener"}

> Several Ticketmaster executives conspired a hack against a rival concert presales firm, in attempt to 'choke off' its business. [...]
