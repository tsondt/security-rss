Title: Threatpost Poll: Weigh in on Ransomware Security
Date: 2021-01-07T18:34:40+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Malware;Vulnerabilities;Cyber Insurance;Cybersecurity;Healthcare;poll;ransomware
Slug: threatpost-poll-weigh-in-on-ransomware-security

[Source](https://threatpost.com/threatpost-poll-ransomware-security/162842/){:target="_blank" rel="noopener"}

> Provide your views on ransomware and how to deal with it in our anonymous Threatpost poll. [...]
