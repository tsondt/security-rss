Title: IObit forums hacked in widespread DeroHE ransomware attack
Date: 2021-01-18T14:57:18
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: iobit-forums-hacked-in-widespread-derohe-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/iobit-forums-hacked-in-widespread-derohe-ransomware-attack/){:target="_blank" rel="noopener"}

> Windows utility developer IObit was hacked over the weekend to perform a widespread attack to distribute the strange DeroHE ransomware to its forum members. [...]
