Title: APT Horoscope
Date: 2021-01-08T20:19:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: hacking;humor
Slug: apt-horoscope

[Source](https://www.schneier.com/blog/archives/2021/01/apt-horoscope.html){:target="_blank" rel="noopener"}

> This delightful essay matches APT hacker groups up with astrological signs. This is me: Capricorn is renowned for its discipline, skilled navigation, and steadfastness. Just like Capricorn, Helix Kitten (also known as APT 35 or OilRig) is a skilled navigator of vast online networks, maneuvering deftly across an array of organizations, including those in aerospace, energy, finance, government, hospitality, and telecommunications. Steadfast in its work and objectives, Helix Kitten has a consistent track record of developing meticulous spear-phishing attacks. [...]
