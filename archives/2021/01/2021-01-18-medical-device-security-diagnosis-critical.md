Title: Medical Device Security: Diagnosis Critical
Date: 2021-01-18T15:35:20+00:00
Author: Tom Spring
Category: Threatpost
Tags: IoT;Malware
Slug: medical-device-security-diagnosis-critical

[Source](https://threatpost.com/medical-device-security/163127/){:target="_blank" rel="noopener"}

> Medical-device security has long been a challenge, suffering the same uphill management battle that the entire sprawling mess of IoT gadgets has faced. [...]
