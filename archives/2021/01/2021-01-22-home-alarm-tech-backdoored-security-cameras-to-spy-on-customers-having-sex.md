Title: Home alarm tech backdoored security cameras to spy on customers having sex
Date: 2021-01-22T20:54:42+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;peeping tom;privacy;security cameras
Slug: home-alarm-tech-backdoored-security-cameras-to-spy-on-customers-having-sex

[Source](https://arstechnica.com/?p=1736874){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images / Aurich Lawson) A home security technician has admitted he repeatedly broke into cameras he installed and viewed customers engaging in sex and other intimate acts. Telesforo Aviles, a 35-year-old former employee of home and small office security company ADT, said that over a five-year period, he accessed the cameras of roughly 200 customer accounts on more than 9,600 occasions—all without the permission or knowledge of customers. He said he took note of homes with women he found attractive and then viewed their cameras for sexual gratification. He said he watched nude women and couples as [...]
