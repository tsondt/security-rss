Title: Outgoing FCC Chair Issues Final Security Salvo Against China
Date: 2021-01-25T21:16:11+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Mobile Security
Slug: outgoing-fcc-chair-issues-final-security-salvo-against-china

[Source](https://threatpost.com/outgoing-fcc-chair-security-china/163318/){:target="_blank" rel="noopener"}

> Ajit Pai says Chinese telecom companies ‘biggest national security threat’ for regulators in exit interview. [...]
