Title: USCellular hit by a data breach after hackers access CRM software
Date: 2021-01-28T18:41:34-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: uscellular-hit-by-a-data-breach-after-hackers-access-crm-software

[Source](https://www.bleepingcomputer.com/news/security/uscellular-hit-by-a-data-breach-after-hackers-access-crm-software/){:target="_blank" rel="noopener"}

> ​Mobile network operator USCellular suffered a data breach after hackers gained access to its CRM and viewed customers' accounts. [...]
