Title: Intel adds hardware-based ransomware detection to 11th gen CPUs
Date: 2021-01-11T21:43:46
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Hardware
Slug: intel-adds-hardware-based-ransomware-detection-to-11th-gen-cpus

[Source](https://www.bleepingcomputer.com/news/security/intel-adds-hardware-based-ransomware-detection-to-11th-gen-cpus/){:target="_blank" rel="noopener"}

> Intel announced today at CES 2021 that they have added hardware-based ransomware detection to their newly announced 11th generation Core vPro business-class processors. [...]
