Title: Biden said to be assembling cyber dream team to sort out US govt computer security
Date: 2021-01-25T22:00:36+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: biden-said-to-be-assembling-cyber-dream-team-to-sort-out-us-govt-computer-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/25/biden_cybersecurity_team/){:target="_blank" rel="noopener"}

> With a little $10bn package proposed to help them on their way President Biden is preparing to assemble a crack US government cybersecurity team, and has pledged $10bn in funding to shore up the defenses of Uncle Sam's computer networks.... [...]
