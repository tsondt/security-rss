Title: Emotet botnet disrupted after global takedown operation
Date: 2021-01-27T07:57:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: emotet-botnet-disrupted-after-global-takedown-operation

[Source](https://www.bleepingcomputer.com/news/security/emotet-botnet-disrupted-after-global-takedown-operation/){:target="_blank" rel="noopener"}

> The infrastructure of today's most dangerous botnet built by cybercriminals using the Emotet malware was taken down following an international coordinated action coordinated by Europol and Eurojust. [...]
