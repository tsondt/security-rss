Title: Kaspersky Lab autopsies evidence on SolarWinds hack
Date: 2021-01-12T06:56:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: kaspersky-lab-autopsies-evidence-on-solarwinds-hack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/12/solarwinds_russia_kaspersky/){:target="_blank" rel="noopener"}

> In a brave move, Russian firm fingers its own govt as one possible source of cyber badness Kaspersky Lab reckons the SolarWinds hackers may have hailed from the Turla malware group, itself linked to Russia’s FSB security service.... [...]
