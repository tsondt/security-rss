Title: Magecart attacks in 2021: Cat-and-mouse game continues between cybercrooks, researchers, law enforcement
Date: 2021-01-20T14:26:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: magecart-attacks-in-2021-cat-and-mouse-game-continues-between-cybercrooks-researchers-law-enforcement

[Source](https://portswigger.net/daily-swig/magecart-attacks-in-2021-cat-and-mouse-game-continues-between-cybercrooks-researchers-law-enforcement){:target="_blank" rel="noopener"}

> Under-resourced webstores urgently need our help, say security researchers [...]
