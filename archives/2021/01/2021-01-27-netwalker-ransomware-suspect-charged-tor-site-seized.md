Title: NetWalker Ransomware Suspect Charged: Tor Site Seized
Date: 2021-01-27T21:08:48+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware
Slug: netwalker-ransomware-suspect-charged-tor-site-seized

[Source](https://threatpost.com/netwalker-ransomware-suspect-charged/163405/){:target="_blank" rel="noopener"}

> The suspect allegedly has extorted $27.6 million from ransomware victims, mostly in the healthcare sector. [...]
