Title: BumbleBee Opens Exchange Servers in xHunt Spy Campaign
Date: 2021-01-12T18:30:47+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: bumblebee-opens-exchange-servers-in-xhunt-spy-campaign

[Source](https://threatpost.com/bumblebee-exchange-servers-xhunt-spy/162973/){:target="_blank" rel="noopener"}

> The BumbleBee web shell allows APT attackers to upload and download files, and move laterally by running commands. [...]
