Title: Microsoft Edge, Google Chrome Roll Out Password Protection Tools
Date: 2021-01-22T21:57:10+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Web Security
Slug: microsoft-edge-google-chrome-roll-out-password-protection-tools

[Source](https://threatpost.com/microsoft-edge-google-chrome-roll-out-password-protection-tools/163272/){:target="_blank" rel="noopener"}

> The new tools on Chrome and Edge will make it easier for browser users to discover - and change - compromised passwords. [...]
