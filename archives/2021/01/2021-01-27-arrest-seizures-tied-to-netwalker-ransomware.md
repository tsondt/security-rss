Title: Arrest, Seizures Tied to Netwalker Ransomware
Date: 2021-01-27T22:42:24+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;Chainalysis;NetWalker;Sebastien Vachon-Desjardins;U.S. Justice Department
Slug: arrest-seizures-tied-to-netwalker-ransomware

[Source](https://krebsonsecurity.com/2021/01/arrest-seizures-tied-to-netwalker-ransomware/){:target="_blank" rel="noopener"}

> U.S. and Bulgarian authorities this week seized the darkweb site used by the NetWalker ransomware cybercrime group to publish data stolen from its victims. In connection with the seizure, a Canadian national suspected of extorting more than $27 million through the spreading of NetWalker was charged in a Florida court. The victim shaming site maintained by the NetWalker ransomware group, after being seized by authorities this week. NetWalker is a ransomware-as-a-service crimeware product in which affiliates rent access to the continuously updated malware code in exchange for a percentage of any funds extorted from victims. The crooks behind NetWalker used [...]
