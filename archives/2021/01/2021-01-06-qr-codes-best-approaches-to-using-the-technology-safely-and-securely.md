Title: QR codes:&nbsp;Best approaches to using the technology safely and securely&nbsp;
Date: 2021-01-06T13:08:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: qr-codes-best-approaches-to-using-the-technology-safely-and-securely

[Source](https://portswigger.net/daily-swig/qr-codes-nbsp-best-approaches-to-using-the-technology-safely-and-securely-nbsp){:target="_blank" rel="noopener"}

> How to ensure your next scan doesn’t lead to a scam [...]
