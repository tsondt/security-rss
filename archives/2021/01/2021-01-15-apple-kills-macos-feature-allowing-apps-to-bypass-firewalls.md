Title: Apple Kills MacOS Feature Allowing Apps to Bypass Firewalls
Date: 2021-01-15T17:02:52+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: apple-kills-macos-feature-allowing-apps-to-bypass-firewalls

[Source](https://threatpost.com/apple-kills-macos-feature-allowing-apps-to-bypass-firewalls/163099/){:target="_blank" rel="noopener"}

> Security researchers lambasted the controversial macOS Big Sur feature for exposing users' sensitive data. [...]
