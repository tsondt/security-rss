Title: Upcoming Speaking Engagements
Date: 2021-01-14T17:42:31+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Schneier news
Slug: upcoming-speaking-engagements-5

[Source](https://www.schneier.com/blog/archives/2021/01/upcoming-speaking-engagements-5.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking (online) as part of Western Washington University’s Internet Studies Lecture Series on January 20, 2021. I’m speaking at ITY Denmark on February 2, 2021. Details to come. I’m being interviewed by Keith Cronin as part of The Center for Innovation, Security, and New Technology’s CSINT Conversations series, February 10, 2021 from 11:00 AM – 11:30 AM CST. I’ll be speaking at an Informa event on February 28, 2021. Details to come. The list is maintained on this page. [...]
