Title: Space Force joins US Intelligence Community to secure outer space
Date: 2021-01-09T08:00:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: space-force-joins-us-intelligence-community-to-secure-outer-space

[Source](https://www.bleepingcomputer.com/news/security/space-force-joins-us-intelligence-community-to-secure-outer-space/){:target="_blank" rel="noopener"}

> Director of National Intelligence John Ratcliffe announced that the US Space Force (USSF) is the ninth Department of Defense component to join the US Intelligence Community (IC). [...]
