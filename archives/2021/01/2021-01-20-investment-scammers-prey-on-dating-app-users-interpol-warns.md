Title: Investment Scammers Prey on Dating App Users, Interpol Warns
Date: 2021-01-20T16:42:58+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security
Slug: investment-scammers-prey-on-dating-app-users-interpol-warns

[Source](https://threatpost.com/investment-scammers-dating-app-interpol/163179/){:target="_blank" rel="noopener"}

> Users of dating apps - like Tinder, Match and Bumble - should be on the lookout for investment-fraud scammers. [...]
