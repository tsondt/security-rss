Title: KindleDrip: Critical vulnerabilities in Amazon Kindle e-reader gave attackers free rein over user accounts
Date: 2021-01-21T16:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: kindledrip-critical-vulnerabilities-in-amazon-kindle-e-reader-gave-attackers-free-rein-over-user-accounts

[Source](https://portswigger.net/daily-swig/kindledrip-critical-vulnerabilities-in-amazon-kindle-e-reader-gave-attackers-free-rein-over-user-accounts){:target="_blank" rel="noopener"}

> Researcher reveals how chained bugs resulted in complete e-reader takeover A researcher detailed how he chained a series of vulnerabilities to achieve remote code execution (RCE) on an Amazon Kindle e [...]
