Title: Feds Issue Recommendations for Maritime Cybersecurity
Date: 2021-01-06T20:29:39+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Vulnerabilities;cybersecurity challenges;federal recommendations;maritime security;seagoing sector;white house report
Slug: feds-issue-recommendations-for-maritime-cybersecurity

[Source](https://threatpost.com/feds-recommendations-maritime-cybersecurity/162804/){:target="_blank" rel="noopener"}

> Report outlines deep cybersecurity challenges for the public/private seagoing sector. [...]
