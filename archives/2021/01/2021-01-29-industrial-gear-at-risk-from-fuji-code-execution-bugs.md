Title: Industrial Gear at Risk from Fuji Code-Execution Bugs
Date: 2021-01-29T18:01:38+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Vulnerabilities
Slug: industrial-gear-at-risk-from-fuji-code-execution-bugs

[Source](https://threatpost.com/industrial-gear-fuji-code-execution-bugs/163490/){:target="_blank" rel="noopener"}

> Fuji Electric’s Tellus Lite V-Simulator and V-Server Lite can allow attackers to take advantage of operational technology (OT)-IT convergence on factory floors, at utility plants and more. [...]
