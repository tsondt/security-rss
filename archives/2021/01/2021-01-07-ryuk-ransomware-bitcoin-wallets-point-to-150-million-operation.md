Title: Ryuk ransomware Bitcoin wallets point to $150 million operation
Date: 2021-01-07T19:17:11
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: ryuk-ransomware-bitcoin-wallets-point-to-150-million-operation

[Source](https://www.bleepingcomputer.com/news/security/ryuk-ransomware-bitcoin-wallets-point-to-150-million-operation/){:target="_blank" rel="noopener"}

> Security researchers following the money circuit from Ryuk ransomware victims into the threat actor's pockets estimate that the criminal organization made at least $150 million. [...]
