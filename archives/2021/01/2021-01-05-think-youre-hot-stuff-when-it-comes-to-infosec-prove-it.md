Title: Think you’re hot stuff when it comes to infosec? Prove it
Date: 2021-01-05T07:00:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: think-youre-hot-stuff-when-it-comes-to-infosec-prove-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/05/update_your_giac_certs/){:target="_blank" rel="noopener"}

> This year’s New Year Resolution: Update your GIAC certs Promo You know you’re good in the trenches when it comes to cybersecurity. You might even have done a stack load of courses to ensure that you’ve filled in any gaps in your knowledge. Your colleagues know they can depend on you.... [...]
