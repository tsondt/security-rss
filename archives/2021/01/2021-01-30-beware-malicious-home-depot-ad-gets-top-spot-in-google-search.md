Title: Beware: Malicious Home Depot ad gets top spot in Google Search
Date: 2021-01-30T12:49:14-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: beware-malicious-home-depot-ad-gets-top-spot-in-google-search

[Source](https://www.bleepingcomputer.com/news/security/beware-malicious-home-depot-ad-gets-top-spot-in-google-search/){:target="_blank" rel="noopener"}

> A malicious Home Depot advertising campaign is redirect Google search visitors to tech support scams. [...]
