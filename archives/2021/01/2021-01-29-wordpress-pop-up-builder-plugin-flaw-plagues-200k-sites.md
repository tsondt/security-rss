Title: WordPress Pop-Up Builder Plugin Flaw Plagues 200K Sites
Date: 2021-01-29T21:56:50+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: wordpress-pop-up-builder-plugin-flaw-plagues-200k-sites

[Source](https://threatpost.com/wordpress-pop-up-builder-plugin-flaw-plagues-200k-sites/163500/){:target="_blank" rel="noopener"}

> The flaw could have let attackers send out custom newsletters and delete newsletter subscribers from 200,000 affected websites. [...]
