Title: Dassault Falcon Jet reports data breach after ransomware attack
Date: 2021-01-08T14:04:50
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: dassault-falcon-jet-reports-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/dassault-falcon-jet-reports-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Dassault Falcon Jet has disclosed a data breach that may have led to the exposure of personal information belonging to current and former employees, as well as their spouses and dependents. [...]
