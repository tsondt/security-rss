Title: It’s Not the Trump Sex Tape, It’s a RAT
Date: 2021-01-06T21:20:47+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;javascript Trump;malware;Phishing;phishing attack;QRAT;Quaverse;RAT;remote access Trojan;sex tape;Trojan;Trump
Slug: its-not-the-trump-sex-tape-its-a-rat

[Source](https://threatpost.com/trump-sex-tape-rat/162810/){:target="_blank" rel="noopener"}

> Criminals are using the end of the Trump presidency to deliver a new remote-access trojan (RAT) variant disguised as a sex video of the outgoing POTUS, researchers report. [...]
