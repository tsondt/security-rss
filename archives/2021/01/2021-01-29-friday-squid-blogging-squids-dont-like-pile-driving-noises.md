Title: Friday Squid Blogging: Squids Don’t Like Pile-Driving Noises
Date: 2021-01-29T22:06:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;squid
Slug: friday-squid-blogging-squids-dont-like-pile-driving-noises

[Source](https://www.schneier.com/blog/archives/2021/01/friday-squid-blogging-squids-dont-like-pile-driving-noises.html){:target="_blank" rel="noopener"}

> New research : Pile driving occurs during construction of marine platforms, including offshore windfarms, producing intense sounds that can adversely affect marine animals. We quantified how a commercially and economically important squid ( Doryteuthis pealeii : Lesueur 1821) responded to pile driving sounds recorded from a windfarm installation within this species’ habitat. Fifteen-minute portions of these sounds were played to 16 individual squid. A subset of animals (n = 11) received a second exposure after a 24-h rest period. Body pattern changes, inking, jetting, and startle responses were observed and nearly all squid exhibited at least one response. These responses [...]
