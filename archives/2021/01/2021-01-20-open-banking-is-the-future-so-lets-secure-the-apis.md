Title: Open banking is the future, so let’s secure the APIs
Date: 2021-01-20T08:30:08+00:00
Author: Eira Hayward
Category: The Register
Tags: 
Slug: open-banking-is-the-future-so-lets-secure-the-apis

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/20/open_banking_is_the_future/){:target="_blank" rel="noopener"}

> Equinix levels the playing field for all fintechs Sponsored The future of banking is digital, of that there is no doubt. It may be at an early stage, but we can already see that future, as, all over the world, the banking community moves to embrace open banking.... [...]
