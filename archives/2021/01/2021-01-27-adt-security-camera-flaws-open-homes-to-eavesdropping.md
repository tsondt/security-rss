Title: ADT Security Camera Flaws Open Homes to Eavesdropping
Date: 2021-01-27T18:05:51+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: IoT;Privacy;Vulnerabilities
Slug: adt-security-camera-flaws-open-homes-to-eavesdropping

[Source](https://threatpost.com/adt-security-camera-flaw-opened-homes-stores-to-eavesdropping/163378/){:target="_blank" rel="noopener"}

> Researchers publicly disclosed flaws in ADT's LifeShield DIY HD Video Doorbell, which could have allowed local attackers to access credentials, video feeds and more. [...]
