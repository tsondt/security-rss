Title: Vovalex is likely the first ransomware written in D
Date: 2021-01-29T14:25:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: vovalex-is-likely-the-first-ransomware-written-in-d

[Source](https://www.bleepingcomputer.com/news/security/vovalex-is-likely-the-first-ransomware-written-in-d/){:target="_blank" rel="noopener"}

> A new ransomware called Vovalex is being distributed through fake pirated software that impersonates popular Windows utilities, such as CCleaner. [...]
