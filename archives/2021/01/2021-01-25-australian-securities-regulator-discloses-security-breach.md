Title: Australian securities regulator discloses security breach
Date: 2021-01-25T11:54:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: australian-securities-regulator-discloses-security-breach

[Source](https://www.bleepingcomputer.com/news/security/australian-securities-regulator-discloses-security-breach/){:target="_blank" rel="noopener"}

> The Australian Securities and Investments Commission (ASIC) has revealed that one of its servers has been accessed by an unknown threat actor following a security breach. [...]
