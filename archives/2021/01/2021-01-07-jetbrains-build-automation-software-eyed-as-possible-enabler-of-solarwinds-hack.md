Title: JetBrains' build automation software eyed as possible enabler of SolarWinds hack
Date: 2021-01-07T05:53:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: jetbrains-build-automation-software-eyed-as-possible-enabler-of-solarwinds-hack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/07/jetbrains_solarwinds_accusation/){:target="_blank" rel="noopener"}

> Maker of developer tools says it played no role in the attack, hasn't heard from investigators The SolarWinds security breach disclosed last month, which US authorities believe was of Russian origin and led to the compromise of at least 18,000 organizations, may have been enabled in part by software from JetBrains.... [...]
