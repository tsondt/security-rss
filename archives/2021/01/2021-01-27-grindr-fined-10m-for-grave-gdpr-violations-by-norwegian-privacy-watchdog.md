Title: Grindr fined $10m for ‘grave’ GDPR violations by Norwegian privacy watchdog
Date: 2021-01-27T13:19:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: grindr-fined-10m-for-grave-gdpr-violations-by-norwegian-privacy-watchdog

[Source](https://portswigger.net/daily-swig/grindr-fined-10m-for-grave-gdpr-violations-by-norwegian-privacy-watchdog){:target="_blank" rel="noopener"}

> LGBT social networking app reprimanded for ‘take-it-or-leave-it consents’ to sharing sensitive personal data [...]
