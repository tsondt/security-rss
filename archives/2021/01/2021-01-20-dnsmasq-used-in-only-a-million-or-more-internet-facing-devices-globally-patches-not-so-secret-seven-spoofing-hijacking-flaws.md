Title: Dnsmasq, used in only a million or more internet-facing devices globally, patches not-so-secret seven spoofing, hijacking flaws
Date: 2021-01-20T01:49:43+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: dnsmasq-used-in-only-a-million-or-more-internet-facing-devices-globally-patches-not-so-secret-seven-spoofing-hijacking-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2021/01/20/dns_cache_poisoning/){:target="_blank" rel="noopener"}

> Get your updates when you can for gear from scores of manufacturers Seven vulnerabilities have been found in a popular DNS caching proxy and DHCP server known as dnsmasq, raising the possibility of widespread online attacks on networking devices.... [...]
