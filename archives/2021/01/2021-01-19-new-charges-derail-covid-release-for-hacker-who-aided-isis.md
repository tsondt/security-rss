Title: New Charges Derail COVID Release for Hacker Who Aided ISIS
Date: 2021-01-19T18:39:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ardit Ferizi;Kosova Hacker's Security;Mahmud Abouhalima;Pentagon Crew;Shawn Bridges
Slug: new-charges-derail-covid-release-for-hacker-who-aided-isis

[Source](https://krebsonsecurity.com/2021/01/new-charges-derail-covid-release-for-hacker-who-aided-isis/){:target="_blank" rel="noopener"}

> A hacker serving a 20-year sentence for stealing personal data on 1,300 U.S. military and government employees and giving it to an Islamic State hacker group in 2015 has been charged once again with fraud and identity theft. The new charges have derailed plans to deport him under compassionate release because of the COVID-19 pandemic. Ardit Ferizi, a 25-year-old citizen of Kosovo, was slated to be sent home earlier this month after a federal judge signed an order commuting his sentence to time served. The release was granted in part due to Ferizi’s 2018 diagnosis of asthma, as well as [...]
