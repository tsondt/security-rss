Title: Data from August Breach of Amazon Partner Juspay Dumped Online
Date: 2021-01-05T13:51:38+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Privacy
Slug: data-from-august-breach-of-amazon-partner-juspay-dumped-online

[Source](https://threatpost.com/data-from-august-breach-of-amazon-partner-juspay-dumped-online/162740/){:target="_blank" rel="noopener"}

> Researcher discovered info of 35 million credit-card users from an attack on the Indian startup, which handles payments for numerous online marketplaces. [...]
