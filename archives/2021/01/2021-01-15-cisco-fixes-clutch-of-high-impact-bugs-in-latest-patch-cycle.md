Title: Cisco fixes clutch of high-impact bugs in latest patch cycle
Date: 2021-01-15T13:54:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cisco-fixes-clutch-of-high-impact-bugs-in-latest-patch-cycle

[Source](https://portswigger.net/daily-swig/cisco-fixes-clutch-of-high-impact-bugs-in-latest-patch-cycle){:target="_blank" rel="noopener"}

> Patch batch includes borderline critical flaw in WiFi location services tech [...]
