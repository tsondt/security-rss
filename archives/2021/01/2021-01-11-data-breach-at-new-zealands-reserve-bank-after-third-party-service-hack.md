Title: Data breach at New Zealand’s Reserve Bank after third-party service hack
Date: 2021-01-11T12:53:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-at-new-zealands-reserve-bank-after-third-party-service-hack

[Source](https://portswigger.net/daily-swig/data-breach-at-new-zealands-reserve-bank-after-third-party-service-hack){:target="_blank" rel="noopener"}

> Security incident at file transfer provider potentially leaks sensitive information [...]
