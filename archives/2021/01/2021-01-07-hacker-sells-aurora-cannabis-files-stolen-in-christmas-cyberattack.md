Title: Hacker sells Aurora Cannabis files stolen in Christmas cyberattack
Date: 2021-01-07T17:29:32
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hacker-sells-aurora-cannabis-files-stolen-in-christmas-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/hacker-sells-aurora-cannabis-files-stolen-in-christmas-cyberattack/){:target="_blank" rel="noopener"}

> ​A hacker is selling the data stolen from cannabis giant Aurora Cannabis after breaching their systems on Christmas day. [...]
