Title: T-Mobile data breach: ‘Malicious, unauthorized’ hack exposes customer call information
Date: 2021-01-04T13:25:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: t-mobile-data-breach-malicious-unauthorized-hack-exposes-customer-call-information

[Source](https://portswigger.net/daily-swig/t-mobile-data-breach-malicious-unauthorized-hack-exposes-customer-call-information){:target="_blank" rel="noopener"}

> Mobile giant suffers another security incident [...]
