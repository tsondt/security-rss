Title: CES 2021 Gadgets: Worst in Privacy and Security Awards
Date: 2021-01-15T22:04:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: IoT;Privacy;Vulnerabilities
Slug: ces-2021-gadgets-worst-in-privacy-and-security-awards

[Source](https://threatpost.com/tractors-pod-ice-cream-lipstick-ces-2021-worst/163117/){:target="_blank" rel="noopener"}

> Expert panel awards dubious honors to 2021 Consumer Electronics Show’s biggest flops, including security and privacy failures. [...]
