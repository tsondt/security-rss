Title: In-the-Wild Series: Chrome Infinity Bug
Date: 2021-01-12T09:36:00.005000-08:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: in-the-wild-series-chrome-infinity-bug

[Source](https://googleprojectzero.blogspot.com/2021/01/in-wild-series-chrome-infinity-bug.html){:target="_blank" rel="noopener"}

> This is part 2 of a 6-part series detailing a set of vulnerabilities found by Project Zero being exploited in the wild. To read the other parts of the series, see the introduction post. Posted by Sergei Glazunov, Project Zero This post only covers one of the exploits, specifically a renderer exploit targeting Chrome 73-78 on Android. We use it as an opportunity to talk about an interesting vulnerability class in Chrome’s JavaScript engine. Brief introduction to typer bugs One of the features that make JavaScript code especially difficult to optimize is the dynamic type system. Even for a trivial [...]
