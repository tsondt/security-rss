Title: Facebook sues makers of malicious Chrome extensions for scraping data
Date: 2021-01-14T15:16:14
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: facebook-sues-makers-of-malicious-chrome-extensions-for-scraping-data

[Source](https://www.bleepingcomputer.com/news/security/facebook-sues-makers-of-malicious-chrome-extensions-for-scraping-data/){:target="_blank" rel="noopener"}

> Facebook has taken legal action against the makers of malicious Chrome extensions used for scraping user-profiles and other information from Facebook's website and from users' systems without authorization. [...]
