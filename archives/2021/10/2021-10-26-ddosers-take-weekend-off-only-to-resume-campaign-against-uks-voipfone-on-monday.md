Title: DDoSers take weekend off only to resume campaign against UK's Voipfone on Monday
Date: 2021-10-26T10:23:39+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-10-26-ddosers-take-weekend-off-only-to-resume-campaign-against-uks-voipfone-on-monday

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/26/voipfone_outage/){:target="_blank" rel="noopener"}

> Firm fingers 'overseas criminals' for sending internet phone business TITSUP* It never rains but it pours. Internet telephone service provider Voipfone, currently battling a "major outage" across all voice services, has admitted to being hit by an "extortion-based DDoS attack from overseas criminals" that knocked it offline last week.... [...]
