Title: Microsoft is disabling Excel 4.0 macros by default to protect users
Date: 2021-10-07T18:32:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-07-microsoft-is-disabling-excel-40-macros-by-default-to-protect-users

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-is-disabling-excel-40-macros-by-default-to-protect-users/){:target="_blank" rel="noopener"}

> ​Microsoft will soon begin disabling Excel 4.0 XLM macros by default in Microsoft 365 tenants to protect customers from malicious documents. [...]
