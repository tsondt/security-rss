Title: Microsoft shares Windows 11 TPM check bypass for unsupported PCs
Date: 2021-10-06T06:27:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-06-microsoft-shares-windows-11-tpm-check-bypass-for-unsupported-pcs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-shares-windows-11-tpm-check-bypass-for-unsupported-pcs/){:target="_blank" rel="noopener"}

> Microsoft has published a new support webpage where they provide an official method to bypass the TPM 2.0 and CPU checks (TPM 1.2 is still required) and have Windows 11 installed on unsupported systems. [...]
