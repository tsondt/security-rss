Title: Malicious NPM libraries install ransomware, password stealer
Date: 2021-10-27T11:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-27-malicious-npm-libraries-install-ransomware-password-stealer

[Source](https://www.bleepingcomputer.com/news/security/malicious-npm-libraries-install-ransomware-password-stealer/){:target="_blank" rel="noopener"}

> Malicious NPM packages pretending to be Roblox libraries are delivering ransomware and password-stealing trojans on unsuspecting users. [...]
