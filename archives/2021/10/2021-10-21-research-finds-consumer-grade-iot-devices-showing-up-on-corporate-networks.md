Title: Research finds consumer-grade IoT devices showing up... on corporate networks
Date: 2021-10-21T09:27:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-21-research-finds-consumer-grade-iot-devices-showing-up-on-corporate-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/21/iot_devices_corporate_networks_security_warning/){:target="_blank" rel="noopener"}

> Considering the slack security of such kit, it's a perfect storm Increasing numbers of "non-business" Internet of Things devices are showing up inside corporate networks, Palo Alto Networks has warned, saying that smart lightbulbs and internet-connected pet feeders may not feature in organisations' threat models.... [...]
