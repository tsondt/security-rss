Title: Sir Tim Berners-Lee and the BBC stage a very British coup to rescue our data from Facebook and friends
Date: 2021-10-04T08:30:12+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2021-10-04-sir-tim-berners-lee-and-the-bbc-stage-a-very-british-coup-to-rescue-our-data-from-facebook-and-friends

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/04/column_data_privacy/){:target="_blank" rel="noopener"}

> Terms and conditions apply, and that’s a good thing Opinion Personal data is the oil of the internet. The great engines of Facebook and Google pump it relentlessly, burning it at will to power their marketing monetisation magic. The pollution it creates in broken privacy, shattered politics and the corrupting force of hidden agendas, is out of control.... [...]
