Title: Ukrainian police arrest DDoS operator controlling 100,000 bots
Date: 2021-10-11T09:10:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-11-ukrainian-police-arrest-ddos-operator-controlling-100000-bots

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-police-arrest-ddos-operator-controlling-100-000-bots/){:target="_blank" rel="noopener"}

> Ukrainian police have arrested a hacker who controlled a 100,000 device botnet used to perform DDoS attacks on behalf of paid customers. [...]
