Title: ‘Prolific’ ransomware operators arrested in Ukraine – Europol
Date: 2021-10-04T11:36:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-04-prolific-ransomware-operators-arrested-in-ukraine-europol

[Source](https://portswigger.net/daily-swig/prolific-ransomware-operators-arrested-in-ukraine-europol){:target="_blank" rel="noopener"}

> Assets also frozen over ‘string of targeted attacks’ against US and European targets [...]
