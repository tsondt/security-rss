Title: Ransomware operators behind hundreds of attacks arrested in Ukraine
Date: 2021-10-04T08:39:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2021-10-04-ransomware-operators-behind-hundreds-of-attacks-arrested-in-ukraine

[Source](https://www.bleepingcomputer.com/news/security/ransomware-operators-behind-hundreds-of-attacks-arrested-in-ukraine/){:target="_blank" rel="noopener"}

> Europol has announced the arrest of two men in Ukraine, said to be members of a prolific ransomware operation that extorted victims with ransom demands ranging between €5 to €70 million. [...]
