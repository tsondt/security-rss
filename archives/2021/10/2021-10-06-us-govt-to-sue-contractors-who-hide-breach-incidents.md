Title: U.S. govt to sue contractors who hide breach incidents
Date: 2021-10-06T20:01:40-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-06-us-govt-to-sue-contractors-who-hide-breach-incidents

[Source](https://www.bleepingcomputer.com/news/security/us-govt-to-sue-contractors-who-hide-breach-incidents/){:target="_blank" rel="noopener"}

> Under the new Civil Cyber-Fraud Initiative that the U.S. Department of Justice announced today, government contractors are accountable in a civil court if they don't report a breach or fail to meet required cybersecurity standards. [...]
