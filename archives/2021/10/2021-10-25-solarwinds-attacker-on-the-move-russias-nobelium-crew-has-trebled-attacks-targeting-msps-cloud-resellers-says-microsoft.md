Title: SolarWinds attacker on the move: Russia's Nobelium crew has trebled attacks targeting MSPs, cloud resellers, says Microsoft
Date: 2021-10-25T13:16:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-25-solarwinds-attacker-on-the-move-russias-nobelium-crew-has-trebled-attacks-targeting-msps-cloud-resellers-says-microsoft

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/25/nobelium_russia_svr_msp_warning_microsoft/){:target="_blank" rel="noopener"}

> Phishing and password spraying on the up Russia's Nobelium group – fingered as being a Russian state actor by both the United States and Britain – has massively ramped up phishing and password spraying attempts against managed service providers (MSPs) and cloud resellers, Microsoft's security arm has warned.... [...]
