Title: Android October patch fixes three critical bugs, 41 flaws in total
Date: 2021-10-05T08:38:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-10-05-android-october-patch-fixes-three-critical-bugs-41-flaws-in-total

[Source](https://www.bleepingcomputer.com/news/security/android-october-patch-fixes-three-critical-bugs-41-flaws-in-total/){:target="_blank" rel="noopener"}

> Google has released the Android October security updates, addressing 41 vulnerabilities, all ranging between high and critical severity. [...]
