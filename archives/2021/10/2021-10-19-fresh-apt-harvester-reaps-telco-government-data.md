Title: Fresh APT Harvester Reaps Telco, Government Data
Date: 2021-10-19T20:15:01+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Malware;Web Security
Slug: 2021-10-19-fresh-apt-harvester-reaps-telco-government-data

[Source](https://threatpost.com/apt-harvester-telco-government-data/175585/){:target="_blank" rel="noopener"}

> The group is likely nation-state-backed and is mounting an ongoing spy campaign using custom malware and stealthy tactics. [...]
