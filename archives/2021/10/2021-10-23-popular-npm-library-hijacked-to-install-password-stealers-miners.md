Title: Popular NPM library hijacked to install password-stealers, miners
Date: 2021-10-23T12:51:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-23-popular-npm-library-hijacked-to-install-password-stealers-miners

[Source](https://www.bleepingcomputer.com/news/security/popular-npm-library-hijacked-to-install-password-stealers-miners/){:target="_blank" rel="noopener"}

> Hackers hijacked the popular UA-Parser-JS NPM library, with millions of downloads a week, to infect Linux and Windows devices with cryptominers and password-stealing trojans in a supply-chain attack. [...]
