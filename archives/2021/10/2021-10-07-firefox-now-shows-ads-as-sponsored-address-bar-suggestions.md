Title: Firefox now shows ads as sponsored address bar suggestions
Date: 2021-10-07T10:15:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Software
Slug: 2021-10-07-firefox-now-shows-ads-as-sponsored-address-bar-suggestions

[Source](https://www.bleepingcomputer.com/news/security/firefox-now-shows-ads-as-sponsored-address-bar-suggestions/){:target="_blank" rel="noopener"}

> Mozilla is now showing ads in the form of sponsored Firefox contextual suggestions when U.S. users type in the URL address bar. [...]
