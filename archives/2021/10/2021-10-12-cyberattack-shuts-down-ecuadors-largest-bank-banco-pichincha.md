Title: Cyberattack shuts down Ecuador's largest bank, Banco Pichincha
Date: 2021-10-12T11:12:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-12-cyberattack-shuts-down-ecuadors-largest-bank-banco-pichincha

[Source](https://www.bleepingcomputer.com/news/security/cyberattack-shuts-down-ecuadors-largest-bank-banco-pichincha/){:target="_blank" rel="noopener"}

> Ecuador's largest private bank Banco Pichincha has suffered a cyberattack that disrupted operations and taken the ATM and online banking portal offline. [...]
