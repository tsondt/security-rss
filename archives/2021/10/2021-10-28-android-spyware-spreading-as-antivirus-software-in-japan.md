Title: Android spyware spreading as antivirus software in Japan
Date: 2021-10-28T12:31:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-10-28-android-spyware-spreading-as-antivirus-software-in-japan

[Source](https://www.bleepingcomputer.com/news/security/android-spyware-spreading-as-antivirus-software-in-japan/){:target="_blank" rel="noopener"}

> A new variant of the Android info-stealer called FakeCop has been spotted by Japanese security researchers, who warn that the distribution of the malicious APK is picking up pace. [...]
