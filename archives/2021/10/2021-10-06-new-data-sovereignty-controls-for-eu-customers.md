Title: New data sovereignty controls for EU customers
Date: 2021-10-06T16:00:00+00:00
Author: Joseph Valente
Category: GCP Security
Tags: Google Cloud in Europe;Identity & Security
Slug: 2021-10-06-new-data-sovereignty-controls-for-eu-customers

[Source](https://cloud.google.com/blog/products/identity-security/new-sovereign-controls-for-gcp-via-assured-workloads/){:target="_blank" rel="noopener"}

> European organizations—in both the public and private sectors—want a cloud provider that can meet their requirements for security, privacy, and digital sovereignty, without compromising on functionality or innovation. As part of our ‘Cloud. On Europe’s Terms.’ initiative, we’ve been working diligently to provide solutions to meet customers’ needs in this area, with capabilities built into our public cloud platform and, more recently, with our announcements to provide sovereign cloud solutions powered by Google Cloud to be offered through trusted partners like T-Systems in Germany and Thales in France. Today, we’re excited to announce the preview of another option for our [...]
