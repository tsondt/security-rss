Title: Google launches Android Enterprise bug bounty program
Date: 2021-10-21T12:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2021-10-21-google-launches-android-enterprise-bug-bounty-program

[Source](https://www.bleepingcomputer.com/news/security/google-launches-android-enterprise-bug-bounty-program/){:target="_blank" rel="noopener"}

> Google has announced the launch of its first vulnerability rewards program for Android Enterprise with bounties of up to $250,000. [...]
