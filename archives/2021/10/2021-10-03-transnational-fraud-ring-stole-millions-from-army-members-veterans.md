Title: Transnational fraud ring stole millions from Army members, veterans
Date: 2021-10-03T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-03-transnational-fraud-ring-stole-millions-from-army-members-veterans

[Source](https://www.bleepingcomputer.com/news/security/transnational-fraud-ring-stole-millions-from-army-members-veterans/){:target="_blank" rel="noopener"}

> Fredrick Brown, a former U.S. Army contrractor, was sentenced today to 151 months in prison after admitting to his role in a conspiracy that targeted thousands of U.S. servicemembers and veterans and caused millions of dollars in losses. [...]
