Title: OpenSea ‘Free Gift’ NFTs Drain Cryptowallet Balances
Date: 2021-10-13T13:04:37+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Cryptography;Vulnerabilities
Slug: 2021-10-13-opensea-free-gift-nfts-drain-cryptowallet-balances

[Source](https://threatpost.com/opensea-nfts-cryptowallet-balances/175453/){:target="_blank" rel="noopener"}

> Cybercriminals exploited bugs in the world's largest digital-goods marketplace to create malicious artwork offered as a perk to unsuspecting users. [...]
