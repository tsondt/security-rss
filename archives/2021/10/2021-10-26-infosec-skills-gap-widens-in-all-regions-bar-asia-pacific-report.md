Title: Infosec skills gap widens in all regions bar Asia-Pacific – report
Date: 2021-10-26T15:44:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-26-infosec-skills-gap-widens-in-all-regions-bar-asia-pacific-report

[Source](https://portswigger.net/daily-swig/infosec-skills-gap-widens-in-all-regions-bar-asia-pacific-report){:target="_blank" rel="noopener"}

> Overall worldwide shortfall shrinks 400k to 2.7m unfilled positions [...]
