Title: Russian orgs heavily targeted by smaller tier ransomware gangs
Date: 2021-10-08T10:40:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-08-russian-orgs-heavily-targeted-by-smaller-tier-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/russian-orgs-heavily-targeted-by-smaller-tier-ransomware-gangs/){:target="_blank" rel="noopener"}

> Even though American and European companies enjoy the lion's share in ransomware attacks launched from Russian ground, companies in the country aren't spared from having to deal with file encryption and double-extortion troubles. [...]
