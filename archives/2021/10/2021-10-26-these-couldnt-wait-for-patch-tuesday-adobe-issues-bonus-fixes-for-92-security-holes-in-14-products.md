Title: These couldn't wait for Patch Tuesday: Adobe issues bonus fixes for 92 security holes in 14 products
Date: 2021-10-26T19:57:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-10-26-these-couldnt-wait-for-patch-tuesday-adobe-issues-bonus-fixes-for-92-security-holes-in-14-products

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/26/adobe_october_extra_patches/){:target="_blank" rel="noopener"}

> It's 2021 and of course code with classic buffer overflows is still shipping A mere two weeks after its most recent set of security patches, Adobe has issued another 14 security bulletins covering 92 CVE-listed bugs.... [...]
