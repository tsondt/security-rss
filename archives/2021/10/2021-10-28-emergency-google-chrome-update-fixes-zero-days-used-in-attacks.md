Title: Emergency Google Chrome update fixes zero-days used in attacks
Date: 2021-10-28T18:11:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Security
Slug: 2021-10-28-emergency-google-chrome-update-fixes-zero-days-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/google/emergency-google-chrome-update-fixes-zero-days-used-in-attacks/){:target="_blank" rel="noopener"}

> Google has released Chrome 95.0.4638.69 for Windows, Mac, and Linux to fix two zero-day vulnerabilities that attackers have actively exploited. [...]
