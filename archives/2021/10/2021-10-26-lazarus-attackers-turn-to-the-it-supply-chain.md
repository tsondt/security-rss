Title: Lazarus Attackers Turn to the IT Supply Chain
Date: 2021-10-26T19:30:37+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Web Security
Slug: 2021-10-26-lazarus-attackers-turn-to-the-it-supply-chain

[Source](https://threatpost.com/lazarus-apt-it-supply-chain/175772/){:target="_blank" rel="noopener"}

> Kaspersky researchers saw The North Korean state APT use a new variant of the BlindingCan RAT to breach a Latvian IT vendor and then a South Korean think tank. [...]
