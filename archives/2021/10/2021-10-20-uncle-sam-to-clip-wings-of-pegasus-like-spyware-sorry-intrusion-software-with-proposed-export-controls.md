Title: Uncle Sam to clip wings of Pegasus-like spyware – sorry, 'intrusion software' – with proposed export controls
Date: 2021-10-20T22:09:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-10-20-uncle-sam-to-clip-wings-of-pegasus-like-spyware-sorry-intrusion-software-with-proposed-export-controls

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/20/us_intrusion_software_rules/){:target="_blank" rel="noopener"}

> Surveillance tech faces trade limits as America syncs policy with treaty obligations More than six years after proposing export restrictions on "intrusion software," the US Commerce Department's Bureau of Industry and Security (BIS) has formulated a rule that it believes balances the latitude required to investigate cyber threats with the need to limit dangerous code.... [...]
