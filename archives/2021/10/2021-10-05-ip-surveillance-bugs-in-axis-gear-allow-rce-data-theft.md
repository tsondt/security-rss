Title: IP Surveillance Bugs in Axis Gear Allow RCE, Data Theft
Date: 2021-10-05T21:09:10+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Vulnerabilities;Web Security
Slug: 2021-10-05-ip-surveillance-bugs-in-axis-gear-allow-rce-data-theft

[Source](https://threatpost.com/ip-surveillance-bugs-axis-rce-data-theft/175350/){:target="_blank" rel="noopener"}

> Three security vulnerabilities in Axis video products could open up the door to a bevy of different cyberattacks on businesses. [...]
