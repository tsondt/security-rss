Title: Internet Archive's 2046 Wayforward Machine says Google will cease to exist
Date: 2021-10-01T20:03:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-01-internet-archives-2046-wayforward-machine-says-google-will-cease-to-exist

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/01/internet_archive_wayforward_machine/){:target="_blank" rel="noopener"}

> Stop cheering, you're meant to think this is a bad thing The Internet Archive has launched a campaign against tech regulation by setting up a Wayforward Machine, semi-parodying its famous Wayback Machine archiving site.... [...]
