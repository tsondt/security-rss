Title: Money launderers for Russian hacking groups arrested in Ukraine
Date: 2021-10-26T10:02:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-26-money-launderers-for-russian-hacking-groups-arrested-in-ukraine

[Source](https://www.bleepingcomputer.com/news/security/money-launderers-for-russian-hacking-groups-arrested-in-ukraine/){:target="_blank" rel="noopener"}

> The Ukrainian cybercrime police force has arrested members of a group of money launderers and hackers at the request of U.S. intelligence services. [...]
