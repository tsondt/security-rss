Title: How a simple Linux kernel memory corruption bug can lead to complete system compromise
Date: 2021-10-19T09:08:00-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2021-10-19-how-a-simple-linux-kernel-memory-corruption-bug-can-lead-to-complete-system-compromise

[Source](https://googleprojectzero.blogspot.com/2021/10/how-simple-linux-kernel-memory.html){:target="_blank" rel="noopener"}

> An analysis of current and potential kernel security mitigations Posted by Jann Horn, Project Zero Introduction This blog post describes a straightforward Linux kernel locking bug and how I exploited it against Debian Buster's 4.19.0-13-amd64 kernel. Based on that, it explores options for security mitigations that could prevent or hinder exploitation of issues similar to this one. I hope that stepping through such an exploit and sharing this compiled knowledge with the wider security community can help with reasoning about the relative utility of various mitigation approaches. A lot of the individual exploitation techniques and mitigation options that I am [...]
