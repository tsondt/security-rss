Title: VMware ESXi Servers Encrypted by Lightning-Fast Python Script
Date: 2021-10-06T20:34:28+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware
Slug: 2021-10-06-vmware-esxi-servers-encrypted-by-lightning-fast-python-script

[Source](https://threatpost.com/vmware-esxi-encrypted-python-script-ransomware/175374/){:target="_blank" rel="noopener"}

> The little snippet of Python code strikes fast and nasty, taking less than three hours to complete a ransomware attack from initial breach to encryption. [...]
