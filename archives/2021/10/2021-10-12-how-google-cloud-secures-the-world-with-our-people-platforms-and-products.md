Title: How Google Cloud secures the world with our people, platforms and products
Date: 2021-10-12T12:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Google Cloud;Google Workspace;Next;Identity & Security
Slug: 2021-10-12-how-google-cloud-secures-the-world-with-our-people-platforms-and-products

[Source](https://cloud.google.com/blog/products/identity-security/next21-how-google-cloud-secures-the-world/){:target="_blank" rel="noopener"}

> In every industry, in every part of the world, cybersecurity concerns continue to grow in the wake of attacks on critical infrastructure and the software supply chain. Governments and businesses of all sizes recognize that they must do more to protect their employees, customers and citizens. But doing more of the same, like putting security band-aids on legacy infrastructure, is no longer helpful or productive. We need an enduring commitment, in products, people and monetary terms, to drive meaningful improvements in our collective security posture. Google recently announced a $10 billion investment to advance the security of governments around the [...]
