Title: Phishing campaign uses math symbols to evade detection
Date: 2021-10-12T12:22:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-12-phishing-campaign-uses-math-symbols-to-evade-detection

[Source](https://www.bleepingcomputer.com/review/security/phishing-campaign-uses-math-symbols-to-evade-detection/){:target="_blank" rel="noopener"}

> Phishing actors are now using mathematical symbols on impersonated company logos to evade detection from anti-phishing systems. [...]
