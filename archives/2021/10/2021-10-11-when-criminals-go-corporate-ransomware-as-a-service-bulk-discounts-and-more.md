Title: When criminals go corporate: Ransomware-as-a-service, bulk discounts and more
Date: 2021-10-11T09:30:04+00:00
Author: Emma Woollacott
Category: The Register
Tags: 
Slug: 2021-10-11-when-criminals-go-corporate-ransomware-as-a-service-bulk-discounts-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/11/ransomware_as_a_service/){:target="_blank" rel="noopener"}

> Pen-testers, rogue developers, dodgy hosters, etc. etc. Feature This summer, Abnormal Security discovered that some of its customers' staff were receiving emails inviting them to install ransomware on a company computer in return for a $1m share of the "profits".... [...]
