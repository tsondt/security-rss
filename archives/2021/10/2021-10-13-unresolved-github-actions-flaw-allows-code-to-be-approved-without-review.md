Title: Unresolved GitHub Actions flaw allows code to be approved without review
Date: 2021-10-13T15:47:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-13-unresolved-github-actions-flaw-allows-code-to-be-approved-without-review

[Source](https://portswigger.net/daily-swig/unresolved-github-actions-flaw-allows-code-to-be-approved-without-review){:target="_blank" rel="noopener"}

> Mitigations are available for yet-to-be-fixed vulnerability [...]
