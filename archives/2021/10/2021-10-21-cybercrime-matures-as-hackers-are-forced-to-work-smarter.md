Title: Cybercrime matures as hackers are forced to work smarter
Date: 2021-10-21T10:58:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-21-cybercrime-matures-as-hackers-are-forced-to-work-smarter

[Source](https://www.bleepingcomputer.com/news/security/cybercrime-matures-as-hackers-are-forced-to-work-smarter/){:target="_blank" rel="noopener"}

> An analysis of 500 hacking incidents across a wide range of industries has revealed trends that characterize a maturity in the way hacking groups operate today. [...]
