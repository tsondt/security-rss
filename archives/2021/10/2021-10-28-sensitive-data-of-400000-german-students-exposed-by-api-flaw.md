Title: Sensitive data of 400,000 German students exposed by API flaw
Date: 2021-10-28T03:03:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-28-sensitive-data-of-400000-german-students-exposed-by-api-flaw

[Source](https://www.bleepingcomputer.com/news/security/sensitive-data-of-400-000-german-students-exposed-by-api-flaw/){:target="_blank" rel="noopener"}

> Approximately 400,000 users of Scoolio, a student community app widely used in Germany, had sensitive information exposed due to an API flaw in the platform. [...]
