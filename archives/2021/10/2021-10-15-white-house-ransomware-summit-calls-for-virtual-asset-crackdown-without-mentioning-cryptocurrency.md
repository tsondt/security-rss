Title: White House ransomware summit calls for virtual asset crackdown, without mentioning cryptocurrency
Date: 2021-10-15T05:59:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-15-white-house-ransomware-summit-calls-for-virtual-asset-crackdown-without-mentioning-cryptocurrency

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/15/ransomware_summit_cryptocurrency_crackdown/){:target="_blank" rel="noopener"}

> Inconsistent regulation means crooks can sneak cryptos through cracks – pretty much everyone wants them filled The 30-nation gabfest convened under the auspices of the US National Security Council’s Counter-Ransomware Initiative has ended with agreement that increased regulation of virtual assets is required to curb the digital coins' allure to criminals.... [...]
