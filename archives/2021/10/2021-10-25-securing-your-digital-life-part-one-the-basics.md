Title: Securing your digital life, part one: The basics
Date: 2021-10-25T13:00:24+00:00
Author: Sean Gallagher
Category: Ars Technica
Tags: Biz & IT;Features;infosec;securing your digital life
Slug: 2021-10-25-securing-your-digital-life-part-one-the-basics

[Source](https://arstechnica.com/?p=1804178){:target="_blank" rel="noopener"}

> Enlarge / Artist's impression of how to keep your digital stuff safe from all kinds of threats. (credit: Aurich Lawson | Getty Images) I spend most of my time these days investigating the uglier side of digital life—examining the techniques, tools, and practices of cyber criminals to help people better defend against them. It’s not entirely different from my days at Ars Technica, but it has given me a greater appreciation for just how hard it is for normal folks to stay “safe” digitally. Even those who consider themselves well educated about cyber crime and security threats—and who do everything [...]
