Title: Hackers used billing software zero-day to deploy ransomware
Date: 2021-10-25T10:31:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-25-hackers-used-billing-software-zero-day-to-deploy-ransomware

[Source](https://www.bleepingcomputer.com/news/security/hackers-used-billing-software-zero-day-to-deploy-ransomware/){:target="_blank" rel="noopener"}

> An unknown ransomware group is exploiting a critical SQL injection bug found in the BillQuick Web Suite time and billing solution to deploy ransomware on their targets' networks in ongoing attacks. [...]
