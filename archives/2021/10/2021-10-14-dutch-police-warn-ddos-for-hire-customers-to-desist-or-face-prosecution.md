Title: Dutch police warn DDoS-for-hire customers to desist or face prosecution
Date: 2021-10-14T15:29:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-14-dutch-police-warn-ddos-for-hire-customers-to-desist-or-face-prosecution

[Source](https://portswigger.net/daily-swig/dutch-police-warn-ddos-for-hire-customers-to-desist-or-face-prosecution){:target="_blank" rel="noopener"}

> We know what you DDoSed last summer [...]
