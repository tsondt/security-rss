Title: NPM packages disguised as Roblox API code caught carrying ransomware
Date: 2021-10-27T20:43:38+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-10-27-npm-packages-disguised-as-roblox-api-code-caught-carrying-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/27/npm_roblox_ransomware/){:target="_blank" rel="noopener"}

> Subverted libraries likely intended as a prank but should be taken seriously, say security researchers Yet another NPM library has turned up infected with malware. Security firm Sonatype on Wednesday said it had spotted two related malicious NPM libraries that were named so they might be mistaken for a popular legitimate module that serves as a Roblox API wrapper.... [...]
