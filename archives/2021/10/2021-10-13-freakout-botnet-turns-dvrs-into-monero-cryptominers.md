Title: FreakOut Botnet Turns DVRs Into Monero Cryptominers
Date: 2021-10-13T20:17:09+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;Web Security
Slug: 2021-10-13-freakout-botnet-turns-dvrs-into-monero-cryptominers

[Source](https://threatpost.com/freakout-botnet-dvrs-monero-cryptominers/175467/){:target="_blank" rel="noopener"}

> The new Necro Python exploit targets Visual Tool DVRs used in surveillance systems. [...]
