Title: SolarWinds APT Targets Tech Resellers in Latest Supply-Chain Cyberattacks
Date: 2021-10-25T19:16:45+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Government;Hacks;Web Security
Slug: 2021-10-25-solarwinds-apt-targets-tech-resellers-in-latest-supply-chain-cyberattacks

[Source](https://threatpost.com/solarwinds-tech-resellers-supply-chain-cyberattacks/175716/){:target="_blank" rel="noopener"}

> The Nobelium group, linked to Russia's spy agency, is looking to use resellers as a path to infiltrate their valuable downstream customers - and it's working. [...]
