Title: US clothing brand Next Level Apparel reports phishing-related data breach
Date: 2021-10-06T11:03:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-06-us-clothing-brand-next-level-apparel-reports-phishing-related-data-breach

[Source](https://portswigger.net/daily-swig/us-clothing-brand-next-level-apparel-reports-phishing-related-data-breach){:target="_blank" rel="noopener"}

> Exposed data includes payment card and driver’s license numbers [...]
