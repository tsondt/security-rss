Title: Medtronic urgently recalls insulin pump controllers over hacking concerns
Date: 2021-10-06T10:48:56-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-06-medtronic-urgently-recalls-insulin-pump-controllers-over-hacking-concerns

[Source](https://www.bleepingcomputer.com/news/security/medtronic-urgently-recalls-insulin-pump-controllers-over-hacking-concerns/){:target="_blank" rel="noopener"}

> Medtronic is urgently recalling remote controllers for insulin pumps belonging to its 'MiniMed Paradigm' family of products, due to potential cybersecurity risks. [...]
