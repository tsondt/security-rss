Title: Nine arrested for impersonating bank clerks to steal from the elderly
Date: 2021-10-21T09:18:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2021-10-21-nine-arrested-for-impersonating-bank-clerks-to-steal-from-the-elderly

[Source](https://www.bleepingcomputer.com/news/security/nine-arrested-for-impersonating-bank-clerks-to-steal-from-the-elderly/){:target="_blank" rel="noopener"}

> The Dutch Police have arrested nine people for targeting and stealing money from the elderly by impersonating bank employees. [...]
