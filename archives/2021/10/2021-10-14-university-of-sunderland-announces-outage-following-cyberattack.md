Title: University of Sunderland announces outage following cyberattack
Date: 2021-10-14T12:17:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-14-university-of-sunderland-announces-outage-following-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/university-of-sunderland-announces-outage-following-cyberattack/){:target="_blank" rel="noopener"}

> The University of Sunderland in the UK has announced extensive operational issues that have taken most of its IT systems down, attributing the problem to a cyber-attack. [...]
