Title: Cloud Data Loss Prevention is now automatic!
Date: 2021-10-18T16:00:00+00:00
Author: Jordanna Chord
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-10-18-cloud-data-loss-prevention-is-now-automatic

[Source](https://cloud.google.com/blog/products/identity-security/automatic-dlp-for-bigquery/){:target="_blank" rel="noopener"}

> Data is one of your most valuable assets; understanding and using data effectively powers your business. However, it can also be a source of privacy, security, and compliance risk. The data discovery and classification capabilities of Cloud Data Loss Prevention (DLP) have helped many Google Cloud customers identify where sensitive data resides. However, data growth is outpacing the ability to manually inspect data, and data sprawl means sensitive data is increasingly appearing in places it’s not expected. You need a solution for managing data risk that can automatically scale with the rapid growth of your data without additional management overhead. [...]
