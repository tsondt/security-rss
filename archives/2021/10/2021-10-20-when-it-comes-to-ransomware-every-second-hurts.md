Title: When it comes to ransomware, every second hurts
Date: 2021-10-20T07:30:08+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2021-10-20-when-it-comes-to-ransomware-every-second-hurts

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/20/fortiedr_ransomware_defenses/){:target="_blank" rel="noopener"}

> Fortinet seeks to make EDR easy for non-specialists Sponsored For the longest time it seemed that modern endpoint detection and response (EDR) was getting on top of the worst malware, only for that certainty to evaporate in a single day in June 2017 thanks to a strange malware event remembered as the NotPetya attack.... [...]
