Title: Microsoft: Windows web content filtering now generally available
Date: 2021-10-29T06:52:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-29-microsoft-windows-web-content-filtering-now-generally-available

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-windows-web-content-filtering-now-generally-available/){:target="_blank" rel="noopener"}

> Microsoft has announced that web content filtering has reached general availability and is now available for all Windows enterprise customers. [...]
