Title: Sinclair Confirms Ransomware Attack That Disrupted TV Stations
Date: 2021-10-18T20:16:36+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Malware
Slug: 2021-10-18-sinclair-confirms-ransomware-attack-that-disrupted-tv-stations

[Source](https://threatpost.com/sinclair-ransomware-tv-stations/175548/){:target="_blank" rel="noopener"}

> A major cyberattack resulted in data being stolen, too, but Sinclair's not sure which information is now in the hands of the crooks. [...]
