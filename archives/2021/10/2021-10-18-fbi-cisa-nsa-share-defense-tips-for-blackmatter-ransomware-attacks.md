Title: FBI, CISA, NSA share defense tips for BlackMatter ransomware attacks
Date: 2021-10-18T18:03:04-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-18-fbi-cisa-nsa-share-defense-tips-for-blackmatter-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-cisa-nsa-share-defense-tips-for-blackmatter-ransomware-attacks/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA), the Federal Bureau of Investigation (FBI), and the National Security Agency (NSA) published today an advisory with details about how the BlackMatter ransomware gang operates. [...]
