Title: US nuke sub plans leaked on SD card hidden in peanut butter sandwich, claims FBI
Date: 2021-10-11T01:20:46+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-11-us-nuke-sub-plans-leaked-on-sd-card-hidden-in-peanut-butter-sandwich-claims-fbi

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/11/doj_alleges_nuclear_sub_data_leak/){:target="_blank" rel="noopener"}

> Docs were smuggled past security and sold for $110K of Monero after ProtonMail exchanges between 'Alice and 'Bob' The United States Department of Justice has announced a leak of information pertaining to the design of the nuclear-powered Virginia-class submarine, and the arrest of the alleged leakers.... [...]
