Title: Groove ransomware calls on all extortion gangs to attack US interests
Date: 2021-10-22T11:48:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-22-groove-ransomware-calls-on-all-extortion-gangs-to-attack-us-interests

[Source](https://www.bleepingcomputer.com/news/security/groove-ransomware-calls-on-all-extortion-gangs-to-attack-us-interests/){:target="_blank" rel="noopener"}

> The Groove ransomware gang is calling on other extortion groups to attack US interests after law enforcement took down REvil's infrastructure last week. [...]
