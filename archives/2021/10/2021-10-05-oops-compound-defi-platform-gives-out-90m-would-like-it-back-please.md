Title: Oops! Compound DeFi Platform Gives Out $90M, Would Like it Back, Please
Date: 2021-10-05T14:16:22+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Cryptography;Privacy
Slug: 2021-10-05-oops-compound-defi-platform-gives-out-90m-would-like-it-back-please

[Source](https://threatpost.com/compound-defi-platform-90m/175321/){:target="_blank" rel="noopener"}

> The Compound cryptocurrency exchange accidentally botched a platform upgrade and distributed millions in free COMP tokens to users - then threatened to dox the recipients. [...]
