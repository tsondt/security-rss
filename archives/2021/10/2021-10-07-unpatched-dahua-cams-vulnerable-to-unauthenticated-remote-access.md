Title: Unpatched Dahua cams vulnerable to unauthenticated remote access
Date: 2021-10-07T06:56:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-07-unpatched-dahua-cams-vulnerable-to-unauthenticated-remote-access

[Source](https://www.bleepingcomputer.com/news/security/unpatched-dahua-cams-vulnerable-to-unauthenticated-remote-access/){:target="_blank" rel="noopener"}

> Unpatched Dahua cameras are prone to two authentication bypass vulnerabilities, and a proof of concept exploit that came out today makes the case of upgrading pressing. [...]
