Title: Injection vulnerabilities in popular WordPress plugin could expose credentials, allow admin access
Date: 2021-10-15T12:41:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-15-injection-vulnerabilities-in-popular-wordpress-plugin-could-expose-credentials-allow-admin-access

[Source](https://portswigger.net/daily-swig/injection-vulnerabilities-in-popular-wordpress-plugin-could-expose-credentials-allow-admin-access){:target="_blank" rel="noopener"}

> Fastest Cache is used by more than one million people [...]
