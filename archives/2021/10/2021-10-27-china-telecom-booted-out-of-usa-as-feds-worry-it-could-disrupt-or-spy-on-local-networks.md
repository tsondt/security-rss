Title: China Telecom booted out of USA as Feds worry it could disrupt or spy on local networks
Date: 2021-10-27T01:57:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-27-china-telecom-booted-out-of-usa-as-feds-worry-it-could-disrupt-or-spy-on-local-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/27/china_telecom_booted_out_of/){:target="_blank" rel="noopener"}

> FCC urges more action against Huawei and DJI, too The US Federal Communications Commission (FCC) has terminated China Telecom's authority to provide communications services in the USA.... [...]
