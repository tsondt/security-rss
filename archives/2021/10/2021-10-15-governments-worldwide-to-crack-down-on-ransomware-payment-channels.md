Title: Governments worldwide to crack down on ransomware payment channels
Date: 2021-10-15T05:13:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency;Government
Slug: 2021-10-15-governments-worldwide-to-crack-down-on-ransomware-payment-channels

[Source](https://www.bleepingcomputer.com/news/security/governments-worldwide-to-crack-down-on-ransomware-payment-channels/){:target="_blank" rel="noopener"}

> Senior officials from 31 countries and the European Union said that their governments would take action to disrupt the cryptocurrency payment channels used by ransomware gangs to finance their operations. [...]
