Title: Data breach at Colorado university impacts 30,000 students
Date: 2021-10-27T14:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-27-data-breach-at-colorado-university-impacts-30000-students

[Source](https://portswigger.net/daily-swig/data-breach-at-colorado-university-impacts-30-000-students){:target="_blank" rel="noopener"}

> Atlassian vulnerability believed to be attack vector [...]
