Title: Canopy Parental Control App Wide Open to Unpatched XSS Bugs
Date: 2021-10-06T21:27:32+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;Web Security
Slug: 2021-10-06-canopy-parental-control-app-wide-open-to-unpatched-xss-bugs

[Source](https://threatpost.com/canopy-parental-control-app-unpatched-xss-bugs/175384/){:target="_blank" rel="noopener"}

> The possible cyberattacks include disabling monitoring, location-tracking of children and malicious redirects of parent-console users. [...]
