Title: About 26% of all malicious JavaScript threats are obfuscated
Date: 2021-10-19T12:03:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-19-about-26-of-all-malicious-javascript-threats-are-obfuscated

[Source](https://www.bleepingcomputer.com/news/security/about-26-percent-of-all-malicious-javascript-threats-are-obfuscated/){:target="_blank" rel="noopener"}

> A research that analyzed over 10,000 samples of diverse malicious software written in JavaScript concluded that roughly 26% of it is obfuscated to evade detection and analysis. [...]
