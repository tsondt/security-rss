Title: Acer servers cracked in India and Taiwan – including systems with customer data
Date: 2021-10-20T04:45:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-20-acer-servers-cracked-in-india-and-taiwan-including-systems-with-customer-data

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/20/acer_india_taiwan_data_leaks/){:target="_blank" rel="noopener"}

> Gang says it grabbed internal info, could do the same to Acer elsewhere Taiwanese PC maker Acer has not only admitted servers it operates in India and and Taiwan were compromised but that only those systems in India contained customer data.... [...]
