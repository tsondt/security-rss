Title: Ransom Disclosure Act would give victims 48 hours to report payments
Date: 2021-10-06T04:22:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2021-10-06-ransom-disclosure-act-would-give-victims-48-hours-to-report-payments

[Source](https://www.bleepingcomputer.com/news/legal/ransom-disclosure-act-would-give-victims-48-hours-to-report-payments/){:target="_blank" rel="noopener"}

> Victims of ransomware attacks in the United States may soon have to report any payments to hackers within 48 hours, as required by a new legislation proposal titled the 'Ransom Disclosure Act'. [...]
