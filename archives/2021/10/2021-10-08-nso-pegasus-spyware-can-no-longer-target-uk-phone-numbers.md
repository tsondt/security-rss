Title: NSO Pegasus spyware can no longer target UK phone numbers
Date: 2021-10-08T17:53:09+00:00
Author: Dan Sabbagh Defence and security editor
Category: The Guardian
Tags: Surveillance;Technology;Sheikh Mohammed bin Rashid al-Maktoum;Data and computer security;Malware;Hacking;Dubai;United Arab Emirates;Middle East and North Africa;UK news
Slug: 2021-10-08-nso-pegasus-spyware-can-no-longer-target-uk-phone-numbers

[Source](https://www.theguardian.com/world/2021/oct/08/nso-pegasus-spyware-can-no-longer-target-uk-phone-numbers){:target="_blank" rel="noopener"}

> Israeli maker of surveillance software blocked +44 code after detecting hack against Princess Haya, source says The powerful spyware used to hack into mobile phones belonging to Princess Haya and her divorce lawyer Fiona Shackleton is no longer effective against UK numbers, sources familiar with the software’s developer have said. NSO Group, the Israeli maker of the Pegasus surveillance tool, implemented a change preventing client countries from targeting +44 numbers, the sources said, after it became aware of the British hacking scandal on 5 August last year. Continue reading... [...]
