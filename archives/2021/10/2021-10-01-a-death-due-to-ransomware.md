Title: A Death Due to Ransomware
Date: 2021-10-01T14:56:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybercrime;hacking;healthcare;ransomware
Slug: 2021-10-01-a-death-due-to-ransomware

[Source](https://www.schneier.com/blog/archives/2021/10/a-death-due-to-ransomware.html){:target="_blank" rel="noopener"}

> The Wall Street Journal is reporting on a baby’s death at an Alabama hospital in 2019, which they argue was a direct result of the ransomware attack the hospital was undergoing. Amid the hack, fewer eyes were on the heart monitors — normally tracked on a large screen at the nurses’ station, in addition to inside the delivery room. Attending obstetrician Katelyn Parnell texted the nurse manager that she would have delivered the baby by caesarean section had she seen the monitor readout. “I need u to help me understand why I was not notified.” In another text, Dr. Parnell [...]
