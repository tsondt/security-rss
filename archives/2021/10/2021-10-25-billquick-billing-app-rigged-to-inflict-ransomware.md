Title: BillQuick Billing App Rigged to Inflict Ransomware
Date: 2021-10-25T20:51:06+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;Web Security
Slug: 2021-10-25-billquick-billing-app-rigged-to-inflict-ransomware

[Source](https://threatpost.com/billquick-billing-app-ransomware/175720/){:target="_blank" rel="noopener"}

> An SQL-injection bug in the BillQuick billing app has not only leaked sensitive information, it’s also let malicious actors execute code and deploy ransomware. [...]
