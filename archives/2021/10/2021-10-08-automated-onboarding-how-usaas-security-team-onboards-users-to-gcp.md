Title: Automated onboarding: How USAA’s security team onboards users to GCP
Date: 2021-10-08T16:00:00+00:00
Author: James DeLuna
Category: GCP Security
Tags: Customers;IT Ops;Identity & Security
Slug: 2021-10-08-automated-onboarding-how-usaas-security-team-onboards-users-to-gcp

[Source](https://cloud.google.com/blog/products/identity-security/gcp-developer-onboarding-automation/){:target="_blank" rel="noopener"}

> Since 1922, technology and innovation have played an integral role in USAA's ability to serve military members and their families. As membership has grown to over 13 million, the services required to meet members' needs have also evolved. As a result, application teams across our banking and insurance businesses have turned to Google Cloud to develop the services of tomorrow. At USAA, partners throughout the business rely heavily on the security team to get application teams onboarded to Google Cloud and productive in short order. This post details the automated processes our security team uses to satisfy those requirements. Specifically, [...]
