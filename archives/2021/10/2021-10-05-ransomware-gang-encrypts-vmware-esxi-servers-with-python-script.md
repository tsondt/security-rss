Title: Ransomware gang encrypts VMware ESXi servers with Python script
Date: 2021-10-05T09:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-05-ransomware-gang-encrypts-vmware-esxi-servers-with-python-script

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-encrypts-vmware-esxi-servers-with-python-script/){:target="_blank" rel="noopener"}

> Operators of an unknown ransomware gang are using a Python script to encrypt virtual machines hosted on VMware ESXi servers. [...]
