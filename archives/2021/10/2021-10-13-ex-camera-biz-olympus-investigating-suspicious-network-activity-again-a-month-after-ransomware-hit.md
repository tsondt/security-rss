Title: Ex-camera biz Olympus investigating 'suspicious' network activity again a month after ransomware hit
Date: 2021-10-13T13:53:39+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-13-ex-camera-biz-olympus-investigating-suspicious-network-activity-again-a-month-after-ransomware-hit

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/13/olympus_ransomware_suspicions_again/){:target="_blank" rel="noopener"}

> Plus: Extortionist gang threatens victims who talk to the press Olympus, the Japanese company once known for making cameras, is investigating "suspicious" activity on its networks again – a month after those same networks were ravaged by ransomware.... [...]
