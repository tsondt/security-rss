Title: Malicious Chrome ad blocker injects ads behind the scenes
Date: 2021-10-14T10:35:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-14-malicious-chrome-ad-blocker-injects-ads-behind-the-scenes

[Source](https://www.bleepingcomputer.com/news/security/malicious-chrome-ad-blocker-injects-ads-behind-the-scenes/){:target="_blank" rel="noopener"}

> The AllBlock Chromium ad blocking extension has been found to be injecting hidden affiliate links that generate commissions for the developers. [...]
