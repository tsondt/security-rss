Title: FCC orders phone carriers to enforce unlawful robocall blocking
Date: 2021-10-01T14:27:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-01-fcc-orders-phone-carriers-to-enforce-unlawful-robocall-blocking

[Source](https://www.bleepingcomputer.com/news/security/fcc-orders-phone-carriers-to-enforce-unlawful-robocall-blocking/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) announced earlier this week that phone companies are now required to filter calls from providers who haven't complied with a deadline to block illegal robocalls expired on September 28th. [...]
