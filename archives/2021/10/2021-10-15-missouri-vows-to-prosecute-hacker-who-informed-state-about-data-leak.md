Title: Missouri Vows to Prosecute ‘Hacker’ Who Informed State About Data Leak
Date: 2021-10-15T17:44:00+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Privacy;Vulnerabilities;Web Security
Slug: 2021-10-15-missouri-vows-to-prosecute-hacker-who-informed-state-about-data-leak

[Source](https://threatpost.com/missouri-prosecute-hacker-data-leak/175501/){:target="_blank" rel="noopener"}

> Missouri Gov. Mike Parson launched a criminal investigation of a reporter who flagged a state website that exposed 100K+ Social-Security numbers for teachers and other state employees. [...]
