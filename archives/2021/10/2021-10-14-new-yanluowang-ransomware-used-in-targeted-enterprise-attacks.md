Title: New Yanluowang ransomware used in targeted enterprise attacks
Date: 2021-10-14T06:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-14-new-yanluowang-ransomware-used-in-targeted-enterprise-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-yanluowang-ransomware-used-in-targeted-enterprise-attacks/){:target="_blank" rel="noopener"}

> A new and still under development ransomware strain is being used in highly targeted attacks against enterprise entities as Broadcom's Symantec Threat Hunter Team discovered. [...]
