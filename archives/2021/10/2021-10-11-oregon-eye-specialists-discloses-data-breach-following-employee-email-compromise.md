Title: Oregon Eye Specialists discloses data breach following employee email compromise
Date: 2021-10-11T12:25:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-11-oregon-eye-specialists-discloses-data-breach-following-employee-email-compromise

[Source](https://portswigger.net/daily-swig/oregon-eye-specialists-discloses-data-breach-following-employee-email-compromise){:target="_blank" rel="noopener"}

> Attackers had access to mailboxes over a two-month period [...]
