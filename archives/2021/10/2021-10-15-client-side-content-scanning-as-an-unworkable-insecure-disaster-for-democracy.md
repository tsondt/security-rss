Title: Client-side content scanning as an unworkable, insecure disaster for democracy
Date: 2021-10-15T00:35:20+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-10-15-client-side-content-scanning-as-an-unworkable-insecure-disaster-for-democracy

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/15/clientside_side_scanning/){:target="_blank" rel="noopener"}

> Hopefully we're still listening to experts Fourteen of the world's leading computer security and cryptography experts have released a paper arguing against the use of client-side scanning because it creates security and privacy risks.... [...]
