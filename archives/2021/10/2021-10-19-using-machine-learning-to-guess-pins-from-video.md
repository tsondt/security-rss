Title: Using Machine Learning to Guess PINs from Video
Date: 2021-10-19T13:07:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2021-10-19-using-machine-learning-to-guess-pins-from-video

[Source](https://www.schneier.com/blog/archives/2021/10/using-machine-learning-to-guess-pins-from-video.html){:target="_blank" rel="noopener"}

> Researchers trained a machine-learning system on videos of people typing their PINs into ATMs: By using three tries, which is typically the maximum allowed number of attempts before the card is withheld, the researchers reconstructed the correct sequence for 5-digit PINs 30% of the time, and reached 41% for 4-digit PINs. This works even if the person is covering the pad with their hands. The article doesn’t contain a link to the original research. If someone knows it, please put it in the comments. Slashdot thread. [...]
