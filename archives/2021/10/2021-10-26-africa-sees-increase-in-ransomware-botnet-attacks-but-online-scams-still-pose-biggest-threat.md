Title: Africa sees increase in ransomware, botnet attacks – but online scams still pose biggest threat
Date: 2021-10-26T14:26:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-26-africa-sees-increase-in-ransomware-botnet-attacks-but-online-scams-still-pose-biggest-threat

[Source](https://portswigger.net/daily-swig/africa-sees-increase-in-ransomware-botnet-attacks-but-online-scams-still-pose-biggest-threat){:target="_blank" rel="noopener"}

> Fraud is still the primary goal of cybercriminals operating across the continent, Interpol warns in latest market report [...]
