Title: Scrambling to counter a ransomware attack could leave you with egg on your face
Date: 2021-10-19T18:00:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-10-19-scrambling-to-counter-a-ransomware-attack-could-leave-you-with-egg-on-your-face

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/19/randsome_pre_protection/){:target="_blank" rel="noopener"}

> Join this masterclass and learn how to plan a far more efficient response Sponsored When you read about security teams “scrambling” to respond to a ransomware attack, what do you think is the real problem?... [...]
