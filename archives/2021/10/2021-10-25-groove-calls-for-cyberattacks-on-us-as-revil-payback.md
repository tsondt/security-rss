Title: Groove Calls for Cyberattacks on US as REvil Payback
Date: 2021-10-25T21:13:17+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-10-25-groove-calls-for-cyberattacks-on-us-as-revil-payback

[Source](https://threatpost.com/groove-ransomware-revil-revenge-us-cyberattacks/175726/){:target="_blank" rel="noopener"}

> The bold move signals a looming clash between Russian ransomware groups and the U.S. [...]
