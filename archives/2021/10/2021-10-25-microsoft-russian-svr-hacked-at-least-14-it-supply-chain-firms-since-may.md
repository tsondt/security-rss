Title: Microsoft: Russian SVR hacked at least 14 IT supply chain firms since May
Date: 2021-10-25T04:37:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-25-microsoft-russian-svr-hacked-at-least-14-it-supply-chain-firms-since-may

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-russian-svr-hacked-at-least-14-it-supply-chain-firms-since-may/){:target="_blank" rel="noopener"}

> Microsoft says the Russian-backed Nobelium threat group behind last year's SolarWinds hack is still targeting the global IT supply chain, with 140 resellers and technology service providers attacked and at least 14 breached since May 2021. [...]
