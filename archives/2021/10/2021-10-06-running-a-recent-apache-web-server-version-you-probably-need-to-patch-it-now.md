Title: Running a recent Apache web server version? You probably need to patch it. Now
Date: 2021-10-06T16:28:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-10-06-running-a-recent-apache-web-server-version-you-probably-need-to-patch-it-now

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/06/apache_web_server_data_patch/){:target="_blank" rel="noopener"}

> Unless you want to leak like a sieve The Apache Software Foundation has hurried out a patch to address a pair of HTTP Web Server vulnerabilities, at least one of which is already being actively exploited.... [...]
