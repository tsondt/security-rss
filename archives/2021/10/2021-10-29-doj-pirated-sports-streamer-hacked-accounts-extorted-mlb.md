Title: DOJ: Pirated sports streamer hacked accounts, extorted MLB
Date: 2021-10-29T09:56:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2021-10-29-doj-pirated-sports-streamer-hacked-accounts-extorted-mlb

[Source](https://www.bleepingcomputer.com/news/security/doj-pirated-sports-streamer-hacked-accounts-extorted-mlb/){:target="_blank" rel="noopener"}

> The U.S. Attorney's Office for the Southern District of New York has charged a man for illegally streaming MLB, NBA, NFL, and NHL games via the web and hacking into sports leagues' customer accounts. [...]
