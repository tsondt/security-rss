Title: Twitch Leak Included Emails, Passwords: Researcher
Date: 2021-10-07T20:25:32+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Breaking News;Cloud Security;Hacks;Web Security
Slug: 2021-10-07-twitch-leak-included-emails-passwords-researcher

[Source](https://threatpost.com/twitch-leak-emails-passwords/175390/){:target="_blank" rel="noopener"}

> A researcher combed through the Twitch leak and found what they said was evidence of PayPal chargebacks with names and emails, employees' emails, and more. [...]
