Title: Vidar stealer abuses Mastodon to silently get C2 configuration
Date: 2021-10-07T11:59:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-07-vidar-stealer-abuses-mastodon-to-silently-get-c2-configuration

[Source](https://www.bleepingcomputer.com/news/security/vidar-stealer-abuses-mastodon-to-silently-get-c2-configuration/){:target="_blank" rel="noopener"}

> The Vidar stealer has returned in a new campaign that abuses the Mastodon social media network to get C2 configuration without raising alarms. [...]
