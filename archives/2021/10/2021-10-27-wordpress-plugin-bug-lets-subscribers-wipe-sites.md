Title: WordPress Plugin Bug Lets Subscribers Wipe Sites
Date: 2021-10-27T21:39:11+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-10-27-wordpress-plugin-bug-lets-subscribers-wipe-sites

[Source](https://threatpost.com/wordpress-plugin-bug-wipe-sites/175826/){:target="_blank" rel="noopener"}

> The flaw, found in the Hashthemes Demo Importer plugin, allows any authenticated user to exsanguinate a vulnerable WordPress site, deleting nearly all database content and uploaded media. [...]
