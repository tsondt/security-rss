Title: Microsoft asks admins to patch PowerShell to fix WDAC bypass
Date: 2021-10-18T09:30:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-18-microsoft-asks-admins-to-patch-powershell-to-fix-wdac-bypass

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-asks-admins-to-patch-powershell-to-fix-wdac-bypass/){:target="_blank" rel="noopener"}

> Microsoft has asked system administrators to patch PowerShell 7 against two vulnerabilities allowing attackers to bypass Windows Defender Application Control (WDAC) enforcements and gain access to plain text credentials. [...]
