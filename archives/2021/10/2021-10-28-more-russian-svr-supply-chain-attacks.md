Title: More Russian SVR Supply-Chain Attacks
Date: 2021-10-28T11:12:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Microsoft;Russia;supply chain
Slug: 2021-10-28-more-russian-svr-supply-chain-attacks

[Source](https://www.schneier.com/blog/archives/2021/10/more-russian-svr-supply-chain-attacks.html){:target="_blank" rel="noopener"}

> Microsoft is reporting that the same attacker that was behind the SolarWinds breach — the Russian SVR, which Microsoft is calling Nobelium — is continuing with similar supply-chain attacks: Nobelium has been attempting to replicate the approach it has used in past attacks by targeting organizations integral to the global IT supply chain. This time, it is attacking a different part of the supply chain: resellers and other technology service providers that customize, deploy and manage cloud services and other technologies on behalf of their customers. We believe Nobelium ultimately hopes to piggyback on any direct access that resellers may [...]
