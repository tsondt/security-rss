Title: Whatever sort of disaster we’re talking about, if your backups are fried, you’re not going to recover
Date: 2021-10-18T07:30:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-10-18-whatever-sort-of-disaster-were-talking-about-if-your-backups-are-fried-youre-not-going-to-recover

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/18/rubrik_zero_trust_architecture/){:target="_blank" rel="noopener"}

> Here’s how zero trust and immutability can save you Sponsored When you’re putting your enterprise security and data management strategy in place, should you worry more about ransomware or natural disasters?... [...]
