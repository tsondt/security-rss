Title: What’s missing from most ICS cybersecurity training? The ICS itself…
Date: 2021-10-12T06:30:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-10-12-whats-missing-from-most-ics-cybersecurity-training-the-ics-itself

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/12/whats_missing_from_most_ics/){:target="_blank" rel="noopener"}

> SANS gets you hands-on with 'unique and amazing' industrial setup Sponsored You can learn virtually anything virtually these days.... [...]
