Title: Microsoft: Iran-linked hackers target US defense tech companies
Date: 2021-10-11T11:52:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-11-microsoft-iran-linked-hackers-target-us-defense-tech-companies

[Source](https://www.bleepingcomputer.com/news/security/microsoft-iran-linked-hackers-target-us-defense-tech-companies/){:target="_blank" rel="noopener"}

> Iran-linked threat actors are targeting the Office 365 tenants of US and Israeli defense technology companies in extensive password spraying attacks. [...]
