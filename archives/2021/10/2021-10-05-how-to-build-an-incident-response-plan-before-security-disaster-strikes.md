Title: How to Build an Incident-Response Plan, Before Security Disaster Strikes
Date: 2021-10-05T14:55:30+00:00
Author: Joseph Carson
Category: Threatpost
Tags: Breach;Cloud Security;InfoSec Insider;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2021-10-05-how-to-build-an-incident-response-plan-before-security-disaster-strikes

[Source](https://threatpost.com/incident-response-plan-security-disaster/175335/){:target="_blank" rel="noopener"}

> Joseph Carson, Chief Security Scientist at ThycoticCentrify, offers a 7-step practical IR checklist for ensuring a swift recovery from a cyberattack. [...]
