Title: Cloud CISO Perspectives: October 2021
Date: 2021-10-29T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-10-29-cloud-ciso-perspectives-october-2021

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-october-2021/){:target="_blank" rel="noopener"}

> October has been a busy month for Google Cloud. We just held our annual conference, Google Cloud Next ‘21, where we made significant security announcements for our customers of all sizes and geographies. It’s also National Cybersecurity Awareness Month where our security teams across Google delivered important research on new threat campaigns and product updates to provide the highest levels of account security for all users. In this month’s post, I’ll recap all of the security “ Action ” from Next ‘21, including product updates that deliver “secure products” not just “security products” and important industry momentum for tackling open [...]
