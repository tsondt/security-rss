Title: Upcoming Speaking Engagements
Date: 2021-10-14T16:45:10+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2021-10-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2021/10/upcoming-speaking-engagements-13.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’ll be speaking at an Informa event on November 29, 2021. Details to come. The list is maintained on this page. [...]
