Title: Problems with Multifactor Authentication
Date: 2021-10-21T11:25:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;phishing;ransomware;social engineering;two-factor authentication
Slug: 2021-10-21-problems-with-multifactor-authentication

[Source](https://www.schneier.com/blog/archives/2021/10/problems-with-multifactor-authentication.html){:target="_blank" rel="noopener"}

> Roger Grimes on why multifactor authentication isn’t a panacea : The first time I heard of this issue was from a Midwest CEO. His organization had been hit by ransomware to the tune of $10M. Operationally, they were still recovering nearly a year later. And, embarrassingly, it was his most trusted VP who let the attackers in. It turns out that the VP had approved over 10 different push-based messages for logins that he was not involved in. When the VP was asked why he approved logins for logins he was not actually doing, his response was, “They (IT) told [...]
