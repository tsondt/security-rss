Title: Microsoft October 2021 Patch Tuesday fixes 4 zero-days, 71 flaws
Date: 2021-10-12T13:31:12-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-12-microsoft-october-2021-patch-tuesday-fixes-4-zero-days-71-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-october-2021-patch-tuesday-fixes-4-zero-days-71-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's October 2021 Patch Tuesday, and with it comes fixes for four zero-day vulnerabilities and a total of 74 flaws. [...]
