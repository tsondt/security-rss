Title: EU ban on anonymous domain registration welcomed by threat intel firm
Date: 2021-10-22T10:31:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-22-eu-ban-on-anonymous-domain-registration-welcomed-by-threat-intel-firm

[Source](https://portswigger.net/daily-swig/eu-ban-on-anonymous-domain-registration-welcomed-by-threat-intel-firm){:target="_blank" rel="noopener"}

> ‘This raises the bar and makes it expensive for easy cyber criminality,’ argues DomainTools [...]
