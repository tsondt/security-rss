Title: Next’21 - Your guide to Google Cloud Security sessions
Date: 2021-10-07T16:00:00+00:00
Author: Rob Sadowski
Category: GCP Security
Tags: Next;Identity & Security
Slug: 2021-10-07-next21-your-guide-to-google-cloud-security-sessions

[Source](https://cloud.google.com/blog/products/identity-security/security-topics-at-google-cloud-next-2021/){:target="_blank" rel="noopener"}

> Google Next '21 is just a few days away! We have an exciting lineup in the security track that will help you understand the Google Cloud security offerings available to protect your employees and assets, intellectual property, users, and brand. The security track at Next '21 includes a security spotlight session, breakout sessions, panel discussions, hands-on labs, a live demo, and live Q&A. This post provides you a guide to all the security track sessions that you can attend to learn about the latest products, features, and tools that help you to secure your organization. You don't want to miss [...]
