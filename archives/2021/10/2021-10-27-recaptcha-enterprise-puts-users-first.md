Title: reCAPTCHA Enterprise puts users first
Date: 2021-10-27T16:00:00+00:00
Author: Aaron Malenfant
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-10-27-recaptcha-enterprise-puts-users-first

[Source](https://cloud.google.com/blog/products/identity-security/recaptcha-enterprise-protects-users-and-is-frictionless/){:target="_blank" rel="noopener"}

> reCAPTCHA has defended the web for more than 14 years, and is protecting more than 5+ million websites on the Internet today. The heart of our mission has always been to be hard on bots and easy on humans. This is a challenge that evolves with all the new ways the web can be used and the increasing sophistication of bots. reCAPTCHA started with simple warped text. As bots got smarter, reCAPTCHA provided harder images for end users to solve. We recognize this race between the intelligence of AI and humans has made the users’ experience increasingly challenging. So, in [...]
