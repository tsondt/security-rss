Title: New Atom Silo ransomware targets vulnerable Confluence servers
Date: 2021-10-04T09:21:40-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-04-new-atom-silo-ransomware-targets-vulnerable-confluence-servers

[Source](https://www.bleepingcomputer.com/news/security/new-atom-silo-ransomware-targets-vulnerable-confluence-servers/){:target="_blank" rel="noopener"}

> Atom Silo, a newly spotted ransomware group, is targeting a recently patched and actively exploited Confluence Server and Data Center vulnerability to deploy their ransomware payloads. [...]
