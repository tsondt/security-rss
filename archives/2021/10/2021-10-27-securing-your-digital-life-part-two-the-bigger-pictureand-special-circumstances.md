Title: Securing your digital life, part two: The bigger picture—and special circumstances
Date: 2021-10-27T13:00:55+00:00
Author: Sean Gallagher
Category: Ars Technica
Tags: Biz & IT;Features;infosec;securing your digital life
Slug: 2021-10-27-securing-your-digital-life-part-two-the-bigger-pictureand-special-circumstances

[Source](https://arstechnica.com/?p=1804312){:target="_blank" rel="noopener"}

> Enlarge (credit: ANDRZEJ WOJCICKI / SCIENCE PHOTO LIBRARY / Getty Images) In the first half of this guide to personal digital security, I covered the basics of assessing digital risks and protecting what you can control: your devices. But the physical devices you use represent only a fraction of your overall digital exposure. According to a report by Aite Group, nearly half of US consumers experienced some form of identity theft over the last two years. Losses from these thefts are expected to reach $721.3 billion for 2021—and that’s only counting cases where criminals take over and abuse online accounts. [...]
