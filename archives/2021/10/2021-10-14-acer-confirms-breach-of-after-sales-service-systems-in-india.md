Title: Acer confirms breach of after-sales service systems in India
Date: 2021-10-14T06:13:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-14-acer-confirms-breach-of-after-sales-service-systems-in-india

[Source](https://www.bleepingcomputer.com/news/security/acer-confirms-breach-of-after-sales-service-systems-in-india/){:target="_blank" rel="noopener"}

> Taiwanese computer giant Acer has confirmed that its after-sales service systems in India were recently breached in what the company called "an isolated attack." [...]
