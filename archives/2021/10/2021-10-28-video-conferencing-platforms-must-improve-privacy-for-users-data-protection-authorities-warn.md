Title: Video conferencing platforms must improve privacy for users, data protection authorities warn
Date: 2021-10-28T11:27:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-28-video-conferencing-platforms-must-improve-privacy-for-users-data-protection-authorities-warn

[Source](https://portswigger.net/daily-swig/video-conferencing-platforms-must-improve-privacy-for-users-data-protection-authorities-warn){:target="_blank" rel="noopener"}

> New cross-country report highlights need for better policies [...]
