Title: OPPA: Ohio could become the third US state to enact a new consumer privacy law in 2021
Date: 2021-10-06T15:41:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-06-oppa-ohio-could-become-the-third-us-state-to-enact-a-new-consumer-privacy-law-in-2021

[Source](https://portswigger.net/daily-swig/oppa-ohio-could-become-the-third-us-state-to-enact-a-new-consumer-privacy-law-in-2021){:target="_blank" rel="noopener"}

> Ohio Personal Privacy Act will grant Ohioans an expansive set of new rights, writes US attorney David Oberly [...]
