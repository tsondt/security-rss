Title: Facebook sues scraper who sold 178 million phone numbers and user IDs
Date: 2021-10-25T08:01:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-10-25-facebook-sues-scraper-who-sold-178-million-phone-numbers-and-user-ids

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/25/facebook_sues_man_for_scraping/){:target="_blank" rel="noopener"}

> Apparently The Social Network TM is the only one allowed to do nasty things with users' data Facebook has sued a Ukrainian national for allegedly harvesting and selling personal data describing 178 million of the Social Network TM 's users – actions it says violates the service's terms of service.... [...]
