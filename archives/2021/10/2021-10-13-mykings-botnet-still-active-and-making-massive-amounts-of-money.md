Title: MyKings botnet still active and making massive amounts of money
Date: 2021-10-13T13:14:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-10-13-mykings-botnet-still-active-and-making-massive-amounts-of-money

[Source](https://www.bleepingcomputer.com/news/security/mykings-botnet-still-active-and-making-massive-amounts-of-money/){:target="_blank" rel="noopener"}

> The MyKings botnet (aka Smominru or DarkCloud) is still actively spreading, making massive amounts of money in crypto, five years after it first appeared in the wild. [...]
