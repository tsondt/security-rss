Title: Cisco SD-WAN Security Bug Allows Root Code Execution
Date: 2021-10-22T14:48:26+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2021-10-22-cisco-sd-wan-security-bug-allows-root-code-execution

[Source](https://threatpost.com/cisco-sd-wan-bug-code-execution-root/175669/){:target="_blank" rel="noopener"}

> The high-severity bug, tracked as CVE-2021-1529, is an OS command-injection flaw. [...]
