Title: Google to auto-enroll 150m users, 2m YouTubers with two-factor authentication
Date: 2021-10-06T00:54:25+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2021-10-06-google-to-auto-enroll-150m-users-2m-youtubers-with-two-factor-authentication

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/06/google_twofactor_authentication/){:target="_blank" rel="noopener"}

> Uh, cool? Google is going to automatically enroll 150 million users and two million YouTube creators into using two-factor authentication for their accounts by the end of the year, it announced on Tuesday.... [...]
