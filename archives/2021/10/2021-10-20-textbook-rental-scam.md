Title: Textbook Rental Scam
Date: 2021-10-20T11:16:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Amazon;courts;crime;fraud;scams
Slug: 2021-10-20-textbook-rental-scam

[Source](https://www.schneier.com/blog/archives/2021/10/textbook-rental-scam.html){:target="_blank" rel="noopener"}

> Here’s a story of someone who, with three compatriots, rented textbooks from Amazon and then sold them instead of returning them. They used gift cards and prepaid credit cards to buy the books, so there was no available balance when Amazon tried to charge them the buyout price for non-returned books. They also used various aliases and other tricks to bypass Amazon’s fifteen-book limit. In all, they stole 14,000 textbooks worth over $1.5 million. The article doesn’t link to the indictment, so I don’t know how they were discovered. [...]
