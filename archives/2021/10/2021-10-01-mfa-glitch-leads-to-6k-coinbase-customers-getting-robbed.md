Title: MFA Glitch Leads to 6K+ Coinbase Customers Getting Robbed
Date: 2021-10-01T20:08:23+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks;Mobile Security;Vulnerabilities
Slug: 2021-10-01-mfa-glitch-leads-to-6k-coinbase-customers-getting-robbed

[Source](https://threatpost.com/mfa-glitch-coinbase-customers-robbery/175290/){:target="_blank" rel="noopener"}

> Coinbase suspects phishing led to attackers getting personal details needed to access wallets but also blamed a flaw in its SMS-based 2FA. [...]
