Title: The Week in Ransomware - October 8th 2021 - Making arrrests
Date: 2021-10-08T17:44:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-08-the-week-in-ransomware-october-8th-2021-making-arrrests

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-8th-2021-making-arrrests/){:target="_blank" rel="noopener"}

> This week's big news is the arrests of two ransomware operators in Ukraine responsible for hundreds of attacks targeting organizations worldwide. [...]
