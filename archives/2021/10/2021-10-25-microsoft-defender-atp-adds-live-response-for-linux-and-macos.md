Title: Microsoft Defender ATP adds live response for Linux and macOS
Date: 2021-10-25T11:50:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Apple;Linux;Security
Slug: 2021-10-25-microsoft-defender-atp-adds-live-response-for-linux-and-macos

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-atp-adds-live-response-for-linux-and-macos/){:target="_blank" rel="noopener"}

> Microsoft has announced the addition of new live macOS and Linux response capabilities to Defender for Endpoint,, the enterprise version of Redmond's Windows 10 Defender antivirus. [...]
