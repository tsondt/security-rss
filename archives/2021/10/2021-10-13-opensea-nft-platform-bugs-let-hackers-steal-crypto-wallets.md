Title: OpenSea NFT platform bugs let hackers steal crypto wallets ?
Date: 2021-10-13T07:17:23-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-13-opensea-nft-platform-bugs-let-hackers-steal-crypto-wallets

[Source](https://www.bleepingcomputer.com/news/security/opensea-nft-platform-bugs-let-hackers-steal-crypto-wallets-/){:target="_blank" rel="noopener"}

> Security researchers found that an attacker could leave OpenSea account owners with an empty cryptocurrency balance by luring them to click on malicious NFT art. [...]
