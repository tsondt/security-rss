Title: Embedded insecurity: Broadcom SDK vulnerabilities create lingering risk for router manufacturers
Date: 2021-10-05T16:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-05-embedded-insecurity-broadcom-sdk-vulnerabilities-create-lingering-risk-for-router-manufacturers

[Source](https://portswigger.net/daily-swig/embedded-insecurity-broadcom-sdk-vulnerabilities-create-lingering-risk-for-router-manufacturers){:target="_blank" rel="noopener"}

> Genesis of ‘forever-day’ vulnerability in Cisco business-grade router line uncovered [...]
