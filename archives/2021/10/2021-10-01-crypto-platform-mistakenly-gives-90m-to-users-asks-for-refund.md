Title: Crypto platform mistakenly gives $90M to users, asks for refund
Date: 2021-10-01T13:27:57-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-10-01-crypto-platform-mistakenly-gives-90m-to-users-asks-for-refund

[Source](https://www.bleepingcomputer.com/news/security/crypto-platform-mistakenly-gives-90m-to-users-asks-for-refund/){:target="_blank" rel="noopener"}

> In a major blunder, cryptocurrency platform Compound accidentally paid out $90 million among its users. Shortly after the mistake, the platform's founder began asking users to return the money—or else they would be reported to IRS, and possibly doxxed, threatened the founder. [...]
