Title: BlackMatter ransomware gang will target agriculture for its next harvest – Uncle Sam
Date: 2021-10-19T20:14:14+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-19-blackmatter-ransomware-gang-will-target-agriculture-for-its-next-harvest-uncle-sam

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/19/cisa_blackmatter_agricutlure/){:target="_blank" rel="noopener"}

> What was that about hackable tractors? The US CISA cybersecurity agency has warned that the Darkside ransomware gang, aka BlackMatter, has been targeting American food and agriculture businesses – and urges security pros to be on the lookout for indicators of compromise.... [...]
