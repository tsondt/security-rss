Title: Google, Salesforce, others team up to launch MVSP security baseline project
Date: 2021-10-29T12:35:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-29-google-salesforce-others-team-up-to-launch-mvsp-security-baseline-project

[Source](https://portswigger.net/daily-swig/google-salesforce-others-team-up-to-launch-mvsp-security-baseline-project){:target="_blank" rel="noopener"}

> The collaboration is focused on creating a vendor-neutral security standard [...]
