Title: HIV Scotland fined £10,000 for BCC email blunder identifying names of virus-carriers' patient-advocates
Date: 2021-10-25T11:48:21+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2021-10-25-hiv-scotland-fined-10000-for-bcc-email-blunder-identifying-names-of-virus-carriers-patient-advocates

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/25/hiv_scotland_email_fail/){:target="_blank" rel="noopener"}

> 'Serious deficiencies in tech and organisational measures' The United Kingdom's data watchdog is calling on organisations to review their "bulk email practices" after a BCC blunder by HIV Scotland incurred a £10,000 fine for breaking data protection regulations.... [...]
