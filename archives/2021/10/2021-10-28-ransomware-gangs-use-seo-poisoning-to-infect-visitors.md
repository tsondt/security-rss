Title: Ransomware gangs use SEO poisoning to infect visitors
Date: 2021-10-28T09:02:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-28-ransomware-gangs-use-seo-poisoning-to-infect-visitors

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-use-seo-poisoning-to-infect-visitors/){:target="_blank" rel="noopener"}

> Researchers have spotted two campaigns linked to either the REvil ransomware gang or the SolarMarker backdoor that use SEO poisoning to serve payloads to targets. [...]
