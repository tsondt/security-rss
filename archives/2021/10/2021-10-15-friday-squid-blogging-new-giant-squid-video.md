Title: Friday Squid Blogging: New Giant Squid Video
Date: 2021-10-15T21:18:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2021-10-15-friday-squid-blogging-new-giant-squid-video

[Source](https://www.schneier.com/blog/archives/2021/10/friday-squid-blogging-new-giant-squid-video.html){:target="_blank" rel="noopener"}

> New video of a large squid in the Red Sea at about 2,800 feet. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
