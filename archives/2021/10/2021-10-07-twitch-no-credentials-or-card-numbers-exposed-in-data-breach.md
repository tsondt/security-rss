Title: Twitch: No credentials or card numbers exposed in data breach
Date: 2021-10-07T03:39:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-07-twitch-no-credentials-or-card-numbers-exposed-in-data-breach

[Source](https://www.bleepingcomputer.com/news/security/twitch-no-credentials-or-card-numbers-exposed-in-data-breach/){:target="_blank" rel="noopener"}

> Twitch says that no login credentials and credit card numbers belonging to users or streamers were exposed following yesterday's massive data leak. [...]
