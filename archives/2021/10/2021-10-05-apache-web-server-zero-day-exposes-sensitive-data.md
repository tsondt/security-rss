Title: Apache Web Server Zero-Day Exposes Sensitive Data
Date: 2021-10-05T20:01:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-10-05-apache-web-server-zero-day-exposes-sensitive-data

[Source](https://threatpost.com/apache-web-server-zero-day-sensitive-data/175340/){:target="_blank" rel="noopener"}

> The open-source project has rolled out a security fix for CVE-2021-41773, for which public cyberattack exploit code is circulating. [...]
