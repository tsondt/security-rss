Title: Attack the block – How a security researcher cracked 70% of urban WiFi networks in one hit
Date: 2021-10-27T13:19:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-27-attack-the-block-how-a-security-researcher-cracked-70-of-urban-wifi-networks-in-one-hit

[Source](https://portswigger.net/daily-swig/attack-the-block-how-a-security-researcher-cracked-70-of-urban-wifi-networks-in-one-hit){:target="_blank" rel="noopener"}

> A new attack takes advantage of weak WiFi passwords [...]
