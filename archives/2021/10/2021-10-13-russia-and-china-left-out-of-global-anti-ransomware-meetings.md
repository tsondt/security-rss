Title: Russia and China left out of global anti-ransomware meetings
Date: 2021-10-13T06:56:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-13-russia-and-china-left-out-of-global-anti-ransomware-meetings

[Source](https://www.bleepingcomputer.com/news/security/russia-and-china-left-out-of-global-anti-ransomware-meetings/){:target="_blank" rel="noopener"}

> The White House National Security Council facilitates virtual meetings this week with senior officials and ministers from more than 30 countries in a virtual international counter-ransomware event to rally allies in the fight against the ransomware threat. [...]
