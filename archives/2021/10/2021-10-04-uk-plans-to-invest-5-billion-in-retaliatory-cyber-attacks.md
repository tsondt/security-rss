Title: UK plans to invest £5 billion in retaliatory cyber-attacks
Date: 2021-10-04T09:47:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-10-04-uk-plans-to-invest-5-billion-in-retaliatory-cyber-attacks

[Source](https://www.bleepingcomputer.com/news/security/uk-plans-to-invest-5-billion-in-retaliatory-cyber-attacks/){:target="_blank" rel="noopener"}

> The United Kingdom has revealed plans to invest £5 billion in bolstering national cybersecurity that includes creating a "Cyber Force" unit to perform retaliatory attacks. [...]
