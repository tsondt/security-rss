Title: Node.js sandboxes are open to prototype pollution
Date: 2021-10-22T14:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-22-nodejs-sandboxes-are-open-to-prototype-pollution

[Source](https://portswigger.net/daily-swig/node-js-sandboxes-are-open-to-prototype-pollution){:target="_blank" rel="noopener"}

> Sandbox breakout can lead to remote code execution, researchers warn [...]
