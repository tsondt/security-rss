Title: Verizon’s Visible Wireless Carrier Confirms Credential-Stuffing Attack
Date: 2021-10-14T18:18:03+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Mobile Security;Web Security
Slug: 2021-10-14-verizons-visible-wireless-carrier-confirms-credential-stuffing-attack

[Source](https://threatpost.com/verizon-visible-wireless-credential-stuffing/175483/){:target="_blank" rel="noopener"}

> Visible says yes, user accounts were hijacked, but it denied a breach. As of today, users are still posting tales of forcibly changed passwords and getting stuck with bills for pricey new iPhones. [...]
