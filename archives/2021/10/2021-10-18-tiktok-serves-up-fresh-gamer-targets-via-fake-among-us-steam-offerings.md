Title: TikTok Serves Up Fresh Gamer Targets via Fake Among Us, Steam Offerings
Date: 2021-10-18T18:23:40+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2021-10-18-tiktok-serves-up-fresh-gamer-targets-via-fake-among-us-steam-offerings

[Source](https://threatpost.com/tiktok-gamer-targets-among-us-steam/175546/){:target="_blank" rel="noopener"}

> The tween-friendly video app is being used to serve up malvertising, disguised as free Steam game accounts or Among Us game hacks. [...]
