Title: New Karma ransomware group likely a Nemty rebrand
Date: 2021-10-19T08:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-19-new-karma-ransomware-group-likely-a-nemty-rebrand

[Source](https://www.bleepingcomputer.com/news/security/new-karma-ransomware-group-likely-a-nemty-rebrand/){:target="_blank" rel="noopener"}

> Threat analysts at Sentinel Labs have found evidence of the Karma ransomware being just another evolutionary step in the strain that started as JSWorm, became Nemty, then Nefilim, Fusion, Milihpen, and most recently, Gangbang. [...]
