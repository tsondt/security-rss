Title: FBI: Ranzy Locker ransomware hit at least 30 US companies this year
Date: 2021-10-26T09:59:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-26-fbi-ranzy-locker-ransomware-hit-at-least-30-us-companies-this-year

[Source](https://www.bleepingcomputer.com/news/security/fbi-ranzy-locker-ransomware-hit-at-least-30-us-companies-this-year/){:target="_blank" rel="noopener"}

> The FBI said on Monday that Ranzy Locker ransomware operators had compromised at least 30 US companies this year from various industry sectors. [...]
