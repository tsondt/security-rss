Title: We regret to inform you there's an RCE vuln in old version of WinRAR. Yes, the file decompression utility
Date: 2021-10-21T15:25:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-21-we-regret-to-inform-you-theres-an-rce-vuln-in-old-version-of-winrar-yes-the-file-decompression-utility

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/21/winrar_rce_vuln_positive_technologies/){:target="_blank" rel="noopener"}

> Update to v6.02 – or don't, but on your head be it A remote code execution vulnerability existed in an old and free trial version of WinRAR, according to infosec firm Positive Technologies.... [...]
