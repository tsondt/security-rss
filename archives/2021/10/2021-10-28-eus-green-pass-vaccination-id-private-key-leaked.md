Title: EU’s Green Pass Vaccination ID Private Key Leaked
Date: 2021-10-28T15:34:06+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Web Security
Slug: 2021-10-28-eus-green-pass-vaccination-id-private-key-leaked

[Source](https://threatpost.com/eus-green-pass-vaccination-id-private-key-leaked/175857/){:target="_blank" rel="noopener"}

> UPDATE: French & Polish authorities found no sign of cryptographic compromise in the leak of the private key used to sign the vaccine passports and to create fake passes for Mickey Mouse and Adolf Hitler, et al. [...]
