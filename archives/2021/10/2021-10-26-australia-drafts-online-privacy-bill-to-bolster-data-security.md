Title: Australia drafts Online Privacy Bill to bolster data security
Date: 2021-10-26T08:14:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government;Legal
Slug: 2021-10-26-australia-drafts-online-privacy-bill-to-bolster-data-security

[Source](https://www.bleepingcomputer.com/news/security/australia-drafts-online-privacy-bill-to-bolster-data-security/){:target="_blank" rel="noopener"}

> Australia's Attorney-General has submitted the first draft of a new Online Privacy Bill that contains striking reforms over existing privacy laws. [...]
