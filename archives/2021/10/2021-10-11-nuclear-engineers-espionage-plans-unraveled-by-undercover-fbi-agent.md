Title: Nuclear engineer's espionage plans unraveled by undercover FBI agent
Date: 2021-10-11T08:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-11-nuclear-engineers-espionage-plans-unraveled-by-undercover-fbi-agent

[Source](https://www.bleepingcomputer.com/news/security/nuclear-engineers-espionage-plans-unraveled-by-undercover-fbi-agent/){:target="_blank" rel="noopener"}

> A Navy nuclear engineer and his wife were arrested under espionage-related charges alleging violations of the Atomic Energy Act after selling restricted nuclear-powered warship design data to a person they believed was a foreign power agent. [...]
