Title: US bans China Telecom Americas over national security risks
Date: 2021-10-27T11:15:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-27-us-bans-china-telecom-americas-over-national-security-risks

[Source](https://www.bleepingcomputer.com/news/security/us-bans-china-telecom-americas-over-national-security-risks/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) has revoked China Telecom Americas' license to provide telecommunication services within the United States. [...]
