Title: Microsoft Teams adds end-to-end encryption for one-to-one calls
Date: 2021-10-22T09:29:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-22-microsoft-teams-adds-end-to-end-encryption-for-one-to-one-calls

[Source](https://www.bleepingcomputer.com/news/security/microsoft-teams-adds-end-to-end-encryption-for-one-to-one-calls/){:target="_blank" rel="noopener"}

> Microsoft has announced the public preview roll-out of end-to-end encryption (E2EE) support for one-to-one Microsoft Teams calls. [...]
