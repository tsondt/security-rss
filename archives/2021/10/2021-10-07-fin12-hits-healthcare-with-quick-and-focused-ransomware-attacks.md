Title: FIN12 hits healthcare with quick and focused ransomware attacks
Date: 2021-10-07T13:53:05-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-07-fin12-hits-healthcare-with-quick-and-focused-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/fin12-hits-healthcare-with-quick-and-focused-ransomware-attacks/){:target="_blank" rel="noopener"}

> While most ransomware actors spend time on the victim network looking for important data to steal, one group favors quick malware deployment against sensitive, high-value targets. [...]
