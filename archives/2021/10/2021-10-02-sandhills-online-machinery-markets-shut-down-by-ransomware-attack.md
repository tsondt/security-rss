Title: Sandhills online machinery markets shut down by ransomware attack
Date: 2021-10-02T16:57:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-02-sandhills-online-machinery-markets-shut-down-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/sandhills-online-machinery-markets-shut-down-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Industry publication giant Sandhills Global has suffered a ransomware attack, causing hosted websites to become inaccessible and disrupting their business operations. [...]
