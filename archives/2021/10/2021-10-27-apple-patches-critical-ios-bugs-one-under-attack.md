Title: Apple Patches Critical iOS Bugs; One Under Attack
Date: 2021-10-27T16:14:24+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: News;Vulnerabilities;Web Security
Slug: 2021-10-27-apple-patches-critical-ios-bugs-one-under-attack

[Source](https://threatpost.com/apple-patches-ios-bugs/175803/){:target="_blank" rel="noopener"}

> Researchers found that one critical flaw in question is exploitable from the browser, allowing watering-hole attacks. [...]
