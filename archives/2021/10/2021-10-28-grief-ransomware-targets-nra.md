Title: Grief Ransomware Targets NRA
Date: 2021-10-28T12:07:02+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-10-28-grief-ransomware-targets-nra

[Source](https://threatpost.com/grief-ransomware-nra/175850/){:target="_blank" rel="noopener"}

> Grief, a ransomware group with ties to Russia-based Evil Corp, claims to have stolen data from the gun-rights group and has posted files on its dark web site. [...]
