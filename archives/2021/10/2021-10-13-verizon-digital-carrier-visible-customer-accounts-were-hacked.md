Title: Verizon digital carrier Visible customer accounts were hacked
Date: 2021-10-13T08:21:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-13-verizon-digital-carrier-visible-customer-accounts-were-hacked

[Source](https://www.bleepingcomputer.com/news/security/verizon-digital-carrier-visible-customer-accounts-were-hacked/){:target="_blank" rel="noopener"}

> Visible, a US digital wireless carrier owned by Verizon, admitted that some customer accounts were hacked after dealing with technical problems in the past couple of days. [...]
