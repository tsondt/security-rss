Title: Online harms don’t need dangerous legislation, they need a spot of naval action
Date: 2021-10-25T09:29:06+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2021-10-25-online-harms-dont-need-dangerous-legislation-they-need-a-spot-of-naval-action

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/25/online_harms_dont_need_dangerous/){:target="_blank" rel="noopener"}

> It worked on Jolly Roger, it can work on ProudWhiteGuy66373 Opinion Three things on the morning news reliably ruin breakfast for socially aware technogeeks.... [...]
