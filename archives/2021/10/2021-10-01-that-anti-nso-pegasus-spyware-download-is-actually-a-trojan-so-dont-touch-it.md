Title: That 'anti-NSO Pegasus spyware' download is actually a Trojan – so don't touch it
Date: 2021-10-01T16:28:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-01-that-anti-nso-pegasus-spyware-download-is-actually-a-trojan-so-dont-touch-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/01/amnesty_website_impersonator_trojan_pegasus/){:target="_blank" rel="noopener"}

> Cisco Talos spots early-stage campaign targeting low-info users A malware peddler has created a fake website posing as Amnesty International to serve gullible marks with software that claims to protect users against NSO Group's Pegasus malware. In fact it's a remote access Trojan (RAT).... [...]
