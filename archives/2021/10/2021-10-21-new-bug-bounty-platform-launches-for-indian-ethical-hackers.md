Title: New bug bounty platform launches for Indian ethical hackers
Date: 2021-10-21T15:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-21-new-bug-bounty-platform-launches-for-indian-ethical-hackers

[Source](https://portswigger.net/daily-swig/new-bug-bounty-platform-launches-for-indian-ethical-hackers){:target="_blank" rel="noopener"}

> Security researchers can sign up now [...]
