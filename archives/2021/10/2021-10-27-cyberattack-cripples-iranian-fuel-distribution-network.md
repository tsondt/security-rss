Title: Cyberattack Cripples Iranian Fuel Distribution Network
Date: 2021-10-27T13:04:20+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Critical Infrastructure;Hacks;Web Security
Slug: 2021-10-27-cyberattack-cripples-iranian-fuel-distribution-network

[Source](https://threatpost.com/cyberattack-cripples-iranian-fuel-distribution-network/175794/){:target="_blank" rel="noopener"}

> The incident triggered shutdowns at pumps across the country as attackers flashed the phone number of Supreme Leader Ali Khamenei across video screens. [...]
