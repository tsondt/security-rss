Title: BlackMatter ransomware victims quietly helped using secret decryptor
Date: 2021-10-24T11:27:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-24-blackmatter-ransomware-victims-quietly-helped-using-secret-decryptor

[Source](https://www.bleepingcomputer.com/news/security/blackmatter-ransomware-victims-quietly-helped-using-secret-decryptor/){:target="_blank" rel="noopener"}

> Cybersecurity firm Emsisoft has been secretly decrypting BlackMatter ransomware victims since this summer, saving victims millions of dollars. [...]
