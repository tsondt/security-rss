Title: How Etsy uses reCAPTCHA Enterprise to stop fraud and not customers
Date: 2021-10-05T16:00:00+00:00
Author: Ivan Tse
Category: GCP Security
Tags: Retail;Identity & Security
Slug: 2021-10-05-how-etsy-uses-recaptcha-enterprise-to-stop-fraud-and-not-customers

[Source](https://cloud.google.com/blog/products/identity-security/online-retailer-etsy-stops-fraud-with-recaptcha-enterprise/){:target="_blank" rel="noopener"}

> At Etsy.com, our mission is to Keep Commerce Human by providing a global marketplace that connects 5.2 million sellers with more than 90.5 million active buyers looking for unique items with a human touch. For our vibrant global community to continue to grow and thrive, interactions on the platform must be safe, private, and secure. Like many other online businesses, Etsy saw a sharp increase in traffic over the past year as people turned to e-commerce during the COVID-19 pandemic. With this surge, we wanted to get ahead of any potential challenges that could impact our brand, revenue, and customers. [...]
