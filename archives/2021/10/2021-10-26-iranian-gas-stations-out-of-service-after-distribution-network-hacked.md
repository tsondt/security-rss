Title: Iranian gas stations out of service after distribution network hacked
Date: 2021-10-26T16:24:41-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-26-iranian-gas-stations-out-of-service-after-distribution-network-hacked

[Source](https://www.bleepingcomputer.com/news/security/iranian-gas-stations-out-of-service-after-distribution-network-hacked/){:target="_blank" rel="noopener"}

> Gas stations from the National Iranian Oil Products Distribution Company (NIOPDC) have stopped working today due to what appears to be a cyberattack that affected the entire distribution network. [...]
