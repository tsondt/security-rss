Title: Twitter Suspends Accounts Used to Snare Security Researchers
Date: 2021-10-18T16:23:21+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;Web Security
Slug: 2021-10-18-twitter-suspends-accounts-used-to-snare-security-researchers

[Source](https://threatpost.com/twitter-suspends-security-researchers/175524/){:target="_blank" rel="noopener"}

> The accounts were used to catfish security researchers into downloading malware in a long-running cyber-espionage campaign attributed to North Korea. [...]
