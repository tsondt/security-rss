Title: Pacific City Bank discloses ransomware attack claimed by AvosLocker
Date: 2021-10-11T05:18:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-11-pacific-city-bank-discloses-ransomware-attack-claimed-by-avoslocker

[Source](https://www.bleepingcomputer.com/news/security/pacific-city-bank-discloses-ransomware-attack-claimed-by-avoslocker/){:target="_blank" rel="noopener"}

> Pacific City Bank (PCB), one of the largest Korean-American community banking service providers in America, has disclosed a ransomware incident that took place last month. [...]
