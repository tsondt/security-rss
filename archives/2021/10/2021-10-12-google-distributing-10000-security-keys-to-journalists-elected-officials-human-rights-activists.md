Title: Google distributing 10,000 security keys to journalists, elected officials, human rights activists
Date: 2021-10-12T16:10:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-12-google-distributing-10000-security-keys-to-journalists-elected-officials-human-rights-activists

[Source](https://portswigger.net/daily-swig/google-distributing-10-000-security-keys-to-journalists-elected-officials-human-rights-activists){:target="_blank" rel="noopener"}

> Global initiative ‘will definitely prevent some cyber-attacks’, says expert [...]
