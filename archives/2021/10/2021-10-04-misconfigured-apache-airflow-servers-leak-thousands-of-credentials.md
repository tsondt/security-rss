Title: Misconfigured Apache Airflow servers leak thousands of credentials
Date: 2021-10-04T10:00:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Software
Slug: 2021-10-04-misconfigured-apache-airflow-servers-leak-thousands-of-credentials

[Source](https://www.bleepingcomputer.com/news/security/misconfigured-apache-airflow-servers-leak-thousands-of-credentials/){:target="_blank" rel="noopener"}

> While investigating a misconfiguration flaw in Apache Airflow, researchers discovered many exposed instances over the web leaking sensitive information, including credentials, from well-known tech companies. Apache Airflow is a popular open-source workflow management platform for organizing and managing tasks. [...]
