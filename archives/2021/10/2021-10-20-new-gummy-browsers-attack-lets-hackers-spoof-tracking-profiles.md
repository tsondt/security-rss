Title: New Gummy Browsers attack lets hackers spoof tracking profiles
Date: 2021-10-20T09:49:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-20-new-gummy-browsers-attack-lets-hackers-spoof-tracking-profiles

[Source](https://www.bleepingcomputer.com/news/security/new-gummy-browsers-attack-lets-hackers-spoof-tracking-profiles/){:target="_blank" rel="noopener"}

> University researchers in the US have developed a new fingerprint capturing and browser spoofing attack called Gummy Browsers. They warn how easy the attack is to carry out and the severe implications it can have. [...]
