Title: Newer PurpleFox botnet variants leverage WebSockets for coms
Date: 2021-10-20T08:39:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-20-newer-purplefox-botnet-variants-leverage-websockets-for-coms

[Source](https://www.bleepingcomputer.com/news/security/newer-purplefox-botnet-variants-leverage-websockets-for-coms/){:target="_blank" rel="noopener"}

> The PurpleFox botnet has refreshed its arsenal with new vulnerability exploits and dropped payloads, now also leveraging WebSockets for C2 bidirectional communication. [...]
