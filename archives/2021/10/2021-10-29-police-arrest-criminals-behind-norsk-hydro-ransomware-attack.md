Title: Police arrest criminals behind Norsk Hydro ransomware attack
Date: 2021-10-29T05:07:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2021-10-29-police-arrest-criminals-behind-norsk-hydro-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/police-arrest-criminals-behind-norsk-hydro-ransomware-attack/){:target="_blank" rel="noopener"}

> The Europol has announced the arrest of 12 individuals who are believed to be linked to ransomware attacks against 1,800 victims in 71 countries. [...]
