Title: Google Chromebooks failing to enroll due to network issue
Date: 2021-10-29T05:53:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-29-google-chromebooks-failing-to-enroll-due-to-network-issue

[Source](https://www.bleepingcomputer.com/news/security/google-chromebooks-failing-to-enroll-due-to-network-issue/){:target="_blank" rel="noopener"}

> Since Thursday evening, Google has been investigating reports of customers having issues enrolling their Chromebooks with a network error. [...]
