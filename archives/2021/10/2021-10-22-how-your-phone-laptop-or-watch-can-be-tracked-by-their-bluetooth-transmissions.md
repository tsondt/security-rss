Title: How your phone, laptop, or watch can be tracked by their Bluetooth transmissions
Date: 2021-10-22T06:50:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-10-22-how-your-phone-laptop-or-watch-can-be-tracked-by-their-bluetooth-transmissions

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/22/bluetooth_tracking_device/){:target="_blank" rel="noopener"}

> Unique fingerprints lurk in radio signals more often than not, it seems Over the past few years, mobile devices have become increasingly chatty over the Bluetooth Low Energy (BLE) protocol and this turns out to be a somewhat significant privacy risk.... [...]
