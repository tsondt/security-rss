Title: SQL injection flaw in billing software app tied to US ransomware infection
Date: 2021-10-26T14:54:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-26-sql-injection-flaw-in-billing-software-app-tied-to-us-ransomware-infection

[Source](https://portswigger.net/daily-swig/sql-injection-flaw-in-billing-software-app-tied-to-us-ransomware-infection){:target="_blank" rel="noopener"}

> BillQuick customers blindsided by recently patched web security flaw [...]
