Title: German investigators identify REvil ransomware gang core member
Date: 2021-10-28T07:26:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-28-german-investigators-identify-revil-ransomware-gang-core-member

[Source](https://www.bleepingcomputer.com/news/security/german-investigators-identify-revil-ransomware-gang-core-member/){:target="_blank" rel="noopener"}

> German investigators have reportedly identified a Russian man named Nikolay K. whom they believe to be one of REvil ransomware gang's core members, one of the most notorious and successful ransomware groups in recent years. [...]
