Title: US gov claims ransomware 'earned' $590m in the first half of 2021 alone – mostly in Bitcoin
Date: 2021-10-18T04:33:40+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-18-us-gov-claims-ransomware-earned-590m-in-the-first-half-of-2021-alone-mostly-in-bitcoin

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/18/fincen_ransomware_report/){:target="_blank" rel="noopener"}

> Names and bars crypto exchange SUEX, warns paying ransoms could spell trouble Ransomware extracted at least $590 million for the miscreants who create and distribute it in the first half of 2021 alone – more than the $416 million tracked in all of 2020, according to the US government’s Financial Crimes Enforcement Network (FinCEN). Total ransomware-related financial activity may have reached $5.2 billion.... [...]
