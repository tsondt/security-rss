Title: WhatsApp's got your back(ups) with encryption for stored messages
Date: 2021-10-14T20:11:46+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-10-14-whatsapps-got-your-backups-with-encryption-for-stored-messages

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/14/whatsapps_backups_encryption/){:target="_blank" rel="noopener"}

> Global messaging giant extends security and privacy to Google Drive and Apple iCloud Facebook's WhatsApp on Thursday began a global rollout of end-to-end (E2E) encryption for message backups, which offers Android and iOS users with the ability to protect WhatsApp messages stored in Google Drive and Apple iCloud.... [...]
