Title: Microsoft releases Linux version of the Windows Sysmon tool
Date: 2021-10-14T13:44:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Linux;Security;Software
Slug: 2021-10-14-microsoft-releases-linux-version-of-the-windows-sysmon-tool

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-releases-linux-version-of-the-windows-sysmon-tool/){:target="_blank" rel="noopener"}

> Microsoft has released a Linux version of the very popular Sysmon system monitoring utility for Windows, allowing Linux administrators to monitor devices for malicious activity. [...]
