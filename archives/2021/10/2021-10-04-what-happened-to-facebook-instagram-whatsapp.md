Title: What Happened to Facebook, Instagram, & WhatsApp?
Date: 2021-10-04T19:05:06+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;60 Minutes;BGP;Border Gateway Protocol;CBS;Doug Madory;Facebook outage;Frances Haugen;Instagram outage;Kentik;WhatsApp outage
Slug: 2021-10-04-what-happened-to-facebook-instagram-whatsapp

[Source](https://krebsonsecurity.com/2021/10/what-happened-to-facebook-instagram-whatsapp/){:target="_blank" rel="noopener"}

> Facebook and its sister properties Instagram and WhatsApp are suffering from ongoing, global outages. We don’t yet know why this happened, but the how is clear: Earlier this morning, something inside Facebook caused the company to revoke key digital records that tell computers and other Internet-enabled devices how to find these destinations online. Kentik’s view of the Facebook, Instagram and WhatsApp outage. Doug Madory is director of internet analysis at Kentik, a San Francisco-based network monitoring company. Madory said at approximately 11:39 a.m. ET today (15:39 UTC), someone at Facebook caused an update to be made to the company’s Border [...]
