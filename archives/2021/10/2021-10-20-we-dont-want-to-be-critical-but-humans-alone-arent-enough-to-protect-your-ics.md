Title: We don’t want to be critical, but humans alone aren’t enough to protect your ICS
Date: 2021-10-20T18:00:04+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-10-20-we-dont-want-to-be-critical-but-humans-alone-arent-enough-to-protect-your-ics

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/20/protect_your_ics/){:target="_blank" rel="noopener"}

> If you want to know the solution, join this Regcast Sponsored We know for sure that ransomware attackers and sundry dark forces want to break into critical infrastructure. Ransomware attacks on industrial environments have increased by 500 per cent since 2018.... [...]
