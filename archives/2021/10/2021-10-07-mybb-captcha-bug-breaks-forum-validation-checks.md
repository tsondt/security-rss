Title: MyBB CAPTCHA bug breaks forum validation checks
Date: 2021-10-07T10:22:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-07-mybb-captcha-bug-breaks-forum-validation-checks

[Source](https://portswigger.net/daily-swig/mybb-captcha-bug-breaks-forum-validation-checks){:target="_blank" rel="noopener"}

> Forum owners can apply a workaround until a full fix is released [...]
