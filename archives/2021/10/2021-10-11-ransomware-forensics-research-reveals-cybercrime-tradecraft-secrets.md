Title: Ransomware forensics research reveals cybercrime tradecraft secrets
Date: 2021-10-11T15:25:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-11-ransomware-forensics-research-reveals-cybercrime-tradecraft-secrets

[Source](https://portswigger.net/daily-swig/ransomware-forensics-research-reveals-cybercrime-tradecraft-secrets){:target="_blank" rel="noopener"}

> Resident REvil [...]
