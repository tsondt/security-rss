Title: Japanese messaging giant Line admits it mishandled user data, promises to do better
Date: 2021-10-19T01:27:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-19-japanese-messaging-giant-line-admits-it-mishandled-user-data-promises-to-do-better

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/19/line_data_governance_report/){:target="_blank" rel="noopener"}

> Sent user data to China without once thinking Beijing might decide to snoop, lied about server location Line, the Japan-based messaging and payments app with millions of users around Southeast Asia, has conceded that its data protection regimes had multiple shortcomings, and therefore put users' personal information at risk.... [...]
