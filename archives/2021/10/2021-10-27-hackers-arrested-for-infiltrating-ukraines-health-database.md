Title: Hackers arrested for ‘infiltrating’ Ukraine’s health database
Date: 2021-10-27T12:15:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2021-10-27-hackers-arrested-for-infiltrating-ukraines-health-database

[Source](https://www.bleepingcomputer.com/news/security/hackers-arrested-for-infiltrating-ukraine-s-health-database/){:target="_blank" rel="noopener"}

> The Security Service of Ukraine (SSU) has arrested a team of actors who illegally infiltrated the information system of the National Health Service of Ukraine (NHSU) and entered false vaccination entries for other people. [...]
