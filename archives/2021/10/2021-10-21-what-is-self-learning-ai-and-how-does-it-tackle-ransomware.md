Title: What is self-learning AI and how does it tackle ransomware?
Date: 2021-10-21T07:30:09+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2021-10-21-what-is-self-learning-ai-and-how-does-it-tackle-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/21/what_is_selflearning_ai_and/){:target="_blank" rel="noopener"}

> Darktrace: Why you need defence that operates at machine speed Sponsored There used to be two certainties in life - death and taxes - but thanks to online crooks around the world, there's a third: ransomware. This attack mechanism continues to gain traction because of its phenomenal success. Despite admonishments from governments, victims continue to pay up using low-friction cryptocurrency channels, emboldening criminal groups even further.... [...]
