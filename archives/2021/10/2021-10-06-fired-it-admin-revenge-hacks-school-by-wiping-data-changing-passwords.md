Title: Fired IT admin revenge-hacks school by wiping data, changing passwords
Date: 2021-10-06T03:34:35-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-06-fired-it-admin-revenge-hacks-school-by-wiping-data-changing-passwords

[Source](https://www.bleepingcomputer.com/news/security/fired-it-admin-revenge-hacks-school-by-wiping-data-changing-passwords/){:target="_blank" rel="noopener"}

> A 29-year old wiped data on systems of a secondary school in the U.K. and changed the passwords at an IT company, in retaliatory cyber attacks for being fired. [...]
