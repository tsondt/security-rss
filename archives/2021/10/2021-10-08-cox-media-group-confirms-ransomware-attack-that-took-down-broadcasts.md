Title: Cox Media Group confirms ransomware attack that took down broadcasts
Date: 2021-10-08T15:59:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-08-cox-media-group-confirms-ransomware-attack-that-took-down-broadcasts

[Source](https://www.bleepingcomputer.com/news/security/cox-media-group-confirms-ransomware-attack-that-took-down-broadcasts/){:target="_blank" rel="noopener"}

> American media conglomerate Cox Media Group (CMG) confirmed that it was hit by a ransomware attack that took down live TV and radio broadcast streams in June 2021. [...]
