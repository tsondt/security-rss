Title: FIN7 Lures Unwitting Security Pros to Carry Out Ransomware Attacks
Date: 2021-10-22T19:59:23+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-10-22-fin7-lures-unwitting-security-pros-to-carry-out-ransomware-attacks

[Source](https://threatpost.com/fin7-security-pros-ransomware-attacks/175681/){:target="_blank" rel="noopener"}

> The infamous Carbanak operator is looking to juice its ransomware game by recruiting IT staff to its fake Bastion Secure "pen-testing" company. [...]
