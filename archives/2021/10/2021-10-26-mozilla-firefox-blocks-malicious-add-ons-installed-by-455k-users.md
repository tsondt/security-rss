Title: Mozilla Firefox Blocks Malicious Add-Ons Installed by 455K Users
Date: 2021-10-26T15:44:39+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-10-26-mozilla-firefox-blocks-malicious-add-ons-installed-by-455k-users

[Source](https://threatpost.com/mozilla-firefox-blocks-malicious-add-ons-installed-by-455k-users/175745/){:target="_blank" rel="noopener"}

> The misbehaving Firefox add-ons were misusing an API that controls how Firefox connects to the internet. [...]
