Title: Chinese hackers use Windows zero-day to attack defense, IT firms
Date: 2021-10-12T14:01:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-10-12-chinese-hackers-use-windows-zero-day-to-attack-defense-it-firms

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-use-windows-zero-day-to-attack-defense-it-firms/){:target="_blank" rel="noopener"}

> A Chinese-speaking hacking group exploited a zero-day vulnerability in the Windows Win32k kernel driver to deploy a previously unknown remote access trojan (RAT). [...]
