Title: Sinclair TV network crippled by potential ransomware attack
Date: 2021-10-18T05:52:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-18-sinclair-tv-network-crippled-by-potential-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/sinclair-tv-network-crippled-by-potential-ransomware-attack/){:target="_blank" rel="noopener"}

> TV stations owned by the Sinclair Broadcast Group broadcast television company went down over the weekend across the US, with multiple sources telling BleepingComputer the downtime was caused by a ransomware attack. [...]
