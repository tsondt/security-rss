Title: Mandating a Zero-Trust Approach for Software Supply Chains
Date: 2021-10-13T13:22:41+00:00
Author: Sounil Yu
Category: Threatpost
Tags: InfoSec Insider;Vulnerabilities
Slug: 2021-10-13-mandating-a-zero-trust-approach-for-software-supply-chains

[Source](https://threatpost.com/mandate-zero-trust-software-supply-chains/175333/){:target="_blank" rel="noopener"}

> Sounil Yu, CISO at JupiterOne, discusses software bills of materials (SBOMs) and the need for a shift in thinking about securing software supply chains. [...]
