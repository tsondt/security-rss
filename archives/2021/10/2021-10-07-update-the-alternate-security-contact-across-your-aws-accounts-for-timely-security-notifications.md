Title: Update the alternate security contact across your AWS accounts for timely security notifications
Date: 2021-10-07T17:01:43+00:00
Author: Steven Bedeker
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Account management;Alternate contacts;Alternate security contact;Full name field;Security Blog
Slug: 2021-10-07-update-the-alternate-security-contact-across-your-aws-accounts-for-timely-security-notifications

[Source](https://aws.amazon.com/blogs/security/update-the-alternate-security-contact-across-your-aws-accounts-for-timely-security-notifications/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) will send you important security notifications from time to time related to your account. From a security perspective, the ability for AWS Security to reach you in a timely manner is important whether you have one AWS account or thousands. These notifications could include alerts from AWS Security for potentially fraudulent activity [...]
