Title: Suspected Chinese hackers behind attacks on ten Israeli hospitals
Date: 2021-10-18T10:55:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-18-suspected-chinese-hackers-behind-attacks-on-ten-israeli-hospitals

[Source](https://www.bleepingcomputer.com/news/security/suspected-chinese-hackers-behind-attacks-on-ten-israeli-hospitals/){:target="_blank" rel="noopener"}

> A joint announcement from the Ministry of Health and the National Cyber Directorate in Israel describes a spike in ransomware attacks over the weekend that targeted the systems of nine health institutes in the country. [...]
