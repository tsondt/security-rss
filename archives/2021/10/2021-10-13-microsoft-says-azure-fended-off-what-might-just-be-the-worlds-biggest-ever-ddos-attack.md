Title: Microsoft says Azure fended off what might just be the world's biggest-ever DDoS attack
Date: 2021-10-13T07:00:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-13-microsoft-says-azure-fended-off-what-might-just-be-the-worlds-biggest-ever-ddos-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/13/azure_deflects_massive_ddos/){:target="_blank" rel="noopener"}

> Much of the 2.4Tbit/sec came from across Asia and targeted a single Euro-customer Microsoft claims its Azure cloud has fended off the largest DDOS attack it's detected, which clocked in at 2.4Tbit/sec.... [...]
