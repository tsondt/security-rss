Title: Russia-based criminals are still the UK's number 1 cyber-foe, NSO Group's wares a 'red flag' says NCSC chief
Date: 2021-10-11T15:52:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-11-russia-based-criminals-are-still-the-uks-number-1-cyber-foe-nso-groups-wares-a-red-flag-says-ncsc-chief

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/11/ncsc_ceo_speech_chatham_house/){:target="_blank" rel="noopener"}

> Chatham House speech targets non-state baddies as well as grey zone and nation states A new national cyber strategy will be launched by year-end, the National Cyber Security Centre's chief exec has promised – while calling out spyware vendor NSO Group as a "red flag" for the UK infosec community.... [...]
