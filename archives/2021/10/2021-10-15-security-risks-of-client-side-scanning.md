Title: Security Risks of Client-Side Scanning
Date: 2021-10-15T14:30:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;Apple;backdoors;national security policy;Schneier news;security policies
Slug: 2021-10-15-security-risks-of-client-side-scanning

[Source](https://www.schneier.com/blog/archives/2021/10/security-risks-of-client-side-scanning.html){:target="_blank" rel="noopener"}

> Even before Apple made its announcement, law enforcement shifted their battle for backdoors to client-side scanning. The idea is that they wouldn’t touch the cryptography, but instead eavesdrop on communications and systems before encryption or after decryption. It’s not a cryptographic backdoor, but it’s still a backdoor — and brings with it all the insecurities of a backdoor. I’m part of a group of cryptographers that has just published a paper discussing the security risks of such a system. (It’s substantially the same group that wrote a similar paper about key escrow in 1997, and other “exceptional access” proposals in [...]
