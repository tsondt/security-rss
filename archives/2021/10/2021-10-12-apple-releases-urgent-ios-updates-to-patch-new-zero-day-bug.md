Title: Apple Releases Urgent iOS Updates to Patch New Zero-Day Bug
Date: 2021-10-12T15:17:38+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: 2021-10-12-apple-releases-urgent-ios-updates-to-patch-new-zero-day-bug

[Source](https://threatpost.com/apple-urgent-ios-updates-zero-day/175419/){:target="_blank" rel="noopener"}

> The bug is under attack. Within hours of the patch release, a researcher published POC code, calling it a "great" flaw that can be used for jailbreaks and local privilege escalation. [...]
