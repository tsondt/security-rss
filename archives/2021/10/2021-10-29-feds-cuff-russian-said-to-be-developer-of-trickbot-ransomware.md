Title: Feds cuff Russian said to be developer of 'Trickbot' ransomware
Date: 2021-10-29T05:58:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-29-feds-cuff-russian-said-to-be-developer-of-trickbot-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/29/trickbot_arrest/){:target="_blank" rel="noopener"}

> Indictment reveals org behind banking trojan even had nifty jobs titles like 'Malware Manager' The US Department of Justice claims it's arrested a member of a gang that deployed the Trickbot ransomware.... [...]
