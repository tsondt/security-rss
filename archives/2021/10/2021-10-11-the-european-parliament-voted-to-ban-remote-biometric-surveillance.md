Title: The European Parliament Voted to Ban Remote Biometric Surveillance
Date: 2021-10-11T12:49:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;biometrics;EU;privacy;surveillance
Slug: 2021-10-11-the-european-parliament-voted-to-ban-remote-biometric-surveillance

[Source](https://www.schneier.com/blog/archives/2021/10/the-european-parliament-voted-to-ban-remote-biometric-surveillance.html){:target="_blank" rel="noopener"}

> It’s not actually banned in the EU yet — the legislative process is much more complicated than that — but it’s a step: a total ban on biometric mass surveillance. To respect “privacy and human dignity,” MEPs said that EU lawmakers should pass a permanent ban on the automated recognition of individuals in public spaces, saying citizens should only be monitored when suspected of a crime. The parliament has also called for a ban on the use of private facial recognition databases — such as the controversial AI system created by U.S. startup Clearview (also already in use by some [...]
