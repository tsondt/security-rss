Title: Italian celebs' data exposed in ransomware attack on SIAE
Date: 2021-10-22T10:06:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-22-italian-celebs-data-exposed-in-ransomware-attack-on-siae

[Source](https://www.bleepingcomputer.com/news/security/italian-celebs-data-exposed-in-ransomware-attack-on-siae/){:target="_blank" rel="noopener"}

> The Italian data protection authority Garante per la Protezione dei Dati Personali (GPDP) has announced an investigation into a data breach of the country's copyright protection agency. [...]
