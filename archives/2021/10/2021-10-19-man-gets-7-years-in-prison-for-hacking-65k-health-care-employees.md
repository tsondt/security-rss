Title: Man gets 7 years in prison for hacking 65K health care employees
Date: 2021-10-19T09:17:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-19-man-gets-7-years-in-prison-for-hacking-65k-health-care-employees

[Source](https://www.bleepingcomputer.com/news/security/man-gets-7-years-in-prison-for-hacking-65k-health-care-employees/){:target="_blank" rel="noopener"}

> Justin Sean Johnson, also known as TheDearthStar and Dearthy Star, was sentenced this week to seven years in prison for the 2014 hack of the health care provider and insurer University of Pittsburgh Medical Center (UPMC). [...]
