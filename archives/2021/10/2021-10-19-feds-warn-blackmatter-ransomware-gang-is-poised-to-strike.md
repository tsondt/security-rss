Title: Feds Warn BlackMatter Ransomware Gang is Poised to Strike
Date: 2021-10-19T13:21:43+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware
Slug: 2021-10-19-feds-warn-blackmatter-ransomware-gang-is-poised-to-strike

[Source](https://threatpost.com/feds-warn-blackmatter-ransomware-gang-is-poised-to-strike/175567/){:target="_blank" rel="noopener"}

> An advisory by the CISA, FBI and NSA reveals hallmark tactics of and shares defense tips against the cybercriminal group that’s picked up where its predecessor DarkSide left off. [...]
