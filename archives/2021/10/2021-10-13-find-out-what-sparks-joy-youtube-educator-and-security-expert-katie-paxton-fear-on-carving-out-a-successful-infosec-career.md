Title: ‘Find out what sparks joy’ – YouTube educator and security expert Katie Paxton-Fear on carving out a successful infosec career
Date: 2021-10-13T13:57:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-13-find-out-what-sparks-joy-youtube-educator-and-security-expert-katie-paxton-fear-on-carving-out-a-successful-infosec-career

[Source](https://portswigger.net/daily-swig/find-out-what-sparks-joy-youtube-educator-and-security-expert-katie-paxton-fear-on-carving-out-a-successful-infosec-career){:target="_blank" rel="noopener"}

> ‘Never stop learning’, Swig readers told during Q&A session [...]
