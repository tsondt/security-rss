Title: Prototype pollution vulnerabilities rife among high-traffic websites, study finds
Date: 2021-10-01T15:03:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-01-prototype-pollution-vulnerabilities-rife-among-high-traffic-websites-study-finds

[Source](https://portswigger.net/daily-swig/prototype-pollution-vulnerabilities-rife-among-high-traffic-websites-study-finds){:target="_blank" rel="noopener"}

> Technique is exploitable at scale because it’s so overlooked, speculate researchers [...]
