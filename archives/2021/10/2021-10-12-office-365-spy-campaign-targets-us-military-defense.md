Title: Office 365 Spy Campaign Targets US Military Defense
Date: 2021-10-12T17:46:41+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Web Security
Slug: 2021-10-12-office-365-spy-campaign-targets-us-military-defense

[Source](https://threatpost.com/military-defense-spy-campaign/175425/){:target="_blank" rel="noopener"}

> An Iran-linked group is taking aim at makers of drones and satellites, Persian Gulf ports and maritime shipping companies, among others. [...]
