Title: Millions of Android Users Scammed in SMS Fraud Driven by Tik-Tok Ads
Date: 2021-10-26T12:09:49+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: 2021-10-26-millions-of-android-users-scammed-in-sms-fraud-driven-by-tik-tok-ads

[Source](https://threatpost.com/android-scammed-sms-fraud-tik-tok/175739/){:target="_blank" rel="noopener"}

> UltimaSMS leverages at least 151 apps that have been downloaded collectively more than 10 million times, to extort money through a fake premium SMS subscription service. [...]
