Title: US retailer Neiman Marcus notifies 4.6 million customers of data breach
Date: 2021-10-01T11:30:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-01-us-retailer-neiman-marcus-notifies-46-million-customers-of-data-breach

[Source](https://portswigger.net/daily-swig/us-retailer-neiman-marcus-notifies-4-6-million-customers-of-data-breach){:target="_blank" rel="noopener"}

> Department store chain forces password reset after discovering 2020 incident last month [...]
