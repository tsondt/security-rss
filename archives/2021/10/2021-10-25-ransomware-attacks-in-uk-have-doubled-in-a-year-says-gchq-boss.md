Title: Ransomware attacks in UK have doubled in a year, says GCHQ boss
Date: 2021-10-25T16:05:40+00:00
Author: Rajeev Syal Home affairs editor
Category: The Guardian
Tags: GCHQ;Malware;Cybercrime;Data and computer security;Technology;UK news
Slug: 2021-10-25-ransomware-attacks-in-uk-have-doubled-in-a-year-says-gchq-boss

[Source](https://www.theguardian.com/uk-news/2021/oct/25/ransomware-attacks-in-uk-have-doubled-in-a-year-says-gchq-boss){:target="_blank" rel="noopener"}

> Jeremy Fleming says ransomware is proliferating as it is ‘largely uncontested’ and highly profitable The head of the UK spy agency GCHQ has disclosed that the number of ransomware attacks on British institutions has doubled in the past year. Jeremy Fleming, the director of GCHQ, said locking files and data on a user’s computer and demanding payment for their release had become increasingly popular among criminals because it was “largely uncontested” and highly profitable. Continue reading... [...]
