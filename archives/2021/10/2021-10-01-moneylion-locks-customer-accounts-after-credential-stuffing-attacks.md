Title: MoneyLion locks customer accounts after credential stuffing attacks
Date: 2021-10-01T12:38:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-01-moneylion-locks-customer-accounts-after-credential-stuffing-attacks

[Source](https://www.bleepingcomputer.com/news/security/moneylion-locks-customer-accounts-after-credential-stuffing-attacks/){:target="_blank" rel="noopener"}

> The banking and investing platform MoneyLion had to lock customer accounts that were breached in credential stuffing attacks over the summer, in June and July. [...]
