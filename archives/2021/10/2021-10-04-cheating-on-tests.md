Title: Cheating on Tests
Date: 2021-10-04T14:40:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Bluetooth;cheating;India;schools
Slug: 2021-10-04-cheating-on-tests

[Source](https://www.schneier.com/blog/archives/2021/10/cheating-on-tests.html){:target="_blank" rel="noopener"}

> Interesting story of test-takers in India using Bluetooth-connected flip-flops to communicate with accomplices while taking a test. What’s interesting is how this cheating was discovered. It’s not that someone noticed the communication devices. It’s that the proctors noticed that cheating test takers were acting hinky. [...]
