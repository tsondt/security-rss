Title: Better late than never: Microsoft rolls out a public preview of E2EE in Teams calls
Date: 2021-10-22T17:28:41+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-10-22-better-late-than-never-microsoft-rolls-out-a-public-preview-of-e2ee-in-teams-calls

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/22/e2ee_teams_microsoft/){:target="_blank" rel="noopener"}

> Only for one-to-one voice and video, mind Microsoft has finally kicked off the rollout of end-to-end-encryption (E2EE) in its Teams collaboration platform with a public preview of E2EE for one-to-one calls.... [...]
