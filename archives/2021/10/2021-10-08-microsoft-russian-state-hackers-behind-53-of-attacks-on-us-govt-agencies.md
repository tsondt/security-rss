Title: Microsoft: Russian state hackers behind 53% of attacks on US govt agencies
Date: 2021-10-08T07:04:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-10-08-microsoft-russian-state-hackers-behind-53-of-attacks-on-us-govt-agencies

[Source](https://www.bleepingcomputer.com/news/security/microsoft-russian-state-hackers-behind-53-percent-of-attacks-on-us-govt-agencies/){:target="_blank" rel="noopener"}

> Microsoft says that Russian-sponsored hacking groups are increasingly targeting US government agencies, with roughly 58% of all nation-state attacks observed by Microsoft between July 2020 and June 2021 coming from Russia. [...]
