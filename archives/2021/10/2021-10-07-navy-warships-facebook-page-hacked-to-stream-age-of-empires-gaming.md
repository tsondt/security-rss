Title: Navy Warship’s Facebook Page Hacked to Stream ‘Age of Empires’ Gaming
Date: 2021-10-07T20:27:11+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Facebook;Hacks;Web Security
Slug: 2021-10-07-navy-warships-facebook-page-hacked-to-stream-age-of-empires-gaming

[Source](https://threatpost.com/navy-warships-facebook-age-empires-gaming/175409/){:target="_blank" rel="noopener"}

> The destroyer-class USS Kidd streamed hours of game play in a funny incident that has serious cybersecurity ramifications. [...]
