Title: War-Driving Technique Allows Wi-Fi Password-Cracking at Scale
Date: 2021-10-27T17:00:37+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;Web Security
Slug: 2021-10-27-war-driving-technique-allows-wi-fi-password-cracking-at-scale

[Source](https://threatpost.com/war-driving-wi-fi-password-cracking/175817/){:target="_blank" rel="noopener"}

> A researcher was able to crack 70 percent of the gathered hashes in an experiment in a residential neighborhood. [...]
