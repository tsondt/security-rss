Title: Twitter employees required to use security keys after 2020 hack
Date: 2021-10-27T13:26:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-27-twitter-employees-required-to-use-security-keys-after-2020-hack

[Source](https://www.bleepingcomputer.com/news/security/twitter-employees-required-to-use-security-keys-after-2020-hack/){:target="_blank" rel="noopener"}

> Twitter rolled out security keys to its entire workforce and made two-factor authentication (2FA) mandatory for accessing internal systems following last year's hack. [...]
