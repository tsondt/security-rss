Title: Making public services more accessible with AI-powered translations from Google Cloud
Date: 2021-10-08T16:00:00+00:00
Author: Dominik Steiner
Category: GCP Security
Tags: Public Sector;Identity & Security;Google Cloud;AI & Machine Learning
Slug: 2021-10-08-making-public-services-more-accessible-with-ai-powered-translations-from-google-cloud

[Source](https://cloud.google.com/blog/products/ai-machine-learning/making-public-services-more-accessible-with-ai-based-translations/){:target="_blank" rel="noopener"}

> Editor’s note : This article has originally been published on our German blog. Imagine you’ve done your due diligence for your upcoming trip to the local authority office. You’ve filled out the relevant forms, booked the appointment, found the correct points of contact. But, as much as you’ve prepared, there’s one aspect of your visit you can’t control: you and the clerks don’t speak the same language. In science-fiction, this wouldn’t be a problem. Star Trek has the universal translator. In Douglas Adams’ novel The Hitchhiker’s Guide to the Galaxy, there’s the telepathic Babel Fish, which enables everyone to understand [...]
