Title: Mozilla blocks malicious add-ons installed by 455K Firefox users
Date: 2021-10-25T16:08:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-25-mozilla-blocks-malicious-add-ons-installed-by-455k-firefox-users

[Source](https://www.bleepingcomputer.com/news/security/mozilla-blocks-malicious-add-ons-installed-by-455k-firefox-users/){:target="_blank" rel="noopener"}

> Mozilla blocked malicious Firefox add-ons installed by roughly 455,000 users after discovering in early June that they were abusing the proxy API to block Firefox updates. [...]
