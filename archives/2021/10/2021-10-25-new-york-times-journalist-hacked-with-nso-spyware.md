Title: New York Times Journalist Hacked with NSO Spyware
Date: 2021-10-25T18:46:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberweapons;hacking;Israel;spyware
Slug: 2021-10-25-new-york-times-journalist-hacked-with-nso-spyware

[Source](https://www.schneier.com/blog/archives/2021/10/new-york-times-journalist-hacked-with-nso-spyware.html){:target="_blank" rel="noopener"}

> Citizen Lab is reporting that a New York Times journalist was hacked with the NSO Group’s spyware Pegasus, probably by the Saudis. The world needs to do something about these cyberweapons arms manufacturers. This kind of thing isn’t enough; NSO Group is an Israeli company. [...]
