Title: Firefox improves advertising tracker blocking in private browsing
Date: 2021-10-06T06:01:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-06-firefox-improves-advertising-tracker-blocking-in-private-browsing

[Source](https://www.bleepingcomputer.com/news/security/firefox-improves-advertising-tracker-blocking-in-private-browsing/){:target="_blank" rel="noopener"}

> Mozilla says that Firefox users will be better protected from advertising trackers (like Google Analytics scripts) while browsing the Internet in Private Browsing mode and using Strict Tracking Protection. [...]
