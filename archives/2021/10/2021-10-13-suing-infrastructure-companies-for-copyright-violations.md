Title: Suing Infrastructure Companies for Copyright Violations
Date: 2021-10-13T14:47:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;copyright;courts;infrastructure;Internet and society
Slug: 2021-10-13-suing-infrastructure-companies-for-copyright-violations

[Source](https://www.schneier.com/blog/archives/2021/10/suing-infrastructure-companies-for-copyright-violations.html){:target="_blank" rel="noopener"}

> It’s a matter of going after those with deep pockets. From Wired : Cloudflare was sued in November 2018 by Mon Cheri Bridals and Maggie Sottero Designs, two wedding dress manufacturers and sellers that alleged Cloudflare was guilty of contributory copyright infringement because it didn’t terminate services for websites that infringed on the dressmakers’ copyrighted designs.... [Judge] Chhabria noted that the dressmakers have been harmed “by the proliferation of counterfeit retailers that sell knock-off dresses using the plaintiffs’ copyrighted images” and that they have “gone after the infringers in a range of actions, but to no avail — every time [...]
