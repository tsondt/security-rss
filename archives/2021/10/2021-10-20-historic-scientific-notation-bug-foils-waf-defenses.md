Title: Historic scientific notation bug foils WAF defenses
Date: 2021-10-20T15:12:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-20-historic-scientific-notation-bug-foils-waf-defenses

[Source](https://portswigger.net/daily-swig/historic-scientific-notation-bug-foils-waf-defenses){:target="_blank" rel="noopener"}

> AWS WAF and ModSecurity get ‘blinded by science’ [...]
