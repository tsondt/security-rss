Title: Warehouse belonging to Chinese payment terminal manufacturer raided by FBI
Date: 2021-10-27T09:41:22+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-10-27-warehouse-belonging-to-chinese-payment-terminal-manufacturer-raided-by-fbi

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/27/pax_technology_warehouse_raid/){:target="_blank" rel="noopener"}

> PAX Technology devices allegedly infected with malware US feds were spotted raiding a warehouse belonging to Chinese payment terminal manufacturer PAX Technology in Jacksonville, Florida, on Tuesday, with speculation abounding that the machines contained preinstalled malware.... [...]
