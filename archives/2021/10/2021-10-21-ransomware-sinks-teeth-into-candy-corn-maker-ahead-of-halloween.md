Title: Ransomware Sinks Teeth into Candy-Corn Maker Ahead of Halloween
Date: 2021-10-21T11:50:34+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: 2021-10-21-ransomware-sinks-teeth-into-candy-corn-maker-ahead-of-halloween

[Source](https://threatpost.com/ransomware-candy-corn-halloween/175630/){:target="_blank" rel="noopener"}

> Chicago-based Ferrara acknowledged an Oct. 9 attack that encrypted some systems and disrupted production. [...]
