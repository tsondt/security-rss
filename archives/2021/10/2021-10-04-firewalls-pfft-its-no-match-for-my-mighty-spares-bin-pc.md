Title: Firewalls? Pfft – it's no match for my mighty spares-bin PC
Date: 2021-10-04T07:30:04+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-10-04-firewalls-pfft-its-no-match-for-my-mighty-spares-bin-pc

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/04/who_me/){:target="_blank" rel="noopener"}

> You say 'temporary', they hear 'permanent' Who, Me? Start your week with a warning about those temporary emergency hacks that all too often end up permanent in today's edition of Who, Me?... [...]
