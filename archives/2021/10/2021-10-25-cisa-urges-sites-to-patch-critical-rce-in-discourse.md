Title: CISA Urges Sites to Patch Critical RCE in Discourse
Date: 2021-10-25T15:28:27+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-10-25-cisa-urges-sites-to-patch-critical-rce-in-discourse

[Source](https://threatpost.com/cisa-critical-rce-discourse/175705/){:target="_blank" rel="noopener"}

> The patch, urgently rushed out on Friday, is an emergency fix for the widely deployed platform, whose No. 1 most trafficked site is Amazon’s Seller Central. [...]
