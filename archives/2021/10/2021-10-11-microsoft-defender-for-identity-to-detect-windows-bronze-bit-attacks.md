Title: Microsoft Defender for Identity to detect Windows Bronze Bit attacks
Date: 2021-10-11T09:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-11-microsoft-defender-for-identity-to-detect-windows-bronze-bit-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-for-identity-to-detect-windows-bronze-bit-attacks/){:target="_blank" rel="noopener"}

> Microsoft is working on adding support for Bronze Bit attacks detection to Microsoft Defender for Identity to make it easier for Security Operations teams to detect attempts to abuse a Windows Kerberos bug tracked as CVE-2020-17049. [...]
