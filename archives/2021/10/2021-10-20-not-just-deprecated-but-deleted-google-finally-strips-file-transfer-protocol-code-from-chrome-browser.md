Title: Not just deprecated, but deleted: Google finally strips File Transfer Protocol code from Chrome browser
Date: 2021-10-20T13:07:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-10-20-not-just-deprecated-but-deleted-google-finally-strips-file-transfer-protocol-code-from-chrome-browser

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/20/ftp_chrome_95/){:target="_blank" rel="noopener"}

> A death by a thousand cuts The Chromium team has finally done it – File Transfer Protocol (FTP) support is not just deprecated, but stripped from the codebase in the latest stable build of the Chrome browser, version 95.... [...]
