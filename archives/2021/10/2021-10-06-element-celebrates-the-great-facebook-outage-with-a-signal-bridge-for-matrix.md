Title: Element celebrates The Great Facebook Outage with a Signal bridge for Matrix
Date: 2021-10-06T10:27:05+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-10-06-element-celebrates-the-great-facebook-outage-with-a-signal-bridge-for-matrix

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/06/element_signal/){:target="_blank" rel="noopener"}

> Kudos given to Signal. Not so much for Facebook and Whatsapp Interview Matrix-based communications and collaboration app Element has continued its mission to make bridges into the decentralised network a little more commercially acceptable with connectivity for Signal.... [...]
