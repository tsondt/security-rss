Title: Friday Squid Blogging: Squid Game Has a Cryptocurrency
Date: 2021-10-29T21:09:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;squid
Slug: 2021-10-29-friday-squid-blogging-squid-game-has-a-cryptocurrency

[Source](https://www.schneier.com/blog/archives/2021/10/friday-squid-blogging-squid-game-has-a-cryptocurrency.html){:target="_blank" rel="noopener"}

> In what maybe peak hype, Squid Game has its own cryptocurrency. Not in the fictional show, but in real life. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
