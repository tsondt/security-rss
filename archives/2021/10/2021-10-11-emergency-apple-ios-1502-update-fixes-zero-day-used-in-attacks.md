Title: Emergency Apple iOS 15.0.2 update fixes zero-day used in attacks
Date: 2021-10-11T14:48:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Apple
Slug: 2021-10-11-emergency-apple-ios-1502-update-fixes-zero-day-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/emergency-apple-ios-1502-update-fixes-zero-day-used-in-attacks/){:target="_blank" rel="noopener"}

> Apple has released iOS 15.0.2 and iPadOS 15.0.2 to fix a zero-day vulnerability that is actively exploited in the wild in attacks targeting Phones and iPads. [...]
