Title: Podcast: 67% of Orgs Have Been Hit by Ransomware at Least Once
Date: 2021-10-14T13:32:16+00:00
Author: Threatpost
Category: Threatpost
Tags: Malware;Podcasts;Sponsored
Slug: 2021-10-14-podcast-67-of-orgs-have-been-hit-by-ransomware-at-least-once

[Source](https://threatpost.com/podcast-67-percent-orgs-ransomware/175339/){:target="_blank" rel="noopener"}

> Fortinet’s Derek Manky discusses a recent global survey showing that two-thirds of organizations suffered at least one ransomware attack, while half were hit multiple times. [...]
