Title: DocuSign phishing campaign targets low-ranking employees
Date: 2021-10-14T11:33:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-14-docusign-phishing-campaign-targets-low-ranking-employees

[Source](https://www.bleepingcomputer.com/news/security/docusign-phishing-campaign-targets-low-ranking-employees/){:target="_blank" rel="noopener"}

> Phishing actors are following a new trend of targeting non-executive employees but who still have access to valuable areas within an organization. [...]
