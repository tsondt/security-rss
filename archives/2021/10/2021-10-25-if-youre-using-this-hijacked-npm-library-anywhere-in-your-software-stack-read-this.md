Title: If you're using this hijacked NPM library anywhere in your software stack, read this
Date: 2021-10-25T22:13:44+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-10-25-if-youre-using-this-hijacked-npm-library-anywhere-in-your-software-stack-read-this

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/25/in_brief_security/){:target="_blank" rel="noopener"}

> US govt issues alert over JS package downloaded 8m times a week – plus more news from world of infosec In brief The US government's Cybersecurity and Infrastructure Security Agency (CISA) has warned developers that a version of the ua-parser-js JavaScript library, available via NPM, was infected with data-stealing and cryptocurrency-mining malware.... [...]
