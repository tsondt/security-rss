Title: 3.1M Neiman Marcus Customer Card Details Breached
Date: 2021-10-01T17:50:42+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Web Security
Slug: 2021-10-01-31m-neiman-marcus-customer-card-details-breached

[Source](https://threatpost.com/neiman-marcus-customers-breach/175284/){:target="_blank" rel="noopener"}

> Experts say the detection delay of 17 months is a colossal security blunder by the retailer. [...]
