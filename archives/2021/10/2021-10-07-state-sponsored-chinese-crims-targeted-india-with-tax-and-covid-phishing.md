Title: State-sponsored Chinese crims targeted India with tax and COVID phishing
Date: 2021-10-07T06:58:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-10-07-state-sponsored-chinese-crims-targeted-india-with-tax-and-covid-phishing

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/07/apt_41_phishing_schemes_indian_nationals/){:target="_blank" rel="noopener"}

> Blackberry says APT41 gang used lumpen remixes of Microsoft domain names to lure the unwary Blackberry's Research and Intelligence Team has uncovered three phishing schemes targeting Indian nationals, and says a Chinese state-sponsored malware gang is the culprit.... [...]
