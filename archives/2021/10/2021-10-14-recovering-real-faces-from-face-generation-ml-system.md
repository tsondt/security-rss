Title: Recovering Real Faces from Face-Generation ML System
Date: 2021-10-14T14:56:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;de-anonymization;face recognition;privacy
Slug: 2021-10-14-recovering-real-faces-from-face-generation-ml-system

[Source](https://www.schneier.com/blog/archives/2021/10/recovering-real-faces-from-face-generation-ml-system.html){:target="_blank" rel="noopener"}

> New paper: “ This Person (Probably) Exists. Identity Membership Attacks Against GAN Generated Faces. Abstract: Recently, generative adversarial networks (GANs) have achieved stunning realism, fooling even human observers. Indeed, the popular tongue-in-cheek website http://thispersondoesnotexist.com, taunts users with GAN generated images that seem too real to believe. On the other hand, GANs do leak information about their training data, as evidenced by membership attacks recently demonstrated in the literature. In this work, we challenge the assumption that GAN faces really are novel creations, by constructing a successful membership attack of a new kind. Unlike previous works, our attack can accurately discern [...]
