Title: U.S. Ban on Sales of Cyberattack Tools Is Anemic, Experts Warn
Date: 2021-10-21T19:41:22+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Bug Bounty;Privacy;Vulnerabilities
Slug: 2021-10-21-us-ban-on-sales-of-cyberattack-tools-is-anemic-experts-warn

[Source](https://threatpost.com/us-ban-cyberattack-tools-zerodium/175654/){:target="_blank" rel="noopener"}

> Meanwhile, Zerodium's quest to buy VPN exploits is problematic, researchers said. [...]
