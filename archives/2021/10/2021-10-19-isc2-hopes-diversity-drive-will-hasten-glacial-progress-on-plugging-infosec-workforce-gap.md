Title: (ISC)² hopes diversity drive will hasten glacial progress on plugging infosec workforce gap
Date: 2021-10-19T15:55:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-19-isc2-hopes-diversity-drive-will-hasten-glacial-progress-on-plugging-infosec-workforce-gap

[Source](https://portswigger.net/daily-swig/isc-hopes-diversity-drive-will-hasten-glacial-progress-on-plugging-infosec-workforce-gap){:target="_blank" rel="noopener"}

> CEO tells (ISC)2 Security Congress how orgs should rethink hiring strategies [...]
