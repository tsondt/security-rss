Title: Friday Squid Blogging: Squid Game
Date: 2021-10-01T21:22:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;television
Slug: 2021-10-01-friday-squid-blogging-squid-game

[Source](https://www.schneier.com/blog/archives/2021/10/friday-squid-blogging-squid-game.html){:target="_blank" rel="noopener"}

> Netflix has a new series called Squid Game, about people competing in a deadly game for money. It has nothing to do with actual squid. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
