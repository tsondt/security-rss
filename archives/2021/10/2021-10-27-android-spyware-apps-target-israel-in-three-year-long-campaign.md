Title: Android spyware apps target Israel in three-year-long campaign
Date: 2021-10-27T14:52:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-27-android-spyware-apps-target-israel-in-three-year-long-campaign

[Source](https://www.bleepingcomputer.com/news/security/android-spyware-apps-target-israel-in-three-year-long-campaign/){:target="_blank" rel="noopener"}

> A set of seemingly innocuous Android apps have been infecting Israeli users with spyware since 2018, and the campaign continues to this day. [...]
