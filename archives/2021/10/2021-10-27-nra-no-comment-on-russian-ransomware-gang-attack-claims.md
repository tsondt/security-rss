Title: NRA: No comment on Russian ransomware gang attack claims
Date: 2021-10-27T16:37:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-27-nra-no-comment-on-russian-ransomware-gang-attack-claims

[Source](https://www.bleepingcomputer.com/news/security/nra-no-comment-on-russian-ransomware-gang-attack-claims/){:target="_blank" rel="noopener"}

> The Grief ransomware gang claims to have attacked the National Rifle Association (NRA) and released allegedly stolen data as proof of the attack. [...]
