Title: North Korean state hackers start targeting the IT supply chain
Date: 2021-10-26T13:23:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-26-north-korean-state-hackers-start-targeting-the-it-supply-chain

[Source](https://www.bleepingcomputer.com/news/security/north-korean-state-hackers-start-targeting-the-it-supply-chain/){:target="_blank" rel="noopener"}

> North Korean-sponsored Lazarus hacking group has switched focus on new targets and was observed by Kaspersky security researchers expanding its supply chain attack capabilities. [...]
