Title: Sure, you can do Kubernetes at scale. But can you do it securely too?
Date: 2021-10-01T18:00:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-10-01-sure-you-can-do-kubernetes-at-scale-but-can-you-do-it-securely-too

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/01/kubernetes_secure_at_scale/){:target="_blank" rel="noopener"}

> Learn how to do both at KubeSec Enterprise vSummit Sponsored Doing cloud native at enterprise scale is no mean feat, but doing it securely is the real challenge.... [...]
