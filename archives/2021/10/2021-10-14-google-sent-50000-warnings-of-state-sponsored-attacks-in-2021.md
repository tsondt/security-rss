Title: Google sent 50,000 warnings of state-sponsored attacks in 2021
Date: 2021-10-14T11:20:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2021-10-14-google-sent-50000-warnings-of-state-sponsored-attacks-in-2021

[Source](https://www.bleepingcomputer.com/news/security/google-sent-50-000-warnings-of-state-sponsored-attacks-in-2021/){:target="_blank" rel="noopener"}

> Google said today that it sent roughly 50,000 alerts of state-sponsored phishing or hacking attempts to customers during 2021, a considerable increase compared to the previous year. [...]
