Title: Why is Cybersecurity Failing Against Ransomware?
Date: 2021-10-21T13:16:00+00:00
Author: Nate Warfield
Category: Threatpost
Tags: InfoSec Insider;Malware;Vulnerabilities;Web Security
Slug: 2021-10-21-why-is-cybersecurity-failing-against-ransomware

[Source](https://threatpost.com/cybersecurity-failing-ransomware/175637/){:target="_blank" rel="noopener"}

> Hardly a week goes by without another major company falling victim to a ransomware attack. Nate Warfield, CTO at Prevailion, discusses the immense challenges in changing that status quo. [...]
