Title: ‘Inaction isn’t an option’ – US lawmakers back mandatory standards for transport and logistics cybersecurity
Date: 2021-10-29T10:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-29-inaction-isnt-an-option-us-lawmakers-back-mandatory-standards-for-transport-and-logistics-cybersecurity

[Source](https://portswigger.net/daily-swig/inaction-isnt-an-option-us-lawmakers-back-mandatory-standards-for-transport-and-logistics-cybersecurity){:target="_blank" rel="noopener"}

> House Committee on Homeland Security hearing pulls focus on securing ‘planes, trains, and pipelines’ [...]
