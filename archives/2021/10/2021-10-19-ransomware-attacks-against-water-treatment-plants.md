Title: Ransomware Attacks against Water Treatment Plants
Date: 2021-10-19T11:07:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;infrastructure;ransomware;SCADA
Slug: 2021-10-19-ransomware-attacks-against-water-treatment-plants

[Source](https://www.schneier.com/blog/archives/2021/10/ransomware-attacks-against-water-treatment-plants.html){:target="_blank" rel="noopener"}

> According to a report from CISA last week, there were three ransomware attacks against water treatment plants last year. WWS Sector cyber intrusions from 2019 to early 2021 include: In August 2021, malicious cyber actors used Ghost variant ransomware against a California-based WWS facility. The ransomware variant had been in the system for about a month and was discovered when three supervisory control and data acquisition (SCADA) servers displayed a ransomware message. In July 2021, cyber actors used remote access to introduce ZuCaNo ransomware onto a Maine-based WWS facility’s wastewater SCADA computer. The treatment system was run manually until the [...]
