Title: REvil Servers Shoved Offline by Governments – But They’ll Be Back, Researchers Say
Date: 2021-10-22T17:01:20+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Malware;Web Security
Slug: 2021-10-22-revil-servers-shoved-offline-by-governments-but-theyll-be-back-researchers-say

[Source](https://threatpost.com/revil-servers-offline-governments/175675/){:target="_blank" rel="noopener"}

> A multi-country effort has given ransomware gang REvil a taste of its own medicine by pwning its backups and pushing its leak site and Tor payment site offline. [...]
