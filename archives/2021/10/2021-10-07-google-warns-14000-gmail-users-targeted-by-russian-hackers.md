Title: Google warns 14,000 Gmail users targeted by Russian hackers
Date: 2021-10-07T19:38:57-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-07-google-warns-14000-gmail-users-targeted-by-russian-hackers

[Source](https://www.bleepingcomputer.com/news/security/google-warns-14-000-gmail-users-targeted-by-russian-hackers/){:target="_blank" rel="noopener"}

> Google has warned about 14,000 of its users about being targeted in a state-sponsored phishing campaign from APT28, a threat group that has been linked to Russia. [...]
