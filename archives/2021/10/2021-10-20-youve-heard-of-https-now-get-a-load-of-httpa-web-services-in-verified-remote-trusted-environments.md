Title: You've heard of HTTPS. Now get a load of HTTPA: Web services in verified remote trusted environments?
Date: 2021-10-20T01:25:23+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-10-20-youve-heard-of-https-now-get-a-load-of-httpa-web-services-in-verified-remote-trusted-environments

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/20/intel_sgx_httpa/){:target="_blank" rel="noopener"}

> Intel duo propose fresh use of, yes, SGX but also Arm's TrustZone and similar TEEs Two Intel staffers believe web services can be made more secure by not only carrying out computations in remote trusted execution environments, or TEEs, but by also verifying for clients that this was done so.... [...]
