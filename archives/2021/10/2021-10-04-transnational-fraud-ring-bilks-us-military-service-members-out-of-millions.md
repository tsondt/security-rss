Title: Transnational Fraud Ring Bilks U.S. Military Service Members Out of Millions
Date: 2021-10-04T15:22:32+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Government;Privacy;Web Security
Slug: 2021-10-04-transnational-fraud-ring-bilks-us-military-service-members-out-of-millions

[Source](https://threatpost.com/transnational-fraud-military-members/175298/){:target="_blank" rel="noopener"}

> A former medical records tech stole PII that was then used to fraudulently claim DoD and VA benefits, particularly targeting disabled veterans. [...]
