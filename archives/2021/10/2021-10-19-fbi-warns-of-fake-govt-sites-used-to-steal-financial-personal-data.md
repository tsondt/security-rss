Title: FBI warns of fake govt sites used to steal financial, personal data
Date: 2021-10-19T09:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-19-fbi-warns-of-fake-govt-sites-used-to-steal-financial-personal-data

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-fake-govt-sites-used-to-steal-financial-personal-data/){:target="_blank" rel="noopener"}

> The FBI warned the US public that threat actors actively use fake and spoofed unemployment benefit websites to harvest sensitive financial and personal information from unsuspecting victims. [...]
