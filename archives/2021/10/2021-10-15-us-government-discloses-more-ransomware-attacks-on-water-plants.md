Title: US government discloses more ransomware attacks on water plants
Date: 2021-10-15T03:43:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-15-us-government-discloses-more-ransomware-attacks-on-water-plants

[Source](https://www.bleepingcomputer.com/news/security/us-government-discloses-more-ransomware-attacks-on-water-plants/){:target="_blank" rel="noopener"}

> U.S. Water and Wastewater Systems (WWS) Sector facilities have been breached multiple times in ransomware attacks during the last two years according to joint advisory published by US government agencies on Thursday. [...]
