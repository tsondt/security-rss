Title: Introducing the Security at the Edge: Core Principles whitepaper
Date: 2021-10-14T23:06:43+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Edge;Edge security;IoT;Network and content delivery;Security;Security Blog;Whitepaper
Slug: 2021-10-14-introducing-the-security-at-the-edge-core-principles-whitepaper

[Source](https://aws.amazon.com/blogs/security/introducing-the-security-at-the-edge-core-principles-whitepaper/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) recently released the Security at the Edge: Core Principles whitepaper. Today’s business leaders know that it’s critical to ensure that both the security of their environments and the security present in traditional cloud networks are extended to workloads at the edge. The whitepaper provides security executives the foundations for implementing a [...]
