Title: Are you making good progress with Kubernetes? Cybercriminals are progressing faster
Date: 2021-10-06T19:00:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-10-06-are-you-making-good-progress-with-kubernetes-cybercriminals-are-progressing-faster

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/06/kasten_k10_kubernetes_protection_promo/){:target="_blank" rel="noopener"}

> Protect your deployment from ransomware attacks with Kasten by Veeam Sponsored Who do you consider the most innovative, agile tech team out there? Sadly, it’s probably the cybercriminal community, particularly those who specialise in ransomware attacks.... [...]
