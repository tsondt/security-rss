Title: RaidForums forced to use mirror after Brazilian govt contacts registrar
Date: 2021-10-04T13:00:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-10-04-raidforums-forced-to-use-mirror-after-brazilian-govt-contacts-registrar

[Source](https://www.bleepingcomputer.com/news/security/raidforums-forced-to-use-mirror-after-brazilian-govt-contacts-registrar/){:target="_blank" rel="noopener"}

> The RaidForums hacking forum has gone through a turbulent week, with its website now forced through a mirror domain after a government filed a legal request with their registrar. [...]
