Title: Political-themed actor using old MS Office flaw to drop multiple RATs
Date: 2021-10-20T12:59:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-20-political-themed-actor-using-old-ms-office-flaw-to-drop-multiple-rats

[Source](https://www.bleepingcomputer.com/news/security/political-themed-actor-using-old-ms-office-flaw-to-drop-multiple-rats/){:target="_blank" rel="noopener"}

> A novel threat actor with unclear motives has been discovered running a crimeware campaign which delivers multiple Windows and Android RATs (remote access tools) through the exploitation of CVE-2017-11882. [...]
