Title: DarkSide ransomware rushes to cash out $7 million in Bitcoin
Date: 2021-10-22T14:02:21-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-22-darkside-ransomware-rushes-to-cash-out-7-million-in-bitcoin

[Source](https://www.bleepingcomputer.com/news/security/darkside-ransomware-rushes-to-cash-out-7-million-in-bitcoin/){:target="_blank" rel="noopener"}

> Almost $7 million worth of Bitcoin in a wallet controlled by DarkSide ransomware operators has been moved in what looks like a money laundering rollercoaster. [...]
