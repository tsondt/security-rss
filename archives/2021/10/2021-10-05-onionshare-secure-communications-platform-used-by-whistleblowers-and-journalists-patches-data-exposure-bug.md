Title: OnionShare: Secure communications platform used by whistleblowers and journalists patches data exposure bug
Date: 2021-10-05T12:35:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-05-onionshare-secure-communications-platform-used-by-whistleblowers-and-journalists-patches-data-exposure-bug

[Source](https://portswigger.net/daily-swig/onionshare-secure-communications-platform-used-by-whistleblowers-and-journalists-patches-data-exposure-bug){:target="_blank" rel="noopener"}

> Open source software is used to protect a sender’s identity [...]
