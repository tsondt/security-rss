Title: <i>Reg</i> scribe spends week being watched by government Bluetooth wristband, emerges to more surveillance
Date: 2021-10-19T06:41:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-10-19-reg-scribe-spends-week-being-watched-by-government-bluetooth-wristband-emerges-to-more-surveillance

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/19/7_days_with_singapore_shn/){:target="_blank" rel="noopener"}

> Home quarantine week was the price for an overseas trip, ongoing observation is the price of COVID-19 Feature My family and I recently returned to Singapore after an overseas trip that, for the first time in over a year, did not require the ordeal of two weeks of quarantine in a hotel room.... [...]
