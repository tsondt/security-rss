Title: Chinese phone manufacturer ZTE launches public bug bounty program
Date: 2021-10-12T13:36:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-12-chinese-phone-manufacturer-zte-launches-public-bug-bounty-program

[Source](https://portswigger.net/daily-swig/chinese-phone-manufacturer-zte-launches-public-bug-bounty-program){:target="_blank" rel="noopener"}

> Researchers invited to test for flaws under new YesWeHack platform [...]
