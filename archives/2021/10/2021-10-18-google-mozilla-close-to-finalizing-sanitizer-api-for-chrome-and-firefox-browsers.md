Title: Google, Mozilla close to finalizing Sanitizer API for Chrome and Firefox browsers
Date: 2021-10-18T11:18:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-18-google-mozilla-close-to-finalizing-sanitizer-api-for-chrome-and-firefox-browsers

[Source](https://portswigger.net/daily-swig/google-mozilla-close-to-finalizing-sanitizer-api-for-chrome-and-firefox-browsers){:target="_blank" rel="noopener"}

> Latest specification is a work in progress [...]
