Title: FCC Proposal Targets SIM Swapping, Port-Out Fraud
Date: 2021-10-01T15:09:51+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;SIM Swapping;Allison Nixon;number port-out fraud;SIM swapping;U.S. Federal Communications Commission;U.S. Federal Trade Commission;Unit221B
Slug: 2021-10-01-fcc-proposal-targets-sim-swapping-port-out-fraud

[Source](https://krebsonsecurity.com/2021/10/fcc-proposal-targets-sim-swapping-port-out-fraud/){:target="_blank" rel="noopener"}

> The U.S. Federal Communications Commission (FCC) is asking for feedback on new proposed rules to crack down on SIM swapping and number port-out fraud, increasingly prevalent scams in which identity thieves hijack a target’s mobile phone number and use that to wrest control over the victim’s online identity. In a long-overdue notice issued Sept. 30, the FCC said it plans to move quickly on requiring the mobile companies to adopt more secure methods of authenticating customers before redirecting their phone number to a new device or carrier. “We have received numerous complaints from consumers who have suffered significant distress, inconvenience, [...]
