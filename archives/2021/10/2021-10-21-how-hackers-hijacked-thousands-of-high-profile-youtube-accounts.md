Title: How hackers hijacked thousands of high-profile YouTube accounts
Date: 2021-10-21T15:00:46+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;cryptocurrency scam;google;hacking;YouTube
Slug: 2021-10-21-how-hackers-hijacked-thousands-of-high-profile-youtube-accounts

[Source](https://arstechnica.com/?p=1806361){:target="_blank" rel="noopener"}

> Enlarge (credit: Future Publishing | Getty Images) Since at least 2019, hackers have been hijacking high-profile YouTube channels. Sometimes they broadcast cryptocurrency scams, sometimes they simply auction off access to the account. Now, Google has detailed the technique that hackers-for-hire used to compromise thousands of YouTube creators in just the past couple of years. Cryptocurrency scams and account takeovers themselves aren’t a rarity; look no further than last fall’s Twitter hack for an example of that chaos at scale. But the sustained assault against YouTube accounts stands out both for its breadth and for the methods the hackers used, and [...]
