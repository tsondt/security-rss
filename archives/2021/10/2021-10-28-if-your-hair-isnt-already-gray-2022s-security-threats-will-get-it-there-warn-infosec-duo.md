Title: If your hair isn't already gray, 2022's security threats will get it there, warn infosec duo
Date: 2021-10-28T07:25:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-28-if-your-hair-isnt-already-gray-2022s-security-threats-will-get-it-there-warn-infosec-duo

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/28/fireeye_mcafee_2022/){:target="_blank" rel="noopener"}

> Everyone else really is out to get you Those hoping for some respite from the world's ongoing woes are out of luck, apparently.... [...]
