Title: 9 things I freakin’ love about Google Cloud identity and environments
Date: 2021-10-26T16:00:00+00:00
Author: Forrest Brazeal
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-10-26-9-things-i-freakin-love-about-google-cloud-identity-and-environments

[Source](https://cloud.google.com/blog/products/identity-security/identity-and-environment-in-google-cloud/){:target="_blank" rel="noopener"}

> I’ve been at Google Cloud just a few weeks, following years of experience as an AWS Hero and building on other clouds. So last week’s Google Cloud Next–my first!—was a bit of a culture shock. On the GCP podcast, I used the word “intentionality” to describe what I’m seeing: a thoughtful, holistic approach that informs so much of how Google Cloud is put together. Not just in the headline-grabbing new announcements like Google Distributed Cloud, but in the everyday things too. Things like IAM and project setup. Step 1 of any cloud project is to provision access to an environment, [...]
