Title: Critical flaw in GoCD provides platform for supply chain attacks
Date: 2021-10-28T14:25:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-28-critical-flaw-in-gocd-provides-platform-for-supply-chain-attacks

[Source](https://portswigger.net/daily-swig/critical-flaw-in-gocd-provides-platform-for-supply-chain-attacks){:target="_blank" rel="noopener"}

> Vulnerability in software used by Fortune 500 firms raises fears of SolarWinds-like impact [...]
