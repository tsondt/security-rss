Title: Zero-day hunters seek laws to prevent vendors suing them for helping out and doing their jobs
Date: 2021-10-11T22:01:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-11-zero-day-hunters-seek-laws-to-prevent-vendors-suing-them-for-helping-out-and-doing-their-jobs

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/11/cyan_zero_day_legislative_project/){:target="_blank" rel="noopener"}

> Cybersecurity Advisors Network gets backing from Bugcrowd, infosec luminaries, even the OECD Cybersecurity Advisors Network (CyAN), the Paris-based body that represents infosec pros, has created a new working group to advocate for legislation that stops vendors from suing when security researchers show them zero-day bugs in their kit.... [...]
