Title: Intuit warns QuickBooks customers of ongoing phishing attacks
Date: 2021-10-08T13:16:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-08-intuit-warns-quickbooks-customers-of-ongoing-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/intuit-warns-quickbooks-customers-of-ongoing-phishing-attacks/){:target="_blank" rel="noopener"}

> Intuit has warned QuickBooks customers that they are targeted by an ongoing phishing campaign impersonating the company and trying to lure potential victims with fake renewal charges. [...]
