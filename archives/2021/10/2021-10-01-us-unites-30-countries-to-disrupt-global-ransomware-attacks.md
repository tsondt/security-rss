Title: US unites 30 countries to disrupt global ransomware attacks
Date: 2021-10-01T16:01:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-01-us-unites-30-countries-to-disrupt-global-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-unites-30-countries-to-disrupt-global-ransomware-attacks/){:target="_blank" rel="noopener"}

> U.S. President Joe Biden said today announced today that the U.S. has brought together 30 countries to jointly crackdown on ransomware gangs behind a barrage of attacks impacting organizations worldwide. [...]
