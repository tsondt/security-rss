Title: Olympus US systems hit by cyberattack over the weekend
Date: 2021-10-12T07:46:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-12-olympus-us-systems-hit-by-cyberattack-over-the-weekend

[Source](https://www.bleepingcomputer.com/news/security/olympus-us-systems-hit-by-cyberattack-over-the-weekend/){:target="_blank" rel="noopener"}

> Olympus, a leading medical technology company, was forced to take down IT systems in the Americas (U.S., Canada and Latin America) following a cyberattack that hit its network Sunday, on October 10, 2021. [...]
