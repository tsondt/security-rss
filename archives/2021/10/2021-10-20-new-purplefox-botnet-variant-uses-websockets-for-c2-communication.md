Title: New PurpleFox botnet variant uses WebSockets for C2 communication
Date: 2021-10-20T08:39:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-20-new-purplefox-botnet-variant-uses-websockets-for-c2-communication

[Source](https://www.bleepingcomputer.com/news/security/new-purplefox-botnet-variant-uses-websockets-for-c2-communication/){:target="_blank" rel="noopener"}

> The PurpleFox botnet has refreshed its arsenal with new vulnerability exploits and dropped payloads, now also leveraging WebSockets for C2 bidirectional communication. [...]
