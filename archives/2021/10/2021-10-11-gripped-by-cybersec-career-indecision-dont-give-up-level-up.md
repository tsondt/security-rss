Title: Gripped by cybersec career indecision? Don’t give up. Level up
Date: 2021-10-11T07:30:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-10-11-gripped-by-cybersec-career-indecision-dont-give-up-level-up

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/11/gripped_by_cybersec_career_indecision/){:target="_blank" rel="noopener"}

> Click this way for personalized security training advice Sponsored Whether you’re an experienced cybersecurity hand or a relative newbie, you know the price of security is constant vigilance...and constant honing of your skills.... [...]
