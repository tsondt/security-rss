Title: Forensic investigation environment strategies in the AWS Cloud
Date: 2021-10-28T15:57:41+00:00
Author: Sol Kavanagh
Category: AWS Security
Tags: Advanced (300);Amazon EC2;Security, Identity, & Compliance;DevSecOps;Digital forensics;Incident response;security automation;Security Blog
Slug: 2021-10-28-forensic-investigation-environment-strategies-in-the-aws-cloud

[Source](https://aws.amazon.com/blogs/security/forensic-investigation-environment-strategies-in-the-aws-cloud/){:target="_blank" rel="noopener"}

> When a deviation from your secure baseline occurs, it’s crucial to respond and resolve the issue quickly and follow up with a forensic investigation and root cause analysis. Having a preconfigured infrastructure and a practiced plan for using it when there’s a deviation from your baseline will help you to extract and analyze the information [...]
