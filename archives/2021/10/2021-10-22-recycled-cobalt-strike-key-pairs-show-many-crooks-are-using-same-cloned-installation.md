Title: Recycled Cobalt Strike key pairs show many crooks are using same cloned installation
Date: 2021-10-22T16:32:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-22-recycled-cobalt-strike-key-pairs-show-many-crooks-are-using-same-cloned-installation

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/22/cobalt_strike_virustotal_key_discovery/){:target="_blank" rel="noopener"}

> Researcher spots RSA tell-tale lurking in plain sight on VirusTotal Around 1,500 Cobalt Strike beacons uploaded to VirusTotal were reusing the same RSA keys from a cracked version of the software, according to a security researcher who pored through the malware repository.... [...]
