Title: US links $5.2 billion worth of Bitcoin transactions to ransomware
Date: 2021-10-15T13:40:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-15-us-links-52-billion-worth-of-bitcoin-transactions-to-ransomware

[Source](https://www.bleepingcomputer.com/news/security/us-links-52-billion-worth-of-bitcoin-transactions-to-ransomware/){:target="_blank" rel="noopener"}

> The U.S. Treasury Department's Financial Crimes Enforcement Network (FinCEN) has identified roughly $5.2 billion worth of outgoing Bitcoin transactions likely tied to the top 10 most commonly reported ransomware variants. [...]
