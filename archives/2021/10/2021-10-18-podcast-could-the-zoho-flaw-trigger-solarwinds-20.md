Title: Podcast: Could the Zoho Flaw Trigger SolarWinds 2.0?
Date: 2021-10-18T20:55:23+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Podcasts;Vulnerabilities;Web Security
Slug: 2021-10-18-podcast-could-the-zoho-flaw-trigger-solarwinds-20

[Source](https://threatpost.com/podcast-zoho-solarwinds/175553/){:target="_blank" rel="noopener"}

> Companies are worried that the highly privileged password app could let attackers deep inside an enterprise’s footprint, says Redscan’s George Glass. [...]
