Title: SnapMC hackers skip file encryption and just steal your files
Date: 2021-10-12T08:43:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-12-snapmc-hackers-skip-file-encryption-and-just-steal-your-files

[Source](https://www.bleepingcomputer.com/news/security/snapmc-hackers-skip-file-encryption-and-just-steal-your-files/){:target="_blank" rel="noopener"}

> A new actor tracked as SnapMC has emerged in the cybercrime space, performing the typical data-stealing extortion that underpins ransomware operations, but without doing any file encryption. [...]
