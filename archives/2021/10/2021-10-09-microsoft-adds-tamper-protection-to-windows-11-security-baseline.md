Title: Microsoft adds tamper protection to Windows 11 security baseline
Date: 2021-10-09T11:05:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-10-09-microsoft-adds-tamper-protection-to-windows-11-security-baseline

[Source](https://www.bleepingcomputer.com/news/security/microsoft-adds-tamper-protection-to-windows-11-security-baseline/){:target="_blank" rel="noopener"}

> Microsoft has released the final version of its security configuration baseline settings for Windows 11, downloadable today using the Microsoft Security Compliance Toolkit. [...]
