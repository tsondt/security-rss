Title: Cryptocurrency funds removed from 6,000 Coinbase accounts due to flaw in SMS authentication
Date: 2021-10-04T14:09:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-04-cryptocurrency-funds-removed-from-6000-coinbase-accounts-due-to-flaw-in-sms-authentication

[Source](https://portswigger.net/daily-swig/cryptocurrency-funds-removed-from-6-000-coinbase-accounts-due-to-flaw-in-sms-authentication){:target="_blank" rel="noopener"}

> Victims are told they will be reimbursed [...]
