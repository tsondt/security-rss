Title: FBI Raids Chinese Point-of-Sale Giant PAX Technology
Date: 2021-10-26T17:30:20+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Department of Customs and Border Protection;Department of Homeland Security;Federal Bureau of Investigation;Naval Criminal Investigative Services;PAX Technology;WOKV.com
Slug: 2021-10-26-fbi-raids-chinese-point-of-sale-giant-pax-technology

[Source](https://krebsonsecurity.com/2021/10/fbi-raids-chinese-point-of-sale-giant-pax-technology/){:target="_blank" rel="noopener"}

> U.S. federal investigators today raided the Florida offices of PAX Technology, a Chinese provider of point-of-sale devices used by millions of businesses and retailers globally. KrebsOnSecurity has learned the raid is tied to reports that PAX’s systems may have been involved in cyberattacks on U.S. and E.U. organizations. FBI agents entering PAX Technology offices in Jacksonville today. Source: WOKV.com. Headquartered in Shenzhen, China, PAX Technology Inc. has more than 60 million point-of-sale terminals in use throughout 120 countries. Earlier today, Jacksonville, Fla. based WOKV.com reported that agents with the FBI and Department of Homeland Security (DHS) had raided a local [...]
