Title: Twitch downplays this month's hack, says it had minimal impact
Date: 2021-10-15T11:37:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-15-twitch-downplays-this-months-hack-says-it-had-minimal-impact

[Source](https://www.bleepingcomputer.com/news/security/twitch-downplays-this-months-hack-says-it-had-minimal-impact/){:target="_blank" rel="noopener"}

> In an update regarding this month's security incident, Twitch downplayed the breach saying that it had minimal impact and it only affected a small number of users. [...]
