Title: NSA warns of heightened wildcard TLS certificate risk
Date: 2021-10-12T15:42:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-12-nsa-warns-of-heightened-wildcard-tls-certificate-risk

[Source](https://portswigger.net/daily-swig/nsa-warns-of-heightened-wildcard-tls-certificate-risk){:target="_blank" rel="noopener"}

> Wild Alpaca peril [...]
