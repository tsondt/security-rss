Title: Correlate security findings with AWS Security Hub and Amazon EventBridge
Date: 2021-10-25T16:11:22+00:00
Author: Marshall Jones
Category: AWS Security
Tags: Advanced (300);Amazon EventBridge;AWS Security Hub;Security, Identity, & Compliance;Amazon GuardDuty;AWS GuardDuty;Incident response;Security Blog
Slug: 2021-10-25-correlate-security-findings-with-aws-security-hub-and-amazon-eventbridge

[Source](https://aws.amazon.com/blogs/security/correlate-security-findings-with-aws-security-hub-and-amazon-eventbridge/){:target="_blank" rel="noopener"}

> In this blog post, we’ll walk you through deploying a solution to correlate specific AWS Security Hub findings from multiple AWS services that are related to a single AWS resource, which indicates an increased possibility that a security incident has happened. AWS Security Hub ingests findings from multiple AWS services, including Amazon GuardDuty, Amazon Inspector, [...]
