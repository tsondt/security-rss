Title: 3D printing site Thingiverse suffers breach of 228,000 email addresses amid sluggish disclosure
Date: 2021-10-14T16:03:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-14-3d-printing-site-thingiverse-suffers-breach-of-228000-email-addresses-amid-sluggish-disclosure

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/14/thingiverse_data_breach_228k_email_addresses/){:target="_blank" rel="noopener"}

> So says Have I Been Pwned's maintainer - but site claims breach only impacted 'handful of users' Update Thingiverse, a site that hosts free-to-use 3D printer designs, has suffered a data breach – and at least 228,000 unlucky users' email addresses have been circulating on black-hat crime forums.... [...]
