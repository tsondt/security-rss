Title: GitHub revokes duplicate SSH auth keys linked to library bug
Date: 2021-10-11T16:12:04-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-11-github-revokes-duplicate-ssh-auth-keys-linked-to-library-bug

[Source](https://www.bleepingcomputer.com/news/security/github-revokes-duplicate-ssh-auth-keys-linked-to-library-bug/){:target="_blank" rel="noopener"}

> GitHub has revoked weak SSH authentication keys generated using a library that incorrectly created duplicate RSA keypairs. [...]
