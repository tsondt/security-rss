Title: The Missouri Governor Doesn’t Understand Responsible Disclosure
Date: 2021-10-18T11:20:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;disclosure;overreactions;vulnerabilities
Slug: 2021-10-18-the-missouri-governor-doesnt-understand-responsible-disclosure

[Source](https://www.schneier.com/blog/archives/2021/10/the-missouri-governor-doesnt-understand-responsible-disclosure.html){:target="_blank" rel="noopener"}

> The Missouri governor wants to prosecute the reporter who discovered a security vulnerability in a state’s website, and then reported it to the state. The newspaper agreed to hold off publishing any story while the department fixed the problem and protected the private information of teachers around the state. [...] According to the Post-Dispatch, one of its reporters discovered the flaw in a web application allowing the public to search teacher certifications and credentials. No private information was publicly visible, but teacher Social Security numbers were contained in HTML source code of the pages. The state removed the search tool [...]
