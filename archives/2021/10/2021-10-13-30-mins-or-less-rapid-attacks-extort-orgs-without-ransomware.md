Title: 30 Mins or Less: Rapid Attacks Extort Orgs Without Ransomware
Date: 2021-10-13T11:22:00+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Vulnerabilities;Web Security
Slug: 2021-10-13-30-mins-or-less-rapid-attacks-extort-orgs-without-ransomware

[Source](https://threatpost.com/rapid-attacks-extort-ransomware/175445/){:target="_blank" rel="noopener"}

> The previously unknown SnapMC group exploits unpatched VPNs and webserver apps to breach systems and carry out quick-hit extortion in less time than it takes to order a pizza. [...]
