Title: New UEFI bootkit used to backdoor Windows devices since 2012
Date: 2021-10-05T07:16:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-05-new-uefi-bootkit-used-to-backdoor-windows-devices-since-2012

[Source](https://www.bleepingcomputer.com/news/security/new-uefi-bootkit-used-to-backdoor-windows-devices-since-2012/){:target="_blank" rel="noopener"}

> A newly discovered and previously undocumented UEFI (Unified Extensible Firmware Interface) bootkit has been used by attackers to backdoor Windows systems by hijacking the Windows Boot Manager since at least 2012. [...]
