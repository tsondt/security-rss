Title: Microsoft fixes bug blocking Azure Virtual Desktops security updates
Date: 2021-10-07T08:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-07-microsoft-fixes-bug-blocking-azure-virtual-desktops-security-updates

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-bug-blocking-azure-virtual-desktops-security-updates/){:target="_blank" rel="noopener"}

> Microsoft has fixed a bug blocking some Azure Virtual Desktop (AVD) devices from downloading and installing monthly security via Windows Server Update Services (WSUS) since early July. [...]
