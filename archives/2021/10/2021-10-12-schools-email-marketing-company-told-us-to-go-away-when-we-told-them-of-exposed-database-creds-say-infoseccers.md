Title: Schools email marketing company told us to go away when we told them of exposed database creds, say infoseccers
Date: 2021-10-12T09:15:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-12-schools-email-marketing-company-told-us-to-go-away-when-we-told-them-of-exposed-database-creds-say-infoseccers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/12/schools_marketing_company_database_credentials_exposed/){:target="_blank" rel="noopener"}

> Usernames and passwords could be read (and abused) by anyone in since fixed flaw An email marketing company claiming to hold details on a million UK teachers and school admin personnel was potentially exposing those to the public internet thanks to a misconfigured error page on its website.... [...]
