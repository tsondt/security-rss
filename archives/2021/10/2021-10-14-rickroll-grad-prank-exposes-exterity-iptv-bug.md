Title: Rickroll Grad Prank Exposes Exterity IPTV Bug
Date: 2021-10-14T20:38:02+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-10-14-rickroll-grad-prank-exposes-exterity-iptv-bug

[Source](https://threatpost.com/rickroll-exterity-iptv-bug/175491/){:target="_blank" rel="noopener"}

> IPTV and IP video security is increasingly under scrutiny, even by high school kids. [...]
