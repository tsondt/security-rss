Title: Credit card PINs can be guessed even when covering the ATM pad
Date: 2021-10-18T08:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-18-credit-card-pins-can-be-guessed-even-when-covering-the-atm-pad

[Source](https://www.bleepingcomputer.com/news/security/credit-card-pins-can-be-guessed-even-when-covering-the-atm-pad/){:target="_blank" rel="noopener"}

> Researchers have proven it's possible to train a special-purpose deep-learning algorithm that can guess 4-digit card PINs 41% of the time, even if the victim is covering the pad with their hands. [...]
