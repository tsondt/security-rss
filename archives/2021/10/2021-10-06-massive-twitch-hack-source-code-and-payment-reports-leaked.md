Title: Massive Twitch hack: Source code and payment reports leaked
Date: 2021-10-06T09:13:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-06-massive-twitch-hack-source-code-and-payment-reports-leaked

[Source](https://www.bleepingcomputer.com/news/security/massive-twitch-hack-source-code-and-payment-reports-leaked/){:target="_blank" rel="noopener"}

> Twitch source code, as well as streamers' and users' sensitive information, was allegedly leaked online by an anonymous user on the 4chan imageboard. [...]
