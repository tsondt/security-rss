Title: Protonmail celebrates Swiss court victory exempting it from telco data retention laws
Date: 2021-10-27T06:29:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-27-protonmail-celebrates-swiss-court-victory-exempting-it-from-telco-data-retention-laws

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/27/protonmail_data_victory/){:target="_blank" rel="noopener"}

> Doesn't stop local courts' surveillance orders, though Encrypted email provider Protonmail has hailed a recent Swiss legal ruling as a "victory for privacy," after winning a lawsuit that sees it exempted from data retention laws in the mountainous realm.... [...]
