Title: LibreOffice, OpenOffice bug allows hackers to spoof signed docs
Date: 2021-10-11T12:47:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2021-10-11-libreoffice-openoffice-bug-allows-hackers-to-spoof-signed-docs

[Source](https://www.bleepingcomputer.com/news/security/libreoffice-openoffice-bug-allows-hackers-to-spoof-signed-docs/){:target="_blank" rel="noopener"}

> LibreOffice and OpenOffice have pushed updates to address a vulnerability that makes it possible for an attacker to manipulate documents to appear as signed by a trusted source. [...]
