Title: PyPI removes 'mitmproxy2' over code execution concerns
Date: 2021-10-12T13:50:25-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Software
Slug: 2021-10-12-pypi-removes-mitmproxy2-over-code-execution-concerns

[Source](https://www.bleepingcomputer.com/news/security/pypi-removes-mitmproxy2-over-code-execution-concerns/){:target="_blank" rel="noopener"}

> The PyPI repository has removed a Python package called 'mitmproxy2' that was an identical copy of the official "mitmproxy" library, but with an "artificially introduced" code execution vulnerability. The 'mitmproxy' Python package is a free and open-source interactive HTTPS proxy [...]
