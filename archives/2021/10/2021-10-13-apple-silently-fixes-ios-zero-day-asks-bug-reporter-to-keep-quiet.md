Title: Apple silently fixes iOS zero-day, asks bug reporter to keep quiet
Date: 2021-10-13T11:25:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2021-10-13-apple-silently-fixes-ios-zero-day-asks-bug-reporter-to-keep-quiet

[Source](https://www.bleepingcomputer.com/news/apple/apple-silently-fixes-ios-zero-day-asks-bug-reporter-to-keep-quiet/){:target="_blank" rel="noopener"}

> Apple has silently fixed a gamed zero-day vulnerability with the release of iOS 15.0.2, on Monday, a security flaw that could let attackers gain access to sensitive user information. [...]
