Title: IKEA: Cameras were hidden in the ceiling above warehouse toilets for 'health and safety'
Date: 2021-10-01T13:28:09+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-10-01-ikea-cameras-were-hidden-in-the-ceiling-above-warehouse-toilets-for-health-and-safety

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/01/ikea_spycam_scandal/){:target="_blank" rel="noopener"}

> Spytech removed after staff outrage IKEA has removed hidden security cameras from its warehouse in Peterborough, England, after an employee spotted one in the ceiling void while using the toilet.... [...]
