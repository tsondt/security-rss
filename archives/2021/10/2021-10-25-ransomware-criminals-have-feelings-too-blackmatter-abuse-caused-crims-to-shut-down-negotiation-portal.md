Title: Ransomware criminals have feelings too: BlackMatter abuse caused crims to shut down negotiation portal
Date: 2021-10-25T17:16:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-25-ransomware-criminals-have-feelings-too-blackmatter-abuse-caused-crims-to-shut-down-negotiation-portal

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/25/blackmatter_portal_emsisoft/){:target="_blank" rel="noopener"}

> Or so says infsec outfit Emsisoft Hurling online abuse at ransomware gangs may have contributed to a hardline policy of dumping victims' data online, according to counter-ransomware company Emsisoft.... [...]
