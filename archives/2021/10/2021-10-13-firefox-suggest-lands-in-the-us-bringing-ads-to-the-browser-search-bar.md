Title: Firefox Suggest lands in the US, bringing ads to the browser search bar
Date: 2021-10-13T10:12:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-13-firefox-suggest-lands-in-the-us-bringing-ads-to-the-browser-search-bar

[Source](https://portswigger.net/daily-swig/firefox-suggest-lands-in-the-us-bringing-ads-to-the-browser-search-bar){:target="_blank" rel="noopener"}

> New feature has been rolled out to a select group of users in the US [...]
