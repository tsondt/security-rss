Title: FreakOut botnet now attacks vulnerable video DVR devices
Date: 2021-10-12T11:58:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-12-freakout-botnet-now-attacks-vulnerable-video-dvr-devices

[Source](https://www.bleepingcomputer.com/news/security/freakout-botnet-now-attacks-vulnerable-video-dvr-devices/){:target="_blank" rel="noopener"}

> A new update to the FreakOut (aka Necro, N3Cr0m0rPh) Python botnet has added a recently published PoC exploit for Visual Tools DVR in its arsenal to further aid in breaching systems. [...]
