Title: How Coinbase Phishers Steal One-Time Passwords
Date: 2021-10-13T14:27:40+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Web Fraud 2.0;alex holden;Bleeping Computer;Coinbase;Coinbase phishing;Hold Security;Lawrence Abrams;one-time password phishing
Slug: 2021-10-13-how-coinbase-phishers-steal-one-time-passwords

[Source](https://krebsonsecurity.com/2021/10/how-coinbase-phishers-steal-one-time-passwords/){:target="_blank" rel="noopener"}

> A recent phishing campaign targeting Coinbase users shows thieves are getting smarter about phishing one-time passwords (OTPs) needed to complete the login process. It also shows that phishers are attempting to sign up for new Coinbase accounts by the millions as part of an effort to identify email addresses that are already associated with active accounts. A Google-translated version of the now-defunct Coinbase phishing site, coinbase.com.password-reset[.]com Coinbase is the world’s second-largest cryptocurrency exchange, with roughly 68 million users from over 100 countries. The now-defunct phishing domain at issue — coinbase.com.password-reset[.]com — was targeting Italian Coinbase users (the site’s default language [...]
