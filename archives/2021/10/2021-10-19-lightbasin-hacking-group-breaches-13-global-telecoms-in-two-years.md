Title: LightBasin hacking group breaches 13 global telecoms in two years
Date: 2021-10-19T10:18:53-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-19-lightbasin-hacking-group-breaches-13-global-telecoms-in-two-years

[Source](https://www.bleepingcomputer.com/news/security/lightbasin-hacking-group-breaches-13-global-telecoms-in-two-years/){:target="_blank" rel="noopener"}

> A group of hackers that security researchers call LightBasin has been compromising mobile telecommunication systems across the world for the past five years. [...]
