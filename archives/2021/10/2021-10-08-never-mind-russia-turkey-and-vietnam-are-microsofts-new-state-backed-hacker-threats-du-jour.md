Title: Never mind Russia: Turkey and Vietnam are Microsoft's new state-backed hacker threats du jour
Date: 2021-10-08T15:13:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-08-never-mind-russia-turkey-and-vietnam-are-microsofts-new-state-backed-hacker-threats-du-jour

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/08/microsoft_digital_defence_report/){:target="_blank" rel="noopener"}

> It isn't just the big dogs preparing to bite, warns Redmond Iran, Turkey and both North and South Korea are bases for nation-state cyber attacks, Microsoft has claimed – as well as old favourite Russia.... [...]
