Title: Facebook Blames Outage on Faulty Router Configuration
Date: 2021-10-05T14:30:59+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breaking News;Facebook
Slug: 2021-10-05-facebook-blames-outage-on-faulty-router-configuration

[Source](https://threatpost.com/facebook-blames-outage-on-faulty-router-configuration/175322/){:target="_blank" rel="noopener"}

> One easily disproved conspiracy theory linked the ~six-hour outage to a supposed data breach tied to a Sept. 22 hacker forum ad for 1.5B Facebook user records. [...]
