Title: CISA urges admins to patch critical Discourse code execution bug
Date: 2021-10-25T05:20:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2021-10-25-cisa-urges-admins-to-patch-critical-discourse-code-execution-bug

[Source](https://www.bleepingcomputer.com/news/security/cisa-urges-admins-to-patch-critical-discourse-code-execution-bug/){:target="_blank" rel="noopener"}

> A critical Discourse remote code execution (RCE) vulnerability tracked as CVE-2021-41163 was fixed via an urgent update by the developer on Friday [...]
