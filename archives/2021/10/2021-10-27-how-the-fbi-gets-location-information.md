Title: How the FBI Gets Location Information
Date: 2021-10-27T14:01:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;FBI;geolocation;law enforcement;leaks;privacy;surveillance;tracking
Slug: 2021-10-27-how-the-fbi-gets-location-information

[Source](https://www.schneier.com/blog/archives/2021/10/how-the-fbi-gets-location-information.html){:target="_blank" rel="noopener"}

> Vice has a detailed article about how the FBI gets data from cell phone providers like AT&T, T-Mobile, and Verizon, based on a leaked (I think) 2019 139-page presentation. [...]
