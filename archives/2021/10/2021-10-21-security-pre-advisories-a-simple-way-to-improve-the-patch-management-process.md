Title: Security pre-advisories: A simple way to improve the patch management process
Date: 2021-10-21T12:02:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-21-security-pre-advisories-a-simple-way-to-improve-the-patch-management-process

[Source](https://portswigger.net/daily-swig/security-pre-advisories-a-simple-way-to-improve-the-patch-management-process){:target="_blank" rel="noopener"}

> Improving enterprise security, one patch at a time [...]
