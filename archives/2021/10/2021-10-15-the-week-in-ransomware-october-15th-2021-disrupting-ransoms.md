Title: The Week in Ransomware - October 15th 2021 - Disrupting ransoms
Date: 2021-10-15T16:35:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-15-the-week-in-ransomware-october-15th-2021-disrupting-ransoms

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-15th-2021-disrupting-ransoms/){:target="_blank" rel="noopener"}

> This week, senior officials from over thirty countries held virtual conferences on disrupting ransomware operations and attacks. [...]
