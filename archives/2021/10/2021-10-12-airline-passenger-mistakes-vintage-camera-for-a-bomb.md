Title: Airline Passenger Mistakes Vintage Camera for a Bomb
Date: 2021-10-12T15:04:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;fear;security theater;terrorism;war on the unexpected
Slug: 2021-10-12-airline-passenger-mistakes-vintage-camera-for-a-bomb

[Source](https://www.schneier.com/blog/archives/2021/10/airline-passenger-mistakes-vintage-camera-for-a-bomb.html){:target="_blank" rel="noopener"}

> I feel sorry for the accused : The “security incident” that forced a New-York bound flight to make an emergency landing at LaGuardia Airport on Saturday turned out to be a misunderstanding — after an airline passenger mistook another traveler’s camera for a bomb, sources said Sunday. American Airlines Flight 4817 from Indianapolis — operated by Republic Airways — made an emergency landing at LaGuardia just after 3 p.m., and authorities took a suspicious passenger into custody for several hours. It turns out the would-be “bomber” was just a vintage camera aficionado and the woman who reported him made a [...]
