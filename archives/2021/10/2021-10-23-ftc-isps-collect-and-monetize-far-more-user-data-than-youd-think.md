Title: FTC: ISPs collect and monetize far more user data than you’d think
Date: 2021-10-23T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-23-ftc-isps-collect-and-monetize-far-more-user-data-than-youd-think

[Source](https://www.bleepingcomputer.com/news/security/ftc-isps-collect-and-monetize-far-more-user-data-than-you-d-think/){:target="_blank" rel="noopener"}

> The Federal Trade Commission (FTC) found that six largest internet service providers (ISPs) in the U.S. collect and share customers' personal data without providing them with info on how it's used or meaningful ways to control this process. [...]
