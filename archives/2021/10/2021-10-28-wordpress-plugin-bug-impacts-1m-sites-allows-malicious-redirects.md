Title: WordPress plugin bug impacts 1M sites, allows malicious redirects
Date: 2021-10-28T10:50:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-28-wordpress-plugin-bug-impacts-1m-sites-allows-malicious-redirects

[Source](https://www.bleepingcomputer.com/news/security/wordpress-plugin-bug-impacts-1m-sites-allows-malicious-redirects/){:target="_blank" rel="noopener"}

> The OptinMonster plugin is affected by a high-severity flaw that allows unauthorized API access and sensitive information disclosure on roughly a million WordPress sites. [...]
