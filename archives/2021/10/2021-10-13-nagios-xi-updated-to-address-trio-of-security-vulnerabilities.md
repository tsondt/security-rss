Title: Nagios XI updated to address trio of security vulnerabilities
Date: 2021-10-13T13:04:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-13-nagios-xi-updated-to-address-trio-of-security-vulnerabilities

[Source](https://portswigger.net/daily-swig/nagios-xi-updated-to-address-trio-of-security-vulnerabilities){:target="_blank" rel="noopener"}

> Post-auth flaws could give attackers a platform from which to pivot to other parts of the network [...]
