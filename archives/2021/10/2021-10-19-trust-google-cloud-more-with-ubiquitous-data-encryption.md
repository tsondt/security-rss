Title: Trust Google Cloud more with ubiquitous data encryption
Date: 2021-10-19T16:00:00+00:00
Author: Anoosh Saboori
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-10-19-trust-google-cloud-more-with-ubiquitous-data-encryption

[Source](https://cloud.google.com/blog/products/identity-security/ubiquitous-data-encryption-on-google-cloud/){:target="_blank" rel="noopener"}

> As you move data to the cloud, you face the important question of how to verifiably protect data from unauthorized access without limiting your options for storage and processing. Using public cloud services requires you to place inherent trust in your cloud provider, which can be uncomfortable for your most sensitive data and workloads. On Google Cloud Platform, you can use solutions such as Cloud External Key Manager (EKM) when encrypting data-at-rest to store and manage keys outside of Google’s infrastructure and Confidential Computing to encrypt data-in-use with keys that remain resident in the processor and unavailable to Google. However, [...]
