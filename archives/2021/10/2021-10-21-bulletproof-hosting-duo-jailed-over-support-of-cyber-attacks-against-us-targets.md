Title: Bulletproof hosting duo jailed over support of cyber-attacks against US targets
Date: 2021-10-21T13:34:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-21-bulletproof-hosting-duo-jailed-over-support-of-cyber-attacks-against-us-targets

[Source](https://portswigger.net/daily-swig/bulletproof-hosting-duo-jailed-over-support-of-cyber-attacks-against-us-targets){:target="_blank" rel="noopener"}

> Attacks leveraging defendants’ infrastructure inflicted heavy financial losses on victims [...]
