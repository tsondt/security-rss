Title: Twitch game page backgrounds defaced with Jeff Bezos' face
Date: 2021-10-08T10:58:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2021-10-08-twitch-game-page-backgrounds-defaced-with-jeff-bezos-face

[Source](https://www.bleepingcomputer.com/news/security/twitch-game-page-backgrounds-defaced-with-jeff-bezos-face/){:target="_blank" rel="noopener"}

> On Twitch's website's game pages today appeared a close up of Jeff Bezos' face, in what appears to be a mysterious defacement attack. [...]
