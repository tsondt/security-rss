Title: Amnesty International links cybersecurity firm to spyware operation
Date: 2021-10-10T10:07:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-10-10-amnesty-international-links-cybersecurity-firm-to-spyware-operation

[Source](https://www.bleepingcomputer.com/news/security/amnesty-international-links-cybersecurity-firm-to-spyware-operation/){:target="_blank" rel="noopener"}

> A report by Amnesty International links an Indian cybersecurity company to an Android spyware program used to target prominent activists. [...]
