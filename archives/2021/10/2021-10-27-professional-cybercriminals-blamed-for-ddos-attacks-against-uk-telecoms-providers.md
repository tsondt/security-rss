Title: ‘Professional cybercriminals’ blamed for DDoS attacks against UK telecoms providers
Date: 2021-10-27T15:34:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-27-professional-cybercriminals-blamed-for-ddos-attacks-against-uk-telecoms-providers

[Source](https://portswigger.net/daily-swig/professional-cybercriminals-blamed-for-ddos-attacks-against-uk-telecoms-providers){:target="_blank" rel="noopener"}

> Packet in, says industry group [...]
