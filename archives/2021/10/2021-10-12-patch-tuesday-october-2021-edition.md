Title: Patch Tuesday, October 2021 Edition
Date: 2021-10-12T19:52:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;apple;AskWoody.com;Bleeping Computer;CVE-2021-26427;CVE-2021-30883;CVE-2021-36970;CVE-2021-38672;CVE-2021-40449;CVE-2021-40461;Immersive Labs;Kevin Breen;Lawrence Abrams;Microsoft Patch Tuesday October 2021;Morphus Labs;sans internet storm center;Satnam Narang;Tenable
Slug: 2021-10-12-patch-tuesday-october-2021-edition

[Source](https://krebsonsecurity.com/2021/10/patch-tuesday-october-2021-edition/){:target="_blank" rel="noopener"}

> Microsoft today issued updates to plug more than 70 security holes in its Windows operating systems and other software, including one vulnerability that is already being exploited. This month’s Patch Tuesday also includes security fixes for the newly released Windows 11 operating system. Separately, Apple has released updates for iOS and iPadOS to address a flaw that is being actively attacked. Firstly, Apple has released iOS 15.0.2 and iPadOS 15.0.2 to fix a zero-day vulnerability (CVE-2021-30883) that is being leveraged in active attacks targeting iPhone and iPad users. Lawrence Abrams of Bleeping Computer writes that the flaw could be used [...]
