Title: Facebook Outage Drags Down Instagram, WhatsApp, Messenger, Oculus VR
Date: 2021-10-04T20:40:31+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breaking News;Facebook;Web Security
Slug: 2021-10-04-facebook-outage-drags-down-instagram-whatsapp-messenger-oculus-vr

[Source](https://threatpost.com/facebook-outage-instagram-whatsapp-messenger-oculus-vr/175308/){:target="_blank" rel="noopener"}

> They were all flat on their faces for hours on Monday, throwing off DNS error messages or other server-related errors. [...]
