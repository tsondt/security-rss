Title: Dutch police send warning letters to DDoS booter customers
Date: 2021-10-12T18:34:27-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-12-dutch-police-send-warning-letters-to-ddos-booter-customers

[Source](https://www.bleepingcomputer.com/news/security/dutch-police-send-warning-letters-to-ddos-booter-customers/){:target="_blank" rel="noopener"}

> Dutch authorities gave a final warning to more than a dozen customers of a distributed denial-of-service (DDoS) website, letting them know that continued cyber offenses lead to prosecution. [...]
