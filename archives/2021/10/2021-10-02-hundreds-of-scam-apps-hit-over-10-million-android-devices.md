Title: Hundreds of scam apps hit over 10 million Android devices
Date: 2021-10-02T10:50:42+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Tech;android;google;malware;scam
Slug: 2021-10-02-hundreds-of-scam-apps-hit-over-10-million-android-devices

[Source](https://arstechnica.com/?p=1800102){:target="_blank" rel="noopener"}

> Enlarge / Never put a GriftHorse on your phone. (credit: John Lamparsky | Getty Images) Google has taken increasingly sophisticated steps to keep malicious apps out of Google Play. But a new round of takedowns involving about 200 apps and more than 10 million potential victims shows that this longtime problem remains far from solved—and in this case, potentially cost users hundreds of millions of dollars. Researchers from the mobile security firm Zimperium say the massive scamming campaign has plagued Android since November 2020. As is often the case, the attackers were able to sneak benign-looking apps like "Handy Translator [...]
