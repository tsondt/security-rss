Title: Ad-blocking browser extension actually adds ads, say Imperva researchers
Date: 2021-10-14T04:02:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-14-ad-blocking-browser-extension-actually-adds-ads-say-imperva-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/14/ad_blocker_injects_bad_ads/){:target="_blank" rel="noopener"}

> Oi, Google: how did this get past your review process? And Imperva: why does your web page offer to install software? Security vendor Imperva’s research labs have found a browser extension that claims to block ads, but actually injects them into Chrome or Opera.... [...]
