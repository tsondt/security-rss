Title: Time to Build Accountability Back into Cybersecurity
Date: 2021-10-18T22:00:24+00:00
Author: Chris Hass
Category: Threatpost
Tags: Breach;Cloud Security;InfoSec Insider;Malware;Vulnerabilities;Web Security
Slug: 2021-10-18-time-to-build-accountability-back-into-cybersecurity

[Source](https://threatpost.com/accountability-cybersecurity/175571/){:target="_blank" rel="noopener"}

> Chris Hass, director of information security and research at Automox, discusses how to assign security responsibility, punishment for poor cyber-hygiene and IDing 'security champions' to help small businesses. [...]
