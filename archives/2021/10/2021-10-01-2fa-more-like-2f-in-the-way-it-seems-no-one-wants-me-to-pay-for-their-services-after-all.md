Title: 2FA? More like 2F-in-the-way: It seems no one wants me to pay for their services after all
Date: 2021-10-01T08:30:05+00:00
Author: Alistair Dabbs
Category: The Register
Tags: 
Slug: 2021-10-01-2fa-more-like-2f-in-the-way-it-seems-no-one-wants-me-to-pay-for-their-services-after-all

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/01/something_for_the_weekend/){:target="_blank" rel="noopener"}

> Shut up and take my money Something for the Weekend, Sir? "Buy me a beer?" Sure, I buy beers for perfect strangers all the time. But you will have to wait your turn. There is a queue, and the other strangers are more reluctant to accept my hospitality.... [...]
