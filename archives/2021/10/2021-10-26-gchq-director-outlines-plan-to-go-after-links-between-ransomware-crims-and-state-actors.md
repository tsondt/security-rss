Title: GCHQ director outlines plan to 'go after' links between ransomware crims and state actors
Date: 2021-10-26T06:32:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-10-26-gchq-director-outlines-plan-to-go-after-links-between-ransomware-crims-and-state-actors

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/26/gchq_ransomware_plan/){:target="_blank" rel="noopener"}

> Sir Jeremy Fleming paints picture of a cultural battle over the internet, AI and the soul of future technology The UK's Government Communications Headquarters (GCHQ) boss Sir Jeremy Fleming has outlined a plan to pursue criminal actors who deploy ransomware as well as the state actors that are aware of their efforts.... [...]
