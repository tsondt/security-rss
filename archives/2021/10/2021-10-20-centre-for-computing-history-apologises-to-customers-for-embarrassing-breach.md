Title: Centre for Computing History apologises to customers for 'embarrassing' breach
Date: 2021-10-20T09:15:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2021-10-20-centre-for-computing-history-apologises-to-customers-for-embarrassing-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/20/centre_for_computing_data_breach/){:target="_blank" rel="noopener"}

> Website patched following phishing scam, no financial data exposed Updated The Centre for Computing History (CCH) in Cambridge, England, has apologised for an "embarrassing" breach in its online customer datafile, though thankfully no payment card information was exposed.... [...]
