Title: DDoS attacks against Russian firms have almost tripled in 2021
Date: 2021-10-20T13:47:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-20-ddos-attacks-against-russian-firms-have-almost-tripled-in-2021

[Source](https://www.bleepingcomputer.com/news/security/ddos-attacks-against-russian-firms-have-almost-tripled-in-2021/){:target="_blank" rel="noopener"}

> A report analyzing data from the start of the year concludes that distributed denial-of-service (DDoS) attacks on Russian companies have increased 2.5 times compared to the same period last year. [...]
