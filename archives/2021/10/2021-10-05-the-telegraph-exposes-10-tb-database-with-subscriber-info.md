Title: The Telegraph exposes 10 TB database with subscriber info
Date: 2021-10-05T11:24:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-05-the-telegraph-exposes-10-tb-database-with-subscriber-info

[Source](https://www.bleepingcomputer.com/news/security/the-telegraph-exposes-10-tb-database-with-subscriber-info/){:target="_blank" rel="noopener"}

> 'The Telegraph', one of UK's largest newspapers and online media outlets, has leaked 10 TB of data after failing to properly secure one of its databases. [...]
