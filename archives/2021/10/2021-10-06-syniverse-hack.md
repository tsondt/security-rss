Title: Syniverse Hack
Date: 2021-10-06T14:19:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;data breaches;hacking
Slug: 2021-10-06-syniverse-hack

[Source](https://www.schneier.com/blog/archives/2021/10/synaverse-hack.html){:target="_blank" rel="noopener"}

> This is interesting: A company that is a critical part of the global telecommunications infrastructure used by AT&T, T-Mobile, Verizon and several others around the world such as Vodafone and China Mobile, quietly disclosed that hackers were inside its systems for years, impacting more than 200 of its clients and potentially millions of cellphone users worldwide. I’ve never heard of the company. No details about the hack. It could be nothing. It could be a national intelligence service looking for information. [...]
