Title: Twitch breach leads to leak of source code and streamer earnings data
Date: 2021-10-07T14:28:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-07-twitch-breach-leads-to-leak-of-source-code-and-streamer-earnings-data

[Source](https://portswigger.net/daily-swig/twitch-breach-leads-to-leak-of-source-code-and-streamer-earnings-data){:target="_blank" rel="noopener"}

> This is like ‘KFC losing its secret recipe’ [...]
