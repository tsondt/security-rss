Title: US invites friends to multilateral cybersecurity meetings – Russia and China strangely absent
Date: 2021-10-14T05:58:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-10-14-us-invites-friends-to-multilateral-cybersecurity-meetings-russia-and-china-strangely-absent

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/14/virtual_counter_ransomware_initiative_meeting/){:target="_blank" rel="noopener"}

> Perps not welcome at anti-ransomware gabfest that Biden admin would rather portray as bold infosec alliance The United States has kicked off meetings attended by representatives of nations that all hope to address the scourge of ransomware – without Russia or China in the room.... [...]
