Title: Microsoft 365 will get support for custom ARC configurations
Date: 2021-10-24T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-24-microsoft-365-will-get-support-for-custom-arc-configurations

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-365-will-get-support-for-custom-arc-configurations/){:target="_blank" rel="noopener"}

> Microsoft is working on adding custom Authenticated Received Chain (ARC) configuration support to Microsoft Defender for Office 365. [...]
