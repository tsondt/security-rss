Title: Acer hacked twice in a week by the same threat actor
Date: 2021-10-19T12:40:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-19-acer-hacked-twice-in-a-week-by-the-same-threat-actor

[Source](https://www.bleepingcomputer.com/news/security/acer-hacked-twice-in-a-week-by-the-same-threat-actor/){:target="_blank" rel="noopener"}

> Acer has suffered a second cyberattack in just a week by the same hacking group that says other regions are vulnerable. [...]
