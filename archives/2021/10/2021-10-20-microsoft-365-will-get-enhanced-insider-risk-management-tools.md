Title: Microsoft 365 will get enhanced insider risk management tools
Date: 2021-10-20T09:30:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-20-microsoft-365-will-get-enhanced-insider-risk-management-tools

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-365-will-get-enhanced-insider-risk-management-tools/){:target="_blank" rel="noopener"}

> Microsoft is updating Microsoft 365 to allow admins to better manage insider security threats in their environments with improvements to risky activity detection and visibility. [...]
