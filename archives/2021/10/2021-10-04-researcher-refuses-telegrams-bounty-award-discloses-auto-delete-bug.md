Title: Researcher refuses Telegram’s bounty award, discloses auto-delete bug
Date: 2021-10-04T14:12:18+00:00
Author: Ax Sharma
Category: Ars Technica
Tags: Biz & IT;Tech;bug bounty;confidential;end to end encryption;hackerone;messaging app;NDA;telegram
Slug: 2021-10-04-researcher-refuses-telegrams-bounty-award-discloses-auto-delete-bug

[Source](https://arstechnica.com/?p=1800367){:target="_blank" rel="noopener"}

> Enlarge (credit: Joshua Sortino ) Telegram patched another image self-destruction bug in its app earlier this year. This flaw was a different issue from the one reported in 2019. But the researcher who reported the bug isn't pleased with Telegram's months-long turnaround time—and an offered $1,159 (€1,000) bounty award in exchange for his silence. Self-destructed images remained on the device Like other messaging apps, Telegram allows senders to set communications to "self-destruct," such that messages and any media attachments are automatically deleted from the device after a set period of time. Such a feature offers extended privacy to both the [...]
