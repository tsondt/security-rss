Title: US govt to ban export of hacking tools to authoritarian regimes
Date: 2021-10-20T15:32:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-20-us-govt-to-ban-export-of-hacking-tools-to-authoritarian-regimes

[Source](https://www.bleepingcomputer.com/news/security/us-govt-to-ban-export-of-hacking-tools-to-authoritarian-regimes/){:target="_blank" rel="noopener"}

> The Commerce Department's Bureau of Industry and Security (BIS) today announced export controls for software and hardware tools that could be used for malicious hacking activities. [...]
