Title: ‘The walls are closing in on me’: the hacking of Princess Haya
Date: 2021-10-06T15:00:26+00:00
Author: Dan Sabbagh Defence and security editor
Category: The Guardian
Tags: Sheikh Mohammed bin Rashid al-Maktoum;United Arab Emirates;Dubai;UK news;Malware;Hacking;Espionage;World news;Technology;Middle East and North Africa;Data and computer security
Slug: 2021-10-06-the-walls-are-closing-in-on-me-the-hacking-of-princess-haya

[Source](https://www.theguardian.com/world/2021/oct/06/walls-closing-in-story-behind-princess-haya-hacking-ordeal){:target="_blank" rel="noopener"}

> Court judgments reveal how Sheikh Mohammed’s use of Pegasus spyware against his ex-wife was uncovered Dubai ruler hacked ex-wife using NSO Pegasus spyware, high court judge finds Eleven court judgments, covering 181 pages, plus hundreds of other pages of legal documents have revealed an extraordinary spying scandal: state-sponsored mobile phone hacking conducted on behalf of the ruler of Dubai against his fearful sixth and former wife, Princess Haya, Britain’s most famous divorce lawyer and her associate, plus three others – against the backdrop of a bitter child protection battle being played out day after day in the English courts. The [...]
