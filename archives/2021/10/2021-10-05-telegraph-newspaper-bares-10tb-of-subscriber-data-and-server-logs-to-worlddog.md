Title: Telegraph newspaper bares 10TB of subscriber data and server logs to world+dog
Date: 2021-10-05T13:37:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-05-telegraph-newspaper-bares-10tb-of-subscriber-data-and-server-logs-to-worlddog

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/05/telegraph_newspaper_10tb_data_breach/){:target="_blank" rel="noopener"}

> Poor securing of Elasticsearch cluster strikes again Updated The Telegraph newspaper managed to leak 10TB of subscriber data and server logs after leaving an Elasticsearch cluster unsecured for most of September, according to the researcher who found it online.... [...]
