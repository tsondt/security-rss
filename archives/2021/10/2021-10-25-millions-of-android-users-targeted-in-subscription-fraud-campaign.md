Title: Millions of Android users targeted in subscription fraud campaign
Date: 2021-10-25T14:00:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-25-millions-of-android-users-targeted-in-subscription-fraud-campaign

[Source](https://www.bleepingcomputer.com/news/security/millions-of-android-users-targeted-in-subscription-fraud-campaign/){:target="_blank" rel="noopener"}

> A new SMS scam campaign relying upon 151 apps has been uncovered, with many of these apps managing to find their way into the Play Store where they amassed 10.5 million downloads. [...]
