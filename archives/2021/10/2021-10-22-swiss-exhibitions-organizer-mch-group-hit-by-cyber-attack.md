Title: Swiss exhibitions organizer MCH Group hit by cyber-attack
Date: 2021-10-22T12:46:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-22-swiss-exhibitions-organizer-mch-group-hit-by-cyber-attack

[Source](https://portswigger.net/daily-swig/swiss-exhibitions-organizer-mch-group-hit-by-cyber-attack){:target="_blank" rel="noopener"}

> Investigations yet to confirm if any data was exfiltrated [...]
