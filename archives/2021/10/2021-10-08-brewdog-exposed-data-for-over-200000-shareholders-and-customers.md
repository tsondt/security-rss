Title: BrewDog exposed data for over 200,000 shareholders and customers
Date: 2021-10-08T03:45:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-08-brewdog-exposed-data-for-over-200000-shareholders-and-customers

[Source](https://www.bleepingcomputer.com/news/security/brewdog-exposed-data-for-over-200-000-shareholders-and-customers/){:target="_blank" rel="noopener"}

> BrewDog, the Scottish brewery and pub chain famous for its crowd-ownership model and the tasty IPAs, has irreversibly exposed the details of 200,000 of its shareholders and customers. [...]
