Title: The FCC proposes rules to fight SIM swap and port-out fraud
Date: 2021-10-01T11:33:34-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-01-the-fcc-proposes-rules-to-fight-sim-swap-and-port-out-fraud

[Source](https://www.bleepingcomputer.com/news/security/the-fcc-proposes-rules-to-fight-sim-swap-and-port-out-fraud/){:target="_blank" rel="noopener"}

> The Federal Communications Commission in the U.S. this week announced that it started to work on rules that would pull the brake on SIM swapping attacks. [...]
