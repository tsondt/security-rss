Title: Brave ditches Google for its own privacy-centric search engine
Date: 2021-10-19T17:28:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software;Security
Slug: 2021-10-19-brave-ditches-google-for-its-own-privacy-centric-search-engine

[Source](https://www.bleepingcomputer.com/news/software/brave-ditches-google-for-its-own-privacy-centric-search-engine/){:target="_blank" rel="noopener"}

> Brave Browser has replaced Google with its own no-tracking privacy-centric Brave Search as the default search engine for new users in five regions. [...]
