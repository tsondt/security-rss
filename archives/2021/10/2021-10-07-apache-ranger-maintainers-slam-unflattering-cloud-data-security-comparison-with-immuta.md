Title: Apache Ranger maintainers slam unflattering cloud data security comparison with Immuta
Date: 2021-10-07T13:34:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-07-apache-ranger-maintainers-slam-unflattering-cloud-data-security-comparison-with-immuta

[Source](https://portswigger.net/daily-swig/apache-ranger-maintainers-slam-unflattering-cloud-data-security-comparison-with-immuta){:target="_blank" rel="noopener"}

> Immuta defends benchmark study comparing access control policy management burdens [...]
