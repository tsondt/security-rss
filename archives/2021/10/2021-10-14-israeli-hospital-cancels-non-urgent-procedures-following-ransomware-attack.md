Title: Israeli hospital cancels non-urgent procedures following ransomware attack
Date: 2021-10-14T13:42:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-14-israeli-hospital-cancels-non-urgent-procedures-following-ransomware-attack

[Source](https://portswigger.net/daily-swig/israeli-hospital-cancels-non-urgent-procedures-following-ransomware-attack){:target="_blank" rel="noopener"}

> National cybersecurity agency braced for further serious network intrusions [...]
