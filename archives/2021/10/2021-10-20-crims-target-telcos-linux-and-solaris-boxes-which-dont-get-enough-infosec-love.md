Title: Crims target telcos' Linux and Solaris boxes, which don't get enough infosec love
Date: 2021-10-20T05:40:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-20-crims-target-telcos-linux-and-solaris-boxes-which-dont-get-enough-infosec-love

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/20/linux_solaris_under_attack_at_telcos/){:target="_blank" rel="noopener"}

> CrowdStrike says 'LightBasin' gang avoids Windows, and knows that telco networks run on badly-secured *nix A mysterious criminal gang is targeting telcos' Linux and Solaris boxes, because it perceives they aren't being watched by infosec teams that have focussed their efforts on securing Windows.... [...]
