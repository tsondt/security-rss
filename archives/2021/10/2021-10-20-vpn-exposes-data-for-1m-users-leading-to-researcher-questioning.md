Title: VPN Exposes Data for 1M Users, Leading to Researcher Questioning
Date: 2021-10-20T17:53:42+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Privacy;Vulnerabilities;Web Security
Slug: 2021-10-20-vpn-exposes-data-for-1m-users-leading-to-researcher-questioning

[Source](https://threatpost.com/vpn-exposes-data-1m/175612/){:target="_blank" rel="noopener"}

> Experts warn that virtual private networks are increasingly vulnerable to leaks and attack. [...]
