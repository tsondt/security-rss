Title: Things that are not PogChamp: Twitch has its source code, streamer payout data leaked
Date: 2021-10-06T14:27:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-10-06-things-that-are-not-pogchamp-twitch-has-its-source-code-streamer-payout-data-leaked

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/06/twitch_data_leak/){:target="_blank" rel="noopener"}

> MonkaS Updated Links to torrents that contain 128GB of data seemingly pulled from the Amazon-owned Twitch streaming service have been posted to 4chan.... [...]
