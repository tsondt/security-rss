Title: How to set up a two-way integration between AWS Security Hub and Jira Service Management
Date: 2021-10-04T15:37:27+00:00
Author: Ramesh Venkataraman
Category: AWS Security
Tags: Advanced (300);AWS Security Hub;Security, Identity, & Compliance;Jira;Security Blog
Slug: 2021-10-04-how-to-set-up-a-two-way-integration-between-aws-security-hub-and-jira-service-management

[Source](https://aws.amazon.com/blogs/security/how-to-set-up-a-two-way-integration-between-aws-security-hub-and-jira-service-management/){:target="_blank" rel="noopener"}

> If you use both AWS Security Hub and Jira Service Management, you can use the new AWS Service Management Connector for Jira Service Management to create an automated, bidirectional integration between these two products that keeps your Security Hub findings and Jira issues in sync. In this blog post, I’ll show you how to set up this integration. [...]
