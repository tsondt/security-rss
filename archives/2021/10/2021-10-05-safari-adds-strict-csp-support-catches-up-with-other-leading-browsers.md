Title: Safari adds strict CSP support, catches up with other leading browsers
Date: 2021-10-05T11:09:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-05-safari-adds-strict-csp-support-catches-up-with-other-leading-browsers

[Source](https://portswigger.net/daily-swig/safari-adds-strict-csp-support-catches-up-with-other-leading-browsers){:target="_blank" rel="noopener"}

> Apple offers users greater defense against XSS and other vulnerabilities [...]
