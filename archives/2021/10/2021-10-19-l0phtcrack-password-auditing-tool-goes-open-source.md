Title: L0phtCrack password auditing tool goes open source
Date: 2021-10-19T15:01:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-19-l0phtcrack-password-auditing-tool-goes-open-source

[Source](https://portswigger.net/daily-swig/l0phtcrack-password-auditing-tool-goes-open-source){:target="_blank" rel="noopener"}

> Original developers invite OS community to develop further capabilities [...]
