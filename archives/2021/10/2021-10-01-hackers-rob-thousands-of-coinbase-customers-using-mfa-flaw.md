Title: Hackers rob thousands of Coinbase customers using MFA flaw
Date: 2021-10-01T10:32:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-10-01-hackers-rob-thousands-of-coinbase-customers-using-mfa-flaw

[Source](https://www.bleepingcomputer.com/news/security/hackers-rob-thousands-of-coinbase-customers-using-mfa-flaw/){:target="_blank" rel="noopener"}

> Crypto exchange Coinbase disclosed that a threat actor stole cryptocurrency from 6,000 customers after using a vulnerability to bypass the company's SMS multi-factor authentication security feature. [...]
