Title: TA505 Gang Is Back With Newly Polished FlawedGrace RAT
Date: 2021-10-19T09:00:30+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-10-19-ta505-gang-is-back-with-newly-polished-flawedgrace-rat

[Source](https://threatpost.com/ta505-retooled-flawedgrace-rat/175559/){:target="_blank" rel="noopener"}

> TA505 – cybercrime trailblazers with ever-evolving TTPs – have returned to mass-volume email attacks, flashing retooled malware and exotic scripting languages. [...]
