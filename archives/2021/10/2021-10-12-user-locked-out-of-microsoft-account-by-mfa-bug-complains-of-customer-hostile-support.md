Title: User locked out of Microsoft account by MFA bug, complains of customer-hostile support
Date: 2021-10-12T19:59:05+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: 2021-10-12-user-locked-out-of-microsoft-account-by-mfa-bug-complains-of-customer-hostile-support

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/12/user_locked_out_of_microsoft/){:target="_blank" rel="noopener"}

> 'So sorry' says Microsoft Identity VP – but its unhelpful support systems will be hard to fix Interview Konstantin Gizdov, an IT professional, was locked out of his Microsoft account by a bug in the company's Multi-Factor Authentication (MFA), but says support refused to acknowledge the bug or recover his account.... [...]
