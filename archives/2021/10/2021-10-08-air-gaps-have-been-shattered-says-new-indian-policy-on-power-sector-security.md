Title: Air gaps have been 'shattered’, says new Indian policy on power sector security
Date: 2021-10-08T04:58:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-08-air-gaps-have-been-shattered-says-new-indian-policy-on-power-sector-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/08/india_power_sector_infosec_policy/){:target="_blank" rel="noopener"}

> Calls for anything connected to the Internet to live in a room controlled by the CISO India has announced a new security policy for its power sector and specified a grade of isolation it says exceeds that offered by air gaps.... [...]
