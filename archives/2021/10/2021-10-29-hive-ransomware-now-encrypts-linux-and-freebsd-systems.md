Title: Hive ransomware now encrypts Linux and FreeBSD systems
Date: 2021-10-29T12:08:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux
Slug: 2021-10-29-hive-ransomware-now-encrypts-linux-and-freebsd-systems

[Source](https://www.bleepingcomputer.com/news/security/hive-ransomware-now-encrypts-linux-and-freebsd-systems/){:target="_blank" rel="noopener"}

> The Hive ransomware gang now also encrypts Linux and FreeBSD using new malware variants specifically developed to target these platforms. [...]
