Title: REvil gang member identified living luxury lifestyle in Russia, says German media
Date: 2021-10-28T17:41:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-28-revil-gang-member-identified-living-luxury-lifestyle-in-russia-says-german-media

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/28/revil_member_identified_german_reports/){:target="_blank" rel="noopener"}

> Die Zeit: He's got a Beemer, a Bitcoin watch and a swimming pool German news outlets claim to have identified a member of the infamous REvil ransomware gang – who reportedly lives the life of Riley off his ill-gotten gains.... [...]
