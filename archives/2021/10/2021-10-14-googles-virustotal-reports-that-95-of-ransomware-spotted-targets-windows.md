Title: Google's VirusTotal reports that 95% of ransomware spotted targets Windows
Date: 2021-10-14T18:53:02+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: 2021-10-14-googles-virustotal-reports-that-95-of-ransomware-spotted-targets-windows

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/14/googles_virustotal_malware/){:target="_blank" rel="noopener"}

> Criminals follow the money, code flaws Google's VirusTotal service showing that 95 per cent of ransomware malware identified by its systems targets Windows.... [...]
