Title: BlackByte ransomware decryptor released to recover files for free
Date: 2021-10-19T10:51:19-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-19-blackbyte-ransomware-decryptor-released-to-recover-files-for-free

[Source](https://www.bleepingcomputer.com/news/security/blackbyte-ransomware-decryptor-released-to-recover-files-for-free/){:target="_blank" rel="noopener"}

> A free decryptor for the BlackByte ransomware has been released, allowing past victims to recover their files for free. [...]
