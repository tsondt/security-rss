Title: Migrate and secure your Windows PKI to AWS with AWS CloudHSM
Date: 2021-10-27T19:47:00+00:00
Author: Govindarajan Varadan
Category: AWS Security
Tags: Advanced (300);AWS CloudHSM;Security, Identity, & Compliance;Encryption;Microsoft CA;PKCS#11;Security Blog
Slug: 2021-10-27-migrate-and-secure-your-windows-pki-to-aws-with-aws-cloudhsm

[Source](https://aws.amazon.com/blogs/security/migrate-and-secure-your-windows-pki-to-aws-with-aws-cloudhsm/){:target="_blank" rel="noopener"}

> AWS CloudHSM provides a cloud-based hardware security module (HSM) that enables you to easily generate and use your own encryption keys in AWS. Using CloudHSM as part of a Microsoft Active Directory Certificate Services (AD CS) public key infrastructure (PKI) fortifies the security of your certificate authority (CA) private key and ensures the security of [...]
