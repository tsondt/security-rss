Title: Squirrel Bug Lets Attackers Execute Code in Games, Cloud Services
Date: 2021-10-19T21:42:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-10-19-squirrel-bug-lets-attackers-execute-code-in-games-cloud-services

[Source](https://threatpost.com/squirrel-attackers-execute-code-games-cloud-services/175586/){:target="_blank" rel="noopener"}

> The out-of-bounds read vulnerability enables an attacker to escape a Squirrel VM in games with millions of monthly players – such as Counter-Strike: Global Offensive and Portal 2 – and in cloud services such as Twilio Electric Imp. [...]
