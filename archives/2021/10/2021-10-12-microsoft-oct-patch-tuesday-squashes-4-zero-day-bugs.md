Title: Microsoft Oct. Patch Tuesday Squashes 4 Zero-Day Bugs
Date: 2021-10-12T21:51:06+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;News;Vulnerabilities;Web Security
Slug: 2021-10-12-microsoft-oct-patch-tuesday-squashes-4-zero-day-bugs

[Source](https://threatpost.com/microsoft-patch-tuesday-bug-exploited-mysterysnail-espionage-campaign/175431/){:target="_blank" rel="noopener"}

> Microsoft's October 2021 Patch Tuesday included security fixes for 74 vulnerabilities, one of which is an actively exploited zero-day. [...]
