Title: Police arrest 150 dark web vendors of illegal drugs and guns
Date: 2021-10-26T10:41:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-26-police-arrest-150-dark-web-vendors-of-illegal-drugs-and-guns

[Source](https://www.bleepingcomputer.com/news/security/police-arrest-150-dark-web-vendors-of-illegal-drugs-and-guns/){:target="_blank" rel="noopener"}

> Law enforcement authorities arrested 150 suspects allegedly involved in selling and buying illicit goods on DarkMarket, the largest illegal marketplace on the dark web when it was taken down in January 2021. [...]
