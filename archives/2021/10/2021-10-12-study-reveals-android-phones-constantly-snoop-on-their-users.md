Title: Study reveals Android phones constantly snoop on their users
Date: 2021-10-12T09:34:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-10-12-study-reveals-android-phones-constantly-snoop-on-their-users

[Source](https://www.bleepingcomputer.com/news/security/study-reveals-android-phones-constantly-snoop-on-their-users/){:target="_blank" rel="noopener"}

> A new study by a team of university researchers in the UK has unveiled a host of privacy issues that arise from using Android smartphones. [...]
