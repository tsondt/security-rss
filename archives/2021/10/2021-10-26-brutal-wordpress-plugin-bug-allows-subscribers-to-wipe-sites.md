Title: Brutal WordPress plugin bug allows subscribers to wipe sites
Date: 2021-10-26T15:19:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-26-brutal-wordpress-plugin-bug-allows-subscribers-to-wipe-sites

[Source](https://www.bleepingcomputer.com/news/security/brutal-wordpress-plugin-bug-allows-subscribers-to-wipe-sites/){:target="_blank" rel="noopener"}

> A high severity security flaw found in a WordPress plugin with more than 8,000 active installs can let authenticated attackers reset and wipe vulnerable websites. [...]
