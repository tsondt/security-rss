Title: UK data watchdog calls for end-to-end encryption across video chat apps by default
Date: 2021-10-29T14:28:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-29-uk-data-watchdog-calls-for-end-to-end-encryption-across-video-chat-apps-by-default

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/29/ico_end_to_end_encryption_call_video_apps/){:target="_blank" rel="noopener"}

> Then backtracks and tells El Reg: 'It's not a formal opinion' Britain's new Information Commissioner has called for video conferencing companies to enable end-to-end encryption on their products – even as police managers and politicians condemn the technology and demand its removal.... [...]
