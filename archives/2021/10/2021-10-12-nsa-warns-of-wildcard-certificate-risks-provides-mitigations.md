Title: NSA warns of wildcard certificate risks, provides mitigations
Date: 2021-10-12T02:23:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-12-nsa-warns-of-wildcard-certificate-risks-provides-mitigations

[Source](https://www.bleepingcomputer.com/news/security/nsa-warns-of-wildcard-certificate-risks-provides-mitigations/){:target="_blank" rel="noopener"}

> The U.S. National Security Agency (NSA) is warning of the dangers stemming from the use of broadly-scoped certificates to authenticate multiple servers in an organization. These include a recently disclosed ALPACA technique that could be used for various traffic redirect attacks. [...]
