Title: Adobe’s Surprise Security Bulletin Dominated by Critical Patches
Date: 2021-10-27T19:13:47+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-10-27-adobes-surprise-security-bulletin-dominated-by-critical-patches

[Source](https://threatpost.com/critical-patches-adobe-security-bulletin/175825/){:target="_blank" rel="noopener"}

> Out of 92 security vulnerabilities, 66 are rated critical in severity, mostly allowing code execution. The most severe can lead to information disclosure. [...]
