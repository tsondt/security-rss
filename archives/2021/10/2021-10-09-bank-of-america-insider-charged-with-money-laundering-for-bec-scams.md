Title: Bank of America insider charged with money laundering for BEC scams
Date: 2021-10-09T12:08:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-09-bank-of-america-insider-charged-with-money-laundering-for-bec-scams

[Source](https://www.bleepingcomputer.com/news/security/bank-of-america-insider-charged-with-money-laundering-for-bec-scams/){:target="_blank" rel="noopener"}

> The U.S. District Court for the Eastern District of Virginia has charged three men with money laundering and aggravated identity theft after allegedly conducting a business email compromise (BEC) scheme. [...]
