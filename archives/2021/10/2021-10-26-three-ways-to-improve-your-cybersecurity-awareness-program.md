Title: Three ways to improve your cybersecurity awareness program
Date: 2021-10-26T17:07:54+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Compliance;cybersecurity;Cybersecurity awareness;Security Blog;Training
Slug: 2021-10-26-three-ways-to-improve-your-cybersecurity-awareness-program

[Source](https://aws.amazon.com/blogs/security/three-ways-to-improve-your-cybersecurity-awareness-program/){:target="_blank" rel="noopener"}

> Raising the bar on cybersecurity starts with education. That’s why we announced in August that Amazon is making its internal Cybersecurity Awareness Training Program available to businesses and individuals for free starting this month. This is the same annual training we provide our employees to help them better understand and anticipate potential cybersecurity risks. The [...]
