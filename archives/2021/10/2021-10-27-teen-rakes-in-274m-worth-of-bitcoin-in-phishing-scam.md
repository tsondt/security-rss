Title: Teen Rakes in $2.74M Worth of Bitcoin in Phishing Scam
Date: 2021-10-27T20:17:29+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2021-10-27-teen-rakes-in-274m-worth-of-bitcoin-in-phishing-scam

[Source](https://threatpost.com/teen-rakes-in-2-74m-worth-of-bitcoin-in-phishing-scam/175834/){:target="_blank" rel="noopener"}

> The kid was busted after abusing Google Ads to lure users to his fake gift card site. [...]
