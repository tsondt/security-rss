Title: Shrootless: Microsoft found a way to evade Apple's SIP macOS filesystem protection
Date: 2021-10-29T18:01:30+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-29-shrootless-microsoft-found-a-way-to-evade-apples-sip-macos-filesystem-protection

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/29/shrootless_macos_sip_bypass_microsoft/){:target="_blank" rel="noopener"}

> Flaw could have let miscreants slide rootkits onto your iDesktop A vulnerability in MacOS that could let a malicious person install rootkits on Apple Macs has been patched, following its discovery and disclosure by Microsoft.... [...]
