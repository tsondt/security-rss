Title: WhatsApp rolls out iOS, Android end-to-end encrypted chat backups
Date: 2021-10-14T16:31:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-14-whatsapp-rolls-out-ios-android-end-to-end-encrypted-chat-backups

[Source](https://www.bleepingcomputer.com/news/security/whatsapp-rolls-out-ios-android-end-to-end-encrypted-chat-backups/){:target="_blank" rel="noopener"}

> ​WhatsApp is rolling out end-to-end encrypted chat backups on iOS and Android to prevent anyone from accessing your chats, regardless of where they are stored. [...]
