Title: Trickbot arrest: Russian national extradited to US for alleged role in developing notorious banking trojan
Date: 2021-10-29T13:21:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-29-trickbot-arrest-russian-national-extradited-to-us-for-alleged-role-in-developing-notorious-banking-trojan

[Source](https://portswigger.net/daily-swig/trickbot-arrest-russian-national-extradited-to-us-for-alleged-role-in-developing-notorious-banking-trojan){:target="_blank" rel="noopener"}

> Vladimir Dunaev made his first appearance in federal court this week [...]
