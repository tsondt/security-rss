Title: Russian cybercrime gang targets finance firms with stealthy macros
Date: 2021-10-15T09:58:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-15-russian-cybercrime-gang-targets-finance-firms-with-stealthy-macros

[Source](https://www.bleepingcomputer.com/news/security/russian-cybercrime-gang-targets-finance-firms-with-stealthy-macros/){:target="_blank" rel="noopener"}

> A new phishing campaign dubbed MirrorBlast is deploying weaponized Excel documents that are extremely difficult to detect to compromise financial service organizations [...]
