Title: Russian spies reportedly used SolarWinds hack to steal US counterintelligence details
Date: 2021-10-07T19:30:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-07-russian-spies-reportedly-used-solarwinds-hack-to-steal-us-counterintelligence-details

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/07/solarwinds_russia_us_counterintelligence_details/){:target="_blank" rel="noopener"}

> Jackpot moment for SVR operatives Russia's SVR spy agency made off with information about US counterintelligence investigations in the wake of the SolarWinds hack, according to people familiar with the American government cleanup operation.... [...]
