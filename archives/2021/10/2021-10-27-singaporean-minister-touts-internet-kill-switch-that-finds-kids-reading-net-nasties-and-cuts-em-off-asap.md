Title: Singaporean minister touts internet 'kill switch' that finds kids reading net nasties and cuts 'em off ASAP
Date: 2021-10-27T05:15:04+00:00
Author: Team Register
Category: The Register
Tags: 
Slug: 2021-10-27-singaporean-minister-touts-internet-kill-switch-that-finds-kids-reading-net-nasties-and-cuts-em-off-asap

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/27/ng_eng_hen_speech/){:target="_blank" rel="noopener"}

> Fancies a real-time crowdsourced content rating scheme too A Minister in the Singapore government has suggested the creation of an internet kill switch that would prevent minors from reading questionable material online – perhaps using ratings of content created in real time by crowdsourced contributors.... [...]
