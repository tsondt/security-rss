Title: Email phishing crapcannon operators TA505 are back from the dead, researchers warn
Date: 2021-10-19T17:15:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-19-email-phishing-crapcannon-operators-ta505-are-back-from-the-dead-researchers-warn

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/19/ta505_email_phishing_threat_group_returns/){:target="_blank" rel="noopener"}

> And they're packing a new dirty RAT as well A prolific email phishing threat actor – TA505 – is back from the dead, according to enterprise security software slinger Proofpoint.... [...]
