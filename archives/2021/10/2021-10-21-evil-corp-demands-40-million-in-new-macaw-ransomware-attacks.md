Title: Evil Corp demands $40 million in new Macaw ransomware attacks
Date: 2021-10-21T15:07:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-21-evil-corp-demands-40-million-in-new-macaw-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/evil-corp-demands-40-million-in-new-macaw-ransomware-attacks/){:target="_blank" rel="noopener"}

> Evil Corp has launched a new ransomware called Macaw Locker to evade US sanctions that prevent victims from making ransom payments. [...]
