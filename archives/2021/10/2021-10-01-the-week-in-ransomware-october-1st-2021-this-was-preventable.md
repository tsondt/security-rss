Title: The Week in Ransomware - October 1st 2021 - "This was preventable"
Date: 2021-10-01T16:33:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-01-the-week-in-ransomware-october-1st-2021-this-was-preventable

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-1st-2021-this-was-preventable/){:target="_blank" rel="noopener"}

> This week comes with reports of a hospital ransomware attack that led to the death of a baby and new efforts by governments worldwide to combat ransomware. [...]
