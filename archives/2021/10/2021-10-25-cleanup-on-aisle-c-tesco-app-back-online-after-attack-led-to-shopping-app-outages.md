Title: Cleanup on aisle C: Tesco app back online after attack led to shopping app outages
Date: 2021-10-25T05:03:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-10-25-cleanup-on-aisle-c-tesco-app-back-online-after-attack-led-to-shopping-app-outages

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/25/tesco_outage/){:target="_blank" rel="noopener"}

> With an average 1.27 million orders a week, many customers left hacked off Updated The UK's largest retailer, supermarket titan Tesco, has restored its online operations after an attack left its customers unable to order, amend, or cancel deliveries for two days.... [...]
