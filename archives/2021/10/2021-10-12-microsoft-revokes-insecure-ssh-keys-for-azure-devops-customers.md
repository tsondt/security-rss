Title: Microsoft revokes insecure SSH keys for Azure DevOps customers
Date: 2021-10-12T08:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-12-microsoft-revokes-insecure-ssh-keys-for-azure-devops-customers

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-revokes-insecure-ssh-keys-for-azure-devops-customers/){:target="_blank" rel="noopener"}

> Microsoft revoked insecure SSH keys some Azure DevOps have generated using a GitKraken git GUI client version impacted by an underlying issue found in one of its dependencies. [...]
