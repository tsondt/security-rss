Title: Zales.com Leaked Customer Data, Just Like Sister Firms Jared, Kay Jewelers Did in 2018
Date: 2021-10-28T18:54:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Brandon Sheehy;Jared;Kay Jeweler;Signet Jewelers;Zales.com
Slug: 2021-10-28-zalescom-leaked-customer-data-just-like-sister-firms-jared-kay-jewelers-did-in-2018

[Source](https://krebsonsecurity.com/2021/10/zales-com-leaked-customer-data-just-like-sister-firms-jared-kay-jewelers-did-in-2018/){:target="_blank" rel="noopener"}

> In December 2018, bling vendor Signet Jewelers fixed a weakness in their Kay Jewelers and Jared websites that exposed the order information for all of their online customers. This week, Signet subsidiary Zales.com updated its website to remediate a nearly identical customer data exposure. Last week, KrebsOnSecurity heard from a reader who was browsing Zales.com and suddenly found they were looking at someone else’s order information on the website, including their name, billing address, shipping address, phone number, email address, items and total amount purchased, delivery date, tracking link, and the last four digits of the customer’s credit card number. [...]
