Title: Lawsuit claims hospital ransomware infection cost baby her life
Date: 2021-10-04T19:28:05+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-10-04-lawsuit-claims-hospital-ransomware-infection-cost-baby-her-life

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/04/in_brief_security/){:target="_blank" rel="noopener"}

> Plus Russia arrests security boss, two Chrome flaws exploited In Brief A hospital that continued to admit patients during a ransomware attack has been sued over claims that a baby died after doctors and nurses failed to spot there was a problem due to networks being shut down.... [...]
