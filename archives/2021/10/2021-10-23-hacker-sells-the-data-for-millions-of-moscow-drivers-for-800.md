Title: Hacker sells the data for millions of Moscow drivers for $800
Date: 2021-10-23T11:12:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-23-hacker-sells-the-data-for-millions-of-moscow-drivers-for-800

[Source](https://www.bleepingcomputer.com/news/security/hacker-sells-the-data-for-millions-of-moscow-drivers-for-800/){:target="_blank" rel="noopener"}

> Hackers are selling a stolen database containing 50 million records of Moscow driver data on an underground forum for only $800. [...]
