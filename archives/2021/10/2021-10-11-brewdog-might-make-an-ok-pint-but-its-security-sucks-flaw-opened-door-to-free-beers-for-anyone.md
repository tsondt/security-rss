Title: Brewdog might make an OK pint but its security sucks: Flaw opened door to free beers for anyone
Date: 2021-10-11T11:31:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-10-11-brewdog-might-make-an-ok-pint-but-its-security-sucks-flaw-opened-door-to-free-beers-for-anyone

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/11/in_brief_security/){:target="_blank" rel="noopener"}

> Plus two failings this week at Apache and Twitch and nostalgia for Flash fans In brief Hipster beer maker Brewdog has been caught out by a basic, but potentially very expensive, security problem, and the team that discovered it says the Scottish tipple-merchant's response was hardly encouraging.... [...]
