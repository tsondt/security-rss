Title: Nation-State Attacker of Telecommunications Networks
Date: 2021-10-22T11:13:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;attribution;China;cyberespionage;espionage;hacking
Slug: 2021-10-22-nation-state-attacker-of-telecommunications-networks

[Source](https://www.schneier.com/blog/archives/2021/10/nation-state-attacker-of-telecommunications-networks.html){:target="_blank" rel="noopener"}

> Someone has been hacking telecommunications networks around the world: LightBasin (aka UNC1945) is an activity cluster that has been consistently targeting the telecommunications sector at a global scale since at least 2016, leveraging custom tools and an in-depth knowledge of telecommunications network architectures. Recent findings highlight this cluster’s extensive knowledge of telecommunications protocols, including the emulation of these protocols to facilitate command and control (C2) and utilizing scanning/packet-capture tools to retrieve highly specific information from mobile communication infrastructure, such as subscriber information and call metadata. The nature of the data targeted by the actor aligns with information likely to be [...]
