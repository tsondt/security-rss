Title: Yet again, Cream Finance skimmed by crooks: $130m in crypto assets stolen
Date: 2021-10-28T19:59:39+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-10-28-yet-again-cream-finance-skimmed-by-crooks-130m-in-crypto-assets-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/28/cream_ethereum_theft/){:target="_blank" rel="noopener"}

> Third time's the unlucky charm for loan outfit Decentralized finance biz Cream Finance became further decentralized on Wednesday with the theft of $130m worth of crypto assets from its Ethereum lending protocol.... [...]
