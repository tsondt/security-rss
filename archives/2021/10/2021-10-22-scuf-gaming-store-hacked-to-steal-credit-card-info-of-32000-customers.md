Title: SCUF Gaming store hacked to steal credit card info of 32,000 customers
Date: 2021-10-22T14:26:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-22-scuf-gaming-store-hacked-to-steal-credit-card-info-of-32000-customers

[Source](https://www.bleepingcomputer.com/news/security/scuf-gaming-store-hacked-to-steal-credit-card-info-of-32-000-customers/){:target="_blank" rel="noopener"}

> SCUF Gaming International, a leading manufacturer of custom PC and console controllers, is notifying customers that its website was hacked in February to plant a malicious script used to steal their credit card information. [...]
