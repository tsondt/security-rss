Title: Enabling data classification for Amazon RDS database with Macie
Date: 2021-10-05T16:59:53+00:00
Author: Bruno Silveira
Category: AWS Security
Tags: Amazon Macie;Amazon RDS;Intermediate (200);Security, Identity, & Compliance;AWS DMS;GDPR;Security Blog
Slug: 2021-10-05-enabling-data-classification-for-amazon-rds-database-with-macie

[Source](https://aws.amazon.com/blogs/security/enabling-data-classification-for-amazon-rds-database-with-amazon-macie/){:target="_blank" rel="noopener"}

> Customers have been asking us about ways to use Amazon Macie data discovery on their Amazon Relational Database Service (Amazon RDS) instances. This post presents how to do so using AWS Database Migration Service (AWS DMS) to extract data from Amazon RDS, store it on Amazon Simple Storage Service (Amazon S3), and then classify the [...]
