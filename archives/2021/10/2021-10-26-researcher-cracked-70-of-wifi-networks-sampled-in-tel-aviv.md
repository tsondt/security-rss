Title: Researcher cracked 70% of WiFi networks sampled in Tel Aviv
Date: 2021-10-26T12:42:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-26-researcher-cracked-70-of-wifi-networks-sampled-in-tel-aviv

[Source](https://www.bleepingcomputer.com/news/security/researcher-cracked-70-percent-of-wifi-networks-sampled-in-tel-aviv/){:target="_blank" rel="noopener"}

> A researcher has managed to crack 70% of a 5,000 WiFi network sample in his hometown, Tel Aviv, to prove that home networks are severely unsecured and easy to hijack. [...]
