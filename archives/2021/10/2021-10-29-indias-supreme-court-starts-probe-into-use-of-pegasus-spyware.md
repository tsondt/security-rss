Title: India's Supreme Court starts probe into use of Pegasus spyware
Date: 2021-10-29T04:03:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-29-indias-supreme-court-starts-probe-into-use-of-pegasus-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/29/india_nso_pegasus_probe/){:target="_blank" rel="noopener"}

> Government offered to investigate itself – Court politely declined that kind suggestion India's Supreme Court has taken the unusual step of commissioning a Technical Committee to investigate whether the national government used the NSO Group's "Pegasus" spyware on its citizens.... [...]
