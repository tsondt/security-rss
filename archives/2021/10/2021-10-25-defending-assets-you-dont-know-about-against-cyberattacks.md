Title: Defending Assets You Don’t Know About Against Cyberattacks
Date: 2021-10-25T21:41:20+00:00
Author: David “moose” Wolpoff
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;InfoSec Insider;Mobile Security;Vulnerabilities;Web Security
Slug: 2021-10-25-defending-assets-you-dont-know-about-against-cyberattacks

[Source](https://threatpost.com/defending-unknown-assets-cyberattacks/175730/){:target="_blank" rel="noopener"}

> No security defense is perfect, and shadow IT means no company can inventory every single asset that it has. David “moose” Wolpoff, CTO at Randori, discusses strategies for core asset protection given this reality. [...]
