Title: Google Crushes YouTube Cookie-Stealing Channel Hijackers
Date: 2021-10-20T19:45:21+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2021-10-20-google-crushes-youtube-cookie-stealing-channel-hijackers

[Source](https://threatpost.com/google-youtube-channel-hijackers-cryptocurrency-scams/175617/){:target="_blank" rel="noopener"}

> Google has caught and brushed off a bunch of cookie-stealing YouTube channel hijackers who were running cryptocurrency scams on, or auctioning off, ripped-off channels. [...]
