Title: Microsoft: Shrootless bug lets hackers install macOS rootkits
Date: 2021-10-28T12:44:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple;Microsoft
Slug: 2021-10-28-microsoft-shrootless-bug-lets-hackers-install-macos-rootkits

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shrootless-bug-lets-hackers-install-macos-rootkits/){:target="_blank" rel="noopener"}

> Attackers could use a new macOS vulnerability discovered by Microsoft to bypass System Integrity Protection (SIP) and perform arbitrary operations, elevate privileges to root, and install rootkits on vulnerable devices. [...]
