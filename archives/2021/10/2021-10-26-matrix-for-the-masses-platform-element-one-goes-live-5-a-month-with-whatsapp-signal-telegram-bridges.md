Title: Matrix for the masses platform Element One goes live: $5 a month with WhatsApp, Signal, Telegram bridges
Date: 2021-10-26T11:41:59+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-10-26-matrix-for-the-masses-platform-element-one-goes-live-5-a-month-with-whatsapp-signal-telegram-bridges

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/26/element_one/){:target="_blank" rel="noopener"}

> New package aimed at consumers Element, which makes Matrix-based communications and collaboration tools, has launched a consumer-oriented version of its messaging platform, complete with bridges for WhatsApp, Signal and Telegram.... [...]
