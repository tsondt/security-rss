Title: New AWS workbook for New Zealand financial services customers
Date: 2021-10-14T23:53:37+00:00
Author: Julian Busic
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;banking;Financial Services;Guidance on Cyber Resilience;New Zealand;Reserve Bank of New Zealand;Security Blog
Slug: 2021-10-14-new-aws-workbook-for-new-zealand-financial-services-customers

[Source](https://aws.amazon.com/blogs/security/new-aws-workbook-for-new-zealand-financial-services-customers/){:target="_blank" rel="noopener"}

> We are pleased to announce a new AWS workbook designed to help New Zealand financial services customers align with the Reserve Bank of New Zealand (RBNZ) Guidance on Cyber Resilience. The RBNZ Guidance on Cyber Resilience sets out the RBNZ expectations for its regulated entities regarding cyber resilience, and aims to raise awareness and promote [...]
