Title: Microsoft Patch Tuesday bug harvest festival comes to town
Date: 2021-10-12T20:12:35+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-10-12-microsoft-patch-tuesday-bug-harvest-festival-comes-to-town

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/12/microsoft_patch_tuesday/){:target="_blank" rel="noopener"}

> With 71 new CVEs, there are patches enough for everyone Microsoft's October Patch Tuesday has arrived with fixes for 71 new CVEs, two patch revisions to address bugs from previous months that just won't die, and three CVEs tied to OpenSSL flaws. That's in addition to eight Edge-Chromium CVEs dealt with earlier this month.... [...]
