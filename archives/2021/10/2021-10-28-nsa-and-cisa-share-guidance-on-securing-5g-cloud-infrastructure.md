Title: NSA and CISA share guidance on securing 5G cloud infrastructure
Date: 2021-10-28T13:06:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-28-nsa-and-cisa-share-guidance-on-securing-5g-cloud-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/nsa-and-cisa-share-guidance-on-securing-5g-cloud-infrastructure/){:target="_blank" rel="noopener"}

> CISA and the NSA shared guidance on securing cloud-native 5G networks from attacks seeking to compromise information or deny access by taking down cloud infrastructure. [...]
