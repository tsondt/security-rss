Title: Google to auto-enroll 150 million user accounts into 2FA
Date: 2021-10-05T15:53:23-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Security
Slug: 2021-10-05-google-to-auto-enroll-150-million-user-accounts-into-2fa

[Source](https://www.bleepingcomputer.com/news/google/google-to-auto-enroll-150-million-user-accounts-into-2fa/){:target="_blank" rel="noopener"}

> Google announced today that they plan on auto-enrolling 150 million accounts into two-factor authentication by the end of 2021. [...]
