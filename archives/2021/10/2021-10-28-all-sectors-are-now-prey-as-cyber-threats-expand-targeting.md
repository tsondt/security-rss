Title: All Sectors Are Now Prey as Cyber Threats Expand Targeting
Date: 2021-10-28T21:54:44+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: Hacks;InfoSec Insider;IoT;Malware;Web Security
Slug: 2021-10-28-all-sectors-are-now-prey-as-cyber-threats-expand-targeting

[Source](https://threatpost.com/cyber-threats-targeting-all-sectors/175873/){:target="_blank" rel="noopener"}

> Aamir Lakhani, security researcher at Fortinet, says no sector is off limits these days: It's time for everyone to strengthen the kill chain. [...]
