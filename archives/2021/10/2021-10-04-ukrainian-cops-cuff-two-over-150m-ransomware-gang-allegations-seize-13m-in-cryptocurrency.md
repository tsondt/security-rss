Title: Ukrainian cops cuff two over $150m ransomware gang allegations, seize $1.3m in cryptocurrency
Date: 2021-10-04T14:44:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-04-ukrainian-cops-cuff-two-over-150m-ransomware-gang-allegations-seize-13m-in-cryptocurrency

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/04/ukraine_arrests_two_ransomware_150m_allegations_revil/){:target="_blank" rel="noopener"}

> Was it REvil? We don't (yet) know for sure Ukrainian police have reportedly arrested two members of a ransomware gang – and while some have fingered REvil, no firm details have been published by cops from multiple countries.... [...]
