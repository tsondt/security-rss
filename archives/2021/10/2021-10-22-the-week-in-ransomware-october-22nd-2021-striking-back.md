Title: The Week in Ransomware - October 22nd 2021 - Striking back
Date: 2021-10-22T17:47:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-22-the-week-in-ransomware-october-22nd-2021-striking-back

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-22nd-2021-striking-back/){:target="_blank" rel="noopener"}

> Between law enforcement operations, REvil's second shut down, and ransomware gangs' response to the hacking of their servers, it has been quite the week. [...]
