Title: Friday Squid Blogging: Squid Eating Maine Shrimp
Date: 2021-10-22T21:10:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-10-22-friday-squid-blogging-squid-eating-maine-shrimp

[Source](https://www.schneier.com/blog/archives/2021/10/friday-squid-blogging-squid-eating-maine-shrimp.html){:target="_blank" rel="noopener"}

> Squid are eating Maine shrimp, causing a collapse of the ecosystem. This seems to be a result of climate change. Maine’s shrimp fishery has been closed for nearly a decade since the stock’s collapse in 2013. Scientists are now saying a species of squid that came into the Gulf of Maine during a historic ocean heatwave the year before may have been a “major player” in the shrimp’s downturn. In 2012, the Gulf of Maine experienced some of its warmest temperatures in decades. Within a couple of years, the cold-water-loving northern shrimp had rapidly declined and the fishery, a small [...]
