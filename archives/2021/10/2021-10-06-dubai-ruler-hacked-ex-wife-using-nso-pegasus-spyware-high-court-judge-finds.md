Title: Dubai ruler hacked ex-wife using NSO Pegasus spyware, high court judge finds
Date: 2021-10-06T15:00:27+00:00
Author: Haroon Siddique Legal affairs correspondent
Category: The Guardian
Tags: Sheikh Mohammed bin Rashid al-Maktoum;Hacking;Malware;Dubai;United Arab Emirates;World news;Technology;Middle East and North Africa;Data and computer security;Law
Slug: 2021-10-06-dubai-ruler-hacked-ex-wife-using-nso-pegasus-spyware-high-court-judge-finds

[Source](https://www.theguardian.com/world/2021/oct/06/dubai-ruler-hacked-ex-wife-using-nso-pegasus-spyware-high-court-judge-finds-sheikh-mohammed-princess-haya){:target="_blank" rel="noopener"}

> Sheikh Mohammed used spyware on Princess Haya and five associates in unlawful abuse of power, judge rules ‘The walls are closing in on me’: the hacking of Princess Haya Ruling in Princess Haya case raises fresh questions for Cherie Blair The ruler of Dubai hacked the phone of his ex-wife Princess Haya using NSO Group’s controversial Pegasus spyware in an unlawful abuse of power and trust, a senior high court judge has ruled. The president of the family division found that agents acting on behalf of Sheikh Mohammed bin Rashid al-Maktoum, who is also prime minister of the United Arab [...]
