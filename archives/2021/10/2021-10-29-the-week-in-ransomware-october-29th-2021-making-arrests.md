Title: The Week in Ransomware - October 29th 2021 - Making arrests
Date: 2021-10-29T17:43:14-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-29-the-week-in-ransomware-october-29th-2021-making-arrests

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-29th-2021-making-arrests/){:target="_blank" rel="noopener"}

> This week, international law enforcement operations went on the offensive, making arrests in numerous countries for ransomware-related activities. [...]
