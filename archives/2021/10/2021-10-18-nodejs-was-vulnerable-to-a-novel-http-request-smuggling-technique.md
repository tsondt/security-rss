Title: Node.js was vulnerable to a novel HTTP request smuggling technique
Date: 2021-10-18T15:16:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-18-nodejs-was-vulnerable-to-a-novel-http-request-smuggling-technique

[Source](https://portswigger.net/daily-swig/node-js-was-vulnerable-to-a-novel-http-request-smuggling-technique){:target="_blank" rel="noopener"}

> Bad line termination and incorrect parsing of chunk extensions exposed one of two HRS flaws [...]
