Title: Conti Ransom Gang Starts Selling Access to Victims
Date: 2021-10-25T19:49:37+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other;Ransomware;Conti ransomware;Digital Shadows;Emsisoft;Fabian Wosar;FIN 12;Ivan Righi;rEvil;Ryuk
Slug: 2021-10-25-conti-ransom-gang-starts-selling-access-to-victims

[Source](https://krebsonsecurity.com/2021/10/conti-ransom-gang-starts-selling-access-to-victims/){:target="_blank" rel="noopener"}

> The Conti ransomware affiliate program appears to have altered its business plan recently. Organizations infected with Conti’s malware who refuse to negotiate a ransom payment are added to Conti’s victim shaming blog, where confidential files stolen from victims may be published or sold. But sometime over the past 48 hours, the cybercriminal syndicate updated its victim shaming blog to indicate that it is now selling access to many of the organizations it has hacked. A redacted screenshot of the Conti News victim shaming blog. “We are looking for a buyer to access the network of this organization and sell data [...]
