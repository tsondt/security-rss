Title: Accenture confirms data breach after August ransomware attack
Date: 2021-10-15T10:49:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-15-accenture-confirms-data-breach-after-august-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/accenture-confirms-data-breach-after-august-ransomware-attack/){:target="_blank" rel="noopener"}

> Global IT consultancy giant Accenture confirmed that LockBit ransomware operators stole data from its systems during an attack that hit the company's systems in August 2021. [...]
