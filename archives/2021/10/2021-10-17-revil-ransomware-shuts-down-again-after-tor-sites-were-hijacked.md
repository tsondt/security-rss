Title: REvil ransomware shuts down again after Tor sites were hijacked
Date: 2021-10-17T19:19:46-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-10-17-revil-ransomware-shuts-down-again-after-tor-sites-were-hijacked

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-shuts-down-again-after-tor-sites-were-hijacked/){:target="_blank" rel="noopener"}

> The REvil ransomware operation has likely shut down once again after an unknown person hijacked their Tor payment portal and data leak blog. [...]
