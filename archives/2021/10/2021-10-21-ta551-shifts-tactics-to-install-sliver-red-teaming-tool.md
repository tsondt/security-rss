Title: TA551 Shifts Tactics to Install Sliver Red-Teaming Tool
Date: 2021-10-21T19:31:40+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-10-21-ta551-shifts-tactics-to-install-sliver-red-teaming-tool

[Source](https://threatpost.com/ta551-tactics-sliver-red-teaming/175651/){:target="_blank" rel="noopener"}

> A new email campaign from the threat group uses the attack-simulation framework in a likely leadup to ransomware deployment. [...]
