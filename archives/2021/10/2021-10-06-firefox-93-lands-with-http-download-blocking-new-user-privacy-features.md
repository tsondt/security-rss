Title: Firefox 93 lands with HTTP download blocking, new user privacy features
Date: 2021-10-06T13:36:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-06-firefox-93-lands-with-http-download-blocking-new-user-privacy-features

[Source](https://portswigger.net/daily-swig/firefox-93-lands-with-http-download-blocking-new-user-privacy-features){:target="_blank" rel="noopener"}

> Roadblocks erected against untrusted content and unwanted ads [...]
