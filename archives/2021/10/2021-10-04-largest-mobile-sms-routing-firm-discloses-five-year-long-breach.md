Title: Largest mobile SMS routing firm discloses five-year-long breach
Date: 2021-10-04T17:42:04-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-10-04-largest-mobile-sms-routing-firm-discloses-five-year-long-breach

[Source](https://www.bleepingcomputer.com/news/security/largest-mobile-sms-routing-firm-discloses-five-year-long-breach/){:target="_blank" rel="noopener"}

> Syniverse, a service provider for most telecommunications companies, disclosed that hackers had access to its databases over the past five years and compromised login credentials belonging to hundreds of customers. [...]
