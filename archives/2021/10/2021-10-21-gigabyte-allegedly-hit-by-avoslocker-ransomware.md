Title: Gigabyte Allegedly Hit by AvosLocker Ransomware
Date: 2021-10-21T17:33:24+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Breaking News;Malware;News;Web Security
Slug: 2021-10-21-gigabyte-allegedly-hit-by-avoslocker-ransomware

[Source](https://threatpost.com/gigabyte-avoslocker-ransomware-gang/175642/){:target="_blank" rel="noopener"}

> If AvosLocker stole Gigabyte's master keys, threat actors could force hardware to download fake drivers or BIOS updates in a supply-chain attack a la SolarWinds. [...]
