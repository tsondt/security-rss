Title: Using Kerberos for Authentication Relay Attacks
Date: 2021-10-20T09:26:00-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2021-10-20-using-kerberos-for-authentication-relay-attacks

[Source](https://googleprojectzero.blogspot.com/2021/10/using-kerberos-for-authentication-relay.html){:target="_blank" rel="noopener"}

> Posted by James Forshaw, Project Zero This blog post is a summary of some research I've been doing into relaying Kerberos authentication in Windows domain environments. To keep this blog shorter I am going to assume you have a working knowledge of Windows network authentication, and specifically Kerberos and NTLM. For a quick primer on Kerberos see this page which is part of Microsoft's Kerberos extension documentation or you can always read RFC4120. Background Windows based enterprise networks rely on network authentication protocols, such as NT Lan Manager (NTLM) and Kerberos to implement single sign on. These protocols allow domain [...]
