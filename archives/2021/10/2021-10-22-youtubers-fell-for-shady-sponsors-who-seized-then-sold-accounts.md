Title: YouTubers fell for shady 'sponsors' who seized, then sold, accounts
Date: 2021-10-22T06:28:52+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-10-22-youtubers-fell-for-shady-sponsors-who-seized-then-sold-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/22/russian_crims_lured_youtubers_with/){:target="_blank" rel="noopener"}

> Vid-slingers had been asking how this happened for years, even while their channels were spruiking dodgy crypto After years of complaints from YouTubers, Google has pinpointed the root cause of a series of account hijackings: software sponsorship deals that delivered malware.... [...]
