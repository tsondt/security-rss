Title: Microsoft warns of rise in password sprays targeting cloud accounts
Date: 2021-10-31T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-31-microsoft-warns-of-rise-in-password-sprays-targeting-cloud-accounts

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-warns-of-rise-in-password-sprays-targeting-cloud-accounts/){:target="_blank" rel="noopener"}

> The Microsoft Detection and Response Team (DART) says it detected an increase in password spray attacks targeting privileged cloud accounts and high-profile identities such as C-level executives. [...]
