Title: Microsoft: Azure customer hit by record DDoS attack in August
Date: 2021-10-12T04:30:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-10-12-microsoft-azure-customer-hit-by-record-ddos-attack-in-august

[Source](https://www.bleepingcomputer.com/news/security/microsoft-azure-customer-hit-by-record-ddos-attack-in-august/){:target="_blank" rel="noopener"}

> Microsoft has mitigated a record 2.4 Tbps (terabytes per second) Distributed Denial-of-Service (DDoS) attack targeting an European Azure customer during the last week of August. [...]
