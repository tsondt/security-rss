Title: At least 13 phone firms hit by suspected Chinese hackers since 2019, say experts
Date: 2021-10-19T14:42:24+00:00
Author: Dan Sabbagh Defence and security editor
Category: The Guardian
Tags: Hacking;Telecoms;China;Telecommunications industry;World news;Asia Pacific;Technology;Business;Espionage;Data and computer security;Cyberwar;Internet
Slug: 2021-10-19-at-least-13-phone-firms-hit-by-suspected-chinese-hackers-since-2019-say-experts

[Source](https://www.theguardian.com/technology/2021/oct/19/phone-firms-hit-by-suspected-chinese-hackers-lightbasin-china){:target="_blank" rel="noopener"}

> LightBasin hackers were able to obtain subscriber information and call metadata, says CrowdStrike At least 13 phone companies around the world have been compromised since 2019 by sophisticated hackers who are believed to come from China, a cybersecurity expert group has said. The roaming hackers – known as LightBasin – were able to “search and find” individual mobile phones and “target accordingly”, according to CrowdStrike, a group regularly cited by western intelligence. Continue reading... [...]
