Title: Chinese tech minister says he's 'dealt with' 73,000 sites that breached the law
Date: 2021-10-18T07:56:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-10-18-chinese-tech-minister-says-hes-dealt-with-73000-sites-that-breached-the-law

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/18/china_tech_crackdown_impact/){:target="_blank" rel="noopener"}

> Ongoing crackdown saw apps 1.83 million apps tested, 4,200 told to clean up their act, pop-up ads popped China's Minister of Industry and Information Technology, Xiao Yaqing, has given a rare interview in which he signalled the nation's crackdown on the internet and predatory companies will continue.... [...]
