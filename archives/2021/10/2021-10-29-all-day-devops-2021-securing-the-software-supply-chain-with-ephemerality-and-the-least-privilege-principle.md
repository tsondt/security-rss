Title: All Day DevOps 2021: Securing the software supply chain with ephemerality and the least-privilege principle
Date: 2021-10-29T15:45:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-29-all-day-devops-2021-securing-the-software-supply-chain-with-ephemerality-and-the-least-privilege-principle

[Source](https://portswigger.net/daily-swig/all-day-devops-2021-securing-the-software-supply-chain-with-ephemerality-and-the-least-privilege-principle){:target="_blank" rel="noopener"}

> ‘The new boundary for systems engineering is how ephemeral can you make any given process with a privilege’ [...]
