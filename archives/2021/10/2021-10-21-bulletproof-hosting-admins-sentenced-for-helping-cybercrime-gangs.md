Title: Bulletproof hosting admins sentenced for helping cybercrime gangs
Date: 2021-10-21T07:52:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-21-bulletproof-hosting-admins-sentenced-for-helping-cybercrime-gangs

[Source](https://www.bleepingcomputer.com/news/security/bulletproof-hosting-admins-sentenced-for-helping-cybercrime-gangs/){:target="_blank" rel="noopener"}

> Two Eastern European men were sentenced to prison on Racketeer Influenced Corrupt Organization (RICO) charges for bulletproof hosting services used by multiple cybercrime operations to target US organizations. [...]
