Title: Suspected REvil Gang Insider Identified
Date: 2021-10-28T20:04:35+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-10-28-suspected-revil-gang-insider-identified

[Source](https://threatpost.com/revil-ransomware-core-member/175863/){:target="_blank" rel="noopener"}

> German investigators have identified a deep-pocketed, big-spending Russian billionaire whom they suspect of being a core member of the REvil ransomware gang. [...]
