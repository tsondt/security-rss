Title: Unhappy customers and their own tricks used against them, REvil ransomware gang reportedly pulled offline by 'multi-country' operations
Date: 2021-10-22T10:43:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-10-22-unhappy-customers-and-their-own-tricks-used-against-them-revil-ransomware-gang-reportedly-pulled-offline-by-multi-country-operations

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/22/revil_offline_again/){:target="_blank" rel="noopener"}

> The second vanishing of the cybergang... for now As we noted a few days back, notorious ransomware gang REvil "disappeared" again this week. Recent reports have now shed light on why that may be.... [...]
