Title: Australia to tackle ransomware data breaches by deleting stolen files
Date: 2021-10-13T11:01:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government;Legal
Slug: 2021-10-13-australia-to-tackle-ransomware-data-breaches-by-deleting-stolen-files

[Source](https://www.bleepingcomputer.com/news/security/australia-to-tackle-ransomware-data-breaches-by-deleting-stolen-files/){:target="_blank" rel="noopener"}

> Australia's Minister for Home Affairs has announced the "Australian Government's Ransomware Action Plan," which is a set of new measures the country will adopt in an attempt to tackle the rising threat. [...]
