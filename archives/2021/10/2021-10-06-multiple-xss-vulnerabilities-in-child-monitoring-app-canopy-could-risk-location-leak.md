Title: Multiple XSS vulnerabilities in child monitoring app Canopy ‘could risk location leak’
Date: 2021-10-06T14:25:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-06-multiple-xss-vulnerabilities-in-child-monitoring-app-canopy-could-risk-location-leak

[Source](https://portswigger.net/daily-swig/multiple-xss-vulnerabilities-in-child-monitoring-app-canopy-could-risk-location-leak){:target="_blank" rel="noopener"}

> Pair of unpatched security bugs are ‘just the tip of the iceberg’ [...]
