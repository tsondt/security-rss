Title: Apache HTTP Server update fails to squash path traversal, RCE bugs
Date: 2021-10-08T13:29:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-08-apache-http-server-update-fails-to-squash-path-traversal-rce-bugs

[Source](https://portswigger.net/daily-swig/apache-http-server-update-fails-to-squash-path-traversal-rce){:target="_blank" rel="noopener"}

> Web admins told to upgrade (once again) to latest version [...]
