Title: Incident Response: 5 Principles to Boost the Infosec/Legal Relationship
Date: 2021-10-11T12:00:13+00:00
Author: Matt Dunn
Category: Threatpost
Tags: Breach;Hacks;InfoSec Insider;Privacy
Slug: 2021-10-11-incident-response-5-principles-to-boost-the-infoseclegal-relationship

[Source](https://threatpost.com/incident-response-infosec-legal-relationship/175461/){:target="_blank" rel="noopener"}

> Effective cyber-incident response means working well with legal. Matt Dunn, associate managing director for cyber-risk at Kroll, lays out how to do it. [...]
