Title: Good Grief! Ransomware gang has only gone and pwned the NRA – or so it claims
Date: 2021-10-28T11:39:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-10-28-good-grief-ransomware-gang-has-only-gone-and-pwned-the-nra-or-so-it-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/28/grief_ransomware_gang_nra/){:target="_blank" rel="noopener"}

> Between US sanctions on baddies and NRA claiming bankruptcy, what are the chances anyone’s getting paid? Grief ransomware gang took to a dark portal website where it typically publishes the data of victims that haven't paid up, to identify its latest target: the National Rifle Association (NRA).... [...]
