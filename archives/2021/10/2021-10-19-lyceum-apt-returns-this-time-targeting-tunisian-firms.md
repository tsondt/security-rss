Title: Lyceum APT Returns, This Time Targeting Tunisian Firms
Date: 2021-10-19T17:16:42+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2021-10-19-lyceum-apt-returns-this-time-targeting-tunisian-firms

[Source](https://threatpost.com/lyceum-apt-tunisian-firms/175579/){:target="_blank" rel="noopener"}

> The APT, which targets Middle-Eastern energy firms & telecoms, has been relatively quiet since its exposure but not entirely silent. It's kept up attacks through 2021 and is working on retooling its arsenal yet again. [...]
