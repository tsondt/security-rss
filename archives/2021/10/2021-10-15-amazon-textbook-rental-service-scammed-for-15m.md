Title: Amazon textbook rental service scammed for $1.5m
Date: 2021-10-15T23:24:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-10-15-amazon-textbook-rental-service-scammed-for-15m

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/15/amazon_textbook_rental/){:target="_blank" rel="noopener"}

> Michigan man arrested for borrowing costly textbooks and selling them A 36-year-old man from Portage, Michigan, was arrested on Thursday for allegedly renting thousands of textbooks from Amazon and selling them rather than returning them.... [...]
