Title: Microsoft now defends nonprofits against nation-state attacks
Date: 2021-10-21T13:23:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-10-21-microsoft-now-defends-nonprofits-against-nation-state-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-now-defends-nonprofits-against-nation-state-attacks/){:target="_blank" rel="noopener"}

> Microsoft announced today a new security program for nonprofits to provide them with protection against nation-state attacks that have increasingly targeting them in recent years. [...]
