Title: Japanese punctuation exacerbates privacy flaw that leaks one-word search terms in Google, Firefox browsers
Date: 2021-10-22T11:31:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-22-japanese-punctuation-exacerbates-privacy-flaw-that-leaks-one-word-search-terms-in-google-firefox-browsers

[Source](https://portswigger.net/daily-swig/japanese-punctuation-exacerbates-privacy-flaw-that-leaks-one-word-search-terms-in-google-firefox-browsers){:target="_blank" rel="noopener"}

> Researcher questions efficacy of proposed remedies as debate rumbles on 18 months after disclosure [...]
