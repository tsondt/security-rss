Title: UK's £5bn National Cyber Force HQ to be sited in Lancashire beside Defence Secretary's constituency
Date: 2021-10-04T15:37:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-04-uks-5bn-national-cyber-force-hq-to-be-sited-in-lancashire-beside-defence-secretarys-constituency

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/04/national_cyber_force_hq_samlesbury/){:target="_blank" rel="noopener"}

> How convenient for influx of potential new voters Britain's National Cyber Force will be based in Lancashire, the government has said – though despite obvious clues neither the Ministry of Defence nor BAE Systems will confirm the force's planned new location.... [...]
