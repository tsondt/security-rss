Title: Data-breached Guntrader website calls in liquidators, is reborn as Guntrader 2 Ltd
Date: 2021-10-29T15:27:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-10-29-data-breached-guntrader-website-calls-in-liquidators-is-reborn-as-guntrader-2-ltd

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/29/guntrader_liquidators_order/){:target="_blank" rel="noopener"}

> Viscount still helms new firm – while since-deleted posts on firm's Facebook page enrage users A British firearms sales website's owner has called in the liquidators as his company faces data breach lawsuits – while continuing to trade from a newly incorporated business.... [...]
