Title: Facebook Is Down
Date: 2021-10-04T22:55:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;DNS;Facebook;Internet;WhatsApp
Slug: 2021-10-04-facebook-is-down

[Source](https://www.schneier.com/blog/archives/2021/10/facebook-is-down.html){:target="_blank" rel="noopener"}

> Facebook — along with Instagram and WhatsApp — went down globally today. Basically, someone deleted their BGP records, which made their DNS fall apart....at approximately 11:39 a.m. ET today (15:39 UTC), someone at Facebook caused an update to be made to the company’s Border Gateway Protocol (BGP) records. BGP is a mechanism by which Internet service providers of the world share information about which providers are responsible for routing Internet traffic to which specific groups of Internet addresses. In simpler terms, sometime this morning Facebook took away the map telling the world’s computers how to find its various online properties. [...]
