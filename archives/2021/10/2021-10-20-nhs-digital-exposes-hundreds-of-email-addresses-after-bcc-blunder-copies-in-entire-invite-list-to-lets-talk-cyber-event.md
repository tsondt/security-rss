Title: NHS Digital exposes hundreds of email addresses after BCC blunder copies in entire invite list to 'Let's talk cyber' event
Date: 2021-10-20T11:28:09+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2021-10-20-nhs-digital-exposes-hundreds-of-email-addresses-after-bcc-blunder-copies-in-entire-invite-list-to-lets-talk-cyber-event

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/20/bcc_fail_nhs_digital/){:target="_blank" rel="noopener"}

> It's like rai-iiiiiin on your wedding day NHS Digital has scored a classic Mail All own-goal by dispatching not one, not two, not three, but four emails concerning an infosec breakfast briefing, each time copying the entirety of the invite list in on the messages.... [...]
