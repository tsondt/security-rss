Title: There’s a wave of ransomware coming down the pipeline. What can you do about it?
Date: 2021-10-25T18:00:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-10-25-theres-a-wave-of-ransomware-coming-down-the-pipeline-what-can-you-do-about-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/25/ransomeware_ai_defense/){:target="_blank" rel="noopener"}

> AI can help. Here’s how... Sponsored The Colonial Pipeline attack earlier this year showed just how devastating a ransomware attack is when it is targeted at critical infrastructure.... [...]
