Title: Free decryptor released for Atom Silo and LockFile ransomware
Date: 2021-10-27T14:35:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-27-free-decryptor-released-for-atom-silo-and-lockfile-ransomware

[Source](https://www.bleepingcomputer.com/news/security/free-decryptor-released-for-atom-silo-and-lockfile-ransomware/){:target="_blank" rel="noopener"}

> Avast has just released a decryption tool that will help AtomSilo and LockFile ransomware victims recover some of their files for free, without having to pay a ransom. [...]
