Title: You know what ransomware attackers really, really like? Yes, your backups…
Date: 2021-10-07T18:30:09+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-10-07-you-know-what-ransomware-attackers-really-really-like-yes-your-backups

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/07/ransome_backups_regcast/){:target="_blank" rel="noopener"}

> If you want near-zero RTO for your VMs, check out this webcast Sponsored Recovery time is everything when it comes to recovering from a cyber-attack of any description. But when it comes to ransomware, miscreants are doing all they can to set your RTO to, well, never.... [...]
