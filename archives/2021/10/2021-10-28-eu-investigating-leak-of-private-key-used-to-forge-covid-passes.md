Title: EU investigating leak of private key used to forge Covid passes
Date: 2021-10-28T05:53:55-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-10-28-eu-investigating-leak-of-private-key-used-to-forge-covid-passes

[Source](https://www.bleepingcomputer.com/news/security/eu-investigating-leak-of-private-key-used-to-forge-covid-passes/){:target="_blank" rel="noopener"}

> The private key used to sign EU Digital Covid certificates has been reportedly leaked and is being circulated on messaging apps and forums. The key has also been misused to generate forged certificates, such as those for Adolf Hitler, Mickey Mouse, Sponge Bob—all of which are being recognized as valid by the official government apps. [...]
