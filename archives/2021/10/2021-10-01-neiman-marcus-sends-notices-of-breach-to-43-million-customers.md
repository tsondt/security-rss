Title: Neiman Marcus sends notices of breach to 4.3 million customers
Date: 2021-10-01T11:49:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2021-10-01-neiman-marcus-sends-notices-of-breach-to-43-million-customers

[Source](https://www.bleepingcomputer.com/news/security/neiman-marcus-sends-notices-of-breach-to-43-million-customers/){:target="_blank" rel="noopener"}

> Neiman Marcus, the Texas-based luxury department stores chain, is sending notices of a data breach to roughly 4.3 million customers. [...]
