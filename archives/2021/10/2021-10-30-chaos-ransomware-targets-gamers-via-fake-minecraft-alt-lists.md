Title: Chaos ransomware targets gamers via fake Minecraft alt lists
Date: 2021-10-30T11:02:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2021-10-30-chaos-ransomware-targets-gamers-via-fake-minecraft-alt-lists

[Source](https://www.bleepingcomputer.com/news/security/chaos-ransomware-targets-gamers-via-fake-minecraft-alt-lists/){:target="_blank" rel="noopener"}

> The Chaos Ransomware gang encrypts gamers' Windows devices through fake Minecraft alt lists promoted on gaming forums. [...]
