Title: Twitch Gets Gutted: All Source Code Leaked
Date: 2021-10-06T15:26:17+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;Privacy;Web Security
Slug: 2021-10-06-twitch-gets-gutted-all-source-code-leaked

[Source](https://threatpost.com/twitch-source-code-leaked/175359/){:target="_blank" rel="noopener"}

> An anonymous user posted a link to a 125GB torrent to 4chan yesterday, containing all of Twitch's source code, comments going back to its inception and more. [...]
