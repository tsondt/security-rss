Title: UK competition watchdog unveils principles to make a kinder antivirus business
Date: 2021-10-19T15:45:55+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-10-19-uk-competition-watchdog-unveils-principles-to-make-a-kinder-antivirus-business

[Source](https://go.theregister.com/feed/www.theregister.com/2021/10/19/cma_antivirus/){:target="_blank" rel="noopener"}

> Treat customers fairly when it comes to auto-renewal. Or else The UK's Competition and Markets Authority (CMA) has unveiled compliance principles to curb locally some of the sharper auto-renewal practices of antivirus software firms.... [...]
