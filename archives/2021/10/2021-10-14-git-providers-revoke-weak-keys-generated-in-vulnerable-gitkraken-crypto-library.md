Title: Git providers revoke weak keys generated in vulnerable GitKraken crypto library
Date: 2021-10-14T12:32:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-14-git-providers-revoke-weak-keys-generated-in-vulnerable-gitkraken-crypto-library

[Source](https://portswigger.net/daily-swig/git-providers-revoke-weak-keys-generated-in-vulnerable-gitkraken-crypto-library){:target="_blank" rel="noopener"}

> Weak SSH keys have been revoked by vendors to protect their users [...]
