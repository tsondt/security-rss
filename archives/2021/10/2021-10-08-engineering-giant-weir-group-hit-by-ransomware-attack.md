Title: Engineering giant Weir Group hit by ransomware attack
Date: 2021-10-08T08:20:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-08-engineering-giant-weir-group-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/engineering-giant-weir-group-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Scottish multinational engineering firm Weir Group has disclosed by what it called an "attempted ransomware attack" that led to "significant temporary disruption" in the second half of September. [...]
