Title: New APT ChamelGang Targets Russian Energy, Aviation Orgs
Date: 2021-10-01T12:36:25+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;News;Vulnerabilities;Web Security
Slug: 2021-10-01-new-apt-chamelgang-targets-russian-energy-aviation-orgs

[Source](https://threatpost.com/apt-chamelgang-targets-russian-energy-aviation/175272/){:target="_blank" rel="noopener"}

> First appearing in March, the group has been leveraging ProxyShell against targets in 10 countries and employs a variety of malware to steal data from compromised networks. [...]
