Title: Hong Kong’s anti-doxxing law comes into force despite human rights criticism
Date: 2021-10-08T15:06:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-08-hong-kongs-anti-doxxing-law-comes-into-force-despite-human-rights-criticism

[Source](https://portswigger.net/daily-swig/hong-kongs-anti-doxxing-law-comes-into-force-despite-human-rights-criticism){:target="_blank" rel="noopener"}

> Violations could attract hefty fines and up to five years in prison [...]
