Title: Babuk ransomware decryptor released to recover files for free
Date: 2021-10-27T11:52:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-10-27-babuk-ransomware-decryptor-released-to-recover-files-for-free

[Source](https://www.bleepingcomputer.com/news/security/babuk-ransomware-decryptor-released-to-recover-files-for-free/){:target="_blank" rel="noopener"}

> Czech cybersecurity software firm Avast has created and released a decryption tool to help Babuk ransomware victims recover their files for free. [...]
