Title: SquirrelWaffle Loader Malspams, Packing Qakbot, Cobalt Strike
Date: 2021-10-26T22:25:05+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-10-26-squirrelwaffle-loader-malspams-packing-qakbot-cobalt-strike

[Source](https://threatpost.com/squirrelwaffle-loader-malspams-packing-qakbot-cobalt-strike/175775/){:target="_blank" rel="noopener"}

> Say hello to what could be the next big spam player: SquirrelWaffle, which is spreading with increasing frequency via spam campaigns and infecting systems with a new malware loader. [...]
