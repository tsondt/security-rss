Title: Ransom Disclosure Act: US bill mandates organizations to report ransomware payments
Date: 2021-10-11T14:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-10-11-ransom-disclosure-act-us-bill-mandates-organizations-to-report-ransomware-payments

[Source](https://portswigger.net/daily-swig/ransom-disclosure-act-us-bill-mandates-organizations-to-report-ransomware-payments){:target="_blank" rel="noopener"}

> Newly proposed law hopes to further understanding of cybercrime landscape [...]
