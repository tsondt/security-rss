Title: US govt sites showing porn, viagra ads share a common software vendor
Date: 2021-09-17T06:11:17-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-09-17-us-govt-sites-showing-porn-viagra-ads-share-a-common-software-vendor

[Source](https://www.bleepingcomputer.com/news/security/us-govt-sites-showing-porn-viagra-ads-share-a-common-software-vendor/){:target="_blank" rel="noopener"}

> Multiple U.S. government sites using.gov and.mil domains have been seen hosting porn and spam content, such as Viagra ads, in the last year. A security researcher noticed all of these sites share a common software vendor, Laserfiche. [...]
