Title: “FudCo” Spam Empire Tied to Pakistani Software Firm
Date: 2021-09-06T19:04:41+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Bilal Waddaich;Bilal Warraich;Burhan Ul Haq;Fud Co;Fudtools;Rameez Shahzad;Saim Raza;Sasha Angus;Scylla Intel;The Manipulaters;We Code Solutions;Wecodesolutions.pk
Slug: 2021-09-06-fudco-spam-empire-tied-to-pakistani-software-firm

[Source](https://krebsonsecurity.com/2021/09/fudco-spam-empire-tied-to-pakistani-software-firm/){:target="_blank" rel="noopener"}

> In May 2015, KrebsOnSecurity briefly profiled “ The Manipulaters,” the name chosen by a prolific cybercrime group based in Pakistan that was very publicly selling spam tools and a range of services for crafting, hosting and deploying malicious email. Six years later, a review of the social media postings from this group shows they are prospering, while rather poorly hiding their activities behind a software development firm in Lahore that has secretly enabled an entire generation of spammers and scammers. The Web site in 2015 for the “Manipulaters Team,” a group of Pakistani hackers behind the dark web identity “Saim [...]
