Title: ‘Nasty stuff’: Research into Russian push-button cellphones uncovers legion of security issues
Date: 2021-09-03T15:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-03-nasty-stuff-research-into-russian-push-button-cellphones-uncovers-legion-of-security-issues

[Source](https://portswigger.net/daily-swig/nasty-stuff-research-into-russian-push-button-cellphones-uncovers-legion-of-security-issues){:target="_blank" rel="noopener"}

> Itel, DEXP, Irbis, and F+ mobile devices put under the microscope [...]
