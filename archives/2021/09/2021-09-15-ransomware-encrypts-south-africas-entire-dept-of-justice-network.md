Title: Ransomware encrypts South Africa's entire Dept of Justice network
Date: 2021-09-15T15:35:17-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-15-ransomware-encrypts-south-africas-entire-dept-of-justice-network

[Source](https://www.bleepingcomputer.com/news/security/ransomware-encrypts-south-africas-entire-dept-of-justice-network/){:target="_blank" rel="noopener"}

> The justice ministry of the South African government is working on restoring its operations after a recent ransomware attack encrypted all its systems, making all electronic services unavailable both internally and to the public. [...]
