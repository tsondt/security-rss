Title: New macOS zero-day bug lets attackers run commands remotely
Date: 2021-09-21T16:01:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2021-09-21-new-macos-zero-day-bug-lets-attackers-run-commands-remotely

[Source](https://www.bleepingcomputer.com/news/apple/new-macos-zero-day-bug-lets-attackers-run-commands-remotely/){:target="_blank" rel="noopener"}

> Security researchers disclosed today a new vulnerability in Apple's macOS Finder, which makes it possible for attackers to run arbitrary commands on Macs running any macOS version up to the latest release, Big Sur. [...]
