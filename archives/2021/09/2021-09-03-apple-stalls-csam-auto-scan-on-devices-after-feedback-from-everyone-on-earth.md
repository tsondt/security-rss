Title: Apple stalls CSAM auto-scan on devices after 'feedback' from everyone on Earth
Date: 2021-09-03T20:48:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-03-apple-stalls-csam-auto-scan-on-devices-after-feedback-from-everyone-on-earth

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/03/apple_scanning_pause/){:target="_blank" rel="noopener"}

> Critics celebrate reconsideration of 'spyPhone' regime Apple on Friday said it intends to delay the introduction of its plan to commandeer customers' own devices to scan their iCloud-bound photos for illegal child exploitation imagery, a concession to the broad backlash that followed from the initiative.... [...]
