Title: Military’s RFID Tracking of Guns May Endanger Troops
Date: 2021-09-30T19:32:14+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Hacks;Web Security
Slug: 2021-09-30-militarys-rfid-tracking-of-guns-may-endanger-troops

[Source](https://threatpost.com/military-rfid-track-guns-endanger-troops/175260/){:target="_blank" rel="noopener"}

> RFID gun tags leave the military exposed to tracking, sniffing and spoofing attacks, experts say. [...]
