Title: Microsoft September 2021 Patch Tuesday fixes 2 zero-days, 60 flaws
Date: 2021-09-14T13:56:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-14-microsoft-september-2021-patch-tuesday-fixes-2-zero-days-60-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-september-2021-patch-tuesday-fixes-2-zero-days-60-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's September 2021 Patch Tuesday, and with it comes fixes for two zero-day vulnerabilities and a total of 61 flaws. [...]
