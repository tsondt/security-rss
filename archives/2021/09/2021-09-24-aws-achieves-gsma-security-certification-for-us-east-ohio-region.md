Title: AWS achieves GSMA security certification for US East (Ohio) Region
Date: 2021-09-24T17:17:06+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;GSM;Security Blog;Telecom
Slug: 2021-09-24-aws-achieves-gsma-security-certification-for-us-east-ohio-region

[Source](https://aws.amazon.com/blogs/security/aws-achieves-gsma-security-certification-for-us-east-ohio-region/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that our US East (Ohio) Region (us-east-2) is now certified by the GSM Association (GSMA) under its Security Accreditation Scheme Subscription Management (SAS-SM) with scope Data Center Operations and Management (DCOM). This alignment with GSMA [...]
