Title: 3 years, 17 alphas, 2 betas, and over 7,500 commits later, OpenSSL version 3 is here
Date: 2021-09-08T17:27:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-09-08-3-years-17-alphas-2-betas-and-over-7500-commits-later-openssl-version-3-is-here

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/08/openssl_3/){:target="_blank" rel="noopener"}

> What have we learned during that time? Quite a bit, it appears The OpenSSL team has released version 3.0 of its eponymous secure communications library after a lengthy gestation period.... [...]
