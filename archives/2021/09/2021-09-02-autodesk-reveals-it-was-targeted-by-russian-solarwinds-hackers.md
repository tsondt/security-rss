Title: Autodesk reveals it was targeted by Russian SolarWinds hackers
Date: 2021-09-02T07:30:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-02-autodesk-reveals-it-was-targeted-by-russian-solarwinds-hackers

[Source](https://www.bleepingcomputer.com/news/security/autodesk-reveals-it-was-targeted-by-russian-solarwinds-hackers/){:target="_blank" rel="noopener"}

> Autodesk has confirmed that it was also targeted by the Russian state hackers behind the large-scale SolarWinds Orion supply-chain attack, almost nine months after discovering that one of its servers was backdoored with Sunburst malware. [...]
