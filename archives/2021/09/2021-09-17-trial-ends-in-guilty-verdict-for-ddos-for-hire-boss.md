Title: Trial Ends in Guilty Verdict for DDoS-for-Hire Boss
Date: 2021-09-17T01:22:31+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: DDoS-for-Hire;Ne'er-Do-Well News;ampnode;booter;ddos-for-hire;downthem;Juan Martinez;Matthew Gatrel
Slug: 2021-09-17-trial-ends-in-guilty-verdict-for-ddos-for-hire-boss

[Source](https://krebsonsecurity.com/2021/09/trial-ends-in-guilty-verdict-for-ddos-for-hire-boss/){:target="_blank" rel="noopener"}

> A jury in California today reached a guilty verdict in the trial of Matthew Gatrel, a St. Charles, Ill. man charged in 2018 with operating two online services that allowed paying customers to launch powerful distributed denial-of-service (DDoS) attacks against Internet users and websites. Gatrel’s conviction comes roughly two weeks after his co-conspirator pleaded guilty to criminal charges related to running the services. The user interface for Downthem[.]org. Prosecutors for the Central District of California charged Gatrel, 32, and his business partner Juan “Severon” Martinez of Pasadena, Calif. with operating two DDoS-for-hire or “booter” services — downthem[.]org and ampnode[.]com. Despite [...]
