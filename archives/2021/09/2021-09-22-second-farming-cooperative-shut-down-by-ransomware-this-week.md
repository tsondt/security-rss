Title: Second farming cooperative shut down by ransomware this week
Date: 2021-09-22T12:09:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-22-second-farming-cooperative-shut-down-by-ransomware-this-week

[Source](https://www.bleepingcomputer.com/news/security/second-farming-cooperative-shut-down-by-ransomware-this-week/){:target="_blank" rel="noopener"}

> Minnesota farming supply cooperative Crystal Valley has suffered a ransomware attack, making it the second farming cooperative attacked this weekend. [...]
