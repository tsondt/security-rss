Title: Anonymous: We've leaked disk images stolen from far-right-friendly web host Epik
Date: 2021-09-30T02:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-30-anonymous-weve-leaked-disk-images-stolen-from-far-right-friendly-web-host-epik

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/30/anonymous_second_epik_dump/){:target="_blank" rel="noopener"}

> Latest data dump also apparently contains 'a wide range of passwords and API tokens' Entities using the name and iconography of Anonymous (EUTNAIOA) claim to have leaked server disk images extracted from Epik – the controversial US outfit that has provided services to far-right orgs such as the Oath Keepers and Gab, provided a home to social-network-for-internet-outcasts Parler, and hosted hate-hole 8chan.... [...]
