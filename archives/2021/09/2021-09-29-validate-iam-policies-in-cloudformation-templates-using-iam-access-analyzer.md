Title: Validate IAM policies in CloudFormation templates using IAM Access Analyzer
Date: 2021-09-29T19:32:52+00:00
Author: Matt Luttrell
Category: AWS Security
Tags: Advanced (300);AWS CloudFormation;AWS IAM Access Analyzer;AWS Identity and Access Management (IAM);Security, Identity, & Compliance;AWS CodePipeline;AWS IAM;Devops;Security Blog
Slug: 2021-09-29-validate-iam-policies-in-cloudformation-templates-using-iam-access-analyzer

[Source](https://aws.amazon.com/blogs/security/validate-iam-policies-in-cloudformation-templates-using-iam-access-analyzer/){:target="_blank" rel="noopener"}

> In this blog post, I introduce IAM Policy Validator for AWS CloudFormation (cfn-policy-validator), an open source tool that extracts AWS Identity and Access Management (IAM) policies from an AWS CloudFormation template, and allows you to run existing IAM Access Analyzer policy validation APIs against the template. I also show you how to run the tool [...]
