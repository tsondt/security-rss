Title: Indonesian President's COVID jab cert leaks – authorities argue that's perfectly reasonable
Date: 2021-09-06T01:53:44+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-06-indonesian-presidents-covid-jab-cert-leaks-authorities-argue-thats-perfectly-reasonable

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/06/joko_widodo_vaccination_certificate_leak/){:target="_blank" rel="noopener"}

> It's not as if politicians' birthdays aren't well known, and they get jabbed on live TV Indonesian authorities have admitted that the COVID-19 vaccination certificate of the nation's President has circulated online and tried to explain that it's an indication of admirable transparency, rather than lamentable security.... [...]
