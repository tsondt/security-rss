Title: How US federal agencies can authenticate to AWS with multi-factor authentication
Date: 2021-09-02T20:07:15+00:00
Author: Kyle Hart
Category: AWS Security
Tags: Federal;Government;Intermediate (200);Public Sector;Security, Identity, & Compliance;Cybersecurity EO;Executive order;MFA;Personal identity verification;Security Blog
Slug: 2021-09-02-how-us-federal-agencies-can-authenticate-to-aws-with-multi-factor-authentication

[Source](https://aws.amazon.com/blogs/security/how-us-federal-agencies-can-authenticate-to-aws-with-multi-factor-authentication/){:target="_blank" rel="noopener"}

> This post is part of a series about how AWS can help your US federal agency meet the requirements of the President’s Executive Order on Improving the Nation’s Cybersecurity. We recognize that government agencies have varying degrees of identity management and cloud maturity and that the requirement to implement multi-factor, risk-based authentication across an entire [...]
