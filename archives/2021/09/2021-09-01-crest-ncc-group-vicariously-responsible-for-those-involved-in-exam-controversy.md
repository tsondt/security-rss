Title: CREST: NCC Group ‘vicariously responsible’ for those involved in exam controversy
Date: 2021-09-01T15:23:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-01-crest-ncc-group-vicariously-responsible-for-those-involved-in-exam-controversy

[Source](https://portswigger.net/daily-swig/crest-ncc-group-vicariously-responsible-for-those-involved-in-exam-controversy){:target="_blank" rel="noopener"}

> Security consulting firm insists no student gained an unfair advantage [...]
