Title: Speer review: Researchers pick apart Node.js communication app
Date: 2021-09-14T15:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-14-speer-review-researchers-pick-apart-nodejs-communication-app

[Source](https://portswigger.net/daily-swig/speer-review-researchers-pick-apart-node-js-communication-app){:target="_blank" rel="noopener"}

> Email content injection flaws chained to bypass security controls [...]
