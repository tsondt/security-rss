Title: SAS 2021: FinSpy Surveillance Kit Re-Emerges Stronger Than Ever
Date: 2021-09-28T17:45:59+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Privacy;Security Analyst Summit;Web Security
Slug: 2021-09-28-sas-2021-finspy-surveillance-kit-re-emerges-stronger-than-ever

[Source](https://threatpost.com/finspy-surveillance-kit/175068/){:target="_blank" rel="noopener"}

> A 'nearly impossible to analyze' version of the malware sports a bootkit and 'steal-everything' capabilities. [...]
