Title: REvil ransomware's servers mysteriously come back online
Date: 2021-09-07T14:19:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-07-revil-ransomwares-servers-mysteriously-come-back-online

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomwares-servers-mysteriously-come-back-online/){:target="_blank" rel="noopener"}

> The dark web servers for the REvil ransomware operation have suddenly turned back on after an almost two-month absence. It is unclear if this marks their ransomware gang's return or the servers being turned on by law enforcement. [...]
