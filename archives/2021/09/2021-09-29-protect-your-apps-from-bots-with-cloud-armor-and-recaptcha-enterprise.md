Title: Protect your apps from bots with Cloud Armor and reCAPTCHA Enterprise
Date: 2021-09-29T16:00:00+00:00
Author: Badr Salmi
Category: GCP Security
Tags: Networking;Identity & Security
Slug: 2021-09-29-protect-your-apps-from-bots-with-cloud-armor-and-recaptcha-enterprise

[Source](https://cloud.google.com/blog/products/identity-security/bot-management-with-google-cloud/){:target="_blank" rel="noopener"}

> Unwelcome web traffic from bots has proliferated, becoming a significant contributor to business and operational risk. The motivations of bot controllers range from disruption of business through DDoS attacks to fraud such as credential stuffing, denial of inventory, scraping, and fraudulent card use. Google is well positioned to help detect and mitigate these risks by leveraging both our AI/ML strengths for combatting bot traffic as well as the global scale of our network to absorb even the largest threats. Today we are announcing the public preview of Cloud Armor bot management with reCAPTCHA Enterprise.This new set of capabilities is centered [...]
