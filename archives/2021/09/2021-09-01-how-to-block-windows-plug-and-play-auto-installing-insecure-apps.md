Title: How to block Windows Plug-and-Play auto-installing insecure apps
Date: 2021-09-01T15:29:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-01-how-to-block-windows-plug-and-play-auto-installing-insecure-apps

[Source](https://www.bleepingcomputer.com/news/microsoft/how-to-block-windows-plug-and-play-auto-installing-insecure-apps/){:target="_blank" rel="noopener"}

> A trick has been discovered that prevents your device from being taken over by vulnerable Windows applications when devices are plugged into your computer. [...]
