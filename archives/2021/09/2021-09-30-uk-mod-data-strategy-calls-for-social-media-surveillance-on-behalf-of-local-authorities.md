Title: UK MoD data strategy calls for social media surveillance on behalf of 'local authorities'
Date: 2021-09-30T10:15:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-30-uk-mod-data-strategy-calls-for-social-media-surveillance-on-behalf-of-local-authorities

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/30/mod_data_strategy_social_media_surveillance/){:target="_blank" rel="noopener"}

> From a document supposedly about better use of existing silos. Eh? The Ministry of Defence has published a data strategy that calls on the British armed forces to make better use of its "enduring strategic asset" – by spying on social media and dobbing in dissenters to local councils.... [...]
