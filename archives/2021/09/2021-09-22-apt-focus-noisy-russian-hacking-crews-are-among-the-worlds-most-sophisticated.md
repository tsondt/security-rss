Title: APT focus: ‘Noisy’ Russian hacking crews are among the world’s most sophisticated
Date: 2021-09-22T13:24:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-22-apt-focus-noisy-russian-hacking-crews-are-among-the-worlds-most-sophisticated

[Source](https://portswigger.net/daily-swig/apt-focus-noisy-russian-hacking-crews-are-among-the-worlds-most-sophisticated){:target="_blank" rel="noopener"}

> Unpacking the Matryoshka dolls behind Kremlin-backed cybercrime campaigns [...]
