Title: Updated data processing terms to reflect new EU Standard Contractual Clauses
Date: 2021-09-24T13:30:00+00:00
Author: Nathaly Rey
Category: GCP Security
Tags: Identity & Security;Google Cloud;Compliance
Slug: 2021-09-24-updated-data-processing-terms-to-reflect-new-eu-standard-contractual-clauses

[Source](https://cloud.google.com/blog/products/compliance/google-cloud-approach-to-implementing-eu-sccs/){:target="_blank" rel="noopener"}

> For years, Google Cloud customers who are subject to European data protection laws 1 have relied on our Standard Contractual Clauses (SCCs), as previously approved by regulators, to legitimize overseas transfers of their customer personal data when using our services. Today, we are glad to announce an update to our data processing terms for Google Cloud Platform, and Google Workspace (including Workspace for Education) and Cloud Identity, to incorporate various modules of the new EU SCCs approved by the European Commission on June 4, 2021, as well as separate UK SCCs. For all Google Cloud customers, this new approach: Offers [...]
