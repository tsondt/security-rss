Title: Security Risks of Relying on a Single Smartphone
Date: 2021-09-08T11:02:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;SIM cards;smartphones;WhatsApp
Slug: 2021-09-08-security-risks-of-relying-on-a-single-smartphone

[Source](https://www.schneier.com/blog/archives/2021/09/security-risks-of-relying-on-a-single-smartphone.html){:target="_blank" rel="noopener"}

> Isracard used a single cell phone to communicate with credit card clients, and receive documents via WhatsApp. An employee stole the phone. He reformatted the SIM, which was oddly the best possible outcome, given the circumstances. Using the data to steal money would have been much worse. Here’s a link to an archived version. [...]
