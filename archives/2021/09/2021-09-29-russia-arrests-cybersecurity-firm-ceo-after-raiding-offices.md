Title: Russia arrests cybersecurity firm CEO after raiding offices
Date: 2021-09-29T15:18:01-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-29-russia-arrests-cybersecurity-firm-ceo-after-raiding-offices

[Source](https://www.bleepingcomputer.com/news/security/russia-arrests-cybersecurity-firm-ceo-after-raiding-offices/){:target="_blank" rel="noopener"}

> Russian law enforcement on Tuesday has arrested Ilya Sachkov, the co-founder and CEO of cybersecurity company Group-IB, on suspicion of high treason resulting from sharing data with foreign intelligence.. [...]
