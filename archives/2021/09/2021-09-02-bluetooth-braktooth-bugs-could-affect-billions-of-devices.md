Title: Bluetooth BrakTooth bugs could affect billions of devices
Date: 2021-09-02T19:22:51-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security;Technology
Slug: 2021-09-02-bluetooth-braktooth-bugs-could-affect-billions-of-devices

[Source](https://www.bleepingcomputer.com/news/security/bluetooth-braktooth-bugs-could-affect-billions-of-devices/){:target="_blank" rel="noopener"}

> Vulnerabilities collectively referred to as BrakTooth are affecting Bluetooth stacks implemented on system-on-a-chip (SoC) circuits from over a dozen vendors. [...]
