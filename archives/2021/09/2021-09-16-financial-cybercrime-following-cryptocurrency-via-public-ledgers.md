Title: Financial Cybercrime: Following Cryptocurrency via Public Ledgers
Date: 2021-09-16T13:08:24+00:00
Author: John Hammond
Category: Threatpost
Tags: Cryptography;InfoSec Insider;Malware
Slug: 2021-09-16-financial-cybercrime-following-cryptocurrency-via-public-ledgers

[Source](https://threatpost.com/financial-cybercrime-cryptocurrency-public-ledgers/169987/){:target="_blank" rel="noopener"}

> John Hammond, security researcher with Huntress, discusses a wallet-hijacking RAT, and how law enforcement recovered millions in Bitcoin after the Colonial Pipeline attack. [...]
