Title: New Mēris botnet breaks DDoS record with 21.8 million RPS attack
Date: 2021-09-09T09:25:10-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-09-new-mēris-botnet-breaks-ddos-record-with-218-million-rps-attack

[Source](https://www.bleepingcomputer.com/news/security/new-m-ris-botnet-breaks-ddos-record-with-218-million-rps-attack/){:target="_blank" rel="noopener"}

> A new distributed denial-of-service (DDoS) botnet that kept growing over the summer has been hammering Russian internet giant Yandex for the past month, the attack peaking at the unprecedented rate of 21.8 million requests per second. [...]
