Title: QNAP fixes bug that let attackers run malicious commands remotely
Date: 2021-09-30T16:56:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-30-qnap-fixes-bug-that-let-attackers-run-malicious-commands-remotely

[Source](https://www.bleepingcomputer.com/news/security/qnap-fixes-bug-that-let-attackers-run-malicious-commands-remotely/){:target="_blank" rel="noopener"}

> Taiwan-based network-attached storage (NAS) maker QNAP has released security patches for multiple vulnerabilities that could allow attackers to inject and execute malicious code and commands remotely on vulnerable NAS devices. [...]
