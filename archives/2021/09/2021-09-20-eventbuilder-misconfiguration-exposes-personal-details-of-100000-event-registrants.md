Title: EventBuilder misconfiguration exposes personal details of 100,000 event registrants
Date: 2021-09-20T13:08:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-20-eventbuilder-misconfiguration-exposes-personal-details-of-100000-event-registrants

[Source](https://portswigger.net/daily-swig/eventbuilder-misconfiguration-exposes-personal-details-of-100-000-event-registrants){:target="_blank" rel="noopener"}

> Vulnerability has now been addressed in the Microsoft Teams add-on [...]
