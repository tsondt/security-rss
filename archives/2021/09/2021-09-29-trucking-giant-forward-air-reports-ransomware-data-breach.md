Title: Trucking giant Forward Air reports ransomware data breach
Date: 2021-09-29T13:47:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-29-trucking-giant-forward-air-reports-ransomware-data-breach

[Source](https://www.bleepingcomputer.com/news/security/trucking-giant-forward-air-reports-ransomware-data-breach/){:target="_blank" rel="noopener"}

> Trucking giant Forward Air has disclosed a data breach after a ransomware attack that allowed threat actors to access employees' personal information. [...]
