Title: Apple AirTag Zero-Day Weaponizes Trackers
Date: 2021-09-29T20:48:33+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Malware;Vulnerabilities;Web Security
Slug: 2021-09-29-apple-airtag-zero-day-weaponizes-trackers

[Source](https://threatpost.com/apple-airtag-zero-day-trackers/175143/){:target="_blank" rel="noopener"}

> Apple's personal item-tracker devices can be used to deliver malware, slurp credentials, steal tokens and more thanks to XSS. [...]
