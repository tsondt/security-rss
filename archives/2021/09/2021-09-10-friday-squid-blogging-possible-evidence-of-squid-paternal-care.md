Title: Friday Squid Blogging: Possible Evidence of Squid Paternal Care
Date: 2021-09-10T21:13:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2021-09-10-friday-squid-blogging-possible-evidence-of-squid-paternal-care

[Source](https://www.schneier.com/blog/archives/2021/09/friday-squid-blogging-possible-evidence-of-squid-paternal-care.html){:target="_blank" rel="noopener"}

> Researchers have found possible evidence of paternal care among bigfin reef squid. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
