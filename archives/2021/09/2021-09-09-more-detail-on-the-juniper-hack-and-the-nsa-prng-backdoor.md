Title: More Detail on the Juniper Hack and the NSA PRNG Backdoor
Date: 2021-09-09T11:13:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;China;firewall;hacking;Juniper;NSA;random numbers
Slug: 2021-09-09-more-detail-on-the-juniper-hack-and-the-nsa-prng-backdoor

[Source](https://www.schneier.com/blog/archives/2021/09/more-detail-on-the-juniper-hack-and-the-nsa-prng-backdoor.html){:target="_blank" rel="noopener"}

> We knew the basics of this story, but it’s good to have more detail. Here’s me in 2015 about this Juniper hack. Here’s me in 2007 on the NSA backdoor. [...]
