Title: WhatsApp Photo Filter Bug Allows Sensitive Info to Be Lifted
Date: 2021-09-02T12:28:13+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: 2021-09-02-whatsapp-photo-filter-bug-allows-sensitive-info-to-be-lifted

[Source](https://threatpost.com/whatsapp-photo-filter-bug-allows-sensitive-info-to-be-lifted/169141/){:target="_blank" rel="noopener"}

> Users should be careful whose pics they view and should, of course, update their apps. [...]
