Title: Indictment, Lawsuits Revive Trump-Alfa Bank Story
Date: 2021-09-23T13:53:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;66.216.133.29;Alfa Bank;B.G.R. Group;Daniel J. Jones;DNS;fbi;Heartland Payment Systems;Indiana University School of Informatics and Computing;James A. Baker;John H. Durham;L. Jean Camp;Mandiant;Michael Sussmann;Nicholas Weaver;Paul Vixie;Spectrum Health;Stroz Friedberg;The Democracy Integrity Project;Trump Organization;trump1.contact-client.com;University of California Berkeley
Slug: 2021-09-23-indictment-lawsuits-revive-trump-alfa-bank-story

[Source](https://krebsonsecurity.com/2021/09/lawsuits-indictments-revive-trump-alfa-bank-story/){:target="_blank" rel="noopener"}

> In October 2016, media outlets reported that data collected by some of the world’s most renowned cybersecurity experts had identified frequent and unexplained communications between an email server used by the Trump Organization and Alfa Bank, one of Russia’s largest financial institutions. Those publications set off speculation about a possible secret back-channel of communications, as well as a series of lawsuits and investigations that culminated last week with the indictment of the same former federal cybercrime prosecutor who brought the data to the attention of the FBI five years ago. The first page of Alfa Bank’s 2020 complaint. Since 2018, [...]
