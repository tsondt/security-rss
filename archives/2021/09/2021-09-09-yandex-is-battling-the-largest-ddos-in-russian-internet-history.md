Title: Yandex is battling the largest DDoS in Russian Internet history
Date: 2021-09-09T02:26:11-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-09-yandex-is-battling-the-largest-ddos-in-russian-internet-history

[Source](https://www.bleepingcomputer.com/news/security/yandex-is-battling-the-largest-ddos-in-russian-internet-history/){:target="_blank" rel="noopener"}

> Russian internet giant Yandex has been targeted in a massive distributed denial-of-service (DDoS) attack that started last week and reportedly continues this week. [...]
