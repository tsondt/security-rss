Title: McDonald’s Email Blast Includes Password to Monopoly Game Database
Date: 2021-09-09T20:38:41+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2021-09-09-mcdonalds-email-blast-includes-password-to-monopoly-game-database

[Source](https://threatpost.com/mcdonalds-email-blast-includes-password-to-monopoly-game-database/169346/){:target="_blank" rel="noopener"}

> Usernames, passwords for database sent in prize redemption emails. [...]
