Title: Security bods boost Apple iPhone hardware attack research with iTimed toolkit
Date: 2021-09-14T16:45:10+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: 2021-09-14-security-bods-boost-apple-iphone-hardware-attack-research-with-itimed-toolkit

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/14/security_bods_boost_apple_iphone/){:target="_blank" rel="noopener"}

> 'The first complete infrastructure to enable general-purpose hardware security experiments on the Apple iPhone SoCs,' they claim A trio of researchers at North Carolina State University (NC State) have released what they describe as a "novel research toolkit" for Apple's iDevices - and to prove its functionality, have disclosed side-channel attacks against the company's A10 Fusion system-on-chip.... [...]
