Title: Hacker-Themed Board Game
Date: 2021-09-03T19:21:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;games;hacking
Slug: 2021-09-03-hacker-themed-board-game

[Source](https://www.schneier.com/blog/archives/2021/09/hacker-themed-board-game.html){:target="_blank" rel="noopener"}

> Black Hat is a hacker-themed board game. [...]
