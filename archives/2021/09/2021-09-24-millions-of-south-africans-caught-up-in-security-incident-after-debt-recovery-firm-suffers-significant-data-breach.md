Title: Millions of South Africans caught up in security incident after debt recovery firm suffers ‘significant data breach’
Date: 2021-09-24T11:29:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-24-millions-of-south-africans-caught-up-in-security-incident-after-debt-recovery-firm-suffers-significant-data-breach

[Source](https://portswigger.net/daily-swig/millions-of-south-africans-caught-up-in-security-incident-after-debt-recovery-firm-suffers-significant-data-breach){:target="_blank" rel="noopener"}

> Sensitive information is among datasets potentially exposed [...]
