Title: ProtonMail deletes 'we don't log your IP' boast from website after French climate activist reportedly arrested
Date: 2021-09-07T11:31:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-07-protonmail-deletes-we-dont-log-your-ip-boast-from-website-after-french-climate-activist-reportedly-arrested

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/07/protonmail_hands_user_ip_address_police/){:target="_blank" rel="noopener"}

> Cops can read the SMTP spec too, y'know Encrypted email service ProtonMail has become embroiled in a minor scandal after responding to a legal request to hand over a user's IP address and details of the devices he used to access his mailbox to Swiss police – resulting in the user's arrest.... [...]
