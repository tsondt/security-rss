Title: French government visa website hit by cyber-attack that exposed applicants’ personal data
Date: 2021-09-06T15:22:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-06-french-government-visa-website-hit-by-cyber-attack-that-exposed-applicants-personal-data

[Source](https://portswigger.net/daily-swig/french-government-visa-website-hit-by-cyber-attack-that-exposed-applicants-personal-data){:target="_blank" rel="noopener"}

> Nationalities, birth dates, and passport numbers among potentially exposed data [...]
