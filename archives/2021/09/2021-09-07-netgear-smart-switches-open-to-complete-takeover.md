Title: Netgear Smart Switches Open to Complete Takeover
Date: 2021-09-07T20:39:19+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-09-07-netgear-smart-switches-open-to-complete-takeover

[Source](https://threatpost.com/netgear-smart-switches-takeover/169259/){:target="_blank" rel="noopener"}

> The Demon's Cries, Draconian Fear and Seventh Inferno security bugs are high-severity entryways to corporate networks. [...]
