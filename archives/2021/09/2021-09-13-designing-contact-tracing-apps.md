Title: Designing Contact-Tracing Apps
Date: 2021-09-13T11:41:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;COVID-19;geolocation;medicine;privacy;smartphones
Slug: 2021-09-13-designing-contact-tracing-apps

[Source](https://www.schneier.com/blog/archives/2021/09/designing-contact-tracing-apps.html){:target="_blank" rel="noopener"}

> Susan Landau wrote an essay on the privacy, efficacy, and equity of contract-tracing smartphone apps. Also see her excellent book on the topic. [...]
