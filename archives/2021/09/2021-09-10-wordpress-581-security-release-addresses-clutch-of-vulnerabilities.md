Title: WordPress 5.8.1 security release addresses clutch of vulnerabilities
Date: 2021-09-10T13:23:44+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-10-wordpress-581-security-release-addresses-clutch-of-vulnerabilities

[Source](https://portswigger.net/daily-swig/wordpress-5-8-1-security-release-addresses-clutch-of-vulnerabilities){:target="_blank" rel="noopener"}

> Block editor XSS and REST API data exposure issues among now-patched bugs [...]
