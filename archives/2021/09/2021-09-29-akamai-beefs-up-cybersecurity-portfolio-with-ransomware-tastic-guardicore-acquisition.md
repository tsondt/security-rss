Title: Akamai beefs up cybersecurity portfolio with ransomware-tastic Guardicore acquisition
Date: 2021-09-29T17:02:04+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-09-29-akamai-beefs-up-cybersecurity-portfolio-with-ransomware-tastic-guardicore-acquisition

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/29/akamai_guardicore_acquisition/){:target="_blank" rel="noopener"}

> $600m buys a lot of microsegmentation Content delivery network Akamai is set to crack open the piggy bank with the purchase of Israel-based Guardicore.... [...]
