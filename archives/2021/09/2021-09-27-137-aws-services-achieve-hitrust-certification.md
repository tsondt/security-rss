Title: 137 AWS services achieve HITRUST certification
Date: 2021-09-27T17:46:13+00:00
Author: Sonali Vaidya
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;HITRUST CSF certification;Security Blog
Slug: 2021-09-27-137-aws-services-achieve-hitrust-certification

[Source](https://aws.amazon.com/blogs/security/137-aws-services-achieve-hitrust-certification/){:target="_blank" rel="noopener"}

> We’re excited to announce that 137 Amazon Web Services (AWS) services are certified for the Health Information Trust Alliance (HITRUST) Common Security Framework (CSF) for the 2021 cycle. The full list of AWS services that were audited by a third-party auditor and certified under HITRUST CSF is available on our Services in Scope by Compliance [...]
