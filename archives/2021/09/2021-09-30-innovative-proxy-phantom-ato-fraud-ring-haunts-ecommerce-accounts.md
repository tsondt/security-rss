Title: Innovative Proxy Phantom ATO Fraud Ring Haunts eCommerce Accounts
Date: 2021-09-30T17:05:09+00:00
Author: Tara Seals
Category: Threatpost
Tags: Most Recent ThreatLists;Web Security
Slug: 2021-09-30-innovative-proxy-phantom-ato-fraud-ring-haunts-ecommerce-accounts

[Source](https://threatpost.com/proxy-phantom-fraud-ecommerce-accounts/175241/){:target="_blank" rel="noopener"}

> The group uses millions of password combos at the rate of nearly 2,700 login attempts per minute with new techniques that push the ATO envelope. [...]
