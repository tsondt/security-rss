Title: MyRepublic discloses data breach exposing government ID cards
Date: 2021-09-10T14:47:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2021-09-10-myrepublic-discloses-data-breach-exposing-government-id-cards

[Source](https://www.bleepingcomputer.com/news/security/myrepublic-discloses-data-breach-exposing-government-id-cards/){:target="_blank" rel="noopener"}

> MyRepublic Singapore has disclosed a data breach exposing the personal information of approximately 80,000 mobile subscribers. [...]
