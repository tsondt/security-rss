Title: How a glitch in the Matrix led to apps potentially exposing encrypted chats
Date: 2021-09-13T20:22:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-13-how-a-glitch-in-the-matrix-led-to-apps-potentially-exposing-encrypted-chats

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/13/matrix_foundation_implementation_bug/){:target="_blank" rel="noopener"}

> Forget Agent Smith, we want to see Neo fighting implementation bugs The Matrix.org Foundation, which oversees the Matrix decentralized communication protocol, said on Monday multiple Matrix clients and libraries contain a vulnerability that can potentially be abused to expose encrypted messages.... [...]
