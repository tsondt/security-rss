Title: Unpatched Bugs Plague Databases; Your Data Is Probably Not Secure – Podcast
Date: 2021-09-14T13:45:31+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Podcasts;Vulnerabilities;Web Security
Slug: 2021-09-14-unpatched-bugs-plague-databases-your-data-is-probably-not-secure-podcast

[Source](https://threatpost.com/unpatched-databases-data-not-secure-podcast/169428/){:target="_blank" rel="noopener"}

> Imperva's Elad Erez discusses findings that 46 percent of on-prem databases are sitting ducks, unpatched and vulnerable to attack, each with an average of 26 flaws. [...]
