Title: US policy change states healthcare apps must follow data breach notification rules
Date: 2021-09-20T16:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-20-us-policy-change-states-healthcare-apps-must-follow-data-breach-notification-rules

[Source](https://portswigger.net/daily-swig/us-policy-change-states-healthcare-apps-must-follow-data-breach-notification-rules){:target="_blank" rel="noopener"}

> Connected devices such as fitness trackers also obliged to follow tougher privacy rules [...]
