Title: Microsoft rolls out passwordless login for all Microsoft accounts
Date: 2021-09-15T10:49:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-15-microsoft-rolls-out-passwordless-login-for-all-microsoft-accounts

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-rolls-out-passwordless-login-for-all-microsoft-accounts/){:target="_blank" rel="noopener"}

> Microsoft is rolling out passwordless login support over the coming weeks, allowing customers to sign in to Microsoft accounts without using a password. [...]
