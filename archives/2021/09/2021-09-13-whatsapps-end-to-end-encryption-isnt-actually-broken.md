Title: WhatsApp’s End-to-End Encryption Isn’t Actually Broken
Date: 2021-09-13T18:41:05+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cryptography;Mobile Security
Slug: 2021-09-13-whatsapps-end-to-end-encryption-isnt-actually-broken

[Source](https://threatpost.com/whatsapp-end-encryption-broken/169399/){:target="_blank" rel="noopener"}

> WhatsApp’s moderators sent messages flagged by intended recipients. Researchers say this isn't concerning -- yet. [...]
