Title: Fired credit union employee admits: I wiped 21GB of files from company's shared drive in retaliation
Date: 2021-09-01T23:34:42+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-01-fired-credit-union-employee-admits-i-wiped-21gb-of-files-from-companys-shared-drive-in-retaliation

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/01/credit_union_delete/){:target="_blank" rel="noopener"}

> Access should have been revoked... but wasn't, court told On Tuesday, a woman from Brooklyn, New York, pleaded guilty to destroying computer data at an unidentified credit union from which she had recently been fired.... [...]
