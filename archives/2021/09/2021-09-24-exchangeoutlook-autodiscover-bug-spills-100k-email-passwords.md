Title: Exchange/Outlook Autodiscover Bug Spills $100K+ Email Passwords
Date: 2021-09-24T18:46:59+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-09-24-exchangeoutlook-autodiscover-bug-spills-100k-email-passwords

[Source](https://threatpost.com/exchange-outlook-autodiscover-bug-spills-100k-email-passwords/175004/){:target="_blank" rel="noopener"}

> Hundreds of thousands of email credentials, many of which double as Active Directory domain credentials, came through to credential-trapping domains in clear text. [...]
