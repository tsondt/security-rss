Title: AWS achieves FedRAMP P-ATO for 18 additional services in the AWS US East/West and AWS GovCloud (US) Regions
Date: 2021-09-27T20:02:05+00:00
Author: Alexis Robinson
Category: AWS Security
Tags: Announcements;AWS GovCloud (US);Federal;Foundational (100);Government;Public Sector;Security, Identity, & Compliance;AWS (US) GovCloud;AWS East/West;FedRAMP;Security Blog
Slug: 2021-09-27-aws-achieves-fedramp-p-ato-for-18-additional-services-in-the-aws-us-eastwest-and-aws-govcloud-us-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-fedramp-p-ato-for-18-additional-services-in-the-aws-us-east-west-and-aws-govcloud-us-regions/){:target="_blank" rel="noopener"}

> We’re pleased to announce that 18 additional AWS services have achieved Provisional Authority to Operate (P-ATO) by the Federal Risk and Authorization Management Program (FedRAMP) Joint Authorization Board (JAB). The following are the 18 additional services with FedRAMP authorization for the US federal government, and organizations with regulated workloads: Amazon Cognito lets you add user [...]
