Title: Turla APT Plants Novel Backdoor In Wake of Afghan Unrest
Date: 2021-09-21T16:02:35+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Web Security
Slug: 2021-09-21-turla-apt-plants-novel-backdoor-in-wake-of-afghan-unrest

[Source](https://threatpost.com/turla-apt-backdoor-afghanistan/174858/){:target="_blank" rel="noopener"}

> “TinyTurla,” simply coded malware that hides away as a legitimate Windows service, has flown under the radar for two years. [...]
