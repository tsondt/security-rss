Title: OWASP shakes up web app threat categories with release of draft Top 10
Date: 2021-09-09T16:47:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-09-owasp-shakes-up-web-app-threat-categories-with-release-of-draft-top-10

[Source](https://portswigger.net/daily-swig/owasp-shakes-up-web-app-threat-categories-with-release-of-draft-top-10){:target="_blank" rel="noopener"}

> The Top 10 list is an acclaimed guide to modern web application security threats [...]
