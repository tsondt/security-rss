Title: Apple patches new zero-day bug used to hack iPhones and Macs
Date: 2021-09-23T14:23:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2021-09-23-apple-patches-new-zero-day-bug-used-to-hack-iphones-and-macs

[Source](https://www.bleepingcomputer.com/news/apple/apple-patches-new-zero-day-bug-used-to-hack-iphones-and-macs/){:target="_blank" rel="noopener"}

> Apple has released security updates to fix a zero-day vulnerability exploited in the wild by attackers to hack into iPhones and Macs running older iOS and macOS versions. [...]
