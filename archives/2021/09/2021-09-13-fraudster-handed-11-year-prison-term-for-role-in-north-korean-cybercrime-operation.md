Title: Fraudster handed 11-year prison term for role in North Korean cybercrime operation
Date: 2021-09-13T14:11:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-13-fraudster-handed-11-year-prison-term-for-role-in-north-korean-cybercrime-operation

[Source](https://portswigger.net/daily-swig/fraudster-handed-11-year-prison-term-for-role-in-north-korean-cybercrime-operation){:target="_blank" rel="noopener"}

> Defendant ordered to pay $30m in restitution to victims [...]
