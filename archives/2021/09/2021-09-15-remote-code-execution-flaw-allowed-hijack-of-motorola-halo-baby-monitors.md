Title: Remote code execution flaw allowed hijack of Motorola Halo+ baby monitors
Date: 2021-09-15T15:53:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-15-remote-code-execution-flaw-allowed-hijack-of-motorola-halo-baby-monitors

[Source](https://portswigger.net/daily-swig/remote-code-execution-flaw-allowed-hijack-of-motorola-halo-baby-monitors){:target="_blank" rel="noopener"}

> Expectant parent finds severe security problems in his new baby monitor [...]
