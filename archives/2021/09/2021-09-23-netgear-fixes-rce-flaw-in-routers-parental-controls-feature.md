Title: Netgear fixes RCE flaw in routers’ parental controls feature
Date: 2021-09-23T12:47:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-23-netgear-fixes-rce-flaw-in-routers-parental-controls-feature

[Source](https://portswigger.net/daily-swig/netgear-fixes-rce-flaw-in-routers-parental-controls-feature){:target="_blank" rel="noopener"}

> Bug in third-party code offers salutary lessons around enterprise risk management, say researchers [...]
