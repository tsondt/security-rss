Title: Romance, BEC Scams Lands Soldier in Jail for 46 Months
Date: 2021-09-14T13:10:49+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2021-09-14-romance-bec-scams-lands-soldier-in-jail-for-46-months

[Source](https://threatpost.com/romance-bec-scams-soldier-jail/169434/){:target="_blank" rel="noopener"}

> A former Army Reservist pleaded guilty to scamming the elderly with catfishing and stealing from veterans. [...]
