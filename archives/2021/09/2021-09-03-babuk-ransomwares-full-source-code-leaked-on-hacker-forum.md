Title: Babuk ransomware's full source code leaked on hacker forum
Date: 2021-09-03T11:22:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-03-babuk-ransomwares-full-source-code-leaked-on-hacker-forum

[Source](https://www.bleepingcomputer.com/news/security/babuk-ransomwares-full-source-code-leaked-on-hacker-forum/){:target="_blank" rel="noopener"}

> A threat actor has leaked the complete source code for the Babuk ransomware on a Russian-speaking hacking forum. [...]
