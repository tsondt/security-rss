Title: Windows MSHTML zero-day defenses bypassed as new info emerges
Date: 2021-09-09T16:37:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-09-windows-mshtml-zero-day-defenses-bypassed-as-new-info-emerges

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-mshtml-zero-day-defenses-bypassed-as-new-info-emerges/){:target="_blank" rel="noopener"}

> New details have emerged about the recent Windows CVE-2021-40444 zero-day vulnerability, how it is being exploited in attacks, and the threat actor's ultimate goal of taking over corporate networks. [...]
