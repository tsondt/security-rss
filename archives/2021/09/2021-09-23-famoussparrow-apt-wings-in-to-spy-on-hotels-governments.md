Title: FamousSparrow APT Wings in to Spy on Hotels, Governments
Date: 2021-09-23T14:08:12+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2021-09-23-famoussparrow-apt-wings-in-to-spy-on-hotels-governments

[Source](https://threatpost.com/famoussparrow-spy-hotels-governments/174948/){:target="_blank" rel="noopener"}

> A custom "SparrowDoor" backdoor has allowed the attackers to collect data from targets around the globe. [...]
