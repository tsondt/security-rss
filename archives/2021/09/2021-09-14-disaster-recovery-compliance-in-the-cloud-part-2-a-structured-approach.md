Title: Disaster recovery compliance in the cloud, part 2: A structured approach
Date: 2021-09-14T15:40:43+00:00
Author: Dan MacKay
Category: AWS Security
Tags: Finance and Investment;Security, Identity, & Compliance;Asset management;banking;Business continuity;Canada;Compliance;data localization;data residency;data sovereignty;Disaster Recovery;Financial Services;operational resilience;OSFI;PIPEDA;Privacy;Reliability;Resiliency;Security Blog;Wealth management
Slug: 2021-09-14-disaster-recovery-compliance-in-the-cloud-part-2-a-structured-approach

[Source](https://aws.amazon.com/blogs/security/disaster-recovery-compliance-in-the-cloud-part-2-a-structured-approach/){:target="_blank" rel="noopener"}

> Compliance in the cloud is fraught with myths and misconceptions. This is particularly true when it comes to something as broad as disaster recovery (DR) compliance where the requirements are rarely prescriptive and often based on legacy risk-mitigation techniques that don’t account for the exceptional resilience of modern cloud-based architectures. For regulated entities subject to [...]
