Title: The Week in Ransomware - September 3rd 2021 - Targeting Exchange
Date: 2021-09-03T16:17:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-03-the-week-in-ransomware-september-3rd-2021-targeting-exchange

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-3rd-2021-targeting-exchange/){:target="_blank" rel="noopener"}

> Over the past two weeks, it has been busy with ransomware news ranging from a gang shutting down and releasing a master decryption key to threat actors turning to Microsoft Exchange exploits to breach networks. [...]
