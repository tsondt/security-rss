Title: WhatsApp to appeal $266 million fine for violating EU privacy laws
Date: 2021-09-02T09:29:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-02-whatsapp-to-appeal-266-million-fine-for-violating-eu-privacy-laws

[Source](https://www.bleepingcomputer.com/news/security/whatsapp-to-appeal-266-million-fine-for-violating-eu-privacy-laws/){:target="_blank" rel="noopener"}

> Ireland's Data Privacy Commissioner (DPC) has hit Facebook-owned messaging platform WhatsApp with a €225 million ($266 million) administrative fine for violating the EU's GDPR privacy regulation after failing to inform users and non-users on what it does with their data. [...]
