Title: TikTok, GitHub, Facebook Join Open-Source Bug Bounty
Date: 2021-09-22T14:52:40+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Facebook;Vulnerabilities
Slug: 2021-09-22-tiktok-github-facebook-join-open-source-bug-bounty

[Source](https://threatpost.com/tiktok-github-facebook-open-source-bug-bounty/174898/){:target="_blank" rel="noopener"}

> The initiative, run by HackerOne, aims to uncover dangerous code repository bugs that end up going viral across the application supply-chain. [...]
