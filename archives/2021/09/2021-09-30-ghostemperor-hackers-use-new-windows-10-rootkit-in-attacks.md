Title: GhostEmperor hackers use new Windows 10 rootkit in attacks
Date: 2021-09-30T13:34:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-30-ghostemperor-hackers-use-new-windows-10-rootkit-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/ghostemperor-hackers-use-new-windows-10-rootkit-in-attacks/){:target="_blank" rel="noopener"}

> Chinese-speaking cyberspies have targeted Southeast Asian governmental entities and telecommunication companies for more than a year, backdooring systems running the latest Windows 10 versions with a newly discovered rootkit. [...]
