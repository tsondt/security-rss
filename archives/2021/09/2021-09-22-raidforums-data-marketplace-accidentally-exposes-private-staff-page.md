Title: RaidForums data marketplace accidentally exposes private staff page
Date: 2021-09-22T08:05:54-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-09-22-raidforums-data-marketplace-accidentally-exposes-private-staff-page

[Source](https://www.bleepingcomputer.com/news/security/raidforums-data-marketplace-accidentally-exposes-private-staff-page/){:target="_blank" rel="noopener"}

> Underground marketplace and hacker forum, Raidforums, recently exposed internal pages from its website, meant for staff members only. Raidforums is a data breach marketplace where threat actors often sell or leak illicitly obtained data dumps. [...]
