Title: QNAP fixes critical bugs in QVR video surveillance solution
Date: 2021-09-27T12:56:16-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-27-qnap-fixes-critical-bugs-in-qvr-video-surveillance-solution

[Source](https://www.bleepingcomputer.com/news/security/qnap-fixes-critical-bugs-in-qvr-video-surveillance-solution/){:target="_blank" rel="noopener"}

> Network-attached storage (NAS) maker QNAP has patched its QVR video management system against two critical-severity issues that could be exploited to run arbitrary commands. [...]
