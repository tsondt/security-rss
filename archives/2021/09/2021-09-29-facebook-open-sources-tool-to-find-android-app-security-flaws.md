Title: Facebook open-sources tool to find Android app security flaws
Date: 2021-09-29T16:11:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-29-facebook-open-sources-tool-to-find-android-app-security-flaws

[Source](https://www.bleepingcomputer.com/news/security/facebook-open-sources-tool-to-find-android-app-security-flaws/){:target="_blank" rel="noopener"}

> Facebook today open-sourced a static analysis tool its software and security engineers use internally to find potentially dangerous security and privacy flaws in the company's Android and Java applications. [...]
