Title: Bandwidth.com is latest victim of DDoS attacks against VoIP providers
Date: 2021-09-27T21:07:31-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-27-bandwidthcom-is-latest-victim-of-ddos-attacks-against-voip-providers

[Source](https://www.bleepingcomputer.com/news/security/bandwidthcom-is-latest-victim-of-ddos-attacks-against-voip-providers/){:target="_blank" rel="noopener"}

> Bandwidth.com has become the latest victim of distributed denial of service attacks targeting VoIP providers this month, leading to nationwide voice outages over the past few days. [...]
