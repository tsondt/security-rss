Title: Threat Actors Weaponize Telegram Bots to Compromise PayPal Accounts
Date: 2021-09-29T13:55:05+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Web Security
Slug: 2021-09-29-threat-actors-weaponize-telegram-bots-to-compromise-paypal-accounts

[Source](https://threatpost.com/telegram-bots-compromise-paypal/175099/){:target="_blank" rel="noopener"}

> A campaign is stealing one-time password tokens to gain access to PayPal, Apple Pay and Google Pay, among others. [...]
