Title: SpyFone & CEO Banned From Stalkerware Biz
Date: 2021-09-02T20:12:24+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Mobile Security;News;Privacy;Web Security
Slug: 2021-09-02-spyfone-ceo-banned-from-stalkerware-biz

[Source](https://threatpost.com/spyfone-ban-stalkerware-surveillance/169165/){:target="_blank" rel="noopener"}

> The FTC's first spyware ban nixes a company whose "slipshod" security practices led to exposure of thousands of victims' illegally collected personal data. [...]
