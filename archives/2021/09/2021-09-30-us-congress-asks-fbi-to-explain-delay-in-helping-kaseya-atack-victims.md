Title: US Congress asks FBI to explain delay in helping Kaseya atack victims
Date: 2021-09-30T07:48:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-30-us-congress-asks-fbi-to-explain-delay-in-helping-kaseya-atack-victims

[Source](https://www.bleepingcomputer.com/news/security/us-congress-asks-fbi-to-explain-delay-in-helping-kaseya-atack-victims/){:target="_blank" rel="noopener"}

> The House Committee on Oversight and Reform has requested a briefing to understand the rationale behind FBI's decision to delay providing the victims of the Kaseya REvil ransomware with an universal decryption key for three weeks. [...]
