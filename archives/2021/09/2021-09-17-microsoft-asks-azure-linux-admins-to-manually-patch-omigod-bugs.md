Title: Microsoft asks Azure Linux admins to manually patch OMIGOD bugs
Date: 2021-09-17T08:06:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-17-microsoft-asks-azure-linux-admins-to-manually-patch-omigod-bugs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-asks-azure-linux-admins-to-manually-patch-omigod-bugs/){:target="_blank" rel="noopener"}

> Microsoft has issued additional guidance on securing Azure Linux machines impacted by recently addressed critical OMIGOD vulnerabilities. [...]
