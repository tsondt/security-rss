Title: Computer and data scientists should be as highly regarded as 'warriors' says top UK cybergeneral
Date: 2021-09-16T12:14:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-16-computer-and-data-scientists-should-be-as-highly-regarded-as-warriors-says-top-uk-cybergeneral

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/16/dsei_compsci_prestige_warriors_sanders_speech/){:target="_blank" rel="noopener"}

> Translation: Skills shortage here! DSEI 2021 Military computer scientists ought to be treated with the same regard as pilots and warship captains, the head of the Army's cyber command has said.... [...]
