Title: Microsoft Patch Tuesday, September 2021 Edition
Date: 2021-09-14T21:00:42+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;adobe;Allan Liska;apple;chrome;CVE-2021-28316;CVE-2021-30860;CVE-2021-36965;CVE-2021-40444;google;Immersive Labs;Kevin Breen;Microsoft Patch Tuesday September 2021;Recorded Future
Slug: 2021-09-14-microsoft-patch-tuesday-september-2021-edition

[Source](https://krebsonsecurity.com/2021/09/microsoft-patch-tuesday-september-2021-edition/){:target="_blank" rel="noopener"}

> Microsoft today pushed software updates to plug dozens of security holes in Windows and related products, including a vulnerability that is already being exploited in active attacks. Also, Apple has issued an emergency update to fix a flaw that’s reportedly been abused to install spyware on iOS products, and Google ‘s got a new version of Chrome that tackles two zero-day flaws. Finally, Adobe has released critical security updates for Acrobat, Reader and a slew of other software. Four of the flaws fixed in this patch batch earned Microsoft’s most-dire “critical” rating, meaning they could be exploited by miscreants or [...]
