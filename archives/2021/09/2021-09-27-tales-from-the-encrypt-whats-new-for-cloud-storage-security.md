Title: Tales from the (en)crypt: What's new for Cloud Storage security
Date: 2021-09-27T16:00:00+00:00
Author: Henry Yuen
Category: GCP Security
Tags: Identity & Security;Storage & Data Transfer
Slug: 2021-09-27-tales-from-the-encrypt-whats-new-for-cloud-storage-security

[Source](https://cloud.google.com/blog/products/storage-data-transfer/learn-about-the-latest-cloud-storage-encryption-releases/){:target="_blank" rel="noopener"}

> Encryption is critical for securing sensitive data while it is stored and transits the cloud. Today, Cloud Storage encrypts data server-side with standard Google-managed encryption keys by default, and can also encrypt data with customer-managed encryption keys that are stored and managed by Cloud Key Management Service (Cloud KMS). While customers have been able to secure their data on Cloud Storage with Cloud KMS keys for some time, we are always updating our encryption offerings to deliver better performance, lower costs and more capabilities that support critical business workloads. In this post, we’ll discuss some of our latest developments in [...]
