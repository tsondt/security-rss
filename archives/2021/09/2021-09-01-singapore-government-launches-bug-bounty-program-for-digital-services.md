Title: Singapore government launches bug bounty program for digital services
Date: 2021-09-01T12:50:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-01-singapore-government-launches-bug-bounty-program-for-digital-services

[Source](https://portswigger.net/daily-swig/singapore-government-launches-bug-bounty-program-for-digital-services){:target="_blank" rel="noopener"}

> Monetary rewards for ethical hackers [...]
