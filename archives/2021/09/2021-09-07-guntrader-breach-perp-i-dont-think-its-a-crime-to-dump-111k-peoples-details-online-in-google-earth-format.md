Title: Guntrader breach perp: I don't think it's a crime to dump 111k people's details online in Google Earth format
Date: 2021-09-07T10:01:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-07-guntrader-breach-perp-i-dont-think-its-a-crime-to-dump-111k-peoples-details-online-in-google-earth-format

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/07/guntrader_hack_perp_interview/){:target="_blank" rel="noopener"}

> Plus: Police aren't treating breaches as terror offence The person who reformatted the Guntrader hack data as a Google Earth-compatible CSV has said they are prepared to go to prison – while denying their actions amounted to a criminal offence.... [...]
