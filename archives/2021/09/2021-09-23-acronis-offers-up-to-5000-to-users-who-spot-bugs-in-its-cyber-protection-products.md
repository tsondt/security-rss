Title: Acronis Offers up to $5,000 to Users Who Spot Bugs in Its Cyber Protection Products
Date: 2021-09-23T13:00:25+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Sponsored;Web Security
Slug: 2021-09-23-acronis-offers-up-to-5000-to-users-who-spot-bugs-in-its-cyber-protection-products

[Source](https://threatpost.com/acronis-bug-bounty-program/174849/){:target="_blank" rel="noopener"}

> Once available only to the cybersecurity community, Acronis has opened its bug-hunting program to the public and aims to double the total bounties paid. [...]
