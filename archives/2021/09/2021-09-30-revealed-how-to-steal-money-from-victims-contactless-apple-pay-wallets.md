Title: Revealed: How to steal money from victims' contactless Apple Pay wallets
Date: 2021-09-30T23:38:52+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-30-revealed-how-to-steal-money-from-victims-contactless-apple-pay-wallets

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/30/apple_pay_contactless_visa_fraud/){:target="_blank" rel="noopener"}

> Boffins devise tricks to dupe stolen or nearby iPhones into paying out when in transit mode and using Visa Apple's digital wallet Apple Pay will pay whatever amount is demanded of it, without authorization, if configured for transit mode with a Visa card, and exposed to a hostile contactless reader.... [...]
