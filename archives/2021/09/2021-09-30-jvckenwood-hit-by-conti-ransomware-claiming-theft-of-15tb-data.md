Title: JVCKenwood hit by Conti ransomware claiming theft of 1.5TB data
Date: 2021-09-30T12:38:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-30-jvckenwood-hit-by-conti-ransomware-claiming-theft-of-15tb-data

[Source](https://www.bleepingcomputer.com/news/security/jvckenwood-hit-by-conti-ransomware-claiming-theft-of-15tb-data/){:target="_blank" rel="noopener"}

> JVCKenwood has suffered a Conti ransomware attack where the threat actors claim to have stolen 1.7 TB of data and are demanding a $7 million ransom. [...]
