Title: Financial Cybercrime: Why Cryptocurrency is the Perfect ‘Getaway Car’
Date: 2021-09-09T17:51:24+00:00
Author: John Hammond
Category: Threatpost
Tags: Breach;Cloud Security;Cryptography;Hacks;InfoSec Insider;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2021-09-09-financial-cybercrime-why-cryptocurrency-is-the-perfect-getaway-car

[Source](https://threatpost.com/financial-cybercrime-cryptocurrency/169327/){:target="_blank" rel="noopener"}

> John Hammond, security researcher with Huntress, discusses how financially motivated cybercrooks use and abuse cryptocurrency. [...]
