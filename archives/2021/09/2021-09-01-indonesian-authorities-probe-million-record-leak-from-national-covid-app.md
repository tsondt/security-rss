Title: Indonesian authorities probe million-record leak from national COVID app
Date: 2021-09-01T05:16:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-01-indonesian-authorities-probe-million-record-leak-from-national-covid-app

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/01/indonesia_ehac_covid_app_pii_leak/){:target="_blank" rel="noopener"}

> Someone didn't secure an Elasticsearch database, researchers allege Indonesia's Ministry of Communications and Informatics is investigating a leak of over a million records from the nation's COVID-19 quarantine management app.... [...]
