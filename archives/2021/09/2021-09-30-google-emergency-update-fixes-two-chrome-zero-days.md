Title: Google Emergency Update Fixes Two Chrome Zero Days
Date: 2021-09-30T22:38:50+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: News;Vulnerabilities;Web Security
Slug: 2021-09-30-google-emergency-update-fixes-two-chrome-zero-days

[Source](https://threatpost.com/google-emergency-update-chrome-zero-days/175266/){:target="_blank" rel="noopener"}

> This is the second pair of zero days that Google's fixed this month, all four of which have been actively exploited in the wild. [...]
