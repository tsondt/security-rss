Title: Tooling Network Detection & Response for Ransomware
Date: 2021-09-08T19:00:26+00:00
Author: Justin Jett
Category: Threatpost
Tags: Breach;Hacks;InfoSec Insider
Slug: 2021-09-08-tooling-network-detection-response-for-ransomware

[Source](https://threatpost.com/network-detection-response-ransomware/169290/){:target="_blank" rel="noopener"}

> Justin Jett, director of audit and compliance at Plixer, discusses how to effectively use network flow data in the fight against ransomware. [...]
