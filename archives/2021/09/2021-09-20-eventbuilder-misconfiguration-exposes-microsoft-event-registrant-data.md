Title: EventBuilder misconfiguration exposes Microsoft event registrant data
Date: 2021-09-20T09:37:45-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-20-eventbuilder-misconfiguration-exposes-microsoft-event-registrant-data

[Source](https://www.bleepingcomputer.com/news/security/eventbuilder-misconfiguration-exposes-microsoft-event-registrant-data/){:target="_blank" rel="noopener"}

> Personal details of registrants to virtual events available through the EventBuilder platform have stayed accessible over the public internet, open to indexing by various engines. [...]
