Title: Eight US financial services firms given six-figure fines over BEC data breaches
Date: 2021-09-01T13:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-01-eight-us-financial-services-firms-given-six-figure-fines-over-bec-data-breaches

[Source](https://portswigger.net/daily-swig/eight-us-financial-services-firms-given-six-figure-fines-over-bec-data-breaches){:target="_blank" rel="noopener"}

> Thousands of victims involved as separate report warns of wider rise in brute-force attacks against accounts [...]
