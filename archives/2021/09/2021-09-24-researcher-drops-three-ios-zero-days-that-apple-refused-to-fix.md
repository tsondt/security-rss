Title: Researcher drops three iOS zero-days that Apple refused to fix
Date: 2021-09-24T07:13:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2021-09-24-researcher-drops-three-ios-zero-days-that-apple-refused-to-fix

[Source](https://www.bleepingcomputer.com/news/security/researcher-drops-three-ios-zero-days-that-apple-refused-to-fix/){:target="_blank" rel="noopener"}

> Proof-of-concept exploit code for three iOS zero-day vulnerabilities (and a fourth one patched in July) was published on GitHub after Apple delayed patching and failed to credit the researcher. [...]
