Title: CISA releases tool to help orgs fend off insider threat risks
Date: 2021-09-29T14:17:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-29-cisa-releases-tool-to-help-orgs-fend-off-insider-threat-risks

[Source](https://www.bleepingcomputer.com/news/security/cisa-releases-tool-to-help-orgs-fend-off-insider-threat-risks/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) has released a new tool that allows public and private sector organizations to assess their vulnerability to insider threats and devise their own defense plans against such risks. [...]
