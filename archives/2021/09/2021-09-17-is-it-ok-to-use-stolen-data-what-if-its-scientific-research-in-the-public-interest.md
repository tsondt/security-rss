Title: Is it OK to use stolen data? What if it's scientific research in the public interest?
Date: 2021-09-17T13:02:07+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2021-09-17-is-it-ok-to-use-stolen-data-what-if-its-scientific-research-in-the-public-interest

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/17/unethical_data_research/){:target="_blank" rel="noopener"}

> Not always, but Swiss team says you can manage the risks There's a fine line between getting hold of data that may be in the public interest and downright stealing data just because you can. And simply because the data is out there – having been stolen by online intruders and then leaked – does not mean it is right to use it.... [...]
