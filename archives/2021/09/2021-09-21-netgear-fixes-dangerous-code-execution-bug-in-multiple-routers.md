Title: Netgear fixes dangerous code execution bug in multiple routers
Date: 2021-09-21T11:24:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-21-netgear-fixes-dangerous-code-execution-bug-in-multiple-routers

[Source](https://www.bleepingcomputer.com/news/security/netgear-fixes-dangerous-code-execution-bug-in-multiple-routers/){:target="_blank" rel="noopener"}

> Netgear has fixed a high severity remote code execution (RCE) vulnerability found in the Circle parental control service, which runs with root permissions on almost a dozen modern Small Offices/Home Offices (SOHO) Netgear routers. [...]
