Title: Ransomware crim: Yeah, what I do is bad. No, I don't care. Yes, infosec bods are all mouth and no trousers
Date: 2021-09-30T19:00:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-30-ransomware-crim-yeah-what-i-do-is-bad-no-i-dont-care-yes-infosec-bods-are-all-mouth-and-no-trousers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/30/revil_ransomware_contractor_interview_lenta/){:target="_blank" rel="noopener"}

> Claimed REvil contractor badmouths West from anonymous pulpit Someone claiming to be a former contractor for the REvil ransomware gang has given an interview to a security firm, saying he struggles to sleep at night but isn't ashamed of what he does.... [...]
