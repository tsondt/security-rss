Title: FTC bans stalkerware maker Spyfone from surveillance business
Date: 2021-09-01T14:49:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-01-ftc-bans-stalkerware-maker-spyfone-from-surveillance-business

[Source](https://www.bleepingcomputer.com/news/security/ftc-bans-stalkerware-maker-spyfone-from-surveillance-business/){:target="_blank" rel="noopener"}

> FTC has banned stalkerware maker Spyfone and CEO Scott Zuckerman from the surveillance business after failing to protect customers' devices from hackers and sharing info on their location and activity. [...]
