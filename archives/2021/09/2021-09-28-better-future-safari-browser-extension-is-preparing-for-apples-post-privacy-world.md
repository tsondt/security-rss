Title: Better future? Safari browser extension is preparing for Apple’s ‘post-privacy’ world
Date: 2021-09-28T11:37:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-28-better-future-safari-browser-extension-is-preparing-for-apples-post-privacy-world

[Source](https://portswigger.net/daily-swig/better-future-safari-browser-extension-is-preparing-for-apples-post-privacy-world){:target="_blank" rel="noopener"}

> ‘Apple’s plans to violate your privacy have left a sour taste in our mouths’, says developers [...]
