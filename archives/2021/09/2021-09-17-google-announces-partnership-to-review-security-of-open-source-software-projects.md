Title: Google announces partnership to review security of open source software projects
Date: 2021-09-17T12:54:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-17-google-announces-partnership-to-review-security-of-open-source-software-projects

[Source](https://portswigger.net/daily-swig/google-announces-partnership-to-review-security-of-open-source-software-projects){:target="_blank" rel="noopener"}

> Tech giant will lend its support to security reviews of eight projects, including Git, Lodash, and Laravel [...]
