Title: Apple will disable insecure TLS in future iOS, macOS releases
Date: 2021-09-22T12:59:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2021-09-22-apple-will-disable-insecure-tls-in-future-ios-macos-releases

[Source](https://www.bleepingcomputer.com/news/apple/apple-will-disable-insecure-tls-in-future-ios-macos-releases/){:target="_blank" rel="noopener"}

> Apple has deprecated the insecure Transport Layer Security (TLS) 1.0 and 1.1 protocols in recently launched iOS and macOS versions and plans to remove support in future releases altogether. [...]
