Title: Stolen Credentials Led to Data Theft at United Nations
Date: 2021-09-10T10:46:17+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Hacks
Slug: 2021-09-10-stolen-credentials-led-to-data-theft-at-united-nations

[Source](https://threatpost.com/data-theft-united-nations/169357/){:target="_blank" rel="noopener"}

> Threat actors accessed the organization’s proprietary project management software, Umoja, in April, accessing the network and stealing info that can be used in further attacks. [...]
