Title: IoT Attacks Skyrocket, Doubling in 6 Months
Date: 2021-09-06T12:00:02+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Most Recent ThreatLists
Slug: 2021-09-06-iot-attacks-skyrocket-doubling-in-6-months

[Source](https://threatpost.com/iot-attacks-doubling/169224/){:target="_blank" rel="noopener"}

> The first half of 2021 saw 1.5 billion attacks on smart devices, with attackers looking to steal data, mine cryptocurrency or build botnets. [...]
