Title: Ransomware-hit law firm secures High Court judgment against unknown criminals
Date: 2021-09-16T15:15:26+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-16-ransomware-hit-law-firm-secures-high-court-judgment-against-unknown-criminals

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/16/ransomware_judgment_4_new_square_chambers/){:target="_blank" rel="noopener"}

> You tell 'em, 4 New Square chambers The London law firm which secured a court injunction forbidding ransomware criminals from publishing data stolen from them has now gone a step further – by securing a default judgment from the High Court.... [...]
