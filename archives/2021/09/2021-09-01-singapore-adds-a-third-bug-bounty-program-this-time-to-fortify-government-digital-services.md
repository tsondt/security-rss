Title: Singapore adds a third bug bounty program – this time to fortify government digital services
Date: 2021-09-01T04:14:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-09-01-singapore-adds-a-third-bug-bounty-program-this-time-to-fortify-government-digital-services

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/01/singapore_partners_with_hackerone_to/){:target="_blank" rel="noopener"}

> HackerOne gets the gig Singapore's governmental digital services arm, GovTech, has launched a "rewards programme" to further crowdsource tests of the nation's cybersecurity.... [...]
