Title: Ex-US intel, military trio were cyber-mercenaries for UAE, say prosecutors
Date: 2021-09-15T06:45:12+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2021-09-15-ex-us-intel-military-trio-were-cyber-mercenaries-for-uae-say-prosecutors

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/15/three_rogue_former_nsa_hackers/){:target="_blank" rel="noopener"}

> Three men charged with breaking export, security laws, agree to deal after infiltrating smartphones with zero-click exploits Three former US intelligence and military operatives broke America's weapons export and computer security laws by, among other things, helping the United Arab Emirates hijack and siphon data from people's iPhones, it emerged on Tuesday.... [...]
