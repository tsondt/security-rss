Title: WireX DDoS botnet admin charged for attacking hotel chain
Date: 2021-09-30T09:14:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-30-wirex-ddos-botnet-admin-charged-for-attacking-hotel-chain

[Source](https://www.bleepingcomputer.com/news/security/wirex-ddos-botnet-admin-charged-for-attacking-hotel-chain/){:target="_blank" rel="noopener"}

> The US Department of Justice charged the admin of the WireX Android botnet for targeting an American multinational hotel chain in a distributed denial-of-service (DDoS) attack. [...]
