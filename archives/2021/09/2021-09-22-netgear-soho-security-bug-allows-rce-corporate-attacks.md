Title: Netgear SOHO Security Bug Allows RCE, Corporate Attacks
Date: 2021-09-22T19:41:59+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Vulnerabilities;Web Security
Slug: 2021-09-22-netgear-soho-security-bug-allows-rce-corporate-attacks

[Source](https://threatpost.com/netgear-soho-security-bug-rce/174921/){:target="_blank" rel="noopener"}

> The issue lies in a parental-control function that's always enabled by default, even if users don't configure for child security. [...]
