Title: How to improve visibility into AWS WAF with anomaly detection
Date: 2021-09-01T21:43:00+00:00
Author: Cyril Soler
Category: AWS Security
Tags: AWS WAF;Intermediate (200);Security, Identity, & Compliance;Amazon Lookout for Metrics;Anomaly detection;Machine learning;Security Blog
Slug: 2021-09-01-how-to-improve-visibility-into-aws-waf-with-anomaly-detection

[Source](https://aws.amazon.com/blogs/security/how-to-improve-visibility-into-aws-waf-with-anomaly-detection/){:target="_blank" rel="noopener"}

> When your APIs are exposed on the internet, they naturally face unpredictable traffic. AWS WAF helps protect your application’s API against common web exploits, such as SQL injection and cross-site scripting. In this blog post, you’ll learn how to automatically detect anomalies in the AWS WAF metrics to improve your visibility into AWS WAF activity, [...]
