Title: Epik Confirms Hack, Gigabytes of Data on Offer
Date: 2021-09-21T19:22:19+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Privacy;Web Security
Slug: 2021-09-21-epik-confirms-hack-gigabytes-of-data-on-offer

[Source](https://threatpost.com/epik-confirms-hack-data/174872/){:target="_blank" rel="noopener"}

> "Time to find out who in your family secretly ran... [a] QAnon hellhole," said attackers who affiliated themselves with the hacktivist collective Anonymous, noting that Epik had laughable security. [...]
