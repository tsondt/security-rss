Title: Lithuania tells its citizens to throw Xiaomi mobile devices in the bin
Date: 2021-09-22T20:36:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-22-lithuania-tells-its-citizens-to-throw-xiaomi-mobile-devices-in-the-bin

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/22/xiaomi_phone_handset_censorship_lithuania/){:target="_blank" rel="noopener"}

> Baltic state's cyber security centre uncovers remote censorship blocklist function in Mi 10T handset Lithuania's National Cyber Security Centre has told its citizens to get rid of Xiaomi-made mobile devices amid fears that the Chinese company could remotely enable censorship tools.... [...]
