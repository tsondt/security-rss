Title: FIN7 Capitalizes on Windows 11 Release in Latest Gambit
Date: 2021-09-03T16:07:53+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-09-03-fin7-capitalizes-on-windows-11-release-in-latest-gambit

[Source](https://threatpost.com/fin7-windows-11-release/169206/){:target="_blank" rel="noopener"}

> The financially motivated group looked to steal payment-card data from a California-based point-of-sale service provider. [...]
