Title: Microsoft will disable Basic Auth in Exchange Online in October 2022
Date: 2021-09-26T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-26-microsoft-will-disable-basic-auth-in-exchange-online-in-october-2022

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-will-disable-basic-auth-in-exchange-online-in-october-2022/){:target="_blank" rel="noopener"}

> Microsoft announced that Basic Authentication will be turned off for all protocols in all tenants starting October 1st, 2022, to protect millions of Exchange Online users. [...]
