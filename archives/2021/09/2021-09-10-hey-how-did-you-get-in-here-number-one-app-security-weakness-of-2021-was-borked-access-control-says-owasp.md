Title: Hey – how did you get in here? Number one app security weakness of 2021 was borked access control, says OWASP
Date: 2021-09-10T18:35:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-10-hey-how-did-you-get-in-here-number-one-app-security-weakness-of-2021-was-borked-access-control-says-owasp

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/10/owasp_top_ten_appsec_list/){:target="_blank" rel="noopener"}

> Org releases its top ten list of bad things software vendors do The Open Web App Security Project has released its Top Ten list of vulnerabilities in web software, as part of the general movement to make software less painfully insecure at the design stage.... [...]
