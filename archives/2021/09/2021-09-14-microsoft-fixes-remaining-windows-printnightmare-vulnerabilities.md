Title: Microsoft fixes remaining Windows PrintNightmare vulnerabilities
Date: 2021-09-14T16:43:14-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-14-microsoft-fixes-remaining-windows-printnightmare-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-remaining-windows-printnightmare-vulnerabilities/){:target="_blank" rel="noopener"}

> Microsoft has released a security update to fix the last remaining PrintNightmare zero-day vulnerabilities that allowed attackers to gain administrative privileges on Windows devices quickly. [...]
