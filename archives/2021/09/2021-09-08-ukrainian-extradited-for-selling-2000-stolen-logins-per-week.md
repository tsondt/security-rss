Title: Ukrainian extradited for selling 2,000 stolen logins per week
Date: 2021-09-08T18:10:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-08-ukrainian-extradited-for-selling-2000-stolen-logins-per-week

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-extradited-for-selling-2-000-stolen-logins-per-week/){:target="_blank" rel="noopener"}

> The US Department of Justice has indicted a Ukrainian man for using a malware botnet to brute force computer logon credentials and then selling them on a criminal remote access marketplace. [...]
