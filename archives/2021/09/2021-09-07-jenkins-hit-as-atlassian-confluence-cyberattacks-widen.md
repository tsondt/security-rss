Title: Jenkins Hit as Atlassian Confluence Cyberattacks Widen
Date: 2021-09-07T16:07:58+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Vulnerabilities
Slug: 2021-09-07-jenkins-hit-as-atlassian-confluence-cyberattacks-widen

[Source](https://threatpost.com/jenkins-atlassian-confluence-cyberattacks/169249/){:target="_blank" rel="noopener"}

> Patch now: The popular biz-collaboration platform is seeing mass scanning and exploitation just two weeks after a critical RCE bug was disclosed. [...]
