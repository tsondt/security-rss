Title: Comcast RF Attack Leveraged Remotes for Surveillance
Date: 2021-09-02T11:03:23+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;IoT;Privacy;Vulnerabilities
Slug: 2021-09-02-comcast-rf-attack-leveraged-remotes-for-surveillance

[Source](https://threatpost.com/comcast-rf-attack-remotes-surveillance/169133/){:target="_blank" rel="noopener"}

> IoT vulnerabilities turned the remote into a listening device, researchers found, which impacted 18 million Xfinity customers. [...]
