Title: Mission accomplished: Security plugin HTTPS Everywhere to be deprecated in 2022
Date: 2021-09-28T14:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-28-mission-accomplished-security-plugin-https-everywhere-to-be-deprecated-in-2022

[Source](https://portswigger.net/daily-swig/mission-accomplished-security-plugin-https-everywhere-to-be-deprecated-in-2022){:target="_blank" rel="noopener"}

> Browser extension can be retired as push to encrypt the web is almost complete, says EFF [...]
