Title: RCE vulnerabilities in open source software Cachet could put users at risk
Date: 2021-09-29T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-29-rce-vulnerabilities-in-open-source-software-cachet-could-put-users-at-risk

[Source](https://portswigger.net/daily-swig/rce-vulnerabilities-in-open-source-software-cachet-could-put-users-at-risk){:target="_blank" rel="noopener"}

> Patches released for status page management system flaws [...]
