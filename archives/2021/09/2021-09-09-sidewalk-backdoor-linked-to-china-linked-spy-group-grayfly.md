Title: SideWalk Backdoor Linked to China-Linked Spy Group ‘Grayfly’
Date: 2021-09-09T14:30:56+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;Web Security
Slug: 2021-09-09-sidewalk-backdoor-linked-to-china-linked-spy-group-grayfly

[Source](https://threatpost.com/sidewalk-backdoor-china-espionage-grayfly/169310/){:target="_blank" rel="noopener"}

> Grayfly campaigns have launched the novel malware against businesses in Taiwan, Vietnam, the US and Mexico and are targeting Exchange and MySQL servers. [...]
