Title: BlackMatter ransomware hits medical technology giant Olympus
Date: 2021-09-13T07:49:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-13-blackmatter-ransomware-hits-medical-technology-giant-olympus

[Source](https://www.bleepingcomputer.com/news/security/blackmatter-ransomware-hits-medical-technology-giant-olympus/){:target="_blank" rel="noopener"}

> Olympus, a leading medical technology company, is investigating a "potential cybersecurity incident" that impacted some of its EMEA (Europe, Middle East, Africa) IT systems last week. [...]
