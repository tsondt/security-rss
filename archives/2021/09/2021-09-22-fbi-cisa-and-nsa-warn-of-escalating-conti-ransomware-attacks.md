Title: FBI, CISA, and NSA warn of escalating Conti ransomware attacks
Date: 2021-09-22T13:24:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-22-fbi-cisa-and-nsa-warn-of-escalating-conti-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-cisa-and-nsa-warn-of-escalating-conti-ransomware-attacks/){:target="_blank" rel="noopener"}

> CISA, the Federal Bureau of Investigation (FBI), and the National Security Agency (NSA) warned today of an increased number of Conti ransomware attacks targeting US organizations. [...]
