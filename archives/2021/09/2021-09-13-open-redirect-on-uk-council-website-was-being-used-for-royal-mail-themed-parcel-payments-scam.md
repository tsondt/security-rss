Title: Open redirect on UK council website was being used for Royal Mail-themed parcel payments scam
Date: 2021-09-13T15:15:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-13-open-redirect-on-uk-council-website-was-being-used-for-royal-mail-themed-parcel-payments-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/13/open_redirect_council_property_website_spam/){:target="_blank" rel="noopener"}

> Homes4Wiltshire, aka Homes4Spammers An open redirect on a UK council-backed property website allowed low-level miscreants to evade filters.... [...]
