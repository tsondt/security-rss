Title: Women, Minorities Are Hacked More Than Others
Date: 2021-09-27T18:27:15+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Malware;Mobile Security;Most Recent ThreatLists;Privacy;Web Security
Slug: 2021-09-27-women-minorities-are-hacked-more-than-others

[Source](https://threatpost.com/women-minorities-hacked/175038/){:target="_blank" rel="noopener"}

> Income level, education and being part of a disadvantaged population all contribute to cybercrime outcomes, a survey suggests. [...]
