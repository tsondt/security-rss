Title: The State of Incident Response: Measuring Risk and Evaluating Your Preparedness
Date: 2021-09-03T16:15:26+00:00
Author: Grant Oviatt
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Malware;Vulnerabilities;Web Security
Slug: 2021-09-03-the-state-of-incident-response-measuring-risk-and-evaluating-your-preparedness

[Source](https://threatpost.com/incident-response-risk-preparedness/169211/){:target="_blank" rel="noopener"}

> Grant Oviatt, director of incident-response engagements at Red Canary, provides advice and best practices on how to get there faster. [...]
