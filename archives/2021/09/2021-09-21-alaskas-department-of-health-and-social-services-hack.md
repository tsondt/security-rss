Title: Alaska’s Department of Health and Social Services Hack
Date: 2021-09-21T11:05:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberespionage;hacking;healthcare;leaks
Slug: 2021-09-21-alaskas-department-of-health-and-social-services-hack

[Source](https://www.schneier.com/blog/archives/2021/09/alaskas-department-of-health-and-social-services-hack.html){:target="_blank" rel="noopener"}

> Apparently, a nation-state hacked Alaska’s Department of Health and Social Services. Not sure why Alaska’s Department of Health and Social Services is of any interest to a nation-state, but that’s probably just my failure of imagination. [...]
