Title: McDonald's email blunder broadcasts database creds to comedy competition winners
Date: 2021-09-09T12:58:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-09-mcdonalds-email-blunder-broadcasts-database-creds-to-comedy-competition-winners

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/09/mcdonalds_database_credentials_blunder/){:target="_blank" rel="noopener"}

> Finder tells El Reg of struggle to report snafu McDonald's customers who won a prize draw competition got more than they hoped for after the burger chain emailed them login credentials for development and production databases used to power the campaign.... [...]
