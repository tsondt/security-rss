Title: REvil ransomware is back in full attack mode and leaking data
Date: 2021-09-11T13:15:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-11-revil-ransomware-is-back-in-full-attack-mode-and-leaking-data

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-is-back-in-full-attack-mode-and-leaking-data/){:target="_blank" rel="noopener"}

> The REvil ransomware gang has fully returned and is once again attacking new victims and publishing stolen files on a data leak site. [...]
