Title: Global pandemic was good for business say UK infosec pros – but we're still burning out
Date: 2021-09-08T11:28:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-08-global-pandemic-was-good-for-business-say-uk-infosec-pros-but-were-still-burning-out

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/08/ciisec_state_uk_infosec_report/){:target="_blank" rel="noopener"}

> Chartered Institute of Information Security reveals what you're all thinking The COVID-19 pandemic was good for business, according to British infosec workers – although half of them still say they feel burnt out amid the surge in work.... [...]
