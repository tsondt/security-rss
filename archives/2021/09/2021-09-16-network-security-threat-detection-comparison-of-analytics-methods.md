Title: Network security threat detection - Comparison of analytics methods
Date: 2021-09-16T16:00:00+00:00
Author: Gregory M. Lebovitz
Category: GCP Security
Tags: Identity & Security;Solutions and How-to's;Open Source;Networking
Slug: 2021-09-16-network-security-threat-detection-comparison-of-analytics-methods

[Source](https://cloud.google.com/blog/products/networking/when-to-use-5-telemetry-types-in-security-threat-monitoring/){:target="_blank" rel="noopener"}

> Jaliesha is responsible for cybersecurity within the DevOps team at her cloud-native software service company – they call it DevSecOps. She has several requirements pressing down on her as their offering explodes in popularity and they take in their second round of VC funding: Meet compliance requirements for Intrusion Detection System and Intrusion Prevention System (IDS / IPS) on the PCI DSS in-scope infrastructure, and produce artifacts for their upcoming SOC2 audit; Continuously advance toward their goal to fulfill 90% of the Cloud Controls Matrix on their Cloud Security Alliance CAIQ ; Collaborate with the Network Operations Center (NOC) team [...]
