Title: French shipping giant CMA CGM suffers data breach
Date: 2021-09-21T12:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-21-french-shipping-giant-cma-cgm-suffers-data-breach

[Source](https://portswigger.net/daily-swig/french-shipping-giant-cma-cgm-suffers-data-breach){:target="_blank" rel="noopener"}

> Customer data impacted by security incident [...]
