Title: Built-in transparency, automation, and interoperability for Cloud KMS
Date: 2021-09-29T16:00:00+00:00
Author: Sonal Shah
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-09-29-built-in-transparency-automation-and-interoperability-for-cloud-kms

[Source](https://cloud.google.com/blog/products/identity-security/transparency-interop-and-automation-for-google-cloud-kms/){:target="_blank" rel="noopener"}

> Cloud KMS helps customers implement scalable, centralized, fast cloud key management for their projects and apps on Google Cloud. As use of Cloud KMS has grown, many organizations have looked for ways to better understand crypto assets and to make more informed decisions around data management and compliance. In response, the Cloud KMS team is pleased to announce several new features to help customers meet goals around increased transparency, improved interoperability, and greater automation as they use Cloud KMS. Transparency: Key Inventory Dashboard One major request we’ve heard from our largest adopters of Cloud KMS is for improved transparency around [...]
