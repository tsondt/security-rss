Title: Protect your remote workforce by using a managed DNS firewall and network firewall
Date: 2021-09-13T18:35:16+00:00
Author: Patrick Duffy
Category: AWS Security
Tags: Amazon Route 53;AWS Network Firewall;Foundational (100);Security, Identity, & Compliance;Networking;Security Blog
Slug: 2021-09-13-protect-your-remote-workforce-by-using-a-managed-dns-firewall-and-network-firewall

[Source](https://aws.amazon.com/blogs/security/protect-your-remote-workforce-by-using-a-managed-dns-firewall-and-network-firewall/){:target="_blank" rel="noopener"}

> More of our customers are adopting flexible work-from-home and remote work strategies that use virtual desktop solutions, such as Amazon WorkSpaces and Amazon AppStream 2.0, to deliver their user applications. Securing these workloads benefits from a layered approach, and this post focuses on protecting your users at the network level. Customers can now apply these [...]
