Title: Attackers Impersonate DoT in Two-Day Phishing Scam
Date: 2021-09-15T13:06:52+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Web Security
Slug: 2021-09-15-attackers-impersonate-dot-in-two-day-phishing-scam

[Source](https://threatpost.com/attackers-impersonate-dot-phishing-scam/169484/){:target="_blank" rel="noopener"}

> Threat actors dangled the lure of receiving funds from the $1 trillion infrastructure bill and created new domains mimicking the real federal site. [...]
