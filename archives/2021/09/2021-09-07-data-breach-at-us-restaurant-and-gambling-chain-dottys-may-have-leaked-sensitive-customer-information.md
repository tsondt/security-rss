Title: Data breach at US restaurant and gambling chain Dotty’s may have leaked sensitive customer information
Date: 2021-09-07T11:06:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-07-data-breach-at-us-restaurant-and-gambling-chain-dottys-may-have-leaked-sensitive-customer-information

[Source](https://portswigger.net/daily-swig/data-breach-at-us-restaurant-and-gambling-chain-dottys-may-have-leaked-sensitive-customer-information){:target="_blank" rel="noopener"}

> Nevada-based hospitality firm confirms cyber-attack on its networks [...]
