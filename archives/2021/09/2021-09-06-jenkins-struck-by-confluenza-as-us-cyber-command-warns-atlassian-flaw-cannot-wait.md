Title: Jenkins struck by 'Confluenza' as US Cyber Command warns Atlassian flaw 'cannot wait'
Date: 2021-09-06T13:51:56+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2021-09-06-jenkins-struck-by-confluenza-as-us-cyber-command-warns-atlassian-flaw-cannot-wait

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/06/jenkins_confluence_compromised/){:target="_blank" rel="noopener"}

> How was your weekend? Got some patching done? The Jenkins team issued a reminder over the weekend that one should keep one's systems patched as it found itself with a compromised Confluence service.... [...]
