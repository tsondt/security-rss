Title: Confessions of a ransomware negotiator: Well, somebody's got to talk to the criminals holding data hostage
Date: 2021-09-03T10:28:13+00:00
Author: Dominic Connor
Category: The Register
Tags: 
Slug: 2021-09-03-confessions-of-a-ransomware-negotiator-well-somebodys-got-to-talk-to-the-criminals-holding-data-hostage

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/03/how_to_be_a_ransomware/){:target="_blank" rel="noopener"}

> We can't deny people are paying up left, right, and centre... Interview Many people outside of IT believe computers will do away with jobs, but the current ransomware plague shows that new and more curious kinds of jobs are created at least as fast. So what sort of background sets you up to talk to people holding your data for ransom?... [...]
