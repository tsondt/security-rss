Title: Admin of DDoS service behind 200,000 attacks faces 35yrs in prison
Date: 2021-09-17T15:48:39-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-17-admin-of-ddos-service-behind-200000-attacks-faces-35yrs-in-prison

[Source](https://www.bleepingcomputer.com/news/security/admin-of-ddos-service-behind-200-000-attacks-faces-35yrs-in-prison/){:target="_blank" rel="noopener"}

> At the end of a nine-day trial, a jury in California this week found guilty the administrator of two distributed denial-of-service (DDoS) operations. [...]
