Title: Device ‘breakage’ concerns persist days before Let’s Encrypt root cert expiry
Date: 2021-09-22T16:07:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-22-device-breakage-concerns-persist-days-before-lets-encrypt-root-cert-expiry

[Source](https://portswigger.net/daily-swig/device-breakage-concerns-persist-days-before-lets-encrypt-root-cert-expiry){:target="_blank" rel="noopener"}

> Many devices and systems may not be ready for the switchover [...]
