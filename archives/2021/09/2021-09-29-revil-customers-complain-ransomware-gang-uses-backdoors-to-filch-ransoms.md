Title: REvil customers complain ransomware gang uses backdoors to filch ransoms
Date: 2021-09-29T06:04:03+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-29-revil-customers-complain-ransomware-gang-uses-backdoors-to-filch-ransoms

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/29/revil_customers_complain_about_backdoors/){:target="_blank" rel="noopener"}

> There is no honour among thieves Security intelligence vendor Flashpoint claims to have found forum comments from customers of the REvil ransomware-as-a-service gang, and they’re not happy. The gang's malware may contain backdoors that REvil uses to restore encrypted files itself.... [...]
