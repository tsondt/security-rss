Title: FBI: $113 million lost to online romance scams this year
Date: 2021-09-16T12:54:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-16-fbi-113-million-lost-to-online-romance-scams-this-year

[Source](https://www.bleepingcomputer.com/news/security/fbi-113-million-lost-to-online-romance-scams-this-year/){:target="_blank" rel="noopener"}

> The FBI warned today that a massive spike of online romance scams this year caused Americans to lose more than $113 million since the start of 2021. [...]
