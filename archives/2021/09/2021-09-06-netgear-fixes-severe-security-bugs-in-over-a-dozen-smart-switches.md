Title: Netgear fixes severe security bugs in over a dozen smart switches
Date: 2021-09-06T09:07:14-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-06-netgear-fixes-severe-security-bugs-in-over-a-dozen-smart-switches

[Source](https://www.bleepingcomputer.com/news/security/netgear-fixes-severe-security-bugs-in-over-a-dozen-smart-switches/){:target="_blank" rel="noopener"}

> Netgear has released firmware updates for more than a dozen of its smart switches used on corporate networks to address high-severity vulnerabilities. [...]
