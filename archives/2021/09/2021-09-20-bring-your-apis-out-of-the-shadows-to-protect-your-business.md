Title: Bring Your APIs Out of the Shadows to Protect Your Business
Date: 2021-09-20T13:00:24+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Sponsored;Web Security
Slug: 2021-09-20-bring-your-apis-out-of-the-shadows-to-protect-your-business

[Source](https://threatpost.com/apis-out-of-shadows-protect-your-business/169334/){:target="_blank" rel="noopener"}

> APIs are immensely more complex to secure. Shadow APIs—those unknown or forgotten API endpoints that escape the attention and protection of IT¬—present a real risk to your business. Learn how to identify shadow APIs and take control of them before attackers do. [...]
