Title: Apple tried to patch this security hole in macOS Finder but didn't consider upper and lowercase characters
Date: 2021-09-22T23:07:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-22-apple-tried-to-patch-this-security-hole-in-macos-finder-but-didnt-consider-upper-and-lowercase-characters

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/22/macos_rce_flaw/){:target="_blank" rel="noopener"}

> file:// is blocked? Oh OK, we'll just use File:// or fiLE://... Apple's macOS Finder application is currently vulnerable to a remote code execution bug, despite an apparent attempt to fix the problem.... [...]
