Title: Bitcoin.org hack nets giveaway scammers $17,000 overnight
Date: 2021-09-24T13:28:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-24-bitcoinorg-hack-nets-giveaway-scammers-17000-overnight

[Source](https://portswigger.net/daily-swig/bitcoin-org-hack-nets-giveaway-scammers-17-000-overnight){:target="_blank" rel="noopener"}

> Open source project back online after fraudsters dangled double-your-money lure [...]
