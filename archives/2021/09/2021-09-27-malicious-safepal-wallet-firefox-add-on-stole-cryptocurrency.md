Title: Malicious 'Safepal Wallet' Firefox add-on stole cryptocurrency
Date: 2021-09-27T07:21:29-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-09-27-malicious-safepal-wallet-firefox-add-on-stole-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/security/malicious-safepal-wallet-firefox-add-on-stole-cryptocurrency/){:target="_blank" rel="noopener"}

> A malicious Firefox add-on named "Safepal Wallet" lived on the Mozilla add-ons site for seven months and scammed users by emptying out their wallets. Safepal is a cryptocurrency wallet application capable of securely storing a variety of crypto assets, including Bitcoin, Ethereum, and Litecoin. [...]
