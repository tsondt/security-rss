Title: Suex to be you: Feds sanction cryptocurrency exchange for handling payments from 8+ ransomware variants
Date: 2021-09-21T19:59:23+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-21-suex-to-be-you-feds-sanction-cryptocurrency-exchange-for-handling-payments-from-8-ransomware-variants

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/21/feds_sanction_suex/){:target="_blank" rel="noopener"}

> Russia-based biz targeted in Uncle Sam's crack down on cyber-extortion The US Treasury on Tuesday sanctioned virtual cryptocurrency exchange Suex OTC for handling financial transactions for ransomware operators, an intervention that's part of a broad US government effort to disrupt online extortion and related cyber-crime.... [...]
