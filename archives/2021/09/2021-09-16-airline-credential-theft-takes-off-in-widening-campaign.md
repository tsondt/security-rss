Title: Airline Credential-Theft Takes Off in Widening Campaign
Date: 2021-09-16T18:26:59+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Malware;Web Security
Slug: 2021-09-16-airline-credential-theft-takes-off-in-widening-campaign

[Source](https://threatpost.com/airline-credential-theft-campaign/174264/){:target="_blank" rel="noopener"}

> A spyware effort bent on stealing cookies and logins is being driven by unsophisticated attackers cashing in on the initial-access-broker boom. [...]
