Title: Zoom's $15bn merger with Five9 probed by Uncle Sam for national security risks
Date: 2021-09-22T23:46:07+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2021-09-22-zooms-15bn-merger-with-five9-probed-by-uncle-sam-for-national-security-risks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/22/zooms_15bn_merger_with_five9/){:target="_blank" rel="noopener"}

> Vid-chat giant's ties to China under the microscope by AG-led panel Zoom’s ties to China are at the center of a US government investigation into the video-conferencing giant's $15bn plan to take over Five9, a California call-center-in-the-cloud.... [...]
