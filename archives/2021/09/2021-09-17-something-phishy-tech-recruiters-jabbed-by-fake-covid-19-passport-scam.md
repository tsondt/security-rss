Title: Something phishy: Tech recruiters jabbed by fake COVID-19 Passport scam
Date: 2021-09-17T16:42:06+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-09-17-something-phishy-tech-recruiters-jabbed-by-fake-covid-19-passport-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/17/tech_recruiters_get_jabbed_by/){:target="_blank" rel="noopener"}

> Tells clients it is tackling the issue An IT recruitment agency says a "phishing scam" is behind a fake email sent to its customers with details on how to apply for a "Coronavirus Digital Passport."... [...]
