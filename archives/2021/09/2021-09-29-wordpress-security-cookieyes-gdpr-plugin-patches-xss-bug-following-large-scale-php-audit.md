Title: WordPress security: CookieYes GDPR plugin patches XSS bug following large-scale PHP audit
Date: 2021-09-29T12:24:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-29-wordpress-security-cookieyes-gdpr-plugin-patches-xss-bug-following-large-scale-php-audit

[Source](https://portswigger.net/daily-swig/wordpress-security-cookieyes-gdpr-plugin-patches-xss-bug-following-large-scale-php-audit){:target="_blank" rel="noopener"}

> Researchers claim five plugins use extract() function insecurely – but some maintainers disagree [...]
