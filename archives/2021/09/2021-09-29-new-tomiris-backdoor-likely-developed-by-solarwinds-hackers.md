Title: New Tomiris backdoor likely developed by SolarWinds hackers
Date: 2021-09-29T12:09:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-29-new-tomiris-backdoor-likely-developed-by-solarwinds-hackers

[Source](https://www.bleepingcomputer.com/news/security/new-tomiris-backdoor-likely-developed-by-solarwinds-hackers/){:target="_blank" rel="noopener"}

> Kaspersky security researchers have discovered a new backdoor likely developed by the Nobelium hacking group behind last year's SolarWinds supply chain attack. [...]
