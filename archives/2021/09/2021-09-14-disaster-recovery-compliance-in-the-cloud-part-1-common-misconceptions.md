Title: Disaster recovery compliance in the cloud, part 1: Common misconceptions
Date: 2021-09-14T15:39:37+00:00
Author: Dan MacKay
Category: AWS Security
Tags: Finance and Investment;Security, Identity, & Compliance;Asset management;banking;Business continuity;Canada;Compliance;data localization;data residency;data sovereignty;Disaster Recovery;Financial Services;operational resilience;OSFI;PIPEDA;Privacy;Reliability;Resiliency;Security Blog;Wealth management
Slug: 2021-09-14-disaster-recovery-compliance-in-the-cloud-part-1-common-misconceptions

[Source](https://aws.amazon.com/blogs/security/disaster-recovery-compliance-in-the-cloud-part-1-common-misconceptions/){:target="_blank" rel="noopener"}

> Compliance in the cloud can seem challenging, especially for organizations in heavily regulated sectors such as financial services. Regulated financial institutions (FIs) must comply with laws and regulations (often in multiple jurisdictions), global security standards, their own corporate policies, and even contractual obligations with their customers and counterparties. These various compliance requirements may impose constraints [...]
