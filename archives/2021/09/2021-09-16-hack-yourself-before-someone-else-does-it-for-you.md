Title: Hack yourself before someone else does it for you
Date: 2021-09-16T18:00:07+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2021-09-16-hack-yourself-before-someone-else-does-it-for-you

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/16/keysight_breach_and_attack_simulation/){:target="_blank" rel="noopener"}

> Breach and attack simulation tools help you raise your game, Keysight says Sponsored Stop me if you’ve heard this before, but something appears to be amiss with cybersecurity. The spectacular success of ransomware is only the latest and worst example, a phenomenon in which small groups of often barely technically literate attackers ransack some of the biggest and best resourced companies on earth for easy money.... [...]
