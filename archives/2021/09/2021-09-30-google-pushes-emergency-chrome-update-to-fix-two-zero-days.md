Title: Google pushes emergency Chrome update to fix two zero-days
Date: 2021-09-30T16:25:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Google
Slug: 2021-09-30-google-pushes-emergency-chrome-update-to-fix-two-zero-days

[Source](https://www.bleepingcomputer.com/news/security/google-pushes-emergency-chrome-update-to-fix-two-zero-days/){:target="_blank" rel="noopener"}

> Google has released Chrome 94.0.4606.71 for Windows, Mac, and Linux, to fix two zero-day vulnerabilities that have been exploited by attackers. [...]
