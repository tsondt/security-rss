Title: New Zealand DDoS wave targets banks, post offices, weather forecasters and more
Date: 2021-09-08T19:36:35+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-08-new-zealand-ddos-wave-targets-banks-post-offices-weather-forecasters-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/08/new_zealand_ddos_attacks_widespread/){:target="_blank" rel="noopener"}

> Nobody from government will say a word about who's behind it Banks and post offices in New Zealand have been hit by a cyber offensive, according to reports, consisting of sustained DDoS attacks against a number of critical online services.... [...]
