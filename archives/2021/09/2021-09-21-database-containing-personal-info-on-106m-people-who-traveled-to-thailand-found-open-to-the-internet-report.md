Title: Database containing personal info on 106m people who traveled to Thailand found open to the internet – report
Date: 2021-09-21T23:36:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-09-21-database-containing-personal-info-on-106m-people-who-traveled-to-thailand-found-open-to-the-internet-report

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/21/thailand_traveler_info/){:target="_blank" rel="noopener"}

> Misconfigured Elasticsearch server blamed A database containing personal information on 106 million international travelers to Thailand was exposed to the public internet this year, a Brit biz claimed this week.... [...]
