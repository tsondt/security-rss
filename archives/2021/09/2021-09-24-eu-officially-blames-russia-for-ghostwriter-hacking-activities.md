Title: EU officially blames Russia for 'Ghostwriter' hacking activities
Date: 2021-09-24T12:11:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-24-eu-officially-blames-russia-for-ghostwriter-hacking-activities

[Source](https://www.bleepingcomputer.com/news/security/eu-officially-blames-russia-for-ghostwriter-hacking-activities/){:target="_blank" rel="noopener"}

> The European Union has officially linked Russia to a hacking operation known as Ghostwriter that targets high-profile EU officials, journalists, and the general public. [...]
