Title: Manufacturing industry must limit internal data access to prevent sensitive leaks – report
Date: 2021-09-16T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-16-manufacturing-industry-must-limit-internal-data-access-to-prevent-sensitive-leaks-report

[Source](https://portswigger.net/daily-swig/manufacturing-industry-must-limit-internal-data-access-to-prevent-sensitive-leaks-report){:target="_blank" rel="noopener"}

> Sector advised to monitor what employees can do on company networks [...]
