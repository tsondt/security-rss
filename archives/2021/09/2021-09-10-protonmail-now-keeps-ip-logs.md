Title: ProtonMail Now Keeps IP Logs
Date: 2021-09-10T11:10:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;anonymity;courts;data collection;data protection;e-mail;privacy
Slug: 2021-09-10-protonmail-now-keeps-ip-logs

[Source](https://www.schneier.com/blog/archives/2021/09/protonmail-now-keeps-ip-logs.html){:target="_blank" rel="noopener"}

> After being compelled by a Swiss court to monitor IP logs for a particular user, ProtonMail no longer claims that “we do not keep any IP logs.” [...]
