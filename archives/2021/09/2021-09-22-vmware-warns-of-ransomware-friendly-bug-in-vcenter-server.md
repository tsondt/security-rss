Title: VMware Warns of Ransomware-Friendly Bug in vCenter Server
Date: 2021-09-22T16:17:33+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2021-09-22-vmware-warns-of-ransomware-friendly-bug-in-vcenter-server

[Source](https://threatpost.com/vmware-ransomware-bug-vcenter-server/174901/){:target="_blank" rel="noopener"}

> VMware urged immediate patching of the max-severity, arbitrary file upload flaw in Analytics service, which affects all appliances running default 6.5, 6.7 and 7.0 installs. [...]
