Title: NSA: We 'don't know when or even if' a quantum computer will ever be able to break today's public-key encryption
Date: 2021-09-01T18:21:31+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-01-nsa-we-dont-know-when-or-even-if-a-quantum-computer-will-ever-be-able-to-break-todays-public-key-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/01/nsa_quantum_computing_faq/){:target="_blank" rel="noopener"}

> Then again, it would say that America's National Security Agency has published an FAQ about quantum cryptography, saying it does not know "when or even if" a quantum computer will ever exist to "exploit" public-key cryptography.... [...]
