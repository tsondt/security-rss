Title: ASUS patches ROG Armoury Crate app after researcher spots all-too-common flaw
Date: 2021-09-28T18:31:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-28-asus-patches-rog-armoury-crate-app-after-researcher-spots-all-too-common-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/28/asus_rog_armoury_crate_security_flaw_patched/){:target="_blank" rel="noopener"}

> It tries to load a file from a location any old user can write to A flaw in ASUS's ROG Armoury Crate hardware management app could have allowed low-privileged users to execute code as administrator.... [...]
