Title: Microsoft's end-of-summer software security cleanse crushes more than 80 bugs
Date: 2021-09-15T00:00:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-15-microsofts-end-of-summer-software-security-cleanse-crushes-more-than-80-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/15/microsoft_endofsummer_cleanse_crushes_more/){:target="_blank" rel="noopener"}

> Patch Tuesday fiesta also sees Adobe and SAP tidying up Patch Tuesday For its September Patch Tuesday, Microsoft churned out fixes for 66 vulnerabilities, alongside 20 Chromium bugs in Microsoft Edge.... [...]
