Title: Gift Card Gang Extracts Cash From 100k Inboxes Daily
Date: 2021-09-02T16:40:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Web Fraud 2.0;gift card fraud;Gift Card Gang;IMAP;microsoft
Slug: 2021-09-02-gift-card-gang-extracts-cash-from-100k-inboxes-daily

[Source](https://krebsonsecurity.com/2021/09/gift-card-gang-extracts-cash-from-100k-inboxes-daily/){:target="_blank" rel="noopener"}

> Some of the most successful and lucrative online scams employ a “low-and-slow” approach — avoiding detection or interference from researchers and law enforcement agencies by stealing small bits of cash from many people over an extended period. Here’s the story of a cybercrime group that compromises up to 100,000 email inboxes per day, and apparently does little else with this access except siphon gift card and customer loyalty program data that can be resold online. The data in this story come from a trusted source in the security industry who has visibility into a network of hacked machines that fraudsters [...]
