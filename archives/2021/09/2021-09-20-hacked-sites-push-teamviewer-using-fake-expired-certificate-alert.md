Title: Hacked sites push TeamViewer using fake expired certificate alert
Date: 2021-09-20T16:15:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-09-20-hacked-sites-push-teamviewer-using-fake-expired-certificate-alert

[Source](https://www.bleepingcomputer.com/news/security/hacked-sites-push-teamviewer-using-fake-expired-certificate-alert/){:target="_blank" rel="noopener"}

> Threat actors are compromising Windows IIS servers to add expired certificate notification pages that prompt visitors to download a malicious fake installer. [...]
