Title: Microsoft fixes bug letting hackers take over Azure containers
Date: 2021-09-09T11:08:22-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-09-microsoft-fixes-bug-letting-hackers-take-over-azure-containers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-bug-letting-hackers-take-over-azure-containers/){:target="_blank" rel="noopener"}

> Microsoft has fixed a vulnerability in Azure Container Instances called Azurescape that allowed a malicious container to take over containers belonging to other customers on the platform. [...]
