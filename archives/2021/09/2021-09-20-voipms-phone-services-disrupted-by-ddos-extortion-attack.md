Title: VoIP.ms phone services disrupted by DDoS extortion attack
Date: 2021-09-20T11:39:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-20-voipms-phone-services-disrupted-by-ddos-extortion-attack

[Source](https://www.bleepingcomputer.com/news/security/voipms-phone-services-disrupted-by-ddos-extortion-attack/){:target="_blank" rel="noopener"}

> Threat actors are targeting voice-over-Internet provider VoIP.ms with a DDoS attack and extorting the company to stop the assault that's severely disrupting the company's operation. [...]
