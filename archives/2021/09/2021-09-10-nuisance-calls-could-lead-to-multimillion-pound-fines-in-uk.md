Title: Nuisance calls could lead to multimillion-pound fines in UK
Date: 2021-09-10T10:08:37+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: Data protection;GDPR;Telecoms;Data and computer security;Technology;Business;UK news;Law
Slug: 2021-09-10-nuisance-calls-could-lead-to-multimillion-pound-fines-in-uk

[Source](https://www.theguardian.com/technology/2021/sep/10/nuisance-calls-could-lead-to-multimillion-pound-fines-in-uk){:target="_blank" rel="noopener"}

> Ministers considering bringing punishment in line with GDPR, which can issue fine of up to £17.5m Multimillion-pound fines could be imposed for nuisance or fraudulent calls and texts under a proposed overhaul of the UK’s data rules. Companies behind nuisance communications can be fined £500,000 by the Information Commissioner’s Office (ICO) but ministers are considering bringing the punishment in line with General Data Protection Regulation (GDPR), which can issue a fine of up to £17.5m or 4% of global turnover. Continue reading... [...]
