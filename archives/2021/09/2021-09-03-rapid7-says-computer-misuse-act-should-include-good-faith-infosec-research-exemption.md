Title: Rapid7 says Computer Misuse Act should include 'good faith' infosec research exemption
Date: 2021-09-03T15:16:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-03-rapid7-says-computer-misuse-act-should-include-good-faith-infosec-research-exemption

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/03/rapid7_computer_misuse_act_reform_plans/){:target="_blank" rel="noopener"}

> Security biz publishes plans for law reforms Infosec firm Rapid7 has joined the chorus of voices urging reform to the UK's Computer Misuse Act, publishing its detailed proposals intended to change the cobwebby old law for the better.... [...]
