Title: Fake Amnesty International Pegasus scanner used to infect Windows
Date: 2021-09-30T15:32:05-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-30-fake-amnesty-international-pegasus-scanner-used-to-infect-windows

[Source](https://www.bleepingcomputer.com/news/security/fake-amnesty-international-pegasus-scanner-used-to-infect-windows/){:target="_blank" rel="noopener"}

> Threat actors are trying to capitalize on the recent revelations on Pegasus spyware from Amnesty International to drop a less-known remote access tool called Sarwent. [...]
