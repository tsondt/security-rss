Title: Latest FinFisher spyware upgrades 'particularly worrying,' says Kaspersky
Date: 2021-09-28T15:50:06+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: 2021-09-28-latest-finfisher-spyware-upgrades-particularly-worrying-says-kaspersky

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/28/kasperky_finfisher_spyware_report/){:target="_blank" rel="noopener"}

> Eight-month analysis finds four-layer obfuscation, two-stage loader, and a new UEFI attack Kaspersky has presented the findings of an eight-month probe into the FinFisher spyware toolset – including the discovery of a UEFI "bootkit" infection method and "advanced anti-analysis methods" such as "four-layer obfuscation."... [...]
