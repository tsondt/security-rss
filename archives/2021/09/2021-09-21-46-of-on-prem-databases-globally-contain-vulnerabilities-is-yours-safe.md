Title: 46% of On-Prem Databases Globally Contain Vulnerabilities: Is Yours Safe?
Date: 2021-09-21T13:00:56+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Sponsored;Web Security
Slug: 2021-09-21-46-of-on-prem-databases-globally-contain-vulnerabilities-is-yours-safe

[Source](https://threatpost.com/46-on-prem-databases-globally-contain-vulnerabilities/174815/){:target="_blank" rel="noopener"}

> Are organizations neglecting the security of their data? An unprecedented five-year study reveals that internal databases are riddled with vulnerabilities – some even years old. [...]
