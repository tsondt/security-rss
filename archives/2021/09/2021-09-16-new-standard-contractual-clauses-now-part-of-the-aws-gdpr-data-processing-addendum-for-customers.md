Title: New Standard Contractual Clauses now part of the AWS GDPR Data Processing Addendum for customers
Date: 2021-09-16T22:51:05+00:00
Author: Stéphane Ducable
Category: AWS Security
Tags: Announcements;Europe;Foundational (100);Security, Identity, & Compliance;Compliance;GDPR;Security Blog
Slug: 2021-09-16-new-standard-contractual-clauses-now-part-of-the-aws-gdpr-data-processing-addendum-for-customers

[Source](https://aws.amazon.com/blogs/security/new-standard-contractual-clauses-now-part-of-the-aws-gdpr-data-processing-addendum-for-customers/){:target="_blank" rel="noopener"}

> Today, we’re happy to announce an update to our online AWS GDPR Data Processing Addendum (AWS GDPR DPA) and our online Service Terms to include the new Standard Contractual Clauses (SCCs) that the European Commission (EC) adopted in June 2021. The EC-approved SCCs give our customers the ability to comply with the General Data Protection [...]
