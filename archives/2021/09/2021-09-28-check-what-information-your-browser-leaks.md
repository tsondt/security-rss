Title: Check What Information Your Browser Leaks
Date: 2021-09-28T14:51:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;browsers;leaks
Slug: 2021-09-28-check-what-information-your-browser-leaks

[Source](https://www.schneier.com/blog/archives/2021/09/check-what-information-your-browser-leaks.html){:target="_blank" rel="noopener"}

> These two sites tell you what sorts of information you’re leaking from your browser. [...]
