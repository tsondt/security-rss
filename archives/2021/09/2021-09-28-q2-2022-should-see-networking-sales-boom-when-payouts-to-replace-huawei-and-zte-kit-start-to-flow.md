Title: Q2 2022 should see networking sales boom – when payouts to replace Huawei and ZTE kit start to flow
Date: 2021-09-28T06:45:33+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-28-q2-2022-should-see-networking-sales-boom-when-payouts-to-replace-huawei-and-zte-kit-start-to-flow

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/28/fcc_huawei_zte_reimbursements/){:target="_blank" rel="noopener"}

> Netadmins need not assume they can't take a break, as supply chain hassles may persist How much of a national security risk does the USA think Huawei and ZTE pose?... [...]
