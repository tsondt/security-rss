Title: Thousands of internet-connected databases contain high or critical CVEs, says report by cloud security biz
Date: 2021-09-14T11:30:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-14-thousands-of-internet-connected-databases-contain-high-or-critical-cves-says-report-by-cloud-security-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/14/imperva_12k_database_vuln_report/){:target="_blank" rel="noopener"}

> Put your data on someone else's computer to keep it safe, urges Imperva After spending five years poring over port scan results, infosec firm Imperva reckons there's about 12,000 vulnerability-containing databases accessible through the internet.... [...]
