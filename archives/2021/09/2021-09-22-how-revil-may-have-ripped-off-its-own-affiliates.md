Title: How REvil May Have Ripped Off Its Own Affiliates
Date: 2021-09-22T16:50:34+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2021-09-22-how-revil-may-have-ripped-off-its-own-affiliates

[Source](https://threatpost.com/how-revil-may-have-ripped-off-its-own-affiliates/174887/){:target="_blank" rel="noopener"}

> A newly discovered backdoor and double chats could have enabled REvil ransomware-as-a-service operators to hijack victim cases and snatch affiliates’ cuts of ransom payments. [...]
