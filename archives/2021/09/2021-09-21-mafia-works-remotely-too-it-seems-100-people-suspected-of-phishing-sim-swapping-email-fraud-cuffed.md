Title: Mafia works remotely, too, it seems: 100+ people suspected of phishing, SIM swapping, email fraud cuffed
Date: 2021-09-21T05:16:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-09-21-mafia-works-remotely-too-it-seems-100-people-suspected-of-phishing-sim-swapping-email-fraud-cuffed

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/21/europol_arrests/){:target="_blank" rel="noopener"}

> Dare we say, these Euro cops ran mobprobe Police arrested 106 people suspected of carrying out online fraud for an organized crime gang linked to the Italian Mafia, Europol said on Monday.... [...]
