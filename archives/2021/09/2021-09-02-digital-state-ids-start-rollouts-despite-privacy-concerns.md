Title: Digital State IDs Start Rollouts Despite Privacy Concerns
Date: 2021-09-02T11:28:29+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Mobile Security;Privacy
Slug: 2021-09-02-digital-state-ids-start-rollouts-despite-privacy-concerns

[Source](https://threatpost.com/digital-state-ids-rollouts-privacy/169136/){:target="_blank" rel="noopener"}

> Eight states are introducing drivers licenses and identification cards available for use on Apple iPhones and Watches, but critics warn about the dangers of eliminating the use of a paper-based system entirely. [...]
