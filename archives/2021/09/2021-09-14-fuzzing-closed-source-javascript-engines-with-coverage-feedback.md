Title: Fuzzing Closed-Source JavaScript Engines with Coverage Feedback
Date: 2021-09-14T10:14:00-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2021-09-14-fuzzing-closed-source-javascript-engines-with-coverage-feedback

[Source](https://googleprojectzero.blogspot.com/2021/09/fuzzing-closed-source-javascript.html){:target="_blank" rel="noopener"}

> Posted by Ivan Fratric, Project Zero tl;dr I combined Fuzzilli (an open-source JavaScript engine fuzzer), with TinyInst (an open-source dynamic instrumentation library for fuzzing). I also added grammar-based mutation support to Jackalope (my black-box binary fuzzer). So far, these two approaches resulted in finding three security issues in jscript9.dll (default JavaScript engine used by Internet Explorer). Introduction or “when you can’t beat them, join them” In the past, I’ve invested a lot of time in generation-based fuzzing, which was a successful way to find vulnerabilities in various targets, especially those that take some form of language as input. For example, [...]
