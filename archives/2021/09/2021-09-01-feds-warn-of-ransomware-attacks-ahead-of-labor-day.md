Title: Feds Warn of Ransomware Attacks Ahead of Labor Day
Date: 2021-09-01T12:17:17+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware
Slug: 2021-09-01-feds-warn-of-ransomware-attacks-ahead-of-labor-day

[Source](https://threatpost.com/ransomware-attacks-labor-day/169087/){:target="_blank" rel="noopener"}

> Threat actors recently have used long holiday weekends -- when many staff are taking time off -- as a prime opportunity to ambush organizations. [...]
