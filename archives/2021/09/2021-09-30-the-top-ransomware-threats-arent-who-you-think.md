Title: The Top Ransomware Threats Aren’t Who You Think
Date: 2021-09-30T12:50:55+00:00
Author: Becky Bracken
Category: Threatpost
Tags: News
Slug: 2021-09-30-the-top-ransomware-threats-arent-who-you-think

[Source](https://threatpost.com/the-top-ransomware-threats-arent-who-you-think/175164/){:target="_blank" rel="noopener"}

> Move over REvil, Ragnar Locker, BlackMatter, Conti et al: Three lesser-known gangs account for the vast majority of ransomware attacks in the U.S. and globally. [...]
