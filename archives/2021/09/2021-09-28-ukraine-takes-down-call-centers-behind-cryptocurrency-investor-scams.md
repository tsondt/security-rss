Title: Ukraine takes down call centers behind cryptocurrency investor scams
Date: 2021-09-28T09:25:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-09-28-ukraine-takes-down-call-centers-behind-cryptocurrency-investor-scams

[Source](https://www.bleepingcomputer.com/news/security/ukraine-takes-down-call-centers-behind-cryptocurrency-investor-scams/){:target="_blank" rel="noopener"}

> The Security Service of Ukraine (SBU) has taken down a network of six call centers in Lviv, used by a ring of scammers to defraud cryptocurrency and stock market investors worldwide. [...]
