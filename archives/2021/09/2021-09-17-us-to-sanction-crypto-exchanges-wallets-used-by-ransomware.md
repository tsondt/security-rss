Title: U.S. to sanction crypto exchanges, wallets used by ransomware
Date: 2021-09-17T17:49:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government
Slug: 2021-09-17-us-to-sanction-crypto-exchanges-wallets-used-by-ransomware

[Source](https://www.bleepingcomputer.com/news/security/us-to-sanction-crypto-exchanges-wallets-used-by-ransomware/){:target="_blank" rel="noopener"}

> The Biden administration is expected to issue sanctions against crypto exchanges, wallets, and traders used by ransomware gangs to convert ransom payments into fiat money. [...]
