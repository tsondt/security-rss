Title: ExpressVPN bought for $1bn by Brit biz with an intriguing history in adware
Date: 2021-09-14T07:39:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-09-14-expressvpn-bought-for-1bn-by-brit-biz-with-an-intriguing-history-in-adware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/14/expressvpn_bought_kape/){:target="_blank" rel="noopener"}

> 'Kape has moved on from those times' UK-headquartered Kape Technologies announced on Monday it has acquired ExpressVPN in a $936m (£675m) cash and stocks deal, a move it claims will double its customer base to at least six million.... [...]
