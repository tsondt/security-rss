Title: Google Report Spotlights Uptick in Controversial ‘Geofence Warrants’ by Police
Date: 2021-09-23T13:16:25+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Mobile Security;Privacy
Slug: 2021-09-23-google-report-spotlights-uptick-in-controversial-geofence-warrants-by-police

[Source](https://threatpost.com/google-controversial-geofence-warrants/174938/){:target="_blank" rel="noopener"}

> Digital privacy rights defenders contend that geofencing warrants grab data on everyone near a crime, without cause. [...]
