Title: Upcoming Speaking Engagements
Date: 2021-09-14T17:02:14+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2021-09-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2021/09/upcoming-speaking-engagements-12.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m keynoting CIISec Live —an all-online event—September 15-16, 2021. I’m speaking at the Infosecurity Magazine EMEA Autumn Online Summit on September 21, 2021. I’m speaking at the Cybersecurity and Data Privacy Law Conference in Plano, Texas, USA, September 22-23, 2021. I’m speaking at the fourth annual Managing Cyber Risk from the C-Suite conference—a virtual event conducted through Webex—on October 5, 2021. I’ll be speaking at an Informa event on November 29, 2021. Details to come. The list is maintained on this page. [...]
