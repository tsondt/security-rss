Title: How US federal agencies can use AWS to encrypt data at rest and in transit
Date: 2021-09-10T18:22:42+00:00
Author: Robert George
Category: AWS Security
Tags: AWS Key Management Service;Federal;Government;Intermediate (200);Public Sector;Security, Identity, & Compliance;ACM;AWS KMS;Cybersecurity EO;Encryption;Executive order;Security Blog
Slug: 2021-09-10-how-us-federal-agencies-can-use-aws-to-encrypt-data-at-rest-and-in-transit

[Source](https://aws.amazon.com/blogs/security/how-us-federal-agencies-can-use-aws-to-encrypt-data-at-rest-and-in-transit/){:target="_blank" rel="noopener"}

> This post is part of a series about how Amazon Web Services (AWS) can help your US federal agency meet the requirements of the President’s Executive Order on Improving the Nation’s Cybersecurity. You will learn how you can use AWS information security practices to meet the requirement to encrypt your data at rest and in [...]
