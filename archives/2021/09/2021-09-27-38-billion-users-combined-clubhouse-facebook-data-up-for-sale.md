Title: 3.8 Billion Users’ Combined Clubhouse, Facebook Data Up for Sale
Date: 2021-09-27T14:59:58+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Web Security
Slug: 2021-09-27-38-billion-users-combined-clubhouse-facebook-data-up-for-sale

[Source](https://threatpost.com/clubhouse-facebook-data-sale/175023/){:target="_blank" rel="noopener"}

> Combined cache of data likely to fuel rash of account takeover, smishing attacks, experts warn. [...]
