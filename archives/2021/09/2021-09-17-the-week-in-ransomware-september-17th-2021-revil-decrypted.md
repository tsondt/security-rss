Title: The Week in Ransomware - September 17th 2021 - REvil decrypted
Date: 2021-09-17T18:16:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-17-the-week-in-ransomware-september-17th-2021-revil-decrypted

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-17th-2021-revil-decrypted/){:target="_blank" rel="noopener"}

> It has been an interesting week with decryptors released, ransomware gangs continuing to rail against negotiators, and the US government expected to sanction crypto exchanges next week. [...]
