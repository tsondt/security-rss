Title: New Windows security updates break network printing
Date: 2021-09-16T12:08:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-16-new-windows-security-updates-break-network-printing

[Source](https://www.bleepingcomputer.com/news/security/new-windows-security-updates-break-network-printing/){:target="_blank" rel="noopener"}

> Windows administrators report wide-scale network printing problems after installing this week's September 2021 Patch Tuesday security updates. [...]
