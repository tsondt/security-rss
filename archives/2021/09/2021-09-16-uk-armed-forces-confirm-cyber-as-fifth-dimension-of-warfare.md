Title: UK armed forces confirm cyber as fifth dimension of warfare
Date: 2021-09-16T12:01:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-16-uk-armed-forces-confirm-cyber-as-fifth-dimension-of-warfare

[Source](https://portswigger.net/daily-swig/uk-armed-forces-confirm-cyber-as-fifth-dimension-of-warfare){:target="_blank" rel="noopener"}

> Armed forces needs to adapt to recruit more digital quartermasters rather than conventional soldiers, conference attendees told [...]
