Title: UK Ministry of Defence apologises after Afghan interpreters' personal data exposed in email blunder
Date: 2021-09-21T11:30:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-21-uk-ministry-of-defence-apologises-after-afghan-interpreters-personal-data-exposed-in-email-blunder

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/21/mod_email_fail_afghan_interpreters_data/){:target="_blank" rel="noopener"}

> We joke about lethal consequences of failure but this isn't funny The UK's Ministry of Defence has launched an internal investigation after committing the classic CC-instead-of-BCC email error – but with the names and contact details of Afghan interpreters trapped in the Taliban-controlled nation.... [...]
