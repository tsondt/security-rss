Title: Lightning Cable with Embedded Eavesdropping
Date: 2021-09-07T11:14:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;eavesdropping;hacking;key logging;Wi-Fi
Slug: 2021-09-07-lightning-cable-with-embedded-eavesdropping

[Source](https://www.schneier.com/blog/archives/2021/09/lightning-cable-with-embedded-eavesdropping.html){:target="_blank" rel="noopener"}

> Normal-looking cables (USB-C, Lightning, and so on) that exfiltrate data over a wireless network. I blogged about a previous prototype here [...]
