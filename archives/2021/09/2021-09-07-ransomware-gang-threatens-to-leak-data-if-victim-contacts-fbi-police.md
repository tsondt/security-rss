Title: Ransomware gang threatens to leak data if victim contacts FBI, police
Date: 2021-09-07T02:28:34-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-09-07-ransomware-gang-threatens-to-leak-data-if-victim-contacts-fbi-police

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-threatens-to-leak-data-if-victim-contacts-fbi-police/){:target="_blank" rel="noopener"}

> The Ragnar Locker ransomware group is warning that they will leak stolen data from victims that contact law enforcement authorities, like the FBI. Ragnar Locker has previously hit prominent companies with ransomware attacks, demanding millions of dollars in ransom payments. [...]
