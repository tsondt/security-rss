Title: Free REvil ransomware master decrypter released for past victims
Date: 2021-09-16T09:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-16-free-revil-ransomware-master-decrypter-released-for-past-victims

[Source](https://www.bleepingcomputer.com/news/security/free-revil-ransomware-master-decrypter-released-for-past-victims/){:target="_blank" rel="noopener"}

> A free master decryptor for the REvil ransomware operation has been released, allowing all victims encrypted before the gang disappeared to recover their files for free. [...]
