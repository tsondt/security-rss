Title: Europol Breaks Open Extensive Mafia Cybercrime Ring
Date: 2021-09-20T19:50:19+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Web Security
Slug: 2021-09-20-europol-breaks-open-extensive-mafia-cybercrime-ring

[Source](https://threatpost.com/europol-mafia-cybercrime-ring/174838/){:target="_blank" rel="noopener"}

> Organized crime ring thrived on violence, intimidation and $12 million in online fraud profits. [...]
