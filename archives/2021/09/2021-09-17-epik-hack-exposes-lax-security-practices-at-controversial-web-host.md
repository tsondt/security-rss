Title: Epik hack exposes lax security practices at controversial web host
Date: 2021-09-17T13:57:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-17-epik-hack-exposes-lax-security-practices-at-controversial-web-host

[Source](https://portswigger.net/daily-swig/epik-hack-exposes-lax-security-practices-at-controversial-web-host){:target="_blank" rel="noopener"}

> ISP guilty of ‘laziest design possible’, critics allege [...]
