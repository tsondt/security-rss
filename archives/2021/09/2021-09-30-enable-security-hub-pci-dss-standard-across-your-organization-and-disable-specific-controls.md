Title: Enable Security Hub PCI DSS standard across your organization and disable specific controls
Date: 2021-09-30T16:24:27+00:00
Author: Pablo Pagani
Category: AWS Security
Tags: AWS Security Hub;Intermediate (200);Security, Identity, & Compliance;AWS Organizations;Multiple accounts;PCI DSS;Security Blog
Slug: 2021-09-30-enable-security-hub-pci-dss-standard-across-your-organization-and-disable-specific-controls

[Source](https://aws.amazon.com/blogs/security/enable-security-hub-pci-dss-standard-across-your-organization-and-disable-specific-controls/){:target="_blank" rel="noopener"}

> At this time, enabling the PCI DSS standard from within AWS Security Hub enables this compliance framework only within the Amazon Web Services (AWS) account you are presently administering. This blog post showcases a solution that can be used to customize the configuration and deployment of the PCI DSS standard compliance standard using AWS Security [...]
