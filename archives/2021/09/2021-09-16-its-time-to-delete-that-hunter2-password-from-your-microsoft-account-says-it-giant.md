Title: It's time to delete that hunter2 password from your Microsoft account, says IT giant
Date: 2021-09-16T05:58:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2021-09-16-its-time-to-delete-that-hunter2-password-from-your-microsoft-account-says-it-giant

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/16/microsoft_passwordless/){:target="_blank" rel="noopener"}

> And go passwordless, use auth app, keys, Windows Hello, or codes to login From this week, Microsoft won't require you, or your password manager, to come up with strings of letters, numbers, and special characters forming a silly sentence or a reconfiguration of an ex’s name and birthday to access the Windows giant's services.... [...]
