Title: Honing Cybersecurity Strategy When Everyone’s a Target for Ransomware
Date: 2021-09-13T18:17:37+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: InfoSec Insider;Malware;Web Security
Slug: 2021-09-13-honing-cybersecurity-strategy-when-everyones-a-target-for-ransomware

[Source](https://threatpost.com/cybersecurity-strategy-ransomware/169397/){:target="_blank" rel="noopener"}

> Aamir Lakhani, researcher at FortiGuard Labs, explains why organizations must extend cyber-awareness training across the entire enterprise, from Luddites to the C-suite. [...]
