Title: Ethereum dev admits helping North Korea mine crypto-bucks, faces 20 years jail
Date: 2021-09-28T05:44:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-28-ethereum-dev-admits-helping-north-korea-mine-crypto-bucks-faces-20-years-jail

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/28/virgil_griffith_guilty_north_korea/){:target="_blank" rel="noopener"}

> Also advised on how smart contracts could help DPRK in US nuke talks A US citizen has admitted to helping the Democratic People's Republic of Korea (DPRK) to establish cryptocurrency capabilities and faces up to 20 years jail for his actions.... [...]
