Title: UK umbrella payroll firm GiantPay confirms it was hit by 'sophisticated' cyber-attack
Date: 2021-09-28T12:01:11+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-09-28-uk-umbrella-payroll-firm-giantpay-confirms-it-was-hit-by-sophisticated-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/28/giantpay_confirms_cyberattack/){:target="_blank" rel="noopener"}

> Tech contractors fume at lack of info as company says it will 'try' to get them paid by Friday Giant Group, the umbrella company that has thousands of contractors on its books, has been targeted by a "sophisticated" cyber-attack that floored systems and left workers out in the cold, the biz has now confirmed.... [...]
