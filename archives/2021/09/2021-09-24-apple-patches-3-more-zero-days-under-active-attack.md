Title: Apple Patches 3 More Zero-Days Under Active Attack
Date: 2021-09-24T11:29:27+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: 2021-09-24-apple-patches-3-more-zero-days-under-active-attack

[Source](https://threatpost.com/apple-patches-zero-days-attack/174988/){:target="_blank" rel="noopener"}

> One of the bugs, which affects macOS as well as older versions of iPhones, could allow an attacker to execute arbitrary code with kernel privileges. [...]
