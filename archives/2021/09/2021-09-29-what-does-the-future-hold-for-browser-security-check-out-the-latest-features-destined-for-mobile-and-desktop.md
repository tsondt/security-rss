Title: What does the future hold for browser security? Check out the latest features destined for mobile and desktop
Date: 2021-09-29T13:28:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-29-what-does-the-future-hold-for-browser-security-check-out-the-latest-features-destined-for-mobile-and-desktop

[Source](https://portswigger.net/daily-swig/what-does-the-future-hold-for-browser-security-check-out-the-latest-features-destined-for-mobile-and-desktop){:target="_blank" rel="noopener"}

> A rundown of leading web browsers’ privacy and security features – both in place and in the pipeline [...]
