Title: Authorities Arrest Another TrickBot Gang Member in South Korea
Date: 2021-09-07T12:48:58+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks
Slug: 2021-09-07-authorities-arrest-another-trickbot-gang-member-in-south-korea

[Source](https://threatpost.com/authorities-arrest-trickbot-member/169236/){:target="_blank" rel="noopener"}

> A hacker known only as “Mr. A” was picked up by authorities at a South Korean airport after getting stuck in the country due to COVID-19 travel restrictions. [...]
