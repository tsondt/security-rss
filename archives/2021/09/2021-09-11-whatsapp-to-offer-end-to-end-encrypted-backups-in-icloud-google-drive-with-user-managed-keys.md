Title: WhatsApp to offer end-to-end encrypted backups in iCloud, Google Drive with user-managed keys
Date: 2021-09-11T01:21:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-11-whatsapp-to-offer-end-to-end-encrypted-backups-in-icloud-google-drive-with-user-managed-keys

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/11/whatsapp_cloud_encryption/){:target="_blank" rel="noopener"}

> Funny how 'privacy-focused' Apple and Google haven't managed that Facebook's WhatsApp on Friday said users will soon be able to store end-to-end (E2E) encrypted backups of their chat history on Google Drive in Android or Apple iCloud in iOS, with an option to self-manage the encryption key.... [...]
