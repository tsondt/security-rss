Title: LA cops told to harvest social media handles from people they stop, suspect or not
Date: 2021-09-09T03:50:04+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2021-09-09-la-cops-told-to-harvest-social-media-handles-from-people-they-stop-suspect-or-not

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/09/lapd_social_media_monitoring/){:target="_blank" rel="noopener"}

> Policies revealed after long battle for transparency and accountability Los Angeles police are instructed to collect social media details from people they stop and talk to, even if those civilians aren’t suspected of breaking the law, according to documents finally revealed after a lengthy legal battle.... [...]
