Title: OWASP toasts 20th anniversary with revised Top 10 for 2021
Date: 2021-09-27T14:59:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-27-owasp-toasts-20th-anniversary-with-revised-top-10-for-2021

[Source](https://portswigger.net/daily-swig/owasp-toasts-20th-anniversary-with-revised-top-10-for-2021){:target="_blank" rel="noopener"}

> Non-profit confirms latest iteration of web attack hit list during 24-hour live event [...]
