Title: SOVA, Worryingly Sophisticated Android Trojan, Takes Flight
Date: 2021-09-10T16:25:53+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: 2021-09-10-sova-worryingly-sophisticated-android-trojan-takes-flight

[Source](https://threatpost.com/sova-sophisticated-android-trojan/169366/){:target="_blank" rel="noopener"}

> The malware appeared in August with an ambitious roadmap (think ransomware, DDoS) that could make it 'the most feature-rich Android malware on the market.' [...]
