Title: Yandex Pummeled by Potent Meris DDoS Botnet
Date: 2021-09-10T16:31:14+00:00
Author: Tom Spring
Category: Threatpost
Tags: IoT;Vulnerabilities;Web Security
Slug: 2021-09-10-yandex-pummeled-by-potent-meris-ddos-botnet

[Source](https://threatpost.com/yandex-meris-botnet/169368/){:target="_blank" rel="noopener"}

> Record-breaking distributed denial of service attack targets Russia’s version of Google - Yandex. [...]
