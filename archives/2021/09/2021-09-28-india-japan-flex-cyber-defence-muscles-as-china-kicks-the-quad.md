Title: India, Japan flex cyber-defence muscles as China kicks the Quad
Date: 2021-09-28T04:23:52+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-28-india-japan-flex-cyber-defence-muscles-as-china-kicks-the-quad

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/28/india_japan_flex_cyberdefence_muscles/){:target="_blank" rel="noopener"}

> Tokyo's new cyber-security policy names China, Russia, North Korea as sources of increasing threat India and Japan have each flexed their cyber-defence muscles in ways that China can't miss.... [...]
