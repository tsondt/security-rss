Title: Azure Zero-Day Flaws Highlight Lurking Supply-Chain Risk
Date: 2021-09-16T11:37:48+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2021-09-16-azure-zero-day-flaws-highlight-lurking-supply-chain-risk

[Source](https://threatpost.com/azure-zero-day-supply-chain/169508/){:target="_blank" rel="noopener"}

> Dubbed OMIGOD, a series of vulnerabilities in the Open Management Infrastructure used in Azure on Linux demonstrate hidden security threats, researchers said. [...]
