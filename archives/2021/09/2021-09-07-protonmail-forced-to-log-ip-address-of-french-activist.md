Title: ProtonMail Forced to Log IP Address of French Activist
Date: 2021-09-07T16:07:40+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Privacy;Web Security
Slug: 2021-09-07-protonmail-forced-to-log-ip-address-of-french-activist

[Source](https://threatpost.com/protonmail-log-ip-address-french-activist/169242/){:target="_blank" rel="noopener"}

> The privacy-touting, end-to-end encrypted email provider erased its site's “we don’t log your IP” boast after France sicced Swiss cops on it. [...]
