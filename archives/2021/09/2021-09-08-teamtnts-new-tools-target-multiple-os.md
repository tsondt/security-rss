Title: TeamTNT’s New Tools Target Multiple OS
Date: 2021-09-08T17:03:06+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Malware;Web Security
Slug: 2021-09-08-teamtnts-new-tools-target-multiple-os

[Source](https://threatpost.com/teamtnt-target-multiple-os/169279/){:target="_blank" rel="noopener"}

> The attackers are indiscriminately striking thousands of victims worldwide with their new “Chimaera” campaign. [...]
