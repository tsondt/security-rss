Title: Dallas Independent School District reports data breach impacting current and former students, staff
Date: 2021-09-03T12:23:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-03-dallas-independent-school-district-reports-data-breach-impacting-current-and-former-students-staff

[Source](https://portswigger.net/daily-swig/dallas-independent-school-district-reports-data-breach-impacting-current-and-former-students-staff){:target="_blank" rel="noopener"}

> Attacker promises ‘data was not disseminated or sold to anyone’ [...]
