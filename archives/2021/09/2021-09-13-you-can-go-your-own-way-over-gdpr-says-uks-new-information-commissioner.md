Title: You can 'go your own way' over GDPR, says UK's new Information Commissioner
Date: 2021-09-13T09:15:13+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-09-13-you-can-go-your-own-way-over-gdpr-says-uks-new-information-commissioner

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/13/new_uk_ico_promises_to/){:target="_blank" rel="noopener"}

> Tells committee: I know I said Facebook was 'morally bankrupt' but... The incoming head of the UK's data watchdog has "gone on the record" to say he will be fair and impartial in his dealings with tech companies despite once describing Facebook as "morally bankrupt pathological liars."... [...]
