Title: Introducing the Ransomware Risk Management on AWS Whitepaper
Date: 2021-09-28T19:21:53+00:00
Author: Temi Adebambo
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;cybersecurity;Data protection;Incident response;infrastructure security;NIST CSF;ransomware;Risk management;Security Blog;Security controls;threat detection
Slug: 2021-09-28-introducing-the-ransomware-risk-management-on-aws-whitepaper

[Source](https://aws.amazon.com/blogs/security/introducing-the-ransomware-risk-management-on-aws-whitepaper/){:target="_blank" rel="noopener"}

> AWS recently released the Ransomware Risk Management on AWS Using the NIST Cyber Security Framework (CSF) whitepaper. This whitepaper aligns the National Institute of Standards and Technology (NIST) recommendations for security controls that are related to ransomware risk management, for workloads built on AWS. The whitepaper maps the technical capabilities to AWS services and implementation [...]
