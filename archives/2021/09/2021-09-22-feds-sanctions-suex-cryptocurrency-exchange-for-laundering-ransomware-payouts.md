Title: Feds Sanctions SUEX Cryptocurrency Exchange for Laundering Ransomware Payouts
Date: 2021-09-22T14:10:57+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware;Web Security
Slug: 2021-09-22-feds-sanctions-suex-cryptocurrency-exchange-for-laundering-ransomware-payouts

[Source](https://threatpost.com/feds-sanctions-suex-cryptocurrency-ransomware/174895/){:target="_blank" rel="noopener"}

> The action is the first of its kind in the U.S., as the government increases efforts to get a handle on cybercrime. [...]
