Title: Don't look a GriftHorse in the mouth: Trojan trampled 10 million Android devices
Date: 2021-09-29T22:27:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-29-dont-look-a-grifthorse-in-the-mouth-trojan-trampled-10-million-android-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/29/grifthorse_trojan_android/){:target="_blank" rel="noopener"}

> Pushy code pressured people to sign up for premium services, netted 'millions of euros' You may be advised not to look a gift horse in the mouth, lest you appear ungrateful for questioning its health. But you probably want to examine your Android phone for GriftHorse, or rather for any of the 200 or so apps with different names that incorporate the malicious code.... [...]
