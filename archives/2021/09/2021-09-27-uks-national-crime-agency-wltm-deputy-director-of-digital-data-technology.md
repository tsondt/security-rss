Title: UK's National Crime Agency WLTM Deputy Director of Digital Data &amp; Technology
Date: 2021-09-27T14:31:12+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2021-09-27-uks-national-crime-agency-wltm-deputy-director-of-digital-data-technology

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/27/national_crime_agency_hiring/){:target="_blank" rel="noopener"}

> Up to £118,000 and use of Cycle2work scheme for successful applicant Britain's National Crime Agency – charged with thwarting serious and organised crime – is putting out the feelers for a senior figure to head up, among other things, the threat response, analysis, capability exploration and research unit, otherwise known as TRACER.... [...]
