Title: Ransomware gang threatens to wipe decryption key if negotiator hired
Date: 2021-09-15T14:22:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-15-ransomware-gang-threatens-to-wipe-decryption-key-if-negotiator-hired

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-threatens-to-wipe-decryption-key-if-negotiator-hired/){:target="_blank" rel="noopener"}

> The Grief ransomware gang is threatening to delete victim's decryption keys if they hire a negotiation firm, making it impossible to recover encrypted files. [...]
