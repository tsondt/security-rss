Title: When the bits hit the fan: What to do when ransomware strikes
Date: 2021-09-06T10:01:13+00:00
Author: Dominic Connor
Category: The Register
Tags: 
Slug: 2021-09-06-when-the-bits-hit-the-fan-what-to-do-when-ransomware-strikes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/06/what_do_do_when_hit_by_ransomware/){:target="_blank" rel="noopener"}

> Don't trust the insurer's techies, take the blame and other practical tips Feature When I first became a company chief techie, the finance director patronisingly explained the basic asymmetry of prevention vs cure. Spending money on assets to stop an attack come out of capex, but spending after the disaster would be up to the insurer, with premiums deducted out of opex. Also, prevention costs reduced current bonuses.... [...]
