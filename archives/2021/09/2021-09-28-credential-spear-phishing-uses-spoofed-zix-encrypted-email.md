Title: Credential Spear-Phishing Uses Spoofed Zix Encrypted Email
Date: 2021-09-28T10:00:26+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: News
Slug: 2021-09-28-credential-spear-phishing-uses-spoofed-zix-encrypted-email

[Source](https://threatpost.com/credential-spear-phishing-uses-spoofed-zix-encrypted-email/175044/){:target="_blank" rel="noopener"}

> The spoofed email has targeted close to 75K inboxes, slipping past spam and security controls across Office 365, Google Workspace, Exchange, Cisco ESA and more. [...]
