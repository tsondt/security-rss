Title: Apple AirTag Bug Enables ‘Good Samaritan’ Attack
Date: 2021-09-28T15:49:46+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Web Fraud 2.0;Apple AirTag;Ars Technica;Bobby Rauch;Good Samaritan attack;Jim Salter;washington post
Slug: 2021-09-28-apple-airtag-bug-enables-good-samaritan-attack

[Source](https://krebsonsecurity.com/2021/09/apple-airtag-bug-enables-good-samaritan-attack/){:target="_blank" rel="noopener"}

> The new $30 AirTag tracking device from Apple has a feature that allows anyone who finds one of these tiny location beacons to scan it with a mobile phone and discover its owner’s phone number if the AirTag has been set to lost mode. But according to new research, this same feature can be abused to redirect the Good Samaritan to an iCloud phishing page — or to any other malicious website. The AirTag’s “Lost Mode” lets users alert Apple when an AirTag is missing. Setting it to Lost Mode generates a unique URL at https://found.apple.com, and allows the user [...]
