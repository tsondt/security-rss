Title: Human Fraud: Detecting Them Before They Detect You
Date: 2021-09-06T15:29:26+00:00
Author: Tony Lauro
Category: Threatpost
Tags: InfoSec Insider;Web Security
Slug: 2021-09-06-human-fraud-detecting-them-before-they-detect-you

[Source](https://threatpost.com/human-fraud-detecting/169230/){:target="_blank" rel="noopener"}

> Tony Lauro, director of security technology and strategy at Akamai, discusses how to disrupt account takeovers in the exploitation phase of an attack. [...]
