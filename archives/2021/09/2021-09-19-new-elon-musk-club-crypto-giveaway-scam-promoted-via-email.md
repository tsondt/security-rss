Title: New "Elon Musk Club" crypto giveaway scam promoted via email
Date: 2021-09-19T12:58:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-19-new-elon-musk-club-crypto-giveaway-scam-promoted-via-email

[Source](https://www.bleepingcomputer.com/news/security/new-elon-musk-club-crypto-giveaway-scam-promoted-via-email/){:target="_blank" rel="noopener"}

> A new Elon Musk-themed cryptocurrency giveaway scam called the "Elon Musk Mutual Aid Fund" or "Elon Musk Club" is being promoted through spam email campaigns that started over the past few weeks. [...]
