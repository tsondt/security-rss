Title: Researchers compile list of vulnerabilities abused by ransomware gangs
Date: 2021-09-18T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-18-researchers-compile-list-of-vulnerabilities-abused-by-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/researchers-compile-list-of-vulnerabilities-abused-by-ransomware-gangs/){:target="_blank" rel="noopener"}

> Security researchers are working on compiling an easy to follow list of initial access attack vectors ransomware gangs and their affiliates are using to breach victims' networks. [...]
