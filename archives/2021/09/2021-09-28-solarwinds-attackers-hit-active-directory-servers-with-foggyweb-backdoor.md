Title: SolarWinds Attackers Hit Active Directory Servers with FoggyWeb Backdoor
Date: 2021-09-28T14:39:49+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: 2021-09-28-solarwinds-attackers-hit-active-directory-servers-with-foggyweb-backdoor

[Source](https://threatpost.com/solarwinds-active-directory-servers-foggyweb-backdoor/175056/){:target="_blank" rel="noopener"}

> Microsoft is warning that the Nobelium APT is compromising single-sign-on servers to install a post-exploitation backdoor that steals data and maintains network persistence. [...]
