Title: Telegram emerges as new dark web for cyber criminals
Date: 2021-09-17T13:39:35+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;Policy;cybercrime;dark web;hacking;telegram
Slug: 2021-09-17-telegram-emerges-as-new-dark-web-for-cyber-criminals

[Source](https://arstechnica.com/?p=1796172){:target="_blank" rel="noopener"}

> Enlarge (credit: Carl Court / Getty Images ) Telegram has exploded as a hub for cybercriminals looking to buy, sell, and share stolen data and hacking tools, new research shows, as the messaging app emerges as an alternative to the dark web. An investigation by cyber intelligence group Cyberint, together with the Financial Times, found a ballooning network of hackers sharing data leaks on the popular messaging platform, sometimes in channels with tens of thousands of subscribers, lured by its ease of use and light-touch moderation. In many cases, the content resembled that of the marketplaces found on the dark [...]
