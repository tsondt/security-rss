Title: Google Chrome to incorporate new secure payment feature
Date: 2021-09-27T15:32:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-27-google-chrome-to-incorporate-new-secure-payment-feature

[Source](https://portswigger.net/daily-swig/google-chrome-to-incorporate-new-secure-payment-feature){:target="_blank" rel="noopener"}

> New tech touted as faster and stronger than web-based authentication alternatives [...]
