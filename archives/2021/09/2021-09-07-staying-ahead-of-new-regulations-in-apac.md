Title: Staying Ahead of New Regulations in APAC
Date: 2021-09-07T16:00:00+00:00
Author: Barbara Navarro
Category: GCP Security
Tags: Identity & Security;Google Cloud;Compliance
Slug: 2021-09-07-staying-ahead-of-new-regulations-in-apac

[Source](https://cloud.google.com/blog/products/compliance/google-cloud-stays-ahead-of-apac-compliance/){:target="_blank" rel="noopener"}

> Over the course of the COVID-19 pandemic, we’ve seen our customers across the globe increase their use of cloud services, in large part due to an increase in e-commerce activities, digitization efforts, and the move to remote work. This shift has put further emphasis on the importance of security and control in cloud computing. Cloud Service Providers (CSP) have a responsibility to provide transparency and assurance around how customer data is being stored, processed, and protected, which is why in 2021 we’ve increased our efforts to support security and compliance in the APAC region. At Google Cloud, we strongly believe [...]
