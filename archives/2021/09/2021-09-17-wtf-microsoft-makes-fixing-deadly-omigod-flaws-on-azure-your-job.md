Title: WTF? Microsoft makes fixing deadly OMIGOD flaws on Azure <i>your</i> job
Date: 2021-09-17T04:58:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-17-wtf-microsoft-makes-fixing-deadly-omigod-flaws-on-azure-your-job

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/17/microsoft_manual_omigod_fixes/){:target="_blank" rel="noopener"}

> Clouds usually fix this sort of thing before bugs go public. This time it's best to assume you need to do this yourself Microsoft Azure users running Linux VMs in the IT giant's Azure cloud need to take action to protect themselves against the four "OMIGOD" bugs in the Open Management Infrastructure (OMI) framework, because Microsoft hasn't raced to do it for them.... [...]
