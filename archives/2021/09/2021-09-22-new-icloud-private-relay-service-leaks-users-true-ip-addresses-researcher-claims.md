Title: <span>New iCloud Private Relay service leaks users’ true IP addresses, researcher claims</span>
Date: 2021-09-22T11:42:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-22-new-icloud-private-relay-service-leaks-users-true-ip-addresses-researcher-claims

[Source](https://portswigger.net/daily-swig/new-icloud-private-relay-service-leaks-users-true-ip-addresses-researcher-claims){:target="_blank" rel="noopener"}

> De-anonymizing users of VPN-like service, launched with iOS 15 yesterday, is ‘easily accomplished’ [...]
