Title: One in five IceWarp mail servers still vulnerable to pre-pandemic security flaw
Date: 2021-09-09T13:47:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-09-one-in-five-icewarp-mail-servers-still-vulnerable-to-pre-pandemic-security-flaw

[Source](https://portswigger.net/daily-swig/one-in-five-icewarp-mail-servers-still-vulnerable-to-pre-pandemic-security-flaw){:target="_blank" rel="noopener"}

> Vendor agrees that XSS bug poses a grave risk, but warns it ‘can’t force users to upgrade’ [...]
