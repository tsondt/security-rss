Title: Story of the creds-leaking Exchange Autodiscover flaw – the one Microsoft wouldn't fix even after 5 years
Date: 2021-09-27T23:57:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-27-story-of-the-creds-leaking-exchange-autodiscover-flaw-the-one-microsoft-wouldnt-fix-even-after-5-years

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/27/microsoft_exchange_autodiscover/){:target="_blank" rel="noopener"}

> Redmond reckoned protocol weakness is not a security vulnerability Microsoft Exchange clients like Outlook have been supplying unprotected user credentials if you ask in a particular way since at least 2016. Though aware of this, Microsoft's advice continues to be that customers should communicate only with servers they trust.... [...]
