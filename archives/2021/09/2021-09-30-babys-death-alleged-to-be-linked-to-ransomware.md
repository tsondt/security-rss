Title: Baby’s Death Alleged to Be Linked to Ransomware
Date: 2021-09-30T17:08:50+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2021-09-30-babys-death-alleged-to-be-linked-to-ransomware

[Source](https://threatpost.com/babys-death-linked-ransomware/175232/){:target="_blank" rel="noopener"}

> Access to heart monitors disabled by the attack allegedly kept staff from spotting blood & oxygen deprivation that led to the baby's death. [...]
