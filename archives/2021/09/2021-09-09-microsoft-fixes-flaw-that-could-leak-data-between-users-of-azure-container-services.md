Title: Microsoft fixes flaw that could leak data between users of Azure container services
Date: 2021-09-09T02:56:00+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-09-microsoft-fixes-flaw-that-could-leak-data-between-users-of-azure-container-services

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/09/azure_container_flaw/){:target="_blank" rel="noopener"}

> No data went awry, Cosmos DB had a similar bug just two weeks ago Microsoft today revealed it fixed a vulnerability in its Azure Container Instances services that could have been exploited by a malicious user "to access other customers' information."... [...]
