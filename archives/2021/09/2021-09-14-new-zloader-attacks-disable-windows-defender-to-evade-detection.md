Title: New Zloader attacks disable Windows Defender to evade detection
Date: 2021-09-14T11:02:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-14-new-zloader-attacks-disable-windows-defender-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/new-zloader-attacks-disable-windows-defender-to-evade-detection/){:target="_blank" rel="noopener"}

> An ongoing Zloader campaign uses a new infection chain to disable Microsoft Defender Antivirus (formerly Windows Defender) on victims' computers to evade detection. [...]
