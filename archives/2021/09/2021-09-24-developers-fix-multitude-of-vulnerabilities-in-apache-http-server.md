Title: Developers fix multitude of vulnerabilities in Apache HTTP Server
Date: 2021-09-24T15:34:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-24-developers-fix-multitude-of-vulnerabilities-in-apache-http-server

[Source](https://portswigger.net/daily-swig/developers-fix-multitude-of-vulnerabilities-in-apache-http-server){:target="_blank" rel="noopener"}

> High-impact SSRF and request smuggling bugs among flaws addressed in bumper patch cycle [...]
