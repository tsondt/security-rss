Title: Brits open doors for tech-enabled fraudsters because they 'don't want to seem rude'
Date: 2021-09-14T15:15:05+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-09-14-brits-open-doors-for-tech-enabled-fraudsters-because-they-dont-want-to-seem-rude

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/14/too_polite_brits_scammed/){:target="_blank" rel="noopener"}

> Impersonation scams and smishing rocket, say UK Finance and Which? Brits are too polite to tell phone scammers to "get stuffed", "take a hike" or "sling yer 'ook" when they impersonate so-called "trusted organisations" such as banks.... [...]
