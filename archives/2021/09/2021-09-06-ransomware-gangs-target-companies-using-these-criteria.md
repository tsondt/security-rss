Title: Ransomware gangs target companies using these criteria
Date: 2021-09-06T06:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-06-ransomware-gangs-target-companies-using-these-criteria

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-target-companies-using-these-criteria/){:target="_blank" rel="noopener"}

> Ransomware gangs increasingly purchase access to a victim's network on dark web marketplaces and from other threat actors. Analyzing their want ads makes it possible to get an inside look at the types of companies ransomware operations are targeting for attacks. [...]
