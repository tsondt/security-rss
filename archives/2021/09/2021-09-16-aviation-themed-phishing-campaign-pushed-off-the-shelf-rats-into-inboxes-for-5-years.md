Title: Aviation-themed phishing campaign pushed off-the-shelf RATs into inboxes for 5 years
Date: 2021-09-16T20:35:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-16-aviation-themed-phishing-campaign-pushed-off-the-shelf-rats-into-inboxes-for-5-years

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/16/aviation_phishing_campaign_talos_five_years/){:target="_blank" rel="noopener"}

> Not all promises of international flight itineraries are real, warns Cisco Talos A phishing campaign that mostly targeted the global aviation industry may be connected to Nigeria, according to Cisco Talos.... [...]
