Title: FBI Had the REvil Decryption Key
Date: 2021-09-22T14:30:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;externalities;extortion;FBI;hacking;ransomware;tradecraft
Slug: 2021-09-22-fbi-had-the-revil-decryption-key

[Source](https://www.schneier.com/blog/archives/2021/09/fbi-had-the-revil-decryption-key.html){:target="_blank" rel="noopener"}

> The Washington Post reports that the FBI had a decryption key for the REvil ransomware, but didn’t pass it along to victims because it would have disrupted an ongoing operation. The key was obtained through access to the servers of the Russia-based criminal gang behind the July attack. Deploying it immediately could have helped the victims, including schools and hospitals, avoid what analysts estimate was millions of dollars in recovery costs. But the FBI held on to the key, with the agreement of other agencies, in part because it was planning to carry out an operation to disrupt the hackers, [...]
