Title: United Health Centers ransomware attack claimed by Vice Society
Date: 2021-09-24T17:10:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-24-united-health-centers-ransomware-attack-claimed-by-vice-society

[Source](https://www.bleepingcomputer.com/news/security/united-health-centers-ransomware-attack-claimed-by-vice-society/){:target="_blank" rel="noopener"}

> ​California-based United Health Centers suffered a ransomware attack that reportedly disrupted all of their locations and resulted in patient data theft. [...]
