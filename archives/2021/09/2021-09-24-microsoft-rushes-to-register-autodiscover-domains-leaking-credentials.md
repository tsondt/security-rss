Title: Microsoft rushes to register Autodiscover domains leaking credentials
Date: 2021-09-24T13:03:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-24-microsoft-rushes-to-register-autodiscover-domains-leaking-credentials

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-rushes-to-register-autodiscover-domains-leaking-credentials/){:target="_blank" rel="noopener"}

> Microsoft is rushing to register Internet domains used to steal Windows credentials sent from faulty implementations of the Microsoft Exchange Autodiscover protocol. [...]
