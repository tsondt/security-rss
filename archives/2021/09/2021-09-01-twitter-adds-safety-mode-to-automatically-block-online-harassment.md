Title: Twitter adds Safety Mode to automatically block online harassment
Date: 2021-09-01T12:40:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-01-twitter-adds-safety-mode-to-automatically-block-online-harassment

[Source](https://www.bleepingcomputer.com/news/security/twitter-adds-safety-mode-to-automatically-block-online-harassment/){:target="_blank" rel="noopener"}

> Twitter has introduced today Safety Mode, a new feature that aims to block online harassment attempts and reduce disruptive interactions on the platform. [...]
