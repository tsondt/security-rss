Title: US SEC: Watch out for Hurricane Ida-related investment scams
Date: 2021-09-04T11:12:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-04-us-sec-watch-out-for-hurricane-ida-related-investment-scams

[Source](https://www.bleepingcomputer.com/news/security/us-sec-watch-out-for-hurricane-ida-related-investment-scams/){:target="_blank" rel="noopener"}

> The US Securities and Exchange Commission has warned investors to be "extremely wary" of potential investment scams related to Hurricane Ida's aftermath. [...]
