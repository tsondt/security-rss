Title: Howard University shuts down network after ransomware attack
Date: 2021-09-08T10:26:56-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-08-howard-university-shuts-down-network-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/howard-university-shuts-down-network-after-ransomware-attack/){:target="_blank" rel="noopener"}

> The private Howard University in Washington disclosed that it suffered a ransomware attack late last week and is currently working to restore affected systems. [...]
