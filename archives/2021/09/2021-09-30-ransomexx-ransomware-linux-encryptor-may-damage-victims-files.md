Title: RansomEXX ransomware Linux encryptor may damage victims' files
Date: 2021-09-30T09:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-30-ransomexx-ransomware-linux-encryptor-may-damage-victims-files

[Source](https://www.bleepingcomputer.com/news/security/ransomexx-ransomware-linux-encryptor-may-damage-victims-files/){:target="_blank" rel="noopener"}

> Cybersecurity firm Profero has discovered that the RansomExx gang does not correctly lock Linux files during encryption, leading to potentially corrupted files. [...]
