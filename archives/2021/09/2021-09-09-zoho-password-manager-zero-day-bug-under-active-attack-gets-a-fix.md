Title: Zoho Password Manager Zero-Day Bug Under Active Attack Gets a Fix
Date: 2021-09-09T12:58:48+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: 2021-09-09-zoho-password-manager-zero-day-bug-under-active-attack-gets-a-fix

[Source](https://threatpost.com/zoho-password-manager-zero-day-attack/169303/){:target="_blank" rel="noopener"}

> An authentication bypass vulnerability leading to remote code execution offers up the keys to the corporate kingdom. [...]
