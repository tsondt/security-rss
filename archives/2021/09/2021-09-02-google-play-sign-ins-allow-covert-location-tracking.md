Title: Google Play Sign-Ins Allow Covert Location-Tracking
Date: 2021-09-02T16:03:51+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Mobile Security;Privacy;Vulnerabilities
Slug: 2021-09-02-google-play-sign-ins-allow-covert-location-tracking

[Source](https://threatpost.com/google-play-covert-location-tracking/169151/){:target="_blank" rel="noopener"}

> A design flaw involving Google Timeline could allow someone to track another device without installing a stalkerware app. [...]
