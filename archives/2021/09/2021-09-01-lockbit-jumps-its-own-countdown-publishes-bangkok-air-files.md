Title: LockBit Jumps Its Own Countdown, Publishes Bangkok Air Files
Date: 2021-09-01T15:55:23+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks;Malware;Web Security
Slug: 2021-09-01-lockbit-jumps-its-own-countdown-publishes-bangkok-air-files

[Source](https://threatpost.com/lockbit-publishes-bangkok-air-files/169101/){:target="_blank" rel="noopener"}

> The ransomware gang claims to have pulled off successful attacks against two airlines and one airport with help from its Accenture attack. [...]
