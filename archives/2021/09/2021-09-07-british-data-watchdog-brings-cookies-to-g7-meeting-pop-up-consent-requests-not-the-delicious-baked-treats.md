Title: British data watchdog brings cookies to G7 meeting – pop-up consent requests, not the delicious baked treats
Date: 2021-09-07T12:20:19+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2021-09-07-british-data-watchdog-brings-cookies-to-g7-meeting-pop-up-consent-requests-not-the-delicious-baked-treats

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/07/ico_cookies_g7/){:target="_blank" rel="noopener"}

> Why are they asking G7 to do their job for them, muses critic Cookies are on the menu today for the G7 as the UK's Information Commissioner's Office (ICO) proposes to the group of leading global economies that consent pop-ups should be reduced.... [...]
