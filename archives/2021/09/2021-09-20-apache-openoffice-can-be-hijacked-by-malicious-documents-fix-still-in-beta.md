Title: Apache OpenOffice can be hijacked by malicious documents, fix still in beta
Date: 2021-09-20T20:52:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-20-apache-openoffice-can-be-hijacked-by-malicious-documents-fix-still-in-beta

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/20/apache_openoffice_rce/){:target="_blank" rel="noopener"}

> If you need another reason to try an alternative software suite Apache OpenOffice (AOO) is currently vulnerable to a remote code execution vulnerability and while the app's source code has been patched, the fix has only been made available as beta software and awaits an official release.... [...]
