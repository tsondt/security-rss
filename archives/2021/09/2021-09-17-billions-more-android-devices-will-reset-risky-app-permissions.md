Title: Billions more Android devices will reset risky app permissions
Date: 2021-09-17T13:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2021-09-17-billions-more-android-devices-will-reset-risky-app-permissions

[Source](https://www.bleepingcomputer.com/news/security/billions-more-android-devices-will-reset-risky-app-permissions/){:target="_blank" rel="noopener"}

> Google announced today that support for a recently released Android privacy protection feature would be backported to billions of devices running older Android versions later this year. [...]
