Title: Attacks against Remote Desktop Protocol endpoints have exploded this year, warns ESET's latest Threat Report
Date: 2021-09-30T09:30:09+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: 2021-09-30-attacks-against-remote-desktop-protocol-endpoints-have-exploded-this-year-warns-esets-latest-threat-report

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/30/eset_threat_report/){:target="_blank" rel="noopener"}

> Security firm points to a 'stalkerware' epidemic, new Nobelium group activity Security specialist ESET's latest Threat Report warns of a massive increase in attacks on Remote Desktop Protocol (RDP) endpoints – and new activity from the Nobelium gang against European government organisations.... [...]
