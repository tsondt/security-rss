Title: Critical Cisco Bugs Allow Code Execution on Wireless, SD-WAN
Date: 2021-09-24T14:01:06+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2021-09-24-critical-cisco-bugs-allow-code-execution-on-wireless-sd-wan

[Source](https://threatpost.com/critical-cisco-bugs-wireless-sd-wan/174991/){:target="_blank" rel="noopener"}

> Unauthenticated cyberattackers can also wreak havoc on networking device configurations. [...]
