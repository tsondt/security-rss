Title: De-identify, re-identify: Anonymised data's dirty little secret
Date: 2021-09-16T08:28:03+00:00
Author: Danny Bradbury
Category: The Register
Tags: 
Slug: 2021-09-16-de-identify-re-identify-anonymised-datas-dirty-little-secret

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/16/anonymising_data_feature/){:target="_blank" rel="noopener"}

> Jeffrey Singh, stamp-collecting bachelor (35) of Milwaukee, Wisconsin – is that you? Feature Publishing data of all kinds offers big benefits for government, academic, and business users. Regulators demand that we make that data anonymous to deliver its benefits while protecting personal privacy. But what happens when people read between the lines?... [...]
