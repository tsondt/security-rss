Title: Manage your AWS Directory Service credentials using AWS Secrets Manager
Date: 2021-09-28T16:53:15+00:00
Author: Ashwin Bhargava
Category: AWS Security
Tags: Advanced (300);AWS Directory Service;AWS Secrets Manager;Security, Identity, & Compliance;AWS Lambda;Password storage;rotation;Security Blog
Slug: 2021-09-28-manage-your-aws-directory-service-credentials-using-aws-secrets-manager

[Source](https://aws.amazon.com/blogs/security/manage-your-aws-directory-service-credentials-using-aws-secrets-manager/){:target="_blank" rel="noopener"}

> AWS Secrets Manager helps you protect the secrets that are needed to access your applications, services, and IT resources. With this service, you can rotate, manage, and retrieve database credentials, API keys, OAuth tokens, and other secrets throughout their lifecycle. The secret value rotation feature has built-in integration for services like Amazon Relational Database Service [...]
