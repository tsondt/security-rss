Title: Apple Issues Emergency Fix for NSO Zero-Click Zero Day
Date: 2021-09-13T22:10:15+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breaking News;Malware;Mobile Security;Privacy;Vulnerabilities;Web Security
Slug: 2021-09-13-apple-issues-emergency-fix-for-nso-zero-click-zero-day

[Source](https://threatpost.com/apple-emergency-fix-nso-zero-click-zero-day/169416/){:target="_blank" rel="noopener"}

> Citizen Lab urges Apple users to update immediately. The new zero-click zero-day ForcedEntry flaw affects all things Apple: iPhones, iPads, Macs and Watches. [...]
