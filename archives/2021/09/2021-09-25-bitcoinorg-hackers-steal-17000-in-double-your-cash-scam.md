Title: Bitcoin.org hackers steal $17,000 in 'double your cash' scam
Date: 2021-09-25T10:00:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-09-25-bitcoinorg-hackers-steal-17000-in-double-your-cash-scam

[Source](https://www.bleepingcomputer.com/news/security/bitcoinorg-hackers-steal-17-000-in-double-your-cash-scam/){:target="_blank" rel="noopener"}

> This week, threat actors hijacked Bitcoin.org, the authentic website of the Bitcoin project, and altered parts of the website to push a cryptocurrency giveaway scam that unfortunately some users fell for. Although the hack lasted for less than a day, hackers seem to have walked away with a little over $17,000. [...]
