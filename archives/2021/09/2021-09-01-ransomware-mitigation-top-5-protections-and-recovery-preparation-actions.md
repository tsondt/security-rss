Title: Ransomware mitigation: Top 5 protections and recovery preparation actions
Date: 2021-09-01T19:52:40+00:00
Author: Brad Dispensa
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;ransomware;Security;Security Blog
Slug: 2021-09-01-ransomware-mitigation-top-5-protections-and-recovery-preparation-actions

[Source](https://aws.amazon.com/blogs/security/ransomware-mitigation-top-5-protections-and-recovery-preparation-actions/){:target="_blank" rel="noopener"}

> In this post, I’ll cover the top five things that Amazon Web Services (AWS) customers can do to help protect and recover their resources from ransomware. This blog post focuses specifically on preemptive actions that you can take. #1 – Set up the ability to recover your apps and data In order for a traditional [...]
