Title: Adobe Snuffs Critical Bugs in Acrobat, Experience Manager
Date: 2021-09-14T21:02:49+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-09-14-adobe-snuffs-critical-bugs-in-acrobat-experience-manager

[Source](https://threatpost.com/adobe-bugs-acrobat-experience-manager/169467/){:target="_blank" rel="noopener"}

> Adobe releases security updates for 59 bugs affecting its core products, including Adobe Acrobat Reader, XMP Toolkit SDK and Photoshop. [...]
