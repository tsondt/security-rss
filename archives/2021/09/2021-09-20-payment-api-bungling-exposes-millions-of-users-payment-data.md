Title: Payment API Bungling Exposes Millions of Users’ Payment Data
Date: 2021-09-20T19:02:57+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Most Recent ThreatLists;Vulnerabilities;Web Security
Slug: 2021-09-20-payment-api-bungling-exposes-millions-of-users-payment-data

[Source](https://threatpost.com/payment-api-exposes-payment-data/174825/){:target="_blank" rel="noopener"}

> Misconfigured APIs make any app risky, but when you’re talking about financial apps, you’re talking about handing ne’er-do-wells the power to turn your pockets inside-out. [...]
