Title: Does Your Organization Have a Security.txt File?
Date: 2021-09-20T21:57:27+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;alex holden;Alphabet;Amazon;Edwin Foudil;Facebook;HCA Healthcare;Hold Security;Kroger;Procter & Gamble;security.txt;USAA;Walmart
Slug: 2021-09-20-does-your-organization-have-a-securitytxt-file

[Source](https://krebsonsecurity.com/2021/09/does-your-organization-have-a-security-txt-file/){:target="_blank" rel="noopener"}

> It happens all the time: Organizations get hacked because there isn’t an obvious way for security researchers to let them know about security vulnerabilities or data leaks. Or maybe it isn’t entirely clear who should get the report when remote access to an organization’s internal network is being sold in the cybercrime underground. In a bid to minimize these scenarios, a growing number of major companies are adopting “ Security.txt,” a proposed new Internet standard that helps organizations describe their vulnerability disclosure practices and preferences. An example of a security.txt file. Image: Securitytxt.org. The idea behind Security.txt is straightforward: The [...]
