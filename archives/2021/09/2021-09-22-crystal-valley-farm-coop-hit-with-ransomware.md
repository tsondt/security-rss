Title: Crystal Valley Farm Coop Hit with Ransomware
Date: 2021-09-22T22:17:33+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-09-22-crystal-valley-farm-coop-hit-with-ransomware

[Source](https://threatpost.com/crystal-valley-farm-coop-hit-with-ransomware/174928/){:target="_blank" rel="noopener"}

> It's the second agricultural business to be seized this week and portends a bitter harvest with yet another nasty jab at critical infrastructure. [...]
