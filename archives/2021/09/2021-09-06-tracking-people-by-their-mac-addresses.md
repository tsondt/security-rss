Title: Tracking People by their MAC Addresses
Date: 2021-09-06T11:11:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Bluetooth;tracking;vulnerabilities
Slug: 2021-09-06-tracking-people-by-their-mac-addresses

[Source](https://www.schneier.com/blog/archives/2021/09/tracking-people-by-their-mac-addresses.html){:target="_blank" rel="noopener"}

> Yet another article on the privacy risks of static MAC addresses and always-on Bluetooth connections. This one is about wireless headphones. The good news is that product vendors are fixing this: Several of the headphones which could be tracked over time are for sale in electronics stores, but according to two of the manufacturers NRK have spoken to, these models are being phased out. “The products in your line-up, Elite Active 65t, Elite 65e and Evolve 75e, will be going out of production before long and newer versions have already been launched with randomized MAC addresses. We have a lot [...]
