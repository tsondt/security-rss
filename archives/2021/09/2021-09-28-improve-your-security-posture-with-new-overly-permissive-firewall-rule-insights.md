Title: Improve your security posture with new Overly Permissive Firewall Rule Insights
Date: 2021-09-28T16:00:00+00:00
Author: Tracy Jiang
Category: GCP Security
Tags: Networking;Identity & Security;Management Tools;Solutions and How-to's;Google Cloud
Slug: 2021-09-28-improve-your-security-posture-with-new-overly-permissive-firewall-rule-insights

[Source](https://cloud.google.com/blog/products/gcp/use-firewall-insights-to-improve-security-posture/){:target="_blank" rel="noopener"}

> Are you a network security engineer managing large shared VPCs with many projects and applications deployed, and struggling to clean up hundreds of firewall rules accumulated overtime in the VPC firewall rule set? Are you a network admin setting up open firewall rules to accelerate cloud migration, but later struggling to close them down without worrying about causing outages? Are you a security admin trying to get a realistic assessment of the quality of your firewall rule configuration, and to evaluate and improve your security posture? If the answer to any of the questions above is a “Yes”, you’ve come [...]
