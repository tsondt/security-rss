Title: Miscreants fling booby-trapped Office files at victims, no patch yet, says Microsoft
Date: 2021-09-07T22:20:25+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-09-07-miscreants-fling-booby-trapped-office-files-at-victims-no-patch-yet-says-microsoft

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/07/microsoft_office_zero_day/){:target="_blank" rel="noopener"}

> ActiveX and MSHTML, the gift that keeps on giving... to intruders In an advisory issued on Tuesday, Microsoft said some of its users were targeted by poisoned Office documents that exploit an unpatched flaw to hijack their Windows machines.... [...]
