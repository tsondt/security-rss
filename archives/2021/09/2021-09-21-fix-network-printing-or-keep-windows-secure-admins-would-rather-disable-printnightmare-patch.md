Title: Fix network printing or keep Windows secure? Admins would rather disable PrintNightmare patch
Date: 2021-09-21T13:00:09+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: 2021-09-21-fix-network-printing-or-keep-windows-secure-admins-would-rather-disable-printnightmare-patch

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/21/microsoft_printnightmare/){:target="_blank" rel="noopener"}

> 'Our >3,000 customers had to print again' Microsoft's Patch Tuesday update last week was meant to fix print vulnerabilities in Windows but also broke network printing for many, with some admins disabling security or removing the patch to get it working.... [...]
