Title: 17 additional AWS services authorized for DoD workloads in the AWS GovCloud Regions
Date: 2021-09-08T23:33:22+00:00
Author: Tyler Harding
Category: AWS Security
Tags: Announcements;Federal;Foundational (100);Government;Public Sector;Security, Identity, & Compliance;Amazon Cognito;Amazon EKS;Amazon Pinpoint;Amazon SES;Amazon Textract;AWS Backup;AWS Budgets;AWS CodePipeline;AWS Cost & Usage Report;AWS Cost Explorer;AWS Fargate;AWS License Manager;AWS Marketplace;AWS Personal Health Dashboard;AWS Security Hub;AWS Systems Manager;AWS X-Ray;DoD IL 4;DoD IL 5;FedRAMP;NIST SP 800-53;P-ATO;Security Blog
Slug: 2021-09-08-17-additional-aws-services-authorized-for-dod-workloads-in-the-aws-govcloud-regions

[Source](https://aws.amazon.com/blogs/security/17-additional-aws-services-authorized-for-dod-workloads-in-the-aws-govcloud-regions/){:target="_blank" rel="noopener"}

> I’m pleased to announce that the Defense Information Systems Agency (DISA) has authorized 17 additional Amazon Web Services (AWS) services and features in the AWS GovCloud (US) Regions, bringing the total to 105 services and major features that are authorized for use by the U.S. Department of Defense (DoD). AWS now offers additional services to [...]
