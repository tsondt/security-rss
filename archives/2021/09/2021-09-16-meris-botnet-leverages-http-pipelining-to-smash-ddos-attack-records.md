Title: Meris botnet leverages HTTP pipelining to smash DDoS attack records
Date: 2021-09-16T13:59:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-16-meris-botnet-leverages-http-pipelining-to-smash-ddos-attack-records

[Source](https://portswigger.net/daily-swig/meris-botnet-leverages-http-pipelining-to-smash-ddos-attack-records){:target="_blank" rel="noopener"}

> Source of attacks ‘almost entirely composed of Mikrotik devices’ [...]
