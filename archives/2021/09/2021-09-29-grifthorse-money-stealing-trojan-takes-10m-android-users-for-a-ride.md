Title: GriftHorse Money-Stealing Trojan Takes 10M Android Users for a Ride
Date: 2021-09-29T18:08:54+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: 2021-09-29-grifthorse-money-stealing-trojan-takes-10m-android-users-for-a-ride

[Source](https://threatpost.com/grifthorse-money-stealing-trojan-android/175130/){:target="_blank" rel="noopener"}

> The mobile malware has fleeced hundreds of millions of dollars from victims globally, using sophisticated techniques. [...]
