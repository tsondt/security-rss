Title: In space, no one can hear cyber security professionals scream
Date: 2021-09-02T13:22:04+00:00
Author: Davey Winder
Category: The Register
Tags: 
Slug: 2021-09-02-in-space-no-one-can-hear-cyber-security-professionals-scream

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/02/in_space_no_security/){:target="_blank" rel="noopener"}

> Miscreants hacking vulnerable orbital hardware could set living standards back by decades in seconds Feature "Space is an invaluable domain, but it is also increasingly crowded and particularly susceptible to a range of cyber vulnerabilities and threats."... [...]
