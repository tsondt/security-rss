Title: ‘Stalkerware’ vendor SpyFone barred from surveillance market, FTC announces
Date: 2021-09-02T14:39:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-02-stalkerware-vendor-spyfone-barred-from-surveillance-market-ftc-announces

[Source](https://portswigger.net/daily-swig/stalkerware-vendor-spyfone-barred-from-surveillance-market-ftc-announces){:target="_blank" rel="noopener"}

> Stalkers and domestic abusers can use app to ‘stealthily track’ victims, says watchdog [...]
