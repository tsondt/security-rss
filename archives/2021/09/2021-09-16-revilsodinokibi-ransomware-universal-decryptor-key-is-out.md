Title: REvil/Sodinokibi Ransomware Universal Decryptor Key Is Out
Date: 2021-09-16T13:00:37+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-09-16-revilsodinokibi-ransomware-universal-decryptor-key-is-out

[Source](https://threatpost.com/revil-sodinokibi-ransomware-universal-decryptor/169498/){:target="_blank" rel="noopener"}

> Bitdefender worked with law enforcement to create a key to unlock victims encrypted in ransomware attacks before REvil's servers went belly-up on July 13. [...]
