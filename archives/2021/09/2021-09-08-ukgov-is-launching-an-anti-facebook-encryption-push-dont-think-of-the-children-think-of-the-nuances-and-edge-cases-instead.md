Title: UK.gov is launching an anti-Facebook encryption push. Don't think of the children: Think of the nuances and edge cases instead
Date: 2021-09-08T13:44:32+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-08-ukgov-is-launching-an-anti-facebook-encryption-push-dont-think-of-the-children-think-of-the-nuances-and-edge-cases-instead

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/08/uk_anti_encryption_facebook_e2ee_push_begins/){:target="_blank" rel="noopener"}

> You can't reduce such a vital issue to concern over paedophiles and terrorists Opinion The British government is preparing to launch a full-scale policy assault against Facebook as the company gears up to introduce end-to-end encryption across all of its services.... [...]
