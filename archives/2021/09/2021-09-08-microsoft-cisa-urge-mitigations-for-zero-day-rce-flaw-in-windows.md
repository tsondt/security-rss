Title: Microsoft, CISA Urge Mitigations for Zero-Day RCE Flaw in Windows
Date: 2021-09-08T12:24:51+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-09-08-microsoft-cisa-urge-mitigations-for-zero-day-rce-flaw-in-windows

[Source](https://threatpost.com/microsoft-zero-day-rce-flaw-in-windows/169273/){:target="_blank" rel="noopener"}

> Attackers are actively attempting to exploit a vulnerability in MSHTML that allows them to craft a malicious ActiveX control to be used by Microsoft Office files. [...]
