Title: The Week in Ransomware - September 24th 2021 - Targeting crypto
Date: 2021-09-24T19:27:03-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-24-the-week-in-ransomware-september-24th-2021-targeting-crypto

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-24th-2021-targeting-crypto/){:target="_blank" rel="noopener"}

> This week's biggest news is the USA sanctioning a crypto exchange used by ransomware gangs to convert cryptocurrency into fiat currency. By targeting rogue exchanges, the US government is hoping to disrupt ransomware's payment system. [...]
