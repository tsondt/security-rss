Title: Apple Pay with Visa Hacked to Make Payments via Locked iPhones
Date: 2021-09-30T15:26:52+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Mobile Security;Vulnerabilities
Slug: 2021-09-30-apple-pay-with-visa-hacked-to-make-payments-via-locked-iphones

[Source](https://threatpost.com/apple-pay-visa-hacked-locked-iphones/175229/){:target="_blank" rel="noopener"}

> Researchers have demonstrated that someone could use a stolen, locked iPhone to pay for thousands of dollars of goods or services, no authentication needed. [...]
