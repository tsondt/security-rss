Title: Large-Scale Phishing-as-a-Service Operation Exposed
Date: 2021-09-23T11:10:45+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks
Slug: 2021-09-23-large-scale-phishing-as-a-service-operation-exposed

[Source](https://threatpost.com/phishing-as-a-service-exposed/174932/){:target="_blank" rel="noopener"}

> Discovery of BulletProofLink—which provides phishing kits, email templates, hosting and other tools—sheds light on how wannabe cybercriminals can get into the business. [...]
