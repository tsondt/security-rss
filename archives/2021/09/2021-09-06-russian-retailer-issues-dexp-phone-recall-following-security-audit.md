Title: Russian retailer issues DEXP phone recall following security audit
Date: 2021-09-06T12:55:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-06-russian-retailer-issues-dexp-phone-recall-following-security-audit

[Source](https://portswigger.net/daily-swig/russian-retailer-issues-dexp-phone-recall-following-security-audit){:target="_blank" rel="noopener"}

> Electronics retailer DNS issued the product recall after a security researcher published their findings last week [...]
