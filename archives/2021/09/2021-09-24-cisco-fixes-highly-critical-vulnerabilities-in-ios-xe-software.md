Title: Cisco fixes highly critical vulnerabilities in IOS XE Software
Date: 2021-09-24T03:23:23-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-24-cisco-fixes-highly-critical-vulnerabilities-in-ios-xe-software

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-highly-critical-vulnerabilities-in-ios-xe-software/){:target="_blank" rel="noopener"}

> Cisco has patched three critical vulnerabilities affecting components in its IOS XE internetworking operating system powering routers and wireless controllers, or products running with a specific configuration. [...]
