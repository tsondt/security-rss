Title: Cloud CISO Perspectives: September 2021
Date: 2021-09-28T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2021-09-28-cloud-ciso-perspectives-september-2021

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-september-2021/){:target="_blank" rel="noopener"}

> We’re busy getting ready for Google Cloud Next ‘21 where we’re excited to talk about the latest updates to our security portfolio and new ways we’re committing to help all of our customers build securely with our cloud. Here are a few sessions you don’t want to miss with our Google Cloud security experts and customers that cover top-of-mind areas in today’s cybersecurity landscape: The path to invisible security Secure supply chain best practices & toolshare Ransomware and cyber resilience Operate with zero trust using BeyondCorp Enterprise Trust the cloud more by trusting it less: Ubiquitous data encryption In this [...]
