Title: BlackMatter Ransomware Hits Japanese Tech Giant Olympus
Date: 2021-09-14T11:24:06+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: 2021-09-14-blackmatter-ransomware-hits-japanese-tech-giant-olympus

[Source](https://threatpost.com/blackmatter-ransomware-olympus/169423/){:target="_blank" rel="noopener"}

> The incident that occurred Sept. 8 and affected its EMEA IT systems seems to signal a return to business as usual for ransomware groups. [...]
