Title: Glasgow firm fined £150k after half a million nuisance calls, spoofing phone number, using false trading names
Date: 2021-09-07T08:29:12+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-09-07-glasgow-firm-fined-150k-after-half-a-million-nuisance-calls-spoofing-phone-number-using-false-trading-names

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/07/dialadeal_ico_fine/){:target="_blank" rel="noopener"}

> DialADeal is no longer operating A Glasgow-based company is facing a £150,000 penalty handed down by the UK's data watchdog for making more than half a million nuisance calls about bogus green energy deals.... [...]
