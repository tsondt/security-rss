Title: Republican Governors Association email server breached by state hackers
Date: 2021-09-20T09:43:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-20-republican-governors-association-email-server-breached-by-state-hackers

[Source](https://www.bleepingcomputer.com/news/security/republican-governors-association-email-server-breached-by-state-hackers/){:target="_blank" rel="noopener"}

> The Republican Governors Association (RGA) revealed in data breach notification letters sent last week that its servers were breached during an extensive Microsoft Exchange hacking campaign that hit organizations worldwide in March 2021. [...]
