Title: EU: Russia Behind ‘Ghostwriter’ Campaign Targeting Germany
Date: 2021-09-27T15:35:42+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Web Security
Slug: 2021-09-27-eu-russia-behind-ghostwriter-campaign-targeting-germany

[Source](https://threatpost.com/eu-russia-ghostwriter-germany/175025/){:target="_blank" rel="noopener"}

> It's not the first time that the disinformation/spearphishing campaign, which originally smeared NATO, has been linked to Russia. [...]
