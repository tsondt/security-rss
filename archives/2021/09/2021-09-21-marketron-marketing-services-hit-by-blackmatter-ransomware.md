Title: Marketron marketing services hit by Blackmatter ransomware
Date: 2021-09-21T03:25:06-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-21-marketron-marketing-services-hit-by-blackmatter-ransomware

[Source](https://www.bleepingcomputer.com/news/security/marketron-marketing-services-hit-by-blackmatter-ransomware/){:target="_blank" rel="noopener"}

> BlackMatter ransomware gang over the weekend hit Marketron, a business software solutions provider that serves more than 6,000 customers in the media industry. [...]
