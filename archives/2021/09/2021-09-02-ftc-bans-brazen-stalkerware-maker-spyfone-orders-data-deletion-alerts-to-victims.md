Title: FTC bans 'brazen' stalkerware maker SpyFone, orders data deletion, alerts to victims
Date: 2021-09-02T21:05:39+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-09-02-ftc-bans-brazen-stalkerware-maker-spyfone-orders-data-deletion-alerts-to-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/02/ftc_spyfone_stalkerware/){:target="_blank" rel="noopener"}

> Insecure systems were compromised by miscreant, too, watchdog said America's trade watchdog today banned stalkerware developer SpyFone and its CEO from the surveillance industry, effectively putting an end to its business.... [...]
