Title: US farmer cooperative hit by $5.9M BlackMatter ransomware attack
Date: 2021-09-20T14:07:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-20-us-farmer-cooperative-hit-by-59m-blackmatter-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/us-farmer-cooperative-hit-by-59m-blackmatter-ransomware-attack/){:target="_blank" rel="noopener"}

> U.S. farmers cooperative NEW Cooperative has suffered a BlackMatter ransomware attack demanding $5.9 million not to leak stolen data and provide a decryptor. [...]
