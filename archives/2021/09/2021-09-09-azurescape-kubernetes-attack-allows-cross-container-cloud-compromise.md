Title: ‘Azurescape’ Kubernetes Attack Allows Cross-Container Cloud Compromise
Date: 2021-09-09T16:39:13+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2021-09-09-azurescape-kubernetes-attack-allows-cross-container-cloud-compromise

[Source](https://threatpost.com/azurescape-kubernetes-attack-container-cloud-compromise/169319/){:target="_blank" rel="noopener"}

> A chain of exploits could allow a malicious Azure user to infiltrate other customers' cloud instances within Microsoft's container-as-a-service offering. [...]
