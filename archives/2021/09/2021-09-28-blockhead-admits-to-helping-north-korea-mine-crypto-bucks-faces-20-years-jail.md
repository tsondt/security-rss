Title: Blockhead admits to helping North Korea mine crypto-bucks, faces 20 years jail
Date: 2021-09-28T05:44:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-28-blockhead-admits-to-helping-north-korea-mine-crypto-bucks-faces-20-years-jail

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/28/blockhead_admits_to_helping_north/){:target="_blank" rel="noopener"}

> Also advised on how smart contracts could help DPRK in US nuke talks A US citizen has admitted to helping the Democratic People's Republic of Korea (DPRK) to establish cryptocurrency capabilities and faces up to 20 years jail for his actions.... [...]
