Title: Supply chain attacks against the open source ecosystem soar by 650% – report
Date: 2021-09-15T13:49:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-15-supply-chain-attacks-against-the-open-source-ecosystem-soar-by-650-report

[Source](https://portswigger.net/daily-swig/supply-chain-attacks-against-the-open-source-ecosystem-soar-by-650-report){:target="_blank" rel="noopener"}

> Dependency confusion has quickly become the attack technique of choice [...]
