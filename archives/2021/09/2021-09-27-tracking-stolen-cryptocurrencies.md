Title: Tracking Stolen Cryptocurrencies
Date: 2021-09-27T11:25:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;blockchain;cryptocurrency;forensics;theft;tracking
Slug: 2021-09-27-tracking-stolen-cryptocurrencies

[Source](https://www.schneier.com/blog/archives/2021/09/tracking-stolen-cryptocurrencies.html){:target="_blank" rel="noopener"}

> Good article about the current state of cryptocurrency forensics. [...]
