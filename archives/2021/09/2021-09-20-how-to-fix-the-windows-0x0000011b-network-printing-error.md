Title: How to fix the Windows 0x0000011b network printing error
Date: 2021-09-20T19:03:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-20-how-to-fix-the-windows-0x0000011b-network-printing-error

[Source](https://www.bleepingcomputer.com/news/microsoft/how-to-fix-the-windows-0x0000011b-network-printing-error/){:target="_blank" rel="noopener"}

> A Windows security update released in January and now fully enforced this month is causing Windows users to experience 0x0000011b errors when printing to network printers. [...]
