Title: How to automate incident response to security events with AWS Systems Manager Incident Manager
Date: 2021-09-17T15:46:13+00:00
Author: Sumit Patel
Category: AWS Security
Tags: AWS Systems Manager;Intermediate (200);Security, Identity, & Compliance;Amazon EventBridge;Amazon GuardDuty;AWS CloudTrail;AWS Config;AWS Systems Manager Incident Manager;Security Blog
Slug: 2021-09-17-how-to-automate-incident-response-to-security-events-with-aws-systems-manager-incident-manager

[Source](https://aws.amazon.com/blogs/security/how-to-automate-incident-response-to-security-events-with-aws-systems-manager-incident-manager/){:target="_blank" rel="noopener"}

> Incident response is a core security capability for organizations to develop, and a core element in the AWS Cloud Adoption Framework (AWS CAF). Responding to security incidents quickly is important to minimize their impacts. Automating incident response helps you scale your capabilities, rapidly reduce the scope of compromised resources, and reduce repetitive work by your [...]
