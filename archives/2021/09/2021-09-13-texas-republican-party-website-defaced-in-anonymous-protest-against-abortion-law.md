Title: Texas Republican Party website defaced in ‘Anonymous’ protest against abortion law
Date: 2021-09-13T12:29:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-13-texas-republican-party-website-defaced-in-anonymous-protest-against-abortion-law

[Source](https://portswigger.net/daily-swig/texas-republican-party-website-defaced-in-anonymous-protest-against-abortion-law){:target="_blank" rel="noopener"}

> Hacktivists take aim at ‘Heartbeat Act’ with references to The Handmaid’s Tale and Rick-rolling meme [...]
