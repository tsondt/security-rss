Title: Going beyond backup: Acronis True Image is now Acronis Cyber Protect Home Office
Date: 2021-09-08T09:00:00-04:00
Author: Sponsored by Acronis
Category: BleepingComputer
Tags: Security
Slug: 2021-09-08-going-beyond-backup-acronis-true-image-is-now-acronis-cyber-protect-home-office

[Source](https://www.bleepingcomputer.com/news/security/going-beyond-backup-acronis-true-image-is-now-acronis-cyber-protect-home-office/){:target="_blank" rel="noopener"}

> After nearly two decades, one of the most recognizable software brands is getting a new name. Acronis True Image, the leading personal cyber protection solution, is changing its name to Acronis Cyber Protect Home Office. [...]
