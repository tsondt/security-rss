Title: ZLoader’s Back, Abusing Google AdWords, Disabling Windows Defender
Date: 2021-09-14T17:21:59+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware
Slug: 2021-09-14-zloaders-back-abusing-google-adwords-disabling-windows-defender

[Source](https://threatpost.com/zloader-google-adwords-windows-defender/169448/){:target="_blank" rel="noopener"}

> The well-known banking trojan retools for stealth with a whole new attack routine, including using ads for Microsoft TeamViewer and Zoom to lure victims in. [...]
