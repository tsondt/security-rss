Title: 2021’s Most Dangerous Software Weaknesses
Date: 2021-09-14T20:05:52+00:00
Author: Saryu Nayyar
Category: Threatpost
Tags: InfoSec Insider;Vulnerabilities;Web Security
Slug: 2021-09-14-2021s-most-dangerous-software-weaknesses

[Source](https://threatpost.com/2021-angerous-software-weaknesses/169458/){:target="_blank" rel="noopener"}

> Saryu Nayyar, CEO at Gurucul, peeks into Mitre's list of dangerous software bug types, highlighting that the oldies are still the goodies for attackers. [...]
