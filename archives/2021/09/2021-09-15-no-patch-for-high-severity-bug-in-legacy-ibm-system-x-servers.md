Title: No Patch for High-Severity Bug in Legacy IBM System X Servers
Date: 2021-09-15T19:01:48+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: 2021-09-15-no-patch-for-high-severity-bug-in-legacy-ibm-system-x-servers

[Source](https://threatpost.com/no-patch-for-ibm-system-x-servers/169491/){:target="_blank" rel="noopener"}

> Two of IBM's aging flagship server models, retired in 2020, won’t be patched for a command-injection flaw. [...]
