Title: Apple emergency patches fix zero-click iMessage bug used to inject NSO spyware
Date: 2021-09-13T23:06:00+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-13-apple-emergency-patches-fix-zero-click-imessage-bug-used-to-inject-nso-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/13/apple_ios_macos_security_fixes/){:target="_blank" rel="noopener"}

> Separate flaw in WebKit also under attack squashed, too – and two zero-days in Chrome, as well Updated Apple on Monday issued security patches for its mobile and desktop operating systems, and for its WebKit browser engine, to address two security flaws, at least one of which was, it is said, used by autocratic governments to spy on human rights advocates.... [...]
