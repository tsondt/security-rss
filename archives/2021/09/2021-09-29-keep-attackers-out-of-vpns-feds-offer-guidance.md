Title: Keep Attackers Out of VPNs: Feds Offer Guidance
Date: 2021-09-29T23:10:32+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Vulnerabilities;Web Security
Slug: 2021-09-29-keep-attackers-out-of-vpns-feds-offer-guidance

[Source](https://threatpost.com/vpns-nsa-cisa-guidance/175150/){:target="_blank" rel="noopener"}

> The NSA and CISA issued recommendations on choosing and hardening VPNs to prevent nation-state APTs from weaponizing flaws & CVEs to break into protected networks. [...]
