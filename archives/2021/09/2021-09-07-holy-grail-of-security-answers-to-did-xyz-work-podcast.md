Title: Holy Grail of Security: Answers to ‘Did XYZ Work?’ – Podcast
Date: 2021-09-07T12:00:31+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks;Malware;Mobile Security;Podcasts;Vulnerabilities;Web Security
Slug: 2021-09-07-holy-grail-of-security-answers-to-did-xyz-work-podcast

[Source](https://threatpost.com/holy-grail-of-security-answers-to-did-xyz-work-podcast/169192/){:target="_blank" rel="noopener"}

> Verizon DBIR is already funny, useful & well-written, and it just got better with mapping to MITRE ATT&CK TTPs. The marriage could finally bring answers to "What are we doing right?" instead of the constant reminders of what's not working in fending off threats. [...]
