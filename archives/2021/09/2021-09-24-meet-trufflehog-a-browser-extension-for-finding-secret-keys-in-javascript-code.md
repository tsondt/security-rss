Title: Meet TruffleHog – a browser extension for finding secret keys in JavaScript code
Date: 2021-09-24T14:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-24-meet-trufflehog-a-browser-extension-for-finding-secret-keys-in-javascript-code

[Source](https://portswigger.net/daily-swig/meet-trufflehog-a-browser-extension-for-finding-secret-keys-in-javascript-code){:target="_blank" rel="noopener"}

> API keys are accidentally being leaked by websites. Here’s how to find them [...]
