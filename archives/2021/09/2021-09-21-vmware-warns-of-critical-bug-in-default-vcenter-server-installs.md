Title: VMware warns of critical bug in default vCenter Server installs
Date: 2021-09-21T13:40:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-21-vmware-warns-of-critical-bug-in-default-vcenter-server-installs

[Source](https://www.bleepingcomputer.com/news/security/vmware-warns-of-critical-bug-in-default-vcenter-server-installs/){:target="_blank" rel="noopener"}

> VMware warns customers to immediately patch a critical arbitrary file upload vulnerability in the Analytics service, impacting all appliances running default vCenter Server 6.7 and 7.0 deployments. [...]
