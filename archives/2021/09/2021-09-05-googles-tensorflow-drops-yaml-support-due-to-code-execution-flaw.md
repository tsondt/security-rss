Title: Google's TensorFlow drops YAML support due to code execution flaw
Date: 2021-09-05T03:23:48-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Software
Slug: 2021-09-05-googles-tensorflow-drops-yaml-support-due-to-code-execution-flaw

[Source](https://www.bleepingcomputer.com/news/security/googles-tensorflow-drops-yaml-support-due-to-code-execution-flaw/){:target="_blank" rel="noopener"}

> TensorFlow, a popular Python-based machine learning and artificial intelligence project developed by Google has dropped support for YAML, to patch a critical code execution vulnerability. YAML is a convenient choice among developers looking for a human-readable data serialization language. [...]
