Title: Jenkins project's Confluence server hacked to mine Monero
Date: 2021-09-07T11:46:41-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-07-jenkins-projects-confluence-server-hacked-to-mine-monero

[Source](https://www.bleepingcomputer.com/news/security/jenkins-projects-confluence-server-hacked-to-mine-monero/){:target="_blank" rel="noopener"}

> Hackers exploiting the recently disclosed Atlassian Confluence remote code execution vulnerability breached an internal server from the Jenkins project. [...]
