Title: Emails, chat logs, more leaked online from far-right militia linked to US Capitol riot
Date: 2021-09-28T06:17:14+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2021-09-28-emails-chat-logs-more-leaked-online-from-far-right-militia-linked-to-us-capitol-riot

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/28/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Other infosec news from this month In brief Emails, chat logs, membership records, donor lists and other files siphoned from a far-right anti-government self-styled militia were leaked online on Monday, it appears.... [...]
