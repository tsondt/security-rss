Title: WordPress security: information leak flaw addressed in Ninja Forms
Date: 2021-09-08T16:23:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-08-wordpress-security-information-leak-flaw-addressed-in-ninja-forms

[Source](https://portswigger.net/daily-swig/wordpress-security-information-leak-flaw-addressed-in-ninja-forms){:target="_blank" rel="noopener"}

> Developer reveals error-proofing improvements after delay to rollout of rapid fix [...]
