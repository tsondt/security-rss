Title: 'Quad' group seeks to set security standards for global tech industry
Date: 2021-09-27T02:21:01+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-27-quad-group-seeks-to-set-security-standards-for-global-tech-industry

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/27/quad_communqiue_technology_announcements/){:target="_blank" rel="noopener"}

> USA, India, Australia, and Japan pledge to build own 5G tech, share space data, secure rare earth supply chains, and more The Quad group of nations – the USA, India, Australia, and Japan – has announced several joint initiatives to share technology and spur its development, among them a plan to set new global security standards for the technology industry.... [...]
