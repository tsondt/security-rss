Title: Ransomware crims saying 'We'll burn your data if you get a negotiator' can't be legally paid off anyway
Date: 2021-09-15T11:33:35+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-15-ransomware-crims-saying-well-burn-your-data-if-you-get-a-negotiator-cant-be-legally-paid-off-anyway

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/15/grief_corp_ransomware_negotiator_rage/){:target="_blank" rel="noopener"}

> Grief Corp are already under US sanctions, says Emsisoft A couple of ransomware gangs have threatened to start deleting files if targeted companies call in professional negotiators to help lower prices for decryption tools.... [...]
