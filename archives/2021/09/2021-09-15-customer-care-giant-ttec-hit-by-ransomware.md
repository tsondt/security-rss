Title: Customer Care Giant TTEC Hit By Ransomware
Date: 2021-09-15T21:31:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;Ragnar Locker;TTEC
Slug: 2021-09-15-customer-care-giant-ttec-hit-by-ransomware

[Source](https://krebsonsecurity.com/2021/09/customer-care-giant-ttec-hit-by-ransomware/){:target="_blank" rel="noopener"}

> TTEC, [ NASDAQ: TTEC ], a company used by some of the world’s largest brands to help manage customer support and sales online and over the phone, is dealing with disruptions from a network security incident resulting from a ransomware attack, KrebsOnSecurity has learned. While many companies have been laying off or furloughing workers in response to the Coronavirus pandemic, TTEC has been massively hiring. Formerly TeleTech Holdings Inc., Englewood, Co.-based TTEC now has nearly 60,000 employees, most of whom work from home and answer customer support calls on behalf of a large number of name-brand companies, like Bank of [...]
