Title: Thousands of University Wi-Fi Networks Expose Log-In Credentials
Date: 2021-09-30T11:29:23+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Mobile Security;Vulnerabilities;Web Security
Slug: 2021-09-30-thousands-of-university-wi-fi-networks-expose-log-in-credentials

[Source](https://threatpost.com/misconfiguration-university-wifi-login-credentials/175157/){:target="_blank" rel="noopener"}

> Certificate misconfigurations of the EAP protocol in Eduroam (and likely other networks globally) threaten Android and Windows users. [...]
