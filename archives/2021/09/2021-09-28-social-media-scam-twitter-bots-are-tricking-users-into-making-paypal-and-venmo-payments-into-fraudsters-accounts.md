Title: Social media scam: Twitter bots are tricking users into making PayPal and Venmo payments into fraudsters’ accounts
Date: 2021-09-28T13:41:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-28-social-media-scam-twitter-bots-are-tricking-users-into-making-paypal-and-venmo-payments-into-fraudsters-accounts

[Source](https://portswigger.net/daily-swig/social-media-scam-twitter-bots-are-tricking-users-into-making-paypal-and-venmo-payments-into-fraudsters-accounts){:target="_blank" rel="noopener"}

> Social engineering scammers are using cloned social media accounts to carry out deceit [...]
