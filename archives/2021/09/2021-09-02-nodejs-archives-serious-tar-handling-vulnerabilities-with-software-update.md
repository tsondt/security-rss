Title: Node.js archives serious tar handling vulnerabilities with software update
Date: 2021-09-02T15:41:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-02-nodejs-archives-serious-tar-handling-vulnerabilities-with-software-update

[Source](https://portswigger.net/daily-swig/node-js-archives-serious-tar-handling-vulnerabilities-with-software-update){:target="_blank" rel="noopener"}

> Enter the tar pit [...]
