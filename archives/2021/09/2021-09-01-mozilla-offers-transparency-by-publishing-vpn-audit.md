Title: Mozilla offers transparency by publishing VPN audit
Date: 2021-09-01T16:40:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-01-mozilla-offers-transparency-by-publishing-vpn-audit

[Source](https://portswigger.net/daily-swig/mozilla-offers-transparency-by-publishing-vpn-audit){:target="_blank" rel="noopener"}

> Non-profit reveals more favorable results than those uncovered by similar review last year [...]
