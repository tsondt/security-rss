Title: Which? survey finds people would actually pay the online giants not to take their data
Date: 2021-09-30T15:28:12+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-09-30-which-survey-finds-people-would-actually-pay-the-online-giants-not-to-take-their-data

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/30/which_data_survey/){:target="_blank" rel="noopener"}

> On the other hand, sweeten the deal with a couple of quid and they'll be a lot more happy to share Consumer guardian Which? has attempted to put a price on people's personal information as it ramps up pressure for tougher rules around data-ravenous tech giants such as Google and Facebook.... [...]
