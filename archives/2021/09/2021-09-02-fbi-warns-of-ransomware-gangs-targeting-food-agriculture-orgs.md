Title: FBI warns of ransomware gangs targeting food, agriculture orgs
Date: 2021-09-02T13:52:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-02-fbi-warns-of-ransomware-gangs-targeting-food-agriculture-orgs

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-ransomware-gangs-targeting-food-agriculture-orgs/){:target="_blank" rel="noopener"}

> The FBI says ransomware gangs are actively targeting and disrupting the operations of organizations in the food and agriculture sector, causing financial loss and directly affecting the food supply chain. [...]
