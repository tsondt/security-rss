Title: McDonald's leaks password for Monopoly VIP database to winners
Date: 2021-09-07T10:56:12-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-07-mcdonalds-leaks-password-for-monopoly-vip-database-to-winners

[Source](https://www.bleepingcomputer.com/news/security/mcdonalds-leaks-password-for-monopoly-vip-database-to-winners/){:target="_blank" rel="noopener"}

> ug in the McDonald's Monopoly VIP game in the United Kingdom caused the login names and passwords for the game's database to be sent to all winners. [...]
