Title: Researchers finger new APT group, FamousSparrow, for hotel attacks
Date: 2021-09-23T10:00:35+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: 2021-09-23-researchers-finger-new-apt-group-famoussparrow-for-hotel-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/23/researchers_finger_new_apt_group/){:target="_blank" rel="noopener"}

> Espionage motive mooted in attacks which hit industry, government too Researchers at security specialist ESET claim to have found a shiny new advanced persistent threat (APT) group dubbed FamousSparrow - after discovering its custom backdoor, SparrowDoor, on hotels and government systems around the world.... [...]
