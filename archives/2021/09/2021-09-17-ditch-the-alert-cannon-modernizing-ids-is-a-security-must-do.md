Title: Ditch the Alert Cannon: Modernizing IDS is a Security Must-Do
Date: 2021-09-17T13:20:03+00:00
Author: Jeff Costlow
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Web Security
Slug: 2021-09-17-ditch-the-alert-cannon-modernizing-ids-is-a-security-must-do

[Source](https://threatpost.com/modernizing-ids-security/174789/){:target="_blank" rel="noopener"}

> Jeff Costlow, CISO at ExtraHop, makes the case for implementing next-gen intrusion-detection systems (NG-IDS) and retiring those noisy 90s compliance platforms. [...]
