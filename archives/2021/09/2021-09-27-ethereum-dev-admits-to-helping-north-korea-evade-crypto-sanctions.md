Title: Ethereum dev admits to helping North Korea evade crypto sanctions
Date: 2021-09-27T15:14:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-09-27-ethereum-dev-admits-to-helping-north-korea-evade-crypto-sanctions

[Source](https://www.bleepingcomputer.com/news/security/ethereum-dev-admits-to-helping-north-korea-evade-crypto-sanctions/){:target="_blank" rel="noopener"}

> Cryptocurrency expert Virgil Griffith pled guilty today to assisting the Democratic People's Republic of Korea in evading U.S. sanctions by conspiring to violate the International Emergency Economic Powers Act (IEEPA) and Executive Order 13466. [...]
