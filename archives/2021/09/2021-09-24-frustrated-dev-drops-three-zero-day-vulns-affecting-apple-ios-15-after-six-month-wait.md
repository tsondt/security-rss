Title: Frustrated dev drops three zero-day vulns affecting Apple iOS 15 after six-month wait
Date: 2021-09-24T19:43:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-24-frustrated-dev-drops-three-zero-day-vulns-affecting-apple-ios-15-after-six-month-wait

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/24/apple_zeroday/){:target="_blank" rel="noopener"}

> Security Bounty program scolded for broken promises Upset with Apple's handling of its Security Bounty program, a bug researcher has released proof-of-concept exploit code for three zero-day vulnerabilities in Apple's newly released iOS 15 mobile operating system.... [...]
