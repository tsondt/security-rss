Title: Raider: A tool to test authentication in web applications
Date: 2021-09-06T14:45:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-06-raider-a-tool-to-test-authentication-in-web-applications

[Source](https://portswigger.net/daily-swig/raider-a-tool-to-test-authentication-in-web-applications){:target="_blank" rel="noopener"}

> Open source project aims to offer ‘unlimited flexibility’ for security researchers [...]
