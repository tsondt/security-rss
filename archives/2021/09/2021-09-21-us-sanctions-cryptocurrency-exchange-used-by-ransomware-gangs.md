Title: US sanctions cryptocurrency exchange used by ransomware gangs
Date: 2021-09-21T12:35:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2021-09-21-us-sanctions-cryptocurrency-exchange-used-by-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-cryptocurrency-exchange-used-by-ransomware-gangs/){:target="_blank" rel="noopener"}

> The US Treasury Department announced the first-ever sanctions against a cryptocurrency exchange, the Russian-linked Suex, for facilitating ransom transactions for ransomware gangs and helping them evade sanctions. [...]
