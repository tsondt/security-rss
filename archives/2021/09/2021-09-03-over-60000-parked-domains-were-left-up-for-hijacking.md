Title: Over 60,000 parked domains were left up for hijacking
Date: 2021-09-03T03:00:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2021-09-03-over-60000-parked-domains-were-left-up-for-hijacking

[Source](https://www.bleepingcomputer.com/news/security/over-60-000-parked-domains-were-left-up-for-hijacking/){:target="_blank" rel="noopener"}

> Domain registrar MarkMonitor had left more than 60,000 parked domains vulnerable to domain hijacking. The parked domains were seen pointing to nonexistent Amazon S3 bucket addresses, hinting that there existed a domain takeover weakness. [...]
