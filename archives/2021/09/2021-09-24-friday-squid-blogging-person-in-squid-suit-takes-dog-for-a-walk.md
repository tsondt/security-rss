Title: Friday Squid Blogging: Person in Squid Suit Takes Dog for a Walk
Date: 2021-09-24T21:20:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2021-09-24-friday-squid-blogging-person-in-squid-suit-takes-dog-for-a-walk

[Source](https://www.schneier.com/blog/archives/2021/09/friday-squid-blogging-person-in-squid-suit-takes-dog-for-a-walk.html){:target="_blank" rel="noopener"}

> No, I don’t understand it, either. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
