Title: Top Steps for Ransomware Recovery and Preparation
Date: 2021-09-10T19:37:45+00:00
Author: Alex Restrepo
Category: Threatpost
Tags: News
Slug: 2021-09-10-top-steps-for-ransomware-recovery-and-preparation

[Source](https://threatpost.com/top-steps-ransomware-recovery-preparation/169378/){:target="_blank" rel="noopener"}

> Alex Restrepo, Virtual Data Center Solutions at Veritas Technologies, discusses post-attack restoration options, and how to prepare for another one in the future. [...]
