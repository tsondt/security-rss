Title: LockBit gang leaks Bangkok Airways data, hits Accenture customers
Date: 2021-09-01T03:36:46-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-01-lockbit-gang-leaks-bangkok-airways-data-hits-accenture-customers

[Source](https://www.bleepingcomputer.com/news/security/lockbit-gang-leaks-bangkok-airways-data-hits-accenture-customers/){:target="_blank" rel="noopener"}

> Bangkok Airways, a major airline company in Thailand, confirmed it was the victim of a cyberattack earlier this month that compromised personal data of passengers. [...]
