Title: How to fix printers asking for admins creds after PrintNightmare patch
Date: 2021-09-17T06:52:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-17-how-to-fix-printers-asking-for-admins-creds-after-printnightmare-patch

[Source](https://www.bleepingcomputer.com/news/microsoft/how-to-fix-printers-asking-for-admins-creds-after-printnightmare-patch/){:target="_blank" rel="noopener"}

> Some printers will request administrator credentials every time users try to print in Windows Point and Print environments due to a known issue caused by KB5005033 or later security updates addressing the PrintNightmare vulnerability. [...]
