Title: History of the HX-63 Rotor Machine
Date: 2021-09-03T15:19:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;encryption;history of cryptography
Slug: 2021-09-03-history-of-the-hx-63-rotor-machine

[Source](https://www.schneier.com/blog/archives/2021/09/history-of-the-hx-63-rotor-machine.html){:target="_blank" rel="noopener"}

> Jon D. Paul has written the fascinating story of the HX-63, a super-complicated electromechanical rotor cipher machine made by Crypto AG. [...]
