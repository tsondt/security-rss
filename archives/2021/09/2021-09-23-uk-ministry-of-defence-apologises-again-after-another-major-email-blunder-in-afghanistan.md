Title: UK Ministry of Defence apologises – again – after another major email blunder in Afghanistan
Date: 2021-09-23T13:00:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-23-uk-ministry-of-defence-apologises-again-after-another-major-email-blunder-in-afghanistan

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/23/afghan_email_fail_ministry_defence/){:target="_blank" rel="noopener"}

> This time affecting candidates for potential relocation A second leak of Afghan interpreters' personal data was reportedly committed by the Ministry of Defence, raising further questions about the ministry's commitment to the safety of people in Afghanistan, some of whom are its own former employees.... [...]
