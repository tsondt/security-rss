Title: REvil’s Back; Coder Fat-Fingered Away Its Decryptor Key?
Date: 2021-09-13T18:59:22+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware
Slug: 2021-09-13-revils-back-coder-fat-fingered-away-its-decryptor-key

[Source](https://threatpost.com/revil-back-coder-decryptor-key/169403/){:target="_blank" rel="noopener"}

> How did Kaseya get a universal decryptor after a mind-bogglingly big ransomware attack? A REvil coder misclicked, generated & issued it, and “That’s how we sh*t ourselves.” [...]
