Title: Identifying Computer-Generated Faces
Date: 2021-09-15T15:31:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;deep fake;identification
Slug: 2021-09-15-identifying-computer-generated-faces

[Source](https://www.schneier.com/blog/archives/2021/09/identifying-computer-generated-faces.html){:target="_blank" rel="noopener"}

> It’s the eyes : The researchers note that in many cases, users can simply zoom in on the eyes of a person they suspect may not be real to spot the pupil irregularities. They also note that it would not be difficult to write software to spot such errors and for social media sites to use it to remove such content. Unfortunately, they also note that now that such irregularities have been identified, the people creating the fake pictures can simply add a feature to ensure the roundness of pupils. And the arms race continues.... Research paper. [...]
