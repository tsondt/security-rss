Title: This is AUKUS for China – US, UK, Australia reveal defence tech-sharing pact
Date: 2021-09-16T03:27:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-16-this-is-aukus-for-china-us-uk-australia-reveal-defence-tech-sharing-pact

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/16/aukus_defence_pact/){:target="_blank" rel="noopener"}

> Will build nuke-powered subs together and share cyber, AI, quantum and mysterious 'undersea capabilities' tech Australia, the United States of America, and the United Kingdom have signed a new defence and technology-sharing pact.... [...]
