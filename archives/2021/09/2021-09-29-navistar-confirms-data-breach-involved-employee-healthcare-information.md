Title: Navistar confirms data breach involved employee healthcare information
Date: 2021-09-29T14:19:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-29-navistar-confirms-data-breach-involved-employee-healthcare-information

[Source](https://portswigger.net/daily-swig/navistar-confirms-data-breach-involved-employee-healthcare-information){:target="_blank" rel="noopener"}

> US truck manufacturer breaks bad news to employees and retired workers [...]
