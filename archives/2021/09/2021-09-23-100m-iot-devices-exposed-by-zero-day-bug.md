Title: 100M IoT Devices Exposed By Zero-Day Bug
Date: 2021-09-23T18:35:31+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;IoT;Vulnerabilities
Slug: 2021-09-23-100m-iot-devices-exposed-by-zero-day-bug

[Source](https://threatpost.com/100m-iot-devices-zero-day-bug/174963/){:target="_blank" rel="noopener"}

> A high-severity vulnerability could cause system crashes, knocking out sensors, medical equipment and more. [...]
