Title: Fake 'BT' caller fleeces elderly victim of £30k in APP app scam
Date: 2021-09-27T12:58:09+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-09-27-fake-bt-caller-fleeces-elderly-victim-of-30k-in-app-app-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/27/fake_bt_caller_fraud/){:target="_blank" rel="noopener"}

> That's authorised push payment – where they get the mark to make the transfer Police have issued an urgent warning after an elderly man was scammed out of £30,000 by phone fraudsters pretending to be from BT.... [...]
