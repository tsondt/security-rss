Title: Translated Conti ransomware playbook gives insight into attacks
Date: 2021-09-02T17:10:45-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-02-translated-conti-ransomware-playbook-gives-insight-into-attacks

[Source](https://www.bleepingcomputer.com/news/security/translated-conti-ransomware-playbook-gives-insight-into-attacks/){:target="_blank" rel="noopener"}

> Almost a month after a disgruntled Conti affiliate leaked the gang's attack playbook, security researchers shared a translated variant that clarifies on any misinterpretation caused by automated translation. [...]
