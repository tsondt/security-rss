Title: Hacker-made Linux Cobalt Strike beacon used in ongoing attacks
Date: 2021-09-13T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux
Slug: 2021-09-13-hacker-made-linux-cobalt-strike-beacon-used-in-ongoing-attacks

[Source](https://www.bleepingcomputer.com/news/security/hacker-made-linux-cobalt-strike-beacon-used-in-ongoing-attacks/){:target="_blank" rel="noopener"}

> An unofficial Cobalt Strike Beacon Linux version made by unknown threat actors from scratch has been spotted by security researchers while actively used in attacks targeting organizations worldwide. [...]
