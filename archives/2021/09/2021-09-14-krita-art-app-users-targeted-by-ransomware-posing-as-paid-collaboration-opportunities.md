Title: Krita art app users targeted by ransomware posing as paid 'collaboration' opportunities
Date: 2021-09-14T19:27:12+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: 2021-09-14-krita-art-app-users-targeted-by-ransomware-posing-as-paid-collaboration-opportunities

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/14/krita_users_targeted_by_ransomware/){:target="_blank" rel="noopener"}

> Artists advised to delete emails asking them to download 'media bundle' Krita, an open-source cross-platform digital painting application, has become the latest victim of ransomware – but rather than being attacked directly, its name is being used to spread malware among users via emails offering advertising revenue.... [...]
