Title: NFT Collector Tricked into Buying Fake Banksy
Date: 2021-09-02T21:38:40+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2021-09-02-nft-collector-tricked-into-buying-fake-banksy

[Source](https://threatpost.com/nft-collector-tricked-into-buying-fake-banksy/169179/){:target="_blank" rel="noopener"}

> An attacker breached the site of famed street artist Banksy to host a fraudulent NFT auction but then gave back the money. [...]
