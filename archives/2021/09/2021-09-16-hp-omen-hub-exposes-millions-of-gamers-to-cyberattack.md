Title: HP Omen Hub Exposes Millions of Gamers to Cyberattack
Date: 2021-09-16T12:01:55+00:00
Author: Becky Bracken
Category: Threatpost
Tags: IoT;Vulnerabilities;Web Security
Slug: 2021-09-16-hp-omen-hub-exposes-millions-of-gamers-to-cyberattack

[Source](https://threatpost.com/hp-omen-hub-gamers-cyberattack/169739/){:target="_blank" rel="noopener"}

> A driver privilege-escalation bug gives attackers kernel-mode access to millions of PCs used for gaming. [...]
