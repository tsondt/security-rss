Title: MikroTik shares info on securing routers hit by massive Mēris botnet
Date: 2021-09-15T14:57:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-15-mikrotik-shares-info-on-securing-routers-hit-by-massive-mēris-botnet

[Source](https://www.bleepingcomputer.com/news/security/mikrotik-shares-info-on-securing-routers-hit-by-massive-m-ris-botnet/){:target="_blank" rel="noopener"}

> Latvian network equipment manufacturer MikroTik has shared details on how customers can secure and clean routers compromised by the massive Mēris DDoS botnet over the summer. [...]
