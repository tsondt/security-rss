Title: New York State vaccine pass shortcomings offer lessons for other coronavirus app developers
Date: 2021-09-08T14:55:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-08-new-york-state-vaccine-pass-shortcomings-offer-lessons-for-other-coronavirus-app-developers

[Source](https://portswigger.net/daily-swig/new-york-state-vaccine-pass-shortcomings-offer-lessons-for-other-coronavirus-app-developers){:target="_blank" rel="noopener"}

> ‘Incomplete threat modelling’ blamed for credential forgery vulnerability [...]
