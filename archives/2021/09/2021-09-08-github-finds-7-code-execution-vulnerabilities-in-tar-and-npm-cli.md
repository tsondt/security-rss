Title: GitHub finds 7 code execution vulnerabilities in 'tar' and npm CLI
Date: 2021-09-08T23:37:14-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Software
Slug: 2021-09-08-github-finds-7-code-execution-vulnerabilities-in-tar-and-npm-cli

[Source](https://www.bleepingcomputer.com/news/security/github-finds-7-code-execution-vulnerabilities-in-tar-and-npm-cli/){:target="_blank" rel="noopener"}

> GitHub security team has identified several high-severity vulnerabilities in npm packages, "tar" and "@npmcli/arborist," used by npm CLI. The tar package receives 20 million weekly downloads on average, whereas arborist gets downloaded over 300,000 times every week. [...]
