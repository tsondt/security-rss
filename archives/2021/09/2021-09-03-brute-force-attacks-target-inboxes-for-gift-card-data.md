Title: Brute-Force Attacks Target Inboxes for Gift Card Data
Date: 2021-09-03T11:31:13+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks
Slug: 2021-09-03-brute-force-attacks-target-inboxes-for-gift-card-data

[Source](https://threatpost.com/attacks-inboxes-gift-card/169187/){:target="_blank" rel="noopener"}

> Cybercriminal enterprise is mass testing millions of usernames and passwords per day in a hunt for loyalty card data. [...]
