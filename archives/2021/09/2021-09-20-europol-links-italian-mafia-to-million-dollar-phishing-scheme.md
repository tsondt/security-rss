Title: Europol links Italian Mafia to million-dollar phishing scheme
Date: 2021-09-20T08:41:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-20-europol-links-italian-mafia-to-million-dollar-phishing-scheme

[Source](https://www.bleepingcomputer.com/news/security/europol-links-italian-mafia-to-million-dollar-phishing-scheme/){:target="_blank" rel="noopener"}

> In collaboration with Europol and Eurojust, European law enforcement dismantled an extensive network of cybercriminals linked to the Italian Mafia that was able to defraud their victims of roughly €10 million ($11.7 million) last year alone. [...]
