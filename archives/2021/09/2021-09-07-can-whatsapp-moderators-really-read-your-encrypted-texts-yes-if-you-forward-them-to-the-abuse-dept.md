Title: Can WhatsApp moderators really read your encrypted texts? Yes ... if you forward them to the abuse dept
Date: 2021-09-07T21:13:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-07-can-whatsapp-moderators-really-read-your-encrypted-texts-yes-if-you-forward-them-to-the-abuse-dept

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/07/whatsapp_privacy_propublica/){:target="_blank" rel="noopener"}

> Where did people think spam and harassment reports were going? Facebook's WhatsApp states its messages are protected by the Signal encryption protocol. A report published today by investigative non-profit ProPublica contends that WhatsApp communication is less private than users understand or expect.... [...]
