Title: Conti Ransomware Expands Ability to Blow Up Backups
Date: 2021-09-29T15:43:52+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2021-09-29-conti-ransomware-expands-ability-to-blow-up-backups

[Source](https://threatpost.com/conti-ransomware-backups/175114/){:target="_blank" rel="noopener"}

> The Conti ransomware gang has developed novel tactics to demolish backups, especially the Veeam recovery software. [...]
