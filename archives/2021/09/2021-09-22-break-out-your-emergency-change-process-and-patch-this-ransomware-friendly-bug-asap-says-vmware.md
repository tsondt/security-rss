Title: Break out your emergency change process and patch this ransomware-friendly bug ASAP, says VMware
Date: 2021-09-22T00:45:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-22-break-out-your-emergency-change-process-and-patch-this-ransomware-friendly-bug-asap-says-vmware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/22/vmware_emergency_vcenter_patch_recommendation/){:target="_blank" rel="noopener"}

> File upload vuln lets miscreants hijack vCenter Server VMware has disclosed a critical bug in its flagship vSphere and vCenter products and urged users to drop everything and patch it. The virtualization giant also offered a workaround.... [...]
