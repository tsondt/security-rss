Title: Give put-upon infosec bods professional recognition to keep them working for you, says chartered institute
Date: 2021-09-29T10:00:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-29-give-put-upon-infosec-bods-professional-recognition-to-keep-them-working-for-you-says-chartered-institute

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/29/ciisec_amanda_finch_interview/){:target="_blank" rel="noopener"}

> Nice ideas, if anyone adopts them Interview As the UK infosec industry prepares for government initiatives intended to expand the sector, how should existing companies keep skilled professionals from jumping ship? Amanda Finch, CEO of the Chartered Institute of Information Security, tells us a thing or two about what she thinks works.... [...]
