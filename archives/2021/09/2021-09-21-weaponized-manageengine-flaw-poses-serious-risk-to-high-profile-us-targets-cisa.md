Title: Weaponized ManageEngine flaw poses ‘serious risk’ to high-profile US targets – CISA
Date: 2021-09-21T15:15:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-21-weaponized-manageengine-flaw-poses-serious-risk-to-high-profile-us-targets-cisa

[Source](https://portswigger.net/daily-swig/weaponized-manageengine-flaw-poses-serious-risk-to-high-profile-us-targets-cisa){:target="_blank" rel="noopener"}

> Warning from US government agency urges prompt triage [...]
