Title: SonicWall fixes critical bug allowing SMA 100 device takeover
Date: 2021-09-24T02:19:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-24-sonicwall-fixes-critical-bug-allowing-sma-100-device-takeover

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-fixes-critical-bug-allowing-sma-100-device-takeover/){:target="_blank" rel="noopener"}

> SonicWall has patched a critical security flaw impacting several Secure Mobile Access (SMA) 100 series products that can let unauthenticated attackers remotely gain admin access on targeted devices. [...]
