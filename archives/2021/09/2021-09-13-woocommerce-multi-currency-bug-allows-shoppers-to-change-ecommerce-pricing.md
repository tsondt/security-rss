Title: WooCommerce Multi Currency Bug Allows Shoppers to Change eCommerce Pricing
Date: 2021-09-13T18:08:10+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-09-13-woocommerce-multi-currency-bug-allows-shoppers-to-change-ecommerce-pricing

[Source](https://threatpost.com/woocommerce-multi-currency-bug-pricing/169394/){:target="_blank" rel="noopener"}

> The security vulnerability can be exploited with a malicious CSV file. [...]
