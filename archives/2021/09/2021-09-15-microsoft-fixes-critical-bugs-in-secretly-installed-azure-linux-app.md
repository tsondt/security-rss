Title: Microsoft fixes critical bugs in secretly installed Azure Linux app
Date: 2021-09-15T17:05:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-15-microsoft-fixes-critical-bugs-in-secretly-installed-azure-linux-app

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-critical-bugs-in-secretly-installed-azure-linux-app/){:target="_blank" rel="noopener"}

> Microsoft has addressed four critical vulnerabilities collectively known as OMIGOD, found in the Open Management Infrastructure (OMI) software agent silently installed on Azure Linux machines accounting for more than half of Azure instances. [...]
