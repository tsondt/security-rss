Title: What Ragnar Locker Got Wrong About Ransomware Negotiators – Podcast
Date: 2021-09-08T21:14:06+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Podcasts;Web Security
Slug: 2021-09-08-what-ragnar-locker-got-wrong-about-ransomware-negotiators-podcast

[Source](https://threatpost.com/ragnar-locker-ransomware-negotiators/169292/){:target="_blank" rel="noopener"}

> There are a lot of "tells" that the ransomware group doesn't understand how negotiators work, despite threatening to dox data if victims call for help. [...]
