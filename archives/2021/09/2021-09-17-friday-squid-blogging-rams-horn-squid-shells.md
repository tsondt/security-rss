Title: Friday Squid Blogging: Ram’s Horn Squid Shells
Date: 2021-09-17T21:14:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-09-17-friday-squid-blogging-rams-horn-squid-shells

[Source](https://www.schneier.com/blog/archives/2021/09/friday-squid-blogging-rams-horn-squid-shells.html){:target="_blank" rel="noopener"}

> You can find ram’s horn squid shells on beaches in Texas (and presumably elsewhere). As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
