Title: Hackers Are Going ‘Deep-Sea Phishing,’ So What Can You Do About It?
Date: 2021-09-21T17:49:24+00:00
Author: Nick Kael
Category: Threatpost
Tags: InfoSec Insider;Malware;Web Security
Slug: 2021-09-21-hackers-are-going-deep-sea-phishing-so-what-can-you-do-about-it

[Source](https://threatpost.com/hackers-deep-sea-phishing/174868/){:target="_blank" rel="noopener"}

> Nick Kael, CTO at Ericom, discusses how phishing is gaining sophistication and what it means for businesses. [...]
