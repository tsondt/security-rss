Title: Proton welcomes Sir Tim Berners-Lee to its advisory board – as ProtonMail suffers a privacy backlash
Date: 2021-09-08T18:55:09+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: 2021-09-08-proton-welcomes-sir-tim-berners-lee-to-its-advisory-board-as-protonmail-suffers-a-privacy-backlash

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/08/proton_welcomes_sir_tim_bernerslee/){:target="_blank" rel="noopener"}

> 'I am a firm supporter of privacy,' Sir Tim declares - even as the service is lambasted over IP logging Privacy-centric communications specialist Proton, best known for its ProtonMail encrypted email platform, has announced the appointment of web daddy Sir Tim Berners-Lee to its advisory board.... [...]
