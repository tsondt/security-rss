Title: Bluetooth Bugs Open Billions of Devices to DoS, Code Execution
Date: 2021-09-02T18:32:18+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Vulnerabilities
Slug: 2021-09-02-bluetooth-bugs-open-billions-of-devices-to-dos-code-execution

[Source](https://threatpost.com/bluetooth-bugs-dos-code-execution/169159/){:target="_blank" rel="noopener"}

> The BrakTooth set of security vulnerabilities impacts at least 11 vendors' chipsets. [...]
