Title: Phishing-as-a-service operation uses double theft to boost profits
Date: 2021-09-22T09:43:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-22-phishing-as-a-service-operation-uses-double-theft-to-boost-profits

[Source](https://www.bleepingcomputer.com/news/microsoft/phishing-as-a-service-operation-uses-double-theft-to-boost-profits/){:target="_blank" rel="noopener"}

> Microsoft says BulletProofLink, a large-scale phishing-as-a-service operation it spotted while investigating recent phishing attacks, is the driving force behind many phishing campaigns that have targeted many corporate organizations lately. [...]
