Title: Zero-day flaws in IoT baby monitors could give attackers access to camera feeds
Date: 2021-09-02T16:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-02-zero-day-flaws-in-iot-baby-monitors-could-give-attackers-access-to-camera-feeds

[Source](https://portswigger.net/daily-swig/zero-day-flaws-in-iot-baby-monitors-could-give-attackers-access-to-camera-feeds){:target="_blank" rel="noopener"}

> Customers should ‘stop using devices altogether’, say researchers [...]
