Title: BlackMatter Strikes Iowa Farmers Cooperative, Demands $5.9M Ransom
Date: 2021-09-21T13:14:02+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: 2021-09-21-blackmatter-strikes-iowa-farmers-cooperative-demands-59m-ransom

[Source](https://threatpost.com/blackmatter-strikes-iowa-farmers-cooperative-demands-5-9m-ransom/174846/){:target="_blank" rel="noopener"}

> Critical infrastructure appears to be targeted in latest ransomware attack, diminishing the hopes of governments to curb such attacks. [...]
