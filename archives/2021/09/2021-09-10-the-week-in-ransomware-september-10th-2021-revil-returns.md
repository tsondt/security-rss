Title: The Week in Ransomware - September 10th 2021 - REvil returns
Date: 2021-09-10T16:34:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-10-the-week-in-ransomware-september-10th-2021-revil-returns

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-10th-2021-revil-returns/){:target="_blank" rel="noopener"}

> This week marked the return of the notorious REvil ransomware group, who disappeared in July after conducting a massive attack using a Kaseya zero-day vulnerability. [...]
