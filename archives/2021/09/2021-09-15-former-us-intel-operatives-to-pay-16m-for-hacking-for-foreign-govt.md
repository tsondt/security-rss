Title: Former U.S. intel operatives to pay $1.6M for hacking for foreign govt
Date: 2021-09-15T19:22:27-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-15-former-us-intel-operatives-to-pay-16m-for-hacking-for-foreign-govt

[Source](https://www.bleepingcomputer.com/news/security/former-us-intel-operatives-to-pay-16m-for-hacking-for-foreign-govt/){:target="_blank" rel="noopener"}

> The U.S. government has entered a Deferred Prosecution Agreement (DPA) with three former intelligence operatives to resolve criminal charges relating to their offering of hacking services to a foreign government. [...]
