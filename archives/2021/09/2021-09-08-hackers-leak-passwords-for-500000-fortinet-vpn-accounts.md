Title: Hackers leak passwords for 500,000 Fortinet VPN accounts
Date: 2021-09-08T15:03:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-08-hackers-leak-passwords-for-500000-fortinet-vpn-accounts

[Source](https://www.bleepingcomputer.com/news/security/hackers-leak-passwords-for-500-000-fortinet-vpn-accounts/){:target="_blank" rel="noopener"}

> A threat actor has leaked a list of almost 500,000 Fortinet VPN login names and passwords that were allegedly scraped from exploitable devices last summer. [...]
