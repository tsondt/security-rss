Title: AT&T lost $200M in seven years to illegal phone unlocking scheme
Date: 2021-09-19T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-19-att-lost-200m-in-seven-years-to-illegal-phone-unlocking-scheme

[Source](https://www.bleepingcomputer.com/news/security/atandt-lost-200m-in-seven-years-to-illegal-phone-unlocking-scheme/){:target="_blank" rel="noopener"}

> A Pakistani fraudster was sentenced earlier this week to 12 years in prison after AT&T, the world's largest telecommunications company, lost over $200 million after he and his co-conspirators coordinated a seven year scheme that led to the fraudulent unlocking of almost 2 million phones. [...]
