Title: MyRepublic Data Breach Raises Data-Protection Questions
Date: 2021-09-10T20:17:59+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Hacks
Slug: 2021-09-10-myrepublic-data-breach-raises-data-protection-questions

[Source](https://threatpost.com/myrepublic-data-breach-protection/169382/){:target="_blank" rel="noopener"}

> The incident raises considerations for security for critical data housed in third-party infrastructure, researchers say. [...]
