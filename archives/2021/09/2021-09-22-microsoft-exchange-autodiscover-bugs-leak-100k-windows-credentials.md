Title: Microsoft Exchange Autodiscover bugs leak 100K Windows credentials
Date: 2021-09-22T09:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-22-microsoft-exchange-autodiscover-bugs-leak-100k-windows-credentials

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-exchange-autodiscover-bugs-leak-100k-windows-credentials/){:target="_blank" rel="noopener"}

> Bugs in the implementation of Microsoft Exchange's Autodiscover feature have leaked approximately 100,000 login names and passwords for Windows domains worldwide. [...]
