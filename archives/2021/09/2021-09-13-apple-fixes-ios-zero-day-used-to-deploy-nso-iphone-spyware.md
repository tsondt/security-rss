Title: Apple fixes iOS zero-day used to deploy NSO iPhone spyware
Date: 2021-09-13T15:10:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Apple;Security
Slug: 2021-09-13-apple-fixes-ios-zero-day-used-to-deploy-nso-iphone-spyware

[Source](https://www.bleepingcomputer.com/news/apple/apple-fixes-ios-zero-day-used-to-deploy-nso-iphone-spyware/){:target="_blank" rel="noopener"}

> Apple has released security updates to fix two zero-day vulnerabilities that have been seen exploited in the wild to attack iPhones and Macs. One is known to be used to install the Pegasus spyware on iPhones. [...]
