Title: TrickBot gang developer arrested when trying to leave Korea
Date: 2021-09-06T11:24:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2021-09-06-trickbot-gang-developer-arrested-when-trying-to-leave-korea

[Source](https://www.bleepingcomputer.com/news/security/trickbot-gang-developer-arrested-when-trying-to-leave-korea/){:target="_blank" rel="noopener"}

> An alleged Russian developer for the notorious TrickBot malware gang was arrested in South Korea after attempting to leave the country. [...]
