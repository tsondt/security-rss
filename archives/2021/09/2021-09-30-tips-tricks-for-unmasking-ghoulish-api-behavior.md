Title: Tips & Tricks for Unmasking Ghoulish API Behavior
Date: 2021-09-30T17:56:05+00:00
Author: Jason Kent
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Vulnerabilities;Web Security
Slug: 2021-09-30-tips-tricks-for-unmasking-ghoulish-api-behavior

[Source](https://threatpost.com/unmasking-ghoulish-api-behavior/175253/){:target="_blank" rel="noopener"}

> Jason Kent, hacker-in-residence at Cequence Security, discusses how to track user-agent connections to mobile and desktop APIs, to spot malicious activity. [...]
