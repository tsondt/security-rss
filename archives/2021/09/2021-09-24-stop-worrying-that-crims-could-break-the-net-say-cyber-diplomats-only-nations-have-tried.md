Title: Stop worrying that crims could break the 'net, say cyber-diplomats – only nations have tried
Date: 2021-09-24T07:28:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-24-stop-worrying-that-crims-could-break-the-net-say-cyber-diplomats-only-nations-have-tried

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/24/gcsc_norm_on_protecting_internet_core/){:target="_blank" rel="noopener"}

> Global Commission on the Stability of Cyberspace is a bit miffed its 'Don't attack the internet core' norm is misunderstood The Global Commission on the Stability of Cyberspace (GCSC) is worried its guidance on preventing the internet and all it connects becoming a casualty of war is being misinterpreted.... [...]
