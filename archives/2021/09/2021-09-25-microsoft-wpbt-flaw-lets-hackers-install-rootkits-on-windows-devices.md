Title: Microsoft WPBT flaw lets hackers install rootkits on Windows devices
Date: 2021-09-25T11:16:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-09-25-microsoft-wpbt-flaw-lets-hackers-install-rootkits-on-windows-devices

[Source](https://www.bleepingcomputer.com/news/security/microsoft-wpbt-flaw-lets-hackers-install-rootkits-on-windows-devices/){:target="_blank" rel="noopener"}

> Security researchers have found a flaw in the Microsoft Windows Platform Binary Table (WPBT) that could be exploited in easy attacks to install rootkits on all Windows computers shipped since 2012. [...]
