Title: Securely extend and access on-premises Active Directory domain controllers in AWS
Date: 2021-09-29T16:06:41+00:00
Author: Mangesh Budkule
Category: AWS Security
Tags: Advanced (300);Amazon EC2;Security, Identity, & Compliance;Active Directory;AD;AD PORTS;AWS Systems Manager Session Manager;Bastion host;RDGW;Remote desktop gateway;Security Blog
Slug: 2021-09-29-securely-extend-and-access-on-premises-active-directory-domain-controllers-in-aws

[Source](https://aws.amazon.com/blogs/security/securely-extend-and-access-on-premises-active-directory-domain-controllers-in-aws/){:target="_blank" rel="noopener"}

> If you have an on-premises Windows Server Active Directory infrastructure, it’s important to plan carefully how to extend it into Amazon Web Services (AWS) when you’re migrating or implementing cloud-based applications. In this scenario, existing applications require Active Directory for authentication and identity management. When you migrate these applications to the cloud, having a locally [...]
