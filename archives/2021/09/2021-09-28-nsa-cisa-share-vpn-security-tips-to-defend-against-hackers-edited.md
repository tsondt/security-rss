Title: NSA, CISA share VPN security tips to defend against hackers (edited)
Date: 2021-09-28T17:45:26-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-28-nsa-cisa-share-vpn-security-tips-to-defend-against-hackers-edited

[Source](https://www.bleepingcomputer.com/news/security/nsa-cisa-share-vpn-security-tips-to-defend-against-hackers-edited/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) and the National Security Agency (NSA) have released guidance for hardening the security of virtual private network (VPN) solutions. [...]
