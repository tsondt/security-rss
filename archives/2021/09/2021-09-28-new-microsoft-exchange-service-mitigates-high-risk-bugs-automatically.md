Title: New Microsoft Exchange service mitigates high-risk bugs automatically
Date: 2021-09-28T07:30:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2021-09-28-new-microsoft-exchange-service-mitigates-high-risk-bugs-automatically

[Source](https://www.bleepingcomputer.com/news/microsoft/new-microsoft-exchange-service-mitigates-high-risk-bugs-automatically/){:target="_blank" rel="noopener"}

> Microsoft has added a new Exchange Server feature that automatically applies interim mitigations for high-risk (and likely actively exploited) security flaws to secure on-premises servers against incoming attacks and give admins more time to apply security updates. [...]
