Title: Office 365 to let admins block Active Content on Trusted Docs
Date: 2021-09-05T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2021-09-05-office-365-to-let-admins-block-active-content-on-trusted-docs

[Source](https://www.bleepingcomputer.com/news/security/office-365-to-let-admins-block-active-content-on-trusted-docs/){:target="_blank" rel="noopener"}

> Microsoft plans to allow Office 365 admins ensure that end-users can't ignore organization-wide policies set up to block active content on Trusted Documents. [...]
