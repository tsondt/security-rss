Title: Credential leak fears raised following security breach at Travis CI
Date: 2021-09-15T12:40:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-15-credential-leak-fears-raised-following-security-breach-at-travis-ci

[Source](https://portswigger.net/daily-swig/credential-leak-fears-raised-following-security-breach-at-travis-ci){:target="_blank" rel="noopener"}

> DevOps firm slammed for ‘abysmal’ incident response [...]
