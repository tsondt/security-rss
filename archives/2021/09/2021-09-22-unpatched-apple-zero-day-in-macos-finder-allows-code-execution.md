Title: Unpatched Apple Zero-Day in macOS Finder Allows Code Execution
Date: 2021-09-22T17:22:53+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-09-22-unpatched-apple-zero-day-in-macos-finder-allows-code-execution

[Source](https://threatpost.com/unpatched-apple-zero-day-code-execution/174915/){:target="_blank" rel="noopener"}

> All a user needs to do is click on an email attachment, and boom – the code is silently executed without the victim knowing. It affects Big Sur and prior versions of macOS. [...]
