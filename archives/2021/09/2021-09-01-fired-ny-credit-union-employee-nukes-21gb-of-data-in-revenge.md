Title: Fired NY credit union employee nukes 21GB of data in revenge
Date: 2021-09-01T11:23:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-01-fired-ny-credit-union-employee-nukes-21gb-of-data-in-revenge

[Source](https://www.bleepingcomputer.com/news/security/fired-ny-credit-union-employee-nukes-21gb-of-data-in-revenge/){:target="_blank" rel="noopener"}

> Juliana Barile, the former employee of a New York credit union, pleaded guilty to accessing the financial institution's computer systems without authorization and destroying over 21 gigabytes of data in revenge. [...]
