Title: BladeHawk Attackers Target Kurds with Android Apps
Date: 2021-09-09T11:26:58+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security;Privacy;Web Security
Slug: 2021-09-09-bladehawk-attackers-target-kurds-with-android-apps

[Source](https://threatpost.com/bladehawk-attackers-kurds-android/169300/){:target="_blank" rel="noopener"}

> Pro-Kurd Facebook profiles deliver '888 RAT' and 'SpyNote' trojans, masked as legitimate apps, to perform mobile espionage. [...]
