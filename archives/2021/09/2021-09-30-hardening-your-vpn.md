Title: Hardening Your VPN
Date: 2021-09-30T16:51:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;advanced persistent threats;NSA;VPN
Slug: 2021-09-30-hardening-your-vpn

[Source](https://www.schneier.com/blog/archives/2021/09/hardening-your-vpn.html){:target="_blank" rel="noopener"}

> The NSA and CISA have released a document on how to harden your VPN. [...]
