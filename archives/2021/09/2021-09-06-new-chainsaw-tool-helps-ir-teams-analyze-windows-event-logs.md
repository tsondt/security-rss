Title: New Chainsaw tool helps IR teams analyze Windows event logs
Date: 2021-09-06T13:42:08-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-06-new-chainsaw-tool-helps-ir-teams-analyze-windows-event-logs

[Source](https://www.bleepingcomputer.com/news/security/new-chainsaw-tool-helps-ir-teams-analyze-windows-event-logs/){:target="_blank" rel="noopener"}

> Incident responders and blue teams have a new tool called Chainsaw that speeds up searching through Windows event log records to identify threats. [...]
