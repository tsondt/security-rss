Title: Microsoft shares temp fix for ongoing Office 365 zero-day attacks
Date: 2021-09-07T15:36:51-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-07-microsoft-shares-temp-fix-for-ongoing-office-365-zero-day-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-temp-fix-for-ongoing-office-365-zero-day-attacks/){:target="_blank" rel="noopener"}

> Microsoft today shared mitigation for a remote code execution vulnerability in Windows that is being exploited in targeted attacks against Office 365 and Office 2019 on Windows 10. [...]
