Title: UK VoIP telco receives 'colossal ransom demand', reveals REvil cybercrooks suspected of 'organised' DDoS attacks on UK VoIP companies
Date: 2021-09-02T10:32:12+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-09-02-uk-voip-telco-receives-colossal-ransom-demand-reveals-revil-cybercrooks-suspected-of-organised-ddos-attacks-on-uk-voip-companies

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/02/uk_voip_telcos_revil_ransom/){:target="_blank" rel="noopener"}

> One firm hit with at least 2 attacks as outages continue Two UK VoIP operators have had their services disrupted over the last couple of days by ongoing, aggressive DDoS attacks.... [...]
