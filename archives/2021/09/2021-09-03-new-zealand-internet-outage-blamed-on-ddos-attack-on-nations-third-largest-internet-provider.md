Title: New Zealand internet outage blamed on DDoS attack on nation's third largest internet provider
Date: 2021-09-03T13:13:10+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: 2021-09-03-new-zealand-internet-outage-blamed-on-ddos-attack-on-nations-third-largest-internet-provider

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/03/nz_outage/){:target="_blank" rel="noopener"}

> Here in the UK, Sky broadband users back online Parts of New Zealand were cut off from the digital world today after a major local ISP was hit by an aggressive DDoS attack.... [...]
