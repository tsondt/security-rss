Title: FTC warns of extortionists targeting LGBTQ+ community on dating apps
Date: 2021-09-13T12:08:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-13-ftc-warns-of-extortionists-targeting-lgbtq-community-on-dating-apps

[Source](https://www.bleepingcomputer.com/news/security/ftc-warns-of-extortionists-targeting-lgbtq-plus-community-on-dating-apps/){:target="_blank" rel="noopener"}

> The US Federal Trade Commission (FTC) warns of extortion scammers targeting the LGBTQ+ community via online dating apps such as Grindr and Feeld. [...]
