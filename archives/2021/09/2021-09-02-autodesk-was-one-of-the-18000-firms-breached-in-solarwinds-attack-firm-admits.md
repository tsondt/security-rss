Title: Autodesk was one of the 18,000 firms breached in SolarWinds attack, firm admits
Date: 2021-09-02T17:33:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-02-autodesk-was-one-of-the-18000-firms-breached-in-solarwinds-attack-firm-admits

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/02/autodesk_solarwinds_hack_victim/){:target="_blank" rel="noopener"}

> Door was opened but nobody stepped inside, luckily Autodesk, makers of computer-aided design (CAD) software for manufacturing, has told the US stock market it was targeted as part of the the supply chain attack on SolarWinds' Orion software.... [...]
