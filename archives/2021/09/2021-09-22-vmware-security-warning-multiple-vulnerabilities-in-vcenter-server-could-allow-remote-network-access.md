Title: VMware security warning: Multiple vulnerabilities in vCenter Server could allow remote network access
Date: 2021-09-22T14:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-22-vmware-security-warning-multiple-vulnerabilities-in-vcenter-server-could-allow-remote-network-access

[Source](https://portswigger.net/daily-swig/vmware-security-warning-multiple-vulnerabilities-in-vcenter-server-could-allow-remote-network-access){:target="_blank" rel="noopener"}

> Several issues including one critical bug have been remedied in latest patch cycle [...]
