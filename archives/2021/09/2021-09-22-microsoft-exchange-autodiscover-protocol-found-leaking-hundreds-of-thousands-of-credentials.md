Title: Microsoft Exchange Autodiscover protocol found leaking hundreds of thousands of credentials
Date: 2021-09-22T13:00:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2021-09-22-microsoft-exchange-autodiscover-protocol-found-leaking-hundreds-of-thousands-of-credentials

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/22/microsoft_exchange_autodiscover_protocol_found/){:target="_blank" rel="noopener"}

> Email clients fail over to unexpected domains if they can't find the right resources A flaw in Microsoft's Autodiscover protocol, used to configure Exchange clients like Outlook, can cause user credentials to leak to miscreants in certain circumstances.... [...]
