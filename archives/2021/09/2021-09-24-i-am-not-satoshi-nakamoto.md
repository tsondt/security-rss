Title: I Am Not Satoshi Nakamoto
Date: 2021-09-24T19:05:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bitcoin;identification
Slug: 2021-09-24-i-am-not-satoshi-nakamoto

[Source](https://www.schneier.com/blog/archives/2021/09/i-am-not-satoshi-nakamoto.html){:target="_blank" rel="noopener"}

> This isn’t the first time I’ve received an e-mail like this: Hey! I’ve done my research and looked at a lot of facts and old forgotten archives. I know that you are Satoshi, I do not want to tell anyone about this. I just wanted to say that you created weapons of mass destruction where niches remained poor and the rich got richer! When bitcoin first appeared, I was small, and alas, my family lost everything on this, you won’t find an apple in the winter garden, people only need strength and money. Sorry for the English, I am from [...]
