Title: Tick, tick, tick … TikTok China just limited kids to 40 minutes' use each day
Date: 2021-09-20T01:14:46+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2021-09-20-tick-tick-tick-tiktok-china-just-limited-kids-to-40-minutes-use-each-day

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/20/douyin_youth_mode_time_limits/){:target="_blank" rel="noopener"}

> And added a bug bounty program to detect any holes in its 'youth mode' Douyin, the Chinese app known as TikTok outside the Middle Kingdom, has imposed limits on usage time for kids.... [...]
