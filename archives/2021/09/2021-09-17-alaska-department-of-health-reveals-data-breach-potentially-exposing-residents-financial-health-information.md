Title: Alaska Department of Health reveals data breach potentially exposing residents’ financial, health information
Date: 2021-09-17T15:23:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-17-alaska-department-of-health-reveals-data-breach-potentially-exposing-residents-financial-health-information

[Source](https://portswigger.net/daily-swig/alaska-department-of-health-reveals-data-breach-potentially-exposing-residents-financial-health-information){:target="_blank" rel="noopener"}

> Disclosure part of lengthy investigation into sophisticated attack that took place in May [...]
