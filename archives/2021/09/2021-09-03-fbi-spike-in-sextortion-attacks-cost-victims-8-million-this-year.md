Title: FBI: Spike in sextortion attacks cost victims $8 million this year
Date: 2021-09-03T07:36:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2021-09-03-fbi-spike-in-sextortion-attacks-cost-victims-8-million-this-year

[Source](https://www.bleepingcomputer.com/news/security/fbi-spike-in-sextortion-attacks-cost-victims-8-million-this-year/){:target="_blank" rel="noopener"}

> The FBI Internet Crime Complaint Center (IC3) has warned of a massive increase in sextortion complaints since the start of 2021, resulting in total financial losses of more than $8 million until the end of July. [...]
