Title: Latest web hacking tools – Q4 2021
Date: 2021-09-30T14:36:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-30-latest-web-hacking-tools-q4-2021

[Source](https://portswigger.net/daily-swig/latest-web-hacking-tools-q4-2021){:target="_blank" rel="noopener"}

> We take a look at the latest additions to security researchers’ armoury [...]
