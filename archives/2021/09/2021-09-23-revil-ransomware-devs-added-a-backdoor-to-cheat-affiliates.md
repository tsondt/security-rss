Title: REVil ransomware devs added a backdoor to cheat affiliates
Date: 2021-09-23T02:26:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-23-revil-ransomware-devs-added-a-backdoor-to-cheat-affiliates

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-devs-added-a-backdoor-to-cheat-affiliates/){:target="_blank" rel="noopener"}

> Cybercriminals are slowly realizing that the REvil ransomware operators have been hijacking ransom negotiations, to cut affiliates out of payments. [...]
