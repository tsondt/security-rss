Title: Ragnar Locker Gang Warns Victims Not to Call the FBI
Date: 2021-09-07T22:41:45+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2021-09-07-ragnar-locker-gang-warns-victims-not-to-call-the-fbi

[Source](https://threatpost.com/ragnar-locker-gang-dont-call-fbi-police/169266/){:target="_blank" rel="noopener"}

> Investigators/the FBI/ransomware negotiators just screw everything up, the ransomware gang said, threatening to publish files if victims look for help. [...]
