Title: KrebsOnSecurity Hit By Huge New IoT Botnet “Meris”
Date: 2021-09-10T18:12:44+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Akamai;Cambridge University;CloudFlare;DDoS;google;Meris;MikroTik;Project Shield;Qrator;Richard Clayton;Yandex
Slug: 2021-09-10-krebsonsecurity-hit-by-huge-new-iot-botnet-meris

[Source](https://krebsonsecurity.com/2021/09/krebsonsecurity-hit-by-huge-new-iot-botnet-meris/){:target="_blank" rel="noopener"}

> On Thursday evening, KrebsOnSecurity was the subject of a rather massive (and mercifully brief) distributed denial-of-service (DDoS) attack. The assault came from “ Meris,” the same new botnet behind record-shattering attacks against Russian search giant Yandex this week and internet infrastructure firm Cloudflare earlier this summer. Cloudflare recently wrote about its attack, which clocked in at 17.2 million bogus requests-per-second. To put that in perspective, Cloudflare serves over 25 million HTTP requests per second on average. In its Aug. 19 writeup, Cloudflare neglected to assign a name to the botnet behind the attack. But on Thursday DDoS protection firm Qrator [...]
