Title: Amazon Driver-Surveillance Cameras Roll Out, Sparking Debate
Date: 2021-09-20T21:25:24+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Privacy
Slug: 2021-09-20-amazon-driver-surveillance-cameras-roll-out-sparking-debate

[Source](https://threatpost.com/amazon-driver-surveillance-cameras/174843/){:target="_blank" rel="noopener"}

> Drivers bristle under constant surveillance by artificial-intelligence (AI) tech, but Amazon says it works and boosts safety. [...]
