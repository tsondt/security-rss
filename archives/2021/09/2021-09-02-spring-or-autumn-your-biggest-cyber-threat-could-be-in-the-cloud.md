Title: Spring or autumn, your biggest cyber threat could be in the cloud
Date: 2021-09-02T23:00:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-09-02-spring-or-autumn-your-biggest-cyber-threat-could-be-in-the-cloud

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/02/cyber_threats_in_the_cloud/){:target="_blank" rel="noopener"}

> Tune in to SANS Institute's seasoned security experts Sponsored The sun never seems to set on the cybercriminal threat, but whether you’re heading into autumn or bursting into spring you can tap into the world’s finest cyber security training, at upcoming SANS Institute events in Asia and Oceania.... [...]
