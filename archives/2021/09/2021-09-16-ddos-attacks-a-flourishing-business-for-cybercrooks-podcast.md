Title: DDoS Attacks: A Flourishing Business for Cybercrooks – Podcast
Date: 2021-09-16T13:00:01+00:00
Author: Threatpost
Category: Threatpost
Tags: Podcasts;Sponsored;Web Security
Slug: 2021-09-16-ddos-attacks-a-flourishing-business-for-cybercrooks-podcast

[Source](https://threatpost.com/ddos-attacks-a-flourishing-business-for-cybercrooks-podcast/169473/){:target="_blank" rel="noopener"}

> Imperva’s Peter Klimek on how DDoS attacks started out as inconveniences but evolved to the point where attackers can disrupt businesses for as little as the price of a cup of coffee, [...]
