Title: Gutenberg Template Library & Redux Framework Bugs Plague WordPress Sites
Date: 2021-09-01T17:58:38+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-09-01-gutenberg-template-library-redux-framework-bugs-plague-wordpress-sites

[Source](https://threatpost.com/gutenberg-template-library-redux-bugs-wordpress/169111/){:target="_blank" rel="noopener"}

> Two vulnerabilities in the site-building plugin could be useful tools in the hands of a skilled attacker, researchers warned. [...]
