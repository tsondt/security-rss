Title: Norwegian student tracks Bluetooth headset wearers by wardriving around Oslo on a bicycle
Date: 2021-09-04T07:09:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2021-09-04-norwegian-student-tracks-bluetooth-headset-wearers-by-wardriving-around-oslo-on-a-bicycle

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/04/bluetooth_headphones_tracking_oslo/){:target="_blank" rel="noopener"}

> Address randomisation not implemented on some, it seems A Norwegian student who went wardriving around Oslo on a pushbike has discovered that several popular models of Bluetooth headphones don't implement MAC address randomisation – meaning they can be used to track their wearers.... [...]
