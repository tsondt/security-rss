Title: You’ve trained at the cutting edge, here’s how to keep your DFIR skills razor sharp
Date: 2021-09-21T04:00:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2021-09-21-youve-trained-at-the-cutting-edge-heres-how-to-keep-your-dfir-skills-razor-sharp

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/21/keep_you_dfir_skills_sharp/){:target="_blank" rel="noopener"}

> Sometimes the most important tool is a bookmark Sponsored There’s nothing like five or six days of in-depth training with SANS Institute to develop cutting-edge Digital Forensics and Incident Response security skills.... [...]
