Title: Spoofing Bug Highlights Cybersecurity for Digital Vaccine Passports
Date: 2021-09-08T17:28:35+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: 2021-09-08-spoofing-bug-highlights-cybersecurity-for-digital-vaccine-passports

[Source](https://threatpost.com/spoofing-bug-cybersecurity-vaccine-passports/169287/){:target="_blank" rel="noopener"}

> Australian immunization app bug lets attackers fake vaccine status. [...]
