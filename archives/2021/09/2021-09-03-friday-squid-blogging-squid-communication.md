Title: Friday Squid Blogging: Squid Communication
Date: 2021-09-03T21:05:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2021-09-03-friday-squid-blogging-squid-communication

[Source](https://www.schneier.com/blog/archives/2021/09/friday-squid-blogging-squid-communication.html){:target="_blank" rel="noopener"}

> Interesting article on squid communication. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
