Title: Apple Pay with VISA lets hackers force payments on locked iPhones
Date: 2021-09-29T20:37:09-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2021-09-29-apple-pay-with-visa-lets-hackers-force-payments-on-locked-iphones

[Source](https://www.bleepingcomputer.com/news/security/apple-pay-with-visa-lets-hackers-force-payments-on-locked-iphones/){:target="_blank" rel="noopener"}

> Academic researchers have found a way to make fraudulent payments using Apple Pay from a locked iPhone with a Visa card in the digital wallet set as a transit card. [...]
