Title: The Proliferation of Zero-days
Date: 2021-09-24T14:51:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;exploits;hacking;zero-day
Slug: 2021-09-24-the-proliferation-of-zero-days

[Source](https://www.schneier.com/blog/archives/2021/09/the-proliferation-of-zero-days.html){:target="_blank" rel="noopener"}

> The MIT Technology Review is reporting that 2021 is a blockbuster year for zero-day exploits: One contributing factor in the higher rate of reported zero-days is the rapid global proliferation of hacking tools. Powerful groups are all pouring heaps of cash into zero-days to use for themselves — and they’re reaping the rewards. At the top of the food chain are the government-sponsored hackers. China alone is suspected to be responsible for nine zero-days this year, says Jared Semrau, a director of vulnerability and exploitation at the American cybersecurity firm FireEye Mandiant. The US and its allies clearly possess some [...]
