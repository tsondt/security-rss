Title: Unpatched flaw 'weaponises' Apple AirTags to turn them into the phisherman's friend
Date: 2021-09-29T19:24:06+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: 2021-09-29-unpatched-flaw-weaponises-apple-airtags-to-turn-them-into-the-phishermans-friend

[Source](https://go.theregister.com/feed/www.theregister.com/2021/09/29/weaponised_apple_airtags/){:target="_blank" rel="noopener"}

> XSS vulnerability allows miscreants to hijack phone number field on website Apple has been accused of ignoring a vulnerability in the Lost Mode functionality of its AirTags location-tracking accessories which would allow an attacker to seed "weaponised AirTags" for harvesting the iCloud credentials of anyone who find them.... [...]
