Title: US optometry provider Simon Eye hit by data breach impacting 144,000 patients
Date: 2021-09-21T11:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2021-09-21-us-optometry-provider-simon-eye-hit-by-data-breach-impacting-144000-patients

[Source](https://portswigger.net/daily-swig/us-optometry-provider-simon-eye-hit-by-data-breach-impacting-144-000-patients){:target="_blank" rel="noopener"}

> Compromise of employee mailboxes may have exposed sensitive medical data [...]
