Title: BEC Scammers Seek Native English Speakers on Underground
Date: 2021-09-01T14:40:46+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2021-09-01-bec-scammers-seek-native-english-speakers-on-underground

[Source](https://threatpost.com/bec-scammers-native-english-speakers/169092/){:target="_blank" rel="noopener"}

> Cybercrooks are posting help-wanted ads on dark web forums, promising to do the technical work of compromising email accounts but looking for native English speakers to carry out the social-engineering part of these lucrative scams. [...]
