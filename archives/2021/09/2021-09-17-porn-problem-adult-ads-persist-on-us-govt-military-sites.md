Title: Porn Problem: Adult Ads Persist on US Gov’t, Military Sites
Date: 2021-09-17T17:16:42+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2021-09-17-porn-problem-adult-ads-persist-on-us-govt-military-sites

[Source](https://threatpost.com/porn-viagra-spams-govt-military-sites/174794/){:target="_blank" rel="noopener"}

> Cities, states, federal and military agencies should patch the Laserfiche CMS post-haste, said the security researcher whose jaw dropped at 50 sites hosting porn and Viagra spam. [...]
