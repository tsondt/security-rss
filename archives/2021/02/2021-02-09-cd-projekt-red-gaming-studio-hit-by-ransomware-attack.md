Title: CD PROJEKT RED gaming studio hit by ransomware attack
Date: 2021-02-09T05:33:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Gaming
Slug: cd-projekt-red-gaming-studio-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/cd-projekt-red-gaming-studio-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> CD PROJEKT RED, the video game development studio behind Cyberpunk 2077 and The Witcher trilogy, has disclosed a ransomware attack that impacted its network. [...]
