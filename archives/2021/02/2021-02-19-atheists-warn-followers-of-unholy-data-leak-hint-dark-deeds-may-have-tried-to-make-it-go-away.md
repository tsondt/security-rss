Title: Atheists warn followers of unholy data leak, hint dark deeds may have tried to make it go away
Date: 2021-02-19T06:04:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: atheists-warn-followers-of-unholy-data-leak-hint-dark-deeds-may-have-tried-to-make-it-go-away

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/19/atheists_data_leak/){:target="_blank" rel="noopener"}

> Rival atheists accused of not believing in privacy law The Atheist Alliance International, an organisation that works to demystify atheism and advocate for secular governance, has warned members their personal information appears to have been leaked.... [...]
