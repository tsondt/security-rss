Title: Researcher hacks over 35 tech firms in novel supply chain attack
Date: 2021-02-09T13:04:16-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Apple;Microsoft;Software
Slug: researcher-hacks-over-35-tech-firms-in-novel-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/researcher-hacks-over-35-tech-firms-in-novel-supply-chain-attack/){:target="_blank" rel="noopener"}

> A researcher managed to hack systems of over 35 major tech companies including Microsoft, Apple, PayPal, Shopify, Netflix, Tesla, Yelp, Tesla, and Uber in a novel software supply chain attack. For his ethical hacking research efforts, the researcher has been awarded over $130,000 in bug bounties. [...]
