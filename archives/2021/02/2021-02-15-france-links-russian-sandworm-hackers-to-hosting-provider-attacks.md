Title: France links Russian Sandworm hackers to hosting provider attacks
Date: 2021-02-15T13:23:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: france-links-russian-sandworm-hackers-to-hosting-provider-attacks

[Source](https://www.bleepingcomputer.com/news/security/france-links-russian-sandworm-hackers-to-hosting-provider-attacks/){:target="_blank" rel="noopener"}

> The French national cyber-security agency has linked a series of attacks that resulted in the breach of multiple French IT providers over a span of four years to the Russian-backed Sandworm hacking group. [...]
