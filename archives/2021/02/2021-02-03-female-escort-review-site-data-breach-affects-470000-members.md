Title: Female escort review site data breach affects 470,000 members
Date: 2021-02-03T03:03:03-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: female-escort-review-site-data-breach-affects-470000-members

[Source](https://www.bleepingcomputer.com/news/security/female-escort-review-site-data-breach-affects-470-000-members/){:target="_blank" rel="noopener"}

> An online community promoting female escorts and reviews of their services has suffered a data breach after a hacker downloaded the site's database. [...]
