Title: Russian Sandworm hackers only hit orgs with old Centreon software
Date: 2021-02-17T10:32:40-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: russian-sandworm-hackers-only-hit-orgs-with-old-centreon-software

[Source](https://www.bleepingcomputer.com/news/security/russian-sandworm-hackers-only-hit-orgs-with-old-centreon-software/){:target="_blank" rel="noopener"}

> Centreon, the maker of the IT monitoring software exploited by Russian state hackers to infiltrate French companies' networks, said today that only organizations using obsolete software were compromised. [...]
