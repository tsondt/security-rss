Title: Friday Squid Blogging: Live Giant Squid Found in Japan
Date: 2021-02-05T22:13:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid;video
Slug: friday-squid-blogging-live-giant-squid-found-in-japan

[Source](https://www.schneier.com/blog/archives/2021/02/friday-squid-blogging-live-giant-squid-found-in-japan.html){:target="_blank" rel="noopener"}

> A giant squid was found alive in the port of Izumo, Japan. Not a lot of news, just this Twitter thread (with a couple of videos). If confirmed, I believe this will be the THIRD time EVER a giant squid was filmed alive! As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
