Title: Presidential Cybersecurity and Pelotons
Date: 2021-02-05T11:58:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;essays;Internet of Things;laws;national security policy;risk assessment;risks;software liability
Slug: presidential-cybersecurity-and-pelotons

[Source](https://www.schneier.com/blog/archives/2021/02/presidential-cybersecurity-and-pelotons.html){:target="_blank" rel="noopener"}

> President Biden wants his Peloton in the White House. For those who have missed the hype, it’s an Internet-connected stationary bicycle. It has a screen, a camera, and a microphone. You can take live classes online, work out with your friends, or join the exercise social network. And all of that is a security risk, especially if you are the president of the United States. Any computer brings with it the risk of hacking. This is true of our computers and phones, and it’s also true about all of the Internet-of-Things devices that are increasingly part of our lives. These [...]
