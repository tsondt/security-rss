Title: No phish for the likes of you, thank you very much! Google finds email villains are picky about demographics, country
Date: 2021-02-10T09:30:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: no-phish-for-the-likes-of-you-thank-you-very-much-google-finds-email-villains-are-picky-about-demographics-country

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/10/google_phishing/){:target="_blank" rel="noopener"}

> Soon may the phisherman come to leave our inbox quite undone Kind old Google has published data on targeted email attacks and dispensed advice to help users separate friend from foe.... [...]
