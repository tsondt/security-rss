Title: India's demand to identify people on chat apps will 'break end-to-end encryption', say digital rights warriors
Date: 2021-02-26T02:10:57+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: indias-demand-to-identify-people-on-chat-apps-will-break-end-to-end-encryption-say-digital-rights-warriors

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/26/india_information_technology_intermediary_guidelines_and_digital_media_ethics_code_rules_2021/){:target="_blank" rel="noopener"}

> Announced rules also require fast takedowns of content, online profile verification, and more After a three-year review process, India has announced strict regulations for instant chat services, social network operators, and video-streaming companies.... [...]
