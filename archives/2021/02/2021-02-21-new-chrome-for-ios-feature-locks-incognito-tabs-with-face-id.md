Title: New Chrome for iOS feature locks Incognito tabs with Face ID
Date: 2021-02-21T08:31:05-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google
Slug: new-chrome-for-ios-feature-locks-incognito-tabs-with-face-id

[Source](https://www.bleepingcomputer.com/news/google/new-chrome-for-ios-feature-locks-incognito-tabs-with-face-id/){:target="_blank" rel="noopener"}

> Google Chrome for iOS is getting a new privacy feature that lets you lock your opened Incognito tabs behind your iPhone's Face ID or Touch ID biometric authentication features. [...]
