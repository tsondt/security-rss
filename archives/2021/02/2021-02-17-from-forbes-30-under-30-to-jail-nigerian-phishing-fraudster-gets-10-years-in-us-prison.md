Title: From ‘Forbes 30 under 30’ to jail – Nigerian phishing fraudster gets 10 years in US prison
Date: 2021-02-17T14:21:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: from-forbes-30-under-30-to-jail-nigerian-phishing-fraudster-gets-10-years-in-us-prison

[Source](https://portswigger.net/daily-swig/from-forbes-30-under-30-to-jail-nigerian-phishing-fraudster-gets-10-years-in-us-prison){:target="_blank" rel="noopener"}

> Obinwanne Okeke sentenced for masterminding transatlantic BEC scam [...]
