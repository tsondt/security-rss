Title: More patches for SolarWinds Orion after researchers find flaw allowing low-priv users to execute code, among others
Date: 2021-02-03T21:25:30+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: more-patches-for-solarwinds-orion-after-researchers-find-flaw-allowing-low-priv-users-to-execute-code-among-others

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/03/solarwinds_patch_trustwave/){:target="_blank" rel="noopener"}

> Probably not used by last year's US government-busting attackers, though As if that supply chain attack wasn't bad enough, SolarWinds has had to patch its Orion software again after eagle-eyed researchers discovered fresh vulnerabilities – including one that can be exploited to achieve remote code execution.... [...]
