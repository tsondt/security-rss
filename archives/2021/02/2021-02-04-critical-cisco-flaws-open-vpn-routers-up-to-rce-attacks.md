Title: Critical Cisco Flaws Open VPN Routers Up to RCE Attacks
Date: 2021-02-04T15:59:01+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: critical-cisco-flaws-open-vpn-routers-up-to-rce-attacks

[Source](https://threatpost.com/cisco-flaws-vpn-routers-rce/163662/){:target="_blank" rel="noopener"}

> The vulnerabilities exist in Cisco's RV160, RV160W, RV260, RV260P, and RV260W VPN routers for small businesses. [...]
