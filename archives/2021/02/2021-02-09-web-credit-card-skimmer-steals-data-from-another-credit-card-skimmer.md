Title: Web Credit Card Skimmer Steals Data from Another Credit Card Skimmer
Date: 2021-02-09T12:01:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: credit cards;skimmers;theft
Slug: web-credit-card-skimmer-steals-data-from-another-credit-card-skimmer

[Source](https://www.schneier.com/blog/archives/2021/02/web-credit-card-skimmer-steals-data-from-another-credit-card-skimmer.html){:target="_blank" rel="noopener"}

> MalwareBytes is reporting a weird software credit card skimmer. It harvests credit card data stolen by another, different skimmer: Even though spotting multiple card skimmer scripts on the same online shop is not unheard of, this one stood out due to its highly specialized nature. “The threat actors devised a version of their script that is aware of sites already injected with a Magento 1 skimmer,” Malwarebytes’ Head of Threat Intelligence Jérôme Segura explains in a report shared in advance with Bleeping Computer. “That second skimmer will simply harvest credit card details from the already existing fake form injected by [...]
