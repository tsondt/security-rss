Title: Chrome zero-day bug that is actively being abused by bad folks affects Edge, Vivaldi, and other Chromium-tinged browsers
Date: 2021-02-05T15:07:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: chrome-zero-day-bug-that-is-actively-being-abused-by-bad-folks-affects-edge-vivaldi-and-other-chromium-tinged-browsers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/05/chrome_zero_day_update/){:target="_blank" rel="noopener"}

> Install your updates pronto If you use Google Chrome or a Chromium-based browser such as Microsoft Edge, update it immediately and/or check it for updates over the coming days: there is a zero-day bug being "actively exploited" in the older version of Chrome that will also affect other vendors' browsers.... [...]
