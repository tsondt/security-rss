Title: Brave browser leaks visited Tor .onion addresses in DNS traffic, fix released after bug hunter raises alarm
Date: 2021-02-22T07:14:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: brave-browser-leaks-visited-tor-onion-addresses-in-dns-traffic-fix-released-after-bug-hunter-raises-alarm

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/22/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: IBM's lawyers hacked, Kia denies ransomware hit, France declares war on hackers, and more In brief Brave has patched up its privacy-focused web browser after it was spotted leaking its Tor users' dark-web habits.... [...]
