Title: Helping users keep their organization secure with their phone's built-in security key
Date: 2021-02-16T17:00:00+00:00
Author: Bakh Inamov
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: helping-users-keep-their-organization-secure-with-their-phones-built-in-security-key

[Source](https://cloud.google.com/blog/products/identity-security/helping-users-keep-their-organization-secure-with-phones-built-in-security-key/){:target="_blank" rel="noopener"}

> Phishing remains among an organization’s most prevalent security threats. At Google, we’ve developed advanced tools to help protect users from phishing attacks, including our Titan Security Keys. With the goal of making security keys even easier to use and more ubiquitous, we’ve recently made it possible to use your phone’s built-in security key to secure your account. Security keys based on FIDO standards are a form of 2-Step verification (2SV) and we consider them to be the strongest, most phishing-resistant method of 2SV because they leverage public key cryptography to verify a user’s identity, and that of the login page, [...]
