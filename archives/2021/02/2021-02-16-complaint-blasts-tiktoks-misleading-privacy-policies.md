Title: Complaint Blasts TikTok’s ‘Misleading’ Privacy Policies
Date: 2021-02-16T22:00:57+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Mobile Security;Privacy;Vulnerabilities;Web Security
Slug: complaint-blasts-tiktoks-misleading-privacy-policies

[Source](https://threatpost.com/tiktok-complaint-data-collection/163991/){:target="_blank" rel="noopener"}

> TikTok is again in hot water for how the popular video-sharing app collects and shares data - particularly from its underage userbase. [...]
