Title: Dev creeped out after he fired up Ubuntu VM on Azure, was immediately approached by Canonical sales rep
Date: 2021-02-11T14:14:02+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: dev-creeped-out-after-he-fired-up-ubuntu-vm-on-azure-was-immediately-approached-by-canonical-sales-rep

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/11/microsoft_azure_ubuntu_data_sharing/){:target="_blank" rel="noopener"}

> I always feel like somebody's watching me An Azure customer was outraged after finding himself on the receiving end of an unexpected LinkedIn message from Ubuntu maker Canonical last night.... [...]
