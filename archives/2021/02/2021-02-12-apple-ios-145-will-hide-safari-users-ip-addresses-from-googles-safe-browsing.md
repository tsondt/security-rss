Title: Apple iOS 14.5 will hide Safari users' IP addresses from Google's Safe Browsing
Date: 2021-02-12T02:20:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: apple-ios-145-will-hide-safari-users-ip-addresses-from-googles-safe-browsing

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/12/apple_safe_browing/){:target="_blank" rel="noopener"}

> Another privacy improvement from Cupertino, just a small one Apple's forthcoming iOS 14.5 release, currently in beta, will conceal the IP address of Safari web surfers from Google's Safe Browsing service, integrated into Safari to spot fraudulent websites.... [...]
