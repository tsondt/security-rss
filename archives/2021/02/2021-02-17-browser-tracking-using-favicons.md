Title: Browser Tracking Using Favicons
Date: 2021-02-17T12:05:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;browsers;tracking
Slug: browser-tracking-using-favicons

[Source](https://www.schneier.com/blog/archives/2021/02/browser-tracking-using-favicons.html){:target="_blank" rel="noopener"}

> Interesting research on persistent web tracking using favicons. (For those who don’t know, favicons are those tiny icons that appear in browser tabs next to the page name.) Abstract: The privacy threats of online tracking have garnered considerable attention in recent years from researchers and practitioners alike. This has resulted in users becoming more privacy-cautious and browser vendors gradually adopting countermeasures to mitigate certain forms of cookie-based and cookie-less tracking. Nonetheless, the complexity and feature-rich nature of modern browsers often lead to the deployment of seemingly innocuous functionality that can be readily abused by adversaries. In this paper we introduce [...]
