Title: Amazon Dismisses Claims Alexa ‘Skills’ Can Bypass Security Vetting Process
Date: 2021-02-26T21:53:26+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Privacy;Web Security
Slug: amazon-dismisses-claims-alexa-skills-can-bypass-security-vetting-process

[Source](https://threatpost.com/amazon-dismisses-claims-alexa-skills-can-bypass-security-vetting/164316/){:target="_blank" rel="noopener"}

> Researchers found a number of privacy and security issues in Amazon's Alexa skill vetting process, which could lead to attackers stealing data or launching phishing attacks. [...]
