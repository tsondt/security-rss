Title: SolarWinds Orion Bug Allows Easy Remote-Code Execution and Takeover
Date: 2021-02-03T11:00:21+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: solarwinds-orion-bug-allows-easy-remote-code-execution-and-takeover

[Source](https://threatpost.com/solarwinds-orion-bug-remote-code-execution/163618/){:target="_blank" rel="noopener"}

> The by-now infamous company has issued patches for three security vulnerabilities in total. [...]
