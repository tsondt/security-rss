Title: Dutch Police post "say no to cybercrime" warnings on hacker forums
Date: 2021-02-17T09:40:54-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: dutch-police-post-say-no-to-cybercrime-warnings-on-hacker-forums

[Source](https://www.bleepingcomputer.com/news/security/dutch-police-post-say-no-to-cybercrime-warnings-on-hacker-forums/){:target="_blank" rel="noopener"}

> The Dutch Police have begun posting warnings on Russian and English-speaking hacker forums not to commit cybercrime as law enforcement is watching their activity. [...]
