Title: Indian Railways suffers unspecified security 'breaches in various IT applications'
Date: 2021-02-24T03:13:33+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: indian-railways-suffers-unspecified-security-breaches-in-various-it-applications

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/24/indian_railways_admits_multiple_breaches/){:target="_blank" rel="noopener"}

> 13m passengers a day, a million tickets bought on digital platforms, and yet few details offered on what went wrong Indian Railways has revealed it has suffered "a number of incidents... regarding breaches in various IT applications" and appears to have blamed some of them on sloppy infosec practices among staff working from home due to the COVID-19 pandemic.... [...]
