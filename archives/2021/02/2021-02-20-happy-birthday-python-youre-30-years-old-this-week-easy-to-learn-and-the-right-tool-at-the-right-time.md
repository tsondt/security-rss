Title: Happy birthday, Python, you're 30 years old this week: Easy to learn, and the right tool at the right time
Date: 2021-02-20T13:10:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: happy-birthday-python-youre-30-years-old-this-week-easy-to-learn-and-the-right-tool-at-the-right-time

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/20/python_at_30/){:target="_blank" rel="noopener"}

> Popular programming language, at the top of its game, still struggles to please everyone Feature The 30th anniversary of Python this week finds the programming language at the top of its game, but not without challenges.... [...]
