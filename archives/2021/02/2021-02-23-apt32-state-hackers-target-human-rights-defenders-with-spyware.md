Title: APT32 state hackers target human rights defenders with spyware
Date: 2021-02-23T20:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: apt32-state-hackers-target-human-rights-defenders-with-spyware

[Source](https://www.bleepingcomputer.com/news/security/apt32-state-hackers-target-human-rights-defenders-with-spyware/){:target="_blank" rel="noopener"}

> Vietnam-backed hacking group APT32 has coordinated several spyware attacks targeting Vietnamese human rights defenders (HRDs) between February 2018 and November 2020. [...]
