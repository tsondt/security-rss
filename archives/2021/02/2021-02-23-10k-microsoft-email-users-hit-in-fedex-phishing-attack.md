Title: 10K Microsoft Email Users Hit in FedEx Phishing Attack
Date: 2021-02-23T14:00:38+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Web Security
Slug: 10k-microsoft-email-users-hit-in-fedex-phishing-attack

[Source](https://threatpost.com/microsoft-fedex-phishing-attack/164143/){:target="_blank" rel="noopener"}

> Microsoft users are receiving emails pretending to be from mail couriers FedEx and DHL Express - but that really steal their credentials. [...]
