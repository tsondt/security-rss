Title: Android barcode scanner with 10 million+ downloads infects users
Date: 2021-02-08T19:46:18+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;Android Apps;google play;malware
Slug: android-barcode-scanner-with-10-million-downloads-infects-users

[Source](https://arstechnica.com/?p=1740600){:target="_blank" rel="noopener"}

> Enlarge (credit: portal gda / Flickr ) A benign barcode scanner with more than 10 million downloads from Google Play has been caught receiving an upgrade that turned it to the dark side, prompting the search-and-advertising giant to remove it. Barcode Scanner, one of dozens of such apps available in the official Google app repository, began its life as a legitimate offering. Then in late December, researchers with security firm Malwarebytes began receiving messages from customers complaining that ads were opening out of nowhere on their default browser. One update is all it takes Malwarebytes mobile malware researcher Nathan Collier [...]
