Title: Twitter scammers earned over $145k this week in Bitcoin, Ethereum, Doge
Date: 2021-02-26T17:00:28-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency
Slug: twitter-scammers-earned-over-145k-this-week-in-bitcoin-ethereum-doge

[Source](https://www.bleepingcomputer.com/news/security/twitter-scammers-earned-over-145k-this-week-in-bitcoin-ethereum-doge/){:target="_blank" rel="noopener"}

> Cryptocurrency scammers have made at least $145,000 this week by promoting fake giveaways through hacked verified Twitter accounts. [...]
