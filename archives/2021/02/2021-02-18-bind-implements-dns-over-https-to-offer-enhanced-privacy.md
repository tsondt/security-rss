Title: BIND implements DNS over HTTPS to offer enhanced privacy
Date: 2021-02-18T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: bind-implements-dns-over-https-to-offer-enhanced-privacy

[Source](https://portswigger.net/daily-swig/bind-implements-dns-over-https-to-offer-enhanced-privacy){:target="_blank" rel="noopener"}

> DNS server technology gets experimental upgrade [...]
