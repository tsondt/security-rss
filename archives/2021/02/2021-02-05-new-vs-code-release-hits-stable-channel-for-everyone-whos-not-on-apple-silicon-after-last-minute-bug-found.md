Title: New VS Code release hits stable channel for everyone who's not on Apple Silicon after last-minute bug found
Date: 2021-02-05T16:03:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: new-vs-code-release-hits-stable-channel-for-everyone-whos-not-on-apple-silicon-after-last-minute-bug-found

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/05/vs_code_update/){:target="_blank" rel="noopener"}

> Electron 11, source control tweaks, plus general spit and polish Microsoft has pushed out another update to dev favourite Visual Studio Code, but opted to hold off on the Apple Silicon version after a last-minute bug reared its head.... [...]
