Title: Cyberpunk 2077 developers held to ransom after cyber-attack, source code theft
Date: 2021-02-09T15:31:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cyberpunk-2077-developers-held-to-ransom-after-cyber-attack-source-code-theft

[Source](https://portswigger.net/daily-swig/cyberpunk-developers-held-to-ransom-after-cyber-attack-source-code-theft){:target="_blank" rel="noopener"}

> CD Projekt Red confirmed that its systems were breached by malicious hackers [...]
