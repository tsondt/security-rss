Title: Supporting the Python ecosystem
Date: 2021-02-11T17:00:00+00:00
Author: Dustin Ingram
Category: GCP Security
Tags: Application Development;Identity & Security;Open Source
Slug: supporting-the-python-ecosystem

[Source](https://cloud.google.com/blog/products/open-source/supporting-the-python-ecosystem/){:target="_blank" rel="noopener"}

> Python is critically important to both Google Cloud and our customers. It serves as a popular runtime for many of our hosted services, from the launch of App Engine more than a decade ago, to modern serverless products like Cloud Functions. We use the Python Package Index (PyPI) to distribute hundreds of client libraries and developer tools, including the popular open-source machine-learning library TensorFlow. And we use it internally as well, where it helps power many of our core products and services. As part of our longstanding support for the Python ecosystem, we are happy to increase our support for [...]
