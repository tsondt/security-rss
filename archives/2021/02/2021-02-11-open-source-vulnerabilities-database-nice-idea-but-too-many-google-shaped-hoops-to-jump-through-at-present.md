Title: Open Source Vulnerabilities database: Nice idea but too many Google-shaped hoops to jump through at present
Date: 2021-02-11T09:30:04+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: open-source-vulnerabilities-database-nice-idea-but-too-many-google-shaped-hoops-to-jump-through-at-present

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/11/google_osv_database/){:target="_blank" rel="noopener"}

> Google Cloud Platform account required, API key comes with Ts&Cs Hands On Google has big ambitions for its new Open Source Vulnerabilities database, but getting started requires a Google Cloud Platform account and there are other obstacles that may add friction to adoption.... [...]
