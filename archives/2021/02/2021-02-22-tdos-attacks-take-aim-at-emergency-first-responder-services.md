Title: TDoS Attacks Take Aim at Emergency First-Responder Services
Date: 2021-02-22T22:02:10+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Hacks;Web Security
Slug: tdos-attacks-take-aim-at-emergency-first-responder-services

[Source](https://threatpost.com/tdos-attacks-emergency-first-responder/164176/){:target="_blank" rel="noopener"}

> The FBI has warned that telephony denial-of-service attacks are taking aim at emergency dispatch centers, which could make it impossible to call for police, fire or ambulance services. [...]
