Title: Facebook, Instagram, TikTok and Twitter Target Resellers of Hacked Accounts
Date: 2021-02-04T18:02:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;The Coming Storm;Web Fraud 2.0;@H4CK;@Trusted;Beam;extortion;Facebook;Instagram;Noah Hawkins;ogusers;Ryan Zanelli;sextortion;SIM swapping;SWATting;Tiktok;twitter
Slug: facebook-instagram-tiktok-and-twitter-target-resellers-of-hacked-accounts

[Source](https://krebsonsecurity.com/2021/02/facebook-instagram-tiktok-and-twitter-target-resellers-of-hacked-accounts/){:target="_blank" rel="noopener"}

> Facebook, Instagram, TikTok, and Twitter this week all took steps to crack down on users involved in trafficking hijacked user accounts across their platforms. The coordinated action seized hundreds of accounts the companies say have played a major role in facilitating the trade and often lucrative resale of compromised, highly sought-after usernames. At the center of the account ban wave are some of the most active members of OGUsers, a forum that caters to thousands of people selling access to hijacked social media and other online accounts. Particularly prized by this community are short usernames, which can often be resold [...]
