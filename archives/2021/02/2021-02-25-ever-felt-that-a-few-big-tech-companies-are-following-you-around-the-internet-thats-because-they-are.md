Title: Ever felt that a few big tech companies are following you around the internet? That's because ... they are
Date: 2021-02-25T12:04:05+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: ever-felt-that-a-few-big-tech-companies-are-following-you-around-the-internet-thats-because-they-are

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/25/big_tech_extension/){:target="_blank" rel="noopener"}

> Experimental blocking of sites that load resources from four big companies makes the web unusable A new extension for Google Chrome has made explicit how most popular sites on the internet load resources from one or more of Google, Facebook, Microsoft and Amazon.... [...]
