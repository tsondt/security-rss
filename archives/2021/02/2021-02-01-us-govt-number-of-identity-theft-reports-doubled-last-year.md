Title: US govt: Number of identity theft reports doubled last year
Date: 2021-02-01T15:10:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-govt-number-of-identity-theft-reports-doubled-last-year

[Source](https://www.bleepingcomputer.com/news/security/us-govt-number-of-identity-theft-reports-doubled-last-year/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) said today that the number of identity theft reports has doubled during 2020 when compared to 2019, reaching a record 1.4 million reports within a single year. [...]
