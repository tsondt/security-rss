Title: mHealth Apps Expose Millions to Cyberattacks
Date: 2021-02-12T21:01:25+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Mobile Security;Vulnerabilities
Slug: mhealth-apps-expose-millions-to-cyberattacks

[Source](https://threatpost.com/mhealth-apps-millions-cyberattacks/163966/){:target="_blank" rel="noopener"}

> Researcher testing of 30 mobile health apps for clinicians found that all of them had vulnerable APIs. [...]
