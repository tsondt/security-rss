Title: Microsoft urges customers to patch critical Windows TCP/IP bugs
Date: 2021-02-09T13:52:25-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-urges-customers-to-patch-critical-windows-tcpip-bugs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-urges-customers-to-patch-critical-windows-tcp-ip-bugs/){:target="_blank" rel="noopener"}

> Microsoft has urged customers today to install security updates for three Windows TCP/IP vulnerabilities rated as critical and high severity as soon as possible. [...]
