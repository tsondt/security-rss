Title: What are these suspicious Google GVT1.com URLs?
Date: 2021-02-28T11:52:40-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Google
Slug: what-are-these-suspicious-google-gvt1com-urls

[Source](https://www.bleepingcomputer.com/news/security/what-are-these-suspicious-google-gvt1com-urls/){:target="_blank" rel="noopener"}

> These Google-owned domains have confused even the most skilled researchers and security products time and time again if these are malicious. The domains in question are redirector.gvt1.com and gvt1/gvt2 subdomains that have spun many threads on the internet. BleepingComputer has dug deeper into the origin of these domains. [...]
