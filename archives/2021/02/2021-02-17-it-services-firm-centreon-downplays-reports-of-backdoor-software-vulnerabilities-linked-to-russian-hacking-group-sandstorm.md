Title: IT services firm Centreon downplays reports of backdoor software vulnerabilities linked to Russian hacking group Sandstorm
Date: 2021-02-17T15:35:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: it-services-firm-centreon-downplays-reports-of-backdoor-software-vulnerabilities-linked-to-russian-hacking-group-sandstorm

[Source](https://portswigger.net/daily-swig/it-services-firm-centreon-downplays-reports-of-backdoor-software-vulnerabilities-linked-to-russian-hacking-group-sandstorm){:target="_blank" rel="noopener"}

> Company says victims were using outdated versions of its monitoring software [...]
