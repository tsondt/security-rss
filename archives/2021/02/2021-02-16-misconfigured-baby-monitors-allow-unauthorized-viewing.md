Title: Misconfigured Baby Monitors Allow Unauthorized Viewing
Date: 2021-02-16T16:50:35+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;IoT;Mobile Security;Vulnerabilities;Web Security
Slug: misconfigured-baby-monitors-allow-unauthorized-viewing

[Source](https://threatpost.com/baby-monitors-unauthorized-viewing/163982/){:target="_blank" rel="noopener"}

> Hundreds of thousands of individuals are potentially affected by this vulnerability. [...]
