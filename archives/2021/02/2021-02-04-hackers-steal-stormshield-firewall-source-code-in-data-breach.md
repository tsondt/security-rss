Title: Hackers steal StormShield firewall source code in data breach
Date: 2021-02-04T13:41:58-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hackers-steal-stormshield-firewall-source-code-in-data-breach

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-stormshield-firewall-source-code-in-data-breach/){:target="_blank" rel="noopener"}

> Leading French cybersecurity company StormShield disclosed that their systems were hacked, allowing a threat actor to access the company's support ticket system and steal source code for Stormshield Network Security firewall software. [...]
