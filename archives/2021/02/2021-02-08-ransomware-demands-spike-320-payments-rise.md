Title: Ransomware Demands Spike 320%, Payments Rise
Date: 2021-02-08T21:06:39+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Most Recent ThreatLists;Web Security
Slug: ransomware-demands-spike-320-payments-rise

[Source](https://threatpost.com/ransomware-demands-spike-payments-rise/163744/){:target="_blank" rel="noopener"}

> Remote work continues to fueling a spike in phishing and cyberattacks, particularly in the U.S. [...]
