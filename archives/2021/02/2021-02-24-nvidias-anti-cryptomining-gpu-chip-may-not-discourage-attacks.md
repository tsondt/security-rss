Title: Nvidia’s Anti-Cryptomining GPU Chip May Not Discourage Attacks
Date: 2021-02-24T15:31:59+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cryptography;Malware;Vulnerabilities
Slug: nvidias-anti-cryptomining-gpu-chip-may-not-discourage-attacks

[Source](https://threatpost.com/nvidia-tries-discourage-crypto-jacking-new-gpu/164221/){:target="_blank" rel="noopener"}

> The hotly anticipated GeForce RTX 3060, a ray-tracing-friendly, advanced gaming graphics chip, will also throttle Ethereum mining. [...]
