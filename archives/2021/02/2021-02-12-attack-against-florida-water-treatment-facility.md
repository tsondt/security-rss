Title: Attack against Florida Water Treatment Facility
Date: 2021-02-12T12:08:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;infrastructure;physical security
Slug: attack-against-florida-water-treatment-facility

[Source](https://www.schneier.com/blog/archives/2021/02/attack-against-florida-water-treatment-facility.html){:target="_blank" rel="noopener"}

> A water treatment plant in Oldsmar, Florida, was attacked last Friday. The attacker took control of one of the systems, and increased the amount of sodium hydroxide — that’s lye — by a factor of 100. This could have been fatal to people living downstream, if an alert operator hadn’t noticed the change and reversed it. We don’t know who is behind this attack. Despite its similarities to a Russian attack of a Ukrainian power plant in 2015, my bet is that it’s a disgruntled insider: either a current or former employee. It just doesn’t make sense for Russia to [...]
