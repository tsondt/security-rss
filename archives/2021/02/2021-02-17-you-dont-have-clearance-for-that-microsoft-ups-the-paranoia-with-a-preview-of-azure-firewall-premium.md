Title: You don't have clearance for that: Microsoft ups the paranoia with a preview of Azure Firewall Premium
Date: 2021-02-17T16:30:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: you-dont-have-clearance-for-that-microsoft-ups-the-paranoia-with-a-preview-of-azure-firewall-premium

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/17/azure_firewall_premium/){:target="_blank" rel="noopener"}

> Reassuring the regulators Microsoft has unveiled a preview of Azure Firewall Premium, aimed at highly sensitive and regulated environments.... [...]
