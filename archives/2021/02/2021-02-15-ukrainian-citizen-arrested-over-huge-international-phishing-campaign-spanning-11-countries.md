Title: Ukrainian citizen arrested over huge international phishing campaign spanning 11 countries
Date: 2021-02-15T14:46:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ukrainian-citizen-arrested-over-huge-international-phishing-campaign-spanning-11-countries

[Source](https://portswigger.net/daily-swig/ukrainian-citizen-arrested-over-huge-international-phishing-campaign-spanning-11-countries){:target="_blank" rel="noopener"}

> Cybercrime operation accounted for more than half of all phishing attempts made against Australians in 2019 [...]
