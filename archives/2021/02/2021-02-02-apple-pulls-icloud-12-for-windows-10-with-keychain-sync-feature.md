Title: Apple pulls iCloud 12 for Windows 10 with Keychain sync feature
Date: 2021-02-02T10:25:21-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Apple
Slug: apple-pulls-icloud-12-for-windows-10-with-keychain-sync-feature

[Source](https://www.bleepingcomputer.com/news/apple/apple-pulls-icloud-12-for-windows-10-with-keychain-sync-feature/){:target="_blank" rel="noopener"}

> Apple has pulled iCloud 12 for Windows 10 from the Microsoft Store for what is believed to be issues with their new Chrome iCloud Keychain password synchronization feature. [...]
