Title: Nurserycam horror show: 'Secure' daycare video monitoring product beamed DVR admin creds to all users
Date: 2021-02-18T12:01:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: nurserycam-horror-show-secure-daycare-video-monitoring-product-beamed-dvr-admin-creds-to-all-users

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/18/nurserycam_security_problems_footfallcam_ltd/){:target="_blank" rel="noopener"}

> Company has a habit of reacting badly to vuln disclosures A parental webcam targeted at nursery schools was so poorly designed that anyone who downloaded its mobile app gained access to admin credentials, bypassing intended authentication, according to security pros – with one dad saying its creators brushed off his complaints about insecurities six years ago.... [...]
