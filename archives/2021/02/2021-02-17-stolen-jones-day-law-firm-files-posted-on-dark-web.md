Title: Stolen Jones Day Law Firm Files Posted on Dark Web
Date: 2021-02-17T22:02:23+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Hacks;Vulnerabilities
Slug: stolen-jones-day-law-firm-files-posted-on-dark-web

[Source](https://threatpost.com/stolen-jones-day-law-firm-files-posted/164066/){:target="_blank" rel="noopener"}

> Jones Day, which represented Trump, said the breach is part of the Accellion attack from December. [...]
