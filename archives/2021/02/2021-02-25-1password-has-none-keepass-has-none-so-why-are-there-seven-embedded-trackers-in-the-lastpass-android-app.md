Title: 1Password has none, KeePass has none... So why are there seven embedded trackers in the LastPass Android app?
Date: 2021-02-25T18:39:09+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: 1password-has-none-keepass-has-none-so-why-are-there-seven-embedded-trackers-in-the-lastpass-android-app

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/25/lastpass_android_trackers_found/){:target="_blank" rel="noopener"}

> Third-party code in security-critical apps is obviously suboptimal, but company says you can opt out A security researcher has recommended against using the LastPass password manager Android app after noting seven embedded trackers. The software's maker says users can opt out if they want.... [...]
