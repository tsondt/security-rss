Title: Microsoft: SolarWinds Attackers Downloaded Azure, Exchange Code
Date: 2021-02-19T14:11:33+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Hacks;Malware;Vulnerabilities
Slug: microsoft-solarwinds-attackers-downloaded-azure-exchange-code

[Source](https://threatpost.com/microsoft-solarwinds-azure-exchange-code/164104/){:target="_blank" rel="noopener"}

> However, internal products and systems were not leveraged to attack others during the massive supply-chain incident, the tech giant said upon completion of its Solorigate investigation. [...]
