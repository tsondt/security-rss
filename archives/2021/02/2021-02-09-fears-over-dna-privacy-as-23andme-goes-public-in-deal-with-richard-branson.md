Title: Fears over DNA privacy as 23andMe goes public in deal with Richard Branson
Date: 2021-02-09T21:52:43+00:00
Author: Kari Paul in San Francisco
Category: The Guardian
Tags: Data protection;US news;Technology;Data and computer security;Hacking
Slug: fears-over-dna-privacy-as-23andme-goes-public-in-deal-with-richard-branson

[Source](https://www.theguardian.com/technology/2021/feb/09/23andme-dna-privacy-richard-branson-genetics){:target="_blank" rel="noopener"}

> Genetic testing company with 10 million customers’ data has ‘huge cybersecurity implications’ The genetic testing company 23andMe will go public through a partnership with a firm backed by the billionaire Richard Branson, in a deal that has raised fresh privacy questions about the information of millions of customers. Launched in 2006, 23andMe sells tests to determine consumers’ genetic ancestry and risk of developing certain illnesses, using saliva samples sent in by mail. Related: Your DNA is a valuable asset, so why give it to ancestry websites for free? | Laura Spinney Continue reading... [...]
