Title: Kia Motors America experiences massive IT outage across the US
Date: 2021-02-16T14:24:24-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Business
Slug: kia-motors-america-experiences-massive-it-outage-across-the-us

[Source](https://www.bleepingcomputer.com/news/security/kia-motors-america-experiences-massive-it-outage-across-the-us/){:target="_blank" rel="noopener"}

> Kia Motors USA is experiencing a nationwide outage affecting IT servers, self-payment phone services, dealer platforms, and phone support. [...]
