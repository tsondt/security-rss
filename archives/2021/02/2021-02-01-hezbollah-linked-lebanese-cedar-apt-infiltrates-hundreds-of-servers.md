Title: Hezbollah-Linked Lebanese Cedar APT Infiltrates Hundreds of Servers
Date: 2021-02-01T21:18:09+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: hezbollah-linked-lebanese-cedar-apt-infiltrates-hundreds-of-servers

[Source](https://threatpost.com/hezbollah-lebanese-cedar-apt-servers/163555/){:target="_blank" rel="noopener"}

> Enhanced Explosive RAT and Caterpillar tools are at the forefront of a global espionage campaign. [...]
