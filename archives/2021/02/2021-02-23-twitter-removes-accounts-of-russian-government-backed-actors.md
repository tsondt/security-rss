Title: Twitter removes accounts of Russian government-backed actors
Date: 2021-02-23T13:36:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: twitter-removes-accounts-of-russian-government-backed-actors

[Source](https://www.bleepingcomputer.com/news/security/twitter-removes-accounts-of-russian-government-backed-actors/){:target="_blank" rel="noopener"}

> Twitter has removed dozens of accounts connected to Russian government-backed actors disseminating disinformation and targeting the European Union, the United States, and the NATO alliance. [...]
