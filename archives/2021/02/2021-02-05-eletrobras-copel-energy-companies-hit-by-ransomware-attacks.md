Title: Eletrobras, Copel energy companies hit by ransomware attacks
Date: 2021-02-05T03:46:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: eletrobras-copel-energy-companies-hit-by-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/eletrobras-copel-energy-companies-hit-by-ransomware-attacks/){:target="_blank" rel="noopener"}

> Centrais Eletricas Brasileiras (Eletrobras) and Companhia Paranaense de Energia (Copel), two major electric utilities companies in Brazil have announced that they suffered ransomware attacks over the past week. [...]
