Title: Ryuk ransomware now self-spreads to other Windows LAN devices
Date: 2021-02-26T12:37:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ryuk-ransomware-now-self-spreads-to-other-windows-lan-devices

[Source](https://www.bleepingcomputer.com/news/security/ryuk-ransomware-now-self-spreads-to-other-windows-lan-devices/){:target="_blank" rel="noopener"}

> A new Ryuk ransomware variant with worm-like capabilities that allow it to spread to other devices on victims' local networks has been discovered by the French national cyber-security agency while investigating an attack in early 2021. [...]
