Title: Babyk Ransomware won't hit charities, unless they support LGBT, BLM
Date: 2021-02-02T14:02:02-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: babyk-ransomware-wont-hit-charities-unless-they-support-lgbt-blm

[Source](https://www.bleepingcomputer.com/news/security/babyk-ransomware-wont-hit-charities-unless-they-support-lgbt-blm/){:target="_blank" rel="noopener"}

> The Babyk ransomware operation has launched a new data leak site used to publish victim's stolen data as part of a double extortion strategy. Included is a list of targets they wont attack with some exclusions that definitely stand out. [...]
