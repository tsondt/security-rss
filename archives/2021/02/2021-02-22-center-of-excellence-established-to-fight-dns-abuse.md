Title: Center of excellence established to fight DNS abuse
Date: 2021-02-22T15:52:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: center-of-excellence-established-to-fight-dns-abuse

[Source](https://portswigger.net/daily-swig/center-of-excellence-established-to-fight-dns-abuse){:target="_blank" rel="noopener"}

> Malware slingers and spammers targeted by cross-industry institute led by industry veteran [...]
