Title: Bluetooth Overlay Skimmer That Blocks Chip
Date: 2021-02-15T22:34:22+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: All About Skimmers;Bluetooth skimmer;overlay skimmer
Slug: bluetooth-overlay-skimmer-that-blocks-chip

[Source](https://krebsonsecurity.com/2021/02/bluetooth-overlay-skimmer-that-blocks-chip/){:target="_blank" rel="noopener"}

> As a total sucker for anything skimming-related, I was interested to hear from a reader working security for a retail chain in the United States who recently found Bluetooth-enabled skimming devices placed over top of payment card terminals at several stores. Interestingly, these skimmers interfered with the terminal’s ability to read chip-based cards, forcing customers to swipe the stripe instead. The payment card skimmer overlay transmitted stolen data via Bluetooth, physically blocked chip-based transactions, and included a PIN pad overlay. Here’s a closer look at the electronic gear jammed into these overlay skimmers. It includes a hidden PIN pad overlay [...]
