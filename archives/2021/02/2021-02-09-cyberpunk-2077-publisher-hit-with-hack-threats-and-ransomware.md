Title: Cyberpunk 2077 Publisher Hit with Hack, Threats and Ransomware
Date: 2021-02-09T15:33:11+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Malware
Slug: cyberpunk-2077-publisher-hit-with-hack-threats-and-ransomware

[Source](https://threatpost.com/cyberpunk-2077-publisher-hack-ransomware/163775/){:target="_blank" rel="noopener"}

> CD Projekt Red was hit with a cyberattack (possibly the work of the "Hello Kitty" gang), and the attackers are threatening to release source code for Witcher 3, corporate documents and more. [...]
