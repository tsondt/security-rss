Title: Bad education: Universities struggle to defend against surging cyber-attacks during coronavirus pandemic
Date: 2021-02-23T17:18:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: bad-education-universities-struggle-to-defend-against-surging-cyber-attacks-during-coronavirus-pandemic

[Source](https://portswigger.net/daily-swig/bad-education-universities-struggle-to-defend-against-surging-cyber-attacks-during-coronavirus-pandemic){:target="_blank" rel="noopener"}

> Number of ransomware attacks against the sector doubled between 2019 and 2020 [...]
