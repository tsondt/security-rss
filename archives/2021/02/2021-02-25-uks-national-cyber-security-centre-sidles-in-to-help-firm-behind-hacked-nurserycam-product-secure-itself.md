Title: UK's National Cyber Security Centre sidles in to help firm behind hacked NurseryCam product secure itself
Date: 2021-02-25T13:07:43+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uks-national-cyber-security-centre-sidles-in-to-help-firm-behind-hacked-nurserycam-product-secure-itself

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/25/ncsc_nurserycam_security/){:target="_blank" rel="noopener"}

> Plus: User passwords were stored in plain text after all The UK's National Cyber Security Centre is now helping IoT gadget firm FootfallCam Ltd secure product lines following the recent digital burglary of its nursery webcam operation.... [...]
