Title: Barcode scan app amassed millions of downloads before weird update starting popping open webpages...
Date: 2021-02-08T21:14:17+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: barcode-scan-app-amassed-millions-of-downloads-before-weird-update-starting-popping-open-webpages

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/08/barcode_scan_app_malwarebytes_update/){:target="_blank" rel="noopener"}

> Android software kicked out of Google Play Store, may still be active on many handhelds Barcode Scanner, a popular Android app, slipped undesirable code into an update in early December, an update that had the potential to reach more than 10m devices though actual distribution is believed to be far less.... [...]
