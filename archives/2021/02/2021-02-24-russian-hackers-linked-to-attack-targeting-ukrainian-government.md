Title: Russian hackers linked to attack targeting Ukrainian government
Date: 2021-02-24T12:08:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: russian-hackers-linked-to-attack-targeting-ukrainian-government

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-linked-to-attack-targeting-ukrainian-government/){:target="_blank" rel="noopener"}

> The National Security and Defense Council of Ukraine (NSDC) has linked Russian-backed hackers to attempts to compromise state agencies after breaching the government's document management system. [...]
