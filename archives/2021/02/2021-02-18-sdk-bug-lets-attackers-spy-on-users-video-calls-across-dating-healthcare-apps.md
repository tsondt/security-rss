Title: SDK Bug Lets Attackers Spy on User’s Video Calls Across Dating, Healthcare Apps
Date: 2021-02-18T14:01:54+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: sdk-bug-lets-attackers-spy-on-users-video-calls-across-dating-healthcare-apps

[Source](https://threatpost.com/sdk-bug-spy-calls-dating-healthcare-apps/164068/){:target="_blank" rel="noopener"}

> Apps like eHarmony and MeetMe are affected by a flaw in the Agora toolkit that went unpatched for eight months, researchers discovered. [...]
