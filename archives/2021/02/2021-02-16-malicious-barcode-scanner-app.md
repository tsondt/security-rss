Title: Malicious Barcode Scanner App
Date: 2021-02-16T12:13:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Android;malware;Ukraine
Slug: malicious-barcode-scanner-app

[Source](https://www.schneier.com/blog/archives/2021/02/malicious-barcode-scanner-app.html){:target="_blank" rel="noopener"}

> Interesting story about a barcode scanner app that has been pushing malware on to Android phones. The app is called Barcode Scanner. It’s been around since 2017 and is owned by the Ukrainian company Lavabird Ldt. But a December 2020 update included some new features: However, a rash of malicious activity was recently traced back to the app. Users began noticing something weird going on with their phones: their default browsers kept getting hijacked and redirected to random advertisements, seemingly out of nowhere. Generally, when this sort of thing happens it’s because the app was recently sold. That’s not the [...]
