Title: Microsoft Defender ATP detects Chrome updates as PHP backdoors
Date: 2021-02-03T11:17:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Microsoft
Slug: microsoft-defender-atp-detects-chrome-updates-as-php-backdoors

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-atp-detects-chrome-updates-as-php-backdoors/){:target="_blank" rel="noopener"}

> Microsoft Defender for Endpoint is currently detecting at least two Chrome updates as malware, tagging the Slovenian localization file bundled with the Google Chrome installer as a malicious file. [...]
