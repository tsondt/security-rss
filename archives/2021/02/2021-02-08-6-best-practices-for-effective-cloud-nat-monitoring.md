Title: 6 best practices for effective Cloud NAT monitoring
Date: 2021-02-08T17:00:00+00:00
Author: Ghaleb Al-Habian
Category: GCP Security
Tags: Identity & Security;Management Tools;Google Cloud Platform;Networking
Slug: 6-best-practices-for-effective-cloud-nat-monitoring

[Source](https://cloud.google.com/blog/products/networking/6-best-practices-for-running-cloud-nat/){:target="_blank" rel="noopener"}

> For anyone building distributed applications, Cloud Network Address Translation (NAT) is a powerful tool: with it, Compute Engine and Google Kubernetes Engine (GKE) workloads can access internet resources in a scalable and secure manner, without exposing the workloads running on them to outside access using external IPs. Cloud NAT features a proxy-less design, implementing NAT directly at the Andromeda SDN layer. As such, there’s no performance impact to your workload and it scales effortlessly to many VMs, regions and VPCs. In addition, you can combine Cloud NAT with private GKE clusters, enabling secure containerized workloads that are isolated from the [...]
