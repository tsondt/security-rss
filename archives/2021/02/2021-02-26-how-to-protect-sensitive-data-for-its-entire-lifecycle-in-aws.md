Title: How to protect sensitive data for its entire lifecycle in AWS
Date: 2021-02-26T20:54:29+00:00
Author: Raj Jain
Category: AWS Security
Tags: Amazon CloudFront;AWS Key Management Service;Expert (400);Lambda@Edge;Security, Identity, & Compliance;AWS KMS;CCPA;field-level encryption;GDPR;HIPAA;PCI;PHI;PII;RSA;Security Blog
Slug: how-to-protect-sensitive-data-for-its-entire-lifecycle-in-aws

[Source](https://aws.amazon.com/blogs/security/how-to-protect-sensitive-data-for-its-entire-lifecycle-in-aws/){:target="_blank" rel="noopener"}

> Many Amazon Web Services (AWS) customer workflows require ingesting sensitive and regulated data such as Payments Card Industry (PCI) data, personally identifiable information (PII), and protected health information (PHI). In this post, I’ll show you a method designed to protect sensitive data for its entire lifecycle in AWS. This method can help enhance your data [...]
