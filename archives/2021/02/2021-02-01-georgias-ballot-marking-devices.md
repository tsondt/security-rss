Title: Georgia’s Ballot-Marking Devices
Date: 2021-02-01T16:09:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: georgias-ballot-marking-devices

[Source](https://www.schneier.com/blog/archives/2021/02/georgias-ballot-marking-devices.html){:target="_blank" rel="noopener"}

> Andrew Appel discusses Georgia’s voting machines, how the paper ballots facilitated a recount, and the problem with automatic ballot-marking devices: Suppose the polling-place optical scanners had been hacked (enough to change the outcome). Then this would have been detected in the audit, and (in principle) Georgia would have been able to recover by doing a full recount. That’s what we mean when we say optical-scan voting machines have “strong software independence”­you can obtain a trustworthy result even if you’re not sure about the software in the machine on election day. If Georgia had still been using the paperless touchscreen DRE [...]
