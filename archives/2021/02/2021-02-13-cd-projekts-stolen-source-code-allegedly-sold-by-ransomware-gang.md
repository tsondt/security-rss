Title: CD Projekt's stolen source code allegedly sold by ransomware gang
Date: 2021-02-13T10:35:01-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: cd-projekts-stolen-source-code-allegedly-sold-by-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/cd-projekts-stolen-source-code-allegedly-sold-by-ransomware-gang/){:target="_blank" rel="noopener"}

> A ransomware gang who says they stole unencrypted source code for the company's most popular games and then encrypted CD Projekt's servers claims to have sold the data. [...]
