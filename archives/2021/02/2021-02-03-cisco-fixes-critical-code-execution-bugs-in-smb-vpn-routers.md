Title: Cisco fixes critical code execution bugs in SMB VPN routers
Date: 2021-02-03T12:24:31-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisco-fixes-critical-code-execution-bugs-in-smb-vpn-routers

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-critical-code-execution-bugs-in-smb-vpn-routers/){:target="_blank" rel="noopener"}

> Cisco has addressed multiple pre-auth remote code execution (RCE) vulnerabilities affecting several small business VPN routers and allowing attackers to execute arbitrary code as root on successfully exploited devices. [...]
