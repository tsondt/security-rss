Title: Say my CNAME: Rise of sneaky adtech tactic poses threat to web security and privacy
Date: 2021-02-25T15:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: say-my-cname-rise-of-sneaky-adtech-tactic-poses-threat-to-web-security-and-privacy

[Source](https://portswigger.net/daily-swig/say-my-cname-rise-of-sneaky-adtech-tactic-poses-threat-to-web-security-and-privacy){:target="_blank" rel="noopener"}

> Whack-a-mole game in play between trackers and ad blockers [...]
