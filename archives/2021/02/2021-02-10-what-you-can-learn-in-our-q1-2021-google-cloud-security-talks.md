Title: What you can learn in our Q1 2021 Google Cloud Security Talks
Date: 2021-02-10T17:00:00+00:00
Author: Rob Sadowski
Category: GCP Security
Tags: Google Cloud Platform;Google Workspace;Events;Identity & Security
Slug: what-you-can-learn-in-our-q1-2021-google-cloud-security-talks

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-security-talks-q1-2021/){:target="_blank" rel="noopener"}

> Join us for our first Google Cloud Security Talks of 2021, a live online event on March 3rd where we’ll help you navigate the latest in cloud security. We’ll share expert insights into our security ecosystem and cover the following topics Sunil Potti and Rob Sadowski will kick off Security Talks on March 3rd. Thomas Kurian and Juan Rajlin join us for a conversation on overcoming risk management challenges in the Cloud. This will be followed by a roundtable to get insight into cloud risk management with Phil Venables and leaders from the industry. Javier Soltero and Karthik Lakshminarayan will [...]
