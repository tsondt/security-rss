Title: Hoffman Construction shores up its defense systems after employee healthcare data breach
Date: 2021-02-16T16:14:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: hoffman-construction-shores-up-its-defense-systems-after-employee-healthcare-data-breach

[Source](https://portswigger.net/daily-swig/hoffman-construction-shores-up-its-defense-systems-after-employee-healthcare-data-breach){:target="_blank" rel="noopener"}

> Workers warned after healthcare plan data is potentially exposed [...]
