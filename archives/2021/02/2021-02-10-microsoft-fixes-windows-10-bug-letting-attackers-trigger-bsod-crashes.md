Title: Microsoft fixes Windows 10 bug letting attackers trigger BSOD crashes
Date: 2021-02-10T03:30:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-fixes-windows-10-bug-letting-attackers-trigger-bsod-crashes

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-windows-10-bug-letting-attackers-trigger-bsod-crashes/){:target="_blank" rel="noopener"}

> Microsoft has fixed a bug that could allow a threat actor to create specially crafted downloads that crash Windows 10 simply by opening the folder where they are downloaded. [...]
