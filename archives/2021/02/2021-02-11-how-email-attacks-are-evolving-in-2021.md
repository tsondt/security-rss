Title: How Email Attacks are Evolving in 2021
Date: 2021-02-11T15:52:38+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Videos;Web Security
Slug: how-email-attacks-are-evolving-in-2021

[Source](https://threatpost.com/email-security-attacks-bec/163869/){:target="_blank" rel="noopener"}

> The money being wire transferred by business email compromise victims is on the rise, as cybersecurity criminals evolve their tactics. [...]
