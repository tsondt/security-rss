Title: How to set up a recurring Security Hub summary email
Date: 2021-02-24T17:02:02+00:00
Author: Justin Criswell
Category: AWS Security
Tags: AWS Security Hub;Intermediate (200);Security, Identity, & Compliance;AWS CloudFormation;Cloud security;Security Blog;Security Hub notification;Serverless
Slug: how-to-set-up-a-recurring-security-hub-summary-email

[Source](https://aws.amazon.com/blogs/security/how-to-set-up-a-recurring-security-hub-summary-email/){:target="_blank" rel="noopener"}

> AWS Security Hub provides a comprehensive view of your security posture in Amazon Web Services (AWS) and helps you check your environment against security standards and best practices. In this post, we’ll show you how to set up weekly email notifications using Security Hub to provide account owners with a summary of the existing security [...]
