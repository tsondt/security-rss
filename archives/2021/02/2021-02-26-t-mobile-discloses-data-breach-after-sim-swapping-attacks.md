Title: T-Mobile discloses data breach after SIM swapping attacks
Date: 2021-02-26T15:18:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: t-mobile-discloses-data-breach-after-sim-swapping-attacks

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-discloses-data-breach-after-sim-swapping-attacks/){:target="_blank" rel="noopener"}

> American telecommunications provider T-Mobile has disclosed a data breach after an unknown number of customers were apparently affected by SIM swap attacks. [...]
