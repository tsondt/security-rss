Title: Palo Alto Networks drops $156m to absorb DevSecOps firm Bridgecrew
Date: 2021-02-17T11:20:55+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: palo-alto-networks-drops-156m-to-absorb-devsecops-firm-bridgecrew

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/17/palo_alto_networks_bridgecrew_buyout/){:target="_blank" rel="noopener"}

> Open-source stuff stays for now, company promises Palo Alto Networks (PAN) has described its $156m buy of cloudy DevSecOps biz Bridgecrew as a "key bet" at a time when the world has never been more reliant on off-premises computing.... [...]
