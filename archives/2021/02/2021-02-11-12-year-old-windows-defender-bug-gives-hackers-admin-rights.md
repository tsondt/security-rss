Title: 12-year-old Windows Defender bug gives hackers admin rights
Date: 2021-02-11T09:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: 12-year-old-windows-defender-bug-gives-hackers-admin-rights

[Source](https://www.bleepingcomputer.com/news/security/12-year-old-windows-defender-bug-gives-hackers-admin-rights/){:target="_blank" rel="noopener"}

> Microsoft has fixed a privilege escalation vulnerability in Microsoft Defender Antivirus (formerly Windows Defender) that could allow attackers to gain admin rights on unpatched Windows systems. [...]
