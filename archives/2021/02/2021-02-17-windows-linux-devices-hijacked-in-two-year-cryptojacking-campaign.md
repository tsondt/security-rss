Title: Windows, Linux Devices Hijacked In Two-Year Cryptojacking Campaign
Date: 2021-02-17T21:39:10+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware
Slug: windows-linux-devices-hijacked-in-two-year-cryptojacking-campaign

[Source](https://threatpost.com/windows-linux-devices-hijacked-in-two-year-cryptojacking-campaign/164048/){:target="_blank" rel="noopener"}

> The WatchDog malware has flown under the radar for two years in what researchers call one of the 'largest' Monero cryptojacking attacks ever. [...]
