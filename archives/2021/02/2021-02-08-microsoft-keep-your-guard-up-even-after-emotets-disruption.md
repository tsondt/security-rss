Title: Microsoft: Keep your guard up even after Emotet’s disruption
Date: 2021-02-08T13:53:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-keep-your-guard-up-even-after-emotets-disruption

[Source](https://www.bleepingcomputer.com/news/security/microsoft-keep-your-guard-up-even-after-emotet-s-disruption/){:target="_blank" rel="noopener"}

> Microsoft warns customers not to let their guard down even after hundreds of Emotet botnet servers were taken down in late January 2021. [...]
