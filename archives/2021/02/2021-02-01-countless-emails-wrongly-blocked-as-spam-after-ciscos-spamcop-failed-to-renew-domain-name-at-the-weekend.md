Title: Countless emails wrongly blocked as spam after Cisco's SpamCop failed to renew domain name at the weekend
Date: 2021-02-01T07:04:04+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: countless-emails-wrongly-blocked-as-spam-after-ciscos-spamcop-failed-to-renew-domain-name-at-the-weekend

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/01/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Second ransomware operation in the sights of Uncle Sam – and the insurance industry under fire for fueling extortionware rise In brief Cisco's anti-spam service SpamCop failed to renew spamcop.net over weekend, causing it to lapse, which resulted in countless messages being falsely labeled and rejected as spam around the world.... [...]
