Title: Spotify Suffers Second Credential-Stuffing Cyberattack in 3 Months
Date: 2021-02-04T19:31:33+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Privacy;Web Security
Slug: spotify-suffers-second-credential-stuffing-cyberattack-in-3-months

[Source](https://threatpost.com/spotify-credential-stuffing-cyberattack/163672/){:target="_blank" rel="noopener"}

> As many as 100,000 of the music streaming service's customers could face account takeover. [...]
