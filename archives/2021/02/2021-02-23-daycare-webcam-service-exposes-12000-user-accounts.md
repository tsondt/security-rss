Title: Daycare Webcam Service Exposes 12,000 User Accounts
Date: 2021-02-23T19:59:59+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Privacy;Vulnerabilities;Web Security
Slug: daycare-webcam-service-exposes-12000-user-accounts

[Source](https://threatpost.com/daycare-webcam-exposes-12000/164203/){:target="_blank" rel="noopener"}

> NurseryCam suspends service across 40 daycare centers until a security fix is in place. [...]
