Title: U.S. Indicts North Korean Hackers in Theft of $200 Million
Date: 2021-02-17T21:12:56+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;AppleJeus;APT 38;Cybersecurity and Infrastructure Agency;Department of Homeland Security;fbi;Ghaleb Alaumary;Hidden Cobra;Jon Chang Hyok;Kim Il;Lazarus Group;Marine Chain Token;Park Jin Hyok;Sony Pictures;WannaCry
Slug: us-indicts-north-korean-hackers-in-theft-of-200-million

[Source](https://krebsonsecurity.com/2021/02/u-s-indicts-north-korean-hackers-in-theft-of-200-million/){:target="_blank" rel="noopener"}

> The U.S. Justice Department today unsealed indictments against three men accused of working with the North Korean regime to carry out some of the most damaging cybercrime attacks over the past decade, including the 2014 hack of Sony Pictures, the global WannaCry ransomware contagion of 2017, and the theft of roughly $200 million and attempted theft of more than $1.2 billion from banks and other victims worldwide. Investigators with the DOJ, U.S. Secret Service and Department of Homeland Security told reporters on Wednesday the trio’s activities involved extortion, phishing, direct attacks on financial institutions and ATM networks, as well as [...]
