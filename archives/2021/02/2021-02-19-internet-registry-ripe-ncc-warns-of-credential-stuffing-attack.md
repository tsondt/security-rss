Title: Internet registry RIPE NCC warns of credential stuffing attack
Date: 2021-02-19T13:24:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: internet-registry-ripe-ncc-warns-of-credential-stuffing-attack

[Source](https://portswigger.net/daily-swig/internet-registry-ripe-ncc-warns-of-credential-stuffing-attack){:target="_blank" rel="noopener"}

> Malicious hackers targeted the organization’s members through SSO service [...]
