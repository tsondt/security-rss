Title: Kia Motors Hit With $20M Ransomware Attack – Report
Date: 2021-02-18T20:05:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Malware;Mobile Security
Slug: kia-motors-hit-with-20m-ransomware-attack-report

[Source](https://threatpost.com/kia-motors-ransomware-attack/164085/){:target="_blank" rel="noopener"}

> DoppelPaymer ransomware gang claims credit for Kia’s outage, demands $20 million in double-extortion attack. [...]
