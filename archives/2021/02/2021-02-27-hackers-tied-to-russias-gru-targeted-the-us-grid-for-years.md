Title: Hackers tied to Russia’s GRU targeted the US grid for years
Date: 2021-02-27T11:50:15+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;electric gird;GRU;hacking;Infrastructure;utilities;utlilties
Slug: hackers-tied-to-russias-gru-targeted-the-us-grid-for-years

[Source](https://arstechnica.com/?p=1745731){:target="_blank" rel="noopener"}

> Enlarge (credit: Yuri Smityuk | Getty Images) For all the nation-state hacker groups that have targeted the United States power grid —and even successfully breached American electric utilities —only the Russian military intelligence group known as Sandworm has been brazen enough to trigger actual blackouts, shutting the lights off in Ukraine in 2015 and 2016. Now one grid-focused security firm is warning that a group with ties to Sandworm’s uniquely dangerous hackers has also been actively targeting the US energy system for years. On Wednesday, industrial cybersecurity firm Dragos published its annual report on the state of industrial control systems [...]
