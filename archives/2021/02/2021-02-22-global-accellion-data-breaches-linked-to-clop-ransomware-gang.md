Title: Global Accellion data breaches linked to Clop ransomware gang
Date: 2021-02-22T09:06:36-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: global-accellion-data-breaches-linked-to-clop-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/global-accellion-data-breaches-linked-to-clop-ransomware-gang/){:target="_blank" rel="noopener"}

> Threat actors associated with a financially-motivated hacker groups combined multiple zero-day vulnerabilities and a new web shell to breach up to 100 companies using Accellion's legacy File Transfer Appliance and steal data. [...]
