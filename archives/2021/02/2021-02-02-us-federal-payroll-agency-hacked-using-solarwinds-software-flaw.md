Title: US federal payroll agency hacked using SolarWinds software flaw
Date: 2021-02-02T16:39:20-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-federal-payroll-agency-hacked-using-solarwinds-software-flaw

[Source](https://www.bleepingcomputer.com/news/security/us-federal-payroll-agency-hacked-using-solarwinds-software-flaw/){:target="_blank" rel="noopener"}

> The FBI has discovered that the National Finance Center (NFC), a U.S. Department of Agriculture (USDA) federal payroll agency, was compromised by exploiting a SolarWinds Orion software flaw, according to a Reuters report. [...]
