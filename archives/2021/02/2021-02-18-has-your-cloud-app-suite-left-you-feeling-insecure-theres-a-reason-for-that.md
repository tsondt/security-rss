Title: Has your cloud app suite left you feeling insecure? There’s a reason for that
Date: 2021-02-18T08:30:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: has-your-cloud-app-suite-left-you-feeling-insecure-theres-a-reason-for-that

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/18/webcast_mimecast_security_gap/){:target="_blank" rel="noopener"}

> Tune in next week and discover how to tighten that gap in your remote worker security Webcast You’ve seen large parts of your workforce take to the hills over the past year with nothing but a laptop and a Microsoft 365 account for company.... [...]
