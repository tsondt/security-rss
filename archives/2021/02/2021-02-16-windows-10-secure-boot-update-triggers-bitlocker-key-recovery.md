Title: Windows 10 Secure Boot update triggers BitLocker key recovery
Date: 2021-02-16T12:38:14-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: windows-10-secure-boot-update-triggers-bitlocker-key-recovery

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-10-secure-boot-update-triggers-bitlocker-key-recovery/){:target="_blank" rel="noopener"}

> Microsoft has acknowledged an issue affecting Windows 10 customers who have installed the KB4535680 security update that addresses a security feature bypass vulnerability in Secure Boot. [...]
