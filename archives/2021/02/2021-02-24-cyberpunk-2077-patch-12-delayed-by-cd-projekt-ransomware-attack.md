Title: Cyberpunk 2077 patch 1.2 delayed by CD Projekt ransomware attack
Date: 2021-02-24T13:14:07-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Gaming
Slug: cyberpunk-2077-patch-12-delayed-by-cd-projekt-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/gaming/cyberpunk-2077-patch-12-delayed-by-cd-projekt-ransomware-attack/){:target="_blank" rel="noopener"}

> CD Projekt Red announced today that they are delaying the anticipated Cyberpunk 2077 Patch 1.2 to the second half of March 2021 due to their recent cyberattack. [...]
