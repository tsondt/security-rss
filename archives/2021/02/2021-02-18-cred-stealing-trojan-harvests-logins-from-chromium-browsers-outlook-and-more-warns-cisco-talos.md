Title: Cred-stealing trojan harvests logins from Chromium browsers, Outlook and more, warns Cisco Talos
Date: 2021-02-18T07:25:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: cred-stealing-trojan-harvests-logins-from-chromium-browsers-outlook-and-more-warns-cisco-talos

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/18/masslogger_cisco_talos_research/){:target="_blank" rel="noopener"}

> Masslogger evolution rears its ugly head, $30 gets you three month license to cause carnage Cisco Talos has uncovered a credential-stealing trojan that lifts your login details from the Chrome browser, Microsoft's Outlook and instant messengers.... [...]
