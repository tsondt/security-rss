Title: Location tracking report: X-Mode SDK use much more widespread than first thought
Date: 2021-02-03T10:15:06+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: location-tracking-report-x-mode-sdk-use-much-more-widespread-than-first-thought

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/03/location_tracking_report_xmode_sdk/){:target="_blank" rel="noopener"}

> 450 Android apps tracked your whereabouts, 1.7bn downloads, 44% used X-Mode code Updated Apps that tracked and sold people's whereabouts were more prevalent than perhaps first thought. A report out today has identified 450 Android apps downloaded 1.7 billion times that used SDKs to track the location of smartphones. Many of these programs were using an SDK called X-Mode, which Apple and Google banned at the end of last year.... [...]
