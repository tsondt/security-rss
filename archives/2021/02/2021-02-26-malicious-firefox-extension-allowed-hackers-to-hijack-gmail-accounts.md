Title: Malicious Firefox extension allowed hackers to hijack Gmail accounts
Date: 2021-02-26T10:09:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: malicious-firefox-extension-allowed-hackers-to-hijack-gmail-accounts

[Source](https://www.bleepingcomputer.com/news/security/malicious-firefox-extension-allowed-hackers-to-hijack-gmail-accounts/){:target="_blank" rel="noopener"}

> Several Tibetan organizations were targeted in a cyber-espionage campaign by a state-backed hacking group using a malicious Firefox extension designed to hijack Gmail accounts and infect victims with malware. [...]
