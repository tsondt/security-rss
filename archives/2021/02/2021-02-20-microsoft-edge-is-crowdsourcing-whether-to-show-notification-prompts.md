Title: Microsoft Edge is crowdsourcing whether to show notification prompts
Date: 2021-02-20T15:05:03-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-edge-is-crowdsourcing-whether-to-show-notification-prompts

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-edge-is-crowdsourcing-whether-to-show-notification-prompts/){:target="_blank" rel="noopener"}

> Microsoft is now using crowdsourcing to determine whether to show a site's website subscription dialog prompt in the Microsoft Edge web browser. [...]
