Title: Dutch Research Council (NWO) confirms ransomware attack, data leak
Date: 2021-02-25T13:30:39-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: dutch-research-council-nwo-confirms-ransomware-attack-data-leak

[Source](https://www.bleepingcomputer.com/news/security/dutch-research-council-nwo-confirms-ransomware-attack-data-leak/){:target="_blank" rel="noopener"}

> The recent cyberattack that forced the Dutch Research Council (NWO) to take its servers offline and suspend grant allocation processes was caused by the DoppelPaymer ransomware gang. [...]
