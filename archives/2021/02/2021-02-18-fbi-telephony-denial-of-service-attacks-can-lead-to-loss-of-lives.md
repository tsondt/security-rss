Title: FBI: Telephony denial-of-service attacks can lead to loss of lives
Date: 2021-02-18T11:41:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-telephony-denial-of-service-attacks-can-lead-to-loss-of-lives

[Source](https://www.bleepingcomputer.com/news/security/fbi-telephony-denial-of-service-attacks-can-lead-to-loss-of-lives/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) has warned of the harsh consequences of telephony denial-of-service (TDoS) attacks and has also provided the steps needed to mitigate their impact. [...]
