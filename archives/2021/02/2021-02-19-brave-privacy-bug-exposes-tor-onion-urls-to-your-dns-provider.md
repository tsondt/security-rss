Title: Brave privacy bug exposes Tor onion URLs to your DNS provider
Date: 2021-02-19T11:37:52-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: brave-privacy-bug-exposes-tor-onion-urls-to-your-dns-provider

[Source](https://www.bleepingcomputer.com/news/security/brave-privacy-bug-exposes-tor-onion-urls-to-your-dns-provider/){:target="_blank" rel="noopener"}

> Brave Browser is fixing a privacy issue that leaks the Tor onion URL addresses you visit to your locally configured DNS server, exposing the dark web websites you visit. [...]
