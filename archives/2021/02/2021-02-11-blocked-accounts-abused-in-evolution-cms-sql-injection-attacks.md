Title: Blocked accounts abused in Evolution CMS SQL injection attacks
Date: 2021-02-11T15:29:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: blocked-accounts-abused-in-evolution-cms-sql-injection-attacks

[Source](https://portswigger.net/daily-swig/blocked-accounts-abused-in-evolution-cms-sql-injection-attacks){:target="_blank" rel="noopener"}

> Details of duo of flaws in management portal made public weeks after fix [...]
