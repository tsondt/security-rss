Title: Brave browser’s Tor feature found to leak .onion queries to ISPs
Date: 2021-02-19T14:27:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: brave-browsers-tor-feature-found-to-leak-onion-queries-to-isps

[Source](https://portswigger.net/daily-swig/brave-browsers-tor-feature-found-to-leak-onion-queries-to-isps){:target="_blank" rel="noopener"}

> Developers are issuing hotfix [...]
