Title: Cisco reveals critical bug in small biz VPN routers when half the world is stuck working at home
Date: 2021-02-05T07:05:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: cisco-reveals-critical-bug-in-small-biz-vpn-routers-when-half-the-world-is-stuck-working-at-home

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/05/cisco_critical_rv_vpn_router_bugs/){:target="_blank" rel="noopener"}

> And we all know how good small business are at patching... NOT Cisco has addressed a clutch of critical vulnerabilities in its small business and VPN routers that can be exploited by an unauthenticated, remote attacker to execute arbitrary code as the root user. All the attacker needs to do is send a maliciously crafted HTTP request to the web-based management interface.... [...]
