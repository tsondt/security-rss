Title: Imperva pretty adamant that security analytics aggregator product Sonar is not 'one dashboard to rule them all'
Date: 2021-02-26T21:48:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: imperva-pretty-adamant-that-security-analytics-aggregator-product-sonar-is-not-one-dashboard-to-rule-them-all

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/26/imperva_sonar_security_dashboard/){:target="_blank" rel="noopener"}

> Uh huh... it's a good time to be in enterprise security analytics Tired of keeping up with security alerts from your system? Worried that your Security Operations Centre (SOC) is getting deluged in low-level reporting? Fear not: Imperva has produced an aggregator aggregation product that sits over the top of all your other alert-generating security software.... [...]
