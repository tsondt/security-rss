Title: Singtel, QIMR Berghofer report Accellion-related data breaches
Date: 2021-02-11T12:55:35-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: singtel-qimr-berghofer-report-accellion-related-data-breaches

[Source](https://www.bleepingcomputer.com/news/security/singtel-qimr-berghofer-report-accellion-related-data-breaches/){:target="_blank" rel="noopener"}

> Singtel and the QIMR Berghofer Medical Research Institute are the latest companies to disclose data breaches caused by a vulnerability in the Accellion FTA secure file transfer software. [...]
