Title: Multiple new flaws uncovered in SolarWinds software just weeks after high-profile supply chain attack
Date: 2021-02-03T15:45:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: multiple-new-flaws-uncovered-in-solarwinds-software-just-weeks-after-high-profile-supply-chain-attack

[Source](https://portswigger.net/daily-swig/multiple-new-flaws-uncovered-in-solarwinds-software-just-weeks-after-high-profile-supply-chain-attack){:target="_blank" rel="noopener"}

> Attention ‘provides the sunlight necessary to expose major flaws’, says Trustwave [...]
