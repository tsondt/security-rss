Title: U.K. Arrest in ‘SMS Bandits’ Phishing Service
Date: 2021-02-01T15:21:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;National Crime Agency;otp agency;proofpoint;Sasha Angus;Scylla Intel;smishing;SMS phishing;SMSBandits
Slug: uk-arrest-in-sms-bandits-phishing-service

[Source](https://krebsonsecurity.com/2021/02/u-k-arrest-in-sms-bandits-phishing-service/){:target="_blank" rel="noopener"}

> Authorities in the United Kingdom have arrested a 20-year-old man for allegedly operating an online service for sending high-volume phishing campaigns via mobile text messages. The service, marketed in the underground under the name “ SMS Bandits,” has been responsible for blasting out huge volumes of phishing lures spoofing everything from COVID-19 pandemic relief efforts to PayPal, telecommunications providers and tax revenue agencies. The U.K.’s National Crime Agency (NCA) declined to name the suspect, but confirmed that the Metropolitan Police Service’s cyber crime unit had detained an individual from Birmingham in connection to a business that supplied “criminal services related [...]
