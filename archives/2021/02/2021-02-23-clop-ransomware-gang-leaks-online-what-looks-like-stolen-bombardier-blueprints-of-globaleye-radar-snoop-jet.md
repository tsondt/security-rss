Title: Clop ransomware gang leaks online what looks like stolen Bombardier blueprints of GlobalEye radar snoop jet
Date: 2021-02-23T21:22:02+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: clop-ransomware-gang-leaks-online-what-looks-like-stolen-bombardier-blueprints-of-globaleye-radar-snoop-jet

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/23/bombardier_clop_ransomware_leaks/){:target="_blank" rel="noopener"}

> And what may be CAD drawing of a military radar antenna The Clop ransomware gang claims to have stolen documents from aerospace giant Bombardier’s defense division – and has leaked what appears to be a CAD drawing of one of its military aircraft products, raising fears over what else they’ve got.... [...]
