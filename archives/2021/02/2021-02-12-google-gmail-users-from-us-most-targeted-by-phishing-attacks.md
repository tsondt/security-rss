Title: Google: Gmail users from US most targeted by phishing attacks
Date: 2021-02-12T13:50:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-gmail-users-from-us-most-targeted-by-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-gmail-users-from-us-most-targeted-by-phishing-attacks/){:target="_blank" rel="noopener"}

> Google has revealed earlier this week that Gmail users from the United States are the most popular target for email-based phishing and malware attacks. [...]
