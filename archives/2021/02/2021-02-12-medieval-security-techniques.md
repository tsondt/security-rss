Title: Medieval Security Techniques
Date: 2021-02-12T20:05:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: authentication;history of security
Slug: medieval-security-techniques

[Source](https://www.schneier.com/blog/archives/2021/02/medieval-security-techniques.html){:target="_blank" rel="noopener"}

> Sonja Drummer describes (with photographs) two medieval security techniques. The first is a for authentication: a document has been cut in half with an irregular pattern, so that the two halves can be brought together to prove authenticity. The second is for integrity: hashed lines written above and below a block of text ensure that no one can add additional text at a later date. [...]
