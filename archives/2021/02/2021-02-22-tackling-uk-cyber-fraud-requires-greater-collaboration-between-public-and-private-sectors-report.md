Title: Tackling UK cyber fraud requires greater collaboration between public and private sectors – report
Date: 2021-02-22T16:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: tackling-uk-cyber-fraud-requires-greater-collaboration-between-public-and-private-sectors-report

[Source](https://portswigger.net/daily-swig/tackling-uk-cyber-fraud-requires-greater-collaboration-between-public-and-private-sectors-report){:target="_blank" rel="noopener"}

> Paper calls for increased knowledge sharing between government and businesses [...]
