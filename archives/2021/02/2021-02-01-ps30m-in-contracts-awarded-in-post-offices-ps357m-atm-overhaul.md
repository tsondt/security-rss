Title: £30m in contracts awarded in Post Office's £357m ATM overhaul
Date: 2021-02-01T12:22:10+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: ps30m-in-contracts-awarded-in-post-offices-ps357m-atm-overhaul

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/01/30m_contracts_awarded_as_part/){:target="_blank" rel="noopener"}

> New network will have 600 fewer cash machines The UK Post Office has awarded two contracts worth a total of £30m for a banking network and ATMs system in a procurement expected to be worth £357m once all contracts are awarded.... [...]
