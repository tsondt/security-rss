Title: Ignore that ransomware demand and restore from backup – well... if only it were that easy
Date: 2021-02-09T07:30:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: ignore-that-ransomware-demand-and-restore-from-backup-well-if-only-it-were-that-easy

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/09/ignore_that_ransomware_demand/){:target="_blank" rel="noopener"}

> Do you really think your safe copies aren’t top of the cyber-cims' hit list? Webcast Ransomware is one of the most insidious forms of cyber-attack, not least because it presents you with a range of highly unpleasant options.... [...]
