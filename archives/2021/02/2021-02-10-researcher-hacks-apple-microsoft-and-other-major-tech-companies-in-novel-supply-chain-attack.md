Title: Researcher hacks Apple, Microsoft, and other major tech companies in novel supply chain attack
Date: 2021-02-10T15:04:29+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: researcher-hacks-apple-microsoft-and-other-major-tech-companies-in-novel-supply-chain-attack

[Source](https://portswigger.net/daily-swig/researcher-hacks-apple-microsoft-and-other-major-tech-companies-in-novel-supply-chain-attack){:target="_blank" rel="noopener"}

> Bug hunter rewarded with $130k payout for ‘dependency confusion’ vulnerability [...]
