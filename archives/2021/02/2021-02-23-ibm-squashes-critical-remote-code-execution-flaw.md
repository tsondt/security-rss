Title: IBM Squashes Critical Remote Code-Execution Flaw
Date: 2021-02-23T19:36:32+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: ibm-squashes-critical-remote-code-execution-flaw

[Source](https://threatpost.com/ibm-critical-remote-code-execution-flaw/164187/){:target="_blank" rel="noopener"}

> A critical-severity buffer-overflow flaw that affects IBM Integration Designer could allow remote attackers to execute code. [...]
