Title: North Korean attacks on crypto exchanges reportedly netted $316m in two years
Date: 2021-02-10T04:54:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: north-korean-attacks-on-crypto-exchanges-reportedly-netted-316m-in-two-years

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/10/north_korea_cryptocurrency/){:target="_blank" rel="noopener"}

> United Nations sanctions made silly by sloppy security North Korean attacks on crypto exchanges reportedly netted an estimated $316m in cryptocurrency in 2019 and 2020, according to a report by Japan’s Nikkei.... [...]
