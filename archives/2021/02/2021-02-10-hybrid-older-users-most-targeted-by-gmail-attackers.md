Title: Hybrid, Older Users Most-Targeted by Gmail Attackers
Date: 2021-02-10T19:07:08+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security;Most Recent ThreatLists;Web Security
Slug: hybrid-older-users-most-targeted-by-gmail-attackers

[Source](https://threatpost.com/hybrid-older-users-gmail-attackers/163826/){:target="_blank" rel="noopener"}

> Researchers at Google and Stanford analyzed a 1.2 billion malicious emails to find out what makes users likely to get attacked. 2FA wasn't a big factor. [...]
