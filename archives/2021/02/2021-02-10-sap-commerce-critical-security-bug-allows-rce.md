Title: SAP Commerce Critical Security Bug Allows RCE
Date: 2021-02-10T21:32:28+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: sap-commerce-critical-security-bug-allows-rce

[Source](https://threatpost.com/sap-commerce-critical-security-bug/163822/){:target="_blank" rel="noopener"}

> The critical SAP cybersecurity flaw could allow for the compromise of an application used by e-commerce businesses. [...]
