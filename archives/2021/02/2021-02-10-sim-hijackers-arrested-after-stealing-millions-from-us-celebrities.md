Title: SIM hijackers arrested after stealing millions from US celebrities
Date: 2021-02-10T10:34:46-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: sim-hijackers-arrested-after-stealing-millions-from-us-celebrities

[Source](https://www.bleepingcomputer.com/news/security/sim-hijackers-arrested-after-stealing-millions-from-us-celebrities/){:target="_blank" rel="noopener"}

> Ten men part of a criminal gang involved in series of SIM swapping attacks targeting high-profile victims in the United States were arrested in the UK, Malta, and Belgium. [...]
