Title: Apple fixes SUDO root privilege escalation flaw in macOS
Date: 2021-02-09T19:07:10-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Apple
Slug: apple-fixes-sudo-root-privilege-escalation-flaw-in-macos

[Source](https://www.bleepingcomputer.com/news/apple/apple-fixes-sudo-root-privilege-escalation-flaw-in-macos/){:target="_blank" rel="noopener"}

> Apple has fixed a sudo vulnerability in macOS Big Sur, Catalina, and Mojave, allowing any local user to gain root-level privileges. [...]
