Title: Yandex Data Breach Exposes 4K+ Email Accounts
Date: 2021-02-12T20:17:10+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Breach;Hacks
Slug: yandex-data-breach-exposes-4k-email-accounts

[Source](https://threatpost.com/yandex-data-breach-email-accounts/163960/){:target="_blank" rel="noopener"}

> In a security notice, Yandex said an employee had been providing unauthorized access to users’ email accounts “for personal gain.” [...]
