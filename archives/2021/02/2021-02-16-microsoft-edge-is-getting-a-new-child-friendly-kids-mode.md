Title: Microsoft Edge is getting a new child-friendly Kids Mode
Date: 2021-02-16T15:54:21-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-edge-is-getting-a-new-child-friendly-kids-mode

[Source](https://www.bleepingcomputer.com/news/security/microsoft-edge-is-getting-a-new-child-friendly-kids-mode/){:target="_blank" rel="noopener"}

> Microsoft is adding a new 'Kids Mode' to the Microsoft Edge browser that provides a safe environment for children to browse the web and consume family-friendly content. [...]
