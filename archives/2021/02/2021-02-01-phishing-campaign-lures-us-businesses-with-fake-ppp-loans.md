Title: Phishing campaign lures US businesses with fake PPP loans
Date: 2021-02-01T14:15:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: phishing-campaign-lures-us-businesses-with-fake-ppp-loans

[Source](https://www.bleepingcomputer.com/news/security/phishing-campaign-lures-us-businesses-with-fake-ppp-loans/){:target="_blank" rel="noopener"}

> Threat actors are sending phishing emails impersonating a Small Business Administration (SBA) lender to prey on US business owners who want to apply for a PPP loan to keep their business going during the COVID-19 crisis. [...]
