Title: Android users now have an easy way to check the security of their passwords
Date: 2021-02-24T12:52:00+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;android;google;passwords
Slug: android-users-now-have-an-easy-way-to-check-the-security-of-their-passwords

[Source](https://arstechnica.com/?p=1744701){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Google is adding its password checkup feature to Android, making the mobile OS the latest company offering to give users an easy way to check if the passcodes they’re using have been compromised. Password Checkup works by checking credentials entered into apps against a list of billions of credentials compromised in the innumerable website breaches that have occurred in recent years. In the event there’s a match, users receive an alert, along with a prompt that can take them to Google’s password manager page, which offers a way to review the security of all saved credentials. [...]
