Title: Nespresso smart cards hacked to provide infinite coffee after someone wasn't too perky about security
Date: 2021-02-04T06:40:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: nespresso-smart-cards-hacked-to-provide-infinite-coffee-after-someone-wasnt-too-perky-about-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/04/nespresso_cards_hacked/){:target="_blank" rel="noopener"}

> Older commercial machines rely on insecure Mifare Classic payments Some commercial Nespresso machines in Europe that incorporate a smart card payment system can be manipulated to add unlimited funds to purchase coffee, thanks to reliance on technology that's been known to be insecure for more than a decade.... [...]
