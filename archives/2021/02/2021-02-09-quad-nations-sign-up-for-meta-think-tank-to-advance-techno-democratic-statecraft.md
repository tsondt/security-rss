Title: ‘Quad’ nations sign up for meta think-tank to advance ‘Techno-Democratic Statecraft’
Date: 2021-02-09T05:01:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: quad-nations-sign-up-for-meta-think-tank-to-advance-techno-democratic-statecraft

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/09/quad_tech_network/){:target="_blank" rel="noopener"}

> As China bristles, USA, Japan, India, and Australia create forum to explore the intersection of tech and regional security Universities and think tanks from Australia, the USA, Japan, and India have come together in a new group that together hopes to advance discussions on the intersection of information technology, regional security, and internet freedom.... [...]
