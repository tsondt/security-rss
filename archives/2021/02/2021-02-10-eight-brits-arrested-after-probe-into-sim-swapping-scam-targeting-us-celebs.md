Title: Eight Brits arrested after probe into SIM-swapping scam targeting US celebs
Date: 2021-02-10T16:15:07+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: eight-brits-arrested-after-probe-into-sim-swapping-scam-targeting-us-celebs

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/10/eight_arrested_sim_swapping_scam/){:target="_blank" rel="noopener"}

> National Crime Agency nabbed network that stole money, Bitcoin, personal data Brit cops have cuffed eight men in England and Scotland amid a probe into SIM-swapping attacks on high-profile US targets – including sports stars, musicians, and "influencers" – that had money and personal data stolen.... [...]
