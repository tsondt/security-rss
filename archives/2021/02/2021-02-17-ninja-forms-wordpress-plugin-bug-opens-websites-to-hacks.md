Title: Ninja Forms WordPress Plugin Bug Opens Websites to Hacks
Date: 2021-02-17T19:57:26+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: ninja-forms-wordpress-plugin-bug-opens-websites-to-hacks

[Source](https://threatpost.com/ninja-forms-wordpress-plugin-hacks/164042/){:target="_blank" rel="noopener"}

> The popular plugin is installed on more than 1 million websites, and has four flaws that allow various kinds of serious attacks, including site takeover and email hijacking. [...]
