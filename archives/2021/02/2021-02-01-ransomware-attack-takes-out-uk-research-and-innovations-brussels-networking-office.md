Title: Ransomware attack takes out UK Research and Innovation's Brussels networking office
Date: 2021-02-01T16:24:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ransomware-attack-takes-out-uk-research-and-innovations-brussels-networking-office

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/01/ukri_ransomware_ukro_brussels/){:target="_blank" rel="noopener"}

> 'Sensitive' personal data not accessed – so what about names and contact deets? UK Research and Innovation, the British government's science and research organisation, has temporarily turned off a couple of its web-facing services after an apparent ransomware attack.... [...]
