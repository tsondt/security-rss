Title: Masslogger Swipes Microsoft Outlook, Google Chrome Credentials
Date: 2021-02-17T16:31:40+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware;Web Security
Slug: masslogger-swipes-microsoft-outlook-google-chrome-credentials

[Source](https://threatpost.com/masslogger-microsoft-outlook-google-chrome/164011/){:target="_blank" rel="noopener"}

> A new version of the Masslogger trojan has been targeting Windows users - now using a compiled HTML (CHM) file format to start the infection chain. [...]
