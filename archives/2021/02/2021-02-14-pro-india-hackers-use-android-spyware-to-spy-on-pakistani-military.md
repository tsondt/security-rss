Title: Pro-India hackers use Android spyware to spy on Pakistani military
Date: 2021-02-14T12:12:06-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Government
Slug: pro-india-hackers-use-android-spyware-to-spy-on-pakistani-military

[Source](https://www.bleepingcomputer.com/news/security/pro-india-hackers-use-android-spyware-to-spy-on-pakistani-military/){:target="_blank" rel="noopener"}

> This week a report has revealed details on the two spyware strains leveraged by state-sponsored threat actors during the India-Pakistan conflict. The malware strains named Hornbill and SunBird have been delivered as fake Android apps (APKs) by the Confucius advanced persistent threat group (APT), a state-sponsored operation. [...]
