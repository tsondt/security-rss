Title: In wake of Apple privacy controls, Facebook mulls just begging its iOS app users to let it track them over the web
Date: 2021-02-01T23:42:33+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: in-wake-of-apple-privacy-controls-facebook-mulls-just-begging-its-ios-app-users-to-let-it-track-them-over-the-web

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/01/facebook_app_privacy_justification/){:target="_blank" rel="noopener"}

> I am once again asking for your financial support, says Zuckerberg's empire Facebook has created a new screen in its iOS app that will urge people to allow it to continue stalking their online activities for targeted advertising.... [...]
