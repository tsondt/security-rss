Title: Microsoft warns of increasing OAuth Office 365 phishing attacks
Date: 2021-02-05T12:07:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-warns-of-increasing-oauth-office-365-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-increasing-oauth-office-365-phishing-attacks/){:target="_blank" rel="noopener"}

> Microsoft has warned of an increasing number of consent phishing (aka OAuth phishing) attacks targeting remote workers during recent months, BleepingComputer has learned. [...]
