Title: Yandex suffers data breach after sysadmin sold access to user emails
Date: 2021-02-12T11:02:37-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: yandex-suffers-data-breach-after-sysadmin-sold-access-to-user-emails

[Source](https://www.bleepingcomputer.com/news/security/yandex-suffers-data-breach-after-sysadmin-sold-access-to-user-emails/){:target="_blank" rel="noopener"}

> Russian internet and search company Yandex announced today that one of its system administrators had enabled unauthorized access to thousands of user mailboxes. [...]
