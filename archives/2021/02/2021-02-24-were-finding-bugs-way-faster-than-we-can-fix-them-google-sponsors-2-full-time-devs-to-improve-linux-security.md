Title: 'We're finding bugs way faster than we can fix them': Google sponsors 2 full-time devs to improve Linux security
Date: 2021-02-24T16:01:14+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: were-finding-bugs-way-faster-than-we-can-fix-them-google-sponsors-2-full-time-devs-to-improve-linux-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/24/google_ups_linux_security_effort/){:target="_blank" rel="noopener"}

> Plus: Why the Chocolate Factory only uses code it builds from source Interview Worried about the security of Linux and open-source code, Google is sponsoring a pair of full-time developers to work on the kernel's security.... [...]
