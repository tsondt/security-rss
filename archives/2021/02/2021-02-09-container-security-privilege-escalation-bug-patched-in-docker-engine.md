Title: Container security: Privilege escalation bug patched in Docker Engine
Date: 2021-02-09T12:47:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: container-security-privilege-escalation-bug-patched-in-docker-engine

[Source](https://portswigger.net/daily-swig/container-security-privilege-escalation-bug-patched-in-docker-engine){:target="_blank" rel="noopener"}

> ‘An odd one, impact wise’ [...]
