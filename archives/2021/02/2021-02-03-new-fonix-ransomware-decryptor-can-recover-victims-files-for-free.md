Title: New Fonix ransomware decryptor can recover victim's files for free
Date: 2021-02-03T17:55:34-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: new-fonix-ransomware-decryptor-can-recover-victims-files-for-free

[Source](https://www.bleepingcomputer.com/news/security/new-fonix-ransomware-decryptor-can-recover-victims-files-for-free/){:target="_blank" rel="noopener"}

> Kaspersky has released a decryptor for the Fonix Ransomware (XONIF) that allows victims to recover their encrypted files for free. [...]
