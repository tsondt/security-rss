Title: Chrome 89 beta: Google presses on with 'advanced hardware interactions' that Mozilla, Apple see as harmful
Date: 2021-02-01T17:54:07+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: chrome-89-beta-google-presses-on-with-advanced-hardware-interactions-that-mozilla-apple-see-as-harmful

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/01/chrome_89_beta_brings_desktop/){:target="_blank" rel="noopener"}

> Adding Serial API, Web NFC support, richer human interface device support Google has released a beta of Chrome 89, adding further hardware interaction APIs even though Mozilla and Apple consider many of these features harmful, as well as introducing a desktop-sharing API for Windows and Chrome OS.... [...]
