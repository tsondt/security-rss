Title: Microsoft fixes the Windows 10 console driver crash bug
Date: 2021-02-10T03:30:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-fixes-the-windows-10-console-driver-crash-bug

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-the-windows-10-console-driver-crash-bug/){:target="_blank" rel="noopener"}

> Microsoft has fixed a bug that could allow a threat actor to create specially crafted downloads that crash Windows 10 simply by opening the folder where they are downloaded. [...]
