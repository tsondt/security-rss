Title: NoxPlayer Android Emulator Supply-Chain Attack
Date: 2021-02-08T12:34:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Android;data protection;malware;supply chain
Slug: noxplayer-android-emulator-supply-chain-attack

[Source](https://www.schneier.com/blog/archives/2021/02/noxplayer-android-emulator-supply-chain-attack.html){:target="_blank" rel="noopener"}

> It seems to be the season of sophisticated supply-chain attacks. This one is in the NoxPlayer Android emulator : ESET says that based on evidence its researchers gathered, a threat actor compromised one of the company’s official API (api.bignox.com) and file-hosting servers (res06.bignox.com). Using this access, hackers tampered with the download URL of NoxPlayer updates in the API server to deliver malware to NoxPlayer users. [...] Despite evidence implying that attackers had access to BigNox servers since at least September 2020, ESET said the threat actor didn’t target all of the company’s users but instead focused on specific machines, suggesting [...]
