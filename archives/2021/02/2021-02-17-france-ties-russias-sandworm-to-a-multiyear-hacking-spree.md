Title: France ties Russia’s Sandworm to a multiyear hacking spree
Date: 2021-02-17T01:26:20+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Centreon;hacking;notpetya;russia;sandworm
Slug: france-ties-russias-sandworm-to-a-multiyear-hacking-spree

[Source](https://arstechnica.com/?p=1742949){:target="_blank" rel="noopener"}

> Enlarge / The logo of the French national cybersecurity agency Agence Nationale de la securite des systemes d'information(ANSSI) taken at ANSSI headquarters in Paris. (credit: Eric Piermont | AFP | Getty Images ) The Russian military hackers known as Sandworm, responsible for everything from blackouts in Ukraine to NotPetya, the most destructive malware in history, don't have a reputation for discretion. But a French security agency now warns that hackers with tools and techniques it links to Sandworm have stealthily hacked targets in that country by exploiting an IT monitoring tool called Centreon—and appear to have gotten away with it [...]
