Title: Hackers auction alleged stolen Cyberpunk 2077, Witcher source code
Date: 2021-02-10T10:43:45-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Gaming
Slug: hackers-auction-alleged-stolen-cyberpunk-2077-witcher-source-code

[Source](https://www.bleepingcomputer.com/news/security/hackers-auction-alleged-stolen-cyberpunk-2077-witcher-source-code/){:target="_blank" rel="noopener"}

> Threat actors are auctioning the alleged source code for CD Projekt Red games, including Witcher 3, Thronebreaker, and Cyberpunk 2077, that they state were stolen in a ransomware attack. [...]
