Title: Egregor ransomware members arrested by Ukrainian, French police
Date: 2021-02-14T13:46:20-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: egregor-ransomware-members-arrested-by-ukrainian-french-police

[Source](https://www.bleepingcomputer.com/news/security/egregor-ransomware-members-arrested-by-ukrainian-french-police/){:target="_blank" rel="noopener"}

> A joint operation between French and Ukrainian law enforcement has reportedly led to the arrests of several members of the Egregor ransomware operation in Ukraine. [...]
