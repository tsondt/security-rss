Title: Hacker Sets Alleged Auction for Witcher 3 Source Code
Date: 2021-02-10T21:20:19+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: hacker-sets-alleged-auction-for-witcher-3-source-code

[Source](https://threatpost.com/hacker-auction-witcher-source-code/163851/){:target="_blank" rel="noopener"}

> The ransomware gang behind the hack of CD Projekt Red may be asking for $1 million opening bids for the company's valuable data. [...]
