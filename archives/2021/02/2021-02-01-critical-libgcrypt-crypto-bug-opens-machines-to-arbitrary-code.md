Title: Critical Libgcrypt Crypto Bug Opens Machines to Arbitrary Code
Date: 2021-02-01T16:59:19+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cryptography;Vulnerabilities
Slug: critical-libgcrypt-crypto-bug-opens-machines-to-arbitrary-code

[Source](https://threatpost.com/critical-libgcrypt-crypto-bug-arbitrary-code/163546/){:target="_blank" rel="noopener"}

> The flaw in the free-source library could have been ported to multiple applications. [...]
