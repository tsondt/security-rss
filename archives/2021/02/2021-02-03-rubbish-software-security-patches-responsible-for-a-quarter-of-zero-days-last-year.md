Title: Rubbish software security patches responsible for a quarter of zero-days last year
Date: 2021-02-03T08:03:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: rubbish-software-security-patches-responsible-for-a-quarter-of-zero-days-last-year

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/03/enigma_patch_zero/){:target="_blank" rel="noopener"}

> Google wants researchers, vendors to stop making attacks easy Enigma To limit the impact of zero-day vulnerabilities, Google security researcher Maddie Stone would like those developing software fixes to stop delivering shoddy patches.... [...]
