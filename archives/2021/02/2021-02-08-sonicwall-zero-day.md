Title: SonicWall Zero-Day
Date: 2021-02-08T18:11:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberattack;cybersecurity;hacking;zero-day
Slug: sonicwall-zero-day

[Source](https://www.schneier.com/blog/archives/2021/02/sonicwall-zero-day.html){:target="_blank" rel="noopener"}

> Hackers are exploiting zero-day in SonicWall: In an email, an NCC Group spokeswoman wrote: “Our team has observed signs of an attempted exploitation of a vulnerabilitythat affects the SonicWall SMA 100 series devices. We are working closely with SonicWall to investigate this in more depth.” In Monday’s update, SonicWall representatives said the company’s engineering team confirmed that the submission by NCC Group included a “critical zero-day” in the SMA 100 series 10.x code. SonicWall is tracking it as SNWLID-2021-0001. The SMA 100 series is a line of secure remote access appliances. The disclosure makes SonicWall at least the fifth large [...]
