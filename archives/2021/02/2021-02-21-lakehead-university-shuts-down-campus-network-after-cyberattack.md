Title: Lakehead University shuts down campus network after cyberattack
Date: 2021-02-21T12:01:01-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: lakehead-university-shuts-down-campus-network-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/lakehead-university-shuts-down-campus-network-after-cyberattack/){:target="_blank" rel="noopener"}

> Canadian undergraduate research university Lakehead has been dealing with a cyberattack that forced the institution earlier this week to cut off access to its servers. [...]
