Title: Skype ‘spoofing vulnerabilities’ are a haven for social engineering attacks, security researcher claims
Date: 2021-02-05T14:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: skype-spoofing-vulnerabilities-are-a-haven-for-social-engineering-attacks-security-researcher-claims

[Source](https://portswigger.net/daily-swig/skype-spoofing-vulnerabilities-are-a-haven-for-social-engineering-attacks-security-researcher-claims){:target="_blank" rel="noopener"}

> Microsoft doesn’t feel the bugs are important enough to fix immediately, although one researcher disagrees [...]
