Title: Don't fear the authentication: Google Drive edition
Date: 2021-02-09T17:30:00+00:00
Author: Gabe Weiss
Category: GCP Security
Tags: Google Cloud Platform;Drive;Google Workspace;Identity & Security;Developers & Practitioners
Slug: dont-fear-the-authentication-google-drive-edition

[Source](https://cloud.google.com/blog/topics/developers-practitioners/dont-fear-authentication-google-drive-edition/){:target="_blank" rel="noopener"}

> There are times when I’m building an application on GCP when I don’t want to use a more traditional datastore like Cloud SQL or Bigtable. Right now, for example, I’m building an app that allows non-technical folks to easily add icons into a build system. I’m not going to write a front-end from scratch, and teaching them source control, while valuable, isn’t really something I wanted to tackle right now. So an easy solution is to use Google Drive. Maybe you never thought of it as a data store...but let’s talk about it here for a minute. Super simple interface, [...]
