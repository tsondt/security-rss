Title: Kroger data breach exposes pharmacy and employee data
Date: 2021-02-20T12:57:44-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: kroger-data-breach-exposes-pharmacy-and-employee-data

[Source](https://www.bleepingcomputer.com/news/security/kroger-data-breach-exposes-pharmacy-and-employee-data/){:target="_blank" rel="noopener"}

> Supermarket giant Kroger has suffered a data breach after a service used to transfer files securely was hacked, and threat actors stole files. [...]
