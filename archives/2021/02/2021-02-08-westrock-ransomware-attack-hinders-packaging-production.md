Title: WestRock Ransomware Attack Hinders Packaging Production
Date: 2021-02-08T16:39:52+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Critical Infrastructure;Hacks;Malware
Slug: westrock-ransomware-attack-hinders-packaging-production

[Source](https://threatpost.com/westrock-ransomware-attack/163717/){:target="_blank" rel="noopener"}

> The ransomware attack, affecting OT systems, resulted in some of WestRock's facilities lagging in production levels. [...]
