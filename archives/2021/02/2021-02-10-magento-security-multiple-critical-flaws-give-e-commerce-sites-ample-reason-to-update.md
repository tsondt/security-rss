Title: Magento security: Multiple critical flaws give e-commerce sites ample reason to update
Date: 2021-02-10T17:06:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: magento-security-multiple-critical-flaws-give-e-commerce-sites-ample-reason-to-update

[Source](https://portswigger.net/daily-swig/magento-security-multiple-critical-flaws-give-e-commerce-sites-ample-reason-to-update){:target="_blank" rel="noopener"}

> Adobe urges users to update popular e-commerce platform [...]
