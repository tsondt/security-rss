Title: Copycats imitate novel supply chain attack that hit tech giants
Date: 2021-02-12T12:11:03-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Software;Technology
Slug: copycats-imitate-novel-supply-chain-attack-that-hit-tech-giants

[Source](https://www.bleepingcomputer.com/news/security/copycats-imitate-novel-supply-chain-attack-that-hit-tech-giants/){:target="_blank" rel="noopener"}

> This week, hundreds of new packages have been published to the npm open-source repository named after private components being internally used by major companies. These npm packages are identical to the proof-of-concept packages created by Alex Birsan, the researcher who had recently managed to infiltrate over major 35 tech firms. [...]
