Title: Supply-Chain Hack Breaches 35 Companies, Including PayPal, Microsoft, Apple
Date: 2021-02-10T13:49:32+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Bug Bounty;Vulnerabilities
Slug: supply-chain-hack-breaches-35-companies-including-paypal-microsoft-apple

[Source](https://threatpost.com/supply-chain-hack-paypal-microsoft-apple/163814/){:target="_blank" rel="noopener"}

> Ethical hacker Alex Birsan developed a way to inject malicious code into open-source developer tools to exploit dependencies in organizations internal applications. [...]
