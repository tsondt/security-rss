Title: VC giant Sequoia Capital discloses data breach after failed BEC attack
Date: 2021-02-25T09:36:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: vc-giant-sequoia-capital-discloses-data-breach-after-failed-bec-attack

[Source](https://www.bleepingcomputer.com/news/security/vc-giant-sequoia-capital-discloses-data-breach-after-failed-bec-attack/){:target="_blank" rel="noopener"}

> American VC firm Sequoia Capital has disclosed a data breach following what looks like a failed business email compromise (BEC) attack from January. [...]
