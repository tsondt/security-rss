Title: Checkout Skimmers Powered by Chip Cards
Date: 2021-02-23T15:53:04+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: All About Skimmers;shimmer;smart card skimmer
Slug: checkout-skimmers-powered-by-chip-cards

[Source](https://krebsonsecurity.com/2021/02/checkout-skimmers-powered-by-chip-cards/){:target="_blank" rel="noopener"}

> Easily the most sophisticated skimming devices made for hacking terminals at retail self-checkout lanes are a new breed of PIN pad overlay combined with a flexible, paper-thin device that fits inside the terminal’s chip reader slot. What enables these skimmers to be so slim? They draw their power from the low-voltage current that gets triggered when a chip-based card is inserted. As a result, they do not require external batteries, and can remain in operation indefinitely. A point-of-sale skimming device that consists of a PIN pad overlay (top) and a smart card skimmer (a.k.a. “shimmer”). The entire device folds onto [...]
