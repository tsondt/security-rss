Title: Just 2020 things: Miscreants hit remote desktops 700% harder as world's IT teams try to support locked-down staff
Date: 2021-02-09T14:26:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: just-2020-things-miscreants-hit-remote-desktops-700-harder-as-worlds-it-teams-try-to-support-locked-down-staff

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/09/eset_threat_report_2020/){:target="_blank" rel="noopener"}

> Bar the doors lest you get targeted Online criminals have increasingly targeted Remote Desktop Protocol connections over the past year, according to infosec biz ESET.... [...]
