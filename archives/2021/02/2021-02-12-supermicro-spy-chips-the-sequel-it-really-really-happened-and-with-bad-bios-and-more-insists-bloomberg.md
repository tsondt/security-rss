Title: Supermicro spy chips, the sequel: It really, really happened, and with bad BIOS and more, insists Bloomberg
Date: 2021-02-12T23:28:36+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: supermicro-spy-chips-the-sequel-it-really-really-happened-and-with-bad-bios-and-more-insists-bloomberg

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/12/supermicro_bloomberg_spying/){:target="_blank" rel="noopener"}

> Server maker says latest article is 'a mishmash of disparate allegations' Following up on a disputed 2018 claim in its BusinessWeek publication that tiny spy chips were found on Supermicro server motherboards in 2015, Bloomberg on Friday doubled down by asserting that Supermicro's products were targeted by Chinese operatives for over a decade, that US intelligence officials have been aware of this, and that authorities kept this information quiet while crafting defenses in order to study the attack.... [...]
