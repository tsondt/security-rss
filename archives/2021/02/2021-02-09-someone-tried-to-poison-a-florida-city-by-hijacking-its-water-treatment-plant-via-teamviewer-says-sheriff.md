Title: Someone tried to poison a Florida city by hijacking its water treatment plant via TeamViewer, says sheriff
Date: 2021-02-09T00:18:13+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: someone-tried-to-poison-a-florida-city-by-hijacking-its-water-treatment-plant-via-teamviewer-says-sheriff

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/09/florida_water_hacked/){:target="_blank" rel="noopener"}

> Attempt to flood supply with sodium hydroxide thwarted, safeguards would have kicked in anyway, we're told The sheriff of a small city in Florida warned on Monday that hackers had tried to poison its water.... [...]
