Title: Google Chrome Zero-Day Afflicts Windows, Mac Users
Date: 2021-02-05T15:47:55+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security
Slug: google-chrome-zero-day-afflicts-windows-mac-users

[Source](https://threatpost.com/google-chrome-zero-day-windows-mac/163688/){:target="_blank" rel="noopener"}

> Google warns of a zero-day vulnerability in the V8 open-source engine that's being actively exploited by attackers. [...]
