Title: Cybersecurity conferences 2021: A schedule of virtual, and potentially in-person or ‘hybrid’, events
Date: 2021-02-25T17:36:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cybersecurity-conferences-2021-a-schedule-of-virtual-and-potentially-in-person-or-hybrid-events

[Source](https://portswigger.net/daily-swig/cybersecurity-conferences-2021-a-schedule-of-virtual-and-potentially-in-person-or-hybrid-events){:target="_blank" rel="noopener"}

> Your one-stop reference for infosec events announced for 2021 so far [...]
