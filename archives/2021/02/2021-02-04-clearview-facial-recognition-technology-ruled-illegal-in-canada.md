Title: Clearview Facial-Recognition Technology Ruled Illegal in Canada
Date: 2021-02-04T12:52:35+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Privacy
Slug: clearview-facial-recognition-technology-ruled-illegal-in-canada

[Source](https://threatpost.com/clearview-facial-recognition-canada/163650/){:target="_blank" rel="noopener"}

> The company’s controversial practice of collecting and selling billions of faceprints was dealt a heavy blow by the Privacy Commissioner that could set a precedent in other legal challenges. [...]
