Title: Texas electric company warns of scammers threatening to cut power
Date: 2021-02-22T14:54:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: texas-electric-company-warns-of-scammers-threatening-to-cut-power

[Source](https://www.bleepingcomputer.com/news/security/texas-electric-company-warns-of-scammers-threatening-to-cut-power/){:target="_blank" rel="noopener"}

> Texas electric utility Austin Energy today warned of unknown individuals impersonating the company and threatening customers over the phone that their power will be cut off unless they pay fictitious overdue bills. [...]
