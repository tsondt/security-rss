Title: Chrome users have faced 3 security concerns over the past 24 hours
Date: 2021-02-05T21:21:38+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;browsers;chrome;exploits;extensions;google;vulnerabilities;zerodays
Slug: chrome-users-have-faced-3-security-concerns-over-the-past-24-hours

[Source](https://arstechnica.com/?p=1740160){:target="_blank" rel="noopener"}

> (credit: Chrome ) Users of Google’s Chrome browser have faced three security concerns over the past 24 hours in the form of a malicious extension with more than 2 million users, a just-fixed zero-day, and new information about how malware can abuse Chrome's sync feature to bypass firewalls. Let’s discuss them one by one. First up, the Great Suspender, an extension with more than 2 million downloads from the Chrome Web Store, has been pulled from Google servers and deleted from users’ computers. The extension has been an almost essential tool for users with small amounts of RAM on their [...]
