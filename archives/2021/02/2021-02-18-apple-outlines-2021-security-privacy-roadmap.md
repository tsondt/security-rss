Title: Apple Outlines 2021 Security, Privacy Roadmap
Date: 2021-02-18T21:04:03+00:00
Author: Tom Spring
Category: Threatpost
Tags: Malware;Privacy;Web Security
Slug: apple-outlines-2021-security-privacy-roadmap

[Source](https://threatpost.com/apple-2021-platform-security-guide/164094/){:target="_blank" rel="noopener"}

> Latest Apple Platform Security update folds iOS, macOS and hardware into security 2021 roadmap. [...]
