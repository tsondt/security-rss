Title: High-performance computers are under siege by a newly discovered backdoor
Date: 2021-02-02T20:46:18+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;backdoors;high-performance computing;malware
Slug: high-performance-computers-are-under-siege-by-a-newly-discovered-backdoor

[Source](https://arstechnica.com/?p=1739124){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) High-performance computer networks, some belonging to the world’s most prominent organizations, are under attack by a newly discovered backdoor that gives hackers the ability to remotely execute commands of their choice, researchers said on Tuesday. Kobalos, as researchers from security firm Eset have named the malware, is a backdoor that runs on Linux, FreeBSD, and Solaris, and code artifacts suggest it may have once run on AIX and the ancient Windows 3.11 and Windows 95 platforms. The backdoor was released into the wild no later than 2019, and the group behind it was active throughout last [...]
