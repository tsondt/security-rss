Title: Popular Node.js package vulnerable to command injection attacks
Date: 2021-02-23T15:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: popular-nodejs-package-vulnerable-to-command-injection-attacks

[Source](https://portswigger.net/daily-swig/popular-node-js-package-vulnerable-to-command-injection-attacks){:target="_blank" rel="noopener"}

> Developer of ‘systeminformation’ library addresses moderate severity flaw in security update [...]
