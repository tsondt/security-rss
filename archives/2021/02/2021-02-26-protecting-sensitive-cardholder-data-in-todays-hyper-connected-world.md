Title: Protecting Sensitive Cardholder Data in Today’s Hyper-Connected World
Date: 2021-02-26T13:25:22+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Web Security
Slug: protecting-sensitive-cardholder-data-in-todays-hyper-connected-world

[Source](https://threatpost.com/protecting-sensitive-cardholder-data-in-todays-hyper-connected-world/164277/){:target="_blank" rel="noopener"}

> Retailers that lacked significant digital presence pre-COVID are now reaching new audiences through e-commerce sites that are accessible anytime, from anywhere, on any device. [...]
