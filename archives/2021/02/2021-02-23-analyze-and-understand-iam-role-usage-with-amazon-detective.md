Title: Analyze and understand IAM role usage with Amazon Detective
Date: 2021-02-23T17:23:04+00:00
Author: Sheldon Sides
Category: AWS Security
Tags: Advanced (300);Amazon Detective;AWS Identity and Access Management (IAM);Security, Identity, & Compliance;AWS IAM;Security Blog
Slug: analyze-and-understand-iam-role-usage-with-amazon-detective

[Source](https://aws.amazon.com/blogs/security/analyze-and-understand-iam-role-usage-with-amazon-detective/){:target="_blank" rel="noopener"}

> In this blog post, we’ll demonstrate how you can use Amazon Detective’s new role session analysis feature to investigate security findings that are tied to the usage of an AWS Identity and Access Management (IAM) role. You’ll learn about how you can use this new role session analysis feature to determine which Amazon Web Services [...]
