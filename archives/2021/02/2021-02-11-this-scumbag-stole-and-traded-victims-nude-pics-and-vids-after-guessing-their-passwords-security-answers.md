Title: This scumbag stole and traded victims' nude pics and vids after guessing their passwords, security answers
Date: 2021-02-11T01:00:12+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: this-scumbag-stole-and-traded-victims-nude-pics-and-vids-after-guessing-their-passwords-security-answers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/11/hacker_suny_pictures/){:target="_blank" rel="noopener"}

> Nicholas Faber joins accomplice Michael Fish in admitting he raided university portal for sensitive info A college graduate has admitted hacking into the email and online accounts of female students, stealing their nude photos and videos, and trading them with others.... [...]
