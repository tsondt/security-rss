Title: How do you fix a problem like open-source security? Google has an idea tho constraints may not go down well
Date: 2021-02-04T19:32:53+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: how-do-you-fix-a-problem-like-open-source-security-google-has-an-idea-tho-constraints-may-not-go-down-well

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/04/google_open_source_security/){:target="_blank" rel="noopener"}

> 'Try telling leaders of libpng, libjpeg-turbo, openssl, ffmpeg etc they can't make "unilateral" changes to their own projects' Google has proposed a framework for discussing and addressing open-source security based on factors like verified identity, code review, and trusted builds, but its approach may be at odds with open-source culture.... [...]
