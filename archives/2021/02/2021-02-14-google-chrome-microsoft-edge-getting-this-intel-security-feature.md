Title: Google Chrome, Microsoft Edge getting this Intel security feature
Date: 2021-02-14T16:00:49-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: google-chrome-microsoft-edge-getting-this-intel-security-feature

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-microsoft-edge-getting-this-intel-security-feature/){:target="_blank" rel="noopener"}

> Chromium-based browsers such as Microsoft Edge and Google Chrome will soon support the Intel CET security feature to prevent a wide range of vulnerabilities. [...]
