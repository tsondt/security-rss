Title: VMware fixes critical RCE bug in all default vCenter installs
Date: 2021-02-23T14:26:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: vmware-fixes-critical-rce-bug-in-all-default-vcenter-installs

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-critical-rce-bug-in-all-default-vcenter-installs/){:target="_blank" rel="noopener"}

> VMware has addressed a critical remote code execution (RCE) vulnerability in the vCenter Server virtual infrastructure management platform that may allow attackers to potentially take control of affected systems. [...]
