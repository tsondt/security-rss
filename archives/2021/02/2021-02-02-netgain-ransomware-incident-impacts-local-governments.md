Title: Netgain ransomware incident impacts local governments
Date: 2021-02-02T03:32:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: netgain-ransomware-incident-impacts-local-governments

[Source](https://www.bleepingcomputer.com/news/security/netgain-ransomware-incident-impacts-local-governments/){:target="_blank" rel="noopener"}

> The ransomware incident that Netgain, a provider of managed IT services, had late last year rippled onto its customers. Now, Ramsey County, Minnesota, is informing clients of the Family Health Division program that the hackers may have accessed personal data. [...]
