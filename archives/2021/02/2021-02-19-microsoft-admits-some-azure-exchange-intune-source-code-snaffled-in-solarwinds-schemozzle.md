Title: Microsoft admits some Azure, Exchange, Intune source code snaffled in SolarWinds schemozzle
Date: 2021-02-19T02:32:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: microsoft-admits-some-azure-exchange-intune-source-code-snaffled-in-solarwinds-schemozzle

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/19/microsoft_source_code/){:target="_blank" rel="noopener"}

> We’ll be fine, says Redmond security crew. No word on whether you will be too once crims analyse their haul Microsoft has admitted that as a result of installing backdoored SolarWinds tools in some parts of its corporate network, portions of its source code was obtained and exfiltrated by parties unknown.... [...]
