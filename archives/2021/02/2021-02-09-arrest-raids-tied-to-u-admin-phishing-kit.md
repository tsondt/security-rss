Title: Arrest, Raids Tied to ‘U-Admin’ Phishing Kit
Date: 2021-02-09T03:16:54+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Web Fraud 2.0;Australian Federal Police;Brad Marden;Check Point Research;fr3d.hk;Intel 471;Kaktys;Qakbot;Remco Verhoef;sans internet storm center;U-Admin
Slug: arrest-raids-tied-to-u-admin-phishing-kit

[Source](https://krebsonsecurity.com/2021/02/arrest-raids-tied-to-u-admin-phishing-kit/){:target="_blank" rel="noopener"}

> Cyber cops in Ukraine carried out an arrest and several raids last week in connection with the author of a U-Admin, a software package used to administer what’s being called “one of the world’s largest phishing services.” The operation was carried out in coordination with the FBI and authorities in Australia, which was particularly hard hit by phishing scams perpetrated by U-Admin customers. The U-Admin phishing panel interface. Image: fr3d.hk/blog The Ukrainian attorney general’s office said it worked with the nation’s police force to identify a 39-year-old man from the Ternopil region who developed a phishing package and special administrative [...]
