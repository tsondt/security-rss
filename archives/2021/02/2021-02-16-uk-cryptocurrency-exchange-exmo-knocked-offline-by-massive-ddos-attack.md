Title: UK cryptocurrency exchange EXMO knocked offline by ‘massive’ DDoS attack
Date: 2021-02-16T12:26:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uk-cryptocurrency-exchange-exmo-knocked-offline-by-massive-ddos-attack

[Source](https://portswigger.net/daily-swig/uk-cryptocurrency-exchange-exmo-knocked-offline-by-massive-ddos-attack){:target="_blank" rel="noopener"}

> Services halted by unknown attacker [...]
