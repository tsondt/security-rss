Title: WEIS 2021 Call for Papers
Date: 2021-02-18T20:14:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: conferences;economics of security
Slug: weis-2021-call-for-papers

[Source](https://www.schneier.com/blog/archives/2021/02/weis-2021-call-for-papers.html){:target="_blank" rel="noopener"}

> The 20th Annual Workshop on the Economics of Information Security (WEIS 2021) will be held online in June. We just published the call for papers. [...]
