Title: Finnish IT services giant TietoEVRY discloses ransomware attack
Date: 2021-02-23T14:22:26-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: finnish-it-services-giant-tietoevry-discloses-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/finnish-it-services-giant-tietoevry-discloses-ransomware-attack/){:target="_blank" rel="noopener"}

> Finnish IT services giant TietoEVRY has suffered a ransomware attack that forced them to disconnect clients' services. [...]
