Title: Google looks at bypass in Chromium's ASLR security defense, throws hands up, won't patch garbage issue
Date: 2021-02-26T11:58:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: google-looks-at-bypass-in-chromiums-aslr-security-defense-throws-hands-up-wont-patch-garbage-issue

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/26/chrome_aslr_bypass/){:target="_blank" rel="noopener"}

> Engineers write off GC abuse because Spectre broke everything anyway In early November, a developer contributing to Google's open-source Chromium project reported a problem with Oilpan, the garbage collector for the browser's Blink rendering engine: it can be used to break a memory defense known as address space layout randomization (ASLR).... [...]
