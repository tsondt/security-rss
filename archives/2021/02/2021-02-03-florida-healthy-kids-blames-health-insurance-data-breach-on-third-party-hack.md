Title: Florida Healthy Kids blames health insurance data breach on third-party hack
Date: 2021-02-03T15:17:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: florida-healthy-kids-blames-health-insurance-data-breach-on-third-party-hack

[Source](https://portswigger.net/daily-swig/florida-healthy-kids-blames-health-insurance-data-breach-on-third-party-hack){:target="_blank" rel="noopener"}

> Non-profit alleges that web hosting platform failed to apply security patches [...]
