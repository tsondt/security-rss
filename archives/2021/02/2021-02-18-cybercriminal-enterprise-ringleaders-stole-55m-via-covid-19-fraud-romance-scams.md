Title: Cybercriminal Enterprise ‘Ringleaders’ Stole $55M Via COVID-19 Fraud, Romance Scams
Date: 2021-02-18T21:30:16+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;Web Security
Slug: cybercriminal-enterprise-ringleaders-stole-55m-via-covid-19-fraud-romance-scams

[Source](https://threatpost.com/cybercriminal-enterprise-ringleaders-stole-55m-via-covid-19-fraud-romance-scams/164086/){:target="_blank" rel="noopener"}

> The Department of Justice (DoJ) cracked down on a Ghana-based cybercriminal enterprise behind a slew of romance scams, COVID-19 fraud attacks and business email compromise schemes since 2013. [...]
