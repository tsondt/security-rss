Title: SHAREit fixes security bugs in app with 1 billion downloads
Date: 2021-02-22T13:28:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: shareit-fixes-security-bugs-in-app-with-1-billion-downloads

[Source](https://www.bleepingcomputer.com/news/security/shareit-fixes-security-bugs-in-app-with-1-billion-downloads/){:target="_blank" rel="noopener"}

> Singapore-based Smart Media4U Technology said today that it fixed SHAREit vulnerabilities that may have allowed attackers to execute arbitrary code remotely on users' devices. [...]
