Title: Experian investigating data breach claims in Brazil
Date: 2021-02-08T13:09:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: experian-investigating-data-breach-claims-in-brazil

[Source](https://portswigger.net/daily-swig/experian-investigating-data-breach-claims-in-brazil){:target="_blank" rel="noopener"}

> Questions surround source of breach as Experian says database includes details not held by firm [...]
