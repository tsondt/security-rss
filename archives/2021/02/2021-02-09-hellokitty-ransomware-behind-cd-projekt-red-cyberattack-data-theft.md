Title: HelloKitty ransomware behind CD Projekt Red cyberattack, data theft
Date: 2021-02-09T15:42:45-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hellokitty-ransomware-behind-cd-projekt-red-cyberattack-data-theft

[Source](https://www.bleepingcomputer.com/news/security/hellokitty-ransomware-behind-cd-projekt-red-cyberattack-data-theft/){:target="_blank" rel="noopener"}

> The ransomware attack against CD Projekt Red was conducted by a ransomware group that goes by the name 'HelloKitty,' and yes, that's the name the threat actors utilize. [...]
