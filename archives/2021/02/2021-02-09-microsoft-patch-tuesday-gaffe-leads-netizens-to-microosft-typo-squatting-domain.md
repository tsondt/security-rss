Title: Microsoft Patch Tuesday gaffe leads netizens to 'Microosft' typo-squatting domain
Date: 2021-02-09T22:02:23+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: microsoft-patch-tuesday-gaffe-leads-netizens-to-microosft-typo-squatting-domain

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/09/microsoft_patch_tuesday/){:target="_blank" rel="noopener"}

> That aside, enjoy the light load of 56 vulns in Windows and other code Patch Tuesday For its February Patch Day, Microsoft released security advisories covering 56 CVE-assigned vulnerabilities, 11 of them rated critical.... [...]
