Title: What's CNAME of your game? This DNS-based tracking defies your browser privacy defenses
Date: 2021-02-24T06:11:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: whats-cname-of-your-game-this-dns-based-tracking-defies-your-browser-privacy-defenses

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/24/dns_cname_tracking/){:target="_blank" rel="noopener"}

> Study sees increasing adoption of cloaking to bypass cookie barriers Boffins based in Belgium have found that a DNS-based technique for bypassing defenses against online tracking has become increasingly common and represents a growing threat to both privacy and security.... [...]
