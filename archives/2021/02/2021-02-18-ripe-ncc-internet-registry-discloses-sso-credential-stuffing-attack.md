Title: RIPE NCC Internet Registry discloses SSO credential stuffing attack
Date: 2021-02-18T14:00:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ripe-ncc-internet-registry-discloses-sso-credential-stuffing-attack

[Source](https://www.bleepingcomputer.com/news/security/ripe-ncc-internet-registry-discloses-sso-credential-stuffing-attack/){:target="_blank" rel="noopener"}

> RIPE NCC is warning members that they suffered a credential stuffing attack attempting to gain access to single sign-on (SSO) accounts. [...]
