Title: Magento Web Skimmers Piggyback in Ongoing Costway Website Compromise
Date: 2021-02-02T17:31:10+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security
Slug: magento-web-skimmers-piggyback-in-ongoing-costway-website-compromise

[Source](https://threatpost.com/magento-web-skimmers-costway/163593/){:target="_blank" rel="noopener"}

> An e-commerce credit-card skimmer is being used by a second skimmer to steal payment data - and both are on Costway's website. [...]
