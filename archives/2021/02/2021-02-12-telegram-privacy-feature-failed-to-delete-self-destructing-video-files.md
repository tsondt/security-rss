Title: Telegram privacy feature failed to delete self-destructing video files
Date: 2021-02-12T14:57:56-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: telegram-privacy-feature-failed-to-delete-self-destructing-video-files

[Source](https://www.bleepingcomputer.com/news/security/telegram-privacy-feature-failed-to-delete-self-destructing-video-files/){:target="_blank" rel="noopener"}

> Telegram has fixed a security issue where self-destructing audio and video recording were not being deleted from user's macOS devices as expected. [...]
