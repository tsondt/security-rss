Title: Is there a widening gulf between you and your remote workers? Yes – and it’s security shaped
Date: 2021-02-04T07:30:09+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: is-there-a-widening-gulf-between-you-and-your-remote-workers-yes-and-its-security-shaped

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/04/mind_the_security_gap_regcast/){:target="_blank" rel="noopener"}

> Tune in online this month and learn how to mind the security gap Webcast It’s been almost a year since large parts of the workforce beat a hasty retreat from their offices, and began a mass experiment in working from home, often courtesy of Microsoft 365.... [...]
