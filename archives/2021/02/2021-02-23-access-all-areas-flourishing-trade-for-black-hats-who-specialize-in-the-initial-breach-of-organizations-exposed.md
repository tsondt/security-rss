Title: Access all areas: Flourishing trade for black hats who specialize in the initial breach of organizations exposed
Date: 2021-02-23T13:44:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: access-all-areas-flourishing-trade-for-black-hats-who-specialize-in-the-initial-breach-of-organizations-exposed

[Source](https://portswigger.net/daily-swig/access-all-areas-flourishing-trade-for-black-hats-who-specialize-in-the-initial-breach-of-organizations-exposed){:target="_blank" rel="noopener"}

> Compromised network access averaging out at around $7,000 on underground markets [...]
