Title: Stalkerware Volumes Remain Concerningly High, Despite Bans
Date: 2021-02-26T21:26:21+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Most Recent ThreatLists
Slug: stalkerware-volumes-remain-concerningly-high-despite-bans

[Source](https://threatpost.com/stalkerware-volumes-high-bans/164325/){:target="_blank" rel="noopener"}

> COVID-19 impacted volumes for the year, but the U.S. moved into third place on the list of countries most infected by stalkerware. [...]
