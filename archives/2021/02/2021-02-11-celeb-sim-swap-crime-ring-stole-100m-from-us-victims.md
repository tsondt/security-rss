Title: Celeb SIM-Swap Crime Ring Stole $100M from U.S. Victims
Date: 2021-02-11T16:03:00+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Mobile Security;Privacy;Web Security
Slug: celeb-sim-swap-crime-ring-stole-100m-from-us-victims

[Source](https://threatpost.com/celeb-sim-swap-crime-ring-100m/163888/){:target="_blank" rel="noopener"}

> The attackers ported victims' cell phone lines and then defeated 2FA to access accounts and apps. [...]
