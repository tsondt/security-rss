Title: Second SolarWinds Attack Group Breaks into USDA Payroll — Report
Date: 2021-02-03T21:22:49+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Vulnerabilities
Slug: second-solarwinds-attack-group-breaks-into-usda-payroll-report

[Source](https://threatpost.com/second-solarwinds-attack-group-usda-payroll/163635/){:target="_blank" rel="noopener"}

> A second APT, potentially linked to the Chinese government, could be behind the Supernova malware. [...]
