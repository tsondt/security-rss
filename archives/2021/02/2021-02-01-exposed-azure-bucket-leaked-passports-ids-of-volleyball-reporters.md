Title: Exposed Azure bucket leaked passports, IDs of volleyball reporters
Date: 2021-02-01T10:45:03-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Cloud;Microsoft
Slug: exposed-azure-bucket-leaked-passports-ids-of-volleyball-reporters

[Source](https://www.bleepingcomputer.com/news/security/exposed-azure-bucket-leaked-passports-ids-of-volleyball-reporters/){:target="_blank" rel="noopener"}

> A publicly exposed cloud storage bucket was found to contain images of hundreds of passports and identity documents belonging to journalists and volleyball players from around the world. [...]
