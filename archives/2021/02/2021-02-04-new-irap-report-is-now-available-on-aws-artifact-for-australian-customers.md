Title: New IRAP report is now available on AWS Artifact for Australian customers
Date: 2021-02-04T21:45:07+00:00
Author: Henry Xu
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;ACSC;Australian government;Cloud security guidance;IRAP;IRAP PROTECTED;Security Blog
Slug: new-irap-report-is-now-available-on-aws-artifact-for-australian-customers

[Source](https://aws.amazon.com/blogs/security/new-irap-report-is-now-available-on-aws-artifact-for-australian-customers/){:target="_blank" rel="noopener"}

> We are excited to announce that a new Information Security Registered Assessors Program (IRAP) report is now available on AWS Artifact. The new IRAP documentation pack brings new services in scope, and includes a Cloud Security Control Matrix (CSCM) for specific information to help customers assess each applicable control that is required by the Australian [...]
