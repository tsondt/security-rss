Title: Florida Water Plant Hack: Leaked Credentials Found in Breach Database
Date: 2021-02-12T15:34:06+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Critical Infrastructure;Hacks
Slug: florida-water-plant-hack-leaked-credentials-found-in-breach-database

[Source](https://threatpost.com/florida-water-plant-hack-credentials-breach/163919/){:target="_blank" rel="noopener"}

> Researchers discovered credentials for the Oldsmar water treatment facility in the massive compilation of data from breaches posted just days before the attack. [...]
