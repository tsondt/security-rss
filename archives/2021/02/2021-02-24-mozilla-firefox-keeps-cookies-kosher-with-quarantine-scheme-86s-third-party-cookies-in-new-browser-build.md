Title: Mozilla Firefox keeps cookies kosher with quarantine scheme, 86s third-party cookies in new browser build
Date: 2021-02-24T07:02:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: mozilla-firefox-keeps-cookies-kosher-with-quarantine-scheme-86s-third-party-cookies-in-new-browser-build

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/24/firefox_cookies_86/){:target="_blank" rel="noopener"}

> Hey man, are your cookies trackin' me? Take 'em out. You gotta keep 'em separated Mozilla has revised the way the latest build of the Firefox browser handles HTTP cookies to prevent third-parties from using them to track people online, as part of improvements in build 86 of the code.... [...]
