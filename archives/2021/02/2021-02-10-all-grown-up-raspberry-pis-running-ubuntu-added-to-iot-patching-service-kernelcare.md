Title: All grown up: Raspberry Pis running Ubuntu added to IoT patching service KernelCare
Date: 2021-02-10T12:30:05+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: all-grown-up-raspberry-pis-running-ubuntu-added-to-iot-patching-service-kernelcare

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/10/pi_kernelcare/){:target="_blank" rel="noopener"}

> No downtime for major updates CloudLinux has added the Raspberry Pi to its KernelCare patching service, although only if you're running Ubuntu.... [...]
