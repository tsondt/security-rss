Title: New phishing attack uses Morse code to hide malicious URLs
Date: 2021-02-07T10:40:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: new-phishing-attack-uses-morse-code-to-hide-malicious-urls

[Source](https://www.bleepingcomputer.com/news/security/new-phishing-attack-uses-morse-code-to-hide-malicious-urls/){:target="_blank" rel="noopener"}

> A new targeted phishing campaign includes the novel obfuscation technique of using Morse code to hide malicious URLs in an email attachment. [...]
