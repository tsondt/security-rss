Title: Call for comments: European Data Protection Board lays out data breach notification guidelines for organizations
Date: 2021-02-10T12:29:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: call-for-comments-european-data-protection-board-lays-out-data-breach-notification-guidelines-for-organizations

[Source](https://portswigger.net/daily-swig/call-for-comments-european-data-protection-board-lays-out-data-breach-notification-guidelines-for-organizations){:target="_blank" rel="noopener"}

> Regulator asks for industry feedback as it develops breach notification examples for a range of cyber-attacks [...]
