Title: CIS now offers free ransomware protection to all US hospitals
Date: 2021-02-19T12:05:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cis-now-offers-free-ransomware-protection-to-all-us-hospitals

[Source](https://www.bleepingcomputer.com/news/security/cis-now-offers-free-ransomware-protection-to-all-us-hospitals/){:target="_blank" rel="noopener"}

> The Center for Internet Security (CIS), a non-profit dedicated to securing IT systems and data, announced the launch of free ransomware protection for US private hospitals through the Malicious Domain Blocking and Reporting (MDBR) service. [...]
