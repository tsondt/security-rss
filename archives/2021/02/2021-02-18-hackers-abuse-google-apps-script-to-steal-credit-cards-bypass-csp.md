Title: Hackers abuse Google Apps Script to steal credit cards, bypass CSP
Date: 2021-02-18T12:56:41-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hackers-abuse-google-apps-script-to-steal-credit-cards-bypass-csp

[Source](https://www.bleepingcomputer.com/news/security/hackers-abuse-google-apps-script-to-steal-credit-cards-bypass-csp/){:target="_blank" rel="noopener"}

> Attackers are abusing Google's Apps Script business application development platform to steal credit card information submitted by customers of e-commerce websites while shopping online. [...]
