Title: Android Devices Prone to Botnet’s DDoS Onslaught
Date: 2021-02-04T21:47:10+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: android-devices-prone-to-botnets-ddos-onslaught

[Source](https://threatpost.com/android-devices-prone-to-botnets-ddos-onslaught/163680/){:target="_blank" rel="noopener"}

> A new DDoS botnet propagates via the Android Debug Bridge and uses Tor to hide its activity. [...]
