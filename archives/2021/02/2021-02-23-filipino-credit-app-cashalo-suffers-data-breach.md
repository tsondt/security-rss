Title: Filipino credit app Cashalo suffers data breach
Date: 2021-02-23T12:43:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: filipino-credit-app-cashalo-suffers-data-breach

[Source](https://portswigger.net/daily-swig/filipino-credit-app-cashalo-suffers-data-breach){:target="_blank" rel="noopener"}

> Unknown actor accessed database illegally, the lender confirmed [...]
