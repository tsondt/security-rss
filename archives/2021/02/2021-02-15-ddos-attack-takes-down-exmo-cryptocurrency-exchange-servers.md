Title: DDoS attack takes down EXMO cryptocurrency exchange servers
Date: 2021-02-15T15:08:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: ddos-attack-takes-down-exmo-cryptocurrency-exchange-servers

[Source](https://www.bleepingcomputer.com/news/security/ddos-attack-takes-down-exmo-cryptocurrency-exchange-servers/){:target="_blank" rel="noopener"}

> The servers of British cryptocurrency exchange EXMO were taken offline temporarily after being targeted in a distributed denial-of-service (DDoS) attack. [...]
