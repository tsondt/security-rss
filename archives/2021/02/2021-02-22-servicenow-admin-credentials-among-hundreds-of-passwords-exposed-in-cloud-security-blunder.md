Title: ServiceNow admin credentials among hundreds of passwords exposed in cloud security blunder
Date: 2021-02-22T13:21:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: servicenow-admin-credentials-among-hundreds-of-passwords-exposed-in-cloud-security-blunder

[Source](https://portswigger.net/daily-swig/servicenow-admin-credentials-among-hundreds-of-passwords-exposed-in-cloud-security-blunder){:target="_blank" rel="noopener"}

> Vulnerability that could lead to full environment compromise has now been patched [...]
