Title: Chinese Supply-Chain Attack on Computer Systems
Date: 2021-02-13T17:41:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;China;cybersecurity;FBI;FISA;intelligence;Internet of Things;national security policy;reports;supply chain
Slug: chinese-supply-chain-attack-on-computer-systems

[Source](https://www.schneier.com/blog/archives/2021/02/chinese-supply-chain-attack-on-computer-systems.html){:target="_blank" rel="noopener"}

> Bloomberg News has a major story about the Chinese hacking computer motherboards made by Supermicro, Levono, and others. It’s been going on since at least 2008. The US government has known about it for almost as long, and has tried to keep the attack secret: China’s exploitation of products made by Supermicro, as the U.S. company is known, has been under federal scrutiny for much of the past decade, according to 14 former law enforcement and intelligence officials familiar with the matter. That included an FBI counterintelligence investigation that began around 2012, when agents started monitoring the communications of a [...]
