Title: The time for Insider Risk Management is now: Code42 2021 Data Exposure Report Reveals a Perfect Storm
Date: 2021-02-10T14:00:28+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Web Security
Slug: the-time-for-insider-risk-management-is-now-code42-2021-data-exposure-report-reveals-a-perfect-storm

[Source](https://threatpost.com/the-time-for-insider-risk-management-is-now-code42-2021-data-exposure-report-reveals-a-perfect-storm/163754/){:target="_blank" rel="noopener"}

> The Code42 2021 Data Exposure Report highlights the need to adopt a new approach to data security and invest in modern Insider Risk technology. [...]
