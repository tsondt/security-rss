Title: Malicious extension abuses Chrome sync to steal users’ data
Date: 2021-02-05T15:14:46-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: malicious-extension-abuses-chrome-sync-to-steal-users-data

[Source](https://www.bleepingcomputer.com/news/security/malicious-extension-abuses-chrome-sync-to-steal-users-data/){:target="_blank" rel="noopener"}

> The Google Chrome Sync feature can be abused by threat actors to harvest information from compromised computers using maliciously-crafted Chrome browser extensions. [...]
