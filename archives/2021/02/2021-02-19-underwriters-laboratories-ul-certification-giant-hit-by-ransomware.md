Title: Underwriters Laboratories (UL) certification giant hit by ransomware
Date: 2021-02-19T19:06:53-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: underwriters-laboratories-ul-certification-giant-hit-by-ransomware

[Source](https://www.bleepingcomputer.com/news/security/underwriters-laboratories-ul-certification-giant-hit-by-ransomware/){:target="_blank" rel="noopener"}

> UL LLC, better known as Underwriters Laboratories, has suffered a ransomware attack that encrypted its servers and caused them to shut down systems while they recover. [...]
