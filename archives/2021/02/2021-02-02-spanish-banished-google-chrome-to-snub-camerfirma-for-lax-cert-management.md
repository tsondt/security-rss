Title: Spanish banished: Google Chrome to snub Camerfirma for lax cert management
Date: 2021-02-02T08:02:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: spanish-banished-google-chrome-to-snub-camerfirma-for-lax-cert-management

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/02/chrome_camerfirma_certificates/){:target="_blank" rel="noopener"}

> Mozilla meanwhile wants to continue compliance discussions with security certificate vendor When Google Chrome 90 arrives in April, visitors to websites that depend on TLS server authentication certificates from AC Camerfirma SA, a digital certificate authority based in Madrid, Spain, will find that those sites no longer present the secure lock icon.... [...]
