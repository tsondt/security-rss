Title: UK watchdog fines two firms £270k for cold-calling 531,000 people who had opted out
Date: 2021-02-15T15:32:12+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: uk-watchdog-fines-two-firms-ps270k-for-cold-calling-531000-people-who-had-opted-out

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/15/cold_call_ico_fines/){:target="_blank" rel="noopener"}

> Ah, the old 'liquidate your company' trick. Classic Another month and two more British companies behind nuisance marketing calls are collectively facing a £270,000 penalty for breaking the law by calling people registered by the Telephone Preference Service (TPS).... [...]
