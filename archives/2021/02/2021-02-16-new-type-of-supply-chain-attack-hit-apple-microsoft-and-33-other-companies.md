Title: New type of supply-chain attack hit Apple, Microsoft and 33 other companies
Date: 2021-02-16T12:49:59+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;bug bounties;malware;open source;repositories
Slug: new-type-of-supply-chain-attack-hit-apple-microsoft-and-33-other-companies

[Source](https://arstechnica.com/?p=1742499){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Last week, a researcher demonstrated a new supply-chain attack that executed counterfeit code on networks belonging to some of the biggest companies on the planet, Apple, Microsoft, and Tesla included. Now, fellow researchers are peppering the Internet with copycat packages, with more than 150 of them detected so far. The technique was unveiled last Tuesday by security researcher Alex Birsan. His so-called dependency confusion or namespace confusion attack starts by placing malicious code in an official public repository such as NPM, PyPI, or RubyGems. By giving the submissions the same package name as dependencies used by [...]
