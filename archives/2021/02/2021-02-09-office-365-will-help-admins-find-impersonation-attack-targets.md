Title: Office 365 will help admins find impersonation attack targets
Date: 2021-02-09T12:05:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: office-365-will-help-admins-find-impersonation-attack-targets

[Source](https://www.bleepingcomputer.com/news/security/office-365-will-help-admins-find-impersonation-attack-targets/){:target="_blank" rel="noopener"}

> Microsoft will make it easier for Defender for Office 365 customers to identify users and domains targeted in impersonation-based phishing attacks as recently revealed on the Microsoft 365 roadmap. [...]
