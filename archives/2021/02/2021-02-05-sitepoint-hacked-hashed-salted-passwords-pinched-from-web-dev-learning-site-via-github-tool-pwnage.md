Title: SitePoint hacked: Hashed, salted passwords pinched from web dev learning site via GitHub tool pwnage
Date: 2021-02-05T19:05:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: sitepoint-hacked-hashed-salted-passwords-pinched-from-web-dev-learning-site-via-github-tool-pwnage

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/05/sitepoint_hack_supply_chain/){:target="_blank" rel="noopener"}

> If you started off there, best change your reused credentials SitePoint, an Australian learn-to-code publishing website, has been compromised while promoting the book Hacking for Dummies on its homepage.... [...]
