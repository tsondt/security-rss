Title: Déjà vu-lnerability
Date: 2021-02-03T09:10:00.003000-08:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: deja-vu-lnerability

[Source](https://googleprojectzero.blogspot.com/2021/02/deja-vu-lnerability.html){:target="_blank" rel="noopener"}

> A Year in Review of 0-days Exploited In-The-Wild in 2020 Posted by Maddie Stone, Project Zero 2020 was a year full of 0-day exploits. Many of the Internet’s most popular browsers had their moment in the spotlight. Memory corruption is still the name of the game and how the vast majority of detected 0-days are getting in. While we tried new methods of 0-day detection with modest success, 2020 showed us that there is still a long way to go in detecting these 0-day exploits in-the-wild. But what may be the most notable fact is that 25% of the 0-days [...]
