Title: Fake Forcepoint Google Chrome Extension Hacks Windows Users
Date: 2021-02-08T17:24:31+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Web Security
Slug: fake-forcepoint-google-chrome-extension-hacks-windows-users

[Source](https://threatpost.com/fake-forcepoint-google-chrome-extension-hacks/163728/){:target="_blank" rel="noopener"}

> In a unique attack, cybercriminals locally install an extension to manipulate data in internal web applications that the victims have access to. [...]
