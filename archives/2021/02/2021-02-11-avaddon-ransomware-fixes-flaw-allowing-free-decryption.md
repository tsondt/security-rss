Title: Avaddon ransomware fixes flaw allowing free decryption
Date: 2021-02-11T18:30:22-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: avaddon-ransomware-fixes-flaw-allowing-free-decryption

[Source](https://www.bleepingcomputer.com/news/security/avaddon-ransomware-fixes-flaw-allowing-free-decryption/){:target="_blank" rel="noopener"}

> The Avaddon ransomware gang has fixed a bug that let victims recover their files without paying the ransom. The flaw came to light after a security researcher exploited it to create a decryptor. [...]
