Title: Beware: AOL phishing email states your account will be closed
Date: 2021-02-28T12:45:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: beware-aol-phishing-email-states-your-account-will-be-closed

[Source](https://www.bleepingcomputer.com/news/security/beware-aol-phishing-email-states-your-account-will-be-closed/){:target="_blank" rel="noopener"}

> An AOL mail phishing campaign is underway to steal users' login name and password by warning recipients that their account is about to be closed. [...]
