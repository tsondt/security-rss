Title: Linux Mint users in hot water for being slow with security updates, running old versions
Date: 2021-02-23T13:33:55+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: linux-mint-users-in-hot-water-for-being-slow-with-security-updates-running-old-versions

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/23/linux_mint_team_berates_users/){:target="_blank" rel="noopener"}

> Automatic updates? 'We have ideas on how to improve this,' says founder Linux Mint founder Clem Lefebvre has complained that too many users are slow to apply updates or run unsupported versions of the operating system.... [...]
