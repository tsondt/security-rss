Title: Plex Media servers actively abused to amplify DDoS attacks
Date: 2021-02-04T10:54:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: plex-media-servers-actively-abused-to-amplify-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/plex-media-servers-actively-abused-to-amplify-ddos-attacks/){:target="_blank" rel="noopener"}

> Plex Media Server systems are actively being abused by DDoS-for-hire services as a UDP reflection/amplification vector in Distributed Denial of Service (DDoS) attacks. [...]
