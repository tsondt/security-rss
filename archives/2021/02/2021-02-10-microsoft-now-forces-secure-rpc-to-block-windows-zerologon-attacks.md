Title: Microsoft now forces secure RPC to block Windows Zerologon attacks
Date: 2021-02-10T12:56:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-now-forces-secure-rpc-to-block-windows-zerologon-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-now-forces-secure-rpc-to-block-windows-zerologon-attacks/){:target="_blank" rel="noopener"}

> Microsoft has enabled enforcement mode for updates addressing the Windows Zerologon vulnerability on all devices that installed this month's Patch Tuesday security updates. [...]
