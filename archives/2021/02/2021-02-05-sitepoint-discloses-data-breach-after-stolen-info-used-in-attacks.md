Title: SitePoint discloses data breach after stolen info used in attacks
Date: 2021-02-05T12:34:09-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: sitepoint-discloses-data-breach-after-stolen-info-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/sitepoint-discloses-data-breach-after-stolen-info-used-in-attacks/){:target="_blank" rel="noopener"}

> The SitePoint web professional community has disclosed a data breach after their user database was sold and eventually leaked for free on a hacker forum. [...]
