Title: Ransomware gang hacks Ecuador's largest private bank, Ministry of Finance
Date: 2021-02-26T14:25:56-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-hacks-ecuadors-largest-private-bank-ministry-of-finance

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-hacks-ecuadors-largest-private-bank-ministry-of-finance/){:target="_blank" rel="noopener"}

> ​A hacking group called 'Hotarus Corp' has hacked Ecuador's Ministry of Finance and the country's largest bank, Banco Pichincha, where they claim to have stolen internal data. [...]
