Title: Soviet 'Enigma' cipher machine sells for $22k at collapsed museum's exhibits auction
Date: 2021-02-17T12:44:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: soviet-enigma-cipher-machine-sells-for-22k-at-collapsed-museums-exhibits-auction

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/17/soviet_spy_gadgets_museum_auction/){:target="_blank" rel="noopener"}

> James Bond? Inspector Gadget? Yup, all here A Soviet equivalent of Nazi Germany's Enigma cipher machine has sold for more than double its auction asking price – while a secret camera disguised as a pack of cigarettes went for nearly $20,000.... [...]
