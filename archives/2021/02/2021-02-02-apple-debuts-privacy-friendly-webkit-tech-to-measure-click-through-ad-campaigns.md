Title: Apple debuts privacy-friendly WebKit tech to measure click-through ad campaigns
Date: 2021-02-02T17:06:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: apple-debuts-privacy-friendly-webkit-tech-to-measure-click-through-ad-campaigns

[Source](https://portswigger.net/daily-swig/apple-debuts-privacy-friendly-webkit-tech-to-measure-click-through-ad-campaigns){:target="_blank" rel="noopener"}

> Cookie cutter [...]
