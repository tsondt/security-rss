Title: Fall 2020 PCI DSS report now available with eight additional services in scope
Date: 2021-02-25T17:15:54+00:00
Author: Michael Oyeniya
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;Compliance reports;PCI;PCI DSS;Security Blog
Slug: fall-2020-pci-dss-report-now-available-with-eight-additional-services-in-scope

[Source](https://aws.amazon.com/blogs/security/fall-2020-pci-dss-report-now-available-with-eight-additional-services-in-scope/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs and are pleased to announce that eight additional services have been added to the scope of our Payment Card Industry Data Security Standard (PCI DSS) certification. This gives our customers more options to process and store their payment card data and architect their cardholder data [...]
