Title: Tax Season Ushers in Quickbooks Data-Theft Spike
Date: 2021-02-24T21:52:29+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Malware;Vulnerabilities;Web Security
Slug: tax-season-ushers-in-quickbooks-data-theft-spike

[Source](https://threatpost.com/tax-quickbooks-data-theft/164253/){:target="_blank" rel="noopener"}

> Quickbooks malware targets tax data for attackers to sell and use in phishing scams. [...]
