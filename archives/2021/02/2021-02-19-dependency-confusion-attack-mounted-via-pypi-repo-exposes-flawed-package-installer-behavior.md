Title: Dependency confusion attack mounted via PyPi repo exposes flawed package installer behavior
Date: 2021-02-19T16:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: dependency-confusion-attack-mounted-via-pypi-repo-exposes-flawed-package-installer-behavior

[Source](https://portswigger.net/daily-swig/dependency-confusion-attack-mounted-via-pypi-repo-exposes-flawed-package-installer-behavior){:target="_blank" rel="noopener"}

> Novel supply chain attack detected in the wild just days after security researcher disclosed the technique [...]
