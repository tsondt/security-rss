Title: Network hackers asked for over $1 million in initial access offers
Date: 2021-02-11T04:21:08-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: network-hackers-asked-for-over-1-million-in-initial-access-offers

[Source](https://www.bleepingcomputer.com/news/security/network-hackers-asked-for-over-1-million-in-initial-access-offers/){:target="_blank" rel="noopener"}

> The number of offers for network access and their median prices on the public face of hacker forums dropped in the final quarter of last year but the statistics fail to reflect the real size of the initial access market. [...]
