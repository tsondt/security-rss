Title: Over 8 million COVID-19 test results leaked online
Date: 2021-02-24T21:00:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Government
Slug: over-8-million-covid-19-test-results-leaked-online

[Source](https://www.bleepingcomputer.com/news/security/over-8-million-covid-19-test-results-leaked-online/){:target="_blank" rel="noopener"}

> Millions of COVID-19 test reports were found to be publicly accessible due to flawed online system implementation. [...]
