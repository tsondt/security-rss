Title: Microsoft Lures Populate Half of Credential-Swiping Phishing Emails
Date: 2021-02-24T15:00:37+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security
Slug: microsoft-lures-populate-half-of-credential-swiping-phishing-emails

[Source](https://threatpost.com/microsoft-lures-credential-swiping-phishing-emails/164207/){:target="_blank" rel="noopener"}

> As more organizations migrate to Office 365, cybercriminals are using Outlook, Teams and other Microsoft-themed phishing lures to swipe user credentials. [...]
