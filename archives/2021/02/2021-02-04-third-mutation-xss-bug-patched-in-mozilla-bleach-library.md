Title: Third mutation XSS bug patched in Mozilla Bleach library
Date: 2021-02-04T12:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: third-mutation-xss-bug-patched-in-mozilla-bleach-library

[Source](https://portswigger.net/daily-swig/third-mutation-xss-bug-patched-in-mozilla-bleach-library){:target="_blank" rel="noopener"}

> Coordinated disclosure helps protect more than 100,000 dependencies [...]
