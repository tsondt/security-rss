Title: Telegram 'Secret Chat' didn't delete self-destructing media files
Date: 2021-02-12T14:57:56-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: telegram-secret-chat-didnt-delete-self-destructing-media-files

[Source](https://www.bleepingcomputer.com/news/security/telegram-secret-chat-didnt-delete-self-destructing-media-files/){:target="_blank" rel="noopener"}

> Telegram has fixed a security issue where self-destructing audio and video recording were not being deleted from user's macOS devices as expected. [...]
