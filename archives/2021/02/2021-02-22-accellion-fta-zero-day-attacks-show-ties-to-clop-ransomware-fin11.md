Title: Accellion FTA Zero-Day Attacks Show Ties to Clop Ransomware, FIN11
Date: 2021-02-22T17:51:20+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Vulnerabilities;Web Security
Slug: accellion-fta-zero-day-attacks-show-ties-to-clop-ransomware-fin11

[Source](https://threatpost.com/accellion-zero-day-attacks-clop-ransomware-fin11/164150/){:target="_blank" rel="noopener"}

> The threat actors stole data and used Clop's leaks site to demand money in an extortion scheme, though no ransomware was deployed. [...]
