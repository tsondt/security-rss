Title: Health Website Leaks 8 Million COVID-19 Test Results
Date: 2021-02-25T17:34:30+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Mobile Security;Web Security
Slug: health-website-leaks-8-million-covid-19-test-results

[Source](https://threatpost.com/health-website-leaks-covid-19-test/164274/){:target="_blank" rel="noopener"}

> A teenaged ethical hacker discovered a flawed endpoint associated with a health-department website in the state of Bengal, which exposed personally identifiable information related to test results. [...]
