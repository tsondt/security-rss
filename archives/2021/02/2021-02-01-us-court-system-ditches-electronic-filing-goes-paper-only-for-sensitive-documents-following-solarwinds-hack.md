Title: US court system ditches electronic filing, goes paper-only for sensitive documents following SolarWinds hack
Date: 2021-02-01T21:25:53+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: us-court-system-ditches-electronic-filing-goes-paper-only-for-sensitive-documents-following-solarwinds-hack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/01/us_court_papers/){:target="_blank" rel="noopener"}

> Lawyers required to hand in dead-tree copies. No, seriously The US court system has banned the electronic submission of legal documents in sensitive cases out of concern that Russian hackers have compromised the filing system.... [...]
