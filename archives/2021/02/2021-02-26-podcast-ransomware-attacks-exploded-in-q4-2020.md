Title: Podcast: Ransomware Attacks Exploded in Q4 2020
Date: 2021-02-26T13:36:48+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Podcasts
Slug: podcast-ransomware-attacks-exploded-in-q4-2020

[Source](https://threatpost.com/podcast-ransomware-attacks-exploded-in-q4-2020/164285/){:target="_blank" rel="noopener"}

> Researchers said they saw a seven-times increase in ransomware activity in the fourth quarter of 2020, across various families – from Ryuk to Egregor. [...]
