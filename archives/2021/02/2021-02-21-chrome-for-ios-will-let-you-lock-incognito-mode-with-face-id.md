Title: Chrome for iOS will let you lock Incognito mode with Face ID
Date: 2021-02-21T08:31:05-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google
Slug: chrome-for-ios-will-let-you-lock-incognito-mode-with-face-id

[Source](https://www.bleepingcomputer.com/news/google/chrome-for-ios-will-let-you-lock-incognito-mode-with-face-id/){:target="_blank" rel="noopener"}

> Google Chrome for iOS is getting a new privacy feature that lets you lock your opened Incognito tabs behind your iPhone's Face ID or Touch ID biometric authentication features. [...]
