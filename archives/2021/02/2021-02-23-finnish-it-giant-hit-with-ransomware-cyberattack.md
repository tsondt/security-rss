Title: Finnish IT Giant Hit with Ransomware Cyberattack
Date: 2021-02-23T16:51:24+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: finnish-it-giant-hit-with-ransomware-cyberattack

[Source](https://threatpost.com/finnish-it-giant-ransomware-cyberattack/164193/){:target="_blank" rel="noopener"}

> A major Finnish IT provider has been hit with a ransomware attack that has forced the company to turn off some services and infrastructure in a disruption to customers, while it takes recovery measures. Norwegian business journal E24 reported the attack on Espoo, Finland-based TietoEVRY on Tuesday, claiming to have spoken with Geir Remman, a [...]
