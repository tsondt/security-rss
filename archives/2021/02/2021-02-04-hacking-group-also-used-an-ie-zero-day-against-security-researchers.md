Title: Hacking group also used an IE zero-day against security researchers
Date: 2021-02-04T12:07:46-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hacking-group-also-used-an-ie-zero-day-against-security-researchers

[Source](https://www.bleepingcomputer.com/news/security/hacking-group-also-used-an-ie-zero-day-against-security-researchers/){:target="_blank" rel="noopener"}

> An Internet Explorer zero-day vulnerability has been discovered used in recent North Korean attacks against security and vulnerability researchers. [...]
