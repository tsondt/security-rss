Title: Microsoft warns of an increasing number of web shell attacks
Date: 2021-02-11T13:11:25-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-warns-of-an-increasing-number-of-web-shell-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-an-increasing-number-of-web-shell-attacks/){:target="_blank" rel="noopener"}

> Microsoft says that the number of monthly web shell attacks has almost doubled since last year, with an average of 140,000 such malicious tools being found on compromised servers every month. [...]
