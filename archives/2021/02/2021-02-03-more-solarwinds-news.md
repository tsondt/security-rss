Title: More SolarWinds News
Date: 2021-02-03T12:10:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: attribution;forensics;hacking;malware;Russia;tamper detection
Slug: more-solarwinds-news

[Source](https://www.schneier.com/blog/archives/2021/02/more-solarwinds-news.html){:target="_blank" rel="noopener"}

> Microsoft analyzed details of the SolarWinds attack: Microsoft and FireEye only detected the Sunburst or Solorigate malware in December, but Crowdstrike reported this month that another related piece of malware, Sunspot, was deployed in September 2019, at the time hackers breached SolarWinds’ internal network. Other related malware includes Teardrop aka Raindrop. Details are in the Microsoft blog: We have published our in-depth analysis of the Solorigate backdoor malware (also referred to as SUNBURST by FireEye), the compromised DLL that was deployed on networks as part of SolarWinds products, that allowed attackers to gain backdoor access to affected devices. We have [...]
