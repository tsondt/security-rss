Title: Hacker Tries to Poison Water Supply of Florida Town
Date: 2021-02-09T12:54:39+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Critical Infrastructure
Slug: hacker-tries-to-poison-water-supply-of-florida-town

[Source](https://threatpost.com/hacker-tries-to-poison-water-supply-of-florida-town/163761/){:target="_blank" rel="noopener"}

> A threat actor remotely accessed the IT system of the water treatment facility of Oldsmar and raised the levels of sodium hydroxide in the water, an action that was quickly noticed and remediated. [...]
