Title: Tokyo Gas discloses data breach impacting anime-style dating simulation game
Date: 2021-02-08T14:39:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: tokyo-gas-discloses-data-breach-impacting-anime-style-dating-simulation-game

[Source](https://portswigger.net/daily-swig/tokyo-gas-discloses-data-breach-impacting-anime-style-dating-simulation-game){:target="_blank" rel="noopener"}

> Developed by Japan’s largest gas utility, ‘Furo Koi’ was created to offer bathing advice to users [...]
