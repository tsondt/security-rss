Title: SHAREit app for Android said to share way too much: Billion-download code with holes no one wants to fix
Date: 2021-02-16T20:25:44+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: shareit-app-for-android-said-to-share-way-too-much-billion-download-code-with-holes-no-one-wants-to-fix

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/16/shareit_app_flaws/){:target="_blank" rel="noopener"}

> Trend Micro claims software is full of security flaws that allow data out and malware in Trend Micro has published a report claiming that data-sharing Android app SHAREit, which has over a billion downloads, contains multiple vulnerabilities after the app's maker ignored advice to fix the flaws.... [...]
