Title: Use new account assignment APIs for AWS SSO to automate multi-account access
Date: 2021-02-08T20:44:58+00:00
Author: Akhil Aendapally
Category: AWS Security
Tags: AWS CloudFormation;AWS Command Line Interface;AWS Single Sign-On (SSO);Intermediate (200);Security, Identity, & Compliance;AWS IAM;AWS Organizations;AWS SSO;Permission sets;Security Blog
Slug: use-new-account-assignment-apis-for-aws-sso-to-automate-multi-account-access

[Source](https://aws.amazon.com/blogs/security/use-new-account-assignment-apis-for-aws-sso-to-automate-multi-account-access/){:target="_blank" rel="noopener"}

> In this blog post, we’ll show how you can programmatically assign and audit access to multiple AWS accounts for your AWS Single Sign-On (SSO) users and groups, using the AWS Command Line Interface (AWS CLI) and AWS CloudFormation. With AWS SSO, you can centrally manage access and user permissions to all of your accounts in [...]
