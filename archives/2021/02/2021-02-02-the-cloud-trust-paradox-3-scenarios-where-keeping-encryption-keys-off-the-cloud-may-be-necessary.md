Title: The cloud trust paradox: 3 scenarios where keeping encryption keys off the cloud may be necessary
Date: 2021-02-02T17:30:00+00:00
Author: Il-Sung Lee
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: the-cloud-trust-paradox-3-scenarios-where-keeping-encryption-keys-off-the-cloud-may-be-necessary

[Source](https://cloud.google.com/blog/products/identity-security/3-scenarios-where-keeping-encryption-keys-off-the-cloud-may-be-necessary/){:target="_blank" rel="noopener"}

> As we discussed in “The Cloud trust paradox: To trust cloud computing more, you need the ability to trust it less” and hinted at in “ Unlocking the mystery of stronger security key management,” there are situations where the encryption keys must be kept away from the cloud provider environment. While we argue that these are rare, they absolutely do exist. Moreover, when these situations materialize, the data in question or the problem being solved is typically hugely important. Here are three patterns where keeping the keys off the cloud may in fact be truly necessary or outweighs the benefits [...]
