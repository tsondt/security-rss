Title: Australian research institute confirms ‘likely’ data breach after third-party Accellion hack
Date: 2021-02-11T13:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: australian-research-institute-confirms-likely-data-breach-after-third-party-accellion-hack

[Source](https://portswigger.net/daily-swig/australian-research-institute-confirms-likely-data-breach-after-third-party-accellion-hack){:target="_blank" rel="noopener"}

> Clinical trial files accessed after ‘secure sharing service’ suffers cyber intrusion [...]
