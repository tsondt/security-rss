Title: Over 40 services require TLS 1.2 minimum for AWS FIPS endpoints
Date: 2021-02-02T17:47:30+00:00
Author: Janelle Hopper
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS service FIPS endpoints;Federal Information Processing Standard;FedRAMP;FIPS;FIPS 140-2;FIPS endpoints;NIST;Security Blog;TLS;Transport Layer Security
Slug: over-40-services-require-tls-12-minimum-for-aws-fips-endpoints

[Source](https://aws.amazon.com/blogs/security/over-40-services-require-tls-1-2-minimum-for-aws-fips-endpoints/){:target="_blank" rel="noopener"}

> In a March 2020 blog post, we told you about work Amazon Web Services (AWS) was undertaking to update all of our AWS Federal Information Processing Standard (FIPS) endpoints to a minimum of Transport Layer Security (TLS) 1.2 across all AWS Regions. Today, we’re happy to announce that over 40 services have been updated and [...]
