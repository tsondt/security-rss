Title: VMWare Patches Critical RCE Flaw in vCenter Server
Date: 2021-02-24T17:14:55+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: vmware-patches-critical-rce-flaw-in-vcenter-server

[Source](https://threatpost.com/vmware-patches-critical-rce-flaw-in-vcenter-server/164240/){:target="_blank" rel="noopener"}

> The vulnerability, one of three patched by the company this week, could allow threat actors to breach the external perimeter of a data center or leverage backdoors already installed to take over a system. [...]
