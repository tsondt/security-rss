Title: Microsoft fixes Windows 10 drive corruption bug — what you need to know
Date: 2021-02-27T10:34:11-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-fixes-windows-10-drive-corruption-bug-what-you-need-to-know

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-windows-10-drive-corruption-bug-what-you-need-to-know/){:target="_blank" rel="noopener"}

> Microsoft has fixed a Windows 10 bug that could cause NTFS volumes to become corrupted by merely accessing a particular path or viewing a specially crafted file. [...]
