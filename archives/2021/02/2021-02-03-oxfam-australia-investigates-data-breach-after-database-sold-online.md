Title: Oxfam Australia investigates data breach after database sold online
Date: 2021-02-03T22:30:08-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: oxfam-australia-investigates-data-breach-after-database-sold-online

[Source](https://www.bleepingcomputer.com/news/security/oxfam-australia-investigates-data-breach-after-database-sold-online/){:target="_blank" rel="noopener"}

> Oxfam Australia investigates a suspected data breach after a threat actor claimed to be selling their database belonging on a hacker forum. [...]
