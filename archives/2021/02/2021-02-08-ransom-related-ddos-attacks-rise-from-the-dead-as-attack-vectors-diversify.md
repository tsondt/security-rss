Title: Ransom-related DDoS attacks rise from the dead as attack vectors diversify
Date: 2021-02-08T17:58:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ransom-related-ddos-attacks-rise-from-the-dead-as-attack-vectors-diversify

[Source](https://portswigger.net/daily-swig/ransom-related-ddos-attacks-rise-from-the-dead-as-attack-vectors-diversify){:target="_blank" rel="noopener"}

> DDoS extortion is back... [...]
