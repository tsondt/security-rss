Title: H2C smuggling named top web hacking technique of 2020
Date: 2021-02-24T17:10:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: h2c-smuggling-named-top-web-hacking-technique-of-2020

[Source](https://portswigger.net/daily-swig/h2c-smuggling-named-top-web-hacking-technique-of-2020){:target="_blank" rel="noopener"}

> ‘We anticipate some serious carnage in future’ [...]
