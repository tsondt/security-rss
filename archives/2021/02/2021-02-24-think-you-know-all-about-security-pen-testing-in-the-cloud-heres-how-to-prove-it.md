Title: Think you know all about security pen-testing in the cloud? Here’s how to prove it
Date: 2021-02-24T08:00:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: think-you-know-all-about-security-pen-testing-in-the-cloud-heres-how-to-prove-it

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/24/sans_institute_pen_testing_certification/){:target="_blank" rel="noopener"}

> New GIAC qual shows you can put the Sec into DevSecOps and quantify the risk in SRE Promo On the face of it, cloud penetration testing might appear a complex undertaking involving very different architectures, such as containers and Kubernetes, to those found in traditional on-prem infrastructure.... [...]
