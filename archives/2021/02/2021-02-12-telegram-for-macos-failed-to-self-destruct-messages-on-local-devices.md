Title: Telegram for macOS failed to self-destruct messages on local devices
Date: 2021-02-12T15:50:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: telegram-for-macos-failed-to-self-destruct-messages-on-local-devices

[Source](https://portswigger.net/daily-swig/telegram-for-macos-failed-to-self-destruct-messages-on-local-devices){:target="_blank" rel="noopener"}

> A vulnerability in the messaging app meant that some conversations were never actually deleted [...]
