Title: NurseryCam hacked, company shuts down IoT camera service
Date: 2021-02-22T17:30:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: nurserycam-hacked-company-shuts-down-iot-camera-service

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/22/nurserycam_breach/){:target="_blank" rel="noopener"}

> Real names, usernames, and what appears to be SHA-1 hashed passwords exposed Daycare camera product NurseryCam was hacked late last week with the person behind the digital break-in coming forward to tip us off.... [...]
