Title: Leading Canadian rental car company hit by DarkSide ransomware
Date: 2021-02-13T13:08:58-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: leading-canadian-rental-car-company-hit-by-darkside-ransomware

[Source](https://www.bleepingcomputer.com/news/security/leading-canadian-rental-car-company-hit-by-darkside-ransomware/){:target="_blank" rel="noopener"}

> Canadian Discount Car and Truck Rentals has been hit with a DarkSide ransomware attack where the hackers claim to have stolen 120GB of data. [...]
