Title: Microsoft says it found 1,000-plus developers' fingerprints on the SolarWinds attack
Date: 2021-02-15T05:57:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: microsoft-says-it-found-1000-plus-developers-fingerprints-on-the-solarwinds-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/15/solarwinds_microsoft_fireeye_analysis/){:target="_blank" rel="noopener"}

> As FireEye reveals how suspicious second phone signed up for 2FA gave the game away Microsoft president Brad Smith said the software giant’s analysis of the SolarWinds hack suggests the code behind the crack was the work of a thousand or more developers.... [...]
