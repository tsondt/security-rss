Title: ‘Annoyingly Believable’ Tax Scam Targets Mobile Users
Date: 2021-02-12T19:03:01+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Web Security
Slug: annoyingly-believable-tax-scam-targets-mobile-users

[Source](https://threatpost.com/believable-tax-scam-mobile-users/163951/){:target="_blank" rel="noopener"}

> A well-crafted SMS phishing effort is harvesting personal data and credit-card details under the guise of offering tax refunds. [...]
