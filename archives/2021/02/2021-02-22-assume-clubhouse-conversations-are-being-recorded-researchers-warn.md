Title: Assume Clubhouse Conversations Are Being Recorded, Researchers Warn
Date: 2021-02-22T19:40:50+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Mobile Security;Privacy
Slug: assume-clubhouse-conversations-are-being-recorded-researchers-warn

[Source](https://threatpost.com/clubhouse-conversations-recorded/164158/){:target="_blank" rel="noopener"}

> Two breaches of the audio-based social media app reinforce privacy, security concerns. [...]
