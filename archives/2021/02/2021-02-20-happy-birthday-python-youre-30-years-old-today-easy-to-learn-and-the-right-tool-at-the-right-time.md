Title: Happy birthday, Python, you're 30 years old today: Easy to learn, and the right tool at the right time
Date: 2021-02-20T13:10:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: happy-birthday-python-youre-30-years-old-today-easy-to-learn-and-the-right-tool-at-the-right-time

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/20/happy_birthday_python_youre_30/){:target="_blank" rel="noopener"}

> Popular programming language, at the top of its game, still struggles to please everyone Feature February 20, 2021, the 30th anniversary of Python, finds the programming language at the top of its game but not without challenges.... [...]
