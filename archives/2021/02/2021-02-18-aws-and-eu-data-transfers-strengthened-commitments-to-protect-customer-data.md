Title: AWS and EU data transfers: strengthened commitments to protect customer data
Date: 2021-02-18T01:54:03+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Data transfers;EU;GDPR;Security Blog
Slug: aws-and-eu-data-transfers-strengthened-commitments-to-protect-customer-data

[Source](https://aws.amazon.com/blogs/security/aws-and-eu-data-transfers-strengthened-commitments-to-protect-customer-data/){:target="_blank" rel="noopener"}

> Last year we published a blog post describing how our customers can transfer personal data in compliance with both GDPR and the new “Schrems II” ruling. In that post, we set out some of the robust and comprehensive measures that AWS takes to protect customers’ personal data. Today, we are announcing strengthened contractual commitments that [...]
