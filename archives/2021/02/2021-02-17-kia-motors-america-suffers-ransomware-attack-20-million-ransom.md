Title: Kia Motors America suffers ransomware attack, $20 million ransom
Date: 2021-02-17T13:30:58-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: kia-motors-america-suffers-ransomware-attack-20-million-ransom

[Source](https://www.bleepingcomputer.com/news/security/kia-motors-america-suffers-ransomware-attack-20-million-ransom/){:target="_blank" rel="noopener"}

> Kia Motors America has suffered a ransomware attack by the DoppelPaymer gang, demanding $20 million for a decryptor and not to leak stolen data. [...]
