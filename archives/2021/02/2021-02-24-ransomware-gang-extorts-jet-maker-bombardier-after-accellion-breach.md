Title: Ransomware gang extorts jet maker Bombardier after Accellion breach
Date: 2021-02-24T09:01:09-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-extorts-jet-maker-bombardier-after-accellion-breach

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-extorts-jet-maker-bombardier-after-accellion-breach/){:target="_blank" rel="noopener"}

> Business jet maker Bombardier is the latest company to suffer a data breach by the Clop ransomware gang after attackers exploited a zero-day vulnerability to steal company data. [...]
