Title: Scammers posing as FBI agents threaten targets with jail time
Date: 2021-02-01T10:24:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: scammers-posing-as-fbi-agents-threaten-targets-with-jail-time

[Source](https://www.bleepingcomputer.com/news/security/scammers-posing-as-fbi-agents-threaten-targets-with-jail-time/){:target="_blank" rel="noopener"}

> The U.S. Federal Bureau of Investigation (FBI) is warning of scammers actively posing as FBI representatives and threatening targets with fines and jail time unless they don't hand out personal and/or financial information. [...]
