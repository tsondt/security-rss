Title: Friday Squid Blogging: Flying Squid
Date: 2021-02-12T22:03:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-flying-squid

[Source](https://www.schneier.com/blog/archives/2021/02/friday-squid-blogging-flying-squid.html){:target="_blank" rel="noopener"}

> How squid fly. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
