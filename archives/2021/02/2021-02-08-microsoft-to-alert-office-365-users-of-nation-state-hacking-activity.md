Title: Microsoft to alert Office 365 users of nation-state hacking activity
Date: 2021-02-08T08:52:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-to-alert-office-365-users-of-nation-state-hacking-activity

[Source](https://www.bleepingcomputer.com/news/security/microsoft-to-alert-office-365-users-of-nation-state-hacking-activity/){:target="_blank" rel="noopener"}

> Microsoft will soon notify Office 365 of suspected nation-state hacking activity detected within their tenants according to a new listing on the company's Microsoft 365 roadmap. [...]
