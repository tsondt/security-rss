Title: Secure communication: Beleaguered Hong Kong dissidents seek refuge on ‘digital underground’
Date: 2021-02-11T16:28:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: secure-communication-beleaguered-hong-kong-dissidents-seek-refuge-on-digital-underground

[Source](https://portswigger.net/daily-swig/secure-communication-beleaguered-hong-kong-dissidents-seek-refuge-on-digital-underground){:target="_blank" rel="noopener"}

> Assailed by law enforcement and APT groups, activists turn to dark web and encrypted messaging apps [...]
