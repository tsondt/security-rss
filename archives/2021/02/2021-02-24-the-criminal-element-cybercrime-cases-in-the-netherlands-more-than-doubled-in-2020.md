Title: The criminal element: Cybercrime cases in the Netherlands more than doubled in 2020
Date: 2021-02-24T14:55:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: the-criminal-element-cybercrime-cases-in-the-netherlands-more-than-doubled-in-2020

[Source](https://portswigger.net/daily-swig/the-criminal-element-cybercrime-cases-in-the-netherlands-more-than-doubled-in-2020){:target="_blank" rel="noopener"}

> Threat actors are becoming increasingly sophisticated, warns one of the country’s top legal experts [...]
