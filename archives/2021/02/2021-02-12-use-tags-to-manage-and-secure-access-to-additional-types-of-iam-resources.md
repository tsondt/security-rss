Title: Use tags to manage and secure access to additional types of IAM resources
Date: 2021-02-12T17:58:43+00:00
Author: Michael Switzer
Category: AWS Security
Tags: Advanced (300);AWS Identity and Access Management (IAM);Security, Identity, & Compliance;ABAC;Attribute-based access control;AWS IAM;AWS Identity and Access Management;Security Blog;Tagging
Slug: use-tags-to-manage-and-secure-access-to-additional-types-of-iam-resources

[Source](https://aws.amazon.com/blogs/security/use-tags-to-manage-and-secure-access-to-additional-types-of-iam-resources/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) now enables Amazon Web Services (AWS) administrators to use tags to manage and secure access to more types of IAM resources, such as customer managed IAM policies, Security Assertion Markup Language (SAML) providers, and virtual multi-factor authentication (MFA) devices. A tag is an attribute that consists of a key [...]
