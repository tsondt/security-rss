Title: US Coast Guard orders maritime facilities to report SolarWinds breaches
Date: 2021-02-10T15:47:49-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-coast-guard-orders-maritime-facilities-to-report-solarwinds-breaches

[Source](https://www.bleepingcomputer.com/news/security/us-coast-guard-orders-maritime-facilities-to-report-solarwinds-breaches/){:target="_blank" rel="noopener"}

> The U.S. Coast Guard (USCG) has ordered MTSA-regulated facilities and vessels using SolarWinds software for critical functions to report security breaches in case of suspicions of being affected by the SolarWinds supply-chain attack. [...]
