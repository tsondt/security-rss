Title: Microsoft is seeing a big spike in Web shell use
Date: 2021-02-12T13:19:07+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;compromise;hacking;web shell;websites
Slug: microsoft-is-seeing-a-big-spike-in-web-shell-use

[Source](https://arstechnica.com/?p=1741731){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Security personnel at Microsoft are seeing a big increase in the use of Web shells, the light-weight programs that hackers install so they can burrow further into compromised websites. The average number of Web shells installed from August, 2020 to January of this year was 144,000, almost twice that for the same months in 2019 and 2020. The spike represents an acceleration in growth that the same Microsoft researchers saw throughout last year. (credit: Microsoft) A Swiss Army knife for hackers The growth is a sign of just how useful and hard to detect these simple [...]
