Title: The Linux box that runs the exec carpark gate is down! A chance for PostgreSQL Man to show his quality
Date: 2021-02-05T07:55:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: the-linux-box-that-runs-the-exec-carpark-gate-is-down-a-chance-for-postgresql-man-to-show-his-quality

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/05/on_call/){:target="_blank" rel="noopener"}

> They still laid him off, though On Call This week's episode of On Call, as ever, comes with a warning: Be careful moving that beige box, for you may not realise what it does.... [...]
