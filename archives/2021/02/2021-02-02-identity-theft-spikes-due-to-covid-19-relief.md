Title: Identity Theft Spikes Due to COVID-19 Relief
Date: 2021-02-02T14:00:11+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks;Privacy;Web Security
Slug: identity-theft-spikes-due-to-covid-19-relief

[Source](https://threatpost.com/identity-theft-spikes-covid-19-relief/163577/){:target="_blank" rel="noopener"}

> Cases reported to the FTC doubled last year as cybercriminals took advantage of increased filing for government relief benefits due to the pandemic. [...]
