Title: The Great Suspender Chrome extension's fall from grace
Date: 2021-02-06T11:49:41-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: the-great-suspender-chrome-extensions-fall-from-grace

[Source](https://www.bleepingcomputer.com/news/software/the-great-suspender-chrome-extensions-fall-from-grace/){:target="_blank" rel="noopener"}

> Google has forcibly uninstalled the immensely popular 'The Great Suspender' extension from Google Chrome and classified it as malware. [...]
