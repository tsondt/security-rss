Title: Intel Squashes High-Severity Graphics Driver Flaws
Date: 2021-02-10T15:16:15+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: intel-squashes-high-severity-graphics-driver-flaws

[Source](https://threatpost.com/intel-graphics-driver-flaws/163810/){:target="_blank" rel="noopener"}

> Intel is warning on security bugs across its graphics drivers, server boards, compute modules and modems. [...]
