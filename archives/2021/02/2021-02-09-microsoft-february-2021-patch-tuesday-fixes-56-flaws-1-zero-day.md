Title: Microsoft February 2021 Patch Tuesday fixes 56 flaws, 1 zero-day
Date: 2021-02-09T13:25:48-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: microsoft-february-2021-patch-tuesday-fixes-56-flaws-1-zero-day

[Source](https://www.bleepingcomputer.com/news/security/microsoft-february-2021-patch-tuesday-fixes-56-flaws-1-zero-day/){:target="_blank" rel="noopener"}

> Today is Microsoft's February 2021 Patch Tuesday, so please be buy your Windows administrators some snacks to keep their energy up throughout the day. [...]
