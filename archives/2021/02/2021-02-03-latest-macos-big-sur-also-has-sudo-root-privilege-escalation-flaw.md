Title: Latest macOS Big Sur also has SUDO root privilege escalation flaw
Date: 2021-02-03T06:00:39-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Apple;Linux
Slug: latest-macos-big-sur-also-has-sudo-root-privilege-escalation-flaw

[Source](https://www.bleepingcomputer.com/news/security/latest-macos-big-sur-also-has-sudo-root-privilege-escalation-flaw/){:target="_blank" rel="noopener"}

> Recently discovered Linux SUDO privilege escalation vulnerability, CVE-2021-3156 (aka Baron Samedit) also impacts the latest Apple macOS Big Sur with no patch available yet. [...]
