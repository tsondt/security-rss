Title: Hacked by SolarWinds backdoor masterminds, Mimecast now lays off staff after profit surge
Date: 2021-02-07T10:04:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: hacked-by-solarwinds-backdoor-masterminds-mimecast-now-lays-off-staff-after-profit-surge

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/07/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: British Mensa in data leak blunder, DARPA are Star Wars fans, Sonicwall patch out, and more In brief Email security biz Mimecast not only fell victim to the SolarWinds hackers, leading to its own customers being attacked, it is also trimming its workforce amid healthy profits.... [...]
