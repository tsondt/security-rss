Title: Security researchers warn of critical zero-day flaws in ‘age gap’ dating app Gaper
Date: 2021-02-18T13:08:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-researchers-warn-of-critical-zero-day-flaws-in-age-gap-dating-app-gaper

[Source](https://portswigger.net/daily-swig/security-researchers-warn-of-critical-zero-day-flaws-in-age-gap-dating-app-gaper){:target="_blank" rel="noopener"}

> ‘We identified that it was possible to compromise any account on the application within a 10-minute timeframe’ [...]
