Title: Rising healthcare breaches driven by hacking and unsecured servers
Date: 2021-02-17T10:51:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: rising-healthcare-breaches-driven-by-hacking-and-unsecured-servers

[Source](https://www.bleepingcomputer.com/news/security/rising-healthcare-breaches-driven-by-hacking-and-unsecured-servers/){:target="_blank" rel="noopener"}

> 2020 was a bad year for healthcare organizations in the U.S., which had to deal with a record-high number of cybersecurity incidents on the backdrop of the COVID-19 pandemic. [...]
