Title: Beyond Corp Enterprise: True zero trust architecture for the multicloud
Date: 2021-02-03T17:00:00+00:00
Author: Biodun Awojobi
Category: GCP Security
Tags: Google Cloud Platform;Chrome Enterprise;Identity & Security
Slug: beyond-corp-enterprise-true-zero-trust-architecture-for-the-multicloud

[Source](https://cloud.google.com/blog/products/identity-security/how-beyondcorp-enterprise-supports-multicloud-applications-and-resources/){:target="_blank" rel="noopener"}

> We recently announced the general availability of BeyondCorp Enterprise, Google’s comprehensive zero trust product offering. As we work to democratize zero trust, building a solution to support customers across different environments was top of mind for our team. Google has over a decade of experience managing and securing cloud applications at a global scale and this new offering was developed based on learnings from our experience managing our own enterprise, feedback from customers and partners, as well as informed by leading engineering and security research. We recognize the complexities that come with a zero trust journey and understand that most [...]
