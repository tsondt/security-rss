Title: No joy for Julian Assange as Uncle Sam confirms it will keep pushing for WikiLeaker's extradition to America
Date: 2021-02-10T23:38:22+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: no-joy-for-julian-assange-as-uncle-sam-confirms-it-will-keep-pushing-for-wikileakers-extradition-to-america

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/10/julian_assange_dept_justice/){:target="_blank" rel="noopener"}

> Biden-era Dept of Justice forced to make call after UK judge blocks on mental health grounds The US Dept of Justice will continue pushing for the extradition of WikiLeaks founder Julian Assange, a spokesperson confirmed on Wednesday.... [...]
