Title: CDPA: Virginia’s new Consumer Data Protection Act heralds start of another busy year for US privacy legislators
Date: 2021-02-18T14:55:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cdpa-virginias-new-consumer-data-protection-act-heralds-start-of-another-busy-year-for-us-privacy-legislators

[Source](https://portswigger.net/daily-swig/cdpa-virginias-new-consumer-data-protection-act-heralds-start-of-another-busy-year-for-us-privacy-legislators){:target="_blank" rel="noopener"}

> Soon-to-be enacted privacy bill will go further to protect Virginia residents, writes attorney David Oberly [...]
