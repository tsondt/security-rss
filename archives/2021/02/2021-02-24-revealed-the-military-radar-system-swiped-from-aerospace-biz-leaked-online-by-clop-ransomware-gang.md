Title: Revealed: The military radar system swiped from aerospace biz, leaked online by Clop ransomware gang
Date: 2021-02-24T20:04:43+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: revealed-the-military-radar-system-swiped-from-aerospace-biz-leaked-online-by-clop-ransomware-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/24/seaspray_radar_ransomware/){:target="_blank" rel="noopener"}

> Not a great day for Bombardier, Leonardo and Seaspray customers A CAD drawing of a radar antenna stolen and leaked online by criminals is of a military radar system produced by defense contractor Leonardo and fitted to a number of US and UAE aircraft, The Register has learned.... [...]
