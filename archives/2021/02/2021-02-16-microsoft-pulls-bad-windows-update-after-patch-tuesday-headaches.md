Title: Microsoft Pulls Bad Windows Update After Patch Tuesday Headaches
Date: 2021-02-16T16:47:36+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: microsoft-pulls-bad-windows-update-after-patch-tuesday-headaches

[Source](https://threatpost.com/microsoft-windows-update-patch-tuesday/163981/){:target="_blank" rel="noopener"}

> Microsoft released a new servicing stack update (KB5001078) after an older one caused problems for Windows users installing Patch Tuesday security updates. [...]
