Title: Deliberately Playing Copyrighted Music to Avoid Being Live-Streamed
Date: 2021-02-15T19:11:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: copyright;hacking;law enforcement;police
Slug: deliberately-playing-copyrighted-music-to-avoid-being-live-streamed

[Source](https://www.schneier.com/blog/archives/2021/02/deliberately-playing-copyrighted-music-to-avoid-being-live-streamed.html){:target="_blank" rel="noopener"}

> Vice is reporting on a new police hack: playing copyrighted music when being filmed by citizens, trying to provoke social media sites into taking the videos down and maybe even banning the filmers: In a separate part of the video, which Devermont says was filmed later that same afternoon, Devermont approaches [BHPD Sgt. Billy] Fair outside. The interaction plays out almost exactly like it did in the department — when Devermont starts asking questions, Fair turns on the music. Devermont backs away, and asks him to stop playing music. Fair says “I can’t hear you” — again, despite holding a [...]
