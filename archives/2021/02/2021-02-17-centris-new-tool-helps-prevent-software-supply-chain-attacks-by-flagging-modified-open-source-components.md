Title: Centris: New tool helps prevent software supply chain attacks by flagging modified open source components
Date: 2021-02-17T13:14:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: centris-new-tool-helps-prevent-software-supply-chain-attacks-by-flagging-modified-open-source-components

[Source](https://portswigger.net/daily-swig/centris-new-tool-helps-prevent-software-supply-chain-attacks-by-flagging-modified-open-source-components){:target="_blank" rel="noopener"}

> Tackling vulnerability propagation and license violation, one scan at a time [...]
