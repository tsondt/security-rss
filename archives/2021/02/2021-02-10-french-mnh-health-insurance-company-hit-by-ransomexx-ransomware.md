Title: French MNH health insurance company hit by RansomExx ransomware
Date: 2021-02-10T17:01:53-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: french-mnh-health-insurance-company-hit-by-ransomexx-ransomware

[Source](https://www.bleepingcomputer.com/news/security/french-mnh-health-insurance-company-hit-by-ransomexx-ransomware/){:target="_blank" rel="noopener"}

> French health insurance company Mutuelle Nationale des Hospitaliers (MNH) has suffered a ransomware attack that has severely disrupted the company's operations. BleepingComputer has learned. [...]
