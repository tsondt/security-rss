Title: Microsoft shares CodeQL queries to scan code for SolarWinds-like implants
Date: 2021-02-25T13:11:14-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: microsoft-shares-codeql-queries-to-scan-code-for-solarwinds-like-implants

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-codeql-queries-to-scan-code-for-solarwinds-like-implants/){:target="_blank" rel="noopener"}

> Microsoft has open-sourced CodeQL queries that developers can use to scan source code for malicious implants matching the SolarWinds supply-chain attack. [...]
