Title: On Chinese-Owned Technology Platforms
Date: 2021-02-25T12:19:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: China;national security policy;reports;threat models
Slug: on-chinese-owned-technology-platforms

[Source](https://www.schneier.com/blog/archives/2021/02/on-chinese-owned-technology-platforms.html){:target="_blank" rel="noopener"}

> I am a co-author on a report published by the Hoover Institution: “ Chinese Technology Platforms Operating in the United States.” From a blog post : The report suggests a comprehensive framework for understanding and assessing the risks posed by Chinese technology platforms in the United States and developing tailored responses. It starts from the common view of the signatories — one reflected in numerous publicly available threat assessments — that China’s power is growing, that a large part of that power is in the digital sphere, and that China can and will wield that power in ways that adversely [...]
