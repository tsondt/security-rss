Title: Ransomware attacks increasingly destroy victims’ data by mistake
Date: 2021-02-04T03:21:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: ransomware-attacks-increasingly-destroy-victims-data-by-mistake

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attacks-increasingly-destroy-victims-data-by-mistake/){:target="_blank" rel="noopener"}

> More and more ransomware victims are resisting the extortionists and refuse to pay when they can recover from backups, despite hackers' threats to leak the data stolen before encryption. [...]
