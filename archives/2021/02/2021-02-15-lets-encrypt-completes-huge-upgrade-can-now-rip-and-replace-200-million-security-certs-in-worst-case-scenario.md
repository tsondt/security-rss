Title: Let's Encrypt completes huge upgrade, can now rip and replace 200 million security certs in 'worst case scenario'
Date: 2021-02-15T11:41:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: lets-encrypt-completes-huge-upgrade-can-now-rip-and-replace-200-million-security-certs-in-worst-case-scenario

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/15/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: SentinelOne picks up Scalyr, fatal flaws in TCP, and a view on Supermicro In brief Internet Security Research Group nonprofit Let's Encrypt has massively upgraded its certification hardware and software so that it can delete and reissue all its certs in less than 24 hours.... [...]
