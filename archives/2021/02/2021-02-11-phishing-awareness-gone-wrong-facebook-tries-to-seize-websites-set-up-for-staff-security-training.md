Title: Phishing awareness gone wrong: Facebook tries to seize websites set up for staff security training
Date: 2021-02-11T20:42:38+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: phishing-awareness-gone-wrong-facebook-tries-to-seize-websites-set-up-for-staff-security-training

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/11/facebook_phishing_domains/){:target="_blank" rel="noopener"}

> Antisocial network sued by Proofpoint in scrap over domain names Security biz Proofpoint and its subsidiary Wombat Security Technologies have sued Facebook and its Instagram subsidiary to prevent the seizure of internet domain names used for security testing.... [...]
