Title: DARPA bug bounty helps strengthen military research agency’s security defenses
Date: 2021-02-03T13:41:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: darpa-bug-bounty-helps-strengthen-military-research-agencys-security-defenses

[Source](https://portswigger.net/daily-swig/darpa-bug-bounty-helps-strengthen-military-research-agencys-security-defenses){:target="_blank" rel="noopener"}

> Run in partnership with Synack, the FETT program focused on addressing hardware vulnerabilities at their source [...]
