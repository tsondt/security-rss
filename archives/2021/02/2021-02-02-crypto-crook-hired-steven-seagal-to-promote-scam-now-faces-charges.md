Title: Crypto Crook Hired Steven Seagal to Promote Scam, Now Faces Charges
Date: 2021-02-02T18:17:18+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cryptography;Web Security
Slug: crypto-crook-hired-steven-seagal-to-promote-scam-now-faces-charges

[Source](https://threatpost.com/crypto-crook-steven-segal-scam/163612/){:target="_blank" rel="noopener"}

> Feds charged California-based private detective for stealing $11M from investors, with help from actor Steven Seagal. [...]
