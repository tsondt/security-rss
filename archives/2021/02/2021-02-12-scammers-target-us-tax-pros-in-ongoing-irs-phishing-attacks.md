Title: Scammers target US tax pros in ongoing IRS phishing attacks
Date: 2021-02-12T15:15:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: scammers-target-us-tax-pros-in-ongoing-irs-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/scammers-target-us-tax-pros-in-ongoing-irs-phishing-attacks/){:target="_blank" rel="noopener"}

> The Internal Revenue Service (IRS) has warned US tax professionals of identity thieves actively targeting them in a series of phishing attacks attempting to steal Electronic Filing Identification Numbers (EFINs). [...]
