Title: Five Eyes members warn of Accellion FTA extortion attacks
Date: 2021-02-24T10:09:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: five-eyes-members-warn-of-accellion-fta-extortion-attacks

[Source](https://www.bleepingcomputer.com/news/security/five-eyes-members-warn-of-accellion-fta-extortion-attacks/){:target="_blank" rel="noopener"}

> Four members of Five Eyes, in collaboration with Singapore as an active contributor, have issued a joint security advisory about ongoing attacks and extortion attempts targeting organizations using the Accellion File Transfer Appliance (FTA). [...]
