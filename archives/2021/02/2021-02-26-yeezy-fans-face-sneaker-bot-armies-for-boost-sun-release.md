Title: Yeezy Fans Face Sneaker-Bot Armies for Boost ‘Sun’ Release
Date: 2021-02-26T18:00:30+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: yeezy-fans-face-sneaker-bot-armies-for-boost-sun-release

[Source](https://threatpost.com/yeezy-sneaker-bots-boost-sun/164312/){:target="_blank" rel="noopener"}

> Sneaker bots ready to scoop up the new Yeezy Boost 700 “Sun” shoes to resell at a huge markup. [...]
