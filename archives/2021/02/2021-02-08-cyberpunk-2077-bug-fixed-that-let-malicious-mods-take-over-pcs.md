Title: Cyberpunk 2077 bug fixed that let malicious mods take over PCs
Date: 2021-02-08T14:10:42-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Gaming
Slug: cyberpunk-2077-bug-fixed-that-let-malicious-mods-take-over-pcs

[Source](https://www.bleepingcomputer.com/news/security/cyberpunk-2077-bug-fixed-that-let-malicious-mods-take-over-pcs/){:target="_blank" rel="noopener"}

> CD Projekt Red has released a hotfix for Cyberpunk 2077 to fix a remote code execution vulnerability that could be exploited by third-party data file modifications and save games files. [...]
