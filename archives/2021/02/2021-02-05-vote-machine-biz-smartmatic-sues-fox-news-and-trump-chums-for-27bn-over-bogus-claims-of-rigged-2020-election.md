Title: Vote machine biz Smartmatic sues Fox News and Trump chums for $2.7bn over bogus claims of rigged 2020 election
Date: 2021-02-05T02:41:23+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: vote-machine-biz-smartmatic-sues-fox-news-and-trump-chums-for-27bn-over-bogus-claims-of-rigged-2020-election

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/05/smartmatic_election_lawsuit/){:target="_blank" rel="noopener"}

> Turns out words have consequences Electronic voting machine maker Smartmatic has sued Fox News, three of its hosts, and two of Donald Trump’s loyalists – Rudy Giuliani and Sidney Powell – for an eye-popping $2.7bn in defamation damages over the false claims it stole the 2020 presidential election for Joe Biden.... [...]
