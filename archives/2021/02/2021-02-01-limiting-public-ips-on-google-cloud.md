Title: Limiting public IPs on Google Cloud
Date: 2021-02-01T18:00:00+00:00
Author: Stephanie Wong
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security;Networking;Developers & Practitioners
Slug: limiting-public-ips-on-google-cloud

[Source](https://cloud.google.com/blog/topics/developers-practitioners/limiting-public-ips-google-cloud/){:target="_blank" rel="noopener"}

> You’ve heard this saying: Hope is not a strategy when it comes to security. You have to approach security from all angles, while minimizing the burden on dev and SecOps. But with an ever increasing number of endpoints, networks, and attack surfaces, setting automated and trickle down security policies across your cloud infrastructure can be a challenge. On top of that administrators need to set guardrails to ensure that their workloads are always compliant with security requirements and industry regulations. Public IPs are among the most common ways that enterprise environments are exposed to the internet, making them susceptible to [...]
