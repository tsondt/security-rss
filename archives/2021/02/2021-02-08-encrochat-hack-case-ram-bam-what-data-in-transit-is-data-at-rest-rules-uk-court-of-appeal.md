Title: EncroChat hack case: RAM, bam... what? Data in transit is data at rest, rules UK Court of Appeal
Date: 2021-02-08T16:34:02+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: encrochat-hack-case-ram-bam-what-data-in-transit-is-data-at-rest-rules-uk-court-of-appeal

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/08/encrochat_court_appeal_ruling/){:target="_blank" rel="noopener"}

> That's the Snoopers' Charter in action for you British prosecutors can make use of evidence gathered by the French and Dutch police from encrypted messaging service Encrochat’s servers thanks to a legal interpretation of whether RAM counts as data storage, the Court of Appeal has ruled.... [...]
