Title: Joker’s Stash closure kicks off competition for a successor
Date: 2021-02-15T16:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: jokers-stash-closure-kicks-off-competition-for-a-successor

[Source](https://portswigger.net/daily-swig/jokers-stash-closure-kicks-off-competition-for-a-successor){:target="_blank" rel="noopener"}

> Market for stolen payment cards will persist, threat intel firm predicts [...]
