Title: Planespotters’ weekends turn traumatic as engine pieces fall from the sky in the Netherlands and the US
Date: 2021-02-22T13:03:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: planespotters-weekends-turn-traumatic-as-engine-pieces-fall-from-the-sky-in-the-netherlands-and-the-us

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/22/planespotters_weekends_turn_traumatic_as/){:target="_blank" rel="noopener"}

> It’s a bird, it’s a plane... holy crap there’s a nacelle in my kitchen In what can only be described as a bad day for Boeing, not one but two of its planes suffered engine fire and began shedding parts along their respective flight paths.... [...]
