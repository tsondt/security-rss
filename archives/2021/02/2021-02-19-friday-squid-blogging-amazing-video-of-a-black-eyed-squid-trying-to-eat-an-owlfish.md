Title: Friday Squid Blogging: Amazing Video of a Black-Eyed Squid Trying to Eat an Owlfish
Date: 2021-02-19T22:16:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid;video
Slug: friday-squid-blogging-amazing-video-of-a-black-eyed-squid-trying-to-eat-an-owlfish

[Source](https://www.schneier.com/blog/archives/2021/02/friday-squid-blogging-amazing-video-of-a-black-eyed-squid-trying-to-eat-an-owlfish.html){:target="_blank" rel="noopener"}

> From the Monterey Bay Aquarium. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
