Title: Microsoft Patch Tuesday, February 2021 Edition
Date: 2021-02-09T22:37:19+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;CVE-2020-1472;CVE-2021-1732;CVE-2021-21148;CVE-2021-24078;Immersive Labs;Kevin Breen;Microsoft Patch Tuesday February 2021;Netlogon;Recorded Future;ZeroLogon
Slug: microsoft-patch-tuesday-february-2021-edition

[Source](https://krebsonsecurity.com/2021/02/microsoft-patch-tuesday-february-2021-edition/){:target="_blank" rel="noopener"}

> Microsoft today rolled out updates to plug at least 56 security holes in its Windows operating systems and other software. One of the bugs is already being actively exploited, and six of them were publicized prior to today, potentially giving attackers a head start in figuring out how to exploit the flaws. Nine of the 56 vulnerabilities earned Microsoft’s most urgent “critical” rating, meaning malware or miscreants could use them to seize remote control over unpatched systems with little or no help from users. The flaw being exploited in the wild already — CVE-2021-1732 — affects Windows 10, Server 2016 [...]
