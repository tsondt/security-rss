Title: Unpatched WordPress Plugin Code-Injection Bug Afflicts 50K Sites
Date: 2021-02-05T22:20:20+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: unpatched-wordpress-plugin-code-injection-bug-afflicts-50k-sites

[Source](https://threatpost.com/unpatched-wordpress-plugin-code-injection/163706/){:target="_blank" rel="noopener"}

> An CRSF-to-stored-XSS security bug plagues 50,000 'Contact Form 7' Style users. [...]
