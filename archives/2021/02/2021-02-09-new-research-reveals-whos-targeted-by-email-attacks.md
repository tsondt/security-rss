Title: New research reveals who’s targeted by email attacks
Date: 2021-02-09T14:00:00+00:00
Author: Neil Kumaran
Category: GCP Security
Tags: Identity & Security;Google Workspace
Slug: new-research-reveals-whos-targeted-by-email-attacks

[Source](https://cloud.google.com/blog/products/workspace/how-gmail-helps-users-avoid-email-scams/){:target="_blank" rel="noopener"}

> Every day, we stop more than 100 million harmful emails from reaching Gmail users. Last year, during the peak of the pandemic crisis we saw 18 million daily malware and phishing emails related to COVID-19. This is in addition to more than 240 million COVID-related daily spam messages. Our ML models evolve to understand and filter new threats, and we continue to block more than 99.9% of spam, phishing, and malware from reaching our users. We wanted to explore what factors influence being targeted by email phishing and malware and whether higher-risk users are adopting the strongest protections we have [...]
