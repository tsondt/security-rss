Title: Updated whitepaper available: Encrypting File Data with Amazon Elastic File System
Date: 2021-02-23T19:33:46+00:00
Author: Joe Travaglini
Category: AWS Security
Tags: Amazon Elastic File System (EFS);Announcements;Foundational (100);Security, Identity, & Compliance;Amazon EFS;Amazon Elastic File System;Security Blog;Whitepaper
Slug: updated-whitepaper-available-encrypting-file-data-with-amazon-elastic-file-system

[Source](https://aws.amazon.com/blogs/security/updated-whitepaper-available-encrypting-file-data-with-amazon-elastic-file-system/){:target="_blank" rel="noopener"}

> We’re sharing an update to the Encrypting File Data with Amazon Elastic File System whitepaper to provide customers with guidance on enforcing encryption of data at rest and in transit in Amazon Elastic File System (Amazon EFS). Amazon EFS provides simple, scalable, highly available, and highly durable shared file systems in the cloud. The file [...]
