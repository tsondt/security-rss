Title: Cyberattacks Launch Against Vietnamese Human-Rights Activists
Date: 2021-02-25T20:06:04+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware
Slug: cyberattacks-launch-against-vietnamese-human-rights-activists

[Source](https://threatpost.com/cyberattacks-vietnam-human-rights-activists/164284/){:target="_blank" rel="noopener"}

> Vietnam joins the ranks of governments using spyware to crack down on human-rights defenders. [...]
