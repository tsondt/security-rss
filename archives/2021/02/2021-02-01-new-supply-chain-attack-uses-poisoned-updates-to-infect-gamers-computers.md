Title: New supply chain attack uses poisoned updates to infect gamers’ computers
Date: 2021-02-01T20:41:40+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Gaming & Culture;Tech;bignox;gaming;malware;noxplayer;supply chain attack
Slug: new-supply-chain-attack-uses-poisoned-updates-to-infect-gamers-computers

[Source](https://arstechnica.com/?p=1738739){:target="_blank" rel="noopener"}

> Enlarge / Circuit board with speed motion and light. (credit: Getty Images ) Researchers have uncovered a software supply chain attack that is being used to install surveillance malware on the computers of online gamers. The unknown attackers are targeting select users of NoxPlayer, a software package that emulates the Android operating system on PCs and Macs. People use it primarily for playing mobile Android games on these platforms. NoxPlayer-maker BigNox says the software has 150 million users in 150 countries. Poisoning the well Security firm Eset said on Monday that the BigNox software distribution system was hacked and used [...]
