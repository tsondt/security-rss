Title: European volleyball org's Azure bucket exposed reporter passports
Date: 2021-02-01T10:45:03-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Cloud;Microsoft
Slug: european-volleyball-orgs-azure-bucket-exposed-reporter-passports

[Source](https://www.bleepingcomputer.com/news/security/european-volleyball-orgs-azure-bucket-exposed-reporter-passports/){:target="_blank" rel="noopener"}

> A publicly exposed cloud storage bucket was found to contain images of hundreds of passports and identity documents belonging to journalists and volleyball players from around the world. [...]
