Title: Microsoft president asks Congress to force private-sector orgs to publicly admit when they've been hacked
Date: 2021-02-24T00:53:52+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: microsoft-president-asks-congress-to-force-private-sector-orgs-to-publicly-admit-when-theyve-been-hacked

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/24/microsoft_solarwinds_congress/){:target="_blank" rel="noopener"}

> Senate intelligence committee hears ideas in light of SolarWinds disaster The private sector should be legally obliged to disclose any major hacks of their systems, says Microsoft’s president and top lawyer Brad Smith.... [...]
