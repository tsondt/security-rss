Title: The Problem with Treating Data as a Commodity
Date: 2021-02-26T12:28:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: data protection;laws;privacy;reports
Slug: the-problem-with-treating-data-as-a-commodity

[Source](https://www.schneier.com/blog/archives/2021/02/the-problem-with-treating-data-as-a-commodity.html){:target="_blank" rel="noopener"}

> Excellent Brookings paper: “ Why data ownership is the wrong approach to protecting privacy.” From the introduction: Treating data like it is property fails to recognize either the value that varieties of personal information serve or the abiding interest that individuals have in their personal information even if they choose to “sell” it. Data is not a commodity. It is information. Any system of information rights­ — whether patents, copyrights, and other intellectual property, or privacy rights — ­presents some tension with strong interest in the free flow of information that is reflected by the First Amendment. Our personal information [...]
