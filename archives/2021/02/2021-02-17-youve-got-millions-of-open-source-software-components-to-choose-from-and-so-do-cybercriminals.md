Title: You’ve got millions of open-source software components to choose from... and so do cybercriminals
Date: 2021-02-17T20:00:13+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: youve-got-millions-of-open-source-software-components-to-choose-from-and-so-do-cybercriminals

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/17/who_is_running_your_project/){:target="_blank" rel="noopener"}

> Just who is running your favourite project these days? Sponsored In November 2020, the JavaScript registry npm flashed a security advisory that a library called twilio-npm harboured malicious code which could backdoor any machine it was downloaded to. Perhaps the most troubling aspect of this tale is that this was the seventh such malicious package found on npm within a month, a stark illustration of the effort that cybercriminals are making to insert themselves into the open source software supply chain.... [...]
