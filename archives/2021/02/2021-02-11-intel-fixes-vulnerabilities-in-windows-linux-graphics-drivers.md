Title: Intel fixes vulnerabilities in Windows, Linux graphics drivers
Date: 2021-02-11T10:02:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Linux;Microsoft
Slug: intel-fixes-vulnerabilities-in-windows-linux-graphics-drivers

[Source](https://www.bleepingcomputer.com/news/security/intel-fixes-vulnerabilities-in-windows-linux-graphics-drivers/){:target="_blank" rel="noopener"}

> Intel addressed 57 vulnerabilities during this month's Patch Tuesday, including high severity ones impacting Intel Graphics Drivers. [...]
