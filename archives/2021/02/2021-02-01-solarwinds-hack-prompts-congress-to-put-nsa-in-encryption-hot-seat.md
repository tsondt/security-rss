Title: SolarWinds Hack Prompts Congress to Put NSA in Encryption Hot Seat
Date: 2021-02-01T21:12:13+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cryptography
Slug: solarwinds-hack-prompts-congress-to-put-nsa-in-encryption-hot-seat

[Source](https://threatpost.com/solarwinds-nsa-encryption/163561/){:target="_blank" rel="noopener"}

> Congress is demanding the National Security Agency come clean on what it knows about the 2015 supply-chain attack against Juniper Networks. [...]
