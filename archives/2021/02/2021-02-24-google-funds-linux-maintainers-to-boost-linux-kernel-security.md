Title: Google funds Linux maintainers to boost Linux kernel security
Date: 2021-02-24T13:48:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Linux
Slug: google-funds-linux-maintainers-to-boost-linux-kernel-security

[Source](https://www.bleepingcomputer.com/news/security/google-funds-linux-maintainers-to-boost-linux-kernel-security/){:target="_blank" rel="noopener"}

> Together with the Linux Foundation, Google announced today that they would fund two Linux kernel developers' efforts as full-time maintainers exclusively focused on improving Linux security. [...]
