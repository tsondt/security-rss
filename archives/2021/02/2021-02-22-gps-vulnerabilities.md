Title: GPS Vulnerabilities
Date: 2021-02-22T12:17:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;GPS;infrastructure;national security policy
Slug: gps-vulnerabilities

[Source](https://www.schneier.com/blog/archives/2021/02/gps-vulnerabilities.html){:target="_blank" rel="noopener"}

> Really good op-ed in the New York Times about how vulnerable the GPS system is to interference, spoofing, and jamming — and potential alternatives. The 2018 National Defense Authorization Act included funding for the Departments of Defense, Homeland Security and Transportation to jointly conduct demonstrations of various alternatives to GPS, which were concluded last March. Eleven potential systems were tested, including eLoran, a low-frequency, high-power timing and navigation system transmitted from terrestrial towers at Coast Guard facilities throughout the United States. “China, Russia, Iran, South Korea and Saudi Arabia all have eLoran systems because they don’t want to be as [...]
