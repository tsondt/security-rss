Title: Buggy WordPress plugin exposes 100K sites to takeover attacks
Date: 2021-02-11T12:05:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: buggy-wordpress-plugin-exposes-100k-sites-to-takeover-attacks

[Source](https://www.bleepingcomputer.com/news/security/buggy-wordpress-plugin-exposes-100k-sites-to-takeover-attacks/){:target="_blank" rel="noopener"}

> Critical and high severity vulnerabilities in the Responsive Menu WordPress plugin exposed over 100,000 sites to takeover attacks as discovered by Wordfence. [...]
