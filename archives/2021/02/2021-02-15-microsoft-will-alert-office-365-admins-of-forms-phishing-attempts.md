Title: Microsoft will alert Office 365 admins of Forms phishing attempts
Date: 2021-02-15T11:51:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-will-alert-office-365-admins-of-forms-phishing-attempts

[Source](https://www.bleepingcomputer.com/news/security/microsoft-will-alert-office-365-admins-of-forms-phishing-attempts/){:target="_blank" rel="noopener"}

> Microsoft is adding new security warnings to the Security and Compliance Center (SCC) default alert policies to inform IT admins of detected phishing attempts abusing Microsoft Forms in their tenants. [...]
