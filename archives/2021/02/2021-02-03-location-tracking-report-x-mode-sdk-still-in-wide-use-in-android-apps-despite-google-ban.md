Title: Location tracking report: X-Mode SDK still in wide use in Android apps despite Google ban
Date: 2021-02-03T10:15:06+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: location-tracking-report-x-mode-sdk-still-in-wide-use-in-android-apps-despite-google-ban

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/03/location_tracking_report_xmode_sdk/){:target="_blank" rel="noopener"}

> 450 Android apps track location, 1.7bn downloads, 44% use X-Mode code: only 10% pulled off Play Store A report on Android apps that do location tracking identified 450 apps that use tracker SDKs, many of which use an SDK called X-Mode, which Apple and Google have banned, but are still in Google's Play Store.... [...]
