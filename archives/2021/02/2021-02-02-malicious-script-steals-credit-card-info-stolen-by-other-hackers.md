Title: Malicious script steals credit card info stolen by other hackers
Date: 2021-02-02T11:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: malicious-script-steals-credit-card-info-stolen-by-other-hackers

[Source](https://www.bleepingcomputer.com/news/security/malicious-script-steals-credit-card-info-stolen-by-other-hackers/){:target="_blank" rel="noopener"}

> A threat actor has infected an e-commerce store with a custom credit card skimmer designed to siphon data stolen by a previously deployed Magento card stealer. [...]
