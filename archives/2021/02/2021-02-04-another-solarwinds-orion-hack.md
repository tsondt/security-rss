Title: Another SolarWinds Orion Hack
Date: 2021-02-04T12:11:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;China;cyberespionage;FBI;hacking;Russia;supply chain
Slug: another-solarwinds-orion-hack

[Source](https://www.schneier.com/blog/archives/2021/02/another-solarwinds-orion-hack.html){:target="_blank" rel="noopener"}

> At the same time the Russians were using a backdoored SolarWinds update to attack networks worldwide, another threat actor — believed to be Chinese in origin — was using an already existing vulnerability in Orion to penetrate networks : Two people briefed on the case said FBI investigators recently found that the National Finance Center, a federal payroll agency inside the U.S. Department of Agriculture, was among the affected organizations, raising fears that data on thousands of government employees may have been compromised. [...] Reuters was not able to establish how many organizations were compromised by the suspected Chinese operation. [...]
