Title: Mexican Politician Removed Over Alleged Ties to Romanian ATM Skimmer Gang
Date: 2021-02-19T16:25:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: All About Skimmers;Ne'er-Do-Well News;ATM Skimmers;Constantin Sorinel Marcu;Florian Tudor;Intacash;José Alberto Gómez Álvarez;Jose de la Peña Ruiz de Chávez;José Luis Jonathan Yong;José Luis Yong Cruz;Mexico;Naín Díaz Medina;OCCRP;Organized Crime and Corruption Reporting Project;Riviera Maya gang;The Shark
Slug: mexican-politician-removed-over-alleged-ties-to-romanian-atm-skimmer-gang

[Source](https://krebsonsecurity.com/2021/02/mexican-politician-removed-over-alleged-ties-to-romanian-atm-skimmer-gang/){:target="_blank" rel="noopener"}

> The leader of Mexico’s Green Party has been removed from office following allegations that he received money from a Romanian ATM skimmer gang that stole hundreds of millions of dollars from tourists visiting Mexico’s top tourist destinations over the past five years. The scandal is the latest fallout stemming from a three-part investigation into the organized crime group by KrebsOnSecurity in 2015. One of the Bluetooth-enabled PIN pads pulled from a compromised ATM in Mexico. The two components on the left are legitimate parts of the machine. The fake PIN pad made to be slipped under the legit PIN pad [...]
