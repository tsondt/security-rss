Title: Microsoft Defender now detects macOS system, app vulnerabilities
Date: 2021-02-02T13:46:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-now-detects-macos-system-app-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-now-detects-macos-system-app-vulnerabilities/){:target="_blank" rel="noopener"}

> Microsoft announced that Defender for Endpoint will now also help admins discover OS and software vulnerabilities affecting macOS devices on their organization's network. [...]
