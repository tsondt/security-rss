Title: Five Critical Android Bugs Patched, Part of Feb. Security Bulletin
Date: 2021-02-03T15:40:01+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security
Slug: five-critical-android-bugs-patched-part-of-feb-security-bulletin

[Source](https://threatpost.com/five-critical-bugs-patched-feb-security-bulletin/163623/){:target="_blank" rel="noopener"}

> February’s security update for the mobile OS includes a Qualcomm flaw rated critical, with a CVSS score of 9.8. [...]
