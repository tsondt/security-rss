Title: Cybercrooks Rake in $304M in Romance Scams
Date: 2021-02-15T20:50:58+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Most Recent ThreatLists;Web Security
Slug: cybercrooks-rake-in-304m-in-romance-scams

[Source](https://threatpost.com/cybercrooks-304m-romance-scams/163972/){:target="_blank" rel="noopener"}

> The number of people being targeted by fake relationship-seekers has spiked during the COVID-19 pandemic. [...]
