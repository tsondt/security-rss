Title: Nginx: Server misconfigurations found in the wild that expose websites to attacks
Date: 2021-02-24T13:13:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nginx-server-misconfigurations-found-in-the-wild-that-expose-websites-to-attacks

[Source](https://portswigger.net/daily-swig/nginx-server-misconfigurations-found-in-the-wild-that-expose-websites-to-attacks){:target="_blank" rel="noopener"}

> Widely used web server’s flexibility means mistakes all too easy to make [...]
