Title: Mitigate data leakage through the use of AppStream 2.0 and end-to-end auditing
Date: 2021-02-10T19:33:35+00:00
Author: Chaim Landau
Category: AWS Security
Tags: Amazon AppStream 2.0;Amazon SageMaker;AWS Cloud Development Kit;Expert (400);Security, Identity, & Compliance;AWS CDK;Devops;Security Blog
Slug: mitigate-data-leakage-through-the-use-of-appstream-20-and-end-to-end-auditing

[Source](https://aws.amazon.com/blogs/security/mitigate-data-leakage-through-the-use-of-appstream-2-0-and-end-to-end-auditing/){:target="_blank" rel="noopener"}

> Customers want to use AWS services to operate on their most sensitive data, but they want to make sure that only the right people have access to that data. Even when the right people are accessing data, customers want to account for what actions those users took while accessing the data. In this post, we [...]
