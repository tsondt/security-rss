Title: Ziggy ransomware shuts down and releases victims' decryption keys
Date: 2021-02-07T13:53:26-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ziggy-ransomware-shuts-down-and-releases-victims-decryption-keys

[Source](https://www.bleepingcomputer.com/news/security/ziggy-ransomware-shuts-down-and-releases-victims-decryption-keys/){:target="_blank" rel="noopener"}

> The Ziggy ransomware operation has shut down and released the victims' decryption keys after concerns about recent law enforcement activity and guilt for encrypting victims. [...]
