Title: Microsoft releases Azure Firewall Premium in public preview
Date: 2021-02-16T16:20:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-releases-azure-firewall-premium-in-public-preview

[Source](https://www.bleepingcomputer.com/news/security/microsoft-releases-azure-firewall-premium-in-public-preview/){:target="_blank" rel="noopener"}

> Microsoft has announced that the new Premium tier for its managed cloud-based network security service Azure Firewall has entered public preview starting today. [...]
