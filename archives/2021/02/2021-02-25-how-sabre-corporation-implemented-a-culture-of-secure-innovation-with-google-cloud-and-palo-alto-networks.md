Title: How Sabre Corporation implemented a culture of secure innovation with Google Cloud and Palo Alto Networks
Date: 2021-02-25T17:00:00+00:00
Author: Manvinder Singh
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security;Customers
Slug: how-sabre-corporation-implemented-a-culture-of-secure-innovation-with-google-cloud-and-palo-alto-networks

[Source](https://cloud.google.com/blog/topics/customers/why-sabre-corp-uses-prisma-cloud-compute-gmon-google-cloud/){:target="_blank" rel="noopener"}

> Sabre Corporation is committed to driving innovation in the global travel industry and developing solutions that help airlines, hotels, and travel agencies transform the traveler experience and satisfy the ever-evolving travel needs of its customers. In order to build these solutions, Sabre moved to the cloud for newer, faster, and more open platform technologies. Sabre’s adoption of Google Cloud was key to paving the way to deliver a new, personalized travel experience. “Sabre was really embarking on unchartered territory and Google Cloud was the cloud service provider and partner who worked alongside us as a co-champion to help us develop [...]
