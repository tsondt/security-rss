Title: Hackers tried poisoning town after breaching its water facility
Date: 2021-02-08T17:50:12-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hackers-tried-poisoning-town-after-breaching-its-water-facility

[Source](https://www.bleepingcomputer.com/news/security/hackers-tried-poisoning-town-after-breaching-its-water-facility/){:target="_blank" rel="noopener"}

> A hacker gained access to the water treatment system for the city of Oldsmar, Florida, and attempted to increase the concentration of sodium hydroxide (NaOH), also known as lye and caustic soda, to extremely dangerous levels. [...]
