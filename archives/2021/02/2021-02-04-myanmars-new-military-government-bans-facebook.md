Title: Myanmar’s new military government bans Facebook
Date: 2021-02-04T00:24:17+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: myanmars-new-military-government-bans-facebook

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/04/myanmar_facebook_ban/){:target="_blank" rel="noopener"}

> Oh look, Cloudflare spots a sudden surge in use of other messaging apps The new self-appointed military government of Myanmar has temporarily banned Facebook.... [...]
