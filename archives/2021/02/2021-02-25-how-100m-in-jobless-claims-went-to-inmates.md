Title: How $100M in Jobless Claims Went to Inmates
Date: 2021-02-25T22:26:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Blake Hall;id.me;Jim Patterson;Labor Department
Slug: how-100m-in-jobless-claims-went-to-inmates

[Source](https://krebsonsecurity.com/2021/02/how-100m-in-jobless-claims-went-to-inmates/){:target="_blank" rel="noopener"}

> The U.S. Labor Department’s inspector general said this week that roughly $100 million in fraudulent unemployment insurance claims were paid in 2020 to criminals who are already in jail. That’s a tiny share of the estimated tens of billions of dollars in jobless benefits states have given to identity thieves in the past year. To help reverse that trend, many states are now turning to a little-known private company called ID.me. This post examines some of what that company is seeing in its efforts to stymie unemployment fraud. These prisoners tried to apply for jobless benefits. Personal information from the [...]
