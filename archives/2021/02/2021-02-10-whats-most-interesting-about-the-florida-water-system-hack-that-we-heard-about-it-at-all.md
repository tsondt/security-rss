Title: What’s most interesting about the Florida water system hack? That we heard about it at all.
Date: 2021-02-10T22:13:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Andrew Hildick-Smith;Applied Control Solutions;Bob Gualtieri;CERT Polska;Environmental Protection Agency;Florida water hack;Joe Weiss;Marcin Dudek;Teamviewer;Water ISAC
Slug: whats-most-interesting-about-the-florida-water-system-hack-that-we-heard-about-it-at-all

[Source](https://krebsonsecurity.com/2021/02/whats-most-interesting-about-the-florida-water-system-hack-that-we-heard-about-it-at-all/){:target="_blank" rel="noopener"}

> Stories about computer security tend to go viral when they bridge the vast divide between geeks and luddites, and this week’s news about a hacker who tried to poison a Florida town’s water supply was understandably front-page material. But for security nerds who’ve been warning about this sort of thing for ages, the most surprising aspect of the incident seems to be that we learned about it at all. Spend a few minutes searching Twitter, Reddit or any number of other social media sites and you’ll find countless examples of researchers posting proof of being able to access so-called “human-machine [...]
