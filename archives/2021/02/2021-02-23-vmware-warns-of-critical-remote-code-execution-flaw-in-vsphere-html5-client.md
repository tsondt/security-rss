Title: VMware warns of critical remote code execution flaw in vSphere HTML5 client
Date: 2021-02-23T23:35:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: vmware-warns-of-critical-remote-code-execution-flaw-in-vsphere-html5-client

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/23/vmware_vsphere_critical_bugs/){:target="_blank" rel="noopener"}

> If you don’t patch, the hosts driving all your virty servers are at risk. So maybe your to-do list needs a tickle? VMware has revealed a critical-rated bug in the HTML5 client for its flagship vSphere hybrid cloud suite.... [...]
