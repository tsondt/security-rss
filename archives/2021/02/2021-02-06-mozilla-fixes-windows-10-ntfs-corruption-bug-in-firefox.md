Title: Mozilla fixes Windows 10 NTFS corruption bug in Firefox
Date: 2021-02-06T15:07:59-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software;Microsoft
Slug: mozilla-fixes-windows-10-ntfs-corruption-bug-in-firefox

[Source](https://www.bleepingcomputer.com/news/software/mozilla-fixes-windows-10-ntfs-corruption-bug-in-firefox/){:target="_blank" rel="noopener"}

> Mozilla has released Firefox 85.0.1 and includes a fix that prevents a Windows 10 NTFS corruption bug from being triggered from the browser. [...]
