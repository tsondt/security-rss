Title: Software supply chain attacks – everything you need to know
Date: 2021-02-11T12:31:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: software-supply-chain-attacks-everything-you-need-to-know

[Source](https://portswigger.net/daily-swig/software-supply-chain-attacks-everything-you-need-to-know){:target="_blank" rel="noopener"}

> The SolarWinds breach brought a dangerous attack vector to the fore, but supply chain attacks are far from a new phenomenon [...]
