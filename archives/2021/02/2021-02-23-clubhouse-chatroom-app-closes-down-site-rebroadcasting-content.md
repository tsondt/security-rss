Title: Clubhouse chatroom app closes down site rebroadcasting content
Date: 2021-02-23T11:55:27+00:00
Author: Alex Hern UK technology editor
Category: The Guardian
Tags: Internet;US news;Technology;World news;Data and computer security
Slug: clubhouse-chatroom-app-closes-down-site-rebroadcasting-content

[Source](https://www.theguardian.com/technology/2021/feb/23/clubhouse-chatroom-app-closes-down-site-rebroadcasting-content){:target="_blank" rel="noopener"}

> Incident prompts fears for latest Silicon Valley craze’s ability to guarantee users’ security and privacy Clubhouse, the audio-chatroom app that has emerged as the latest craze to consume Silicon Valley, has shut down a site that was rebroadcasting the platform’s content, renewing concerns over the service’s ability to provide security and privacy for its users. The app, currently available only on iPhones, allows users to quickly and easily set up and discover panel-style discussions, with a small group of speakers and potentially thousands of listeners in each room. It has been strictly limited since its launch in April, with users [...]
