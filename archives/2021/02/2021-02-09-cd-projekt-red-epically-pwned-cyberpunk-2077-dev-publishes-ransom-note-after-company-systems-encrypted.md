Title: CD Projekt Red 'EPICALLY pwned': <i>Cyberpunk 2077</i> dev publishes ransom note after company systems encrypted
Date: 2021-02-09T12:28:12+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: cd-projekt-red-epically-pwned-cyberpunk-2077-dev-publishes-ransom-note-after-company-systems-encrypted

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/09/cd_projekt_red_hack/){:target="_blank" rel="noopener"}

> Hackers threaten to release source and docs, but games giant isn't playing ball CD Projekt Red, the Polish developer of Cyberpunk 2077 and The Witcher 3, has disclosed a major security incident in which several company systems were encrypted and confidential data stolen.... [...]
