Title: France's cyber-agency says Centreon IT management software sabotaged by Russian Sandworm
Date: 2021-02-16T08:02:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: frances-cyber-agency-says-centreon-it-management-software-sabotaged-by-russian-sandworm

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/16/centreon_sandworm_attack/){:target="_blank" rel="noopener"}

> Web hosts infiltrated for up to three years in attack that somewhat resembles SolarWinds mess France’s Agence nationale de la sécurité des systèmes d'information (ANSSI), the nation’s cyber-security agency, has identified a years-long campaign to infiltrate IT monitoring platform Centreon.... [...]
