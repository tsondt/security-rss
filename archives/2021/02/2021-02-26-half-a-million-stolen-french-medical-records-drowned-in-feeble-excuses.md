Title: Half a million stolen French medical records, drowned in feeble excuses
Date: 2021-02-26T09:30:10+00:00
Author: Alistair Dabbs
Category: The Register
Tags: 
Slug: half-a-million-stolen-french-medical-records-drowned-in-feeble-excuses

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/26/something_for_the_weekend/){:target="_blank" rel="noopener"}

> A bitter pill best swallowed with eight gallons of swimming pool water Something for the Weekend, Sir? Those files I promised you? Oh, I'm sorry, they accidentally got taken out with the recycling. A gull swooped down and snatched them out of my hands. They were lost in a tsunami. No, a forest fire. An earthquake. Actually, to tell the truth, my mum put them in the washing machine.... [...]
