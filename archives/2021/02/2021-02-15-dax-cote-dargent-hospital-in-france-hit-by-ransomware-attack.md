Title: Dax-Côte d’Argent hospital in France hit by ransomware attack
Date: 2021-02-15T14:05:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: dax-cote-dargent-hospital-in-france-hit-by-ransomware-attack

[Source](https://portswigger.net/daily-swig/dax-cote-dargent-hospital-in-france-hit-by-ransomware-attack){:target="_blank" rel="noopener"}

> Egregor ransomware gang involved, according to reports [...]
