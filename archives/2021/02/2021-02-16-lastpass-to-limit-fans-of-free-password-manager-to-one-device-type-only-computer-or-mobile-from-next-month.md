Title: LastPass to limit fans of free password manager to one device type only – computer or mobile – from next month
Date: 2021-02-16T23:27:45+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: lastpass-to-limit-fans-of-free-password-manager-to-one-device-type-only-computer-or-mobile-from-next-month

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/16/lastpass_pricing_changes/){:target="_blank" rel="noopener"}

> Cough up if you want to use it with your laptop and phone Password manager LastPass has changed its terms and conditions to limit the free version of its code work on a single device type only per user, seemingly in an effort to force free folks into paying for its service.... [...]
