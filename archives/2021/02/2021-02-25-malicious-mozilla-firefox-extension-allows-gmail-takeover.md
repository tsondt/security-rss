Title: Malicious Mozilla Firefox Extension Allows Gmail Takeover
Date: 2021-02-25T17:04:38+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: malicious-mozilla-firefox-extension-allows-gmail-takeover

[Source](https://threatpost.com/malicious-mozilla-firefox-gmail/164263/){:target="_blank" rel="noopener"}

> The malicious extension, FriarFox, snoops in on both Firefox and Gmail-related data. [...]
