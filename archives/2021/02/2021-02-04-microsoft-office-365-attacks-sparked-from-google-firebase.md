Title: Microsoft Office 365 Attacks Sparked from Google Firebase
Date: 2021-02-04T15:58:45+00:00
Author: Tara Seals
Category: Threatpost
Tags: Web Security
Slug: microsoft-office-365-attacks-sparked-from-google-firebase

[Source](https://threatpost.com/microsoft-office-365-attacks-google-firebase/163666/){:target="_blank" rel="noopener"}

> A savvy phishing campaign manages to evade native Microsoft security defenses, looking to steal O365 credentials. [...]
