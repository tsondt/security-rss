Title: Ukrainian citizen jailed for role in cybercrime spree that leeched $3m from US businesses
Date: 2021-02-12T12:41:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ukrainian-citizen-jailed-for-role-in-cybercrime-spree-that-leeched-3m-from-us-businesses

[Source](https://portswigger.net/daily-swig/ukrainian-citizen-jailed-for-role-in-cybercrime-spree-that-leeched-3m-from-us-businesses){:target="_blank" rel="noopener"}

> Accused recruited mules who thought they were working for a legitimate business [...]
