Title: DDoS Attacks Wane in Q4 Amid Cryptomining Resurgence
Date: 2021-02-16T21:27:06+00:00
Author: Tara Seals
Category: Threatpost
Tags: Most Recent ThreatLists;Web Security
Slug: ddos-attacks-wane-in-q4-amid-cryptomining-resurgence

[Source](https://threatpost.com/ddos-attacks-q4-cryptomining-resurgence/163998/){:target="_blank" rel="noopener"}

> The volume of attacks fell 31 percent in the last part of 2020, as Bitcoin values skyrocketed. But there were still several notable trends, such as a rise in Linux botnets. [...]
