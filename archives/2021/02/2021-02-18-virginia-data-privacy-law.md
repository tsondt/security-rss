Title: Virginia Data Privacy Law
Date: 2021-02-18T12:13:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: courts;data protection;laws;privacy
Slug: virginia-data-privacy-law

[Source](https://www.schneier.com/blog/archives/2021/02/virginia-data-privacy-law.html){:target="_blank" rel="noopener"}

> Virginia is about to get a data privacy law, modeled on California’s law. [...]
