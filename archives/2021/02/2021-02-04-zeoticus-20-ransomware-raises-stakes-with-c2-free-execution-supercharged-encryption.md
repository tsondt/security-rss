Title: Zeoticus 2.0 ransomware raises stakes with C2-free execution, supercharged encryption
Date: 2021-02-04T15:08:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: zeoticus-20-ransomware-raises-stakes-with-c2-free-execution-supercharged-encryption

[Source](https://portswigger.net/daily-swig/zeoticus-2-0-ransomware-raises-stakes-with-c2-free-execution-supercharged-encryption){:target="_blank" rel="noopener"}

> Infections now harder to control, contain, and mitigate [...]
