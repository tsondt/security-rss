Title: US cities disclose data breaches after vendor's ransomware attack
Date: 2021-02-18T23:02:17-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: us-cities-disclose-data-breaches-after-vendors-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/us-cities-disclose-data-breaches-after-vendors-ransomware-attack/){:target="_blank" rel="noopener"}

> A ransomware attack against the widely used payment processor ATFS has sparked data breach notifications from numerous cities and agencies within California and Washington. [...]
