Title: Footfallcam kerfuffle: Firm apologises, promises to fix product after viral Twitter thread, infoseccer backlash
Date: 2021-02-12T15:21:19+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: footfallcam-kerfuffle-firm-apologises-promises-to-fix-product-after-viral-twitter-thread-infoseccer-backlash

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/12/footfallcam_twitter_kerfuffle/){:target="_blank" rel="noopener"}

> Accusations of grey hat infosec consultancy extortion ring drop away after El Reg intervenes A cautionary tale about the dangers posed by affordable Internet of Things devices turned into a much more sinister story after a company threatened an infosec bod with a police report ( since retracted ) unless he deleted a Twitter thread highlighting shortcomings in one of its products.... [...]
