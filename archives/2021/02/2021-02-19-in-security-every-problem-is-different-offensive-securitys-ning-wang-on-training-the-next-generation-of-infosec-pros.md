Title: ‘In security, every problem is different’ – Offensive Security’s Ning Wang on training the next generation of infosec pros
Date: 2021-02-19T15:51:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: in-security-every-problem-is-different-offensive-securitys-ning-wang-on-training-the-next-generation-of-infosec-pros

[Source](https://portswigger.net/daily-swig/in-security-every-problem-is-different-offensive-securitys-ning-wang-on-training-the-next-generation-of-infosec-pros){:target="_blank" rel="noopener"}

> Curious, creative problem-solvers required – and ‘the 10,000-hour rule does not skip security’ [...]
