Title: NSA, Microsoft promote a Zero Trust approach to cybersecurity
Date: 2021-02-27T12:03:44-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: nsa-microsoft-promote-a-zero-trust-approach-to-cybersecurity

[Source](https://www.bleepingcomputer.com/news/security/nsa-microsoft-promote-a-zero-trust-approach-to-cybersecurity/){:target="_blank" rel="noopener"}

> The National Security Agency (NSA) and Microsoft are advocating for the Zero Trust security model as a more efficient way for enterprises to defend against today's increasingly sophisticated threats. [...]
