Title: Fortinet fixes critical vulnerabilities in SSL VPN and web firewall
Date: 2021-02-07T09:31:22-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: fortinet-fixes-critical-vulnerabilities-in-ssl-vpn-and-web-firewall

[Source](https://www.bleepingcomputer.com/news/security/fortinet-fixes-critical-vulnerabilities-in-ssl-vpn-and-web-firewall/){:target="_blank" rel="noopener"}

> Fortinet has fixed multiple severe vulnerabilities impacting its products. The vulnerabilities range from Remote Code Execution to SQL Injection, to Denial of Service (DoS) and impact the FortiProxy SSL VPN and FortiWeb Web Application Firewall (WAF) products. [...]
