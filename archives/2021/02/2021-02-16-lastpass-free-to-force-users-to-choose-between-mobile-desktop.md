Title: LastPass Free to force users to choose between mobile, desktop
Date: 2021-02-16T10:57:08-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: lastpass-free-to-force-users-to-choose-between-mobile-desktop

[Source](https://www.bleepingcomputer.com/news/security/lastpass-free-to-force-users-to-choose-between-mobile-desktop/){:target="_blank" rel="noopener"}

> Starting next month, LastPass will no longer allow a free account to be used on multiple types of devices (computers and mobile) at the same time. [...]
