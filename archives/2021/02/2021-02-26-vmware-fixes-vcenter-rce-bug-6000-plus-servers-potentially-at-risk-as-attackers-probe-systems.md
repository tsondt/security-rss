Title: VMware fixes vCenter RCE bug – 6,000-plus servers potentially at risk as attackers probe systems
Date: 2021-02-26T13:45:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: vmware-fixes-vcenter-rce-bug-6000-plus-servers-potentially-at-risk-as-attackers-probe-systems

[Source](https://portswigger.net/daily-swig/vmware-fixes-vcenter-rce-bug-6-000-plus-servers-potentially-at-risk-as-attackers-probe-systems){:target="_blank" rel="noopener"}

> Public disclosure brought forward after exploits surface online [...]
