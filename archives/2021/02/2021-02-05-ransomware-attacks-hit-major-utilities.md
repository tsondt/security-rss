Title: Ransomware Attacks Hit Major Utilities
Date: 2021-02-05T15:17:53+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Malware
Slug: ransomware-attacks-hit-major-utilities

[Source](https://threatpost.com/ransomware-attacks-major-utilities/163687/){:target="_blank" rel="noopener"}

> Eletrobras, the largest power company in Latin America, faces a temporary suspension of some operations. [...]
