Title: Google Play Boots Barcode Scanner App After Ad Explosion
Date: 2021-02-09T22:31:16+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Mobile Security
Slug: google-play-boots-barcode-scanner-app-after-ad-explosion

[Source](https://threatpost.com/google-boots-barcode-scanner-app-ad-explosion/163803/){:target="_blank" rel="noopener"}

> A barcode scanner with 10 million downloads is removed from Google Play marketplace after ad blitz hits phones. [...]
