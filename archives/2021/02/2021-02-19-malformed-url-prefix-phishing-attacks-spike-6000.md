Title: Malformed URL Prefix Phishing Attacks Spike 6,000%
Date: 2021-02-19T21:06:32+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Most Recent ThreatLists;Web Security
Slug: malformed-url-prefix-phishing-attacks-spike-6000

[Source](https://threatpost.com/malformed-url-prefix-phishing-attacks-spike-6000/164132/){:target="_blank" rel="noopener"}

> Sneaky attackers are flipping backslashes in phishing email URLs to evade protections, researchers said. [...]
