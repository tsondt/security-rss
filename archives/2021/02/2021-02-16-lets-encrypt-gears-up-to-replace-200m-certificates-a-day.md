Title: Let’s Encrypt Gears Up to Replace 200M Certificates a Day
Date: 2021-02-16T21:47:30+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Cryptography;Web Security
Slug: lets-encrypt-gears-up-to-replace-200m-certificates-a-day

[Source](https://threatpost.com/lets-encrypt-gears-up-to-replace-200m-certificates-a-day/164002/){:target="_blank" rel="noopener"}

> The open CA prepares for ‘worst scenarios’ with new fiber, servers, cryptographic signing and more. [...]
