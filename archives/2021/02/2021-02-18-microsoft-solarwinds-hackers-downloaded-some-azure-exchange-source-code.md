Title: Microsoft: SolarWinds hackers downloaded some Azure, Exchange source code
Date: 2021-02-18T11:48:03-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-solarwinds-hackers-downloaded-some-azure-exchange-source-code

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-solarwinds-hackers-downloaded-some-azure-exchange-source-code/){:target="_blank" rel="noopener"}

> Microsoft announced today that the SolarWinds hackers could gain access to source code for a limited amount of components used by Azure, Intune, and Exchange. [...]
