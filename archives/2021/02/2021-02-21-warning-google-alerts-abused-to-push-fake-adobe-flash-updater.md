Title: Warning: Google Alerts abused to push fake Adobe Flash updater
Date: 2021-02-21T09:24:11-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: warning-google-alerts-abused-to-push-fake-adobe-flash-updater

[Source](https://www.bleepingcomputer.com/news/security/warning-google-alerts-abused-to-push-fake-adobe-flash-updater/){:target="_blank" rel="noopener"}

> Threat actors are using Google Alerts to promote a fake Adobe Flash Player updater that installs other unwanted programs on unsuspecting users' computers. [...]
