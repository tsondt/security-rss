Title: Alleged Gaming Software Supply-Chain Attack Installs Spyware
Date: 2021-02-01T16:50:24+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Privacy;Vulnerabilities
Slug: alleged-gaming-software-supply-chain-attack-installs-spyware

[Source](https://threatpost.com/gaming-software-attack-spyware/163537/){:target="_blank" rel="noopener"}

> Researchers allege that software used for downloading Android apps onto PCs and Macs has been compromised to install malware onto victim devices. [...]
