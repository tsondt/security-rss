Title: Data breach exposes 1.6 million Washington unemployment claims
Date: 2021-02-01T16:15:30-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: data-breach-exposes-16-million-washington-unemployment-claims

[Source](https://www.bleepingcomputer.com/news/security/data-breach-exposes-16-million-washington-unemployment-claims/){:target="_blank" rel="noopener"}

> Washington's State Auditor office has suffered a data breach that exposed the personal information in 1.6 million employment claims. [...]
