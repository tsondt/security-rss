Title: Details Tied to Safari Browser-based ‘ScamClub’ Campaign Revealed
Date: 2021-02-17T15:30:37+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: details-tied-to-safari-browser-based-scamclub-campaign-revealed

[Source](https://threatpost.com/safari-browser-scamclub-campaign-revealed/164023/){:target="_blank" rel="noopener"}

> Public disclosure of a privilege escalation attack details how a cybergang bypassed browser iframe sandboxing with malicious PostMessage popups. [...]
