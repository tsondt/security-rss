Title: US indicts North Korean hackers for stealing $1.3 billion
Date: 2021-02-17T12:21:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-indicts-north-korean-hackers-for-stealing-13-billion

[Source](https://www.bleepingcomputer.com/news/security/us-indicts-north-korean-hackers-for-stealing-13-billion/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice has charged three North Koreans for stealing $1.3 billion in money and cryptocurrency in attacks on banks, the entertainment industry, cryptocurrency companies, and more. [...]
