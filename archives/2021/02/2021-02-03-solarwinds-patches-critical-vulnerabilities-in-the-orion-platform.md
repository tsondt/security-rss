Title: SolarWinds patches critical vulnerabilities in the Orion platform
Date: 2021-02-03T06:19:32-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: solarwinds-patches-critical-vulnerabilities-in-the-orion-platform

[Source](https://www.bleepingcomputer.com/news/security/solarwinds-patches-critical-vulnerabilities-in-the-orion-platform/){:target="_blank" rel="noopener"}

> Even with the security updates prompted by the recent SolarWinds Orion supply-chain attack, researchers still found some glaring vulnerabilities affecting the platform, one of them allowing code execution with top privileges. [...]
