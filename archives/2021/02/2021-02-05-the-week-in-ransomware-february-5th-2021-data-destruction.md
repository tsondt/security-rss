Title: The Week in Ransomware - February 5th 2021 - Data destruction
Date: 2021-02-05T18:33:40-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-february-5th-2021-data-destruction

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-february-5th-2021-data-destruction/){:target="_blank" rel="noopener"}

> This week we saw a few large scale attacks and various ransomware reports indicating ransom payments are falling, while attacks are increasingly destroying data permanently. The good news is a new ransomware decryptor was released, allowing victims to recover files for free. [...]
