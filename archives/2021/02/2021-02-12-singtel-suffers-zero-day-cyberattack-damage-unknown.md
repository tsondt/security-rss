Title: Singtel Suffers Zero-Day Cyberattack, Damage Unknown
Date: 2021-02-12T17:05:10+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Vulnerabilities
Slug: singtel-suffers-zero-day-cyberattack-damage-unknown

[Source](https://threatpost.com/singtel-zero-day-cyberattack/163938/){:target="_blank" rel="noopener"}

> The Tier 1 telecom giant was caught up in a coordinated, wide-ranging attack using unpatched security bugs in the Accellion legacy file-transfer platform. [...]
