Title: Microsoft Office February security updates patch Sharepoint, Excel RCE bugs
Date: 2021-02-10T09:28:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-office-february-security-updates-patch-sharepoint-excel-rce-bugs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-office-february-security-updates-patch-sharepoint-excel-rce-bugs/){:target="_blank" rel="noopener"}

> Microsoft has addressed important severity remote code execution vulnerabilities affecting multiple Office products in the January 2021 Office security updates. [...]
