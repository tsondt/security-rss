Title: Billions of Passwords Offered for $2 in Cyber-Underground
Date: 2021-02-08T21:12:01+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Web Security
Slug: billions-of-passwords-offered-for-2-in-cyber-underground

[Source](https://threatpost.com/billions-passwords-cyber-underground/163738/){:target="_blank" rel="noopener"}

> About 3.27 billion stolen account logins have been posted to the RaidForums English-language cybercrime community in a 'COMB' collection. [...]
