Title: Friday Squid Blogging: Far Side Cartoon
Date: 2021-02-26T22:08:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-far-side-cartoon

[Source](https://www.schneier.com/blog/archives/2021/02/friday-squid-blogging-far-side-cartoon.html){:target="_blank" rel="noopener"}

> The Far Side on squid. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
