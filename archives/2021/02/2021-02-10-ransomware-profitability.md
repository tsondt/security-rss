Title: Ransomware Profitability
Date: 2021-02-10T13:39:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: crime;cryptocurrency;ransomware
Slug: ransomware-profitability

[Source](https://www.schneier.com/blog/archives/2021/02/ransomware-profitability.html){:target="_blank" rel="noopener"}

> Analyzing cryptocurrency data, a research group has estimated a lower-bound on 2020 ransomware revenue: $350 million, four times more than in 2019. Based on the company’s data, among last year’s top earners, there were groups like Ryuk, Maze (now-defunct), Doppelpaymer, Netwalker ( disrupted by authorities ), Conti, and REvil (aka Sodinokibi). Ransomware is now an established worldwide business. Slashdot thread. [...]
