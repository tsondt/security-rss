Title: New browser-tracking hack works even when you flush caches or go incognito
Date: 2021-02-19T12:54:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;browsers;favicons;Fingerprinting;privacy;tracking
Slug: new-browser-tracking-hack-works-even-when-you-flush-caches-or-go-incognito

[Source](https://arstechnica.com/?p=1743712){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) The prospect of Web users being tracked by the sites they visit has prompted several countermeasures over the years, including using Privacy Badger or an alternate anti-tracking extension, enabling private or incognito browsing sessions, or clearing cookies. Now, websites have a new way to defeat all three. The technique leverages the use of favicons, the tiny icons that websites display in users’ browser tabs and bookmark lists. Researchers from the University of Illinois, Chicago said in a new paper that most browsers cache the images in a location that’s separate from the ones used to store [...]
