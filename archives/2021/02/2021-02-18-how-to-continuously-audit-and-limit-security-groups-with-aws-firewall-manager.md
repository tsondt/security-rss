Title: How to continuously audit and limit security groups with AWS Firewall Manager
Date: 2021-02-18T21:50:03+00:00
Author: Jesse Lepich
Category: AWS Security
Tags: AWS Firewall Manager;Intermediate (200);Security, Identity, & Compliance;Security Blog;Security groups
Slug: how-to-continuously-audit-and-limit-security-groups-with-aws-firewall-manager

[Source](https://aws.amazon.com/blogs/security/how-to-continuously-audit-and-limit-security-groups-with-aws-firewall-manager/){:target="_blank" rel="noopener"}

> At AWS re:Invent 2019 and in a subsequent blog post, Stephen Schmidt, Chief Information Security Officer for Amazon Web Services (AWS), laid out the top 10 security items that AWS customers should pay special attention to if they want to improve their security posture. High on the list is the need to manage your network [...]
