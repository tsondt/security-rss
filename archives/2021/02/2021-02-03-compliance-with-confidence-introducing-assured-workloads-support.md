Title: Compliance with confidence: Introducing Assured Workloads Support
Date: 2021-02-03T17:00:00+00:00
Author: Atul Nanda
Category: GCP Security
Tags: Google Cloud Platform;Inside Google Cloud;Identity & Security
Slug: compliance-with-confidence-introducing-assured-workloads-support

[Source](https://cloud.google.com/blog/products/identity-security/introducing-assured-workloads-support/){:target="_blank" rel="noopener"}

> As organizations in regulated industries modernize and adopt cloud technologies, ensuring the security, privacy, and regulatory compliance of their sensitive workloads is an essential part of choosing a cloud provider. Regulated customers have specific compliance needs around data locality and personnel access to customer data. In the US specifically, these are mandated by requirements under the Department of Defense (i.e., IL4), the FBI’s Criminal Justice Information Services Division (CJIS), and the Federal Risk and Authorization Management Program (FedRAMP). Last year, we introduced Assured Workloads (now generally available with additional features in preview) which lets Google Cloud customers easily and quickly [...]
