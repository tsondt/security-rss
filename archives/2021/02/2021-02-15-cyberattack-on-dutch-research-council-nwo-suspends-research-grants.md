Title: Cyberattack on Dutch Research Council (NWO) suspends research grants
Date: 2021-02-15T13:50:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: cyberattack-on-dutch-research-council-nwo-suspends-research-grants

[Source](https://www.bleepingcomputer.com/news/security/cyberattack-on-dutch-research-council-nwo-suspends-research-grants/){:target="_blank" rel="noopener"}

> Servers belonging to the Dutch Research Council (NWO) have been compromised, forcing the organization to make its network unavailable and suspend subsidy allocation for the foreseeable future. [...]
