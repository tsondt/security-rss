Title: Wind River Security Incident Affects SSNs, Passport Numbers
Date: 2021-02-01T21:47:19+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Privacy
Slug: wind-river-security-incident-affects-ssns-passport-numbers

[Source](https://threatpost.com/wind-river-security-incident-ssns-passport-numbers/163550/){:target="_blank" rel="noopener"}

> Wind River Systems is warning of a 'security incident' after one or more files was downloaded from its network. [...]
