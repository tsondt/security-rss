Title: Google adds Password Checkup support to Android autofill
Date: 2021-02-23T12:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Mobile
Slug: google-adds-password-checkup-support-to-android-autofill

[Source](https://www.bleepingcomputer.com/news/security/google-adds-password-checkup-support-to-android-autofill/){:target="_blank" rel="noopener"}

> Google is adding support for the Password Checkup service to Android applications through the passwords autofill feature to warn users if their saved passwords have been compromised or leaked in data breaches. [...]
