Title: Uncle Sam accuses three suspected North Korean govt hackers of stealing $1.3bn+ from banks, crypto orgs
Date: 2021-02-17T22:22:20+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: uncle-sam-accuses-three-suspected-north-korean-govt-hackers-of-stealing-13bn-from-banks-crypto-orgs

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/17/north_korean_hackers/){:target="_blank" rel="noopener"}

> Oh yes, and hacking Hollywood, allegedly Three suspected North Korean military intelligence hackers have been charged with, among other things, conspiring to loot more than $1.3bn (£938m) from banks, ATMs, and cryptocurrency companies, according to an indictment unsealed by the US Department of Justice on Wednesday.... [...]
