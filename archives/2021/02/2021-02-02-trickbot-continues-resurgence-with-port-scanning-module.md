Title: TrickBot Continues Resurgence with Port-Scanning Module
Date: 2021-02-02T21:38:50+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware
Slug: trickbot-continues-resurgence-with-port-scanning-module

[Source](https://threatpost.com/trickbot-port-scanning-module/163615/){:target="_blank" rel="noopener"}

> The infamous malware has incorporated the legitimate Masscan tool, which looks for open TCP/IP ports with lightning-fast results. [...]
