Title: Chinese Hackers Hijacked NSA-Linked Hacking Tool: Report
Date: 2021-02-22T21:07:03+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Podcasts
Slug: chinese-hackers-hijacked-nsa-linked-hacking-tool-report

[Source](https://threatpost.com/chinese-hackers-hijacked-nsa-hacking-tool/164155/){:target="_blank" rel="noopener"}

> APT31, a Chinese-affiliated threat group, copied a Microsoft Windows exploit previously used by the Equation Group, said researchers. [...]
