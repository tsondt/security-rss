Title: They break into your network but do nothing themselves: 'Initial access brokers' resell stolen creds for $7k a pop
Date: 2021-02-23T22:53:14+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: they-break-into-your-network-but-do-nothing-themselves-initial-access-brokers-resell-stolen-creds-for-7k-a-pop

[Source](https://go.theregister.com/feed/www.theregister.com/2021/02/23/initial_access_brokers/){:target="_blank" rel="noopener"}

> So says Digital Shadows as it puts a price on illicit access methods A growing category of cyber-crime consists of breaking into corporate networks and doing nothing else – except selling that illicit access to others for about $7,000 a go, says infosec biz Digital Shadows.... [...]
