Title: US Cyber Command Valentine’s Day Cryptography Puzzles
Date: 2021-02-15T20:50:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cryptography;games
Slug: us-cyber-command-valentines-day-cryptography-puzzles

[Source](https://www.schneier.com/blog/archives/2021/02/us-cyber-command-valentines-day-cryptography-puzzles.html){:target="_blank" rel="noopener"}

> The US Cyber Command has released a series of ten Valentine’s Day “Cryptography Challenge Puzzles.” Slashdot thread. Reddit thread. (And here’s the archived link, in case Cyber Command takes the page down.) [...]
