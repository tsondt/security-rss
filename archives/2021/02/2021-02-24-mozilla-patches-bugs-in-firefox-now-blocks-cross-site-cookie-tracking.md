Title: Mozilla Patches Bugs in Firefox, Now Blocks Cross-Site Cookie Tracking
Date: 2021-02-24T20:50:29+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Privacy;Web Security
Slug: mozilla-patches-bugs-in-firefox-now-blocks-cross-site-cookie-tracking

[Source](https://threatpost.com/mozilla-firefox-bugs-cookie-tracking/164246/){:target="_blank" rel="noopener"}

> Mozilla said its Total Cookie Protection feature in Firefox 86 prevents invasive, cross-site cookie tracking. [...]
