Title: Ukraine: DDoS attacks on govt sites originated from Russia
Date: 2021-02-23T08:56:40-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ukraine-ddos-attacks-on-govt-sites-originated-from-russia

[Source](https://www.bleepingcomputer.com/news/security/ukraine-ddos-attacks-on-govt-sites-originated-from-russia/){:target="_blank" rel="noopener"}

> The National Security and Defense Council (NSDC) of Ukraine is accusing threat actors located on Russia networks of performing DDoS attacks on Ukrainian government websites since February 18th. [...]
