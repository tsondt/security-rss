Title: Critical WordPress Plugin Flaw Allows Site Takeover
Date: 2021-02-08T21:11:57+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: critical-wordpress-plugin-flaw-allows-site-takeover

[Source](https://threatpost.com/critical-wordpress-plugin-flaw-site-takeover/163734/){:target="_blank" rel="noopener"}

> A patch in the NextGen Gallery WordPress plugin fixes critical and high-severity cross-site request forgery flaws. [...]
