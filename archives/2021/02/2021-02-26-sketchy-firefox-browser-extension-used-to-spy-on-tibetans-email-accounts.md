Title: Sketchy Firefox browser extension used to spy on Tibetans’ email accounts
Date: 2021-02-26T15:36:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: sketchy-firefox-browser-extension-used-to-spy-on-tibetans-email-accounts

[Source](https://portswigger.net/daily-swig/sketchy-firefox-browser-extension-used-to-spy-on-tibetans-email-accounts){:target="_blank" rel="noopener"}

> ‘Malicious browser plugins aren’t new, but they are an often-forgotten attack surface in many enterprises’ [...]
