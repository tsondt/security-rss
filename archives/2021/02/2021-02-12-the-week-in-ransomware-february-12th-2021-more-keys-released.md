Title: The Week in Ransomware - February 12th 2021 - More keys released
Date: 2021-02-12T17:32:57-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-february-12th-2021-more-keys-released

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-february-12th-2021-more-keys-released/){:target="_blank" rel="noopener"}

> This week we saw another ransomware shut down its operation and a significant attack against Cyberpunk 2077 game developer CD Projekt Red. [...]
