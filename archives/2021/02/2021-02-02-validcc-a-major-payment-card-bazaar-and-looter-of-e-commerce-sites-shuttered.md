Title: ‘ValidCC,’ a Major Payment Card Bazaar and Looter of E-Commerce Sites, Shuttered
Date: 2021-02-02T18:04:46+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Web Fraud 2.0;Gemini Advisory;Group-IB;Media Land LLC;SPR;Stas Alforov;UltraRank;Validcc
Slug: validcc-a-major-payment-card-bazaar-and-looter-of-e-commerce-sites-shuttered

[Source](https://krebsonsecurity.com/2021/02/validcc-a-major-payment-card-bazaar-and-looter-of-e-commerce-sites-shuttered/){:target="_blank" rel="noopener"}

> ValidCC, a dark web bazaar run by a cybercrime group that for more than six years hacked online merchants and sold stolen payment card data, abruptly closed up shop last week. The proprietors of the popular store said their servers were seized as part of a coordinated law enforcement operation designed to disconnect and confiscate its infrastructure. ValidCC, circa 2017. There are dozens of online shops that sell so-called “card not present” (CNP) payment card data stolen from e-commerce stores, but most source the data from other criminals. In contrast, researchers say ValidCC was actively involved in hacking and pillaging [...]
