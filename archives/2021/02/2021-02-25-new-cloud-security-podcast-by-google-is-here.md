Title: New Cloud Security Podcast by Google is here
Date: 2021-02-25T18:30:00+00:00
Author: Timothy Peacock
Category: GCP Security
Tags: Google Cloud Platform;Inside Google Cloud;Identity & Security
Slug: new-cloud-security-podcast-by-google-is-here

[Source](https://cloud.google.com/blog/products/identity-security/introducing-google-clouds-new-cloud-security-podcast/){:target="_blank" rel="noopener"}

> Security continues to be top of mind for large enterprises as well as smaller organizations and businesses. Furthermore, cloud security continues to puzzle many security leaders and technologists. That is why we are excited to announce the launch of the Cloud Security Podcast by Google. This podcast will bring you stories and insights on security in the cloud, delivering security from the cloud, and, of course, on what we’re doing at Google Cloud to help keep customer data safe and workloads secure. We’ll do our best to avoid security theater (but perhaps have some reverse security theater ), and cut [...]
