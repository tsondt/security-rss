Title: Router Security
Date: 2021-02-19T12:00:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: hardware;Internet of Things;Linux;mitigation;patching;reports;vulnerabilities
Slug: router-security

[Source](https://www.schneier.com/blog/archives/2021/02/router-security.html){:target="_blank" rel="noopener"}

> This report is six months old, and I don’t know anything about the organization that produced it, but it has some alarming data about router security. Conclusion: Our analysis showed that Linux is the most used OS running on more than 90% of the devices. However, many routers are powered by very old versions of Linux. Most devices are still powered with a 2.6 Linux kernel, which is no longer maintained for many years. This leads to a high number of critical and high severity CVEs affecting these devices. Since Linux is the most used OS, exploit mitigation techniques could [...]
