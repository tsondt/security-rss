Title: Industrial Networks See Sharp Uptick in Hackable Security Holes
Date: 2021-02-05T22:21:56+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Most Recent ThreatLists;Vulnerabilities
Slug: industrial-networks-see-sharp-uptick-in-hackable-security-holes

[Source](https://threatpost.com/industrial-networks-hackable-security-holes/163708/){:target="_blank" rel="noopener"}

> Claroty reports that adversaries, CISOs and researchers have all turned their attention to finding critical security bugs in ICS networks. [...]
