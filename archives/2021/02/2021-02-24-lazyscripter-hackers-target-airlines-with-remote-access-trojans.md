Title: LazyScripter hackers target airlines with remote access trojans
Date: 2021-02-24T08:51:03-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: lazyscripter-hackers-target-airlines-with-remote-access-trojans

[Source](https://www.bleepingcomputer.com/news/security/lazyscripter-hackers-target-airlines-with-remote-access-trojans/){:target="_blank" rel="noopener"}

> Security researchers analyzing multiple sets of malicious emails believe they uncovered activity belonging to a previously unidentified actor that fits the description of an advanced persistent threat (APT). [...]
