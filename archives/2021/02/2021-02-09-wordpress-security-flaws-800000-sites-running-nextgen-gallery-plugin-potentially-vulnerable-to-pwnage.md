Title: WordPress security flaws: 800,000 sites running NextGen Gallery plugin potentially vulnerable to pwnage
Date: 2021-02-09T16:44:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: wordpress-security-flaws-800000-sites-running-nextgen-gallery-plugin-potentially-vulnerable-to-pwnage

[Source](https://portswigger.net/daily-swig/wordpress-security-flaws-800-000-sites-running-nextgen-gallery-plugin-potentially-vulnerable-to-pwnage){:target="_blank" rel="noopener"}

> Unpatched sites could get pwned – but admins must fall for social engineering [...]
