Title: The Week in Ransomware - February 26th 2021 -  Back from the Holidays
Date: 2021-02-26T18:44:49-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-february-26th-2021-back-from-the-holidays

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-february-26th-2021-back-from-the-holidays/){:target="_blank" rel="noopener"}

> The number of attacks had slowed down after the winter holidays, but after the past two weeks, it's evident that the ransomware attacks are back at full speed. [...]
