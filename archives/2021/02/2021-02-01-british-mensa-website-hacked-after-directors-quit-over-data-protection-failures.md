Title: British Mensa website hacked after directors quit over ‘data protection failures’
Date: 2021-02-01T13:13:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: british-mensa-website-hacked-after-directors-quit-over-data-protection-failures

[Source](https://portswigger.net/daily-swig/british-mensa-website-hacked-after-directors-quit-over-data-protection-failures){:target="_blank" rel="noopener"}

> High IQ members group suffers intrusion [...]
