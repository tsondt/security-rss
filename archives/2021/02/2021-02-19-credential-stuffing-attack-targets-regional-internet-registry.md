Title: Credential-Stuffing Attack Targets Regional Internet Registry
Date: 2021-02-19T19:32:59+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Web Security
Slug: credential-stuffing-attack-targets-regional-internet-registry

[Source](https://threatpost.com/credential-stuffing-attack-ripe-ncc/164109/){:target="_blank" rel="noopener"}

> RIPE NCC, the regional Internet registry for Europe, West Asia, and the former Soviet Union, said attackers attempted a credential-stuffing attack against its single-sign on service. [...]
