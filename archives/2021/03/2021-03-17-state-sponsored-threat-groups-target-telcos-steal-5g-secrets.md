Title: State-sponsored Threat Groups Target Telcos, Steal 5G Secrets
Date: 2021-03-17T15:08:43+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government
Slug: state-sponsored-threat-groups-target-telcos-steal-5g-secrets

[Source](https://threatpost.com/state-sponsored-threat-groups-target-telcos-steal-5g-secrets/164841/){:target="_blank" rel="noopener"}

> Researchers say China-linked APTs lure victims with bogus Huawei career pages in what they dub ‘Operation Diànxùn’. [...]
