Title: Cloudflare launches JavaScript dependency dashboard utility to warn against Magecart-style malfeasance
Date: 2021-03-25T16:28:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cloudflare-launches-javascript-dependency-dashboard-utility-to-warn-against-magecart-style-malfeasance

[Source](https://portswigger.net/daily-swig/cloudflare-launches-javascript-dependency-dashboard-utility-to-warn-against-magecart-style-malfeasance){:target="_blank" rel="noopener"}

> Script Monitor aims to skittle skimmers [...]
