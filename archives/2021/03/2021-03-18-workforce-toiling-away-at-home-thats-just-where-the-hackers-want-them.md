Title: Workforce toiling away at home? That’s just where the hackers want them
Date: 2021-03-18T07:30:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: workforce-toiling-away-at-home-thats-just-where-the-hackers-want-them

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/18/workforce_at_home_security/){:target="_blank" rel="noopener"}

> Here’s how to work out what your new security model should look like Webcast Upheaval always brings opportunity. And no one knows how to exploit the opportunity that upheaval brings the way cybercriminals do.... [...]
