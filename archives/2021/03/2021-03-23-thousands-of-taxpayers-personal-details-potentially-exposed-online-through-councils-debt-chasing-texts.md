Title: Thousands of taxpayers' personal details potentially exposed online through councils' debt-chasing texts
Date: 2021-03-23T11:08:00+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: thousands-of-taxpayers-personal-details-potentially-exposed-online-through-councils-debt-chasing-texts

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/23/council_tax_texts_exposure/){:target="_blank" rel="noopener"}

> Got a link? Change the last character and bingo, it's blackmail time Exclusive Bulk SMS messages sent by local councils across the UK contained weblinks leading to pages that freely exposed to the public thousands of taxpayers' names, addresses, and outstanding debts, The Register can reveal.... [...]
