Title: Microsoft Patch Tuesday Updates Fix 14 Critical Bugs
Date: 2021-03-09T22:12:56+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security
Slug: microsoft-patch-tuesday-updates-fix-14-critical-bugs

[Source](https://threatpost.com/microsoft-patch-tuesday-updates-critical-bugs/164621/){:target="_blank" rel="noopener"}

> Microsoft's regularly scheduled March Patch Tuesday updates address 89 CVEs overall. [...]
