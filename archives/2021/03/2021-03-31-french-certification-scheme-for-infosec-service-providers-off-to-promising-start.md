Title: French certification scheme for infosec service providers off to promising start
Date: 2021-03-31T13:03:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: french-certification-scheme-for-infosec-service-providers-off-to-promising-start

[Source](https://portswigger.net/daily-swig/french-certification-scheme-for-infosec-service-providers-off-to-promising-start){:target="_blank" rel="noopener"}

> Dozens of companies already ExpertCyber-certified, just six weeks after initiative’s launch [...]
