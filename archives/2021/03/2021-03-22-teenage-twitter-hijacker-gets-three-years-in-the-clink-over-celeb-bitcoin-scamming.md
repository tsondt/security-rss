Title: Teenage Twitter hijacker gets three years in the clink over celeb Bitcoin scamming
Date: 2021-03-22T11:03:40+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: teenage-twitter-hijacker-gets-three-years-in-the-clink-over-celeb-bitcoin-scamming

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/22/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Exchange and SolarWinds autopatch tools and shocking news! In Brief Graham Ivan Clark, part of the crew that hijacked around 130 high-profile Twitter accounts and used them to collect cryptocurrency, has been sentenced to three years in prison for his part in the scam.... [...]
