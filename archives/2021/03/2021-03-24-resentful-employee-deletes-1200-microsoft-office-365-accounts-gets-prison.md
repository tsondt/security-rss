Title: Resentful employee deletes 1,200 Microsoft Office 365 accounts, gets prison
Date: 2021-03-24T03:23:32-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: resentful-employee-deletes-1200-microsoft-office-365-accounts-gets-prison

[Source](https://www.bleepingcomputer.com/news/security/resentful-employee-deletes-1-200-microsoft-office-365-accounts-gets-prison/){:target="_blank" rel="noopener"}

> A former IT consultant hacked a company in Carlsbad, California, and deleted almost all its Microsoft Office 365 accounts in an act of revenge that has brought him two years of prison time. [...]
