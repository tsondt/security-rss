Title: NSW Transport agency extorted by ransomware gang after Accellion attack
Date: 2021-03-01T11:43:07-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Government
Slug: nsw-transport-agency-extorted-by-ransomware-gang-after-accellion-attack

[Source](https://www.bleepingcomputer.com/news/security/nsw-transport-agency-extorted-by-ransomware-gang-after-accellion-attack/){:target="_blank" rel="noopener"}

> The transport system for the Australian state of New South Wales has suffered a data breach after the Clop ransomware exploited a vulnerability to steal files. [...]
