Title: Indian payment provider MobiKwik denies 8.2 TB data breach
Date: 2021-03-31T09:12:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: indian-payment-provider-mobikwik-denies-82-tb-data-breach

[Source](https://portswigger.net/daily-swig/indian-payment-provider-mobikwik-denies-8-2-tb-data-breach){:target="_blank" rel="noopener"}

> Fintech firm refutes breach reports, claims customer information is safe [...]
