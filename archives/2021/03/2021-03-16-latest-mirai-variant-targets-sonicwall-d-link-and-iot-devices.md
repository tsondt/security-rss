Title: Latest Mirai Variant Targets SonicWall, D-Link and IoT Devices
Date: 2021-03-16T16:57:46+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: IoT;Malware;Vulnerabilities
Slug: latest-mirai-variant-targets-sonicwall-d-link-and-iot-devices

[Source](https://threatpost.com/mirai-variant-sonicwall-d-link-iot/164811/){:target="_blank" rel="noopener"}

> A new Mirai variant is targeting known flaws in D-Link, Netgear and SonicWall devices, as well as newly-discovered flaws in unknown IoT devices. [...]
