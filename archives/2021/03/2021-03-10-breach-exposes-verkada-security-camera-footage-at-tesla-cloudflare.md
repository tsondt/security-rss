Title: Breach Exposes Verkada Security Camera Footage at Tesla, Cloudflare
Date: 2021-03-10T14:44:05+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: IoT;Vulnerabilities;Web Security
Slug: breach-exposes-verkada-security-camera-footage-at-tesla-cloudflare

[Source](https://threatpost.com/breach-verkada-security-camera-tesla-cloudflare/164635/){:target="_blank" rel="noopener"}

> Surveillance footage from companies such as Tesla as well as hospitals, prisons, police departments and schools was accessed in the hack. [...]
