Title: More than half of women in infosec are treated unequally to male colleagues due to ‘blatant sexism’ in the workplace – report
Date: 2021-03-04T14:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: more-than-half-of-women-in-infosec-are-treated-unequally-to-male-colleagues-due-to-blatant-sexism-in-the-workplace-report

[Source](https://portswigger.net/daily-swig/more-than-half-of-women-in-infosec-are-treated-unequally-to-male-colleagues-due-to-blatant-sexism-in-the-workplace-report){:target="_blank" rel="noopener"}

> Study finds unconscious bias and ‘micro-aggressions’ are holding female industry workers back [...]
