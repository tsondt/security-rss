Title: 800Gbps DDoS extortion attack hits gambling company
Date: 2021-03-31T17:31:30-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: 800gbps-ddos-extortion-attack-hits-gambling-company

[Source](https://www.bleepingcomputer.com/news/security/800gbps-ddos-extortion-attack-hits-gambling-company/){:target="_blank" rel="noopener"}

> Distributed denial-of-service (DDoS) attacks started strong this year, setting new records and taking the extortion trend that started last August to the next level. [...]
