Title: OVH data center fire likely caused by faulty UPS power supply
Date: 2021-03-12T02:45:26-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Cloud;Technology
Slug: ovh-data-center-fire-likely-caused-by-faulty-ups-power-supply

[Source](https://www.bleepingcomputer.com/news/security/ovh-data-center-fire-likely-caused-by-faulty-ups-power-supply/){:target="_blank" rel="noopener"}

> OVH founder and chairman Octave Klaba has provided a plausible explanation for the fire that burned down OVH data centers in Strasbourg, France. [...]
