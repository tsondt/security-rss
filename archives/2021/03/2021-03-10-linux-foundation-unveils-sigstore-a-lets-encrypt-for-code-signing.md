Title: Linux Foundation unveils Sigstore — a Let's Encrypt for code signing
Date: 2021-03-10T15:49:33-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software;Google
Slug: linux-foundation-unveils-sigstore-a-lets-encrypt-for-code-signing

[Source](https://www.bleepingcomputer.com/news/software/linux-foundation-unveils-sigstore-a-lets-encrypt-for-code-signing/){:target="_blank" rel="noopener"}

> The Linux Foundation, Red Hat, Google, and Purdue have unveiled the free 'sigstore' service that lets developers code-sign and verify open source software to prevent supply-chain attacks. [...]
