Title: Manufacturing’s Cloud Migration Opens Door to Major Cyber-Risk
Date: 2021-03-25T17:11:28+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Mobile Security;Vulnerabilities;Web Security
Slug: manufacturings-cloud-migration-opens-door-to-major-cyber-risk

[Source](https://threatpost.com/manufacturing-cloud-migration-cyber-risk/165028/){:target="_blank" rel="noopener"}

> New research shows that while all sectors are at risk, 70 percent of manufacturing apps have vulnerabilities. [...]
