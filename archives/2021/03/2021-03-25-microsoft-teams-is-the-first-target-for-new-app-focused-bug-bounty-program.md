Title: Microsoft Teams is the first target for new app-focused bug bounty program
Date: 2021-03-25T13:03:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: microsoft-teams-is-the-first-target-for-new-app-focused-bug-bounty-program

[Source](https://portswigger.net/daily-swig/microsoft-teams-is-the-first-target-for-new-app-focused-bug-bounty-program){:target="_blank" rel="noopener"}

> Payment ceiling for Microsoft Applications Bounty Program is $10k higher than online services counterpart [...]
