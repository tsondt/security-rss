Title: C5 Type 2 attestation report now available with one new Region and 123 services in scope
Date: 2021-03-03T16:11:55+00:00
Author: Mercy Kanengoni
Category: AWS Security
Tags: Announcements;Europe;Foundational (100);Security, Identity, & Compliance;Auditing;C5;Certification;Germany;Security Blog
Slug: c5-type-2-attestation-report-now-available-with-one-new-region-and-123-services-in-scope

[Source](https://aws.amazon.com/blogs/security/c5-type-2-attestation-report-available-one-new-region-123-services-in-scope/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the issuance of the 2020 Cloud Computing Compliance Controls Catalogue (C5) Type 2 attestation report. We added one new AWS Region (Europe-Milan) and 21 additional services and service features to the scope of the 2020 report. Germany’s national cybersecurity authority, Bundesamt für Sicherheit in der Informationstechnik (BSI), [...]
