Title: Microsoft promises end-to-end encrypted Teams calls for some, invites you to go passwordless with Azure AD
Date: 2021-03-03T07:24:06+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: microsoft-promises-end-to-end-encrypted-teams-calls-for-some-invites-you-to-go-passwordless-with-azure-ad

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/03/microsoft_ups_security/){:target="_blank" rel="noopener"}

> When there are passphrases, there is inherent risk, says Redmond Ignite Microsoft has said it will add end-to-end encryption for some one-to-one Teams calls later this year – and urged folks to move away from using passwords with Azure AD.... [...]
