Title: Google awards Uruguayan researcher $133,337 top prize in cloud security competition
Date: 2021-03-18T16:29:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-awards-uruguayan-researcher-133337-top-prize-in-cloud-security-competition

[Source](https://portswigger.net/daily-swig/google-awards-uruguayan-researcher-133-337-top-prize-in-cloud-security-competition){:target="_blank" rel="noopener"}

> Six winning write-ups ranged from identity management issues and privilege escalation to ‘full blown RCE’ [...]
