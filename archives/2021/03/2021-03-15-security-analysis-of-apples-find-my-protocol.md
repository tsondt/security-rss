Title: Security Analysis of Apple’s “Find My…” Protocol
Date: 2021-03-15T11:16:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;Apple;Bluetooth;crowdsourcing;de-anonymization;privacy;reverse engineering;security analysis;tracking
Slug: security-analysis-of-apples-find-my-protocol

[Source](https://www.schneier.com/blog/archives/2021/03/security-analysis-of-apples-find-my-protocol.html){:target="_blank" rel="noopener"}

> Interesting research: “ Who Can Find My Devices? Security and Privacy of Apple’s Crowd-Sourced Bluetooth Location Tracking System “: Abstract: Overnight, Apple has turned its hundreds-of-million-device ecosystem into the world’s largest crowd-sourced location tracking network called offline finding (OF). OF leverages online finder devices to detect the presence of missing offline devices using Bluetooth and report an approximate location back to the owner via the Internet. While OF is not the first system of its kind, it is the first to commit to strong privacy goals. In particular, OF aims to ensure finder anonymity, untrackability of owner devices, and confidentiality [...]
