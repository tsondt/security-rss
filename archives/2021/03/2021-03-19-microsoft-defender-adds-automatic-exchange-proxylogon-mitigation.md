Title: Microsoft Defender adds automatic Exchange ProxyLogon mitigation
Date: 2021-03-19T07:40:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-adds-automatic-exchange-proxylogon-mitigation

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-adds-automatic-exchange-proxylogon-mitigation/){:target="_blank" rel="noopener"}

> Microsoft Defender Antivirus will now protect unpatched on-premises Exchange servers from ongoing attacks by automatically mitigating the actively exploited CVE-2021-26855 vulnerability. [...]
