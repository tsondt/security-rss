Title: Fastway data breach: Security incident at Irish courier impacts more than 440,000 parcel recipients
Date: 2021-03-16T14:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: fastway-data-breach-security-incident-at-irish-courier-impacts-more-than-440000-parcel-recipients

[Source](https://portswigger.net/daily-swig/fastway-data-breach-security-incident-at-irish-courier-impacts-more-than-440-000-parcel-recipients){:target="_blank" rel="noopener"}

> Cyber-attack compromises delivery data [...]
