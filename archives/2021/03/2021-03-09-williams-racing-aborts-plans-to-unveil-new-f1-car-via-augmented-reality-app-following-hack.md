Title: Williams Racing aborts plans to unveil new F1 car via augmented reality app following hack
Date: 2021-03-09T14:02:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: williams-racing-aborts-plans-to-unveil-new-f1-car-via-augmented-reality-app-following-hack

[Source](https://portswigger.net/daily-swig/williams-racing-aborts-plans-to-unveil-new-f1-car-via-augmented-reality-app-following-hack){:target="_blank" rel="noopener"}

> Mobile app fails to get off starting grid [...]
