Title: What could possibly go wrong? Sublet your home broadband to strangers who totally won't commit crimes
Date: 2021-03-19T18:01:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: what-could-possibly-go-wrong-sublet-your-home-broadband-to-strangers-who-totally-wont-commit-crimes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/19/reselling_home_broadband/){:target="_blank" rel="noopener"}

> Money for nothing but your nicked IP In-depth The latest passive income trend, we're told by Lithuania-based internet biz IPRoyal, is internet sharing, a term that here means "subletting" or "reselling."... [...]
