Title: Magecart Attackers Save Stolen Credit-Card Data in .JPG File
Date: 2021-03-16T16:40:44+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: magecart-attackers-save-stolen-credit-card-data-in-jpg-file

[Source](https://threatpost.com/magecart-attackers-stolen-data-jpg/164815/){:target="_blank" rel="noopener"}

> Researchers from Sucuri discovered the tactic, which creatively hides malicious activity until the info can be retrieved, during an investigation into a compromised Magento 2 e-commerce site. [...]
