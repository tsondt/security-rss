Title: Microsoft Office 365 gets protection against malicious XLM macros
Date: 2021-03-07T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-office-365-gets-protection-against-malicious-xlm-macros

[Source](https://www.bleepingcomputer.com/news/security/microsoft-office-365-gets-protection-against-malicious-xlm-macros/){:target="_blank" rel="noopener"}

> Microsoft has added XLM macro protection for Microsoft 365 customers by expanding the runtime defense provided by Office 365's integration with Antimalware Scan Interface (AMSI) to include Excel 4.0 (XLM) macro scanning. [...]
