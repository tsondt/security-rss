Title: Fiserv Forgets to Buy Domain It Used as System Default
Date: 2021-03-18T20:15:37+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: fiserv-forgets-to-buy-domain-it-used-as-system-default

[Source](https://threatpost.com/fiserv-forgets-to-buy-domain-it-used-as-system-default/164903/){:target="_blank" rel="noopener"}

> Fintech security provider Fiserv acknowledges it used unregistered domain as default email. [...]
