Title: Belgian cops crack down on encrypted phone network Sky ECC in 200 overnight raids as firm denies criminal ties
Date: 2021-03-10T14:48:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: belgian-cops-crack-down-on-encrypted-phone-network-sky-ecc-in-200-overnight-raids-as-firm-denies-criminal-ties

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/10/sky_ecc_encrypted_phones_belgium_police_raids/){:target="_blank" rel="noopener"}

> Shades of the Encrochat bust all over again A series of police raids in Belgium have resulted in the apparent shutdown of the Sky ECC encrypted mobile phone network.... [...]
