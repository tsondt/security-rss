Title: Cisco addresses critical bug in Windows, macOS Jabber clients
Date: 2021-03-24T14:08:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisco-addresses-critical-bug-in-windows-macos-jabber-clients

[Source](https://www.bleepingcomputer.com/news/security/cisco-addresses-critical-bug-in-windows-macos-jabber-clients/){:target="_blank" rel="noopener"}

> Cisco has addressed a critical arbitrary program execution vulnerability impacting several Cisco Jabber client software for Windows, macOS, Android, and iOS. [...]
