Title: Podcast: Microsoft Exchange Server Attack Onslaught Continues
Date: 2021-03-23T16:39:43+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Podcasts;Vulnerabilities
Slug: podcast-microsoft-exchange-server-attack-onslaught-continues

[Source](https://threatpost.com/podcast-microsoft-exchange-server-attack-onslaught-continues/164968/){:target="_blank" rel="noopener"}

> Derek Manky, Chief of Security Insights & Global Threat Alliances at Fortinet’s FortiGuard Labs, gives insight into the surge in attacks against vulnerable Microsoft Exchange servers over the last week. [...]
