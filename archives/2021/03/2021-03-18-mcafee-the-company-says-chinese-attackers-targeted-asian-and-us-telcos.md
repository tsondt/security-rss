Title: McAfee, the company, says Chinese attackers targeted Asian and US telcos
Date: 2021-03-18T06:58:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: mcafee-the-company-says-chinese-attackers-targeted-asian-and-us-telcos

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/18/operation_dianxun/){:target="_blank" rel="noopener"}

> Fake Huawei and Flash sites helped steal info about 5G tech Security vendor McAfee has detected an attack it believes was likely aimed at telecoms companies in the hope of stealing information related to 5G networks.... [...]
