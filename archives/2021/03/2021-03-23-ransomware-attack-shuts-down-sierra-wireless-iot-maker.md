Title: Ransomware attack shuts down Sierra Wireless IoT maker
Date: 2021-03-23T11:39:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ransomware-attack-shuts-down-sierra-wireless-iot-maker

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-shuts-down-sierra-wireless-iot-maker/){:target="_blank" rel="noopener"}

> Sierra Wireless, a world-leading IoT solutions provider, today disclosed a ransomware attack that forced it to halt production at all manufacturing sites. [...]
