Title: Airline data hack: hundreds of thousands of Star Alliance passengers' details stolen
Date: 2021-03-05T06:59:49+00:00
Author: Martin Farrer
Category: The Guardian
Tags: Air transport;Data and computer security;Airline industry
Slug: airline-data-hack-hundreds-of-thousands-of-star-alliance-passengers-details-stolen

[Source](https://www.theguardian.com/world/2021/mar/05/airline-data-hack-hundreds-of-thousands-of-star-alliance-passengers-details-stolen){:target="_blank" rel="noopener"}

> IT operator Sita, which serves airlines including Singapore, Lufthansa and United, reports systems breach revealing frequent flyer data Data on hundreds of thousands of airline passengers around the world has been hacked via a “highly sophisticated” attack on the IT systems operator that serves around 90% of the global aviation industry. Sita, which serves the Star Alliance of airlines including Singapore Airlines, Lufthansa and United, said on Thursday it had been the victim of a cyber attack leading to a breach of passenger data held on its servers. Related: Airbus reveals planes sold in last two years will emit over [...]
