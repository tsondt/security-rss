Title: Tens of thousands of US organizations hit in ongoing Microsoft Exchange hack
Date: 2021-03-06T22:50:07+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;Exchange Server;exploits;malware;microsoft;vulnerabilities
Slug: tens-of-thousands-of-us-organizations-hit-in-ongoing-microsoft-exchange-hack

[Source](https://arstechnica.com/?p=1747745){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Tens of thousands of US-based organizations are running Microsoft Exchange servers that have been backdoored by threat actors who are stealing administrator passwords and exploiting critical vulnerabilities in the email and calendaring application, it was widely reported. Microsoft issued emergency patches on Tuesday, but they do nothing to disinfect systems that are already compromised. KrebsOnSecurity was the first to report the mass hack. Citing multiple unnamed people, reporter Brian Krebs put the number of compromised US organizations at at least 30,000. Worldwide, Krebs said there were at least 100,000 hacked organizations. Other news outlets, also [...]
