Title: Healthcare data breach fears raised as US orthopedic clinic admits leaving patient records on insecure server
Date: 2021-03-23T15:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: healthcare-data-breach-fears-raised-as-us-orthopedic-clinic-admits-leaving-patient-records-on-insecure-server

[Source](https://portswigger.net/daily-swig/healthcare-data-breach-fears-raised-as-us-orthopedic-clinic-admits-leaving-patient-records-on-insecure-server){:target="_blank" rel="noopener"}

> Mendelson Kornblum is alerting nearly 30,000 patients to a potential security incident [...]
