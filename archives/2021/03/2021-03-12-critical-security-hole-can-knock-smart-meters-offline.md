Title: Critical Security Hole Can Knock Smart Meters Offline
Date: 2021-03-12T21:42:59+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;IoT;Vulnerabilities
Slug: critical-security-hole-can-knock-smart-meters-offline

[Source](https://threatpost.com/critical-security-smart-meter-offline/164753/){:target="_blank" rel="noopener"}

> Unpatched Schneider Electric PowerLogic ION/PM smart meters are open to dangerous attacks. [...]
