Title: Malicious Code Bombs Target Amazon, Lyft, Slack, Zillow
Date: 2021-03-03T19:12:17+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Mobile Security;Vulnerabilities;Web Security
Slug: malicious-code-bombs-target-amazon-lyft-slack-zillow

[Source](https://threatpost.com/malicious-code-bombs-amazon-lyft-slack-zillow/164455/){:target="_blank" rel="noopener"}

> Attackers have weaponized code dependency confusion to target internal apps at tech giants. [...]
