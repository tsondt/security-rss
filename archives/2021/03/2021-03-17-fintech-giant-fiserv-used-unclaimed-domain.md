Title: Fintech Giant Fiserv Used Unclaimed Domain
Date: 2021-03-17T14:26:57+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Abraham Vegh;CashEdge;Credit One Bank;defaultinstitution.com;fiserv;Netspend;Popmoney;TCF National Bank;Union Bank;Zelle
Slug: fintech-giant-fiserv-used-unclaimed-domain

[Source](https://krebsonsecurity.com/2021/03/fintech-giant-fiserv-used-unclaimed-domain/){:target="_blank" rel="noopener"}

> If you sell Web-based software for a living and ship code that references an unregistered domain name, you are asking for trouble. But when the same mistake is made by a Fortune 500 company, the results can range from costly to disastrous. Here’s the story of one such goof committed by Fiserv [ NASDAQ:FISV ], a $15 billion firm that provides online banking software and other technology solutions to thousands of financial institutions. In November 2020, KrebsOnSecurity heard from security researcher Abraham Vegh, who noticed something odd while inspecting an email from his financial institution. Vegh could see the message [...]
