Title: Cybersecurity Bug-Hunting Sparks Enterprise Confidence
Date: 2021-03-15T20:11:20+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Bug Bounty;Most Recent ThreatLists;Vulnerabilities
Slug: cybersecurity-bug-hunting-sparks-enterprise-confidence

[Source](https://threatpost.com/cybersecurity-bug-hunting-enterprise-confidence/164782/){:target="_blank" rel="noopener"}

> A survey from Intel shows that most organizations prefer tech providers to have proactive security, but few meet security expectations. [...]
