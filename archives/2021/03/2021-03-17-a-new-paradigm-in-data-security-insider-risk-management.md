Title: A New Paradigm in Data Security: Insider Risk Management
Date: 2021-03-17T13:00:28+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Web Security
Slug: a-new-paradigm-in-data-security-insider-risk-management

[Source](https://threatpost.com/a-new-paradigm-in-data-security-insider-risk-management/164768/){:target="_blank" rel="noopener"}

> Insider Risk Management builds a framework around the new paradigm of "risk tolerance," aiming to give security teams the visibility and context around data activity to protect that data, without putting rigid constraints on users. [...]
