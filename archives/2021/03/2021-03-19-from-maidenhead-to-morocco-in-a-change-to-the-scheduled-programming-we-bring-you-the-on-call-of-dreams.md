Title: From Maidenhead to Morocco: In a change to the scheduled programming, we bring you The On Call of Dreams
Date: 2021-03-19T08:15:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: from-maidenhead-to-morocco-in-a-change-to-the-scheduled-programming-we-bring-you-the-on-call-of-dreams

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/19/on_call/){:target="_blank" rel="noopener"}

> Here's looking at you, kid On Call It's Friday! Pour yourself a beverage, break out the end-of-week treats, and enjoy a reader's tale of international intrigue and derring-do that began with an innocent stint On Call.... [...]
