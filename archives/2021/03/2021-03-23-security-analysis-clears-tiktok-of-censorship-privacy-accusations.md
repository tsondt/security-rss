Title: Security Analysis Clears TikTok of Censorship, Privacy Accusations
Date: 2021-03-23T20:27:24+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Hacks;Malware;Mobile Security;Privacy;Web Security
Slug: security-analysis-clears-tiktok-of-censorship-privacy-accusations

[Source](https://threatpost.com/security-analysis-tiktok-censorship-privacy/164990/){:target="_blank" rel="noopener"}

> TikTok’s source code is in line with industry standards, security researchers say. [...]
