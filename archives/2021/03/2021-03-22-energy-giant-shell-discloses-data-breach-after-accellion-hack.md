Title: Energy giant Shell discloses data breach after Accellion hack
Date: 2021-03-22T10:58:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: energy-giant-shell-discloses-data-breach-after-accellion-hack

[Source](https://www.bleepingcomputer.com/news/security/energy-giant-shell-discloses-data-breach-after-accellion-hack/){:target="_blank" rel="noopener"}

> Energy giant Shell has disclosed a data breach after attackers compromised the company's secure file-sharing system powered by Accellion's File Transfer Appliance (FTA). [...]
