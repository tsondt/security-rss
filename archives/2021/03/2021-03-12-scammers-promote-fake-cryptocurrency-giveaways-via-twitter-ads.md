Title: Scammers promote fake cryptocurrency giveaways via Twitter ads
Date: 2021-03-12T14:57:57-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: scammers-promote-fake-cryptocurrency-giveaways-via-twitter-ads

[Source](https://www.bleepingcomputer.com/news/security/scammers-promote-fake-cryptocurrency-giveaways-via-twitter-ads/){:target="_blank" rel="noopener"}

> Threat actors have started to use 'Promoted' tweets, otherwise known as Twitter ads, to spread cryptocurrency giveaway scams. [...]
