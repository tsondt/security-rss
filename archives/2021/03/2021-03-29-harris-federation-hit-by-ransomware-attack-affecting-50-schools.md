Title: Harris Federation hit by ransomware attack affecting 50 schools
Date: 2021-03-29T14:00:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: harris-federation-hit-by-ransomware-attack-affecting-50-schools

[Source](https://www.bleepingcomputer.com/news/security/harris-federation-hit-by-ransomware-attack-affecting-50-schools/){:target="_blank" rel="noopener"}

> The IT systems and email servers of London-based nonprofit multi-academy trust Harris Federation were taken down by a ransomware attack on Saturday. [...]
