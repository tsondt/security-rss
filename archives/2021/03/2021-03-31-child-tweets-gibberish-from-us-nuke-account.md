Title: Child Tweets Gibberish from U.S. Nuke Account
Date: 2021-03-31T18:22:48+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: child-tweets-gibberish-from-us-nuke-account

[Source](https://threatpost.com/child-tweets-gibberish-nuke-account/165140/){:target="_blank" rel="noopener"}

> Telecommuting social-media manager for the U.S. Strategic Command left the laptop open and unsecured while stepping away. [...]
