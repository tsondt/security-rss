Title: Malaysia Airlines discloses a nine-year-long data breach
Date: 2021-03-02T13:13:36-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: malaysia-airlines-discloses-a-nine-year-long-data-breach

[Source](https://www.bleepingcomputer.com/news/security/malaysia-airlines-discloses-a-nine-year-long-data-breach/){:target="_blank" rel="noopener"}

> ​Malaysia Airlines has suffered a data breach spanning nine years that exposed the personal information of members in its Enrich frequent flyer program. [...]
