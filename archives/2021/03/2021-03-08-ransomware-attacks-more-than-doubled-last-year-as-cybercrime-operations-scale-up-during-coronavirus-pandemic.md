Title: Ransomware attacks more than doubled last year as cybercrime operations scale up during coronavirus pandemic
Date: 2021-03-08T14:28:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ransomware-attacks-more-than-doubled-last-year-as-cybercrime-operations-scale-up-during-coronavirus-pandemic

[Source](https://portswigger.net/daily-swig/ransomware-attacks-more-than-doubled-last-year-as-cybercrime-operations-scale-up-during-coronavirus-pandemic){:target="_blank" rel="noopener"}

> Sobering news for organizations, as average ransom demand closes in on $200,000 [...]
