Title: Chained vulnerabilities used to take control of MyBB forums
Date: 2021-03-18T17:14:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: chained-vulnerabilities-used-to-take-control-of-mybb-forums

[Source](https://portswigger.net/daily-swig/chained-vulnerabilities-used-to-take-control-of-mybb-forums){:target="_blank" rel="noopener"}

> Esoteric hacking risk addressed through recent update [...]
