Title: More hacking groups join Microsoft Exchange attack frenzy
Date: 2021-03-10T09:42:20-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: more-hacking-groups-join-microsoft-exchange-attack-frenzy

[Source](https://www.bleepingcomputer.com/news/security/more-hacking-groups-join-microsoft-exchange-attack-frenzy/){:target="_blank" rel="noopener"}

> More state-sponsored hacking groups have joined the ongoing attacks targeting tens of thousands of on-premises Exchange servers impacted by severe vulnerabilities tracked as ProxyLogon. [...]
