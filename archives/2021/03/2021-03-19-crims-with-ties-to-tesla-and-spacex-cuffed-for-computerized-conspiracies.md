Title: Crims with ties to Tesla and SpaceX cuffed for computerized conspiracies
Date: 2021-03-19T06:38:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: crims-with-ties-to-tesla-and-spacex-cuffed-for-computerized-conspiracies

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/19/musk_crimes/){:target="_blank" rel="noopener"}

> Russian chap tried to crack Tesla security, SpaceX engineer traded dodgy info on dark web stock tipping forums The US Department of Justice (DOJ) has revealed that two sets of crooks have confessed to conspiracies against companies led by Elon Musk.... [...]
