Title: Teen hacker agrees to 3 years in prison for Twitter Bitcoin scam
Date: 2021-03-16T17:52:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: teen-hacker-agrees-to-3-years-in-prison-for-twitter-bitcoin-scam

[Source](https://www.bleepingcomputer.com/news/security/teen-hacker-agrees-to-3-years-in-prison-for-twitter-bitcoin-scam/){:target="_blank" rel="noopener"}

> A Florida teenager has pleaded guilty to fraud charges after coordinating the hack of high-profile Twitter accounts to run a cryptocurrency scam that collected roughly $120,000 worth of bitcoins. [...]
