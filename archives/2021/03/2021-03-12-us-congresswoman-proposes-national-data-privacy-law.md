Title: US Congresswoman proposes national data privacy law
Date: 2021-03-12T16:20:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-congresswoman-proposes-national-data-privacy-law

[Source](https://portswigger.net/daily-swig/us-congresswoman-proposes-national-data-privacy-law){:target="_blank" rel="noopener"}

> Bill would enact a unified federal law governing the use of citizens’ personal information [...]
