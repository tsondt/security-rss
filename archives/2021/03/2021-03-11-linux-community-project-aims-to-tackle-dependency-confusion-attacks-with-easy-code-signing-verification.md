Title: Linux community project aims to tackle dependency confusion attacks with easy code signing, verification
Date: 2021-03-11T16:40:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: linux-community-project-aims-to-tackle-dependency-confusion-attacks-with-easy-code-signing-verification

[Source](https://portswigger.net/daily-swig/linux-community-project-aims-to-tackle-dependency-confusion-attacks-with-easy-code-signing-verification){:target="_blank" rel="noopener"}

> Sigstore: a Let’s Encrypt for software integrity [...]
