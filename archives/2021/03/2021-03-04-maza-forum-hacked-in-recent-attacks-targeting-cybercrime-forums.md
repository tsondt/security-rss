Title: Maza forum hacked in recent attacks targeting cybercrime forums
Date: 2021-03-04T13:34:15-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: maza-forum-hacked-in-recent-attacks-targeting-cybercrime-forums

[Source](https://www.bleepingcomputer.com/news/security/maza-forum-hacked-in-recent-attacks-targeting-cybercrime-forums/){:target="_blank" rel="noopener"}

> The Maza cybercrime forum was hacked and member data leaked in the latest of a series of attacks targeting mostly Russian-speaking hacker forums. [...]
