Title: Friday Squid Blogging: Squid Cartoon
Date: 2021-03-19T21:14:05+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: humor;squid
Slug: friday-squid-blogging-squid-cartoon

[Source](https://www.schneier.com/blog/archives/2021/03/friday-squid-blogging-squid-cartoon.html){:target="_blank" rel="noopener"}

> Squid ink. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
