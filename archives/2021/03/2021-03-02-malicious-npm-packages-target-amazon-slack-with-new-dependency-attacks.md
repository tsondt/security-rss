Title: Malicious NPM packages target Amazon, Slack with new dependency attacks
Date: 2021-03-02T00:14:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: malicious-npm-packages-target-amazon-slack-with-new-dependency-attacks

[Source](https://www.bleepingcomputer.com/news/security/malicious-npm-packages-target-amazon-slack-with-new-dependency-attacks/){:target="_blank" rel="noopener"}

> Threat actors are targeting Amazon, Zillow, Lyft, and Slack NodeJS apps using the new 'Dependency Confusion' vulnerability to steal Linux/Unix password files and open reverse shells back to the attackers. [...]
