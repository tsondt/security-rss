Title: IoT vendor Sierra Wireless suffers ransomware attack, production halted
Date: 2021-03-24T14:12:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: iot-vendor-sierra-wireless-suffers-ransomware-attack-production-halted

[Source](https://portswigger.net/daily-swig/iot-vendor-sierra-wireless-suffers-ransomware-attack-production-halted){:target="_blank" rel="noopener"}

> Device manufacturer said it is working to get systems back online [...]
