Title: UK terror law reviewer calls for expanded police powers to imprison people who refuse to hand over passwords
Date: 2021-03-29T14:01:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-terror-law-reviewer-calls-for-expanded-police-powers-to-imprison-people-who-refuse-to-hand-over-passwords

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/29/terror_cops_password_law_change_call/){:target="_blank" rel="noopener"}

> Cops should be exempted from regulatory safeguards, says lawyer The UK's Government Reviewer of Terrorism Laws is again advising the removal of legal safeguards around a controversial law that allows people to be jailed if they refuse police demands for forced decryption of their devices.... [...]
