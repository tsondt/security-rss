Title: SAP Stomps Out Critical RCE Flaw in Manufacturing Software
Date: 2021-03-10T22:00:24+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: sap-stomps-out-critical-rce-flaw-in-manufacturing-software

[Source](https://threatpost.com/sap-critical-rce-flaw-manufacturing/164666/){:target="_blank" rel="noopener"}

> The remote code execution flaw could allow attackers to deploy malware, modify network configurations and view databases. [...]
