Title: World's leading dairy group Lactalis hit by cyberattack
Date: 2021-03-01T14:29:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: worlds-leading-dairy-group-lactalis-hit-by-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/worlds-leading-dairy-group-lactalis-hit-by-cyberattack/){:target="_blank" rel="noopener"}

> Lactalis, the world's leading dairy group, has disclosed a cyberattack after unknown threat actors have breached some of the company's systems. [...]
