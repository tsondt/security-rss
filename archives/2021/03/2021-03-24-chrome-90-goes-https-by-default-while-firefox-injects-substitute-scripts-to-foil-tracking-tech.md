Title: Chrome 90 goes HTTPS by default while Firefox injects substitute scripts to foil tracking tech
Date: 2021-03-24T08:19:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: chrome-90-goes-https-by-default-while-firefox-injects-substitute-scripts-to-foil-tracking-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/24/chrome_firefox_privacy/){:target="_blank" rel="noopener"}

> Privacy. Are we there yet? No, but there's some progress at least When version 90 of Google's Chrome browser arrives in mid-April, initial website visits will default to a secure HTTPS connection in the event the user has failed to specify a preferred URI scheme.... [...]
