Title: It's not easy being green: EV HTTPS cert seller Sectigo questions Chrome's logic in burying EV HTTPS cert info
Date: 2021-03-03T11:45:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: its-not-easy-being-green-ev-https-cert-seller-sectigo-questions-chromes-logic-in-burying-ev-https-cert-info

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/03/sectigo_google_certificates/){:target="_blank" rel="noopener"}

> Seeing as Google thinks no one cares about location records, we'll remove street addresses from all our sites, says compliance chief Sectigo’s chief compliance officer has hit out at Google for minimizing the visibility of Extended Validation HTTPS certificates in Chrome.... [...]
