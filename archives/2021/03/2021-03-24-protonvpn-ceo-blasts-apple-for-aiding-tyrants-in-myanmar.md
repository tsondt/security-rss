Title: ProtonVPN CEO Blasts Apple for ‘Aiding Tyrants’ in Myanmar
Date: 2021-03-24T20:53:54+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Mobile Security;Privacy
Slug: protonvpn-ceo-blasts-apple-for-aiding-tyrants-in-myanmar

[Source](https://threatpost.com/protonvpn-ceo-blasts-apple-myanmar/165022/){:target="_blank" rel="noopener"}

> CEO says Apple rejected a security update needed to protect human-rights abuse evidence. [...]
