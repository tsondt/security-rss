Title: Blockchain may be the machinery of mischief, but it can't help telling the truth
Date: 2021-03-29T09:32:05+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: blockchain-may-be-the-machinery-of-mischief-but-it-cant-help-telling-the-truth

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/29/nft_authenticity/){:target="_blank" rel="noopener"}

> It's still totally bananas, though Column One of the many joys of blockchain is that it generates even more heat online than a Chinese Bitcoin mine pumps into the atmosphere. This month's posterchild is the NFT, the Non-Fungible Token, which is seen by all the right-thinking fold as practically the fundamental particle of crypto-scam physics.... [...]
