Title: Apple emits patches for iOS, macOS, Safari, etc to stop dodgy websites hijacking people's gadgets
Date: 2021-03-09T01:07:16+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: apple-emits-patches-for-ios-macos-safari-etc-to-stop-dodgy-websites-hijacking-peoples-gadgets

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/09/apple_security_update/){:target="_blank" rel="noopener"}

> Plus: Chrome also patched, Microsoft and Intel team up for homomorphic encryption, and more In brief Apple on Monday released security patches for macOS, iOS, iPadOS, watchOS, and Safari to fix up a vulnerability that can be exploited by malicious web pages to run malware on victims' computers and gadgets.... [...]
