Title: Newest Intel Side-Channel Attack Sniffs Out Sensitive Data
Date: 2021-03-08T21:20:15+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: newest-intel-side-channel-attack-sniffs-out-sensitive-data

[Source](https://threatpost.com/intel-side-channel-attack-data/164582/){:target="_blank" rel="noopener"}

> A new side-channel attack takes aim at Intel's CPU ring interconnect in order to glean sensitive data. [...]
