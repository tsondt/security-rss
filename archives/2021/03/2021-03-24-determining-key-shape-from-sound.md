Title: Determining Key Shape from Sound
Date: 2021-03-24T11:10:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;keys;locks;physical security;smartphones
Slug: determining-key-shape-from-sound

[Source](https://www.schneier.com/blog/archives/2021/03/determining-key-shape-from-sound.html){:target="_blank" rel="noopener"}

> It’s not yet very accurate or practical, but under ideal conditions it is possible to figure out the shape of a house key by listening to it being used. Listen to Your Key: Towards Acoustics-based Physical Key Inference Abstract: Physical locks are one of the most prevalent mechanisms for securing objects such as doors. While many of these locks are vulnerable to lock-picking, they are still widely used as lock-picking requires specific training with tailored instruments, and easily raises suspicion. In this paper, we propose SpiKey, a novel attack that significantly lowers the bar for an attacker as opposed to [...]
