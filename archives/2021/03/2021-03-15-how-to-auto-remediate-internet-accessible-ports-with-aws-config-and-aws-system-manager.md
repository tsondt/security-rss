Title: How to auto-remediate internet accessible ports with AWS Config and AWS System Manager
Date: 2021-03-15T17:33:05+00:00
Author: Amal AlQhtani
Category: AWS Security
Tags: AWS Config;AWS Systems Manager;Intermediate (200);Security, Identity, & Compliance;Automation;Network;Network security;security automation;Security Blog;Security groups;SSM automation
Slug: how-to-auto-remediate-internet-accessible-ports-with-aws-config-and-aws-system-manager

[Source](https://aws.amazon.com/blogs/security/how-to-auto-remediate-internet-accessible-ports-with-aws-config-and-aws-system-manager/){:target="_blank" rel="noopener"}

> With the AWS Config service, you can assess, audit, and evaluate the configuration of your Amazon Web Services (AWS) resources. AWS Config continuously monitors and records your AWS resource configurations changes, and enables you to automate the evaluation of those recordings against desired configurations. Not only can AWS Config monitor and detect deviations from desired [...]
