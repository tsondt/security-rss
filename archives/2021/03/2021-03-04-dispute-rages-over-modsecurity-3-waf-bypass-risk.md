Title: Dispute rages over ModSecurity 3 WAF ‘bypass risk’
Date: 2021-03-04T12:30:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: dispute-rages-over-modsecurity-3-waf-bypass-risk

[Source](https://portswigger.net/daily-swig/dispute-rages-over-modsecurity-3-waf-bypass-risk){:target="_blank" rel="noopener"}

> A security researcher went public with his concerns, but ModSec maintainers insist that default WAF settings are safe [...]
