Title: Cypriot hacker who extorted website owners by threatening to leak stolen data is jailed
Date: 2021-03-18T15:13:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cypriot-hacker-who-extorted-website-owners-by-threatening-to-leak-stolen-data-is-jailed

[Source](https://portswigger.net/daily-swig/cypriot-hacker-who-extorted-website-owners-by-threatening-to-leak-stolen-data-is-jailed){:target="_blank" rel="noopener"}

> US sentence and a heavy fine for criminal hacker who targeted multiple organizations [...]
