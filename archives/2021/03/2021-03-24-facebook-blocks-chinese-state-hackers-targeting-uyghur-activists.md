Title: Facebook blocks Chinese state hackers targeting Uyghur activists
Date: 2021-03-24T16:17:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: facebook-blocks-chinese-state-hackers-targeting-uyghur-activists

[Source](https://www.bleepingcomputer.com/news/security/facebook-blocks-chinese-state-hackers-targeting-uyghur-activists/){:target="_blank" rel="noopener"}

> Facebook took down accounts used by a Chinese-sponsored hacking group to deploy surveillance malware on devices used by Uyghurs activists, journalists, and dissidents living outside China. [...]
