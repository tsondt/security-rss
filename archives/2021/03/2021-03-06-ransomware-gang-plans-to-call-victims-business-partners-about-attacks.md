Title: Ransomware gang plans to call victim's business partners about attacks
Date: 2021-03-06T12:47:07-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-plans-to-call-victims-business-partners-about-attacks

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-plans-to-call-victims-business-partners-about-attacks/){:target="_blank" rel="noopener"}

> The REvil ransomware operation announced this week that they are using DDoS attacks and voice calls to journalists and victim's business partners to generate ransom payments. [...]
