Title: Twitter now supports multiple 2FA security keys on mobile and web
Date: 2021-03-15T14:00:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: twitter-now-supports-multiple-2fa-security-keys-on-mobile-and-web

[Source](https://www.bleepingcomputer.com/news/security/twitter-now-supports-multiple-2fa-security-keys-on-mobile-and-web/){:target="_blank" rel="noopener"}

> Twitter has added support for multiple security keys to accounts with two-factor authentication (2FA) enabled for logging into the social network's web interface and mobile apps. [...]
