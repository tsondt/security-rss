Title: UK defense and foreign policy review places ‘cyber’ front and center
Date: 2021-03-16T16:55:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uk-defense-and-foreign-policy-review-places-cyber-front-and-center

[Source](https://portswigger.net/daily-swig/uk-defense-and-foreign-policy-review-places-cyber-front-and-center){:target="_blank" rel="noopener"}

> Prime Minister says the new National Cyber Force will have its headquarters in the north of England [...]
