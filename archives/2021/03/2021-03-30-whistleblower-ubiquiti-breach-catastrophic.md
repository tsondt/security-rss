Title: Whistleblower: Ubiquiti Breach “Catastrophic”
Date: 2021-03-30T18:00:49+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ubiquiti breach;Ubiquiti Inc.;Ubiquiti Networks
Slug: whistleblower-ubiquiti-breach-catastrophic

[Source](https://krebsonsecurity.com/2021/03/whistleblower-ubiquiti-breach-catastrophic/){:target="_blank" rel="noopener"}

> On Jan. 11, Ubiquiti Inc. [NYSE:UI] — a major vendor of cloud-enabled Internet of Things (IoT) devices such as routers, network video recorders and security cameras — disclosed that a breach involving a third-party cloud provider had exposed customer account credentials. Now a source who participated in the response to that breach alleges Ubiquiti massively downplayed a “catastrophic” incident to minimize the hit to its stock price, and that the third-party cloud provider claim was a fabrication. A security professional at Ubiquiti who helped the company respond to the two-month breach beginning in December 2020 contacted KrebsOnSecurity after raising his [...]
