Title: Mimecast confirms hackers behind SolarWinds supply chain attack accessed limited amount of customer information
Date: 2021-03-17T14:25:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: mimecast-confirms-hackers-behind-solarwinds-supply-chain-attack-accessed-limited-amount-of-customer-information

[Source](https://portswigger.net/daily-swig/mimecast-confirms-hackers-behind-solarwinds-supply-chain-attack-accessed-limited-amount-of-customer-information){:target="_blank" rel="noopener"}

> Third-party report finds ‘no evidence’ that security firm’s source code was modified [...]
