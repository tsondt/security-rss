Title: Vulnerabilities in Smarty PHP template engine renders popular CMS platforms open to abuse
Date: 2021-03-03T16:34:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: vulnerabilities-in-smarty-php-template-engine-renders-popular-cms-platforms-open-to-abuse

[Source](https://portswigger.net/daily-swig/vulnerabilities-in-smarty-php-template-engine-renders-cms-platforms-open-to-abuse){:target="_blank" rel="noopener"}

> Now-patched exploits surface for Tiki Wiki CMS and CMS Made Simple, but researchers warn that many other apps may be vulnerable [...]
