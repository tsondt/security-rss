Title: EU cybersecurity strategy: Coronavirus, supply chain attacks highlight ‘lack of coordination’ among member states
Date: 2021-03-24T11:59:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: eu-cybersecurity-strategy-coronavirus-supply-chain-attacks-highlight-lack-of-coordination-among-member-states

[Source](https://portswigger.net/daily-swig/eu-cybersecurity-strategy-coronavirus-supply-chain-attacks-highlight-lack-of-coordination-among-member-states){:target="_blank" rel="noopener"}

> ‘Everybody knew that the pandemic was coming. Were we ready to react collectively? We were not’ [...]
