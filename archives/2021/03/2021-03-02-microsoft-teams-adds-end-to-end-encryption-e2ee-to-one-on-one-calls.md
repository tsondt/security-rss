Title: Microsoft Teams adds end-to-end encryption (E2EE) to one-on-one calls
Date: 2021-03-02T09:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-teams-adds-end-to-end-encryption-e2ee-to-one-on-one-calls

[Source](https://www.bleepingcomputer.com/news/security/microsoft-teams-adds-end-to-end-encryption-e2ee-to-one-on-one-calls/){:target="_blank" rel="noopener"}

> Microsoft adds new security, privacy, and compliance features to the Microsoft Teams chat and collaboration solution, including end-to-end encryption support for one-on-one calls. [...]
