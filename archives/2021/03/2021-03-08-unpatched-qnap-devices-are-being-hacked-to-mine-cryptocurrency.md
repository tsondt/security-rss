Title: Unpatched QNAP devices are being hacked to mine cryptocurrency
Date: 2021-03-08T08:55:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: unpatched-qnap-devices-are-being-hacked-to-mine-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/security/unpatched-qnap-devices-are-being-hacked-to-mine-cryptocurrency/){:target="_blank" rel="noopener"}

> Unpatched network-attached storage (NAS) devices are targeted in ongoing attacks where the attackers try to take them over and install cryptominer malware to mine for cryptocurrency. [...]
