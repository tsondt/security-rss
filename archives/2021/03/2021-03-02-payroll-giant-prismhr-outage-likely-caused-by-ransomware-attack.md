Title: Payroll giant PrismHR outage likely caused by ransomware attack
Date: 2021-03-02T16:14:26-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: payroll-giant-prismhr-outage-likely-caused-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/payroll-giant-prismhr-outage-likely-caused-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Leading payroll company PrismHR is suffering a massive outage after suffering a cyberattack this weekend that looks like a ransomware attack from conversations with customers. [...]
