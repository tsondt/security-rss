Title: Announcing the Risk Protection Program: Moving from shared responsibility to shared fate
Date: 2021-03-02T12:30:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: announcing-the-risk-protection-program-moving-from-shared-responsibility-to-shared-fate

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-risk-protection-program-now-in-preview/){:target="_blank" rel="noopener"}

> At Google Cloud, our mission is to enable organizations around the world to transform their business using digital technology. We want to make it easy for organizations to transition even their most sensitive workloads onto our platform, and that includes navigating risk management in the ever-evolving cloud-native environment. Working closely with customers to deliver better security and risk outcomes is a core pillar of our strategy to be the most Trusted Cloud provider. We are committed to delivering a more secure experience to our customers and enhancing trust in the cloud ecosystem. As we explored how we could help organizations [...]
