Title: All mapped out: Researchers uncover hidden flaws in Apple’s offline ‘find my device’ feature
Date: 2021-03-09T16:16:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: all-mapped-out-researchers-uncover-hidden-flaws-in-apples-offline-find-my-device-feature

[Source](https://portswigger.net/daily-swig/all-mapped-out-researchers-uncover-hidden-flaws-in-apples-offline-find-my-device-feature){:target="_blank" rel="noopener"}

> Bluetooth tracking system earns plaudits from independent security analysis despite recently resolved flaw [...]
