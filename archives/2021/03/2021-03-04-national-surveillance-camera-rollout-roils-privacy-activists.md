Title: National Surveillance Camera Rollout Roils Privacy Activists
Date: 2021-03-04T17:21:34+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy;Videos
Slug: national-surveillance-camera-rollout-roils-privacy-activists

[Source](https://threatpost.com/camera-roll-out-roils-privacy-activists/164502/){:target="_blank" rel="noopener"}

> TALON, a network of smart, connected security cameras developed by the Atlanta-based startup and installed by law enforcement around the country, raises surveillance-related privacy concerns. [...]
