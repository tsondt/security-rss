Title: CISA: No federal civilian agency hacked in Exchange attacks, so far
Date: 2021-03-11T11:14:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-no-federal-civilian-agency-hacked-in-exchange-attacks-so-far

[Source](https://www.bleepingcomputer.com/news/security/cisa-no-federal-civilian-agency-hacked-in-exchange-attacks-so-far/){:target="_blank" rel="noopener"}

> CISA officials said that, so far, there is no evidence of US federal civilian agencies compromised during ongoing attacks targeting Microsoft Exchange servers. [...]
