Title: UK cybersecurity spending on the rise despite pandemic-induced budget cuts
Date: 2021-03-08T15:34:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uk-cybersecurity-spending-on-the-rise-despite-pandemic-induced-budget-cuts

[Source](https://portswigger.net/daily-swig/uk-cybersecurity-spending-on-the-rise-despite-pandemic-induced-budget-cuts){:target="_blank" rel="noopener"}

> Coronavirus has increased the infosec workload, but redundancies and recruitment freezes are still widespread, according to a new report [...]
