Title: Details of a Computer Banking Scam
Date: 2021-03-22T11:15:05+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: banking;phones;psychology of security;scams;social engineering;video
Slug: details-of-a-computer-banking-scam

[Source](https://www.schneier.com/blog/archives/2021/03/details-of-a-computer-banking-scam.html){:target="_blank" rel="noopener"}

> This is a longish video that describes a profitable computer banking scam that’s run out of call centers in places like India. There’s a lot of fluff about glitterbombs and the like, but the details are interesting. The scammers convince the victims to give them remote access to their computers, and then that they’ve mistyped a dollar amount and have received a large refund that they didn’t deserve. Then they convince the victims to send cash to a drop site, where a money mule retrieves it and forwards it to the scammers. I found it interesting for several reasons. One, [...]
