Title: Warning the World of a Ticking Time Bomb
Date: 2021-03-09T21:04:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: The Coming Storm;Time to Patch;Allison Nixon;Check My OWA;Unit221B
Slug: warning-the-world-of-a-ticking-time-bomb

[Source](https://krebsonsecurity.com/2021/03/warning-the-world-of-a-ticking-time-bomb/){:target="_blank" rel="noopener"}

> Globally, hundreds of thousand of organizations running Exchange email servers from Microsoft just got mass-hacked, including at least 30,000 victims in the United States. Each hacked server has been retrofitted with a “web shell” backdoor that gives the bad guys total, remote control, the ability to read all email, and easy access to the victim’s other computers. Researchers are now racing to identify, alert and help victims, and hopefully prevent further mayhem. On Mar. 5, KrebsOnSecurity broke the news that at least 30,000 organizations and hundreds of thousands globally had been hacked. The same sources who shared those figures say [...]
