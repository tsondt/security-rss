Title: Researchers hacked Indian govt sites via exposed git and env files
Date: 2021-03-12T11:46:39-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Government
Slug: researchers-hacked-indian-govt-sites-via-exposed-git-and-env-files

[Source](https://www.bleepingcomputer.com/news/security/researchers-hacked-indian-govt-sites-via-exposed-git-and-env-files/){:target="_blank" rel="noopener"}

> Researchers have now disclosed more information on how they were able to breach multiple websites of the Indian government. The full findings disclosed today shed light on the routes leveraged by the researchers, including finding exposed.git directories and.env files on some of these systems. [...]
