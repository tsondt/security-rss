Title: TikTok no worse than Facebook for privacy, says Citizen Lab (although Chinese TikTok is a horror)
Date: 2021-03-23T07:58:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: tiktok-no-worse-than-facebook-for-privacy-says-citizen-lab-although-chinese-tiktok-is-a-horror

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/23/tiktok_privacy_report_citizenlab/){:target="_blank" rel="noopener"}

> App has common code base for three versions: one global edition; one for south-east Asian; one for China TikTok is likely no more of a threat to users than Facebook, according to an analysis by academic research group Citizen Lab that analyzed the video-sharing social networking service’s app to probe for security, privacy and censorship issues.... [...]
