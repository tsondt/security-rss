Title: Was 2020 a year of lost innovation? Not for cybercriminals
Date: 2021-03-17T22:00:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: was-2020-a-year-of-lost-innovation-not-for-cybercriminals

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/17/malwarebytes_webcast/){:target="_blank" rel="noopener"}

> Tune in, join our APAC editor and Malwarebytes – and learn how to start fighting back Webcast The shift to remote working over the last year hasn’t been all bad – it’s forced the pace of digital transformation and encouraged many organisations to rethink the way they operate.... [...]
