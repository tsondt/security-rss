Title: German Parliament targeted again by Russian state hackers
Date: 2021-03-26T15:14:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: german-parliament-targeted-again-by-russian-state-hackers

[Source](https://www.bleepingcomputer.com/news/security/german-parliament-targeted-again-by-russian-state-hackers/){:target="_blank" rel="noopener"}

> Email accounts of multiple German Parliament members were targeted in a spearphishing attack. It is not yet known if any data was stolen during the incident. [...]
