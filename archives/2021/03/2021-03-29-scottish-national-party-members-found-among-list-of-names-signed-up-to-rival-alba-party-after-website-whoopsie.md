Title: Scottish National Party members found among list of names signed up to rival Alba Party after website whoopsie
Date: 2021-03-29T12:32:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: scottish-national-party-members-found-among-list-of-names-signed-up-to-rival-alba-party-after-website-whoopsie

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/29/alba_party_website_error/){:target="_blank" rel="noopener"}

> Freeeedommm! (for your data) Alex Salmond's Alba Party has got off to a rocky start after a coding error on its website appeared to expose the names of those signed up.... [...]
