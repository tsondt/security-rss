Title: Mozilla tightens Firefox’s HTTP referrer header controls to boost privacy
Date: 2021-03-24T16:14:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: mozilla-tightens-firefoxs-http-referrer-header-controls-to-boost-privacy

[Source](https://portswigger.net/daily-swig/mozilla-tightens-firefoxs-http-referrer-header-controls-to-boost-privacy){:target="_blank" rel="noopener"}

> Most of the web now protected against one class of information disclosure vulnerability [...]
