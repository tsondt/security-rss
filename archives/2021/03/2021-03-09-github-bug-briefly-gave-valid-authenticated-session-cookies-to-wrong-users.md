Title: GitHub bug briefly gave valid authenticated session cookies to wrong users
Date: 2021-03-09T06:45:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: github-bug-briefly-gave-valid-authenticated-session-cookies-to-wrong-users

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/09/github_authentication_bug/){:target="_blank" rel="noopener"}

> Don’t panic: Fewer than 0.001% of sessions compromised through flaw that couldn’t be maliciously triggered If you visit GitHub today you’ll be asked to authenticate anew because the code collaboration locker has squished a bug that sometimes “misrouted a user’s session to the browser of another authenticated user, giving them the valid and authenticated session cookie for another user.”... [...]
