Title: CISA Warns of Security Flaws in GE Power Management Devices
Date: 2021-03-22T20:39:08+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: cisa-warns-of-security-flaws-in-ge-power-management-devices

[Source](https://threatpost.com/cisa-security-flaws-ge-power-management/164961/){:target="_blank" rel="noopener"}

> The flaws could allow an attacker to access sensitive information, reboot the UR, gain privileged access, or cause a denial-of-service condition. [...]
