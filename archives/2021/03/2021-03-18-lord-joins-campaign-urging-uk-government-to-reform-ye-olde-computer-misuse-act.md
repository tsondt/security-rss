Title: Lord joins campaign urging UK government to reform ye olde Computer Misuse Act
Date: 2021-03-18T16:15:34+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: lord-joins-campaign-urging-uk-government-to-reform-ye-olde-computer-misuse-act

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/18/lord_joins_cyberup_cma_reform_campaigners/){:target="_blank" rel="noopener"}

> Conservative peer praises CyberUp for potential industry benefits A Conservative peer has joined calls to reform the Computer Misuse Act (CMA) days after the government declared that infosec and "cyber power" are the key to British foreign and industrial policy for the 2020s.... [...]
