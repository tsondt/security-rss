Title: Easy SMS Hijacking
Date: 2021-03-19T11:21:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: authentication;cell phones;cybercrime;SMS;vulnerabilities
Slug: easy-sms-hijacking

[Source](https://www.schneier.com/blog/archives/2021/03/easy-sms-hijacking.html){:target="_blank" rel="noopener"}

> Vice is reporting on a cell phone vulnerability caused by commercial SMS services. One of the things these services permit is text message forwarding. It turns out that with a little bit of anonymous money — in this case, $16 off an anonymous prepaid credit card — and a few lies, you can forward the text messages from any phone to any other phone. For businesses, sending text messages to hundreds, thousands, or perhaps millions of customers can be a laborious task. Sakari streamlines that process by letting business customers import their own number. A wide ecosystem of these companies [...]
