Title: Automate Amazon EC2 instance isolation by using tags
Date: 2021-03-01T21:05:03+00:00
Author: Jose Obando
Category: AWS Security
Tags: Advanced (300);Amazon EC2;Security, Identity, & Compliance;Automation;Incident response;Isolation;SecOps;Security Blog
Slug: automate-amazon-ec2-instance-isolation-by-using-tags

[Source](https://aws.amazon.com/blogs/security/automate-amazon-ec2-instance-isolation-by-using-tags/){:target="_blank" rel="noopener"}

> Containment is a crucial part of an overall Incident Response Strategy, as this practice allows time for responders to perform forensics, eradication and recovery during an Incident. There are many different approaches to containment. In this post, we will be focusing on isolation—the ability to keep multiple targets separated so that each target only sees [...]
