Title: Zoom Screen-Sharing Glitch ‘Briefly’ Leaks Sensitive Data
Date: 2021-03-18T15:52:29+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: zoom-screen-sharing-glitch-briefly-leaks-sensitive-data

[Source](https://threatpost.com/zoom-glitch-leaks-data/164876/){:target="_blank" rel="noopener"}

> A glitch in Zoom's screen-sharing feature shows parts of presenters' screens that they did not intend to share - potentially leaking emails or passwords. [...]
