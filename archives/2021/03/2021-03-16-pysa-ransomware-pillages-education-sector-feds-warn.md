Title: PYSA Ransomware Pillages Education Sector, Feds Warn
Date: 2021-03-16T21:15:18+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware
Slug: pysa-ransomware-pillages-education-sector-feds-warn

[Source](https://threatpost.com/pysa-ransomware-education-feds-warn/164832/){:target="_blank" rel="noopener"}

> A major spike of attacks against higher ed, K-12 and seminaries in March has prompted the FBI to issue a special alert. [...]
