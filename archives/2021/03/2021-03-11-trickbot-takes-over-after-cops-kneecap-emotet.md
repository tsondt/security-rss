Title: TrickBot Takes Over, After Cops Kneecap Emotet
Date: 2021-03-11T21:47:23+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security;Most Recent ThreatLists;Vulnerabilities
Slug: trickbot-takes-over-after-cops-kneecap-emotet

[Source](https://threatpost.com/trickbot-takes-over-emotet/164710/){:target="_blank" rel="noopener"}

> TrickBot rises to top threat in February, overtaking Emotet in Check Point’s new index. [...]
