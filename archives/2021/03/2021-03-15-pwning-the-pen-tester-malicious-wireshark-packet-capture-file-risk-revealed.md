Title: Pwning the pen tester: Malicious Wireshark packet capture file risk revealed
Date: 2021-03-15T16:50:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: pwning-the-pen-tester-malicious-wireshark-packet-capture-file-risk-revealed

[Source](https://portswigger.net/daily-swig/pwning-the-pen-tester-malicious-wireshark-packet-capture-file-risk-revealed){:target="_blank" rel="noopener"}

> CVE assigned due to potential for harm even though some social engineering trickery is required [...]
