Title: Fleeceware Apps Bank $400M in Revenue
Date: 2021-03-25T21:28:26+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security
Slug: fleeceware-apps-bank-400m-in-revenue

[Source](https://threatpost.com/fleeceware-apps-400m-revenue/165040/){:target="_blank" rel="noopener"}

> The cache of apps, found in Apple and Google's official marketplaces is largely targeted towards children, including several "slime simulators." [...]
