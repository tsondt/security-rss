Title: Pair accused of turning photos into vids to crack tax dept facial recognition system in China
Date: 2021-03-31T05:05:28+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: pair-accused-of-turning-photos-into-vids-to-crack-tax-dept-facial-recognition-system-in-china

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/31/tax_scammers_fool_ai_facial_recognition/){:target="_blank" rel="noopener"}

> Then issuing tens of millions in fake invoices A duo in China has been accused of tricking a government-run identity verification system to create fake invoices.... [...]
