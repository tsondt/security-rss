Title: New Sarbloh ransomware supports Indian farmers' protest
Date: 2021-03-08T16:20:06-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: new-sarbloh-ransomware-supports-indian-farmers-protest

[Source](https://www.bleepingcomputer.com/news/security/new-sarbloh-ransomware-supports-indian-farmers-protest/){:target="_blank" rel="noopener"}

> A new ransomware known as Sarbloh encrypts your files while at the same time delivering a message supporting the protests of Indian farmers. [...]
