Title: z0Miner botnet hunts for unpatched ElasticSearch, Jenkins servers
Date: 2021-03-09T10:37:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: z0miner-botnet-hunts-for-unpatched-elasticsearch-jenkins-servers

[Source](https://www.bleepingcomputer.com/news/security/z0miner-botnet-hunts-for-unpatched-elasticsearch-jenkins-servers/){:target="_blank" rel="noopener"}

> A cryptomining botnet spotted last year is now targeting and attempting to take control of Jenkins and ElasticSearch servers to mine for Monero (XMR) cryptocurrency. [...]
