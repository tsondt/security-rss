Title: BP Chargemaster's Pulse rebrand let crims send IcedID banking trojan from formerly legit mailboxes
Date: 2021-03-25T10:15:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: bp-chargemasters-pulse-rebrand-let-crims-send-icedid-banking-trojan-from-formerly-legit-mailboxes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/25/bp_chargemaster_pulse_rebrand_email_trojan_horror/){:target="_blank" rel="noopener"}

> F-Secure confirms nature of email nasty to El Reg BP Chargemaster, purveyors of sockets for electric vehicles, seemingly had its email domain hijacked by criminals who used formerly legitimate addresses to send banking trojans to customers.... [...]
