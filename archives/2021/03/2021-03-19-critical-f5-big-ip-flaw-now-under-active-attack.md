Title: Critical F5 BIG-IP Flaw Now Under Active Attack
Date: 2021-03-19T20:52:15+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: critical-f5-big-ip-flaw-now-under-active-attack

[Source](https://threatpost.com/critical-f5-big-ip-flaw-now-under-active-attack/164940/){:target="_blank" rel="noopener"}

> Researchers are reporting mass scanning for – and in-the-wild exploitation of – a critical-severity flaw in the F5 BIG-IP and BIG-IQ enterprise networking infrastructure. [...]
