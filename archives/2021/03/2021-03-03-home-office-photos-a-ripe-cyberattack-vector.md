Title: Home-Office Photos: A Ripe Cyberattack Vector
Date: 2021-03-03T19:29:14+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Privacy;Web Security
Slug: home-office-photos-a-ripe-cyberattack-vector

[Source](https://threatpost.com/home-office-photos-cyberattack-vector/164460/){:target="_blank" rel="noopener"}

> Threat actors can use personal information gleaned from images to craft targeted scams, putting personal and corporate data at risk. [...]
