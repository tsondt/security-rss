Title: Cybersecurity firm Qualys likely latest victim of Accellion hacks
Date: 2021-03-03T11:39:56-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: cybersecurity-firm-qualys-likely-latest-victim-of-accellion-hacks

[Source](https://www.bleepingcomputer.com/news/security/cybersecurity-firm-qualys-likely-latest-victim-of-accellion-hacks/){:target="_blank" rel="noopener"}

> Cybersecurity firm Qualys is the latest victim to have suffered a data breach after a zero-day vulnerability in their Accellion FTA server was exploited to steal hosted files. [...]
