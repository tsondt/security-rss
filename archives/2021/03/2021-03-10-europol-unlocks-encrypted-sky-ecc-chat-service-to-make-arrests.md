Title: Europol 'unlocks' encrypted Sky ECC chat service to make arrests
Date: 2021-03-10T14:03:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: europol-unlocks-encrypted-sky-ecc-chat-service-to-make-arrests

[Source](https://www.bleepingcomputer.com/news/security/europol-unlocks-encrypted-sky-ecc-chat-service-to-make-arrests/){:target="_blank" rel="noopener"}

> European law enforcement authorities have made a large number of arrests after a joint operation involving the monitoring of organized crime communication channels over the Sky ECC encrypted chat. [...]
