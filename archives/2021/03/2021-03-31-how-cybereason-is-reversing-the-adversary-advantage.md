Title: How Cybereason is reversing the adversary advantage
Date: 2021-03-31T08:00:14+00:00
Author: Elyse Silverberg
Category: The Register
Tags: 
Slug: how-cybereason-is-reversing-the-adversary-advantage

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/31/cybereason_ransomware_defence/){:target="_blank" rel="noopener"}

> Malop provides the context to fend off ransomware attacks Sponsored Here's something you might not know about Lior Div, one of the most accomplished people in protecting the world's computing infrastructure from attack: he struggles to read a page of text. Yet, that's one of his biggest strengths.... [...]
