Title: Jailbreak Tool Works on iPhones Up to iOS 14.3
Date: 2021-03-02T17:54:53+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: jailbreak-tool-works-on-iphones-up-to-ios-143

[Source](https://threatpost.com/jailbreak-tool-works-on-iphones-up-to-ios-14-3/164420/){:target="_blank" rel="noopener"}

> The UnC0ver team took advantage of an iOS flaw patched in January in its latest tool allowing developers and other enthusiasts to hack into their own devices. [...]
