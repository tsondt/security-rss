Title: Apple's app transparency rules: Google's privacy labels for Chrome and Search on iOS highlighted by DuckDuckGo
Date: 2021-03-16T13:15:07+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: apples-app-transparency-rules-googles-privacy-labels-for-chrome-and-search-on-ios-highlighted-by-duckduckgo

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/16/keep_scrolling_googles_privacy_labels/){:target="_blank" rel="noopener"}

> Google reveals how much personal data' collected in Chrome, Google app. 'No wonder they wanted to hide it' Google's Apple-mandated privacy labels for its Chrome and Search apps on iOS have drawn criticism from tiny search rival DuckDuckGo, which tweeted "no wonder they wanted to hide it."... [...]
