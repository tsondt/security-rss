Title: The torture garden of Microsoft Exchange: Grant us the serenity to accept what they cannot EOL
Date: 2021-03-08T10:15:11+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: the-torture-garden-of-microsoft-exchange-grant-us-the-serenity-to-accept-what-they-cannot-eol

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/08/column/){:target="_blank" rel="noopener"}

> Time to fix those legacy evils, though.... right? Column It is the monster which corrupts all it touches. It is an energy-sucking vampire that thrives on the pain it promotes. It cannot be killed, but grows afresh as each manifestation outdoes the last in awfulness and horror. It is Microsoft Exchange and its drooling minion, Outlook.... [...]
