Title: Microsoft blames crypto key rotation snafu for 365 outage
Date: 2021-03-17T16:48:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: microsoft-blames-crypto-key-rotation-snafu-for-365-outage

[Source](https://portswigger.net/daily-swig/microsoft-blames-crypto-key-rotation-snafu-for-365-outage){:target="_blank" rel="noopener"}

> Teams, Exchange Online, and other services were knocked offline for more than 14 hours [...]
