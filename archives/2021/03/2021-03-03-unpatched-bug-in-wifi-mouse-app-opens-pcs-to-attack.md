Title: Unpatched Bug in WiFi Mouse App Opens PCs to Attack
Date: 2021-03-03T21:49:00+00:00
Author: Tom Spring
Category: Threatpost
Tags: Hacks;Mobile Security;Vulnerabilities
Slug: unpatched-bug-in-wifi-mouse-app-opens-pcs-to-attack

[Source](https://threatpost.com/unpatched-bug-in-wifi-mouse-opens-pcs-to-attack/164480/){:target="_blank" rel="noopener"}

> Wireless mouse-utility lacks proper authentication and opens Windows systems to attack. [...]
