Title: US seizes more domains used in COVID-19 vaccine phishing attacks
Date: 2021-03-09T15:20:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-seizes-more-domains-used-in-covid-19-vaccine-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-more-domains-used-in-covid-19-vaccine-phishing-attacks/){:target="_blank" rel="noopener"}

> The US Department of Justice has seized a fifth domain name used to impersonate the official site of a biotechnology company involved in COVID-19 vaccine development. [...]
