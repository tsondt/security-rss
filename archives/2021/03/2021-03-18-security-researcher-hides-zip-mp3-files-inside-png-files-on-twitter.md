Title: Security Researcher Hides ZIP, MP3 Files Inside PNG Files on Twitter
Date: 2021-03-18T14:53:23+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks
Slug: security-researcher-hides-zip-mp3-files-inside-png-files-on-twitter

[Source](https://threatpost.com/researcher-hides-files-in-png-twitter/164881/){:target="_blank" rel="noopener"}

> The newly discovered steganography method could be exploited by threat actors to obscure nefarious activity inside photos hosted on the social-media platform. [...]
