Title: Latest web hacking tools – Q1 2021
Date: 2021-03-31T10:33:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: latest-web-hacking-tools-q1-2021

[Source](https://portswigger.net/daily-swig/latest-web-hacking-tools-q1-2021){:target="_blank" rel="noopener"}

> We take a look back at some of the best offensive security tools that were launched over the past three months [...]
