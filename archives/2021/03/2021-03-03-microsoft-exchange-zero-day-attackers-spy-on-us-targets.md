Title: Microsoft Exchange Zero-Day Attackers Spy on U.S. Targets
Date: 2021-03-03T15:30:52+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Hacks;Malware;Vulnerabilities
Slug: microsoft-exchange-zero-day-attackers-spy-on-us-targets

[Source](https://threatpost.com/microsoft-exchange-zero-day-attackers-spy/164438/){:target="_blank" rel="noopener"}

> Full dumps of email boxes, lateral movement and backdoors characterize sophisticated attacks by a Chinese APT - while more incidents spread like wildfire. [...]
