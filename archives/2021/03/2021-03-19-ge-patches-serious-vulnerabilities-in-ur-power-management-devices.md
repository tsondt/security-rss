Title: GE patches serious vulnerabilities in UR power management devices
Date: 2021-03-19T15:50:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ge-patches-serious-vulnerabilities-in-ur-power-management-devices

[Source](https://portswigger.net/daily-swig/ge-patches-serious-vulnerabilities-in-ur-power-management-devices){:target="_blank" rel="noopener"}

> Universal Relay devices are used to simplify power management in critical infrastructure assets [...]
