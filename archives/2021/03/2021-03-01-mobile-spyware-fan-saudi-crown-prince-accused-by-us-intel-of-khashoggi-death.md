Title: Mobile spyware fan Saudi Crown Prince accused by US intel of Khashoggi death
Date: 2021-03-01T06:40:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: mobile-spyware-fan-saudi-crown-prince-accused-by-us-intel-of-khashoggi-death

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/01/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Critical Cisco flaw, NSA advice, and someone hacked Gab? In Brief The murder of Washington Post columnist Jamal Khashoggi, which is said to be have been aided by digital surveillance, was ordered by the head of the Saudi Arabian government, US intelligence has publicly asserted.... [...]
