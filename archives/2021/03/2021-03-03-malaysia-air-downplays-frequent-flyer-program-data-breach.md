Title: Malaysia Air Downplays Frequent-Flyer Program Data Breach
Date: 2021-03-03T21:15:16+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Privacy;Web Security
Slug: malaysia-air-downplays-frequent-flyer-program-data-breach

[Source](https://threatpost.com/malaysia-air-downplays-data-breach/164472/){:target="_blank" rel="noopener"}

> A third-party IT provider exposed valuable airline data that experts say could be a goldmine for cybercriminals. [...]
