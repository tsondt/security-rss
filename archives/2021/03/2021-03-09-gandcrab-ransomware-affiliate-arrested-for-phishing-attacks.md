Title: GandCrab ransomware affiliate arrested for phishing attacks
Date: 2021-03-09T10:07:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: gandcrab-ransomware-affiliate-arrested-for-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/gandcrab-ransomware-affiliate-arrested-for-phishing-attacks/){:target="_blank" rel="noopener"}

> A suspected GandCrab Ransomware member was arrested in South Korea for using phishing emails to infect victims. [...]
