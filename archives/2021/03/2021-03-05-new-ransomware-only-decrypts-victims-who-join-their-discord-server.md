Title: New ransomware only decrypts victims who join their Discord server
Date: 2021-03-05T16:49:54-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: new-ransomware-only-decrypts-victims-who-join-their-discord-server

[Source](https://www.bleepingcomputer.com/news/security/new-ransomware-only-decrypts-victims-who-join-their-discord-server/){:target="_blank" rel="noopener"}

> A new ransomware called 'Hog' encrypts users' devices and only decrypts them if they join the developer's Discord server. [...]
