Title: VMware fixes bug allowing attackers to steal admin credentials
Date: 2021-03-30T14:01:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: vmware-fixes-bug-allowing-attackers-to-steal-admin-credentials

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-bug-allowing-attackers-to-steal-admin-credentials/){:target="_blank" rel="noopener"}

> VMware has published security updates to address a high severity vulnerability in vRealize Operations that could allow attackers to steal admin credentials after exploiting vulnerable servers. [...]
