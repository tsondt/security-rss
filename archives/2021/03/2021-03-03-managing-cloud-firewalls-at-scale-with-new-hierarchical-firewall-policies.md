Title: Managing cloud firewalls at scale with new Hierarchical Firewall Policies
Date: 2021-03-03T18:30:00+00:00
Author: Tracy Jiang
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: managing-cloud-firewalls-at-scale-with-new-hierarchical-firewall-policies

[Source](https://cloud.google.com/blog/products/identity-security/new-google-cloud-hierarchical-firewall-policies/){:target="_blank" rel="noopener"}

> Following up our previous blog post, we are excited to announce that hierarchical firewalls are generally available. Google Cloud’s hierarchical firewall policies provide new, flexible levels of control so that you can benefit from centralized control at the organization and folder level, while safely delegating more granular control within a project to the project owner. Hierarchical firewall policies Hierarchical firewalls provide a means to enforce firewall rules at the organization and folder levels in the GCP Resource Hierarchy. This allows security administrators at different levels in the hierarchy to define and deploy consistent firewall rules across a number of projects [...]
