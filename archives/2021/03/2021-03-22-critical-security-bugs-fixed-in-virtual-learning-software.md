Title: Critical Security Bugs Fixed in Virtual Learning Software
Date: 2021-03-22T19:01:10+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities
Slug: critical-security-bugs-fixed-in-virtual-learning-software

[Source](https://threatpost.com/security-bugs-virtual-learning-software/164953/){:target="_blank" rel="noopener"}

> Remote ed software bugs give attackers wide access student computers, data. [...]
