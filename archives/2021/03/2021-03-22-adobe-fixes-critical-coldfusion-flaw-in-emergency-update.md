Title: Adobe Fixes Critical ColdFusion Flaw in Emergency Update
Date: 2021-03-22T15:49:59+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: adobe-fixes-critical-coldfusion-flaw-in-emergency-update

[Source](https://threatpost.com/adobe-critical-coldfusion-flaw-update/164946/){:target="_blank" rel="noopener"}

> Attackers can leverage the critical Adobe ColdFusion flaw to launch arbitrary code execution attacks. [...]
