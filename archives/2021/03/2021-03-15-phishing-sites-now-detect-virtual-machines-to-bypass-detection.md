Title: Phishing sites now detect virtual machines to bypass detection
Date: 2021-03-15T13:20:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: phishing-sites-now-detect-virtual-machines-to-bypass-detection

[Source](https://www.bleepingcomputer.com/news/security/phishing-sites-now-detect-virtual-machines-to-bypass-detection/){:target="_blank" rel="noopener"}

> Phishing sites are now using JavaScript to evade detection by checking whether a visitor is browsing the site from a virtual machine or headless device. [...]
