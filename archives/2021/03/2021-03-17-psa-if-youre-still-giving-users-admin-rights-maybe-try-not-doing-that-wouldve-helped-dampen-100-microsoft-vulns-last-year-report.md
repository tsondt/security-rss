Title: PSA: If you're still giving users admin rights, maybe try not doing that. Would've helped dampen 100+ Microsoft vulns last year – report
Date: 2021-03-17T07:45:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: psa-if-youre-still-giving-users-admin-rights-maybe-try-not-doing-that-wouldve-helped-dampen-100-microsoft-vulns-last-year-report

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/17/microsoft_vulns_admin_rights/){:target="_blank" rel="noopener"}

> Limiting access is great though 'patching is the only permanent fix' Access management outfit BeyondTrust has urged organizations to remove admin rights from users, arguing that doing so would have at least mitigated more than 100 vulnerabilities in Microsoft products last year.... [...]
