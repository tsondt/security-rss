Title: City of London Police warn against using ‘open science’ site Sci-Hub
Date: 2021-03-22T04:11:49+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: city-of-london-police-warn-against-using-open-science-site-sci-hub

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/22/city_of_london_police_sci_hub_warning/){:target="_blank" rel="noopener"}

> Pirate papers site is best blocked on grounds it threatens university security The City of London Police, which has responsibility for intellectual property crime across the UK, has warned universities and scientists not to use “open science” site Sci-Hub and called for it to be blocked by universities.... [...]
