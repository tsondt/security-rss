Title: Upcoming Speaking Engagements
Date: 2021-03-14T18:16:00+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Schneier news
Slug: upcoming-speaking-engagements-6

[Source](https://www.schneier.com/blog/archives/2021/03/upcoming-speaking-engagements-6.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at the Australian Cyber Conference 2021 on March 17 and 18, 2021. I’m keynoting the (all-virtual) RSA Conference 2021, May 17-20, 2021. I’ll be speaking at an Informa event on September 14, 2021. Details to come. The list is maintained on this page. [...]
