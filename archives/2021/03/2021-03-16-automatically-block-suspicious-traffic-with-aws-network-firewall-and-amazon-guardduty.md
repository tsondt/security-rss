Title: Automatically block suspicious traffic with AWS Network Firewall and Amazon GuardDuty
Date: 2021-03-16T20:29:13+00:00
Author: Alex Tomic
Category: AWS Security
Tags: Amazon GuardDuty;AWS Network Firewall;AWS Step Functions;Security, Identity, & Compliance;Remediation;Security Blog;step functions
Slug: automatically-block-suspicious-traffic-with-aws-network-firewall-and-amazon-guardduty

[Source](https://aws.amazon.com/blogs/security/automatically-block-suspicious-traffic-with-aws-network-firewall-and-amazon-guardduty/){:target="_blank" rel="noopener"}

> According to the AWS Security Incident Response Guide, by using security response automation, you can increase both the scale and the effectiveness of your security operations. Automation also helps you to adopt a more proactive approach to securing your workloads on AWS. For example, rather than spending time manually reacting to security alerts, you can [...]
