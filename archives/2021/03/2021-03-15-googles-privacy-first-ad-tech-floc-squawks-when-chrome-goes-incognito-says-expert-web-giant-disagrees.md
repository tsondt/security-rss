Title: Google's 'privacy-first' ad tech FLoC squawks when Chrome goes Incognito, says expert. Web giant disagrees
Date: 2021-03-15T22:46:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: googles-privacy-first-ad-tech-floc-squawks-when-chrome-goes-incognito-says-expert-web-giant-disagrees

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/15/google_floc_chrome_incognito/){:target="_blank" rel="noopener"}

> Chocolate Factory still stomping bugs out of its bird-themed post-third-party-cookie features Updated Google's effort at "building a privacy-first future for web advertising" already looks like it will require some privacy retrofitting.... [...]
