Title: While Reg readers know the difference between a true hacker and cyber-crook, for everyone else, hacking means illegal activity
Date: 2021-03-05T11:00:05+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: while-reg-readers-know-the-difference-between-a-true-hacker-and-cyber-crook-for-everyone-else-hacking-means-illegal-activity

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/05/debate_hackers_against/){:target="_blank" rel="noopener"}

> Our vulture Iain argues against this week's motion Reader debate Welcome to the latest Register Debate in which writers discuss technology topics, and you – the reader – choose the winning argument. The format is simple: a motion was proposed this week, the argument for the motion was published on Wednesday, and the argument against is published today.... [...]
