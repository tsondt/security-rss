Title: Penetration testing of enterprise systems more prevalent than ever due to remote working during Covid-19 pandemic
Date: 2021-03-18T14:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: penetration-testing-of-enterprise-systems-more-prevalent-than-ever-due-to-remote-working-during-covid-19-pandemic

[Source](https://portswigger.net/daily-swig/penetration-testing-of-enterprise-systems-more-prevalent-than-ever-due-to-remote-working-during-covid-19-pandemic){:target="_blank" rel="noopener"}

> Cybersecurity staff are being asked to conduct more pen tests – but this still might not be enough [...]
