Title: On Not Fixing Old Vulnerabilities
Date: 2021-03-09T12:16:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: malware;patching;vulnerabilities
Slug: on-not-fixing-old-vulnerabilities

[Source](https://www.schneier.com/blog/archives/2021/03/on-not-fixing-old-vulnerabilities.html){:target="_blank" rel="noopener"}

> How is this even possible?...26% of companies Positive Technologies tested were vulnerable to WannaCry, which was a threat years ago, and some even vulnerable to Heartbleed. “The most frequent vulnerabilities detected during automated assessment date back to 2013­2017, which indicates a lack of recent software updates,” the reported stated. 26%!? One in four networks? Even if we assume that the report is self-serving to the company that wrote it, and that the statistic is not generally representative, this is still a disaster. The number should be 0%. WannaCry was a 2017 cyberattack, based on a NSA-discovered and Russia-stolen-and-published Windows vulnerability. [...]
