Title: F5 urges customers to patch critical BIG-IP pre-auth RCE bug
Date: 2021-03-10T12:04:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: f5-urges-customers-to-patch-critical-big-ip-pre-auth-rce-bug

[Source](https://www.bleepingcomputer.com/news/security/f5-urges-customers-to-patch-critical-big-ip-pre-auth-rce-bug/){:target="_blank" rel="noopener"}

> F5 Networks, a leading provider of enterprise networking gear, has announced four critical remote code execution (RCE) vulnerabilities affecting most versions of BIG-IP and BIG-IQ software. [...]
