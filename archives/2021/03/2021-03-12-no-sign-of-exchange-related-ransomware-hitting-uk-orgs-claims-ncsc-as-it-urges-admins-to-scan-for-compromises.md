Title: No sign of Exchange-related ransomware hitting UK orgs, claims NCSC as it urges admins to scan for compromises
Date: 2021-03-12T18:20:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: no-sign-of-exchange-related-ransomware-hitting-uk-orgs-claims-ncsc-as-it-urges-admins-to-scan-for-compromises

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/12/no_exchange_ransomware_uk_ncsc_hafnium/){:target="_blank" rel="noopener"}

> GCHQ offshoot points orgs at Microsoft advice and tools The UK's National Cyber Security Centre has reminded Brits to patch their Microsoft Exchange Server deployments against Hafnium attacks, 10 days after the US and wider infosec industry shouted the house down saying the same thing.... [...]
