Title: Hades Ransomware Gang Exhibits Connections to Hafnium
Date: 2021-03-29T18:57:53+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Malware
Slug: hades-ransomware-gang-exhibits-connections-to-hafnium

[Source](https://threatpost.com/hades-ransomware-connections-hafnium/165069/){:target="_blank" rel="noopener"}

> There could be more than immediately meets the eye with this targeted attack group. [...]
