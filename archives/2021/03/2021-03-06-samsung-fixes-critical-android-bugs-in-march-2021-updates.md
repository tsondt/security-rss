Title: Samsung fixes critical Android bugs in March 2021 updates
Date: 2021-03-06T09:05:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Mobile
Slug: samsung-fixes-critical-android-bugs-in-march-2021-updates

[Source](https://www.bleepingcomputer.com/news/security/samsung-fixes-critical-android-bugs-in-march-2021-updates/){:target="_blank" rel="noopener"}

> This week Samsung has started rolling out Android's March 2021 security updates to mobile devices to patch critical security vulnerabilities in the runtime, operating system, and related components. Users are advised to update their Android devices immediately to safeguard against these bugs. [...]
