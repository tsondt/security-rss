Title: APT Charming Kitten Pounces on Medical Researchers
Date: 2021-03-31T12:48:58+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks
Slug: apt-charming-kitten-pounces-on-medical-researchers

[Source](https://threatpost.com/charming-kitten-pounces-on-researchers/165129/){:target="_blank" rel="noopener"}

> Researchers uncover a credential-stealing campaign targeting genetic, neurology and oncology professionals. [...]
