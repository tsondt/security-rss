Title: Open source tool SerialDetector speeds up discovery of .Net deserialization bugs
Date: 2021-03-05T12:17:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: open-source-tool-serialdetector-speeds-up-discovery-of-net-deserialization-bugs

[Source](https://portswigger.net/daily-swig/open-source-tool-serialdetector-speeds-up-discovery-of-net-deserialization-bugs){:target="_blank" rel="noopener"}

> The tool has already unearthed critical flaws in Microsoft’s Azure DevOps Server [...]
