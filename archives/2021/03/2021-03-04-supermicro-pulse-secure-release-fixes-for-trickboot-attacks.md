Title: Supermicro, Pulse Secure release fixes for 'TrickBoot' attacks
Date: 2021-03-04T20:14:58-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: supermicro-pulse-secure-release-fixes-for-trickboot-attacks

[Source](https://www.bleepingcomputer.com/news/security/supermicro-pulse-secure-release-fixes-for-trickboot-attacks/){:target="_blank" rel="noopener"}

> Supermicro and Pulse Secure have released advisories warning that some of their motherboards are vulnerable to the TrickBot malware's UEFI firmware-infecting module, known as TrickBoot. [...]
