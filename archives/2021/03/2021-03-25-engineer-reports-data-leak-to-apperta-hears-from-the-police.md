Title: Engineer reports data leak to Apperta, hears from the police
Date: 2021-03-25T04:35:09-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Legal
Slug: engineer-reports-data-leak-to-apperta-hears-from-the-police

[Source](https://www.bleepingcomputer.com/news/security/engineer-reports-data-leak-to-apperta-hears-from-the-police/){:target="_blank" rel="noopener"}

> A security engineer and ex-contributor to the open systems non-profit organization, Apperta Foundation, recently reported a data leak to them. In return, he gets contacted by their lawyers and eventually the police. [...]
