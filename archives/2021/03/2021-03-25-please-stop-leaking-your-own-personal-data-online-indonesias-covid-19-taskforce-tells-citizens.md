Title: Please stop leaking <i>your own</i> personal data online, Indonesia's COVID-19 taskforce tells citizens
Date: 2021-03-25T00:33:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: please-stop-leaking-your-own-personal-data-online-indonesias-covid-19-taskforce-tells-citizens

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/25/indonesia_covid_qr_code/){:target="_blank" rel="noopener"}

> Posting pics of 'I got the jab' certs, complete with their QR codes, has authorities worried Indonesian officials have asked its nation's citizens to stop leaking their own personal data on social media by sharing pictures of certificates attesting to their receipt of COVID-19 vaccinations.... [...]
