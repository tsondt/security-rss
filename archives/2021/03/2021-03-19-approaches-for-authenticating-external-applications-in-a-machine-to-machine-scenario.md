Title: Approaches for authenticating external applications in a machine-to-machine scenario
Date: 2021-03-19T17:54:31+00:00
Author: Patrick Sard
Category: AWS Security
Tags: Best Practices;Foundational (100);Security, Identity, & Compliance;M2M;Machine to machine;Security Blog
Slug: approaches-for-authenticating-external-applications-in-a-machine-to-machine-scenario

[Source](https://aws.amazon.com/blogs/security/approaches-for-authenticating-external-applications-in-a-machine-to-machine-scenario/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) supports multiple authentication mechanisms (AWS Signature v4, OpenID Connect, SAML 2.0, and more), essential in providing secure access to AWS resources. However, in a strictly machine-to machine (m2m) scenario, not all are a good fit. In these cases, a human is not present to provide user credential input. An example of [...]
