Title: In-the-Wild Series: October 2020 0-day discovery
Date: 2021-03-18T09:45:00.001000-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: in-the-wild-series-october-2020-0-day-discovery

[Source](https://googleprojectzero.blogspot.com/2021/03/in-wild-series-october-2020-0-day.html){:target="_blank" rel="noopener"}

> Posted by Maddie Stone, Project Zero In October 2020, Google Project Zero discovered seven 0-day exploits being actively used in-the-wild. These exploits were delivered via "watering hole" attacks in a handful of websites pointing to two exploit servers that hosted exploit chains for Android, Windows, and iOS devices. These attacks appear to be the next iteration of the campaign discovered in February 2020 and documented in this blog post series. In this post we are summarizing the exploit chains we discovered in October 2020. We have already published the details of the seven 0-day vulnerabilities exploited in our root cause [...]
