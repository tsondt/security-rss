Title: Ransomware: Nearly a fifth of victims who pay off extortionists fail to get their data back
Date: 2021-03-30T15:24:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ransomware-nearly-a-fifth-of-victims-who-pay-off-extortionists-fail-to-get-their-data-back

[Source](https://portswigger.net/daily-swig/ransomware-nearly-a-fifth-of-victims-who-pay-off-extortionists-fail-to-get-their-data-back){:target="_blank" rel="noopener"}

> Double jeopardy [...]
