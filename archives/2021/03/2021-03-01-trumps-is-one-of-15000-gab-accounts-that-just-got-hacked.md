Title: Trump’s is one of 15,000 Gab accounts that just got hacked
Date: 2021-03-01T23:14:21+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;ddosecrets;gab;hacking;hate speech;leaks
Slug: trumps-is-one-of-15000-gab-accounts-that-just-got-hacked

[Source](https://arstechnica.com/?p=1746241){:target="_blank" rel="noopener"}

> Enlarge (credit: Gab.com ) The founder of the far-right social media platform Gab said that the private account of former President Donald Trump was among the data stolen and publicly released by hackers who recently breached the site. In a statement on Sunday, founder Andrew Torba used a transphobic slur to refer to Emma Best, the co-founder of Distributed Denial of Secrets. The statement confirmed claims the WikiLeaks-style group made on Monday that it obtained 70GB of passwords, private posts, and more from Gab and was making them available to select researchers and journalists. The data, Best said, was provided [...]
