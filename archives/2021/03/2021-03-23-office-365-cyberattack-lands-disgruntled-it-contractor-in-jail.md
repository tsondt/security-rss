Title: Office 365 Cyberattack Lands Disgruntled IT Contractor in Jail
Date: 2021-03-23T20:05:30+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Breach;Government;Web Security
Slug: office-365-cyberattack-lands-disgruntled-it-contractor-in-jail

[Source](https://threatpost.com/office-365-cyberattack-disgruntled-contractor-jail/164986/){:target="_blank" rel="noopener"}

> A former IT contractor is facing jailtime after a retaliatory hack into a company’s network and wiping the majority of its employees’ Microsoft Office 365 accounts. [...]
