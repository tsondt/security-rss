Title: Apple Plugs Severe WebKit Remote Code-Execution Hole
Date: 2021-03-09T15:58:15+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;Web Security
Slug: apple-plugs-severe-webkit-remote-code-execution-hole

[Source](https://threatpost.com/apple-webkit-remote-code-execution/164595/){:target="_blank" rel="noopener"}

> Apple pushed out security updates for a memory-corruption bug to devices running on iOS, macOS, watchOS and for Safari. [...]
