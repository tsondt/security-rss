Title: Chinese nation state hackers linked to Finnish Parliament hack
Date: 2021-03-18T09:10:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: chinese-nation-state-hackers-linked-to-finnish-parliament-hack

[Source](https://www.bleepingcomputer.com/news/security/chinese-nation-state-hackers-linked-to-finnish-parliament-hack/){:target="_blank" rel="noopener"}

> Chinese nation-state hackers have been linked to an attack on the Parliament of Finland that took place last year and led to the compromise of some parliament email accounts. [...]
