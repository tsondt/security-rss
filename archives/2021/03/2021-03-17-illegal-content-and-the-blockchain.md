Title: Illegal Content and the Blockchain
Date: 2021-03-17T11:10:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: bitcoin;blockchain;botnets;censorship;cryptocurrency;economics of security;essays;Tor
Slug: illegal-content-and-the-blockchain

[Source](https://www.schneier.com/blog/archives/2021/03/illegal-content-and-the-blockchain.html){:target="_blank" rel="noopener"}

> Security researchers have recently discovered a botnet with a novel defense against takedowns. Normally, authorities can disable a botnet by taking over its command-and-control server. With nowhere to go for instructions, the botnet is rendered useless. But over the years, botnet designers have come up with ways to make this counterattack harder. Now the content-delivery network Akamai has reported on a new method: a botnet that uses the Bitcoin blockchain ledger. Since the blockchain is globally accessible and hard to take down, the botnet’s operators appear to be safe. It’s best to avoid explaining the mathematics of Bitcoin’s blockchain, but [...]
