Title: Twitter images can be abused to hide ZIP, MP3 files — here's how
Date: 2021-03-17T10:50:04-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: twitter-images-can-be-abused-to-hide-zip-mp3-files-heres-how

[Source](https://www.bleepingcomputer.com/news/security/twitter-images-can-be-abused-to-hide-zip-mp3-files-heres-how/){:target="_blank" rel="noopener"}

> Yesterday, a researcher disclosed a method of hiding up to three MB of data inside a Twitter image. In his demonstration, the researcher showed both MP3 audio files and ZIP archives contained within the PNG images hosted on Twitter. [...]
