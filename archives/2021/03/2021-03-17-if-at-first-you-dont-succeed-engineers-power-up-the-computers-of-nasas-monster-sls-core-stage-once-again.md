Title: If at first you don't succeed: Engineers power up the computers of NASA's monster SLS core stage once again
Date: 2021-03-17T17:51:04+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: if-at-first-you-dont-succeed-engineers-power-up-the-computers-of-nasas-monster-sls-core-stage-once-again

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/17/sls_green_run_2/){:target="_blank" rel="noopener"}

> Second hot-fire scheduled for (checks weather) tomorrow... maybe NASA has fired up the avionics of the Artemis I core stage ahead of tomorrow's planned redo of the prematurely terminated hotfire test.... [...]
