Title: Blender website in maintenance mode after hacking attempt
Date: 2021-03-15T15:24:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: blender-website-in-maintenance-mode-after-hacking-attempt

[Source](https://www.bleepingcomputer.com/news/security/blender-website-in-maintenance-mode-after-hacking-attempt/){:target="_blank" rel="noopener"}

> Blender.org, the official website of the popular 3D computer graphics software Blender, is now in maintenance mode according to a message displayed on the site. [...]
