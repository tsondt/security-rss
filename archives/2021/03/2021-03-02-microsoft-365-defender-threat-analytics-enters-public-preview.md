Title: Microsoft 365 Defender Threat Analytics enters public preview
Date: 2021-03-02T09:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-365-defender-threat-analytics-enters-public-preview

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-defender-threat-analytics-enters-public-preview/){:target="_blank" rel="noopener"}

> Microsoft announced the addition of Threat Analytics for Microsoft 365 Defender customers and the roll-out of Microsoft 365 Insider Risk Management Analytics, both in public preview. [...]
