Title: Nzyme: A WiFi defense system for detecting ‘bandit’ devices
Date: 2021-03-30T09:56:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nzyme-a-wifi-defense-system-for-detecting-bandit-devices

[Source](https://portswigger.net/daily-swig/nzyme-a-wifi-defense-system-for-detecting-bandit-devices){:target="_blank" rel="noopener"}

> Platform aims to shore up lax wireless security and eradicate WiFi spoofing attacks [...]
