Title: Bitcoin exchange Sovryn launches record $1.25m bug bounty program
Date: 2021-03-11T11:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: bitcoin-exchange-sovryn-launches-record-125m-bug-bounty-program

[Source](https://portswigger.net/daily-swig/bitcoin-exchange-sovryn-launches-record-1-25m-bug-bounty-program){:target="_blank" rel="noopener"}

> Hackers are asked to find security vulnerabilities to better secure user funds [...]
