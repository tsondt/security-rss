Title: Swiss hacker charged for leaking proprietary source code
Date: 2021-03-19T08:56:20-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: swiss-hacker-charged-for-leaking-proprietary-source-code

[Source](https://www.bleepingcomputer.com/news/security/swiss-hacker-charged-for-leaking-proprietary-source-code/){:target="_blank" rel="noopener"}

> Swiss national Till Kottmann, 21, has been charged for conspiracy, wire fraud and aggravated identity theft, the U.S. Department of Justice announced. [...]
