Title: Microsoft releases one-click Exchange On-Premises Mitigation Tool
Date: 2021-03-15T20:13:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-releases-one-click-exchange-on-premises-mitigation-tool

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-releases-one-click-exchange-on-premises-mitigation-tool/){:target="_blank" rel="noopener"}

> Microsoft has released a one-click Exchange On-premises Mitigation Tool (EOMT) tool to allow small business owners to easily mitigate the recently disclosed ProxyLogon vulnerabilities. [...]
