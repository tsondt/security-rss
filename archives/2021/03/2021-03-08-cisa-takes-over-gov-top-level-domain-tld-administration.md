Title: CISA takes over .GOV top-level domain (TLD) administration
Date: 2021-03-08T09:56:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Government
Slug: cisa-takes-over-gov-top-level-domain-tld-administration

[Source](https://www.bleepingcomputer.com/news/security/cisa-takes-over-gov-top-level-domain-tld-administration/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) is taking over the administration of the.gov top-level domain (TLD) as its new policy and management authority. [...]
