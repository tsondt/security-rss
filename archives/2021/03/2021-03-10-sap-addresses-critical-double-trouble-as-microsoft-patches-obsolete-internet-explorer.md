Title: SAP addresses critical double trouble as Microsoft patches obsolete Internet Explorer
Date: 2021-03-10T16:43:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: sap-addresses-critical-double-trouble-as-microsoft-patches-obsolete-internet-explorer

[Source](https://portswigger.net/daily-swig/sap-addresses-critical-double-trouble-as-microsoft-patches-obsolete-internet-explorer){:target="_blank" rel="noopener"}

> Patch Tuesday meets March Madness [...]
