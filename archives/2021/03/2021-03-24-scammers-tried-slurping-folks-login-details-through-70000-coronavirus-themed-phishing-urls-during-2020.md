Title: Scammers tried slurping folks' login details through 70,000 coronavirus-themed phishing URLs during 2020
Date: 2021-03-24T10:24:30+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: scammers-tried-slurping-folks-login-details-through-70000-coronavirus-themed-phishing-urls-during-2020

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/24/covid_phishing_2020_palo_alto_networks_research/){:target="_blank" rel="noopener"}

> Palo Alto Networks lays bare a year of dastardly digital doings Cybercriminals ruthlessly exploited the coronavirus pandemic to set up phishing websites that posed as Pfizer, BioNTech and other household-name suppliers of vaccines and PPE, according to Palo Alto Networks.... [...]
