Title: Three Top Russian Cybercrime Forums Hacked
Date: 2021-03-04T15:01:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Exploit hack;Intel 471;Maza;Mazafaka hack;MFclub;Verified hack
Slug: three-top-russian-cybercrime-forums-hacked

[Source](https://krebsonsecurity.com/2021/03/three-top-russian-cybercrime-forums-hacked/){:target="_blank" rel="noopener"}

> Over the past few weeks, three of the longest running and most venerated Russian-language online forums serving thousands of experienced cybercriminals have been hacked. In two of the intrusions, the attackers made off with the forums’ user databases, including email and Internet addresses and hashed passwords. Members of all three forums are worried the incidents could serve as a virtual Rosetta Stone for connecting the real-life identities of the same users across multiple crime forums. References to the leaked Mazafaka crime forum database were posted online in the past 48 hours. On Tuesday, someone dumped thousands of usernames, email addresses [...]
