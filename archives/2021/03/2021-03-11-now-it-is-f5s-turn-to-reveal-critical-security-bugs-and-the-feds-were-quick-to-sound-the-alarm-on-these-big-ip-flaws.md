Title: Now it is F5’s turn to reveal critical security bugs – and the Feds were quick to sound the alarm on these BIG-IP flaws
Date: 2021-03-11T02:03:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: now-it-is-f5s-turn-to-reveal-critical-security-bugs-and-the-feds-were-quick-to-sound-the-alarm-on-these-big-ip-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/11/f5_critical_flaws/){:target="_blank" rel="noopener"}

> Remote code execution, denial of service, API abuse possible. Meanwhile, FBI pegs China for Exchange hacks Security and automation vendor F5 has warned of seven patch-ASAP-grade vulnerabilities in its Big-IP network security and traffic-grooming products, plus another 14 vulns worth fixing.... [...]
