Title: iPhone Call Recorder bug gave acess to other people's conversations
Date: 2021-03-09T19:05:30-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: iphone-call-recorder-bug-gave-acess-to-other-peoples-conversations

[Source](https://www.bleepingcomputer.com/news/security/iphone-call-recorder-bug-gave-acess-to-other-peoples-conversations/){:target="_blank" rel="noopener"}

> An iOS call recording app patched a security vulnerability that gave anyone access to the conversations of thousands of users by simply providing the correct phone numbers. [...]
