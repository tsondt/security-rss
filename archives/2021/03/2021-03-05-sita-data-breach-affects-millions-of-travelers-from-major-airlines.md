Title: SITA data breach affects millions of travelers from major airlines
Date: 2021-03-05T14:13:45-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: sita-data-breach-affects-millions-of-travelers-from-major-airlines

[Source](https://www.bleepingcomputer.com/news/security/sita-data-breach-affects-millions-of-travelers-from-major-airlines/){:target="_blank" rel="noopener"}

> Passenger data from multiple airlines around the world has been compromised after hackers breached servers belonging to SITA, a global information technology company. [...]
