Title: Researcher adds his fake package to Microsoft Azure SDK releases list
Date: 2021-03-17T04:01:03-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Cloud;Software
Slug: researcher-adds-his-fake-package-to-microsoft-azure-sdk-releases-list

[Source](https://www.bleepingcomputer.com/news/security/researcher-adds-his-fake-package-to-microsoft-azure-sdk-releases-list/){:target="_blank" rel="noopener"}

> A security researcher was able to add a counterfeit test package to the official list of Microsoft Azure SDK latest releases. The simple trick if abused by an attacker can give off the impression that their malicious package is part of the Azure SDK suite. [...]
