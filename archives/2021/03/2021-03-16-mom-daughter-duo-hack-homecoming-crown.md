Title: Mom & Daughter Duo Hack Homecoming Crown
Date: 2021-03-16T20:27:31+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Privacy;Web Security
Slug: mom-daughter-duo-hack-homecoming-crown

[Source](https://threatpost.com/mom-daughter-duo-hack-homecoming-crown/164829/){:target="_blank" rel="noopener"}

> A Florida high-school student faces jail time for rigging her school's Homecoming Queen election. [...]
