Title: Hobby Lobby Exposes Customer Data in Cloud Misconfiguration
Date: 2021-03-23T19:46:43+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Privacy
Slug: hobby-lobby-exposes-customer-data-in-cloud-misconfiguration

[Source](https://threatpost.com/hobby-lobby-customer-data-cloud-misconfiguration/164980/){:target="_blank" rel="noopener"}

> The arts-and-crafts retailer left 138GB of sensitive information open to the public internet. [...]
