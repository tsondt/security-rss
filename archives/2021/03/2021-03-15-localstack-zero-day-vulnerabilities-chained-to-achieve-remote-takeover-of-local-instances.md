Title: LocalStack zero-day vulnerabilities chained to achieve remote takeover of local instances
Date: 2021-03-15T13:55:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: localstack-zero-day-vulnerabilities-chained-to-achieve-remote-takeover-of-local-instances

[Source](https://portswigger.net/daily-swig/localstack-zero-day-vulnerabilities-chained-to-achieve-remote-takeover-of-local-instances){:target="_blank" rel="noopener"}

> Project maintainers reportedly declined to fix flaws due to limited attack scenarios [...]
