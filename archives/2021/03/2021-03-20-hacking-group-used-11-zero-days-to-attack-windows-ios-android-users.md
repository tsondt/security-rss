Title: Hacking group used 11 zero-days to attack Windows, iOS, Android users
Date: 2021-03-20T10:41:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hacking-group-used-11-zero-days-to-attack-windows-ios-android-users

[Source](https://www.bleepingcomputer.com/news/security/hacking-group-used-11-zero-days-to-attack-windows-ios-android-users/){:target="_blank" rel="noopener"}

> Project Zero, Google's zero-day bug-hunting team, discovered a group of hackers that used 11 zero-days in attacks targeting Windows, iOS, and Android users within a single year. [...]
