Title: Introducing Cloud Code Secret Manager Integration
Date: 2021-03-09T16:00:00+00:00
Author: Abby Carey
Category: GCP Security
Tags: Identity & Security;Developers & Practitioners
Slug: introducing-cloud-code-secret-manager-integration

[Source](https://cloud.google.com/blog/topics/developers-practitioners/introducing-cloud-code-secret-manager-integration/){:target="_blank" rel="noopener"}

> Storing secrets like database credentials and passwords in code is never secure. Wouldn't it be great if your IDE tool could help you write more secure code? That's why we’re excited to announce the new Cloud Code integration with Secret Manager! Today, many applications require credentials to connect to a database, API keys to invoke a service, or certificates for authentication. Managing and securing access to these secrets is often complicated by secret sprawl, poor visibility, or lack of integrations. To help you build more secure applications, without the hassle of figuring out complicated ways to store you secrets, we [...]
