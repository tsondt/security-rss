Title: European e-ticketing platform Ticketcounter extorted in data breach
Date: 2021-03-01T17:35:35-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: european-e-ticketing-platform-ticketcounter-extorted-in-data-breach

[Source](https://www.bleepingcomputer.com/news/security/european-e-ticketing-platform-ticketcounter-extorted-in-data-breach/){:target="_blank" rel="noopener"}

> A Dutch e-Ticketing platform has suffered a data breach after a user database containing 1.9 million unique email addresses was stolen from an unsecured staging server. [...]
