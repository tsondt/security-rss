Title: US National Security Council urges review of Exchange Servers in wake of Hafnium attack
Date: 2021-03-08T04:58:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: us-national-security-council-urges-review-of-exchange-servers-in-wake-of-hafnium-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/08/us_national_security_council_says/){:target="_blank" rel="noopener"}

> Don't just patch, check for p0wnage, says top natsec team The Biden administration has urged users of Microsoft's Exchange mail and messaging server to ensure they have not fallen victim to the recently-detected "Hafnium" attack on Exchange Server that Microsoft says originated in China.... [...]
