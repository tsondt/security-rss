Title: Ziggy Ransomware Gang Offers Refunds to Victims
Date: 2021-03-30T20:31:44+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware
Slug: ziggy-ransomware-gang-offers-refunds-to-victims

[Source](https://threatpost.com/ziggy-ransomware-gang-offers-refund-to-victims/165124/){:target="_blank" rel="noopener"}

> Ziggy joins Fonix ransomware group and shuts down, with apologies to targets. [...]
