Title: Scammers target universities in ongoing IRS phishing attacks
Date: 2021-03-30T12:43:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: scammers-target-universities-in-ongoing-irs-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/scammers-target-universities-in-ongoing-irs-phishing-attacks/){:target="_blank" rel="noopener"}

> The Internal Revenue Service (IRS) is warning of ongoing phishing attacks impersonating the IRS and targeting educational institutions. [...]
