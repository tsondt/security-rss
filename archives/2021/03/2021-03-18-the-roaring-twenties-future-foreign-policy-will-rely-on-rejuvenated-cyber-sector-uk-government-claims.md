Title: The Roaring Twenties: Future foreign policy will rely on rejuvenated 'cyber' sector, UK government claims
Date: 2021-03-18T09:30:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: the-roaring-twenties-future-foreign-policy-will-rely-on-rejuvenated-cyber-sector-uk-government-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/18/ukgov_integrated_review_cyber/){:target="_blank" rel="noopener"}

> Good news for Mancunian infosec and chip design bods, but we're raising an eyebrow on the nukes The British government has published its Integrated Review into defence and security policy – and though you'll like it if you're in the UK infosec industry, threats of nuking North Korea in revenge for WannaCry are very wide of the mark.... [...]
