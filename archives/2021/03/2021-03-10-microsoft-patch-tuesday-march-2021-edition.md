Title: Microsoft Patch Tuesday, March 2021 Edition
Date: 2021-03-10T01:42:39+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other;AskWoody.com;CVE-2021-26411;Dustin Childs;Exchange Server hack;Microsoft Patch Tuesday March 2021;Satnam Narang;Tenable;Windows DNS
Slug: microsoft-patch-tuesday-march-2021-edition

[Source](https://krebsonsecurity.com/2021/03/microsoft-patch-tuesday-march-2021-edition/){:target="_blank" rel="noopener"}

> On the off chance you were looking for more security to-dos from Microsoft today...the company released software updates to plug more than 82 security flaws in Windows and other supported software. Ten of these earned Microsoft’s “critical” rating, meaning they can be exploited by malware or miscreants with little or no help from users. Top of the heap this month (apart from the ongoing, global Exchange Server mass-compromise ) is a patch for an Internet Explorer bug that is seeing active exploitation. The IE weakness — CVE-2021-26411 — affects both IE11 and newer EdgeHTML-based versions, and it allows attackers to [...]
