Title: Hackers backdoor PHP source code after breaching internal git server
Date: 2021-03-29T19:19:23+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;GitHub;hacking;php;supply chain attacks;website security
Slug: hackers-backdoor-php-source-code-after-breaching-internal-git-server

[Source](https://arstechnica.com/?p=1752909){:target="_blank" rel="noopener"}

> Enlarge (credit: BeeBright / Getty Images / iStockphoto ) A hacker compromised the server used to distribute the PHP programming language and added a backdoor to source code that would have made websites vulnerable to complete takeover, members of the open source project said. Two updates pushed to the PHP Git server over the weekend added a line that, if run by a PHP-powered website, would have allowed visitors with no authorization to execute code of their choice. The malicious commits here and here gave the code the code-injection capability to visitors who had the word “zerodium” in an HTTP [...]
