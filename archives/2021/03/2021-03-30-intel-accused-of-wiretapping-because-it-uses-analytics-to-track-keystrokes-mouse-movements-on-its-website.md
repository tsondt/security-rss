Title: Intel accused of wiretapping because it uses analytics to track keystrokes, mouse movements on its website
Date: 2021-03-30T00:46:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: intel-accused-of-wiretapping-because-it-uses-analytics-to-track-keystrokes-mouse-movements-on-its-website

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/30/intel_wiretapping_data/){:target="_blank" rel="noopener"}

> Session monitoring scripts prompt dozens of privacy lawsuits against Big Biz, mainly in California and Florida Intel is among the growing list of companies being sued for allegedly violating American wiretapping laws by running third-party code to track interactions, such as keystrokes, click events, and cursor movements, on its website.... [...]
