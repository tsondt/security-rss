Title: Demystifying KMS keys operations, bring your own key (BYOK), custom key store, and ciphertext portability
Date: 2021-03-12T20:39:18+00:00
Author: Arthur Mnev
Category: AWS Security
Tags: AWS CloudHSM;AWS Key Management Service;Intermediate (200);Security, Identity, & Compliance;AWS KMS;BYOK;FIPS compliance;Security Blog
Slug: demystifying-kms-keys-operations-bring-your-own-key-byok-custom-key-store-and-ciphertext-portability

[Source](https://aws.amazon.com/blogs/security/demystifying-kms-keys-operations-bring-your-own-key-byok-custom-key-store-and-ciphertext-portability/){:target="_blank" rel="noopener"}

> As you prepare to build or migrate your workload on Amazon Web Services (AWS), designing your encryption scheme can be a challenging—and sometimes confusing—endeavor. This blog post gives you a framework to select the right AWS cryptographic services and tools for your application to help you with your journey. I share common repeatable cryptographic patterns, [...]
