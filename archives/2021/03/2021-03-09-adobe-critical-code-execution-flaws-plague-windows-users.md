Title: Adobe Critical Code-Execution Flaws Plague Windows Users
Date: 2021-03-09T20:44:18+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: adobe-critical-code-execution-flaws-plague-windows-users

[Source](https://threatpost.com/adobe-critical-flaws-windows/164611/){:target="_blank" rel="noopener"}

> The critical flaws exist in Adobe Framemaker, Connect and the Creative Cloud desktop application for Windows. [...]
