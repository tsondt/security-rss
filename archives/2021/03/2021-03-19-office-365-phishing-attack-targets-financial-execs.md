Title: Office 365 Phishing Attack Targets Financial Execs
Date: 2021-03-19T18:18:52+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: office-365-phishing-attack-targets-financial-execs

[Source](https://threatpost.com/office-365-phishing-attack-financial-execs/164925/){:target="_blank" rel="noopener"}

> Attackers move on new CEOs, using transition confusion to harvest Microsoft credentials. [...]
