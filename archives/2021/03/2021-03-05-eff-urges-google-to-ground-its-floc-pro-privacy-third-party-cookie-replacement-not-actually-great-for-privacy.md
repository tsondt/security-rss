Title: EFF urges Google to ground its FLoC: 'Pro-privacy' third-party cookie replacement not actually great for privacy
Date: 2021-03-05T21:18:41+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: eff-urges-google-to-ground-its-floc-pro-privacy-third-party-cookie-replacement-not-actually-great-for-privacy

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/05/eff_google_floc/){:target="_blank" rel="noopener"}

> 'That is not the world we want, nor the one users deserve' With the arrival of Google Chrome v89 on Tuesday, Google is preparing to test a technology called Federated Learning of Cohorts, or FLoC, that it hopes will replace increasingly shunned, privacy-denying third-party cookies.... [...]
