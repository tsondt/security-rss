Title: Dark Web Markets for Stolen Data See Banner Sales
Date: 2021-03-09T21:59:30+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Malware;Privacy;Vulnerabilities
Slug: dark-web-markets-for-stolen-data-see-banner-sales

[Source](https://threatpost.com/dark-web-markets-stolen-data/164626/){:target="_blank" rel="noopener"}

> Despite an explosion in the sheer amount of stolen data available on the Dark Web, the value of personal information is holding steady, according to the 2021 Dark Web price index from Privacy Affairs. That leaves these thriving dirty data dealers in a familiar predicament — they need to lock down their growing businesses for [...]
