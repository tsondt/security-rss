Title: Exchange Cyberattacks Escalate as Microsoft Rolls One-Click Fix
Date: 2021-03-16T16:56:26+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security
Slug: exchange-cyberattacks-escalate-as-microsoft-rolls-one-click-fix

[Source](https://threatpost.com/microsoft-exchange-cyberattacks-one-click-fix/164817/){:target="_blank" rel="noopener"}

> Public proof-of-concept (PoC) exploits for ProxyLogon could be fanning a feeding frenzy of attacks even as patching makes progress. [...]
