Title: Fake Ad Blocker Delivers Hybrid Cryptominer/Ransomware Infection
Date: 2021-03-10T21:44:55+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware
Slug: fake-ad-blocker-delivers-hybrid-cryptominerransomware-infection

[Source](https://threatpost.com/fake-ad-blocker-cryptominer-ransomware/164669/){:target="_blank" rel="noopener"}

> A hybrid Monero cryptominer and ransomware bug has hit 20,000 machines in 60 days. [...]
