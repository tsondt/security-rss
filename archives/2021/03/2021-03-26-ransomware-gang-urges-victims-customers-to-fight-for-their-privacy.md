Title: Ransomware gang urges victims’ customers to fight for their privacy
Date: 2021-03-26T15:42:58-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-urges-victims-customers-to-fight-for-their-privacy

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-urges-victims-customers-to-fight-for-their-privacy/){:target="_blank" rel="noopener"}

> A ransomware operation known as 'Clop' is applying maximum pressure on victims by emailing their customers and asking them to demand a ransom payment to protect their privacy. [...]
