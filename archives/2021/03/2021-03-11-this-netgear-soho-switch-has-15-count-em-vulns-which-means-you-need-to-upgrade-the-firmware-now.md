Title: This Netgear SOHO switch has 15 – count 'em! – vulns, which means you need to upgrade the firmware... now
Date: 2021-03-11T17:59:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: this-netgear-soho-switch-has-15-count-em-vulns-which-means-you-need-to-upgrade-the-firmware-now

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/11/netgear_jgs516pe_switch_15_vulns/){:target="_blank" rel="noopener"}

> One of them is a critical RCE bug Netgear has released a swathe of security and firmware updates for its JGS516PE Ethernet switch after researchers from NCC Group discovered 15 vulnerabilities in the device – including an unauthenticated remote code execution flaw.... [...]
