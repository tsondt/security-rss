Title: Google Chrome to block port 554 to stop NAT Slipstreaming attacks
Date: 2021-03-08T13:21:05-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Software
Slug: google-chrome-to-block-port-554-to-stop-nat-slipstreaming-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-to-block-port-554-to-stop-nat-slipstreaming-attacks/){:target="_blank" rel="noopener"}

> Google Chrome will block the browser's access to TCP port 554 to protect against attacks using the NAT Slipstreaming 2.0 vulnerability. [...]
