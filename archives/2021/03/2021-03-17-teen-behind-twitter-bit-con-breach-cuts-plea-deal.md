Title: Teen Behind Twitter Bit-Con Breach Cuts Plea Deal
Date: 2021-03-17T20:26:30+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Government;Hacks;Web Security
Slug: teen-behind-twitter-bit-con-breach-cuts-plea-deal

[Source](https://threatpost.com/teen-twitter-breach-plea-deal/164863/){:target="_blank" rel="noopener"}

> The ‘young mastermind’ of the Twitter hack will serve three years in juvenile detention. [...]
