Title: REvil Group Claims Slew of Ransomware Attacks
Date: 2021-03-12T21:05:14+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware
Slug: revil-group-claims-slew-of-ransomware-attacks

[Source](https://threatpost.com/revil-claims-ransomware-attacks/164739/){:target="_blank" rel="noopener"}

> The threat group behind the Sodinokibi ransomware claimed to have recently compromised nine organizations. [...]
