Title: GitHub users forcibly logged out of accounts to patch ‘potentially serious’ security bug
Date: 2021-03-09T12:07:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: github-users-forcibly-logged-out-of-accounts-to-patch-potentially-serious-security-bug

[Source](https://portswigger.net/daily-swig/github-users-forcibly-logged-out-of-accounts-to-patch-potentially-serious-security-bug){:target="_blank" rel="noopener"}

> Vulnerability has the potential to expose users’ session cookies [...]
