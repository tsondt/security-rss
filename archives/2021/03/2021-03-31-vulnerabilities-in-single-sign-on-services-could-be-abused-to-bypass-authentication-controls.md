Title: Vulnerabilities in Single Sign-On services could be abused to bypass authentication controls
Date: 2021-03-31T14:37:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: vulnerabilities-in-single-sign-on-services-could-be-abused-to-bypass-authentication-controls

[Source](https://portswigger.net/daily-swig/vulnerabilities-in-single-sign-on-services-could-be-abused-to-bypass-authentication-controls){:target="_blank" rel="noopener"}

> SAML XML injection gives attackers free rein over user accounts, although hard-to-execute bug proves real-world threat is minimal [...]
