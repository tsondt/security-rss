Title: Outsourced techie gets 2-year sentence after trashing system of former client: 1,200 Office 365 accounts zapped
Date: 2021-03-23T16:33:09+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: outsourced-techie-gets-2-year-sentence-after-trashing-system-of-former-client-1200-office-365-accounts-zapped

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/23/outsourced_techie_2_years/){:target="_blank" rel="noopener"}

> 'In my 30-plus years as an IT pro, I have never been a part of a more difficult work situation,' says victim A California federal court has sentenced a "vengeful" techie to two years in the clink after he deleted 1,200 Microsoft user accounts belonging to a client.... [...]
