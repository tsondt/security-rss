Title: Transform data to secure it: Use Cloud DLP
Date: 2021-03-11T17:00:00+00:00
Author: Scott Ellis
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: transform-data-to-secure-it-use-cloud-dlp

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-dlp-can-modify-data-to-protect-it/){:target="_blank" rel="noopener"}

> When you want to protect data in-motion, at rest or in use, you usually think about data discovery, data loss detection and prevention. Few would immediately consider transforming or modifying data in order to protect it. But doing so can be a powerful and relatively easy tactic to prevent data loss. Our data security vision includes transforming data to secure it, and that's why our DLP product includes powerful data transformation capabilities. So what are some data modification techniques that you can use to protect your data and the use cases for them? Delete sensitive elements Let’s start with a [...]
