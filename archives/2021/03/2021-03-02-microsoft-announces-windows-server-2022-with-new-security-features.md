Title: Microsoft announces Windows Server 2022 with new security features
Date: 2021-03-02T09:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-announces-windows-server-2022-with-new-security-features

[Source](https://www.bleepingcomputer.com/news/security/microsoft-announces-windows-server-2022-with-new-security-features/){:target="_blank" rel="noopener"}

> Microsoft says that Windows Server 2022 will come with security improvements and will bring Secured-core to the Windows Server platform for added protection against a wide range of threats. [...]
