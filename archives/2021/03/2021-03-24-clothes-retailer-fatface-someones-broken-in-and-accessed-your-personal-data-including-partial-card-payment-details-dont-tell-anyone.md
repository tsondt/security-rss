Title: Clothes retailer Fatface: Someone's broken in and accessed your personal data, including partial card payment details... Don't tell anyone
Date: 2021-03-24T13:02:59+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: clothes-retailer-fatface-someones-broken-in-and-accessed-your-personal-data-including-partial-card-payment-details-dont-tell-anyone

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/24/fatface/){:target="_blank" rel="noopener"}

> 'Strictly private and confidential'? SERIOUSLY? British clothes retailer Fatface has infuriated some customers by telling them "an unauthorised third party" gained access to systems holding their data earlier this year, and then asking them to keep news of the blunder to themselves.... [...]
