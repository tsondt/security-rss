Title: Oxfam Australia confirms data breach after supporters’ details ‘unlawfully accessed’
Date: 2021-03-02T13:44:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: oxfam-australia-confirms-data-breach-after-supporters-details-unlawfully-accessed

[Source](https://portswigger.net/daily-swig/oxfam-australia-confirms-data-breach-after-supporters-details-unlawfully-accessed){:target="_blank" rel="noopener"}

> Charity said donors’ personal information was exposed [...]
