Title: H2C smuggling proves effective against Azure, Cloudflare Access, and more
Date: 2021-03-24T16:55:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: h2c-smuggling-proves-effective-against-azure-cloudflare-access-and-more

[Source](https://portswigger.net/daily-swig/h2c-smuggling-proves-effective-against-azure-cloudflare-access-and-more){:target="_blank" rel="noopener"}

> However, Google Cloud Platform repelled security researchers’ attack [...]
