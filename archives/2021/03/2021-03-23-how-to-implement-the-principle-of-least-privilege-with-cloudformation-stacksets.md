Title: How to implement the principle of least privilege with CloudFormation StackSets
Date: 2021-03-23T18:53:10+00:00
Author: Joel Knight
Category: AWS Security
Tags: AWS CloudFormation;Intermediate (200);Security, Identity, & Compliance;least privilege;Security Blog;Well-Architected
Slug: how-to-implement-the-principle-of-least-privilege-with-cloudformation-stacksets

[Source](https://aws.amazon.com/blogs/security/how-to-implement-the-principle-of-least-privilege-with-cloudformation-stacksets/){:target="_blank" rel="noopener"}

> AWS CloudFormation is a service that lets you create a collection of related Amazon Web Services and third-party resources and provision them in an orderly and predictable fashion. A typical access control pattern is to delegate permissions for users to interact with CloudFormation and remove or limit their permissions to provision resources directly. You can [...]
