Title: Metamorfo Banking Trojan Abuses AutoHotKey to Avoid Detection
Date: 2021-03-12T17:21:22+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: metamorfo-banking-trojan-abuses-autohotkey-to-avoid-detection

[Source](https://threatpost.com/metamorfo-banking-trojan-autohotkey/164735/){:target="_blank" rel="noopener"}

> A legitimate binary for creating shortcut keys in Windows is being used to help the malware sneak past defenses, in a rash of new campaigns. [...]
