Title: PHP's Git server hacked to add backdoors to PHP source code
Date: 2021-03-29T03:32:59-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: phps-git-server-hacked-to-add-backdoors-to-php-source-code

[Source](https://www.bleepingcomputer.com/news/security/phps-git-server-hacked-to-add-backdoors-to-php-source-code/){:target="_blank" rel="noopener"}

> In the latest software supply chain attack, the official PHP Git repository was hacked and tampered with. Yesterday, two malicious commits were pushed to the php-src Git repository maintained by the PHP team on their git.php.net server. The threat actors had signed off on these commits as if they were made by known PHP developers. [...]
