Title: CNA insurance firm hit by a cyberattack, operations impacted
Date: 2021-03-23T19:33:33-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: cna-insurance-firm-hit-by-a-cyberattack-operations-impacted

[Source](https://www.bleepingcomputer.com/news/security/cna-insurance-firm-hit-by-a-cyberattack-operations-impacted/){:target="_blank" rel="noopener"}

> CNA Financial, a leading US-based insurance company, has suffered a cyberattack impacting its business operations and shutting down its website. [...]
