Title: Intel Sued Under Wiretapping Laws for Tracking User Activity on its Website
Date: 2021-03-30T12:49:12+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy
Slug: intel-sued-under-wiretapping-laws-for-tracking-user-activity-on-its-website

[Source](https://threatpost.com/intel-sued-under-wiretapping-laws/165104/){:target="_blank" rel="noopener"}

> A class-action suit in Florida accuses the tech giant of unlawfully intercepting communications by using session-replay software to capture the interaction of people visiting the corporate homepage Intel.com. [...]
