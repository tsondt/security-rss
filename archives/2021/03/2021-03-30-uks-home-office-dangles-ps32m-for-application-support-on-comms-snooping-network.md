Title: UK's Home Office dangles £32m for application support on comms-snooping network
Date: 2021-03-30T08:30:07+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: uks-home-office-dangles-ps32m-for-application-support-on-comms-snooping-network

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/30/home_office_ncds_application_support/){:target="_blank" rel="noopener"}

> No prior experience of working with the intelligence community? 'Knowledge of the technological landscape' will do The UK's Home Office is on the hunt for a supplier to help support applications running on its counter-terrorism data network to fulfil a contract that could be worth up to £32m.... [...]
