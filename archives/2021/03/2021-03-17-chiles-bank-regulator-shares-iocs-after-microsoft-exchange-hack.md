Title: Chile's bank regulator shares IOCs after Microsoft Exchange hack
Date: 2021-03-17T11:58:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: chiles-bank-regulator-shares-iocs-after-microsoft-exchange-hack

[Source](https://www.bleepingcomputer.com/news/security/chiles-bank-regulator-shares-iocs-after-microsoft-exchange-hack/){:target="_blank" rel="noopener"}

> Chile's Comisión para el Mercado Financiero (CMF) has disclosed that their Microsoft Exchange server was compromised through the recently disclosed ProxyLogon vulnerabilities. [...]
