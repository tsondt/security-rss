Title: Mozilla VPN now nudges users to put shields up on dodgy networks, adds LAN access
Date: 2021-03-30T19:35:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: mozilla-vpn-now-nudges-users-to-put-shields-up-on-dodgy-networks-adds-lan-access

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/30/mozilla_vpn/){:target="_blank" rel="noopener"}

> Apple fans will need to wait just a little longer Mozilla's attempts to augment its income continued apace with an update to the company's VPN subscription service.... [...]
