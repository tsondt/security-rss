Title: Chinese businessman plotted with GE insider to steal transistor secrets, say Feds
Date: 2021-03-01T20:06:04+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: chinese-businessman-plotted-with-ge-insider-to-steal-transistor-secrets-say-feds

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/01/china_mosfet_theft/){:target="_blank" rel="noopener"}

> Hong Kong-based suspect wanted to create rival startup using pilfered silicon carbide MOSFET blueprints – claim A Chinese businessman has been accused by the US government of trying to steal silicon secrets from General Electric (GE).... [...]
