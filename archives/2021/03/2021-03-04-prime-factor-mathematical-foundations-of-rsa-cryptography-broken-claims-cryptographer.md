Title: Prime-factor mathematical foundations of RSA cryptography ‘broken’, claims cryptographer
Date: 2021-03-04T16:52:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: prime-factor-mathematical-foundations-of-rsa-cryptography-broken-claims-cryptographer

[Source](https://portswigger.net/daily-swig/prime-factor-mathematical-foundations-of-rsa-cryptography-broken-claims-cryptographer){:target="_blank" rel="noopener"}

> Not so fast on those ‘fast factoring’ boasts [...]
