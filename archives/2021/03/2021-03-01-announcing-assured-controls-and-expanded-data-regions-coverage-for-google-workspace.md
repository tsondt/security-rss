Title: Announcing Assured Controls and expanded Data Regions coverage for Google Workspace
Date: 2021-03-01T17:00:00+00:00
Author: Ganesh Chilakapati
Category: GCP Security
Tags: Identity & Security;Google Workspace
Slug: announcing-assured-controls-and-expanded-data-regions-coverage-for-google-workspace

[Source](https://cloud.google.com/blog/products/workspace/assured-controls-and-expanded-data-regions-for-google-workspace/){:target="_blank" rel="noopener"}

> As Google Workspace rapidly introduces new collaboration tools to help organizations embrace the Future of Work, we want to further empower our admins with the tools they need to control their data according to their organizational requirements and business goals. Helping admins achieve these information governance goals is a critical part of earning and maintaining customer trust, alongside our strong commitments to protecting privacy and upholding transparency. Today, we’re adding new information governance features to Google Workspace with the introduction of Assured Controls and enhancements to Data Regions, both rolling out in the coming weeks. Assured Controls for Google Workspace [...]
