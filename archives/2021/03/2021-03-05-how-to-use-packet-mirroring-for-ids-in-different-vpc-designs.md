Title: How to use Packet Mirroring for IDS in different VPC designs
Date: 2021-03-05T17:00:00+00:00
Author: Jonny Almaleh
Category: GCP Security
Tags: Identity & Security;Google Cloud Platform;Networking
Slug: how-to-use-packet-mirroring-for-ids-in-different-vpc-designs

[Source](https://cloud.google.com/blog/products/networking/using-packet-mirroring-with-ids/){:target="_blank" rel="noopener"}

> When migrating from on-premises to the cloud, many Google Cloud customers want scalable solutions to detect and alert on higher-layer network anomalies, keeping the same level of network visibility they have on-prem. The answer may be to combine Packet Mirroring with an Intrusion Detection System (IDS) such as the open-source Suricata, or some other preferred threat detection system. This type of solution can provide the visibility you need in the cloud to detect malicious activity, alert, and perhaps even implement security measures to help prevent subsequent intrusions. However, design strategies for Packet Mirroring plus IDS can be confusing, considering the [...]
