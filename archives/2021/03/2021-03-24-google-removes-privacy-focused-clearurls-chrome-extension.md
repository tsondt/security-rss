Title: Google removes privacy-focused ClearURLs Chrome extension
Date: 2021-03-24T07:37:58-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Google
Slug: google-removes-privacy-focused-clearurls-chrome-extension

[Source](https://www.bleepingcomputer.com/news/security/google-removes-privacy-focused-clearurls-chrome-extension/){:target="_blank" rel="noopener"}

> Google has mysteriously removed the popular browser extension ClearURLs from the Chrome Web Store. ClearURLs is a privacy-preserving browser add-on which automatically removes tracking elements from URLs. This, according to its developer, can help protect your privacy when browsing the Internet. [...]
