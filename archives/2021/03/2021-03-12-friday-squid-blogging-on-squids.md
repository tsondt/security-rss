Title: Friday Squid Blogging: On SQUIDS
Date: 2021-03-12T22:10:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-on-squids

[Source](https://www.schneier.com/blog/archives/2021/03/friday-squid-blogging-on-squids.html){:target="_blank" rel="noopener"}

> A good tutorial : But we can go beyond the polarization of electrons and really leverage the electron waviness. By interleaving thin layers of superconducting and normal materials, we can make the quantum electronic equivalents of transistors and diodes such as Superconducting Tunnel Junctions (SJTs) and Superconducting Quantum Interference Devices (affectionately known as SQUIDs). These devices take full advantage of the wave-like nature of electrons and can be used as building blocks for all sorts of novel electronics. Because of the superconducting requirement, they need to be kept very cold, but quantum electronics have already revolutionized precision measurement. The most [...]
