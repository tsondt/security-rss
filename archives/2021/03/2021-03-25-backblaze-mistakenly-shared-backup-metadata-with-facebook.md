Title: BackBlaze mistakenly shared backup metadata with Facebook
Date: 2021-03-25T09:36:40-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: backblaze-mistakenly-shared-backup-metadata-with-facebook

[Source](https://www.bleepingcomputer.com/news/security/backblaze-mistakenly-shared-backup-metadata-with-facebook/){:target="_blank" rel="noopener"}

> Backblaze has removed Facebook tracking code (also known as an advertising pixel) accidentally added to web UI pages only accessible to logged-in customers. [...]
