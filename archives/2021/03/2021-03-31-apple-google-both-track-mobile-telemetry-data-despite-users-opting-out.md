Title: Apple, Google Both Track Mobile Telemetry Data, Despite Users Opting Out
Date: 2021-03-31T19:43:03+00:00
Author: Tom Spring
Category: Threatpost
Tags: Mobile Security;Privacy
Slug: apple-google-both-track-mobile-telemetry-data-despite-users-opting-out

[Source](https://threatpost.com/google-apple-track-mobile-opting-out/165147/){:target="_blank" rel="noopener"}

> Google’s Pixel and Apple’s iPhone both in privacy hot seat for siphoning mobile device data without consent. [...]
