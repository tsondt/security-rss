Title: Leading Indian fintech platform MobiKwik denies data breach
Date: 2021-03-30T11:20:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: leading-indian-fintech-platform-mobikwik-denies-data-breach

[Source](https://www.bleepingcomputer.com/news/security/leading-indian-fintech-platform-mobikwik-denies-data-breach/){:target="_blank" rel="noopener"}

> Indian digital financial services platform Mobikwik denies claims that almost 8 TB of data put up for sale was allegedly stolen from its servers. [...]
