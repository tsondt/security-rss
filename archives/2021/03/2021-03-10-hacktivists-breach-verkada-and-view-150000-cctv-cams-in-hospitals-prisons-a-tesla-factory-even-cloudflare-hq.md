Title: Hacktivists breach Verkada and view 150,000 CCTV cams in hospitals, prisons, a Tesla factory, even Cloudflare HQ
Date: 2021-03-10T19:01:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: hacktivists-breach-verkada-and-view-150000-cctv-cams-in-hospitals-prisons-a-tesla-factory-even-cloudflare-hq

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/10/150k_cctv_cameras_verkada_breach/){:target="_blank" rel="noopener"}

> Life in the 21st century is great A CCTV camera biz which left an admin account username and password exposed on the World Wide Web has, you guessed it, been targeted by hacktivists.... [...]
