Title: Facebook Disrupts Spy Effort Aimed at Uyghurs
Date: 2021-03-25T18:31:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: Facebook;Government;Malware;Mobile Security;Web Security
Slug: facebook-disrupts-spy-effort-aimed-at-uyghurs

[Source](https://threatpost.com/facebook-disrupts-spy-uyghurs/165032/){:target="_blank" rel="noopener"}

> The social-media giant took down legions of fake profiles aimed at spreading espionage malware. [...]
