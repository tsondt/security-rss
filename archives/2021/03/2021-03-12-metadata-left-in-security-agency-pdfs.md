Title: Metadata Left in Security Agency PDFs
Date: 2021-03-12T12:03:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;metadata;security analysis
Slug: metadata-left-in-security-agency-pdfs

[Source](https://www.schneier.com/blog/archives/2021/03/metadata-left-in-security-agency-pdfs.html){:target="_blank" rel="noopener"}

> Really interesting research : “Exploitation and Sanitization of Hidden Data in PDF Files” Abstract: Organizations publish and share more and more electronic documents like PDF files. Unfortunately, most organizations are unaware that these documents can compromise sensitive information like authors names, details on the information system and architecture. All these information can be exploited easily by attackers to footprint and later attack an organization. In this paper, we analyze hidden data found in the PDF files published by an organization. We gathered a corpus of 39664 PDF files published by 75 security agencies from 47 countries. We have been able [...]
