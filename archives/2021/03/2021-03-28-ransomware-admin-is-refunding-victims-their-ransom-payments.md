Title: Ransomware admin is refunding victims their ransom payments
Date: 2021-03-28T18:53:34-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: ransomware-admin-is-refunding-victims-their-ransom-payments

[Source](https://www.bleepingcomputer.com/news/security/ransomware-admin-is-refunding-victims-their-ransom-payments/){:target="_blank" rel="noopener"}

> After recently announcing the end of the operation, the administrator of Ziggy ransomware is now stating that they will also give the money back. [...]
