Title: How to automate SCAP testing with AWS Systems Manager and Security Hub
Date: 2021-03-24T21:11:01+00:00
Author: John Trollinger
Category: AWS Security
Tags: AWS Security Hub;AWS Systems Manager;Intermediate (200);Security, Identity, & Compliance;Automation;OpenSCAP;SCAP;Security Blog
Slug: how-to-automate-scap-testing-with-aws-systems-manager-and-security-hub

[Source](https://aws.amazon.com/blogs/security/how-to-automate-scap-testing-with-aws-systems-manager-and-security-hub/){:target="_blank" rel="noopener"}

> US federal government agencies use the National Institute of Standards and Technology (NIST) framework to provide security and compliance guidance for their IT systems. The US Department of Defense (DoD) also requires its IT systems to follow the Security Technical Implementation Guides (STIGs) produced by the Defense Information Systems Agency (DISA). To aid in managing [...]
