Title: MangaDex website taken offline following cyber-attack, data breach
Date: 2021-03-22T14:23:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: mangadex-website-taken-offline-following-cyber-attack-data-breach

[Source](https://portswigger.net/daily-swig/mangadex-website-taken-offline-following-cyber-attack-data-breach){:target="_blank" rel="noopener"}

> Owners of manga fan site are rebuilding the codebase following series of security incidents [...]
