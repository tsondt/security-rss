Title: This new Microsoft tool checks Exchange Servers for ProxyLogon hacks
Date: 2021-03-06T14:04:41-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: this-new-microsoft-tool-checks-exchange-servers-for-proxylogon-hacks

[Source](https://www.bleepingcomputer.com/news/microsoft/this-new-microsoft-tool-checks-exchange-servers-for-proxylogon-hacks/){:target="_blank" rel="noopener"}

> Microsoft has released a PowerShell script that admins can use to check whether the recently disclosed ProxyLogon vulnerabilities have hacked a Microsoft Exchange server. [...]
