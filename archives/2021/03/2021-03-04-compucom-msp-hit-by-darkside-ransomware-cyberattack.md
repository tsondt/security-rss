Title: CompuCom MSP hit by DarkSide ransomware cyberattack
Date: 2021-03-04T15:58:41-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: compucom-msp-hit-by-darkside-ransomware-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/compucom-msp-hit-by-darkside-ransomware-cyberattack/){:target="_blank" rel="noopener"}

> US managed service provider CompuCom has suffered a DarkSide ransomware attack leading to service outages and customers disconnecting from the MSP's network to prevent the spread of malware. [...]
