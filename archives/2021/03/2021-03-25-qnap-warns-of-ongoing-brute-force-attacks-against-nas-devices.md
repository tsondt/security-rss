Title: QNAP warns of ongoing brute-force attacks against NAS devices
Date: 2021-03-25T10:58:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qnap-warns-of-ongoing-brute-force-attacks-against-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-of-ongoing-brute-force-attacks-against-nas-devices/){:target="_blank" rel="noopener"}

> QNAP warns customers of ongoing attacks targeting QNAP NAS (network-attached storage) devices and urges them to immediately take action to mitigate them. [...]
