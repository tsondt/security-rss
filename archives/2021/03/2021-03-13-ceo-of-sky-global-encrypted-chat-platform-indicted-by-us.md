Title: CEO of Sky Global encrypted chat platform indicted by US
Date: 2021-03-13T12:28:50-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ceo-of-sky-global-encrypted-chat-platform-indicted-by-us

[Source](https://www.bleepingcomputer.com/news/security/ceo-of-sky-global-encrypted-chat-platform-indicted-by-us/){:target="_blank" rel="noopener"}

> The US Department of Justice has indicted the CEO of encrypted messaging company Sky Global, and an associate for allegedly aiding criminal enterprises avoid detection by law enforcement. [...]
