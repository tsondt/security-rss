Title: Post-Cyberattack, Universal Health Services Faces $67M in Losses
Date: 2021-03-02T21:27:40+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks
Slug: post-cyberattack-universal-health-services-faces-67m-in-losses

[Source](https://threatpost.com/post-cyberattack-universal-health-services-faces-67m-in-losses/164424/){:target="_blank" rel="noopener"}

> The Fortune-500 hospital network owner is facing steep costs in damages after a cyberattack impacted patient care and billing in September and October. [...]
