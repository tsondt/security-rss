Title: Passwords, Private Posts Exposed in Hack of Gab Social Network
Date: 2021-03-01T20:41:51+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Web Security
Slug: passwords-private-posts-exposed-in-hack-of-gab-social-network

[Source](https://threatpost.com/hacktivists-gab-posts-passwords/164360/){:target="_blank" rel="noopener"}

> The Distributed Denial of Secrets group claim they have received more than 70 gigabytes of data exfiltrated from social media platform Gab. [...]
