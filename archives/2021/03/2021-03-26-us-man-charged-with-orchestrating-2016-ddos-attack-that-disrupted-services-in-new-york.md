Title: US man charged with orchestrating 2016 DDoS attack that disrupted services in New York
Date: 2021-03-26T13:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-man-charged-with-orchestrating-2016-ddos-attack-that-disrupted-services-in-new-york

[Source](https://portswigger.net/daily-swig/us-man-charged-with-orchestrating-2016-ddos-attack-that-disrupted-services-in-new-york){:target="_blank" rel="noopener"}

> Prosecutors allege that suspect intentionally caused cyber-attack [...]
