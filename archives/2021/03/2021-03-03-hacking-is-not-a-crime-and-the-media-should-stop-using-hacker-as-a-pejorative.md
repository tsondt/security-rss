Title: Hacking is not a crime – and the media should stop using 'hacker' as a pejorative
Date: 2021-03-03T11:00:05+00:00
Author: Alyssa Miller
Category: The Register
Tags: 
Slug: hacking-is-not-a-crime-and-the-media-should-stop-using-hacker-as-a-pejorative

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/03/debate_hackers_for/){:target="_blank" rel="noopener"}

> Hackers are friends not foes, says Alyssa Miller in this opening argument for our latest debate Register debate Welcome to the latest Register Debate in which writers discuss technology topics, and you – the reader – choose the winning argument. The format is simple: a motion is proposed, the argument for the motion is published today, and the argument against will be published on Friday.... [...]
