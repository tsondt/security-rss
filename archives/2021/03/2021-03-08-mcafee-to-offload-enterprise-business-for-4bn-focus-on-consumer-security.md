Title: McAfee to offload enterprise business for $4bn, focus on consumer security
Date: 2021-03-08T20:56:07+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: mcafee-to-offload-enterprise-business-for-4bn-focus-on-consumer-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/08/mcafee_enterprise_sale_indictment/){:target="_blank" rel="noopener"}

> While its namesake founder is indicted for fraud and money laundering McAfee will sell off its enterprise business to private equity firm Symphony Technology Group (STG) for $4bn in cash, the venerable security biz announced on Monday.... [...]
