Title: How to delegate management of identity in AWS Single Sign-On
Date: 2021-03-03T23:42:32+00:00
Author: Louay Shaat
Category: AWS Security
Tags: Advanced (300);AWS Single Sign-On (SSO);Security, Identity, & Compliance;AWS IAM;Conditions;Delegation;Distributed teams;Identity providers;IdP;Permissions;SAML;Security Blog;Sessions;Tags
Slug: how-to-delegate-management-of-identity-in-aws-single-sign-on

[Source](https://aws.amazon.com/blogs/security/how-to-delegate-management-of-identity-in-aws-single-sign-on/){:target="_blank" rel="noopener"}

> In this blog post, I show how you can use AWS Single Sign-On (AWS SSO) to delegate administration of user identities. Delegation is the process of providing your teams permissions to manage accounts and identities associated with their teams. You can achieve this by using the existing integration that AWS SSO has with AWS Organizations, [...]
