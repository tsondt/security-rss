Title: Exchange flaws could be much worse than thought: Six hacking groups suspected of using the zero days pre-patch
Date: 2021-03-15T12:30:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: exchange-flaws-could-be-much-worse-than-thought-six-hacking-groups-suspected-of-using-the-zero-days-pre-patch

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/15/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Verkada flaw finder cuffed and Apple boss may have leaked secrets In brief It's looking like the exploitation of critical Exchange flaws that Microsoft revealed at the start of the month could be much worse than folks first suspected.... [...]
