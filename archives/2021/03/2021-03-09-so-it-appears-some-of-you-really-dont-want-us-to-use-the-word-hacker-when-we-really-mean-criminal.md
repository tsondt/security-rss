Title: So it appears some of you really don't want us to use the word 'hacker' when we really mean 'criminal'
Date: 2021-03-09T11:00:18+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: so-it-appears-some-of-you-really-dont-want-us-to-use-the-word-hacker-when-we-really-mean-criminal

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/09/debate_hacker_result/){:target="_blank" rel="noopener"}

> The votes have been cast and counted... and it's a landslide Register debate Last week, we argued over whether or not the media, including El Reg, should stop using the word hacker as a pejorative.... [...]
