Title: Cyberattacks See Fundamental Changes, A Year into COVID-19
Date: 2021-03-15T18:17:15+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Hacks;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: cyberattacks-see-fundamental-changes-a-year-into-covid-19

[Source](https://threatpost.com/cyberattacks-fundamental-changes-covid-19/164775/){:target="_blank" rel="noopener"}

> A year after COVID-19 was officially determined to be a pandemic, the methods and tactics used by cybercriminals have drastically changed. [...]
