Title: US indicts John McAfee for cryptocurrency fraud, money laundering
Date: 2021-03-05T17:16:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: us-indicts-john-mcafee-for-cryptocurrency-fraud-money-laundering

[Source](https://www.bleepingcomputer.com/news/security/us-indicts-john-mcafee-for-cryptocurrency-fraud-money-laundering/){:target="_blank" rel="noopener"}

> US federal prosecutors have charged John McAfee, founder of cybersecurity firm McAfee, and his executive advisor Jimmy Gale Watson Jr for cryptocurrency fraud and money laundering. [...]
