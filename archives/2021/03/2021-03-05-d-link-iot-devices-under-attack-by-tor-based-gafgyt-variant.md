Title: D-Link, IoT Devices Under Attack By Tor-Based Gafgyt Variant
Date: 2021-03-05T15:55:41+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: IoT;Malware;Vulnerabilities;Web Security
Slug: d-link-iot-devices-under-attack-by-tor-based-gafgyt-variant

[Source](https://threatpost.com/d-link-iot-tor-gafgyt-variant/164529/){:target="_blank" rel="noopener"}

> A new variant of the Gafgyt botnet - that's actively targeting vulnerable D-Link and Internet of Things devices - is the first variant of the malware to rely on Tor communications, researchers say. [...]
