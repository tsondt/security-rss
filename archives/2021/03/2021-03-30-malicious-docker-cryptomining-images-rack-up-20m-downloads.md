Title: Malicious Docker Cryptomining Images Rack Up 20M Downloads
Date: 2021-03-30T20:22:42+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Malware
Slug: malicious-docker-cryptomining-images-rack-up-20m-downloads

[Source](https://threatpost.com/malicious-docker-cryptomining-images/165120/){:target="_blank" rel="noopener"}

> Publicly available cloud images are spreading Monero-mining malware to unsuspecting cloud developers. [...]
