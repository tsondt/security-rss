Title: Google engineer urges web devs to step up and secure their code in this data-spilling Spectre-haunted world
Date: 2021-03-08T23:22:27+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: google-engineer-urges-web-devs-to-step-up-and-secure-their-code-in-this-data-spilling-spectre-haunted-world

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/08/post_spectre_programming/){:target="_blank" rel="noopener"}

> 'This is going to be a lot of work... a reasonable set of mitigation primitives exists today, ready and waiting for use' After the disclosure of the 2018 Spectre family of vulnerabilities in modern microprocessor chips, hardware vendor and operating system makers scrambled to reduce the impact of data-leaking side-channel attacks designed to exploit the way chips try to predict future instructions.... [...]
