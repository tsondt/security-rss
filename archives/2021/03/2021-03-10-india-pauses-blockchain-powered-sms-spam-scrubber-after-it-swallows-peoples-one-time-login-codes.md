Title: India pauses blockchain-powered SMS spam-scrubber after it swallows people's one-time login codes
Date: 2021-03-10T02:24:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: india-pauses-blockchain-powered-sms-spam-scrubber-after-it-swallows-peoples-one-time-login-codes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/10/india_sms_blocking_mess/){:target="_blank" rel="noopener"}

> Tardiness by stakeholders, rather than over-optimistic blockheads, blamed India’s Telecom Regulatory Authority has paused the rollout of a national SMS “scrubbing” service and blamed business for the delay.... [...]
