Title: Biden administration labels China top tech threat, promises proportionate responses to cyberattacks
Date: 2021-03-05T05:02:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: biden-administration-labels-china-top-tech-threat-promises-proportionate-responses-to-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/05/bide_administration_interim_national_security_guidance/){:target="_blank" rel="noopener"}

> Tech visa reform pledged, alongside diplomacy to write new rules for tech The Biden administration has named China as the most threatening nation the United States faces, on grounds that it can combine its technological and other capabilities like no other.... [...]
