Title: The Week in Ransomware - March 12th 2021 - Encrypting Exchange servers
Date: 2021-03-12T18:51:27-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-march-12th-2021-encrypting-exchange-servers

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-12th-2021-encrypting-exchange-servers/){:target="_blank" rel="noopener"}

> For the past two weeks, the cybersecurity news has been dominated by stories about the Microsoft Exchange ProxyLogon vulnerabilities. One overriding concern has been when will ransomware actors use the vulnerabilities to compromise and encrypt mail servers. [...]
