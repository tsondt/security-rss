Title: Guilty: Sister and brother who over-ordered hundreds of MacBooks for university and sold the kit for millions
Date: 2021-03-23T22:38:08+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: guilty-sister-and-brother-who-over-ordered-hundreds-of-macbooks-for-university-and-sold-the-kit-for-millions

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/23/stanford_macbook_theft/){:target="_blank" rel="noopener"}

> Castanedas pocketed $2.3m from 800 laptops alone, Feds say A sister and brother have admitted over-ordering hundreds of new MacBooks for "a private university" in Silicon Valley to steal and sell the expensive gear for millions of dollars.... [...]
