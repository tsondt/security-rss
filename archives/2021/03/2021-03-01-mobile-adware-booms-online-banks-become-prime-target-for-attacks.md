Title: Mobile Adware Booms, Online Banks Become Prime Target for Attacks
Date: 2021-03-01T22:54:02+00:00
Author: Tom Spring
Category: Threatpost
Tags: Malware;Mobile Security;Vulnerabilities
Slug: mobile-adware-booms-online-banks-become-prime-target-for-attacks

[Source](https://threatpost.com/mobile-adware-booms-attacks/164386/){:target="_blank" rel="noopener"}

> A snapshot of the 2020 mobile threat landscape reveals major shifts toward adware and threats to online banks. [...]
