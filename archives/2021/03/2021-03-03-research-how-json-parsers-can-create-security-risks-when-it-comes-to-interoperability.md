Title: Research: How JSON parsers can create security risks when it comes to interoperability
Date: 2021-03-03T12:17:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: research-how-json-parsers-can-create-security-risks-when-it-comes-to-interoperability

[Source](https://portswigger.net/daily-swig/research-how-json-parsers-can-create-security-risks-when-it-comes-to-interoperability){:target="_blank" rel="noopener"}

> Open-ended specifications are partly to blame, researcher suggests [...]
