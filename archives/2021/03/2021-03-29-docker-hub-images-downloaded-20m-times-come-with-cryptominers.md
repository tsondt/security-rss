Title: Docker Hub images downloaded 20M times come with cryptominers
Date: 2021-03-29T14:30:59-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: docker-hub-images-downloaded-20m-times-come-with-cryptominers

[Source](https://www.bleepingcomputer.com/news/security/docker-hub-images-downloaded-20m-times-come-with-cryptominers/){:target="_blank" rel="noopener"}

> Researchers found that more than two-dozen containers on Docker Hub have been downloaded more than 20 million times for cryptojacking operations spanning at least two years. [...]
