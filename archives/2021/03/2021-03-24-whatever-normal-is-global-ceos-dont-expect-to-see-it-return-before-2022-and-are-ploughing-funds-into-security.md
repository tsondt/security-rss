Title: Whatever 'normal' is, global CEOs don't expect to see it return before 2022 and are ploughing funds into security
Date: 2021-03-24T07:58:04+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: whatever-normal-is-global-ceos-dont-expect-to-see-it-return-before-2022-and-are-ploughing-funds-into-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/24/global_ceos_security_pandemic/){:target="_blank" rel="noopener"}

> It's a grim time to be in the commercial real-estate business KPMG's latest survey of global CEOs shows widespread belief that the remote-working trend will linger into 2022 as the world gets to grip with COVID-19.... [...]
