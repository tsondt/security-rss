Title: Russian pleads guilty to Tesla hacking and extortion attempt
Date: 2021-03-19T09:05:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: russian-pleads-guilty-to-tesla-hacking-and-extortion-attempt

[Source](https://www.bleepingcomputer.com/news/security/russian-pleads-guilty-to-tesla-hacking-and-extortion-attempt/){:target="_blank" rel="noopener"}

> Russian national Egor Igorevich Kriuchkov has pleaded guilty to recruiting a Tesla employee to plant malware designed to steal data within the network of Tesla's Nevada Gigafactory. [...]
