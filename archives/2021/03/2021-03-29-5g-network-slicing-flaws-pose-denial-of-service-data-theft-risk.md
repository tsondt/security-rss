Title: 5G network slicing flaws pose denial-of-service, data theft risk
Date: 2021-03-29T15:12:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 5g-network-slicing-flaws-pose-denial-of-service-data-theft-risk

[Source](https://portswigger.net/daily-swig/5g-network-slicing-flaws-pose-denial-of-service-data-theft-risk){:target="_blank" rel="noopener"}

> Researchers urge telco industry to improve authentication controls before widespread deployment [...]
