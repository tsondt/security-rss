Title: Australia, India, Japan, and USA create joint critical tech working group
Date: 2021-03-14T23:46:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: australia-india-japan-and-usa-create-joint-critical-tech-working-group

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/14/quad_critical_tech_working_group/){:target="_blank" rel="noopener"}

> 'Quad' group wants alternatives to China. Also freedom and governance that reflects shared values The first “Quad summit” of leaders from Australia, India, Japan, and the USA has announced the group will create a “Critical and Emerging Technology Working Group”.... [...]
