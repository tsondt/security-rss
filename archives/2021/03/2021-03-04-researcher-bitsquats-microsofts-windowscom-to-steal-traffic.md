Title: Researcher bitsquats Microsoft's windows.com to steal traffic
Date: 2021-03-04T11:37:15-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Microsoft
Slug: researcher-bitsquats-microsofts-windowscom-to-steal-traffic

[Source](https://www.bleepingcomputer.com/news/security/researcher-bitsquats-microsofts-windowscom-to-steal-traffic/){:target="_blank" rel="noopener"}

> A researcher was able to bitsquat Microsoft's windows.com domain by cybersquatting variations of windows.com. Adversaries can abuse this tactic to conduct automated attacks or collect data due to the nature of bit flipping. [...]
