Title: Oxfam Australia confirms data breach after stolen info sold online
Date: 2021-03-02T10:47:45-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: oxfam-australia-confirms-data-breach-after-stolen-info-sold-online

[Source](https://www.bleepingcomputer.com/news/security/oxfam-australia-confirms-data-breach-after-stolen-info-sold-online/){:target="_blank" rel="noopener"}

> Oxfam Australia has confirmed a data breach after suffering a cyberattack and their donor databases put up for sale on a hacker forum in January. [...]
