Title: Crypto-Miner Campaign Targets Unpatched QNAP NAS Devices
Date: 2021-03-08T21:16:20+00:00
Author: Tom Spring
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: crypto-miner-campaign-targets-unpatched-qnap-nas-devices

[Source](https://threatpost.com/miner-campaign-targets-unpatched-qnap-nas/164580/){:target="_blank" rel="noopener"}

> Researchers warn two critical bugs impacting multiple QNAP firmware versions are under active attack. [...]
