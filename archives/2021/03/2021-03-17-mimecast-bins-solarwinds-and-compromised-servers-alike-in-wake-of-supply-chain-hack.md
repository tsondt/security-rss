Title: Mimecast bins SolarWinds and compromised servers alike in wake of supply chain hack
Date: 2021-03-17T18:30:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: mimecast-bins-solarwinds-and-compromised-servers-alike-in-wake-of-supply-chain-hack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/17/mimecast_bins_solarwinds_after_compromise/){:target="_blank" rel="noopener"}

> Signs up for Cisco, says some encrypted creds were stolen Email security biz Mimecast has dumped SolarWinds' network monitoring tool in favour of Cisco's Netflow product after falling victim to the infamous December supply chain attack.... [...]
