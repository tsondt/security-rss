Title: Ryuk ransomware hits 700 Spanish government labor agency offices
Date: 2021-03-10T08:35:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Government
Slug: ryuk-ransomware-hits-700-spanish-government-labor-agency-offices

[Source](https://www.bleepingcomputer.com/news/security/ryuk-ransomware-hits-700-spanish-government-labor-agency-offices/){:target="_blank" rel="noopener"}

> The systems of SEPE, the Spanish government agency for labor, were taken down following a ransomware attack that hit more than 700 agency offices across Spain. [...]
