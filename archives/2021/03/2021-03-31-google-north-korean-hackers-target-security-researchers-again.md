Title: Google: North Korean hackers target security researchers again
Date: 2021-03-31T13:33:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: google-north-korean-hackers-target-security-researchers-again

[Source](https://www.bleepingcomputer.com/news/security/google-north-korean-hackers-target-security-researchers-again/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group (TAG) says that North Korean government-sponsored hackers are once again targeting security researchers using fake Twitter and LinkedIn social media accounts. [...]
