Title: How to replicate secrets in AWS Secrets Manager to multiple Regions
Date: 2021-03-04T18:52:07+00:00
Author: Fatima Ahmed
Category: AWS Security
Tags: AWS Secrets Manager;Intermediate (200);Security, Identity, & Compliance;Cloud security;Disaster Recovery;Replication;Security Blog
Slug: how-to-replicate-secrets-in-aws-secrets-manager-to-multiple-regions

[Source](https://aws.amazon.com/blogs/security/how-to-replicate-secrets-aws-secrets-manager-multiple-regions/){:target="_blank" rel="noopener"}

> On March 3, 2021, we launched a new feature for AWS Secrets Manager that makes it possible for you to replicate secrets across multiple AWS Regions. You can give your multi-Region applications access to replicated secrets in the required Regions and rely on Secrets Manager to keep the replicas in sync with the primary secret. [...]
