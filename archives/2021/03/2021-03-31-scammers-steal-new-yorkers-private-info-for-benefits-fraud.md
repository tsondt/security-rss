Title: Scammers steal New Yorkers' private info for benefits fraud
Date: 2021-03-31T10:09:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: scammers-steal-new-yorkers-private-info-for-benefits-fraud

[Source](https://www.bleepingcomputer.com/news/security/scammers-steal-new-yorkers-private-info-for-benefits-fraud/){:target="_blank" rel="noopener"}

> New York's Department of Financial Services (DFS) warns of an ongoing series of attacks resulting in the theft of personal information belonging to hundreds of thousands of New Yorkers. [...]
