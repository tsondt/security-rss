Title: Fake Google reCAPTCHA Phishing Attack Swipes Office 365 Passwords
Date: 2021-03-08T17:04:59+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security
Slug: fake-google-recaptcha-phishing-attack-swipes-office-365-passwords

[Source](https://threatpost.com/google-recaptcha-phishing-office-365/164566/){:target="_blank" rel="noopener"}

> A phishing attack targeting Microsoft users leverages a bogus Google reCAPTCHA system. [...]
