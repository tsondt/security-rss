Title: TPG buys Thycotic, immediately merges it with Centrify to create ~$230m access management monster
Date: 2021-03-03T07:58:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: tpg-buys-thycotic-immediately-merges-it-with-centrify-to-create-230m-access-management-monster

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/03/tpg_buys_centrify_then_turns/){:target="_blank" rel="noopener"}

> Product consolidation roadmap? Not yet. But all involved say the combo will work out just fine Private equity group TPG has acquired security vendor Thycotic and announced it will merge it with another recent acquisition, Centrify.... [...]
