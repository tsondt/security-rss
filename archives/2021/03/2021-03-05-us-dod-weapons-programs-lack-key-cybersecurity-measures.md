Title: U.S. DoD Weapons Programs Lack ‘Key’ Cybersecurity Measures
Date: 2021-03-05T20:45:01+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: us-dod-weapons-programs-lack-key-cybersecurity-measures

[Source](https://threatpost.com/dod-weapons-programs-lack-cybersecurity/164545/){:target="_blank" rel="noopener"}

> The lack of cybersecurity requirements in weapons contracts from the Department of Defense opens the door for dangerous cyberattacks. [...]
