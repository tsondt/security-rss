Title: OVH data center burns down knocking major sites offline
Date: 2021-03-10T03:08:14-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: ovh-data-center-burns-down-knocking-major-sites-offline

[Source](https://www.bleepingcomputer.com/news/technology/ovh-data-center-burns-down-knocking-major-sites-offline/){:target="_blank" rel="noopener"}

> In a major unprecedented incident, data centers of OVH located in Strasbourg, France have been destroyed by fire. Customers are being advised by the company to enact their disaster recovery plans after the fire has rendered multiple data centers unserviceable, impacting websites around the world. [...]
