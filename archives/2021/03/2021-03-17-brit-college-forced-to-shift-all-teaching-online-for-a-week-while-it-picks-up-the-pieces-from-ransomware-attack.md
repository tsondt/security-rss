Title: Brit college forced to shift all teaching online for a week while it picks up the pieces from ransomware attack
Date: 2021-03-17T12:40:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: brit-college-forced-to-shift-all-teaching-online-for-a-week-while-it-picks-up-the-pieces-from-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/17/south_city_birmingham_college_ransomware/){:target="_blank" rel="noopener"}

> Plus: Stop bigging up these despicable criminals An English college has temporarily closed all eight of its campuses and moved all teaching online after a "major" ransomware attack "disabled" its IT systems.... [...]
