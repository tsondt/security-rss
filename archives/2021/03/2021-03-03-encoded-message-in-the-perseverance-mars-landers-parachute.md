Title: Encoded Message in the Perseverance Mars Lander’s Parachute
Date: 2021-03-03T12:00:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: encryption;steganography
Slug: encoded-message-in-the-perseverance-mars-landers-parachute

[Source](https://www.schneier.com/blog/archives/2021/03/encoded-message-in-the-perseverance-mars-landers-parachute.html){:target="_blank" rel="noopener"}

> NASA made an oblique reference to a coded message in the color pattern of the Perseverance Mars Lander ‘s parachute. More information. [...]
