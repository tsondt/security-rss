Title: Mimecast: SolarWinds Attackers Stole Source Code
Date: 2021-03-17T16:18:15+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: mimecast-solarwinds-attackers-stole-source-code

[Source](https://threatpost.com/mimecast-solarwinds-attackers-stole-source-code/164847/){:target="_blank" rel="noopener"}

> A new Mimecast update reveals the SolarWinds hackers accessed several "limited" source code repositories. [...]
