Title: The age of Covid-19: Lockdowns and cybersecurity, 12 months on
Date: 2021-03-16T12:59:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: the-age-of-covid-19-lockdowns-and-cybersecurity-12-months-on

[Source](https://portswigger.net/daily-swig/the-age-of-covid-19-lockdowns-and-cybersecurity-12-months-on){:target="_blank" rel="noopener"}

> Infosec ‘slow-pocalypse’ sees surge in ransomware and online fraud [...]
