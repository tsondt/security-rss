Title: How to scale your authorization needs by using attribute-based access control with S3
Date: 2021-03-18T19:17:36+00:00
Author: Koen van Blijderveen
Category: AWS Security
Tags: Amazon Simple Storage Services (S3);AWS Identity and Access Management (IAM);Expert (400);Security, Identity, & Compliance;ABAC;Amazon S3;authorization;Security Blog;Tags;TBAC
Slug: how-to-scale-your-authorization-needs-by-using-attribute-based-access-control-with-s3

[Source](https://aws.amazon.com/blogs/security/how-to-scale-authorization-needs-using-attribute-based-access-control-with-s3/){:target="_blank" rel="noopener"}

> In this blog post, we show you how to scale your Amazon Simple Storage Service (Amazon S3) authorization strategy as an alternative to using path based authorization. You are going to combine attribute-based access control (ABAC) using AWS Identity and Access Management (IAM) with a standard Active Directory Federation Services (AD FS) connected to Microsoft [...]
