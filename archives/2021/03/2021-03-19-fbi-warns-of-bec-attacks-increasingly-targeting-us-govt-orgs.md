Title: FBI warns of BEC attacks increasingly targeting US govt orgs
Date: 2021-03-19T10:09:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-bec-attacks-increasingly-targeting-us-govt-orgs

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-bec-attacks-increasingly-targeting-us-govt-orgs/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) is warning US private sector companies about an increase in business email compromise (BEC) attacks targeting state, local, tribal, and territorial (SLTT) government entities. [...]
