Title: Facebook awards $55k bug bounty for third-party vulnerabilities that could compromise its internal network
Date: 2021-03-23T11:56:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: facebook-awards-55k-bug-bounty-for-third-party-vulnerabilities-that-could-compromise-its-internal-network

[Source](https://portswigger.net/daily-swig/facebook-awards-55k-bug-bounty-for-third-party-vulnerabilities-that-could-compromise-its-internal-network){:target="_blank" rel="noopener"}

> Two vulnerabilities could be chained to lead to server-side request forgery [...]
