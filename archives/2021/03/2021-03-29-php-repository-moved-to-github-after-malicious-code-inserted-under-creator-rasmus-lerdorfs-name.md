Title: PHP repository moved to GitHub after malicious code inserted under creator Rasmus Lerdorf's name
Date: 2021-03-29T11:46:17+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: php-repository-moved-to-github-after-malicious-code-inserted-under-creator-rasmus-lerdorfs-name

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/29/php_repository_infected/){:target="_blank" rel="noopener"}

> Backdoor quickly spotted and reverted The main code repository for PHP, which powers nearly 80 per cent of the internet, was breached to add malicious code and is now being moved to GitHub as a precaution.... [...]
