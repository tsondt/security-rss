Title: Google Warns Mac, Windows Users of Chrome Zero-Day Flaw
Date: 2021-03-15T15:40:21+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities
Slug: google-warns-mac-windows-users-of-chrome-zero-day-flaw

[Source](https://threatpost.com/google-mac-windows-chrome-zero-day/164759/){:target="_blank" rel="noopener"}

> The use-after-free vulnerability is the third Google Chrome zero-day flaw to be disclosed in three months. [...]
