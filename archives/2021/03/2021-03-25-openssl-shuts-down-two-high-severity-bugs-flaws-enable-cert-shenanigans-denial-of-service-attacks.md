Title: OpenSSL shuts down two high-severity bugs: Flaws enable cert shenanigans, denial-of-service attacks
Date: 2021-03-25T20:28:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: openssl-shuts-down-two-high-severity-bugs-flaws-enable-cert-shenanigans-denial-of-service-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/25/openssl_bug_fix/){:target="_blank" rel="noopener"}

> Debian, Ubuntu ahead of the curve in patching at least – don't be late yourself Two high-severity vulnerabilities in the OpenSSL software library were disclosed on Thursday alongside the release of a patched version of the software, OpenSSL 1.1.1k.... [...]
