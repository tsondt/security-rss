Title: Fraud Ring Launders Money Via Fake Charity Donations
Date: 2021-03-31T18:26:26+00:00
Author: Tara Seals
Category: Threatpost
Tags: Web Security
Slug: fraud-ring-launders-money-via-fake-charity-donations

[Source](https://threatpost.com/fraud-lauders-money-charity-donations/165138/){:target="_blank" rel="noopener"}

> The Cart Crasher gang is testing stolen payment cards while cleaning ill-gotten funds. [...]
