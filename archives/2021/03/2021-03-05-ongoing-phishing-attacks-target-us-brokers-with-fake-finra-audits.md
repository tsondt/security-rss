Title: Ongoing phishing attacks target US brokers with fake FINRA audits
Date: 2021-03-05T08:28:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ongoing-phishing-attacks-target-us-brokers-with-fake-finra-audits

[Source](https://www.bleepingcomputer.com/news/security/ongoing-phishing-attacks-target-us-brokers-with-fake-finra-audits/){:target="_blank" rel="noopener"}

> The US Financial Industry Regulatory Authority (FINRA) has issued a regulatory notice warning US brokerage firms and brokers of an ongoing phishing campaign using fake compliance audit alerts to harvest information. [...]
