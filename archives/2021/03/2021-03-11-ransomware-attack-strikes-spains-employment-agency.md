Title: Ransomware Attack Strikes Spain’s Employment Agency
Date: 2021-03-11T21:52:15+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware
Slug: ransomware-attack-strikes-spains-employment-agency

[Source](https://threatpost.com/ransomware-attack-spain-employment-agency/164703/){:target="_blank" rel="noopener"}

> Reports say that the agency in charge of managing Spain's unemployment benefits has been hit by the Ryuk ransomware. [...]
