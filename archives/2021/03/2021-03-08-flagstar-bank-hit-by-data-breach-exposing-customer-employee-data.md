Title: Flagstar Bank hit by data breach exposing customer, employee data
Date: 2021-03-08T10:21:59-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: flagstar-bank-hit-by-data-breach-exposing-customer-employee-data

[Source](https://www.bleepingcomputer.com/news/security/flagstar-bank-hit-by-data-breach-exposing-customer-employee-data/){:target="_blank" rel="noopener"}

> US bank and mortgage lender Flagstar has disclosed a data breach after the Clop ransomware gang hacked their Accellion file transfer server in January. [...]
