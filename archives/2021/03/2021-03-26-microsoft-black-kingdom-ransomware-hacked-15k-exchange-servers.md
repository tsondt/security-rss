Title: Microsoft: Black Kingdom ransomware hacked 1.5K Exchange servers
Date: 2021-03-26T12:03:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-black-kingdom-ransomware-hacked-15k-exchange-servers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-black-kingdom-ransomware-hacked-15k-exchange-servers/){:target="_blank" rel="noopener"}

> Microsoft has discovered web shells deployed by Black Kingdom operators on approximately 1,500 Exchange servers vulnerable to ProxyLogon attacks. [...]
