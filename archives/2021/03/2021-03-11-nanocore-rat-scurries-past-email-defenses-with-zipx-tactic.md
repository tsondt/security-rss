Title: NanoCore RAT Scurries Past Email Defenses with .ZIPX Tactic
Date: 2021-03-11T18:58:10+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: nanocore-rat-scurries-past-email-defenses-with-zipx-tactic

[Source](https://threatpost.com/nanocore-rat-email-defenses-zipx/164701/){:target="_blank" rel="noopener"}

> A spam campaign hides a malicious executable behind file archive extensions. [...]
