Title: Ex-asylum seeker with infosec degree loses discrimination claim against UK cyber range provider after storming out
Date: 2021-03-16T11:01:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ex-asylum-seeker-with-infosec-degree-loses-discrimination-claim-against-uk-cyber-range-provider-after-storming-out

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/16/cyber_range_racial_discrimination_judgment/){:target="_blank" rel="noopener"}

> 'It had nothing whatsoever to do with the claimant's country of birth', rules Employment Tribunal A former asylum seeker with a postgraduate degree in cybersecurity who alleged his bosses were spying on him for MI5 has lost his attempt to claim he was racially discriminated against.... [...]
