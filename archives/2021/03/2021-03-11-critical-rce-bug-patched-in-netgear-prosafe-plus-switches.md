Title: Critical RCE bug patched in Netgear ProSAFE Plus switches
Date: 2021-03-11T14:45:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: critical-rce-bug-patched-in-netgear-prosafe-plus-switches

[Source](https://portswigger.net/daily-swig/critical-rce-bug-patched-in-netgear-prosafe-plus-switches){:target="_blank" rel="noopener"}

> Network admins also offered mitigation advice for numerous other unpatched vulnerabilities [...]
