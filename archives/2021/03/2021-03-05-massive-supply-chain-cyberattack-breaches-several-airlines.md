Title: Massive Supply-Chain Cyberattack Breaches Several Airlines
Date: 2021-03-05T19:52:39+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Vulnerabilities;Web Security
Slug: massive-supply-chain-cyberattack-breaches-several-airlines

[Source](https://threatpost.com/supply-chain-cyberattack-airlines/164549/){:target="_blank" rel="noopener"}

> The cyberattack on SITA, a nearly ubiquitous airline service provider, has compromised frequent-flyer data across many carriers. [...]
