Title: Data breach at healthcare provider Elara Caring exposes 100,000 patients’ information
Date: 2021-03-08T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-at-healthcare-provider-elara-caring-exposes-100000-patients-information

[Source](https://portswigger.net/daily-swig/data-breach-at-healthcare-provider-elara-caring-exposes-100-000-patients-information){:target="_blank" rel="noopener"}

> Intruder gained access via phishing attacks [...]
