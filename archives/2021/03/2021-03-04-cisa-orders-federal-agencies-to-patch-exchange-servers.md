Title: CISA Orders Federal Agencies to Patch Exchange Servers
Date: 2021-03-04T17:08:36+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Vulnerabilities;Web Security
Slug: cisa-orders-federal-agencies-to-patch-exchange-servers

[Source](https://threatpost.com/cisa-federal-agencies-patch-exchange-servers/164499/){:target="_blank" rel="noopener"}

> Espionage attacks exploiting the just-patched remote code-execution security bugs in Microsoft Exchange servers are quickly spreading. [...]
