Title: Money can buy you insurance against network break-ins but investing in infosec hygiene wouldn't go amiss, says new NCSC chief
Date: 2021-03-30T11:18:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: money-can-buy-you-insurance-against-network-break-ins-but-investing-in-infosec-hygiene-wouldnt-go-amiss-says-new-ncsc-chief

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/30/ncsc_ceo_infosec_better_than_insurance/){:target="_blank" rel="noopener"}

> C-suites need a kick up the proverbial, says Lindy Cameron in first speech So-called cyber-attack insurance "cannot be a substitute for better basic cybersecurity," the National Cyber Security Centre's chief exec has said in her first major speech since taking office.... [...]
