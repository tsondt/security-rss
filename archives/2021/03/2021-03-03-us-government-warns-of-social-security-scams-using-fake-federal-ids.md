Title: US government warns of Social Security scams using fake federal IDs
Date: 2021-03-03T12:47:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-government-warns-of-social-security-scams-using-fake-federal-ids

[Source](https://www.bleepingcomputer.com/news/security/us-government-warns-of-social-security-scams-using-fake-federal-ids/){:target="_blank" rel="noopener"}

> Government imposter scams now come with a new twist that has the potential to make them even more effective, as the Inspector General for the Social Security Administration (SSA) warns. [...]
