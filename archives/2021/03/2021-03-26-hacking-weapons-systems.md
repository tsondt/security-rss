Title: Hacking Weapons Systems
Date: 2021-03-26T13:41:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberattack;cyberweapons;hacking;infrastructure;military;national security policy;weapons
Slug: hacking-weapons-systems

[Source](https://www.schneier.com/blog/archives/2021/03/hacking-weapons-systems.html){:target="_blank" rel="noopener"}

> Lukasz Olejnik has a good essay on hacking weapons systems. Basically, there is no reason to believe that software in weapons systems is any more vulnerability free than any other software. So now the question is whether the software can be accessed over the Internet. Increasingly, it is. This is likely to become a bigger problem in the near future. We need to think about future wars where the tech simply doesn’t work. [...]
