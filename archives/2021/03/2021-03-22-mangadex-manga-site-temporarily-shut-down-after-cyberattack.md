Title: MangaDex manga site temporarily shut down after cyberattack
Date: 2021-03-22T17:29:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: mangadex-manga-site-temporarily-shut-down-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/mangadex-manga-site-temporarily-shut-down-after-cyberattack/){:target="_blank" rel="noopener"}

> Manga scanlation giant MangaDex has been temporarily shut down after suffering a cyberattack and having its source code stolen. [...]
