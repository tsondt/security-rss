Title: At Least 30,000 U.S. Organizations Newly Hacked Via Holes in Microsoft’s Email Software
Date: 2021-03-05T21:07:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Time to Patch;Hafnium;Microsoft Exchange server flaws;Steven Adair;Volexity
Slug: at-least-30000-us-organizations-newly-hacked-via-holes-in-microsofts-email-software

[Source](https://krebsonsecurity.com/2021/03/at-least-30000-u-s-organizations-newly-hacked-via-holes-in-microsofts-email-software/){:target="_blank" rel="noopener"}

> At least 30,000 organizations across the United States — including a significant number of small businesses, towns, cities and local governments — have over the past few days been hacked by an unusually aggressive Chinese cyber espionage unit that’s focused on stealing email from victim organizations, multiple sources tell KrebsOnSecurity. The espionage group is exploiting four newly-discovered flaws in Microsoft Exchange Server email software, and has seeded hundreds of thousands of victim organizations worldwide with tools that give the attackers total, remote control over affected systems. On March 2, Microsoft released emergency security updates to plug four security holes in [...]
