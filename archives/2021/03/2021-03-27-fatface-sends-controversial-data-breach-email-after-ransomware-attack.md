Title: FatFace sends controversial data breach email after ransomware attack
Date: 2021-03-27T09:41:12-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fatface-sends-controversial-data-breach-email-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/fatface-sends-controversial-data-breach-email-after-ransomware-attack/){:target="_blank" rel="noopener"}

> British clothing brand FatFace has sent a controversial 'confidential' data breach notification to customers after suffering a ransomware attack earlier this year. [...]
