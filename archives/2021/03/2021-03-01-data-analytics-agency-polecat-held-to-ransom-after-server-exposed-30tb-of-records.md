Title: Data analytics agency Polecat held to ransom after server exposed 30TB of records
Date: 2021-03-01T11:15:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-analytics-agency-polecat-held-to-ransom-after-server-exposed-30tb-of-records

[Source](https://portswigger.net/daily-swig/data-analytics-agency-polecat-held-to-ransom-after-server-exposed-30tb-of-records){:target="_blank" rel="noopener"}

> Researchers say ‘billions’ of records were leaked before cyber-attackers took advantage [...]
