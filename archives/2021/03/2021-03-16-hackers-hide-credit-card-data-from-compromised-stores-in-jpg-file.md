Title: Hackers hide credit card data from compromised stores in JPG file
Date: 2021-03-16T05:22:34-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hackers-hide-credit-card-data-from-compromised-stores-in-jpg-file

[Source](https://www.bleepingcomputer.com/news/security/hackers-hide-credit-card-data-from-compromised-stores-in-jpg-file/){:target="_blank" rel="noopener"}

> Hackers have come up with a sneaky method to steal payment card data from compromised online stores that reduces the suspicious traffic footprint and helps them evade detection. [...]
