Title: Ransomware Attack Foils IoT Giant Sierra Wireless
Date: 2021-03-24T18:39:29+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;IoT;Malware
Slug: ransomware-attack-foils-iot-giant-sierra-wireless

[Source](https://threatpost.com/ransomware-iot-sierra-wireless/165003/){:target="_blank" rel="noopener"}

> The ransomware attack has impacted the IoT manufacturer's production lines across multiple sites, and other internal operations. [...]
