Title: DHS orders agencies to urgently patch or disconnect Exchange servers
Date: 2021-03-04T08:04:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Government
Slug: dhs-orders-agencies-to-urgently-patch-or-disconnect-exchange-servers

[Source](https://www.bleepingcomputer.com/news/security/dhs-orders-agencies-to-urgently-patch-or-disconnect-exchange-servers/){:target="_blank" rel="noopener"}

> The Department of Homeland Security's cybersecurity unit has ordered federal agencies to urgently update or disconnect Microsoft Exchange on-premises products on their networks. [...]
