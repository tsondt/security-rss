Title: US govt indicted me because I make privacy tools, says crypto-chat app CEO accused of helping drug smugglers
Date: 2021-03-15T23:57:08+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: us-govt-indicted-me-because-i-make-privacy-tools-says-crypto-chat-app-ceo-accused-of-helping-drug-smugglers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/15/sky_global_indicted/){:target="_blank" rel="noopener"}

> Sky Global’s Jean-Francois Eap denies any wrongdoing The CEO of Sky Global – which sells customized smartphones bundled with encryption chat software – has come out fighting after Uncle Sam charged him with knowingly assisting the international drug smuggling trade.... [...]
