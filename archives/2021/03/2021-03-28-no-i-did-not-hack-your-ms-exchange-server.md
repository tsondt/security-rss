Title: No, I Did Not Hack Your MS Exchange Server
Date: 2021-03-28T17:40:44+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Babydraco backdoor;Babydraco shell;David Watson;Shadowserver;Windows Defender
Slug: no-i-did-not-hack-your-ms-exchange-server

[Source](https://krebsonsecurity.com/2021/03/no-i-did-not-hack-your-ms-exchange-server/){:target="_blank" rel="noopener"}

> New data suggests someone has compromised more than 21,000 Microsoft Exchange Server email systems worldwide and infected them with malware that invokes both KrebsOnSecurity and Yours Truly by name. Let’s just get this out of the way right now: It wasn’t me. The Shadowserver Foundation, a nonprofit that helps network owners identify and fix security threats, says it has found 21,248 different Exchange servers which appear to be compromised by a backdoor and communicating with brian[.]krebsonsecurity[.]top (NOT a safe domain, hence the hobbling). Shadowserver has been tracking wave after wave of attacks targeting flaws in Exchange that Microsoft addressed earlier [...]
