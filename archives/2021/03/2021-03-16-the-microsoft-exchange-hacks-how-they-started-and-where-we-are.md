Title: The Microsoft Exchange hacks: How they started and where we are
Date: 2021-03-16T03:29:33-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: the-microsoft-exchange-hacks-how-they-started-and-where-we-are

[Source](https://www.bleepingcomputer.com/news/security/the-microsoft-exchange-hacks-how-they-started-and-where-we-are/){:target="_blank" rel="noopener"}

> The emergency patches for the recently disclosed critical vulnerabilities in Microsoft Exchange email server did not come soon enough and organizations had little time to prepare before en masse exploitation began. [...]
