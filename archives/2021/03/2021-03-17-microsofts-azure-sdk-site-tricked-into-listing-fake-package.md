Title: Microsoft's Azure SDK site tricked into listing fake package
Date: 2021-03-17T04:01:03-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Cloud;Software
Slug: microsofts-azure-sdk-site-tricked-into-listing-fake-package

[Source](https://www.bleepingcomputer.com/news/security/microsofts-azure-sdk-site-tricked-into-listing-fake-package/){:target="_blank" rel="noopener"}

> A security researcher was able to add a counterfeit test package to the official list of Microsoft Azure SDK latest releases. The simple trick if abused by an attacker can give off the impression that their malicious package is part of the Azure SDK suite. [...]
