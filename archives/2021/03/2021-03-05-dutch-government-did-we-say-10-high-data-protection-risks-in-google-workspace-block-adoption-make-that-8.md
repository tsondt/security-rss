Title: Dutch government: Did we say 10 'high data protection risks' in Google Workspace block adoption? Make that 8
Date: 2021-03-05T10:15:06+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: dutch-government-did-we-say-10-high-data-protection-risks-in-google-workspace-block-adoption-make-that-8

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/05/dutch_government_identifies_10_high/){:target="_blank" rel="noopener"}

> Untangling privacy implications of using Google's productivity suite not easy A Dutch government report identifying "10 high data protection risks" for users of Google Workspace, formerly known as G Suite, has been revised after Google's response, and now says eight high risk issues still remain.... [...]
