Title: Microsoft Exchange Servers See ProxyLogon Patching Frenzy
Date: 2021-03-24T18:39:26+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Malware;Vulnerabilities;Web Security
Slug: microsoft-exchange-servers-see-proxylogon-patching-frenzy

[Source](https://threatpost.com/microsoft-exchange-servers-proxylogon-patching/165001/){:target="_blank" rel="noopener"}

> Vast swathes of companies were likely compromised before patches were applied, so the danger remains. [...]
