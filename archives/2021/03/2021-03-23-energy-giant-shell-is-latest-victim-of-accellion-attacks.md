Title: Energy Giant Shell Is Latest Victim of Accellion Attacks
Date: 2021-03-23T14:16:14+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Vulnerabilities
Slug: energy-giant-shell-is-latest-victim-of-accellion-attacks

[Source](https://threatpost.com/shell-victim-of-accellion-attacks/164973/){:target="_blank" rel="noopener"}

> Attackers accessed personal and business data from the company’s legacy file-transfer service in a recent data-security incident but core IT systems remained untouched. [...]
