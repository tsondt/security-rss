Title: Hacked SendGrid accounts used in phishing attacks to steal logins
Date: 2021-03-04T11:00:33-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hacked-sendgrid-accounts-used-in-phishing-attacks-to-steal-logins

[Source](https://www.bleepingcomputer.com/news/security/hacked-sendgrid-accounts-used-in-phishing-attacks-to-steal-logins/){:target="_blank" rel="noopener"}

> A phishing campaign targeting users of Outlook Web Access and Office 365 services collected thousands of credentials relying on trusted domains such as SendGrid. [...]
