Title: Norway parliament data stolen in Microsoft Exchange attack
Date: 2021-03-10T10:57:36-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: norway-parliament-data-stolen-in-microsoft-exchange-attack

[Source](https://www.bleepingcomputer.com/news/security/norway-parliament-data-stolen-in-microsoft-exchange-attack/){:target="_blank" rel="noopener"}

> Norway's parliament, the Storting, has suffered another cyberattack after threat actors stole data using the recently disclosed Microsoft Exchange vulnerabilities. [...]
