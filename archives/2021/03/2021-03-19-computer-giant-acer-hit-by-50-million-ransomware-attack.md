Title: Computer giant Acer hit by $50 million ransomware attack
Date: 2021-03-19T11:11:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: computer-giant-acer-hit-by-50-million-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/computer-giant-acer-hit-by-50-million-ransomware-attack/){:target="_blank" rel="noopener"}

> Computer giant Acer has been hit by a REvil ransomware attack where the threat actors are demanding the largest known ransom to date, $50,000,000. [...]
