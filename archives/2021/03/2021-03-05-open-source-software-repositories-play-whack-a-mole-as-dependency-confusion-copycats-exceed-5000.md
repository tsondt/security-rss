Title: Open source software repositories play ‘whack-a-mole’ as ‘dependency confusion’ copycats exceed 5,000
Date: 2021-03-05T16:18:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: open-source-software-repositories-play-whack-a-mole-as-dependency-confusion-copycats-exceed-5000

[Source](https://portswigger.net/daily-swig/open-source-software-repositories-play-whack-a-mole-as-dependency-confusion-copycats-exceed-5-000){:target="_blank" rel="noopener"}

> Tactic gathers pace as malicious packages proliferate [...]
