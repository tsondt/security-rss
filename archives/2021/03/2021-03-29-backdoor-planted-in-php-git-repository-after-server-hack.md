Title: Backdoor planted in PHP Git repository after server hack
Date: 2021-03-29T12:00:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: backdoor-planted-in-php-git-repository-after-server-hack

[Source](https://portswigger.net/daily-swig/backdoor-planted-in-php-git-repository-after-server-hack){:target="_blank" rel="noopener"}

> Scripting language falls victim to cyber-attack [...]
