Title: Devices and zero trust
Date: 2021-03-29T20:00:00+00:00
Author: Max Saltonstall
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security;Developers & Practitioners
Slug: devices-and-zero-trust

[Source](https://cloud.google.com/blog/topics/developers-practitioners/devices-and-zero-trust/){:target="_blank" rel="noopener"}

> In a zero trust environment, every device has to earn trust in order to be granted access. When determining whether access should be granted, the security system relies on device metadata, such as what software is running or when the OS was last updated, and checks to see if the device meets that organization's minimum bar for health. Think of it like your temperature: under 100 degrees and you are safe, but go over and you are now medically in fever territory, and you may not be allowed into certain venues. Zero Trust relies on WHO you are and WHAT [...]
