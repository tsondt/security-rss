Title: Insurance giant CNA hit by new Phoenix CryptoLocker ransomware
Date: 2021-03-25T14:26:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: insurance-giant-cna-hit-by-new-phoenix-cryptolocker-ransomware

[Source](https://www.bleepingcomputer.com/news/security/insurance-giant-cna-hit-by-new-phoenix-cryptolocker-ransomware/){:target="_blank" rel="noopener"}

> Insurance giant CNA has suffered a ransomware attack using a new variant called Phoenix CryptoLocker that is possibly linked to the Evil Corp hacking group. [...]
