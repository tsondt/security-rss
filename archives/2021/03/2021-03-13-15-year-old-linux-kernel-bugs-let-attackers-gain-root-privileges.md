Title: 15-year-old Linux kernel bugs let attackers gain root privileges
Date: 2021-03-13T10:15:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Linux
Slug: 15-year-old-linux-kernel-bugs-let-attackers-gain-root-privileges

[Source](https://www.bleepingcomputer.com/news/security/15-year-old-linux-kernel-bugs-let-attackers-gain-root-privileges/){:target="_blank" rel="noopener"}

> Three security vulnerabilities found in the iSCSI subsystem of the Linux kernel could allow local attackers with basic user privileges to gain root privileges on unpatched Linux systems. [...]
