Title: Would you let users vouch for unknown software's safety with an upvote? Google does
Date: 2021-03-04T07:55:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: would-you-let-users-vouch-for-unknown-softwares-safety-with-an-upvote-google-does

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/04/google_malware_upvote/){:target="_blank" rel="noopener"}

> And so can you, because its vote-for-code tools are now on GitHub POLL Google has revealed that its internal anti-malware tools include a “social voting” scheme that lets staff vouch for code they want to install won’t do any damage.... [...]
