Title: Microsoft: Chinese Cyberspies Used 4 Exchange Server Flaws to Plunder Emails
Date: 2021-03-02T21:19:17+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Time to Patch;CVE-2021-26855;CVE-2021-26857;CVE-2021-26858;CVE-2021-27065;Hafnium;Microsoft Exchange;zero day
Slug: microsoft-chinese-cyberspies-used-4-exchange-server-flaws-to-plunder-emails

[Source](https://krebsonsecurity.com/2021/03/microsoft-chinese-cyberspies-used-4-exchange-server-flaws-to-plunder-emails/){:target="_blank" rel="noopener"}

> Microsoft Corp. today released software updates to plug four security holes that attackers have been using to plunder email communications at companies that use its Exchange Server products. The company says all four flaws are being actively exploited as part of a complex attack chain deployed by a previously unidentified Chinese cyber espionage group. The software giant typically releases security updates on the second Tuesday of each month, but it occasionally deviates from that schedule when addressing active attacks that target newly identified and serious vulnerabilities in its products. The patches released today fix security problems in Microsoft Exchange Server [...]
