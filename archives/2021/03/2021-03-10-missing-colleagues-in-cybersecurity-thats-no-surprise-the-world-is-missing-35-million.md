Title: Missing colleagues in cybersecurity? That’s no surprise – the world is missing 3.5 million
Date: 2021-03-10T07:00:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: missing-colleagues-in-cybersecurity-thats-no-surprise-the-world-is-missing-35-million

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/10/managing_your_cybersec_talent/){:target="_blank" rel="noopener"}

> SANS Institute lines up webcast guide to managing your cybersec talent Promo You might think not seeing too much of your cybersecurity colleagues is a good thing – it means everything is going smoothly, doesn’t it? Or it could be that your security team is worrying short-handed. In fact, research by CyberSecurity Ventures predicted 3.5 million cybersecurity jobs will go unfulfilled globally this year.... [...]
