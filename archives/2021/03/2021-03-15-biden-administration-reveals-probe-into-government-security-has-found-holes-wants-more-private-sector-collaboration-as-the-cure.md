Title: Biden administration reveals probe into government security has found holes, wants more private sector collaboration as the cure
Date: 2021-03-15T04:59:04+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: biden-administration-reveals-probe-into-government-security-has-found-holes-wants-more-private-sector-collaboration-as-the-cure

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/15/biden_administration_outlines_response_to/){:target="_blank" rel="noopener"}

> Plans include replicating Singapore’s consumer tech security ratings scheme The Biden administration has outlined its plan to address US government security in the wake of attacks on SolarWinds' Orion platform and Microsoft Exchange, with closer private sector collaboration the centerpiece of its response.... [...]
