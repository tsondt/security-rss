Title: Encrypted phones biz Sky Global shuts up shop after CEO indictment, police raids on users in Europe
Date: 2021-03-19T07:11:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: encrypted-phones-biz-sky-global-shuts-up-shop-after-ceo-indictment-police-raids-on-users-in-europe

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/19/sky_global_ecc_shuts_down_indictment_raids/){:target="_blank" rel="noopener"}

> Tell us more about those services you say were stealing your brand Encrypted phone network Sky Global has seemingly shut down after European police swooped on users and distributors, and its chief exec was indicted by American prosecutors.... [...]
