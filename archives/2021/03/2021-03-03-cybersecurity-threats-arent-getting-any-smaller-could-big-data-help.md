Title: Cybersecurity threats aren't getting any smaller. Could big data help?
Date: 2021-03-03T16:00:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: cybersecurity-threats-arent-getting-any-smaller-could-big-data-help

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/03/big_data_analytics_to_cybersecurity/){:target="_blank" rel="noopener"}

> Spend a little time with our Tim and Nutanix’s Kong Yang – and learn how to crack the conundrum Webcast Upheaval always creates opportunity, and it’s often cyber criminals who are the first to exploit it.... [...]
