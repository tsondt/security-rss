Title: Molson Coors Cracks Open a Cyberattack Investigation
Date: 2021-03-12T15:39:57+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Hacks;Malware;Vulnerabilities
Slug: molson-coors-cracks-open-a-cyberattack-investigation

[Source](https://threatpost.com/molson-coors-cyberattack-investigation/164722/){:target="_blank" rel="noopener"}

> The multinational brewing company did not say what type of incident caused a ‘systems outage,’ but it's investigating and working to get networks back online. [...]
