Title: Executive Order Would Strengthen Cybersecurity Requirements for Federal Agencies
Date: 2021-03-26T20:08:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Government;Hacks
Slug: executive-order-would-strengthen-cybersecurity-requirements-for-federal-agencies

[Source](https://threatpost.com/executive-order-cybersecurity-federal-agencies/165056/){:target="_blank" rel="noopener"}

> The post-SolarWinds EO could be issued as soon as next week, according to a report. [...]
