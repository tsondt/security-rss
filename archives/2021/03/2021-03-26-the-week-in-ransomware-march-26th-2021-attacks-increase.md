Title: The Week in Ransomware - March 26th 2021 - Attacks increase
Date: 2021-03-26T19:45:19-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-march-26th-2021-attacks-increase

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-26th-2021-attacks-increase/){:target="_blank" rel="noopener"}

> Ransomware attacks against the enterprise continue in the form of Accellion data leaks, full-fledged ransomware attacks, and more ransomware gangs targeting Microsoft Exchange. [...]
