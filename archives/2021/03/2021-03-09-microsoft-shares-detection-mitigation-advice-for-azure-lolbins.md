Title: Microsoft shares detection, mitigation advice for Azure LoLBins
Date: 2021-03-09T13:05:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-shares-detection-mitigation-advice-for-azure-lolbins

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-detection-mitigation-advice-for-azure-lolbins/){:target="_blank" rel="noopener"}

> Azure LoLBins can be used by attackers to bypass network defenses, deploy cryptominers, elevate privileges, and disable real-time protection on a targeted device. [...]
