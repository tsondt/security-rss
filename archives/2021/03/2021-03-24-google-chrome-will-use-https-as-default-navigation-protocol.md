Title: Google Chrome will use HTTPS as default navigation protocol
Date: 2021-03-24T15:27:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-chrome-will-use-https-as-default-navigation-protocol

[Source](https://www.bleepingcomputer.com/news/google/google-chrome-will-use-https-as-default-navigation-protocol/){:target="_blank" rel="noopener"}

> Google Chrome will switch to choosing HTTPS as the default protocol for all URLs typed in the address bar, starting with the web browser's next stable version. [...]
