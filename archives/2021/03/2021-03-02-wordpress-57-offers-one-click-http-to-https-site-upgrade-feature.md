Title: WordPress 5.7 offers ‘one-click’ HTTP to HTTPS site upgrade feature
Date: 2021-03-02T16:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: wordpress-57-offers-one-click-http-to-https-site-upgrade-feature

[Source](https://portswigger.net/daily-swig/wordpress-5-7-offers-one-click-http-to-https-site-upgrade-feature){:target="_blank" rel="noopener"}

> Pain relief for mixed-content headaches goes mainstream next week [...]
