Title: AWS achieves its first ISMAP certification in Japan
Date: 2021-03-17T00:32:47+00:00
Author: Niyaz Noor
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;AWS Compliance;Information System Security Management and Assessment Program;ISMAP;Japan;Japan public sector;Security Blog
Slug: aws-achieves-its-first-ismap-certification-in-japan

[Source](https://aws.amazon.com/blogs/security/aws-achieves-its-first-ismap-certification-in-japan/){:target="_blank" rel="noopener"}

> Earning and maintaining customer trust is an ongoing commitment at Amazon Web Services (AWS). Our customers’ industry security requirements drive the scope and portfolio of the compliance reports, attestations, and certifications we pursue. We’re excited to announce that AWS has achieved certification under the Information System Security Management and Assessment Program (ISMAP) program, effective from March [...]
