Title: SolarWinds reports $3.5 million in expenses from supply-chain attack
Date: 2021-03-02T12:42:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: solarwinds-reports-35-million-in-expenses-from-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/solarwinds-reports-35-million-in-expenses-from-supply-chain-attack/){:target="_blank" rel="noopener"}

> SolarWinds has reported expenses of $3.5 million from last year's supply-chain attack, including costs related to incident investigation and remediation. [...]
