Title: Smart doorbells on business premises make your property more attractive to burglars, warns researcher
Date: 2021-03-15T20:07:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: smart-doorbells-on-business-premises-make-your-property-more-attractive-to-burglars-warns-researcher

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/15/smart_locks_attract_burglars_business_premises/){:target="_blank" rel="noopener"}

> Spend your cash on real locks, advises Cranfield University Installing a smart doorbell on your abode could actually increase your home's attractiveness to burglars, researchers from Britain's Cranfield University have said.... [...]
