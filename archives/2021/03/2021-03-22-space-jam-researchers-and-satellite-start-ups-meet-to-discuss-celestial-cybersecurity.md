Title: Space jam: Researchers and satellite start-ups meet to discuss celestial cybersecurity&nbsp;
Date: 2021-03-22T16:45:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: space-jam-researchers-and-satellite-start-ups-meet-to-discuss-celestial-cybersecurity

[Source](https://portswigger.net/daily-swig/space-jam-researchers-and-satellite-start-ups-meet-to-discuss-celestial-cybersecurity-nbsp){:target="_blank" rel="noopener"}

> Space industry can no longer rely on ‘security through obscurity’, Cysat ’21 delegates hear [...]
