Title: Brit cybercops issue tender to rip and replace their formerly flaw-ridden CyberAlarm tool
Date: 2021-03-10T09:30:14+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: brit-cybercops-issue-tender-to-rip-and-replace-their-formerly-flaw-ridden-cyberalarm-tool

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/10/police_cyberalarm_pervade_software_new_tender/){:target="_blank" rel="noopener"}

> Plus: Where did their original logo come from? Police have issued a tender to replace their CyberAlarm tool following reporting by The Register and infosec researchers revealing security flaws in the logging software.... [...]
