Title: SolarWinds patches critical code execution bug in Orion Platform
Date: 2021-03-26T09:19:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: solarwinds-patches-critical-code-execution-bug-in-orion-platform

[Source](https://www.bleepingcomputer.com/news/security/solarwinds-patches-critical-code-execution-bug-in-orion-platform/){:target="_blank" rel="noopener"}

> SolarWinds has released security updates to address four vulnerabilities impacting the company's Orion IT monitoring platform, two o them allowing remote attackers to execute arbitrary code following exploitation. [...]
