Title: Is Your Browser Extension a Botnet Backdoor?
Date: 2021-03-01T17:22:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;chrome extensions;chrome-stats.com;Cisco Systems;crxcavator.io;extendbalanc;Hao Nguyen;HolaVPN;Infatica;iNinja VPN;Luminati;ModHeader;Vladimir M. Fomenko
Slug: is-your-browser-extension-a-botnet-backdoor

[Source](https://krebsonsecurity.com/2021/03/is-your-browser-extension-a-botnet-backdoor/){:target="_blank" rel="noopener"}

> A company that rents out access to more than 10 million Web browsers so that clients can hide their true Internet addresses has built its network by paying browser extension makers to quietly include its code in their creations. This story examines the lopsided economics of extension development, and why installing an extension can be such a risky proposition. Singapore-based Infatica[.]io is part of a growing industry of shadowy firms trying to woo developers who maintain popular browser extensions — desktop and mobile device software add-ons available for download from Apple, Google, Microsoft and Mozilla designed to add functionality or [...]
