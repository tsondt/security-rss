Title: No, RSA Is Not Broken
Date: 2021-03-05T16:48:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cryptanalysis;cryptography;RSA
Slug: no-rsa-is-not-broken

[Source](https://www.schneier.com/blog/archives/2021/03/no-rsa-is-not-broken.html){:target="_blank" rel="noopener"}

> I have been seeing this paper by cryptographer Peter Schnorr making the rounds: “Fast Factoring Integers by SVP Algorithms.” It describes a new factoring method, and its abstract ends with the provocative sentence: “This destroys the RSA cryptosystem.” It does not. At best, it’s an improvement in factoring — and I’m not sure it’s even that. The paper is a preprint: it hasn’t been peer reviewed. Be careful taking its claims at face value. Some discussion here. I’ll append more analysis links to this post when I find them. [...]
