Title: CISA releases new SolarWinds malicious activity detection tool
Date: 2021-03-18T15:56:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-releases-new-solarwinds-malicious-activity-detection-tool

[Source](https://www.bleepingcomputer.com/news/security/cisa-releases-new-solarwinds-malicious-activity-detection-tool/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has released a new tool to detect post-compromise malicious activity associated with the SolarWinds hackers in on-premises enterprise environments. [...]
