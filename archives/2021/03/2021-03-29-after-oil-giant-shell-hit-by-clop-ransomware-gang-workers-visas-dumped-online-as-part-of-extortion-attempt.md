Title: After oil giant Shell hit by Clop ransomware gang, workers' visas dumped online as part of extortion attempt
Date: 2021-03-29T23:46:22+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: after-oil-giant-shell-hit-by-clop-ransomware-gang-workers-visas-dumped-online-as-part-of-extortion-attempt

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/29/shell_clop_ransomware_leaks_update/){:target="_blank" rel="noopener"}

> Another day, another data nightmare Updated Royal Dutch Shell is the latest corporation to be attacked by the Clop ransomware gang.... [...]
