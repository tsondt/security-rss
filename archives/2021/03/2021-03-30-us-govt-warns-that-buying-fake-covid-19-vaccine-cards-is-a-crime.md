Title: US govt warns that buying fake COVID-19 vaccine cards is a crime
Date: 2021-03-30T14:50:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-govt-warns-that-buying-fake-covid-19-vaccine-cards-is-a-crime

[Source](https://www.bleepingcomputer.com/news/security/us-govt-warns-that-buying-fake-covid-19-vaccine-cards-is-a-crime/){:target="_blank" rel="noopener"}

> US federal agencies have warned today against making or selling fake COVID-19 vaccination record cards as this is breaking the law. [...]
