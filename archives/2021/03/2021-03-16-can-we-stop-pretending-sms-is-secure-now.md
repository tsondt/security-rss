Title: Can We Stop Pretending SMS Is Secure Now?
Date: 2021-03-16T22:30:28+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Allison Nixon;ALT-SPID;Lucky225;NetNumber;NPAC;Number Portability Administration Center;SIM swapping;Unit221B
Slug: can-we-stop-pretending-sms-is-secure-now

[Source](https://krebsonsecurity.com/2021/03/can-we-stop-pretending-sms-is-secure-now/){:target="_blank" rel="noopener"}

> SMS text messages were already the weakest link securing just about anything online, mainly because there are tens of thousands of people (many of them low-paid mobile store employees) who can be tricked or bribed into swapping control over a mobile phone number to someone else. Now we’re learning about an entire ecosystem of companies that anyone could use to silently intercept text messages intended for other mobile users. Security researcher “ Lucky225 ” worked with Vice.com’s Joseph Cox to intercept Cox’s incoming text messages with his permission. Lucky225 showed how anyone could do the same after creating an account [...]
