Title: Explainer: What does the UK’s Integrated Review mean for cybersecurity?
Date: 2021-03-23T13:38:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: explainer-what-does-the-uks-integrated-review-mean-for-cybersecurity

[Source](https://portswigger.net/daily-swig/explainer-what-does-the-uks-integrated-review-mean-for-cybersecurity){:target="_blank" rel="noopener"}

> Stephen Pritchard sheds light on the government’s new defense strategy [...]
