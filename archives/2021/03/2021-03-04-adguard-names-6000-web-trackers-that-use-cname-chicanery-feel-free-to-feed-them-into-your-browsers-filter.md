Title: AdGuard names 6,000+ web trackers that use CNAME chicanery: Feel free to feed them into your browser's filter
Date: 2021-03-04T21:18:32+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: adguard-names-6000-web-trackers-that-use-cname-chicanery-feel-free-to-feed-them-into-your-browsers-filter

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/04/adguard_cname_tracker/){:target="_blank" rel="noopener"}

> Assuming your content blocker can scrutinize DNS AdGuard on Thursday published a list of more than 6,000 CNAME-based trackers so they can be incorporated into content-blocking filters.... [...]
