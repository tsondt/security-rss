Title: Cockup or conspiracy? Popular privacy extension ClearURLs removed from Chrome web store
Date: 2021-03-24T17:08:10+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: cockup-or-conspiracy-popular-privacy-extension-clearurls-removed-from-chrome-web-store

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/24/popular_privacy_extension_clearurls_removed/){:target="_blank" rel="noopener"}

> Developer appeals decision, saying 'the reasons are ridiculous' The Chrome browser extension ClearURLs has been removed from the Chrome Web Store, for reasons its developer describes as "ridiculous."... [...]
