Title: Microsoft Offers Up To $30K For Teams Bugs
Date: 2021-03-25T20:04:36+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Bug Bounty;Cloud Security;Vulnerabilities
Slug: microsoft-offers-up-to-30k-for-teams-bugs

[Source](https://threatpost.com/microsoft-30k-teams-bugs/165037/){:target="_blank" rel="noopener"}

> A bug-bounty program launched for the Teams desktop videoconferencing and collaboration application has big payouts for finding security holes. [...]
