Title: Microsoft: 92% of Exchange servers safe from ProxyLogon attacks
Date: 2021-03-23T10:33:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-92-of-exchange-servers-safe-from-proxylogon-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-92-percent-of-exchange-servers-safe-from-proxylogon-attacks/){:target="_blank" rel="noopener"}

> Roughly 92% of all Internet-connected on-premises Microsoft Exchange servers affected by the ProxyLogon vulnerabilities are now patched and safe from attacks, Microsoft said on Monday. [...]
