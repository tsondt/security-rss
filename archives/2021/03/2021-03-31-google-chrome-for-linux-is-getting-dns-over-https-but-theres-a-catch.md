Title: Google Chrome for Linux is getting DNS-over-HTTPS, but there's a catch
Date: 2021-03-31T03:41:14-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Google
Slug: google-chrome-for-linux-is-getting-dns-over-https-but-theres-a-catch

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-for-linux-is-getting-dns-over-https-but-theres-a-catch/){:target="_blank" rel="noopener"}

> Google Chrome developers have announced plans to roll out DNS-over-HTTPS (DoH) support to Chrome web browser for Linux. DoH has been supported on Google Chrome for other platforms, including Android, since at least 2020. But, there's a catch. [...]
