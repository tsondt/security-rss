Title: Cisco Plugs Security Hole in Small Business Routers
Date: 2021-03-17T20:26:52+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: cisco-plugs-security-hole-in-small-business-routers

[Source](https://threatpost.com/cisco-security-hole-small-business-routers/164859/){:target="_blank" rel="noopener"}

> The Cisco security vulnerability exists in the RV132W ADSL2+ Wireless-N VPN Routers and RV134W VDSL2 Wireless-AC VPN Routers. [...]
