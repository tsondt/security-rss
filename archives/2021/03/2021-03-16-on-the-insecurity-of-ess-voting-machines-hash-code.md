Title: On the Insecurity of ES&amp;S Voting Machines’ Hash Code
Date: 2021-03-16T11:36:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: authentication;certifications;hashes;voting
Slug: on-the-insecurity-of-ess-voting-machines-hash-code

[Source](https://www.schneier.com/blog/archives/2021/03/on-the-insecurity-of-ess-voting-machines-hash-code.html){:target="_blank" rel="noopener"}

> Andrew Appel and Susan Greenhalgh have a blog post on the insecurity of ES&S’s software authentication system: It turns out that ES&S has bugs in their hash-code checker: if the “reference hashcode” is completely missing, then it’ll say “yes, boss, everything is fine” instead of reporting an error. It’s simultaneously shocking and unsurprising that ES&S’s hashcode checker could contain such a blunder and that it would go unnoticed by the U.S. Election Assistance Commission’s federal certification process. It’s unsurprising because testing naturally tends to focus on “does the system work right when used as intended?” Using the system in unintended [...]
