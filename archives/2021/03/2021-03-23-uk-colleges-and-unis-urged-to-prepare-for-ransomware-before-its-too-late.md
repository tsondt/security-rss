Title: UK colleges and unis urged to prepare for ransomware before it's too late
Date: 2021-03-23T15:00:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-colleges-and-unis-urged-to-prepare-for-ransomware-before-its-too-late

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/23/ransomware_targeting_education_ncsc_warning/){:target="_blank" rel="noopener"}

> Uptick in attacks since schools reopened, warns National Cyber Security Centre Britain's National Cyber Security Centre (NCSC) has urged universities, schools, and colleges to be vigilant following an increase in ransomware attacks targeting educational institutions.... [...]
