Title: Essential security for everyone: Building a secure AWS foundation
Date: 2021-03-02T17:33:18+00:00
Author: Byron Pogson
Category: AWS Security
Tags: Best Practices;Foundational (100);Security, Identity, & Compliance;Security Blog;Startup
Slug: essential-security-for-everyone-building-a-secure-aws-foundation

[Source](https://aws.amazon.com/blogs/security/essential-security-for-everyone-building-a-secure-aws-foundation/){:target="_blank" rel="noopener"}

> In this post, I will show you how teams of all sizes can gain access to world-class security in the cloud without a dedicated security person in your organization. I look at how small teams can build securely on Amazon Web Services (AWS) in a way that’s cost effective and time efficient. I show you [...]
