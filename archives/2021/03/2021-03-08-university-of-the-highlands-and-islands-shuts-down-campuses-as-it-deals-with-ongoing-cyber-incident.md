Title: University of the Highlands and Islands shuts down campuses as it deals with 'ongoing cyber incident'
Date: 2021-03-08T14:55:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: university-of-the-highlands-and-islands-shuts-down-campuses-as-it-deals-with-ongoing-cyber-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/08/uni_highlands_islands_cyber_incident/){:target="_blank" rel="noopener"}

> Ten letters, starts with R, ends with E, three syllables The University of the Highlands and Islands (UHI) in Scotland is fending off "an ongoing cyber incident" that has shut down its campuses.... [...]
