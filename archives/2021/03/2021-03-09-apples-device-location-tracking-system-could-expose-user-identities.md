Title: Apple’s Device Location-Tracking System Could Expose User Identities
Date: 2021-03-09T23:31:44+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy;Vulnerabilities
Slug: apples-device-location-tracking-system-could-expose-user-identities

[Source](https://threatpost.com/apples-location-system-expose-identities/164615/){:target="_blank" rel="noopener"}

> Researchers have identified two vulnerabilities in the company’s crowd-sourced Offline Finding technology that could jeopardize its promise of privacy. [...]
