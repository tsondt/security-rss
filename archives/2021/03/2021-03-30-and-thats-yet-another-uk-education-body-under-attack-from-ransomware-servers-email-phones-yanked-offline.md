Title: And that's yet another UK education body under attack from ransomware: Servers, email, phones yanked offline
Date: 2021-03-30T12:12:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: and-thats-yet-another-uk-education-body-under-attack-from-ransomware-servers-email-phones-yanked-offline

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/30/harris_federation_ransomware/){:target="_blank" rel="noopener"}

> The Harris Federation learns infosec lessons the hard way The Harris Federation, a not-for-profit charity responsible for running 50 primary and secondary academies in London and Essex, has become the latest UK education body to fall victim to ransomware.... [...]
