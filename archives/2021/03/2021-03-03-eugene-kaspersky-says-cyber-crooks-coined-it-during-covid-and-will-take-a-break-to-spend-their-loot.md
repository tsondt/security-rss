Title: Eugene Kaspersky says cyber-crooks coined it during COVID and will take a break to spend their loot
Date: 2021-03-03T05:58:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: eugene-kaspersky-says-cyber-crooks-coined-it-during-covid-and-will-take-a-break-to-spend-their-loot

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/03/eugene_kaspersky_post_covid_security_predictions/){:target="_blank" rel="noopener"}

> Not so fast, says infosec boffin, because crims know returning workers will be easy prey Kaspersky Lab CEO Eugene Kaspersky has suggested that the end of the COVID-19 pandemic will bring a slowdown in cyber-crime.... [...]
