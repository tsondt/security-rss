Title: Android sends 20x more data to Google than iOS sends to Apple, study says
Date: 2021-03-30T22:18:06+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;android;apple;data;google;iOS;privacy
Slug: android-sends-20x-more-data-to-google-than-ios-sends-to-apple-study-says

[Source](https://arstechnica.com/?p=1753357){:target="_blank" rel="noopener"}

> Enlarge / Insomnia people and mobile-addiction concepts. (credit: Getty Images ) Whether you have an iPhone or an Android device, it’s continuously sending data including your location, phone number, and local network details to Apple or Google. Now, a researcher has provided a side-by-side comparison that suggests that, while both iOS and Android collect handset data around the clock—even when devices are idle, just out of the box, or after users have opted out—the Google mobile OS collects about 20 times as much data than its Apple competitor. Both iOS and Android, researcher Douglas Leith from Trinity College in Ireland [...]
