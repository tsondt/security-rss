Title: LINE stops data flowing to China after Japanese officials ditch app over privacy concerns
Date: 2021-03-24T03:31:25+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: line-stops-data-flowing-to-china-after-japanese-officials-ditch-app-over-privacy-concerns

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/24/line_blocks_china_server_access/){:target="_blank" rel="noopener"}

> App's owner says users didn't bail Messaging app LINE has removed Chinese affiliate’s access to personal data, after infosec concerns led Japanese government officials to stop using the app. Information stored in South Korea will also be rehomed to Japan by September.... [...]
