Title: F5, CISA Warn of Critical BIG-IP and BIG-IQ RCE Bugs
Date: 2021-03-11T14:21:50+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: f5-cisa-warn-of-critical-big-ip-and-big-iq-rce-bugs

[Source](https://threatpost.com/f5-cisa-critical-rce-bugs/164679/){:target="_blank" rel="noopener"}

> The F5 flaws could affect the networking infrastructure for some of the largest tech and Fortune 500 companies - including Microsoft, Oracle and Facebook. [...]
