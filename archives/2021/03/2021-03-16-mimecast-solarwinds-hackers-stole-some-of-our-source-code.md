Title: Mimecast: SolarWinds hackers stole some of our source code
Date: 2021-03-16T12:53:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: mimecast-solarwinds-hackers-stole-some-of-our-source-code

[Source](https://www.bleepingcomputer.com/news/security/mimecast-solarwinds-hackers-stole-some-of-our-source-code/){:target="_blank" rel="noopener"}

> Email security company Mimecast has confirmed today that the state-sponsored SolarWinds hackers who breached its network earlier this year used the Sunburst backdoor during the initial intrusion. [...]
