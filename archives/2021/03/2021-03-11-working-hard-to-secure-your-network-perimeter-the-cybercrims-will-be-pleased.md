Title: Working hard to secure your network perimeter? The cybercrims will be pleased…
Date: 2021-03-11T07:58:09+00:00
Author: Elyse Silverberg
Category: The Register
Tags: 
Slug: working-hard-to-secure-your-network-perimeter-the-cybercrims-will-be-pleased

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/11/working_hard_to_secure_your/){:target="_blank" rel="noopener"}

> Because that’s not where the threat is anymore Webcast If you’re currently running your company’s cyber security operation from home, ask yourself, where exactly is the “perimeter” you’re supposed to be protecting?... [...]
