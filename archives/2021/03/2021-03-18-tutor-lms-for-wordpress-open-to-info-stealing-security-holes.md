Title: Tutor LMS for WordPress Open to Info-Stealing Security Holes
Date: 2021-03-18T11:50:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: tutor-lms-for-wordpress-open-to-info-stealing-security-holes

[Source](https://threatpost.com/tutor-lms-wordpress-security-holes/164868/){:target="_blank" rel="noopener"}

> The popular learning-management system for teacher-student communication is rife with SQL-injection vulnerabilities. [...]
