Title: FBI warns of escalating Pysa ransomware attacks on education orgs
Date: 2021-03-16T11:22:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-escalating-pysa-ransomware-attacks-on-education-orgs

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-escalating-pysa-ransomware-attacks-on-education-orgs/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) Cyber Division has warned system administrators and cybersecurity professionals of increased Pysa ransomware activity targeting educational institutions. [...]
