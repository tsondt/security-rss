Title: RedTorch Formed from Ashes of Norse Corp.
Date: 2021-03-22T20:36:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Cheetah Counter Surveillance;Frigg;Henry Marx;Mr. Grey;Mr. White;Norse Corp.;Norse Networks;RedTorch;Tommy Stiansen;Tyson Yee
Slug: redtorch-formed-from-ashes-of-norse-corp

[Source](https://krebsonsecurity.com/2021/03/redtorch-formed-from-ashes-of-norse-corp/){:target="_blank" rel="noopener"}

> Remember Norse Corp., the company behind the interactive “pew-pew” cyber attack map shown in the image below? Norse imploded rather suddenly in 2016 following a series of managerial missteps and funding debacles. Now, the founders of Norse have launched a new company with a somewhat different vision: RedTorch, which for the past two years has marketed a mix of services to high end celebrity clients, including spying and anti-spying tools and services. A snapshot of Norse’s semi-live attack map, circa Jan. 2016. Norse’s attack map was everywhere for several years, and even became a common sight in the “brains” of [...]
