Title: Perl.com theft blamed on social engineering attack: Registrar 'convinced' to alter DNS records by miscreants
Date: 2021-03-02T08:25:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: perlcom-theft-blamed-on-social-engineering-attack-registrar-convinced-to-alter-dns-records-by-miscreants

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/02/perl_domain_theft/){:target="_blank" rel="noopener"}

> Network Solutions hasn't confirmed what happened, though The short-lived theft of Perl.com in late January is believed to have been the result of a social engineering attack that convinced registrar Network Solutions to alter the domain's records without valid authorization.... [...]
