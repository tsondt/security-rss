Title: CompuCom MSP expects over $20M in losses after ransomware attack
Date: 2021-03-28T10:41:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: compucom-msp-expects-over-20m-in-losses-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/compucom-msp-expects-over-20m-in-losses-after-ransomware-attack/){:target="_blank" rel="noopener"}

> American managed service provider CompuCom is expecting losses of over $20 million following this month's DarkSide ransomware attack that took down most of its systems. [...]
