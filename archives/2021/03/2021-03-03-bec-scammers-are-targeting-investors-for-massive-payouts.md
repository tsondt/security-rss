Title: BEC scammers are targeting investors for massive payouts
Date: 2021-03-03T14:53:59-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: bec-scammers-are-targeting-investors-for-massive-payouts

[Source](https://www.bleepingcomputer.com/news/security/bec-scammers-are-targeting-investors-for-massive-payouts/){:target="_blank" rel="noopener"}

> Business email compromise (BEC) scammers are utilizing a new type of attack targeting investors that could leverage payouts seven times greater than average. [...]
