Title: Defence Industrial Strategy suggests the UK is ready to start taking its homegrown infosec industry seriously
Date: 2021-03-25T14:48:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: defence-industrial-strategy-suggests-the-uk-is-ready-to-start-taking-its-homegrown-infosec-industry-seriously

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/25/defence_industrial_strategy_infosec_industry_lures/){:target="_blank" rel="noopener"}

> Doc makes all the right noises if you like government support for business In a change from its recent bombastic blather, the British government has published a new Defence Industrial Strategy that looks like it wants to put the infosec industry on a gold-plated pedestal.... [...]
