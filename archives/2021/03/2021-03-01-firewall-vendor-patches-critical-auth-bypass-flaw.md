Title: Firewall Vendor Patches Critical Auth Bypass Flaw
Date: 2021-03-01T15:59:43+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: firewall-vendor-patches-critical-auth-bypass-flaw

[Source](https://threatpost.com/firewall-critical-security-flaw/164347/){:target="_blank" rel="noopener"}

> Cybersecurity firm Genua fixes a critical flaw in its GenuGate High Resistance Firewall, allowing attackers to log in as root users. [...]
