Title: Phish Leads to Breach at Calif. State Controller
Date: 2021-03-23T18:01:13+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;California Department of Technology;California State Controller breach;KnowBe4;phishing
Slug: phish-leads-to-breach-at-calif-state-controller

[Source](https://krebsonsecurity.com/2021/03/phish-leads-to-breach-at-calif-state-controller/){:target="_blank" rel="noopener"}

> A phishing attack last week gave attackers access to email and files at the California State Controller’s Office (SCO), an agency responsible for handling more than $100 billion in public funds each year. The phishers had access for more than 24 hours, and sources tell KrebsOnSecurity the intruders used that time to steal Social Security numbers and sensitive files on thousands of state workers, and to send targeted phishing messages to at least 9,000 other workers and their contacts. A notice of breach posted by the California State Controller’s Office. In a “ Notice of Data Breach ” message posted [...]
