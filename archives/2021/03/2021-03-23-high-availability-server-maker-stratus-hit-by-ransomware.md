Title: High-availability server maker Stratus hit by ransomware
Date: 2021-03-23T12:46:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: high-availability-server-maker-stratus-hit-by-ransomware

[Source](https://www.bleepingcomputer.com/news/security/high-availability-server-maker-stratus-hit-by-ransomware/){:target="_blank" rel="noopener"}

> Stratus Technologies has suffered a ransomware attack that required systems to be taken offline to prevent the attack's spread. [...]
