Title: OpenSSL fixes severe DoS, certificate validation vulnerabilities
Date: 2021-03-25T12:44:46-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: openssl-fixes-severe-dos-certificate-validation-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/openssl-fixes-severe-dos-certificate-validation-vulnerabilities/){:target="_blank" rel="noopener"}

> OpenSSL has patched two high severity vulnerabilities. These include a Denial of Service (DoS) vulnerability (CVE-2021-3449) and an improper CA certificate validation issue (CVE-2021-3450). [...]
