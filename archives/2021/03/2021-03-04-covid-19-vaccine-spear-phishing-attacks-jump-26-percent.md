Title: COVID-19 Vaccine Spear-Phishing Attacks Jump 26 Percent
Date: 2021-03-04T16:01:15+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security
Slug: covid-19-vaccine-spear-phishing-attacks-jump-26-percent

[Source](https://threatpost.com/covid-19-vaccine-spear-phishing-attacks/164489/){:target="_blank" rel="noopener"}

> Cybercriminals are using the COVID-19 vaccine to steal Microsoft credentials, infect systems with malware and bilk victims out of hundreds of dollars. [...]
