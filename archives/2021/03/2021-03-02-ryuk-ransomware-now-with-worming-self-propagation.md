Title: Ryuk Ransomware: Now with Worming Self-Propagation
Date: 2021-03-02T16:54:03+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware
Slug: ryuk-ransomware-now-with-worming-self-propagation

[Source](https://threatpost.com/ryuk-ransomware-worming-self-propagation/164412/){:target="_blank" rel="noopener"}

> The Ryuk scourge has a new trick in its arsenal: Self-replication via SMB shares and port scanning. [...]
