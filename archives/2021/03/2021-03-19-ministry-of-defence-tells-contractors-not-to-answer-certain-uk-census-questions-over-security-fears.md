Title: Ministry of Defence tells contractors not to answer certain UK census questions over security fears
Date: 2021-03-19T14:27:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ministry-of-defence-tells-contractors-not-to-answer-certain-uk-census-questions-over-security-fears

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/19/ministry_defence_tells_staff_dont_answer_census/){:target="_blank" rel="noopener"}

> But there are legal protections... right? The Ministry of Defence has ordered its contractors not to answer certain questions on the UK's once-in-a-decade census – despite threats of £1,000 fines being handed to people who don't complete the national survey.... [...]
