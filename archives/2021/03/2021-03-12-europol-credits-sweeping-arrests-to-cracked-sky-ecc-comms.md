Title: Europol Credits Sweeping Arrests to Cracked Sky ECC Comms
Date: 2021-03-12T20:41:36+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cryptography;Government;Hacks;Mobile Security;Privacy
Slug: europol-credits-sweeping-arrests-to-cracked-sky-ecc-comms

[Source](https://threatpost.com/europol-arrests-cracked-sky-ecc/164744/){:target="_blank" rel="noopener"}

> Sky ECC claims that cops cracked a fake version of the app being passed off by disgruntled reseller. [...]
