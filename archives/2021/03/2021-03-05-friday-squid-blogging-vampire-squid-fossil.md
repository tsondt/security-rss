Title: Friday Squid Blogging: Vampire Squid Fossil
Date: 2021-03-05T22:07:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-vampire-squid-fossil

[Source](https://www.schneier.com/blog/archives/2021/03/friday-squid-blogging-vampire-squid-fossil.html){:target="_blank" rel="noopener"}

> A 30-million-year-old vampire squid fossil was found, lost, and then re-found in Hungary. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
