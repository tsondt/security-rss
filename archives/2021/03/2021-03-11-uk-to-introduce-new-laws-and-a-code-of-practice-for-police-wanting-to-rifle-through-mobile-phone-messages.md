Title: UK to introduce new laws and a code of practice for police wanting to rifle through mobile phone messages
Date: 2021-03-11T11:30:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-to-introduce-new-laws-and-a-code-of-practice-for-police-wanting-to-rifle-through-mobile-phone-messages

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/11/mobile_phone_extraction_law_proposals/){:target="_blank" rel="noopener"}

> But there's a lot more to worry about in new Bill, say campaigners A new UK law will explicitly authorise the "voluntary" slurping of data from mobile phones of crime suspects and witnesses.... [...]
