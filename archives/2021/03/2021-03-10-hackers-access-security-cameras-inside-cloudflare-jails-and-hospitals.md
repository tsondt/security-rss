Title: Hackers access security cameras inside Cloudflare, jails, and hospitals
Date: 2021-03-10T05:14:35+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;hacking;privacy;security cameras;surveillance
Slug: hackers-access-security-cameras-inside-cloudflare-jails-and-hospitals

[Source](https://arstechnica.com/?p=1748489){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Hackers say they broke into the network of Silicon Valley startup Verkada and gained access to live video feeds from more than 150,000 surveillance cameras the company manages for Cloudflare, Tesla, and a host of other organizations. The group published videos and images they said were taken from offices, warehouses, and factories of those companies as well as from jail cells, psychiatric wards, banks, and schools. Bloomberg News, which first reported the breach, said footage viewed by a reporter showed staffers at Florida hospital Halifax Health tackling a man and pinning him to a bed. Another [...]
