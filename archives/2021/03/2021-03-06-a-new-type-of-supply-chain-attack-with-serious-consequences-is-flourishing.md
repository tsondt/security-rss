Title: A new type of supply-chain attack with serious consequences is flourishing
Date: 2021-03-06T15:15:19+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;dependency confusion;malware;network compromise;supply chain
Slug: a-new-type-of-supply-chain-attack-with-serious-consequences-is-flourishing

[Source](https://arstechnica.com/?p=1747637){:target="_blank" rel="noopener"}

> Enlarge (credit: Przemyslaw Klos / EyeEm / Getty Images ) A new type of supply chain attack unveiled last month is targeting more and more companies, with new rounds this week taking aim at Microsoft, Amazon, Slack, Lyft, Zillow, and an unknown number of others. In weeks past, Apple, Microsoft, Tesla, and 32 other companies were targeted by a similar attack that allowed a security researcher to execute unauthorized code inside their networks. The latest attack against Microsoft was also carried out as a proof-of-concept by a researcher. Attacks targeting Amazon, Slack, Lyft, and Zillow, by contrast, were malicious, but [...]
