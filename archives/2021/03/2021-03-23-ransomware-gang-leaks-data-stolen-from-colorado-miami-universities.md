Title: Ransomware gang leaks data stolen from Colorado, Miami universities
Date: 2021-03-23T16:20:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-leaks-data-stolen-from-colorado-miami-universities

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-leaks-data-stolen-from-colorado-miami-universities/){:target="_blank" rel="noopener"}

> Grades and social security numbers for students at the University of Colorado and University of Miami patient data have been posted online by the Clop ransomware group. [...]
