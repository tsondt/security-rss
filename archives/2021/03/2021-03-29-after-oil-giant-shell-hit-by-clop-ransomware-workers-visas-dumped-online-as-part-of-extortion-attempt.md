Title: After Oil giant Shell hit by Clop ransomware, workers' visas dumped online as part of extortion attempt
Date: 2021-03-29T23:46:22+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: after-oil-giant-shell-hit-by-clop-ransomware-workers-visas-dumped-online-as-part-of-extortion-attempt

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/29/shell_clop_ransomware/){:target="_blank" rel="noopener"}

> Another day, another data nightmare Royal Dutch Shell is the latest corporation to be infected by the Clop ransomware. The criminals behind the malware have siphoned internal documents from the oil giant, and publicly leaked some of the data – notably a selection of workers' passport and visa scans – to chivy the corporation along to pay the ransom.... [...]
