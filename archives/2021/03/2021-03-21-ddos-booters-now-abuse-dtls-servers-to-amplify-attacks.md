Title: DDoS booters now abuse DTLS servers to amplify attacks
Date: 2021-03-21T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ddos-booters-now-abuse-dtls-servers-to-amplify-attacks

[Source](https://www.bleepingcomputer.com/news/security/ddos-booters-now-abuse-dtls-servers-to-amplify-attacks/){:target="_blank" rel="noopener"}

> DDoS-for-hire services are now actively abusing misconfigured or out-of-date Datagram Transport Layer Security (D/TLS) servers to amplify Distributed Denial of Service (DDoS) attacks. [...]
