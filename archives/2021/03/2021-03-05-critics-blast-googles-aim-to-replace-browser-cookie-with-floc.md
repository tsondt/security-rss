Title: Critics Blast Google’s Aim to Replace Browser Cookie with ‘FLoC’
Date: 2021-03-05T17:24:05+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy
Slug: critics-blast-googles-aim-to-replace-browser-cookie-with-floc

[Source](https://threatpost.com/critics-googles-browser-cookie-floc/164540/){:target="_blank" rel="noopener"}

> EFF worries that the Google's ‘privacy-first” vision for the future may pose new privacy risks. [...]
