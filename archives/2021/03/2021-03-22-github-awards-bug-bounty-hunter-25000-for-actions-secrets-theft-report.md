Title: GitHub awards bug bounty hunter $25,000 for Actions secrets theft report
Date: 2021-03-22T13:06:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: github-awards-bug-bounty-hunter-25000-for-actions-secrets-theft-report

[Source](https://portswigger.net/daily-swig/github-awards-bug-bounty-hunter-25-000-for-actions-secrets-theft-report){:target="_blank" rel="noopener"}

> The vulnerability was found in GitHub’s pull request mechanism [...]
