Title: RTM Cybergang Adds New Quoter Ransomware to Crime Spree
Date: 2021-03-03T19:18:21+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Malware
Slug: rtm-cybergang-adds-new-quoter-ransomware-to-crime-spree

[Source](https://threatpost.com/rtm-banking-trojan-quoter-ransomware/164447/){:target="_blank" rel="noopener"}

> The Russian-speaking RTM threat group is targeting organizations in an ongoing campaign that leverages a well-known banking trojan, brand new ransomware strain and extortion tactics. [...]
