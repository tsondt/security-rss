Title: WeLeakInfo Leaked Customer Payment Info
Date: 2021-03-15T13:05:02+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;fbi;Flashpoint;Gerald Murphy;Mazafaka;pompompurin;RaidForums;Verified;WeLeakInfo
Slug: weleakinfo-leaked-customer-payment-info

[Source](https://krebsonsecurity.com/2021/03/weleakinfo-leaked-customer-payment-info/){:target="_blank" rel="noopener"}

> A little over a year ago, the FBI and law enforcement partners overseas seized WeLeakInfo[.]com, a wildly popular service that sold access to more than 12 billion usernames and passwords stolen from thousands of hacked websites. In an ironic turn of events, a lapsed domain registration tied to WeLeakInfo let someone plunder and publish account data on 24,000 customers who paid to access the service with a credit card. For several years, WeLeakInfo was the largest of several services selling access to hacked passwords. Prosecutors said it had indexed, searchable information from more than 10,000 data breaches containing over 12 [...]
