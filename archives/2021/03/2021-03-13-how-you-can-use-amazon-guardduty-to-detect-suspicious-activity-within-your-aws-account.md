Title: How you can use Amazon GuardDuty to detect suspicious activity within your AWS account
Date: 2021-03-13T00:35:19+00:00
Author: Amit Meggido
Category: AWS Security
Tags: Amazon GuardDuty;Foundational (100);Security, Identity, & Compliance;account compromise;account monitoring;Amazon EC2;cloud protection;detection system;end point protection;IDS;infrastructure monitoring;infrastructure protection;intrusion detection;intrusion prevention;intrusion security system;IPS;monitoring solutions;network monitoring;Security Blog;security monitoring;threat detection
Slug: how-you-can-use-amazon-guardduty-to-detect-suspicious-activity-within-your-aws-account

[Source](https://aws.amazon.com/blogs/security/how-you-can-use-amazon-guardduty-to-detect-suspicious-activity-within-your-aws-account/){:target="_blank" rel="noopener"}

> Amazon GuardDuty is an automated threat detection service that continuously monitors for suspicious activity and unauthorized behavior to protect your AWS accounts, workloads, and data stored in Amazon S3. In this post, I’ll share how you can use GuardDuty with its newly enhanced highly-customized machine learning model to better protect your AWS environment from potential [...]
