Title: Sitting comfortably? Then it's probably time to patch, as critical flaw uncovered in npm's netmask package
Date: 2021-03-29T18:27:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: sitting-comfortably-then-its-probably-time-to-patch-as-critical-flaw-uncovered-in-npms-netmask-package

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/29/netmask_cve/){:target="_blank" rel="noopener"}

> Are you local? Catastrophically local? The widely used npm library netmask has a networking vulnerability arising from how it parses IP addresses with a leading zero, leaving an estimated 278 million projects at risk.... [...]
