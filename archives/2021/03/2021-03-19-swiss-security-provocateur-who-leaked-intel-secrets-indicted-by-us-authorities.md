Title: Swiss security provocateur who leaked Intel secrets indicted by US authorities
Date: 2021-03-19T04:59:45+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: swiss-security-provocateur-who-leaked-intel-secrets-indicted-by-us-authorities

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/19/till_kottmann_indicted/){:target="_blank" rel="noopener"}

> Till Kottmann may also have helped with Verkada vid-cam exploit and other cracks, now accused of fraud on top of merry pranking The US Department of Justice says a grand jury has indicted Swiss security provocateur Till Kottmann over multiple exploits and attempts at fraud, and authorities have quickly moved to rule out free speech as a defence.... [...]
