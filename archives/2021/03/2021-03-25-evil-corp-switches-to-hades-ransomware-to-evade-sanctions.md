Title: Evil Corp switches to Hades ransomware to evade sanctions
Date: 2021-03-25T13:34:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: evil-corp-switches-to-hades-ransomware-to-evade-sanctions

[Source](https://www.bleepingcomputer.com/news/security/evil-corp-switches-to-hades-ransomware-to-evade-sanctions/){:target="_blank" rel="noopener"}

> Hades ransomware has been linked to the Evil Corp cybercrime gang who uses it to evade sanctions imposed by the Treasury Department's Office of Foreign Assets Control (OFAC). [...]
