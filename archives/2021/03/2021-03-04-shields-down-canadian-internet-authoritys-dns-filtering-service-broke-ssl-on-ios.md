Title: Shields down: Canadian internet authority’s DNS filtering service broke SSL on iOS
Date: 2021-03-04T15:48:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: shields-down-canadian-internet-authoritys-dns-filtering-service-broke-ssl-on-ios

[Source](https://portswigger.net/daily-swig/shields-down-canadian-internet-authoritys-dns-filtering-service-broke-ssl-on-ios){:target="_blank" rel="noopener"}

> Canadian Shield developers have pushed out an update to iOS app that fixes potential MitM issue [...]
