Title: Hackers access surveillance cameras at Tesla, Cloudflare, banks, more
Date: 2021-03-09T17:25:19-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: hackers-access-surveillance-cameras-at-tesla-cloudflare-banks-more

[Source](https://www.bleepingcomputer.com/news/security/hackers-access-surveillance-cameras-at-tesla-cloudflare-banks-more/){:target="_blank" rel="noopener"}

> Hackers gained access to live surveillance cameras installed at Tesla, Equinox, healthcare clinics, jails, and banks, including the Bank of Utah. [...]
