Title: Hijacking traffic to Microsoft’s windows.com with bitflipping
Date: 2021-03-04T11:37:15-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Microsoft
Slug: hijacking-traffic-to-microsofts-windowscom-with-bitflipping

[Source](https://www.bleepingcomputer.com/news/security/hijacking-traffic-to-microsoft-s-windowscom-with-bitflipping/){:target="_blank" rel="noopener"}

> A researcher was able to bitsquat Microsoft's windows.com domain by cybersquatting variations of windows.com. Adversaries can abuse this tactic to conduct automated attacks or collect data due to the nature of bit flipping. [...]
