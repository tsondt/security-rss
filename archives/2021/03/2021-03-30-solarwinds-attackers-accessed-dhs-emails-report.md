Title: SolarWinds Attackers Accessed DHS Emails, Report
Date: 2021-03-30T16:54:20+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Hacks;Malware
Slug: solarwinds-attackers-accessed-dhs-emails-report

[Source](https://threatpost.com/solarwinds-attackers-dhs-emails/165110/){:target="_blank" rel="noopener"}

> Current and former administration sources say the nation-state attackers were able to read the Homeland Security Secretary's emails, among others. [...]
