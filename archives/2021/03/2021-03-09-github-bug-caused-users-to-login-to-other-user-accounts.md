Title: GitHub bug caused users to login to other user accounts
Date: 2021-03-09T04:16:56-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: github-bug-caused-users-to-login-to-other-user-accounts

[Source](https://www.bleepingcomputer.com/news/security/github-bug-caused-users-to-login-to-other-user-accounts/){:target="_blank" rel="noopener"}

> Last night, GitHub automatically logged out many users and invalidated their sessions to protect user accounts against a potentially serious security vulnerability. Earlier this month GitHub had received a report of anomalous behavior from an external party. [...]
