Title: US Office of National Intelligence says Russia, Iran tried to mess with 2020 elections, China sat it out
Date: 2021-03-17T02:57:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: us-office-of-national-intelligence-says-russia-iran-tried-to-mess-with-2020-elections-china-sat-it-out

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/17/2020_us_election_security_report/){:target="_blank" rel="noopener"}

> Security precautions held up, but Putin himself signed off on efforts to scare the public with claims of voting system compromise The USA’s Office of National Intelligence today released its previously classified assessment of “Foreign Threats to the 2020 US Federal Elections” and found “some successful compromises of state and local government networks prior to Election Day—as well as a higher volume of unsuccessful attempts”.... [...]
