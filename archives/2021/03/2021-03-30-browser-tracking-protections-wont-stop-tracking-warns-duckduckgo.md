Title: Browser tracking protections won't stop tracking, warns DuckDuckGo
Date: 2021-03-30T20:53:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: browser-tracking-protections-wont-stop-tracking-warns-duckduckgo

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/30/duckduckgo_tracking_protection_claims/){:target="_blank" rel="noopener"}

> Privacy don't like it, block the tracker, block the tracker Eliminating third-party cookies will not stop companies from tracking web users, says DuckDuckGo, which claims it can help with its desktop browser extensions and mobile apps.... [...]
