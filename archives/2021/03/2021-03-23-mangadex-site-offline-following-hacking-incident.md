Title: MangaDex Site Offline Following Hacking Incident
Date: 2021-03-23T19:50:40+00:00
Author: Tara Seals
Category: Threatpost
Tags: Bug Bounty;Vulnerabilities;Web Security
Slug: mangadex-site-offline-following-hacking-incident

[Source](https://threatpost.com/mangadex-site-offline-hacking/164983/){:target="_blank" rel="noopener"}

> A cyberattacker taunted the site about open security vulnerabilities, prompting a code review. [...]
