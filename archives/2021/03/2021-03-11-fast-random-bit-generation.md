Title: Fast Random Bit Generation
Date: 2021-03-11T12:15:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;Fortuna;random numbers
Slug: fast-random-bit-generation

[Source](https://www.schneier.com/blog/archives/2021/03/fast-random-bit-generation.html){:target="_blank" rel="noopener"}

> Science has a paper (and commentary ) on generating 250 random terabits per second with a laser. I don’t know how cryptographically secure they are, but that can be cleaned up with something like Fortuna. [...]
