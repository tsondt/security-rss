Title: Google shares Spectre PoC targeting browser JavaScript engines
Date: 2021-03-12T14:30:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-shares-spectre-poc-targeting-browser-javascript-engines

[Source](https://www.bleepingcomputer.com/news/security/google-shares-spectre-poc-targeting-browser-javascript-engines/){:target="_blank" rel="noopener"}

> Google has published JavaScript proof-of-concept (PoC) code to demonstrate the practicality of using Spectre exploits targeting web browsers to gain access to information from a browser's memory. [...]
