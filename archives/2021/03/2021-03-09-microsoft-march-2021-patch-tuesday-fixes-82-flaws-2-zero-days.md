Title: Microsoft March 2021 Patch Tuesday fixes 82 flaws, 2 zero-days
Date: 2021-03-09T13:30:08-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-march-2021-patch-tuesday-fixes-82-flaws-2-zero-days

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-march-2021-patch-tuesday-fixes-82-flaws-2-zero-days/){:target="_blank" rel="noopener"}

> Today is Microsoft's March 2021 Patch Tuesday, and with admins already struggling with Microsoft Exchange updates and hacked servers, please be nice to your IT staff today. [...]
