Title: Xerox legal threat reportedly silences researcher at Infiltrate security conference
Date: 2021-03-02T15:10:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: xerox-legal-threat-reportedly-silences-researcher-at-infiltrate-security-conference

[Source](https://portswigger.net/daily-swig/xerox-legal-threat-reportedly-silences-researcher-at-infiltrate-security-conference){:target="_blank" rel="noopener"}

> An infosec conference talk was allegedly canceled due to a ‘cease and desist’ demand [...]
