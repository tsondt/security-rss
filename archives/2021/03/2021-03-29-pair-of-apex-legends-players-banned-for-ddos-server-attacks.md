Title: Pair of Apex Legends Players Banned for DDoS Server Attacks
Date: 2021-03-29T21:07:56+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Web Security
Slug: pair-of-apex-legends-players-banned-for-ddos-server-attacks

[Source](https://threatpost.com/apex-legends-players-banned-ddos-server-attacks/165085/){:target="_blank" rel="noopener"}

> Predator-ranked players on Xbox console game version rigged matches with DDoS attacks. [...]
