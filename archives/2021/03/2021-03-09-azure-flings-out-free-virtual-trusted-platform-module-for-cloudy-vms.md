Title: Azure flings out free virtual trusted platform module for cloudy VMs
Date: 2021-03-09T05:58:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: azure-flings-out-free-virtual-trusted-platform-module-for-cloudy-vms

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/09/azure_vtpm_trusted_launch_preview/){:target="_blank" rel="noopener"}

> Take that, rootkits and other low-level nasties - if they take a crack at fresh VMs, on certain instance types under a handful of OSes Microsoft has revealed that its Azure IaaS platform now offers free a virtual trusted platform module.... [...]
