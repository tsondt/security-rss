Title: National Security Risks of Late-Stage Capitalism
Date: 2021-03-01T12:12:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: China;cybersecurity;essays;hacking;national security policy;Russia
Slug: national-security-risks-of-late-stage-capitalism

[Source](https://www.schneier.com/blog/archives/2021/03/national-security-risks-of-late-stage-capitalism.html){:target="_blank" rel="noopener"}

> Early in 2020, cyberspace attackers apparently working for the Russian government compromised a piece of widely used network management software made by a company called SolarWinds. The hack gave the attackers access to the computer networks of some 18,000 of SolarWinds’s customers, including US government agencies such as the Homeland Security Department and State Department, American nuclear research labs, government contractors, IT companies and nongovernmental agencies around the world. It was a huge attack, with major implications for US national security. The Senate Intelligence Committee is scheduled to hold a hearing on the breach on Tuesday. Who is at fault? [...]
