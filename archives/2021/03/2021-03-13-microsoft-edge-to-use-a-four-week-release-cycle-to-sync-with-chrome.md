Title: Microsoft Edge to use a four-week release cycle to sync with Chrome
Date: 2021-03-13T14:12:21-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: microsoft-edge-to-use-a-four-week-release-cycle-to-sync-with-chrome

[Source](https://www.bleepingcomputer.com/news/security/microsoft-edge-to-use-a-four-week-release-cycle-to-sync-with-chrome/){:target="_blank" rel="noopener"}

> Major 'Stable' versions of Microsoft Edge will now be released every four weeks to synchronize with the new four-week release cycle announced by Google Chrome. [...]
