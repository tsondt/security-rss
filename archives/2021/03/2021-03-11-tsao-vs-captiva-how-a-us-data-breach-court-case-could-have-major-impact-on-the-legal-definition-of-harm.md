Title: Tsao vs. Captiva – How a US data breach court case could have major impact on the legal definition of ‘harm’
Date: 2021-03-11T15:28:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: tsao-vs-captiva-how-a-us-data-breach-court-case-could-have-major-impact-on-the-legal-definition-of-harm

[Source](https://portswigger.net/daily-swig/tsao-vs-captiva-how-a-us-data-breach-court-case-could-have-major-impact-on-the-legal-definition-of-harm){:target="_blank" rel="noopener"}

> Successful data breach class action litigation may soon depend on the location where the lawsuit is filed [...]
