Title: FBI: Over $4.2 billion officially lost to cybercrime in 2020
Date: 2021-03-18T03:25:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: fbi-over-42-billion-officially-lost-to-cybercrime-in-2020

[Source](https://www.bleepingcomputer.com/news/security/fbi-over-42-billion-officially-lost-to-cybercrime-in-2020/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation has published its annual report on cybercrime affecting victims in the U.S., noting a record number of complaints and financial losses in 2020 compared to the previous year. [...]
