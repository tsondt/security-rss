Title: A Basic Timeline of the Exchange Mass-Hack
Date: 2021-03-08T16:05:32+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Chopper web shell;DEVCORE;Dubex;Orange Tsai;proxylogon.com;Volexity
Slug: a-basic-timeline-of-the-exchange-mass-hack

[Source](https://krebsonsecurity.com/2021/03/a-basic-timeline-of-the-exchange-mass-hack/){:target="_blank" rel="noopener"}

> Sometimes when a complex story takes us by surprise or knocks us back on our heels, it pays to revisit the events in a somewhat linear fashion. Here’s a brief timeline of what we know leading up to last week’s mass-hack, when hundreds of thousands of Microsoft Exchange Server systems got compromised and seeded with a powerful backdoor Trojan horse program. When did Microsoft find out about attacks on previously unknown vulnerabilities in Exchange? Pressed for a date when it first became aware of the problem, Microsoft told KrebsOnSecurity it was initially notified “in early January.” So far the earliest [...]
