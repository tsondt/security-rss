Title: Shorteners – new tool allows researchers, orgs to search for exposed shortened URLs
Date: 2021-03-15T12:10:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: shorteners-new-tool-allows-researchers-orgs-to-search-for-exposed-shortened-urls

[Source](https://portswigger.net/daily-swig/shorteners-new-tool-allows-researchers-orgs-to-search-for-exposed-shortened-urls){:target="_blank" rel="noopener"}

> Users can search or browse any links that have been shortened from a specific domain [...]
