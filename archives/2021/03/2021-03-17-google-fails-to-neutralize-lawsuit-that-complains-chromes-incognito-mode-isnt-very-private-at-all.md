Title: Google fails to neutralize lawsuit that complains Chrome's incognito mode isn't very private at all
Date: 2021-03-17T00:10:39+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: google-fails-to-neutralize-lawsuit-that-complains-chromes-incognito-mode-isnt-very-private-at-all

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/17/google_incognito_lawsuit/){:target="_blank" rel="noopener"}

> Judge Lucy Koh allows legal challenge to move forward Netizens who say Google continued to track them around the web even when using Chrome's incognito mode can proceed with their privacy lawsuit against the internet giant, a judge has ruled.... [...]
