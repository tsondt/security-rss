Title: REvil ransomware has a new ‘Windows Safe Mode’ encryption mode
Date: 2021-03-19T07:15:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: revil-ransomware-has-a-new-windows-safe-mode-encryption-mode

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-has-a-new-windows-safe-mode-encryption-mode/){:target="_blank" rel="noopener"}

> The REvil ransomware operation has added a new ability to encrypt files in Windows Safe Mode, likely to evade detection by security software and for greater success when encrypting files. [...]
