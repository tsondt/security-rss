Title: Gab, a haven for pro-Trump conspiracy theories, has been hacked again
Date: 2021-03-09T04:53:40+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;gab;hack;website
Slug: gab-a-haven-for-pro-trump-conspiracy-theories-has-been-hacked-again

[Source](https://arstechnica.com/?p=1748174){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson) Beleaguered social networking site Gab was breached on Monday, marking the second time in as many weeks that hackers have gained unauthorized access to a platform that caters to users pushing hate speech and pro-Trump conspiracy theories. The compromise came to light after someone hijacked the account of Gab founder and CEO Andrew Torba and left a post criticizing him for not paying an 8 bitcoin ransom for the safe return of documents used to verify the identity of some users. The unknown hacker also accused Torba of failing to disclose the full extent of the [...]
