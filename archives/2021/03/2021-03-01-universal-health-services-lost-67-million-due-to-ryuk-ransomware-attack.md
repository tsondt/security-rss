Title: Universal Health Services lost $67 million due to Ryuk ransomware attack
Date: 2021-03-01T12:34:17-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: universal-health-services-lost-67-million-due-to-ryuk-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/universal-health-services-lost-67-million-due-to-ryuk-ransomware-attack/){:target="_blank" rel="noopener"}

> Universal Health Services (UHS) said that the Ryuk ransomware attack it suffered during September 2020 had an estimated impact of $67 million. [...]
