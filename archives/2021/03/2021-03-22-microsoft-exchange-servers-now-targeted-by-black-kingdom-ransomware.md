Title: Microsoft Exchange servers now targeted by Black Kingdom ransomware
Date: 2021-03-22T09:07:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: microsoft-exchange-servers-now-targeted-by-black-kingdom-ransomware

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-servers-now-targeted-by-black-kingdom-ransomware/){:target="_blank" rel="noopener"}

> Another ransomware operation known as 'Black Kingdom' is exploiting the Microsoft Exchange Server ProxyLogon vulnerabilities to encrypt servers. [...]
