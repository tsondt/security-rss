Title: You’ve learned a lot over the past year – and so have the cyber-criminals hiding in your systems
Date: 2021-03-24T23:00:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: youve-learned-a-lot-over-the-past-year-and-so-have-the-cyber-criminals-hiding-in-your-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/24/youve_learned_a_lot_over/){:target="_blank" rel="noopener"}

> They’ve switched tactics. With Malwarebytes, we'll show how you can, too Webcast Much of the tech world patted itself on the back last year, as organisations switched virtually overnight to remote working and online collaboration.... [...]
