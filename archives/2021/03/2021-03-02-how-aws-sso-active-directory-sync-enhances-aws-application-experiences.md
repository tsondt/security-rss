Title: How AWS SSO Active Directory sync enhances AWS application experiences
Date: 2021-03-02T20:32:17+00:00
Author: Sharanya Ramakrishnan
Category: AWS Security
Tags: AWS Single Sign-On (SSO);Foundational (100);Security, Identity, & Compliance;Active Directory;AWS SSO;Identity;Security Blog
Slug: how-aws-sso-active-directory-sync-enhances-aws-application-experiences

[Source](https://aws.amazon.com/blogs/security/how-aws-sso-active-directory-sync-enhances-aws-application-experiences/){:target="_blank" rel="noopener"}

> Identity management is easiest when you can manage identities in a centralized location and use these identities across various accounts and applications. You also want to be able to use these identities for other purposes within applications, like searching through groups, finding members of a certain group, and sharing projects with other users or groups. [...]
