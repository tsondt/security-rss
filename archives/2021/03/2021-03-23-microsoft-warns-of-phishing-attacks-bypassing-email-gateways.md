Title: Microsoft warns of phishing attacks bypassing email gateways
Date: 2021-03-23T13:40:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-warns-of-phishing-attacks-bypassing-email-gateways

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-phishing-attacks-bypassing-email-gateways/){:target="_blank" rel="noopener"}

> An ongoing phishing operation that stole an estimated 400,000 OWA and Office 365 credentials since December has now expanded to abuse new legitimate services to bypass secure email gateways (SEGs). [...]
