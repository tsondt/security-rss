Title: Microsoft Exchange attacks increase while WannaCry gets a restart
Date: 2021-03-30T07:56:19-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: microsoft-exchange-attacks-increase-while-wannacry-gets-a-restart

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-attacks-increase-while-wannacry-gets-a-restart/){:target="_blank" rel="noopener"}

> The recently patched vulnerabilities in Microsoft Exchange have sparked new interest among cybercriminals, who increased the volume of attacks focusing on this particular vector. [...]
