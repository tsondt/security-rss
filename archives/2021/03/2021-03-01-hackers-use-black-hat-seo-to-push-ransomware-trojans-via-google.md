Title: Hackers use black hat SEO to push ransomware, trojans via Google
Date: 2021-03-01T13:10:49-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hackers-use-black-hat-seo-to-push-ransomware-trojans-via-google

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-black-hat-seo-to-push-ransomware-trojans-via-google/){:target="_blank" rel="noopener"}

> The delivery system for the Gootkit information stealer has evolved into a complex and stealthy framework, which earned it the name Gootloader, and is now pushing a wider variety of malware via hacked WordPress sites and malicious SEO techniques for Google results. [...]
