Title: Smart sex toys come with Bluetooth and remote access weaknesses
Date: 2021-03-11T13:45:19-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: smart-sex-toys-come-with-bluetooth-and-remote-access-weaknesses

[Source](https://www.bleepingcomputer.com/news/security/smart-sex-toys-come-with-bluetooth-and-remote-access-weaknesses/){:target="_blank" rel="noopener"}

> Today, researchers have exposed common weaknesses lurking in the latest smart sex toys that can be exploited by attackers. As more as more adult toy brands enter the market, and COVID-19 situation has led to a rapid increase in sex toy sales, researchers believe a discussion around the security of these devices is vital. [...]
