Title: The Week in Ransomware - March 19th 2021 - Highest ransom ever!
Date: 2021-03-19T17:40:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-march-19th-2021-highest-ransom-ever

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-19th-2021-highest-ransom-ever/){:target="_blank" rel="noopener"}

> While the beginning of this week was fairly quiet, it definitely ended with a bang as news came out of the largest ransom demand yet. [...]
