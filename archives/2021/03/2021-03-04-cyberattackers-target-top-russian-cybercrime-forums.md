Title: Cyberattackers Target Top Russian Cybercrime Forums
Date: 2021-03-04T21:42:51+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Web Security
Slug: cyberattackers-target-top-russian-cybercrime-forums

[Source](https://threatpost.com/cyberattackers-target-russian-cybercrime-forums/164511/){:target="_blank" rel="noopener"}

> Elite Russian forums for cybercriminals have been hacked in a string of breaches, leaving hackers edgy and worried about law enforcement. [...]
