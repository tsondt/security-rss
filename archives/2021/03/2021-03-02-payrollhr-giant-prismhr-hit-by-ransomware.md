Title: Payroll/HR Giant PrismHR Hit by Ransomware?
Date: 2021-03-02T19:36:21+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;Decimal;Jacob Cloran;PEOs;PrismHR;professional employer organizations
Slug: payrollhr-giant-prismhr-hit-by-ransomware

[Source](https://krebsonsecurity.com/2021/03/payroll-hr-giant-prismhr-hit-by-ransomware/){:target="_blank" rel="noopener"}

> PrismHR, a company that sells technology used by other firms to help more than 80,000 small businesses manage payroll, benefits, and human resources, has suffered what appears to be an ongoing ransomware attack that is disrupting many of its services. Hopkinton, Mass.-based PrismHR handles everything from payroll processing and human resources to health insurance and tax forms for hundreds of “professional employer organizations” (PEOs) that serve more than two million employees. The company processes more than $80 billion payroll payments annually on behalf of PEOs and their clients. Countless small businesses turn to PEOs in part because they simplify compliance [...]
