Title: European Banking Authority discloses Exchange server hack
Date: 2021-03-08T11:05:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: european-banking-authority-discloses-exchange-server-hack

[Source](https://www.bleepingcomputer.com/news/security/european-banking-authority-discloses-exchange-server-hack/){:target="_blank" rel="noopener"}

> The European Banking Authority (EBA) took down all email systems after their Microsoft Exchange Servers were hacked as part of the ongoing attacks targeting organizations worldwide. [...]
