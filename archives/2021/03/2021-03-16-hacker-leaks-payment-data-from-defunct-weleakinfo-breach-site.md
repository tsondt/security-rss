Title: Hacker leaks payment data from defunct WeLeakInfo breach site
Date: 2021-03-16T15:03:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hacker-leaks-payment-data-from-defunct-weleakinfo-breach-site

[Source](https://www.bleepingcomputer.com/news/security/hacker-leaks-payment-data-from-defunct-weleakinfo-breach-site/){:target="_blank" rel="noopener"}

> WeLeakInfo was a website that offered paid subscriptions that provides searchable access to a database containing 12.5 billion user records stolen during data breaches. This data included email addresses, names, phone numbers, addresses, and in many cases, passwords. [...]
