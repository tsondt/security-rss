Title: Beware the IDEs of March: Microsoft's latest monthly fixes land after frantic Exchange Server updates
Date: 2021-03-09T22:09:18+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: beware-the-ides-of-march-microsofts-latest-monthly-fixes-land-after-frantic-exchange-server-updates

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/09/march_patch_tuesday/){:target="_blank" rel="noopener"}

> Bugs in Visual Studio, Visual Studio Code are the least of it Patch Tuesday A week after Microsoft warned that four zero-day flaws and three others in its Exchange Server were being actively exploited and issued out-of-band remediation, the cloudy Windows biz has delivered software fixes to address 82 other vulnerabilities as part of its monthly Patch Tuesday ritual.... [...]
