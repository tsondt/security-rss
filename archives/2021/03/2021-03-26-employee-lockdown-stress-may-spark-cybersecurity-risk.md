Title: Employee Lockdown Stress May Spark Cybersecurity Risk
Date: 2021-03-26T19:09:55+00:00
Author: Tara Seals
Category: Threatpost
Tags: Most Recent ThreatLists;Vulnerabilities;Web Security
Slug: employee-lockdown-stress-may-spark-cybersecurity-risk

[Source](https://threatpost.com/employee-lockdown-stress-cybersecurity-risk/165050/){:target="_blank" rel="noopener"}

> Younger employees and caregivers report more stress than other groups-- and more shadow IT usage. [...]
