Title: Hackers share methods to bypass 3D Secure  for payment cards
Date: 2021-03-03T15:01:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hackers-share-methods-to-bypass-3d-secure-for-payment-cards

[Source](https://www.bleepingcomputer.com/news/security/hackers-share-methods-to-bypass-3d-secure-for-payment-cards/){:target="_blank" rel="noopener"}

> Cybercriminals are constantly exploring and documenting new ways to go around the 3D Secure (3DS) protocol used for authorizing online card transactions. [...]
