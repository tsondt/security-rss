Title: More on the Chinese Zero-Day Microsoft Exchange Hack
Date: 2021-03-10T12:28:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: China;cybersecurity;hacking;Microsoft;patching;zero-day
Slug: more-on-the-chinese-zero-day-microsoft-exchange-hack

[Source](https://www.schneier.com/blog/archives/2021/03/more-on-the-chinese-zero-day-microsoft-exchange-hack.html){:target="_blank" rel="noopener"}

> Nick Weaver has an excellent post on the Microsoft Exchange hack: The investigative journalist Brian Krebs has produced a handy timeline of events and a few things stand out from the chronology. The attacker was first detected by one group on Jan. 5 and another on Jan. 6, and Microsoft acknowledged the problem immediately. During this time the attacker appeared to be relatively subtle, exploiting particular targets (although we generally lack insight into who was targeted). Microsoft determined on Feb. 18 that it would patch these vulnerabilities on the March 9th “Patch Tuesday” release of fixes. Somehow, the threat actor [...]
