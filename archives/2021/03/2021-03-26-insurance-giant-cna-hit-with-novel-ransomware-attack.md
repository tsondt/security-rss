Title: Insurance Giant CNA Hit with Novel Ransomware Attack
Date: 2021-03-26T16:06:25+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: insurance-giant-cna-hit-with-novel-ransomware-attack

[Source](https://threatpost.com/cna-hit-novel-ransomware/165044/){:target="_blank" rel="noopener"}

> The incident, which forced the company to disconnect its systems, caused significant business disruption. [...]
