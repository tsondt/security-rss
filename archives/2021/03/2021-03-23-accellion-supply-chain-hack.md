Title: Accellion Supply Chain Hack
Date: 2021-03-23T11:32:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: hacking;patching;supply chain;vulnerabilities
Slug: accellion-supply-chain-hack

[Source](https://www.schneier.com/blog/archives/2021/03/accellion-supply-chain-hack.html){:target="_blank" rel="noopener"}

> A vulnerability in the Accellion file-transfer program is being used by criminal groups to hack networks worldwide. There’s much in the article about when Accellion knew about the vulnerability, when it alerted its customers, and when it patched its software. The governor of New Zealand’s central bank, Adrian Orr, says Accellion failed to warn it after first learning in mid-December that the nearly 20-year-old FTA application — using antiquated technology and set for retirement — had been breached. Despite having a patch available on Dec. 20, Accellion did not notify the bank in time to prevent its appliance from being [...]
