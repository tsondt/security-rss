Title: New PoC for Microsoft Exchange bugs puts attacks in reach of anyone
Date: 2021-03-14T15:42:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: new-poc-for-microsoft-exchange-bugs-puts-attacks-in-reach-of-anyone

[Source](https://www.bleepingcomputer.com/news/security/new-poc-for-microsoft-exchange-bugs-puts-attacks-in-reach-of-anyone/){:target="_blank" rel="noopener"}

> A security researcher has released a new proof-of-concept exploit this weekend that requires slight modification to install web shells on Microsoft Exchange servers vulnerable to the actively exploited ProxyLogon vulnerabilities. [...]
