Title: FBI exposes weakness in Mamba ransomware, DiskCryptor
Date: 2021-03-26T03:30:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: fbi-exposes-weakness-in-mamba-ransomware-diskcryptor

[Source](https://www.bleepingcomputer.com/news/security/fbi-exposes-weakness-in-mamba-ransomware-diskcryptor/){:target="_blank" rel="noopener"}

> An alert from the U.S. Federal Bureau of Investigation about Mamba ransomware reveals a weak spot in the encryption process that could help targeted organizations recover from the attack without paying the ransom. [...]
