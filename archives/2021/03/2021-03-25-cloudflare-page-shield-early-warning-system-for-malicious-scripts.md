Title: Cloudflare Page Shield: Early warning system for malicious scripts
Date: 2021-03-25T18:26:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: cloudflare-page-shield-early-warning-system-for-malicious-scripts

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-page-shield-early-warning-system-for-malicious-scripts/){:target="_blank" rel="noopener"}

> Cloudflare has released a new feature that aims to protect websites from Magecart and other malicious JavaScript-based attacks. [...]
