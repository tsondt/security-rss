Title: Tether cryptocurrency firm says docs in $24 million ransom are 'forged'
Date: 2021-03-01T10:14:20-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency
Slug: tether-cryptocurrency-firm-says-docs-in-24-million-ransom-are-forged

[Source](https://www.bleepingcomputer.com/news/security/tether-cryptocurrency-firm-says-docs-in-24-million-ransom-are-forged/){:target="_blank" rel="noopener"}

> USDT cryptocurrency developer Tether has said they are being extorted by threat actors who are demanding 500 bitcoins, or approximately $24 million, not to leak allegedly stolen emails and documents. [...]
