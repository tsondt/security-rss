Title: Adobe fixes critical Creative Cloud, Adobe Connect vulnerabilities
Date: 2021-03-09T11:27:27-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: adobe-fixes-critical-creative-cloud-adobe-connect-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/adobe-fixes-critical-creative-cloud-adobe-connect-vulnerabilities/){:target="_blank" rel="noopener"}

> Adobe has released security updates that fix vulnerabilities in Adobe Creative Cloud Desktop, Framemaker, and Connect. [...]
