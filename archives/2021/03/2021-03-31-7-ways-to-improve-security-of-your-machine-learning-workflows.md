Title: 7 ways to improve security of your machine learning workflows
Date: 2021-03-31T19:59:58+00:00
Author: Annalyn Ng
Category: AWS Security
Tags: Amazon SageMaker;Artificial Intelligence;Best Practices;Intermediate (200);Security, Identity, & Compliance;Data science;Devops;DevSecOps;Machine learning;SageMaker;Security Blog
Slug: 7-ways-to-improve-security-of-your-machine-learning-workflows

[Source](https://aws.amazon.com/blogs/security/7-ways-to-improve-security-of-your-machine-learning-workflows/){:target="_blank" rel="noopener"}

> In this post, you will learn how to use familiar security controls to build more secure machine learning (ML) workflows. The ideal audience for this post includes data scientists who want to learn basic ways to improve security of their ML workflows, as well as security engineers who want to address threats specific to an [...]
