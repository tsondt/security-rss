Title: GRUB2 boot loader reveals multiple high severity vulnerabilities
Date: 2021-03-03T14:37:40-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Linux
Slug: grub2-boot-loader-reveals-multiple-high-severity-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/grub2-boot-loader-reveals-multiple-high-severity-vulnerabilities/){:target="_blank" rel="noopener"}

> GRUB, a popular Linux boot loader project has fixed multiple high severity vulnerabilities. [...]
