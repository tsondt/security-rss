Title: Critical netmask networking bug impacts thousands of applications
Date: 2021-03-28T16:20:04-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: critical-netmask-networking-bug-impacts-thousands-of-applications

[Source](https://www.bleepingcomputer.com/news/security/critical-netmask-networking-bug-impacts-thousands-of-applications/){:target="_blank" rel="noopener"}

> Popular npm component netmask has a critical networking vulnerability, CVE-2021-28918. netmask is frequently used by hundreds of thousands of applications to parse IPv4 addresses and CIDR blocks or compare them. The component gets over 3 million weekly downloads, and as of today, has scored over 238 million total downloads. [...]
