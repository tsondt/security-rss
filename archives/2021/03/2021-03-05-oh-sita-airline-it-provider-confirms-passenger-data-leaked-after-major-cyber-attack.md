Title: Oh SITA: Airline IT provider confirms passenger data leaked after major 'cyber-attack'
Date: 2021-03-05T16:01:35+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: oh-sita-airline-it-provider-confirms-passenger-data-leaked-after-major-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/05/oh_sita_airline_it_provider/){:target="_blank" rel="noopener"}

> Data from multiple aviation giants hit Not that many planes are taking off these days, but that didn’t stop the flight of passenger records from servers belonging to aviation tech supplier SITA after it was hit by a "cyberattack".... [...]
