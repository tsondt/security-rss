Title: European Banking Authority restores email service in wake of Microsoft Exchange hack
Date: 2021-03-09T13:58:26+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: european-banking-authority-restores-email-service-in-wake-of-microsoft-exchange-hack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/09/eba_exchange_breach/){:target="_blank" rel="noopener"}

> Servers breached but no data 'compromised', claims org The European Banking Authority (EBA) has confirmed it is another victim on the list of organisations affected by vulnerabilities in Microsoft Exchange.... [...]
