Title: Like a challenge in a high profile 'face-of-IT' role? Welcome to the Home Office
Date: 2021-03-04T13:20:36+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: like-a-challenge-in-a-high-profile-face-of-it-role-welcome-to-the-home-office

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/04/home_office_it_job/){:target="_blank" rel="noopener"}

> Talk about a hostile environment... It's £70k+ but you'll really sweat for it For the brave – or perhaps foolhardy – senior IT jobs hunters there is an opening at the UK Home Office for a Deputy Director of IT Operations in a lovely '70s era highrise in Croydon.... [...]
