Title: Highlights from the latest AWS Identity launches
Date: 2021-03-18T16:40:22+00:00
Author: Michael Chan
Category: AWS Security
Tags: Amazon Cognito;AWS Directory Service;AWS Identity and Access Management (IAM);AWS Organizations;AWS Single Sign-On (SSO);Foundational (100);Identity;Resource Access Manager (RAM);Security, Identity, & Compliance;AWS IAM;AWS RAM;AWS SSO;Security Blog
Slug: highlights-from-the-latest-aws-identity-launches

[Source](https://aws.amazon.com/blogs/security/highlights-from-the-latest-aws-identity-launches/){:target="_blank" rel="noopener"}

> Here is the latest from AWS Identity from November 2020 through February 2021. The features highlighted in this blog post can help you manage and secure your Amazon Web Services (AWS) environment. Identity services answer the question of who has access to what. They enable you to securely manage identities, resources, and permissions at scale and [...]
