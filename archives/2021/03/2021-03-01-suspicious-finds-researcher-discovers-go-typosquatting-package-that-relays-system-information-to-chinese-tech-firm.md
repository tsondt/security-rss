Title: Suspicious finds: Researcher discovers Go typosquatting package that relays system information to Chinese tech firm
Date: 2021-03-01T15:39:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: suspicious-finds-researcher-discovers-go-typosquatting-package-that-relays-system-information-to-chinese-tech-firm

[Source](https://portswigger.net/daily-swig/suspicious-finds-researcher-discovers-go-typosquatting-package-that-relays-system-information-to-chinese-tech-firm){:target="_blank" rel="noopener"}

> One of several dubious repositories flagged by custom-built tool [...]
