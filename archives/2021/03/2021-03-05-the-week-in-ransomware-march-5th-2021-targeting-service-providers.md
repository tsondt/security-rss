Title: The Week in Ransomware - March 5th 2021 - Targeting service providers
Date: 2021-03-05T18:53:22-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-march-5th-2021-targeting-service-providers

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-5th-2021-targeting-service-providers/){:target="_blank" rel="noopener"}

> This week we have seen ransomware attacks targeting online service providers and MSPs to not only encrypt the victim but also cause significant outages for their customers. [...]
