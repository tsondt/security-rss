Title: Cyber-attackers work 24/7 … but what about your security team?
Date: 2021-03-01T07:30:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: cyber-attackers-work-247-but-what-about-your-security-team

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/01/cyber_attackers_work_24x7_but/){:target="_blank" rel="noopener"}

> If you want the lowdown on managed detection and response, clock this Promo One thing you can say about cyber-attackers. They don’t keep office hours. They – or their code – will chip away at your systems, all day, every day, looking for a way in before quietly exploiting it for as long as possible.... [...]
