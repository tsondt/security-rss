Title: Microsoft Exchange Servers Face APT Attack Tsunami
Date: 2021-03-11T18:01:16+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Government;Hacks;Malware;Vulnerabilities;Web Security
Slug: microsoft-exchange-servers-face-apt-attack-tsunami

[Source](https://threatpost.com/microsoft-exchange-servers-apt-attack/164695/){:target="_blank" rel="noopener"}

> At least 10 nation-state-backed groups are using the ProxyLogon exploit chain to compromise email servers, as compromises mount. [...]
