Title: TLS 1.2 will be required for all AWS FIPS endpoints beginning March 31, 2021
Date: 2021-03-01T18:04:09+00:00
Author: Janelle Hopper
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;AWS service FIPS endpoints;Federal Information Processing Standard;FedRAMP;FIPS;FIPS 140-2;FIPS endpoints;NIST;Security Blog;TLS;Transport Layer Security
Slug: tls-12-will-be-required-for-all-aws-fips-endpoints-beginning-march-31-2021

[Source](https://aws.amazon.com/blogs/security/tls-1-2-required-for-aws-fips-endpoints/){:target="_blank" rel="noopener"}

> To help you meet your compliance needs, we’re updating all AWS Federal Information Processing Standard (FIPS) endpoints to a minimum of Transport Layer Security (TLS) 1.2. We have already updated over 40 services to require TLS 1.2, removing support for TLS 1.0 and TLS 1.1. Beginning March 31, 2021, if your client application cannot support [...]
