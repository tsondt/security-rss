Title: Friday Squid Blogging: Squid Potato Masher
Date: 2021-03-26T21:04:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-squid-potato-masher

[Source](https://www.schneier.com/blog/archives/2021/03/friday-squid-blogging-squid-potato-masher.html){:target="_blank" rel="noopener"}

> A squid potato masher for only $11.50. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
