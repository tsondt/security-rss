Title: US newspaper's 'Biden will hack Russia' claim: A good way to reassure Putin you'll leave him alone
Date: 2021-03-09T19:02:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: us-newspapers-biden-will-hack-russia-claim-a-good-way-to-reassure-putin-youll-leave-him-alone

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/09/us_wont_hack_russia/){:target="_blank" rel="noopener"}

> Titbit for domestic consumption looks darn silly from abroad Opinion The US government might have subtly signalled that it likely won't hack Russia this month – by telling credulous journalists it has a "clandestine" plan to, er, launch an attack against its rival before April.... [...]
