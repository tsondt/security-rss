Title: CISA gives federal agencies 5 days to find hacked Exchange servers
Date: 2021-03-31T14:55:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-gives-federal-agencies-5-days-to-find-hacked-exchange-servers

[Source](https://www.bleepingcomputer.com/news/security/cisa-gives-federal-agencies-5-days-to-find-hacked-exchange-servers/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has ordered federal agencies to scan their networks again for any signs of compromised on-premises Microsoft Exchange servers and report their findings within five days. [...]
