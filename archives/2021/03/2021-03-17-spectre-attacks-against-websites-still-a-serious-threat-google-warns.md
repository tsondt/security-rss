Title: Spectre attacks against websites still a serious threat, Google warns
Date: 2021-03-17T12:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: spectre-attacks-against-websites-still-a-serious-threat-google-warns

[Source](https://portswigger.net/daily-swig/spectre-attacks-against-websites-still-a-serious-threat-google-warns){:target="_blank" rel="noopener"}

> Browser-maker urges web developers to take action against vulnerability that continues to haunt the industry [...]
