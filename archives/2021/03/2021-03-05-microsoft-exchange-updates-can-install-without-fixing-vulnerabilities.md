Title: Microsoft: Exchange updates can install without fixing vulnerabilities
Date: 2021-03-05T10:12:40-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-exchange-updates-can-install-without-fixing-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-updates-can-install-without-fixing-vulnerabilities/){:target="_blank" rel="noopener"}

> Due to the critical nature of recently issued Microsoft Exchange security updates, admins need to know that the updates may have installation issues on servers where User Account Control (UAC) is enabled. [...]
