Title: Hacking Digitally Signed PDF Files
Date: 2021-03-08T12:10:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;Adobe;hacking;signatures
Slug: hacking-digitally-signed-pdf-files

[Source](https://www.schneier.com/blog/archives/2021/03/hacking-digitally-signed-pdf-files.html){:target="_blank" rel="noopener"}

> Interesting paper: “ Shadow Attacks: Hiding and Replacing Content in Signed PDFs “: Abstract: Digitally signed PDFs are used in contracts and invoices to guarantee the authenticity and integrity of their content. A user opening a signed PDF expects to see a warning in case of any modification. In 2019, Mladenov et al. revealed various parsing vulnerabilities in PDF viewer implementations.They showed attacks that could modify PDF documents without invalidating the signature. As a consequence, affected vendors of PDF viewers implemented countermeasures preventing all attacks. This paper introduces a novel class of attacks, which we call shadow attacks. The shadow [...]
