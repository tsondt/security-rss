Title: Microsoft's MSERT tool now finds web shells from Exchange Server attacks
Date: 2021-03-07T16:28:08-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsofts-msert-tool-now-finds-web-shells-from-exchange-server-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsofts-msert-tool-now-finds-web-shells-from-exchange-server-attacks/){:target="_blank" rel="noopener"}

> Microsoft has pushed out a new update for their Microsoft Safety Scanner (MSERT) tool to detect web shells deployed in the recent Exchange Server attacks. [...]
