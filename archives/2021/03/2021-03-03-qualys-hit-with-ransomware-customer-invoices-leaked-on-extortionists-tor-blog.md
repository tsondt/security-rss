Title: Qualys hit with ransomware: Customer invoices leaked on extortionists' Tor blog
Date: 2021-03-03T17:00:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: qualys-hit-with-ransomware-customer-invoices-leaked-on-extortionists-tor-blog

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/03/qualys_ransomware_clop_gang/){:target="_blank" rel="noopener"}

> Ace infosec biz aware and investigating, we're told Infosec outfit Qualys, its cloud-based vuln detection tech, and its SSL server test webpage, have seemingly fallen victim to a ransomware attack.... [...]
