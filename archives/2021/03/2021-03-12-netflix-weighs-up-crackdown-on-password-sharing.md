Title: Netflix weighs up crackdown on password sharing
Date: 2021-03-12T10:04:28+00:00
Author: Mark Sweney
Category: The Guardian
Tags: Netflix;Business;Media;TV streaming;UK news;US news;Television & radio;Data and computer security;Internet safety;Culture;Technology;World news
Slug: netflix-weighs-up-crackdown-on-password-sharing

[Source](https://www.theguardian.com/media/2021/mar/12/netflix-weighs-up-crackdown-on-password-sharing){:target="_blank" rel="noopener"}

> Streaming service tests feature that asks viewers if they share household with subscriber Netflix has begun testing a feature that asks viewers whether they share a household with a subscriber, in a move that could lead to crackdown on the widespread practice of sharing passwords among friends and family. Some Netflix users are reported to have received a message asking them to confirm they live with the account owner by entering a code included in a text message or email sent to the subscriber. Continue reading... [...]
