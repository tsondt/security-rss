Title: Notorious Maza cybercrime forum attacked by other hackers
Date: 2021-03-04T13:34:15-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: notorious-maza-cybercrime-forum-attacked-by-other-hackers

[Source](https://www.bleepingcomputer.com/news/security/notorious-maza-cybercrime-forum-attacked-by-other-hackers/){:target="_blank" rel="noopener"}

> The Maza cybercrime forum was hacked and member data leaked in the latest of a series of attacks targeting mostly Russian-speaking hacker forums. [...]
