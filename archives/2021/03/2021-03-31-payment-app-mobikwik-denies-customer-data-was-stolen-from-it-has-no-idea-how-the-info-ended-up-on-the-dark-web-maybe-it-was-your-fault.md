Title: Payment app MobiKwik denies customer data was stolen from it, has no idea how the info ended up on the dark web: Maybe it was your fault?
Date: 2021-03-31T02:56:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: payment-app-mobikwik-denies-customer-data-was-stolen-from-it-has-no-idea-how-the-info-ended-up-on-the-dark-web-maybe-it-was-your-fault

[Source](https://go.theregister.com/feed/www.theregister.com/2021/03/31/mobikwik_data_breach_denial/){:target="_blank" rel="noopener"}

> Talk about living in hope Indian payment app maker MobiKwik has denied its security has been breached, saying that if it's true, as has been claimed, that its customers' information has appeared on the dark web, then some other platform was totally responsible for that.... [...]
