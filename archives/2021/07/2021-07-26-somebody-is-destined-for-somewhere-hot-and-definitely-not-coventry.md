Title: Somebody is destined for somewhere hot, and definitely not Coventry
Date: 2021-07-26T07:30:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: somebody-is-destined-for-somewhere-hot-and-definitely-not-coventry

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/26/who_me/){:target="_blank" rel="noopener"}

> Praise be for Firewalls Who, Me? Welcome to Who, Me?, where hallowed ground gets trampled as a reader inadvertently cleans up the collective act of the senior staff.... [...]
