Title: So nice of China to put all of its network zero-day vulns in one giant database no one will think to break into
Date: 2021-07-15T01:07:40+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: so-nice-of-china-to-put-all-of-its-network-zero-day-vulns-in-one-giant-database-no-one-will-think-to-break-into

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/15/china_vulnerability_law/){:target="_blank" rel="noopener"}

> We sum up Middle Kingdom's massive crackdown on bug reports Chinese makers of network software and hardware must alert Beijing within two days of learning of a security vulnerability in their products under rules coming into force in China this year.... [...]
