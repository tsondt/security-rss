Title: Implement tenant isolation for Amazon S3 and Aurora PostgreSQL by using ABAC
Date: 2021-07-13T17:17:16+00:00
Author: Ashutosh Upadhyay
Category: AWS Security
Tags: Amazon Aurora;Amazon Simple Storage Services (S3);Intermediate (200);Security, Identity, & Compliance;ABAC;Amazon S3;Attribute-based access control;AWS IAM;Identity and Access Management;Multitenancy;Multitenant architecture;PostgreSQL;SaaS;Security Blog;Tenant isolation
Slug: implement-tenant-isolation-for-amazon-s3-and-aurora-postgresql-by-using-abac

[Source](https://aws.amazon.com/blogs/security/implement-tenant-isolation-for-amazon-s3-and-aurora-postgresql-by-using-abac/){:target="_blank" rel="noopener"}

> In software as a service (SaaS) systems, which are designed to be used by multiple customers, isolating tenant data is a fundamental responsibility for SaaS providers. The practice of isolation of data in a multi-tenant application platform is called tenant isolation. In this post, we describe an approach you can use to achieve tenant isolation [...]
