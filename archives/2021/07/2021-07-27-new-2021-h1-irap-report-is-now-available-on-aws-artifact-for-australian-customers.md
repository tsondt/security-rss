Title: New 2021 H1 IRAP report is now available on AWS Artifact for Australian customers
Date: 2021-07-27T00:58:17+00:00
Author: Clara Lim
Category: AWS Security
Tags: Announcements;AWS Artifact;Foundational (100);Security, Identity, & Compliance;IRAP;Security Blog
Slug: new-2021-h1-irap-report-is-now-available-on-aws-artifact-for-australian-customers

[Source](https://aws.amazon.com/blogs/security/new-2021-h1-irap-report-is-now-available-on-aws-artifact-for-australian-customers/){:target="_blank" rel="noopener"}

> We are excited to announce that an additional 15 AWS services are now assessed to be in scope for Information Security Registered Assessors Program (IRAP) after a successful incremental audit completed in June 2021 by independent ASD (Australian Signals Directorate) certified IRAP assessor. This brings the total to 112 services assessed at IRAP PROTECTED level. [...]
