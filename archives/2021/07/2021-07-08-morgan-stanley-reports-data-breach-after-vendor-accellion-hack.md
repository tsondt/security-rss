Title: Morgan Stanley reports data breach after vendor Accellion hack
Date: 2021-07-08T09:19:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: morgan-stanley-reports-data-breach-after-vendor-accellion-hack

[Source](https://www.bleepingcomputer.com/news/security/morgan-stanley-reports-data-breach-after-vendor-accellion-hack/){:target="_blank" rel="noopener"}

> Investment banking firm Morgan Stanley has reported a data breach after attackers stole personal information belonging to its customers by hacking into the Accellion FTA server of a third party vendor. [...]
