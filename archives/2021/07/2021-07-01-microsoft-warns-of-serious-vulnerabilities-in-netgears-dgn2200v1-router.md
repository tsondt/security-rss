Title: Microsoft warns of serious vulnerabilities in Netgear's DGN2200v1 router
Date: 2021-07-01T17:45:13+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: microsoft-warns-of-serious-vulnerabilities-in-netgears-dgn2200v1-router

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/01/microsoft_netgear_security_advisory_dgn2200v1/){:target="_blank" rel="noopener"}

> Gadget capable of 'opening the gates for attackers to roam untethered through an entire organisation' Netgear has patched serious security vulnerabilities in its DGN2200v1 network router, following the discovery of "very odd behaviour" by a Microsoft security research team - a somewhat understated way of saying that attackers can gain "complete control over the router."... [...]
