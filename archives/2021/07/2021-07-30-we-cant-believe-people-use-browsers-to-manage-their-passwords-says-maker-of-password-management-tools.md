Title: We can't believe people use browsers to manage their passwords, says maker of password management tools
Date: 2021-07-30T06:27:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: we-cant-believe-people-use-browsers-to-manage-their-passwords-says-maker-of-password-management-tools

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/30/infosec_risky_behaviours_study/){:target="_blank" rel="noopener"}

> You just save it in Chrome or Firefox? Ugh. And then it autofills when you need it again? Oh the horror It seems some of us are, in the year of our lord 2021, still reusing the same password for multiple sites, plugging personal gear into work networks, and perhaps overly relying on browser-managed passwords, judging from this poll.... [...]
