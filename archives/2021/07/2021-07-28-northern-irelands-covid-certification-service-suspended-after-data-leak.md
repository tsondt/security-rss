Title: Northern Ireland's COVID certification service suspended after data leak
Date: 2021-07-28T06:50:10-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: northern-irelands-covid-certification-service-suspended-after-data-leak

[Source](https://www.bleepingcomputer.com/news/security/northern-irelands-covid-certification-service-suspended-after-data-leak/){:target="_blank" rel="noopener"}

> Northern Ireland's Department of Health (DoH) has temporarily halted its COVID-19 vaccine certification web service and mobile apps following a data exposure incident. [...]
