Title: LockBit ransomware automates Windows domain encryption via group policies
Date: 2021-07-27T17:10:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: lockbit-ransomware-automates-windows-domain-encryption-via-group-policies

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-automates-windows-domain-encryption-via-group-policies/){:target="_blank" rel="noopener"}

> An new version of the LockBit 2.0 ransomware has been found that automates the encryption of a Windows domain using Active Directory group policies. [...]
