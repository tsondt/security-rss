Title: Build an end-to-end attribute-based access control strategy with AWS SSO and Okta
Date: 2021-07-06T16:42:08+00:00
Author: Louay Shaat
Category: AWS Security
Tags: Advanced (300);AWS Single Sign-On (SSO);Security, Identity, & Compliance;AWS SSO;Conditions;Distributed teams;Identity providers;IdP;Okta;Permissions;SAML;Security Blog;Tags
Slug: build-an-end-to-end-attribute-based-access-control-strategy-with-aws-sso-and-okta

[Source](https://aws.amazon.com/blogs/security/build-an-end-to-end-attribute-based-access-control-strategy-with-aws-sso-and-okta/){:target="_blank" rel="noopener"}

> This blog post discusses the benefits of using an attribute-based access control (ABAC) strategy and also describes how to use ABAC with AWS Single Sign-On (AWS SSO) when you’re using Okta as an identity provider (IdP). Over the past two years, Amazon Web Services (AWS) has invested heavily in making ABAC available across the majority [...]
