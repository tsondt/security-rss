Title: Chipotle’s marketing account hacked to send phishing emails
Date: 2021-07-29T11:56:22-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: chipotles-marketing-account-hacked-to-send-phishing-emails

[Source](https://www.bleepingcomputer.com/news/security/chipotle-s-marketing-account-hacked-to-send-phishing-emails/){:target="_blank" rel="noopener"}

> Hackers have compromised an email marketing account belonging to the Chipotle food chain and used it to send out phishing emails luring recipients to malicious links. [...]
