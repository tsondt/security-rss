Title: Google supercharges Chrome’s phishing detection mechanism
Date: 2021-07-22T16:04:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-supercharges-chromes-phishing-detection-mechanism

[Source](https://portswigger.net/daily-swig/google-supercharges-chromes-phishing-detection-mechanism){:target="_blank" rel="noopener"}

> Vastly improved detection speeds in Chrome 92 eases CPU processing burden [...]
