Title: Node.js fixes severe HTTP bug that could let attackers crash apps
Date: 2021-07-30T17:44:48-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: nodejs-fixes-severe-http-bug-that-could-let-attackers-crash-apps

[Source](https://www.bleepingcomputer.com/news/security/nodejs-fixes-severe-http-bug-that-could-let-attackers-crash-apps/){:target="_blank" rel="noopener"}

> Node.js has released updates for a high severity vulnerability that could be exploited by attackers to crash the process and cause unexpected behaviors. The use-after-free vulnerability, tracked as CVE-2021-22930 is to do with how HTTP2 streams are handled in the language. [...]
