Title: MITRE updates list of top 25 most dangerous software bugs
Date: 2021-07-22T09:35:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: mitre-updates-list-of-top-25-most-dangerous-software-bugs

[Source](https://www.bleepingcomputer.com/news/security/mitre-updates-list-of-top-25-most-dangerous-software-bugs/){:target="_blank" rel="noopener"}

> MITRE has shared this year's top 25 list of most common and dangerous weaknesses plaguing software throughout the previous two years. [...]
