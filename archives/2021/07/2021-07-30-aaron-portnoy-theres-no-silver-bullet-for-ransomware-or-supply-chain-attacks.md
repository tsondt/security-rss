Title: Aaron Portnoy – ‘There’s no silver bullet for ransomware or supply chain attacks’
Date: 2021-07-30T12:30:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: aaron-portnoy-theres-no-silver-bullet-for-ransomware-or-supply-chain-attacks

[Source](https://portswigger.net/daily-swig/aaron-portnoy-theres-no-silver-bullet-for-ransomware-or-supply-chain-attacks){:target="_blank" rel="noopener"}

> ‘We don’t have the luxury of starting over,’ offensive security specialist warns in wide-ranging interview INTERVIEW Aaron Portnoy confesses to periodic bouts of imposter syndrome, despite having carv [...]
