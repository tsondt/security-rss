Title: Zero-Day Attacks on Critical WooCommerce Bug Threaten Databases
Date: 2021-07-15T20:50:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: zero-day-attacks-on-critical-woocommerce-bug-threaten-databases

[Source](https://threatpost.com/zero-day-attacks-woocommerce-databases/167846/){:target="_blank" rel="noopener"}

> The popular e-commerce platform for WordPress has started deploying emergency patches. [...]
