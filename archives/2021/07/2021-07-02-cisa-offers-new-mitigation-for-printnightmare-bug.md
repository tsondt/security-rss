Title: CISA Offers New Mitigation for PrintNightmare Bug
Date: 2021-07-02T12:21:02+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Vulnerabilities
Slug: cisa-offers-new-mitigation-for-printnightmare-bug

[Source](https://threatpost.com/cisa-mitigation-printnightmare-bug/167515/){:target="_blank" rel="noopener"}

> CERT urges administrators to disable the Windows Print spooler service in Domain Controllers and systems that don’t print, while Microsoft attempts to clarify RCE flaw with a new CVE assignment. [...]
