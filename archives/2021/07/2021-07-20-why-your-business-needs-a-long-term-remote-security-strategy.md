Title: Why Your Business Needs a Long-Term Remote Security Strategy
Date: 2021-07-20T13:59:46+00:00
Author: Chris Hass
Category: Threatpost
Tags: Breach;Cloud Security;InfoSec Insider;Malware;Mobile Security;Web Security
Slug: why-your-business-needs-a-long-term-remote-security-strategy

[Source](https://threatpost.com/business-long-term-remote-security-strategy/167950/){:target="_blank" rel="noopener"}

> Chris Hass, director of information security and research at Automox, discusses the future of work: A hybrid home/office model that will demand new security approaches. [...]
