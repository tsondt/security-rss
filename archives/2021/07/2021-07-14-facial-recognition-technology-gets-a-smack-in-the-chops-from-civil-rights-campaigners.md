Title: Facial-recognition technology gets a smack in the chops from civil rights campaigners
Date: 2021-07-14T19:14:05+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: facial-recognition-technology-gets-a-smack-in-the-chops-from-civil-rights-campaigners

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/14/civil_rights_facial_recognition/){:target="_blank" rel="noopener"}

> US retailers accused of privacy invasion Civil rights campaigners in the US have called on retailers to stop using facial-recognition technology amid worrying privacy concerns and fears that it could lead to people being wrongly arrested.... [...]
