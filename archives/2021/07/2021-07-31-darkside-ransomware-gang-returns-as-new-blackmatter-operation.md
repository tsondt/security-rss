Title: DarkSide ransomware gang returns as new BlackMatter operation
Date: 2021-07-31T15:13:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: darkside-ransomware-gang-returns-as-new-blackmatter-operation

[Source](https://www.bleepingcomputer.com/news/security/darkside-ransomware-gang-returns-as-new-blackmatter-operation/){:target="_blank" rel="noopener"}

> Encryption algorithms found in a decryptor show that the notorious DarkSide ransomware gang has rebranded as a new BlackMatter ransomware operation and is actively performing attacks on corporate entities. [...]
