Title: REvil infrastructure disappearance sparks speculation about fate of infamous ransomware slingers
Date: 2021-07-14T16:09:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: revil-infrastructure-disappearance-sparks-speculation-about-fate-of-infamous-ransomware-slingers

[Source](https://portswigger.net/daily-swig/revil-infrastructure-disappearance-sparks-speculation-about-fate-of-infamous-ransomware-slingers){:target="_blank" rel="noopener"}

> Resident REvil Websites associated with REvil – the infamous ransomware group blamed for attacks on Kaseya, Travelex, and meat supplier JBS – have dropped offline, sparking feverish speculation in the [...]
