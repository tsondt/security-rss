Title: Spam Kingpin Peter Levashov Gets Time Served
Date: 2021-07-20T21:30:00+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Alan Ralsky;Cosma;Judge Robert Chatigny;Kelihos botnet;mark rasch;microsoft;Peter Levashov;Rustock;Sevantivir;Severa;Storm Worm;waledac
Slug: spam-kingpin-peter-levashov-gets-time-served

[Source](https://krebsonsecurity.com/2021/07/spam-kingpin-peter-levashov-gets-time-served/){:target="_blank" rel="noopener"}

> Peter Levashov, appearing via Zoom at his sentencing hearing today. A federal judge in Connecticut today handed down a sentence of time served to spam kingpin Peter “Severa” Levashov, a prolific purveyor of malicious and junk email, and the creator of malware strains that infected millions of Microsoft computers globally. Levashov has been in federal custody since his extradition to the United States and guilty plea in 2018, and was facing up to 12 more years in prison. Instead, he will go free under three years of supervised release and a possible fine. A native of St. Petersburg, Russia, the [...]
