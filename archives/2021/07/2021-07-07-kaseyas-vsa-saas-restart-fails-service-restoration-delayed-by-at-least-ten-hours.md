Title: Kaseya’s VSA SaaS restart fails, service restoration delayed by at least ten hours
Date: 2021-07-07T02:48:30+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: kaseyas-vsa-saas-restart-fails-service-restoration-delayed-by-at-least-ten-hours

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/07/kaseya_saas_restart_fails/){:target="_blank" rel="noopener"}

> CEO comes out swinging, says 'people make the story and make the impact of this larger than what it is' Kaseya’s attempt to recover its SaaS services has failed, and its CEO has attempted to play down the significance of the incident that has seen its VSA services offline since July 2nd and over 1,000 ransomware infections.... [...]
