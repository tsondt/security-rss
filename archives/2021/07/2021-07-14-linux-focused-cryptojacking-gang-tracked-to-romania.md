Title: Linux-Focused Cryptojacking Gang Tracked to Romania
Date: 2021-07-14T16:45:18+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: linux-focused-cryptojacking-gang-tracked-to-romania

[Source](https://threatpost.com/linux-cryptojacking-gang-romania/167783/){:target="_blank" rel="noopener"}

> The gang is using a new brute-forcer – “Diicot brute” – to crack passwords on Linux-based machines with weak passwords. [...]
