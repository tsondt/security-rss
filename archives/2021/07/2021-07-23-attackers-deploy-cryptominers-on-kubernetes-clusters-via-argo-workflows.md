Title: Attackers deploy cryptominers on Kubernetes clusters via Argo Workflows
Date: 2021-07-23T11:27:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: attackers-deploy-cryptominers-on-kubernetes-clusters-via-argo-workflows

[Source](https://www.bleepingcomputer.com/news/security/attackers-deploy-cryptominers-on-kubernetes-clusters-via-argo-workflows/){:target="_blank" rel="noopener"}

> Threat actors are abusing misconfigured Argo Workflows instances to deploy cryptocurrency miners on Kubernetes (K8s) clusters. [...]
