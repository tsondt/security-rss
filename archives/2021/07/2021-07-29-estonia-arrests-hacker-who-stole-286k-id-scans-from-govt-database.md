Title: Estonia arrests hacker who stole 286K ID scans from govt database
Date: 2021-07-29T17:13:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: estonia-arrests-hacker-who-stole-286k-id-scans-from-govt-database

[Source](https://www.bleepingcomputer.com/news/security/estonia-arrests-hacker-who-stole-286k-id-scans-from-govt-database/){:target="_blank" rel="noopener"}

> A Tallinn man was arrested a week ago in Estonia under suspicion that he has exploited a government photo transfer service vulnerability to download ID scans of 286,438 Estonians from the Identity Documents Database (KMAIS). [...]
