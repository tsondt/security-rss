Title: Microsoft to beef up security portfolio with reported half-billion-dollar RiskIQ buyout
Date: 2021-07-13T12:15:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: microsoft-to-beef-up-security-portfolio-with-reported-half-billion-dollar-riskiq-buyout

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/13/microsoft_riskiq_acquisition/){:target="_blank" rel="noopener"}

> Imagine how many Print Spooler testers that would have bought Microsoft has loosened the purse strings once again with a substantial purchase in the form of security outfit RiskIQ.... [...]
