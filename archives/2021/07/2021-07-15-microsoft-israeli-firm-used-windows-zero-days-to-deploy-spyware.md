Title: Microsoft: Israeli firm used Windows zero-days to deploy spyware
Date: 2021-07-15T12:38:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-israeli-firm-used-windows-zero-days-to-deploy-spyware

[Source](https://www.bleepingcomputer.com/news/security/microsoft-israeli-firm-used-windows-zero-days-to-deploy-spyware/){:target="_blank" rel="noopener"}

> Microsoft and Citizen Lab have linked Israeli spyware company Candiru (also tracked as Sourgum) to new Windows spyware dubbed DevilsTongue deployed using now patched Windows zero-day vulnerabilities. [...]
