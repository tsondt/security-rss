Title: AWS CloudFront API: Research reveals ‘leak’ of partial account IDs
Date: 2021-07-15T13:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: aws-cloudfront-api-research-reveals-leak-of-partial-account-ids

[Source](https://portswigger.net/daily-swig/aws-cloudfront-api-research-reveals-leak-of-partial-account-ids){:target="_blank" rel="noopener"}

> Issue is ‘a design feature, not a bug’ [...]
