Title: CISA, FBI share guidance for victims of Kaseya ransomware attack
Date: 2021-07-05T10:35:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-fbi-share-guidance-for-victims-of-kaseya-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/cisa-fbi-share-guidance-for-victims-of-kaseya-ransomware-attack/){:target="_blank" rel="noopener"}

> CISA and the Federal Bureau of Investigation (FBI) have shared guidance for managed service providers (MSPs) and their customers impacted by the REvil supply-chain ransomware attack that hit the systems of Kaseya's cloud-based MSP platform. [...]
