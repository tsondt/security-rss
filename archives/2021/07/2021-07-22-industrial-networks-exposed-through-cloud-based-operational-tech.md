Title: Industrial Networks Exposed Through Cloud-Based Operational Tech
Date: 2021-07-22T17:46:25+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Vulnerabilities
Slug: industrial-networks-exposed-through-cloud-based-operational-tech

[Source](https://threatpost.com/industrial-networks-exposed-cloud-operational-tech/168024/){:target="_blank" rel="noopener"}

> Critical ICS vulnerabilities can be exploited through leading cloud-management platforms. [...]
