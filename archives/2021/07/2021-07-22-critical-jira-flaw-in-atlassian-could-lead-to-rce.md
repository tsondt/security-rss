Title: Critical Jira Flaw in Atlassian Could Lead to RCE
Date: 2021-07-22T20:52:45+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: critical-jira-flaw-in-atlassian-could-lead-to-rce

[Source](https://threatpost.com/atlassian-critical-jira-flaw/168053/){:target="_blank" rel="noopener"}

> The software-engineering platform is urging users to patch the critical flaw ASAP. [...]
