Title: Cybercriminals took advantage of WFH to target financial services companies, say financial bods
Date: 2021-07-14T05:38:59+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: cybercriminals-took-advantage-of-wfh-to-target-financial-services-companies-say-financial-bods

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/14/financial_stability_board_pandemic_report/){:target="_blank" rel="noopener"}

> Home Wi-Fi was an obvious target, but evildoers also knew to probe cloudy connections Criminals targeted security gaps at financial services firms as their staff moved to working from home, according to a report issued by the international Financial Stability Board (FSB) on Tuesday.... [...]
