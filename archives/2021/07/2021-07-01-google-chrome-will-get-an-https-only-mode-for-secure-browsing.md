Title: Google Chrome will get an HTTPS-Only Mode for secure browsing
Date: 2021-07-01T08:13:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-chrome-will-get-an-https-only-mode-for-secure-browsing

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-will-get-an-https-only-mode-for-secure-browsing/){:target="_blank" rel="noopener"}

> Google is working on adding an HTTPS-Only Mode to the Chrome web browser to protect users' web traffic from eavesdropping by upgrading all connections to HTTPS. [...]
