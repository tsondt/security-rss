Title: REvil ransomware gang's websites vanish soon after Kaseya fiasco, Uncle Sam threatens retaliation
Date: 2021-07-13T19:53:05+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: revil-ransomware-gangs-websites-vanish-soon-after-kaseya-fiasco-uncle-sam-threatens-retaliation

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/13/revil_ransomware_shuts/){:target="_blank" rel="noopener"}

> Has the US just had enough, or is it just a strategic retreat? The clear and dark web sites run by the REvil ransomware gang have gone offline, leaving netizens wondering if the extortionists have closed down – or been closed down.... [...]
