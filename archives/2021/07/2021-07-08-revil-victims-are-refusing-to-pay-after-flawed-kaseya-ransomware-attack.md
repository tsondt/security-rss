Title: REvil victims are refusing to pay after flawed Kaseya ransomware attack
Date: 2021-07-08T16:33:56-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: revil-victims-are-refusing-to-pay-after-flawed-kaseya-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/revil-victims-are-refusing-to-pay-after-flawed-kaseya-ransomware-attack/){:target="_blank" rel="noopener"}

> The REvil ransomware gang's attack on MSPs and their customers last week outwardly should have been successful, yet changes in their typical tactics and procedures have led to few ransom payments. [...]
