Title: No More Ransom celebrates success in helping 600k people recover from ransomware attacks
Date: 2021-07-26T16:03:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: no-more-ransom-celebrates-success-in-helping-600k-people-recover-from-ransomware-attacks

[Source](https://portswigger.net/daily-swig/no-more-ransom-celebrates-success-in-helping-600k-people-recover-from-ransomware-attacks){:target="_blank" rel="noopener"}

> Five-year anniversary for crime-busting website [...]
