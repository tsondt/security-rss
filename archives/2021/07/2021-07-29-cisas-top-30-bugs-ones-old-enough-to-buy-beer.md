Title: CISA’s Top 30 Bugs: One’s Old Enough to Buy Beer
Date: 2021-07-29T18:39:56+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Malware;Vulnerabilities;Web Security
Slug: cisas-top-30-bugs-ones-old-enough-to-buy-beer

[Source](https://threatpost.com/cisa-top-bugs-old-enough-to-buy-beer/168247/){:target="_blank" rel="noopener"}

> There are patches or remediations for all of them, but they're still being picked apart. Why should attackers stop if the flaws remain unpatched, as so many do? [...]
