Title: France warns of APT31 cyberspies targeting French organizations
Date: 2021-07-21T10:13:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: france-warns-of-apt31-cyberspies-targeting-french-organizations

[Source](https://www.bleepingcomputer.com/news/security/france-warns-of-apt31-cyberspies-targeting-french-organizations/){:target="_blank" rel="noopener"}

> The French national cyber-security agency today warned of an ongoing series of attacks against a large number of French organizations coordinated by the Chinese-backed APT31 cyberespionage group. [...]
