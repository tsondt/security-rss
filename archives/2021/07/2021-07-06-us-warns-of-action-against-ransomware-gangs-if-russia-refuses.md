Title: US warns of action against ransomware gangs if Russia refuses
Date: 2021-07-06T17:09:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-warns-of-action-against-ransomware-gangs-if-russia-refuses

[Source](https://www.bleepingcomputer.com/news/security/us-warns-of-action-against-ransomware-gangs-if-russia-refuses/){:target="_blank" rel="noopener"}

> White House Press Secretary Jen Psaki says that the US will take action against cybercriminal groups from Russia if the Russian government refuses to do so. [...]
