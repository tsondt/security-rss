Title: Twitter reveals surprisingly low two-factor auth (2FA) adoption rate
Date: 2021-07-23T08:06:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: twitter-reveals-surprisingly-low-two-factor-auth-2fa-adoption-rate

[Source](https://www.bleepingcomputer.com/news/security/twitter-reveals-surprisingly-low-two-factor-auth-2fa-adoption-rate/){:target="_blank" rel="noopener"}

> Twitter has revealed in its latest transparency report that only 2.3% of all active accounts have enabled at least one method of two-factor authentication (2FA) between July and December 2020. [...]
