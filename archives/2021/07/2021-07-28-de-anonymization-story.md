Title: De-anonymization Story
Date: 2021-07-28T11:03:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: data collection;de-anonymization;geolocation;privacy;pseudonymity;surveillance
Slug: de-anonymization-story

[Source](https://www.schneier.com/blog/archives/2021/07/de-anonymization-story.html){:target="_blank" rel="noopener"}

> This is important : Monsignor Jeffrey Burrill was general secretary of the US Conference of Catholic Bishops (USCCB), effectively the highest-ranking priest in the US who is not a bishop, before records of Grindr usage obtained from data brokers was correlated with his apartment, place of work, vacation home, family members’ addresses, and more. [...] The data that resulted in Burrill’s ouster was reportedly obtained through legal means. Mobile carriers sold­ — and still sell — ­location data to brokers who aggregate it and sell it to a range of buyers, including advertisers, law enforcement, roadside services, and even bounty [...]
