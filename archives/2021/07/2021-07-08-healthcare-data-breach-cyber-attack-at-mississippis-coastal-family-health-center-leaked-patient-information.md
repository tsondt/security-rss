Title: Healthcare data breach: Cyber-attack at Mississippi’s Coastal Family Health Center leaked patient information
Date: 2021-07-08T14:16:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: healthcare-data-breach-cyber-attack-at-mississippis-coastal-family-health-center-leaked-patient-information

[Source](https://portswigger.net/daily-swig/healthcare-data-breach-cyber-attack-at-mississippis-coastal-family-health-center-leaked-patient-information){:target="_blank" rel="noopener"}

> Patients notified as health provider hires consultants to investigate data leak [...]
