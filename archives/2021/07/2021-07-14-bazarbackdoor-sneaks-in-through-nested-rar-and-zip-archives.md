Title: BazarBackdoor sneaks in through nested RAR and ZIP archives
Date: 2021-07-14T15:29:17-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: bazarbackdoor-sneaks-in-through-nested-rar-and-zip-archives

[Source](https://www.bleepingcomputer.com/news/security/bazarbackdoor-sneaks-in-through-nested-rar-and-zip-archives/){:target="_blank" rel="noopener"}

> Security researchers caught a new phishing campaign that tried to deliver the BazarBackdoor malware by using the multi-compression technique and masking it as an image file. [...]
