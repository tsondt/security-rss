Title: Getting the most out of Cloud IDS for advanced network threat detection
Date: 2021-07-29T16:00:00+00:00
Author: Jonny Almaleh
Category: GCP Security
Tags: Google Cloud;Networking;Identity & Security
Slug: getting-the-most-out-of-cloud-ids-for-advanced-network-threat-detection

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-ids-helps-detect-advanced-network-threats/){:target="_blank" rel="noopener"}

> Google Cloud IDS, now available in preview, delivers cloud-native, managed, network-based threat detection, built with Palo Alto Networks’ industry-leading threat detection technologies to provide high levels of security efficacy. Cloud IDS can help customers gain deep insight into network-based threats and support industry-specific compliance goals that call for the use of an intrusion detection system. In this blog, we’re diving deeper into how Cloud IDS works to detect network-based threats and how you can get the most out of a Cloud IDS implementation. Getting the most out of Cloud IDS The implementation of an Intrusion Detection System (IDS) into virtual [...]
