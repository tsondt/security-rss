Title: Details of the REvil Ransomware Attack
Date: 2021-07-08T15:06:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberattack;malware;ransomware;Russia;supply chain;vulnerabilities;zero-day
Slug: details-of-the-revil-ransomware-attack

[Source](https://www.schneier.com/blog/archives/2021/07/details-of-the-revil-ransomware-attack.html){:target="_blank" rel="noopener"}

> ArsTechnica has a good story on the REvil ransomware attack of last weekend, with technical details: This weekend’s attack was carried out with almost surgical precision. According to Cybereason, the REvil affiliates first gained access to targeted environments and then used the zero-day in the Kaseya Agent Monitor to gain administrative control over the target’s network. After writing a base-64-encoded payload to a file named agent.crt the dropper executed it. [...] The ransomware dropper Agent.exe is signed with a Windows-trusted certificate that uses the registrant name “PB03 TRANSPORT LTD.” By digitally signing their malware, attackers are able to suppress many [...]
