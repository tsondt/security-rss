Title: Adobe updates fix 28 vulnerabilities in 6 programs
Date: 2021-07-13T17:09:57-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: adobe-updates-fix-28-vulnerabilities-in-6-programs

[Source](https://www.bleepingcomputer.com/news/security/adobe-updates-fix-28-vulnerabilities-in-6-programs/){:target="_blank" rel="noopener"}

> Adobe has released a giant Patch Tuesday security update release that fixes vulnerabilities in Adobe Dimension, Illustrator, Framemaker, Acrobat, Reader, and Bridge. [...]
