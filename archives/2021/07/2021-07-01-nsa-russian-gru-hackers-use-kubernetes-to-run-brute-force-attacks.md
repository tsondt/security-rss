Title: NSA: Russian GRU hackers use Kubernetes to run brute force attacks
Date: 2021-07-01T11:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Government
Slug: nsa-russian-gru-hackers-use-kubernetes-to-run-brute-force-attacks

[Source](https://www.bleepingcomputer.com/news/security/nsa-russian-gru-hackers-use-kubernetes-to-run-brute-force-attacks/){:target="_blank" rel="noopener"}

> The National Security Agency (NSA) warns that Russian nation-state hackers are conducting brute force attacks to access US networks and steal email and files. [...]
