Title: Podcast: Why Securing Active Directory Is a Nightmare
Date: 2021-07-28T11:01:33+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Black Hat;Malware;Podcasts;Vulnerabilities;Web Security
Slug: podcast-why-securing-active-directory-is-a-nightmare

[Source](https://threatpost.com/podcast-securing-active-directory-nightmare/168203/){:target="_blank" rel="noopener"}

> Researchers preview work to be presented at Black Hat on how AD “misconfiguration debt” lays out a dizzying array of attack paths, such as in PetitPotam. [...]
