Title: IT management biz Kaseya's VSA abused to infect businesses with ransomware
Date: 2021-07-03T15:50:23+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: it-management-biz-kaseyas-vsa-abused-to-infect-businesses-with-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/03/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Cops seize 3D printers 'used to print guns', and more bits and bytes In brief In what's looking like a nasty supply-chain attack, IT management biz Kaseya's on-prem VSA product was abused to infect its customers and/or their customers with ransomware.... [...]
