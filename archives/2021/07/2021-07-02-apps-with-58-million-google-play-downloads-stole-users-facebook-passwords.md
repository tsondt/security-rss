Title: Apps with 5.8 million Google Play downloads stole users’ Facebook passwords
Date: 2021-07-02T21:00:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;android;apps;google play;malware
Slug: apps-with-58-million-google-play-downloads-stole-users-facebook-passwords

[Source](https://arstechnica.com/?p=1778045){:target="_blank" rel="noopener"}

> Enlarge (credit: Mateusz Slodkowski/SOPA Images/LightRocket via Getty Images) Google has given the boot to nine Android apps downloaded more than 5.8 million times from the company's Play marketplace after researchers said these apps used a sneaky way to steal users' Facebook login credentials. In a bid to win users’ trust and lower their guard, the apps provided fully functioning services for photo editing and framing, exercise and training, horoscopes, and removal of junk files from Android devices, according to a post published by security firm Dr. Web. All of the identified apps offered users an option to disable in-app ads [...]
