Title: Google to bolster Chrome privacy protections with HTTPS-First Mode
Date: 2021-07-15T12:16:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-to-bolster-chrome-privacy-protections-with-https-first-mode

[Source](https://portswigger.net/daily-swig/google-to-bolster-chrome-privacy-protections-with-https-first-mode){:target="_blank" rel="noopener"}

> New browser feature will enforce connections over the encrypted web protocol [...]
