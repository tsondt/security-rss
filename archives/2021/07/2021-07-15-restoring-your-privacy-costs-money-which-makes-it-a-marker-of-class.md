Title: Restoring your privacy costs money, which makes it a marker of class
Date: 2021-07-15T09:15:08+00:00
Author: Mark Pesce
Category: The Register
Tags: 
Slug: restoring-your-privacy-costs-money-which-makes-it-a-marker-of-class

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/15/privacy_costs_money/){:target="_blank" rel="noopener"}

> Opting out of data monetisation is neither easy nor cheap Column A colleague was recently required to spend 10 days in a public-health-mandated quarantine after authorities used credit card receipts to determine he'd visited a location that had also hosted a known coronavirus case.... [...]
