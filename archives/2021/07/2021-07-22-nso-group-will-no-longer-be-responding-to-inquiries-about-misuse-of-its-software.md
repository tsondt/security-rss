Title: NSO Group 'will no longer be responding to inquiries' about misuse of its software
Date: 2021-07-22T04:09:46+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: nso-group-will-no-longer-be-responding-to-inquiries-about-misuse-of-its-software

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/22/nso_group_denies_everything_stops_answering_questions/){:target="_blank" rel="noopener"}

> Denies everything, as governments open probes into the company and its wares The NSO Group, a purveyor of spyware it hopes governments and law enforcement bodies will use to fight terrorism, has announced it will not answer any further questions about allegations raised by Amnesty International and Forbidden Stories that its products have been widely misused.... [...]
