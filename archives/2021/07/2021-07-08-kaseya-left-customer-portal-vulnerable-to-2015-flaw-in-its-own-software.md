Title: Kaseya Left Customer Portal Vulnerable to 2015 Flaw in its Own Software
Date: 2021-07-08T15:22:58+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ransomware;alex holden;CVE-2015-2862;CVE-2021-30116;Dutch Institute for Vulnerability Disclosure;Fred Voccola;Hold Security;Kaseya;Mandiant;Michael Sanders;ransomware;rEvil;Victor Gevers;Wietse Boonstra
Slug: kaseya-left-customer-portal-vulnerable-to-2015-flaw-in-its-own-software

[Source](https://krebsonsecurity.com/2021/07/kaseya-left-customer-portal-vulnerable-to-2015-flaw-in-its-own-software/){:target="_blank" rel="noopener"}

> Last week cybercriminals deployed ransomware to 1,500 organizations, including many that provide IT security and technical support to other companies. The attackers exploited a vulnerability in software from Kaseya, a Miami-based company whose products help system administrators manage large networks remotely. Now it appears Kaseya’s customer service portal was left vulnerable until last week to a data-leaking security flaw that was first identified in the same software six years ago. On July 3, the REvil ransomware affiliate program began using a zero-day security hole ( CVE-2021-30116 ) to deploy ransomware to hundreds of IT management companies running Kaseya’s remote management [...]
