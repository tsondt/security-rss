Title: Kaseya Patches Zero-Days Used in REvil Attacks
Date: 2021-07-12T15:53:42+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: kaseya-patches-zero-days-used-in-revil-attacks

[Source](https://threatpost.com/kaseya-patches-zero-days-revil-attacks/167670/){:target="_blank" rel="noopener"}

> The security update addresses three VSA vulnerabilities used by the ransomware gang to launch a worldwide supply-chain attack on MSPs and their customers. [...]
