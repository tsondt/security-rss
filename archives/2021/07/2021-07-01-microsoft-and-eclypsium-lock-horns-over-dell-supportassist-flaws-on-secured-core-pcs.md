Title: Microsoft and Eclypsium lock horns over Dell SupportAssist flaws on secured-core PCs
Date: 2021-07-01T20:45:05+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: microsoft-and-eclypsium-lock-horns-over-dell-supportassist-flaws-on-secured-core-pcs

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/01/microsoft_eclypsium_supportassist/){:target="_blank" rel="noopener"}

> Niggles continue even after problem was quietly fixed The Dell SupportAssist RCE furore has rumbled on after infosec outfit Eclypsium snapped back at Microsoft's statement on the matter.... [...]
