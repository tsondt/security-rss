Title: Digital rights org claims cyberattacks against Filipino media outlets come from government and army
Date: 2021-07-02T19:00:03+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: digital-rights-org-claims-cyberattacks-against-filipino-media-outlets-come-from-government-and-army

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/02/ddos_attack_philippines_dost/){:target="_blank" rel="noopener"}

> IP address inside Department of Science and Technology ran a vulnerability scan on target Qurium Media Foundation has reported a campaign of DDoS attacks on Filipino media outlets and human rights organisations that appear to be coming from the country's Department of Science and Technology (DOST) and Army.... [...]
