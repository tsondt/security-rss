Title: Encryption issues account for minority of flaws in encryption libraries – research
Date: 2021-07-13T16:15:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: encryption-issues-account-for-minority-of-flaws-in-encryption-libraries-research

[Source](https://portswigger.net/daily-swig/encryption-issues-account-for-minority-of-flaws-in-encryption-libraries-research){:target="_blank" rel="noopener"}

> ‘Complexity is an even worse enemy of security in cryptographic software’ [...]
