Title: Kaspersky Password Manager's random password generator was about as random as your wall clock
Date: 2021-07-06T20:49:37+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: kaspersky-password-managers-random-password-generator-was-about-as-random-as-your-wall-clock

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/06/kaspersky_password_manager/){:target="_blank" rel="noopener"}

> Could be brute-forced due to design blunders, according to infosec outfit Last year, Kaspersky Password Manager (KPM) users got an alert telling them to update their weaker passwords. Now we've found out why that happened.... [...]
