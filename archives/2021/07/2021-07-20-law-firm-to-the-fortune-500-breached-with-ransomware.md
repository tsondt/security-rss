Title: Law Firm to the Fortune 500 Breached with Ransomware
Date: 2021-07-20T17:00:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Malware;Web Security
Slug: law-firm-to-the-fortune-500-breached-with-ransomware

[Source](https://threatpost.com/law-firm-fortune-500-breach-ransomware/167951/){:target="_blank" rel="noopener"}

> Deep-pocketed clients' customers & suppliers could be in the attacker's net, with potential PII exposure from an A-list clientele such as Apple, Boeing and IBM. [...]
