Title: Automatically update AWS WAF IP sets with AWS IP ranges
Date: 2021-07-08T18:00:17+00:00
Author: Fola Bolodeoku
Category: AWS Security
Tags: Advanced (300);AWS Shield;AWS WAF;Security, Identity, & Compliance;AWS Threat Research Team;Security Blog;Web application
Slug: automatically-update-aws-waf-ip-sets-with-aws-ip-ranges

[Source](https://aws.amazon.com/blogs/security/automatically-update-aws-waf-ip-sets-with-aws-ip-ranges/){:target="_blank" rel="noopener"}

> Note: This blog post describes how to automatically update AWS WAF IP sets with the most recent AWS IP ranges for AWS services. This related blog post describes how to perform a similar update for Amazon CloudFront IP ranges that are used in VPC Security Groups. You can use AWS Managed Rules for AWS WAF [...]
