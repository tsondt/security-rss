Title: Sysadmins: Why not simply verify there's no backdoor in every program you install, and thus avoid any cyber-drama?
Date: 2021-07-31T07:14:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: sysadmins-why-not-simply-verify-theres-no-backdoor-in-every-program-you-install-and-thus-avoid-any-cyber-drama

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/31/enisa_supply_chain_attack_report/){:target="_blank" rel="noopener"}

> Just 'validate third-party code before using it', says Euro body Half of publicly reported supply chain attacks were carried out by "well known APT groups", according to an analysis by EU infosec agency ENISA, which warned such digital assaults need to drive "new protective methods."... [...]
