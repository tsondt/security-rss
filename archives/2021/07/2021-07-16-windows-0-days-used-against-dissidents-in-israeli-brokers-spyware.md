Title: Windows 0-Days Used Against Dissidents in Israeli Broker’s Spyware
Date: 2021-07-16T15:55:57+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware;Mobile Security;Vulnerabilities
Slug: windows-0-days-used-against-dissidents-in-israeli-brokers-spyware

[Source](https://threatpost.com/windows-zero-days-israeli-spyware-dissidents/167865/){:target="_blank" rel="noopener"}

> Candiru, aka Sourgum, allegedly sells the DevilsTongue surveillance malware to governments around the world. [...]
