Title: Kubernetes Cloud Clusters Face Cyberattacks via Argo Workflows
Date: 2021-07-21T15:19:56+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Malware;Vulnerabilities
Slug: kubernetes-cloud-clusters-face-cyberattacks-via-argo-workflows

[Source](https://threatpost.com/kubernetes-cyberattacks-argo-workflows/167997/){:target="_blank" rel="noopener"}

> Misconfigured permissions for Argo's web-facing dashboard allow unauthenticated attackers to run code on Kubernetes targets, including cryptomining containers. [...]
