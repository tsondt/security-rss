Title: Biden warns 'real shooting war' will be sparked by severe cyber attack
Date: 2021-07-28T04:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: biden-warns-real-shooting-war-will-be-sparked-by-severe-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/28/biden_cyber_attack_real_war_prediction/){:target="_blank" rel="noopener"}

> Suggests incident 'of great consequence' in the real world could be a tipping point United States President Joe Biden has shared his view that a "real shooting war" could be sparked by a severe cyber attack.... [...]
