Title: Kaseya claims SaaS restoration going swimmingly
Date: 2021-07-12T03:05:59+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: kaseya-claims-saas-restoration-going-swimmingly

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/12/kaseya_update/){:target="_blank" rel="noopener"}

> Patch for on-prem is out, too, but company asks users to pause work to review late documentation changes Beleaguered IT management firm Kaseya says sixty per cent of its SaaS services have been successfully restored.... [...]
