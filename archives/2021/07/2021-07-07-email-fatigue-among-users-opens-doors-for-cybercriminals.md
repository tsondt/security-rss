Title: Email fatigue among users opens doors for cybercriminals
Date: 2021-07-07T09:05:00-04:00
Author: Sponsored by Acronis
Category: BleepingComputer
Tags: 
Slug: email-fatigue-among-users-opens-doors-for-cybercriminals

[Source](https://www.bleepingcomputer.com/news/security/email-fatigue-among-users-opens-doors-for-cybercriminals/){:target="_blank" rel="noopener"}

> When it comes to email security, a one-and-done approach never works. Using this multi-layered approach, which includes URL filtering, can often block malicious domains and downloads of malware, preventing systems from being infected in the first place. [...]
