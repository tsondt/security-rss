Title: Microsoft paid out $14m in bug bounty rewards in past 12 months – report
Date: 2021-07-12T14:00:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: microsoft-paid-out-14m-in-bug-bounty-rewards-in-past-12-months-report

[Source](https://portswigger.net/daily-swig/microsoft-paid-out-14m-in-bug-bounty-rewards-in-past-12-months-report){:target="_blank" rel="noopener"}

> Security researchers received an average of $10k per report [...]
