Title: Two-for-Tuesday vulnerabilities send Windows and Linux users scrambling
Date: 2021-07-20T21:17:41+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;exploits;hacking;Linux;vulnerabilities;Windows
Slug: two-for-tuesday-vulnerabilities-send-windows-and-linux-users-scrambling

[Source](https://arstechnica.com/?p=1781750){:target="_blank" rel="noopener"}

> Enlarge The world woke up on Tuesday to two new vulnerabilities—one in Windows and the other in Linux—that allow hackers with a toehold in a vulnerable system to bypass OS security restrictions and access sensitive resources. As operating systems and applications become harder to hack, successful attacks typically require two or more vulnerabilities. One vulnerability allows the attacker access to low-privileged OS resources, where code can be executed or sensitive data can be read. A second vulnerability elevates that code execution or file access to OS resources reserved for password storage or other sensitive operations. The value of so-called local [...]
