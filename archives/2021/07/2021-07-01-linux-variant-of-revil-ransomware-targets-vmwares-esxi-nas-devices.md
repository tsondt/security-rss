Title: Linux Variant of REvil Ransomware Targets VMware’s ESXi, NAS Devices
Date: 2021-07-01T20:56:15+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cloud Security;Hacks;Malware
Slug: linux-variant-of-revil-ransomware-targets-vmwares-esxi-nas-devices

[Source](https://threatpost.com/linux-variant-ransomware-vmwares-nas/167511/){:target="_blank" rel="noopener"}

> Criminals behind the potent REvil ransomware have ported the malware to Linux for targeted attacks. [...]
