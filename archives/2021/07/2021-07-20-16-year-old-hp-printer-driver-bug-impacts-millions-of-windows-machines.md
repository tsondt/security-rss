Title: 16-Year-Old HP Printer-Driver Bug Impacts Millions of Windows Machines
Date: 2021-07-20T13:31:50+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: 16-year-old-hp-printer-driver-bug-impacts-millions-of-windows-machines

[Source](https://threatpost.com/hp-printer-driver-bug-windows/167944/){:target="_blank" rel="noopener"}

> The bug could allow cyberattackers to bypass security products, tamper with data and run code in kernel mode. [...]
