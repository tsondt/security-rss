Title: FIN7’s Liquor Lure Compromises Law Firm with Backdoor
Date: 2021-07-23T16:24:47+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Malware;Web Security
Slug: fin7s-liquor-lure-compromises-law-firm-with-backdoor

[Source](https://threatpost.com/fin7s-liquor-lure-law-firm-backdoor/168086/){:target="_blank" rel="noopener"}

> Using a lure relating to a lawsuit against the owner of Jack Daniels whiskey, the cybergang launched a campaign that may be bent on ransomware deployment. [...]
