Title: China puts national security protection at the center of new data privacy law
Date: 2021-07-09T14:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: china-puts-national-security-protection-at-the-center-of-new-data-privacy-law

[Source](https://portswigger.net/daily-swig/china-puts-national-security-protection-at-the-center-of-new-data-privacy-law){:target="_blank" rel="noopener"}

> Broad and vague definition of sensitive information worries lawyers [...]
