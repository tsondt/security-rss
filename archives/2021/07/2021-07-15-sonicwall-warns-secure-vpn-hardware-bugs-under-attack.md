Title: SonicWall Warns Secure VPN Hardware Bugs Under Attack
Date: 2021-07-15T15:41:31+00:00
Author: Tom Spring
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: sonicwall-warns-secure-vpn-hardware-bugs-under-attack

[Source](https://threatpost.com/sonicwall-vpn-bugs-attack/167824/){:target="_blank" rel="noopener"}

> SonicWall issued an urgent security alert warning customers that some of its current and legacy secure VPN appliances were under active attack. [...]
