Title: LinkedIn’s 1.2B Data-Scrape Victims Already Being Targeted by Attackers
Date: 2021-07-01T11:41:58+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Privacy;Web Security
Slug: linkedins-12b-data-scrape-victims-already-being-targeted-by-attackers

[Source](https://threatpost.com/linkedin-data-scrape-victims-targeted-attackers/167473/){:target="_blank" rel="noopener"}

> A refined database of 88K U.S. business owners on LinkedIn has been posted in a hacker forum. [...]
