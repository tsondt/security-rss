Title: NSA shares guidance on how to secure your wireless devices
Date: 2021-07-29T13:26:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: nsa-shares-guidance-on-how-to-secure-your-wireless-devices

[Source](https://www.bleepingcomputer.com/news/security/nsa-shares-guidance-on-how-to-secure-your-wireless-devices/){:target="_blank" rel="noopener"}

> The US National Security Agency (NSA) today published guidance on how to properly secure wireless devices against potential attacks targeting them when traveling or working remotely. [...]
