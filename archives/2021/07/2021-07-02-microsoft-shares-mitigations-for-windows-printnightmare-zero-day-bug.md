Title: Microsoft shares mitigations for Windows PrintNightmare zero-day bug
Date: 2021-07-02T02:56:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-shares-mitigations-for-windows-printnightmare-zero-day-bug

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-mitigations-for-windows-printnightmare-zero-day-bug/){:target="_blank" rel="noopener"}

> Microsoft has provided mitigation guidance to block attacks on systems vulnerable to exploits targeting the Windows Print Spooler zero-day vulnerability known as PrintNightmare. [...]
