Title: D-Link issues hotfix for hard-coded password router vulnerabilities
Date: 2021-07-16T10:36:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: d-link-issues-hotfix-for-hard-coded-password-router-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/d-link-issues-hotfix-for-hard-coded-password-router-vulnerabilities/){:target="_blank" rel="noopener"}

> D-Link has issued a hotfix to address multiple vulnerabilities in the DIR-3040 AC3000-based wireless internet router that can allow attackers to execute arbitrary code on unpatched routers, gain access to sensitive information, or crash the routers after triggering a denial of service state. [...]
