Title: Kaseya obtains universal decryptor for REvil ransomware victims
Date: 2021-07-22T13:46:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: kaseya-obtains-universal-decryptor-for-revil-ransomware-victims

[Source](https://www.bleepingcomputer.com/news/security/kaseya-obtains-universal-decryptor-for-revil-ransomware-victims/){:target="_blank" rel="noopener"}

> Kaseya received a universal decryptor that allows victims of the July 2nd REvil ransomware attack to recover their files for free. [...]
