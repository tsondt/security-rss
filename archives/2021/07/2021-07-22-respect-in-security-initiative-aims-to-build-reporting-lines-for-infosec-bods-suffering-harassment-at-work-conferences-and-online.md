Title: Respect in Security initiative aims to build reporting lines for infosec bods suffering harassment at work, conferences and online
Date: 2021-07-22T15:30:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: respect-in-security-initiative-aims-to-build-reporting-lines-for-infosec-bods-suffering-harassment-at-work-conferences-and-online

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/22/cyber_security_anti_harassment_initiative/){:target="_blank" rel="noopener"}

> Some of the stuff going on in the industry is completely out of order A new initiative aims to make it easier to report personal abuse and harassment within the information security industry – without the involvement of social media mobs.... [...]
