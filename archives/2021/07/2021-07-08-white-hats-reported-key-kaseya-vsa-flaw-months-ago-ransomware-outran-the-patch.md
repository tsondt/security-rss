Title: White hats reported key Kaseya VSA flaw months ago. Ransomware outran the patch
Date: 2021-07-08T06:19:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: white-hats-reported-key-kaseya-vsa-flaw-months-ago-ransomware-outran-the-patch

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/08/kaseya_dutch_vulnerability/){:target="_blank" rel="noopener"}

> So close, and yet so far One of the vulnerabilities in Kaseya's IT management software VSA that was exploited by miscreants to infect up to 1,500 businesses with ransomware was reported to the vendor in April – and the patch just wasn't ready in time.... [...]
