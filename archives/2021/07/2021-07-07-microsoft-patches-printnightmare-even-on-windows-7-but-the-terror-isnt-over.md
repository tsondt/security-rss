Title: Microsoft patches PrintNightmare — even on Windows 7 — but the terror isn’t over
Date: 2021-07-07T05:20:39+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: microsoft-patches-printnightmare-even-on-windows-7-but-the-terror-isnt-over

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/07/printnightmare_patched/){:target="_blank" rel="noopener"}

> No fixes yet for Windows 10 version 1607, Windows Server 2016, or Windows Server 2012 Microsoft has issued out-of-band patches for the PrintNightmare print spooler bug that allows lets remote Windows users execute code as system on your domain controller.... [...]
