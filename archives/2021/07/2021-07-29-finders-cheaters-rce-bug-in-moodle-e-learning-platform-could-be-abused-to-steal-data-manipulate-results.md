Title: Finders, cheaters: RCE bug in Moodle e-learning platform could be abused to steal data, manipulate results
Date: 2021-07-29T11:49:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: finders-cheaters-rce-bug-in-moodle-e-learning-platform-could-be-abused-to-steal-data-manipulate-results

[Source](https://portswigger.net/daily-swig/finders-cheaters-rce-bug-in-moodle-e-learning-platform-could-be-abused-to-steal-data-manipulate-results){:target="_blank" rel="noopener"}

> Researchers warn of critical vulnerability in popular education management system [...]
