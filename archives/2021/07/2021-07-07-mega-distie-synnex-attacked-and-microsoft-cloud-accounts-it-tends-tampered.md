Title: Mega-distie SYNNEX attacked and Microsoft cloud accounts it tends tampered
Date: 2021-07-07T06:32:00+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: mega-distie-synnex-attacked-and-microsoft-cloud-accounts-it-tends-tampered

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/07/synnex_rnc_microsoft_attack/){:target="_blank" rel="noopener"}

> Republican National Committee said to be a victim, with Cozy Bear in the frame for the attack Updated Technology distributor SYNNEX has admitted that its systems and Microsoft accounts it tends have been attacked, after the National Committee of the US Republican Party (RNC) named it as the source of a recent security incident.... [...]
