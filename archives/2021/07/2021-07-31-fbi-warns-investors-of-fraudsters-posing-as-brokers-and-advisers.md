Title: FBI warns investors of fraudsters posing as brokers and advisers
Date: 2021-07-31T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-investors-of-fraudsters-posing-as-brokers-and-advisers

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-investors-of-fraudsters-posing-as-brokers-and-advisers/){:target="_blank" rel="noopener"}

> The FBI Criminal Investigative Division and Securities and Exchange Commission warn investors of fraudsters impersonating registered investment professionals such as investment advisers and registered brokers. [...]
