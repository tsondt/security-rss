Title: Amazon starts rolling out Ring end-to-end encryption globally
Date: 2021-07-13T17:47:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: amazon-starts-rolling-out-ring-end-to-end-encryption-globally

[Source](https://www.bleepingcomputer.com/news/security/amazon-starts-rolling-out-ring-end-to-end-encryption-globally/){:target="_blank" rel="noopener"}

> Amazon-owned Ring has announced starting the worldwide roll out of video End-to-End Encryption (E2EE) to customers with compatible devices. [...]
