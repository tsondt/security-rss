Title: Phish Swims Past Email Security With Milanote Pages
Date: 2021-07-22T20:53:23+00:00
Author: Tara Seals
Category: Threatpost
Tags: News;Web Security
Slug: phish-swims-past-email-security-with-milanote-pages

[Source](https://threatpost.com/phish-email-security-milanote/168021/){:target="_blank" rel="noopener"}

> The “Evernote for creatives” is anchoring a rapidly spiking phishing campaign, evading SEGs with ease. [...]
