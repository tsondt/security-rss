Title: Critical vulnerabilities in open source text editor Etherpad could lead to remote takeover
Date: 2021-07-14T09:38:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: critical-vulnerabilities-in-open-source-text-editor-etherpad-could-lead-to-remote-takeover

[Source](https://portswigger.net/daily-swig/critical-vulnerabilities-in-open-source-text-editor-etherpad-could-lead-to-remote-takeover){:target="_blank" rel="noopener"}

> XSS bug in open source program has now been patched, though second flaw remains [...]
