Title: Cloud Cryptomining Swindle in Google Play Rakes in Cash
Date: 2021-07-07T11:57:51+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Web Security
Slug: cloud-cryptomining-swindle-in-google-play-rakes-in-cash

[Source](https://threatpost.com/cloud-cryptomining-swindle-google-play/167581/){:target="_blank" rel="noopener"}

> At least 25 apps have lured in tens of thousands of victims with the promise of helping them cash in on the cryptomining craze. [...]
