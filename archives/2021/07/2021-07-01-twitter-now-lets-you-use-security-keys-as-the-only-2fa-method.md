Title: Twitter now lets you use security keys  as the only 2FA method
Date: 2021-07-01T07:37:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: twitter-now-lets-you-use-security-keys-as-the-only-2fa-method

[Source](https://www.bleepingcomputer.com/news/security/twitter-now-lets-you-use-security-keys-as-the-only-2fa-method/){:target="_blank" rel="noopener"}

> Twitter now lets users use security keys as the only two-factor authentication (2FA) method while having all other methods disabled, as the social network announced three months ago, in March. [...]
