Title: Kaseya Obtains Universal Decryptor for REvil Ransomware
Date: 2021-07-23T12:21:24+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Cryptography;Hacks;Malware
Slug: kaseya-obtains-universal-decryptor-for-revil-ransomware

[Source](https://threatpost.com/kaseya-universal-decryptor-revil-ransomware/168070/){:target="_blank" rel="noopener"}

> The vendor will work with customers affected by the early July spate of ransomware attacks to unlock files; it's unclear if the ransom was paid. [...]
