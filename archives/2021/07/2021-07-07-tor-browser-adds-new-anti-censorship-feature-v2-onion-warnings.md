Title: Tor Browser adds new anti-censorship feature, V2 onion warnings
Date: 2021-07-07T13:00:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: tor-browser-adds-new-anti-censorship-feature-v2-onion-warnings

[Source](https://www.bleepingcomputer.com/news/security/tor-browser-adds-new-anti-censorship-feature-v2-onion-warnings/){:target="_blank" rel="noopener"}

> The Tor Project has released Tor Browser 10.5 with V2 onion URL deprecation warnings, a redesigned Tor connection experience, and an improved anti-censorship feature. [...]
