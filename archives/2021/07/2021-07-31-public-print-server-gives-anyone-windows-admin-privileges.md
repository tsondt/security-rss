Title: Public print server gives anyone Windows admin privileges
Date: 2021-07-31T14:23:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: public-print-server-gives-anyone-windows-admin-privileges

[Source](https://www.bleepingcomputer.com/news/microsoft/public-print-server-gives-anyone-windows-admin-privileges/){:target="_blank" rel="noopener"}

> A researcher has created a remote print server allowing any Windows user with limited privileges to gain complete control over a computer by installing a print driver. [...]
