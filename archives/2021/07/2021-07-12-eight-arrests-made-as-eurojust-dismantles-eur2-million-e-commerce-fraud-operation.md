Title: Eight arrests made as Eurojust dismantles €2 million e-commerce fraud operation
Date: 2021-07-12T12:29:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: eight-arrests-made-as-eurojust-dismantles-eur2-million-e-commerce-fraud-operation

[Source](https://portswigger.net/daily-swig/eight-arrests-made-as-eurojust-dismantles-2-million-e-commerce-fraud-operation){:target="_blank" rel="noopener"}

> Phishing victims thought they were buying goods and services via Amazon, eBay, and Airbnb Romanian and Greek police have arrested eight members of an organized crime group that defrauded online shoppe [...]
