Title: Critical Microsoft Hyper-V bug could haunt orgs for a long time
Date: 2021-07-28T09:30:06-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: critical-microsoft-hyper-v-bug-could-haunt-orgs-for-a-long-time

[Source](https://www.bleepingcomputer.com/news/security/critical-microsoft-hyper-v-bug-could-haunt-orgs-for-a-long-time/){:target="_blank" rel="noopener"}

> Technical details are now available for a vulnerability that affects Hyper-V, Microsoft's native hypervisor for creating virtual machines on Windows systems and in Azure cloud computing environment. [...]
