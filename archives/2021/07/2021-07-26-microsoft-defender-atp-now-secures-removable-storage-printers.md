Title: Microsoft Defender ATP now secures removable storage, printers
Date: 2021-07-26T17:21:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-atp-now-secures-removable-storage-printers

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-atp-now-secures-removable-storage-printers/){:target="_blank" rel="noopener"}

> Microsoft has added new removable storage device and printer controls to Microsoft Defender for Endpoint, the enterprise version of its Windows 10 Defender antivirus. [...]
