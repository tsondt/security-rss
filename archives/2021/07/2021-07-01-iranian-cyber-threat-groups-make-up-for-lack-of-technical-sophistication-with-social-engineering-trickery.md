Title: Iranian cyber-threat groups make up for lack of technical sophistication with social engineering trickery
Date: 2021-07-01T15:02:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: iranian-cyber-threat-groups-make-up-for-lack-of-technical-sophistication-with-social-engineering-trickery

[Source](https://portswigger.net/daily-swig/iranian-cyber-threat-groups-make-up-for-lack-of-technical-sophistication-with-social-engineering-trickery){:target="_blank" rel="noopener"}

> We take a look at the underestimated threat posed by Iran’s state-sponsored hacking groups [...]
