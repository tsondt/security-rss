Title: Windows print nightmare continues with malicious driver packages
Date: 2021-07-15T14:57:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: windows-print-nightmare-continues-with-malicious-driver-packages

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-print-nightmare-continues-with-malicious-driver-packages/){:target="_blank" rel="noopener"}

> Microsoft's print nightmare continues with another example of how a threat actor can achieve SYSTEM privileges by abusing malicious printer drivers. [...]
