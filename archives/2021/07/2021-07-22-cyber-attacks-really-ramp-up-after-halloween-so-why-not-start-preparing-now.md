Title: Cyber-attacks really ramp up after Halloween – so why not start preparing now?
Date: 2021-07-22T22:09:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: cyber-attacks-really-ramp-up-after-halloween-so-why-not-start-preparing-now

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/22/learn_at_sans_singapore_2021/){:target="_blank" rel="noopener"}

> Learn tricks of the trade at SANS Singapore 2021 – and treat yourself to a discount Promo Whisper it softly, but we’re fast forwarding through the second half of 2021, which means the holiday shopping season – and accompanying hacking season – is not far behind.... [...]
