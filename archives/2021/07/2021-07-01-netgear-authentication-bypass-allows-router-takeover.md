Title: Netgear Authentication Bypass Allows Router Takeover
Date: 2021-07-01T11:30:35+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: netgear-authentication-bypass-allows-router-takeover

[Source](https://threatpost.com/netgear-authentication-bypass-router-takeover/167469/){:target="_blank" rel="noopener"}

> Microsoft researchers discovered the firmware flaws in the DGN-2200v1 series router that can enable authentication bypass to take over devices and access stored credentials. [...]
