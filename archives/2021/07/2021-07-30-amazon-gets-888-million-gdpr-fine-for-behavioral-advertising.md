Title: Amazon gets $888 million GDPR fine for behavioral advertising
Date: 2021-07-30T12:34:47-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Technology
Slug: amazon-gets-888-million-gdpr-fine-for-behavioral-advertising

[Source](https://www.bleepingcomputer.com/news/technology/amazon-gets-888-million-gdpr-fine-for-behavioral-advertising/){:target="_blank" rel="noopener"}

> Amazon has quietly been hit with a record-breaking €746 million fine for alleged GDPR violations regarding how it performs targeted behavioral advertising. [...]
