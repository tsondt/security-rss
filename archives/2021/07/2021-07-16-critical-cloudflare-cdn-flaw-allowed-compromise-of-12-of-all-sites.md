Title: Critical Cloudflare CDN flaw allowed compromise of 12% of all sites
Date: 2021-07-16T06:29:27-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: critical-cloudflare-cdn-flaw-allowed-compromise-of-12-of-all-sites

[Source](https://www.bleepingcomputer.com/news/security/critical-cloudflare-cdn-flaw-allowed-compromise-of-12-percent-of-all-sites/){:target="_blank" rel="noopener"}

> Cloudflare has fixed a critical vulnerability in its free and open-source CDNJS potentially impacting 12.7% of all websites on the internet. CDNJS serves millions of websites with over 4,000 JavaScript and CSS libraries stored publicly on GitHub, making it the second-largest JavaScript CDN. [...]
