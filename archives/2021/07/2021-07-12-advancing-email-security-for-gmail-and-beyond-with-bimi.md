Title: Advancing email security for Gmail and beyond with BIMI
Date: 2021-07-12T16:00:00+00:00
Author: Wei Chuang
Category: GCP Security
Tags: Google Workspace;Identity & Security
Slug: advancing-email-security-for-gmail-and-beyond-with-bimi

[Source](https://cloud.google.com/blog/products/identity-security/bringing-bimi-to-gmail-in-google-workspace/){:target="_blank" rel="noopener"}

> Creating a secure-by-default experience based on robust defenses has always been a core design principle for Gmail. That’s why we’ve established a strong baseline of security in Gmail, with built-in protections to help automatically filter out potentially malicious messages. While these defenses help keep Gmail users safe, email functions as part of a large, complex, interconnected ecosystem that we continually invest in and work to protect. After first announcing Gmail’s Brand Indicators for Message Identification (BIMI) pilot last year, today we’re announcing that over the coming weeks we’re rolling out Gmail’s general support of BIMI, an industry standard that aims [...]
