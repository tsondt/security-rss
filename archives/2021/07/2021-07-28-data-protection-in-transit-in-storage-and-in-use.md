Title: Data protection in transit, in storage, and in use
Date: 2021-07-28T17:30:00+00:00
Author: Max Saltonstall
Category: GCP Security
Tags: Google Cloud;Identity & Security;Developers & Practitioners
Slug: data-protection-in-transit-in-storage-and-in-use

[Source](https://cloud.google.com/blog/topics/developers-practitioners/data-protection-in-transit-in-storage-and-in-use/){:target="_blank" rel="noopener"}

> In our first episode of the Cloud Security Podcast, we had the pleasure to talk to Nelly Porter, Group Product Manager for the Cloud Security team. In this interview Anton, Tim, and Nelly examine a critical question about data security: how can we process extremely sensitive data in the cloud while also keeping it protected against insider access? Turns out it's easier than it sounds on Google Cloud. Some customers using public cloud worry about their data in many different ways. And they have all sorts of sensitive data, from healthcare records, to credit card numbers, to corporate secrets, and [...]
