Title: Chained vulnerabilities in Aruba Networks firmware allowed remote code execution on routers
Date: 2021-07-20T11:06:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: chained-vulnerabilities-in-aruba-networks-firmware-allowed-remote-code-execution-on-routers

[Source](https://portswigger.net/daily-swig/chained-vulnerabilities-in-aruba-networks-firmware-allowed-remote-code-execution-on-routers){:target="_blank" rel="noopener"}

> Office pen test leads to discovery of multiple bugs in enterprise networking kit [...]
