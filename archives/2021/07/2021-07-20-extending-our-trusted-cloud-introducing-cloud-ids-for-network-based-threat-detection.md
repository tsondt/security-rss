Title: Extending our Trusted Cloud: Introducing Cloud IDS for Network-based Threat Detection
Date: 2021-07-20T13:00:00+00:00
Author: Megan Yahya
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: extending-our-trusted-cloud-introducing-cloud-ids-for-network-based-threat-detection

[Source](https://cloud.google.com/blog/products/identity-security/detect-complex-network-threats-with-cloud-ids/){:target="_blank" rel="noopener"}

> Every day, Google Cloud helps customers around the world stay ahead of evolving threats and keep their workloads safe. Today, we’re advancing these efforts with Cloud IDS, our new network security offering, which is now available in preview. With Cloud IDS, we’re extending our trusted cloud with native network-based threat detection capabilities to help you solve your most critical network security challenges. Cloud IDS delivers easy-to-use, cloud-native, managed, network-based threat detection. With Cloud IDS, customers can enjoy a Google Cloud-integrated experience, built with Palo Alto Networks’ industry-leading threat detection technologies to provide high levels of security efficacy. Cloud IDS is [...]
