Title: Brit firm fined £200k for banging on about missold PPI in 11.4 million nuisance calls
Date: 2021-07-02T08:28:07+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: brit-firm-fined-ps200k-for-banging-on-about-missold-ppi-in-114-million-nuisance-calls

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/02/ppi_claims_biz_fined/){:target="_blank" rel="noopener"}

> 316 people complained to the Information Commissioner's Office A Leeds-based claims management firm has been fined £200,000 for making more than 11 million unwanted PPI calls, the UK's data watchdog announced today.... [...]
