Title: Cloud Armor: enhancing security at the edge with Adaptive Protection, expanded coverage scope, and new rules
Date: 2021-07-16T16:00:00+00:00
Author: Emil Kiner
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: cloud-armor-enhancing-security-at-the-edge-with-adaptive-protection-expanded-coverage-scope-and-new-rules

[Source](https://cloud.google.com/blog/products/identity-security/improve-your-ddos--waf-defense-with-new-cloud-armor-features/){:target="_blank" rel="noopener"}

> Cloud Armor, Google Cloud’s DDoS defense service and web-application firewall (WAF) helps customers protect their websites and services from denial of service and web attacks every day using the same infrastructure, network, and technology that has protected Google’s own internet-facing properties from the largest DDoS attacks reported. To stay ahead of the evolving threat landscape, we’re continuing to enhance protections delivered at the edge of Google’s network through innovations in Cloud Armor. First, we’re releasing the preview of Cloud Armor Adaptive Protection, a machine learning-powered capability to protect your applications and services from Layer 7 DDoS attacks. We have been [...]
