Title: Microsoft Releases Emergency Patch for PrintNightmare Bugs
Date: 2021-07-07T10:55:02+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: microsoft-releases-emergency-patch-for-printnightmare-bugs

[Source](https://threatpost.com/microsoft-emergency-patch-printnightmare/167578/){:target="_blank" rel="noopener"}

> The fix doesn’t cover the entire problem nor all affected systems however, so the company also is offering workarounds and plans to release further remedies at a later date. [...]
