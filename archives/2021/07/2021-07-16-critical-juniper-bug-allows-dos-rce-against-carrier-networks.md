Title: Critical Juniper Bug Allows DoS, RCE Against Carrier Networks
Date: 2021-07-16T17:17:19+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: critical-juniper-bug-allows-dos-rce-against-carrier-networks

[Source](https://threatpost.com/critical-juniper-bug-dos-rce-carrier/167869/){:target="_blank" rel="noopener"}

> Telecom providers, including wireless carriers, are at risk of disruption of network service if the bug in SBR Carrier is exploited. [...]
