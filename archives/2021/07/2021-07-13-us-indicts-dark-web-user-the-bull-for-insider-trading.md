Title: US indicts dark web user 'The Bull' for insider trading
Date: 2021-07-13T15:52:22-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: us-indicts-dark-web-user-the-bull-for-insider-trading

[Source](https://www.bleepingcomputer.com/news/security/us-indicts-dark-web-user-the-bull-for-insider-trading/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DoJ) has charged an individual for engaging in insider trading on the darknet. Greece-based Apostolos Trovias, known as the "The Bull" frequently used encrypted messaging services and the dark web for soliciting, exchanging and selling inside information. [...]
