Title: Insurance giant CNA reports data breach after ransomware attack
Date: 2021-07-09T07:29:40-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: insurance-giant-cna-reports-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/insurance-giant-cna-reports-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> CNA Financial Corporation, a leading US-based insurance company, is notifying customers of a data breach following a Phoenix CryptoLocker ransomware attack that hit its systems in March. [...]
