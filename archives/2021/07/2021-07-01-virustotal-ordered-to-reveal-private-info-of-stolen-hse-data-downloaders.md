Title: VirusTotal ordered to reveal private info of stolen HSE data downloaders
Date: 2021-07-01T15:16:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: virustotal-ordered-to-reveal-private-info-of-stolen-hse-data-downloaders

[Source](https://www.bleepingcomputer.com/news/security/virustotal-ordered-to-reveal-private-info-of-stolen-hse-data-downloaders/){:target="_blank" rel="noopener"}

> An Irish court has ordered VirusTotal to provide the information of subscribers who downloaded or uploaded confidential data stolen from Ireland's national health care service during a ransomware attack. [...]
