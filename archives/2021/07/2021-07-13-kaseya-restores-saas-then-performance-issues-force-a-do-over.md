Title: Kaseya restores SaaS, then 'performance issues' force a do-over
Date: 2021-07-13T05:57:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: kaseya-restores-saas-then-performance-issues-force-a-do-over

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/13/kaseya_update/){:target="_blank" rel="noopener"}

> What’s another 20 minutes of sudden unplanned downtime between friends? Kaseya has fully restored its SaaS product, then quickly inflicted a little more unplanned downtime on users.... [...]
