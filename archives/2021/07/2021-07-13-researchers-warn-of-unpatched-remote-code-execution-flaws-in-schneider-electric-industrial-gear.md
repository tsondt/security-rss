Title: Researchers warn of unpatched remote code execution flaws in Schneider Electric industrial gear
Date: 2021-07-13T10:45:07+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: researchers-warn-of-unpatched-remote-code-execution-flaws-in-schneider-electric-industrial-gear

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/13/armis_schneider_electric_flaw/){:target="_blank" rel="noopener"}

> ModiPwn attack gives full control over Modicon programmable logic controllers Armis security researchers have warned of severe and unpatched remote code execution vulnerabilities in Schneider Electric's programmable logic controllers (PLCs), allowing attackers to take control of a variety of industrial systems.... [...]
