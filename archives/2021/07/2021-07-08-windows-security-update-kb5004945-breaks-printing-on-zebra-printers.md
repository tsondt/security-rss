Title: Windows security update KB5004945 breaks printing on Zebra printers
Date: 2021-07-08T12:14:31-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: windows-security-update-kb5004945-breaks-printing-on-zebra-printers

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-security-update-kb5004945-breaks-printing-on-zebra-printers/){:target="_blank" rel="noopener"}

> Microsoft's recent out-of-band KB5004945 PrintNightmare security updates are preventing Windows users from printing to certain Zebra printers. [...]
