Title: Another 0-Day Looms for Many Western Digital Users
Date: 2021-07-02T16:05:50+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Time to Patch;Ars Technica;Dan Goodin;MyBook Live;MyCloud OS 3;MyCloud OS 5;Pedro Ribeiro;Pwn2Own;Radek Domanski;Western Digital
Slug: another-0-day-looms-for-many-western-digital-users

[Source](https://krebsonsecurity.com/2021/07/another-0-day-looms-for-many-western-digital-users/){:target="_blank" rel="noopener"}

> Some of Western Digital’s MyCloud-based data storage devices. Image: WD. Countless Western Digital customers saw their MyBook Live network storage drives remotely wiped in the past month thanks to a bug in a product line the company stopped supporting in 2015, as well as a previously unknown zero-day flaw. But there is a similarly serious zero-day flaw present in a much broader range of newer Western Digital MyCloud network storage devices that will remain unfixed for many customers who can’t or won’t upgrade to the latest operating system. At issue is a remote code execution flaw residing in all Western [...]
