Title: How Fake Accounts and Sneaker-Bots Took Over the Internet
Date: 2021-07-08T16:20:21+00:00
Author: Jason Kent
Category: Threatpost
Tags: InfoSec Insider;Web Security
Slug: how-fake-accounts-and-sneaker-bots-took-over-the-internet

[Source](https://threatpost.com/fake-accounts-sneaker-bots-internet/167626/){:target="_blank" rel="noopener"}

> Jason Kent, hacker-in-residence at Cequence Security, discusses fake online accounts, and the fraud they carry out on a daily basis. [...]
