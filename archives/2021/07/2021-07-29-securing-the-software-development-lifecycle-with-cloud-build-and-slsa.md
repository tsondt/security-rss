Title: Securing the software development lifecycle with Cloud Build and SLSA
Date: 2021-07-29T16:00:00+00:00
Author: Dustin Ingram
Category: GCP Security
Tags: Application Development;Identity & Security;DevOps & SRE
Slug: securing-the-software-development-lifecycle-with-cloud-build-and-slsa

[Source](https://cloud.google.com/blog/products/devops-sre/google-introduces-slsa-framework/){:target="_blank" rel="noopener"}

> One of the biggest challenges for software developers is the need to make informed choices about the external software and products they use in their own software systems. Evaluating whether a given system is appropriately secured can be challenging, especially if it’s external or owned by a third party. This so-called software supply chain has been under increasing scrutiny in recent years, with attacks on software systems being responsible for damages to both public and private interests. In collaboration with the OpenSSF, Google has proposed Supply-chain Levels for Software Artifacts (SLSA). The new SLSA framework formalizes criteria around software supply [...]
