Title: Comparis customers targeted by scammers after ransomware attack
Date: 2021-07-18T10:16:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: comparis-customers-targeted-by-scammers-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/comparis-customers-targeted-by-scammers-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Leading Swiss price comparison platform Comparis has notified customers of a data breach following a ransomware attack that hit and took down its entire network last week. [...]
