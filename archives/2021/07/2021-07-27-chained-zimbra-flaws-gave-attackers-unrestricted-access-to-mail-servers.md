Title: Chained Zimbra flaws gave attackers unrestricted access to mail servers
Date: 2021-07-27T15:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: chained-zimbra-flaws-gave-attackers-unrestricted-access-to-mail-servers

[Source](https://portswigger.net/daily-swig/chained-zimbra-flaws-gave-attackers-unrestricted-access-to-mail-servers){:target="_blank" rel="noopener"}

> Researchers help fix two security flaws that opened the door to internal data and networks [...]
