Title: You've patched that critical Sage X3 ERP security hole, yeah? Not exposing the suite to the internet, either, yeah?
Date: 2021-07-07T23:56:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: youve-patched-that-critical-sage-x3-erp-security-hole-yeah-not-exposing-the-suite-to-the-internet-either-yeah

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/07/sage_x3_rce/){:target="_blank" rel="noopener"}

> Details of flaws now public for miscreants to exploit Admins of on-premises Sage X3 ERP deployments should check they're not exposing the enterprise resource planning suite to the public internet in case they fall victim to an unauthenticated command execution vulnerability.... [...]
