Title: You'll want to shut down the Windows Print Spooler service (yes, again): Another privilege escalation bug found
Date: 2021-07-16T17:28:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: youll-want-to-shut-down-the-windows-print-spooler-service-yes-again-another-privilege-escalation-bug-found

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/16/spooler_service_local_privilege_escalation/){:target="_blank" rel="noopener"}

> PrintNightmare? More like Groundhog Day for admins Microsoft has shared guidance revealing yet another vulnerability connected to its Windows Print Spooler service, saying it is "developing a security update."... [...]
