Title: Hacker dumps private info of pro-Trump GETTR social network members
Date: 2021-07-06T14:30:23-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hacker-dumps-private-info-of-pro-trump-gettr-social-network-members

[Source](https://www.bleepingcomputer.com/news/security/hacker-dumps-private-info-of-pro-trump-gettr-social-network-members/){:target="_blank" rel="noopener"}

> Newly launched social site GETTR suffered a data breach after a hacker claimed to use an unsecured API to scrape the private information of almost 90,000 members and then shared the data on a hacking forum. [...]
