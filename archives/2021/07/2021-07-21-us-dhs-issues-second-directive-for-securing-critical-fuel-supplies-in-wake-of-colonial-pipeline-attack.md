Title: US DHS issues second directive for securing critical fuel supplies in wake of Colonial Pipeline attack
Date: 2021-07-21T12:47:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-dhs-issues-second-directive-for-securing-critical-fuel-supplies-in-wake-of-colonial-pipeline-attack

[Source](https://portswigger.net/daily-swig/us-dhs-issues-second-directive-for-securing-critical-fuel-supplies-in-wake-of-colonial-pipeline-attack){:target="_blank" rel="noopener"}

> Follow-up to similar recent directive prescribes contingency and recovery plan, architecture design review [...]
