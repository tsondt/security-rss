Title: Friday Squid Blogging: Giant Squid Model
Date: 2021-07-16T21:12:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-giant-squid-model

[Source](https://www.schneier.com/blog/archives/2021/07/friday-squid-blogging-giant-squid-model.html){:target="_blank" rel="noopener"}

> Pretty wooden model. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
