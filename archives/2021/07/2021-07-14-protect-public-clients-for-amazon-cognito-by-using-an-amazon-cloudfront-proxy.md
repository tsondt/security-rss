Title: Protect public clients for Amazon Cognito by using an Amazon CloudFront proxy
Date: 2021-07-14T18:51:27+00:00
Author: Mahmoud Matouk
Category: AWS Security
Tags: Advanced (300);Amazon CloudFront;Amazon Cognito;Security, Identity, & Compliance;AWS best practices;Identity;Security Blog
Slug: protect-public-clients-for-amazon-cognito-by-using-an-amazon-cloudfront-proxy

[Source](https://aws.amazon.com/blogs/security/protect-public-clients-for-amazon-cognito-by-using-an-amazon-cloudfront-proxy/){:target="_blank" rel="noopener"}

> In Amazon Cognito user pools, an app client is an entity that has permission to call unauthenticated API operations (that is, operations that don’t have an authenticated user), such as operations to sign up, sign in, and handle forgotten passwords. In this post, I show you a solution designed to protect these API operations from [...]
