Title: British Airways agrees to pay victims of record-breaking data breach
Date: 2021-07-07T13:16:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: british-airways-agrees-to-pay-victims-of-record-breaking-data-breach

[Source](https://portswigger.net/daily-swig/british-airways-agrees-to-pay-victims-of-record-breaking-data-breach){:target="_blank" rel="noopener"}

> Case described as ‘largest group action personal data claim in UK history’ [...]
