Title: REvil is Off-Line
Date: 2021-07-16T20:03:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybercrime;cybersecurity;ransomware;Russia
Slug: revil-is-off-line

[Source](https://www.schneier.com/blog/archives/2021/07/revil-is-off-line.html){:target="_blank" rel="noopener"}

> This is an interesting development : Just days after President Biden demanded that President Vladimir V. Putin of Russia shut down ransomware groups attacking American targets, the most aggressive of the groups suddenly went off-line early Tuesday. [...] Gone was the publicly available “happy blog” the group maintained, listing some of its victims and the group’s earnings from its digital extortion schemes. Internet security groups said the custom-made sites ­- think of them as virtual conference rooms — where victims negotiated with REvil over how much ransom they would pay to get their data unlocked also disappeared. So did the [...]
