Title: New Paper: Assuring Compliance in the Cloud
Date: 2021-07-22T15:58:00+00:00
Author: Zeal Somani
Category: GCP Security
Tags: Identity & Security
Slug: new-paper-assuring-compliance-in-the-cloud

[Source](https://cloud.google.com/blog/products/identity-security/new-paper-assuring-compliance-cloud/){:target="_blank" rel="noopener"}

> Cloud transformation and the adoption of modern DevOps technology presents both opportunities and challenges for IT compliance functions. With DevOps style application development, the feedback loop for developers and engineers is much tighter than with traditional application development pipelines, enabling speed and agility of application release cycles. While speedy CI/CD is a critical advantage of DevOps, it also shifts compliance left in the development timeline, and therefore puts pressure on the IT risk & compliance organization to modernize their approach to regulatory compliance as well. With the ongoing shift towards cloud technologies and DevOps, modernization of regulatory compliance is no [...]
