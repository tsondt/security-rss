Title: Firefox becomes latest browser to support Fetch Metadata request headers
Date: 2021-07-13T13:55:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: firefox-becomes-latest-browser-to-support-fetch-metadata-request-headers

[Source](https://portswigger.net/daily-swig/firefox-becomes-latest-browser-to-support-fetch-metadata-request-headers){:target="_blank" rel="noopener"}

> Extra layer of security helps protect against CSRF and XS-Leak attacks [...]
