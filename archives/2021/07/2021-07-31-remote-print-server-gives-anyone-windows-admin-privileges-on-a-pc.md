Title: Remote print server gives anyone Windows admin privileges on a PC
Date: 2021-07-31T14:23:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: remote-print-server-gives-anyone-windows-admin-privileges-on-a-pc

[Source](https://www.bleepingcomputer.com/news/microsoft/remote-print-server-gives-anyone-windows-admin-privileges-on-a-pc/){:target="_blank" rel="noopener"}

> A researcher has created a remote print server allowing any Windows user with limited privileges to gain complete control over a device simply by installing a print driver. [...]
