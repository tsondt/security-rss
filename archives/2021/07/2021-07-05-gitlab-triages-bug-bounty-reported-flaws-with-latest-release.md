Title: GitLab triages bug bounty-reported flaws with latest release
Date: 2021-07-05T14:31:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: gitlab-triages-bug-bounty-reported-flaws-with-latest-release

[Source](https://portswigger.net/daily-swig/gitlab-triages-bug-bounty-reported-flaws-with-latest-release){:target="_blank" rel="noopener"}

> CSRF and denial-of-service vulnerabilities extinguished [...]
