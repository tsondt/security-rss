Title: Software maker removes "backdoor" giving root access to radio devices
Date: 2021-07-15T07:21:22-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: software-maker-removes-backdoor-giving-root-access-to-radio-devices

[Source](https://www.bleepingcomputer.com/news/security/software-maker-removes-backdoor-giving-root-access-to-radio-devices/){:target="_blank" rel="noopener"}

> The author of a popular software-defined radio (SDR) project has removed a "backdoor" from radio devices that granted root-level access. The backdoor had been, according to the author, present in all versions of KiwiSDR devices for the purposes of remote administration and debugging. [...]
