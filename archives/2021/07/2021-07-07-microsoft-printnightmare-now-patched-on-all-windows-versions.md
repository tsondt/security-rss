Title: Microsoft: PrintNightmare now patched on all Windows versions
Date: 2021-07-07T17:52:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-printnightmare-now-patched-on-all-windows-versions

[Source](https://www.bleepingcomputer.com/news/security/microsoft-printnightmare-now-patched-on-all-windows-versions/){:target="_blank" rel="noopener"}

> Microsoft has released the KB5004948 emergency security update to address the Windows Print Spooler PrintNightmare vulnerability on all editions of Windows 10 1607 and Windows Server 2016. [...]
