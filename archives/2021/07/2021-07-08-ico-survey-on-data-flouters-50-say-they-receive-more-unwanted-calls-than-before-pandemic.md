Title: ICO survey on data flouters: 50% say they receive more unwanted calls than before pandemic
Date: 2021-07-08T14:29:09+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: ico-survey-on-data-flouters-50-say-they-receive-more-unwanted-calls-than-before-pandemic

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/08/ico_data_protection_survey/){:target="_blank" rel="noopener"}

> Plus: Fewer people proportion agree 'current laws' sufficiently protect personal information The dodgy use of personal data by rogue organisations in fraud and scams continues to be the biggest data protection bugbear for people in the UK, according to research from the Information Commissioners Office (ICO).... [...]
