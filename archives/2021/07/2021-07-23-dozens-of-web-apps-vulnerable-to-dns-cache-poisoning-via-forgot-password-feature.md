Title: Dozens of web apps vulnerable to DNS cache poisoning via ‘forgot password’ feature
Date: 2021-07-23T11:28:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: dozens-of-web-apps-vulnerable-to-dns-cache-poisoning-via-forgot-password-feature

[Source](https://portswigger.net/daily-swig/dozens-of-web-apps-vulnerable-to-dns-cache-poisoning-via-forgot-password-feature){:target="_blank" rel="noopener"}

> Of 146 tested, two applications were vulnerable to Kaminsky attacks, and 62 to IP fragmentation attacks [...]
