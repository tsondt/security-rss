Title: Dropbox researchers develop tool to detect lateral movement attacks against enterprise networks
Date: 2021-07-30T15:01:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: dropbox-researchers-develop-tool-to-detect-lateral-movement-attacks-against-enterprise-networks

[Source](https://portswigger.net/daily-swig/dropbox-researchers-develop-tool-to-detect-lateral-movement-attacks-against-enterprise-networks){:target="_blank" rel="noopener"}

> ‘Hopper’ delivered better detection and much reduced false alarms in early tests [...]
