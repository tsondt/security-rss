Title: Amnesty International and French media protection org claim massive misuse of NSO spyware
Date: 2021-07-19T03:15:32+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: amnesty-international-and-french-media-protection-org-claim-massive-misuse-of-nso-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/19/mass_misuse_of_nso_pegasus_spyware_alleged/){:target="_blank" rel="noopener"}

> Heads of State, academics, diplomats, journalists, and others targeted, iPhones vulnerable Amnesty International and French journalism advocacy organisation Forbidden Stories say they've acquired a leaked list of individuals targeted by users of Israeli spyware-for-law-enforcement operator NSO Group, and that Heads of State, academics, diplomats, human rights advocates, and media figures are among those targeted.... [...]
