Title: Hacked Data for 69K LimeVPN Users Up for Sale on Dark Web
Date: 2021-07-01T16:24:03+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Privacy;Web Security
Slug: hacked-data-for-69k-limevpn-users-up-for-sale-on-dark-web

[Source](https://threatpost.com/hacked-data-limevpn-dark-web/167492/){:target="_blank" rel="noopener"}

> LimeVPN has confirmed a data incident, and meanwhile its website has been knocked offline. [...]
