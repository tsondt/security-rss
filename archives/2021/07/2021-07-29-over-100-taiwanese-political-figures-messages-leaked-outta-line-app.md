Title: Over 100 Taiwanese political figures' messages leaked outta LINE app
Date: 2021-07-29T04:34:29+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: over-100-taiwanese-political-figures-messages-leaked-outta-line-app

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/29/taiwan_line_attack/){:target="_blank" rel="noopener"}

> Attack turned off encryption function, which made snooping rather easier Law enforcement agencies in Taiwan are investigating a cyberattack on over 100 local political figures and dignitaries who used the messaging app LINE.... [...]
