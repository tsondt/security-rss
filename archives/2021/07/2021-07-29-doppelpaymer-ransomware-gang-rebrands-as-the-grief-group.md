Title: DoppelPaymer ransomware gang rebrands as the Grief group
Date: 2021-07-29T02:20:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: doppelpaymer-ransomware-gang-rebrands-as-the-grief-group

[Source](https://www.bleepingcomputer.com/news/security/doppelpaymer-ransomware-gang-rebrands-as-the-grief-group/){:target="_blank" rel="noopener"}

> After a period of little to no activity, the DoppelPaymer ransomware operation has made a rebranding move, now going by the name Grief (a.k.a. Pay or Grief). [...]
