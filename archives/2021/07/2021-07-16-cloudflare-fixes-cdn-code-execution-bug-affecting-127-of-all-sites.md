Title: Cloudflare fixes CDN code execution bug affecting 12.7% of all sites
Date: 2021-07-16T06:29:27-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: cloudflare-fixes-cdn-code-execution-bug-affecting-127-of-all-sites

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-fixes-cdn-code-execution-bug-affecting-127-percent-of-all-sites/){:target="_blank" rel="noopener"}

> Cloudflare has fixed a critical vulnerability in its free and open-source CDNJS potentially impacting 12.7% of all websites on the internet. CDNJS serves millions of websites with over 4,000 JavaScript and CSS libraries stored publicly on GitHub, making it the second-largest JavaScript CDN. [...]
