Title: Interpol urges police to unite against 'potential ransomware pandemic'
Date: 2021-07-12T16:33:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: interpol-urges-police-to-unite-against-potential-ransomware-pandemic

[Source](https://www.bleepingcomputer.com/news/security/interpol-urges-police-to-unite-against-potential-ransomware-pandemic/){:target="_blank" rel="noopener"}

> Interpol (International Criminal Police Organisation) Secretary General Jürgen Stock urged police agencies and industry partners to work together to prevent what looks like a future ransomware pandemic. [...]
