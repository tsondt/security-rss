Title: Mozilla Firefox to roll out DNS over HTTPS for Canadian users
Date: 2021-07-08T09:00:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: mozilla-firefox-to-roll-out-dns-over-https-for-canadian-users

[Source](https://www.bleepingcomputer.com/news/security/mozilla-firefox-to-roll-out-dns-over-https-for-canadian-users/){:target="_blank" rel="noopener"}

> Mozilla has decided to roll out the DNS over HTTPS (DoH) feature by default for Canadian Firefox users later this month. The move comes after DoH has already been offered to US-based Firefox users since 2020. [...]
