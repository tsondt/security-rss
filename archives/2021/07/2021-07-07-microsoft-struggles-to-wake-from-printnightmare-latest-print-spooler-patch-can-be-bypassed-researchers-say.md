Title: Microsoft struggles to wake from PrintNightmare: Latest print spooler patch can be bypassed, researchers say
Date: 2021-07-07T18:18:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: microsoft-struggles-to-wake-from-printnightmare-latest-print-spooler-patch-can-be-bypassed-researchers-say

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/07/printnightmare_fix_fail/){:target="_blank" rel="noopener"}

> I pity the spool Updated Any celebrations that Microsoft's out-of-band patch had put a stop PrintNightmare shenanigans may have been premature.... [...]
