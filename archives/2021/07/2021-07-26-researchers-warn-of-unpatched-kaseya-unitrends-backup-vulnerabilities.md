Title: Researchers warn of unpatched Kaseya Unitrends backup vulnerabilities
Date: 2021-07-26T13:02:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: researchers-warn-of-unpatched-kaseya-unitrends-backup-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/researchers-warn-of-unpatched-kaseya-unitrends-backup-vulnerabilities/){:target="_blank" rel="noopener"}

> Security researchers warn of new zero-day vulnerabilities in the Kaseya Unitrends service and advise users not to expose the service to the Internet. [...]
