Title: SonicWall warns of 'critical' ransomware risk to EOL SMA 100 VPN appliances
Date: 2021-07-14T11:39:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: sonicwall-warns-of-critical-ransomware-risk-to-eol-sma-100-vpn-appliances

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-warns-of-critical-ransomware-risk-to-eol-sma-100-vpn-appliances/){:target="_blank" rel="noopener"}

> SonicWall has issued an "urgent security notice" warning customers of ransomware attacks targeting unpatched end-of-life (EoL) Secure Mobile Access (SMA) 100 series and Secure Remote Access (SRA) products. [...]
