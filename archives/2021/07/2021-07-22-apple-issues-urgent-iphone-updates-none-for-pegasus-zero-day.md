Title: Apple Issues Urgent iPhone Updates; None for Pegasus Zero-Day
Date: 2021-07-22T16:18:25+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;Web Security
Slug: apple-issues-urgent-iphone-updates-none-for-pegasus-zero-day

[Source](https://threatpost.com/apple-iphone-pegasus-zero-day/168040/){:target="_blank" rel="noopener"}

> Update now: The ream of bugs includes some remotely exploitable code execution flaws. Still to come: a fix for what makes iPhones easy prey for Pegasus spyware. [...]
