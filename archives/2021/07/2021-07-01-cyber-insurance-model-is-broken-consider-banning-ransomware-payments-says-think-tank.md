Title: Cyber insurance model is broken, consider banning ransomware payments, says think tank
Date: 2021-07-01T15:30:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: cyber-insurance-model-is-broken-consider-banning-ransomware-payments-says-think-tank

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/01/rusi_cyber_insurance_ransomware_report/){:target="_blank" rel="noopener"}

> UK.gov's carrots and sticks never came to pass, co-author tells us Cyber insurance isn't exactly driving organisations to improve their infosec practices, a think-tank has warned – and some insurers are thinking of giving up thanks to the impact of ransomware.... [...]
