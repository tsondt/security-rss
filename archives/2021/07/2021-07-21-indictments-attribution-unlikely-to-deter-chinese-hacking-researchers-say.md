Title: Indictments, Attribution Unlikely to Deter Chinese Hacking, Researchers Say
Date: 2021-07-21T17:31:16+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Hacks;Malware;Vulnerabilities
Slug: indictments-attribution-unlikely-to-deter-chinese-hacking-researchers-say

[Source](https://threatpost.com/indictments-attribution-chinese-hacking/168005/){:target="_blank" rel="noopener"}

> Researchers are skeptical that much will come from calling out China for the Microsoft Exchange attacks and APT40 activity, but the move marks an important foreign-policy change. [...]
