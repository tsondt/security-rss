Title: Ruthless Attackers Target Florida Condo Collapse Victims
Date: 2021-07-19T18:48:14+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Privacy;Web Security
Slug: ruthless-attackers-target-florida-condo-collapse-victims

[Source](https://threatpost.com/attackers-target-florida-condo-collapse-victims/167917/){:target="_blank" rel="noopener"}

> Hackers are stealing the identities of those lost in the condo-collapse tragedy. [...]
