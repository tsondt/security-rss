Title: Biden: Severe cyberattacks could escalate to 'real shooting war'
Date: 2021-07-28T12:23:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: biden-severe-cyberattacks-could-escalate-to-real-shooting-war

[Source](https://www.bleepingcomputer.com/news/security/biden-severe-cyberattacks-could-escalate-to-real-shooting-war/){:target="_blank" rel="noopener"}

> President Joe Biden warned that cyberattacks leading to severe security breaches could lead to a "real shooting war" with another major world power. [...]
