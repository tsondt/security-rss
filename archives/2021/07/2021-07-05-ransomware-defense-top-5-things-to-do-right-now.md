Title: Ransomware Defense: Top 5 Things to Do Right Now
Date: 2021-07-05T12:00:08+00:00
Author: Matt Bromiley
Category: Threatpost
Tags: InfoSec Insider;Malware
Slug: ransomware-defense-top-5-things-to-do-right-now

[Source](https://threatpost.com/ransomware-defense-top-5-tips/167536/){:target="_blank" rel="noopener"}

> Matt Bromiley, senior consultant with Mandiant Managed Defense, discusses the top tricks and tips for protecting enterprise environments from ransomware. [...]
