Title: DuckDuckGo's new email privacy service forwards tracker-free messages
Date: 2021-07-20T15:03:45-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: duckduckgos-new-email-privacy-service-forwards-tracker-free-messages

[Source](https://www.bleepingcomputer.com/news/security/duckduckgos-new-email-privacy-service-forwards-tracker-free-messages/){:target="_blank" rel="noopener"}

> DuckDuckGo is rolling out an email privacy feature that strips incoming messages of trackers that can help profile you for better profiling and ad targeting. [...]
