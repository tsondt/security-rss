Title: UC San Diego Health discloses data breach after employee email accounts hijacked
Date: 2021-07-28T13:31:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uc-san-diego-health-discloses-data-breach-after-employee-email-accounts-hijacked

[Source](https://portswigger.net/daily-swig/uc-san-diego-health-discloses-data-breach-after-employee-email-accounts-hijacked){:target="_blank" rel="noopener"}

> An array of personal, financial, and medical data is thought to have been compromised [...]
