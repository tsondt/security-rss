Title: NortonLifeLock sniffs around Avast, announces 'advanced discussions' for acquisition
Date: 2021-07-15T10:46:12+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: nortonlifelock-sniffs-around-avast-announces-advanced-discussions-for-acquisition

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/15/nortonlifelock_and_avast_confirm_merger_negotiations/){:target="_blank" rel="noopener"}

> Company now has 28 days to make up its mind NortonLifeLock, the somewhat clunky moniker adopted by the former consumer business arm of the Symantec Corporation, has announced "advanced discussions" with rival Avast over a possible merger.... [...]
