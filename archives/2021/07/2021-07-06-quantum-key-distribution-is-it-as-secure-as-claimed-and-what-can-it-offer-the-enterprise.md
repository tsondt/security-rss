Title: Quantum Key Distribution: Is it as secure as claimed and what can it offer the enterprise?
Date: 2021-07-06T08:30:05+00:00
Author: Davey Winder
Category: The Register
Tags: 
Slug: quantum-key-distribution-is-it-as-secure-as-claimed-and-what-can-it-offer-the-enterprise

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/06/quantum_key_distribution/){:target="_blank" rel="noopener"}

> Er... let's just ask the experts boffins Feature Do the laws of physics trump mathematical complexity, or is Quantum Key Distribution (QKD) nothing more than 21st-century enterprise encryption snake oil? The number of QKD news headlines that have included unhackable, uncrackable or unbreakable could certainly lead you towards the former conclusion.... [...]
