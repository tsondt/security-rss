Title: US authorities are offering $10 million for information on nation-state cyber-attacks
Date: 2021-07-19T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-authorities-are-offering-10-million-for-information-on-nation-state-cyber-attacks

[Source](https://portswigger.net/daily-swig/us-authorities-are-offering-10-million-for-information-on-nation-state-cyber-attacks){:target="_blank" rel="noopener"}

> Sum may be awarded in cryptocurrency to help protect whistleblowers’ anonymity [...]
