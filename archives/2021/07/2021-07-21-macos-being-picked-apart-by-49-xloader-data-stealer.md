Title: MacOS Being Picked Apart by $49 XLoader Data Stealer
Date: 2021-07-21T10:00:23+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: macos-being-picked-apart-by-49-xloader-data-stealer

[Source](https://threatpost.com/macos-49-xloader-data-stealer/167971/){:target="_blank" rel="noopener"}

> Cheap, easy and prolific, the new version of the old FormBook form-stealer and keylogger has added Mac users to its hit list, and it’s selling like hotcakes. [...]
