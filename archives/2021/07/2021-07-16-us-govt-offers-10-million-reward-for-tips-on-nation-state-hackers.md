Title: US govt offers $10 million reward for tips on nation-state hackers
Date: 2021-07-16T14:46:41-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: us-govt-offers-10-million-reward-for-tips-on-nation-state-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-govt-offers-10-million-reward-for-tips-on-nation-state-hackers/){:target="_blank" rel="noopener"}

> The United States government has taken two more active measures to fight and defend against malicious cyber activities affecting the country's business and critical infrastructure sectors. [...]
