Title: PyPI packages caught stealing credit card numbers, Discord tokens
Date: 2021-07-30T08:18:46-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: pypi-packages-caught-stealing-credit-card-numbers-discord-tokens

[Source](https://www.bleepingcomputer.com/news/security/pypi-packages-caught-stealing-credit-card-numbers-discord-tokens/){:target="_blank" rel="noopener"}

> The Python Package Index (PyPI) registry has removed several Python packages this week aimed at stealing users' credit card numbers, Discord tokens, and granting arbitrary code execution capabilities to attackers. These malicious packages were downloaded over 30,000 times according to the researchers who caught them. [...]
