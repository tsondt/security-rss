Title: Lazarus Targets Job-Seeking Engineers with Malicious Documents
Date: 2021-07-09T10:50:37+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: lazarus-targets-job-seeking-engineers-with-malicious-documents

[Source](https://threatpost.com/lazarus-engineers-malicious-docs/167647/){:target="_blank" rel="noopener"}

> Notorious North Korean APT impersonates Airbus, General Motors and Rheinmetall to lure potential victims into downloading malware. [...]
