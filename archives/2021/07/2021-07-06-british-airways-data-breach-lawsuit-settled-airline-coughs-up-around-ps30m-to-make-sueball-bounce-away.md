Title: British Airways data breach lawsuit settled: Airline coughs up around £30m to make sueball bounce away
Date: 2021-07-06T12:58:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: british-airways-data-breach-lawsuit-settled-airline-coughs-up-around-ps30m-to-make-sueball-bounce-away

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/06/british_airways_data_breach_lawsuit_settled/){:target="_blank" rel="noopener"}

> And a third of that's going into the lawyers' pockets British Airways has settled the not-quite-a-class-action* lawsuit against it, paying around £32m to make the data breach case in the High Court of England and Wales go away.... [...]
