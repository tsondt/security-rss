Title: Don’t Wanna Pay Ransom Gangs? Test Your Backups.
Date: 2021-07-19T21:11:11+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;Bill Siegel;Coveware;Emsisoft;Fabian Wosar;kim zetter
Slug: dont-wanna-pay-ransom-gangs-test-your-backups

[Source](https://krebsonsecurity.com/2021/07/dont-wanna-pay-ransom-gangs-test-your-backups/){:target="_blank" rel="noopener"}

> Browse the comments on virtually any story about a ransomware attack and you will almost surely encounter the view that the victim organization could have avoided paying their extortionists if only they’d had proper data backups. But the ugly truth is there are many non-obvious reasons why victims end up paying even when they have done nearly everything right from a data backup perspective. This story isn’t about what organizations do in response to cybercriminals holding their data for hostage, which has become something of a best practice among most of the top ransomware crime groups today. Rather, it’s about [...]
