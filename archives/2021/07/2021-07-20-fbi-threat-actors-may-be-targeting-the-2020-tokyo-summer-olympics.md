Title: FBI: Threat actors may be targeting the 2020 Tokyo Summer Olympics
Date: 2021-07-20T07:27:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-threat-actors-may-be-targeting-the-2020-tokyo-summer-olympics

[Source](https://www.bleepingcomputer.com/news/security/fbi-threat-actors-may-be-targeting-the-2020-tokyo-summer-olympics/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warns of threat actors potentially targeting the upcoming Olympic Games, although evidence of attacks planned against the Olympic Games Tokyo 2020 is yet to be uncovered. [...]
