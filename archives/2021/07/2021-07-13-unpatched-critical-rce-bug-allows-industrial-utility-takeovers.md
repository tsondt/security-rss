Title: Unpatched Critical RCE Bug Allows Industrial, Utility Takeovers
Date: 2021-07-13T20:04:17+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Vulnerabilities
Slug: unpatched-critical-rce-bug-allows-industrial-utility-takeovers

[Source](https://threatpost.com/unpatched-critical-rce-industrial-utility-takeovers/167751/){:target="_blank" rel="noopener"}

> The 'ModiPwn' bug lays open production lines, sensors, conveyor belts, elevators, HVACs and more that use Schneider Electric PLCs. [...]
