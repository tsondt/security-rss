Title: REvil is increasing ransoms for Kaseya ransomware attack victims
Date: 2021-07-04T13:35:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: revil-is-increasing-ransoms-for-kaseya-ransomware-attack-victims

[Source](https://www.bleepingcomputer.com/news/security/revil-is-increasing-ransoms-for-kaseya-ransomware-attack-victims/){:target="_blank" rel="noopener"}

> The REvil ransomware gang is increasing the ransom demands for victims encrypted during Friday's Kaseya ransomware attack. [...]
