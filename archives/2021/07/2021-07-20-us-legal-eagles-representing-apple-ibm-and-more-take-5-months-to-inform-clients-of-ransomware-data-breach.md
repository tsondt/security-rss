Title: US legal eagles representing Apple, IBM, and more take 5 months to inform clients of ransomware data breach
Date: 2021-07-20T10:45:05+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: us-legal-eagles-representing-apple-ibm-and-more-take-5-months-to-inform-clients-of-ransomware-data-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/20/campbell_law_firm_data_breach/){:target="_blank" rel="noopener"}

> Those affected get free protection services – but only if their Social Security numbers were exposed Law firm Campbell Conroy & O'Neil has warned of a breach from late February which may have exposed data from the company's lengthy client list of big-name corporations including Apple and IBM.... [...]
