Title: Leaked NSO Group Data Hints at Widespread Pegasus Spyware Infections
Date: 2021-07-19T15:56:09+00:00
Author: Tom Spring
Category: Threatpost
Tags: Government;Malware;Mobile Security
Slug: leaked-nso-group-data-hints-at-widespread-pegasus-spyware-infections

[Source](https://threatpost.com/nso-group-data-pegasus/167897/){:target="_blank" rel="noopener"}

> The secretive Israeli firm was allegedly storing 50,000+ mobile phone numbers for activists, journalists, business executives and politicians -- possible targets of iPhone and Android hacking. [...]
