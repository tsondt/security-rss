Title: Biden asks Putin to crack down on Russian-based ransomware gangs
Date: 2021-07-10T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: biden-asks-putin-to-crack-down-on-russian-based-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/biden-asks-putin-to-crack-down-on-russian-based-ransomware-gangs/){:target="_blank" rel="noopener"}

> President Biden asked Russian President Putin during a phone call today to take action against ransomware groups operating within Russia's borders behind the ongoing wave of attacks impacting the United States and other countries worldwide. [...]
