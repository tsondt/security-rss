Title: Fake Kaseya VSA security update backdoors networks with Cobalt Strike
Date: 2021-07-07T08:50:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fake-kaseya-vsa-security-update-backdoors-networks-with-cobalt-strike

[Source](https://www.bleepingcomputer.com/news/security/fake-kaseya-vsa-security-update-backdoors-networks-with-cobalt-strike/){:target="_blank" rel="noopener"}

> Threat actors are trying to capitalize on the ongoing Kaseya ransomware attack crisis by targeting potential victims in a spam campaign pushing Cobalt Strike payloads disguised as Kaseya VSA security updates. [...]
