Title: Microsoft names Chinese group as source of new attack on SolarWinds
Date: 2021-07-14T03:44:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: microsoft-names-chinese-group-as-source-of-new-attack-on-solarwinds

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/14/dev_0322_solarwinds_serv_u_zero_day/){:target="_blank" rel="noopener"}

> Bad actor likes to work through insecure consumer routers and has form attacking tech companies and military targets Microsoft has attributed a new attack on SolarWinds to a group operating in China.... [...]
