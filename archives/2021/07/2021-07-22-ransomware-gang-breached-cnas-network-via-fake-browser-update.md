Title: Ransomware gang breached CNA’s network via fake browser update
Date: 2021-07-22T11:25:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-breached-cnas-network-via-fake-browser-update

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-breached-cna-s-network-via-fake-browser-update/){:target="_blank" rel="noopener"}

> Leading US insurance company CNA Financial has provided a glimpse into how Phoenix CryptoLocker operators breached its network, stole data, and deployed ransomware payloads in a ransomware attack that hit its network in March 2021. [...]
