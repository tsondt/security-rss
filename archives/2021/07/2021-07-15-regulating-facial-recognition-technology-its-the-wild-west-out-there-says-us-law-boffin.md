Title: Regulating facial recognition technology? It's the 'Wild West out there,' says US law boffin
Date: 2021-07-15T14:16:30+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: regulating-facial-recognition-technology-its-the-wild-west-out-there-says-us-law-boffin

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/15/us_facial_recognition_technology_regulation/){:target="_blank" rel="noopener"}

> House Committee on the Judiciary told until there is a proper regime, there will be 'misuse, and mistrust' of the tech The role of facial-recognition technology (FRT) was put under the microscope earlier this week after the US House Committee on the Judiciary heard evidence about how it's used by law enforcement agencies.... [...]
