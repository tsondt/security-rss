Title: Why I Love (Breaking Into) Your Security Appliances
Date: 2021-07-07T14:11:14+00:00
Author: David “moose” Wolpoff
Category: Threatpost
Tags: Hacks;InfoSec Insider;Vulnerabilities;Web Security
Slug: why-i-love-breaking-into-your-security-appliances

[Source](https://threatpost.com/breaking-into-security-appliances/167584/){:target="_blank" rel="noopener"}

> David "moose" Wolpoff, CTO at Randori, discusses security appliances and VPNs and how attackers only have to "pick one lock" to invade an enterprise through them. [...]
