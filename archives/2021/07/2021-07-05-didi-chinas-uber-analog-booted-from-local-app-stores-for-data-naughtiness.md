Title: DiDi, China’s Uber analog, booted from local app stores for data naughtiness
Date: 2021-07-05T05:14:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: didi-chinas-uber-analog-booted-from-local-app-stores-for-data-naughtiness

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/05/didi_the_ridesharing_platform_that/){:target="_blank" rel="noopener"}

> And so were two other app-makers that also happen to have listed in the USA lately Updated Chinese ride hailing app DiDi Chuxing was on Sunday removed from local app stores on on grounds that it did not comply with data protection laws. The ban came less than a week after the company’s US stock market debut.... [...]
