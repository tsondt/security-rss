Title: iPhones running latest iOS hacked to deploy NSO Group spyware
Date: 2021-07-19T05:03:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: iphones-running-latest-ios-hacked-to-deploy-nso-group-spyware

[Source](https://www.bleepingcomputer.com/news/security/iphones-running-latest-ios-hacked-to-deploy-nso-group-spyware/){:target="_blank" rel="noopener"}

> Human rights non-governmental organization Amnesty International and non-profit project Forbidden Stories revealed in a recent report that they found spyware made by Israeli surveillance firm NSO Group deployed on iPhones running Apple's latest iOS release, hacked using zero-day zero-click iMessage exploits. [...]
