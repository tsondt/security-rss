Title: Why retailers should run in our trusted cloud
Date: 2021-07-29T16:00:00+00:00
Author: Jess Leroy
Category: GCP Security
Tags: Google Cloud;Identity & Security;Consumer Packaged Goods;Retail
Slug: why-retailers-should-run-in-our-trusted-cloud

[Source](https://cloud.google.com/blog/topics/retail/how-retailers-can-better-protect-their-online-operations/){:target="_blank" rel="noopener"}

> Whether they were ready for it or not, the COVID-19 pandemic transformed many retailers into digital businesses. Retailers made huge investments into commerce technologies, customer experience tools, sales and fulfillment technology, and improving digital experiences to continue providing their goods and services to their customers. Now, more than a year into the COVID-19 pandemic, digital retail is the new normal. In fact, many retailers are planning on expanding their digital investments. However, as their digital footprint expands, so do their threats and security concerns. As a digital-focused retailer, your website is the most visible part of your attack surface. Your [...]
