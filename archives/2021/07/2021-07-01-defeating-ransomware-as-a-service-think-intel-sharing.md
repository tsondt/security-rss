Title: Defeating Ransomware-as-a-Service? Think Intel-Sharing
Date: 2021-07-01T19:09:44+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: InfoSec Insider;Malware
Slug: defeating-ransomware-as-a-service-think-intel-sharing

[Source](https://threatpost.com/ransomware-as-a-service-intel-sharing/167508/){:target="_blank" rel="noopener"}

> Aamir Lakhani, cybersecurity researcher and practitioner at FortiGuard Labs, explains the rise of RaaS and the critical role of threat intel in effectively defending against it. [...]
