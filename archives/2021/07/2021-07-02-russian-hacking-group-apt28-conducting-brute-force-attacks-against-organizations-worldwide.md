Title: Russian hacking group APT28 ‘conducting brute-force attacks’ against organizations worldwide
Date: 2021-07-02T13:20:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: russian-hacking-group-apt28-conducting-brute-force-attacks-against-organizations-worldwide

[Source](https://portswigger.net/daily-swig/russian-hacking-group-apt28-conducting-brute-force-attacks-against-organizations-worldwide){:target="_blank" rel="noopener"}

> State-sponsored actors, also known as Fancy Bear, are using Kubernetes to launch cyber-attacks [...]
