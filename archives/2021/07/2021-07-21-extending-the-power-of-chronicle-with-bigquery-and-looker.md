Title: Extending the power of Chronicle with BigQuery and Looker
Date: 2021-07-21T16:00:00+00:00
Author: Rajesh Gwalani
Category: GCP Security
Tags: Identity & Security;Data Analytics
Slug: extending-the-power-of-chronicle-with-bigquery-and-looker

[Source](https://cloud.google.com/blog/products/data-analytics/security-operations-get-more-efficient-with-google-cloud/){:target="_blank" rel="noopener"}

> Chronicle, Google Cloud’s security analytics platform, is built on Google’s infrastructure to help security teams run security operations at unprecedented speed and scale. Today, we’re excited to announce that we’re bringing more industry-leading Google technology to security teams by integrating Chronicle with Looker and BigQuery. Backed by this powerful toolset, security analysts can create brand new visual workflows that increase efficiency and improve outcomes in the Security Operations Center (SOC). New Looker visualizations in Chronicle Chronicle’s new visualizations - powered by Looker, Google Cloud’s business intelligence (BI) and analytics platform - enables a multitude of new security use cases such [...]
