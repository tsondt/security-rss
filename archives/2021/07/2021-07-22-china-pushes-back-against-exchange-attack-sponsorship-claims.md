Title: China pushes back against Exchange attack sponsorship claims
Date: 2021-07-22T07:00:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: china-pushes-back-against-exchange-attack-sponsorship-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/22/china_pushes_back_against_exchange/){:target="_blank" rel="noopener"}

> Chinese Foreign Ministry spokesperson says 53 per cent of cyber attacks on China come from the US China has very firmly pushed back against the accusation it paid contractors to attack Microsoft's Exchange Server.... [...]
