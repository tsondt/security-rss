Title: cURL developers take a second shot at fixing information disclosure flaw
Date: 2021-07-21T14:41:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: curl-developers-take-a-second-shot-at-fixing-information-disclosure-flaw

[Source](https://portswigger.net/daily-swig/curl-developers-take-a-second-shot-at-fixing-information-disclosure-flaw){:target="_blank" rel="noopener"}

> ‘We borked the fix,’ developer candidly admits [...]
