Title: Top CVEs Trending with Cybercriminals
Date: 2021-07-16T21:07:15+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Vulnerabilities
Slug: top-cves-trending-with-cybercriminals

[Source](https://threatpost.com/top-cves-trending-with-cybercriminals/167889/){:target="_blank" rel="noopener"}

> An analysis of criminal forums reveal what publicly known vulnerabilities attackers are most interested in. [...]
