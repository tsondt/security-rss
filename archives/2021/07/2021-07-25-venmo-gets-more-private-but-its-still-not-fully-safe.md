Title: Venmo gets more private—but it’s still not fully safe
Date: 2021-07-25T10:50:40+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Tech;finance;payments;privacy;Venmo
Slug: venmo-gets-more-private-but-its-still-not-fully-safe

[Source](https://arstechnica.com/?p=1782606){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Venmo, the popular mobile payment service, has redesigned its app. That's normally news you could safely ignore, but this announcement is worth a closer look. In addition to making some navigational tweaks and adding new purchase protections, the PayPal-owned platform is finally shutting down its global social feed, where the app published transactions from people around the world. It's an important step toward resolving one of the most prominent privacy issues in the world of apps, but the work isn't finished yet. Venmo’s global feed has for years been a font of voyeuristic insights into the [...]
