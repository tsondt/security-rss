Title: Cloudstar – IT provider for real estate, finance, insurance worlds – downed by ransomware
Date: 2021-07-19T19:24:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: cloudstar-it-provider-for-real-estate-finance-insurance-worlds-downed-by-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/19/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Telegram security probed, CISA boss confirmed, and more In Brief Cloud-based IT provider Cloudstar has been hit by ransomware, taking down its systems. It said it is currently negotiating with the crooks that infected its computers.... [...]
