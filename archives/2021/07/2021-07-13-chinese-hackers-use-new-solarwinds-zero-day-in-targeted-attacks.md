Title: Chinese hackers use new SolarWinds zero-day in targeted attacks
Date: 2021-07-13T19:54:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: chinese-hackers-use-new-solarwinds-zero-day-in-targeted-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/chinese-hackers-use-new-solarwinds-zero-day-in-targeted-attacks/){:target="_blank" rel="noopener"}

> China-based hackers actively target US defense and software companies using a vulnerability in the SolarWinds Serv-U FTP server. [...]
