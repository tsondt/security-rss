Title: Chinese cyberspies’ wide-scale APT campaign hits Asian govt entities
Date: 2021-07-14T08:33:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: chinese-cyberspies-wide-scale-apt-campaign-hits-asian-govt-entities

[Source](https://www.bleepingcomputer.com/news/security/chinese-cyberspies-wide-scale-apt-campaign-hits-asian-govt-entities/){:target="_blank" rel="noopener"}

> Kaspersky researchers have revealed an ongoing and large-scale advanced persistent threat (APT) campaign with hundreds of victims from Southeast Asia, including Myanmar and the Philippines government entities. [...]
