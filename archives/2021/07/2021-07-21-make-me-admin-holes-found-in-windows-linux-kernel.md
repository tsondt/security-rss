Title: Make-me-admin holes found in Windows, Linux kernel
Date: 2021-07-21T01:55:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: make-me-admin-holes-found-in-windows-linux-kernel

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/21/windows_linux_privilege_escalation/){:target="_blank" rel="noopener"}

> Patches available for priv-esc bug in the open-source software, at least Move over, PrintNightmare. Microsoft has another privilege-escalation hole in Windows that can be potentially exploited by rogue users and malware to gain admin-level powers.... [...]
