Title: Microsoft Defender for Identity now detects PrintNightmare attacks
Date: 2021-07-16T08:56:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-for-identity-now-detects-printnightmare-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-for-identity-now-detects-printnightmare-attacks/){:target="_blank" rel="noopener"}

> Microsoft has added support for PrintNightmare exploitation detection to Microsoft Defender for Identity to help Security Operations teams detect attackers' attempts to abuse this critical vulnerability. [...]
