Title: How NSO became the company whose software can spy on the world
Date: 2021-07-23T11:00:52+00:00
Author: Stephanie Kirchgaessner
Category: The Guardian
Tags: Surveillance;Israel;Malware;Technology;Espionage;Privacy;Data protection;Data and computer security
Slug: how-nso-became-the-company-whose-software-can-spy-on-the-world

[Source](https://www.theguardian.com/news/2021/jul/23/how-nso-became-the-company-whose-software-can-spy-on-the-world){:target="_blank" rel="noopener"}

> The Pegasus project has raised new concerns about the Israeli firm, which is a world leader in the niche surveillance market In 2019, when NSO Group was facing intense scrutiny, new investors in the Israeli surveillance company were on a PR offensive to reassure human rights groups. In an exchange of public letters in 2019, they told Amnesty International and other activists that they would do “whatever is necessary” to ensure NSO’s weapons-grade software would only be used to fight crime and terrorism. What is in the data leak? The Pegasus project is a collaborative journalistic investigation into the NSO [...]
