Title: Trickbot updates its VNC module for high-value targets
Date: 2021-07-14T03:32:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: trickbot-updates-its-vnc-module-for-high-value-targets

[Source](https://www.bleepingcomputer.com/news/security/trickbot-updates-its-vnc-module-for-high-value-targets/){:target="_blank" rel="noopener"}

> The Trickbot botnet malware that often distributes various ransomware strains, continues to be the most prevalent threat as its developers update the VNC module used for remote control over infected systems. [...]
