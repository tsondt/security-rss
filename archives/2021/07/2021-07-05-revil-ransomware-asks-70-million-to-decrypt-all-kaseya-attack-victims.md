Title: REvil ransomware asks $70 million to decrypt all Kaseya attack victims
Date: 2021-07-05T04:59:25-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: revil-ransomware-asks-70-million-to-decrypt-all-kaseya-attack-victims

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-asks-70-million-to-decrypt-all-kaseya-attack-victims/){:target="_blank" rel="noopener"}

> REvil ransomware has set a price for decrypting all systems locked during the Kaseya supply-chain attack. The gang wants $70 million in Bitcoin for the tool that allows all affected businesses to recover their files. [...]
