Title: Northern Train's ticketing system out to lunch as ransomware attack shuts down servers
Date: 2021-07-20T12:33:04+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: northern-trains-ticketing-system-out-to-lunch-as-ransomware-attack-shuts-down-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/20/northern_trains_ticketing_system/){:target="_blank" rel="noopener"}

> £17m on shiny new Flowbird touchscreen kiosks well spent, apparently Publicly owned rail operator Northern Trains has an excuse somewhat more technical than "leaves on the line" for its latest service disruption: a ransomware attack that has left its self-service ticketing booths out for the count.... [...]
