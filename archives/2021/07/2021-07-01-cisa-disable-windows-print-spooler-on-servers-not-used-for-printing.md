Title: CISA: Disable Windows Print Spooler on servers not used for printing
Date: 2021-07-01T12:09:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: cisa-disable-windows-print-spooler-on-servers-not-used-for-printing

[Source](https://www.bleepingcomputer.com/news/security/cisa-disable-windows-print-spooler-on-servers-not-used-for-printing/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has issued a notification regarding the critical PrintNightmare zero-day vulnerability and advises admins to disable the Windows Print Spooler service on servers not used for printing. [...]
