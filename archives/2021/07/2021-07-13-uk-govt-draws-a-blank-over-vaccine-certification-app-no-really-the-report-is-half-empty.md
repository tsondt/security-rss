Title: UK govt draws a blank over vaccine certification app – no really, the report is half-empty
Date: 2021-07-13T17:45:03+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: uk-govt-draws-a-blank-over-vaccine-certification-app-no-really-the-report-is-half-empty

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/13/uk_vaccine_certificate_report/){:target="_blank" rel="noopener"}

> Maybe it's that infamous brain fog The UK government has underscored its attention to detail on the issue of COVID-19 vaccine passports by publishing a report, half of which is made up of pages with no information.... [...]
