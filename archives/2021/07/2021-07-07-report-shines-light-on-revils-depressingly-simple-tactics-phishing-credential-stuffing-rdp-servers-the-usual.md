Title: Report shines light on REvil's depressingly simple tactics: Phishing, credential-stuffing RDP servers... the usual
Date: 2021-07-07T15:00:10+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: report-shines-light-on-revils-depressingly-simple-tactics-phishing-credential-stuffing-rdp-servers-the-usual

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/07/revil_tactics_and_multimillion_dollar/){:target="_blank" rel="noopener"}

> And those multimillion-dollar payouts Palo Alto Networks' global threat intelligence team, Unit 42, has detailed the tactics ransomware group REvil has employed to great impact so far this year – along with an estimation of the multimillion-dollar payouts it's receiving.... [...]
