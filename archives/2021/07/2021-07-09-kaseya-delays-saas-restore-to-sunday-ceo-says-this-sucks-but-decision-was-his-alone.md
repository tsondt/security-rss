Title: Kaseya delays SaaS restore to Sunday, CEO says ‘this sucks’ but decision was his alone
Date: 2021-07-09T04:24:58+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: kaseya-delays-saas-restore-to-sunday-ceo-says-this-sucks-but-decision-was-his-alone

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/09/kaseya_saas_restoration_july_11/){:target="_blank" rel="noopener"}

> Promises “exponentially more secure” product and cash assistance for customers Beleaguered IT management software vendor Kaseya has delayed the restoration of its SaaS services until Sunday, July 11th.... [...]
