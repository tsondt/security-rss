Title: Podcast: IoT Piranhas Are Swarming Industrial Controls
Date: 2021-07-26T22:09:11+00:00
Author: Threatpost
Category: Threatpost
Tags: Critical Infrastructure;IoT;Malware;Podcasts;Vulnerabilities;Web Security
Slug: podcast-iot-piranhas-are-swarming-industrial-controls

[Source](https://threatpost.com/podcast-iot-industrial-controls/168078/){:target="_blank" rel="noopener"}

> Enormous botnets of IoT devices are going after decades-old legacy systems that are rife in systems that control crucial infrastructure. [...]
