Title: Bogus Kaseya VSA patches circulate, booby-trapped with remote-access tool
Date: 2021-07-07T23:03:31+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: bogus-kaseya-vsa-patches-circulate-booby-trapped-with-remote-access-tool

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/07/kaseya_malware_patches_/){:target="_blank" rel="noopener"}

> Phishing campaign aims to capitalize on slow fix deployment, it seems This month's Kaseya VSA ransomware attack took a turn for the worse on Wednesday with word that miscreants have launched a phishing campaign to ensnare victims with a remote-control tool disguised as a VSA update.... [...]
