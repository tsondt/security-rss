Title: Saudi Aramco data breach sees 1 TB stolen data for sale
Date: 2021-07-19T08:02:33-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: saudi-aramco-data-breach-sees-1-tb-stolen-data-for-sale

[Source](https://www.bleepingcomputer.com/news/security/saudi-aramco-data-breach-sees-1-tb-stolen-data-for-sale/){:target="_blank" rel="noopener"}

> Attackers have stolen 1 TB of proprietary data belonging to Saudi Aramco and are offering it for sale on the darknet. The Saudi Arabian Oil Company, better known as Saudi Aramco, is one of the largest public petroleum and natural gas companies in the world. The sales price, albeit negotiable, is set at $5 million. [...]
