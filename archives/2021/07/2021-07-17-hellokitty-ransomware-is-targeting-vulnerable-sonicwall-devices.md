Title: HelloKitty ransomware is targeting vulnerable SonicWall devices
Date: 2021-07-17T11:44:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hellokitty-ransomware-is-targeting-vulnerable-sonicwall-devices

[Source](https://www.bleepingcomputer.com/news/security/hellokitty-ransomware-is-targeting-vulnerable-sonicwall-devices/){:target="_blank" rel="noopener"}

> CISA is warning of threat actors targeting "a known, previously patched, vulnerability" found in SonicWall Secure Mobile Access (SMA) 100 series and Secure Remote Access (SRA) products with end-of-life firmware. [...]
