Title: Tens of thousands scammed using fake Android cryptomining apps
Date: 2021-07-07T07:44:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: tens-of-thousands-scammed-using-fake-android-cryptomining-apps

[Source](https://www.bleepingcomputer.com/news/security/tens-of-thousands-scammed-using-fake-android-cryptomining-apps/){:target="_blank" rel="noopener"}

> Scammers tricked at least 93,000 people into buying fake Android cryptocurrency mining applications, as revealed by researchers from California-based cybersecurity firm Lookout. [...]
