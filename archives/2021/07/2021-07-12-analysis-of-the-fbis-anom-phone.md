Title: Analysis of the FBI’s Anom Phone
Date: 2021-07-12T16:58:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;cell phones;encryption;FBI;law enforcement
Slug: analysis-of-the-fbis-anom-phone

[Source](https://www.schneier.com/blog/archives/2021/07/analysis-of-the-fbis-anom-phone.html){:target="_blank" rel="noopener"}

> Motherboard got its hands on one of those Anom phones that were really FBI honeypots. The details are interesting. [...]
