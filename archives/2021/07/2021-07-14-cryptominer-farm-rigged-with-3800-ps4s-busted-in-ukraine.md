Title: Cryptominer Farm Rigged with 3,800 PS4s Busted in Ukraine
Date: 2021-07-14T19:37:36+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware
Slug: cryptominer-farm-rigged-with-3800-ps4s-busted-in-ukraine

[Source](https://threatpost.com/cryptominer-farm-ps4s-busted-ukraine/167809/){:target="_blank" rel="noopener"}

> Ukrainian cops seize PlayStation 4 consoles, graphics cards, processors and more in cryptomining sting involving alleged electricity theft. [...]
