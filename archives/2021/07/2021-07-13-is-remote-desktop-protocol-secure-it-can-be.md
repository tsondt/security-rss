Title: Is Remote Desktop Protocol Secure? It Can Be
Date: 2021-07-13T14:50:45+00:00
Author: Matt Dunn
Category: Threatpost
Tags: Hacks;InfoSec Insider;Web Security
Slug: is-remote-desktop-protocol-secure-it-can-be

[Source](https://threatpost.com/remote-desktop-protocol-secure/167719/){:target="_blank" rel="noopener"}

> Matt Dunn, associate managing director in Kroll's Cyber Risk practice, discusses options for securing RDP, which differ significantly in terms of effectiveness. [...]
