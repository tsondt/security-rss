Title: No More Ransom saves almost €1 billion in ransomware payments in 5 years
Date: 2021-07-26T09:24:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: no-more-ransom-saves-almost-eur1-billion-in-ransomware-payments-in-5-years

[Source](https://www.bleepingcomputer.com/news/security/no-more-ransom-saves-almost-1-billion-in-ransomware-payments-in-5-years/){:target="_blank" rel="noopener"}

> The No More Ransom project celebrates its fifth anniversary today after helping over six million ransomware victims recover their files and saving them almost €1 billion in ransomware payments. [...]
