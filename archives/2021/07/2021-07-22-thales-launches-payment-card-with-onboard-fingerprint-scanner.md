Title: Thales launches payment card with onboard fingerprint scanner
Date: 2021-07-22T08:30:11+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: thales-launches-payment-card-with-onboard-fingerprint-scanner

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/22/thales_fingerprint_payment/){:target="_blank" rel="noopener"}

> Would you like to pay with a poke? Thales has announced what it claims is the "world's first" payment card to include an onboard fingerprint sensor, promising improved security and usability – and an end to contactless payment limits.... [...]
