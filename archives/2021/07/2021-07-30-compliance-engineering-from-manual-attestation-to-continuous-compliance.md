Title: Compliance Engineering - From manual attestation to continuous compliance
Date: 2021-07-30T16:00:00+00:00
Author: Florian Graf
Category: GCP Security
Tags: Compliance;Google Cloud;Identity & Security
Slug: compliance-engineering-from-manual-attestation-to-continuous-compliance

[Source](https://cloud.google.com/blog/products/identity-security/how-banks-can-engineer-compliance-into-their-cloud-systems/){:target="_blank" rel="noopener"}

> Risk Management and Compliance is as important in the cloud as it is in conventional on-premises environments. To help organizations in regulated industries meet their compliance requirements, Google Cloud offers automated capabilities that ensure the effectiveness of productionalization processes. Continuous compliance in the banking industry Banks have a formidable responsibility in managing the world’s wealth, and are therefore champions in diligently managing risk. Financial regulators in turn publish banking regulations to ensure banks assess and manage their risks accurately. Since banks are heavily reliant on information technology (IT), these regulations also cover the use of IT within banks. Regulated industries [...]
