Title: 16-year-old bug in printer software gives hackers admin rights
Date: 2021-07-20T07:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: 16-year-old-bug-in-printer-software-gives-hackers-admin-rights

[Source](https://www.bleepingcomputer.com/news/security/16-year-old-bug-in-printer-software-gives-hackers-admin-rights/){:target="_blank" rel="noopener"}

> A 16-year-old security vulnerability found in HP, Xerox, and Samsung printers drivers allows attackers to gain admin rights on systems using the vulnerable driver software. [...]
