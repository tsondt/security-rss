Title: FBI warns cryptocurrency owners, exchanges of ongoing attacks
Date: 2021-07-09T14:04:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: fbi-warns-cryptocurrency-owners-exchanges-of-ongoing-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-cryptocurrency-owners-exchanges-of-ongoing-attacks/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warns cryptocurrency owners, exchanges, and third-party payment platforms of threat actors actively targeting virtual assets in attacks that can lead to significant financial losses. [...]
