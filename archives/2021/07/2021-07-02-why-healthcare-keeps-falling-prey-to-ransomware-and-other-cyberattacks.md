Title: Why Healthcare Keeps Falling Prey to Ransomware and Other Cyberattacks
Date: 2021-07-02T16:07:17+00:00
Author: Nate Warfield
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;InfoSec Insider;IoT;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: why-healthcare-keeps-falling-prey-to-ransomware-and-other-cyberattacks

[Source](https://threatpost.com/healthcare-prey-ransomware-cyberattacks/167525/){:target="_blank" rel="noopener"}

> Nate Warfield, CTO of Prevailion and former Microsoft security researcher, discusses the many security challenges and failings plaguing this industry. [...]
