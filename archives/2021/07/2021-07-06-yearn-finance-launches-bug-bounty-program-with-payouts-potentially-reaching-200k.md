Title: Yearn Finance launches bug bounty program with payouts potentially reaching $200k
Date: 2021-07-06T15:25:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: yearn-finance-launches-bug-bounty-program-with-payouts-potentially-reaching-200k

[Source](https://portswigger.net/daily-swig/yearn-finance-launches-bug-bounty-program-with-payouts-potentially-reaching-200k){:target="_blank" rel="noopener"}

> DeFi project aims to bolster security following $11m hack in February [...]
