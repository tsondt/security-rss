Title: It takes intuition and skill to find hidden evidence and hunt for elusive threats
Date: 2021-07-27T07:28:04+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: it-takes-intuition-and-skill-to-find-hidden-evidence-and-hunt-for-elusive-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/27/sans_berlin_october21/){:target="_blank" rel="noopener"}

> Try the SANS DFIR-ence in Berlin this October Promo Whether it’s hunting for threats, tracking down security breaches, or gathering evidence, intuition helps though a thorough grounding in the latest techniques and tools for the platform in question is essential.... [...]
