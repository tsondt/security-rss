Title: Microsoft shares workarounds for new Windows 10 zero-day bug
Date: 2021-07-21T04:32:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-shares-workarounds-for-new-windows-10-zero-day-bug

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-shares-workarounds-for-new-windows-10-zero-day-bug/){:target="_blank" rel="noopener"}

> Microsoft has shared workarounds for a Windows 10 zero-day vulnerability that can let attackers gain admin rights on vulnerable systems and execute arbitrary code with SYSTEM privileges. [...]
