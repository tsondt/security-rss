Title: How to monitor and track failed logins for your AWS Managed Microsoft AD
Date: 2021-07-02T17:47:59+00:00
Author: Tekena Orugbani
Category: AWS Security
Tags: AWS Directory Service;Intermediate (200);Security, Identity, & Compliance;Identity management;Security Blog
Slug: how-to-monitor-and-track-failed-logins-for-your-aws-managed-microsoft-ad

[Source](https://aws.amazon.com/blogs/security/how-to-monitor-and-track-failed-logins-for-your-aws-managed-microsoft-ad/){:target="_blank" rel="noopener"}

> AWS Directory Service for Microsoft Active Directory provides customers with the ability to review security logs on their AWS Managed Microsoft AD domain controllers by either using a domain management Amazon Elastic Compute Cloud (Amazon EC2) instance or by forwarding domain controller security event logs to Amazon CloudWatch Logs. You can further improve visibility by [...]
