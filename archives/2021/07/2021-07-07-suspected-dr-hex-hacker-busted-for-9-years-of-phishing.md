Title: Suspected ‘Dr HeX’ Hacker Busted for 9 Years of Phishing
Date: 2021-07-07T16:23:31+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: suspected-dr-hex-hacker-busted-for-9-years-of-phishing

[Source](https://threatpost.com/dr-hex-hacker-busted-phishing/167597/){:target="_blank" rel="noopener"}

> The unnamed suspect allegedly helped to develop carding and phishing kits with the aim of stealing customers' bank-card data. [...]
