Title: Kaseya was fixing zero-day just as REvil ransomware sprung their attack
Date: 2021-07-04T11:31:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: kaseya-was-fixing-zero-day-just-as-revil-ransomware-sprung-their-attack

[Source](https://www.bleepingcomputer.com/news/security/kaseya-was-fixing-zero-day-just-as-revil-ransomware-sprung-their-attack/){:target="_blank" rel="noopener"}

> The zero-day vulnerability used to breach on-premise Kaseya VSA servers was in the process of being fixed, just as the REvil ransomware gang used it to perform their massive Friday attack. [...]
