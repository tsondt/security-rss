Title: US insurance giant AJG reports data breach after ransomware attack
Date: 2021-07-02T08:39:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-insurance-giant-ajg-reports-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/us-insurance-giant-ajg-reports-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Arthur J. Gallagher (AJG), a US-based global insurance brokerage and risk management firm, is mailing breach notification letters to potentially impacted individuals following a ransomware attack that hit its systems in late September. [...]
