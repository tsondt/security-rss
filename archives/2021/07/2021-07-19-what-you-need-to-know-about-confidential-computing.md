Title: What you need to know about Confidential Computing
Date: 2021-07-19T16:00:00+00:00
Author: Nelly Porter
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: what-you-need-to-know-about-confidential-computing

[Source](https://cloud.google.com/blog/products/identity-security/confidential-computing-data-encryption-during-processing/){:target="_blank" rel="noopener"}

> This blog includes content from Episode One "Confidentially Speaking” of our Cloud Security Podcast, hosted by Anton Chuvakin (Head of Solutions Strategy) and Timothy Peacock (Product Manager). You should listen to the whole conversation for more insights and deeper context. Related Article Read Article We all deal with a lot of sensitive data and today, enterprises must entrust all of this sensitive data to their cloud providers. With on-premises systems, companies used to have a very clear idea about who could access data and who was responsible for protecting that data. Now, data lives in many different places—on-premises, at the [...]
