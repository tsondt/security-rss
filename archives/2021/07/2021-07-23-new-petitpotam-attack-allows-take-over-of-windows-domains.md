Title: New PetitPotam attack allows take over of Windows domains
Date: 2021-07-23T16:54:03-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: new-petitpotam-attack-allows-take-over-of-windows-domains

[Source](https://www.bleepingcomputer.com/news/microsoft/new-petitpotam-attack-allows-take-over-of-windows-domains/){:target="_blank" rel="noopener"}

> A new NTLM relay attack called PetitPotam has been discovered that allows threat actors to take over a domain controller, and thus an entire Windows domain. [...]
