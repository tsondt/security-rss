Title: Research exposes vulnerabilities in IP camera firmware used by multiple vendors
Date: 2021-07-12T15:14:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: research-exposes-vulnerabilities-in-ip-camera-firmware-used-by-multiple-vendors

[Source](https://portswigger.net/daily-swig/research-exposes-vulnerabilities-in-ip-camera-firmware-used-by-multiple-vendors){:target="_blank" rel="noopener"}

> Flawed UDP code bundled in CCTV devices from Geutebrück, VCA, and Sprinx Technologies [...]
