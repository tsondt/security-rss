Title: How to integrate third-party IdP using developer authenticated identities
Date: 2021-07-01T18:27:52+00:00
Author: Andrew Lee
Category: AWS Security
Tags: Amazon Cognito;Intermediate (200);Security, Identity, & Compliance;Security Blog
Slug: how-to-integrate-third-party-idp-using-developer-authenticated-identities

[Source](https://aws.amazon.com/blogs/security/how-to-integrate-third-party-idp-using-developer-authenticated-identities/){:target="_blank" rel="noopener"}

> Amazon Cognito identity pools enable you to create and manage unique identifiers for your users and provide temporary, limited-privilege credentials to your application to access AWS resources. Currently, there are several out of the box external identity providers (IdPs) to integrate with Amazon Cognito identity pools, including Facebook, Google, and Apple. If your application’s primary [...]
