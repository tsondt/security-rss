Title: Advancing our trusted cloud with engineered-in, invisible security
Date: 2021-07-20T13:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: advancing-our-trusted-cloud-with-engineered-in-invisible-security

[Source](https://cloud.google.com/blog/products/identity-security/security-summit-2021-google-expands-trusted-cloud/){:target="_blank" rel="noopener"}

> Security has risen to the forefront of concerns for enterprises and governments around the globe. Attacks ripping across the software supply chain, zero-day issues in widely used email services, and ransomware attacks on critical infrastructure industries all provide evidence that adversaries are getting bolder, more successful and more prevalent. Despite these increasing risks, many security products seem to focus on solving problems created by other security products, rather than the root causes of the issues. But confidence and security can’t be achieved simply by buying another new product, or taking the same approaches while referring to them as the latest [...]
