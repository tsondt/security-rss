Title: Compsci student walks off with $50,000 after bug bounty report blows gaping hole in Shopify software repos
Date: 2021-07-27T12:14:12+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: compsci-student-walks-off-with-50000-after-bug-bounty-report-blows-gaping-hole-in-shopify-software-repos

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/27/shopify_bug_bounty_payout/){:target="_blank" rel="noopener"}

> First-timer wins maximum payout through HackerOne programme Shopify has forked out $50,000 (£36,150) in a bug bounty payment to computer science student Augusto Zanellato following the discovery of a publicly available access token which gave world+dog read-and-write access to the company's source code repositories.... [...]
