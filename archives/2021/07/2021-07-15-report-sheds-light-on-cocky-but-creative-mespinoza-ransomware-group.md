Title: Report sheds light on 'cocky' but 'creative' Mespinoza ransomware group
Date: 2021-07-15T10:00:04+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: report-sheds-light-on-cocky-but-creative-mespinoza-ransomware-group

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/15/mespinoza_ransomware_profile/){:target="_blank" rel="noopener"}

> Palo Alto's Unit 42 rejects claims group has shifted to ransomware-as-a-service Palo Alto Networks' Unit 42 has probed the methods and tactics of the Mespinoza ransomware group, finding its messaging "cocky" and its tools blessed with "creative names" – but turned up no evidence to suggest the group has shifted to ransomware-as-a-service.... [...]
