Title: OSPAR 2021 report now available with 127 services in scope
Date: 2021-07-16T17:06:21+00:00
Author: Clara Lim
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Banking Regulations;Financial Services;OSPAR;Security Blog;Singapore
Slug: ospar-2021-report-now-available-with-127-services-in-scope

[Source](https://aws.amazon.com/blogs/security/ospar-2021-report-now-available-with-127-services-in-scope/){:target="_blank" rel="noopener"}

> We are excited to announce the completion of the third Outsourced Service Provider Audit Report (OSPAR) audit cycle on July 1, 2021. The latest OSPAR certification includes the addition of 19 new services in scope, bringing the total number of services to 127 in the Asia Pacific (Singapore) Region. You can download our latest OSPAR [...]
