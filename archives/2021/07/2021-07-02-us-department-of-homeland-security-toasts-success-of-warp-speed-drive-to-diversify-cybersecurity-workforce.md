Title: US Department of Homeland Security toasts success of warp-speed drive to diversify cybersecurity workforce
Date: 2021-07-02T15:22:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-department-of-homeland-security-toasts-success-of-warp-speed-drive-to-diversify-cybersecurity-workforce

[Source](https://portswigger.net/daily-swig/us-department-of-homeland-security-toasts-success-of-warp-speed-drive-to-diversify-cybersecurity-workforce){:target="_blank" rel="noopener"}

> Federal agency has filled or found candidates for 800 positions in just 60 days [...]
