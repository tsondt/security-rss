Title: Cyberattack on Moldova's Court of Accounts destroyed public audits
Date: 2021-07-16T16:53:58-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: cyberattack-on-moldovas-court-of-accounts-destroyed-public-audits

[Source](https://www.bleepingcomputer.com/news/security/cyberattack-on-moldovas-court-of-accounts-destroyed-public-audits/){:target="_blank" rel="noopener"}

> ​Moldova's "Court of Accounts" has suffered a cyberattack leading to the agency's public databases and audits being destroyed. [...]
