Title: Update now: TIBCO Data Virtualization software vulnerable to RCE via third-party flaws, claims researcher
Date: 2021-07-20T14:58:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: update-now-tibco-data-virtualization-software-vulnerable-to-rce-via-third-party-flaws-claims-researcher

[Source](https://portswigger.net/daily-swig/update-now-tibco-data-virtualization-software-vulnerable-to-rce-via-third-party-flaws-claims-researcher){:target="_blank" rel="noopener"}

> Vulnerabilities affect versions 8.3 and below, but vendor inadvertently addressed exploit in 8.4 [...]
