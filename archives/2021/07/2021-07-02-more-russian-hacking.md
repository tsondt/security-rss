Title: More Russian Hacking
Date: 2021-07-02T11:26:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;hacking;malware;Microsoft;Russia
Slug: more-russian-hacking

[Source](https://www.schneier.com/blog/archives/2021/07/more-russian-hacking.html){:target="_blank" rel="noopener"}

> Two reports this week. The first is from Microsoft, which wrote : As part of our investigation into this ongoing activity, we also detected information-stealing malware on a machine belonging to one of our customer support agents with access to basic account information for a small number of our customers. The actor used this information in some cases to launch highly-targeted attacks as part of their broader campaign. The second is from the NSA, CISA, FBI, and the UK’s NCSC, which wrote that the GRU is continuing to conduct brute-force password guessing attacks around the world, and is in some [...]
