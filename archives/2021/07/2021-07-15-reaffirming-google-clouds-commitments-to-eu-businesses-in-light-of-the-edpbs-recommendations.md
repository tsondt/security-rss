Title: Reaffirming Google Cloud’s commitments to EU businesses in light of the EDPB’s Recommendations
Date: 2021-07-15T13:00:00+00:00
Author: Nathaly Rey
Category: GCP Security
Tags: Identity & Security;Google Cloud;Compliance
Slug: reaffirming-google-clouds-commitments-to-eu-businesses-in-light-of-the-edpbs-recommendations

[Source](https://cloud.google.com/blog/products/compliance/how-google-cloud-helps-customers-stay-current-with-gdpr/){:target="_blank" rel="noopener"}

> From retail companies to auto manufacturers and financial services institutions, organizations across Europe rely on our cloud services to run their businesses. We are committed to helping our customers meet stringent data protection requirements by offering industry-leading technical controls, contractual commitments, and continued transparency to support their risk assessments and compliance needs. On June 21, 2021, the European Data Protection Board (EDPB) published its final Recommendations on supplementary measures following the Court of Justice of the European Union's ruling, which invalidated the EU-US Privacy Shield Framework and upheld the validity of the EU Standard Contractual Clauses (SCCs). The EDPB’s guidance [...]
