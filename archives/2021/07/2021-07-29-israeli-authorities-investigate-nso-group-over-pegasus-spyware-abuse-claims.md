Title: Israeli authorities investigate NSO Group over Pegasus spyware abuse claims
Date: 2021-07-29T07:00:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: israeli-authorities-investigate-nso-group-over-pegasus-spyware-abuse-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/29/israel_probes_nso_group/){:target="_blank" rel="noopener"}

> Reason for probe unknown, but CEO claims it will vindicate company's claims Israel's Ministry of Defense says the nation's government has visited spyware-for-governments developer NSO Group to investigate allegations its wares have been widely – and perhaps willingly – misused.... [...]
