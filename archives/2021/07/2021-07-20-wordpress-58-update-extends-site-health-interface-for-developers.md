Title: WordPress 5.8 update extends Site Health interface for developers
Date: 2021-07-20T15:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: wordpress-58-update-extends-site-health-interface-for-developers

[Source](https://portswigger.net/daily-swig/wordpress-5-8-update-extends-site-health-interface-for-developers){:target="_blank" rel="noopener"}

> Second major release of the year expands usability of PHP security tool [...]
