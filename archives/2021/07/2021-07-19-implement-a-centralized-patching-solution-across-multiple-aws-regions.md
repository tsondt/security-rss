Title: Implement a centralized patching solution across multiple AWS Regions
Date: 2021-07-19T17:40:12+00:00
Author: Akash Kumar
Category: AWS Security
Tags: AWS Systems Manager;Intermediate (200);Security, Identity, & Compliance;AWS System Manager;Hybrid environment patching;infrastructure security;Security Blog;SSM Free Tier
Slug: implement-a-centralized-patching-solution-across-multiple-aws-regions

[Source](https://aws.amazon.com/blogs/security/implement-a-centralized-patching-solution-across-multiple-aws-regions/){:target="_blank" rel="noopener"}

> In this post, I show you how to implement a centralized patching solution across Amazon Web Services (AWS) Regions by using AWS Systems Manager in your AWS account. This helps you to initiate, track, and manage your patching events across AWS Regions from one centralized place. Enterprises with large, multi-Region hybrid environments must determine whether [...]
