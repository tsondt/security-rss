Title: Grief ransomware operation is DoppelPaymer rebranded
Date: 2021-07-29T02:20:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: grief-ransomware-operation-is-doppelpaymer-rebranded

[Source](https://www.bleepingcomputer.com/news/security/grief-ransomware-operation-is-doppelpaymer-rebranded/){:target="_blank" rel="noopener"}

> After a period of little to no activity, the DoppelPaymer ransomware operation has made a rebranding move, now going by the name Grief (a.k.a. Pay or Grief). [...]
