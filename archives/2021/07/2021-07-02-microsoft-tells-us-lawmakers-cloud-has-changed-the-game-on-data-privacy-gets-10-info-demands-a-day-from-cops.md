Title: Microsoft tells US lawmakers cloud has changed the game on data privacy, gets 10 info demands <i>a day</i> from cops
Date: 2021-07-02T18:15:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: microsoft-tells-us-lawmakers-cloud-has-changed-the-game-on-data-privacy-gets-10-info-demands-a-day-from-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/02/us_government_cloud/){:target="_blank" rel="noopener"}

> Technology has outpaced laws, says House committee chairman The US House Committee on the Judiciary met on Wednesday to hear testimony on the government's practice of secretly subpoenaing cloud service providers, and Microsoft was happy to oblige.... [...]
