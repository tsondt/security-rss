Title: Cisco BPA, WSA Bugs Allow Remote Cyberattacks
Date: 2021-07-09T17:31:26+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: cisco-bpa-wsa-bugs-allow-remote-cyberattacks

[Source](https://threatpost.com/cisco-bpa-wsa-bugs-cyberattacks/167654/){:target="_blank" rel="noopener"}

> The high-severity security vulnerabilities allow elevation of privileges, leading to data theft and more. [...]
