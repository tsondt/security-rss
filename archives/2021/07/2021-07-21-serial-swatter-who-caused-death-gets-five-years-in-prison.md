Title: Serial Swatter Who Caused Death Gets Five Years in Prison
Date: 2021-07-21T19:59:03+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;SIM Swapping;Ann Billings;Grindr;Jason Scott;Judge Mark Norris;Mary Frances Herring;QWIKMail;Shane Glass;Shane Sonderman;Sparkware;Valerie Dozono
Slug: serial-swatter-who-caused-death-gets-five-years-in-prison

[Source](https://krebsonsecurity.com/2021/07/serial-swatter-who-caused-death-gets-five-years-in-prison/){:target="_blank" rel="noopener"}

> A 18-year-old Tennessee man who helped set in motion a fraudulent distress call to police that led to the death of a 60-year-old grandfather in 2020 was sentenced to 60 months in prison today. 60-year-old Mark Herring died of a heart attack after police surrounded his home in response to a swatting attack. Shane Sonderman, of Lauderdale County, Tenn. admitted to conspiring with a group of criminals that’s been “swatting” and harassing people for months in a bid to coerce targets into giving up their valuable Twitter and Instagram usernames. At Sonderman’s sentencing hearing today, prosecutors told the court the [...]
