Title: Google Chrome will add HTTPS-First Mode to keep your data safe
Date: 2021-07-14T13:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: google-chrome-will-add-https-first-mode-to-keep-your-data-safe

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-will-add-https-first-mode-to-keep-your-data-safe/){:target="_blank" rel="noopener"}

> Google will add an HTTPS-First Mode to the Chrome web browser to block attackers from intercepting or eavesdropping on users' web traffic. [...]
