Title: Loyalty management tech firm Antavo launches bug bounty program
Date: 2021-07-22T11:45:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: loyalty-management-tech-firm-antavo-launches-bug-bounty-program

[Source](https://portswigger.net/daily-swig/loyalty-management-tech-firm-antavo-launches-bug-bounty-program){:target="_blank" rel="noopener"}

> London-headquartered company seeks ‘outside perspective’ on AppSec [...]
