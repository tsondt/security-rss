Title: Akamai DNS global outage takes down major websites, online services
Date: 2021-07-22T12:39:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: akamai-dns-global-outage-takes-down-major-websites-online-services

[Source](https://www.bleepingcomputer.com/news/security/akamai-dns-global-outage-takes-down-major-websites-online-services/){:target="_blank" rel="noopener"}

> Akamai is investigating an ongoing outage affecting many major websites and online services including Steam, the PlayStation Network, Newegg, AWS, Amazon, Google, and Salesforce. [...]
