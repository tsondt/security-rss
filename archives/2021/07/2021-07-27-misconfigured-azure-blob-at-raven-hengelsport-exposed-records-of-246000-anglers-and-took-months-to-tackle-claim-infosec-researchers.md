Title: Misconfigured Azure Blob at Raven Hengelsport exposed records of 246,000 anglers – and took months to tackle, claim infosec researchers
Date: 2021-07-27T20:49:10+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: misconfigured-azure-blob-at-raven-hengelsport-exposed-records-of-246000-anglers-and-took-months-to-tackle-claim-infosec-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/27/azure_blob_raven_hengelsport/){:target="_blank" rel="noopener"}

> 18GB of Dutch fishing supplier's data left in unsecured server Dutch fishing supply specialist Raven Hengelsport left details of around 246,000 customers visible to anyone on a misconfigured Microsoft Azure cloud server for months.... [...]
