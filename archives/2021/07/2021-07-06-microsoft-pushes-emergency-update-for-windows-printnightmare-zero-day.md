Title: Microsoft pushes emergency update for Windows PrintNightmare zero-day
Date: 2021-07-06T17:31:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-pushes-emergency-update-for-windows-printnightmare-zero-day

[Source](https://www.bleepingcomputer.com/news/security/microsoft-pushes-emergency-update-for-windows-printnightmare-zero-day/){:target="_blank" rel="noopener"}

> Microsoft has released the KB5004945 emergency security update to address the actively exploited PrintNightmare zero-day vulnerability in the Windows Print Spooler service impacting all Windows versions. However, the patch is incomplete and the vulnerability can still be locally exploited to gain SYSTEM privileges. [...]
