Title: Candiru: Another Cyberweapons Arms Manufacturer
Date: 2021-07-19T15:54:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberespionage;cyberweapons;Israel;privilege escalation;spyware
Slug: candiru-another-cyberweapons-arms-manufacturer

[Source](https://www.schneier.com/blog/archives/2021/07/candiru-another-cyberweapons-arms-manufacturer.html){:target="_blank" rel="noopener"}

> Citizen Lab has identified yet another Israeli company that sells spyware to governments around the world: Candiru. From the report : Summary: Candiru is a secretive Israel-based company that sells spyware exclusively to governments. Reportedly, their spyware can infect and monitor iPhones, Androids, Macs, PCs, and cloud accounts. Using Internet scanning we identified more than 750 websites linked to Candiru’s spyware infrastructure. We found many domains masquerading as advocacy organizations such as Amnesty International, the Black Lives Matter movement, as well as media companies, and other civil-society themed entities. We identified a politically active victim in Western Europe and recovered [...]
