Title: Configure SAML single sign-on for Kibana with AD FS on Amazon Elasticsearch Service
Date: 2021-07-09T20:10:35+00:00
Author: Sajeev Attiyil Bhaskaran
Category: AWS Security
Tags: Amazon Elasticsearch Service;Intermediate (200);Security, Identity, & Compliance;Active Directory;Kibana;SAML;Security Blog;Single sign-on
Slug: configure-saml-single-sign-on-for-kibana-with-ad-fs-on-amazon-elasticsearch-service

[Source](https://aws.amazon.com/blogs/security/configure-saml-single-sign-on-for-kibana-with-ad-fs-on-amazon-elasticsearch-service/){:target="_blank" rel="noopener"}

> It’s a common use case for customers to integrate identity providers (IdPs) with Amazon Elasticsearch Service (Amazon ES) to achieve single sign-on (SSO) with Kibana. This integration makes it possible for users to leverage their existing identity credentials and offers administrators a single source of truth for user and permissions management. In this blog post, [...]
