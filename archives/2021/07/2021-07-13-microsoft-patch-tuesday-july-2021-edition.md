Title: Microsoft Patch Tuesday, July 2021 Edition
Date: 2021-07-13T21:41:47+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;Automox;Chad McNaughton;CVE-2021-31979;CVE-2021-33771;CVE-2021-34448;CVE-2021-34458;CVE-2021-34473;CVE-2021-34494;CVE-2021-34523;CVE-2021-34527;Microsoft Patch Tuesday July 2021;PrintNightmare;Satnam Narang;Tenable;Windows updates
Slug: microsoft-patch-tuesday-july-2021-edition

[Source](https://krebsonsecurity.com/2021/07/microsoft-patch-tuesday-july-2021-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to patch at least 116 security holes in its Windows operating systems and related software. At least four of the vulnerabilities addressed today are under active attack, according to Microsoft. Thirteen of the security bugs quashed in this month’s release earned Microsoft’s most-dire “critical” rating, meaning they can be exploited by malware or miscreants to seize remote control over a vulnerable system without any help from users. Another 103 of the security holes patched this month were flagged as “important,” which Microsoft assigns to vulnerabilities “whose exploitation could result in compromise of the confidentiality, integrity, or [...]
