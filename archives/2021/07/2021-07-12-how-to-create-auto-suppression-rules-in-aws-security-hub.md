Title: How to create auto-suppression rules in AWS Security Hub
Date: 2021-07-12T18:00:15+00:00
Author: BK Das
Category: AWS Security
Tags: AWS Security Hub;Intermediate (200);Security, Identity, & Compliance;Security Blog
Slug: how-to-create-auto-suppression-rules-in-aws-security-hub

[Source](https://aws.amazon.com/blogs/security/how-to-create-auto-suppression-rules-in-aws-security-hub/){:target="_blank" rel="noopener"}

> AWS Security Hub gives you a comprehensive view of your security alerts and security posture across your AWS accounts. With Security Hub, you have a single place that aggregates, organizes, and prioritizes your security alerts, or findings, from multiple AWS services. Security Hub lets you assign workflow statuses to these findings, which are NEW, NOTIFIED, [...]
