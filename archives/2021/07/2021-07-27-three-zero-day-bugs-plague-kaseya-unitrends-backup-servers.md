Title: Three Zero-Day Bugs Plague Kaseya Unitrends Backup Servers
Date: 2021-07-27T15:43:56+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: three-zero-day-bugs-plague-kaseya-unitrends-backup-servers

[Source](https://threatpost.com/zero-days-kaseya-unitrends-backup-servers/168180/){:target="_blank" rel="noopener"}

> The unpatched flaws include RCE and authenticated privilege escalation on the client-side: Just the latest woe for the ransomware-walloped MSP. [...]
