Title: The cost of cyber insurance increased 32% last year and shows no signs of easing
Date: 2021-07-05T22:14:06+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: the-cost-of-cyber-insurance-increased-32-last-year-and-shows-no-signs-of-easing

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/05/cyber_insurance_report/){:target="_blank" rel="noopener"}

> 'Claims are up, capacity is down, and underwriting profitability is, at best, under pressure' The cost of insurance to protect businesses and organisations against the ever-increasing threat of cybercrimes has soared by a third in the last year, according to international insurance brokers Howden.... [...]
