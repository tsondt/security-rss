Title: The Week in Ransomware - July 9th 2021 - A flawed attack
Date: 2021-07-09T15:46:51-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-july-9th-2021-a-flawed-attack

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-july-9th-2021-a-flawed-attack/){:target="_blank" rel="noopener"}

> This week's news focuses on the aftermath of REvil's ransomware attack on MSPs and customers using zero-day vulnerabilities in Kaseya VSA. The good news is that it has not been as disruptive as we initially feared. [...]
