Title: Safari Zero-Day Used in Malicious LinkedIn Campaign
Date: 2021-07-15T11:04:49+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Vulnerabilities
Slug: safari-zero-day-used-in-malicious-linkedin-campaign

[Source](https://threatpost.com/safari-zero-day-linkedin/167814/){:target="_blank" rel="noopener"}

> Researchers shed light on how attackers exploited Apple web browser vulnerabilities to target government officials in Western Europe. [...]
