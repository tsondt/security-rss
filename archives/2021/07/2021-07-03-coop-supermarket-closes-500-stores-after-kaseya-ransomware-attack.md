Title: Coop supermarket closes 500 stores after Kaseya ransomware attack
Date: 2021-07-03T11:15:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: coop-supermarket-closes-500-stores-after-kaseya-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/coop-supermarket-closes-500-stores-after-kaseya-ransomware-attack/){:target="_blank" rel="noopener"}

> Swedish supermarket chain Coop has shut down approximately 500 stores after they were affected by an REvil ransomware attack targeting managed service providers through a supply-chain attack. [...]
