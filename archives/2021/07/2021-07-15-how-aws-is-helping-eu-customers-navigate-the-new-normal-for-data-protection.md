Title: How AWS is helping EU customers navigate the new normal for data protection
Date: 2021-07-15T16:09:33+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements;Europe;Foundational (100);Security, Identity, & Compliance;Data transfers;EU;GDPR;SCCs;Security Blog;Standard contractual clauses
Slug: how-aws-is-helping-eu-customers-navigate-the-new-normal-for-data-protection

[Source](https://aws.amazon.com/blogs/security/how-aws-is-helping-eu-customers-navigate-the-new-normal-for-data-protection/){:target="_blank" rel="noopener"}

> Achieving compliance with the European Union’s data protection regulations is critical for hundreds of thousands of Amazon Web Services (AWS) customers. Many of them are subject to the EU’s General Data Protection Regulation (GDPR), which ensures individuals’ fundamental right to privacy and the protection of personal data. In February, we announced strengthened commitments to protect [...]
