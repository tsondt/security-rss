Title: Signal fixes bug that sent random images to wrong contacts
Date: 2021-07-26T08:51:45-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: signal-fixes-bug-that-sent-random-images-to-wrong-contacts

[Source](https://www.bleepingcomputer.com/news/security/signal-fixes-bug-that-sent-random-images-to-wrong-contacts/){:target="_blank" rel="noopener"}

> Signal has fixed a serious bug in its Android app that, in some cases, sent random unintended pictures to contacts without an obvious explanation. Although the issue was reported in December 2020, given the difficulty of reproducing the bug, it isn't until this month that a fix was pushed out. [...]
