Title: Adobe Patches 11 Critical Bugs in Popular Acrobat PDF Reader
Date: 2021-07-13T18:55:41+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: adobe-patches-11-critical-bugs-in-popular-acrobat-pdf-reader

[Source](https://threatpost.com/adobe-patches-critical-acrobat/167743/){:target="_blank" rel="noopener"}

> Adobe July patch roundup includes fixes for its ubiquitous and free PDF reader Acrobat 2020 and other software such as Illustrator and Bridge. [...]
