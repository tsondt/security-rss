Title: US chemical distributor shares info on DarkSide ransomware data theft
Date: 2021-07-03T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-chemical-distributor-shares-info-on-darkside-ransomware-data-theft

[Source](https://www.bleepingcomputer.com/news/security/us-chemical-distributor-shares-info-on-darkside-ransomware-data-theft/){:target="_blank" rel="noopener"}

> World-leading chemical distribution company Brenntag has shared additional info on what data was stolen from its network by DarkSide ransomware operators during an attack from late April 2021 that targeted its North America division. [...]
