Title: ‘Charming Kitten’ APT Siphons Intel From Mid-East Scholars
Date: 2021-07-13T16:44:59+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Web Security
Slug: charming-kitten-apt-siphons-intel-from-mid-east-scholars

[Source](https://threatpost.com/apt-ta453-siphons-intel-mideast/167715/){:target="_blank" rel="noopener"}

> Professors, journalists and think-tank personnel, beware strangers bearing webinars: It’s the focus of a particularly sophisticated, and chatty, phishing campaign. [...]
