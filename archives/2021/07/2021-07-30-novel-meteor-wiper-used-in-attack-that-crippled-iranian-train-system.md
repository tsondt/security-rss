Title: Novel Meteor Wiper Used in Attack that Crippled Iranian Train System
Date: 2021-07-30T15:21:41+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks;IoT;Malware
Slug: novel-meteor-wiper-used-in-attack-that-crippled-iranian-train-system

[Source](https://threatpost.com/novel-meteor-wiper-used-in-attack-that-crippled-iranian-train-system/168262/){:target="_blank" rel="noopener"}

> A July 9th attack disrupted service and taunted Iran’s leadership with hacked screens directing customers to call the phone of Iranian Supreme Leader Khamenei with complaints. [...]
