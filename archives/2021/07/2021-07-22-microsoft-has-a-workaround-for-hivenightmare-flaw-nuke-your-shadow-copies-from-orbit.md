Title: Microsoft has a workaround for 'HiveNightmare' flaw: Nuke your shadow copies from orbit
Date: 2021-07-22T18:27:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: microsoft-has-a-workaround-for-hivenightmare-flaw-nuke-your-shadow-copies-from-orbit

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/22/microsoft_hivenightmare/){:target="_blank" rel="noopener"}

> It's the only way to be sure After setting the "days since a security cock-up" counter back to zero, Microsoft has published an official workaround for its Access Control Lists (ACLs) vulnerability ( CVE-2021-36934 ).... [...]
