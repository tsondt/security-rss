Title: Potential phishing scam averted following Irish coronavirus certificate website typo
Date: 2021-07-26T13:22:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: potential-phishing-scam-averted-following-irish-coronavirus-certificate-website-typo

[Source](https://portswigger.net/daily-swig/potential-phishing-scam-averted-following-irish-coronavirus-certificate-website-typo){:target="_blank" rel="noopener"}

> Quick-thinking citizens snap up correctly spelled domain after spotting blunder [...]
