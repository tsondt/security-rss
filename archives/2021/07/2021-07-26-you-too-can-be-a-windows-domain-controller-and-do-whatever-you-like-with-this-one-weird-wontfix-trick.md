Title: You, too, can be a Windows domain controller and do whatever you like, with this one weird WONTFIX trick
Date: 2021-07-26T20:31:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: you-too-can-be-a-windows-domain-controller-and-do-whatever-you-like-with-this-one-weird-wontfix-trick

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/26/petitpotam_microsoft_windows/){:target="_blank" rel="noopener"}

> Microsoft offers some mitigations for thwarting PetitPotam attacks Microsoft completed a vulnerability hat-trick this month as yet another security weakness was uncovered in its operating systems. And this one doesn't even need authentication to work its magic.... [...]
