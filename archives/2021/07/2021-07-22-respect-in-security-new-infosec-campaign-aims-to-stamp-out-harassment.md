Title: Respect in Security: New infosec campaign aims to stamp out harassment
Date: 2021-07-22T13:15:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: respect-in-security-new-infosec-campaign-aims-to-stamp-out-harassment

[Source](https://portswigger.net/daily-swig/respect-in-security-new-infosec-campaign-aims-to-stamp-out-harassment){:target="_blank" rel="noopener"}

> Group of security professionals calls for higher standards of behavior online and in the workplace [...]
