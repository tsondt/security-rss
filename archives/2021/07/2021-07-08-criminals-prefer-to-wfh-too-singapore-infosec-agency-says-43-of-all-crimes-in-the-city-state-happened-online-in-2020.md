Title: Criminals prefer to WFH too: Singapore infosec agency says 43% of all crimes in the city-state happened online in 2020
Date: 2021-07-08T11:10:00+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: criminals-prefer-to-wfh-too-singapore-infosec-agency-says-43-of-all-crimes-in-the-city-state-happened-online-in-2020

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/08/singapore_crime_data_report/){:target="_blank" rel="noopener"}

> The times they are a-changin' The Cyber Security Agency of Singapore (CSA) today released data revealing that cybercrime accounted for 43 per cent of all crime in the city-state during 2020.... [...]
