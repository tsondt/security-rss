Title: With a straight face, Putin agrees to do something about ransomware coming out of Russia, apparently
Date: 2021-07-12T21:23:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: with-a-straight-face-putin-agrees-to-do-something-about-ransomware-coming-out-of-russia-apparently

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/12/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Beware bogus crypto-mining phone apps, and more In brief Late last week, President Biden said he brought up the epidemic of ransomware hitting American businesses in a phone call with his Russian counterpart, and hinted America may start hitting back.... [...]
