Title: BIOPASS RAT Uses Live Streaming Steal Victims’ Data
Date: 2021-07-12T20:30:15+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Web Security
Slug: biopass-rat-uses-live-streaming-steal-victims-data

[Source](https://threatpost.com/biopass-rat-live-streaming/167695/){:target="_blank" rel="noopener"}

> The malware has targeted Chinese gambling sites with fake app installers. [...]
