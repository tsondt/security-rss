Title: FBI reveals top targeted vulnerabilities of the last two years
Date: 2021-07-28T08:31:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-reveals-top-targeted-vulnerabilities-of-the-last-two-years

[Source](https://www.bleepingcomputer.com/news/security/fbi-reveals-top-targeted-vulnerabilities-of-the-last-two-years/){:target="_blank" rel="noopener"}

> A joint security advisory issued today by several cybersecurity agencies from the US, the UK, and Australia reveals the top 30 most targeted security vulnerabilities of the last two years. [...]
