Title: Kaspersky Password Manager&nbsp;lambasted for multiple cryptographic flaws
Date: 2021-07-07T16:19:25+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: kaspersky-password-manager-lambasted-for-multiple-cryptographic-flaws

[Source](https://portswigger.net/daily-swig/kaspersky-password-manager-nbsp-lambasted-for-multiple-cryptographic-flaws){:target="_blank" rel="noopener"}

> ‘All the passwords it created could be bruteforced,’ bemoan French researchers [...]
