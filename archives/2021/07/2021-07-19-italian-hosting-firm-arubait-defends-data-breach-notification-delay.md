Title: Italian hosting firm Aruba.it defends data breach notification delay
Date: 2021-07-19T13:48:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: italian-hosting-firm-arubait-defends-data-breach-notification-delay

[Source](https://portswigger.net/daily-swig/italian-hosting-firm-aruba-it-defends-data-breach-notification-delay){:target="_blank" rel="noopener"}

> ‘Catenaccio’ defenses breached by CMS hack [...]
