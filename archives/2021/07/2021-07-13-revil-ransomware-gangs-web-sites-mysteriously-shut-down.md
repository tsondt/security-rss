Title: REvil ransomware gang's web sites mysteriously shut down
Date: 2021-07-13T10:49:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: revil-ransomware-gangs-web-sites-mysteriously-shut-down

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-gangs-web-sites-mysteriously-shut-down/){:target="_blank" rel="noopener"}

> The infrastructure and websites for the REvil ransomware operation have mysteriously gone offline as of last night. [...]
