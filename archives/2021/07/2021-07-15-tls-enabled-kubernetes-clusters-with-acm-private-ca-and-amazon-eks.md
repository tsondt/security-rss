Title: TLS-enabled Kubernetes clusters with ACM Private CA and Amazon EKS
Date: 2021-07-15T00:33:54+00:00
Author: Param Sharma
Category: AWS Security
Tags: Amazon Elastic Kubernetes Service;AWS Certificate Manager;Intermediate (200);Security, Identity, & Compliance;ACM;ACM Private CA;Amazon EKS;Security Blog
Slug: tls-enabled-kubernetes-clusters-with-acm-private-ca-and-amazon-eks

[Source](https://aws.amazon.com/blogs/security/tls-enabled-kubernetes-clusters-with-acm-private-ca-and-amazon-eks-2/){:target="_blank" rel="noopener"}

> In this blog post, we show you how to set up end-to-end encryption on Amazon Elastic Kubernetes Service (Amazon EKS) with AWS Certificate Manager Private Certificate Authority. For this example of end-to-end encryption, traffic originates from your client and terminates at an Ingress controller server running inside a sample app. By following the instructions in [...]
