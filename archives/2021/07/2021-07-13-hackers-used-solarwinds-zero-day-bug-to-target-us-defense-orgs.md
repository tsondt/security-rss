Title: Hackers used SolarWinds zero-day bug to target US Defense orgs
Date: 2021-07-13T19:54:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: hackers-used-solarwinds-zero-day-bug-to-target-us-defense-orgs

[Source](https://www.bleepingcomputer.com/news/microsoft/hackers-used-solarwinds-zero-day-bug-to-target-us-defense-orgs/){:target="_blank" rel="noopener"}

> China-based hackers actively target US defense and software companies using a vulnerability in the SolarWinds Serv-U FTP server. [...]
