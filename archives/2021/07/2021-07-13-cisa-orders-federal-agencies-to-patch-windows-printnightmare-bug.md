Title: CISA orders federal agencies to patch Windows PrintNightmare bug
Date: 2021-07-13T12:23:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-orders-federal-agencies-to-patch-windows-printnightmare-bug

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-federal-agencies-to-patch-windows-printnightmare-bug/){:target="_blank" rel="noopener"}

> A new emergency directive ordered by the Cybersecurity and Infrastructure Security Agency (CISA) orders federal agencies to mitigate the actively exploited Window Print Spooler vulnerability on their networks. [...]
