Title: How to restrict IAM roles to access AWS resources from specific geolocations using AWS Client VPN
Date: 2021-07-20T17:48:19+00:00
Author: Artem Lovan
Category: AWS Security
Tags: Advanced (300);AWS Client VPN;AWS Identity and Access Management (IAM);Security, Identity, & Compliance;Amazon VPC;AWS Lambda;AWS Organizations;Geo-restriction;SCPs;Security Blog;service control policies;VPC;VPN
Slug: how-to-restrict-iam-roles-to-access-aws-resources-from-specific-geolocations-using-aws-client-vpn

[Source](https://aws.amazon.com/blogs/security/how-to-restrict-iam-roles-to-access-aws-resources-from-specific-geolocations-using-aws-client-vpn/){:target="_blank" rel="noopener"}

> You can improve your organization’s security posture by enforcing access to Amazon Web Services (AWS) resources based on IP address and geolocation. For example, users in your organization might bring their own devices, which might require additional security authorization checks and posture assessment in order to comply with corporate security requirements. Enforcing access to AWS [...]
