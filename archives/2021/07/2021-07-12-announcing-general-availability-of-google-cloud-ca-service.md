Title: Announcing general availability of Google Cloud CA Service
Date: 2021-07-12T20:00:00+00:00
Author: Anton Chuvakin
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: announcing-general-availability-of-google-cloud-ca-service

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-certificate-authority-service-is-now-ga/){:target="_blank" rel="noopener"}

> We are happy to announce the general availability of Certificate Authority Service offered by Google Cloud (Google Cloud CAS). Google Cloud CAS provides a highly scalable and available private CA to address the unprecedented growth in certificates in the digital world. This exponential growth is due to a perfect storm of conditions over the past few years, achieving almost a flywheel effect - the rise of cloud computing, moving to containers, the emergence of pervasive high speed connectivity, the proliferation of Internet-of-things (IoT) and smart devices (see our whitepaper on this topic). See how easy it is to set up [...]
