Title: US medical imaging center reports possible data breach after emails&nbsp;‘accessed’
Date: 2021-07-27T15:38:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-medical-imaging-center-reports-possible-data-breach-after-emails-accessed

[Source](https://portswigger.net/daily-swig/us-medical-imaging-center-reports-possible-data-breach-after-emails-nbsp-accessed){:target="_blank" rel="noopener"}

> Number of patients affected remains unconfirmed [...]
