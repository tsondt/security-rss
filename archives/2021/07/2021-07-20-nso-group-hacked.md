Title: NSO Group Hacked
Date: 2021-07-20T18:50:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Citizen Lab;cyberespionage;cyberweapons;leaks
Slug: nso-group-hacked

[Source](https://www.schneier.com/blog/archives/2021/07/nso-group-hacked.html){:target="_blank" rel="noopener"}

> NSO Group, the Israeli cyberweapons arms manufacturer behind the Pegasus spyware — used by authoritarian regimes around the world to spy on dissidents, journalists, human rights workers, and others — was hacked. Or, at least, an enormous trove of documents was leaked to journalists. There’s a lot to read out there. Amnesty International has a report. Citizen Lab conducted an independent analysis. The Guardian has extensive coverage. More coverage. Most interesting is a list of over 50,000 phone numbers that were being spied on by NSO Group’s software. Why does NSO Group have that list? The obvious answer is that [...]
