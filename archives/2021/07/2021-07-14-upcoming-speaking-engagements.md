Title: Upcoming Speaking Engagements
Date: 2021-07-14T17:10:01+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: 
Slug: upcoming-speaking-engagements-10

[Source](https://www.schneier.com/blog/archives/2021/07/upcoming-speaking-engagements-10.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at Norbert Wiener in the 21st Century, a virtual conference hosted by The IEEE Society on Social Implications of Technology (SSIT), July 23-25, 2021. I’m speaking at DEFCON 29, August 5-8, 2021. I’m speaking (via Internet) at SHIFT Business Festival in Finland, August 25-26, 2021. I’ll be speaking at an Informa event on September 14, 2021. Details to come. I’m keynoting CIISec Live —an all-online event—September 15-16, 2021. I’m speaking at the Cybersecurity and Data Privacy Law Conference in Plano, Texas, USA, September 22-23, [...]
