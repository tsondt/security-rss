Title: The Week in Ransomware - July 16th 2021 - REvil disappears
Date: 2021-07-16T15:42:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-july-16th-2021-revil-disappears

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-july-16th-2021-revil-disappears/){:target="_blank" rel="noopener"}

> Ransomware operations have been quieter this week as the White House engages in talks with the Russian government about cracking down on cybercriminals believed to be operating in Russia. [...]
