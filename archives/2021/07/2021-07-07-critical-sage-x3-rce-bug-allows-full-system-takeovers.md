Title: Critical Sage X3 RCE Bug Allows Full System Takeovers
Date: 2021-07-07T18:34:24+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Vulnerabilities
Slug: critical-sage-x3-rce-bug-allows-full-system-takeovers

[Source](https://threatpost.com/critical-sage-x3-rce-bug-allows-full-system-takeovers/167612/){:target="_blank" rel="noopener"}

> Security vulnerabilities in the ERP platform could allow attackers to tamper with or sabotage victims' business-critical processes and to intercept data. [...]
