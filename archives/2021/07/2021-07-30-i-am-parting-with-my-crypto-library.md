Title: I Am Parting With My Crypto Library
Date: 2021-07-30T17:13:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: books;Schneier news
Slug: i-am-parting-with-my-crypto-library

[Source](https://www.schneier.com/blog/archives/2021/07/i-am-parting-with-my-crypto-library.html){:target="_blank" rel="noopener"}

> The time has come for me to find a new home for my (paper) cryptography library. It’s about 150 linear feet of books, conference proceedings, journals, and monographs — mostly from the 1980s, 1990s, and 2000s. My preference is that it goes to an educational institution, but will consider a corporate or personal home if that’s the only option available. If you think you can break it up and sell it, I’ll consider that as a last resort. New owner pays all packaging and shipping costs, and possibly a purchase price depending on who you are and what you want [...]
