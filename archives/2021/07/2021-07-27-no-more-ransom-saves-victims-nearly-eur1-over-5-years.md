Title: No More Ransom Saves Victims Nearly €1 Over 5 Years
Date: 2021-07-27T21:10:11+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: no-more-ransom-saves-victims-nearly-eur1-over-5-years

[Source](https://threatpost.com/no-more-ransom-saves-victims-e1-5-years/168192/){:target="_blank" rel="noopener"}

> No More Ransom is collecting decryptors so ransomware victims don’t have to pay to get their data back and attackers don’t get rich. [...]
