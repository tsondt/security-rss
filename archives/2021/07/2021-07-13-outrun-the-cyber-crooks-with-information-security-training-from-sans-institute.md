Title: Outrun the cyber-crooks with information security training from SANS Institute
Date: 2021-07-13T23:24:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: outrun-the-cyber-crooks-with-information-security-training-from-sans-institute

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/13/sans_sydney_september/){:target="_blank" rel="noopener"}

> In-person and online in Asia-Pacific with simultaneous Japanese translation Promo No matter where you are in the world, you’re never far from a potential cyber-attack. Luckily, SANS Institute is going to be girdling the globe over the coming months, bringing you top-notch cyber-security events with a personal touch.... [...]
