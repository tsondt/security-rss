Title: Mint Mobile hit by a data breach after numbers ported, data accessed
Date: 2021-07-10T13:18:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Mobile
Slug: mint-mobile-hit-by-a-data-breach-after-numbers-ported-data-accessed

[Source](https://www.bleepingcomputer.com/news/security/mint-mobile-hit-by-a-data-breach-after-numbers-ported-data-accessed/){:target="_blank" rel="noopener"}

> Mint Mobile has disclosed a data breach after an unauthorized person gained access to subscribers' account information and ported phone numbers to another carrier. [...]
