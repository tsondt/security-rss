Title: Kaseya Attack Fallout: CISA, FBI Offer Guidance
Date: 2021-07-05T20:12:26+00:00
Author: Tom Spring
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: kaseya-attack-fallout-cisa-fbi-offer-guidance

[Source](https://threatpost.com/kaseya-attack-fallout/167541/){:target="_blank" rel="noopener"}

> Following a brazen ransomware attack by the REvil cybergang, CISA and FBI offer guidance to victims. [...]
