Title: Hong Kong working to share its digital IDs with mainland China
Date: 2021-07-14T08:03:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: hong-kong-working-to-share-its-digital-ids-with-mainland-china

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/14/hong_kong_sharing_digital_id_with_china/){:target="_blank" rel="noopener"}

> App currently used for 110 government services, more to come Hong Kong’s Office of the Government Chief Information Officer (OGCIO) has revealed that the territory is investigating the use of its digital ID in mainland China.... [...]
