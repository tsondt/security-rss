Title: Security vulnerabilities in IDEMIA access control devices could allow attackers to ‘remotely open doors’
Date: 2021-07-26T15:06:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-vulnerabilities-in-idemia-access-control-devices-could-allow-attackers-to-remotely-open-doors

[Source](https://portswigger.net/daily-swig/security-vulnerabilities-in-idemia-access-control-devices-could-allow-attackers-to-remotely-open-doors){:target="_blank" rel="noopener"}

> Flaws in biometric hardware have since been patched [...]
