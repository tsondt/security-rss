Title: DARPA nails cash to project 'FENCE' — a smart camera that only sends pics when pixels change
Date: 2021-07-06T05:29:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: darpa-nails-cash-to-project-fence-a-smart-camera-that-only-sends-pics-when-pixels-change

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/06/darpa_open_sources_bug_bounty_platform/){:target="_blank" rel="noopener"}

> Research agency also open-sources FETT hardware bug bounty platform and tools The USA’s Defense Advanced Research Projects Agency (DARPA) has announced it will fund development of a new type of “event-based” camera that only transmits information about pixels that have changed.... [...]
