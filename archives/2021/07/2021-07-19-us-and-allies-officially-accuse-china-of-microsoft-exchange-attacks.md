Title: US and allies officially accuse China of Microsoft Exchange attacks
Date: 2021-07-19T07:49:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-and-allies-officially-accuse-china-of-microsoft-exchange-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-and-allies-officially-accuse-china-of-microsoft-exchange-attacks/){:target="_blank" rel="noopener"}

> US and allies, including the European Union, the United Kingdom, and NATO, are officially blaming China for this year's widespread Microsoft Exchange hacking campaign. [...]
