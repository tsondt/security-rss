Title: Chinese state hackers breached over a dozen US pipeline operators
Date: 2021-07-21T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: chinese-state-hackers-breached-over-a-dozen-us-pipeline-operators

[Source](https://www.bleepingcomputer.com/news/security/chinese-state-hackers-breached-over-a-dozen-us-pipeline-operators/){:target="_blank" rel="noopener"}

> Chinese state-sponsored attackers have breached 13 US oil and natural gas (ONG) pipeline companies between December 2011 to 2013 following a spear-phishing campaign targeting their employees. [...]
