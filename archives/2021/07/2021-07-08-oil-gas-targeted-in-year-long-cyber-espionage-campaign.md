Title: Oil & Gas Targeted in Year-Long Cyber-Espionage Campaign
Date: 2021-07-08T20:29:02+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: oil-gas-targeted-in-year-long-cyber-espionage-campaign

[Source](https://threatpost.com/oil-gas-cyber-espionage-campaign/167639/){:target="_blank" rel="noopener"}

> A global effort to steal information from energy companies is using sophisticated social engineering to deliver Agent Tesla and other RATs. [...]
