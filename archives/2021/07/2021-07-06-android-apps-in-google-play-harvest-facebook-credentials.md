Title: Android Apps in Google Play Harvest Facebook Credentials
Date: 2021-07-06T20:01:25+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: android-apps-in-google-play-harvest-facebook-credentials

[Source](https://threatpost.com/android-apps-google-play-facebook-credentials/167563/){:target="_blank" rel="noopener"}

> The apps all used an unusual tactic of loading a legitimate Facebook page as part of the data theft. [...]
