Title: Google revamps bug bounty program
Date: 2021-07-28T06:58:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: google-revamps-bug-bounty-program

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/28/google_revamps_vulnerability_reward_program/){:target="_blank" rel="noopener"}

> Announces that it's paid out for 11,000 bugs in under eleven years Google has revealed that its bug bounty program – which it styles a "Vulnerability Reward Program" – has paid out for 11,055 bugs found in its services since 2010.... [...]
