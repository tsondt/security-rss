Title: Kaseya: Roughly 1,500 businesses hit by REvil ransomware attack
Date: 2021-07-06T07:59:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: kaseya-roughly-1500-businesses-hit-by-revil-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/kaseya-roughly-1-500-businesses-hit-by-revil-ransomware-attack/){:target="_blank" rel="noopener"}

> Kaseya says the REvil supply-chain ransomware attack breached the systems of roughly 60 of its direct customers using the company's VSA on-premises product. [...]
