Title: Storing Encrypted Photos in Google’s Cloud
Date: 2021-07-30T11:34:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;cloud computing;encryption;Google;photos
Slug: storing-encrypted-photos-in-googles-cloud

[Source](https://www.schneier.com/blog/archives/2021/07/storing-encrypted-photos-in-googles-cloud.html){:target="_blank" rel="noopener"}

> New paper: “ Encrypted Cloud Photo Storage Using Google Photos “: Abstract: Cloud photo services are widely used for persistent, convenient, and often free photo storage, which is especially useful for mobile devices. As users store more and more photos in the cloud, significant privacy concerns arise because even a single compromise of a user’s credentials give attackers unfettered access to all of the user’s photos. We have created Easy Secure Photos (ESP) to enable users to protect their photos on cloud photo services such as Google Photos. ESP introduces a new client-side encryption architecture that includes a novel format-preserving [...]
