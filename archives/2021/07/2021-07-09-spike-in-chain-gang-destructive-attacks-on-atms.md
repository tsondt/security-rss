Title: Spike in “Chain Gang” Destructive Attacks on ATMs
Date: 2021-07-09T19:31:16+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: All About Skimmers;The Coming Storm;ATM smash-and-grab;chain gang attacks;Elaine Dodd;European ATM;explosive ATM attacks;Lachlan Gunn;Oklahoma Bankers Association;Tracey Santor;Travelers
Slug: spike-in-chain-gang-destructive-attacks-on-atms

[Source](https://krebsonsecurity.com/2021/07/spike-in-chain-gang-destructive-attacks-on-atms/){:target="_blank" rel="noopener"}

> Last summer, financial institutions throughout Texas started reporting a sudden increase in attacks involving well-orchestrated teams that would show up at night, use stolen trucks and heavy chains to rip Automated Teller Machines (ATMs) out of their foundations, and make off with the cash boxes inside. Now it appears the crime — known variously as “ ATM smash-and-grab ” or “ chain gang ” attacks — is rapidly increasing in other states. Four different ATM “chain gang” attacks in Texas recently. Image: Texas Bankers Association. The Texas Bankers Association documented at least 139 chain gang attacks against Texas financial institutions [...]
