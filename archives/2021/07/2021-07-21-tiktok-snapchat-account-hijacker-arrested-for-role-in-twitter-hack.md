Title: TikTok, Snapchat account hijacker arrested for role in Twitter hack
Date: 2021-07-21T17:17:53-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: tiktok-snapchat-account-hijacker-arrested-for-role-in-twitter-hack

[Source](https://www.bleepingcomputer.com/news/security/tiktok-snapchat-account-hijacker-arrested-for-role-in-twitter-hack/){:target="_blank" rel="noopener"}

> A fourth suspect has been arrested today for his role in the Twitter hack last year that gave attackers access to the company's internal network exposing high-profile accounts to hijacking. [...]
