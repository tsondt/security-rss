Title: Zimbra Server Bugs Could Lead to Email Plundering
Date: 2021-07-27T17:30:28+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: zimbra-server-bugs-could-lead-to-email-plundering

[Source](https://threatpost.com/zimbra-server-bugs-email-plundering/168188/){:target="_blank" rel="noopener"}

> Two bugs, now patched except in older versions, could be chained to allow attackers to hijack Zimbra server by simply sending a malicious email. [...]
