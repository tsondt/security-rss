Title: US senators warn China's Digital Yuan could compromise Olympic athletes
Date: 2021-07-22T02:48:49+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: us-senators-warn-chinas-digital-yuan-could-compromise-olympic-athletes

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/22/tokyo_olympics_data_leak/){:target="_blank" rel="noopener"}

> Meanwhile, Tokyo games ticket holder data leaks, and those affected can't even use their seats Three US senators have written to their nation's Olympic Committee with a request that it "forbid American athletes from receiving or using Digital Yuan during the Beijing Olympics" – a reference to the Winter Games scheduled to commence on February 4th, 2022.... [...]
