Title: Babuk Ransomware Builder Mysteriously Appears in VirusTotal
Date: 2021-07-01T14:11:42+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware
Slug: babuk-ransomware-builder-mysteriously-appears-in-virustotal

[Source](https://threatpost.com/babuk-ransomware-builder-virustotal/167481/){:target="_blank" rel="noopener"}

> The gang's source code is now available to rivals and security researchers alike - and a decryptor likely is not far behind. [...]
