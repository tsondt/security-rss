Title: UC San Diego Health discloses data breach after phishing attack
Date: 2021-07-27T16:06:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: uc-san-diego-health-discloses-data-breach-after-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/uc-san-diego-health-discloses-data-breach-after-phishing-attack/){:target="_blank" rel="noopener"}

> UC San Diego Health, the academic health system of the University of California, San Diego, has disclosed a data breach after the compromise of some employees' email accounts. [...]
