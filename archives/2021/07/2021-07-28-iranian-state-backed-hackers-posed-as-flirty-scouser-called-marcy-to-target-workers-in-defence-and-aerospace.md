Title: Iranian state-backed hackers posed as flirty Scouser called Marcy to target workers in defence and aerospace
Date: 2021-07-28T16:45:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: iranian-state-backed-hackers-posed-as-flirty-scouser-called-marcy-to-target-workers-in-defence-and-aerospace

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/28/flirty_scouse_fitness_instructor_actually_iranian_spy/){:target="_blank" rel="noopener"}

> Recognise this one? Oh dear... Iranian state-backed hackers posed as a flirty Liverpudlian aerobics instructor in order to trick defence and aerospace workers into revealing secrets, according to a newly-published study.... [...]
