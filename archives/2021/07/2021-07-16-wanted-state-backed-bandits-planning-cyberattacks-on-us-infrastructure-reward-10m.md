Title: Wanted: State-backed bandits planning cyberattacks on US infrastructure. Reward: $10m
Date: 2021-07-16T16:30:13+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: wanted-state-backed-bandits-planning-cyberattacks-on-us-infrastructure-reward-10m

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/16/us_10m_reward_cybercrime/){:target="_blank" rel="noopener"}

> Cops and dobbers: Uncle Sam dangles cash incentive for tattletales The US is offering a $10m reward to anyone who dobs in digital outlaws responsible for foreign government-backed cyberattacks on critical national infrastructure such as pipelines, power grids, and communication networks.... [...]
