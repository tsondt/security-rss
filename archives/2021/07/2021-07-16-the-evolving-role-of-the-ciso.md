Title: The Evolving Role of the CISO
Date: 2021-07-16T17:27:45+00:00
Author: Curtis Simpson
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;InfoSec Insider;IoT;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: the-evolving-role-of-the-ciso

[Source](https://threatpost.com/evolving-role-ciso/167873/){:target="_blank" rel="noopener"}

> Curtis Simpson, CISO at Armis, discusses the top qualities that all CISOs need to possess to excel. [...]
