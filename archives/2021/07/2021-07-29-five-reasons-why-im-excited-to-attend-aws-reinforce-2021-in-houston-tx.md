Title: Five reasons why I’m excited to attend AWS re:Inforce 2021 in Houston, TX
Date: 2021-07-29T00:13:35+00:00
Author: Clarke Rodgers
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;re:Inforce;re:Inforce 2021;Security Blog
Slug: five-reasons-why-im-excited-to-attend-aws-reinforce-2021-in-houston-tx

[Source](https://aws.amazon.com/blogs/security/five-reasons-why-im-excited-to-attend-aws-reinforce-2021-in-houston-tx/){:target="_blank" rel="noopener"}

> You may have seen the recent invitation from Stephen Schmidt, Chief Information Security Officer (CISO) at Amazon Web Services, to join us at AWS re:Inforce in Houston, TX on August 24 and 25. I’d like to dive a little bit deeper into WHY you should attend and HOW to make the most of your time there. [...]
