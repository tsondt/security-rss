Title: Hole blasted in Guntrader: UK firearms sales website's CRM database breached, 111,000 users' info spilled online
Date: 2021-07-23T11:29:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: hole-blasted-in-guntrader-uk-firearms-sales-websites-crm-database-breached-111000-users-info-spilled-online

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/23/guntrader_hacked_111k_users_sql_database/){:target="_blank" rel="noopener"}

> One of the worst things that could happen to privacy-focused community Criminals have hacked into a Gumtree-style website used for buying and selling firearms, making off with a 111,000-entry database containing partial information from a CRM product used by gun shops across the UK.... [...]
