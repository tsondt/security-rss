Title: What's this about a lawyer looking for an heir? City of London Police seek IT crew to help crack down on fraud
Date: 2021-07-05T15:13:04+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: whats-this-about-a-lawyer-looking-for-an-heir-city-of-london-police-seek-it-crew-to-help-crack-down-on-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/05/london_police_fraud_contract/){:target="_blank" rel="noopener"}

> Contract worth £75m over seven years City of London Police is looking to crack down on cybercrime with the purchase of "next-generation IT services" in the hopes it will beef up the systems supporting Action Fraud and the National Fraud Intelligence Bureau (NFIB).... [...]
