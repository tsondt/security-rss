Title: Microsoft Issues Emergency Patch for Windows Flaw
Date: 2021-07-07T14:34:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Security Tools;Time to Patch;CVE-2021-34527;KB5004945;PrintNightmare;Satnam Narang;Tenable
Slug: microsoft-issues-emergency-patch-for-windows-flaw

[Source](https://krebsonsecurity.com/2021/07/microsoft-issues-emergency-patch-for-windows-flaw/){:target="_blank" rel="noopener"}

> Microsoft on Tuesday issued an emergency software update to quash a security bug that’s been dubbed “ PrintNightmare,” a critical vulnerability in all supported versions of Windows that is actively being exploited. The fix comes a week ahead of Microsoft’s normal monthly Patch Tuesday release, and follows the publishing of exploit code showing would-be attackers how to leverage the flaw to break into Windows computers. At issue is CVE-2021-34527, which involves a flaw in the Windows Print Spooler service that could be exploited by attackers to run code of their choice on a target’s system. Microsoft says it has already [...]
