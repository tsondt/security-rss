Title: Data breach at third-party provider exposes medical information of US healthcare patients
Date: 2021-07-06T14:21:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-at-third-party-provider-exposes-medical-information-of-us-healthcare-patients

[Source](https://portswigger.net/daily-swig/data-breach-at-third-party-provider-exposes-medical-information-of-us-healthcare-patients){:target="_blank" rel="noopener"}

> Multiple hospitals affected by the cyber incident [...]
