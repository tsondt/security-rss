Title: Babuk ransomware is back, uses new version on corporate networks
Date: 2021-07-01T19:25:34-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: babuk-ransomware-is-back-uses-new-version-on-corporate-networks

[Source](https://www.bleepingcomputer.com/news/security/babuk-ransomware-is-back-uses-new-version-on-corporate-networks/){:target="_blank" rel="noopener"}

> After announcing their exit from the ransomware business in favor of data theft extortion, the Babuk gang appears to have slipped back into their old habit of encrypting corporate networks. [...]
