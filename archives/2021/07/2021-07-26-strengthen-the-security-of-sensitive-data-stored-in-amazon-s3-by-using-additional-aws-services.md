Title: Strengthen the security of sensitive data stored in Amazon S3 by using additional AWS services
Date: 2021-07-26T17:22:41+00:00
Author: Jerry Mullis
Category: AWS Security
Tags: Amazon GuardDuty;Amazon Macie;Amazon Simple Storage Services (S3);AWS Config;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Amazon S3;AWS IAM Access Analyzer;Security Blog
Slug: strengthen-the-security-of-sensitive-data-stored-in-amazon-s3-by-using-additional-aws-services

[Source](https://aws.amazon.com/blogs/security/strengthen-the-security-of-sensitive-data-stored-in-amazon-s3-by-using-additional-aws-services/){:target="_blank" rel="noopener"}

> In this post, we describe the AWS services that you can use to both detect and protect your data stored in Amazon Simple Storage Service (Amazon S3). When you analyze security in depth for your Amazon S3 storage, consider doing the following: Audit and restrict Amazon S3 access with AWS Identity and Access Management (IAM) [...]
