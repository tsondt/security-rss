Title: NPM package steals Chrome passwords on Windows via recovery tool
Date: 2021-07-21T09:00:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: npm-package-steals-chrome-passwords-on-windows-via-recovery-tool

[Source](https://www.bleepingcomputer.com/news/security/npm-package-steals-chrome-passwords-on-windows-via-recovery-tool/){:target="_blank" rel="noopener"}

> New npm malware has been caught stealing credentials from the Google Chrome web browser by using legitimate password recovery tools on Windows systems. Additionally, this malware listens for incoming connections from the attacker's C2 server and provides advanced capabilities, including screen and camera access. [...]
