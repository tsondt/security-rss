Title: New research: Enterprises more confident than ever in cloud security
Date: 2021-07-01T16:30:00+00:00
Author: Natalie Lambert
Category: GCP Security
Tags: Google Cloud;Research;COVID-19;Identity & Security
Slug: new-research-enterprises-more-confident-than-ever-in-cloud-security

[Source](https://cloud.google.com/blog/products/identity-security/enterprises-trust-cloud-security/){:target="_blank" rel="noopener"}

> Cloud-based solutions were a technology life raft for organizations during the COVID-19 pandemic as employees took to the virtual office and companies scrambled to adjust to a distributed, remote reality. However, these rapid and substantial changes in the role of cloud technologies on the business came with an increased focus on security. The accelerated move to the cloud also meant companies needed to rapidly evolve existing security practices to protect everything that matters at the core of business—from their people and their operational and transactional data to customers and their most sensitive personal information. Suddenly, enterprises were keenly aware of [...]
