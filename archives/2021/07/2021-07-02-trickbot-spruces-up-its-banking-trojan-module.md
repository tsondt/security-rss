Title: TrickBot Spruces Up Its Banking Trojan Module
Date: 2021-07-02T16:17:02+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: trickbot-spruces-up-its-banking-trojan-module

[Source](https://threatpost.com/trickbot-banking-trojan-module/167521/){:target="_blank" rel="noopener"}

> After focusing almost exclusively on delivering ransomware for the past year, the code changes could indicate that TrickBot is getting back into the bank-fraud game. [...]
