Title: Colorado Passes Consumer Privacy Law
Date: 2021-07-15T11:08:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: data collection;data protection;privacy
Slug: colorado-passes-consumer-privacy-law

[Source](https://www.schneier.com/blog/archives/2021/07/colorado-passes-consumer-privacy-law.html){:target="_blank" rel="noopener"}

> First California. Then Virginia. Now Colorado. Here’s a good comparison of the three states’ laws. [...]
