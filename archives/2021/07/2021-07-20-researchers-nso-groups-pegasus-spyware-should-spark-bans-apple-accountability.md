Title: Researchers: NSO Group’s Pegasus Spyware Should Spark Bans, Apple Accountability
Date: 2021-07-20T18:56:23+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Vulnerabilities
Slug: researchers-nso-groups-pegasus-spyware-should-spark-bans-apple-accountability

[Source](https://threatpost.com/nso-pegasus-spyware-bans-apple-accountability/167965/){:target="_blank" rel="noopener"}

> Our roundtable of experts weighs in on implications for Apple and lawmakers in the wake of the bombshell report showing widespread surveillance of dissidents, journalists and others. [...]
