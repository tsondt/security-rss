Title: Tech biz must tell us about more security breaches, says UK.gov as it ponders lowering report thresholds
Date: 2021-07-27T18:15:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: tech-biz-must-tell-us-about-more-security-breaches-says-ukgov-as-it-ponders-lowering-report-thresholds

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/27/uk_security_breach_reporting_law_thresholds/){:target="_blank" rel="noopener"}

> Breach reporting law might have effect on overseas operators too The British government wants to make Amazon, Google, and other digital service providers report cybersecurity breaches to the Information Commissioner, according to newly published plans.... [...]
