Title: Citing cross-border data transfer and privacy concerns, China promises security blitz on securities
Date: 2021-07-07T07:00:37+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: citing-cross-border-data-transfer-and-privacy-concerns-china-promises-security-blitz-on-securities

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/07/china_securities_security_crackdown/){:target="_blank" rel="noopener"}

> Beijing wants tech companies that list offshore to get serious about compliance Infosec concerns have led China’s government to apply closer scrutiny to Chinese companies that list and send data offshore, according to a document written by China’s State Council cabinet and the Communist Party’s General Secretary.... [...]
