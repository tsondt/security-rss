Title: AWS achieves Spain’s ENS High certification across 149 services
Date: 2021-07-01T20:33:54+00:00
Author: Niyaz Noor
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;AWS Compliance;ENS;ENS High Standard;Esquema Nacional de Seguridad;Security Blog;Spain;Spanish public sector
Slug: aws-achieves-spains-ens-high-certification-across-149-services

[Source](https://aws.amazon.com/blogs/security/aws-achieves-spains-ens-high-certification-across-149-services/){:target="_blank" rel="noopener"}

> Gaining and maintaining customer trust is an ongoing commitment at Amazon Web Services (AWS). We continually add more services to our ENS certification scope. This helps to assure public sector organizations in Spain that want to build secure applications and services on AWS that the expected ENS certification security standards are being met. ENS certification [...]
