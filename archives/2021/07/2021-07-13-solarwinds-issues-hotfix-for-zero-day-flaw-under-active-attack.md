Title: SolarWinds Issues Hotfix for Zero-Day Flaw Under Active Attack
Date: 2021-07-13T12:58:11+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: solarwinds-issues-hotfix-for-zero-day-flaw-under-active-attack

[Source](https://threatpost.com/solarwinds-hotfix-zero-day-active-attack/167704/){:target="_blank" rel="noopener"}

> Microsoft alerted the company to a security vulnerability in its Serv-U Managed File Transfer and Secure FTP products that a cyberattacker is using to target a “limited” amount of customers. [...]
