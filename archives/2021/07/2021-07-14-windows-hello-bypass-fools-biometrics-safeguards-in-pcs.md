Title: Windows Hello Bypass Fools Biometrics Safeguards in PCs
Date: 2021-07-14T11:05:04+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: windows-hello-bypass-fools-biometrics-safeguards-in-pcs

[Source](https://threatpost.com/windows-hello-bypass-biometrics-pcs/167771/){:target="_blank" rel="noopener"}

> A Windows security bug would allow an attacker to fool a USB camera used in the biometric facial-recognition aspect of the system. [...]
