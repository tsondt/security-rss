Title: Ransomware hits law firm counseling Fortune 500, Global 500 companies
Date: 2021-07-18T11:22:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ransomware-hits-law-firm-counseling-fortune-500-global-500-companies

[Source](https://www.bleepingcomputer.com/news/security/ransomware-hits-law-firm-counseling-fortune-500-global-500-companies/){:target="_blank" rel="noopener"}

> Campbell Conroy & O'Neil, P.C. (Campbell), a US law firm counseling dozens of Fortune 500 and Global 500 companies, has disclosed a data breach following a February 2021 ransomware attack. [...]
