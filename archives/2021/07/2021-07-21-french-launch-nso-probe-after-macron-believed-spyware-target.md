Title: French Launch NSO Probe After Macron Believed Spyware Target
Date: 2021-07-21T13:32:42+00:00
Author: Tom Spring
Category: Threatpost
Tags: Government;Malware;Mobile Security
Slug: french-launch-nso-probe-after-macron-believed-spyware-target

[Source](https://threatpost.com/french-launch-nso-probe-after-macron-believed-spyware-targe/167986/){:target="_blank" rel="noopener"}

> Fourteen world leaders were among those found on list of NSO believed targets for its Pegasus spyware. [...]
