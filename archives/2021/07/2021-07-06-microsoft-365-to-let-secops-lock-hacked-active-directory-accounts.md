Title: Microsoft 365 to let SecOps lock hacked Active Directory accounts
Date: 2021-07-06T12:53:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: microsoft-365-to-let-secops-lock-hacked-active-directory-accounts

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-to-let-secops-lock-hacked-active-directory-accounts/){:target="_blank" rel="noopener"}

> Microsoft is updating Microsoft Defender for Identity to allow security operations (SecOps) teams to block attacks by locking a compromised user's Active Directory account. [...]
