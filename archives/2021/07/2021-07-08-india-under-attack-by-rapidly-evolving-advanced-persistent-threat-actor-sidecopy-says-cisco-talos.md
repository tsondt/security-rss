Title: India under attack by rapidly-evolving advanced persistent threat actor SideCopy, says Cisco Talos
Date: 2021-07-08T07:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: india-under-attack-by-rapidly-evolving-advanced-persistent-threat-actor-sidecopy-says-cisco-talos

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/08/india_under_attack_by_rapidlyevolving/){:target="_blank" rel="noopener"}

> Gang is using custom RATs malware to target government employees, and has an interest in Pakistan, too Cisco’s Talos security unit says it has detected an increased rate of attacks on targets on the Indian subcontinent and named an advanced persistent threat actor named SideCopy as the source.... [...]
