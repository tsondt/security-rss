Title: Microsoft July 2021 Patch Tuesday fixes 9 zero-days, 117 flaws
Date: 2021-07-13T13:47:35-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-july-2021-patch-tuesday-fixes-9-zero-days-117-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-july-2021-patch-tuesday-fixes-9-zero-days-117-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's July 2021 Patch Tuesday, and with it comes fixes for nine zero-day vulnerabilities and a total of 117 flaws, so Windows admins will be pulling their hair out as they scramble to get devices patched and secured. [...]
