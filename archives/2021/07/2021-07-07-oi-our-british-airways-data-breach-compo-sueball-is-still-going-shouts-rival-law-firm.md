Title: Oi! Our British Airways data breach compo sueball is still going, shouts rival law firm
Date: 2021-07-07T17:10:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: oi-our-british-airways-data-breach-compo-sueball-is-still-going-shouts-rival-law-firm

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/07/british_airways_data_breach_rival_lawsuit/){:target="_blank" rel="noopener"}

> Yesterday's settlement was just one of the bigger firms The British Airways data breach not-quite-a-class-action hasn't ended after all, a rival to yesterday's law firm has told The Register.... [...]
