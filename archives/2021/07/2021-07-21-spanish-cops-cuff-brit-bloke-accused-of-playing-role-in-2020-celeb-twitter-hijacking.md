Title: Spanish cops cuff Brit bloke accused of playing role in 2020 celeb Twitter hijacking
Date: 2021-07-21T20:54:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: spanish-cops-cuff-brit-bloke-accused-of-playing-role-in-2020-celeb-twitter-hijacking

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/21/twitter_arrest_spain/){:target="_blank" rel="noopener"}

> 'PlugWalkJoe' also said to have meddled with TikTok, SnapChat The Spanish National Police have, at the request of America, arrested UK citizen Joseph O’Connor in Estepona, Spain, in connection with the July 2020 takeover of more than 130 Twitter accounts.... [...]
