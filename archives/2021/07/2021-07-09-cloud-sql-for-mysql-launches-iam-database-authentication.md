Title: Cloud SQL for MySQL launches IAM database authentication
Date: 2021-07-09T16:00:00+00:00
Author: Akhil Jariwala
Category: GCP Security
Tags: Google Cloud;Identity & Security;Databases
Slug: cloud-sql-for-mysql-launches-iam-database-authentication

[Source](https://cloud.google.com/blog/products/databases/iam-database-authentication-comes-to-cloud-sql-for-mysql/){:target="_blank" rel="noopener"}

> When enterprise IT administrators design their data systems, security is among the most important considerations they have to make. Security is key to defining where data is stored and how users access it. Traditionally, IT administrators have managed user access to systems like SQL databases through issuing users a separate, dedicated username and password. Although it’s simple to set up, distributed access control requires administrators to spend a lot of time securing each system, instituting password complexity and rotation policies. For some enterprises, such as those bound by SOX or PCI-DSS rules, these measures may be required in each system [...]
