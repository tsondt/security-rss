Title: Kaseya warns of phishing campaign pushing fake security updates
Date: 2021-07-09T05:57:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: kaseya-warns-of-phishing-campaign-pushing-fake-security-updates

[Source](https://www.bleepingcomputer.com/news/security/kaseya-warns-of-phishing-campaign-pushing-fake-security-updates/){:target="_blank" rel="noopener"}

> Kaseya has warned customers that an ongoing phishing campaign attempts to breach their networks by spamming emails bundling malicious attachments and embedded links posing as legitimate VSA security updates. [...]
