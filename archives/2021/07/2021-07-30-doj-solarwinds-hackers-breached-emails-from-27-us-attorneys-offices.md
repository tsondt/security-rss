Title: DOJ: SolarWinds hackers breached emails from 27 US Attorneys’ offices
Date: 2021-07-30T20:12:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: doj-solarwinds-hackers-breached-emails-from-27-us-attorneys-offices

[Source](https://www.bleepingcomputer.com/news/security/doj-solarwinds-hackers-breached-emails-from-27-us-attorneys-offices/){:target="_blank" rel="noopener"}

> The US Department of Justice says that the Microsoft Office 365 email accounts of employees at 27 US Attorneys' offices were breached by the Russian Foreign Intelligence Service (SVR) during the SolarWinds global hacking spree. [...]
