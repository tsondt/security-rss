Title: US indicts members of Chinese-backed hacking group APT40
Date: 2021-07-19T10:44:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-indicts-members-of-chinese-backed-hacking-group-apt40

[Source](https://www.bleepingcomputer.com/news/security/us-indicts-members-of-chinese-backed-hacking-group-apt40/){:target="_blank" rel="noopener"}

> Today, the US Department of Justice (DOJ) indicted four members of the Chinese state-sponsored hacking group known as APT40 for hacking various companies, universities, and government entities in the US and worldwide between 2011 and 2018. [...]
