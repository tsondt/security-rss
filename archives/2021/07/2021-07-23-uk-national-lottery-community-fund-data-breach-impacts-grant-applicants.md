Title: UK National Lottery Community Fund data breach impacts grant applicants
Date: 2021-07-23T12:37:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: uk-national-lottery-community-fund-data-breach-impacts-grant-applicants

[Source](https://portswigger.net/daily-swig/uk-national-lottery-community-fund-data-breach-impacts-grant-applicants){:target="_blank" rel="noopener"}

> Distributor of funding to good causes is quiet on nature of breach and number of potential victims [...]
