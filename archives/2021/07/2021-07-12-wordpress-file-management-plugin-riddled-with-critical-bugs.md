Title: WordPress File Management Plugin Riddled with Critical Bugs
Date: 2021-07-12T20:23:08+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: wordpress-file-management-plugin-riddled-with-critical-bugs

[Source](https://threatpost.com/frontend-file-manager-wordpress-bugs/167687/){:target="_blank" rel="noopener"}

> The bugs allow a range of attacks on websites, including deleting blog pages and remote code execution. [...]
