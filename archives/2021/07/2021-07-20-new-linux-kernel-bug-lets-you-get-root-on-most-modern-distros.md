Title: New Linux kernel bug lets you get root on most modern distros
Date: 2021-07-20T12:21:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Linux
Slug: new-linux-kernel-bug-lets-you-get-root-on-most-modern-distros

[Source](https://www.bleepingcomputer.com/news/security/new-linux-kernel-bug-lets-you-get-root-on-most-modern-distros/){:target="_blank" rel="noopener"}

> Unprivileged attackers can gain root privileges by exploiting a local privilege escalation (LPE) vulnerability in default configurations of the Linux Kernel's filesystem layer on vulnerable devices. [...]
