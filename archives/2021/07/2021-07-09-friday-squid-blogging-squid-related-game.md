Title: Friday Squid Blogging: Squid-Related Game
Date: 2021-07-09T21:03:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: games;squid
Slug: friday-squid-blogging-squid-related-game

[Source](https://www.schneier.com/blog/archives/2021/07/friday-squid-blogging-squid-related-game.html){:target="_blank" rel="noopener"}

> It’s called “ Squid Fishering.” As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
