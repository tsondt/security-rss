Title: Guess Fashion Brand Deals With Data Loss After Ransomware Attack
Date: 2021-07-13T20:10:32+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Malware
Slug: guess-fashion-brand-deals-with-data-loss-after-ransomware-attack

[Source](https://threatpost.com/guess-fashion-data-loss-ransomware/167754/){:target="_blank" rel="noopener"}

> An attack on Guess compromised the personal and banking data of 1,300 victims. [...]
