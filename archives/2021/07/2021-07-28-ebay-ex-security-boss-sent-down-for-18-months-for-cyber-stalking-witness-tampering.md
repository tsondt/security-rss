Title: eBay ex-security boss sent down for 18 months for cyber-stalking, witness tampering
Date: 2021-07-28T00:26:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: ebay-ex-security-boss-sent-down-for-18-months-for-cyber-stalking-witness-tampering

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/28/ebay_security_prison/){:target="_blank" rel="noopener"}

> Four others at online tat bazaar admit trying to silence newsletter couple, two senior execs fight charges The former global security manager for eBay was sentenced on Tuesday to 18 months in prison and was ordered to pay a $15,000 fine for his role in the cyber-stalking and harassment of a Massachusetts couple who published a newsletter critical of the internet yard sale.... [...]
