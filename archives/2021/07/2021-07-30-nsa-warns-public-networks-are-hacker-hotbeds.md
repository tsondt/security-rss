Title: NSA Warns Public Networks are Hacker Hotbeds
Date: 2021-07-30T21:06:23+00:00
Author: Becky Bracken
Category: Threatpost
Tags: IoT;Malware;Vulnerabilities;Web Security
Slug: nsa-warns-public-networks-are-hacker-hotbeds

[Source](https://threatpost.com/nsa-warns-public-networks-are-hacker-hotbeds/168268/){:target="_blank" rel="noopener"}

> Agency warns attackers targeting teleworkers to steal corporate data. [...]
