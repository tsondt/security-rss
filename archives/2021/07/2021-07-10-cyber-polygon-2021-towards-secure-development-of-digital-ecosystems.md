Title: Cyber Polygon 2021: Towards Secure Development of Digital Ecosystems
Date: 2021-07-10T13:00:01+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Sponsored;Web Security
Slug: cyber-polygon-2021-towards-secure-development-of-digital-ecosystems

[Source](https://threatpost.com/cyber-polygon-2021-towards-secure-development-of-digital-ecosystems/167661/){:target="_blank" rel="noopener"}

> Cybersecurity is one of the most important topics on the global agenda, boosted by the pandemic. As the global digitalisation is further accelerating, the world is becoming ever more interconnected. Digital ecosystems are being created all around us: countries, corporations and individuals are taking advantage of the rapid spread of the Internet and smart devices. In this context, a single vulnerable link is enough to bring down the entire system, just like the domino effect. [...]
