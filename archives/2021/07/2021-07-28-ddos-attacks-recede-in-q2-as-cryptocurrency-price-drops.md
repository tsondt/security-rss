Title: DDoS attacks recede in Q2 as cryptocurrency price drops
Date: 2021-07-28T15:57:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ddos-attacks-recede-in-q2-as-cryptocurrency-price-drops

[Source](https://portswigger.net/daily-swig/ddos-attacks-recede-in-q2-as-cryptocurrency-price-drops){:target="_blank" rel="noopener"}

> Quiet before the storm? [...]
