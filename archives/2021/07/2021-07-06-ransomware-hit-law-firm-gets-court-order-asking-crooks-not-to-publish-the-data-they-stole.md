Title: Ransomware-hit law firm gets court order asking crooks not to publish the data they stole
Date: 2021-07-06T18:33:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ransomware-hit-law-firm-gets-court-order-asking-crooks-not-to-publish-the-data-they-stole

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/06/ransomware_4_new_square_chambers/){:target="_blank" rel="noopener"}

> Good luck with that, 4 New Square Chambers A barristers' chambers hit by a ransomware attack has responded by getting a court order demanding the criminals do not share stolen data.... [...]
