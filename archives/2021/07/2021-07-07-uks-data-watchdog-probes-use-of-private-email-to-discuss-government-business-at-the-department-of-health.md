Title: UK's data watchdog probes use of private email to discuss government business at the Department of Health
Date: 2021-07-07T12:15:09+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: uks-data-watchdog-probes-use-of-private-email-to-discuss-government-business-at-the-department-of-health

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/07/ico_dhsc_private_email/){:target="_blank" rel="noopener"}

> Information Commissioner cites loss of transparency as reason for inquiry The UK's Information Commissioner's Office (ICO) has opened an official inquiry into the misuse of private email accounts at the Department of Health and Social Care (DHSC).... [...]
