Title: Verified: UK.gov launching plans for yet another digital identity scheme
Date: 2021-07-20T12:01:10+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: verified-ukgov-launching-plans-for-yet-another-digital-identity-scheme

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/20/government_launches_plans_for_digital/){:target="_blank" rel="noopener"}

> Because Verify was roaring success and cos digi ID will 'reduce cases of online fraud'. Ahem The UK government is launching proposals to boost the legal status of digital identities, something it claims will ensure they are trusted as much as physical documents such as passports.... [...]
