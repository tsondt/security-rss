Title: Reboot of PunkSpider Tool at DEF CON Stirs Debate
Date: 2021-07-28T17:44:50+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security
Slug: reboot-of-punkspider-tool-at-def-con-stirs-debate

[Source](https://threatpost.com/punkspider-def-con-debate/168223/){:target="_blank" rel="noopener"}

> Researchers plan to introduce a revamp of PunkSpider, which helps identify flaws in websites so companies can make their back-end systems more secure, at DEF CON. [...]
