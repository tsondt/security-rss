Title: <span>REvil ransomware attackers demand $70m following Kaseya VSA supply chain attack&nbsp;</span>
Date: 2021-07-05T16:13:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: revil-ransomware-attackers-demand-70m-following-kaseya-vsa-supply-chain-attack

[Source](https://portswigger.net/daily-swig/revil-ransomware-attackers-demand-70m-following-kaseya-vsa-supply-chain-attack-nbsp){:target="_blank" rel="noopener"}

> Cybercrime gang exploited zero-day flaws [...]
