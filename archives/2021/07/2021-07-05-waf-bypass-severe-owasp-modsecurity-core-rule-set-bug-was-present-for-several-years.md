Title: WAF bypass: ‘Severe’ OWASP ModSecurity Core Rule Set bug was present for several years
Date: 2021-07-05T12:31:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: waf-bypass-severe-owasp-modsecurity-core-rule-set-bug-was-present-for-several-years

[Source](https://portswigger.net/daily-swig/waf-bypass-severe-owasp-modsecurity-core-rule-set-bug-was-present-for-several-years){:target="_blank" rel="noopener"}

> High-scoring bug went unnoticed due to time and money constraints [...]
