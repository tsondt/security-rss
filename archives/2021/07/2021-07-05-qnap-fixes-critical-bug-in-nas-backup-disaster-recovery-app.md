Title: QNAP fixes critical bug in NAS backup, disaster recovery app
Date: 2021-07-05T14:48:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qnap-fixes-critical-bug-in-nas-backup-disaster-recovery-app

[Source](https://www.bleepingcomputer.com/news/security/qnap-fixes-critical-bug-in-nas-backup-disaster-recovery-app/){:target="_blank" rel="noopener"}

> Taiwan-based network-attached storage (NAS) maker QNAP has addressed a critical security vulnerability enabling attackers to compromise vulnerable NAS devices' security. [...]
