Title: The Week in Ransomware - July 23rd 2021 - Kaseya decrypted
Date: 2021-07-23T14:33:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-july-23rd-2021-kaseya-decrypted

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-july-23rd-2021-kaseya-decrypted/){:target="_blank" rel="noopener"}

> This week has quite a bit of news ranging from the USA formally accusing China of the recent ProxyLogon vulnerability and Kaseya mysteriously obtaining the universal decryption key. [...]
