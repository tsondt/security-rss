Title: Healthcare data breach: 2.4m records potentially exposed at Forefront Dermatology
Date: 2021-07-15T15:45:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: healthcare-data-breach-24m-records-potentially-exposed-at-forefront-dermatology

[Source](https://portswigger.net/daily-swig/healthcare-data-breach-2-4m-records-potentially-exposed-at-forefront-dermatology){:target="_blank" rel="noopener"}

> Wisconsin-based organization says unauthorized intrusion impacts staff, patient info [...]
