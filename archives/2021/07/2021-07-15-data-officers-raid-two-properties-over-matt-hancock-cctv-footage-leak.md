Title: Data officers raid two properties over Matt Hancock CCTV footage leak
Date: 2021-07-15T13:04:14+00:00
Author: Jim Waterson
Category: The Guardian
Tags: Matt Hancock;Media;Newspapers;Politics;Newspapers & magazines;Coronavirus;Data protection;Data and computer security;Health policy;Digital media;Surveillance
Slug: data-officers-raid-two-properties-over-matt-hancock-cctv-footage-leak

[Source](https://www.theguardian.com/politics/2021/jul/15/data-officers-raid-two-properties-over-matt-hancock-cctv-footage-leak){:target="_blank" rel="noopener"}

> Computer equipment and electronic devices seized in connection with images of minister kissing aide Two residential properties in the south of England have been raided by data protection officers, as part of their investigation into who leaked CCTV footage of Matt Hancock kissing an aide in his office. The Information Commissioner’s Office (ICO) said it had seized computer equipment and electronic devices as part of the operation on Thursday morning, amid an ongoing investigation into alleged breaches of the Data Protection Act. Continue reading... [...]
