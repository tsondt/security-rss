Title: Kaseya denies ransomware payment as it hails ‘100% effective’ decryption tool
Date: 2021-07-27T14:00:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: kaseya-denies-ransomware-payment-as-it-hails-100-effective-decryption-tool

[Source](https://portswigger.net/daily-swig/kaseya-denies-ransomware-payment-as-it-hails-100-effective-decryption-tool){:target="_blank" rel="noopener"}

> ‘We are confirming in no uncertain terms that Kaseya did not pay a ransom’ [...]
