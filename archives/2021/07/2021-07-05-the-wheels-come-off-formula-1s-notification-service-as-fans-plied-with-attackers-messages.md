Title: The wheels come off Formula 1's notification service as fans plied with attacker's messages
Date: 2021-07-05T20:01:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: the-wheels-come-off-formula-1s-notification-service-as-fans-plied-with-attackers-messages

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/05/f1_notification_attack/){:target="_blank" rel="noopener"}

> 'Foo' – not the noise of a passing car The world of Formula 1 racing was livened up over the weekend as the sport's official app sent out some unexpected notifications on the eve of the Austrian Grand Prix.... [...]
