Title: Western Digital Users Face Another RCE
Date: 2021-07-06T17:01:57+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security
Slug: western-digital-users-face-another-rce

[Source](https://threatpost.com/rce-0-day-western-digital-users/167547/){:target="_blank" rel="noopener"}

> Say hello to one more zero-day and yet more potential remote data death for those who can’t/won’t upgrade their My Cloud storage devices. [...]
