Title: Linux Variant of HelloKitty Ransomware Targets VMware ESXi Servers
Date: 2021-07-16T21:10:20+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: linux-variant-of-hellokitty-ransomware-targets-vmware-esxi-servers

[Source](https://threatpost.com/linux-variant-of-hellokitty-ransomware-targets-vmware-esxi-servers/167883/){:target="_blank" rel="noopener"}

> HelloKitty joins the growing list of ransomware bigwigs going after the juicy target of VMware ESXi, where one hit gets scads of VMs. [...]
