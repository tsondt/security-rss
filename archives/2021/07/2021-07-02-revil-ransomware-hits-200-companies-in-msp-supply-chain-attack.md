Title: REvil ransomware hits 200 companies in MSP supply-chain attack
Date: 2021-07-02T15:56:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: revil-ransomware-hits-200-companies-in-msp-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-hits-200-companies-in-msp-supply-chain-attack/){:target="_blank" rel="noopener"}

> A massive REvil ransomware attack affects multiple managed service providers and their clients through a reported Kaseya supply-chain attack. [...]
