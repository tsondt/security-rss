Title: This is the data watchdog! Surrender your Matt Hancock smoochy-kiss pics right now!
Date: 2021-07-15T15:45:38+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: this-is-the-data-watchdog-surrender-your-matt-hancock-smoochy-kiss-pics-right-now

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/15/ico_matt_hancock_raids/){:target="_blank" rel="noopener"}

> ICO raids homes, seizes computers after UK Department of Health leak Two homes in South England have been searched by the Information Commissioner's Office (ICO) today after pictures of former health secretary Matt Hancock kissing a colleague appeared in a Brit newspaper.... [...]
