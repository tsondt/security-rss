Title: Microsoft: New Unpatched Bug in Windows Print Spooler
Date: 2021-07-16T11:57:53+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: microsoft-new-unpatched-bug-in-windows-print-spooler

[Source](https://threatpost.com/microsoft-unpatched-bug-windows-print-spooler/167855/){:target="_blank" rel="noopener"}

> Another vulnerability separate from PrintNightmare allows for local elevation of privilege and system takeover. [...]
