Title: UK and chums call out Chinese Ministry of State Security for Hafnium Microsoft Exchange Server attacks
Date: 2021-07-19T14:52:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-and-chums-call-out-chinese-ministry-of-state-security-for-hafnium-microsoft-exchange-server-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/19/hafnium_china_state_security/){:target="_blank" rel="noopener"}

> And US indicts four Chinese spies on suspicion of the hacks The Microsoft Exchange Server attacks earlier this year were "systemic cyber sabotage" carried out by Chinese state hacking crews including private contractors working for a spy agency, the British government has said.... [...]
