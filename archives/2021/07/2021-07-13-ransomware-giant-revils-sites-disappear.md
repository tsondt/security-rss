Title: Ransomware Giant REvil’s Sites Disappear
Date: 2021-07-13T20:51:53+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: ransomware-giant-revils-sites-disappear

[Source](https://threatpost.com/ransomware-revil-sites-disappears/167745/){:target="_blank" rel="noopener"}

> Just days after President Biden demanded that Russian President Putin shut down ransomware groups, the servers of one of the biggest groups mysteriously went dark. [...]
