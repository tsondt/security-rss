Title: Google Chrome now comes with up to 50x faster phishing detection
Date: 2021-07-21T08:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: google-chrome-now-comes-with-up-to-50x-faster-phishing-detection

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-now-comes-with-up-to-50x-faster-phishing-detection/){:target="_blank" rel="noopener"}

> Google Chrome now comes with up to 50 times faster phishing detection starting with the latest released version 92, promoted to the stable channel on Tuesday. [...]
