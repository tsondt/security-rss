Title: IT for service providers biz Kaseya defers decision about SaaS restoration following supply chain attack
Date: 2021-07-05T04:01:03+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: it-for-service-providers-biz-kaseya-defers-decision-about-saas-restoration-following-supply-chain-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/05/kaseya_vsa_update/){:target="_blank" rel="noopener"}

> REvil ransomware rampages through managed services providers and perhaps 1,000 clients IT management software provider Kaseya has deferred an announcement about restoration of its SaaS services, after falling victim to a supply chain attack that has seen its products become a delivery mechanism for the REvil ransomware.... [...]
