Title: The Week in Ransomware - July 30th 2021 - €1 billion saved
Date: 2021-07-30T19:43:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-july-30th-2021-eur1-billion-saved

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-july-30th-2021-1-billion-saved/){:target="_blank" rel="noopener"}

> Ransomware continues to be active this week, with new threat actors releasing new features, No More Ransom turning five, and a veteran group rebrands. [...]
