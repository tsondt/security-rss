Title: Modernizing SOC ...  Introducing Autonomic Security Operations
Date: 2021-07-21T16:00:00+00:00
Author: Anton Chuvakin
Category: GCP Security
Tags: Identity & Security
Slug: modernizing-soc-introducing-autonomic-security-operations

[Source](https://cloud.google.com/blog/products/identity-security/modernizing-soc-introducing-autonomic-security-operations/){:target="_blank" rel="noopener"}

> Modernizing your Security Operations practice to protect against today’s and tomorrow’s threats is a significant undertaking that involves transforming how people approach security challenges, how workflows are engineered to achieve secure outcomes, and how technologies can be leveraged to maximize their value. Today, we’re thrilled to announce Autonomic Security Operations, a prescriptive solution to guide our customers through this modernization journey. Autonomic Security Operations is a stack of products, integrations, blueprints, technical content, and an accelerator program to enable customers to take advantage of our best-in-class technology stack built on Chronicle and Google’s deep security operations expertise. The solution delivers [...]
