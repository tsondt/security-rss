Title: Widespread Brute-Force Attacks Tied to Russia’s APT28
Date: 2021-07-02T16:14:14+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Government;Malware;Vulnerabilities
Slug: widespread-brute-force-attacks-tied-to-russias-apt28

[Source](https://threatpost.com/kubernetes-brute-force-attacks-russia-apt28/167518/){:target="_blank" rel="noopener"}

> The ongoing attacks are targeting cloud services such as Office 365 to steal passwords and password-spray a vast range of targets, including in U.S. and European governments and military. [...]
