Title: With software supply chain security, developers have a big role to play
Date: 2021-07-27T16:00:00+00:00
Author: Pali Bhat
Category: GCP Security
Tags: Identity & Security;Google Cloud;Application Development
Slug: with-software-supply-chain-security-developers-have-a-big-role-to-play

[Source](https://cloud.google.com/blog/products/application-development/best-practices-and-tools-for-software-supply-chain-security/){:target="_blank" rel="noopener"}

> When it comes to security headlines, 2021 has unfortunately been one for the record books. Colonial Pipeline, which supplies nearly half the United States East Coast’s gasoline, was the victim of a ransomware attack that forced it to take down its systems. Several high-profile breaches, on Kaseya, SolarWinds, Codecov, and others, gained global attention. To strengthen the U.S.’s cybersecurity profile, President Biden signed an executive order mandating changes for companies that do business with the federal government about how to secure their software. While traditional security efforts have centered around securing the perimeter, the responsibility for security is increasingly falling [...]
