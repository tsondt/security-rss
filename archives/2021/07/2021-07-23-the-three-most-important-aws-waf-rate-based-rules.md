Title: The three most important AWS WAF rate-based rules
Date: 2021-07-23T00:00:26+00:00
Author: Artem Lovan
Category: AWS Security
Tags: Advanced (300);Amazon Athena;AWS WAF;Security, Identity, & Compliance;AWS Shield;AWS Threat Research Team;Security Blog
Slug: the-three-most-important-aws-waf-rate-based-rules

[Source](https://aws.amazon.com/blogs/security/three-most-important-aws-waf-rate-based-rules/){:target="_blank" rel="noopener"}

> In this post, we explain what the three most important AWS WAF rate-based rules are for proactively protecting your web applications against common HTTP flood events, and how to implement these rules. We share what the Shield Response Team (SRT) has learned from helping customers respond to HTTP floods and show how all AWS WAF [...]
