Title: Multiple encryption flaws uncovered in Telegram messaging protocol
Date: 2021-07-22T14:56:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: multiple-encryption-flaws-uncovered-in-telegram-messaging-protocol

[Source](https://portswigger.net/daily-swig/multiple-encryption-flaws-uncovered-in-telegram-messaging-protocol){:target="_blank" rel="noopener"}

> Vulnerabilities highlight risks of ‘knit-your-own’ crypto [...]
