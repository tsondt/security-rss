Title: US offers Julian Assange time in Australian prison instead of American supermax if he loses London extradition fight
Date: 2021-07-08T19:45:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: us-offers-julian-assange-time-in-australian-prison-instead-of-american-supermax-if-he-loses-london-extradition-fight

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/08/assange_us_to_appeal_extradition_ruling/){:target="_blank" rel="noopener"}

> Appeal against January decision to be heard by High Court Julian Assange will remain in a British prison for now after the US government won permission to appeal against a January court ruling that freed him from extradition to America.... [...]
