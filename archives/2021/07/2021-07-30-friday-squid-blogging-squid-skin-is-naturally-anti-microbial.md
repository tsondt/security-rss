Title: Friday Squid Blogging: Squid Skin Is Naturally Anti-microbial
Date: 2021-07-30T21:13:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-squid-skin-is-naturally-anti-microbial

[Source](https://www.schneier.com/blog/archives/2021/07/friday-squid-blogging-squid-skin-is-naturally-anti-microbial.html){:target="_blank" rel="noopener"}

> Often it feels like squid just evolved better than us mammals. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
