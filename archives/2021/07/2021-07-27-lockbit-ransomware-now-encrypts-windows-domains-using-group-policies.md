Title: LockBit ransomware now encrypts Windows domains using group policies
Date: 2021-07-27T17:10:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: lockbit-ransomware-now-encrypts-windows-domains-using-group-policies

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-now-encrypts-windows-domains-using-group-policies/){:target="_blank" rel="noopener"}

> An new version of the LockBit 2.0 ransomware has been found that automates the encryption of a Windows domain using Active Directory group policies. [...]
