Title: FBI: Cybercriminals Eyeing Broadcast Disruption at Tokyo Olympics
Date: 2021-07-22T21:15:23+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Government;Hacks;Web Security
Slug: fbi-cybercriminals-eyeing-broadcast-disruption-at-tokyo-olympics

[Source](https://threatpost.com/fbi-cybercriminals-broadcast-disruption-tokyo-olympics/168063/){:target="_blank" rel="noopener"}

> Expected cyberattacks on Tokyo Olympics likely include attempts to hijack video feeds, the Feds warn. [...]
