Title: Dell Wyse Management Suite subject to database exposure, session hijacking
Date: 2021-07-08T15:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: dell-wyse-management-suite-subject-to-database-exposure-session-hijacking

[Source](https://portswigger.net/daily-swig/dell-wyse-management-suite-subject-to-database-exposure-session-hijacking){:target="_blank" rel="noopener"}

> Vulnerabilities were identified that could 'compromise administrative sessions' [...]
