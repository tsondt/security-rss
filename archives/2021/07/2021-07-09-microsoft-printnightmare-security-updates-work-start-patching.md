Title: Microsoft: PrintNightmare security updates work, start patching!
Date: 2021-07-09T02:26:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-printnightmare-security-updates-work-start-patching

[Source](https://www.bleepingcomputer.com/news/security/microsoft-printnightmare-security-updates-work-start-patching/){:target="_blank" rel="noopener"}

> Microsoft says the emergency security updates released at the start of the week correctly patch the PrintNightmare Print Spooler vulnerability for all supported Windows versions and urges users to start applying the updates as soon as possible. [...]
