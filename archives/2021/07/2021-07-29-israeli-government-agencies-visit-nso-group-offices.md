Title: Israeli Government Agencies Visit NSO Group Offices
Date: 2021-07-29T16:25:48+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware;Mobile Security;Privacy
Slug: israeli-government-agencies-visit-nso-group-offices

[Source](https://threatpost.com/government-nso-offices/168241/){:target="_blank" rel="noopener"}

> Authorities opened an investigation into the secretive Israeli security firm. [...]
