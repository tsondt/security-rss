Title: Friday Squid Blogging: The Evolution of Squid
Date: 2021-07-23T20:58:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid;video
Slug: friday-squid-blogging-the-evolution-of-squid

[Source](https://www.schneier.com/blog/archives/2021/07/friday-squid-blogging-the-evolution-of-squid.html){:target="_blank" rel="noopener"}

> Good video about the evolutionary history of squid. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
