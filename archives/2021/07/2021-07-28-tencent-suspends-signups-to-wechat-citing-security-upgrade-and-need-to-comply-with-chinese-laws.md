Title: Tencent suspends signups to WeChat, citing 'security upgrade' and need to comply with Chinese laws
Date: 2021-07-28T01:30:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: tencent-suspends-signups-to-wechat-citing-security-upgrade-and-need-to-comply-with-chinese-laws

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/28/tencent_suspends_new_wechat_accounts/){:target="_blank" rel="noopener"}

> Promises everything will be back to normal sometime in early August Chinese web giant Tencent has suspended new signups to its WeChat messaging service.... [...]
