Title: Google to block logins on old Android devices starting September
Date: 2021-07-30T08:59:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-to-block-logins-on-old-android-devices-starting-september

[Source](https://www.bleepingcomputer.com/news/google/google-to-block-logins-on-old-android-devices-starting-september/){:target="_blank" rel="noopener"}

> Google is emailing Android users to let them know that they will no longer be able to sign in to their Google accounts on devices running Android 2.3.7 (Gingerbread) and lower starting late September. [...]
