Title: Entertainment tech provider D-Box recovering from ransomware attack
Date: 2021-07-29T16:30:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: entertainment-tech-provider-d-box-recovering-from-ransomware-attack

[Source](https://portswigger.net/daily-swig/entertainment-tech-provider-d-box-recovering-from-ransomware-attack){:target="_blank" rel="noopener"}

> Cyber-attack ‘limited to internal systems’ [...]
