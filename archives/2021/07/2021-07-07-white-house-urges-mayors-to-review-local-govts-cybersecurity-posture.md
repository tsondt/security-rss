Title: White House urges mayors to review local govts’ cybersecurity posture
Date: 2021-07-07T14:31:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: white-house-urges-mayors-to-review-local-govts-cybersecurity-posture

[Source](https://www.bleepingcomputer.com/news/security/white-house-urges-mayors-to-review-local-govts-cybersecurity-posture/){:target="_blank" rel="noopener"}

> Following recent ransomware attacks, Deputy National Security Advisor Anne Neuberger asked US mayors to immediately hold a meeting with the heads of state agencies to evaluate their cybersecurity posture. [...]
