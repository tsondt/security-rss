Title: The Pegasus project: why investigations like this are at the heart of the Guardian’s mission
Date: 2021-07-23T11:00:52+00:00
Author: Katharine Viner
Category: The Guardian
Tags: Investigative journalism;Technology;Malware;Data protection;Data and computer security;Privacy;Mobile phones
Slug: the-pegasus-project-why-investigations-like-this-are-at-the-heart-of-the-guardians-mission

[Source](https://www.theguardian.com/news/2021/jul/23/pegasus-project-investigations-nso-spyware-mobile-phones){:target="_blank" rel="noopener"}

> Guardian editor-in-chief Katharine Viner reflects on our recent investigation into NSO Group, which sells hacking spyware used by governments around the world, and explains why journalism like this is so vital When the Guardian’s head of investigations, Paul Lewis, first told me about a huge data leak suggesting authoritarian regimes were possibly using smartphone hacking software to target activists, politicians and journalists, perhaps the worst part is that I wasn’t particularly surprised. Related: What is Pegasus spyware and how does it hack phones? Related: Huge data leak shatters lie that the innocent need not fear surveillance Related: The Pegasus project [...]
