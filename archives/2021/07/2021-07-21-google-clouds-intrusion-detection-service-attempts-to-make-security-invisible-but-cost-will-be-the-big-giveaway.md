Title: Google Cloud's Intrusion Detection Service attempts to make security 'invisible' but cost will be the big giveaway
Date: 2021-07-21T17:25:04+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: google-clouds-intrusion-detection-service-attempts-to-make-security-invisible-but-cost-will-be-the-big-giveaway

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/21/google_cloud_intrusion_detection_system/){:target="_blank" rel="noopener"}

> Fancy new system shown off at online summit Google has introduced a new Intrusion Detection Service together with "Adaptive Protection" for its cloud firewall, but such services make security a costly feature.... [...]
