Title: NPM Package Steals Passwords via Chrome’s Account-Recovery Tool
Date: 2021-07-21T18:11:31+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: npm-package-steals-passwords-via-chromes-account-recovery-tool

[Source](https://threatpost.com/npm-package-steals-chrome-passwords/168004/){:target="_blank" rel="noopener"}

> In another vast software supply-chain attack, the password-stealer is filching credentials from Chrome on Windows systems via ChromePass. [...]
