Title: 'Woefully insufficient': Biden administration's assessment of critical infrastructure infosec protection
Date: 2021-07-29T05:15:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: woefully-insufficient-biden-administrations-assessment-of-critical-infrastructure-infosec-protection

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/29/biden_memo_on_critical_infrastructure_control_systems_security/){:target="_blank" rel="noopener"}

> Memorandum details plans to turn that around with rapid development of security baselines, not mandates The Biden administration has issued a National Security Memorandum on Improving Cybersecurity for Critical Infrastructure Control Systems to address what it describes as a "woefully insufficient" security posture.... [...]
