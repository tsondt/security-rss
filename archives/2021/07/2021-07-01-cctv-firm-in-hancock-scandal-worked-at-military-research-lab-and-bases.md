Title: CCTV firm in Hancock scandal worked at military research lab and bases
Date: 2021-07-01T10:37:39+00:00
Author: Rob Davies
Category: The Guardian
Tags: Matt Hancock;Surveillance;UK security and counter-terrorism;Politics;Data and computer security;UK news;House of Commons;Business
Slug: cctv-firm-in-hancock-scandal-worked-at-military-research-lab-and-bases

[Source](https://www.theguardian.com/politics/2021/jul/01/security-provider-for-matt-hancocks-office-worked-at-porton-down){:target="_blank" rel="noopener"}

> Exclusive: US company Emcor manages CCTV and security, and has worked at Porton Down A firm that provided security at Matt Hancock’s office, where leaked CCTV footage showed him kissing an aide, has also worked at the Porton Down defence research laboratory, RAF bases, and a military bunker that would house ministers in the event of a nuclear strike. Labour called for an immediate audit of government contracts after documents reviewed by the Guardian showed that Emcor provided “facilities management” services at a range of highly sensitive sites, as well as the health department (DHSC). Related: Ministers should not have [...]
