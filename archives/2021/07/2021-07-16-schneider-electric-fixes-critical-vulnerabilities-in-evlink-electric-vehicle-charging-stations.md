Title: Schneider Electric fixes critical vulnerabilities in EVlink electric vehicle charging stations
Date: 2021-07-16T12:11:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: schneider-electric-fixes-critical-vulnerabilities-in-evlink-electric-vehicle-charging-stations

[Source](https://portswigger.net/daily-swig/schneider-electric-fixes-critical-vulnerabilities-in-evlink-electric-vehicle-charging-stations){:target="_blank" rel="noopener"}

> Security flaws could allow an attacker to receive free vehicle charges, or lock up the charging station completely [...]
