Title: Microsoft Issues Windows 10 Workaround Fix for ‘SeriousSAM’ Bug
Date: 2021-07-22T12:57:11+00:00
Author: Tom Spring
Category: Threatpost
Tags: Hacks;Vulnerabilities
Slug: microsoft-issues-windows-10-workaround-fix-for-serioussam-bug

[Source](https://threatpost.com/win-10-serioussam/168034/){:target="_blank" rel="noopener"}

> A privilege elevation bug in Windows 10 opens all systems to attackers to access data and create new accounts on systems. [...]
