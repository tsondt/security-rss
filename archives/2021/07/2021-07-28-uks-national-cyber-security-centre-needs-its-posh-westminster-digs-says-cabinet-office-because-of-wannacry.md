Title: UK's National Cyber Security Centre needs its posh Westminster digs, says Cabinet Office, because of WannaCry
Date: 2021-07-28T07:30:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uks-national-cyber-security-centre-needs-its-posh-westminster-digs-says-cabinet-office-because-of-wannacry

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/28/ncsc_cabinet_office/){:target="_blank" rel="noopener"}

> May need to upgrade 'bunfight' into 'cake-flinging war' over this one Parliamentary criticism of the National Cyber Security Centre's "image over cost" London HQ is being shrugged off by the government because of the GCHQ offshoot's successful response to the WannaCry ransomware outbreak.... [...]
