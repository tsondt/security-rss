Title: Fortinet fixes bug letting unauthenticated hackers run code as root
Date: 2021-07-20T06:47:16-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: fortinet-fixes-bug-letting-unauthenticated-hackers-run-code-as-root

[Source](https://www.bleepingcomputer.com/news/security/fortinet-fixes-bug-letting-unauthenticated-hackers-run-code-as-root/){:target="_blank" rel="noopener"}

> Fortinet has released updates for its FortiManager and FortiAnalyzer network management solutions to fix a serious vulnerability that could be exploited to execute arbitrary code with the highest privileges. [...]
