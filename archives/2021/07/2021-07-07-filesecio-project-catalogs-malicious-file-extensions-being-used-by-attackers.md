Title: Filesec.io project catalogs malicious file extensions being used by attackers
Date: 2021-07-07T10:54:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: filesecio-project-catalogs-malicious-file-extensions-being-used-by-attackers

[Source](https://portswigger.net/daily-swig/filesec-io-project-catalogs-malicious-file-extensions-being-used-by-attackers){:target="_blank" rel="noopener"}

> Sort your.dats from your.dmgs [...]
