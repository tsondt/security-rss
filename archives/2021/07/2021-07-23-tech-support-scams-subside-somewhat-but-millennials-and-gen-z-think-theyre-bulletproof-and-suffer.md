Title: Tech support scams subside somewhat, but Millennials and Gen Z think they're bulletproof and suffer
Date: 2021-07-23T07:54:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: tech-support-scams-subside-somewhat-but-millennials-and-gen-z-think-theyre-bulletproof-and-suffer

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/23/microsoft_tech_support_scam_research/){:target="_blank" rel="noopener"}

> Microsoft study says India is most susceptible, other studies suggest the USA cops it most Tech support scam attempts dropped in frequency over the past two years, but remain a threat. And Millennials and Gen Z – not Boomers – fall prey most frequently, according to Microsoft in its 2021 Global Tech Support Scam Research report, released Thursday.... [...]
