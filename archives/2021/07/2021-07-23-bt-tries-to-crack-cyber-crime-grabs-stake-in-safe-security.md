Title: BT tries to crack cyber crime, grabs stake in Safe Security
Date: 2021-07-23T05:56:05+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: bt-tries-to-crack-cyber-crime-grabs-stake-in-safe-security

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/23/bt_safe_security/){:target="_blank" rel="noopener"}

> Spends £££ on Silicon Valley cyber risk management firm BT is looking to cash in on ever-growing global concerns over digital crime, and has confirmed making a multi million pound investment in US-based cyber risk management firm Safe Security.... [...]
