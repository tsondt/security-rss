Title: Sensitive medical data of cancer patients at Jefferson Health potentially breached following third-party hack
Date: 2021-07-21T15:55:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: sensitive-medical-data-of-cancer-patients-at-jefferson-health-potentially-breached-following-third-party-hack

[Source](https://portswigger.net/daily-swig/sensitive-medical-data-of-cancer-patients-at-jefferson-health-potentially-breached-following-third-party-hack){:target="_blank" rel="noopener"}

> Security incident at Elekta could have exposed the records of even more US citizens [...]
