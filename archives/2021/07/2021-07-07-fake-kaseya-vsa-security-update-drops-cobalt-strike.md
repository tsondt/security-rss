Title: Fake Kaseya VSA Security Update Drops Cobalt Strike
Date: 2021-07-07T14:47:41+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: fake-kaseya-vsa-security-update-drops-cobalt-strike

[Source](https://threatpost.com/fake-kaseya-vsa-update-cobalt-strike/167587/){:target="_blank" rel="noopener"}

> Threat actors are planting Cobalt Strike backdoors by malspamming a bogus Microsoft update along with a SecurityUpdates.exe. [...]
