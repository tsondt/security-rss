Title: Fake crypto-mining Android apps net fraudsters $350k
Date: 2021-07-08T16:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: fake-crypto-mining-android-apps-net-fraudsters-350k

[Source](https://portswigger.net/daily-swig/fake-crypto-mining-android-apps-net-fraudsters-350k){:target="_blank" rel="noopener"}

> Apps – many still available on third-party stores – ‘collect money for services that don’t exist’ [...]
