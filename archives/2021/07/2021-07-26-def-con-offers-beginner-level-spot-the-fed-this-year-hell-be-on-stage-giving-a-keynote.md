Title: DEF CON offers beginner-level Spot the Fed this year: He'll be on stage giving a keynote
Date: 2021-07-26T04:09:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: def-con-offers-beginner-level-spot-the-fed-this-year-hell-be-on-stage-giving-a-keynote

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/26/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Microsoft responds to another NTLM relay attack technique, and more In brief DEF CON's "Spot the Fed" game is going to be a little easier than usual this year: the head of the US government's Homeland Security is giving a keynote.... [...]
