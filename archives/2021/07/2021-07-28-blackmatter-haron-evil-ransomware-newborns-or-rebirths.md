Title: BlackMatter & Haron: Evil Ransomware Newborns or Rebirths
Date: 2021-07-28T18:33:02+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;News;Web Security
Slug: blackmatter-haron-evil-ransomware-newborns-or-rebirths

[Source](https://threatpost.com/ransomware-gangs-haron-blackmatter/168212/){:target="_blank" rel="noopener"}

> They’re either new or old REvil & DarkSide wine in new bottles. Both have a taste for deep-pocketed targets and DarkSide-esque virtue-signaling. [...]
