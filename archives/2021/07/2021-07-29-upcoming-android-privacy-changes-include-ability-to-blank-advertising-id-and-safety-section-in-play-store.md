Title: Upcoming Android privacy changes include ability to blank advertising ID, and 'safety section' in Play store
Date: 2021-07-29T15:30:07+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: upcoming-android-privacy-changes-include-ability-to-blank-advertising-id-and-safety-section-in-play-store

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/29/android_privacy_changes/){:target="_blank" rel="noopener"}

> New policies give users more control, but ad tracking still on by default Google has shared details of upcoming changes to Android including the ability to blank a device's advertising ID, and a new safety section for apps in the Play store.... [...]
