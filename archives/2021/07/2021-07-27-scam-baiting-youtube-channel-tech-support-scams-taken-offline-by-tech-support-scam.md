Title: Scam-baiting YouTube channel Tech Support Scams taken offline by tech support scam
Date: 2021-07-27T19:44:12+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: scam-baiting-youtube-channel-tech-support-scams-taken-offline-by-tech-support-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/27/youtube_channel_tech_scam/){:target="_blank" rel="noopener"}

> 'It was pretty convincing until the very end,' says host Jim Browning The Tech Support Scams YouTube channel has been erased from existence in a blaze of irony as host and creator Jim Browning fell victim to a tech support scam that convinced him to secure his account – by deleting it.... [...]
