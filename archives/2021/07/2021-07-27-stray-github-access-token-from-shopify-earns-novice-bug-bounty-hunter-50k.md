Title: Stray GitHub access token from Shopify earns novice bug bounty hunter $50k
Date: 2021-07-27T16:36:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: stray-github-access-token-from-shopify-earns-novice-bug-bounty-hunter-50k

[Source](https://portswigger.net/daily-swig/stray-github-access-token-from-shopify-earns-novice-bug-bounty-hunter-50k){:target="_blank" rel="noopener"}

> Programming credential that gave access to Shopify repos wasn’t abused, audit reveals [...]
