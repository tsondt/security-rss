Title: Firefox 90 adds enhanced tracker blocking to private browsing
Date: 2021-07-13T10:17:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: firefox-90-adds-enhanced-tracker-blocking-to-private-browsing

[Source](https://www.bleepingcomputer.com/news/security/firefox-90-adds-enhanced-tracker-blocking-to-private-browsing/){:target="_blank" rel="noopener"}

> Mozilla has introduced SmartBlock 2.0, the next version of its intelligent cross-site tracking blocking tech, with the release of Firefox 90. [...]
