Title: Stealing Xbox Codes
Date: 2021-07-05T11:11:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: stealing-xbox-codes

[Source](https://www.schneier.com/blog/archives/2021/07/stealing-xbox-codes.html){:target="_blank" rel="noopener"}

> Detailed story of Volodymyr Kvashuk, a Microsoft insider who noticed a bug in the company’s internal systems that allowed him to create unlimited Xbox gift cards, and stole $10.1 million before he was caught. [...]
