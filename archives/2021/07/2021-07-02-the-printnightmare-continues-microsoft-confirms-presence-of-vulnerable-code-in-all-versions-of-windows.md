Title: The PrintNightmare continues: Microsoft confirms presence of vulnerable code in all versions of Windows
Date: 2021-07-02T13:01:05+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: the-printnightmare-continues-microsoft-confirms-presence-of-vulnerable-code-in-all-versions-of-windows

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/02/printnightmare_cve/){:target="_blank" rel="noopener"}

> That printer plugged into your domain controller? Yeah, you might not be using that for a while Microsoft has assigned CVE-2021-34527 to the print spooler remote code execution vulnerability known as "PrintNightmare" and confirmed that the offending code is lurking in all versions of Windows.... [...]
