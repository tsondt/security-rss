Title: Kaseya patches VSA vulnerabilities used in REvil ransomware attack
Date: 2021-07-11T16:50:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: kaseya-patches-vsa-vulnerabilities-used-in-revil-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/kaseya-patches-vsa-vulnerabilities-used-in-revil-ransomware-attack/){:target="_blank" rel="noopener"}

> Kaseya has released a security update for the VSA zero-day vulnerabilities used by the REvil ransomware gang to attack MSPs and their customers. [...]
