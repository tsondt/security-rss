Title: New CISA Director Confirmed, White House Gains Cyber-Director
Date: 2021-07-13T14:36:47+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Hacks;IoT;Malware;Vulnerabilities
Slug: new-cisa-director-confirmed-white-house-gains-cyber-director

[Source](https://threatpost.com/cisa-director-confirmed-white-house-cyber-director/167710/){:target="_blank" rel="noopener"}

> Jen Easterly, former NSA official and Morgan Stanley vet, will take up the lead at CISA as the ransomware scourge rages on. [...]
