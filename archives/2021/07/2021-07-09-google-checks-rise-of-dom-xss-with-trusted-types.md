Title: Google checks rise of DOM XSS with Trusted Types
Date: 2021-07-09T12:35:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-checks-rise-of-dom-xss-with-trusted-types

[Source](https://portswigger.net/daily-swig/google-checks-rise-of-dom-xss-with-trusted-types){:target="_blank" rel="noopener"}

> Internal deployment has nullified elusive, complex threat since 2019 [...]
