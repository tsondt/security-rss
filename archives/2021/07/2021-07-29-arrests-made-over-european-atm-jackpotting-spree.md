Title: Arrests made over European ATM ‘jackpotting’ spree
Date: 2021-07-29T14:04:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: arrests-made-over-european-atm-jackpotting-spree

[Source](https://portswigger.net/daily-swig/arrests-made-over-european-atm-jackpotting-spree){:target="_blank" rel="noopener"}

> Attacks combined physical and digital exploits to land criminals $273,000 [...]
