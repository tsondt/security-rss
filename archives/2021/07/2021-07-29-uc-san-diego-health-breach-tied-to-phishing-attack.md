Title: UC San Diego Health Breach Tied to Phishing Attack
Date: 2021-07-29T19:16:50+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Privacy
Slug: uc-san-diego-health-breach-tied-to-phishing-attack

[Source](https://threatpost.com/uc-san-diego-health-breach/168250/){:target="_blank" rel="noopener"}

> Employee email takeover exposed personal, medical data of students, employees and patients. [...]
