Title: DevSecAI: GitHub Copilot prone to writing security flaws
Date: 2021-07-14T13:49:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: devsecai-github-copilot-prone-to-writing-security-flaws

[Source](https://portswigger.net/daily-swig/devsecai-github-copilot-prone-to-writing-security-flaws){:target="_blank" rel="noopener"}

> AI pair programmer should be supervised like a toddler, says researcher [...]
