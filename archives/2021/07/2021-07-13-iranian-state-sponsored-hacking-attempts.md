Title: Iranian State-Sponsored Hacking Attempts
Date: 2021-07-13T14:04:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;hacking;Iran;phishing
Slug: iranian-state-sponsored-hacking-attempts

[Source](https://www.schneier.com/blog/archives/2021/07/iranian-state-sponsored-hacking-attempts.html){:target="_blank" rel="noopener"}

> Interesting attack : Masquerading as UK scholars with the University of London’s School of Oriental and African Studies (SOAS), the threat actor TA453 has been covertly approaching individuals since at least January 2021 to solicit sensitive information. The threat actor, an APT who we assess with high confidence supports Islamic Revolutionary Guard Corps (IRGC) intelligence collection efforts, established backstopping for their credential phishing infrastructure by compromising a legitimate site of a highly regarded academic institution to deliver personalized credential harvesting pages disguised as registration links. Identified targets included experts in Middle Eastern affairs from think tanks, senior professors from well-known [...]
