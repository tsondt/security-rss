Title: Fake Zoom App Dropped by New APT ‘LuminousMoth’
Date: 2021-07-15T15:49:57+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: fake-zoom-app-dropped-by-new-apt-luminousmoth

[Source](https://threatpost.com/zoom-apt-luminous-moth/167822/){:target="_blank" rel="noopener"}

> First comes spear-phishing, next download of malicious DLLs that spread to removable USBs, dropping Cobalt Strike Beacon, and then, sometimes, a fake Zoom app. [...]
