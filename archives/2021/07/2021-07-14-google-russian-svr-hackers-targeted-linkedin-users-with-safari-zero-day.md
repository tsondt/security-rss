Title: Google: Russian SVR hackers targeted LinkedIn users with Safari zero-day
Date: 2021-07-14T12:56:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-russian-svr-hackers-targeted-linkedin-users-with-safari-zero-day

[Source](https://www.bleepingcomputer.com/news/security/google-russian-svr-hackers-targeted-linkedin-users-with-safari-zero-day/){:target="_blank" rel="noopener"}

> Google security researcher shared more information on four security vulnerabilities also known as zero-days, unknown before they discovered them being exploited in the wild earlier this year. [...]
