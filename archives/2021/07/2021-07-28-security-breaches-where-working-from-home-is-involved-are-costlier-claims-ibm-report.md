Title: Security breaches where working from home is involved are costlier, claims IBM report
Date: 2021-07-28T19:47:09+00:00
Author: Tim Richardson
Category: The Register
Tags: 
Slug: security-breaches-where-working-from-home-is-involved-are-costlier-claims-ibm-report

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/28/cost_of_a_data_breach_report_2021/){:target="_blank" rel="noopener"}

> Great, it's not like employers need more reasons to haul you back to the office Firms looking to save money by shifting to more flexible ways of working will need to think carefully about IT security and the additional cost of breaches linked to staff working from home.... [...]
