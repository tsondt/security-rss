Title: Journo who went to prison for 2 years for breaking US cyber-security law is jailed again
Date: 2021-07-20T23:56:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: journo-who-went-to-prison-for-2-years-for-breaking-us-cyber-security-law-is-jailed-again

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/20/journalist_prison_offending/){:target="_blank" rel="noopener"}

> Deletion of employer's YouTube account deemed violation of release Former journalist Matthew Keys, who served two years in prison for posting his Tribune Company content management system credentials online a decade ago in violation of America's Computer Fraud and Abuse Act, has been ordered back to prison for violating the terms of his supervised release.... [...]
