Title: Microsoft takes down domains used to scam Office 365 users
Date: 2021-07-19T14:48:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-takes-down-domains-used-to-scam-office-365-users

[Source](https://www.bleepingcomputer.com/news/security/microsoft-takes-down-domains-used-to-scam-office-365-users/){:target="_blank" rel="noopener"}

> Microsoft's Digital Crimes Unit (DCU) has seized 17 malicious domains used by scammers in a business email compromise (BEC) campaign targeting the company's customers. [...]
