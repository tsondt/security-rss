Title: You'll never Guess whose data has been nicked as US fashion firm confirms systems breach
Date: 2021-07-13T14:45:11+00:00
Author: Gareth Halfacree
Category: The Register
Tags: 
Slug: youll-never-guess-whose-data-has-been-nicked-as-us-fashion-firm-confirms-systems-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/13/guess_spread_group_data_breaches/){:target="_blank" rel="noopener"}

> Not the only one either; Spread Group admits to even worse pwnage Updated Fashion brands Guess and Spread Group have confirmed data breaches in which crooks walked off with US Social Security Numbers (SSNs), contracts, passwords, payment details, and more.... [...]
