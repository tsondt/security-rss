Title: Apple fixes bug that breaks iPhone WiFi when joining rogue hotspots
Date: 2021-07-23T14:18:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple
Slug: apple-fixes-bug-that-breaks-iphone-wifi-when-joining-rogue-hotspots

[Source](https://www.bleepingcomputer.com/news/security/apple-fixes-bug-that-breaks-iphone-wifi-when-joining-rogue-hotspots/){:target="_blank" rel="noopener"}

> Apple has rolled out iOS 14.7 earlier this week with security updates to address dozens of iOS and macOS vulnerabilities, including a severe iOS bug dubbed WiFiDemon that could lead to denial of service or arbitrary code execution. [...]
