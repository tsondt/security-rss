Title: Pro-Trump ‘Gettr’ Social Platform Hacked On Day One
Date: 2021-07-07T03:27:13+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Web Security
Slug: pro-trump-gettr-social-platform-hacked-on-day-one

[Source](https://threatpost.com/trump-gettr-social-media-hacked-day-1/167574/){:target="_blank" rel="noopener"}

> The newborn platform was inundated by Sonic the Hedgehog-themed porn and had prominent users' profiles defaced. Next, hackers posted its user database online. [...]
