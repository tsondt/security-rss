Title: Looking for hands on, in-person cybersecurity training? Well, look to London
Date: 2021-07-12T14:30:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: looking-for-hands-on-in-person-cybersecurity-training-well-look-to-london

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/12/sans_london_aug_sept_2021/){:target="_blank" rel="noopener"}

> SANS Institute touches down for August and September visits Promo If you’re going to be near London in August or September, like pitting your wits against cyber attackers, and love exchanging experiences vividly with your peers even more, here’s some really good news.... [...]
