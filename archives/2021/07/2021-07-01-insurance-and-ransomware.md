Title: Insurance and Ransomware
Date: 2021-07-01T16:01:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;cybercrime;cybersecurity;insurance;mitigation;ransomware;reports
Slug: insurance-and-ransomware

[Source](https://www.schneier.com/blog/archives/2021/07/insurance-and-ransomware.html){:target="_blank" rel="noopener"}

> As ransomware becomes more common, I’m seeing more discussions about the ethics of paying the ransom. Here’s one more contribution to that issue: a research paper that the insurance industry is hurting more than it’s helping. However, the most pressing challenge currently facing the industry is ransomware. Although it is a societal problem, cyber insurers have received considerable criticism for facilitating ransom payments to cybercriminals. These add fuel to the fire by incentivising cybercriminals’ engagement in ransomware operations and enabling existing operators to invest in and expand their capabilities. Growing losses from ransomware attacks have also emphasised that the current [...]
