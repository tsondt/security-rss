Title: Linux version of HelloKitty ransomware targets VMware ESXi servers
Date: 2021-07-15T11:13:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Linux
Slug: linux-version-of-hellokitty-ransomware-targets-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-hellokitty-ransomware-targets-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> ​The ransomware gang behind the highly publicized attack on CD Projekt Red uses a Linux variant that targets VMware's ESXi virtual machine platform for maximum damage. [...]
