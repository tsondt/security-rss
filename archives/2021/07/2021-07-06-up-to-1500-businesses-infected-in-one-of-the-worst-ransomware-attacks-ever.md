Title: Up to 1,500 businesses infected in one of the worst ransomware attacks ever
Date: 2021-07-06T20:48:12+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;cascade attack;malware;REvil;REvil ransomware
Slug: up-to-1500-businesses-infected-in-one-of-the-worst-ransomware-attacks-ever

[Source](https://arstechnica.com/?p=1778479){:target="_blank" rel="noopener"}

> Enlarge (credit: Suebsiri Srithanyarat / EyeEm / Getty Images ) As many as 1,500 businesses around the world have been infected by highly destructive malware that first struck software maker Kaseya. In one of the worst ransom attacks ever, the malware, in turn, used that access to fell Kaseya’s customers. The attack struck on Friday afternoon in the lead-up to the three-day Independence Day holiday weekend in the US. Hackers affiliated with REvil, one of ransomware’s most cutthroat gangs, exploited a zero-day vulnerability in the Kaseya VSA remote management service, which the company says is used by 35,000 customers. The [...]
