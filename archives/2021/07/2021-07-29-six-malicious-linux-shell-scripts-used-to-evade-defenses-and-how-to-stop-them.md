Title: Six Malicious Linux Shell Scripts Used to Evade Defenses and How to Stop Them
Date: 2021-07-29T13:00:27+00:00
Author: Threatpost
Category: Threatpost
Tags: Sponsored;Web Security
Slug: six-malicious-linux-shell-scripts-used-to-evade-defenses-and-how-to-stop-them

[Source](https://threatpost.com/six-malicious-linux-shell-scripts-how-to-stop-them/168127/){:target="_blank" rel="noopener"}

> Uptycs Threat Research outline how malicious Linux shell scripts are used to cloak attacks and how defenders can detect and mitigate against them. [...]
