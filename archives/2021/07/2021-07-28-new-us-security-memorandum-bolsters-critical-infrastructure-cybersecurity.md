Title: New US security memorandum bolsters critical infrastructure cybersecurity
Date: 2021-07-28T15:15:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: new-us-security-memorandum-bolsters-critical-infrastructure-cybersecurity

[Source](https://www.bleepingcomputer.com/news/security/new-us-security-memorandum-bolsters-critical-infrastructure-cybersecurity/){:target="_blank" rel="noopener"}

> US President Joe Biden today issued a national security memorandum designed to help strengthen the security of critical infrastructure by setting baseline performance goals for critical infrastructure owners and operators. [...]
