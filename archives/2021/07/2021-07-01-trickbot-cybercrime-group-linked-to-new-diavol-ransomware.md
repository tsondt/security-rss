Title: Trickbot cybercrime group linked to new Diavol ransomware
Date: 2021-07-01T16:11:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: trickbot-cybercrime-group-linked-to-new-diavol-ransomware

[Source](https://www.bleepingcomputer.com/news/security/trickbot-cybercrime-group-linked-to-new-diavol-ransomware/){:target="_blank" rel="noopener"}

> FortiGuard Labs security researchers have linked a new ransomware strain dubbed Diavol to Wizard Spider, the cybercrime group behind the Trickbot botnet. [...]
