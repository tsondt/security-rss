Title: Intuit to Share Payroll Data from 1.4M Small Businesses With Equifax
Date: 2021-07-01T18:56:42+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Acquicent;ADP;Anthony Citrano;Equifax;Equifax breach;Intuit;Intuit Online Payroll;Quickbooks Payroll;TALX;The Work Number
Slug: intuit-to-share-payroll-data-from-14m-small-businesses-with-equifax

[Source](https://krebsonsecurity.com/2021/07/intuit-to-share-payroll-data-from-1-4m-small-businesses-with-equifax/){:target="_blank" rel="noopener"}

> Financial services giant Intuit this week informed 1.4 million small businesses using its QuickBooks Online Payroll and Intuit Online Payroll products that their payroll information will be shared with big-three consumer credit bureau Equifax starting later this year unless customers opt out by the end of this month. Intuit says the change is tied to an “exciting” and “free” new service that will let millions of small business employees get easy access to employment and income verification services when they wish to apply for a loan or line of credit. “In early fall 2021, your QuickBooks Online Payroll subscription will [...]
