Title: Microsoft Rushes Fix for ‘PetitPotam’ Attack PoC
Date: 2021-07-26T19:33:34+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities
Slug: microsoft-rushes-fix-for-petitpotam-attack-poc

[Source](https://threatpost.com/microsoft-petitpotam-poc/168163/){:target="_blank" rel="noopener"}

> Microsoft releases mitigations for a Windows NT LAN Manager exploit that forces remote Windows systems to reveal password hashes that can be easily cracked. [...]
