Title: Flaw in preprocessor language Less.js causes website to leak AWS secret keys
Date: 2021-07-09T15:47:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: flaw-in-preprocessor-language-lessjs-causes-website-to-leak-aws-secret-keys

[Source](https://portswigger.net/daily-swig/flaw-in-preprocessor-language-less-js-causes-website-to-leak-aws-secret-keys){:target="_blank" rel="noopener"}

> Issues in plugin feature can leave users at risk [...]
