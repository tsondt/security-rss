Title: Kaseya says it's seen no sign of supply chain attack, sets SaaS restoration target of Tuesday afternoon, on-prem fix to follow
Date: 2021-07-06T02:24:29+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: kaseya-says-its-seen-no-sign-of-supply-chain-attack-sets-saas-restoration-target-of-tuesday-afternoon-on-prem-fix-to-follow

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/06/kaseya_update/){:target="_blank" rel="noopener"}

> Hikes numbers of known compromised customers and warns countermeasures will be needed before resuming usage Kaseya has said it’s been unable to find signs its code was maliciously modified, and offered its users a ray of hope with news that it is testing a patch for its on-prem software and is considering restoring its SaaS services on Tuesday, US Eastern Daylight Time (EDT).... [...]
