Title: Hackers use new SolarWinds zero-day to target US Defense orgs
Date: 2021-07-13T19:54:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: hackers-use-new-solarwinds-zero-day-to-target-us-defense-orgs

[Source](https://www.bleepingcomputer.com/news/microsoft/hackers-use-new-solarwinds-zero-day-to-target-us-defense-orgs/){:target="_blank" rel="noopener"}

> China-based hackers actively target US defense and software companies using a vulnerability in the SolarWinds Serv-U FTP server. [...]
