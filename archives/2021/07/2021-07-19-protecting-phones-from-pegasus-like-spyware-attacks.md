Title: Protecting Phones From Pegasus-Like Spyware Attacks
Date: 2021-07-19T17:49:50+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Mobile Security;Newsmaker Interviews;Podcasts
Slug: protecting-phones-from-pegasus-like-spyware-attacks

[Source](https://threatpost.com/protecting-phones-from-pegasus-like-spyware-attacks/167909/){:target="_blank" rel="noopener"}

> Podcast: Can a new SIM card and prepaid service from an MVNO help? Former spyware insider, current mobile white hat hacker Adam Weinberg on how to block spyware attacks. [...]
