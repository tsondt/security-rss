Title: The Week in Ransomware - July 2nd 2021 - MSPs under attack
Date: 2021-07-03T12:40:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-july-2nd-2021-msps-under-attack

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-july-2nd-2021-msps-under-attack/){:target="_blank" rel="noopener"}

> Friday afternoon, we saw the largest ransomware attack ever conducted after the REvil ransomware gang used a zero-day vulnerability in the Kaseya VSA management software to encrypt MSPs and their customers worldwide. [...]
