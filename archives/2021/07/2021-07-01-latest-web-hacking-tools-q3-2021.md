Title: Latest web hacking tools – Q3 2021
Date: 2021-07-01T13:39:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: latest-web-hacking-tools-q3-2021

[Source](https://portswigger.net/daily-swig/latest-web-hacking-tools-q3-2021){:target="_blank" rel="noopener"}

> We take a look at the latest additions to security researchers’ armoury [...]
