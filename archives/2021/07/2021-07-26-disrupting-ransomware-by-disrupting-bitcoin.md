Title: Disrupting Ransomware by Disrupting Bitcoin
Date: 2021-07-26T11:30:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: bitcoin;crime;cryptocurrency;cybercrime;law enforcement;ransomware
Slug: disrupting-ransomware-by-disrupting-bitcoin

[Source](https://www.schneier.com/blog/archives/2021/07/disrupting-ransomware-by-disrupting-bitcoin.html){:target="_blank" rel="noopener"}

> Ransomware isn’t new; the idea dates back to 1986 with the “Brain” computer virus. Now, it’s become the criminal business model of the internet for two reasons. The first is the realization that no one values data more than its original owner, and it makes more sense to ransom it back to them — sometimes with the added extortion of threatening to make it public — than it does to sell it to anyone else. The second is a safe way of collecting ransoms: bitcoin. This is where the suggestion to ban cryptocurrencies as a way to “solve” ransomware comes [...]
