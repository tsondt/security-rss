Title: Google Play Protect fails Android security tests once more
Date: 2021-07-28T11:15:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Mobile
Slug: google-play-protect-fails-android-security-tests-once-more

[Source](https://www.bleepingcomputer.com/news/security/google-play-protect-fails-android-security-tests-once-more/){:target="_blank" rel="noopener"}

> Google Play Protect, the Android built-in malware defense system, has failed the real-world tests of antivirus testing lab AV-TEST after detecting just over two thirds out of more than 20,000 malicious apps it was pitted against. [...]
