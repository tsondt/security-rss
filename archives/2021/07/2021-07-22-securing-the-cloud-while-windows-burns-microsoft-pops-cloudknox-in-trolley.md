Title: Securing the cloud while Windows burns: Microsoft pops CloudKnox in trolley
Date: 2021-07-22T17:01:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: securing-the-cloud-while-windows-burns-microsoft-pops-cloudknox-in-trolley

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/22/microsoft_cloudknox/){:target="_blank" rel="noopener"}

> At least Redmond is taking some security seriously Microsoft has snapped up cloud security outfit CloudKnox while researchers continue to poke holes in its down-to-earth Windows operating system.... [...]
