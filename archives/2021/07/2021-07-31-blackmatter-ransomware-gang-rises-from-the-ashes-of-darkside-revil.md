Title: BlackMatter ransomware gang rises from the ashes of DarkSide, REvil
Date: 2021-07-31T11:12:41-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: blackmatter-ransomware-gang-rises-from-the-ashes-of-darkside-revil

[Source](https://www.bleepingcomputer.com/news/security/blackmatter-ransomware-gang-rises-from-the-ashes-of-darkside-revil/){:target="_blank" rel="noopener"}

> ​A new ransomware gang named BlackMatter is purchasing access to corporate networks while claiming to include the best features from the notorious and now-defunct REvil and DarkSide operations. [...]
