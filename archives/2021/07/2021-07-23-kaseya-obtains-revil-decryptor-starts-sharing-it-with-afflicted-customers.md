Title: Kaseya obtains REvil decryptor, starts sharing it with afflicted customers
Date: 2021-07-23T02:15:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: kaseya-obtains-revil-decryptor-starts-sharing-it-with-afflicted-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/23/kaseya_obtains_revil_decryptor_starts/){:target="_blank" rel="noopener"}

> Users sent two further updates – one fixing an issue that prevented installation of antivirus software Software-for-services providers business Kaseya has obtained a "universal decryptor key" for the REvil ransomware and is delivering it to clients.... [...]
