Title: Operation Lyrebird: Cybercops nab Moroccan phish-and-carding kingpin
Date: 2021-07-06T17:00:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: operation-lyrebird-cybercops-nab-moroccan-phish-and-carding-kingpin

[Source](https://portswigger.net/daily-swig/operation-lyrebird-cybercops-nab-moroccan-phish-and-carding-kingpin){:target="_blank" rel="noopener"}

> ‘Dr HeX’ claimed thousands of French-speaking victims through phishing attacks [...]
