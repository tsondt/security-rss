Title: Ecuador's state-run CNT telco hit by RansomEXX ransomware
Date: 2021-07-17T09:53:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ecuadors-state-run-cnt-telco-hit-by-ransomexx-ransomware

[Source](https://www.bleepingcomputer.com/news/security/ecuadors-state-run-cnt-telco-hit-by-ransomexx-ransomware/){:target="_blank" rel="noopener"}

> Ecuador's state-run Corporación Nacional de Telecomunicación (CNT) has suffered a ransomware attack that has disrupted business operations, the payment portal, and customer support. [...]
