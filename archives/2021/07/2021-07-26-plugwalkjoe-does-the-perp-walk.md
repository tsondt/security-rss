Title: PlugwalkJoe Does the Perp Walk
Date: 2021-07-26T18:18:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;SIM Swapping;Addison Rae;Bella Thorne;Joseph James O'Connor;PlugWalkJoe;REACT Task Force;SIM swapping;Snapchat;SWATting;Tiktok;Twitter hack;U.S. Justice Department
Slug: plugwalkjoe-does-the-perp-walk

[Source](https://krebsonsecurity.com/2021/07/plugwalkjoe-does-the-perp-walk/){:target="_blank" rel="noopener"}

> Joseph “PlugwalkJoe” O’Connor, in a photo from a paid press release on Sept. 02, 2020, pitching him as a trustworthy cryptocurrency expert and advisor. One day after last summer’s mass-hack of Twitter, KrebsOnSecurity wrote that 22-year-old British citizen Joseph “PlugwalkJoe” O’Connor appeared to have been involved in the incident. When the U.S. Justice Department last week announced O’Connor’s arrest and indictment, his alleged role in the Twitter compromise was well covered in the media. But most of the coverage seems to have overlooked the far more sinister criminal charges in the indictment, which involve an underground scene wherein young men [...]
