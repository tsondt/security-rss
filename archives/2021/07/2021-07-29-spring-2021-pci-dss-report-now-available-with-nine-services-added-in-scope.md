Title: Spring 2021 PCI DSS report now available with nine services added in scope
Date: 2021-07-29T18:34:30+00:00
Author: Michael Oyeniya
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;Compliance reports;PCI;PCI DSS;Security Blog
Slug: spring-2021-pci-dss-report-now-available-with-nine-services-added-in-scope

[Source](https://aws.amazon.com/blogs/security/spring-2021-pci-dss-report-now-available-with-nine-services-added-in-scope/){:target="_blank" rel="noopener"}

> We’re continuing to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that nine new services have been added to the scope of our Payment Card Industry Data Security Standard (PCI DSS) certification. This provides our customers with more options to process and store their payment card [...]
