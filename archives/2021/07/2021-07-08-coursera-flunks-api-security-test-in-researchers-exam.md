Title: Coursera Flunks API Security Test in Researchers’ Exam
Date: 2021-07-08T18:29:41+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: coursera-flunks-api-security-test-in-researchers-exam

[Source](https://threatpost.com/coursera-flunks-api-security-test-in-researchers-exam/167630/){:target="_blank" rel="noopener"}

> The problem APIs included numero uno on the OWASP API Security Top 10: a Broken Object Level Authorization (BOLA) issue that could have exposed personal data. [...]
