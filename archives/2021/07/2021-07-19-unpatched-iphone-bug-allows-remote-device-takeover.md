Title: Unpatched iPhone Bug Allows Remote Device Takeover
Date: 2021-07-19T21:31:06+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: unpatched-iphone-bug-allows-remote-device-takeover

[Source](https://threatpost.com/unpatched-iphone-bug-remote-takeover/167922/){:target="_blank" rel="noopener"}

> A format-string bug believed to be a low-risk denial-of-service issue turns out to be much nastier than expected. [...]
