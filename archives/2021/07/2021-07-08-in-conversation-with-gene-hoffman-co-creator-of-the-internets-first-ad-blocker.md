Title: In conversation with Gene Hoffman, co-creator of the internet's first ad blocker
Date: 2021-07-08T08:30:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: in-conversation-with-gene-hoffman-co-creator-of-the-internets-first-ad-blocker

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/08/interview_gene_hoffman/){:target="_blank" rel="noopener"}

> El Reg revisits those early days of the internet and learns of the NSA tipping off Boeing about French spying Interview Gene Hoffman is one of the founders of PrivNet, which in 1996 developed Internet Fast Forward, the internet's first commercial ad blocking software. He helped found the company as a student at the University of North Carolina at Chapel Hill, with the help of fellow students Mark Elrod, Jeff Harrell, and James Howard.... [...]
