Title: Microsoft Teams now automatically blocks phishing attempts
Date: 2021-07-27T08:25:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-teams-now-automatically-blocks-phishing-attempts

[Source](https://www.bleepingcomputer.com/news/security/microsoft-teams-now-automatically-blocks-phishing-attempts/){:target="_blank" rel="noopener"}

> Microsoft has extended Defender for Office 365 Safe Links protection to Microsoft Teams to safeguard users from malicious URL-based phishing attacks. [...]
