Title: Commercial Location Data Used to Out Priest
Date: 2021-07-23T13:58:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cell phones;data collection;data mining;de-anonymization;geolocation;privacy;surveillance
Slug: commercial-location-data-used-to-out-priest

[Source](https://www.schneier.com/blog/archives/2021/07/commercial-location-data-used-to-out-priest.html){:target="_blank" rel="noopener"}

> A Catholic priest was outed through commercially available surveillance data. Vice has a good analysis : The news starkly demonstrates not only the inherent power of location data, but how the chance to wield that power has trickled down from corporations and intelligence agencies to essentially any sort of disgruntled, unscrupulous, or dangerous individual. A growing market of data brokers that collect and sell data from countless apps has made it so that anyone with a bit of cash and effort can figure out which phone in a so-called anonymized dataset belongs to a target, and abuse that information. There [...]
