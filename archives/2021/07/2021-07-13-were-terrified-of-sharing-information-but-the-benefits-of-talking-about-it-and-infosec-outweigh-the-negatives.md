Title: We're terrified of sharing information, but the benefits of talking about IT and infosec outweigh the negatives
Date: 2021-07-13T09:15:11+00:00
Author: Dave Cartwright
Category: The Register
Tags: 
Slug: were-terrified-of-sharing-information-but-the-benefits-of-talking-about-it-and-infosec-outweigh-the-negatives

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/13/sharing_information_security_feautre/){:target="_blank" rel="noopener"}

> You may find that gaffe isn't so embarrassing after all Feature When something bad happens to our systems, our applications or our security, it's almost certain that our organisation is not the first it has happened to. We won't be the first in the world, or in our industry, or in our country, or probably even in our area. Why, then, does it feel like we are?... [...]
