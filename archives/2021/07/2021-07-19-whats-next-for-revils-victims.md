Title: What’s Next for REvil’s Victims?
Date: 2021-07-19T23:12:27+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Podcasts;Web Security
Slug: whats-next-for-revils-victims

[Source](https://threatpost.com/whats-next-revil-victims/167926/){:target="_blank" rel="noopener"}

> Podcast: Nothing, says a ransomware negotiator who has tips on staying out of the sad subset of victims left in the lurch, mid-negotiation, after REvil's servers went up in smoke. [...]
