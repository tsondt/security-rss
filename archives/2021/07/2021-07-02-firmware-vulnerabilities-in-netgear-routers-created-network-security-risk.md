Title: Firmware vulnerabilities in Netgear routers created network security risk
Date: 2021-07-02T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: firmware-vulnerabilities-in-netgear-routers-created-network-security-risk

[Source](https://portswigger.net/daily-swig/firmware-vulnerabilities-in-netgear-routers-created-network-security-risk){:target="_blank" rel="noopener"}

> Authentication bypass flaw mitigated thanks to Microsoft researchers Firmware vulnerabilities in a commercial-grade Netgear router opened the door to a range of exploits, including identity theft and [...]
