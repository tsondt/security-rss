Title: Friday Squid Blogging: Best Squid-Related Headline
Date: 2021-07-02T21:06:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-best-squid-related-headline

[Source](https://www.schneier.com/blog/archives/2021/07/friday-squid-blogging-best-squid-related-headline.html){:target="_blank" rel="noopener"}

> From the New York Times : “ When an Eel Climbs a Ramp to Eat Squid From a Clamp, That’s a Moray. ” The article is about the eel; the squid is just eel food. But still.... As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
