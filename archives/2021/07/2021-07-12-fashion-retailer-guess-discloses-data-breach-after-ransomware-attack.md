Title: Fashion retailer Guess discloses data breach after ransomware attack
Date: 2021-07-12T12:33:40-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fashion-retailer-guess-discloses-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/fashion-retailer-guess-discloses-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> American fashion brand and retailer Guess is notifying affected customers of a data breach following a February ransomware attack that led to data theft. [...]
