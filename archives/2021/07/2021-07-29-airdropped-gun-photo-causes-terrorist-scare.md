Title: AirDropped Gun Photo Causes Terrorist Scare
Date: 2021-07-29T11:52:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: airdropped-gun-photo-causes-terrorist-scare

[Source](https://www.schneier.com/blog/archives/2021/07/airdropped-gun-photo-causes-terrorist-scare.html){:target="_blank" rel="noopener"}

> A teenager on an airplane sent a photo of a replica gun via AirDrop to everyone who had their settings configured to receive unsolicited photos from strangers. This caused a three-hour delay as the plane — still at the gate — was evacuated and searched. The teen was not allowed to reboard. I can’t find any information about whether he was charged with any of those vague “terrorist threat” crimes. It’s been a long time since we’ve had one of these sorts of overreactions. [...]
