Title: For years, a backdoor in popular KiwiSDR product gave root to project developer
Date: 2021-07-15T19:22:51+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;backdoors;kiwisdr;software defined radio
Slug: for-years-a-backdoor-in-popular-kiwisdr-product-gave-root-to-project-developer

[Source](https://arstechnica.com/?p=1780666){:target="_blank" rel="noopener"}

> Enlarge (credit: KiwiSDR ) A spectrum painted image made using KiwiSDR. (credit: xssfox) KiwiSDR is hardware that uses a software-defined radio to monitor transmissions in a local area and stream them over the Internet. A largely hobbyist base of users does all kinds of cool things with the playing-card-sized devices. For instance, a user in Manhattan could connect one to the Internet so that people in Madrid, Spain, or Sydney, Australia, could listen to AM radio broadcasts, CB radio conversations, or even watch lightning storms in Manhattan. On Wednesday, users learned that for years, their devices had been equipped with [...]
