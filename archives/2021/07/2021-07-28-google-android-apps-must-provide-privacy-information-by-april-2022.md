Title: Google: Android apps must provide privacy information by April 2022
Date: 2021-07-28T11:03:21-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google
Slug: google-android-apps-must-provide-privacy-information-by-april-2022

[Source](https://www.bleepingcomputer.com/news/google/google-android-apps-must-provide-privacy-information-by-april-2022/){:target="_blank" rel="noopener"}

> Google has announced today more details regarding their upcoming Google Play 'Safety section' feature that provides users information about the data collected and used by an Android app. [...]
