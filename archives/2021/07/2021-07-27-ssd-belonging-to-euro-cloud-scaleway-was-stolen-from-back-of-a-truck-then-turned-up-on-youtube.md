Title: SSD belonging to Euro-cloud Scaleway was stolen from back of a truck, then turned up on YouTube
Date: 2021-07-27T03:58:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: ssd-belonging-to-euro-cloud-scaleway-was-stolen-from-back-of-a-truck-then-turned-up-on-youtube

[Source](https://go.theregister.com/feed/www.theregister.com/2021/07/27/stolen_scaleway_ssd_recovered/){:target="_blank" rel="noopener"}

> Has since been recovered, and Scaleway now ships disks with GPS trackers It sounds like a "dog ate my homework" excuse for the cloud age, but Euro-cloud Scaleway says one of its solid-state disks was stolen from a truck, turned up in the hands of a YouTuber, and has now made its way back home.... [...]
