Title: Microsoft shares mitigations for new PetitPotam NTML relay attack
Date: 2021-07-24T19:38:49-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: microsoft-shares-mitigations-for-new-petitpotam-ntml-relay-attack

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-mitigations-for-new-petitpotam-ntml-relay-attack/){:target="_blank" rel="noopener"}

> Microsoft has released mitigations for the new PetitPotam NTLM relay attack that allows taking over a domain controller or other Windows servers. [...]
