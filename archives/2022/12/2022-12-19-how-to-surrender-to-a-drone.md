Title: How to Surrender to a Drone
Date: 2022-12-19T12:09:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;drones;Russia;Ukraine;war
Slug: 2022-12-19-how-to-surrender-to-a-drone

[Source](https://www.schneier.com/blog/archives/2022/12/how-to-surrender-to-a-drone.html){:target="_blank" rel="noopener"}

> The Ukrainian army has released an instructional video explaining how Russian soldiers should surrender to a drone: “Seeing the drone in the field of view, make eye contact with it,” the video instructs. Soldiers should then raise their arms and signal they’re ready to follow. After that the drone will move up and down a few meters, before heading off at walking pace in the direction of the nearest representatives of Ukraine’s army, it says. The video also warns that the drone’s battery may run low, in which case it will head back to base and the soldiers should stay [...]
