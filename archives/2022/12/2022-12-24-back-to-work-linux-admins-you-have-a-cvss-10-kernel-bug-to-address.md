Title: Back to work, Linux admins: You have a CVSS 10 kernel bug to address
Date: 2022-12-24T10:00:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-12-24-back-to-work-linux-admins-you-have-a-cvss-10-kernel-bug-to-address

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/24/back_to_work_linux_admins/){:target="_blank" rel="noopener"}

> Also, script kiddies are coming for your gift cards, and Meta's Cambridge Analytica pathetic payout Merry Christmas, Linux systems administrators: Here's a kernel vulnerability with a CVSS score of 10 in your SMB server for the holiday season giving an unauthenticated user remote code execution.... [...]
