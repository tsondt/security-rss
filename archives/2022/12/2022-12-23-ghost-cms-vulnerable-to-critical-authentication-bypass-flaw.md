Title: Ghost CMS vulnerable to critical authentication bypass flaw
Date: 2022-12-23T03:12:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-23-ghost-cms-vulnerable-to-critical-authentication-bypass-flaw

[Source](https://www.bleepingcomputer.com/news/security/ghost-cms-vulnerable-to-critical-authentication-bypass-flaw/){:target="_blank" rel="noopener"}

> A critical vulnerability in the Ghost CMS newsletter subscription system could allow external users to create newsletters or modify existing ones so that they contain malicious JavaScript. [...]
