Title: Medibank prognosis gets worse after more stolen data leaked
Date: 2022-12-02T23:10:59+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-02-medibank-prognosis-gets-worse-after-more-stolen-data-leaked

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/02/medibank_data_dump/){:target="_blank" rel="noopener"}

> Plus Australia launches an investigation into insurer's data privacy practices Australian health insurer Medibank's prognosis following an October data breach keeps getting worse as criminals dumped another batch of stolen customer data on the dark web.... [...]
