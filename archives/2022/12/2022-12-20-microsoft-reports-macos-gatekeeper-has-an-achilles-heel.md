Title: Microsoft reports macOS Gatekeeper has an 'Achilles' heel
Date: 2022-12-20T19:30:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-12-20-microsoft-reports-macos-gatekeeper-has-an-achilles-heel

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/20/macos_gatekeeper_flaw_microsoft/){:target="_blank" rel="noopener"}

> Insert your Trojan joke here Security researchers at Microsoft have discovered a bug in macOS that lets malicious apps bypass Apple's Gatekeeper security software "for initial access by malware and other threats."... [...]
