Title: Air-gapped PCs vulnerable to data theft via power supply radiation
Date: 2022-12-10T10:06:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-10-air-gapped-pcs-vulnerable-to-data-theft-via-power-supply-radiation

[Source](https://www.bleepingcomputer.com/news/security/air-gapped-pcs-vulnerable-to-data-theft-via-power-supply-radiation/){:target="_blank" rel="noopener"}

> A new attack method named COVID-bit uses electromagnetic waves to transmit data from air-gapped systems isolated from the internet over a distance of at least two meters (6.5 ft), where its captured by a receiver. [...]
