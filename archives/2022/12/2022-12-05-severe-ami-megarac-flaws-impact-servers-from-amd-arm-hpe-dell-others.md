Title: Severe AMI MegaRAC flaws impact servers from AMD, ARM, HPE, Dell, others
Date: 2022-12-05T10:07:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-05-severe-ami-megarac-flaws-impact-servers-from-amd-arm-hpe-dell-others

[Source](https://www.bleepingcomputer.com/news/security/severe-ami-megarac-flaws-impact-servers-from-amd-arm-hpe-dell-others/){:target="_blank" rel="noopener"}

> Three vulnerabilities in the American Megatrends MegaRAC Baseboard Management Controller (BMC) software impact server equipment used in many cloud service and data center providers. [...]
