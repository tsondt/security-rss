Title: VMware fixes critical ESXi and vRealize security flaws
Date: 2022-12-14T12:46:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-14-vmware-fixes-critical-esxi-and-vrealize-security-flaws

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-critical-esxi-and-vrealize-security-flaws/){:target="_blank" rel="noopener"}

> VMware released security updates to address a critical-severity vulnerability impacting ESXi, Workstation, Fusion, and Cloud Foundation, and a critical-severity command injection flaw affecting vRealize Network Insight. [...]
