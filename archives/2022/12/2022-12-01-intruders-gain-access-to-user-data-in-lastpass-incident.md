Title: Intruders gain access to user data in LastPass incident
Date: 2022-12-01T13:30:08+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-12-01-intruders-gain-access-to-user-data-in-lastpass-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/01/lastpass/){:target="_blank" rel="noopener"}

> Password manager working to identify info affected but says credentials are safely encrypted Intruders broke into a third-party cloud storage service LastPass shares with affiliate company GoTo and gained access to "certain elements" of customers' information, the pair have confirmed.... [...]
