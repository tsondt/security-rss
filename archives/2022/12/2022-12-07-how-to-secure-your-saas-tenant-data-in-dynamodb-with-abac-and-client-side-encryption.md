Title: How to secure your SaaS tenant data in DynamoDB with ABAC and client-side encryption
Date: 2022-12-07T18:30:46+00:00
Author: Jani Muuriaisniemi
Category: AWS Security
Tags: Expert (400);Security, Identity, & Compliance;Technical How-to;ABAC;Data protection;DynamoDB;Encryption;IAM;KMS;SaaS;Security Blog
Slug: 2022-12-07-how-to-secure-your-saas-tenant-data-in-dynamodb-with-abac-and-client-side-encryption

[Source](https://aws.amazon.com/blogs/security/how-to-secure-your-saas-tenant-data-in-dynamodb-with-abac-and-client-side-encryption/){:target="_blank" rel="noopener"}

> If you’re a SaaS vendor, you may need to store and process personal and sensitive data for large numbers of customers across different geographies. When processing sensitive data at scale, you have an increased responsibility to secure this data end-to-end. Client-side encryption of data, such as your customers’ contact information, provides an additional mechanism that [...]
