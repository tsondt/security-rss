Title: NodeBB prototype pollution flaw could lead to account takeover
Date: 2022-12-08T13:57:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-08-nodebb-prototype-pollution-flaw-could-lead-to-account-takeover

[Source](https://portswigger.net/daily-swig/nodebb-prototype-pollution-flaw-could-lead-to-account-takeover){:target="_blank" rel="noopener"}

> ‘Not a prototype pollution vulnerability as you might normally understand it’ [...]
