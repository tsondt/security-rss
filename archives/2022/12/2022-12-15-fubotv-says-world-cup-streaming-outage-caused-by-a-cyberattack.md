Title: FuboTV says World Cup streaming outage caused by a cyberattack
Date: 2022-12-15T18:40:58-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-15-fubotv-says-world-cup-streaming-outage-caused-by-a-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/fubotv-says-world-cup-streaming-outage-caused-by-a-cyberattack/){:target="_blank" rel="noopener"}

> FuboTV has confirmed that a streaming outage preventing subscribers from watching the World Cup Qatar 2022 semifinal match between France and Morocco was caused by a cyberattack. [...]
