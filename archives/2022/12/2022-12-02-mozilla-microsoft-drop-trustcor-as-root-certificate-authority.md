Title: Mozilla, Microsoft drop TrustCor as root certificate authority
Date: 2022-12-02T09:30:51+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-12-02-mozilla-microsoft-drop-trustcor-as-root-certificate-authority

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/02/mozilla_microsoft_trustcor/){:target="_blank" rel="noopener"}

> 'There is no evidence to suggest that TrustCor violated conduct, policy, or procedure' says biz Mozilla and Microsoft have taken action against a certificate authority accused of having close ties to a US military contractor that allegedly paid software developers to embed data-harvesting malware in mobile apps.... [...]
