Title: Restaurant CRM platform ‘SevenRooms’ confirms breach after data for sale
Date: 2022-12-18T11:07:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-18-restaurant-crm-platform-sevenrooms-confirms-breach-after-data-for-sale

[Source](https://www.bleepingcomputer.com/news/security/restaurant-crm-platform-sevenrooms-confirms-breach-after-data-for-sale/){:target="_blank" rel="noopener"}

> SevenRooms, a restaurant CRM software and guest manRestaurant customer management platform SevenRooms has confirmed it suffered a data breach after a threat actor began selling stolen data on a hacking forum.agement service provider, has admitted it has suffered a data breach, result of a security incident on one of its vendors. [...]
