Title: SEC charges crew of social media influencers with $100m fraud
Date: 2022-12-15T13:30:05+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-12-15-sec-charges-crew-of-social-media-influencers-with-100m-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/15/im_playing_this_extremely_smart/){:target="_blank" rel="noopener"}

> Defendants allegedly 'discussed their scheme’ in recorded chats on Discord and Twitter that ‘they believed were private’ Eight braggadocious social media influencers fond of posing next to sportscars are facing charges from the US Securities and Exchange Commission (SEC) and Department of Justice (DoJ), who claim they manipulated their 1.5 million followers in order to help themselves to $100 million in "fraudulent profits."... [...]
