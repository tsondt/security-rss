Title: Prepare for consolidated controls view and consolidated control findings in AWS Security Hub
Date: 2022-12-15T20:29:40+00:00
Author: Priyanka Prakash
Category: AWS Security
Tags: Announcements;AWS Security Hub;Intermediate (200);Security, Identity, & Compliance;Technical How-to;control findings;controls view;security checks;Security controls
Slug: 2022-12-15-prepare-for-consolidated-controls-view-and-consolidated-control-findings-in-aws-security-hub

[Source](https://aws.amazon.com/blogs/security/prepare-for-consolidated-controls-view-and-consolidated-control-findings-in-aws-security-hub/){:target="_blank" rel="noopener"}

> Currently, AWS Security Hub identifies controls and generates control findings in the context of security standards. Security Hub is aiming to release two new features in the first quarter of 2023 that will decouple controls from standards and streamline how you view and receive control findings. The new features to be released are consolidated controls [...]
