Title: Six Charged in Mass Takedown of DDoS-for-Hire Sites
Date: 2022-12-14T19:58:00+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: DDoS-for-Hire;Ne'er-Do-Well News
Slug: 2022-12-14-six-charged-in-mass-takedown-of-ddos-for-hire-sites

[Source](https://krebsonsecurity.com/2022/12/six-charged-in-mass-takedown-of-ddos-for-hire-sites/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DOJ) today seized four-dozen domains that sold “booter” or “stresser” services — businesses that make it easy and cheap for even non-technical users to launch powerful Distributed Denial of Service (DDoS) attacks designed knock targets offline. The DOJ also charged six U.S. men with computer crimes related to their alleged ownership of the popular DDoS-for-hire services. The booter service OrphicSecurityTeam[.]com was one of the 48 DDoS-for-hire domains seized by the Justice Department this week. The DOJ said the 48 domains it seized helped paying customers launch millions of digital sieges capable of knocking Web sites [...]
