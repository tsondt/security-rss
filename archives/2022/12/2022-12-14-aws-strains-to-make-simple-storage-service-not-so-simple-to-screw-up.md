Title: AWS strains to make Simple Storage Service not so simple to screw up
Date: 2022-12-14T21:30:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-12-14-aws-strains-to-make-simple-storage-service-not-so-simple-to-screw-up

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/14/aws_simple_storage_service_simplified/){:target="_blank" rel="noopener"}

> Not Amazon's fault buckets are exposed, but the loaded shotgun and your foot are all there ready and waiting Amazon wants you to know that it's not to blame for the data you've exposed though its cloud storage service. AWS Simple Storage Service (S3) is, after all, simple.... [...]
