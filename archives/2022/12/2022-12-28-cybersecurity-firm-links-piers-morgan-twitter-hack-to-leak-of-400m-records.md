Title: Cybersecurity firm links Piers Morgan Twitter hack to leak of 400m records
Date: 2022-12-28T07:03:16+00:00
Author: Amy Remeikis
Category: The Guardian
Tags: Twitter;Piers Morgan;Scott Morrison;Data and computer security;Australia news;Technology;Internet
Slug: 2022-12-28-cybersecurity-firm-links-piers-morgan-twitter-hack-to-leak-of-400m-records

[Source](https://www.theguardian.com/technology/2022/dec/28/cybersecurity-firm-links-piers-morgan-twitter-hack-to-leak-of-400-million-records-including-scott-morrisons){:target="_blank" rel="noopener"}

> Former Australian prime minister Scott Morrison among politicians and celebrities whose details were in sample of allegedly hacked data published online The former Australian prime minister Scott Morrison appears to have been caught up in a leak of partial data on 400 million Twitter users, along with celebrities including the model Cara Delevingne, US politician Alexandria Ocasio-Cortez and pop singer Shawn Mendes. Morrison’s Twitter account was included in a sample of data released by an alleged cybercriminal last week. Sign up for Guardian Australia’s free morning and afternoon email newsletters for your daily news roundup Continue reading... [...]
