Title: Microsoft to Europe: We're setting an EU 'data boundary' from 2023
Date: 2022-12-15T15:27:05+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-12-15-microsoft-to-europe-were-setting-an-eu-data-boundary-from-2023

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/15/microsoft_launches_eu_data_boundary/){:target="_blank" rel="noopener"}

> Pitches storage, cloudy software compliance to twitchy EU customers thinking about GDPR Microsoft has confirmed that from the beginning of 2023, it will introduce an EU Data Boundary solution designed to help customers in the European Union and the European Free Trade Association comply with legislation including the General Data Protection Regulation (GDPR).... [...]
