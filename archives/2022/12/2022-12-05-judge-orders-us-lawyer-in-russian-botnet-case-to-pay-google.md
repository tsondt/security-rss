Title: Judge Orders U.S. Lawyer in Russian Botnet Case to Pay Google
Date: 2022-12-05T19:44:50+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Dmitry Starovikov;Glupteba botnet;google;Halimah DeLaine Prado;Igor Litvak;Royal Hansen
Slug: 2022-12-05-judge-orders-us-lawyer-in-russian-botnet-case-to-pay-google

[Source](https://krebsonsecurity.com/2022/12/judge-orders-u-s-lawyer-in-russian-botnet-case-to-pay-google/){:target="_blank" rel="noopener"}

> In December 2021, Google filed a civil lawsuit against two Russian men thought to be responsible for operating Glupteba, one of the Internet’s largest and oldest botnets. The defendants, who initially pursued a strategy of counter suing Google for interfering in their sprawling cybercrime business, later brazenly offered to dismantle the botnet in exchange for payment from Google. The judge in the case was not amused, found for the plaintiff, and ordered the defendants and their U.S. attorney to pay Google’s legal fees. A slide from a talk given in Sept. 2022 by Google researcher Luca Nagy. https://www.youtube.com/watch?v=5Gz6_I-wl0E&t=6s Glupteba is [...]
