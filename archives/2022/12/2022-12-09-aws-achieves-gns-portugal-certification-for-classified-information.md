Title: AWS achieves GNS Portugal certification for classified information
Date: 2022-12-09T20:34:18+00:00
Author: Rodrigo Fiuza
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS security;Compliance;GNS;National Restricted;Portugal;Security;Security Blog
Slug: 2022-12-09-aws-achieves-gns-portugal-certification-for-classified-information

[Source](https://aws.amazon.com/blogs/security/aws-achieves-gns-portugal-certification-for-classified-information/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS), and we are pleased to announce that our Regions and AWS Edge locations in Europe are now certified by the Portuguese GNS/NSO (National Security Office) at the National Restricted level. This certification demonstrates our ongoing commitment to adhere to the [...]
