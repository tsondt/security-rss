Title: Hacking the JFK Airport Taxi Dispatch System
Date: 2022-12-23T12:03:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;air travel;hacking;transportation
Slug: 2022-12-23-hacking-the-jfk-airport-taxi-dispatch-system

[Source](https://www.schneier.com/blog/archives/2022/12/hacking-the-jfk-airport-taxi-dispatch-system.html){:target="_blank" rel="noopener"}

> Two men have been convicted of hacking the taxi dispatch system at the JFK airport. This enabled them to reorder the taxis on the list; they charged taxi drivers $10 to cut the line. [...]
