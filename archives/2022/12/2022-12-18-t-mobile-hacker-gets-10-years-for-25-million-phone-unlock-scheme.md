Title: T-Mobile hacker gets 10 years for $25 million phone unlock scheme
Date: 2022-12-18T10:03:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-18-t-mobile-hacker-gets-10-years-for-25-million-phone-unlock-scheme

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-hacker-gets-10-years-for-25-million-phone-unlock-scheme/){:target="_blank" rel="noopener"}

> Argishti Khudaverdyan, the former owner of a T-Mobile retail store, was sentenced to 10 years in prison for a $25 million scheme where he unlocked and unblocked cellphones by hacking into T-Mobile's internal systems. [...]
