Title: Meta to pay $725 million to settle Cambridge Analytica lawsuit
Date: 2022-12-23T15:50:02+00:00
Author: Eric Bangeman
Category: Ars Technica
Tags: Biz & IT;Cambridge Analytica;meta;privacy
Slug: 2022-12-23-meta-to-pay-725-million-to-settle-cambridge-analytica-lawsuit

[Source](https://arstechnica.com/?p=1906625){:target="_blank" rel="noopener"}

> Enlarge (credit: Daniel Leal / Getty Images ) Meta, the parent company of Facebook, will pay $725 million to settle a class-action lawsuit filed in 2018. The lawsuit came in the wake of Facebook's revelation that it had improperly shared data on 87 million users with Cambridge Analytica, a British political consultancy tied to former President Donald Trump's election campaign. Cambridge Analytica got its access to Facebook user data via an app developed by a third party. While only around 270,000 Facebook account-holders used the "This is Your Digital Life" app, the app's permissions allowed it access to data on [...]
