Title: Cloud CISO Perspectives: December 2022
Date: 2022-12-21T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Identity & Security
Slug: 2022-12-21-cloud-ciso-perspectives-december-2022

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-december-2022/){:target="_blank" rel="noopener"}

> Welcome to December’s Cloud CISO Perspectives. This month, we’re going to look back at the most important security lessons of 2022 with my colleagues in our Office of the CISO and on the Google Cybersecurity Action Team. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. How lessons from 2022 can guide us in the new year Like a puzzle whose pieces kept changing shape, 2022 was the year that cloud security [...]
