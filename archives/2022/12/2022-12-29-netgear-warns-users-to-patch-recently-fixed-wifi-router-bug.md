Title: Netgear warns users to patch recently fixed WiFi router bug
Date: 2022-12-29T12:55:38-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-29-netgear-warns-users-to-patch-recently-fixed-wifi-router-bug

[Source](https://www.bleepingcomputer.com/news/security/netgear-warns-users-to-patch-recently-fixed-wifi-router-bug/){:target="_blank" rel="noopener"}

> Netgear has fixed a high-severity vulnerability affecting multiple WiFi router models and advised customers to update their devices to the latest available firmware as soon as possible. [...]
