Title: Samsung Galaxy S22 hacked again on second day of Pwn2Own
Date: 2022-12-08T11:29:28-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-08-samsung-galaxy-s22-hacked-again-on-second-day-of-pwn2own

[Source](https://www.bleepingcomputer.com/news/security/samsung-galaxy-s22-hacked-again-on-second-day-of-pwn2own/){:target="_blank" rel="noopener"}

> Contestants hacked the Samsung Galaxy S22 again during the second day of the consumer-focused Pwn2Own 2022 competition in Toronto, Canada. [...]
