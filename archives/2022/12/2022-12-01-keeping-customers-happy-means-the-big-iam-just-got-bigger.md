Title: Keeping customers happy means the big IAM just got bigger
Date: 2022-12-01T09:02:14+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: 2022-12-01-keeping-customers-happy-means-the-big-iam-just-got-bigger

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/01/keeping_customers_happy_means_the/){:target="_blank" rel="noopener"}

> You need to open up core systems to consumers and partners. Here's how to do it securely Sponsored Feature It's easy to forget the human factor when it comes to cybersecurity. Completely locking down your network will certainly make you secure, just as completely locking down your building will do the same. The problem is you'll struggle to get much work done, because people need access to assets, physical or virtual, to do their jobs.... [...]
