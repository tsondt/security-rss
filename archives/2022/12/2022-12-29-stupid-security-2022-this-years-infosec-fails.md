Title: Stupid security 2022 – this year’s infosec fails
Date: 2022-12-29T10:04:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-29-stupid-security-2022-this-years-infosec-fails

[Source](https://portswigger.net/daily-swig/stupid-security-2022-this-years-infosec-fails){:target="_blank" rel="noopener"}

> Epic web security fails and salutary lessons from another inevitably eventful year in infosec [...]
