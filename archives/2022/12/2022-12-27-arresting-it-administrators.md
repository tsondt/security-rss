Title: Arresting IT Administrators
Date: 2022-12-27T12:01:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;antivirus;cyberattack;law enforcement;patching
Slug: 2022-12-27-arresting-it-administrators

[Source](https://www.schneier.com/blog/archives/2022/12/arresting-it-administrators.html){:target="_blank" rel="noopener"}

> This is one way of ensuring that IT keeps up with patches : Albanian prosecutors on Wednesday asked for the house arrest of five public employees they blame for not protecting the country from a cyberattack by alleged Iranian hackers. Prosecutors said the five IT officials of the public administration department had failed to check the security of the system and update it with the most recent antivirus software. The next step would be to arrest managers at software companies for not releasing patches fast enough. And maybe programmers for writing buggy code. I don’t know where this line of [...]
