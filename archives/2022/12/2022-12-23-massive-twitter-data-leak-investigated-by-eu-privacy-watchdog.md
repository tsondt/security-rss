Title: Massive Twitter data leak investigated by EU privacy watchdog
Date: 2022-12-23T10:06:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-23-massive-twitter-data-leak-investigated-by-eu-privacy-watchdog

[Source](https://www.bleepingcomputer.com/news/security/massive-twitter-data-leak-investigated-by-eu-privacy-watchdog/){:target="_blank" rel="noopener"}

> The Irish Data Protection Commission (DPC) has launched an inquiry following last month's news reports of a massive Twitter data leak. [...]
