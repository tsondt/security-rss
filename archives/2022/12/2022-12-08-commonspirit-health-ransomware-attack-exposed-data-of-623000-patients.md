Title: CommonSpirit Health ransomware attack exposed data of 623,000 patients
Date: 2022-12-08T15:27:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-08-commonspirit-health-ransomware-attack-exposed-data-of-623000-patients

[Source](https://www.bleepingcomputer.com/news/security/commonspirit-health-ransomware-attack-exposed-data-of-623-000-patients/){:target="_blank" rel="noopener"}

> CommonSpirit Health has confirmed that threat actors accessed the personal data for 623,774 patients during an October ransomware attack. [...]
