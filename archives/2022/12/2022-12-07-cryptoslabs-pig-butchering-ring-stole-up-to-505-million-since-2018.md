Title: CryptosLabs ‘pig butchering’ ring stole up to $505 million since 2018
Date: 2022-12-07T11:13:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-07-cryptoslabs-pig-butchering-ring-stole-up-to-505-million-since-2018

[Source](https://www.bleepingcomputer.com/news/security/cryptoslabs-pig-butchering-ring-stole-up-to-505-million-since-2018/){:target="_blank" rel="noopener"}

> A previously unknown investment scam group named 'CryptosLabs' has stolen up to €480 million ($505 million) from victims in France, Belgium, and Luxembourg, since the launch of its operation in 2018. [...]
