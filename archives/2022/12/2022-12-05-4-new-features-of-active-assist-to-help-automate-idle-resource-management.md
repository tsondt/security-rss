Title: 4 new features of Active Assist to help automate idle resource management
Date: 2022-12-05T17:00:00+00:00
Author: Sharon Fang
Category: GCP Security
Tags: Cost Management;Sustainability;Identity & Security;Management Tools
Slug: 2022-12-05-4-new-features-of-active-assist-to-help-automate-idle-resource-management

[Source](https://cloud.google.com/blog/products/management-tools/new-features-unattended-project-recommender/){:target="_blank" rel="noopener"}

> Unattended Project Recommender makes identifying idle cloud projects easy, and helps you mitigate associated security issues, reduce unnecessary spend and environmental impact. In order to implement a scalable and repeatable resource lifecycle management process, it’s important to have the right tools for the job. Today, we’re announcing several new capabilities that can help you make idle project remediation a part of your company’s day-to-day operations and culture: Organization-level aggregation of recommendations for a broader view of your unattended projects Observation period configurability to give you more control into the usage windows See the cost impact of following our recommendations Shareable [...]
