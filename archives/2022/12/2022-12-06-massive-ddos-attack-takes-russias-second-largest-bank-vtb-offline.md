Title: Massive DDoS attack takes Russia’s second-largest bank VTB offline
Date: 2022-12-06T10:11:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-06-massive-ddos-attack-takes-russias-second-largest-bank-vtb-offline

[Source](https://www.bleepingcomputer.com/news/security/massive-ddos-attack-takes-russia-s-second-largest-bank-vtb-offline/){:target="_blank" rel="noopener"}

> Russia's second-largest financial institution VTB Bank says it is facing the worse cyberattack in its history after its website and mobile apps were taken offline due to an ongoing DDoS (distributed denial of service) attack. [...]
