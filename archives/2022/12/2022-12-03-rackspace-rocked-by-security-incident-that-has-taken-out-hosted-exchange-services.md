Title: Rackspace rocked by ‘security incident’ that has taken out hosted Exchange services
Date: 2022-12-03T10:58:16+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-03-rackspace-rocked-by-security-incident-that-has-taken-out-hosted-exchange-services

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/03/rackspace_security_incident_hosted_exchange/){:target="_blank" rel="noopener"}

> Warns recovery could take several days and pledges better support after customer complaints Updated Some of Rackspace’s hosted Microsoft Exchange services have been taken down by what the company has described as a “security incident”.... [...]
