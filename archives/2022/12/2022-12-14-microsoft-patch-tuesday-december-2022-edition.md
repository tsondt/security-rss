Title: Microsoft Patch Tuesday, December 2022 Edition
Date: 2022-12-14T17:01:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Security Tools;Time to Patch;Apple zero-day;CVE-2022-41076;CVE-2022-44698;CVE-2022-44710;CVE-2022-44713;Greg Wiseman;Immersive Labs;Kevin Breen;Microsoft Patch Tuesday December 2022;PowerShell;Rapid7;sophos;Trend Micro's Zero Day Initiative;Will Dormann;windows
Slug: 2022-12-14-microsoft-patch-tuesday-december-2022-edition

[Source](https://krebsonsecurity.com/2022/12/microsoft-patch-tuesday-december-2022-edition/){:target="_blank" rel="noopener"}

> Microsoft has released its final monthly batch of security updates for 2022, fixing more than four dozen security holes in its various Windows operating systems and related software. The most pressing patches include a zero-day in a Windows feature that tries to flag malicious files from the Web, a critical bug in PowerShell, and a dangerous flaw in Windows 11 systems that was detailed publicly prior to this week’s Patch Tuesday. The security updates include patches for Azure, Microsoft Edge, Office, SharePoint Server, SysInternals, and the.NET framework. Six of the update bundles earned Microsoft’s most dire “critical” rating, meaning they [...]
