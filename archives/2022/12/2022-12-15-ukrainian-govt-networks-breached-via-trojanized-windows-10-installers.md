Title: Ukrainian govt networks breached via trojanized Windows 10 installers
Date: 2022-12-15T12:24:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-15-ukrainian-govt-networks-breached-via-trojanized-windows-10-installers

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-govt-networks-breached-via-trojanized-windows-10-installers/){:target="_blank" rel="noopener"}

> Ukrainian government entities were hacked in targeted attacks after their networks were first compromised via trojanized ISO files posing as legitimate Windows 10 installers. [...]
