Title: Black Hat Europe redux: The top web hacking talks for 2022
Date: 2022-12-12T17:23:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-12-black-hat-europe-redux-the-top-web-hacking-talks-for-2022

[Source](https://portswigger.net/daily-swig/black-hat-europe-redux-the-top-web-hacking-talks-for-2022){:target="_blank" rel="noopener"}

> Catch up on the highlights of last week’s cybersecurity conference [...]
