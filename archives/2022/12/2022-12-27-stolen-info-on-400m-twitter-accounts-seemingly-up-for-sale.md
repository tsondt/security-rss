Title: Stolen info on 400m+ Twitter accounts seemingly up for sale
Date: 2022-12-27T20:01:53+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-12-27-stolen-info-on-400m-twitter-accounts-seemingly-up-for-sale

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/27/twitter_hack_morgan/){:target="_blank" rel="noopener"}

> Plus: Cracked Piers Morgan spews offensive tweets, not the usual kind A miscreant this Christmas weekend said they are willing to sell public and private info on more than 400 million Twitter accounts.... [...]
