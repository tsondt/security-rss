Title: Syntax errors are the doom of us all, including botnet authors
Date: 2022-12-05T21:54:14+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Akamai;botnet;cryptomining;DDoS;kmsdbot;syntax error
Slug: 2022-12-05-syntax-errors-are-the-doom-of-us-all-including-botnet-authors

[Source](https://arstechnica.com/?p=1902139){:target="_blank" rel="noopener"}

> Enlarge / If you're going to come at port 443, you best not miss (or forget to put a space between URL and port). (credit: Getty Images) KmsdBot, a cryptomining botnet that could also be used for denial-of-service (DDOS) attacks, broke into systems through weak secure shell credentials. It could remotely control a system, it was hard to reverse-engineer, didn't stay persistent, and could target multiple architectures. KmsdBot was a complex malware with no easy fix. That was the case until researchers at Akamai Security Research witnessed a novel solution : forgetting to put a space between an IP address [...]
