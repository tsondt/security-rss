Title: GitHub rolls out free secret scanning for all public repositories
Date: 2022-12-15T14:09:29-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-15-github-rolls-out-free-secret-scanning-for-all-public-repositories

[Source](https://www.bleepingcomputer.com/news/security/github-rolls-out-free-secret-scanning-for-all-public-repositories/){:target="_blank" rel="noopener"}

> GitHub is rolling out support for the free scanning of exposed secrets (such as credentials and auth tokens) to all public repositories on its code hosting platform. [...]
