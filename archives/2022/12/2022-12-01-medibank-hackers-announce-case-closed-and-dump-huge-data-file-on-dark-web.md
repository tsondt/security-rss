Title: Medibank hackers announce ‘case closed’ and dump huge data file on dark web
Date: 2022-12-01T01:51:49+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Medibank;Australia news;Cybercrime;Data and computer security;Technology
Slug: 2022-12-01-medibank-hackers-announce-case-closed-and-dump-huge-data-file-on-dark-web

[Source](https://www.theguardian.com/australia-news/2022/dec/01/medibank-hackers-announce-case-closed-and-dump-huge-data-file-on-dark-web){:target="_blank" rel="noopener"}

> Medibank confirms it may be the full trove of hundreds of thousands of customers’ private records that were stolen from the health insurer Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast The cybercriminals behind the Medibank cyber-attack have posted on the dark web what appears to be the remainder of the customer data they took from the health insurer, stating it is “case closed” for the hack. On Thursday morning, the blog – which returned online after several days of being offline last week – [...]
