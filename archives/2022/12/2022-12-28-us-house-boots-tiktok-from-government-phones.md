Title: US House boots TikTok from government phones
Date: 2022-12-28T00:12:08+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2022-12-28-us-house-boots-tiktok-from-government-phones

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/28/us_tiktok_government_ban/){:target="_blank" rel="noopener"}

> ByteDance ban for federal devices awaits Biden’s signature The US government's New Year's resolution for 2023: no more TikTok at work.... [...]
