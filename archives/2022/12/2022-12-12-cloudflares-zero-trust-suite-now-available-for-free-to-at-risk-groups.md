Title: Cloudflare's Zero Trust suite now available for free to at-risk groups
Date: 2022-12-12T09:53:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-12-cloudflares-zero-trust-suite-now-available-for-free-to-at-risk-groups

[Source](https://www.bleepingcomputer.com/news/security/cloudflares-zero-trust-suite-now-available-for-free-to-at-risk-groups/){:target="_blank" rel="noopener"}

> Cloudflare has made its 'Cloudflare One Zero Trust' security suite free to public interest groups, election sites, and state organizations that are currently part of Project Galileo and the Athenian Project. [...]
