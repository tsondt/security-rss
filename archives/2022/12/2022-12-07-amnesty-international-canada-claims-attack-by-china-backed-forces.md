Title: Amnesty International Canada claims attack by China-backed forces
Date: 2022-12-07T04:29:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-12-07-amnesty-international-canada-claims-attack-by-china-backed-forces

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/07/china_believed_responsible_for_amnesty_attack/){:target="_blank" rel="noopener"}

> Threat actors allegedly looking for contacts and monitoring org's future plans The Canadian branch of Amnesty International was the target of an attack it has pinned on a Chinese state-sponsored actor.... [...]
