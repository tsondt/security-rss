Title: Remuneration coming for TrustCor customers impacted by CA revocation
Date: 2022-12-05T05:45:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-12-05-remuneration-coming-for-trustcor-customers-impacted-by-ca-revocation

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/05/in_brief_security/){:target="_blank" rel="noopener"}

> Also, a Capone henchman lands behind bars, while nearly 9/10 DoD contract firms fail security standards In brief Certificate Authority TrustCor responded to its ejection from Mozilla and Microsoft's browsers by offering refunds for some customers, while leaving other resellers to pick up the mess on their own.... [...]
