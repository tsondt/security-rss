Title: Comcast Xfinity accounts hacked in widespread 2FA bypass attacks
Date: 2022-12-22T14:32:57-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-22-comcast-xfinity-accounts-hacked-in-widespread-2fa-bypass-attacks

[Source](https://www.bleepingcomputer.com/news/security/comcast-xfinity-accounts-hacked-in-widespread-2fa-bypass-attacks/){:target="_blank" rel="noopener"}

> ​Comcast Xfinity customers report their accounts being hacked in widespread attacks that bypass two-factor authentication. These compromised accounts are then used to reset passwords for other services, such as the Coinbase and Gemini crypto exchanges. [...]
