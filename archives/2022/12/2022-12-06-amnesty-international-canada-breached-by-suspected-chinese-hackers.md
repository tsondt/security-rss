Title: Amnesty International Canada breached by suspected Chinese hackers
Date: 2022-12-06T16:30:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-06-amnesty-international-canada-breached-by-suspected-chinese-hackers

[Source](https://www.bleepingcomputer.com/news/security/amnesty-international-canada-breached-by-suspected-chinese-hackers/){:target="_blank" rel="noopener"}

> Amnesty International's Canadian branch has disclosed a security breach detected in early October and linked by cybersecurity firm Secureworks, who investigated the incident, to a threat group likely sponsored by China. [...]
