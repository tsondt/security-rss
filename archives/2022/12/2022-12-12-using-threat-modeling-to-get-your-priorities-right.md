Title: Using threat modeling to get your priorities right
Date: 2022-12-12T17:01:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-12-12-using-threat-modeling-to-get-your-priorities-right

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/12/red_canary_threat_modeling_promo1/){:target="_blank" rel="noopener"}

> State actors - what reactors? Webinar How does your security team prioritize work? When a new attack from a state actor hits the news, do you know if your team should drop everything to hunt for IOCs? Do you understand your security control coverage for the threat actors that might target your organization? Recently, the Red Canary corporate security team asked itself these questions when it was creating its own threat model.... [...]
