Title: VirusTotal cheat sheet makes it easy to search for specific results
Date: 2022-12-20T16:11:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-12-20-virustotal-cheat-sheet-makes-it-easy-to-search-for-specific-results

[Source](https://www.bleepingcomputer.com/news/security/virustotal-cheat-sheet-makes-it-easy-to-search-for-specific-results/){:target="_blank" rel="noopener"}

> VirusTotal has published a cheat sheet to help researchers create queries leading to more specific results from the malware intelligence platform. [...]
