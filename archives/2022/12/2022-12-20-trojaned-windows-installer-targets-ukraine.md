Title: Trojaned Windows Installer Targets Ukraine
Date: 2022-12-20T12:30:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;malware;Russia;supply chain;torrents;Ukraine
Slug: 2022-12-20-trojaned-windows-installer-targets-ukraine

[Source](https://www.schneier.com/blog/archives/2022/12/trojaned-windows-installer-targets-ukraine.html){:target="_blank" rel="noopener"}

> Mandiant is reporting on a trojaned Windows installer that targets Ukrainian users. The installer was left on various torrent sites, presumably ensnaring people downloading pirated copies of the operating system: Mandiant uncovered a socially engineered supply chain operation focused on Ukrainian government entities that leveraged trojanized ISO files masquerading as legitimate Windows 10 Operating System installers. The trojanized ISOs were hosted on Ukrainian- and Russian-language torrent file sharing sites. Upon installation of the compromised software, the malware gathers information on the compromised system and exfiltrates it. At a subset of victims, additional tools are deployed to enable further intelligence gathering. [...]
