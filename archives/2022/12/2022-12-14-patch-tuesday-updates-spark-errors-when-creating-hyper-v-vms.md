Title: Patch Tuesday updates spark errors when creating Hyper-V VMs
Date: 2022-12-14T17:30:56+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-14-patch-tuesday-updates-spark-errors-when-creating-hyper-v-vms

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/14/microsoft_patch_tuesday_vm/){:target="_blank" rel="noopener"}

> Something's broken, mom! Microsoft offers workaround while trying to think up a fix Updates to Windows Server that were included in Microsoft's Patch Tuesday batch of fixes this week could trip up users who want to spin up new virtual machines in some Hyper-V hosts.... [...]
