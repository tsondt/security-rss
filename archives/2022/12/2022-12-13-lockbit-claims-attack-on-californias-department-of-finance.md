Title: LockBit claims attack on California's Department of Finance
Date: 2022-12-13T16:24:20-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-12-13-lockbit-claims-attack-on-californias-department-of-finance

[Source](https://www.bleepingcomputer.com/news/security/lockbit-claims-attack-on-californias-department-of-finance/){:target="_blank" rel="noopener"}

> The Department of Finance in California has been the target of a cyberattack now claimed by the LockBit ransomware gang. [...]
