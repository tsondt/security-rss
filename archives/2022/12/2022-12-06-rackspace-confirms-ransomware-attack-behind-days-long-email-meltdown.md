Title: Rackspace confirms ransomware attack behind days-long email meltdown
Date: 2022-12-06T22:45:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-06-rackspace-confirms-ransomware-attack-behind-days-long-email-meltdown

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/06/rackspace_confirms_ransomware/){:target="_blank" rel="noopener"}

> Hope the name Hackspace doesn't stick Updated Rackspace has admitted a ransomware infection was to blame for the days-long email outage that disrupted services for customers.... [...]
