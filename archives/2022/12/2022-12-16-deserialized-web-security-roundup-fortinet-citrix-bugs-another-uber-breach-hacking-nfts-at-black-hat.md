Title: Deserialized web security roundup – Fortinet, Citrix bugs; another Uber breach; hacking NFTs at Black Hat
Date: 2022-12-16T17:43:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-16-deserialized-web-security-roundup-fortinet-citrix-bugs-another-uber-breach-hacking-nfts-at-black-hat

[Source](https://portswigger.net/daily-swig/deserialized-web-security-roundup-fortinet-citrix-bugs-another-uber-breach-hacking-nfts-at-black-hat){:target="_blank" rel="noopener"}

> Your fortnightly rundown of AppSec vulnerabilities, new hacking techniques, and other cybersecurity news [...]
