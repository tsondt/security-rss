Title: Antwerp's city services down after hackers attack digital partner
Date: 2022-12-06T16:14:30-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-12-06-antwerps-city-services-down-after-hackers-attack-digital-partner

[Source](https://www.bleepingcomputer.com/news/security/antwerps-city-services-down-after-hackers-attack-digital-partner/){:target="_blank" rel="noopener"}

> The city of Antwerp, Belgium, is working to restore its digital services that were disrupted last night by a cyberattack on its digital provider. [...]
