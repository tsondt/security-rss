Title: FBI’s Vetted Info Sharing Network ‘InfraGard’ Hacked
Date: 2022-12-13T23:54:21+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Web Fraud 2.0;Breached;fbi;InfraGard;pompompurin;RaidForums;USDoD
Slug: 2022-12-13-fbis-vetted-info-sharing-network-infragard-hacked

[Source](https://krebsonsecurity.com/2022/12/fbis-vetted-info-sharing-network-infragard-hacked/){:target="_blank" rel="noopener"}

> InfraGard, a program run by the U.S. Federal Bureau of Investigation (FBI) to build cyber and physical threat information sharing partnerships with the private sector, this week saw its database of contact information on more than 80,000 members go up for sale on an English-language cybercrime forum. Meanwhile, the hackers responsible are communicating directly with members through the InfraGard portal online — using a new account under the assumed identity of a financial industry CEO that was vetted by the FBI itself. On Dec. 10, 2022, the relatively new cybercrime forum Breached featured a bombshell new sales thread: The user [...]
