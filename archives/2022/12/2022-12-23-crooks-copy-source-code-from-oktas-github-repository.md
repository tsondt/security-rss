Title: Crooks copy source code from Okta’s GitHub repository
Date: 2022-12-23T00:27:51+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-23-crooks-copy-source-code-from-oktas-github-repository

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/23/okta_code_copy_hack/){:target="_blank" rel="noopener"}

> The hack wraps up a year of bad security incidents for identity Intruders copied source code belonging to Okta after breaching the identity management company's GitHub repositories.... [...]
