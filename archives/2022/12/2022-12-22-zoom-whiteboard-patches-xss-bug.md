Title: Zoom Whiteboard patches XSS bug
Date: 2022-12-22T12:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-22-zoom-whiteboard-patches-xss-bug

[Source](https://portswigger.net/daily-swig/zoom-whiteboard-patches-xss-bug){:target="_blank" rel="noopener"}

> Video conferencing platform fixes cross-site scripting vulnerability [...]
