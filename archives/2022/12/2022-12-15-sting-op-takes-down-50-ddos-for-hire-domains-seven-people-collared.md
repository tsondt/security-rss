Title: Sting op takes down 50 DDoS-for-hire domains, seven people collared
Date: 2022-12-15T21:30:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-15-sting-op-takes-down-50-ddos-for-hire-domains-seven-people-collared

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/15/ddos_sites_takedown_fbi_europol/){:target="_blank" rel="noopener"}

> Cops give denial-of-service sites an extra special denial of service Police around the globe have seized as many as 50 internet domains said to be involved in tens of millions of distributed-denial-of-service (DDoS) attacks worldwide. Seven people were collared during the swoop.... [...]
