Title: Google Home speakers allowed hackers to snoop on conversations
Date: 2022-12-29T10:35:50-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-12-29-google-home-speakers-allowed-hackers-to-snoop-on-conversations

[Source](https://www.bleepingcomputer.com/news/security/google-home-speakers-allowed-hackers-to-snoop-on-conversations/){:target="_blank" rel="noopener"}

> A bug in Google Home smart speaker allowed installing a backdoor account that could be used to control it remotely and to turn it into a snooping device by accessing the microphone feed. [...]
