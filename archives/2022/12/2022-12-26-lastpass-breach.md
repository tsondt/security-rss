Title: LastPass Breach
Date: 2022-12-26T12:06:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;breaches;cloud computing;data breaches;Password Safe;passwords
Slug: 2022-12-26-lastpass-breach

[Source](https://www.schneier.com/blog/archives/2022/12/lastpass-breach.html){:target="_blank" rel="noopener"}

> Last August, LastPass reported a security breach, saying that no customer information—or passwords—were compromised. Turns out the full story is worse : While no customer data was accessed during the August 2022 incident, some source code and technical information were stolen from our development environment and used to target another employee, obtaining credentials and keys which were used to access and decrypt some storage volumes within the cloud-based storage service. [...] To date, we have determined that once the cloud storage access key and dual storage container decryption keys were obtained, the threat actor copied information from backup that contained [...]
