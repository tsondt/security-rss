Title: Taiwan bans state-owned devices from running Chinese platform TikTok
Date: 2022-12-07T10:48:20+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-12-07-taiwan-bans-state-owned-devices-from-running-chinese-platform-tiktok

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/07/taiwan_bans_chinese_platform_tiktok/){:target="_blank" rel="noopener"}

> US FCC Commissioner praises Taiwan’s security decision as US state governments follow suit Public sector bans of Chinese platform TikTok on the grounds of national security have arisen in both Taiwan and additional US states following last week’s ban in South Dakota.... [...]
