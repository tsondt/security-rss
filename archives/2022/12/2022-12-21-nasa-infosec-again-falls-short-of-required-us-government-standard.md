Title: NASA infosec again falls short of required US government standard
Date: 2022-12-21T14:00:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-21-nasa-infosec-again-falls-short-of-required-us-government-standard

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/21/nasa_immature_cycbersecurity/){:target="_blank" rel="noopener"}

> Good thing space agency doesn’t have any state secrets... oh, hang on The NASA Office of Inspector General (OIG) has published its annual audit of the aerospace agency's infosec capabilities and practices, which earned an overall rating of "Not Effective."... [...]
