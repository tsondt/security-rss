Title: Eurozone plans to formalize passenger data, improve security
Date: 2022-12-20T07:30:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-12-20-eurozone-plans-to-formalize-passenger-data-improve-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/20/europe_plans_to_standardize_passenger/){:target="_blank" rel="noopener"}

> Central hub will make things smoother and safer but not store details The European Commission last week proposed rules governing the use of Advance Passenger Information in a bid to strengthen border security.... [...]
