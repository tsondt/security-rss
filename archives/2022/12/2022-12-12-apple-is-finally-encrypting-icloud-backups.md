Title: Apple Is Finally Encrypting iCloud Backups
Date: 2022-12-12T12:00:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;backups;cloud computing;encryption
Slug: 2022-12-12-apple-is-finally-encrypting-icloud-backups

[Source](https://www.schneier.com/blog/archives/2022/12/apple-is-finally-encrypting-icloud-backups.html){:target="_blank" rel="noopener"}

> After way too many years, Apple is finally encrypting iCloud backups : Based on a screenshot from Apple, these categories are covered when you flip on Advanced Data Protection: device backups, messages backups, iCloud Drive, Notes, Photos, Reminders, Safari bookmarks, Siri Shortcuts, Voice Memos, and Wallet Passes. Apple says the only “major” categories not covered by Advanced Data Protection are iCloud Mail, Contacts, and Calendar because “of the need to interoperate with the global email, contacts, and calendar systems,” according to its press release. You can see the full list of data categories and what is protected under standard data [...]
