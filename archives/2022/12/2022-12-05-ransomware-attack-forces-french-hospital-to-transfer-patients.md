Title: Ransomware attack forces French hospital to transfer patients
Date: 2022-12-05T15:41:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-05-ransomware-attack-forces-french-hospital-to-transfer-patients

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-forces-french-hospital-to-transfer-patients/){:target="_blank" rel="noopener"}

> The André-Mignot teaching hospital in the suburbs of Paris had to shut down its phone and computer systems because of a ransomware attack that hit on Saturday evening. [...]
