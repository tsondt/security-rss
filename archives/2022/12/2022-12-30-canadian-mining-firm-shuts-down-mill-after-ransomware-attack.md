Title: Canadian mining firm shuts down mill after ransomware attack
Date: 2022-12-30T11:09:56-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-30-canadian-mining-firm-shuts-down-mill-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/canadian-mining-firm-shuts-down-mill-after-ransomware-attack/){:target="_blank" rel="noopener"}

> The Copper Mountain Mining Corporation (CMMC), a Canadian copper mining company in British Columbia, has announced it has become the target of a ransomware attack that impacted its operations. [...]
