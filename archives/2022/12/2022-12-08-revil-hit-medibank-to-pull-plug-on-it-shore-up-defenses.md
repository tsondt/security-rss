Title: REvil-hit Medibank to pull plug on IT, shore up defenses
Date: 2022-12-08T21:35:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-08-revil-hit-medibank-to-pull-plug-on-it-shore-up-defenses

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/08/medibank_it_systems_defenses/){:target="_blank" rel="noopener"}

> If safety regulations are written in blood, what are security policies written in? Sweat and cursing? Australian health insurance company Medibank will take all of its IT systems offline and close its branches over the weekend as part of its ongoing efforts to improve security and recover from a massive data security breach in October.... [...]
