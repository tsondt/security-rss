Title: Microsoft: Hackers target cryptocurrency firms over Telegram
Date: 2022-12-06T13:56:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency;Microsoft
Slug: 2022-12-06-microsoft-hackers-target-cryptocurrency-firms-over-telegram

[Source](https://www.bleepingcomputer.com/news/security/microsoft-hackers-target-cryptocurrency-firms-over-telegram/){:target="_blank" rel="noopener"}

> Microsoft says that cryptocurrency investment companies have been targeted by a threat group it tracks as DEV-0139 via Telegram groups used to communicate with the firms' VIP customers. [...]
