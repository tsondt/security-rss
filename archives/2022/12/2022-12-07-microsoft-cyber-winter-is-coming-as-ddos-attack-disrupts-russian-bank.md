Title: Microsoft: (Cyber) winter is coming as DDoS attack disrupts Russian bank
Date: 2022-12-07T07:25:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-07-microsoft-cyber-winter-is-coming-as-ddos-attack-disrupts-russian-bank

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/07/ddos_attack_russian_bank/){:target="_blank" rel="noopener"}

> Where's the Night's Watch when you need them? Microsoft has warned Europe to be on alert for cyber attacks from Russia this winter, just as a series of attacks hit Russian organizations – including the country's second-largest bank.... [...]
