Title: Apple accused of censoring apps in Hong Kong and Russia to maintain market access
Date: 2022-12-22T07:01:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-12-22-apple-accused-of-censoring-apps-in-hong-kong-and-russia-to-maintain-market-access

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/22/apple_accused_of_censoring_apps/){:target="_blank" rel="noopener"}

> Activists note absence of VPNs ponder whether Apple may put revenue above human rights in some markets Apple has been accused of selling out human rights for the sake of profit by cooperating with authoritarian censorship demands in China and Russia, according to two reports issued on Thursday.... [...]
