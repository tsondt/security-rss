Title: Existential Risk and the Fermi Paradox
Date: 2022-12-02T20:07:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;complexity;risks;security analysis
Slug: 2022-12-02-existential-risk-and-the-fermi-paradox

[Source](https://www.schneier.com/blog/archives/2022/12/existential-risk-and-the-fermi-paradox.html){:target="_blank" rel="noopener"}

> We know that complexity is the worst enemy of security, because it makes attack easier and defense harder. This becomes catastrophic as the effects of that attack become greater. In A Hacker’s Mind (coming in February 2023), I write: Our societal systems, in general, may have grown fairer and more just over the centuries, but progress isn’t linear or equitable. The trajectory may appear to be upwards when viewed in hindsight, but from a more granular point of view there are a lot of ups and downs. It’s a “noisy” process. Technology changes the amplitude of the noise. Those near-term [...]
