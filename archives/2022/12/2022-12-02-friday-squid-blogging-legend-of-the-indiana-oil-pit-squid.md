Title: Friday Squid Blogging: Legend of the Indiana Oil-Pit Squid
Date: 2022-12-02T22:12:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-12-02-friday-squid-blogging-legend-of-the-indiana-oil-pit-squid

[Source](https://www.schneier.com/blog/archives/2022/12/friday-squid-blogging-legend-of-the-indiana-oil-pit-squid.html){:target="_blank" rel="noopener"}

> At a GMC plant. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
