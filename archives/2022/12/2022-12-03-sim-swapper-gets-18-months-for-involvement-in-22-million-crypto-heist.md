Title: SIM swapper gets 18-months for involvement in $22 million crypto heist
Date: 2022-12-03T11:15:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-12-03-sim-swapper-gets-18-months-for-involvement-in-22-million-crypto-heist

[Source](https://www.bleepingcomputer.com/news/security/sim-swapper-gets-18-months-for-involvement-in-22-million-crypto-heist/){:target="_blank" rel="noopener"}

> Florida man Nicholas Truglia was sentenced to 18 months in prison on Thursday for his involvement in a fraud scheme that led to the theft of millions from cryptocurrency investor Michael Terpin. [...]
