Title: Open-source repositories flooded by 144,000 phishing packages
Date: 2022-12-14T09:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-14-open-source-repositories-flooded-by-144000-phishing-packages

[Source](https://www.bleepingcomputer.com/news/security/open-source-repositories-flooded-by-144-000-phishing-packages/){:target="_blank" rel="noopener"}

> Unknown threat actors have uploaded a total of 144,294 phishing-related packages on the open-source package repositories NuGet, PyPI, and NPM. [...]
