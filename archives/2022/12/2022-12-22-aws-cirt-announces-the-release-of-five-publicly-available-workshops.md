Title: AWS CIRT announces the release of five publicly available workshops
Date: 2022-12-22T19:49:41+00:00
Author: Steve de Vera
Category: AWS Security
Tags: Announcements;Intermediate (200);Security;Security, Identity, & Compliance;Amazon EC2;Amazon S3;Athena;AWS CloudTrail;cryptomining;Incident response;ransomware;threat detection
Slug: 2022-12-22-aws-cirt-announces-the-release-of-five-publicly-available-workshops

[Source](https://aws.amazon.com/blogs/security/aws-cirt-announces-the-release-of-five-publicly-available-workshops/){:target="_blank" rel="noopener"}

> Greetings from the AWS Customer Incident Response Team (CIRT)! AWS CIRT is dedicated to supporting customers during active security events on the customer side of the AWS Shared Responsibility Model. Over the past year, AWS CIRT has responded to hundreds of such security events, including the unauthorized use of AWS Identity and Access Management (IAM) [...]
