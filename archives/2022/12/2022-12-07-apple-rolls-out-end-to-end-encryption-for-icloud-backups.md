Title: Apple rolls out end-to-end encryption for iCloud backups
Date: 2022-12-07T15:55:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2022-12-07-apple-rolls-out-end-to-end-encryption-for-icloud-backups

[Source](https://www.bleepingcomputer.com/news/apple/apple-rolls-out-end-to-end-encryption-for-icloud-backups/){:target="_blank" rel="noopener"}

> Apple introduced today Advanced Data Protection for iCloud, a new feature that uses end-to-end encryption to protect sensitive iCloud data, including backups, photos, notes, and more. [...]
