Title: Parental control apps prove easy to beat by kids and crims
Date: 2022-12-21T03:00:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-21-parental-control-apps-prove-easy-to-beat-by-kids-and-crims

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/21/parental_control_app_bugs/){:target="_blank" rel="noopener"}

> 20m downloads can't be wrong? Or can they? Parental control apps may do more harm than good, according to researchers who found 18 bugs in eight Android apps with more than 20 million total downloads that could be exploited to, among many nefarious acts, control other devices on the parents' network.... [...]
