Title: How to investigate and take action on security issues in Amazon EKS clusters with Amazon Detective – Part 2
Date: 2022-12-05T18:05:29+00:00
Author: Marshall Jones
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Containers;Detective;EKS;GuardDuty;Incident response;Kubernetes;Security;Security Blog;threat detection
Slug: 2022-12-05-how-to-investigate-and-take-action-on-security-issues-in-amazon-eks-clusters-with-amazon-detective-part-2

[Source](https://aws.amazon.com/blogs/security/how-to-investigate-and-take-action-on-security-issues-in-amazon-eks-clusters-with-amazon-detective-part-2/){:target="_blank" rel="noopener"}

> In part 1 of this of this two-part series, How to detect security issues in Amazon EKS cluster using Amazon GuardDuty, we walked through a real-world observed security issue in an Amazon Elastic Kubernetes Service (Amazon EKS) cluster and saw how Amazon GuardDuty detected each phase by following MITRE ATT&CK tactics. In this blog post, [...]
