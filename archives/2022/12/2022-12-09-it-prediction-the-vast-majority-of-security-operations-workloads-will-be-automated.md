Title: IT prediction: the vast majority of security operations workloads will be automated
Date: 2022-12-09T17:00:00+00:00
Author: Iman Ghanizada
Category: GCP Security
Tags: Identity & Security
Slug: 2022-12-09-it-prediction-the-vast-majority-of-security-operations-workloads-will-be-automated

[Source](https://cloud.google.com/blog/products/identity-security/it-prediction-vast-majority-of-security-operations-workloads-will-be-automated/){:target="_blank" rel="noopener"}

> Editor's note : This post is part of an ongoing series on IT predictions from Google Cloud experts. Check out the full list of our predictions on how IT will change in the coming years. Prediction: By 2025, 90% of security operations workflows will be automated and managed as code There is not enough funding, resourcing, skills, or broadly applicable solutions to help manage security risk effectively across modern technology environments. Organizations are struggling to identify which alerts and security areas to prioritize while moving quickly through their digital transformation. This challenge is compounded by an exponential increase in data [...]
