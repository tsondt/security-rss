Title: The Decoupling Principle
Date: 2022-12-07T12:04:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;anonymity;authentication;privacy;pseudonymity
Slug: 2022-12-07-the-decoupling-principle

[Source](https://www.schneier.com/blog/archives/2022/12/the-decoupling-principle.html){:target="_blank" rel="noopener"}

> This is a really interesting paper that discusses what the authors call the Decoupling Principle: The idea is simple, yet previously not clearly articulated: to ensure privacy, information should be divided architecturally and institutionally such that each entity has only the information they need to perform their relevant function. Architectural decoupling entails splitting functionality for different fundamental actions in a system, such as decoupling authentication (proving who is allowed to use the network) from connectivity (establishing session state for communicating). Institutional decoupling entails splitting what information remains between non-colluding entities, such as distinct companies or network operators, or between a [...]
