Title: Introducing the Security Design of the AWS Nitro System whitepaper
Date: 2022-12-19T20:37:18+00:00
Author: J.D. Bean
Category: AWS Security
Tags: Amazon EC2;Announcements;Foundational (100);Security;Security, Identity, & Compliance;Amazon Elastic Compute Cloud (Amazon EC2);AWS Nitro Enclaves;AWS Nitro System;Confidential Computing;Elastic Compute Cloud
Slug: 2022-12-19-introducing-the-security-design-of-the-aws-nitro-system-whitepaper

[Source](https://aws.amazon.com/blogs/security/introducing-the-security-design-of-the-aws-nitro-system-whitepaper/){:target="_blank" rel="noopener"}

> AWS recently released a whitepaper on the Security Design of the AWS Nitro System. The Nitro System is a combination of purpose-built server designs, data processors, system management components, and specialized firmware that serves as the underlying virtualization technology that powers all Amazon Elastic Compute Cloud (Amazon EC2) instances launched since early 2018. With the [...]
