Title: The Week in Ransomware - December 23rd 2022 - Targeting Microsoft Exchange
Date: 2022-12-23T15:51:03-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-23-the-week-in-ransomware-december-23rd-2022-targeting-microsoft-exchange

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-23rd-2022-targeting-microsoft-exchange/){:target="_blank" rel="noopener"}

> Reports this week illustrate how threat actors consider Microsoft Exchange as a prime target for gaining initial access to corporate networks to steal data and deploy ransomware. [...]
