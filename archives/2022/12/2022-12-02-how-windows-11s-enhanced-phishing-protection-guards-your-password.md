Title: How Windows 11's Enhanced Phishing Protection guards your password
Date: 2022-12-02T10:14:07-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-12-02-how-windows-11s-enhanced-phishing-protection-guards-your-password

[Source](https://www.bleepingcomputer.com/news/microsoft/how-windows-11s-enhanced-phishing-protection-guards-your-password/){:target="_blank" rel="noopener"}

> One of the easier ways to steal a user's credentials is through a convincing fake login page or application. To help combat the constant risk of password theft, Microsoft added enhanced phishing protection in Windows 11 Version 22H2. [...]
