Title: Malicious ‘SentinelOne’ PyPI package steals data from developers
Date: 2022-12-19T12:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-19-malicious-sentinelone-pypi-package-steals-data-from-developers

[Source](https://www.bleepingcomputer.com/news/security/malicious-sentinelone-pypi-package-steals-data-from-developers/){:target="_blank" rel="noopener"}

> Threat actors have published a malicious Python package on PyPI, named 'SentinelOne,' that pretends to be the legitimate SDK client for the trusted American cybersecurity firm but, in reality, steals data from developers. [...]
