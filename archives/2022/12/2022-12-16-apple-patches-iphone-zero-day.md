Title: Apple Patches iPhone Zero-Day
Date: 2022-12-16T12:04:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;iOS;iPhone;patching;zero-day
Slug: 2022-12-16-apple-patches-iphone-zero-day

[Source](https://www.schneier.com/blog/archives/2022/12/apple-patches-iphone-zero-day.html){:target="_blank" rel="noopener"}

> The most recent iPhone update—to version 16.1.2—patches a zero-day vulnerability that “may have been actively exploited against versions of iOS released before iOS 15.1.” News : Apple said security researchers at Google’s Threat Analysis Group, which investigates nation state-backed spyware, hacking and cyberattacks, discovered and reported the WebKit bug. WebKit bugs are often exploited when a person visits a malicious domain in their browser (or via the in-app browser). It’s not uncommon for bad actors to find vulnerabilities that target WebKit as a way to break into the device’s operating system and the user’s private data. WebKit bugs can be [...]
