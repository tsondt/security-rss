Title: New Ransom Payment Schemes Target Executives, Telemedicine
Date: 2022-12-08T18:25:04+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;The Coming Storm;Web Fraud 2.0;alex holden;CL0P;CLOP ransomware;Emsisoft;Fabian Wosar;Hold Security;TA505;Tripwire;Venus ransomware
Slug: 2022-12-08-new-ransom-payment-schemes-target-executives-telemedicine

[Source](https://krebsonsecurity.com/2022/12/new-ransom-payment-schemes-target-executives-telemedicine/){:target="_blank" rel="noopener"}

> Ransomware groups are constantly devising new methods for infecting victims and convincing them to pay up, but a couple of strategies tested recently seem especially devious. The first centers on targeting healthcare organizations that offer consultations over the Internet and sending them booby-trapped medical records for the “patient.” The other involves carefully editing email inboxes of public company executives to make it appear that some were involved in insider trading. Alex Holden is founder of Hold Security, a Milwaukee-based cybersecurity firm. Holden’s team gained visibility into discussions among members of two different ransom groups: CLOP (a.k.a. “ Cl0p ” a.k.a. [...]
