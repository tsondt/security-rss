Title: North Korea using freelance techies to fund missiles and nukes
Date: 2022-12-09T03:35:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-09-north-korea-using-freelance-techies-to-fund-missiles-and-nukes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/09/dprk_tech_freelancer_military_funding/){:target="_blank" rel="noopener"}

> You won't see 'Agent of vile murderous autocracy' on their CVs. Or their faces on vid chats North Korean IT pros are using freelancing platforms to earn money that the nation's authoritarian government uses to fund the development of missiles and nuclear weapons, according to South Korea's government. Seoul therefore wants gig platforms to impose stricter checks to restrict its enemy's activities.... [...]
