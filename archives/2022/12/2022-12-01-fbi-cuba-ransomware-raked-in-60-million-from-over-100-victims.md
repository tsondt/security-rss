Title: FBI: Cuba ransomware raked in $60 million from over 100 victims
Date: 2022-12-01T15:09:15-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-01-fbi-cuba-ransomware-raked-in-60-million-from-over-100-victims

[Source](https://www.bleepingcomputer.com/news/security/fbi-cuba-ransomware-raked-in-60-million-from-over-100-victims/){:target="_blank" rel="noopener"}

> The FBI and CISA revealed in a new joint security advisory that the Cuba ransomware gang raked in over $60 million in ransoms as of August 2022 after breaching more than 100 victims worldwide. [...]
