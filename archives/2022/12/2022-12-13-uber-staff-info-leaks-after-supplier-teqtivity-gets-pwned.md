Title: Uber staff info leaks after supplier Teqtivity gets pwned
Date: 2022-12-13T22:46:56+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-13-uber-staff-info-leaks-after-supplier-teqtivity-gets-pwned

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/13/uber_data_breach_teqtivity/){:target="_blank" rel="noopener"}

> Thankfully no customer info – but the spotlight is back on third-party attacks Uber, which has suffered a few data thefts in its time, is this week dealing with the fallout from yet another – this time from one of its technology suppliers.... [...]
