Title: Twenty years on, command-line virus scanner ClamAV puts out version 1
Date: 2022-12-01T10:51:04+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-12-01-twenty-years-on-command-line-virus-scanner-clamav-puts-out-version-1

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/01/clamav_puts_out_version_1/){:target="_blank" rel="noopener"}

> Used by millions – and the first official finished version The ClamAV command-line virus scanner used on many Linux boxes has attained an important-looking milestone release: version 1.0.0.... [...]
