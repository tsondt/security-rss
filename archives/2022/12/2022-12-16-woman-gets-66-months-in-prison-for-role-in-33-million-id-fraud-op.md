Title: Woman gets 66 months in prison for role in $3.3 million ID fraud op
Date: 2022-12-16T12:03:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-12-16-woman-gets-66-months-in-prison-for-role-in-33-million-id-fraud-op

[Source](https://www.bleepingcomputer.com/news/security/woman-gets-66-months-in-prison-for-role-in-33-million-id-fraud-op/){:target="_blank" rel="noopener"}

> The Australian Federal Police (AFP) have announced today that a 24-year-old woman from Melbourne, arrested in 2019 for her role in large-scale, cyber-enabled identity theft crimes, was sentenced to five years and six months in prison. [...]
