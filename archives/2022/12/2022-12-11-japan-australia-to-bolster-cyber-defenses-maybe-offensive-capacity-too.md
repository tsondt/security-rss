Title: Japan, Australia, to bolster cyber-defenses, maybe offensive capacity too
Date: 2022-12-11T23:06:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-12-11-japan-australia-to-bolster-cyber-defenses-maybe-offensive-capacity-too

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/11/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> FTX Japan payment promise evaporates; VR/AR to boom across APAC; Google wins privacy case Asia In Brief Australia's home affairs and cybersecurity minister Clare O'Neill has given the nation a goal of becoming the world's most cyber secure nation by 2030.... [...]
