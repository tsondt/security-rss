Title: Five British companies fined for making half a million nuisance calls
Date: 2022-12-08T11:41:32+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-12-08-five-british-companies-fined-for-making-half-a-million-nuisance-calls

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/08/ico_fines_marketing/){:target="_blank" rel="noopener"}

> Nice. They went after vulnerable people and folks over 60 who opted out of marketing calls Britain's data watchdog has slapped financial penalties totaling £435,000 (c $529,000) on five companies it says collectively made almost half of million marketing calls to people registered with the Telephone Preference Service (TPS).... [...]
