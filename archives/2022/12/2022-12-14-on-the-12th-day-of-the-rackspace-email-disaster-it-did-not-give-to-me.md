Title: On the 12th day of the Rackspace email disaster, it did not give to me …
Date: 2022-12-14T23:55:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-14-on-the-12th-day-of-the-rackspace-email-disaster-it-did-not-give-to-me

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/14/rackspace_email_outage/){:target="_blank" rel="noopener"}

> ... a working Exchange inbox tree There's no end – or restored data – in sight for some Rackspace customers now on day 12 of the company's ransomware-induced hosted Exchange email outage.... [...]
