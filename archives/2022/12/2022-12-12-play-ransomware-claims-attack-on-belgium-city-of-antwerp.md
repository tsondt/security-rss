Title: Play ransomware claims attack on Belgium city of Antwerp
Date: 2022-12-12T18:34:25-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-12-play-ransomware-claims-attack-on-belgium-city-of-antwerp

[Source](https://www.bleepingcomputer.com/news/security/play-ransomware-claims-attack-on-belgium-city-of-antwerp/){:target="_blank" rel="noopener"}

> The Play ransomware operation has claimed responsibility for a recent cyberattack on the Belgium city of Antwerp. [...]
