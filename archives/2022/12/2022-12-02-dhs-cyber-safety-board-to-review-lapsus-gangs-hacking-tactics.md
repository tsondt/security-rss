Title: DHS Cyber Safety Board to review Lapsus$ gang’s hacking tactics
Date: 2022-12-02T14:05:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-02-dhs-cyber-safety-board-to-review-lapsus-gangs-hacking-tactics

[Source](https://www.bleepingcomputer.com/news/security/dhs-cyber-safety-board-to-review-lapsus-gang-s-hacking-tactics/){:target="_blank" rel="noopener"}

> The Department of Homeland Security (DHS) Cyber Safety Review Board will review attacks linked to an extortion group known as Lapsus$, which breached multiple high-profile companies in recent attacks. [...]
