Title: Hackers steal $8 million from users running trojanized BitKeep apps
Date: 2022-12-27T10:42:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-12-27-hackers-steal-8-million-from-users-running-trojanized-bitkeep-apps

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-8-million-from-users-running-trojanized-bitkeep-apps/){:target="_blank" rel="noopener"}

> Multiple BitKeep crypto wallet users reported that their wallets were emptied during Christmas after hackers triggered transactions that didn't require verification. [...]
