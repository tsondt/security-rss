Title: 2022 FINMA ISAE 3000 Type II attestation report now available with 154 services in scope
Date: 2022-12-23T18:41:34+00:00
Author: Daniel Fuertes
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS Compliance;AWS FINMA;AWS security;Certification;cybersecurity;Financial Services;Outsourcing Guidelines;report;Security Blog;Swiss banking regulations;Switzerland
Slug: 2022-12-23-2022-finma-isae-3000-type-ii-attestation-report-now-available-with-154-services-in-scope

[Source](https://aws.amazon.com/blogs/security/2022-finma-isae-3000-type-ii-attestation-report-now-available-with-154-services-in-scope/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the third issuance of the Swiss Financial Market Supervisory Authority (FINMA) International Standard on Assurance Engagements (ISAE) 3000 Type II attestation report. The scope of the report covers a total of 154 services and 24 global AWS Regions. The latest FINMA ISAE 3000 Type II report covers [...]
