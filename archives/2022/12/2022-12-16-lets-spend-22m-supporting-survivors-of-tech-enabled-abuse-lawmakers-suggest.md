Title: Let's spend $22m supporting survivors of tech-enabled abuse, lawmakers suggest
Date: 2022-12-16T22:43:45+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-16-lets-spend-22m-supporting-survivors-of-tech-enabled-abuse-lawmakers-suggest

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/16/tech_safety_domestic_violence_wyden/){:target="_blank" rel="noopener"}

> And the corporations making the tools for stalking and harassment in the first place? Anyone? A bipartisan trio of US lawmakers has proposed a law that pledges as much as $22 million of public funding to help victims of tech-enabled domestic abuse.... [...]
