Title: KmsdBot botnet is down after operator sends typo in command
Date: 2022-12-06T13:30:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-12-06-kmsdbot-botnet-is-down-after-operator-sends-typo-in-command

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/06/botnet_kmsdbot_typo_code/){:target="_blank" rel="noopener"}

> Cashdollar: 'It’s not often we get this kind of story in security' Somewhere out there, a botnet operator is kicking themselves and probably hoping no one noticed the typo they transmitted in a command that crashed their whole operation.... [...]
