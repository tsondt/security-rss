Title: QR Code Scam
Date: 2022-12-28T18:14:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;forgery;QR codes;scams
Slug: 2022-12-28-qr-code-scam

[Source](https://www.schneier.com/blog/archives/2022/12/qr-code-scam.html){:target="_blank" rel="noopener"}

> An enterprising individual made fake parking tickets with a QR code for easy payment. [...]
