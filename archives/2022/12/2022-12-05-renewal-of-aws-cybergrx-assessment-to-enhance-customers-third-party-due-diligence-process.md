Title: Renewal of AWS CyberGRX assessment to enhance customers’ third-party due diligence process
Date: 2022-12-05T20:14:29+00:00
Author: Naranjan Goklani
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;3P Risk;AWS security;Compliance;Cyber Risk Management;CyberGRX;CyberGRX Assessment;Security Blog;Third Party Risk;Third-Party Risk Management;TPRM
Slug: 2022-12-05-renewal-of-aws-cybergrx-assessment-to-enhance-customers-third-party-due-diligence-process

[Source](https://aws.amazon.com/blogs/security/renewal-of-aws-cybergrx-assessment-to-enhance-customers-third-party-due-diligence-process/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce renewal of the AWS CyberGRX cyber risk assessment report. This third-party validated report helps customers perform effective cloud supplier due diligence on AWS and enhances their third-party risk management process. With the increase in adoption of cloud products and services across multiple sectors and industries, AWS has become a critical component of [...]
