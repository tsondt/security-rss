Title: Iran-linked Charming Kitten espionage gang bares claws to pollies, power orgs
Date: 2022-12-15T02:35:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-15-iran-linked-charming-kitten-espionage-gang-bares-claws-to-pollies-power-orgs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/15/charming_kitten_ta453_expands_targets/){:target="_blank" rel="noopener"}

> If you get email from 'Samantha Wolf', congrats: you're important enough to make a decent target An Iranian cyber espionage gang with ties to the Islamic Revolutionary Guard Corps has learned new methods and phishing techniques, and aimed them at a wider set of targets – including politicians, government officials, critical infrastructure and medical researchers – according to email security vendor Proofpoint.... [...]
