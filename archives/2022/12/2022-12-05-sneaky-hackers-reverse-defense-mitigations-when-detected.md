Title: Sneaky hackers reverse defense mitigations when detected
Date: 2022-12-05T15:08:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-05-sneaky-hackers-reverse-defense-mitigations-when-detected

[Source](https://www.bleepingcomputer.com/news/security/sneaky-hackers-reverse-defense-mitigations-when-detected/){:target="_blank" rel="noopener"}

> A financially motivated threat actor is hacking telecommunication service providers and business process outsourcing firms, actively reversing defensive mitigations applied when the breach is detected. [...]
