Title: ICYMI: Security Talks goes deep on SolarWinds, Chrome, Zero Trust, and today’s toughest SOC challenges
Date: 2022-12-20T17:00:00+00:00
Author: Dan Kaplan
Category: GCP Security
Tags: Identity & Security
Slug: 2022-12-20-icymi-security-talks-goes-deep-on-solarwinds-chrome-zero-trust-and-todays-toughest-soc-challenges

[Source](https://cloud.google.com/blog/products/identity-security/security-talks-on-solarwinds-lessons-todays-toughest-soc-challenges-and-more/){:target="_blank" rel="noopener"}

> Security operations teams are facing a “perfect storm” of challenges from nation-state actors turning their attention to financial crime, to rising uncertainty and potential complexities because of rapidly advancing cloud migration and IoT adoption, to the long-lamented skills shortage. Now imagine having to face this trifecta without visibility into your IT and security infrastructure because scale and cost concerns have impeded your ability to ingest and monitor critical telemetry. To help organizations rethink threat detection and broader security team efforts, we showcased at this month’s Google Cloud Security Talks a keynote on why DEI is a cybersecurity imperative, Mandiant’s review [...]
