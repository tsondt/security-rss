Title: Cisco’s Talos security bods predict new wave of Excel Hell
Date: 2022-12-21T00:08:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-21-ciscos-talos-security-bods-predict-new-wave-of-excel-hell

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/21/microsoft_talos_excel_xll_threats/){:target="_blank" rel="noopener"}

> Criminals have noticed that spreadsheet's XLL files add custom functionality - including malware It took a few years and one temporary halt, but in July Microsoft finally began blocking certain macros by default in Word, Excel, and PowerPoint, cutting off a popular attack vector for those who target users of Microsoft's Windows OS and Office suite.... [...]
