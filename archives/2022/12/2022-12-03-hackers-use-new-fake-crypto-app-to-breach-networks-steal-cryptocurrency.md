Title: Hackers use new, fake crypto app to breach networks, steal cryptocurrency
Date: 2022-12-03T10:12:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-12-03-hackers-use-new-fake-crypto-app-to-breach-networks-steal-cryptocurrency

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-new-fake-crypto-app-to-breach-networks-steal-cryptocurrency/){:target="_blank" rel="noopener"}

> The North Korean 'Lazarus' hacking group is linked to a new attack spreading fake cryptocurrency apps under the made-up brand, "BloxHolder," to install the AppleJeus malware for initial access to networks and steal crypto assets. [...]
