Title: Critical IP spoofing bug patched in Cacti
Date: 2022-12-15T14:24:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-15-critical-ip-spoofing-bug-patched-in-cacti

[Source](https://portswigger.net/daily-swig/critical-ip-spoofing-bug-patched-in-cacti){:target="_blank" rel="noopener"}

> ‘Not that hard to execute if attacker has access to a monitoring platform running Cacti’ [...]
