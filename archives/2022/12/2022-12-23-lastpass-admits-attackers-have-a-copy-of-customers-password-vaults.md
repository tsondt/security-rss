Title: LastPass admits attackers have a copy of customers’ password vaults
Date: 2022-12-23T06:35:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-23-lastpass-admits-attackers-have-a-copy-of-customers-password-vaults

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/23/lastpass_attack_update/){:target="_blank" rel="noopener"}

> Thankfully a well encrypted copy that could take an eon to crack, unless users practiced bad password hygiene Password locker LastPass has warned customers that the August 2022 attack on its systems saw unknown parties copy encrypted files that contains the passwords to their accounts.... [...]
