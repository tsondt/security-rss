Title: The Equifax Breach Settlement Offer is Real, For Now
Date: 2022-12-20T20:08:40+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Other;Equifax breach;Equifax Class Action Settlement;myprepaidcenter.com
Slug: 2022-12-20-the-equifax-breach-settlement-offer-is-real-for-now

[Source](https://krebsonsecurity.com/2022/12/the-equifax-breach-settlement-offer-is-real-for-now/){:target="_blank" rel="noopener"}

> Millions of people likely just received an email or snail mail notice saying they’re eligible to claim a class action payment in connection with the 2017 megabreach at consumer credit bureau Equifax. Given the high volume of reader inquiries about this, it seemed worth pointing out that while this particular offer is legit (if paltry), scammers are likely to soon capitalize on public attention to the settlement money. One reader’s copy of their Equifax Breach Settlement letter. They received a check for $6.97. In 2017, Equifax disclosed a massive, extended data breach that led to the theft of Social Security [...]
