Title: Hackers leak personal info allegedly stolen from 5.7M Gemini users
Date: 2022-12-15T16:10:28-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-12-15-hackers-leak-personal-info-allegedly-stolen-from-57m-gemini-users

[Source](https://www.bleepingcomputer.com/news/security/hackers-leak-personal-info-allegedly-stolen-from-57m-gemini-users/){:target="_blank" rel="noopener"}

> Gemini crypto exchange announced this week that customers were targeted in phishing campaigns after a threat actor collected their personal information from a third-party vendor. [...]
