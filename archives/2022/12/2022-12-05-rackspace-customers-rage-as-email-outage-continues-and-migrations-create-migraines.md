Title: Rackspace customers rage as email outage continues and migrations create migraines
Date: 2022-12-05T04:45:43+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-05-rackspace-customers-rage-as-email-outage-continues-and-migrations-create-migraines

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/05/rackspace_hosted_exchange_security_update/){:target="_blank" rel="noopener"}

> Hosting company has nothing to say on data loss, restore times, or root cause Rackspace has not offered any explanation of the "security incident" that has taken out its hosted Exchange environment and led the company to predict multiple days of downtime before restoration.... [...]
