Title: NIST says you better dump weak SHA-1 ... by 2030
Date: 2022-12-16T02:28:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-12-16-nist-says-you-better-dump-weak-sha-1-by-2030

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/16/nist_sets_sha1_retirement_date/){:target="_blank" rel="noopener"}

> How about right now? Right now is good The US National Institute of Standards and Technology (NIST) says it's time to retire Secure Hash Algorithm-1 (SHA-1), a 27-year-old weak algorithm used in security applications.... [...]
