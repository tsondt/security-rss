Title: FCC calls for mega $300 million fine for massive US robocall campaign
Date: 2022-12-22T20:57:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-22-fcc-calls-for-mega-300-million-fine-for-massive-us-robocall-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/22/fcc_robocalls_fine/){:target="_blank" rel="noopener"}

> 5 billion calls over three months. Was your phone spammed? US regulators want to fine the operators of a claimed massive robocall operation almost $300 million that made more than 5 billion pre-recorded calls over three months early last year.... [...]
