Title: Friday Squid Blogging: Injured Giant Squid and Paddleboarder
Date: 2022-12-23T22:05:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2022-12-23-friday-squid-blogging-injured-giant-squid-and-paddleboarder

[Source](https://www.schneier.com/blog/archives/2022/12/friday-squid-blogging-injured-giant-squid-and-paddleboarder.html){:target="_blank" rel="noopener"}

> Here’s a video —I don’t know where it’s from—of an injured juvenile male giant squid grabbing on to a paddleboard. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
