Title: Researchers smell a cryptomining Chaos RAT targeting Linux systems
Date: 2022-12-13T08:32:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-13-researchers-smell-a-cryptomining-chaos-rat-targeting-linux-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/13/cryptoming_chaos_rat_targets_linux/){:target="_blank" rel="noopener"}

> Smells like Russian miscreants A type of cryptomining malware targeting Linux-based systems has added capabilities by incorporating an open source remote access trojan called Chaos RAT with several advanced functions that bad guys can use to control remote operating systems.... [...]
