Title: Email hijackers scam food out of businesses, not just money
Date: 2022-12-17T15:00:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-12-17-email-hijackers-scam-food-out-of-businesses-not-just-money

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/17/in_brief_security/){:target="_blank" rel="noopener"}

> Also, TLC gets schooled by Karakurt, and Cloudflare is offering free zero trust stuff to some small companies In brief Business email compromise (BEC) continues to be a multibillion-dollar threat, but it's evolving, with the FBI and other federal agencies warning that cybercriminals have started using spoofed emails to steal shipments of physical goods – in this case, food.... [...]
