Title: Being one of the 1% sucks if you're a Rackspace user
Date: 2022-12-22T00:35:26+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-22-being-one-of-the-1-sucks-if-youre-a-rackspace-user

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/22/rackspace_outage_customers/){:target="_blank" rel="noopener"}

> Nearly three weeks and no email for customers As the Rackspace email fiasco approaches week three with the company's hosted Exchange customers' data in limbo, Rackspace execs still won't put an exact number on how many customers were affected by the ransomware-induced email outage, or when — if — they'll be able to recover their old messages and contacts.... [...]
