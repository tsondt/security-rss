Title: Hackers hijack Linux devices using PRoot isolated filesystems
Date: 2022-12-05T12:15:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2022-12-05-hackers-hijack-linux-devices-using-proot-isolated-filesystems

[Source](https://www.bleepingcomputer.com/news/security/hackers-hijack-linux-devices-using-proot-isolated-filesystems/){:target="_blank" rel="noopener"}

> Hackers are abusing the open-source Linux PRoot utility in BYOF (Bring Your Own Filesystem) attacks to provide a consistent repository of malicious tools that work on many Linux distributions. [...]
