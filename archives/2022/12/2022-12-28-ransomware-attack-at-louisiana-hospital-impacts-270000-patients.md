Title: Ransomware attack at Louisiana hospital impacts 270,000 patients
Date: 2022-12-28T08:54:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2022-12-28-ransomware-attack-at-louisiana-hospital-impacts-270000-patients

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-at-louisiana-hospital-impacts-270-000-patients/){:target="_blank" rel="noopener"}

> The Lake Charles Memorial Health System (LCMHS) is sending out notices of a data breach affecting almost 270,000 people who have received care at one of its medical centers. [...]
