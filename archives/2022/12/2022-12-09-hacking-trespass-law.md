Title: Hacking Trespass Law
Date: 2022-12-09T20:02:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;courts;hacking
Slug: 2022-12-09-hacking-trespass-law

[Source](https://www.schneier.com/blog/archives/2022/12/hacking-trespass-law.html){:target="_blank" rel="noopener"}

> This article talks about public land in the US that is completely surrounded by private land, which in some cases makes it inaccessible to the public. But there’s a hack: Some hunters have long believed, however, that the publicly owned parcels on Elk Mountain can be legally reached using a practice called corner-crossing. Corner-crossing can be visualized in terms of a checkerboard. Ever since the Westward Expansion, much of the Western United States has been divided into alternating squares of public and private land. Corner-crossers, like checker pieces, literally step from one public square to another in diagonal fashion, avoiding [...]
