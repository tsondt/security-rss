Title: States label TikTok 'a malicious and menacing threat'
Date: 2022-12-08T04:30:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-12-08-states-label-tiktok-a-malicious-and-menacing-threat

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/08/texas_bans_indiana_tiktok_lawsuit/){:target="_blank" rel="noopener"}

> Texas bucks app off government devices as Indiana takes social media biz to court Two more US states have launched aggressive action against made-in-China social media app TikTok.... [...]
