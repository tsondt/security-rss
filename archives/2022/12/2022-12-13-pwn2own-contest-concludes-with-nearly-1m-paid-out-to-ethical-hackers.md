Title: Pwn2Own contest concludes with nearly $1m paid out to ethical hackers
Date: 2022-12-13T02:15:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-13-pwn2own-contest-concludes-with-nearly-1m-paid-out-to-ethical-hackers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/13/pwn2own_wraps/){:target="_blank" rel="noopener"}

> Which is pocket change compared to what criminals will pay for zero-days, but thankfully community spirit remains strong Pwn2Own paid out almost $1 million to bug hunters at last week's consumer product hacking event in Toronto, but the prize money wasn't big enough attract attempts at cracking the iPhone or Google Pixel because miscreants can score far more from less wholesome sources.... [...]
