Title: Nvidia patches 29 GPU driver bugs that could lead to code execution, device takeover
Date: 2022-12-01T23:30:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-01-nvidia-patches-29-gpu-driver-bugs-that-could-lead-to-code-execution-device-takeover

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/01/nvidia_gpu_driver_bugs/){:target="_blank" rel="noopener"}

> Take a break from the gaming and fix these now Nvidia fixed more than two dozen security flaws in its GPU display driver, the most severe of which could allow an unprivileged user to modify files, and then escalate privileges, execute code, tamper with or steal data, or even take over your device.... [...]
