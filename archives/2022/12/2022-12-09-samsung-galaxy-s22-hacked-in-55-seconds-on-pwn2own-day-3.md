Title: Samsung Galaxy S22 hacked in 55 seconds on Pwn2Own Day 3
Date: 2022-12-09T10:48:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-09-samsung-galaxy-s22-hacked-in-55-seconds-on-pwn2own-day-3

[Source](https://www.bleepingcomputer.com/news/security/samsung-galaxy-s22-hacked-in-55-seconds-on-pwn2own-day-3/){:target="_blank" rel="noopener"}

> On the third day of Pwn2Own, contestants hacked the Samsung Galaxy S22 a fourth time since the start of the competition, and this time they did it in just 55 seconds. [...]
