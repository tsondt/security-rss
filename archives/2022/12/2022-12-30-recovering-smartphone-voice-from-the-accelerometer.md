Title: Recovering Smartphone Voice from the Accelerometer
Date: 2022-12-30T12:18:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;eavesdropping;side-channel attacks;smartphones
Slug: 2022-12-30-recovering-smartphone-voice-from-the-accelerometer

[Source](https://www.schneier.com/blog/archives/2022/12/recovering-smartphone-voice-from-the-accelerometer.html){:target="_blank" rel="noopener"}

> Yet another smartphone side-channel attack: “ EarSpy: Spying Caller Speech and Identity through Tiny Vibrations of Smartphone Ear Speakers “: Abstract: Eavesdropping from the user’s smartphone is a well-known threat to the user’s safety and privacy. Existing studies show that loudspeaker reverberation can inject speech into motion sensor readings, leading to speech eavesdropping. While more devastating attacks on ear speakers, which produce much smaller scale vibrations, were believed impossible to eavesdrop with zero-permission motion sensors. In this work, we revisit this important line of reach. We explore recent trends in smartphone manufacturers that include extra/powerful speakers in place of small [...]
