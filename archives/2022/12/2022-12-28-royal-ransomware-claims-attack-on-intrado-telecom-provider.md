Title: Royal ransomware claims attack on Intrado telecom provider
Date: 2022-12-28T13:40:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-28-royal-ransomware-claims-attack-on-intrado-telecom-provider

[Source](https://www.bleepingcomputer.com/news/security/royal-ransomware-claims-attack-on-intrado-telecom-provider/){:target="_blank" rel="noopener"}

> ​​​​​​​The Royal Ransomware gang claimed responsibility for a cyber attack against telecommunications company Intrado on Tuesday. [...]
