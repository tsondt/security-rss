Title: Lastpass: Hackers stole customer vault data in cloud storage breach
Date: 2022-12-22T16:12:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-22-lastpass-hackers-stole-customer-vault-data-in-cloud-storage-breach

[Source](https://www.bleepingcomputer.com/news/security/lastpass-hackers-stole-customer-vault-data-in-cloud-storage-breach/){:target="_blank" rel="noopener"}

> LastPass revealed today that attackers stole customer vault data after breaching its cloud storage earlier this year using information stolen during an August 2022 incident. [...]
