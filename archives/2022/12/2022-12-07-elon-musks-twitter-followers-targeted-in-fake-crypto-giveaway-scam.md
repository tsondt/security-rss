Title: Elon Musk's Twitter followers targeted in fake crypto giveaway scam
Date: 2022-12-07T07:16:38-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-12-07-elon-musks-twitter-followers-targeted-in-fake-crypto-giveaway-scam

[Source](https://www.bleepingcomputer.com/news/security/elon-musks-twitter-followers-targeted-in-fake-crypto-giveaway-scam/){:target="_blank" rel="noopener"}

> Twitter accounts giving Elon Musk a follow are being targeted in a crypto giveaway scam dubbed 'Freedom Giveaway.' [...]
