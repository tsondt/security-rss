Title: LEGO BrickLink bugs let hackers hijack accounts, breach servers
Date: 2022-12-15T08:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-15-lego-bricklink-bugs-let-hackers-hijack-accounts-breach-servers

[Source](https://www.bleepingcomputer.com/news/security/lego-bricklink-bugs-let-hackers-hijack-accounts-breach-servers/){:target="_blank" rel="noopener"}

> Security analysts have discovered two API security vulnerabilities in BrickLink.com, LEGO Group's official second-hand and vintage marketplace for LEGO bricks. [...]
