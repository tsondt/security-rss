Title: The Week in Ransomware - December 9th 2022 - Wide Impact
Date: 2022-12-09T19:02:48-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-09-the-week-in-ransomware-december-9th-2022-wide-impact

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-9th-2022-wide-impact/){:target="_blank" rel="noopener"}

> This week has been filled with research reports and news of significant attacks having a wide impact on many organizations. [...]
