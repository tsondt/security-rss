Title: Tor Browser 12.0 brings Apple Silicon support, Android enhancements
Date: 2022-12-08T13:03:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-08-tor-browser-120-brings-apple-silicon-support-android-enhancements

[Source](https://www.bleepingcomputer.com/news/security/tor-browser-120-brings-apple-silicon-support-android-enhancements/){:target="_blank" rel="noopener"}

> The Tor Project team has announced the release of Tor Browser 12.0, a major version release introducing support for Apple Silicon chips and several enhancements for the Android version. [...]
