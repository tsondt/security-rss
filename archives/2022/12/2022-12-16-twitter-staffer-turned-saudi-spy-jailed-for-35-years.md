Title: Twitter staffer turned Saudi spy jailed for 3.5 years
Date: 2022-12-16T19:55:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-16-twitter-staffer-turned-saudi-spy-jailed-for-35-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/16/twitter_saudi_spy_sentenced/){:target="_blank" rel="noopener"}

> Tweeter, tailor, soldier, bye A Twitter employee who spied for the Saudi government and royal family has been sentenced to three and half years behind bars in America.... [...]
