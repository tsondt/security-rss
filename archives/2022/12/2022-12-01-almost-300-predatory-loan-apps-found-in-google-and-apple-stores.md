Title: Almost 300 predatory loan apps found in Google and Apple stores
Date: 2022-12-01T07:30:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-01-almost-300-predatory-loan-apps-found-in-google-and-apple-stores

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/01/apple_google_predatory_loans/){:target="_blank" rel="noopener"}

> Note to self: Lenders don’t need the contact list on your mobile device Almost 300 apps, downloaded by around 15 million users, have been pulled from the Google Play and Apple App stores over claims they promised quick loans at reasonable rates but then used extortion and other predatory schemes against borrowers.... [...]
