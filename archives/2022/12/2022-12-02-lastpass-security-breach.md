Title: LastPass Security Breach
Date: 2022-12-02T12:09:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;breaches;passwords
Slug: 2022-12-02-lastpass-security-breach

[Source](https://www.schneier.com/blog/archives/2022/12/lastpass-security-breach.html){:target="_blank" rel="noopener"}

> The company was hacked, and customer information accessed. No passwords were compromised. [...]
