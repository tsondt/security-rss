Title: 2022 PiTuKri ISAE 3000 Type II attestation report available with 154 services in scope
Date: 2022-12-23T18:42:03+00:00
Author: Daniel Fuertes
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS Compliance;AWS PikTuKri;AWS security;cybersecurity;Finland;Finnish Public Sector;PiTuKri Compliance;Security Blog
Slug: 2022-12-23-2022-pitukri-isae-3000-type-ii-attestation-report-available-with-154-services-in-scope

[Source](https://aws.amazon.com/blogs/security/2022-pitukri-isae-3000-type-ii-attestation-report-available-with-154-services-in-scope/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the second issuance of the Criteria to Assess the Information Security of Cloud Services (PiTuKri) International Standard on Assurance Engagements (ISAE) 3000 Type II attestation report. The scope of the report covers a total of 154 services and 24 global AWS Regions. The Finnish Transport and Communications [...]
