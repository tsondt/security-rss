Title: Two signs in the comms cabinet said 'Do not unplug'. Guess what happened
Date: 2022-12-02T07:00:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-02-two-signs-in-the-comms-cabinet-said-do-not-unplug-guess-what-happened

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/02/on_call/){:target="_blank" rel="noopener"}

> No amount of resilience planning can defeat determined idiots whose devices are low on battery On Call Welcome once more to On-Call, The Register 's weekly reader-contributed column that tells tales of IT pros being asked to fix things that should never have broken.... [...]
