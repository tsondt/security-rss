Title: BTC.com lost $3 million worth of cryptocurrency in cyberattack
Date: 2022-12-27T10:58:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-12-27-btccom-lost-3-million-worth-of-cryptocurrency-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/btccom-lost-3-million-worth-of-cryptocurrency-in-cyberattack/){:target="_blank" rel="noopener"}

> BTC.com, one of the world's largest cryptocurrency mining pools, announced it was the victim of a cyberattack that resulted in the theft of approximately $3 million worth of crypto assets belonging to both customers and the company. [...]
