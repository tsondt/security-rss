Title: Friday Squid Blogging: Squid in Concert
Date: 2022-12-16T22:13:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;music;squid
Slug: 2022-12-16-friday-squid-blogging-squid-in-concert

[Source](https://www.schneier.com/blog/archives/2022/12/friday-squid-blogging-squid-in-concert.html){:target="_blank" rel="noopener"}

> Squid is performing a concert in London in February. If you don’t know what their music is like, try this or this or this. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
