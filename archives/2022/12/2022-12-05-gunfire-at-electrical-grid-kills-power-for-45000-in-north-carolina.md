Title: Gunfire at electrical grid kills power for 45,000 in North Carolina
Date: 2022-12-05T23:30:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-12-05-gunfire-at-electrical-grid-kills-power-for-45000-in-north-carolina

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/05/electrical_grid_carolina/){:target="_blank" rel="noopener"}

> You don't have to be a coder to cut off the juice when blunt tools are around Officials in Moore County, North Carolina, declared a state of emergency on Sunday after gunfire damaged an electrical substation and left 45,000 homes and businesses without power in near freezing temperatures.... [...]
