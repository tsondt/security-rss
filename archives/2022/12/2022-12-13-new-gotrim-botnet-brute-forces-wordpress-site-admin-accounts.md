Title: New GoTrim botnet brute forces WordPress site admin accounts
Date: 2022-12-13T12:27:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-13-new-gotrim-botnet-brute-forces-wordpress-site-admin-accounts

[Source](https://www.bleepingcomputer.com/news/security/new-gotrim-botnet-brute-forces-wordpress-site-admin-accounts/){:target="_blank" rel="noopener"}

> A new Go-based botnet malware named 'GoTrim' is scanning the web for self-hosted WordPress websites and attempting to brute force the administrator's password and take control of the site. [...]
