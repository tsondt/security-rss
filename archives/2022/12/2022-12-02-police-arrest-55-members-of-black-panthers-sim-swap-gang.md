Title: Police arrest 55 members of 'Black Panthers' SIM Swap gang
Date: 2022-12-02T10:29:05-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-12-02-police-arrest-55-members-of-black-panthers-sim-swap-gang

[Source](https://www.bleepingcomputer.com/news/security/police-arrest-55-members-of-black-panthers-sim-swap-gang/){:target="_blank" rel="noopener"}

> The Spanish National Police have arrested 55 members of the 'Black Panthers' cybercrime group, including one of the organization's leaders based in Barcelona. [...]
