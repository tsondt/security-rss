Title: Fraudulent ‘popunder’ Google Ad campaign generated millions of dollars
Date: 2022-12-22T07:30:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-22-fraudulent-popunder-google-ad-campaign-generated-millions-of-dollars

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/22/google_ad_popunder_scam/){:target="_blank" rel="noopener"}

> The scam was lurking behind the content of an adult website Scammers using Google Ads, stolen blog articles, and a "popunder" ad scheme on adult websites pulled in more than $275,000 a month by generating millions of ad impressions every month.... [...]
