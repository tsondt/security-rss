Title: Announcing Google Cloud support for Impact Level 5 (IL5) workloads
Date: 2022-12-06T17:30:00+00:00
Author: Jeanette Manfra
Category: GCP Security
Tags: Public Sector;Identity & Security
Slug: 2022-12-06-announcing-google-cloud-support-for-impact-level-5-il5-workloads

[Source](https://cloud.google.com/blog/products/identity-security/introducing-google-cloud-support-for-impact-level-5-workloads/){:target="_blank" rel="noopener"}

> Google Cloud is committed to serving public sector customers around the world, and has made significant progress in building products and services designed specifically to meet your needs. Today, Google Cloud is proud to announce Department of Defense Impact Level 5 (IL5) provisional authorization (PA) — another important milestone that enables us to support additional workloads for U.S. public sector customers. With nine supported regions and 38 zones, Google Cloud now has the largest cloud service offering available on the market for U.S. public sector customers, enabling federal, state, local and educational entities to take advantage of the security, performance, [...]
