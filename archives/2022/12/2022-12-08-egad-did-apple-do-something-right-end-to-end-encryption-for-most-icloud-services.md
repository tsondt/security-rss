Title: Egad, did Apple do something right? End-to-end encryption for (most) iCloud services
Date: 2022-12-08T01:44:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-12-08-egad-did-apple-do-something-right-end-to-end-encryption-for-most-icloud-services

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/08/apple_encryption_icloud/){:target="_blank" rel="noopener"}

> And remember CSAM scanning plan? Forget that was ever a thing Apple says it will provide end-to-end encryption for most iCloud services, having abandoned its previously announced – and then quietly shelved – plan to check the legality of on-device photos prior to cloud synchronization.... [...]
