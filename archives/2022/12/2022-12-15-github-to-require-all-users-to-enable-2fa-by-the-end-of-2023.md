Title: GitHub to require all users to enable 2FA by the end of 2023
Date: 2022-12-15T15:16:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-15-github-to-require-all-users-to-enable-2fa-by-the-end-of-2023

[Source](https://www.bleepingcomputer.com/news/security/github-to-require-all-users-to-enable-2fa-by-the-end-of-2023/){:target="_blank" rel="noopener"}

> GitHub will require all users who contribute code on the platform to enable two-factor authentication (2FA) as an additional protection measure on their accounts by the end of 2023. [...]
