Title: Thousands of Citrix servers vulnerable to patched critical flaws
Date: 2022-12-28T12:28:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-28-thousands-of-citrix-servers-vulnerable-to-patched-critical-flaws

[Source](https://www.bleepingcomputer.com/news/security/thousands-of-citrix-servers-vulnerable-to-patched-critical-flaws/){:target="_blank" rel="noopener"}

> Thousands of Citrix ADC and Gateway deployments remain vulnerable to two critical-severity security issues that the vendor fixed in recent months. [...]
