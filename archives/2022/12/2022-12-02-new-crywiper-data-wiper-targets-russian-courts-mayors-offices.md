Title: New CryWiper data wiper targets Russian courts, mayor’s offices
Date: 2022-12-02T12:29:48-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-02-new-crywiper-data-wiper-targets-russian-courts-mayors-offices

[Source](https://www.bleepingcomputer.com/news/security/new-crywiper-data-wiper-targets-russian-courts-mayor-s-offices/){:target="_blank" rel="noopener"}

> A previously undocumented data wiper named CryWiper is masquerading as ransomware, extorting victims to pay for a decrypter, but in reality, it just destroys data beyond recovery. [...]
