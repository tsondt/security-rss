Title: Amazon ECR Public Gallery flaw could have wiped or poisoned any image
Date: 2022-12-13T09:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-13-amazon-ecr-public-gallery-flaw-could-have-wiped-or-poisoned-any-image

[Source](https://www.bleepingcomputer.com/news/security/amazon-ecr-public-gallery-flaw-could-have-wiped-or-poisoned-any-image/){:target="_blank" rel="noopener"}

> A severe security flaw in the Amazon ECR (Elastic Container Registry) Public Gallery could have allowed attackers to delete any container image or inject malicious code into the images of other AWS accounts. [...]
