Title: Back to work, Linux admins: You may have a CVSS 10 kernel bug to address
Date: 2022-12-24T10:00:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-12-24-back-to-work-linux-admins-you-may-have-a-cvss-10-kernel-bug-to-address

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/24/back_to_work_linux_admins/){:target="_blank" rel="noopener"}

> Also, script kiddies are coming for your gift cards, and Meta's Cambridge Analytica pathetic payout In brief Merry Christmas, Linux systems administrators: here's a kernel vulnerability with a CVSS score of 10 potentially in your SMB server. It can be exploited to achieve unauthenticated user remote code execution.... [...]
