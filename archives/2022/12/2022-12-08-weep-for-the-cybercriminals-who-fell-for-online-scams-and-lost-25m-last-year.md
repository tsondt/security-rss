Title: Weep for the cybercriminals who fell for online scams and lost $2.5m last year
Date: 2022-12-08T09:59:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-08-weep-for-the-cybercriminals-who-fell-for-online-scams-and-lost-25m-last-year

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/08/scammers_scam_cybercriminals_sophos/){:target="_blank" rel="noopener"}

> I'm the smartest guy in the room, I'm sure the message from IRS refunds is legit Scammers have scammed their fellow cybercriminals out of more than $2.5 million on three dark web forums alone over the last 12 months, according to Sophos researchers.... [...]
