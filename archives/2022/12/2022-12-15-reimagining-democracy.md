Title: Reimagining Democracy
Date: 2022-12-15T02:30:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;conferences;infrastructure
Slug: 2022-12-15-reimagining-democracy

[Source](https://www.schneier.com/blog/archives/2022/12/reimagining-democracy.html){:target="_blank" rel="noopener"}

> Last week, I hosted a two-day workshop on reimagining democracy. The idea was to bring together people from a variety of disciplines who are all thinking about different aspects of democracy, less from a “what we need to do today” perspective and more from a blue-sky future perspective. My remit to the participants was this: The idea is to start from scratch, to pretend we’re forming a new country and don’t have any precedent to deal with. And that we don’t have any unique interests to perturb our thinking. The modern representative democracy was the best form of government mid-eighteenth [...]
