Title: Kremlin-backed hackers targeted a “large” petroleum refinery in a NATO nation
Date: 2022-12-20T22:03:02+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Uncategorized;Energy;hacking;kremlin;petrolium;russia
Slug: 2022-12-20-kremlin-backed-hackers-targeted-a-large-petroleum-refinery-in-a-nato-nation

[Source](https://arstechnica.com/?p=1906124){:target="_blank" rel="noopener"}

> Enlarge / Fawley Oil Refinery on a bright day. (credit: Getty Images) One of the Kremlin’s most active hacking groups targeting Ukraine recently tried to hack a large petroleum refining company located in a NATO country. The attack is a sign that the group is expanding its intelligence gathering as Russia’s invasion of its neighboring country continues. The attempted hacking occurred on August 30 and was unsuccessful, researchers with Palo Alto Networks’ Unit 42 said on Tuesday. The hacking group—tracked under various names including Trident Ursa, Gamaredon, UAC-0010, Primitive Bear, and Shuckworm—has been attributed by Ukraine’s Security Service to Russia’s [...]
