Title: Phishing attack uses Facebook posts to evade email security
Date: 2022-12-15T12:38:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-15-phishing-attack-uses-facebook-posts-to-evade-email-security

[Source](https://www.bleepingcomputer.com/news/security/phishing-attack-uses-facebook-posts-to-evade-email-security/){:target="_blank" rel="noopener"}

> A new phishing campaign uses Facebook posts as part of its attack chain to trick users into giving away their account credentials and personally identifiable information (PII). [...]
