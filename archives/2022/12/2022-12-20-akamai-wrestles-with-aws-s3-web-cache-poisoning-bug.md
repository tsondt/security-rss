Title: Akamai wrestles with AWS S3 web cache poisoning bug
Date: 2022-12-20T11:43:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-20-akamai-wrestles-with-aws-s3-web-cache-poisoning-bug

[Source](https://portswigger.net/daily-swig/akamai-wrestles-with-aws-s3-web-cache-poisoning-bug){:target="_blank" rel="noopener"}

> Definitive solution is ‘non-trivial’ since behavior arises from customers processing non-RFC compliant requests [...]
