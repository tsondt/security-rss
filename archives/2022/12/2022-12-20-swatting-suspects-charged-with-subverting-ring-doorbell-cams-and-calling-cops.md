Title: Swatting suspects charged with subverting Ring doorbell cams and calling cops
Date: 2022-12-20T22:30:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-20-swatting-suspects-charged-with-subverting-ring-doorbell-cams-and-calling-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/20/ring_swatting_suspects_charged/){:target="_blank" rel="noopener"}

> Heavily armed US police turning up on the doorstep is no laughing matter Two men have been charged with an alleged week-long US swatting spree in which they used stolen Yahoo email credentials to break into Ring door cameras, livestream the events on social media, and then taunt responding police officers.... [...]
