Title: Kali Linux 2022.4 adds 6 new tools, Azure images, and desktop updates
Date: 2022-12-06T16:43:08-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-06-kali-linux-20224-adds-6-new-tools-azure-images-and-desktop-updates

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20224-adds-6-new-tools-azure-images-and-desktop-updates/){:target="_blank" rel="noopener"}

> Offensive Security has released ​Kali Linux 2022.4, the fourth and final version of 2022, with new Azure and QEMU images, six new tools, and improved desktop experiences. [...]
