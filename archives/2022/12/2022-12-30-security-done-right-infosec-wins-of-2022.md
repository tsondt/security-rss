Title: Security done right – infosec wins of 2022
Date: 2022-12-30T11:24:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-30-security-done-right-infosec-wins-of-2022

[Source](https://portswigger.net/daily-swig/security-done-right-infosec-wins-of-2022){:target="_blank" rel="noopener"}

> The toasts, triumphs, and biggest security wins of the year [...]
