Title: Hyundai app bugs allowed hackers to remotely unlock, start cars
Date: 2022-12-01T08:01:27-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-12-01-hyundai-app-bugs-allowed-hackers-to-remotely-unlock-start-cars

[Source](https://www.bleepingcomputer.com/news/security/hyundai-app-bugs-allowed-hackers-to-remotely-unlock-start-cars/){:target="_blank" rel="noopener"}

> Vulnerabilities in mobile apps exposed Hyundai and Genesis car models after 2012 to remote attacks that allowed unlocking and even starting the vehicles. [...]
