Title: EU takes another step towards US data-sharing agreement
Date: 2022-12-14T15:54:59+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-12-14-eu-takes-another-step-towards-us-data-sharing-agreement

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/14/eu_us_data_sharing_agreement/){:target="_blank" rel="noopener"}

> Campaigners say it's unlikely to pass a test in the courts, though The EU has issued a draft decision agreeing that measures taken by the United States ensure sufficient protection for personal data to be transferred from the region to US companies.... [...]
