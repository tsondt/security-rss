Title: How to use Amazon Macie to preview sensitive data in S3 buckets
Date: 2022-12-01T16:14:24+00:00
Author: Koulick Ghosh
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Amazon Athena;Amazon Macie;AWS Glue;AWS Security Hub;Data security investigations;Security Blog;Sensitive Data Discovery
Slug: 2022-12-01-how-to-use-amazon-macie-to-preview-sensitive-data-in-s3-buckets

[Source](https://aws.amazon.com/blogs/security/how-to-use-amazon-macie-to-preview-sensitive-data-in-s3-buckets/){:target="_blank" rel="noopener"}

> Security teams use Amazon Macie to discover and protect sensitive data, such as names, payment card data, and AWS credentials, in Amazon Simple Storage Service (Amazon S3). When Macie discovers sensitive data, these teams will want to see examples of the actual sensitive data found. Reviewing a sampling of the discovered data helps them quickly [...]
