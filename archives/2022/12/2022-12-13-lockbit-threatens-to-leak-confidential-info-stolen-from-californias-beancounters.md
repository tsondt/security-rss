Title: LockBit threatens to leak confidential info stolen from California's beancounters
Date: 2022-12-13T23:30:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-13-lockbit-threatens-to-leak-confidential-info-stolen-from-californias-beancounters

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/13/california_finance_department_lockbit/){:target="_blank" rel="noopener"}

> Databases, details of 'sexual proceedings in court' and more apparently pilfered from finance IT LockBit claims it was behind a cyber-attack on the California Department of Finance, bragging it stole data during the intrusion.... [...]
