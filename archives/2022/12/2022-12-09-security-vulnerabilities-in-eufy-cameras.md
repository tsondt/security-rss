Title: Security Vulnerabilities in Eufy Cameras
Date: 2022-12-09T12:11:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cameras;lies;privacy;surveillance;vulnerabilities
Slug: 2022-12-09-security-vulnerabilities-in-eufy-cameras

[Source](https://www.schneier.com/blog/archives/2022/12/security-vulnerabilities-in-eufy-cameras.html){:target="_blank" rel="noopener"}

> Eufy cameras claim to be local only, but upload data to the cloud. The company is basically lying to reporters, despite being shown evidence to the contrary. The company’s behavior is so egregious that ReviewGeek is no longer recommending them. This will be interesting to watch. If Eufy can ignore security researchers and the press without there being any repercussions in the market, others will follow suit. And we will lose public shaming as an incentive to improve security. Update : After further testing, we’re not seeing the VLC streams begin based solely on the camera detecting motion. We’re not [...]
