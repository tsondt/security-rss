Title: Boss installed software from behind the Iron Curtain, techies ended up Putin things back together
Date: 2022-12-09T07:27:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-09-boss-installed-software-from-behind-the-iron-curtain-techies-ended-up-putin-things-back-together

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/09/on_call/){:target="_blank" rel="noopener"}

> Comrade offered 'monitoring' tool to keep an eye on the workers On Call Welcome once again, comrades, to On-Call, The Register 's celebration of the tech proletariat's struggles with oppression by bourgeois bosses – and the eventual triumph of the workers!... [...]
