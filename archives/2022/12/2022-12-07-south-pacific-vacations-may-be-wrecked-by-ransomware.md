Title: South Pacific vacations may be wrecked by ransomware
Date: 2022-12-07T02:58:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-07-south-pacific-vacations-may-be-wrecked-by-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/07/nz_vanuatu_cyberattacks/){:target="_blank" rel="noopener"}

> New Zealand government reels, Vanuatu’s spent weeks entirely offline New Zealand's Privacy Commission has signalled it may open an investigation into local managed services provider Mercury IT, which serves many government agencies and businesses and has been hit by ransomware.... [...]
