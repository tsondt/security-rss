Title: Why diversity is a cybersecurity imperative for GCAT
Date: 2022-12-07T19:30:00+00:00
Author: M.K. Palmore
Category: GCP Security
Tags: Identity & Security
Slug: 2022-12-07-why-diversity-is-a-cybersecurity-imperative-for-gcat

[Source](https://cloud.google.com/blog/products/identity-security/why-diversity-is-a-cybersecurity-imperative-for-gcat/){:target="_blank" rel="noopener"}

> Our quarterly Security Talks series brings together experts from Google Cloud security teams and the industry to share information on our latest security products, innovations and best practices. Below is the keynote address for our Security Talks on Dec. 7, 2022, presented by MK Palmore, director, Office of the CISO at Google Cloud. Diverse threats call for diverse teams, which is why one of the goals of the Google Cybersecurity Action Team (GCAT) is to foster a more diverse, equitable and inclusive cybersecurity workforce. GCAT debuted in October 2021 as an effort to bring world class security, compliance, privacy, and [...]
