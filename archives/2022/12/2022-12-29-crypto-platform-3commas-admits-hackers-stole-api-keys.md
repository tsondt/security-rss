Title: Crypto platform 3Commas admits hackers stole API keys
Date: 2022-12-29T10:03:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-12-29-crypto-platform-3commas-admits-hackers-stole-api-keys

[Source](https://www.bleepingcomputer.com/news/security/crypto-platform-3commas-admits-hackers-stole-api-keys/){:target="_blank" rel="noopener"}

> An anonymous Twitter user published yesterday a set of 10,000 API keys allegedly obtained from the 3Commas cryptocurrency trading platform. [...]
