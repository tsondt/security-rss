Title: Want to detect Cobalt Strike on the network? Look to process memory
Date: 2022-12-06T15:30:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-06-want-to-detect-cobalt-strike-on-the-network-look-to-process-memory

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/06/cobalt_strike_memory_unit_42/){:target="_blank" rel="noopener"}

> Security analysts have tools to spot hard-to-find threat, Unit 42 says Enterprise security pros can detect malware samples in environments that incorporate the highly evasive Cobalt Strike attack code by analyzing artifacts in process memory, according to researchers with Palo Alto Networks' Unit 42 threat intelligence unit.... [...]
