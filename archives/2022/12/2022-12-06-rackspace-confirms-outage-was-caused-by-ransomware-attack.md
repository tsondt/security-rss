Title: Rackspace confirms outage was caused by ransomware attack
Date: 2022-12-06T10:31:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-06-rackspace-confirms-outage-was-caused-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/rackspace-confirms-outage-was-caused-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Texas-based cloud computing provider Rackspace has confirmed today that a ransomware attack is behind an ongoing Hosted Exchange outage described as an "isolated disruption." [...]
