Title: It’s time to fill those cloud security gaps
Date: 2022-12-22T12:58:05+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2022-12-22-its-time-to-fill-those-cloud-security-gaps

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/22/its_time_to_fill_those/){:target="_blank" rel="noopener"}

> Here’s how Wiz can help Sponsored Feature When software vulnerabilities and zero days moved up the enterprise worry list 15 years ago, nobody imagined the world would one day end up with a threat as perplexing as Log4Shell – a vulnerability in the Apache Log4j open source logging framework that's used in software on all major operating systems spanning everything from cloud services to PC games.... [...]
