Title: Domain aging gang CashRewindo picks vintage sites to push malvertising
Date: 2022-12-02T10:59:52+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-02-domain-aging-gang-cashrewindo-picks-vintage-sites-to-push-malvertising

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/02/cashrewindo_scam_domain_aging/){:target="_blank" rel="noopener"}

> Like fine wine, the longer it sits, the better it is A sophisticated and very patient threat group behind a global malvertising scheme is using so-called aged domains to skirt past cybersecurity tools and catch victims in investment scams.... [...]
