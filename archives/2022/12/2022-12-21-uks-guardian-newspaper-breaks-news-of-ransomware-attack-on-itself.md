Title: UK's Guardian newspaper breaks news of ransomware attack on itself
Date: 2022-12-21T15:40:06+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-12-21-uks-guardian-newspaper-breaks-news-of-ransomware-attack-on-itself

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/21/the_guardian_hit_by_ransomware/){:target="_blank" rel="noopener"}

> Reporters work from home as publication promises Thursday's print edition will hit newstands on time UK broadsheet media outlet The Guardian has become the victim of a ransomware attack which seems to have taken out a large chunk of office-based systems.... [...]
