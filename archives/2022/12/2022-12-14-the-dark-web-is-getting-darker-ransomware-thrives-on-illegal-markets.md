Title: The Dark Web is Getting Darker - Ransomware Thrives on Illegal Markets
Date: 2022-12-14T10:33:06-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2022-12-14-the-dark-web-is-getting-darker-ransomware-thrives-on-illegal-markets

[Source](https://www.bleepingcomputer.com/news/security/the-dark-web-is-getting-darker-ransomware-thrives-on-illegal-markets/){:target="_blank" rel="noopener"}

> The dark web is getting darker as cybercrime gangs increasingly shop their malware, phishing, and ransomware tools on illegal cybercrime markets. [...]
