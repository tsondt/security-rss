Title: Suspects arrested for hacking US networks to steal employee data
Date: 2022-12-06T14:18:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-12-06-suspects-arrested-for-hacking-us-networks-to-steal-employee-data

[Source](https://www.bleepingcomputer.com/news/security/suspects-arrested-for-hacking-us-networks-to-steal-employee-data/){:target="_blank" rel="noopener"}

> Four men suspected of hacking into US networks to steal employee data for identity theft and the filing of fraudulent US tax returns have been arrested in London, UK, and Malmo, Sweden, at the request of the U.S. law enforcement authorities. [...]
