Title: Microsoft warns of Russian cyberattacks throughout the winter
Date: 2022-12-05T19:44:47-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-12-05-microsoft-warns-of-russian-cyberattacks-throughout-the-winter

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-russian-cyberattacks-throughout-the-winter/){:target="_blank" rel="noopener"}

> Microsoft has warned of Russian-sponsored cyberattacks continuing to target Ukrainian infrastructure and NATO allies in Europe throughout the winter. [...]
