Title: UK arrests five for selling 'dodgy' point of sale software
Date: 2022-12-12T02:58:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-12-uk-arrests-five-for-selling-dodgy-point-of-sale-software

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/12/j5_electronic_sales_suppression_software_probe/){:target="_blank" rel="noopener"}

> Turns a $100 bottle of wine into a $4 soft drink to avoid tax, earning probe by major governments Tax authorities from Australia, Canada, France, the UK and the USA have conducted a joint probe into "electronic sales suppression software" – applications that falsify point of sale data to help merchants avoid paying tax on their true revenue.... [...]
