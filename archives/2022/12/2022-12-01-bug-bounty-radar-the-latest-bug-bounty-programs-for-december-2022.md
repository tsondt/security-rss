Title: Bug Bounty Radar // The latest bug bounty programs for December 2022
Date: 2022-12-01T12:39:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-01-bug-bounty-radar-the-latest-bug-bounty-programs-for-december-2022

[Source](https://portswigger.net/daily-swig/bug-bounty-radar-the-latest-bug-bounty-programs-for-december-2022){:target="_blank" rel="noopener"}

> New web targets for the discerning hacker [...]
