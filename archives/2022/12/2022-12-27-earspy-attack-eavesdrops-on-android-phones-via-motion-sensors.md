Title: EarSpy attack eavesdrops on Android phones via motion sensors
Date: 2022-12-27T08:39:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-12-27-earspy-attack-eavesdrops-on-android-phones-via-motion-sensors

[Source](https://www.bleepingcomputer.com/news/security/earspy-attack-eavesdrops-on-android-phones-via-motion-sensors/){:target="_blank" rel="noopener"}

> A team of researchers has developed an eavesdropping attack for Android devices that can, to various degrees, recognize the caller's gender and identity, and even discern private speech. [...]
