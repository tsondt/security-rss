Title: Eufy publicly acknowledges some parts of its “No clouds” controversy
Date: 2022-12-21T17:12:11+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Tech;anker;cloud storage;eufy;security camera
Slug: 2022-12-21-eufy-publicly-acknowledges-some-parts-of-its-no-clouds-controversy

[Source](https://arstechnica.com/?p=1906209){:target="_blank" rel="noopener"}

> Enlarge / Eufy's security arm has publicly addressed some of the most important claims about the company's local-focused systems, but those who bought into the "no clouds" claims may not be fully assured. (credit: Eufy) Eufy, the Anker brand that positioned its security cameras as prioritizing "local storage" and "No clouds," has issued a statement in response to recent findings by security researchers and tech news sites. Eufy admits it could do better but also leaves some issues unaddressed. In a thread titled "Re: Recent security claims against eufy Security," "eufy_official" writes to its "Security Cutomers and Partners." Eufy is [...]
