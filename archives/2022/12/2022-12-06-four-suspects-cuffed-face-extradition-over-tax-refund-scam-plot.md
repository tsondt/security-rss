Title: Four suspects cuffed, face extradition over tax refund scam plot
Date: 2022-12-06T01:30:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-06-four-suspects-cuffed-face-extradition-over-tax-refund-scam-plot

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/06/identity_thieves_accused_irs/){:target="_blank" rel="noopener"}

> RDP servers allegedly raided in hunt for personal info to exploit Four men suspected of plotting to commit wire fraud and identity theft have been arrested and now face extradition to America.... [...]
