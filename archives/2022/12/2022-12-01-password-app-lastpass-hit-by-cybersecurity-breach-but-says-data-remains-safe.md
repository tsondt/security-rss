Title: Password app LastPass hit by cybersecurity breach but says data remains safe
Date: 2022-12-01T07:14:54+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Cybercrime;Data and computer security;Australia news;Technology
Slug: 2022-12-01-password-app-lastpass-hit-by-cybersecurity-breach-but-says-data-remains-safe

[Source](https://www.theguardian.com/technology/2022/dec/01/password-app-lastpass-hit-by-cybersecurity-breach-but-says-data-remains-safe){:target="_blank" rel="noopener"}

> Company says its security system prevented the hacker accessing customer data or encrypted passwords Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Password manager LastPass has told customers that some of their information has been accessed in a cybersecurity breach, but says passwords remain safe. LastPass is one of several password managers in the market that aims to reduce the reuse of passwords online, by storing themin a single app. It also makes it easier for users to generate strong passwords as required. Sign up [...]
