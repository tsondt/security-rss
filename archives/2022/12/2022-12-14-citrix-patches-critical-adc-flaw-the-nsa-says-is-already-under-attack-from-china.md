Title: Citrix patches critical ADC flaw the NSA says is already under attack from China
Date: 2022-12-14T06:57:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-14-citrix-patches-critical-adc-flaw-the-nsa-says-is-already-under-attack-from-china

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/14/chinas_apt5_attacks_citrix_adc_flaw/){:target="_blank" rel="noopener"}

> Yet more pain for the software formerly known as NetScaler The China-linked crime gang APT5 is already attacking a flaw in Citrix's Application Delivery Controller (ADC) and Gateway products that the vendor patched today.... [...]
