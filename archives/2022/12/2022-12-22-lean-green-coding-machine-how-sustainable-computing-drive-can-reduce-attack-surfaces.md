Title: Lean, green coding machine: How sustainable computing drive can reduce attack surfaces
Date: 2022-12-22T15:35:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-22-lean-green-coding-machine-how-sustainable-computing-drive-can-reduce-attack-surfaces

[Source](https://portswigger.net/daily-swig/lean-green-coding-machine-how-sustainable-computing-drive-can-reduce-attack-surfaces){:target="_blank" rel="noopener"}

> Less is often more when it comes to both infosec and eco-friendly computing practices [...]
