Title: Cloud flaws brought to the fore as bug bounty vulnerabilities hit 65k in 2022 – HackerOne
Date: 2022-12-13T16:15:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-13-cloud-flaws-brought-to-the-fore-as-bug-bounty-vulnerabilities-hit-65k-in-2022-hackerone

[Source](https://portswigger.net/daily-swig/cloud-flaws-brought-to-the-fore-as-bug-bounty-vulnerabilities-hit-65k-in-2022-hackerone){:target="_blank" rel="noopener"}

> Impact of cloud migration and shift to remote work evident in new report [...]
