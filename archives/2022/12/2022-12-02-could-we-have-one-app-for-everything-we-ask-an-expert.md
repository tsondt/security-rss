Title: Could we have one app for everything? We ask an expert
Date: 2022-12-02T15:00:19+00:00
Author: Coco Khan
Category: The Guardian
Tags: Social trends;Society;Social media;Data and computer security;Technology
Slug: 2022-12-02-could-we-have-one-app-for-everything-we-ask-an-expert

[Source](https://www.theguardian.com/lifeandstyle/2022/dec/02/could-we-have-one-app-for-everything-we-ask-an-expert){:target="_blank" rel="noopener"}

> Super apps can revolutionise your life – but do you want to pay the price, wonders AI and innovation professor David Shrier Across Asia, the trend for a single app that does everything – from deliveries to bookings to chatting – is spreading. Known as super apps, they are rumoured to be the inspiration for Elon Musk’s plan for Twitter. Could they take off here – and should they? I asked David Shrier, professor of practice, AI and innovation at Imperial College Business School in London. Have you tried a super app ? Well, what do you mean by “super [...]
