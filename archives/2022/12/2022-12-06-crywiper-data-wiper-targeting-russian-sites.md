Title: CryWiper Data Wiper Targeting Russian Sites
Date: 2022-12-06T12:04:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data destruction;malware;ransomware;Russia
Slug: 2022-12-06-crywiper-data-wiper-targeting-russian-sites

[Source](https://www.schneier.com/blog/archives/2022/12/crywiper-data-wiper-targeting-russian-sites.html){:target="_blank" rel="noopener"}

> Kaspersky is reporting on a data wiper masquerading as ransomware that is targeting local Russian government networks. The Trojan corrupts any data that’s not vital for the functioning of the operating system. It doesn’t affect files with extensions.exe,.dll,.lnk,.sys or.msi, and ignores several system folders in the C:\Windows directory. The malware focuses on databases, archives, and user documents. So far, our experts have seen only pinpoint attacks on targets in the Russian Federation. However, as usual, no one can guarantee that the same code won’t be used against other targets. Nothing leading to an attribution. News article. Slashdot thread. [...]
