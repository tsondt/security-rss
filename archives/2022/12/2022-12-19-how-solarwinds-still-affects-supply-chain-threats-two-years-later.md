Title: How SolarWinds still affects supply chain threats, two years later
Date: 2022-12-19T17:00:00+00:00
Author: Stephen Eckels
Category: GCP Security
Tags: Identity & Security
Slug: 2022-12-19-how-solarwinds-still-affects-supply-chain-threats-two-years-later

[Source](https://cloud.google.com/blog/products/identity-security/how-solarwinds-still-affects-supply-chain-threats-two-years-later/){:target="_blank" rel="noopener"}

> Our quarterly Security Talks series brings together experts from Google Cloud Security teams and the industry to share information on our latest security products, innovations and best practices. Below is an introduction to Mandiant’s Security Talks presentation on Dec. 7, Mandiant Tales from the Front Lines: Activate Cyber Defenses Against Supply Chain Compromise. The discovery in December 2020 of the SolarWinds supply chain compromise, a global computer intrusion campaign, caused organizations and security leaders to completely reassess their Cyber Threat Profiles, risk assessments, and defensive postures. It was a rare, watershed event that savvy security leaders and IT decision makers [...]
