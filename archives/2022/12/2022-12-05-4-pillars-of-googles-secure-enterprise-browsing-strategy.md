Title: 4 pillars of Google’s secure enterprise browsing strategy
Date: 2022-12-05T20:30:00+00:00
Author: Noriko Bouffard
Category: GCP Security
Tags: Identity & Security
Slug: 2022-12-05-4-pillars-of-googles-secure-enterprise-browsing-strategy

[Source](https://cloud.google.com/blog/products/identity-security/4-pillars-of-googles-secure-enterprise-browsing-strategy/){:target="_blank" rel="noopener"}

> Even before the recent mass shift to remote and hybrid work models, the web browser had begun to evolve from the primary web access point to a crucial security layer. Securing enterprise web browsing is vital to the security posture and requirements of many organizations. Google Chrome, which is used by billions of people, is at the forefront of that evolution. What is secure enterprise browsing? A secure enterprise browsing solution should start with a browser with built-in security, extension security, adaptive management, malware and phishing protection, and security reporting. Organizations that want to take a more active approach may [...]
