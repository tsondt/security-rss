Title: Password theft bug chain patched in Passwordstate credential manager
Date: 2022-12-21T16:16:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-21-password-theft-bug-chain-patched-in-passwordstate-credential-manager

[Source](https://portswigger.net/daily-swig/password-theft-bug-chain-patched-in-passwordstate-credential-manager){:target="_blank" rel="noopener"}

> Flaws could be combined to grab passwords in cleartext [...]
