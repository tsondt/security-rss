Title: LockBit ransomware claims attack on Port of Lisbon in Portugal
Date: 2022-12-30T11:44:55-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-30-lockbit-ransomware-claims-attack-on-port-of-lisbon-in-portugal

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-claims-attack-on-port-of-lisbon-in-portugal/){:target="_blank" rel="noopener"}

> A cyberattack hitting the Port of Lisbon Administration (APL), the third-largest port in Portugal, on Christmas day has been claimed by the LockBit ransomware gang. [...]
