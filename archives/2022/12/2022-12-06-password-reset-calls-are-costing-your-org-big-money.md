Title: Password Reset Calls Are Costing Your Org Big Money
Date: 2022-12-06T10:07:14-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2022-12-06-password-reset-calls-are-costing-your-org-big-money

[Source](https://www.bleepingcomputer.com/news/security/password-reset-calls-are-costing-your-org-big-money/){:target="_blank" rel="noopener"}

> Research states that the average help desk labor cost for a single password reset is about $70. With this cost, what can an organization do to lessen the impact of password resets? [...]
