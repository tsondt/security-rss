Title: Uber suffers new data breach after attack on vendor, info leaked online
Date: 2022-12-12T13:30:18-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-12-uber-suffers-new-data-breach-after-attack-on-vendor-info-leaked-online

[Source](https://www.bleepingcomputer.com/news/security/uber-suffers-new-data-breach-after-attack-on-vendor-info-leaked-online/){:target="_blank" rel="noopener"}

> Uber has suffered a new data breach after a threat actor leaked employee email addresses, corporate reports, and IT asset information stolen from a third-party vendor in a cybersecurity incident. [...]
