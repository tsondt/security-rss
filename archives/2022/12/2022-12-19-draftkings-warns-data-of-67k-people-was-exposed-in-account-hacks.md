Title: DraftKings warns data of 67K people was exposed in account hacks
Date: 2022-12-19T12:57:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-19-draftkings-warns-data-of-67k-people-was-exposed-in-account-hacks

[Source](https://www.bleepingcomputer.com/news/security/draftkings-warns-data-of-67k-people-was-exposed-in-account-hacks/){:target="_blank" rel="noopener"}

> Sports betting company DraftKings revealed last week that more than 67,000 customers had their personal information exposed following a credential attack in November. [...]
