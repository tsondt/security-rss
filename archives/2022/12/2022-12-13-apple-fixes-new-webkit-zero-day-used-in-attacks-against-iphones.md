Title: Apple fixes new Webkit zero-day used in attacks against iPhones
Date: 2022-12-13T15:48:43-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Apple;Security
Slug: 2022-12-13-apple-fixes-new-webkit-zero-day-used-in-attacks-against-iphones

[Source](https://www.bleepingcomputer.com/news/apple/apple-fixes-new-webkit-zero-day-used-in-attacks-against-iphones/){:target="_blank" rel="noopener"}

> In security updates released today, Apple has fixed the tenth zero-day vulnerability since the start of the year, with this latest one actively used in attacks against iPhones. [...]
