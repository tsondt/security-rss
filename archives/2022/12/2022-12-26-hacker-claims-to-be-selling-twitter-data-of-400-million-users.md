Title: Hacker claims to be selling Twitter data of 400 million users
Date: 2022-12-26T15:44:03-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-26-hacker-claims-to-be-selling-twitter-data-of-400-million-users

[Source](https://www.bleepingcomputer.com/news/security/hacker-claims-to-be-selling-twitter-data-of-400-million-users/){:target="_blank" rel="noopener"}

> A threat actor claims to be selling public and private data of 400 million Twitter users scraped in 2021 using a now-fixed API vulnerability. They're asking $200,000 for an exclusive sale. [...]
