Title: Okta's source code stolen after GitHub repositories hacked
Date: 2022-12-21T01:15:38-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-12-21-oktas-source-code-stolen-after-github-repositories-hacked

[Source](https://www.bleepingcomputer.com/news/security/oktas-source-code-stolen-after-github-repositories-hacked/){:target="_blank" rel="noopener"}

> In a 'confidential' email notification sent by Okta and seen by BleepingComputer, the company states that attackers gained access to its GitHub repositories this month and stole the company's source code. [...]
