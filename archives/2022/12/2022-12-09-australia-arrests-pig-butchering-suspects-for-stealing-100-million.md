Title: Australia arrests 'Pig Butchering' suspects for stealing $100 million
Date: 2022-12-09T13:17:57-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-09-australia-arrests-pig-butchering-suspects-for-stealing-100-million

[Source](https://www.bleepingcomputer.com/news/security/australia-arrests-pig-butchering-suspects-for-stealing-100-million/){:target="_blank" rel="noopener"}

> The Australian Federal Police (AFP) have arrested four suspected members of a financial investment scam syndicate estimated to have stolen $100 million from victims worldwide. [...]
