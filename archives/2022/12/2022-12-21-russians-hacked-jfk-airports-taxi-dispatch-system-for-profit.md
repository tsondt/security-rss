Title: Russians hacked JFK airport’s taxi dispatch system for profit
Date: 2022-12-21T13:29:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-12-21-russians-hacked-jfk-airports-taxi-dispatch-system-for-profit

[Source](https://www.bleepingcomputer.com/news/security/russians-hacked-jfk-airport-s-taxi-dispatch-system-for-profit/){:target="_blank" rel="noopener"}

> Two U.S. citizens were arrested for allegedly conspiring with Russian hackers to hack the John F. Kennedy International Airport (JFK) taxi dispatch system to move specific taxis to the front of the queue in exchange for a $10 fee. [...]
