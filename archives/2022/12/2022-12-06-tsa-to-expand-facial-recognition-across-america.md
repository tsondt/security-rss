Title: TSA to expand facial recognition across America
Date: 2022-12-06T02:30:07+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-12-06-tsa-to-expand-facial-recognition-across-america

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/06/us_transportation_security_agency_facial/){:target="_blank" rel="noopener"}

> System is optional, for the moment America's Transport Security Administration, better known as the TSA, has been testing facial recognition software to automatically screen passengers flying across the country in 16 airports. And now it's looking into rolling it out nationwide next year.... [...]
