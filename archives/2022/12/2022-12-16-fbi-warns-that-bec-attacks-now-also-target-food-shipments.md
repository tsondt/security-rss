Title: FBI warns that BEC attacks now also target food shipments
Date: 2022-12-16T13:31:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-16-fbi-warns-that-bec-attacks-now-also-target-food-shipments

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-that-bec-attacks-now-also-target-food-shipments/){:target="_blank" rel="noopener"}

> Organizations in the food sector are now also targeted in business email compromise (BEC) attacks, according to a joint advisory issued by the FBI, the Food and Drug Administration Office of Criminal Investigations (FDA OCI), and the U.S. Department of Agriculture (USDA). [...]
