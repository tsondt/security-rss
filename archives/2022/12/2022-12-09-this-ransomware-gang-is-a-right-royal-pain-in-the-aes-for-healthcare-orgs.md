Title: This ransomware gang is a right Royal pain in the AES for healthcare orgs
Date: 2022-12-09T22:57:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-09-this-ransomware-gang-is-a-right-royal-pain-in-the-aes-for-healthcare-orgs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/09/royal_ransomware_hhs_warning/){:target="_blank" rel="noopener"}

> Nothing like your medical files being taken hostage for millions of dollars Newish ransomware gang Royal has been spotted targeting the healthcare sector, the US Department of Health and Human Services (HHS) has said.... [...]
