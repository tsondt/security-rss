Title: How we validated the security controls of our new Confidential Space
Date: 2022-12-13T17:00:00+00:00
Author: Cfir Cohen
Category: GCP Security
Tags: Identity & Security
Slug: 2022-12-13-how-we-validated-the-security-controls-of-our-new-confidential-space

[Source](https://cloud.google.com/blog/products/identity-security/how-to-build-a-secure-confidential-space/){:target="_blank" rel="noopener"}

> We’re pleased to announce that Confidential Space, our new solution that allows you to control access to your sensitive data and securely collaborate in ways not previously possible, is now available in public Preview. First announced at Google Cloud Next, Confidential Space can offer many benefits to securely manage data from financial institutions, healthcare and pharmaceutical companies, and Web3 assets. Today, we will explore some security properties of the Confidential Space system that makes these solutions possible. Confidential Space uses a trusted execution environment (TEE), which allows data contributors to have control over how their data is used and which [...]
