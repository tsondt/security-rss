Title: Black Hat Europe 2022: A defendable internet is possible, but only with industry makeover
Date: 2022-12-07T15:19:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-07-black-hat-europe-2022-a-defendable-internet-is-possible-but-only-with-industry-makeover

[Source](https://portswigger.net/daily-swig/black-hat-europe-2022-a-defendable-internet-is-possible-but-only-with-industry-makeover){:target="_blank" rel="noopener"}

> Empower buyers and stop fixating about zero-days, conference attendees told [...]
