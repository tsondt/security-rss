Title: Go SAML library vulnerable to authentication bypass
Date: 2022-12-02T11:06:44+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-02-go-saml-library-vulnerable-to-authentication-bypass

[Source](https://portswigger.net/daily-swig/go-saml-library-vulnerable-to-authentication-bypass){:target="_blank" rel="noopener"}

> An attacker could masquerade as an authenticated user without presenting credentials [...]
