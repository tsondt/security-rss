Title: CloudSEK claims it was hacked by another cybersecurity firm
Date: 2022-12-07T13:24:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-07-cloudsek-claims-it-was-hacked-by-another-cybersecurity-firm

[Source](https://www.bleepingcomputer.com/news/security/cloudsek-claims-it-was-hacked-by-another-cybersecurity-firm/){:target="_blank" rel="noopener"}

> Indian cybersecurity firm CloudSEK says a threat actor gained access to its Confluence server using stolen credentials for one of its employees' Jira accounts. [...]
