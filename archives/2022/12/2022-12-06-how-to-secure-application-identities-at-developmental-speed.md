Title: How to secure application identities at developmental speed
Date: 2022-12-06T10:30:13+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2022-12-06-how-to-secure-application-identities-at-developmental-speed

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/06/cyberark_webinar_securing_application_identitites2/){:target="_blank" rel="noopener"}

> We discuss the top emerging DevSecOps trends with CyberArk Webinar There you are, standing in front of two peaks, a security roadmap in your back pocket to guide you up the sheer track of the first mountain. In the other pocket, a DevOps plan that will have you leaping like a mountain goat from rock to rock up the next door peak. You wonder which mountain to scale first, but it is an impossible choice. The night is stealing all the light from the sky, and you must make up your mind.... [...]
