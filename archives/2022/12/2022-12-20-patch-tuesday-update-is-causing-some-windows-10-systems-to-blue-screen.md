Title: Patch Tuesday update is causing some Windows 10 systems to blue screen
Date: 2022-12-20T00:30:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-20-patch-tuesday-update-is-causing-some-windows-10-systems-to-blue-screen

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/20/microsoft_windows_10_crash/){:target="_blank" rel="noopener"}

> Microsoft issues a workaround for problem while it works on a fix Some users running Windows 10 who installed the KB5021233 cumulative update this month are seeing their operating system crash with the Blue Screen of Death, Microsoft is warning.... [...]
