Title: DuckDuckGo now blocks Google sign-in pop-ups on all sites
Date: 2022-12-22T15:21:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-12-22-duckduckgo-now-blocks-google-sign-in-pop-ups-on-all-sites

[Source](https://www.bleepingcomputer.com/news/security/duckduckgo-now-blocks-google-sign-in-pop-ups-on-all-sites/){:target="_blank" rel="noopener"}

> DuckDuckGo apps and extensions are now blocking Google Sign-in pop-ups on all its apps and browser extensions, removing what it perceives as an annoyance and a privacy risk for its users. [...]
