Title: Malicious PyPI package found posing as a SentinelOne SDK
Date: 2022-12-21T09:45:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-21-malicious-pypi-package-found-posing-as-a-sentinelone-sdk

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/21/pypi_malware_sentinelone/){:target="_blank" rel="noopener"}

> Security firm tagged with malware misrepresentation Threat researchers have found a rapidly updated malicious Python package on PyPI masquerading as a legitimate software-development kit (SDK) from cybersecurity firm SentinelOne, but actually contains malware designed to exfiltrate data from infected systems.... [...]
