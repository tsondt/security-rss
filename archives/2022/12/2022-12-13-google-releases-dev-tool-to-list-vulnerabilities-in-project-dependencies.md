Title: Google releases dev tool to list vulnerabilities in project dependencies
Date: 2022-12-13T13:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Software
Slug: 2022-12-13-google-releases-dev-tool-to-list-vulnerabilities-in-project-dependencies

[Source](https://www.bleepingcomputer.com/news/security/google-releases-dev-tool-to-list-vulnerabilities-in-project-dependencies/){:target="_blank" rel="noopener"}

> Google has launched OSV Scanner, a new tool that allows developers to scan for vulnerabilities in open-source software dependencies used in their project. [...]
