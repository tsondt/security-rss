Title: Hacking Boston’s CharlieCard
Date: 2022-12-14T12:01:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hacking;public transit;transportation
Slug: 2022-12-14-hacking-bostons-charliecard

[Source](https://www.schneier.com/blog/archives/2022/12/hacking-bostons-charliecard.html){:target="_blank" rel="noopener"}

> Interesting discussion of vulnerabilities and exploits against Boston’s CharlieCard. [...]
