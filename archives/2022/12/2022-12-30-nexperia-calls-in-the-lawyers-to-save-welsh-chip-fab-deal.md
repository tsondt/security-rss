Title: Nexperia calls in the lawyers to save Welsh chip fab deal
Date: 2022-12-30T08:04:09+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2022-12-30-nexperia-calls-in-the-lawyers-to-save-welsh-chip-fab-deal

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/30/newport_nexperia_china/){:target="_blank" rel="noopener"}

> Oh, Shapps Blocked by the British government from acquiring Newport Wafer Fab — Britain's largest chip factory — Nexperia has solicited the help of US law firm Akin Gump in the hopes of overturning the ban.... [...]
