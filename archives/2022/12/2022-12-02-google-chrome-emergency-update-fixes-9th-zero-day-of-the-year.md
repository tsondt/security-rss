Title: Google Chrome emergency update fixes 9th zero-day of the year
Date: 2022-12-02T16:44:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-02-google-chrome-emergency-update-fixes-9th-zero-day-of-the-year

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-emergency-update-fixes-9th-zero-day-of-the-year/){:target="_blank" rel="noopener"}

> Google has released Chrome 108.0.5359.94/.95 for Windows, Mac, and Linux users to address a single high-severity security flaw, the ninth Chrome zero-day exploited in the wild patched since the start of the year. [...]
