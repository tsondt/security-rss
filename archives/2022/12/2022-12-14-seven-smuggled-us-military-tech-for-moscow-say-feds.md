Title: Seven smuggled US military tech for Moscow, say Feds
Date: 2022-12-14T22:30:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-12-14-seven-smuggled-us-military-tech-for-moscow-say-feds

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/14/us_doj_disrupts_russian_smuggling/){:target="_blank" rel="noopener"}

> Nuclear, hypersonic hardware is one thing, but you can probably keep the quantum computer stuff, Vlad The US Department of Justice unsealed a 16-count indictment today accusing five Russians, an American citizen, and a lawful permanent US resident of smuggling export-controlled electronics and military ammunition out of the United States for the Russian government.... [...]
