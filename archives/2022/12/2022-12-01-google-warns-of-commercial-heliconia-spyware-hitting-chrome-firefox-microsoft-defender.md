Title: Google warns of commercial Heliconia spyware hitting Chrome, Firefox, Microsoft Defender
Date: 2022-12-01T20:30:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-12-01-google-warns-of-commercial-heliconia-spyware-hitting-chrome-firefox-microsoft-defender

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/01/google_heliconia_spyware/){:target="_blank" rel="noopener"}

> Meanwhile NSO faces new lawsuit over Pegasus flying onto journalists' phones Google's Threat Analysis Group (TAG) said on Wednesday that its researchers discovered commercial spyware called Heliconia that's designed to exploit vulnerabilities in Chrome and Firefox browsers as well as Microsoft Defender security software.... [...]
