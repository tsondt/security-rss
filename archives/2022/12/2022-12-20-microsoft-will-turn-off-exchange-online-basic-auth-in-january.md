Title: Microsoft will turn off Exchange Online basic auth in January
Date: 2022-12-20T15:22:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-12-20-microsoft-will-turn-off-exchange-online-basic-auth-in-january

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-will-turn-off-exchange-online-basic-auth-in-january/){:target="_blank" rel="noopener"}

> Microsoft warned today that it will permanently turn off Exchange Online basic authentication starting early January 2023 to improve security. [...]
