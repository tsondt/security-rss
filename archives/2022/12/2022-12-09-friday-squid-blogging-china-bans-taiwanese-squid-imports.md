Title: Friday Squid Blogging: China Bans Taiwanese Squid Imports
Date: 2022-12-09T22:06:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;squid;Taiwan
Slug: 2022-12-09-friday-squid-blogging-china-bans-taiwanese-squid-imports

[Source](https://www.schneier.com/blog/archives/2022/12/friday-squid-blogging-china-bans-taiwanese-squid-imports.html){:target="_blank" rel="noopener"}

> Today I have some squid geopolitical news. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
