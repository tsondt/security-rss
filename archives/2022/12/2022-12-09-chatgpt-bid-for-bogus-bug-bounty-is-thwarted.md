Title: ChatGPT bid for bogus bug bounty is thwarted
Date: 2022-12-09T16:55:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-09-chatgpt-bid-for-bogus-bug-bounty-is-thwarted

[Source](https://portswigger.net/daily-swig/chatgpt-bid-for-bogus-bug-bounty-is-thwarted){:target="_blank" rel="noopener"}

> Improving large language models offer ‘just one more way to attack code, and one more way to defend code’ [...]
