Title: Finding&nbsp;the next Log4j –&nbsp;OpenSSF’s Brian Behlendorf on pivoting to a ‘risk-centred view’ of open source development
Date: 2022-12-23T12:17:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-23-finding-the-next-log4j-openssfs-brian-behlendorf-on-pivoting-to-a-risk-centred-view-of-open-source-development

[Source](https://portswigger.net/daily-swig/finding-nbsp-the-next-log4j-nbsp-openssfs-brian-behlendorf-on-pivoting-to-a-risk-centred-view-of-open-source-development){:target="_blank" rel="noopener"}

> Apache pioneer says ‘use at your own risk’ model no longer tenable as OpenSSF ramps up end user engagement [...]
