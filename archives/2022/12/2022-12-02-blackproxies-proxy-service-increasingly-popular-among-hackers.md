Title: BlackProxies proxy service increasingly popular among hackers
Date: 2022-12-02T13:56:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-02-blackproxies-proxy-service-increasingly-popular-among-hackers

[Source](https://www.bleepingcomputer.com/news/security/blackproxies-proxy-service-increasingly-popular-among-hackers/){:target="_blank" rel="noopener"}

> A new residential proxy market is becoming popular among hackers, cybercriminals, phishers, scalpers, and scammers, selling access to a million claimed proxy IP addresses worldwide. [...]
