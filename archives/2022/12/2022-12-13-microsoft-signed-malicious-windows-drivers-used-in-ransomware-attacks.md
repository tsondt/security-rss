Title: Microsoft-signed malicious Windows drivers used in ransomware attacks
Date: 2022-12-13T18:10:03-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-12-13-microsoft-signed-malicious-windows-drivers-used-in-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-signed-malicious-windows-drivers-used-in-ransomware-attacks/){:target="_blank" rel="noopener"}

> Microsoft has revoked several Microsoft hardware developer accounts after drivers signed through their profiles were used in cyberattacks, including ransomware incidents. [...]
