Title: US Health Dept warns of Royal Ransomware targeting healthcare
Date: 2022-12-08T17:40:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-08-us-health-dept-warns-of-royal-ransomware-targeting-healthcare

[Source](https://www.bleepingcomputer.com/news/security/us-health-dept-warns-of-royal-ransomware-targeting-healthcare/){:target="_blank" rel="noopener"}

> The U.S. Department of Health and Human Services (HHS) issued a new warning today for the country's healthcare organizations regarding ongoing attacks from a relatively new operation, the Royal ransomware gang. [...]
