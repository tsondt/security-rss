Title: Clare O’Neil on national security amid cyber hacks and threats to democracy
Date: 2022-12-16T14:00:54+00:00
Author: Hosted by Katharine Murphy. Produced by Allison Chan. Executive producer is Molly Glassey
Category: The Guardian
Tags: Australian politics;Cybercrime;Australia news;Hacking;Data and computer security;Technology
Slug: 2022-12-16-clare-oneil-on-national-security-amid-cyber-hacks-and-threats-to-democracy

[Source](https://www.theguardian.com/australia-news/audio/2022/dec/17/clare-oneil-on-national-security-amid-cyber-hacks-and-threats-to-democracy){:target="_blank" rel="noopener"}

> In the final episode of Australian Politics for 2022, political editor Katharine Murphy speaks to the minister for home affairs and cyber security Clare O’Neil about the strategic challenges for Australia and the region. These include the increased likelihood of cyber-attacks, decreasing trust in democracy and growing risks of foreign interference Read more: Home affairs and the long view – Clare O’Neil’s speech at the National Press Club Continue reading... [...]
