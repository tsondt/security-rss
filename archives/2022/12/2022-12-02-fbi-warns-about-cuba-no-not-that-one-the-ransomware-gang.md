Title: FBI warns about Cuba, no, not that one — the ransomware gang
Date: 2022-12-02T20:30:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-02-fbi-warns-about-cuba-no-not-that-one-the-ransomware-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/02/fbi_warning_cuba_ransomware/){:target="_blank" rel="noopener"}

> Critical infrastructure attacks ramping up The US government has issued an alert about Cuba; not the state but a ransomware gang that's taking millions in purloined profits.... [...]
