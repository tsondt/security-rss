Title: TPG reveals emails of 15,000 iiNet and Westnet customers exposed in hack
Date: 2022-12-14T03:05:11+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Telecommunications industry;Australia news;Data and computer security;Cybercrime;Internet;Medibank;Hacking
Slug: 2022-12-14-tpg-reveals-emails-of-15000-iinet-and-westnet-customers-exposed-in-hack

[Source](https://www.theguardian.com/business/2022/dec/14/tpg-reveals-emails-of-15000-iinet-and-westnet-customers-exposed-in-hack){:target="_blank" rel="noopener"}

> Telecommunications company says hacker searched for customers’ cryptocurrency and financial information Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Telecommunications giant TPG has revealed an email-hosting service used by up to 15,000 iiNet and Westnet business customers has been breached, with the hacker looking for cryptocurrency and other financial information. TPG said in a release to the Australian Securities Exchange (ASX) on Wednesday that cybersecurity firm Mandiant had found evidence of unauthorised access to a Hosted Exchange service used by iiNet and Westnet business customers. [...]
