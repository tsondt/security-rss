Title: Brave launches FrodoPIR, a privacy-focused database query system
Date: 2022-12-22T11:08:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2022-12-22-brave-launches-frodopir-a-privacy-focused-database-query-system

[Source](https://www.bleepingcomputer.com/news/security/brave-launches-frodopir-a-privacy-focused-database-query-system/){:target="_blank" rel="noopener"}

> Brave Software developers have created a new privacy-centric database query system called FrodoPIR that retrieves data from servers without disclosing the content of user queries. [...]
