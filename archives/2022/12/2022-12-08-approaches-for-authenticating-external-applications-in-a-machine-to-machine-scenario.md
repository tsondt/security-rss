Title: Approaches for authenticating external applications in a machine-to-machine scenario
Date: 2022-12-08T22:40:43+00:00
Author: Patrick Sard
Category: AWS Security
Tags: Best Practices;Foundational (100);Security, Identity, & Compliance;M2M;Machine to machine;Security;Security Blog
Slug: 2022-12-08-approaches-for-authenticating-external-applications-in-a-machine-to-machine-scenario

[Source](https://aws.amazon.com/blogs/security/approaches-for-authenticating-external-applications-in-a-machine-to-machine-scenario/){:target="_blank" rel="noopener"}

> December 8, 2022: This post has been updated to reflect changes for M2M options with the new service of IAMRA. This blog post was first published November 19, 2013. August 10, 2022: This blog post has been updated to reflect the new name of AWS Single Sign-On (SSO) – AWS IAM Identity Center. Read more [...]
