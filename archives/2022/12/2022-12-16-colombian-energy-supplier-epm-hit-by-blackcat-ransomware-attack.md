Title: Colombian energy supplier EPM hit by BlackCat ransomware attack
Date: 2022-12-16T13:47:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-16-colombian-energy-supplier-epm-hit-by-blackcat-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/colombian-energy-supplier-epm-hit-by-blackcat-ransomware-attack/){:target="_blank" rel="noopener"}

> Colombian energy company Empresas Públicas de Medellín (EPM) suffered a BlackCat/ALPHV ransomware attack on Monday, disrupting the company's operations and taking down online services. [...]
