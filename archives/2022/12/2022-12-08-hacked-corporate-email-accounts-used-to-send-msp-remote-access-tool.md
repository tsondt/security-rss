Title: Hacked corporate email accounts used to send MSP remote access tool
Date: 2022-12-08T16:19:09-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-12-08-hacked-corporate-email-accounts-used-to-send-msp-remote-access-tool

[Source](https://www.bleepingcomputer.com/news/security/hacked-corporate-email-accounts-used-to-send-msp-remote-access-tool/){:target="_blank" rel="noopener"}

> MuddyWater hackers, a group associated with Iran's Ministry of Intelligence and Security (MOIS), used compromised corporate email accounts to deliver phishing messages to their targets. [...]
