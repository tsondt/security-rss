Title: Big Apple locals hire Russians to game New York's taxi system
Date: 2022-12-20T21:30:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-12-20-big-apple-locals-hire-russians-to-game-new-yorks-taxi-system

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/20/jrk_nyc_russian_hacking/){:target="_blank" rel="noopener"}

> Scheme allegedly allowed taxi drivers to bypass airport waiting line for $10 fee Two men have been charged for allegedly conspiring with Russian hackers to manipulate the taxi dispatch system at New York's John F. Kennedy International Airport.... [...]
