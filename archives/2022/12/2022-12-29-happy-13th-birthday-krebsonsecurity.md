Title: Happy 13th Birthday, KrebsOnSecurity!
Date: 2022-12-29T22:35:36+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other
Slug: 2022-12-29-happy-13th-birthday-krebsonsecurity

[Source](https://krebsonsecurity.com/2022/12/happy-13th-birthday-krebsonsecurity/){:target="_blank" rel="noopener"}

> KrebsOnSecurity turns 13 years old today. That’s a crazy long time for an independent media outlet these days, but then again I’m bound to keep doing this as long as they keep letting me. Heck, I’ve been doing this so long I briefly forgot which birthday this was! Thanks to your readership and support, I was able to spend more time in 2022 on some deep, meaty investigative stories — the really satisfying kind with the potential to affect positive change. Some of that work is highlighted in the 2022 Year in Review review below. Until recently, I was fairly [...]
