Title: McGraw Hill's S3 buckets exposed 100,000 students' grades and personal info
Date: 2022-12-20T03:30:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-20-mcgraw-hills-s3-buckets-exposed-100000-students-grades-and-personal-info

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/20/mcgraw_hills_s3_buckets_exposed/){:target="_blank" rel="noopener"}

> Educator gets an F for security Misconfigured Amazon Web Services S3 buckets belonging to McGraw Hill exposed more than 100,000 students' information as well as the education publishing giant's own source code and digital keys, according to security researchers.... [...]
