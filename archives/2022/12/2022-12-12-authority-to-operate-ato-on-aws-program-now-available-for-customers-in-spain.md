Title: Authority to operate (ATO) on AWS Program now available for customers in Spain
Date: 2022-12-12T21:21:44+00:00
Author: Greg Herrmann
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS security;Commercial Sector;Compliance;Public Sector;Security Blog;Spain
Slug: 2022-12-12-authority-to-operate-ato-on-aws-program-now-available-for-customers-in-spain

[Source](https://aws.amazon.com/blogs/security/authority-to-operate-on-aws-program-now-available-for-customers-in-spain/){:target="_blank" rel="noopener"}

> Meeting stringent security and compliance requirements in regulated or public sector environments can be challenging and time consuming, even for organizations with strong technical competencies. To help customers navigate the different requirements and processes, we launched the ATO on AWS Program in June 2019 for US customers. The program involves a community of expert AWS [...]
