Title: Play ransomware claims attack on German hotel chain H-Hotels
Date: 2022-12-19T16:40:52-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-19-play-ransomware-claims-attack-on-german-hotel-chain-h-hotels

[Source](https://www.bleepingcomputer.com/news/security/play-ransomware-claims-attack-on-german-hotel-chain-h-hotels/){:target="_blank" rel="noopener"}

> The Play ransomware gang has claimed responsibility for a cyber attack on H-Hotels (h-hotels.com) that has resulted in communication outages for the company. [...]
