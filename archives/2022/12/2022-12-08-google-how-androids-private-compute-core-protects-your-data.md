Title: Google: How Android’s Private Compute Core protects your data
Date: 2022-12-08T12:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2022-12-08-google-how-androids-private-compute-core-protects-your-data

[Source](https://www.bleepingcomputer.com/news/security/google-how-android-s-private-compute-core-protects-your-data/){:target="_blank" rel="noopener"}

> Google has disclosed more technical details about how Private Compute Core (PCC) on Android works and keeps sensitive user data processed locally on protected devices. [...]
