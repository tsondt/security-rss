Title: Ukraine shuts down fraudulent call center claiming 18,000 victims
Date: 2022-12-29T17:19:01-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-29-ukraine-shuts-down-fraudulent-call-center-claiming-18000-victims

[Source](https://www.bleepingcomputer.com/news/security/ukraine-shuts-down-fraudulent-call-center-claiming-18-000-victims/){:target="_blank" rel="noopener"}

> A group of imposters operating out of a Ukrainian call center defrauded thousands of victims while pretending to be IT security employees at their banks and leading them to believe that attackers had gained access to their bank accounts. [...]
