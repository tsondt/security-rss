Title: How to use Amazon Verified Permissions for authorization
Date: 2022-12-12T16:14:38+00:00
Author: Jeremy Ware
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Technical How-to;custom application access;fine-grained application permissions;fine-grained authorization;policy-based access control;Security Blog
Slug: 2022-12-12-how-to-use-amazon-verified-permissions-for-authorization

[Source](https://aws.amazon.com/blogs/security/how-to-use-amazon-verified-permissions-for-authorization/){:target="_blank" rel="noopener"}

> Applications with multiple users and shared data require permissions management. The permissions describe what each user of an application is permitted to do. Permissions are defined as allow or deny decisions for resources in the application. To manage permissions, developers often combine attribute-based access control (ABAC) and role-based access control (RBAC) models with custom code [...]
