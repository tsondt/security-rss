Title: Telstra sorry for publishing up to 130,000 customers’ details online
Date: 2022-12-11T01:54:14+00:00
Author: Australian Associated Press
Category: The Guardian
Tags: Telstra;Australia news;Telecommunications industry;Data and computer security;Cybercrime
Slug: 2022-12-11-telstra-sorry-for-publishing-up-to-130000-customers-details-online

[Source](https://www.theguardian.com/business/2022/dec/11/telstra-sorry-for-publishing-up-to-130000-customers-details-online){:target="_blank" rel="noopener"}

> Release of names, numbers and addresses of some unlisted customers was not due to cyber-attack Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Telstra has apologised after publishing the details of thousands of customers online. The company said the release of the names, numbers and addresses of some unlisted customers was not the result of any malicious cyber-attack and was a mistake. Reports say up to 130,000 customers have been affected. Sign up for Guardian Australia’s free morning and afternoon email newsletters for your daily [...]
