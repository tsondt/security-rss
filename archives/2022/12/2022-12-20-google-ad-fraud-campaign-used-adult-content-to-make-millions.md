Title: Google Ad fraud campaign used adult content to make millions
Date: 2022-12-20T15:08:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-12-20-google-ad-fraud-campaign-used-adult-content-to-make-millions

[Source](https://www.bleepingcomputer.com/news/security/google-ad-fraud-campaign-used-adult-content-to-make-millions/){:target="_blank" rel="noopener"}

> A massive advertising fraud campaign using Google Ads and 'popunders' on adult sites is estimated to have generated millions of ad impressions on stolen articles, making the fraudsters an estimated $275k per month. [...]
