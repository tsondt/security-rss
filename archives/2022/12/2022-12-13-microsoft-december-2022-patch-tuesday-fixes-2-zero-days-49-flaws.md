Title: Microsoft December 2022 Patch Tuesday fixes 2 zero-days, 49 flaws
Date: 2022-12-13T13:27:44-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-12-13-microsoft-december-2022-patch-tuesday-fixes-2-zero-days-49-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-december-2022-patch-tuesday-fixes-2-zero-days-49-flaws/){:target="_blank" rel="noopener"}

> ​Today is Microsoft's December 2022 Patch Tuesday, and with it comes fixes for two zero-day vulnerabilities, including an actively exploited bug, and a total of 49 flaws. [...]
