Title: Leading sports betting firm BetMGM discloses data breach
Date: 2022-12-22T17:25:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-22-leading-sports-betting-firm-betmgm-discloses-data-breach

[Source](https://www.bleepingcomputer.com/news/security/leading-sports-betting-firm-betmgm-discloses-data-breach/){:target="_blank" rel="noopener"}

> Leading sports betting company BetMGM disclosed a data breach after a threat actor stole personal information belonging to an undisclosed number of customers. [...]
