Title: Overcoming objections and unblocking the road to Zero Trust
Date: 2022-12-02T17:00:00+00:00
Author: Tim Knudsen
Category: GCP Security
Tags: Identity & Security
Slug: 2022-12-02-overcoming-objections-and-unblocking-the-road-to-zero-trust

[Source](https://cloud.google.com/blog/products/identity-security/industry-insights-on-overcoming-objections-to-zero-trust/){:target="_blank" rel="noopener"}

> Overcoming blockades and potholes that threaten to derail organizational change is key to any IT or security transformation initiative. Many security and risk leaders have made it a priority to adopt Zero Trust access models so they can deliver better user experiences and strengthen security. Yet before they can even think about change management, they often face pushback from within their organization. Earlier this year I had the privilege of chatting twice with Jess Burn, senior analyst at Forrester, on some common challenges CISOs face when planning their Zero Trust journeys. I found our talks enlightening and useful, and wanted [...]
