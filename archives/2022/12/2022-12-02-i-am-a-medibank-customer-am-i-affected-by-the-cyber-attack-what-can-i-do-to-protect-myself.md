Title: I am a Medibank customer. Am I affected by the cyber-attack? What can I do to protect myself?
Date: 2022-12-02T03:08:49+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Cybercrime;Australia news;Technology;Data protection;Data and computer security;Medibank
Slug: 2022-12-02-i-am-a-medibank-customer-am-i-affected-by-the-cyber-attack-what-can-i-do-to-protect-myself

[Source](https://www.theguardian.com/technology/2022/dec/02/i-am-a-medibank-customer-am-i-affected-by-the-cyberattack-what-can-i-do-to-protect-myself){:target="_blank" rel="noopener"}

> Experts suggest using multifactor authentication and telling your bank to put extra security checks in place Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Millions of Medibank’s current and former customers have had their personal information, including health claims, exposed in a hack of the company’s customer database. Here’s what we know so far and what you can do. Sign up for Guardian Australia’s free morning and afternoon email newsletters for your daily news roundup name address date of birth gender email Medicare card number [...]
