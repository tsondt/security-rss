Title: Trust Update: December 2022
Date: 2022-12-08T17:00:00+00:00
Author: Tanya Popova-Jones
Category: GCP Security
Tags: Identity & Security
Slug: 2022-12-08-trust-update-december-2022

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-trust-update-for-december-2022/){:target="_blank" rel="noopener"}

> Every day, teams across Google Cloud come together to help address complex and pressing compliance, risk, and privacy requirements that enable customers to accelerate their digital transformation and innovate faster. Let’s take a look at some of our top trust and compliance-related efforts from the last few months: Combining data processing terms. In September, we updated and consolidated our data processing terms for Google Cloud, Google Workspace (including Workspace for Education), and Cloud Identity (when purchased separately). The combined terms are now one Cloud Data Processing Addendum (the “CDPA”). The updated terms maintain the previous benefits while strengthening our data [...]
