Title: Twitter confirms recent user data leak is from 2021 breach
Date: 2022-12-12T14:27:52-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-12-twitter-confirms-recent-user-data-leak-is-from-2021-breach

[Source](https://www.bleepingcomputer.com/news/security/twitter-confirms-recent-user-data-leak-is-from-2021-breach/){:target="_blank" rel="noopener"}

> Twitter confirmed today that the recent leak of millions of members' profiles, including private phone numbers and email addresses, resulted from the same data breach the company disclosed in August 2022. [...]
