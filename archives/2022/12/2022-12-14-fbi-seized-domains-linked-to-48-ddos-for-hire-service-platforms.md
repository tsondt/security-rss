Title: FBI seized domains linked to 48 DDoS-for-hire service platforms
Date: 2022-12-14T16:20:16-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-14-fbi-seized-domains-linked-to-48-ddos-for-hire-service-platforms

[Source](https://www.bleepingcomputer.com/news/security/fbi-seized-domains-linked-to-48-ddos-for-hire-service-platforms/){:target="_blank" rel="noopener"}

> The US Department of Justice has seized 48 Internet domains and charged six suspects for their involvement in running 'Booter' or 'Stresser' platforms that allow anyone to easily conduct distributed denial of service attacks. [...]
