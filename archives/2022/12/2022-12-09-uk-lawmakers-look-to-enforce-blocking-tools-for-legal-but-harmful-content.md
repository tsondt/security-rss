Title: UK lawmakers look to enforce blocking tools for legal but harmful content
Date: 2022-12-09T13:30:08+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-12-09-uk-lawmakers-look-to-enforce-blocking-tools-for-legal-but-harmful-content

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/09/uk_law_makers_to_enforce/){:target="_blank" rel="noopener"}

> The latest idea in the long gestation of the online harms legislation The UK government is putting forward changes to the law which would require social media platforms to give users the option to avoid seeing and engaging with harmful — but legal — content.... [...]
