Title: Social Blade confirms breach after hacker posts stolen user data
Date: 2022-12-15T10:29:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-15-social-blade-confirms-breach-after-hacker-posts-stolen-user-data

[Source](https://www.bleepingcomputer.com/news/security/social-blade-confirms-breach-after-hacker-posts-stolen-user-data/){:target="_blank" rel="noopener"}

> Social media analytics platform Social Blade has confirmed they suffered a data breach after its database was breached and put up for sale on a hacking forum. [...]
