Title: Apple announces new security and privacy measures amid surge in cyber-attacks
Date: 2022-12-08T18:33:14+00:00
Author: Johana Bhuiyan
Category: The Guardian
Tags: Apple;Encryption;US news;Privacy;Data and computer security;US supreme court;Technology
Slug: 2022-12-08-apple-announces-new-security-and-privacy-measures-amid-surge-in-cyber-attacks

[Source](https://www.theguardian.com/technology/2022/dec/07/apple-new-security-privacy-measures-spike-cyber-attacks){:target="_blank" rel="noopener"}

> Encryption of iCloud storage means the information will be safeguarded from hackers as well as government agencies Apple announced a suite of security and privacy improvements on Wednesday that the company is pitching as a way to help people protect their data from hackers, including one that civil liberty and privacy advocates have long pushed for. The tech giant will soon allow users to choose to secure more of the data backed up to their iCloud using end-to-end encryption, which means no one but the user will be able to access that information. Continue reading... [...]
