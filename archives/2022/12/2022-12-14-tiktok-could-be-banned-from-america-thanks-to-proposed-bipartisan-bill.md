Title: TikTok could be banned from America, thanks to proposed bipartisan bill
Date: 2022-12-14T19:30:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-12-14-tiktok-could-be-banned-from-america-thanks-to-proposed-bipartisan-bill

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/14/tiktok_ban_proposed_us/){:target="_blank" rel="noopener"}

> If you listen really closely, you can hear Mark Zuckerberg's excitement The US government's crackdown on TikTok continues, with the latest salvo being a bipartisan bill that would outright ban the popular social media app from doing business in the country.... [...]
