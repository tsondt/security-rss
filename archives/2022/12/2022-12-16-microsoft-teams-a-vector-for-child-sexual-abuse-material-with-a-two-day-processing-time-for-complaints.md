Title: Microsoft Teams: A vector for child sexual abuse material with a two-day processing time for complaints
Date: 2022-12-16T06:32:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-12-16-microsoft-teams-a-vector-for-child-sexual-abuse-material-with-a-two-day-processing-time-for-complaints

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/16/esafety_comissioner_csea_report/){:target="_blank" rel="noopener"}

> Redmond and Cupertino criticized for slow and weak responses by Australian regulator Australia's e-safety commissioner, a government agency charged with keeping citizens safe online, has delivered a report on seven tech platforms' mechanisms to protect children from online sexual abuse – and found most don't respond quickly, or have the processes to do so well.... [...]
