Title: Five steps to help make your software supply chain more secure
Date: 2022-12-08T17:00:00+00:00
Author: Jacob Crisp
Category: GCP Security
Tags: Identity & Security
Slug: 2022-12-08-five-steps-to-help-make-your-software-supply-chain-more-secure

[Source](https://cloud.google.com/blog/products/identity-security/5-steps-to-help-make-your-software-supply-chain-more-secure/){:target="_blank" rel="noopener"}

> Today, we published a new Google research report on software supply chain security because we’ve seen a sharp rise in software supply chain attacks across almost every sector —and expect these trends to continue for the foreseeable future. We urge all organizations to act now to improve their software supply chain security. Among the report’s conclusions, there are two key findings we want to highlight. First, the lessons we’ve learned from various security events call for a more holistic approach to strengthen defenses against software supply chain attacks. Second, we have worked with the security community to develop and deploy [...]
