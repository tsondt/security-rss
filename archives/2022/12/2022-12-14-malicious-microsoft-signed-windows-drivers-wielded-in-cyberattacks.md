Title: Malicious Microsoft-signed Windows drivers wielded in cyberattacks
Date: 2022-12-14T23:24:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-14-malicious-microsoft-signed-windows-drivers-wielded-in-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/14/microsoft_drivers_ransomware_attacks/){:target="_blank" rel="noopener"}

> Handy tools to kill off security protections get Redmond's stamp of approval Microsoft says it has suspended several third-party developer accounts that submitted malicious Windows drivers for the IT giant to digitally sign so that the code could be used in cyberattacks.... [...]
