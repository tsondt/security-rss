Title: IT security teams, business execs still not on same page
Date: 2022-12-12T07:30:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-12-12-it-security-teams-business-execs-still-not-on-same-page

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/12/study_security_business_leaders_still/){:target="_blank" rel="noopener"}

> Also: Guri the air-gap guru strikes again, while pro-Ukraine hackers set up a proxy network in Russia In brief Let's start with the good news: according to a survey of security and business leaders, executives have become far more aware of the importance of cyber security in the past two years, better aligning security teams and leadership.... [...]
