Title: US adds 36 Chinese entities to naughty list, drops 25 after checking it twice
Date: 2022-12-16T05:14:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-12-16-us-adds-36-chinese-entities-to-naughty-list-drops-25-after-checking-it-twice

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/16/us_bans_companies_it_suspects/){:target="_blank" rel="noopener"}

> Some are suspected of helping other banned suppliers get around sanctions The United States Department of Commerce has added 36 Chinese companies or subsidiaries to its list of companies that cannot import certain US technologies without a license, citing national security, foreign policy interests, and the possibility that some might help already banned companies to evade restrictions.... [...]
