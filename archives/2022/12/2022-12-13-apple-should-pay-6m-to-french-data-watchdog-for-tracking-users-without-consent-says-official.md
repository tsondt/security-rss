Title: Apple should pay €6m to French data watchdog for tracking users without consent, says official
Date: 2022-12-13T13:00:08+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-12-13-apple-should-pay-6m-to-french-data-watchdog-for-tracking-users-without-consent-says-official

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/13/apple_should_be_fined_6mn/){:target="_blank" rel="noopener"}

> Recommendation from top CNIL advisor claims Cupertino broke EU privacy laws Apple tracked users without their consent and deserves to be fined €6 million, according to a top advisor to France's data privacy watchdog.... [...]
