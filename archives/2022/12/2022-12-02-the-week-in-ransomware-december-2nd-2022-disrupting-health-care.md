Title: The Week in Ransomware - December 2nd 2022 - Disrupting Health Care
Date: 2022-12-02T17:51:35-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-02-the-week-in-ransomware-december-2nd-2022-disrupting-health-care

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-2nd-2022-disrupting-health-care/){:target="_blank" rel="noopener"}

> This week's big news was the Colombia health system being severely disrupted by a ransomware attack on Keralty, one of the country's largest healthcare providers. [...]
