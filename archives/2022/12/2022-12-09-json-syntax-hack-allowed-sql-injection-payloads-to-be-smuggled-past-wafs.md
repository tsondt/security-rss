Title: JSON syntax hack allowed SQL injection payloads to be smuggled past WAFs
Date: 2022-12-09T13:17:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-09-json-syntax-hack-allowed-sql-injection-payloads-to-be-smuggled-past-wafs

[Source](https://portswigger.net/daily-swig/json-syntax-hack-allowed-sql-injection-payloads-to-be-smuggled-past-wafs){:target="_blank" rel="noopener"}

> Five vendors act to thwart generic hack [...]
