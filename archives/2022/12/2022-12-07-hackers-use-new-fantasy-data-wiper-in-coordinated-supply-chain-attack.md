Title: Hackers use new Fantasy data wiper in coordinated supply chain attack
Date: 2022-12-07T12:36:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-07-hackers-use-new-fantasy-data-wiper-in-coordinated-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-new-fantasy-data-wiper-in-coordinated-supply-chain-attack/){:target="_blank" rel="noopener"}

> The Iranian Agrius APT hacking group is using a new 'Fantasy' data wiper in supply-chain attacks impacting organizations in Israel, Hong Kong, and South Africa. [...]
