Title: Friday Squid Blogging: Grounded Fishing Boat Carrying 16,000 Pounds of Squid
Date: 2022-12-30T22:56:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-12-30-friday-squid-blogging-grounded-fishing-boat-carrying-16000-pounds-of-squid

[Source](https://www.schneier.com/blog/archives/2022/12/friday-squid-blogging-grounded-fishing-boat-carrying-16000-pounds-of-squid.html){:target="_blank" rel="noopener"}

> Rough seas are hampering efforts to salvage the boat : The Speranza Marie, carrying 16,000 pounds of squid and some 1,000 gallons of diesel fuel, hit the shoreline near Chinese Harbor at about 2 a.m. on Dec. 15. Six crew members were on board, and all were rescued without injury by another fishing boat. [...] However, large swells caused by the recent storm caused the Speranza Marie to pull loose from it anchored position and drift about 100 yards from from its original grounded location in Chinese Harbor, according to the Coast Guard. As usual, you can also use this [...]
