Title: Akamai WAF bypassed via Spring Boot to trigger RCE
Date: 2022-12-14T12:01:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-14-akamai-waf-bypassed-via-spring-boot-to-trigger-rce

[Source](https://portswigger.net/daily-swig/akamai-waf-bypassed-via-spring-boot-to-trigger-rce){:target="_blank" rel="noopener"}

> Akamai issued an update to resolve the flaw several months ago [...]
