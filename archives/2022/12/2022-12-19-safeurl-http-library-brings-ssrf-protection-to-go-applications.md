Title: Safeurl HTTP library brings SSRF protection to Go applications
Date: 2022-12-19T12:30:29+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-19-safeurl-http-library-brings-ssrf-protection-to-go-applications

[Source](https://portswigger.net/daily-swig/safeurl-http-library-brings-ssrf-protection-to-go-applications){:target="_blank" rel="noopener"}

> Prizes offered to anyone who can bypass the library and capture the flag [...]
