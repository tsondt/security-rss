Title: US Air Force reveals B-21 Raider stealth bomber that'll fly the unfriendly skies
Date: 2022-12-03T02:58:59+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-12-03-us-air-force-reveals-b-21-raider-stealth-bomber-thatll-fly-the-unfriendly-skies

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/03/us_air_force_reveals_b21/){:target="_blank" rel="noopener"}

> 'Digital bomber' will bring 'peace through deterrence' In Palmdale, California on Friday, Northrop Grumman CEO Kathy Warden revealed a US Air Force warplane that had only been shown in artist renderings and is supposed to be seldom seen, the B-21 Raider.... [...]
