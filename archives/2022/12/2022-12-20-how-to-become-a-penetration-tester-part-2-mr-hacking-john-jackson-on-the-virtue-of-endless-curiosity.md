Title: How to become a penetration tester: Part 2 – ‘Mr hacking’  John Jackson on the virtue of ‘endless curiosity’
Date: 2022-12-20T16:52:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-20-how-to-become-a-penetration-tester-part-2-mr-hacking-john-jackson-on-the-virtue-of-endless-curiosity

[Source](https://portswigger.net/daily-swig/how-to-become-a-penetration-tester-part-2-mr-hacking-john-jackson-on-the-virtue-of-endless-curiosity){:target="_blank" rel="noopener"}

> Marine Corps engineer-turned offensive security expert offers careers advice and his best and worst experiences [...]
