Title: Samsung Galaxy S22 hacked twice on first day of Pwn2Own Toronto
Date: 2022-12-06T17:35:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-06-samsung-galaxy-s22-hacked-twice-on-first-day-of-pwn2own-toronto

[Source](https://www.bleepingcomputer.com/news/security/samsung-galaxy-s22-hacked-twice-on-first-day-of-pwn2own-toronto/){:target="_blank" rel="noopener"}

> Contestants have hacked the Samsung Galaxy S22 smartphone twice during the first day of the Pwn2Own Toronto 2022 hacking competition, the 10th edition of the consumer-focused event. [...]
