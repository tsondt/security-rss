Title: Bug Bounty Radar // The latest bug bounty programs for January 2023
Date: 2022-12-30T13:40:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-12-30-bug-bounty-radar-the-latest-bug-bounty-programs-for-january-2023

[Source](https://portswigger.net/daily-swig/bug-bounty-radar-the-latest-bug-bounty-programs-for-january-2023){:target="_blank" rel="noopener"}

> New web targets for the discerning hacker [...]
