Title: Hacked Ring Cams Used to Record Swatting Victims
Date: 2022-12-20T01:24:10+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;SIM Swapping;Aspertaine;ChumLul;James Thomas Andrew McCarty;Kya Christian Nelson;SIM swapping;violence-as-a-service
Slug: 2022-12-20-hacked-ring-cams-used-to-record-swatting-victims

[Source](https://krebsonsecurity.com/2022/12/hacked-ring-cams-used-to-record-swatting-victims/){:target="_blank" rel="noopener"}

> Photo: BrandonKleinPhoto / Shutterstock.com Two U.S. men have been charged with hacking into the Ring home security cameras of a dozen random people and then “swatting” them — falsely reporting a violent incident at the target’s address to trick local police into responding with force. Prosecutors say the duo used the compromised Ring devices to stream live video footage on social media of police raiding their targets’ homes, and to taunt authorities when they arrived. Prosecutors in Los Angeles allege 20-year-old James Thomas Andrew McCarty, a.k.a. “Aspertaine,” of Charlotte, N.C., and Kya Christian Nelson, a.k.a. “ChumLul,” 22, of Racine, Wisc., [...]
