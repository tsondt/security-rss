Title: Apple security update fixes new iOS zero-day used to hack iPhones
Date: 2022-12-13T15:48:43-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Apple;Security
Slug: 2022-12-13-apple-security-update-fixes-new-ios-zero-day-used-to-hack-iphones

[Source](https://www.bleepingcomputer.com/news/apple/apple-security-update-fixes-new-ios-zero-day-used-to-hack-iphones/){:target="_blank" rel="noopener"}

> In security updates released today, Apple has fixed the tenth zero-day vulnerability since the start of the year, with this latest one actively used in attacks against iPhones. [...]
