Title: Guess which Fortune 500 brands and govt agencies share data with Twitter?
Date: 2022-12-09T14:30:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-09-guess-which-fortune-500-brands-and-govt-agencies-share-data-with-twitter

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/09/twitter_pixels_data_sharing/){:target="_blank" rel="noopener"}

> Spoiler alert: just about all of them, all across the planet More than 70,000 websites belonging to Fortune 500 brands, government agencies, and universities share consumers' data with Twitter using data tracking code hosted on these other organizations' websites, according to research published on Thursday by Adalytics.... [...]
