Title: The Week in Ransomware - December 16th 2022 - Losing Trust
Date: 2022-12-16T17:59:08-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-12-16-the-week-in-ransomware-december-16th-2022-losing-trust

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-16th-2022-losing-trust/){:target="_blank" rel="noopener"}

> Today's Week in Ransomware brings you the latest news and stories about the cyberattacks, new tactics, and reports related to ransomware operations. [...]
