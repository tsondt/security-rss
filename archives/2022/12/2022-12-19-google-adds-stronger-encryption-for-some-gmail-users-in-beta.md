Title: Google adds stronger encryption for some Gmail users, in beta
Date: 2022-12-19T23:30:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-19-google-adds-stronger-encryption-for-some-gmail-users-in-beta

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/19/google_client_side_encryption/){:target="_blank" rel="noopener"}

> Slowly inching toward E2EE Google has added client-side encryption for some email customers, allowing enterprise and education Gmail users to send and receive encrypted messages.... [...]
