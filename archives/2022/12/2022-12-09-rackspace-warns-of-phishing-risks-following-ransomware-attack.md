Title: Rackspace warns of phishing risks following ransomware attack
Date: 2022-12-09T14:51:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-09-rackspace-warns-of-phishing-risks-following-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/rackspace-warns-of-phishing-risks-following-ransomware-attack/){:target="_blank" rel="noopener"}

> Cloud computing provider Rackspace warned customers on Thursday of increased risks of phishing attacks following a ransomware attack affecting its hosted Microsoft Exchange environment. [...]
