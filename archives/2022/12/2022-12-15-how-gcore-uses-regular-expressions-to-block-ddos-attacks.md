Title: How Gcore uses regular expressions to block DDoS attacks
Date: 2022-12-15T10:07:14-05:00
Author: Sponsored by Gcore
Category: BleepingComputer
Tags: Security
Slug: 2022-12-15-how-gcore-uses-regular-expressions-to-block-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/how-gcore-uses-regular-expressions-to-block-ddos-attacks/){:target="_blank" rel="noopener"}

> In DDoS Protection, Gcore uses the bundle of XDP and regular expressions (regex). This article will explain why Gcore started using this solution (regex in XDP) and how they bound them via a third-party engine and API development. [...]
