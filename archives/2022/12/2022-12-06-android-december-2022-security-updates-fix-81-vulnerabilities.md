Title: Android December 2022 security updates fix 81 vulnerabilities
Date: 2022-12-06T11:36:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2022-12-06-android-december-2022-security-updates-fix-81-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/android-december-2022-security-updates-fix-81-vulnerabilities/){:target="_blank" rel="noopener"}

> Google has released the December 2022 security update for Android, fixing four critical-severity vulnerabilities, including a remote code execution flaw exploitable via Bluetooth. [...]
