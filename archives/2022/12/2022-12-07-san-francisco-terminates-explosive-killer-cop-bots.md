Title: San Francisco terminates explosive killer cop bots
Date: 2022-12-07T20:00:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-12-07-san-francisco-terminates-explosive-killer-cop-bots

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/07/san_francisco_terminates_killer_robots/){:target="_blank" rel="noopener"}

> I'll be back, or perhaps not San Francisco legislators this week changed course on their killer robot policy, banning the police from using remote-control bots fitted with explosives. For now.... [...]
