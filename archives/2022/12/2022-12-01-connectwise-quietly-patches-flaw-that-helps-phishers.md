Title: ConnectWise Quietly Patches Flaw That Helps Phishers
Date: 2022-12-01T19:35:11+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;ConnectWise Control;Cybir;GoTo;Ken Pyle;lastpass;Patrick Beggs
Slug: 2022-12-01-connectwise-quietly-patches-flaw-that-helps-phishers

[Source](https://krebsonsecurity.com/2022/12/connectwise-quietly-patches-flaw-that-helps-phishers/){:target="_blank" rel="noopener"}

> ConnectWise, which offers a self-hosted, remote desktop software application that is widely used by Managed Service Providers (MSPs), is warning about an unusually sophisticated phishing attack that can let attackers take remote control over user systems when recipients click the included link. The warning comes just weeks after the company quietly patched a vulnerability that makes it easier for phishers to launch these attacks. A phishing attack targeting MSP customers using ConnectWise. ConnectWise Control is extremely popular among MSPs that manage, protect and service large numbers of computers remotely for client organizations. Their product provides a dynamic software client and [...]
