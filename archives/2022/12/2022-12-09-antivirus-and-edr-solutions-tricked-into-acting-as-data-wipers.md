Title: Antivirus and EDR solutions tricked into acting as data wipers
Date: 2022-12-09T12:00:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-09-antivirus-and-edr-solutions-tricked-into-acting-as-data-wipers

[Source](https://www.bleepingcomputer.com/news/security/antivirus-and-edr-solutions-tricked-into-acting-as-data-wipers/){:target="_blank" rel="noopener"}

> A security researcher has found a way to exploit the data deletion capabilities of widely used endpoint detection and response (EDR) and antivirus (AV) software from Microsoft, SentinelOne, TrendMicro, Avast, and AVG to turn them into data wipers. [...]
