Title: Securing Application Identities in 2023
Date: 2022-12-05T10:27:03+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2022-12-05-securing-application-identities-in-2023

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/05/cyberark_webinar_securing_non_human_identitite/){:target="_blank" rel="noopener"}

> The rise and rise of non-human identities Webinar Just as Frank Sinatra sang in days gone by, 'love and marriage' goes together like a 'horse and carriage,' there should be no question about the true pairing of security and speed. Or as Sinatra went on to croon, 'try, try, try to separate them, it's an illusion.' Companies may feel they are forced to choose between securing all their application identities at the cost of speed of development, but this doesn't have to be the case.... [...]
