Title: Ukraine Intercepting Russian Soldiers’ Cell Phone Calls
Date: 2022-12-21T12:09:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;eavesdropping;iPhone;Russia;smartphones;Ukraine
Slug: 2022-12-21-ukraine-intercepting-russian-soldiers-cell-phone-calls

[Source](https://www.schneier.com/blog/archives/2022/12/ukraine-intercepting-russian-soldiers-cell-phone-calls.html){:target="_blank" rel="noopener"}

> They’re using commercial phones, which go through the Ukrainian telecom network : “You still have a lot of soldiers bringing cellphones to the frontline who want to talk to their families and they are either being intercepted as they go through a Ukrainian telecommunications provider or intercepted over the air,” said Alperovitch. “That doesn’t pose too much difficulty for the Ukrainian security services.” [...] “Security has always been a mess, both in the army and among defence officials,” the source said. “For example, in 2013 they tried to get all the staff at the ministry of defence to replace our [...]
