Title: Italy, Japan, UK to jointly launch sixth-gen fighter jet by 2035
Date: 2022-12-09T17:35:10+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-12-09-italy-japan-uk-to-jointly-launch-sixth-gen-fighter-jet-by-2035

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/09/gcap/){:target="_blank" rel="noopener"}

> Warplane project may include AI in the cockpit, and comes as tensions rise with China and Russia The United Kingdom, Japan and Italy will pool resources to build a sixth-generation warplane scheduled to be ready for deployment by 2035, with capabilities to rival never-before-seen tech on fighter jets built by China and Russia, although this wasn't stated explicitly.... [...]
