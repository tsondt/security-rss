Title: Google introduces end-to-end encryption for Gmail on the web
Date: 2022-12-17T09:15:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-12-17-google-introduces-end-to-end-encryption-for-gmail-on-the-web

[Source](https://www.bleepingcomputer.com/news/security/google-introduces-end-to-end-encryption-for-gmail-on-the-web/){:target="_blank" rel="noopener"}

> Google announced on Friday that it's adding end-to-end encryption to Gmail on the web, allowing enrolled Google Workspace users to send and receive encrypted emails within their domain and outside their domain. [...]
