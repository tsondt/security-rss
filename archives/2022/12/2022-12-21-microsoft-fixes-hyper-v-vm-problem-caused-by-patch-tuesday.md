Title: Microsoft fixes Hyper-V VM problem caused by Patch Tuesday
Date: 2022-12-21T19:30:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-12-21-microsoft-fixes-hyper-v-vm-problem-caused-by-patch-tuesday

[Source](https://go.theregister.com/feed/www.theregister.com/2022/12/21/microsoft_fix_hyperv_patch/){:target="_blank" rel="noopener"}

> The emergency OOB release should solve those frustrating failures Microsoft has pushed out an emergency fix for a problem in Windows Server caused by patch updates that made it impossible for some organizations to create virtual machines on Hyper-V hosts.... [...]
