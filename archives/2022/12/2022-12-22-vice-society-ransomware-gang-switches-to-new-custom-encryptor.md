Title: Vice Society ransomware gang switches to new custom encryptor
Date: 2022-12-22T12:25:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-22-vice-society-ransomware-gang-switches-to-new-custom-encryptor

[Source](https://www.bleepingcomputer.com/news/security/vice-society-ransomware-gang-switches-to-new-custom-encryptor/){:target="_blank" rel="noopener"}

> The Vice Society ransomware operation has switched to using a custom ransomware encrypt that implements a strong, hybrid encryption scheme based on NTRUEncrypt and ChaCha20-Poly1305. [...]
