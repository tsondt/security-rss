Title: Automated dark web markets sell corporate email accounts for $2
Date: 2022-12-08T11:22:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-08-automated-dark-web-markets-sell-corporate-email-accounts-for-2

[Source](https://www.bleepingcomputer.com/news/security/automated-dark-web-markets-sell-corporate-email-accounts-for-2/){:target="_blank" rel="noopener"}

> Cybercrime marketplaces are increasingly selling stolen corporate email addresses for as low as $2 to fill a growing demand by hackers who use them for business email compromise and phishing attacks or initial access to networks. [...]
