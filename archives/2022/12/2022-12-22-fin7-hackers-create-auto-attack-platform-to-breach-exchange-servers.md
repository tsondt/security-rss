Title: FIN7 hackers create auto-attack platform to breach Exchange servers
Date: 2022-12-22T08:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-12-22-fin7-hackers-create-auto-attack-platform-to-breach-exchange-servers

[Source](https://www.bleepingcomputer.com/news/security/fin7-hackers-create-auto-attack-platform-to-breach-exchange-servers/){:target="_blank" rel="noopener"}

> The notorious FIN7 hacking group uses an auto-attack system that exploits Microsoft Exchange and SQL injection vulnerabilities to breach corporate networks, steal data, and select targets for ransomware attacks based on financial size. [...]
