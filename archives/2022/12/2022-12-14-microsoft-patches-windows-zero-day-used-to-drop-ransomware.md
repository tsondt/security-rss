Title: Microsoft patches Windows zero-day used to drop ransomware
Date: 2022-12-14T13:24:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-12-14-microsoft-patches-windows-zero-day-used-to-drop-ransomware

[Source](https://www.bleepingcomputer.com/news/security/microsoft-patches-windows-zero-day-used-to-drop-ransomware/){:target="_blank" rel="noopener"}

> Microsoft has fixed a security vulnerability used by threat actors to circumvent the Windows SmartScreen security feature and deliver Magniber ransomware and Qbot malware payloads. [...]
