Title: It Might Be Our Data, But It’s Not Our Breach
Date: 2022-08-11T17:45:31+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;The Coming Storm;alex holden;AT&T;AT&T Internet;bellsouth.net;Bleeping Computer;Databreaches.net;Hold Security;Lawrence Abrams;New Jersey Cybersecurity & Communications Integration Cell;sbcglobal.net;ShinyHunters;T-Mobile;U-verse;White House Market
Slug: 2022-08-11-it-might-be-our-data-but-its-not-our-breach

[Source](https://krebsonsecurity.com/2022/08/it-might-be-our-data-but-its-not-our-breach/){:target="_blank" rel="noopener"}

> Image: Shutterstock. A cybersecurity firm says it has intercepted a large, unique stolen data set containing the names, addresses, email addresses, phone numbers, Social Security Numbers and dates of birth on nearly 23 million Americans. The firm’s analysis of the data suggests it corresponds to current and former customers of AT&T. The telecommunications giant stopped short of saying the data wasn’t theirs, but it maintains the records do not appear to have come from its systems and may be tied to a previous data incident at another company. Milwaukee-based cybersecurity consultancy Hold Security said it intercepted a 1.6 gigabyte compressed [...]
