Title: We can make our phones harder to hack but complete security is a pipe dream | John Naughton
Date: 2022-08-21T08:30:04+00:00
Author: John Naughton
Category: The Guardian
Tags: Cybercrime;Internet of things;Apple;Data and computer security;Smart homes;Technology;Smartphones;Smartwatches
Slug: 2022-08-21-we-can-make-our-phones-harder-to-hack-but-complete-security-is-a-pipe-dream-john-naughton

[Source](https://www.theguardian.com/commentisfree/2022/aug/21/watch-out-smart-doorbell-open-invitation-cyber-attackers){:target="_blank" rel="noopener"}

> Even the latest iPhone scare won’t persuade us to choose safety over convenience Apple caused a stir a few weeks ago when it announced that the forthcoming update of its mobile and laptop operating systems would contain an optional high-security mode that would provide users with an unprecedented level of protection against powerful “spyware” software that surreptitiously obtains control of their devices. It’s called Lockdown Mode and, according to Apple, “offers an extreme, optional level of security for the very few users who, because of who they are or what they do, may be personally targeted by some of the [...]
