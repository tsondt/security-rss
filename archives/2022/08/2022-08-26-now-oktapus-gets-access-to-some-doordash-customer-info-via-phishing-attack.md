Title: Now Oktapus gets access to some DoorDash customer info via phishing attack
Date: 2022-08-26T16:33:28+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-26-now-oktapus-gets-access-to-some-doordash-customer-info-via-phishing-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/26/doordash_oktapus_phishing/){:target="_blank" rel="noopener"}

> Double check who exactly you're sending your username and password to, eh? DoorDash has confirmed that "a small percentage" of its customers and delivery drivers' information, including names, email and delivery addresses, phone numbers, and order and partial credit card details, were exposed as part of a broad phishing campaign dubbed Oktapus.... [...]
