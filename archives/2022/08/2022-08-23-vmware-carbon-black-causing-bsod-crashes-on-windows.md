Title: VMware Carbon Black causing BSOD crashes on Windows
Date: 2022-08-23T17:42:16-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-23-vmware-carbon-black-causing-bsod-crashes-on-windows

[Source](https://www.bleepingcomputer.com/news/security/vmware-carbon-black-causing-bsod-crashes-on-windows/){:target="_blank" rel="noopener"}

> Windows servers and workstations at dozens of organizations started to crash earlier today because of an issue caused by certain versions of VMware's Carbon Black endpoint security solution. [...]
