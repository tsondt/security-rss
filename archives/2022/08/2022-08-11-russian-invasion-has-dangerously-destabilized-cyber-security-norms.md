Title: Russian invasion has dangerously destabilized cyber security norms
Date: 2022-08-11T21:30:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-11-russian-invasion-has-dangerously-destabilized-cyber-security-norms

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/black_hat_hacktivists/){:target="_blank" rel="noopener"}

> The inside scoop on the Ukrainian IT army, and what could happen next Black Hat The hacktivist attacks that have occurred during the ongoing war in Ukraine are setting a dangerous precedent for cyber norms — and infrastructure security, according to journalist and author Kim Zetter.... [...]
