Title: VMWare Urges Users to Patch Critical Authentication Bypass Bug
Date: 2022-08-03T15:23:16+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-08-03-vmware-urges-users-to-patch-critical-authentication-bypass-bug

[Source](https://threatpost.com/vmware-patch-critical-bug/180346/){:target="_blank" rel="noopener"}

> Vulnerability—for which a proof-of-concept is forthcoming—is one of a string of flaws the company fixed that could lead to an attack chain. [...]
