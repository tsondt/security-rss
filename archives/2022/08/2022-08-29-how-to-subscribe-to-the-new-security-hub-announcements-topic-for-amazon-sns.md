Title: How to subscribe to the new Security Hub Announcements topic for Amazon SNS
Date: 2022-08-29T20:51:35+00:00
Author: Mike Saintcross
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS Security Hub;notifications;Security;Security Blog;SNS
Slug: 2022-08-29-how-to-subscribe-to-the-new-security-hub-announcements-topic-for-amazon-sns

[Source](https://aws.amazon.com/blogs/security/how-to-subscribe-to-the-new-security-hub-announcements-topic-for-amazon-sns/){:target="_blank" rel="noopener"}

> With AWS Security Hub you are able to manage your security posture in AWS, perform security best practice checks, aggregate alerts, and automate remediation. Now you are able to use Amazon Simple Notification Service (Amazon SNS) to subscribe to the new Security Hub Announcements topic to receive updates about new Security Hub services and features, [...]
