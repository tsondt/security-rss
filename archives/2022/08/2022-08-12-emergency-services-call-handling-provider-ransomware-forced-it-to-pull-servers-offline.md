Title: Emergency services call-handling provider: Ransomware forced it to pull servers offline
Date: 2022-08-12T13:06:23+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-08-12-emergency-services-call-handling-provider-ransomware-forced-it-to-pull-servers-offline

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/12/advanced_confirms_ransomware_forced_it/){:target="_blank" rel="noopener"}

> Advanced's infrastructure still down and out, recovery to take weeks or more Advanced, the MSP forced to shut down some of its servers last week after identifying an "issue" with its infrastructure hosting products, has confirmed a ransomware attack and says recovery will be in the order of weeks.... [...]
