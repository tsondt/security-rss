Title: Xiaomi phones with MediaTek chips vulnerable to forged payments
Date: 2022-08-12T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-08-12-xiaomi-phones-with-mediatek-chips-vulnerable-to-forged-payments

[Source](https://www.bleepingcomputer.com/news/security/xiaomi-phones-with-mediatek-chips-vulnerable-to-forged-payments/){:target="_blank" rel="noopener"}

> Security analysts have found weaknesses in the implementation of the trusted execution environment (TEE) in MediaTek-powered Xiaomi smartphones, which could enable third-party unprivileged apps to disable the payment system or forge payments. [...]
