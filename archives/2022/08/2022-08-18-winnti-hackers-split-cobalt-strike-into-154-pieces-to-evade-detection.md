Title: Winnti hackers split Cobalt Strike into 154 pieces to evade detection
Date: 2022-08-18T11:48:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-18-winnti-hackers-split-cobalt-strike-into-154-pieces-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/winnti-hackers-split-cobalt-strike-into-154-pieces-to-evade-detection/){:target="_blank" rel="noopener"}

> ​The Chinese Winnti hacking group, also known as 'APT41' or 'Wicked Spider,' targeted at least 80 organizations last year and successfully breached the networks of at least thirteen. [...]
