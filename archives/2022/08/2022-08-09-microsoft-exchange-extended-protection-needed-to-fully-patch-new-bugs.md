Title: Microsoft: Exchange ‘Extended Protection’ needed to fully patch new bugs
Date: 2022-08-09T17:14:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-08-09-microsoft-exchange-extended-protection-needed-to-fully-patch-new-bugs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-exchange-extended-protection-needed-to-fully-patch-new-bugs/){:target="_blank" rel="noopener"}

> Microsoft says that some of the Exchange Server flaws addressed as part of the August 2022 Patch Tuesday also require admins to manually enable Extended Protection on affected servers to fully block attacks. [...]
