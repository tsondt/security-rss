Title: Twilio customer data exposed after its staffers got phished
Date: 2022-08-08T17:45:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-08-twilio-customer-data-exposed-after-its-staffers-got-phished

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/08/twilio_phishing_attack/){:target="_blank" rel="noopener"}

> Comms giant says several other firms targeted in 'sophisticated attack' Twilio confirmed a breach of the communication giant's network and accessed "a limited number" of customer accounts after tricking some employees into falling for a phishing attack.... [...]
