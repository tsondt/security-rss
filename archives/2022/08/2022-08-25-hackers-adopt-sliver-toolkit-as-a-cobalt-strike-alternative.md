Title: Hackers adopt Sliver toolkit as a Cobalt Strike alternative
Date: 2022-08-25T08:28:53-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-25-hackers-adopt-sliver-toolkit-as-a-cobalt-strike-alternative

[Source](https://www.bleepingcomputer.com/news/security/hackers-adopt-sliver-toolkit-as-a-cobalt-strike-alternative/){:target="_blank" rel="noopener"}

> Threat actors are dumping the Cobalt Strike penetration testing suite in favor of similar frameworks that are less known. After Brute Ratel, the open-source, cross-platform kit called Sliver is becoming an attractive alternative. [...]
