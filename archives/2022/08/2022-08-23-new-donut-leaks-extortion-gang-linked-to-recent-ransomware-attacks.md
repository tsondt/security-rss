Title: New 'Donut Leaks' extortion gang linked to recent ransomware attacks
Date: 2022-08-23T11:06:31-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-23-new-donut-leaks-extortion-gang-linked-to-recent-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-donut-leaks-extortion-gang-linked-to-recent-ransomware-attacks/){:target="_blank" rel="noopener"}

> A new data extortion group named 'Donut Leaks' is linked to recent cyberattacks, including those on Greek natural gas company DESFA, UK architectural firm Sheppard Robson, and multinational construction company Sando. [...]
