Title: How to export AWS Security Hub findings to CSV format
Date: 2022-08-23T17:48:08+00:00
Author: Andy Robinson
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon EventBridge;Amazon S3;AWS Lambda;AWS Security Hub;Security;Security Blog
Slug: 2022-08-23-how-to-export-aws-security-hub-findings-to-csv-format

[Source](https://aws.amazon.com/blogs/security/how-to-export-aws-security-hub-findings-to-csv-format/){:target="_blank" rel="noopener"}

> AWS Security Hub is a central dashboard for security, risk management, and compliance findings from AWS Audit Manager, AWS Firewall Manager, Amazon GuardDuty, IAM Access Analyzer, Amazon Inspector, and many other AWS and third-party services. You can use the insights from Security Hub to get an understanding of your compliance posture across multiple AWS accounts. [...]
