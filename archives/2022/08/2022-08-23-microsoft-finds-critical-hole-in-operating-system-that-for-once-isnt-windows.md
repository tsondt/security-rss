Title: Microsoft finds critical hole in operating system that for once isn't Windows
Date: 2022-08-23T00:58:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-23-microsoft-finds-critical-hole-in-operating-system-that-for-once-isnt-windows

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/23/microsoft_chromeos_bug/){:target="_blank" rel="noopener"}

> Oh wow, get a load of Google using strcpy() all wrong – strcpy! Haha, you'll never ever catch us doing that Microsoft has described a severe ChromeOS security vulnerability that one of its researchers reported to Google in late April.... [...]
