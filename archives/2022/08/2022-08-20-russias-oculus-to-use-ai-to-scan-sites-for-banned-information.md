Title: Russia's 'Oculus' to use AI to scan sites for banned information
Date: 2022-08-20T10:07:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-20-russias-oculus-to-use-ai-to-scan-sites-for-banned-information

[Source](https://www.bleepingcomputer.com/news/security/russias-oculus-to-use-ai-to-scan-sites-for-banned-information/){:target="_blank" rel="noopener"}

> Russia's internet watchdog Roskomnadzor is developing a neural network that will use artificial intelligence to scan websites for prohibited information. [...]
