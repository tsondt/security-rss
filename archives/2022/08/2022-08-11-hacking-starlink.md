Title: Hacking Starlink
Date: 2022-08-11T13:23:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-08-11-hacking-starlink

[Source](https://www.schneier.com/blog/archives/2022/08/hacking-starlink.html){:target="_blank" rel="noopener"}

> This is the first —of many, I assume—hack of Starlink. Leveraging a string of vulnerabilities, attackers can access the Starlink system and run custom code on the devices. [...]
