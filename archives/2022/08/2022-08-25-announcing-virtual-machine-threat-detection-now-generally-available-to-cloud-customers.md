Title: Announcing Virtual Machine Threat Detection now generally available to Cloud customers
Date: 2022-08-25T16:00:00+00:00
Author: Timothy Peacock
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-08-25-announcing-virtual-machine-threat-detection-now-generally-available-to-cloud-customers

[Source](https://cloud.google.com/blog/products/identity-security/introducing-virtual-machine-threat-detection-to-block-critical-threats/){:target="_blank" rel="noopener"}

> Today we are delighted to announce that our unique, first-to-market detection capability with Virtual Machine Threat Detection (VMTD) in Security Command Center is now generally available for all Google Cloud customers. We launched this service six months ago in public preview and have seen a lot of enthusiasm from our customers. We’ve seen adoption from users around the world and in every industry. For years, we have said security must be engineered in, not bolted on. By baking this capability into our virtualization stack we are living up to our promise of delivering invisible security. Our team has been busy [...]
