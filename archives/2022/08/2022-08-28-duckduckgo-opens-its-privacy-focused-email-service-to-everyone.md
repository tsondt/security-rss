Title: DuckDuckGo opens its privacy-focused email service to everyone
Date: 2022-08-28T10:06:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-28-duckduckgo-opens-its-privacy-focused-email-service-to-everyone

[Source](https://www.bleepingcomputer.com/news/security/duckduckgo-opens-its-privacy-focused-email-service-to-everyone/){:target="_blank" rel="noopener"}

> DuckDuckGo has opened its 'Email Protection' service to anyone wishing to get their own '@duck.com' email address. [...]
