Title: Google Workspace and Google Cloud help build the future of work at Airbus
Date: 2022-08-09T16:00:00+00:00
Author: Christine Koch
Category: GCP Security
Tags: Identity & Security;Google Workspace
Slug: 2022-08-09-google-workspace-and-google-cloud-help-build-the-future-of-work-at-airbus

[Source](https://cloud.google.com/blog/products/workspace/how-google-workspace-helps-airbus-fuel-secure-collaboration/){:target="_blank" rel="noopener"}

> “Any device, anytime, anywhere.” A cohort of CIOs within Airbus believed that the cloud, combined with new ways of working, could provide the foundation for this vision. Google Workspace and Google Cloud have played a pivotal role in helping Airbus realize this new path, transforming security, data management, and collaboration along the way. The Airbus family in flight Adopting a secure-by-design approach In adopting Google Workspace and Google Cloud Airbus needed to ensure a robust, zero-trust security model that works across the entire organization, even when employees are working outside the office. Google Workspace provides a single login that enables [...]
