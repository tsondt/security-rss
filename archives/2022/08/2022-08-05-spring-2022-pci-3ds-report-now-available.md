Title: Spring 2022 PCI 3DS report now available
Date: 2022-08-05T16:57:13+00:00
Author: Michael Oyeniya
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;Compliance reports;PCI;Security Blog
Slug: 2022-08-05-spring-2022-pci-3ds-report-now-available

[Source](https://aws.amazon.com/blogs/security/spring-2022-pci-3ds-report-now-available/){:target="_blank" rel="noopener"}

> We are excited to announce that Amazon Web Services (AWS) has released the latest 2022 Payment Card Industry 3-D Secure (PCI 3DS) attestation to support our customers in the financial services sector. Although AWS doesn’t perform 3DS functions directly, the AWS PCI 3DS attestation of compliance can help customers to attain their own PCI 3DS [...]
