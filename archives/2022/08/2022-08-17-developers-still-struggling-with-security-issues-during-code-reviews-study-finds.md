Title: Developers still struggling with security issues during code reviews, study finds
Date: 2022-08-17T10:46:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-17-developers-still-struggling-with-security-issues-during-code-reviews-study-finds

[Source](https://portswigger.net/daily-swig/developers-still-struggling-with-security-issues-during-code-reviews-study-finds){:target="_blank" rel="noopener"}

> The road to DevSecOps isn’t always the smoothest [...]
