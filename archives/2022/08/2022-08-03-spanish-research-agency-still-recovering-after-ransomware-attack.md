Title: Spanish research agency still recovering after ransomware attack
Date: 2022-08-03T16:50:05-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-03-spanish-research-agency-still-recovering-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/spanish-research-agency-still-recovering-after-ransomware-attack/){:target="_blank" rel="noopener"}

> The Spanish National Research Council (CSIC) last month was hit by a ransomware attack that is now attributed to Russian hackers. [...]
