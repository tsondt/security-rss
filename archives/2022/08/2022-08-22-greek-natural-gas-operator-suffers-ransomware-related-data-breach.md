Title: Greek natural gas operator suffers ransomware-related data breach
Date: 2022-08-22T11:35:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-22-greek-natural-gas-operator-suffers-ransomware-related-data-breach

[Source](https://www.bleepingcomputer.com/news/security/greek-natural-gas-operator-suffers-ransomware-related-data-breach/){:target="_blank" rel="noopener"}

> Greece's largest natural gas distributor DESFA confirmed on Saturday that they suffered a limited scope data breach and IT system outage following a cyberattack. [...]
