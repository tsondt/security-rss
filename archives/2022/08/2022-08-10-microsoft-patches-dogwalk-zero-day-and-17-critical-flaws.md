Title: Microsoft Patches ‘Dogwalk’ Zero-Day and 17 Critical Flaws
Date: 2022-08-10T12:48:05+00:00
Author: Threatpost
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-08-10-microsoft-patches-dogwalk-zero-day-and-17-critical-flaws

[Source](https://threatpost.com/microsoft-patches-dogwalk-zero-day-and-17-critical-flaws/180378/){:target="_blank" rel="noopener"}

> August Patch Tuesday tackles 121 CVEs, 17 critical bugs and one zero-day bug exploited in the wild. [...]
