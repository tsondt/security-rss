Title: Log4Shell legacy? Patching times plummet for most critical vulnerabilities – report
Date: 2022-08-30T15:13:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-30-log4shell-legacy-patching-times-plummet-for-most-critical-vulnerabilities-report

[Source](https://portswigger.net/daily-swig/log4shell-legacy-patching-times-plummet-for-most-critical-vulnerabilities-report){:target="_blank" rel="noopener"}

> Trustwave report also finds 2022 is set to surpass 2021 for volume of critical CVEs [...]
