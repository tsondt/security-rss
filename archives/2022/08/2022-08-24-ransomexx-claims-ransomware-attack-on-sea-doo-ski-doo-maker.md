Title: RansomEXX claims ransomware attack on Sea-Doo, Ski-Doo maker
Date: 2022-08-24T12:36:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-24-ransomexx-claims-ransomware-attack-on-sea-doo-ski-doo-maker

[Source](https://www.bleepingcomputer.com/news/security/ransomexx-claims-ransomware-attack-on-sea-doo-ski-doo-maker/){:target="_blank" rel="noopener"}

> The RansomEXX ransomware gang is claiming responsibility for the cyberattack against Bombardier Recreational Products (BRP), disclosed by the company on August 8, 2022. [...]
