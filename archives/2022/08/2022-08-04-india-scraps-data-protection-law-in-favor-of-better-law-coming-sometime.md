Title: India scraps data protection law in favor of better law coming … sometime
Date: 2022-08-04T06:58:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-08-04-india-scraps-data-protection-law-in-favor-of-better-law-coming-sometime

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/04/india_scraps_data_protection_law/){:target="_blank" rel="noopener"}

> Tech giants and digital rights groups didn't like it, but at least it was a law The government of India has scrapped the Personal Data Protection Bill it's worked on for three years, and announced it will – eventually – unveil a superior bill.... [...]
