Title: Drone Deliveries into Prisons
Date: 2022-08-03T11:50:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;crime;drones;prisons
Slug: 2022-08-03-drone-deliveries-into-prisons

[Source](https://www.schneier.com/blog/archives/2022/08/drone-deliveries-into-prisons.html){:target="_blank" rel="noopener"}

> Seems it’s now common to sneak contraband into prisons with a drone. [...]
