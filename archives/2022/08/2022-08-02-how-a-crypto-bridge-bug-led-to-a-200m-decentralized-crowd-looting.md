Title: How a crypto bridge bug led to a $200m 'decentralized crowd looting'
Date: 2022-08-02T23:34:56+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-02-how-a-crypto-bridge-bug-led-to-a-200m-decentralized-crowd-looting

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/02/flash_mob_robs_nomad_crypto/){:target="_blank" rel="noopener"}

> Flash mob exploits Nomad's validation code blunder Cryptocurrency bridge service Nomad, which describes itself as "an optimistic interoperability protocol that enables secure cross-chain communication," has been drained of tokens notionally worth $190.7 million if exchanged for US dollars.... [...]
