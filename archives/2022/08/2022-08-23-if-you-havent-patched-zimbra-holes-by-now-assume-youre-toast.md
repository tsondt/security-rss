Title: If you haven't patched Zimbra holes by now, assume you're toast
Date: 2022-08-23T00:32:31+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-23-if-you-havent-patched-zimbra-holes-by-now-assume-youre-toast

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/23/cisa_zimbra_signatures/){:target="_blank" rel="noopener"}

> Here's how to detect an intrusion via vulnerable email systems Organizations that didn't immediately patch their Zimbra email systems should assume miscreants have already found and exploited the bugs, and should start hunting for malicious activity across IT networks, according to Uncle Sam.... [...]
