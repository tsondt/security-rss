Title: It's 2022 and there are still thousands of public systems using password-less VNC
Date: 2022-08-16T02:36:42+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-16-its-2022-and-there-are-still-thousands-of-public-systems-using-password-less-vnc

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/16/vnc_cyble_endpoints/){:target="_blank" rel="noopener"}

> Let alone the ones with 123456 to login. How sophisticated do attackers really need to be? Thousands of machines on the public internet can be remotely controlled via VNC without any authentication, a cybersecurity vendor has reminded us this month.... [...]
