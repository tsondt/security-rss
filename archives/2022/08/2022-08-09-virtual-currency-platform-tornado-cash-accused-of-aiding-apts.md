Title: Virtual Currency Platform ‘Tornado Cash’ Accused of Aiding APTs
Date: 2022-08-09T17:58:46+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks
Slug: 2022-08-09-virtual-currency-platform-tornado-cash-accused-of-aiding-apts

[Source](https://threatpost.com/virtual-currency-platform-tornado-cash-accused-of-aiding-apts/180367/){:target="_blank" rel="noopener"}

> U.S. Treasury blocked the business of the virtual currency mixer for laundering more than $7 billion for hackers, including $455 million to help fund North Korea’s missile program. [...]
