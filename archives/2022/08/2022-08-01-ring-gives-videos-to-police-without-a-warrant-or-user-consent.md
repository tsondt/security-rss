Title: Ring Gives Videos to Police without a Warrant or User Consent
Date: 2022-08-01T11:09:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Amazon;cameras;police;privacy;surveillance
Slug: 2022-08-01-ring-gives-videos-to-police-without-a-warrant-or-user-consent

[Source](https://www.schneier.com/blog/archives/2022/08/ring-gives-videos-to-police-without-a-warrant-or-user-consent.html){:target="_blank" rel="noopener"}

> Amazon has revealed that it gives police videos from its Ring doorbells without a warrant and without user consent. Ring recently revealed how often the answer to that question has been yes. The Amazon company responded to an inquiry from US Senator Ed Markey (D-Mass.), confirming that there have been 11 cases in 2022 where Ring complied with police “emergency” requests. In each case, Ring handed over private recordings, including video and audio, without letting users know that police had access to—and potentially downloaded—their data. This raises many concerns about increased police reliance on private surveillance, a practice that has [...]
