Title: Boffins rate npm and PyPI package security and it's not good
Date: 2022-08-11T00:54:25+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-11-boffins-rate-npm-and-pypi-package-security-and-its-not-good

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/npm_pypi_security/){:target="_blank" rel="noopener"}

> Guess what? Open source security still has gaps The Open Source Security Foundation (OpenSSF), as its name plainly states, aims to help make open source software more secure, but improvements flowing from its efforts are hard to find.... [...]
