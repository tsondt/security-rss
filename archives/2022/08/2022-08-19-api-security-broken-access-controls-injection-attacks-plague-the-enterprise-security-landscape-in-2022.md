Title: API security: Broken access controls, injection attacks plague the enterprise security landscape in 2022
Date: 2022-08-19T12:16:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-19-api-security-broken-access-controls-injection-attacks-plague-the-enterprise-security-landscape-in-2022

[Source](https://portswigger.net/daily-swig/api-security-broken-access-controls-injection-attacks-plague-the-enterprise-security-landscape-in-2022){:target="_blank" rel="noopener"}

> Spring4Shell and Veeam RCE exploit topped the list in Q1 2022 [...]
