Title: Microsoft widens enterprise access to its threat intelligence pool
Date: 2022-08-03T21:31:46+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-03-microsoft-widens-enterprise-access-to-its-threat-intelligence-pool

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/03/microsoft_defender_attack_surface/){:target="_blank" rel="noopener"}

> Organizations can be more proactive in tracking threats, finding holes in their protection Microsoft says it will give enterprise security operation centers (SOCs) broader access to the massive amount of threat intelligence it collects every day.... [...]
