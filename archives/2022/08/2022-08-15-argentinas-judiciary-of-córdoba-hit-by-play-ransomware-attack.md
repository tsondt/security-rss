Title: Argentina's Judiciary of Córdoba hit by PLAY ransomware attack
Date: 2022-08-15T20:06:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-15-argentinas-judiciary-of-córdoba-hit-by-play-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/argentinas-judiciary-of-c-rdoba-hit-by-play-ransomware-attack/){:target="_blank" rel="noopener"}

> Argentina's Judiciary of Córdoba has shut down its IT systems after suffering a ransomware attack, reportedly at the hands of the new 'Play' ransomware operation. [...]
