Title: Join us for a show-and-tell edition of Google Cloud Security Talks
Date: 2022-08-16T16:00:00+00:00
Author: Kristen Cooper
Category: GCP Security
Tags: Events;Google Cloud;Identity & Security
Slug: 2022-08-16-join-us-for-a-show-and-tell-edition-of-google-cloud-security-talks

[Source](https://cloud.google.com/blog/products/identity-security/join-us-for-google-cloud-security-talks/){:target="_blank" rel="noopener"}

> If you’re new to Security Talks, you should know that this program is part of an ongoing series where we bring together experts from the Google Cloud security team, including the Google Cybersecurity Action Team and Office of the CISO, and the greater industry to share information on our latest security products, innovations, and best practices. The Q3 installment of the Google Cloud Security Talks on Aug. 31 is a special show-and-tell edition. We’re not just going to share what you need to know about our portfolio of products, we’re also going to be showing you how to use them [...]
