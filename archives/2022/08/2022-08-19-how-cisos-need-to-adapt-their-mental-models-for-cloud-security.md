Title: How CISOs need to adapt their mental models for cloud security
Date: 2022-08-19T16:00:00+00:00
Author: Seth Rosenblatt
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-08-19-how-cisos-need-to-adapt-their-mental-models-for-cloud-security

[Source](https://cloud.google.com/blog/products/identity-security/why-cisos-need-to-adapt-their-mental-models-of-security-for-cloud/){:target="_blank" rel="noopener"}

> Many security leaders head into the cloud armed mostly with tools, practices, skills and ultimately the mental models for how security works that were developed on premise. This leads to cost and efficiency problems that can be solved by mapping their existing mental models to those of the cloud. When it comes to understanding the differences between on-premises cybersecurity mental models and their cloud cybersecurity counterparts, a helpful place to start is by looking at the kinds of threats each one is attempting to block, detect, or investigate. Traditional on-premise threats focused on stealing data from databases, file storage, and [...]
