Title: Over 9,000 VNC servers exposed online without a password
Date: 2022-08-14T10:12:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-14-over-9000-vnc-servers-exposed-online-without-a-password

[Source](https://www.bleepingcomputer.com/news/security/over-9-000-vnc-servers-exposed-online-without-a-password/){:target="_blank" rel="noopener"}

> Researchers have discovered at least 9,000 exposed VNC (virtual network computing) endpoints that can be accessed and used without authentication, allowing threat actors easy access to internal networks. [...]
