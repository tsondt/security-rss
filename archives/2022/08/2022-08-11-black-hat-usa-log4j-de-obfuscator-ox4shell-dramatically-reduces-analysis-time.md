Title: Black Hat USA: Log4j de-obfuscator Ox4Shell ‘dramatically’ reduces analysis time
Date: 2022-08-11T10:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-11-black-hat-usa-log4j-de-obfuscator-ox4shell-dramatically-reduces-analysis-time

[Source](https://portswigger.net/daily-swig/black-hat-usa-log4j-de-obfuscator-ox4shell-dramatically-reduces-analysis-time){:target="_blank" rel="noopener"}

> Open source utility exposes payloads without running vulnerable Java code [...]
