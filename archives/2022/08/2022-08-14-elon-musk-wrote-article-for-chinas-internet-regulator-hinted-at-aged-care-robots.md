Title: Elon Musk wrote article for China's internet regulator, hinted at aged care robots
Date: 2022-08-14T23:45:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-08-14-elon-musk-wrote-article-for-chinas-internet-regulator-hinted-at-aged-care-robots

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/14/elon_musk_hints_at_aged/){:target="_blank" rel="noopener"}

> PLUS Vietnam's massive infosec push; Philippines telco fight; Australia dumps COVID app; and more Asia in Brief Elon Musk has written an article for the Cyberspace Administration of China's flagship magazine.... [...]
