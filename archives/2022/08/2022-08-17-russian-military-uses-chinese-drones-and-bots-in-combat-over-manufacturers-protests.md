Title: Russian military uses Chinese drones and bots in combat, over manufacturers' protests
Date: 2022-08-17T05:30:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-08-17-russian-military-uses-chinese-drones-and-bots-in-combat-over-manufacturers-protests

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/17/russia_weaponizes_chinese_drones_robots/){:target="_blank" rel="noopener"}

> Testimonials from Russian generals not welcomed by DJI or Unitree Robotics Russia's military has praised civilian grade Chinese-made drones and robots for having performed well on the battlefield, leading their manufacturers to point out the equipment is not intended or sold for military purposes.... [...]
