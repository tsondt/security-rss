Title: Sonatype shines light on typosquatting ransomware threat in PyPI
Date: 2022-08-03T17:15:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-08-03-sonatype-shines-light-on-typosquatting-ransomware-threat-in-pypi

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/03/sonatype_typosquatting/){:target="_blank" rel="noopener"}

> It's all fun and games until somebody gets their files encrypted Miscreants making use of typosquatting are being spotted by researchers at Sonatype, emphasizing the need to check that the package is really the one you meant to download.... [...]
