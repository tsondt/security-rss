Title: Over 3,200 apps leak Twitter API keys, some allowing account hijacks
Date: 2022-08-01T18:33:54-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-08-01-over-3200-apps-leak-twitter-api-keys-some-allowing-account-hijacks

[Source](https://www.bleepingcomputer.com/news/security/over-3-200-apps-leak-twitter-api-keys-some-allowing-account-hijacks/){:target="_blank" rel="noopener"}

> Cybersecurity researchers have uncovered a set of 3,207 mobile apps that are exposing Twitter API keys to the public, potentially enabling a threat actor to take over users' Twitter accounts that are associated with the app. [...]
