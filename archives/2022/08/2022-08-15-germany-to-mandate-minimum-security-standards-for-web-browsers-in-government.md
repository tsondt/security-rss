Title: Germany to mandate minimum security standards for web browsers in government
Date: 2022-08-15T14:18:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-15-germany-to-mandate-minimum-security-standards-for-web-browsers-in-government

[Source](https://portswigger.net/daily-swig/germany-to-mandate-minimum-security-standards-for-web-browsers-in-government){:target="_blank" rel="noopener"}

> Less celebrated browsers and deprecated applications like Internet Explorer will be browsers non-grata [...]
