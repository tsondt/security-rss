Title: VMware patches critical 'make me admin' auth bypass bug, plus nine other flaws
Date: 2022-08-03T00:26:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-03-vmware-patches-critical-make-me-admin-auth-bypass-bug-plus-nine-other-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/03/vmware_critical_authentication_bypass/){:target="_blank" rel="noopener"}

> Meanwhile, a security update for rsync VMware has fixed a critical authentication bypass vulnerability that hits 9.8 out of 10 on the CVSS severity scale and is present in multiple products.... [...]
