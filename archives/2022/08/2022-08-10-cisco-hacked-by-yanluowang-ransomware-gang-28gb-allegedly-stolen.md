Title: Cisco hacked by Yanluowang ransomware gang, 2.8GB allegedly stolen
Date: 2022-08-10T16:05:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-10-cisco-hacked-by-yanluowang-ransomware-gang-28gb-allegedly-stolen

[Source](https://www.bleepingcomputer.com/news/security/cisco-hacked-by-yanluowang-ransomware-gang-28gb-allegedly-stolen/){:target="_blank" rel="noopener"}

> Cisco confirmed today that the Yanluowang ransomware group breached its corporate network in late May and that the actor tried to extort them under the threat of leaking stolen files online. [...]
