Title: ReNgine upgrade: New subscan feature, PDF reports, expanded toolbox showcased at Black Hat USA
Date: 2022-08-11T15:35:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-11-rengine-upgrade-new-subscan-feature-pdf-reports-expanded-toolbox-showcased-at-black-hat-usa

[Source](https://portswigger.net/daily-swig/rengine-upgrade-new-subscan-feature-pdf-reports-expanded-toolbox-showcased-at-black-hat-usa){:target="_blank" rel="noopener"}

> Open source recon tool automates some of the more time-consuming pen testing tasks [...]
