Title: CIA accused of illegally spying on Americans visiting Assange in embassy
Date: 2022-08-15T19:37:02+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-15-cia-accused-of-illegally-spying-on-americans-visiting-assange-in-embassy

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/15/assange_cia_spyware/){:target="_blank" rel="noopener"}

> Lawyers, journalists sue super-snoop agency and Spanish security biz The CIA illegally spied on US citizens while they visited WikiLeaks publisher Julian Assange inside the Ecuadorian embassy in London, a lawsuit filed today has claimed.... [...]
