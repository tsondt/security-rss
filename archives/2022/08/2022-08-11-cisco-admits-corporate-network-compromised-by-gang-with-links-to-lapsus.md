Title: Cisco admits corporate network compromised by gang with links to Lapsus$
Date: 2022-08-11T05:59:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-08-11-cisco-admits-corporate-network-compromised-by-gang-with-links-to-lapsus

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/cisco_corporate_network_compromised/){:target="_blank" rel="noopener"}

> Voice-phished their way in, but Switchzilla claims no damage done Cisco disclosed on Wednesday that its corporate network was accessed by cyber-criminals in May after an employee's personal Google account was compromised – an act a ransomware gang named "Yanluowang" has now claimed as its work.... [...]
