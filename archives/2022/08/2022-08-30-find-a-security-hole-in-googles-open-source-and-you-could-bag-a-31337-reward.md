Title: Find a security hole in Google's open source and you could bag a $31,337 reward
Date: 2022-08-30T22:58:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-30-find-a-security-hole-in-googles-open-source-and-you-could-bag-a-31337-reward

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/30/google_open_source_bug_bounty/){:target="_blank" rel="noopener"}

> Will it be enough to prevent the next software supply-chain attack? Google has created a bug bounty program that will reward those who find and report vulnerabilities in its open-source projects, thereby hopefully strengthening software supply-chain security.... [...]
