Title: Trio of XSS bugs in open source web apps could lead to complete system compromise
Date: 2022-08-02T15:19:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-02-trio-of-xss-bugs-in-open-source-web-apps-could-lead-to-complete-system-compromise

[Source](https://portswigger.net/daily-swig/trio-of-xss-bugs-in-open-source-web-apps-could-lead-to-complete-system-compromise){:target="_blank" rel="noopener"}

> Evolution CMS, FUDForum, and GitBucket vulnerabilities chained for maximum impact [...]
