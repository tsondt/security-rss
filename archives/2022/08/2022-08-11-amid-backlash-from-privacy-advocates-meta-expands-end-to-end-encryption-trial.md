Title: Amid backlash from privacy advocates, Meta expands end-to-end encryption trial
Date: 2022-08-11T17:46:59+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;encryption;meta;privacy
Slug: 2022-08-11-amid-backlash-from-privacy-advocates-meta-expands-end-to-end-encryption-trial

[Source](https://arstechnica.com/?p=1873181){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Meta is ever so slowly expanding its trial of end-to-end encryption in a bid to protect users from snoops and law enforcement. End-to-end encryption, often abbreviated as E2EE, uses strong cryptography to encrypt messages with a key that is unique to each user. Because the key is in the sole possession of each user, E2EE prevents everyone else—including the app maker, ISP or carrier, and three-letter agencies—from reading a message. Meta first rolled out E2EE in 2016 in its WhatsApp and Messenger apps, with the former providing it by default and the latter offering it as [...]
