Title: Announcing curated detections in Chronicle SecOps Suite
Date: 2022-08-17T16:00:00+00:00
Author: Rick Correa
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-08-17-announcing-curated-detections-in-chronicle-secops-suite

[Source](https://cloud.google.com/blog/products/identity-security/introducing-curated-detections-in-chronicle-secops-suite/){:target="_blank" rel="noopener"}

> A critical component of any security operations team’s job is to deliver high-fidelity detections of potential threats across the breadth of adversary tactics. But increasingly sophisticated threat actors, an expanding attack surface, and an ever-present cybersecurity talent shortage make this task more challenging than ever. Google keeps more people safe online than anyone else. Individuals, businesses and governments globally depend on our products that are secure-by-design and secure-by-default. Part of the “magic” behind Google’s security is the sheer scale of threat intelligence we are able to derive from our billions of users, browsers, and devices. Today, we are putting the [...]
