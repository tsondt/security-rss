Title: Windows KB5012170 Secure Boot DBX update may fail with 0x800f0922 error
Date: 2022-08-15T11:41:56-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-08-15-windows-kb5012170-secure-boot-dbx-update-may-fail-with-0x800f0922-error

[Source](https://www.bleepingcomputer.com/news/security/windows-kb5012170-secure-boot-dbx-update-may-fail-with-0x800f0922-error/){:target="_blank" rel="noopener"}

> Users may see a 0x800f0922 error when trying to install security update KB5012170 on the currently supported Windows operating system for consumers and the enterprise-class Server version. [...]
