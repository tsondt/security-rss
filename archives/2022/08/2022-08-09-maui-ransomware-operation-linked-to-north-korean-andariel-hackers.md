Title: Maui ransomware operation linked to North Korean 'Andariel' hackers
Date: 2022-08-09T11:00:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-09-maui-ransomware-operation-linked-to-north-korean-andariel-hackers

[Source](https://www.bleepingcomputer.com/news/security/maui-ransomware-operation-linked-to-north-korean-andariel-hackers/){:target="_blank" rel="noopener"}

> The Maui ransomware operation has been linked to the North Korean state-sponsored hacking group 'Andariel,' known for using malicious cyber activities to generate revenue and causing discord in South Korea. [...]
