Title: Meta privacy red team lead: Does your business know its privacy adversaries?
Date: 2022-08-11T01:15:40+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-11-meta-privacy-red-team-lead-does-your-business-know-its-privacy-adversaries

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/meta_privacy_red_team/){:target="_blank" rel="noopener"}

> Ethical hackers, but for privacy programs Black Hat Miscreants aren't only working to exploit flaws in an enterprise's security posture, they're also looking for holes in organizations' privacy programs to steal user data, according to Meta's Scott Tenaglia.... [...]
