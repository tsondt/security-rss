Title: Remotely Controlling Touchscreens
Date: 2022-08-16T11:59:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;control;cybersecurity
Slug: 2022-08-16-remotely-controlling-touchscreens

[Source](https://www.schneier.com/blog/archives/2022/08/remotely-controlling-touchscreens-2.html){:target="_blank" rel="noopener"}

> This is more of a demonstration than a real-world vulnerability, but researchers can use electromagnetic interference to remotely control touchscreens. From a news article : It’s important to note that the attack has a few key limitations. Firstly, the hackers need to know the target’s phone passcode, or launch the attack while the phone is unlocked. Secondly, the victim needs to put the phone face down, otherwise the battery and motherboard will block the electromagnetic signal. Thirdly, the antenna array has to be no more than four centimeters (around 1.5 inches) away. For all these reasons the researchers themselves admit [...]
