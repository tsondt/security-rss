Title: How to avoid cloud misconfigurations and move towards continuous compliance
Date: 2022-08-26T16:00:00+00:00
Author: Zeal Somani
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-08-26-how-to-avoid-cloud-misconfigurations-and-move-towards-continuous-compliance

[Source](https://cloud.google.com/blog/products/identity-security/how-to-move-towards-continuous-compliance-while-avoiding-misconfigurations/){:target="_blank" rel="noopener"}

> Security is often seen as a zero-sum game between “go fast” or “stay secure.” We would like to challenge this school of thought and introduce a framework to change that paradigm to a “win-win game,” so you can do both—“go fast” and “stay secure.” Historically, application security tools have been implemented much like a gate at a parking lot. The parking lot has perimeter-based ingress and egress boom gates. The boom gates let one car through at a time, and vehicles often are backed up at the gates during busy hours. However, there are few controls once you get inside. [...]
