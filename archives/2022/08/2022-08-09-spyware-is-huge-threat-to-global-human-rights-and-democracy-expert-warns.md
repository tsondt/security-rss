Title: Spyware is huge threat to global human rights and democracy, expert warns
Date: 2022-08-09T12:16:49+00:00
Author: Leyland Cecco
Category: The Guardian
Tags: Canada;World news;Americas;Hacking;Data and computer security;Technology
Slug: 2022-08-09-spyware-is-huge-threat-to-global-human-rights-and-democracy-expert-warns

[Source](https://www.theguardian.com/world/2022/aug/09/spyware-canada-threat-democracy-human-rights){:target="_blank" rel="noopener"}

> Cybersecurity expert Ron Deibert to testify to Canadian MPs about troubling spread of invasive surveillance tools The mercenary spyware industry represents “one of the greatest contemporary threats to civil society, human rights and democracy”, a leading cybersecurity expert warns, as countries grapple with the unregulated spread of powerful and invasive surveillance tools. Ron Deibert, a political science professor at the university of Toronto and head of Citizen Lab, will testify in front of a Canadian parliamentary committee on Tuesday afternoon about the growing threat he and others believe the technology poses to citizens and democracies. Continue reading... [...]
