Title: deBridge Finance crypto platform targeted by Lazarus hackers
Date: 2022-08-08T19:04:07-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-08-debridge-finance-crypto-platform-targeted-by-lazarus-hackers

[Source](https://www.bleepingcomputer.com/news/security/debridge-finance-crypto-platform-targeted-by-lazarus-hackers/){:target="_blank" rel="noopener"}

> Hackers suspected to be from the North Korean Lazarus group tried their luck at stealing cryptocurrency from deBridge Finance, a cross-chain protocol that enables the decentralized transfer of assets between various blockchains. [...]
