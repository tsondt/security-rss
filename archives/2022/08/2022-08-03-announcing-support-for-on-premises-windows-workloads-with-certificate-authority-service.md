Title: Announcing support for on-premises Windows workloads with Certificate Authority Service
Date: 2022-08-03T16:00:00+00:00
Author: Bahul Harikumar
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-08-03-announcing-support-for-on-premises-windows-workloads-with-certificate-authority-service

[Source](https://cloud.google.com/blog/products/identity-security/supporting-windows-workloads-with-certificate-authority-service/){:target="_blank" rel="noopener"}

> The use of digital certificates to establish trust across our digital infrastructure continues to grow at a rapid pace, driven by development and deployment of cloud-based, containerized, microservice-based applications and the proliferation of connected Internet of Things and smart devices. Google Cloud Certificate Authority Service (CAS) provides a highly scalable and available private CA to help organizations address the growing need for certificates. With CAS, you can offload time-consuming tasks associated with operating a private CA, like hardware provisioning, infrastructure security, software deployment, high-availability configuration, disaster recovery, backups, and more to the cloud. While a cloud-based CA is uniquely suited [...]
