Title: 80,000 internet-connected cameras still vulnerable after critical patch offered
Date: 2022-08-24T20:46:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-08-24-80000-internet-connected-cameras-still-vulnerable-after-critical-patch-offered

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/24/hikvision_camera_patch/){:target="_blank" rel="noopener"}

> Just more IoT conscripts for the botnet armies Tens of thousands of internet-facing IP cameras made by China-based Hikvision remain unpatched and exploitable despite a fix being issued for a critical security bug nearly a year ago.... [...]
