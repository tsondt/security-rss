Title: Block sued after ex-staffer siphons customer data
Date: 2022-08-24T23:09:01+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-24-block-sued-after-ex-staffer-siphons-customer-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/24/block_headed_to_court_to/){:target="_blank" rel="noopener"}

> 'Don't be such a Square' hits different these days Block – the digital payments giant formerly known as Square – faces allegations it failed to take adequate measures to protect customers' personal information.... [...]
