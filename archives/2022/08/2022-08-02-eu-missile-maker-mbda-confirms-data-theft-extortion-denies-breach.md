Title: EU missile maker MBDA confirms data theft extortion, denies breach
Date: 2022-08-02T08:43:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-02-eu-missile-maker-mbda-confirms-data-theft-extortion-denies-breach

[Source](https://www.bleepingcomputer.com/news/security/eu-missile-maker-mbda-confirms-data-theft-extortion-denies-breach/){:target="_blank" rel="noopener"}

> MBDA, one of the largest missile developers and manufacturers in Europe, has responded to rumors about a cyberattack on its infrastructure saying that claims of a breach of its systems are false. [...]
