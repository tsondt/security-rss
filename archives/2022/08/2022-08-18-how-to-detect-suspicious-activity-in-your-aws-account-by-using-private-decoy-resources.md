Title: How to detect suspicious activity in your AWS account by using private decoy resources
Date: 2022-08-18T18:23:44+00:00
Author: Maitreya Ranganath
Category: AWS Security
Tags: Advanced (300);Intermediate (200);Security, Identity, & Compliance;Technical How-to;Canaries;Decoys;Detective Controls;Honeypots;Security Blog
Slug: 2022-08-18-how-to-detect-suspicious-activity-in-your-aws-account-by-using-private-decoy-resources

[Source](https://aws.amazon.com/blogs/security/how-to-detect-suspicious-activity-in-your-aws-account-by-using-private-decoy-resources/){:target="_blank" rel="noopener"}

> As customers mature their security posture on Amazon Web Services (AWS), they are adopting multiple ways to detect suspicious behavior and notify response teams or workflows to take action. One example is using Amazon GuardDuty to monitor AWS accounts and workloads for malicious activity and deliver detailed security findings for visibility and remediation. Another tactic [...]
