Title: Minimizing the security risks of Single Sign On implementations
Date: 2022-08-03T10:04:08-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-08-03-minimizing-the-security-risks-of-single-sign-on-implementations

[Source](https://www.bleepingcomputer.com/news/security/minimizing-the-security-risks-of-single-sign-on-implementations/){:target="_blank" rel="noopener"}

> While the use of Single Sign On resulted in some organizations adopting stronger password policies, it also created additional security risks. Learn what these risks are and how you can make SSO more secure. [...]
