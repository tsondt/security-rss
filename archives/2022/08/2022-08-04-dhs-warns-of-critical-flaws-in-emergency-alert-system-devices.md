Title: DHS warns of critical flaws in Emergency Alert System devices
Date: 2022-08-04T15:41:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-04-dhs-warns-of-critical-flaws-in-emergency-alert-system-devices

[Source](https://www.bleepingcomputer.com/news/security/dhs-warns-of-critical-flaws-in-emergency-alert-system-devices/){:target="_blank" rel="noopener"}

> The Department of Homeland Security (DHS) warned that attackers could exploit critical security vulnerabilities in unpatched Emergency Alert System (EAS) encoder/decoder devices to send fake emergency alerts via TV and radio networks. [...]
