Title: Twilio hackers hit over 130 orgs in massive Okta phishing attack
Date: 2022-08-25T10:53:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-25-twilio-hackers-hit-over-130-orgs-in-massive-okta-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/twilio-hackers-hit-over-130-orgs-in-massive-okta-phishing-attack/){:target="_blank" rel="noopener"}

> Threat analysts have discovered the phishing kit responsible for thousands of attacks against 136 high-profile organizations that have compromised 9,931 accounts. [...]
