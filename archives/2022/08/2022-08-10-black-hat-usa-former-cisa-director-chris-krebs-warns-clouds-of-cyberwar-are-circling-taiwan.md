Title: Black Hat USA: Former CISA director Chris Krebs warns clouds of cyberwar are circling Taiwan
Date: 2022-08-10T21:05:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-10-black-hat-usa-former-cisa-director-chris-krebs-warns-clouds-of-cyberwar-are-circling-taiwan

[Source](https://portswigger.net/daily-swig/black-hat-usa-former-cisa-director-chris-krebs-warns-clouds-of-cyberwar-are-circling-taiwan){:target="_blank" rel="noopener"}

> Attack on Taiwan seemingly a case of ‘when’ not ‘if’ Chris Krebs, the former director of the US Cybersecurity and Infrastructure Security Agency (CISA), is “bearish in the short term, bullish in the l [...]
