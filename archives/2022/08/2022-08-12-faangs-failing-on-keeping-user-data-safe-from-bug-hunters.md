Title: FAANGs failing on keeping user data safe from bug hunters
Date: 2022-08-12T00:58:32+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-12-faangs-failing-on-keeping-user-data-safe-from-bug-hunters

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/12/faang_bug_hunters/){:target="_blank" rel="noopener"}

> Time to call in the legal team Black Hat Dylan Ayrey, a bug hunter and CEO of Truffle Security, discovered a big data company credential dump containing personal information belonging to about 50,000 of its users, and still hasn't fixed it.... [...]
