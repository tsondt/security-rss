Title: Ukraine takes down cybercrime group hitting crypto fraud victims
Date: 2022-08-30T18:20:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-08-30-ukraine-takes-down-cybercrime-group-hitting-crypto-fraud-victims

[Source](https://www.bleepingcomputer.com/news/security/ukraine-takes-down-cybercrime-group-hitting-crypto-fraud-victims/){:target="_blank" rel="noopener"}

> The National Police of Ukraine (NPU) took down a network of call centers used by a cybercrime group focused on financial scams and targeting victims of cryptocurrency scams under the guise of helping them recover their stolen funds. [...]
