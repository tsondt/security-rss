Title: Mozilla finds 18 of 25 popular reproductive health apps share your data
Date: 2022-08-17T08:00:20+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-17-mozilla-finds-18-of-25-popular-reproductive-health-apps-share-your-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/17/mozilla_pregnancy_app/){:target="_blank" rel="noopener"}

> Scary in post-Roe America, and Poland, and far too many other places It's official: your period and/or pregnancy tracker will probably share your data with law enforcement.... [...]
