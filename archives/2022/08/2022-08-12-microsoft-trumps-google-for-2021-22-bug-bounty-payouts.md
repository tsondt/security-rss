Title: Microsoft trumps Google for 2021-22 bug bounty payouts
Date: 2022-08-12T18:00:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-08-12-microsoft-trumps-google-for-2021-22-bug-bounty-payouts

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/12/microsoft_bug_bounty/){:target="_blank" rel="noopener"}

> Another $13.7m handed out to researchers, but then again it does have an awful lot of attack surfaces Microsoft appears to have beat Google on the bug bounty front, with $13.7 million in rewards spread out over 335 researchers.... [...]
