Title: Decisions on health data sharing should not be taken by politicians, citizen juries find
Date: 2022-08-31T11:16:40+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-08-31-decisions-on-health-data-sharing-should-not-be-taken-by-politicians-citizen-juries-find

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/31/uk_health_data_share/){:target="_blank" rel="noopener"}

> Britain's National Data Guardian report also warns NHS needs to earn people’s trust, support for controversial data platform As the NHS in England is set to launch a competition for a far-reaching patient data platform, a public consultation has said decisions about health data sharing should not be taken by politicians.... [...]
