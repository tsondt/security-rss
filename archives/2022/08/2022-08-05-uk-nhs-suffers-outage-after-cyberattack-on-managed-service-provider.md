Title: UK NHS suffers outage after cyberattack on managed service provider
Date: 2022-08-05T18:43:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-05-uk-nhs-suffers-outage-after-cyberattack-on-managed-service-provider

[Source](https://www.bleepingcomputer.com/news/security/uk-nhs-suffers-outage-after-cyberattack-on-managed-service-provider/){:target="_blank" rel="noopener"}

> United Kingdom's National Health Service (NHS) 111 emergency services are affected by a major outage triggered by a cyberattack that hit the systems of managed service provider (MSP) Advanced. [...]
