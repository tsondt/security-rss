Title: Microsoft accounts targeted with new MFA-bypassing phishing kit
Date: 2022-08-03T14:02:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-03-microsoft-accounts-targeted-with-new-mfa-bypassing-phishing-kit

[Source](https://www.bleepingcomputer.com/news/security/microsoft-accounts-targeted-with-new-mfa-bypassing-phishing-kit/){:target="_blank" rel="noopener"}

> A new large-scale phishing campaign targeting credentials for Microsoft email services use a custom proxy-based phishing kit to bypass multi-factor authentication. [...]
