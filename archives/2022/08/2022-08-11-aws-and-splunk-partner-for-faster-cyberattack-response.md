Title: AWS and Splunk partner for faster cyberattack response
Date: 2022-08-11T20:45:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-11-aws-and-splunk-partner-for-faster-cyberattack-response

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/ocsf_cybersecurity_standard_aws/){:target="_blank" rel="noopener"}

> OCSF initiative will give enterprise security teams an open standard for moving and analyzing threat data Black Hat AWS and Splunk are leading an initiative aimed at creating an open standard for ingesting and analyzing data, enabling enterprise security teams to more quickly respond to cyberthreats.... [...]
