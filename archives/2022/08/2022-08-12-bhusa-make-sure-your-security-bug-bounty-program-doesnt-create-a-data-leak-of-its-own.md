Title: BHUSA: Make sure your security bug bounty program doesn’t create a data leak of its own
Date: 2022-08-12T14:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-12-bhusa-make-sure-your-security-bug-bounty-program-doesnt-create-a-data-leak-of-its-own

[Source](https://portswigger.net/daily-swig/bhusa-make-sure-your-security-bug-bounty-program-doesnt-create-a-data-leak-of-its-own){:target="_blank" rel="noopener"}

> Researchers, organizations, and bug disclosure platforms can all make improvements to help protect user data [...]
