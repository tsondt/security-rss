Title: Identifying publicly accessible resources with Amazon VPC Network Access Analyzer
Date: 2022-08-22T16:04:11+00:00
Author: Patrick Duffy
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;IAM Access Analyzer;Network;Security;Security Blog
Slug: 2022-08-22-identifying-publicly-accessible-resources-with-amazon-vpc-network-access-analyzer

[Source](https://aws.amazon.com/blogs/security/identifying-publicly-accessible-resources-with-amazon-vpc-network-access-analyzer/){:target="_blank" rel="noopener"}

> Network and security teams often need to evaluate the internet accessibility of all their resources on AWS and block any non-essential internet access. Validating who has access to what can be complicated—there are several different controls that can prevent or authorize access to resources in your Amazon Virtual Private Cloud (Amazon VPC). The recently launched [...]
