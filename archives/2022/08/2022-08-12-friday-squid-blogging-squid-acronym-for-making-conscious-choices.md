Title: Friday Squid Blogging: SQUID Acronym for Making Conscious Choices
Date: 2022-08-12T21:06:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-08-12-friday-squid-blogging-squid-acronym-for-making-conscious-choices

[Source](https://www.schneier.com/blog/archives/2022/08/friday-squid-blogging-squid-acronym-for-making-conscious-choices.html){:target="_blank" rel="noopener"}

> I think the U is forced : SQUID consists of five steps: Stop, Question, Understand, Imagine, and Decide. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
