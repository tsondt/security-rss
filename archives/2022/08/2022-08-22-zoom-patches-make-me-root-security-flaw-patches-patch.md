Title: Zoom patches make-me-root security flaw, patches patch
Date: 2022-08-22T06:20:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-08-22-zoom-patches-make-me-root-security-flaw-patches-patch

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/22/in-brief-security/){:target="_blank" rel="noopener"}

> Plus: See if in-app browsers are monitoring you, a novel industrial network attack technique, and more In brief Zoom fixed a pair of privilege escalation vulnerabilities, which were detailed at the Black Hat conference this month, but that patch was bypassed, necessitating yet another fix.... [...]
