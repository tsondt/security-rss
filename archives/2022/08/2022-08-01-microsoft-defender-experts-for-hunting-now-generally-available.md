Title: Microsoft Defender Experts for Hunting now generally available
Date: 2022-08-01T14:32:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-08-01-microsoft-defender-experts-for-hunting-now-generally-available

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-experts-for-hunting-now-generally-available/){:target="_blank" rel="noopener"}

> Microsoft Defender Experts for Hunting, a new managed security service for Microsoft 365 Defender customers, is now generally available. [...]
