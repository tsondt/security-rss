Title: AWS achieves FedRAMP P-ATO for 20 services in the AWS US East/West Regions and AWS GovCloud (US) Regions
Date: 2022-08-29T21:41:06+00:00
Author: Steve Earley
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS (US) GovCloud;AWS East/West;Federal government;FedRAMP;Public Sector;Security Blog
Slug: 2022-08-29-aws-achieves-fedramp-p-ato-for-20-services-in-the-aws-us-eastwest-regions-and-aws-govcloud-us-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-fedramp-p-ato-for-20-services-in-the-aws-us-east-west-regions-and-aws-govcloud-us-regions/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce that 20 additional AWS services have achieved Provisional Authority to Operate (P-ATO) from the Federal Risk and Authorization Management Program (FedRAMP) Joint Authorization Board (JAB). The following are the 20 AWS services with FedRAMP authorization for the U.S. federal government and organizations with regulated workloads: AWS App [...]
