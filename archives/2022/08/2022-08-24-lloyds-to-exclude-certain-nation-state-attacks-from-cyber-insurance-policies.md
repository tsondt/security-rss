Title: Lloyd's to exclude certain nation-state attacks from cyber insurance policies
Date: 2022-08-24T06:28:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-24-lloyds-to-exclude-certain-nation-state-attacks-from-cyber-insurance-policies

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/24/lloyds_cybersecurity_insurance/){:target="_blank" rel="noopener"}

> Kim Jong-un has entered the chat Lloyd's of London insurance policies will stop covering losses from certain nation-state cyber attacks and those that happen during wars, beginning in seven months' time.... [...]
