Title: Software developer cracks Hyundai car security with Google search
Date: 2022-08-17T20:19:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-17-software-developer-cracks-hyundai-car-security-with-google-search

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/17/software_developer_cracks_hyundai_encryption/){:target="_blank" rel="noopener"}

> Top tip: Your RSA private key should not be copied from a public code tutorial A developer says he was able to run his own software on his car infotainment hardware after discovering the vehicle's manufacturer had secured its system using keys that were not only publicly known but had been lifted from programming examples.... [...]
