Title: Microsoft Patch Tuesday, August 2022 Edition
Date: 2022-08-09T23:01:10+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;CVE-2022-21980;CVE-2022-24477;CVE-2022-24516;CVE-2022-30133;CVE-2022-30134;CVE-2022-34713;CVE-2022-35743;Follina;Greg Wiseman;Immersive Labs;Kevin Breen;Microsoft Patch Tuesday August 2022;Rapid7;sans internet storm center;Visual Studio;Windows Hello
Slug: 2022-08-09-microsoft-patch-tuesday-august-2022-edition

[Source](https://krebsonsecurity.com/2022/08/microsoft-patch-tuesday-august-2022-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to fix a record 141 security vulnerabilities in its Windows operating systems and related software. Once again, Microsoft is patching a zero-day vulnerability in the Microsoft Support Diagnostics Tool (MSDT), a service built into Windows. Redmond also addressed multiple flaws in Exchange Server — including one that was disclosed publicly prior to today — and it is urging organizations that use Exchange for email to update as soon as possible and to enable additional protections. In June, Microsoft patched a vulnerability in MSDT dubbed “ Follina ” that had been used in active attacks for at [...]
