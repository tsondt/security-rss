Title: Highlighting What should be Patched First at the Endpoint
Date: 2022-08-24T10:01:02-04:00
Author: Sponsored by Fortinet
Category: BleepingComputer
Tags: Security
Slug: 2022-08-24-highlighting-what-should-be-patched-first-at-the-endpoint

[Source](https://www.bleepingcomputer.com/news/security/highlighting-what-should-be-patched-first-at-the-endpoint/){:target="_blank" rel="noopener"}

> FortiGuard Labs has released its Global Threat Landscape Report for the first half of 2022. This valuable report offers insights on the world's cyberthreats for the first six months of the year by examining the compiled data gathered from Fortinet's global array of sensors. [...]
