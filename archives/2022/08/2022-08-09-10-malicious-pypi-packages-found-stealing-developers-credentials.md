Title: 10 malicious PyPI packages found stealing developer's credentials
Date: 2022-08-09T13:02:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-09-10-malicious-pypi-packages-found-stealing-developers-credentials

[Source](https://www.bleepingcomputer.com/news/security/10-malicious-pypi-packages-found-stealing-developers-credentials/){:target="_blank" rel="noopener"}

> Threat analysts have discovered ten malicious Python packages on the PyPI repository, used to infect developer's systems with password-stealing malware. [...]
