Title: Black Hat and DEF CON visitors differ on physical risk management
Date: 2022-08-15T04:58:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-08-15-black-hat-and-def-con-visitors-differ-on-physical-risk-management

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/15/black_hat_covid/){:target="_blank" rel="noopener"}

> COVID, flood surfing, crowds – what to pick? Black Hat As last week's hacker summer camps would down it's clear that attendee numbers are still well down on the pre-COVID days, although things are recovering.... [...]
