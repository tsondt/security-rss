Title: AWS co-announces release of the Open Cybersecurity Schema Framework (OCSF) project
Date: 2022-08-10T16:15:20+00:00
Author: Mark Ryland
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;cybersecurity;OCSF;OCSF project;Open Cybersecurity Schema Framework;Security Blog
Slug: 2022-08-10-aws-co-announces-release-of-the-open-cybersecurity-schema-framework-ocsf-project

[Source](https://aws.amazon.com/blogs/security/aws-co-announces-release-of-the-open-cybersecurity-schema-framework-ocsf-project/){:target="_blank" rel="noopener"}

> In today’s fast-changing security environment, security professionals must continuously monitor, detect, respond to, and mitigate new and existing security issues. To do so, security teams must be able to analyze security-relevant telemetry and log data by using multiple tools, technologies, and vendors. The complex and heterogeneous nature of this task drives up costs and may [...]
