Title: Sonatype spots another PyPI package behaving badly
Date: 2022-08-11T18:30:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-08-11-sonatype-spots-another-pypi-package-behaving-badly

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/sonatype/){:target="_blank" rel="noopener"}

> Identity of a real person was used to lend credence to a package that dropped cryptominer in memory Sonatype has unearthed yet more malware lurking on PyPI, this time a fileless Linux nasty designed to mine Monero and using the identity of a real person to lend credibility to the package.... [...]
