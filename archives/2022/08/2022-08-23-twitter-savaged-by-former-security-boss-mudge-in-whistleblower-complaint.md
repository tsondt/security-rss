Title: Twitter savaged by former security boss Mudge in whistleblower complaint
Date: 2022-08-23T22:00:57+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-23-twitter-savaged-by-former-security-boss-mudge-in-whistleblower-complaint

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/23/twitter_security_whisterblower/){:target="_blank" rel="noopener"}

> Loose access to production systems, out of date software, and more claimed Twitter's former security chief Peiter "Mudge" Zatko accused the company and its board of directors of violating financial rules, of fraud, and of grossly neglecting its security obligations in a complaint to the US Securities & Exchange Commission, the Federal Trade Commission, and the US Justice Department last month.... [...]
