Title: Nancy Pelosi ties Chinese cyber-attacks to need for Taiwan visit
Date: 2022-08-03T02:58:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-08-03-nancy-pelosi-ties-chinese-cyber-attacks-to-need-for-taiwan-visit

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/03/pelosi_taiwan_visit_cyberattacks/){:target="_blank" rel="noopener"}

> And as if to confirm the link, a DDoS takes out Taiwan's presidential website ahead of senior politico's arrival Speaker of the US House of Representatives Nancy Pelosi has tied her controversial visit to Taiwan to an alleged barrage of China-directed cyber-attacks against the territory.... [...]
