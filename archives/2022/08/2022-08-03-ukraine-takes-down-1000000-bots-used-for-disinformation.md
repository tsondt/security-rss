Title: Ukraine takes down 1,000,000 bots used for disinformation
Date: 2022-08-03T11:51:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-03-ukraine-takes-down-1000000-bots-used-for-disinformation

[Source](https://www.bleepingcomputer.com/news/security/ukraine-takes-down-1-000-000-bots-used-for-disinformation/){:target="_blank" rel="noopener"}

> The Ukrainian cyber police (SSU) has shut down a massive bot farm of 1,000,000 bots used to spread disinformation on social networks. [...]
