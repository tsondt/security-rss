Title: LastPass developer systems hacked to steal source code
Date: 2022-08-25T16:59:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-25-lastpass-developer-systems-hacked-to-steal-source-code

[Source](https://www.bleepingcomputer.com/news/security/lastpass-developer-systems-hacked-to-steal-source-code/){:target="_blank" rel="noopener"}

> Password management firm LastPass was hacked two weeks ago, enabling threat actors to steal the company's source code and proprietary technical information. [...]
