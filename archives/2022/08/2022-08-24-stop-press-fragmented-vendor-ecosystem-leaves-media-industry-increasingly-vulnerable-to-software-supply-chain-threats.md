Title: Stop, press: Fragmented vendor ecosystem leaves media industry increasingly vulnerable to software supply chain threats
Date: 2022-08-24T13:27:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-24-stop-press-fragmented-vendor-ecosystem-leaves-media-industry-increasingly-vulnerable-to-software-supply-chain-threats

[Source](https://portswigger.net/daily-swig/stop-press-fragmented-vendor-ecosystem-leaves-media-industry-increasingly-vulnerable-to-software-supply-chain-threats){:target="_blank" rel="noopener"}

> New study highlights the myriad cyber defense challenges faced by media companies in 2022 [...]
