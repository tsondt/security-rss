Title: 1,900 Signal users exposed: Twilio attacker 'explicitly' looked for certain numbers
Date: 2022-08-16T12:33:06+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-08-16-1900-signal-users-exposed-twilio-attacker-explicitly-looked-for-certain-numbers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/16/twilio_breach_fallout_signal_user/){:target="_blank" rel="noopener"}

> Bad guy also got SMS verification codes, and re-registered one of the numbers they searched for The security breach at Twilio earlier this month affected at least one high-value customer, Signal, and led to the exposure of the phone number and SMS registration codes for 1,900 users of the encrypted messaging service, it confirmed.... [...]
