Title: Chinese hackers use new Cobalt Strike-like attack framework
Date: 2022-08-02T16:01:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-02-chinese-hackers-use-new-cobalt-strike-like-attack-framework

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-use-new-cobalt-strike-like-attack-framework/){:target="_blank" rel="noopener"}

> Researchers have observed a new post-exploitation attack framework used in the wild, named Manjusaka, which can be deployed as an alternative to the widely abused Cobalt Strike toolset or parallel to it for redundancy. [...]
