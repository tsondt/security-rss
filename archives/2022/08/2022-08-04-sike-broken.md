Title: SIKE Broken
Date: 2022-08-04T11:56:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;algorithms;cryptanalysis;cryptography;encryption;NIST;quantum computing
Slug: 2022-08-04-sike-broken

[Source](https://www.schneier.com/blog/archives/2022/08/sike-broken.html){:target="_blank" rel="noopener"}

> SIKE is one of the new algorithms that NIST recently added to the post-quantum cryptography competition. It was just broken, really badly. We present an efficient key recovery attack on the Supersingular Isogeny Diffie­-Hellman protocol (SIDH), based on a “glue-and-split” theorem due to Kani. Our attack exploits the existence of a small non-scalar endomorphism on the starting curve, and it also relies on the auxiliary torsion point information that Alice and Bob share during the protocol. Our Magma implementation breaks the instantiation SIKEp434, which aims at security level 1 of the Post-Quantum Cryptography standardization process currently ran by NIST, in [...]
