Title: How to use customer managed policies in AWS IAM Identity Center for advanced use cases
Date: 2022-08-15T20:49:17+00:00
Author: Ron Cully
Category: AWS Security
Tags: AWS IAM Identity Center;Intermediate (200);Security, Identity, & Compliance;Uncategorized;AWS SSO;Conditions;Customer Managed Policy;IAM Identity Center;IdP;Permissions;SAML;Security Blog
Slug: 2022-08-15-how-to-use-customer-managed-policies-in-aws-iam-identity-center-for-advanced-use-cases

[Source](https://aws.amazon.com/blogs/security/how-to-use-customer-managed-policies-in-aws-single-sign-on-for-advanced-use-cases/){:target="_blank" rel="noopener"}

> Are you looking for a simpler way to manage permissions across all your AWS accounts? Perhaps you federate your identity provider (IdP) to each account and divide permissions and authorization between cloud and identity teams, but want a simpler administrative model. Maybe you use AWS IAM Identity Center (successor to AWS Single Sign-On) but are [...]
