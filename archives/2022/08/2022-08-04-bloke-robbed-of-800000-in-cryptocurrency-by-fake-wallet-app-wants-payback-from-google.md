Title: Bloke robbed of $800,000 in cryptocurrency by fake wallet app wants payback from Google
Date: 2022-08-04T23:45:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-04-bloke-robbed-of-800000-in-cryptocurrency-by-fake-wallet-app-wants-payback-from-google

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/04/google_wallet_crypto_lawsuit/){:target="_blank" rel="noopener"}

> I got played via the Play store Last October, California resident Jacob Pearlman downloaded an Android version of a cryptocurrency wallet app called Phantom from the Google Play app store.... [...]
