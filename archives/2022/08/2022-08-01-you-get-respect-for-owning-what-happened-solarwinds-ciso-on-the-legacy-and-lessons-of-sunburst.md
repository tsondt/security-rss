Title: ‘You get respect for owning what happened’ – SolarWinds’ CISO on the legacy and lessons of Sunburst
Date: 2022-08-01T15:36:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-01-you-get-respect-for-owning-what-happened-solarwinds-ciso-on-the-legacy-and-lessons-of-sunburst

[Source](https://portswigger.net/daily-swig/you-get-respect-for-owning-what-happened-solarwinds-ciso-on-the-legacy-and-lessons-of-sunburst){:target="_blank" rel="noopener"}

> Security chief counts new build system and greater intel sharing among positive legacies of watershed cyber-attack [...]
