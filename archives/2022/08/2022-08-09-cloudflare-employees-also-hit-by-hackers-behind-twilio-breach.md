Title: Cloudflare employees also hit by hackers behind Twilio breach
Date: 2022-08-09T13:28:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-09-cloudflare-employees-also-hit-by-hackers-behind-twilio-breach

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-employees-also-hit-by-hackers-behind-twilio-breach/){:target="_blank" rel="noopener"}

> Cloudflare says some of its employees' credentials were also stolen in an SMS phishing attack very similar to the one that led to Twilio's network being breached last week. [...]
