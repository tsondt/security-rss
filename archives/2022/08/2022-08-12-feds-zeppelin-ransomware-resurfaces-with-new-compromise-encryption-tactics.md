Title: Feds: Zeppelin Ransomware Resurfaces with New Compromise, Encryption Tactics
Date: 2022-08-12T18:20:38+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2022-08-12-feds-zeppelin-ransomware-resurfaces-with-new-compromise-encryption-tactics

[Source](https://threatpost.com/zeppelin-ransomware-resurfaces/180405/){:target="_blank" rel="noopener"}

> The CISA has seen a resurgence of the malware targeting a range of verticals and critical infrastructure organizations by exploiting RDP, firewall vulnerabilities. [...]
