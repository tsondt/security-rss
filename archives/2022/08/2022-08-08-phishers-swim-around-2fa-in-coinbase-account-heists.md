Title: Phishers Swim Around 2FA in Coinbase Account Heists
Date: 2022-08-08T15:26:17+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2022-08-08-phishers-swim-around-2fa-in-coinbase-account-heists

[Source](https://threatpost.com/phishers-2fa-coinbase/180356/){:target="_blank" rel="noopener"}

> Attackers are spoofing the widely used cryptocurrency exchange to trick users into logging in so they can steal their credentials and eventually their funds. [...]
