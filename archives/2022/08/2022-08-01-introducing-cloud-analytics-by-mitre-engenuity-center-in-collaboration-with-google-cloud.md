Title: Introducing Cloud Analytics by MITRE Engenuity Center in collaboration with Google Cloud
Date: 2022-08-01T16:00:00+00:00
Author: Jon Baker
Category: GCP Security
Tags: Google Cloud;Data Analytics;Identity & Security
Slug: 2022-08-01-introducing-cloud-analytics-by-mitre-engenuity-center-in-collaboration-with-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/announcing-cloud-analytics-googles-latest-partnership-with-mitre/){:target="_blank" rel="noopener"}

> The cybersecurity industry is faced with the tremendous challenge of analyzing growing volumes of security data in a dynamic threat landscape with evolving adversary behaviors. Today’s security data is heterogeneous, including logs and alerts, and often comes from more than one cloud platform. In order to better analyze that data, we’re excited to announce the release of the Cloud Analytics project by the MITRE Engenuity Center for Threat-Informed Defense, and sponsored by Google Cloud and several other industry collaborators. Since 2021, Google Cloud has partnered with the Center to help level the playing field for everyone in the cybersecurity community [...]
