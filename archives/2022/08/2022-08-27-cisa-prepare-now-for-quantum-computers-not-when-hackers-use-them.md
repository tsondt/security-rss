Title: CISA: Prepare now for quantum computers, not when hackers use them
Date: 2022-08-27T10:11:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-27-cisa-prepare-now-for-quantum-computers-not-when-hackers-use-them

[Source](https://www.bleepingcomputer.com/news/security/cisa-prepare-now-for-quantum-computers-not-when-hackers-use-them/){:target="_blank" rel="noopener"}

> Although quantum computing is not commercially available, CISA (Cybersecurity and Infrastructure Security Agency) urges organizations to prepare for the dawn of this new age, which is expected to bring groundbreaking changes in cryptography, and how we protect our secrets. [...]
