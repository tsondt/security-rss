Title: Xiaomi Phone Bug Allowed Payment Forgery
Date: 2022-08-16T12:26:27+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: 2022-08-16-xiaomi-phone-bug-allowed-payment-forgery

[Source](https://threatpost.com/xiaomi-phones-found-vulnerable-to-payment-forgery/180416/){:target="_blank" rel="noopener"}

> Mobile transactions could’ve been disabled, created and signed by attackers. [...]
