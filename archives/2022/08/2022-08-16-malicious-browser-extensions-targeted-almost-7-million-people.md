Title: Malicious browser extensions targeted almost 7 million people
Date: 2022-08-16T14:09:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-16-malicious-browser-extensions-targeted-almost-7-million-people

[Source](https://www.bleepingcomputer.com/news/security/malicious-browser-extensions-targeted-almost-7-million-people/){:target="_blank" rel="noopener"}

> Almost 7 million users have attempted to install malicious browser extensions since 2020, with 70% of those extensions used as adware to target users with advertisements. [...]
