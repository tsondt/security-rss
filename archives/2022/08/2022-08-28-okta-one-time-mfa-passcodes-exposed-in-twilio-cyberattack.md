Title: Okta one-time MFA passcodes exposed in Twilio cyberattack
Date: 2022-08-28T13:15:05-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-28-okta-one-time-mfa-passcodes-exposed-in-twilio-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/okta-one-time-mfa-passcodes-exposed-in-twilio-cyberattack/){:target="_blank" rel="noopener"}

> The threat actor behind the Twilio hack used their access to steal one-time passwords (OTPs) delivered over SMS to from customers of Okta identity and access management company. [...]
