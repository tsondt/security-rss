Title: Introducing Google Cloud and Google Workspace support for multiple Identity providers with Single Sign-On
Date: 2022-08-09T16:00:00+00:00
Author: Matthew Soldo
Category: GCP Security
Tags: Google Workspace;Google Cloud;Identity & Security
Slug: 2022-08-09-introducing-google-cloud-and-google-workspace-support-for-multiple-identity-providers-with-single-sign-on

[Source](https://cloud.google.com/blog/products/identity-security/google-now-supports-multi-idp-sso-in-google-workspace-and-google-cloud/){:target="_blank" rel="noopener"}

> Google is one of the largest identity providers on the Internet. Users rely on our identity systems to log into Google’s own offerings, as well as third-party apps and services. For our business customers, we provide administratively managed Google accounts that can be used to access Google Workspace, Google Cloud, and BeyondCorp Enterprise. Today we’re announcing that these organizational accounts support single sign-on (SSO) from multiple third-party identity providers (IdPs), available in general availability immediately. This allows customers to more easily access Google’s services using their existing identity systems. Google has long provided customers with a choice of digital identity [...]
