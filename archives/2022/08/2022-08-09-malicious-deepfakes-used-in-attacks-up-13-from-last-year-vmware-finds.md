Title: Malicious deepfakes used in attacks up 13% from last year, VMware finds
Date: 2022-08-09T15:11:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-08-09-malicious-deepfakes-used-in-attacks-up-13-from-last-year-vmware-finds

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/09/vmware_malware/){:target="_blank" rel="noopener"}

> Plus: Crooks swimming around your network, looking for a way in, says Incident Response Threat Report Security teams are facing down more cyberattacks following Russia's invasion of Ukraine, and sophisticated crooks are using double-extortion techniques and, increasingly, deepfakes in their strikes.... [...]
