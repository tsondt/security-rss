Title: 7-Eleven Denmark confirms ransomware attack behind store closures
Date: 2022-08-10T18:21:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-10-7-eleven-denmark-confirms-ransomware-attack-behind-store-closures

[Source](https://www.bleepingcomputer.com/news/security/7-eleven-denmark-confirms-ransomware-attack-behind-store-closures/){:target="_blank" rel="noopener"}

> 7-Eleven Denmark has confirmed that a ransomware attack was behind the closure of 175 stores in the country on Monday. [...]
