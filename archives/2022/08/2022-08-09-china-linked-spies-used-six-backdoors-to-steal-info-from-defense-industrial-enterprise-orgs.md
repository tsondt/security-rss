Title: China-linked spies used six backdoors to steal info from defense, industrial enterprise orgs
Date: 2022-08-09T00:23:18+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-09-china-linked-spies-used-six-backdoors-to-steal-info-from-defense-industrial-enterprise-orgs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/09/china_apt_kaspersky/){:target="_blank" rel="noopener"}

> We're 'highly likely' to see similar attacks, Kaspersky warned Beijing-backed cyberspies used specially crafted phishing emails and six different backdoors to break into and then steal confidential data from military and industrial groups, government agencies and other public institutions, according to Kaspersky researchers.... [...]
