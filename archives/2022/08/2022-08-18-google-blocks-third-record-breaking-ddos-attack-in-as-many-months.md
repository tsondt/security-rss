Title: Google blocks third record-breaking DDoS attack in as many months
Date: 2022-08-18T16:00:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-18-google-blocks-third-record-breaking-ddos-attack-in-as-many-months

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/18/google_record_ddos/){:target="_blank" rel="noopener"}

> 46 million requests per second network flood comes as attacks increase by more than 200% compared to last year Google says it has blocked the largest ever HTTPS-based distributed-denial-of-service (DDoS) attack in June, which peaked at 46 million requests per second.... [...]
