Title: How to centralize findings and automate deletion for unused IAM roles
Date: 2022-08-25T19:21:16+00:00
Author: Hong Pham
Category: AWS Security
Tags: Expert (400);Security, Identity, & Compliance;IAM;Security;Security Blog
Slug: 2022-08-25-how-to-centralize-findings-and-automate-deletion-for-unused-iam-roles

[Source](https://aws.amazon.com/blogs/security/how-to-centralize-findings-and-automate-deletion-for-unused-iam-roles/){:target="_blank" rel="noopener"}

> Maintaining AWS Identity and Access Management (IAM) resources is similar to keeping your garden healthy over time. Having visibility into your IAM resources, especially the resources that are no longer used, is important to keep your AWS environment secure. Proactively detecting and responding to unused IAM roles helps you prevent unauthorized entities from gaining access [...]
