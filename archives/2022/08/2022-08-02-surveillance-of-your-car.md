Title: Surveillance of Your Car
Date: 2022-08-02T11:49:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;data collection;privacy;surveillance
Slug: 2022-08-02-surveillance-of-your-car

[Source](https://www.schneier.com/blog/archives/2022/08/surveillance-of-your-car.html){:target="_blank" rel="noopener"}

> TheMarkup has an extensive analysis of connected vehicle data and the companies that are collecting it. The Markup has identified 37 companies that are part of the rapidly growing connected vehicle data industry that seeks to monetize such data in an environment with few regulations governing its sale or use. While many of these companies stress they are using aggregated or anonymized data, the unique nature of location and movement data increases the potential for violations of user privacy. [...]
