Title: Black Hat and DEF CON Roundup
Date: 2022-08-15T13:56:58+00:00
Author: Threatpost
Category: Threatpost
Tags: Black Hat;Government;Hacks;Malware;Vulnerabilities
Slug: 2022-08-15-black-hat-and-def-con-roundup

[Source](https://threatpost.com/black-hat-and-def-con-roundup/180409/){:target="_blank" rel="noopener"}

> ‘Summer Camp’ for hackers features a compromised satellite, a homecoming for hackers and cyberwarfare warnings. [...]
