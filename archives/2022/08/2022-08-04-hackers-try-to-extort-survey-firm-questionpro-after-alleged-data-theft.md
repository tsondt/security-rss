Title: Hackers try to extort survey firm QuestionPro after alleged data theft
Date: 2022-08-04T17:29:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-04-hackers-try-to-extort-survey-firm-questionpro-after-alleged-data-theft

[Source](https://www.bleepingcomputer.com/news/security/hackers-try-to-extort-survey-firm-questionpro-after-alleged-data-theft/){:target="_blank" rel="noopener"}

> Hackers attempted to extort the online survey platform QuestionPro after claiming to have stolen the company's database containing respondents' personal information. [...]
