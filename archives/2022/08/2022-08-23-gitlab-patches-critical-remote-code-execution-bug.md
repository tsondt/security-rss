Title: GitLab patches critical remote code execution bug
Date: 2022-08-23T11:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-23-gitlab-patches-critical-remote-code-execution-bug

[Source](https://portswigger.net/daily-swig/gitlab-patches-critical-remote-code-execution-bug){:target="_blank" rel="noopener"}

> Update now to protect against security vulnerability [...]
