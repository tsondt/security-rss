Title: AWS announces migration plans for NIST 800-53 Revision 5
Date: 2022-08-29T17:45:15+00:00
Author: James Mueller
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;AWS (US) GovCloud;AWS East/West;DoD;Federal government;FedRAMP;OSCAL;Public Sector;Security Blog
Slug: 2022-08-29-aws-announces-migration-plans-for-nist-800-53-revision-5

[Source](https://aws.amazon.com/blogs/security/aws-announces-migration-plans-for-nist-800-53-revision-5/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is excited to begin migration plans for National Institute of Standards and Technology (NIST) 800-53 Revision 5. The NIST 800-53 framework is a regulatory standard that defines the minimum baseline of security controls for U.S. federal information systems. In 2020, NIST released Revision 5 of the framework to improve security standards [...]
