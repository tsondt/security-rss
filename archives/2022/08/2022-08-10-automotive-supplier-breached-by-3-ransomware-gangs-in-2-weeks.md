Title: Automotive supplier breached by 3 ransomware gangs in 2 weeks
Date: 2022-08-10T17:07:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-10-automotive-supplier-breached-by-3-ransomware-gangs-in-2-weeks

[Source](https://www.bleepingcomputer.com/news/security/automotive-supplier-breached-by-3-ransomware-gangs-in-2-weeks/){:target="_blank" rel="noopener"}

> An automotive supplier had its systems breached and files encrypted by three different ransomware gangs over a two-week span in May, two of the attacks happening within just two hours. [...]
