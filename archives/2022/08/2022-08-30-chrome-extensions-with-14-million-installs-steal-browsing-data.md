Title: Chrome extensions with 1.4 million installs steal browsing data
Date: 2022-08-30T11:11:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-30-chrome-extensions-with-14-million-installs-steal-browsing-data

[Source](https://www.bleepingcomputer.com/news/security/chrome-extensions-with-14-million-installs-steal-browsing-data/){:target="_blank" rel="noopener"}

> Threat analysts at McAfee found five Google Chrome extensions that steal track users' browsing activity. Collectively, the extensions have been downloaded more then 1.4 million times. [...]
