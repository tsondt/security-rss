Title: Phishing attacks abusing SaaS platforms see a massive 1,100% growth
Date: 2022-08-23T16:08:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-23-phishing-attacks-abusing-saas-platforms-see-a-massive-1100-growth

[Source](https://www.bleepingcomputer.com/news/security/phishing-attacks-abusing-saas-platforms-see-a-massive-1-100-percent-growth/){:target="_blank" rel="noopener"}

> Threat actors are increasingly abusing legitimate software-as-a-service (SaaS) platforms like website builders and personal branding spaces to create malicious phishing websites that steal login credentials. [...]
