Title: An encrypted ZIP file can have two correct passwords — here's why
Date: 2022-08-21T12:27:34-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-08-21-an-encrypted-zip-file-can-have-two-correct-passwords-heres-why

[Source](https://www.bleepingcomputer.com/news/security/an-encrypted-zip-file-can-have-two-correct-passwords-heres-why/){:target="_blank" rel="noopener"}

> Password-protected ZIP archives are common means of compressing and sharing sets of files—from sensitive documents to malware samples to even malware (phishing "invoices" in emails). But, did you know it is possible for an encrypted ZIP file to have two correct passwords, with both producing the same outcome on extraction? [...]
