Title: Student Loan Breach Exposes 2.5M Records
Date: 2022-08-31T12:57:48+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Breach
Slug: 2022-08-31-student-loan-breach-exposes-25m-records

[Source](https://threatpost.com/student-loan-breach-exposes-2-5m-records/180492/){:target="_blank" rel="noopener"}

> 2.5 million people were affected, in a breach that could spell more trouble down the line. [...]
