Title: Google Chrome bug lets sites write to clipboard without asking
Date: 2022-08-31T13:13:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-31-google-chrome-bug-lets-sites-write-to-clipboard-without-asking

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-bug-lets-sites-write-to-clipboard-without-asking/){:target="_blank" rel="noopener"}

> Chrome version 104 accidentally introduced a bug that removes the user requirement to approve clipboard writing events from websites they visit. [...]
