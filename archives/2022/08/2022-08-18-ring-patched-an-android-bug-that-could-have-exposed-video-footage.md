Title: Ring patched an Android bug that could have exposed video footage
Date: 2022-08-18T15:38:52+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Amazon;computer vision;machine learning;ring;video surveillance
Slug: 2022-08-18-ring-patched-an-android-bug-that-could-have-exposed-video-footage

[Source](https://arstechnica.com/?p=1874519){:target="_blank" rel="noopener"}

> Enlarge / Ring camera images give you a view of what's happening and, in one security firm's experiments, a good base for machine learning surveillance. (credit: Ring) Amazon quietly but quickly patched a vulnerability in its Ring app that could have exposed users' camera recordings and other data, according to security firm Checkmarx. Checkmarx researchers write in a blog post that Ring's Android app, downloaded more than 10 million times, made an activity available to all other applications on Android devices. Ring's com.ring.nh.deeplink.DeepLinkActivity would execute any web content given to it, so long as the address included the text /better-neighborhoods/. [...]
