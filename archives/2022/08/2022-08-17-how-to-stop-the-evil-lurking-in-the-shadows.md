Title: How to stop the evil lurking in the shadows
Date: 2022-08-17T16:54:08+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-08-17-how-to-stop-the-evil-lurking-in-the-shadows

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/17/how_to_stop_the_evil/){:target="_blank" rel="noopener"}

> Webinar Barely a day goes by without news of a ransomware attack somewhere in the media. And these types of cyber security incident can seriously derail financial, social, health and industrial activity, inflicting massive damage and requiring a multiagency response in their aftermath.... [...]
