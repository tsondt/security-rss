Title: Ex-HP finance manager jailed after going on $5m spending spree using company plastic
Date: 2022-08-19T19:27:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-19-ex-hp-finance-manager-jailed-after-going-on-5m-spending-spree-using-company-plastic

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/19/hp_manager_fraud/){:target="_blank" rel="noopener"}

> Tesla sedan, 46 Chanel bags, 16 Rolexes, and more equals three years behind bars Now-former HP finance manager Shelbee Szeto has been sentenced to three years in prison and ordered to forfeit more than 250 luxury items after she blew $5m on herself using company credit cards.... [...]
