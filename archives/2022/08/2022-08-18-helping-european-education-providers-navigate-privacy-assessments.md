Title: Helping European education providers navigate privacy assessments
Date: 2022-08-18T11:29:00+00:00
Author: Marc Crandall
Category: GCP Security
Tags: Identity & Security
Slug: 2022-08-18-helping-european-education-providers-navigate-privacy-assessments

[Source](https://cloud.google.com/blog/products/identity-security/helping-european-education-providers-navigate-privacy-assessments/){:target="_blank" rel="noopener"}

> Every student and educator deserves access to learning tools that are private and secure. Google Workspace for Education and Chromebooks have positively transformed teaching and learning, while creating safe learning environments for more than 170 million students and educators around the world. Our education products are built with data protection at their core, enabling school administrators to demonstrate their privacy compliance when using our services. Before using the products and services of technology providers like Google, schools in Europe may be required by the EU’s General Data Protection Regulation (GDPR) or similar laws to conduct Data Protection Impact Assessments (DPIAs). [...]
