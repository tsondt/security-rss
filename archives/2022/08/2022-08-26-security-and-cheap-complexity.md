Title: Security and Cheap Complexity
Date: 2022-08-26T11:54:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;complexity;cybersecurity;hardware
Slug: 2022-08-26-security-and-cheap-complexity

[Source](https://www.schneier.com/blog/archives/2022/08/security-and-cheap-complexity.html){:target="_blank" rel="noopener"}

> I’ve been saying that complexity is the worst enemy of security for a long time now. ( Here’s me in 1999.) And it’s been true for a long time. In 2018, Thomas Dullien of Google’s Project Zero talked about “cheap complexity.” Andrew Appel summarizes : The anomaly of cheap complexity. For most of human history, a more complex device was more expensive to build than a simpler device. This is not the case in modern computing. It is often more cost-effective to take a very complicated device, and make it simulate simplicity, than to make a simpler device. This is [...]
