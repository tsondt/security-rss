Title: Twitter confirms zero-day used to expose data of 5.4 million accounts
Date: 2022-08-05T12:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-05-twitter-confirms-zero-day-used-to-expose-data-of-54-million-accounts

[Source](https://www.bleepingcomputer.com/news/security/twitter-confirms-zero-day-used-to-expose-data-of-54-million-accounts/){:target="_blank" rel="noopener"}

> Twitter has confirmed a recent data breach was caused by a now-patched zero-day vulnerability used to link email addresses and phone numbers to users' accounts, allowing a threat actor to compile a list of 5.4 million user account profiles. [...]
