Title: UK Parliament bins its TikTok account over China surveillance fears
Date: 2022-08-04T05:58:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-08-04-uk-parliament-bins-its-tiktok-account-over-china-surveillance-fears

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/04/uk_parliament_tiktok_account_discontinued/){:target="_blank" rel="noopener"}

> Plan to educate the children turned out to be a 'won't someone think of the children?' moment The UK's Parliament has ended its presence on TikTok after MPs pointed out the made-in-China social media service probably sends data about its users back to Beijing.... [...]
