Title: As Black Hat kicks off, the US government is getting the message on hiring security talent
Date: 2022-08-10T20:58:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-08-10-as-black-hat-kicks-off-the-us-government-is-getting-the-message-on-hiring-security-talent

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/10/us_security_hiring/){:target="_blank" rel="noopener"}

> Katie Moussouris tells it like it is Black Hat With the world's largest collection of security folk gathering in Las Vegas for the Black Hat conference there are encouraging signs that the US government might actually be getting smarter about hiring.... [...]
