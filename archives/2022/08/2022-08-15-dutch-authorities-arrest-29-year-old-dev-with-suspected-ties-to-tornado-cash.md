Title: Dutch authorities arrest 29-year-old dev with suspected ties to Tornado Cash
Date: 2022-08-15T17:31:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-15-dutch-authorities-arrest-29-year-old-dev-with-suspected-ties-to-tornado-cash

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/15/tornado_cash_suspect_arrested/){:target="_blank" rel="noopener"}

> The arrest comes days after US Treasury levies sanctions against the crypto mixing service Dutch authorities have arrested a software developer suspected of working with Tornado Cash, a cryptocurrency mixing service that only two days earlier was sanctioned by the US government for allegedly laundering money for ransomware operators and other cybercriminals.... [...]
