Title: FTC sues data broker that tracks locations of 125M phones per month
Date: 2022-08-29T19:06:47+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;data brokers;federal trade commission;FTC;privacy
Slug: 2022-08-29-ftc-sues-data-broker-that-tracks-locations-of-125m-phones-per-month

[Source](https://arstechnica.com/?p=1876747){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) The Federal Trade Commission on Monday sued a data broker for allegedly selling location data culled from hundreds of millions of phones that can be used to track the movements of people visiting abortion clinics, domestic abuse shelters, places of worship, and other sensitive places. In a complaint, the agency said that Idaho-based Kochava has promoted its marketplace as providing "rich geo data spanning billions of devices globally." The data broker has also said it "delivers raw latitude/longitude data with volumes around 94B+ geo transactions per month, 125 million monthly active users, and 35 million [...]
