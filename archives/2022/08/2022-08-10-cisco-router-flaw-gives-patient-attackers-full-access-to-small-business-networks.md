Title: Cisco router flaw gives patient attackers full access to small business networks
Date: 2022-08-10T12:52:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-10-cisco-router-flaw-gives-patient-attackers-full-access-to-small-business-networks

[Source](https://portswigger.net/daily-swig/cisco-router-flaw-gives-patient-attackers-full-access-to-small-business-networks){:target="_blank" rel="noopener"}

> Vulnerable path is reachable just once a day, but patches still need to be implemented as a matter of priority [...]
