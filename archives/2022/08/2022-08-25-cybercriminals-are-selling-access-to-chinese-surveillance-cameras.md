Title: Cybercriminals Are Selling Access to Chinese Surveillance Cameras
Date: 2022-08-25T18:47:15+00:00
Author: Nate Nelson
Category: Threatpost
Tags: IoT;Privacy;Vulnerabilities
Slug: 2022-08-25-cybercriminals-are-selling-access-to-chinese-surveillance-cameras

[Source](https://threatpost.com/cybercriminals-are-selling-access-to-chinese-surveillance-cameras/180478/){:target="_blank" rel="noopener"}

> Tens of thousands of cameras have failed to patch a critical, 11-month-old CVE, leaving thousands of organizations exposed. [...]
