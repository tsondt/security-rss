Title: Graph-based JavaScript bug scanner discovers more than 100 zero-day vulnerabilities in Node.js libraries
Date: 2022-08-30T11:13:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-30-graph-based-javascript-bug-scanner-discovers-more-than-100-zero-day-vulnerabilities-in-nodejs-libraries

[Source](https://portswigger.net/daily-swig/graph-based-javascript-bug-scanner-discovers-more-than-100-zero-day-vulnerabilities-in-node-js-libraries){:target="_blank" rel="noopener"}

> ODGen tool was presented at this year’s Usenix Security Symposium [...]
