Title: RTLS systems vulnerable to MiTM attacks, location manipulation
Date: 2022-08-16T16:10:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-16-rtls-systems-vulnerable-to-mitm-attacks-location-manipulation

[Source](https://www.bleepingcomputer.com/news/security/rtls-systems-vulnerable-to-mitm-attacks-location-manipulation/){:target="_blank" rel="noopener"}

> Security researchers have uncovered multiple vulnerabilities impacting UWB (ultra-wideband) RTLS (real-time locating systems), enabling threat actors to conduct man-in-the-middle attacks and manipulate tag geo-location data. [...]
