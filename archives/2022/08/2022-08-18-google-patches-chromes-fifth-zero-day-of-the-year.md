Title: Google Patches Chrome’s Fifth Zero-Day of the Year
Date: 2022-08-18T14:31:38+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security;Google Chrome;zero-day vulnerabilities
Slug: 2022-08-18-google-patches-chromes-fifth-zero-day-of-the-year

[Source](https://threatpost.com/google-patches-chromes-fifth-zero-day-of-the-year/180432/){:target="_blank" rel="noopener"}

> An insufficient validation input flaw, one of 11 patched in an update this week, could allow for arbitrary code execution and is under active attack. [...]
