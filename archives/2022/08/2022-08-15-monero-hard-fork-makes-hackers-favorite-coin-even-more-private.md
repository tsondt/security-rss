Title: Monero hard fork makes hackers' favorite coin even more private
Date: 2022-08-15T18:18:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-08-15-monero-hard-fork-makes-hackers-favorite-coin-even-more-private

[Source](https://www.bleepingcomputer.com/news/security/monero-hard-fork-makes-hackers-favorite-coin-even-more-private/){:target="_blank" rel="noopener"}

> Monero, the privacy-oriented decentralized cryptocurrency project, underwent a planned hard fork event on Saturday, introducing new features to boost its privacy and security. [...]
