Title: Introducing on-demand backup, schema extension support for Google Cloud’s Managed Microsoft AD
Date: 2022-08-29T16:00:00+00:00
Author: Muthuraj Thangavel
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-08-29-introducing-on-demand-backup-schema-extension-support-for-google-clouds-managed-microsoft-ad

[Source](https://cloud.google.com/blog/products/identity-security/announcing-google-cloud-managed-microsoft-ad-now-supports-schema-extension/){:target="_blank" rel="noopener"}

> Managed Service for Microsoft Active Directory (Managed Microsoft AD) is a Google Cloud service that offers highly available, hardened Microsoft Active Directory running on Windows virtual machines. We recently added on-demand backup and schema extension capabilities that can help Google Cloud users more easily and effectively manage AD tasks. Managed Microsoft AD is a fully managed service with automated AD server updates, maintenance, and security configuration, and needs no hardware management or patching. The service is constantly evolving, adding new capabilities to effectively manage your cloud-based, AD-dependent workloads. Here’s a closer look at the benefits for Google Cloud users of [...]
