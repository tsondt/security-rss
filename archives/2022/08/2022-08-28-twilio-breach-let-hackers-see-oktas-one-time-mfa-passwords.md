Title: Twilio breach let hackers see Okta's one-time MFA passwords
Date: 2022-08-28T13:15:05-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-28-twilio-breach-let-hackers-see-oktas-one-time-mfa-passwords

[Source](https://www.bleepingcomputer.com/news/security/twilio-breach-let-hackers-see-oktas-one-time-mfa-passwords/){:target="_blank" rel="noopener"}

> The threat actor behind the Twilio hack used their access to steal one-time passwords (OTPs) delivered over SMS to from customers of Okta identity and access management company. [...]
