Title: Spyware developer charged by Australian Police after 14,500 sales
Date: 2022-08-01T00:30:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-08-01-spyware-developer-charged-by-australian-police-after-14500-sales

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/01/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> PLUS: India open to space tourism; China/Indonesia infosec pact; Paytm denies breach; Infosys dodges government again; and more Asia In Brief Australia's federal police (AFP) on Friday charged a man with creating and profiting from spyware that allowed total remote control of victims' computers.... [...]
