Title: Authentication bypass bug in Nextauth.js could allow email account takeover
Date: 2022-08-05T12:25:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-05-authentication-bypass-bug-in-nextauthjs-could-allow-email-account-takeover

[Source](https://portswigger.net/daily-swig/authentication-bypass-bug-in-nextauth-js-could-allow-email-account-takeover){:target="_blank" rel="noopener"}

> Vulnerability has been patched in latest versions [...]
