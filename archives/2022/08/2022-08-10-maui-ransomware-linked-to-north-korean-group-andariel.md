Title: Maui ransomware linked to North Korean group Andariel
Date: 2022-08-10T18:14:40+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-10-maui-ransomware-linked-to-north-korean-group-andariel

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/10/maui_ransomware_andariel/){:target="_blank" rel="noopener"}

> Attack origins point to April 2021 first strike on Japanese target The Maui ransomware that has been used against US healthcare operations has been linked to Andariel, a North Korean state-sponsored threat with links to the notorious Lazarus Group.... [...]
