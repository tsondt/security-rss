Title: Facebook’s In-app Browser on iOS Tracks ‘Anything You Do on Any Website’
Date: 2022-08-12T13:24:03+00:00
Author: Threatpost
Category: Threatpost
Tags: Privacy
Slug: 2022-08-12-facebooks-in-app-browser-on-ios-tracks-anything-you-do-on-any-website

[Source](https://threatpost.com/facebook-ios-tracks-anything/180395/){:target="_blank" rel="noopener"}

> Researcher shows how Instagram and Facebook’s use of an in-app browser within both its iOS apps can track interactions with external websites. [...]
