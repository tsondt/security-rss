Title: Expanded eligibility for the free MFA security key program
Date: 2022-08-24T15:05:01+00:00
Author: CJ Moses
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;authentication;AWS Identity;free security keys;MFA security keys;passwords;Security Blog;two factor authentication
Slug: 2022-08-24-expanded-eligibility-for-the-free-mfa-security-key-program

[Source](https://aws.amazon.com/blogs/security/expanded-eligibility-for-the-free-mfa-security-key-program/){:target="_blank" rel="noopener"}

> Since the broad launch of our multi-factor authentication (MFA) security key program, customers have been enthusiastic about the program and how they will use it to improve their organizations’ security posture. Given the level of interest, we’re expanding eligibility for the program to allow more US-based AWS account root users and payer accounts to take [...]
