Title: Novant Health admits leak of 1.3m patients' info to Facebook
Date: 2022-08-22T22:00:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-22-novant-health-admits-leak-of-13m-patients-info-to-facebook

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/22/novant_meta_data/){:target="_blank" rel="noopener"}

> But don't worry, Zuck would never misuse this type of sensitive data Novant Health confirmed that it may have disclosed 1.3 million patients' sensitive data, including email addresses, phone numbers, financial information - even doctor's appointment details - to Meta.... [...]
