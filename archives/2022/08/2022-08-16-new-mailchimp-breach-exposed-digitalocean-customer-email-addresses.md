Title: New MailChimp breach exposed DigitalOcean customer email addresses
Date: 2022-08-16T12:46:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-16-new-mailchimp-breach-exposed-digitalocean-customer-email-addresses

[Source](https://www.bleepingcomputer.com/news/security/new-mailchimp-breach-exposed-digitalocean-customer-email-addresses/){:target="_blank" rel="noopener"}

> DigitalOcean is warning customers that a recent MailChimp security breach exposed the email addresses of some customers, with a small number receiving unauthorized password resets. [...]
