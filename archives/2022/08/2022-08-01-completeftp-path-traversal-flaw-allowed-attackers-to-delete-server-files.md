Title: CompleteFTP path traversal flaw allowed attackers to delete server files
Date: 2022-08-01T13:14:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-01-completeftp-path-traversal-flaw-allowed-attackers-to-delete-server-files

[Source](https://portswigger.net/daily-swig/completeftp-path-traversal-flaw-allowed-attackers-to-delete-server-files){:target="_blank" rel="noopener"}

> Security issue fixed in version 22.1.1 of file transfer software [...]
