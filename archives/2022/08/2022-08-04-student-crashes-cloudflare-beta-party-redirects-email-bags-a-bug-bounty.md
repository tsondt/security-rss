Title: Student crashes Cloudflare beta party, redirects email, bags a bug bounty
Date: 2022-08-04T06:31:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-04-student-crashes-cloudflare-beta-party-redirects-email-bags-a-bug-bounty

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/04/cloudflare_email_beta_bug/){:target="_blank" rel="noopener"}

> Simple to exploit, enough to pocket $3,000 A Danish ethical hacker was able to work his way uninvited into a closed Cloudflare beta and found a vulnerability that could have been exploited by a cybercriminal to hijack and steal someone else's email.... [...]
