Title: Slack resets passwords after exposing hashes in invitation links
Date: 2022-08-05T13:44:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-05-slack-resets-passwords-after-exposing-hashes-in-invitation-links

[Source](https://www.bleepingcomputer.com/news/security/slack-resets-passwords-after-exposing-hashes-in-invitation-links/){:target="_blank" rel="noopener"}

> Slack notified roughly 0.5% of its users that it reset their passwords after fixing a bug exposing salted password hashes when creating or revoking shared invitation links for workspaces. [...]
