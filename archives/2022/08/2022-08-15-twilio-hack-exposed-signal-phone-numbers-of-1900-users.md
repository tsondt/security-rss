Title: Twilio hack exposed Signal phone numbers of 1,900 users
Date: 2022-08-15T17:46:24-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-15-twilio-hack-exposed-signal-phone-numbers-of-1900-users

[Source](https://www.bleepingcomputer.com/news/security/twilio-hack-exposed-signal-phone-numbers-of-1-900-users/){:target="_blank" rel="noopener"}

> Phone numbers of close to 1,900 Signal users were exposed in the data breach Twilio cloud communications company suffered at the beginning of the month. [...]
