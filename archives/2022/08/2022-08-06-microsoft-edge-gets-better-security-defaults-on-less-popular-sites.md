Title: Microsoft Edge gets better security defaults on less popular sites
Date: 2022-08-06T11:12:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-08-06-microsoft-edge-gets-better-security-defaults-on-less-popular-sites

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-edge-gets-better-security-defaults-on-less-popular-sites/){:target="_blank" rel="noopener"}

> Microsoft is rolling out a new update to the Microsoft Edge Stable Channel over the coming days to improve the web browser's security defaults when visiting less popular websites. [...]
