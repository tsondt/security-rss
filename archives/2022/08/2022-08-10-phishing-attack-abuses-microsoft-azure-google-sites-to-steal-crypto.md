Title: Phishing attack abuses Microsoft Azure, Google Sites to steal crypto
Date: 2022-08-10T12:50:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-08-10-phishing-attack-abuses-microsoft-azure-google-sites-to-steal-crypto

[Source](https://www.bleepingcomputer.com/news/security/phishing-attack-abuses-microsoft-azure-google-sites-to-steal-crypto/){:target="_blank" rel="noopener"}

> A new large-scale phishing campaign targeting Coinbase, MetaMask, Kraken, and Gemini users is abusing Google Sites and Microsoft Azure Web App to create fraudulent sites. [...]
