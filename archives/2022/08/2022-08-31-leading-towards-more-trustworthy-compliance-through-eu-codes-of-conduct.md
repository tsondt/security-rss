Title: Leading towards more trustworthy compliance through EU Codes of Conduct
Date: 2022-08-31T16:00:00+00:00
Author: Marc Crandall
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-08-31-leading-towards-more-trustworthy-compliance-through-eu-codes-of-conduct

[Source](https://cloud.google.com/blog/products/identity-security/how-google-is-leading-towards-more-trustworthy-compliance-through-eu-codes-of-conduct/){:target="_blank" rel="noopener"}

> Google is committed to be the best possible place for sustainable digital transformation for European organizations. Our Cloud on Europe’s terms initiative works to meet regional requirements for security, privacy, and digital sovereignty, without compromising on functionality or innovation. In support of this initiative, we are making our annual declaration of adherence to two important EU codes of conduct for cloud service providers: the SWIPO Code of Conduct and the EU Cloud Code of Conduct. We believe that codes of conduct are effective collaboration instruments among service providers and data protection authorities, where state-of-the-art industry practices can be tailored to [...]
