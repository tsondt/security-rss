Title: Thousands of hackers flock to 'Dark Utilities' C2-as-a-Service
Date: 2022-08-04T15:00:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-04-thousands-of-hackers-flock-to-dark-utilities-c2-as-a-service

[Source](https://www.bleepingcomputer.com/news/security/thousands-of-hackers-flock-to-dark-utilities-c2-as-a-service/){:target="_blank" rel="noopener"}

> Security researchers found a new service called Dark Utilities that provides an easy and inexpensive way for cybercriminals to set up a command and control (C2) center for their malicious operations. [...]
