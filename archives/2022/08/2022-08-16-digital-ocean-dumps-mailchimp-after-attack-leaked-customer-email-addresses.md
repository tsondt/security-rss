Title: Digital Ocean dumps Mailchimp after attack leaked customer email addresses
Date: 2022-08-16T05:31:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-08-16-digital-ocean-dumps-mailchimp-after-attack-leaked-customer-email-addresses

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/16/digital_ocean_dumps_mailchimp/){:target="_blank" rel="noopener"}

> Somebody went after crypto-centric companies’ outsourced email but the damage was felt in the cloud Junior cloud Digital Ocean has revealed that some of its clients’ email addresses were exposed to attackers, thanks to an attack on email marketing service Mailchimp.... [...]
