Title: iOS VPNs have leaked traffic for more than 2 years, researcher claims
Date: 2022-08-17T16:32:07+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;apple;iOS;Michael horowitz;privacy;proton;protonvpn;VPN
Slug: 2022-08-17-ios-vpns-have-leaked-traffic-for-more-than-2-years-researcher-claims

[Source](https://arstechnica.com/?p=1874198){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A security researcher says that Apple's iOS devices don't fully route all network traffic through VPNs as a user might expect, a potential security issue the device maker has known about for years. Michael Horowitz, a longtime computer security blogger and researcher, puts it plainly—if contentiously—in a continually updated blog post. "VPNs on iOS are broken," he says. Any third-party VPN seems to work at first, giving the device a new IP address, DNS servers, and a tunnel for new traffic, Horowitz writes. But sessions and connections established before a VPN is activated do not terminate [...]
