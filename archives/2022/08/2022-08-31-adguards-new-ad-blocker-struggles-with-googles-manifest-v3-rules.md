Title: AdGuard’s new ad blocker struggles with Google’s Manifest v3 rules
Date: 2022-08-31T13:52:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-31-adguards-new-ad-blocker-struggles-with-googles-manifest-v3-rules

[Source](https://www.bleepingcomputer.com/news/security/adguard-s-new-ad-blocker-struggles-with-google-s-manifest-v3-rules/){:target="_blank" rel="noopener"}

> AdGuard has published the first ad blocker extension for Chrome that is compatible with Manifest V3, Google's newest extension platform protocol for the world's most popular web browser. [...]
