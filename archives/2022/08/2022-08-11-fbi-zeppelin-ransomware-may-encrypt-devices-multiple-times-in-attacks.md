Title: FBI: Zeppelin ransomware may encrypt devices multiple times in attacks
Date: 2022-08-11T12:54:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-11-fbi-zeppelin-ransomware-may-encrypt-devices-multiple-times-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-zeppelin-ransomware-may-encrypt-devices-multiple-times-in-attacks/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) and the Federal Bureau of Investigation (FBI) warned US organizations today that attackers deploying Zeppelin ransomware might encrypt their files multiple times. [...]
