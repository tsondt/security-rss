Title: Email marketing firm hacked to steal crypto-focused mailing lists
Date: 2022-08-08T14:17:51-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-08-email-marketing-firm-hacked-to-steal-crypto-focused-mailing-lists

[Source](https://www.bleepingcomputer.com/news/security/email-marketing-firm-hacked-to-steal-crypto-focused-mailing-lists/){:target="_blank" rel="noopener"}

> Email marketing firm Klaviyo disclosed a data breach after threat actors gained access to internal systems and downloaded marketing lists for cryptocurrency-related customers. [...]
