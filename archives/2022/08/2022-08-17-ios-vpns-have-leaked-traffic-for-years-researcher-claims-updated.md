Title: iOS VPNs have leaked traffic for years, researcher claims [Updated]
Date: 2022-08-17T16:32:07+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;apple;iOS;Michael horowitz;privacy;proton;protonvpn;VPN
Slug: 2022-08-17-ios-vpns-have-leaked-traffic-for-years-researcher-claims-updated

[Source](https://arstechnica.com/?p=1874198){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) (Update, Aug. 18, 2:40 p.m.: Proton founder and CEO Andy Yen said in a statement: "The fact that this is still an issue is disappointing to say the least. We first notified Apple privately of this issue two years ago. Apple declined to fix the issue, which is why we disclosed the vulnerability to protect the public. Millions of people’s security is in Apple’s hands, they are the only ones who can fix the issue, but given the lack of action for the past two years, we are not very optimistic Apple will do the right [...]
