Title: Making the cloud a safer place with SANS
Date: 2022-08-11T11:01:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-08-11-making-the-cloud-a-safer-place-with-sans

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/making_the_cloud_a_safer/){:target="_blank" rel="noopener"}

> Get advice from experts on how to nail cloud native security in a multi-cloud world Sponsored Post Protecting sensitive data and mission critical applications spread across multiple on- and off-prem cloud environments and different service providers is a tough gig for busy security professionals. So a chance to hear from experts and peers on how best to stop hackers from making hay will be welcome.... [...]
