Title: Levels of Assurance for DoD Microelectronics
Date: 2022-08-29T14:30:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-08-29-levels-of-assurance-for-dod-microelectronics

[Source](https://www.schneier.com/blog/archives/2022/08/levels-of-assurance-for-dod-microelectronics.html){:target="_blank" rel="noopener"}

> The NSA has has published criteria for evaluating levels of assurance required for DoD microelectronics. The introductory report in a DoD microelectronics series outlines the process for determining levels of hardware assurance for systems and custom microelectronic components, which include application-specific integrated circuits (ASICs), field programmable gate arrays (FPGAs) and other devices containing reprogrammable digital logic. The levels of hardware assurance are determined by the national impact caused by failure or subversion of the top-level system and the criticality of the component to that top-level system. The guidance helps programs acquire a better understanding of their system and components so [...]
