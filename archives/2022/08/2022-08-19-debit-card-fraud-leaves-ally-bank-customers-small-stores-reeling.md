Title: Debit card fraud leaves Ally Bank customers, small stores reeling
Date: 2022-08-19T16:44:35+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Features;ally bank;credit card fraud;debit card fraud;debit cards;fraud;shopify;Stripe
Slug: 2022-08-19-debit-card-fraud-leaves-ally-bank-customers-small-stores-reeling

[Source](https://arstechnica.com/?p=1874768){:target="_blank" rel="noopener"}

> Enlarge / Ally debit card owners are reporting fraudulent charges at a steady cadence over the past week. (credit: Getty Images) Ben Langhofer, a financial planner and single father of three in Wichita, Kansas, decided to start a side business. He had made a handbook for his family, laying out core values, a mission statement, and a constitution. He wanted to help other families put their beliefs into a real book, one they could hold and display. So Langhofer hired web developers about two years ago and set up a website, customer relationship management system, and payment processing. On Father's [...]
