Title: Man-in-the-Middle Phishing Attack
Date: 2022-08-25T11:45:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;man-in-the-middle attacks;phishing;scams;two-factor authentication
Slug: 2022-08-25-man-in-the-middle-phishing-attack

[Source](https://www.schneier.com/blog/archives/2022/08/man-in-the-middle-phishing-attack.html){:target="_blank" rel="noopener"}

> Here’s a phishing campaign that uses a man-in-the-middle attack to defeat multi-factor authentication: Microsoft observed a campaign that inserted an attacker-controlled proxy site between the account users and the work server they attempted to log into. When the user entered a password into the proxy site, the proxy site sent it to the real server and then relayed the real server’s response back to the user. Once the authentication was completed, the threat actor stole the session cookie the legitimate site sent, so the user doesn’t need to be reauthenticated at every new page visited. The campaign began with a [...]
