Title: How to incorporate ACM PCA into your existing Windows Active Directory Certificate Services
Date: 2022-08-10T19:37:17+00:00
Author: Geoff Sweet
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;ACM Private CA;Active-Directory-Certificate-Services private-certificates;Security Blog
Slug: 2022-08-10-how-to-incorporate-acm-pca-into-your-existing-windows-active-directory-certificate-services

[Source](https://aws.amazon.com/blogs/security/how-to-incorporate-acm-pca-into-your-existing-windows-active-directory-certificate-services/){:target="_blank" rel="noopener"}

> Using certificates to authenticate and encrypt data is vital to any enterprise security. For example, companies rely on certificates to provide TLS encryption for web applications so that client data is protected. However, not all certificates need to be issued from a publicly trusted certificate authority (CA). A privately trusted CA can be leveraged to [...]
