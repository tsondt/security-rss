Title: LockBit claims ransomware attack on security giant Entrust, leaks data
Date: 2022-08-18T19:06:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-18-lockbit-claims-ransomware-attack-on-security-giant-entrust-leaks-data

[Source](https://www.bleepingcomputer.com/news/security/lockbit-claims-ransomware-attack-on-security-giant-entrust-leaks-data/){:target="_blank" rel="noopener"}

> The LockBit ransomware gang has claimed responsibility for the June cyberattack on digital security giant Entrust. [...]
