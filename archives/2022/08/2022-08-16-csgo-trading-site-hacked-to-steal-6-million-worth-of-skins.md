Title: CS:GO trading site hacked to steal $6 million worth of skins
Date: 2022-08-16T09:59:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2022-08-16-csgo-trading-site-hacked-to-steal-6-million-worth-of-skins

[Source](https://www.bleepingcomputer.com/news/security/cs-go-trading-site-hacked-to-steal-6-million-worth-of-skins/){:target="_blank" rel="noopener"}

> CS.MONEY, one of the largest platforms for trading CS:GO skins, has taken its website offline after a cyberattack allowed hackers to loot 20,000 items worth approximately $6,000,000. [...]
