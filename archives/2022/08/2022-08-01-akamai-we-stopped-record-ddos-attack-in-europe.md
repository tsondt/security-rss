Title: Akamai: We stopped record DDoS attack in Europe
Date: 2022-08-01T07:27:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-01-akamai-we-stopped-record-ddos-attack-in-europe

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/01/ddos_europe_akamai/){:target="_blank" rel="noopener"}

> A 'sophisticated, global botnet' held an Eastern European biz under siege over 30 days Akamai Technologies squelched the largest-ever distributed denial-of-service (DDoS) attack in Europe earlier this month against a company that was being consistently hammered over a 30-day period.... [...]
