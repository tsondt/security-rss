Title: APIC fail: Intel 'Sunny Cove' chips with SGX spill secrets
Date: 2022-08-09T17:00:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-09-apic-fail-intel-sunny-cove-chips-with-sgx-spill-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/09/intel_sunny_cove/){:target="_blank" rel="noopener"}

> AMD Zen chips, meanwhile, are vulnerable to side-channel data scrying A group of computer scientists has identified an architectural error in certain recent Intel CPUs that can be abused to expose SGX enclave data like private encryption keys.... [...]
