Title: Smartphone gyroscopes threaten air-gapped systems, researcher finds
Date: 2022-08-23T18:00:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-08-23-smartphone-gyroscopes-threaten-air-gapped-systems-researcher-finds

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/23/phone_gyroscopes_airgapped_systems/){:target="_blank" rel="noopener"}

> Network interface card LEDs are a risk too by blinking in Morse code An Israeli security researcher known for foiling air gap security measures has published a reminder of just how vulnerable the approaches are to both visual and ultrasonic threats.... [...]
