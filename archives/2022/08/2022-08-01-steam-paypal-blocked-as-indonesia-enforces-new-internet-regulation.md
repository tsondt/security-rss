Title: Steam, PayPal blocked as Indonesia enforces new Internet regulation
Date: 2022-08-01T13:09:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-01-steam-paypal-blocked-as-indonesia-enforces-new-internet-regulation

[Source](https://www.bleepingcomputer.com/news/security/steam-paypal-blocked-as-indonesia-enforces-new-internet-regulation/){:target="_blank" rel="noopener"}

> The Indonesian Ministry of Communication and Information Technology, Kominfo, is now blocking access to internet service and content providers who had not registered on the country's new licensing platform by July 27th, 2022, as the country begins to restrict access to online content providers and services. [...]
