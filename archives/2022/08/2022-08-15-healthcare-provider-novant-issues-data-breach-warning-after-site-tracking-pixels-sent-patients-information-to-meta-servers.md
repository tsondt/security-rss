Title: Healthcare provider Novant issues data breach warning after site tracking pixels sent patients’  information to Meta servers
Date: 2022-08-15T12:31:44+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-15-healthcare-provider-novant-issues-data-breach-warning-after-site-tracking-pixels-sent-patients-information-to-meta-servers

[Source](https://portswigger.net/daily-swig/healthcare-provider-novant-issues-data-breach-warning-after-site-tracking-pixels-sent-patients-information-to-meta-servers){:target="_blank" rel="noopener"}

> Leaked data potentially included patients’ email addresses, phone numbers, and device IP addresses [...]
