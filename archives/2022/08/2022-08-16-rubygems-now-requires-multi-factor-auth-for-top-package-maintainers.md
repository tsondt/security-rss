Title: RubyGems now requires multi-factor auth for top package maintainers
Date: 2022-08-16T23:17:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-16-rubygems-now-requires-multi-factor-auth-for-top-package-maintainers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/16/rubygems_package_registry_mfa/){:target="_blank" rel="noopener"}

> Sign-on you crazy diamond RubyGems.org, the Ruby programming community's software package registry, now requires maintainers of popular "gems" to secure their accounts using multi-factor authentication (MFA).... [...]
