Title: $23 Million YouTube Royalties Scam
Date: 2022-08-15T14:14:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;copyright;fraud;music;scams
Slug: 2022-08-15-23-million-youtube-royalties-scam

[Source](https://www.schneier.com/blog/archives/2022/08/23-million-youtube-royalties-scam.html){:target="_blank" rel="noopener"}

> Scammers were able to convince YouTube that other peoples’ music was their own. They successfully stole $23 million before they were caught. No one knows how common this scam is, and how much money total is being stolen in this way. Presumably this is not an uncommon fraud. While the size of the heist and the breadth of the scheme may be very unique, it’s certainly a situation that many YouTube content creators have faced before. YouTube’s Content ID system, meant to help creators, has been weaponized by bad faith actors in order to make money off content that isn’t [...]
