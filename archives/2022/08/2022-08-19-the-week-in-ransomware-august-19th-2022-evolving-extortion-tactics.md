Title: The Week in Ransomware - August 19th 2022 - Evolving extortion tactics
Date: 2022-08-19T19:08:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-19-the-week-in-ransomware-august-19th-2022-evolving-extortion-tactics

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-august-19th-2022-evolving-extortion-tactics/){:target="_blank" rel="noopener"}

> Bringing you the latest ransomware news, including new research, tactics, and cyberattacks. We also saw the return of the BlackByte ransomware operation, who has started to use new extortion tactics. [...]
