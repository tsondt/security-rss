Title: Sounding the Alarm on Emergency Alert System Flaws
Date: 2022-08-12T15:26:58+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;Comcast;Cybir;David McGuire;defcon;Department of Homeland Security;Digital Alert Systems;Emergency Alert System;Ken Pyle;Monroe Electronics
Slug: 2022-08-12-sounding-the-alarm-on-emergency-alert-system-flaws

[Source](https://krebsonsecurity.com/2022/08/sounding-the-alarm-on-emergency-alert-system-flaws/){:target="_blank" rel="noopener"}

> The Department of Homeland Security (DHS) is urging states and localities to beef up security around proprietary devices that connect to the Emergency Alert System — a national public warning system used to deliver important emergency information, such as severe weather and AMBER alerts. The DHS warning came in advance of a workshop to be held this weekend at the DEFCON security conference in Las Vegas, where a security researcher is slated to demonstrate multiple weaknesses in the nationwide alert system. A Digital Alert Systems EAS encoder/decoder that Pyle said he acquired off eBay in 2019. It had the username [...]
