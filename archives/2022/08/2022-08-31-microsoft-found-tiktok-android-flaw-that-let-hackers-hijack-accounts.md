Title: Microsoft found TikTok Android flaw that let hackers hijack accounts
Date: 2022-08-31T12:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-08-31-microsoft-found-tiktok-android-flaw-that-let-hackers-hijack-accounts

[Source](https://www.bleepingcomputer.com/news/security/microsoft-found-tiktok-android-flaw-that-let-hackers-hijack-accounts/){:target="_blank" rel="noopener"}

> Microsoft found and reported a high severity flaw in the TikTok Android app in February that allowed attackers to "quickly and quietly" take over accounts with one click by tricking targets into clicking a specially crafted malicious link. [...]
