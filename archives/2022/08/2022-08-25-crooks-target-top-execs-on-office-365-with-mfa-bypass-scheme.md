Title: Crooks target top execs on Office 365 with MFA-bypass scheme
Date: 2022-08-25T18:01:53+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-25-crooks-target-top-execs-on-office-365-with-mfa-bypass-scheme

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/25/microsoft_365_bec/){:target="_blank" rel="noopener"}

> 'Widespread' campaign hunts for multimillion-dollar transactions A business email compromise scheme targeting CEOs and CFOs using Microsoft Office 365 combines phishing with a man-in-the-middle attack to defeat multi-factor authentication.... [...]
