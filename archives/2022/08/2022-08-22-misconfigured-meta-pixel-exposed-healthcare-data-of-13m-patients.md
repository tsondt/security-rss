Title: Misconfigured Meta Pixel exposed healthcare data of 1.3M patients
Date: 2022-08-22T14:16:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-22-misconfigured-meta-pixel-exposed-healthcare-data-of-13m-patients

[Source](https://www.bleepingcomputer.com/news/security/misconfigured-meta-pixel-exposed-healthcare-data-of-13m-patients/){:target="_blank" rel="noopener"}

> U.S. healthcare provider Novant Health has disclosed a data breach impacting 1,362,296 individuals who have had their sensitive information mistakenly collected by the Meta Pixel ad tracking script. [...]
