Title: DoorDash discloses new data breach tied to Twilio hackers
Date: 2022-08-26T15:30:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-26-doordash-discloses-new-data-breach-tied-to-twilio-hackers

[Source](https://www.bleepingcomputer.com/news/security/doordash-discloses-new-data-breach-tied-to-twilio-hackers/){:target="_blank" rel="noopener"}

> Food delivery firm DoorDash has disclosed a data breach exposing customer and employee data that is linked to the recent cyberattack on Twilio. [...]
