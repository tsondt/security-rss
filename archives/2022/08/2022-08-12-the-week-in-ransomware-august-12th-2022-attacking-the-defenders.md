Title: The Week in Ransomware - August 12th 2022 - Attacking the defenders
Date: 2022-08-12T19:19:19-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-12-the-week-in-ransomware-august-12th-2022-attacking-the-defenders

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-august-12th-2022-attacking-the-defenders/){:target="_blank" rel="noopener"}

> It was a very busy week for ransomware news and attacks, especially with the disclosure that Cisco was breached by a threat actor affiliated with the Yanluowang ransomware gang. [...]
