Title: UK NHS service recovery may take a month after MSP ransomware attack
Date: 2022-08-11T12:18:40-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-11-uk-nhs-service-recovery-may-take-a-month-after-msp-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/uk-nhs-service-recovery-may-take-a-month-after-msp-ransomware-attack/){:target="_blank" rel="noopener"}

> Managed service provider (MSP) Advanced confirmed that a ransomware attack on its systems caused the disruption of emergency services (111) from the United Kingdom's National Health Service (NHS). [...]
