Title: Miscreants aim to cause Discord discord with malicious npm packages
Date: 2022-08-02T09:31:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-02-miscreants-aim-to-cause-discord-discord-with-malicious-npm-packages

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/02/npm_lofylife_discord_kaspersky/){:target="_blank" rel="noopener"}

> LofyLife campaign comes amid GitHub security lockdown Cybercriminals continue to use npm packages to drop malicious packages on unsuspecting victims, most recently to steal Discord login tokens, bank card data, and other user information from infected systems.... [...]
