Title: PyPI warns of first-ever phishing campaign against its users
Date: 2022-08-26T19:21:03+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-26-pypi-warns-of-first-ever-phishing-campaign-against-its-users

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/26/pypi_warns_of_firstever_phishing/){:target="_blank" rel="noopener"}

> On the bright side, top devs are getting hardware security keys The Python Package Index, better known among developers as PyPI, has issued a warning about a phishing attack targeting developers who use the service.... [...]
