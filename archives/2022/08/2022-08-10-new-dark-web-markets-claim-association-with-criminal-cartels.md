Title: New dark web markets claim association with criminal cartels
Date: 2022-08-10T19:12:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-10-new-dark-web-markets-claim-association-with-criminal-cartels

[Source](https://www.bleepingcomputer.com/news/security/new-dark-web-markets-claim-association-with-criminal-cartels/){:target="_blank" rel="noopener"}

> Several new marketplaces have appeared on the dark web, claiming to be the dedicated online portals for notorious criminal cartels from Mexico. [...]
