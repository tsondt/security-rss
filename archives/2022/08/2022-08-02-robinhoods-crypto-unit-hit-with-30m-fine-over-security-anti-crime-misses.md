Title: Robinhood's crypto unit hit with $30m fine over security, anti-crime misses
Date: 2022-08-02T19:42:48+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-02-robinhoods-crypto-unit-hit-with-30m-fine-over-security-anti-crime-misses

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/02/robinhoods_crypto_penalty/){:target="_blank" rel="noopener"}

> And just lays off about a quarter of staff Updated Robinhood's cryptocurrency operations has been formally fined $30 million for violating New York's anti-money-laundering and cybersecurity regulations.... [...]
