Title: Higher risks and premiums are creating critical gap in cyber insurance
Date: 2022-08-11T23:03:21+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-11-higher-risks-and-premiums-are-creating-critical-gap-in-cyber-insurance

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/insurance_ransomware_blackberry/){:target="_blank" rel="noopener"}

> Most organizations don’t have the financial resources necessary to address ransomware and other cyberattacks, BlackBerry says Black Hat Many organizations are increasingly unprepared to deal with the skyrocketing costs of a ransomware attacks, at a time when the number of incidents and the payments demanded by cybercriminals are rising rapidly.... [...]
