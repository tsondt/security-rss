Title: Russian hackers target Ukraine with default Word template hijacker
Date: 2022-08-15T12:39:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-15-russian-hackers-target-ukraine-with-default-word-template-hijacker

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-target-ukraine-with-default-word-template-hijacker/){:target="_blank" rel="noopener"}

> Threat analysts monitoring cyberattacks on Ukraine report that the operations of the notorious Russian state-backed hacking group 'Gamaredon' continue to heavily target the war-torn country. [...]
