Title: Indian military ready to put long-range quantum key distribution on the line
Date: 2022-08-15T06:56:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-08-15-indian-military-ready-to-put-long-range-quantum-key-distribution-on-the-line

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/15/indian_military_qkd/){:target="_blank" rel="noopener"}

> Local startup can make it happen over 150km India's military has celebrated the nation's Independence Day by announcing it will adopt locally developed quantum key distribution (QKD)technology that can operate across distances of 150km.... [...]
