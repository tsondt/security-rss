Title: After 7 years, long-term threat DarkTortilla crypter is still evolving
Date: 2022-08-17T18:41:18+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-17-after-7-years-long-term-threat-darktortilla-crypter-is-still-evolving

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/17/darktortilla_crypter_malware_secureworks/){:target="_blank" rel="noopener"}

> .NET-based malware can push wide range of malicious payloads, and evades detection, Secureworks says A highly pervasive.NET-based crypter that has flown under the radar since about 2015 and can deliver a wide range of malicious payloads continues to evolve rapidly, with almost 10,000 code samples being uploaded to VirusTotal over a 16-month period.... [...]
