Title: Taiwanese military reports DDoS in wake of Pelosi visit
Date: 2022-08-04T12:23:31+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-08-04-taiwanese-military-reports-ddos-in-wake-of-pelosi-visit

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/04/taiwanese_military_reports_ddos_in/){:target="_blank" rel="noopener"}

> Controversial visit to Taiwan continues to reverberate through cyberspace, the real world, and the semiconductor industry Taiwan's Ministry of National Defense confirmed it was hit by a DDoS attack on Wednesday in what has been an eventful week for the island nation, US-Sino relations, and semiconductors.... [...]
