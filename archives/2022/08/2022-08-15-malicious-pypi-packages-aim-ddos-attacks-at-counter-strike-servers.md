Title: Malicious PyPi packages aim DDoS attacks at Counter-Strike servers
Date: 2022-08-15T18:03:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-15-malicious-pypi-packages-aim-ddos-attacks-at-counter-strike-servers

[Source](https://www.bleepingcomputer.com/news/security/malicious-pypi-packages-aim-ddos-attacks-at-counter-strike-servers/){:target="_blank" rel="noopener"}

> A dozen malicious Python packages were uploaded to the PyPi repository this weekend in a typosquatting attack that performs DDoS attacks on a Counter-Strike 1.6 server. [...]
