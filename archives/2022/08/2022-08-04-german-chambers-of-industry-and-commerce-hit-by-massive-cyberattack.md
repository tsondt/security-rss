Title: German Chambers of Industry and Commerce hit by 'massive' cyberattack
Date: 2022-08-04T10:06:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-04-german-chambers-of-industry-and-commerce-hit-by-massive-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/german-chambers-of-industry-and-commerce-hit-by-massive-cyberattack/){:target="_blank" rel="noopener"}

> The Association of German Chambers of Industry and Commerce (DIHK) was forced to shut down all of its IT systems and switch off digital services, telephones, and email servers, in response to a cyberattack. [...]
