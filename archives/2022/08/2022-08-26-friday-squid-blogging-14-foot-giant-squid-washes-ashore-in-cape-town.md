Title: Friday Squid Blogging: 14-foot Giant Squid Washes Ashore in Cape Town
Date: 2022-08-26T21:08:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-08-26-friday-squid-blogging-14-foot-giant-squid-washes-ashore-in-cape-town

[Source](https://www.schneier.com/blog/archives/2022/08/friday-squid-blogging-14-foot-giant-squid-washes-ashore-in-cape-town.html){:target="_blank" rel="noopener"}

> It’s an Architeuthis dux, the second this year. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
