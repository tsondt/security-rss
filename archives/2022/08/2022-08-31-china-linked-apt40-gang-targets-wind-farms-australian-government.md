Title: China-linked APT40 gang targets wind farms, Australian government
Date: 2022-08-31T05:02:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-08-31-china-linked-apt40-gang-targets-wind-farms-australian-government

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/31/chinanexus_apt40_targeting_australian_government/){:target="_blank" rel="noopener"}

> ScanBox installed after victims lured to fake Murdoch news sites with phishing emails Researchers at security company Proofpoint and PricewaterhouseCoopers (PWC) said on Tuesday they had identified a cyber espionage campaign that delivers the ScanBox exploitation framework through a malicious fake Australian news site.... [...]
