Title: SEC says brokerage accounts hijacked for $1.3m pump-and-dump scam
Date: 2022-08-16T21:25:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-16-sec-says-brokerage-accounts-hijacked-for-13m-pump-and-dump-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/16/sec_hacking_fraud_charges/){:target="_blank" rel="noopener"}

> 18 people and businesses charged, one giant web of connections America's financial watchdog has accused 18 individuals and shell companies of using compromised brokerage accounts to manipulate stock prices to rake in $1.3 million in illicit profits.... [...]
