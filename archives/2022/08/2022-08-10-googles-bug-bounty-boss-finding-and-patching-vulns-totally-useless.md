Title: Google's bug bounty boss: Finding and patching vulns? 'Totally useless'
Date: 2022-08-10T16:00:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-10-googles-bug-bounty-boss-finding-and-patching-vulns-totally-useless

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/10/google_bug_bounty_boss/){:target="_blank" rel="noopener"}

> Disclosing exploits, however, will earn you $100k Simply finding vulnerabilities and patching them "is totally useless," according to Google's Eduardo Vela, who heads the cloud giant's product security response team.... [...]
