Title: Legitimate hacking activities under UK law proposed by ‘expert consensus’
Date: 2022-08-16T15:38:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-16-legitimate-hacking-activities-under-uk-law-proposed-by-expert-consensus

[Source](https://portswigger.net/daily-swig/legitimate-hacking-activities-under-uk-law-proposed-by-expert-consensus){:target="_blank" rel="noopener"}

> Contentious edge case activities are no excuse for further delaying of ‘much overdue’ reform, say campaigners [...]
