Title: US govt will pay you $10 million for info on Conti ransomware members
Date: 2022-08-11T17:46:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-11-us-govt-will-pay-you-10-million-for-info-on-conti-ransomware-members

[Source](https://www.bleepingcomputer.com/news/security/us-govt-will-pay-you-10-million-for-info-on-conti-ransomware-members/){:target="_blank" rel="noopener"}

> The U.S. State Department announced a $10 million reward today for information on five high-ranking Conti ransomware members, including showing the face of one of the members for the first time. [...]
