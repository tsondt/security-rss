Title: NIST’s Post-Quantum Cryptography Standards
Date: 2022-08-08T11:20:29+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;algorithms;cryptanalysis;cryptography;NIST;quantum computing;quantum cryptography
Slug: 2022-08-08-nists-post-quantum-cryptography-standards

[Source](https://www.schneier.com/blog/archives/2022/08/nists-post-quantum-cryptography-standards.html){:target="_blank" rel="noopener"}

> Quantum computing is a completely new paradigm for computers. A quantum computer uses quantum properties such as superposition, which allows a qubit (a quantum bit) to be neither 0 nor 1, but something much more complicated. In theory, such a computer can solve problems too complex for conventional computers. Current quantum computers are still toy prototypes, and the engineering advances required to build a functionally useful quantum computer are somewhere between a few years away and impossible. Even so, we already know that that such a computer could potentially factor large numbers and compute discrete logs, and break the RSA [...]
