Title: Ukraine's cyber chief comes to Black Hat in surprise visit
Date: 2022-08-13T10:00:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-08-13-ukraines-cyber-chief-comes-to-black-hat-in-surprise-visit

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/13/in_brief_security_black_hat/){:target="_blank" rel="noopener"}

> Tl;DR - the news isn't good Black Hat In Brief Victor Zhora, Ukraine's lead cybersecurity official, made an unannounced visit to Black Hat in Las Vegas this week, where he spoke to attendees about the state of cyberwarfare in the country's conflict with Russia. The picture Zhora painted was bleak.... [...]
