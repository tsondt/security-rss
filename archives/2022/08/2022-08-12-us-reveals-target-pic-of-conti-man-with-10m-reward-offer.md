Title: US reveals 'Target' pic of Conti man with $10m reward offer
Date: 2022-08-12T19:30:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-12-us-reveals-target-pic-of-conti-man-with-10m-reward-offer

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/12/conti_suspect_photo_reward/){:target="_blank" rel="noopener"}

> Fashion Police chipping in on the bounty related to costliest strain of ransomware on record The US government is putting a face on a claimed member of the infamous Conti ransomware group as part of a $10 million reward for information about five of the gang's crew.... [...]
