Title: Deluge of of entries to Spamhaus blocklists includes 'various household names'
Date: 2022-08-18T05:59:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-08-18-deluge-of-of-entries-to-spamhaus-blocklists-includes-various-household-names

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/18/deluge_of_entries_to_spamhaus/){:target="_blank" rel="noopener"}

> Nastymail tracking service blames sloppy sending practices for swelling lists of dangerous mailers Spam-tracking service Spamhaus reported Tuesday that some of the world's biggest brands are getting loose with their email practices, causing its spam blocklists (SBL) to swell significantly.... [...]
