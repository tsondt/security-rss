Title: CEO of Israeli Pegasus spyware firm NSO to step down
Date: 2022-08-21T23:50:25+00:00
Author: Staff and agencies
Category: The Guardian
Tags: Israel;World news;Technology;Privacy;Surveillance;Malware;Middle East and north Africa;Data and computer security
Slug: 2022-08-21-ceo-of-israeli-pegasus-spyware-firm-nso-to-step-down

[Source](https://www.theguardian.com/world/2022/aug/22/nso-group-ceo-shalev-hulio-step-down-israel-pegasus-spyware){:target="_blank" rel="noopener"}

> CEO Shalev Hulio is stepping down as part of NSO reorganisation that will see it focus on sales in Nato member countries Israel’s NSO Group, which makes the globally controversial Pegasus spyware said on Sunday its CEO Shalev Hulio would step down as part of a reorganisation. The indebted, privately owned company also said it would focus sales on countries belonging to the Nato alliance. Continue reading... [...]
