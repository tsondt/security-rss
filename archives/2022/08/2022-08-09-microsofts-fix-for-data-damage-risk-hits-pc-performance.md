Title: Microsoft's fix for 'data damage' risk hits PC performance
Date: 2022-08-09T13:30:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-08-09-microsofts-fix-for-data-damage-risk-hits-pc-performance

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/09/widows_data_damage/){:target="_blank" rel="noopener"}

> 'AES-based operations might be two times slower' without latest updates Microsoft has warned that Windows devices with the newest supported processors might be susceptible to data damage, noting the initial fix might have slowed operations down for some.... [...]
