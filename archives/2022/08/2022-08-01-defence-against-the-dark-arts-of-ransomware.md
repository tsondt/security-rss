Title: Defence against the dark arts of ransomware
Date: 2022-08-01T16:30:09+00:00
Author: Martin Courtney
Category: The Register
Tags: 
Slug: 2022-08-01-defence-against-the-dark-arts-of-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/01/defence_against_the_dark_arts/){:target="_blank" rel="noopener"}

> Locking in safeguards against incursion with Rubrik Zero Trust Security Webinar It's just any old Monday, already you are mentally ticking off the to do list, and then, as you reach for your morning coffee and switch on your screen. Devastation. You've been hacked.... [...]
