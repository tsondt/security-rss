Title: ParseThru: HTTP parameter smuggling flaw uncovered in several Go applications
Date: 2022-08-04T10:55:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-04-parsethru-http-parameter-smuggling-flaw-uncovered-in-several-go-applications

[Source](https://portswigger.net/daily-swig/parsethru-http-parameter-smuggling-flaw-uncovered-in-several-go-applications){:target="_blank" rel="noopener"}

> Harbor, Traefik, and Skipper projects tackle unsafe URL parsing methods [...]
