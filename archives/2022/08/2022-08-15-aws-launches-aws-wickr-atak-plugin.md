Title: AWS launches AWS Wickr ATAK Plugin
Date: 2022-08-15T18:59:28+00:00
Author: Anne Grahn
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;collaboration;data privacy;Data protection;data security;disaster response;E2EE;Encryption;secure communications;Security Blog;situational awareness
Slug: 2022-08-15-aws-launches-aws-wickr-atak-plugin

[Source](https://aws.amazon.com/blogs/security/aws-launches-aws-wickr-atak-plugin/){:target="_blank" rel="noopener"}

> AWS is excited to announce the launch of the AWS Wickr ATAK Plugin, which makes it easier for ATAK users to maintain secure communications. The Android Team Awareness Kit (ATAK)—also known as Android Tactical Assault Kit for military use—is a smartphone geospatial infrastructure and situational awareness application. It provides mapping, messaging, and geofencing capabilities to [...]
