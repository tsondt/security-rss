Title: Twilio breach let hackers gain access to Authy 2FA accounts
Date: 2022-08-26T12:20:04-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-26-twilio-breach-let-hackers-gain-access-to-authy-2fa-accounts

[Source](https://www.bleepingcomputer.com/news/security/twilio-breach-let-hackers-gain-access-to-authy-2fa-accounts/){:target="_blank" rel="noopener"}

> Twilio's investigation into the attack on August 4 reveals that hackers gained access to some Authy user accounts and registered unauthorized devices. [...]
