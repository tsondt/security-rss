Title: IT industry guilty of ‘lack of imagination’ in failure to anticipate cyber-attack evolution
Date: 2022-08-12T15:13:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-12-it-industry-guilty-of-lack-of-imagination-in-failure-to-anticipate-cyber-attack-evolution

[Source](https://portswigger.net/daily-swig/it-industry-guilty-of-lack-of-imagination-in-failure-to-anticipate-cyber-attack-evolution){:target="_blank" rel="noopener"}

> ‘We have a habit of reacting to threats after they occur, rather than preparing for them,’ journalist Kim Zetter tells Black Hat [...]
