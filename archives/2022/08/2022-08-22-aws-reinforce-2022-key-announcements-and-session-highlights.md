Title: AWS re:Inforce 2022: Key announcements and session highlights
Date: 2022-08-22T20:01:59+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS re:Inforce;AWS security;Cloud security;cloud security conference;Live Events;Security Blog;Session Recordings
Slug: 2022-08-22-aws-reinforce-2022-key-announcements-and-session-highlights

[Source](https://aws.amazon.com/blogs/security/aws-reinforce-2022-key-announcements-and-session-highlights/){:target="_blank" rel="noopener"}

> AWS re:Inforce returned to Boston, MA, in July after 2 years, and we were so glad to be back in person with customers. The conference featured over 250 sessions and hands-on labs, 100 AWS partner sponsors, and over 6,000 attendees over 2 days. If you weren’t able to join us in person, or just want [...]
