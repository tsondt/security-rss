Title: The Week in Ransomware - August 26th 2022 - Fighting back
Date: 2022-08-26T16:32:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-26-the-week-in-ransomware-august-26th-2022-fighting-back

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-august-26th-2022-fighting-back/){:target="_blank" rel="noopener"}

> We saw a bit of ransomware drama this week, mostly centered around LockBit, who saw their data leak sites taken down by a DDoS attack after they started leaking the allegedly stolen Entrust data. [...]
