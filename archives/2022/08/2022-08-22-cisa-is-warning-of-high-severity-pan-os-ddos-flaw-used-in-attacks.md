Title: CISA is warning of high-severity PAN-OS DDoS flaw used in attacks
Date: 2022-08-22T17:34:02-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-22-cisa-is-warning-of-high-severity-pan-os-ddos-flaw-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-is-warning-of-high-severity-pan-os-ddos-flaw-used-in-attacks/){:target="_blank" rel="noopener"}

> A recent vulnerability found in Palo Alto Networks' PAN-OS has been added to the catalog of Known Exploitable Vulnerabilities from the U.S. Cybersecurity and Infrastructure Security Agency (CISA). [...]
