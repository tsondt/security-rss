Title: Russian streaming platform confirms data breach affecting 7.5M users
Date: 2022-08-30T16:15:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-30-russian-streaming-platform-confirms-data-breach-affecting-75m-users

[Source](https://www.bleepingcomputer.com/news/security/russian-streaming-platform-confirms-data-breach-affecting-75m-users/){:target="_blank" rel="noopener"}

> Russian media streaming platform 'START' (start.ru) has confirmed rumors of a data breach impacting millions of users. [...]
