Title: Plex forces password resets after database access incident
Date: 2022-08-24T07:49:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-24-plex-forces-password-resets-after-database-access-incident

[Source](https://www.bleepingcomputer.com/news/security/plex-forces-password-resets-after-database-access-incident/){:target="_blank" rel="noopener"}

> The Plex media streaming platform is sending password reset notices to many of its users in response to discovering unauthorized access to one of its databases. [...]
