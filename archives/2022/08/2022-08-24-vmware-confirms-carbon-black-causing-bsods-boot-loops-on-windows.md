Title: VMware confirms Carbon Black causing BSODs, boot loops on Windows
Date: 2022-08-24T16:08:10+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-08-24-vmware-confirms-carbon-black-causing-bsods-boot-loops-on-windows

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/24/vmware_carbon_black_boot_loop/){:target="_blank" rel="noopener"}

> Well, you can't be attacked if your PC won't boot VMware has admitted an update on some versions of its Carbon Black endpoint solution is responsible for BSODs and boot loops on Windows machines after multiple organizations were affected by the problem.... [...]
