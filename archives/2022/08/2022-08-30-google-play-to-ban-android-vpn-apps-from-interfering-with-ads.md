Title: Google Play to ban Android VPN apps from interfering with ads
Date: 2022-08-30T00:43:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-30-google-play-to-ban-android-vpn-apps-from-interfering-with-ads

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/30/google_play_vpn_rules_changed/){:target="_blank" rel="noopener"}

> Developers say this is not the privacy protection it's made out to be Google in November will prohibit Android VPN apps in its Play store from interfering with or blocking advertising, a change that may pose problems for some privacy applications.... [...]
