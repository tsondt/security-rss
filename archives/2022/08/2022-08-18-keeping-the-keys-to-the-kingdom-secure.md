Title: Keeping the keys to the kingdom secure
Date: 2022-08-18T16:30:07+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-08-18-keeping-the-keys-to-the-kingdom-secure

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/18/specops_webinar/){:target="_blank" rel="noopener"}

> Learn how you can improve your password security and keep your organization's data safe Webinar Believe it or not the word 'password' is still being used as the most common password across all industries, including retail and ecommerce.... [...]
