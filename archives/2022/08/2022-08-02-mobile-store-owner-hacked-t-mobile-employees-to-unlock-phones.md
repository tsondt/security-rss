Title: Mobile store owner hacked T-Mobile employees to unlock phones
Date: 2022-08-02T11:02:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-08-02-mobile-store-owner-hacked-t-mobile-employees-to-unlock-phones

[Source](https://www.bleepingcomputer.com/news/security/mobile-store-owner-hacked-t-mobile-employees-to-unlock-phones/){:target="_blank" rel="noopener"}

> A former owner of a T-Mobile retail store in California has been found guilty of a $25 million scheme where he illegally accessed T-Mobile's internal systems to unlock and unblock cell phones. [...]
