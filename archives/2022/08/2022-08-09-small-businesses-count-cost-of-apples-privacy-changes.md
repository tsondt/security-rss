Title: Small businesses count cost of Apple’s privacy changes
Date: 2022-08-09T13:29:23+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;advertising;apple;privacy;tracking
Slug: 2022-08-09-small-businesses-count-cost-of-apples-privacy-changes

[Source](https://arstechnica.com/?p=1872301){:target="_blank" rel="noopener"}

> Enlarge (credit: Kentaroo Tryman | Getty Images ) Small businesses are cutting back marketing spending due to Apple’s sweeping privacy changes that have made it harder to target new customers online, in a growing trend that has led to billions of dollars in lost revenues for platforms like Facebook. Apple last year began forcing app developers to get permission to track users and serve them personalized ads on iPhones and iPads in changes that have transformed the online advertising sector. Many small companies that are reliant on online ads to attract new customers told the Financial Times they did not [...]
