Title: Let there be ambient light sensing, without fear of data theft
Date: 2022-08-13T00:24:19+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-13-let-there-be-ambient-light-sensing-without-fear-of-data-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/13/ambient_light_security/){:target="_blank" rel="noopener"}

> Six years on web devs finally settle on sensor privacy defenses Six years after web security and privacy concerns surfaced about ambient light sensors in mobile phones and notebooks, browser boffins have finally implemented defenses.... [...]
