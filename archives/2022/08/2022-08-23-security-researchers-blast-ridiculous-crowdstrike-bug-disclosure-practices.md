Title: Security researchers blast ‘ridiculous’ CrowdStrike bug disclosure practices
Date: 2022-08-23T13:57:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-23-security-researchers-blast-ridiculous-crowdstrike-bug-disclosure-practices

[Source](https://portswigger.net/daily-swig/security-researchers-blast-ridiculous-crowdstrike-bug-disclosure-practices){:target="_blank" rel="noopener"}

> The vulnerability might not be noteworthy, but the reporting process may be A security firm has criticized CrowdStrike for operating a “ridiculous” bug bounty disclosure program following a sensor fla [...]
