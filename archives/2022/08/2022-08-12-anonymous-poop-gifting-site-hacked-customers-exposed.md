Title: Anonymous poop gifting site hacked, customers exposed
Date: 2022-08-12T16:15:56-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-08-12-anonymous-poop-gifting-site-hacked-customers-exposed

[Source](https://www.bleepingcomputer.com/news/security/anonymous-poop-gifting-site-hacked-customers-exposed/){:target="_blank" rel="noopener"}

> ShitExpress, a web service that lets you send a box of feces along with a personalized message to friends and enemies, has been breached after a "customer" spotted a vulnerability. [...]
