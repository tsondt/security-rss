Title: Hyundai Uses Example Keys for Encryption System
Date: 2022-08-22T11:38:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AES;cars;encryption;keys
Slug: 2022-08-22-hyundai-uses-example-keys-for-encryption-system

[Source](https://www.schneier.com/blog/archives/2022/08/hyundai-uses-example-keys-for-encryption-system.html){:target="_blank" rel="noopener"}

> This is a dumb crypto mistake I had not previously encountered: A developer says it was possible to run their own software on the car infotainment hardware after discovering the vehicle’s manufacturer had secured its system using keys that were not only publicly known but had been lifted from programming examples. [...] “Turns out the [AES] encryption key in that script is the first AES 128-bit CBC example key listed in the NIST document SP800-38A [PDF]”. [...] Luck held out, in a way. “Greenluigi1” found within the firmware image the RSA public key used by the updater, and searched online [...]
