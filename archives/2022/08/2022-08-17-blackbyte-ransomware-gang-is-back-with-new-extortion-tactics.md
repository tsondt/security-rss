Title: BlackByte ransomware gang is back with new extortion tactics
Date: 2022-08-17T17:28:33-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-17-blackbyte-ransomware-gang-is-back-with-new-extortion-tactics

[Source](https://www.bleepingcomputer.com/news/security/blackbyte-ransomware-gang-is-back-with-new-extortion-tactics/){:target="_blank" rel="noopener"}

> The BlackByte ransomware is back with version 2.0 of their operation, including a new data leak site utilizing new extortion techniques borrowed from LockBit. [...]
