Title: Do you know what’s happening on your users’ devices?
Date: 2022-08-16T14:39:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-08-16-do-you-know-whats-happening-on-your-users-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/16/do_you_know_whats_happening/){:target="_blank" rel="noopener"}

> Head this way to find if your strategy’s on (end) point Sponsored Post You might be happy with your cloud infrastructure and totally on top of your internal network, but one thing for certain is that whatever your workforce is doing, they'll have endpoints. Are you sure you know exactly what's happening on all those devices?... [...]
