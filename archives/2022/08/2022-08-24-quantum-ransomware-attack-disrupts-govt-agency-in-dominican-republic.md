Title: Quantum ransomware attack disrupts govt agency in Dominican Republic
Date: 2022-08-24T17:39:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-24-quantum-ransomware-attack-disrupts-govt-agency-in-dominican-republic

[Source](https://www.bleepingcomputer.com/news/security/quantum-ransomware-attack-disrupts-govt-agency-in-dominican-republic/){:target="_blank" rel="noopener"}

> The Dominican Republic's Instituto Agrario Dominicano has suffered a Quantum ransomware attack that encrypted multiple services and workstations throughout the government agency. [...]
