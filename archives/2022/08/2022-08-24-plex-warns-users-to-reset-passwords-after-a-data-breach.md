Title: Plex warns users to reset passwords after a data breach
Date: 2022-08-24T07:49:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-24-plex-warns-users-to-reset-passwords-after-a-data-breach

[Source](https://www.bleepingcomputer.com/news/security/plex-warns-users-to-reset-passwords-after-a-data-breach/){:target="_blank" rel="noopener"}

> The Plex media streaming platform is sending password reset notices to many of its users in response to discovering unauthorized access to one of its databases. [...]
