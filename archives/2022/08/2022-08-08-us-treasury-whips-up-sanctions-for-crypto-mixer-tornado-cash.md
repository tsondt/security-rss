Title: US treasury whips up sanctions for crypto mixer Tornado Cash
Date: 2022-08-08T23:00:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-08-us-treasury-whips-up-sanctions-for-crypto-mixer-tornado-cash

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/08/treasury_sanctions_tornado_cash_korea/){:target="_blank" rel="noopener"}

> Being the money launderer for North Korea’s Lazarus Group comes at a price The US Treasury Department is levying sanctions against Tornado Cash, a notorious cryptocurrency mixer that it says has been used by threat groups like ransomware gang Lazarus to launder stolen digital assets.... [...]
