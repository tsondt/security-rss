Title: Security needs to learn from the aviation biz to avoid crashing
Date: 2022-08-11T22:30:15+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-08-11-security-needs-to-learn-from-the-aviation-biz-to-avoid-crashing

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/wheeler_black_hat/){:target="_blank" rel="noopener"}

> 'Until someone has to go to jail for doing it wrong the teeth are not going to be the same' Black Hat video The security industry needs to take a leaf from the manual of an industry where smart incident response is literally life and death, if it is to fix systemic problems.... [...]
