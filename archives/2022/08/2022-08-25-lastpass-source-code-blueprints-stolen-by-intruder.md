Title: LastPass source code, blueprints stolen by intruder
Date: 2022-08-25T21:02:19+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: 2022-08-25-lastpass-source-code-blueprints-stolen-by-intruder

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/25/lastpass_security/){:target="_blank" rel="noopener"}

> Your passwords are still safe, biz says Internal source code and documents have been stolen from LastPass by a cyber-thief.... [...]
