Title: 241 npm and PyPI packages caught dropping Linux cryptominers
Date: 2022-08-19T16:11:07-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-08-19-241-npm-and-pypi-packages-caught-dropping-linux-cryptominers

[Source](https://www.bleepingcomputer.com/news/security/241-npm-and-pypi-packages-caught-dropping-linux-cryptominers/){:target="_blank" rel="noopener"}

> More than 200 malicious packages were discovered infiltrating the PyPI and npm open source registries this week. These packages are largely typosquats of widely used libraries and each one of them downloads a Bash script on Linux systems that run cryptominers. [...]
