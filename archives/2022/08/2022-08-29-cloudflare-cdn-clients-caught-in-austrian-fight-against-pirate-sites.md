Title: Cloudflare CDN clients caught in Austrian fight against pirate sites
Date: 2022-08-29T12:22:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-08-29-cloudflare-cdn-clients-caught-in-austrian-fight-against-pirate-sites

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-cdn-clients-caught-in-austrian-fight-against-pirate-sites/){:target="_blank" rel="noopener"}

> Excessive and indiscriminate blocking is underway in Austria, with internet service providers (ISPs) complying to a court order to block pirate sites causing significant collateral damage. [...]
