Title: LockBit ransomware gang gets aggressive with triple-extortion tactic
Date: 2022-08-28T18:44:52-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-28-lockbit-ransomware-gang-gets-aggressive-with-triple-extortion-tactic

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-gang-gets-aggressive-with-triple-extortion-tactic/){:target="_blank" rel="noopener"}

> LockBit ransomware gang announced that it is improving defenses against distributed denial-of-service (DDoS) attacks and working to take the operation to triple extortion level. [...]
