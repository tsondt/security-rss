Title: US sanctions crypto mixer Tornado Cash used by North Korean hackers
Date: 2022-08-08T11:21:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-08-08-us-sanctions-crypto-mixer-tornado-cash-used-by-north-korean-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-crypto-mixer-tornado-cash-used-by-north-korean-hackers/){:target="_blank" rel="noopener"}

> The U.S. Treasury Department's Office of Foreign Assets Control (OFAC) sanctioned Tornado Cash today, a decentralized cryptocurrency mixer service used to launder more than $7 billion since its creation in 2019. [...]
