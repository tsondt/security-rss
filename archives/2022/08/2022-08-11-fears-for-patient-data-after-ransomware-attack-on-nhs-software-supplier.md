Title: Fears for patient data after ransomware attack on NHS software supplier
Date: 2022-08-11T10:01:10+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: NHS;Data and computer security;Data protection;Privacy;Technology;Society;Health
Slug: 2022-08-11-fears-for-patient-data-after-ransomware-attack-on-nhs-software-supplier

[Source](https://www.theguardian.com/society/2022/aug/11/fears-patient-data-ransomware-attack-nhs-software-supplier){:target="_blank" rel="noopener"}

> Attack being investigated for potential data theft as experts warn criminals could use stolen details as leverage A ransomware attack on an NHS software supplier last week is being investigated for potential theft of patient data, as experts warned that criminals could use personal information as leverage in negotiations. Advanced, which provides services for NHS 111 and patient records, said it was investigating “potentially impacted data” and that it would provide updates when it had more information about “potential data access or exfiltration”. The UK data watchdog confirmed it was aware of the incident and was “making inquiries.” Continue reading... [...]
