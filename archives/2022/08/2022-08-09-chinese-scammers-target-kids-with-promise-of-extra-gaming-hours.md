Title: Chinese scammers target kids with promise of extra gaming hours
Date: 2022-08-09T02:45:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-08-09-chinese-scammers-target-kids-with-promise-of-extra-gaming-hours

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/09/china_minors_gaming/){:target="_blank" rel="noopener"}

> Cyberspace regulator's fraud report finds all is not well behind the Great Firewall Fraudsters in China have targeted a child with promises of allowing them to get around the nation's time limits on playing computer games – for a mere $560, according to the nation's cyberspace administration. Yesterday the CAC detailed some of the 12,000 acts of online fraud perpetrated against minors it handled this year.... [...]
