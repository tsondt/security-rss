Title: Ex-CIA security boss predicts coming crackdown on spyware
Date: 2022-08-11T19:15:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-11-ex-cia-security-boss-predicts-coming-crackdown-on-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/cia_security_boss_spyware/){:target="_blank" rel="noopener"}

> Plus, spoiler alert: ransomware is gonna get a lot worse Black Hat video It turns out that ex-CIA chief information security officers don't spill secrets at bars in Vegas. Or via Zoom, while pretending to be at a Black Hat cocktail party.... [...]
