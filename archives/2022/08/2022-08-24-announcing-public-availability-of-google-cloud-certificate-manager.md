Title: Announcing public availability of Google Cloud Certificate Manager
Date: 2022-08-24T19:00:00+00:00
Author: Babi Seal
Category: GCP Security
Tags: Developers & Practitioners;Networking;DevOps & SRE;Google Cloud;Identity & Security
Slug: 2022-08-24-announcing-public-availability-of-google-cloud-certificate-manager

[Source](https://cloud.google.com/blog/products/identity-security/introducing-general-availability-of-google-cloud-certificate-manager/){:target="_blank" rel="noopener"}

> Today we are pleased to announce that Cloud Certificate Manager is now in general availability. Cloud Certificate Manager enables our users to acquire, manage, and deploy public Transport Layer Security (TLS) certificates at scale for use with your Google Cloud workloads. TLS certificates are required to secure browser connections and transactions. Cloud Certificate Manager supports both self-managed and Google-managed certificates, as well as wildcard certificates, and has monitoring capabilities to alert for expiring certificates. Scale to support as many domains as you need Since our public preview announcement supporting the SaaS use cases, we have scaled the solution to serve [...]
