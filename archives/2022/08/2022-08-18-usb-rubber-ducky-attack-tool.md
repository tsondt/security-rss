Title: USB “Rubber Ducky” Attack Tool
Date: 2022-08-18T11:45:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cybersecurity;hacking;USB
Slug: 2022-08-18-usb-rubber-ducky-attack-tool

[Source](https://www.schneier.com/blog/archives/2022/08/usb-rubber-ducky-attack-tool.html){:target="_blank" rel="noopener"}

> The USB Rubber Ducky is getting better and better. Already, previous versions of the Rubber Ducky could carry out attacks like creating a fake Windows pop-up box to harvest a user’s login credentials or causing Chrome to send all saved passwords to an attacker’s webserver. But these attacks had to be carefully crafted for specific operating systems and software versions and lacked the flexibility to work across platforms. The newest Rubber Ducky aims to overcome these limitations. It ships with a major upgrade to the DuckyScript programming language, which is used to create the commands that the Rubber Ducky will [...]
