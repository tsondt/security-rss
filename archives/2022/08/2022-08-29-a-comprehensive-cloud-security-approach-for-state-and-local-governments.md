Title: A comprehensive cloud security approach for state and local governments
Date: 2022-08-29T16:00:00+00:00
Author: Mike Williams
Category: GCP Security
Tags: Identity & Security;Public Sector
Slug: 2022-08-29-a-comprehensive-cloud-security-approach-for-state-and-local-governments

[Source](https://cloud.google.com/blog/topics/public-sector/comprehensive-cloud-security-approach-state-and-local-governments/){:target="_blank" rel="noopener"}

> While the digitization of government services and operations has helped enhance the constituent experience, it has also increased the cyber threat surface for governments of all sizes. In 2020, 79 ransomware attacks hit government organizations, amounting to nearly $19 billion in downtime and recovery costs 1. Other research indicates 34% of local governments worldwide were hit by ransomware in 2022 2. This year, the National Association of State Chief Information Officers (NASCIO) ranked "harmonizing disparate federal cybersecurity regulations" as their number one priority. To that end, Google Cloud is here to help government leaders develop a more comprehensive approach to [...]
