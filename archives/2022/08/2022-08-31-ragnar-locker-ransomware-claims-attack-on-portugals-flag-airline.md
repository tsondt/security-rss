Title: Ragnar Locker ransomware claims attack on Portugal's flag airline
Date: 2022-08-31T13:01:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-31-ragnar-locker-ransomware-claims-attack-on-portugals-flag-airline

[Source](https://www.bleepingcomputer.com/news/security/ragnar-locker-ransomware-claims-attack-on-portugals-flag-airline/){:target="_blank" rel="noopener"}

> The Ragnar Locker ransomware gang has claimed an attack on the flag carrier of Portugal, TAP Air Portugal, disclosed by the airline last Friday. [...]
