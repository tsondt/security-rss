Title: Ex-T-Mobile US store owner phished staff, raked in $25m from unlocking phones
Date: 2022-08-03T20:17:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-03-ex-t-mobile-us-store-owner-phished-staff-raked-in-25m-from-unlocking-phones

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/03/tmobile_unlock_prison_phone/){:target="_blank" rel="noopener"}

> That's just the tip of the iceberg – and now he faces potentially years in the clink A now-former T-Mobile US store stole at least 50 employees' work credentials to run a phone unlocking and unblocking service that prosecutors said netted $25 million.... [...]
