Title: Warning over Java libraries and deserialization security weaknesses
Date: 2022-08-22T20:00:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-22-warning-over-java-libraries-and-deserialization-security-weaknesses

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/22/java_library_flaws/){:target="_blank" rel="noopener"}

> There is a madness to the methods Boffins at universities in France, Germany, Luxembourg, and Sweden took a deep dive into known Java deserialization vulnerabilities, and have now resurfaced with their findings. In short, they've drawn attention to the ways in which libraries can accidentally introduce serious security flaws.... [...]
