Title: Businesses should dump Windows for the Linux desktop
Date: 2022-08-10T10:32:05+00:00
Author: Steven J. Vaughan-Nichols
Category: The Register
Tags: 
Slug: 2022-08-10-businesses-should-dump-windows-for-the-linux-desktop

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/10/opinion_column_drop_windows_for_linux/){:target="_blank" rel="noopener"}

> It makes perfect sense for enterprises as well as enthusiasts. Just ask GitLab Opinion I've been preaching the gospel of the Linux desktop for more years than some of you have been alive. However, unless you argue that the Linux desktop includes Android smartphones and ChromeOS laptops, there will be no year of the Linux desktop.... [...]
