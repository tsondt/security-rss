Title: Watering Hole Attacks Push ScanBox Keylogger
Date: 2022-08-30T16:00:43+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Malware
Slug: 2022-08-30-watering-hole-attacks-push-scanbox-keylogger

[Source](https://threatpost.com/watering-hole-attacks-push-scanbox-keylogger/180490/){:target="_blank" rel="noopener"}

> Researchers uncover a watering hole attack likely carried out by APT TA423, which attempts to plant the ScanBox JavaScript-based reconnaissance tool. [...]
