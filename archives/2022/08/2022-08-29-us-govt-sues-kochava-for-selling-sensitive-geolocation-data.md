Title: US govt sues Kochava for selling sensitive geolocation data
Date: 2022-08-29T12:00:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-29-us-govt-sues-kochava-for-selling-sensitive-geolocation-data

[Source](https://www.bleepingcomputer.com/news/security/us-govt-sues-kochava-for-selling-sensitive-geolocation-data/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) announced today that it filed a lawsuit against Idaho-based location data broker Kochava for selling sensitive and precise geolocation data (in meters) collected from hundreds of millions of mobile devices. [...]
