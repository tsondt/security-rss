Title: How 1-Time Passcodes Became a Corporate Liability
Date: 2022-08-30T14:53:39+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Security Tools;Web Fraud 2.0;0ktapus;Christopher Knauer;CloudFlare;DigitalOcean;DoorDash;Group-IB;Klaviyo;Mailchimp;Matthew Prince;Security Keys;signal;Sitel Group;T-Mobile;Teleperformance;Twilio;twitter
Slug: 2022-08-30-how-1-time-passcodes-became-a-corporate-liability

[Source](https://krebsonsecurity.com/2022/08/how-1-time-passcodes-became-a-corporate-liability/){:target="_blank" rel="noopener"}

> Phishers are enjoying remarkable success using text messages to steal remote access credentials and one-time passcodes from employees at some of the world’s largest technology companies and customer support firms. A recent spate of SMS phishing attacks from one cybercriminal group has spawned a flurry of breach disclosures from affected companies, which are all struggling to combat the same lingering security threat: The ability of scammers to interact directly with employees through their mobile devices. In mid-June 2022, a flood of SMS phishing messages began targeting employees at commercial staffing firms that provide customer support and outsourcing to thousands of [...]
