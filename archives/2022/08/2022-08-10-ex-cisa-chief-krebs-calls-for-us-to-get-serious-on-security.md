Title: Ex-CISA chief Krebs calls for US to get serious on security
Date: 2022-08-10T23:26:34+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-10-ex-cisa-chief-krebs-calls-for-us-to-get-serious-on-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/10/krebs_black_hat/){:target="_blank" rel="noopener"}

> Black Hat kicks off with call for single infosec agency with real clout and less confused crossover Black Hat It's time to reorganize the US government and create a new agency focused solely on on digital risk management services, according to former CISA director Chris Krebs.... [...]
