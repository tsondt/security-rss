Title: Solana, Phantom blame Slope after millions in crypto-coins stolen from 8,000 wallets
Date: 2022-08-04T03:26:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-04-solana-phantom-blame-slope-after-millions-in-crypto-coins-stolen-from-8000-wallets

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/04/solana_wallet_slope/){:target="_blank" rel="noopener"}

> SOL holders literally S.O.L. Millions of dollars worth of Solana cryptocurrency and other tokens were stolen from seemingly thousands of netizens this week by thieves exploiting some kind of security weakness or blunder.... [...]
