Title: Black Hat USA: Pen testing tool that aims to ‘keep the fun in hacking’ unveiled
Date: 2022-08-12T09:58:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-12-black-hat-usa-pen-testing-tool-that-aims-to-keep-the-fun-in-hacking-unveiled

[Source](https://portswigger.net/daily-swig/black-hat-usa-pen-testing-tool-that-aims-to-keep-the-fun-in-hacking-unveiled){:target="_blank" rel="noopener"}

> Latest version of AttackForge ReportGen DevSecOps aid demonstrated during conference Arsenal track [...]
