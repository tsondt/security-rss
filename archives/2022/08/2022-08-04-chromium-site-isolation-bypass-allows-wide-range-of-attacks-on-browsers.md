Title: Chromium site isolation bypass allows wide range of attacks on browsers
Date: 2022-08-04T14:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-04-chromium-site-isolation-bypass-allows-wide-range-of-attacks-on-browsers

[Source](https://portswigger.net/daily-swig/chromium-site-isolation-bypass-allows-wide-range-of-attacks-on-browsers){:target="_blank" rel="noopener"}

> Flaw that opened the door to cookie modification and data theft resolved [...]
