Title: Access to hacked corporate networks still strong but sales fall
Date: 2022-08-11T08:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-11-access-to-hacked-corporate-networks-still-strong-but-sales-fall

[Source](https://www.bleepingcomputer.com/news/security/access-to-hacked-corporate-networks-still-strong-but-sales-fall/){:target="_blank" rel="noopener"}

> Statistics collected by cyber-intelligence firm KELA during this year's second quarter show that marketplaces selling initial access to corporate networks have taken a blow. [...]
