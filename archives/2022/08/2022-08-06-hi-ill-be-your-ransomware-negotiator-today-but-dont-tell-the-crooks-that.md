Title: Hi, I'll be your ransomware negotiator today – but don't tell the crooks that
Date: 2022-08-06T08:19:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-06-hi-ill-be-your-ransomware-negotiator-today-but-dont-tell-the-crooks-that

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/06/interview_ransomware_negotiator/){:target="_blank" rel="noopener"}

> What it's like bargaining with criminals... and advising clients suffering their worst day yet Interview The first rule of being a ransomware negotiator is that you don't admit you're a ransomware negotiator — at least not to LockBit or another cybercrime gang.... [...]
