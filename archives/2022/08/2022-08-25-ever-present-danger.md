Title: Ever present danger
Date: 2022-08-25T13:41:08+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-08-25-ever-present-danger

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/25/ever_present_danger/){:target="_blank" rel="noopener"}

> Recognizing the signs of an impending ransomware attack with Red Canary Webinar It's no surprise that there has been an explosion in ransomware following the evolution of cryptocurrencies. The emergence of Bitcoin in 2010 suddenly provided an easy and untraceable way to force victims to pay.... [...]
