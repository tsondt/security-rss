Title: Attacker snags account details from streaming service Plex
Date: 2022-08-24T14:00:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-08-24-attacker-snags-account-details-from-streaming-service-plex

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/24/data_breach_plex_hack/){:target="_blank" rel="noopener"}

> 'Limited subset' of users have emails, usernames, and hashed passwords stolen from the platform Users of popular streaming and media organizing service Plex are waking up to an unpleasant email this morning saying, in the words of a Reg reader, "Plex have been hacked and their main site is down as we all rush to change passwords."... [...]
