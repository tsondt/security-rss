Title: NortonLifeLock and Avast $8.6b deal gets provisional yes from UK regulator
Date: 2022-08-03T11:30:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-08-03-nortonlifelock-and-avast-86b-deal-gets-provisional-yes-from-uk-regulator

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/03/norton_cma/){:target="_blank" rel="noopener"}

> Plus: Even market authorities can't seem to keep up with Microsoft's Defender branding The UK's Competition and Markets Authority has given a provisional nod to the proposed merger of British cybersecurity company Avast and US rival NortonLifeLock.... [...]
