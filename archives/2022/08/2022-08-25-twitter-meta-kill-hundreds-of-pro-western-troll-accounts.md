Title: Twitter, Meta kill hundreds of pro-Western troll accounts
Date: 2022-08-25T15:00:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-08-25-twitter-meta-kill-hundreds-of-pro-western-troll-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/25/twitter_meta_troll_accounts/){:target="_blank" rel="noopener"}

> It turns out online chicanery aiming to destabilize foreign nations is a two-way street Well known for an abundance of anti-western troll accounts and propaganda, Twitter and Meta are reporting that they've taken down nearly 200 accounts that, for the past five years, have been amplifying pro-Western messages in the Middle East and Central Asia.... [...]
