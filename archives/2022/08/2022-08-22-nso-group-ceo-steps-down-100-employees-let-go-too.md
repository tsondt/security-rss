Title: NSO Group CEO steps down, 100 employees let go too
Date: 2022-08-22T05:01:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-08-22-nso-group-ceo-steps-down-100-employees-let-go-too

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/22/nso_group_ceo_steps_down/){:target="_blank" rel="noopener"}

> Controversial Pegasus spyware maker to focus on NATO sales while battling various court cases Pegasus spyware slinger NSO Group announced on Sunday it will reorganize, replacing its CEO and letting go of around 100 workers.... [...]
