Title: Twilio: 125 customers affected by data breach, no passwords stolen
Date: 2022-08-12T13:44:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-12-twilio-125-customers-affected-by-data-breach-no-passwords-stolen

[Source](https://www.bleepingcomputer.com/news/security/twilio-125-customers-affected-by-data-breach-no-passwords-stolen/){:target="_blank" rel="noopener"}

> Cloud communications giant Twilio, the owner of the highly popular two-factor authentication (2FA) provider Authy, says that it has so far identified 125 customers who had their data accessed during a security breach discovered last week. [...]
