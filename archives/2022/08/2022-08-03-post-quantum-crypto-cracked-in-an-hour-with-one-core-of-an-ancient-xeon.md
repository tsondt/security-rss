Title: Post-quantum crypto cracked in an hour with one core of an ancient Xeon
Date: 2022-08-03T06:59:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-08-03-post-quantum-crypto-cracked-in-an-hour-with-one-core-of-an-ancient-xeon

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/03/nist_quantum_resistant_crypto_cracked/){:target="_blank" rel="noopener"}

> NIST's nifty new algorithm looks like it's in trouble One of the four encryption algorithms the US National Institute of Standards and Technology (NIST) recommended as likely to resist decryption by quantum computers has had holes kicked in it by researchers using a single core of an Intel Xeon CPU, released in 2013.... [...]
