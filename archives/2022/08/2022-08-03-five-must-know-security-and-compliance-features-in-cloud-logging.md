Title: Five must-know security and compliance features in Cloud Logging
Date: 2022-08-03T16:00:00+00:00
Author: Collin Frierson
Category: GCP Security
Tags: Public Sector;Application Modernization;Google Cloud;Identity & Security
Slug: 2022-08-03-five-must-know-security-and-compliance-features-in-cloud-logging

[Source](https://cloud.google.com/blog/products/identity-security/5-must-know-security-and-compliance-features-in-cloud-logging/){:target="_blank" rel="noopener"}

> As enterprise and public sector cloud adoption continues to accelerate, having an accurate picture of who did what in your cloud environment is important for security and compliance purposes. Logs are critical when you are attempting to detect a breach, investigating ongoing security issues, or performing forensic investigations. These five must-know Cloud Logging security and compliance features can help customers create logs to best conduct security audits. The first three features were launched recently in 2022, while the last two features have been available for some time. 1. Cloud Logging is a part of Assured Workloads. Google Cloud’s Assured Workloads [...]
