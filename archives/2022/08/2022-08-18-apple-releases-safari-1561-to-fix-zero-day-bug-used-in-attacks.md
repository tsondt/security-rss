Title: Apple releases Safari 15.6.1 to fix zero-day bug used in attacks
Date: 2022-08-18T15:49:45-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-18-apple-releases-safari-1561-to-fix-zero-day-bug-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/apple-releases-safari-1561-to-fix-zero-day-bug-used-in-attacks/){:target="_blank" rel="noopener"}

> Apple has released Safari 15.6.1 for macOS Big Sur and Catalina to fix a zero-day vulnerability exploited in the wild to hack Macs. [...]
