Title: Ethereum Foundation offers $1m bug bounty payouts with proof-of-stake migration multiplier
Date: 2022-08-25T13:07:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-25-ethereum-foundation-offers-1m-bug-bounty-payouts-with-proof-of-stake-migration-multiplier

[Source](https://portswigger.net/daily-swig/ethereum-foundation-offers-1m-bug-bounty-payouts-with-proof-of-stake-migration-multiplier){:target="_blank" rel="noopener"}

> Eco-friendly upgrade sends bounties soaring as computational demands plummet [...]
