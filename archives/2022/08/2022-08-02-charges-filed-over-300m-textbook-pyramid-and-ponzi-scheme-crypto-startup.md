Title: Charges filed over $300m 'textbook pyramid and Ponzi scheme' crypto startup
Date: 2022-08-02T01:09:01+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-02-charges-filed-over-300m-textbook-pyramid-and-ponzi-scheme-crypto-startup

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/02/sec_smacks_fraudsters_in_alleged/){:target="_blank" rel="noopener"}

> Financial watchdog accuses 11 of playing role in alleged scam Forsage, an alleged crypto Ponzi scheme purporting to be a decentralized smart contract platform, bilked millions of investors worldwide out of more than $300 million, according to America's securities watchdog.... [...]
