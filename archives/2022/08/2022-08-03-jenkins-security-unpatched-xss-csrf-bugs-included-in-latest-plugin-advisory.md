Title: Jenkins security: Unpatched XSS, CSRF bugs included in latest plugin advisory
Date: 2022-08-03T13:52:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-03-jenkins-security-unpatched-xss-csrf-bugs-included-in-latest-plugin-advisory

[Source](https://portswigger.net/daily-swig/jenkins-security-unpatched-xss-csrf-bugs-included-in-latest-plugin-advisory){:target="_blank" rel="noopener"}

> ‘We believe that announcing vulnerabilities without a fix is the best solution for a difficult problem’ [...]
