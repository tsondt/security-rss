Title: How to deploy AWS Network Firewall by using AWS Firewall Manager
Date: 2022-08-26T16:17:13+00:00
Author: Harith Gaddamanugu
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Application development;Application security;AWS Firewall Manager;AWS Network Firewall;Security Blog
Slug: 2022-08-26-how-to-deploy-aws-network-firewall-by-using-aws-firewall-manager

[Source](https://aws.amazon.com/blogs/security/how-to-deploy-aws-network-firewall-by-using-aws-firewall-manager/){:target="_blank" rel="noopener"}

> AWS Network Firewall helps make it easier for you to secure virtual networks at scale inside Amazon Web Services (AWS). Without having to worry about availability, scalability, or network performance, you can now deploy Network Firewall with the AWS Firewall Manager service. Firewall Manager allows administrators in your organization to apply network firewalls across accounts. [...]
