Title: Oh Deere: Farm hardware jailbroken to run Doom
Date: 2022-08-16T00:53:59+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-16-oh-deere-farm-hardware-jailbroken-to-run-doom

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/16/john_deere_doom/){:target="_blank" rel="noopener"}

> Corn-y demo heralded as right-to-repair win At DEF CON 30 on Saturday, an Australian who goes by the handle "Sick Codes" showed off a way to fully take control of some John Deere farming machine electronics to run first-person shooter Doom.... [...]
