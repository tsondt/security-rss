Title: New tool checks if a mobile app's browser is a privacy risk
Date: 2022-08-19T14:12:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-19-new-tool-checks-if-a-mobile-apps-browser-is-a-privacy-risk

[Source](https://www.bleepingcomputer.com/news/security/new-tool-checks-if-a-mobile-apps-browser-is-a-privacy-risk/){:target="_blank" rel="noopener"}

> A new online tool named 'InAppBrowser' lets you analyze the behavior of in-app browsers embedded within mobile apps and determine if they inject privacy-threatening JavaScript into websites you visit. [...]
