Title: Mudge Files Whistleblower Complaint against Twitter
Date: 2022-08-24T11:40:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;Twitter;whistleblowers
Slug: 2022-08-24-mudge-files-whistleblower-complaint-against-twitter

[Source](https://www.schneier.com/blog/archives/2022/08/mudge-files-whistleblower-complaint-against-twitter.html){:target="_blank" rel="noopener"}

> Peiter Zatko, aka Mudge, has filed a whistleblower complaint with the SEC against Twitter, claiming that they violated an eleven-year-old FTC settlement by having lousy security. And he should know; he was Twitter’s chief security officer until he was fired in January. The Washington Post has the scoop (with documents) and companion backgrounder. This CNN story is also comprehensive. [...]
