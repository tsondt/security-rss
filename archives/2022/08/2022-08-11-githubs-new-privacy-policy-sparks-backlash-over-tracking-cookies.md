Title: GitHub's new privacy policy sparks backlash over tracking cookies
Date: 2022-08-11T03:45:42-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-08-11-githubs-new-privacy-policy-sparks-backlash-over-tracking-cookies

[Source](https://www.bleepingcomputer.com/news/security/githubs-new-privacy-policy-sparks-backlash-over-tracking-cookies/){:target="_blank" rel="noopener"}

> Developers are furious at GitHub's upcoming privacy policy changes that would allow GitHub to place tracking cookies on some of its subdomains. The Microsoft subsidiary announced this month, it would be adding "non-essential cookies" on some marketing web pages starting in September, and offered a 30-day "comment period." [...]
