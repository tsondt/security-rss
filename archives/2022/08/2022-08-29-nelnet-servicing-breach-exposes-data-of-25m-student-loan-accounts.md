Title: Nelnet Servicing breach exposes data of 2.5M student loan accounts
Date: 2022-08-29T14:16:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-29-nelnet-servicing-breach-exposes-data-of-25m-student-loan-accounts

[Source](https://www.bleepingcomputer.com/news/security/nelnet-servicing-breach-exposes-data-of-25m-student-loan-accounts/){:target="_blank" rel="noopener"}

> Nelnet Serving, a Nebraska-based student loan technology services provider, has been breached by unauthorized network intruders who exploited a vulnerability in its systems. [...]
