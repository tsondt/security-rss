Title: FBI warns of residential proxies used in credential stuffing attacks
Date: 2022-08-22T14:59:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-22-fbi-warns-of-residential-proxies-used-in-credential-stuffing-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-residential-proxies-used-in-credential-stuffing-attacks/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warns of a rising trend of cybercriminals using residential proxies to conduct large-scale credential stuffing attacks without being tracked, flagged, or blocked. [...]
