Title: Google blocks largest HTTPS DDoS attack 'reported to date'
Date: 2022-08-18T12:00:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-18-google-blocks-largest-https-ddos-attack-reported-to-date

[Source](https://www.bleepingcomputer.com/news/security/google-blocks-largest-https-ddos-attack-reported-to-date/){:target="_blank" rel="noopener"}

> A Google Cloud Armor customer was hit with a distributed denial-of-service (DDoS) attack over the HTTPS protocol that reached 46 million requests per second (RPS), making it the largest ever recorded of its kind. [...]
