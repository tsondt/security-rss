Title: Slack leaked hashed passwords from its servers for years
Date: 2022-08-08T11:45:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-08-08-slack-leaked-hashed-passwords-from-its-servers-for-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/08/slack_passwords/){:target="_blank" rel="noopener"}

> Users who created shared invitation links for their workspace had login details slip out among encrypted traffic Did Slack send you a password reset link last week? The company has admitted to accidentally exposing the hashed passwords of workspace users.... [...]
