Title: Montenegro says Russian cyberattacks threaten key state functions
Date: 2022-08-29T10:44:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-29-montenegro-says-russian-cyberattacks-threaten-key-state-functions

[Source](https://www.bleepingcomputer.com/news/security/montenegro-says-russian-cyberattacks-threaten-key-state-functions/){:target="_blank" rel="noopener"}

> Members of the government in Montenegro are stating that the country is being hit with sophisticated and persistent cyberattacks that threaten the country's essential infrastructure. [...]
