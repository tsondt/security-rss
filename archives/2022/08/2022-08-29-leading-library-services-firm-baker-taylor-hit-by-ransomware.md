Title: Leading library services firm Baker & Taylor hit by ransomware
Date: 2022-08-29T13:48:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-29-leading-library-services-firm-baker-taylor-hit-by-ransomware

[Source](https://www.bleepingcomputer.com/news/security/leading-library-services-firm-baker-and-taylor-hit-by-ransomware/){:target="_blank" rel="noopener"}

> Baker & Taylor, which describes itself as the world's largest distributor of books to libraries worldwide, today confirmed it's still working on restoring systems after being hit by ransomware more than a week ago. [...]
