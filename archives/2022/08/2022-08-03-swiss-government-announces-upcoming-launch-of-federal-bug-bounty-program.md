Title: Swiss government announces upcoming launch of federal bug bounty program
Date: 2022-08-03T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-03-swiss-government-announces-upcoming-launch-of-federal-bug-bounty-program

[Source](https://portswigger.net/daily-swig/swiss-government-announces-upcoming-launch-of-federal-bug-bounty-program){:target="_blank" rel="noopener"}

> Bug Bounty Switzerland AG awarded program management contract [...]
