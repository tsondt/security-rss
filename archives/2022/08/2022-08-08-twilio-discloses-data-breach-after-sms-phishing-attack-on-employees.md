Title: Twilio discloses data breach after SMS phishing attack on employees
Date: 2022-08-08T10:37:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-08-twilio-discloses-data-breach-after-sms-phishing-attack-on-employees

[Source](https://www.bleepingcomputer.com/news/security/twilio-discloses-data-breach-after-sms-phishing-attack-on-employees/){:target="_blank" rel="noopener"}

> Cloud communications company Twilio says some of its customers' data was accessed by attackers who breached internal systems after stealing employee credentials in an SMS phishing attack. [...]
