Title: Pirate sites ban in Austria took down Cloudflare CDNs by mistake
Date: 2022-08-29T12:22:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-08-29-pirate-sites-ban-in-austria-took-down-cloudflare-cdns-by-mistake

[Source](https://www.bleepingcomputer.com/news/security/pirate-sites-ban-in-austria-took-down-cloudflare-cdns-by-mistake/){:target="_blank" rel="noopener"}

> Excessive and indiscriminate blocking is underway in Austria, with internet service providers (ISPs) complying to a court order to block pirate sites causing significant collateral damage. [...]
