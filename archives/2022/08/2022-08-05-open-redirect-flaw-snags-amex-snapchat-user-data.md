Title: Open Redirect Flaw Snags Amex, Snapchat User Data
Date: 2022-08-05T13:17:09+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security
Slug: 2022-08-05-open-redirect-flaw-snags-amex-snapchat-user-data

[Source](https://threatpost.com/open-redirect-flaw-snags-amex-snapchat-user-data/180354/){:target="_blank" rel="noopener"}

> Separate phishing campaigns targeting thousands of victims impersonate FedEx and Microsoft, among others, to trick victims. [...]
