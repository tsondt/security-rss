Title: High-School Graduation Prank Hack
Date: 2022-08-31T14:33:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-08-31-high-school-graduation-prank-hack

[Source](https://www.schneier.com/blog/archives/2022/08/high-school-graduation-prank-hack.html){:target="_blank" rel="noopener"}

> This is a fun story, detailing the hack a group of high school students perpetrated against an Illinois school district, hacking 500 screens across a bunch of schools. During the process, the group broke into the school’s IT systems; repurposed software used to monitor students’ computers; discovered a new vulnerability (and reported it ); wrote their own scripts; secretly tested their system at night; and managed to avoid detection in the school’s network. Many of the techniques were not sophisticated, but they were pretty much all illegal. It has a happy ending: no one was prosecuted. A spokesperson for the [...]
