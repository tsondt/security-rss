Title: Scammers Sent Uber to Take Elderly Lady to the Bank
Date: 2022-08-04T15:41:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Web Fraud 2.0;elder scams;email scam;fbi;Travis Hardaway;Uber
Slug: 2022-08-04-scammers-sent-uber-to-take-elderly-lady-to-the-bank

[Source](https://krebsonsecurity.com/2022/08/scammers-sent-uber-to-take-elderly-lady-to-the-bank/){:target="_blank" rel="noopener"}

> Email scammers sent an Uber to the home of an 80-year-old woman who responded to a well-timed email scam, in a bid to make sure she went to the bank and wired money to the fraudsters. In this case, the woman figured out she was being scammed before embarking for the bank, but her story is a chilling reminder of how far crooks will go these days to rip people off. Travis Hardaway is a former music teacher turned app developer from Towson, Md. Hardaway said his mother last month replied to an email she received regarding an appliance installation [...]
