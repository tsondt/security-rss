Title: The Benefits of Making Password Strength More Transparent
Date: 2022-08-16T10:02:01-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-08-16-the-benefits-of-making-password-strength-more-transparent

[Source](https://www.bleepingcomputer.com/news/security/the-benefits-of-making-password-strength-more-transparent/){:target="_blank" rel="noopener"}

> Google is in the process of developing a password strength indicator for its Chrome browser. The good news is that there is an easy way of starting users down the road to using strong passwords even before the new version of Chrome is released. [...]
