Title: 7-Eleven stores in Denmark closed due to a cyberattack
Date: 2022-08-08T10:14:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-08-7-eleven-stores-in-denmark-closed-due-to-a-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/7-eleven-stores-in-denmark-closed-due-to-a-cyberattack/){:target="_blank" rel="noopener"}

> 7-Eleven stores in Denmark shut down today after a cyberattack disrupted stores' payment and checkout systems throughout the country. [...]
