Title: Friday Squid Blogging: The Language of the Jumbo Flying Squid
Date: 2022-08-19T21:05:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-08-19-friday-squid-blogging-the-language-of-the-jumbo-flying-squid

[Source](https://www.schneier.com/blog/archives/2022/08/friday-squid-blogging-the-language-of-the-jumbo-flying-squid.html){:target="_blank" rel="noopener"}

> The jumbo flying squid ( Dosidicus gigas ) uses its color-changing ability as a language : In 2020, however, marine biologists discovered that jumbo flying squid are surprisingly coordinated. Despite their large numbers, the squid rarely bumped into each other or competed for the same prey. The scientists hypothesized that the flickering pigments allowed the squid to quickly communicate complex messages, such as when it was preparing to attack and what it was targeting. The researchers observed that the squid displayed 12 distinct pigmentation patterns in a variety of sequences, similar to how humans arrange words in a sentence. For [...]
