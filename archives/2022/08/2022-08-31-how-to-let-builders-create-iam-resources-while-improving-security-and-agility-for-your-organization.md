Title: How to let builders create IAM resources while improving security and agility for your organization
Date: 2022-08-31T20:38:24+00:00
Author: Jeb Benson
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;IAM;Security;Security Blog
Slug: 2022-08-31-how-to-let-builders-create-iam-resources-while-improving-security-and-agility-for-your-organization

[Source](https://aws.amazon.com/blogs/security/how-to-let-builders-create-iam-resources-while-improving-security-and-agility-for-your-organization/){:target="_blank" rel="noopener"}

> Many organizations restrict permissions to create and manage AWS Identity and Access Management (IAM) resources to a group of privileged users or a central team. This post explains how you can safely grant these permissions to builders – the people who are developing, testing, launching, and managing cloud infrastructure – to speed up your development, [...]
