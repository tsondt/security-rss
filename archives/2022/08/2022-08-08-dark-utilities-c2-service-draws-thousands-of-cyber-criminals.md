Title: Dark Utilities C2 service draws thousands of cyber criminals
Date: 2022-08-08T06:31:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-08-dark-utilities-c2-service-draws-thousands-of-cyber-criminals

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/08/dark_utilities_c2_service/){:target="_blank" rel="noopener"}

> Nascent platform provides miscreants an easier and cheaper way to launch remote access, DDoS, and other attacks A platform that makes it easier for cyber criminals to establish command-and-control (C2) servers has already attracted 3,000 users since launching earlier this year, and will likely expand its client list in the coming months.... [...]
