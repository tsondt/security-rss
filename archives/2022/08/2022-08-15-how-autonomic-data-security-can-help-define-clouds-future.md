Title: How autonomic data security can help define cloud’s future
Date: 2022-08-15T16:00:00+00:00
Author: John Stone
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-08-15-how-autonomic-data-security-can-help-define-clouds-future

[Source](https://cloud.google.com/blog/products/identity-security/clouds-future-points-to-autonomic-data-security/){:target="_blank" rel="noopener"}

> “Ninety percent of all data today was created in the last two years—that’s 2.5 quintillion bytes of data per day,” according to business data analytics company Domo. That would be a mind-bending statistic, except that it’s already five years old. As data usage has undergone drastic expansion and changes in the past five years, so have your business needs for data. Technology such as cloud computing and AI have changed how we use data, derive value from data, and glean insights from data. Your organization is no longer just crunching and re-crunching the same data sets. Data moves, shifts, and [...]
