Title: Microsoft Edge deepens defenses against malicious websites with enhanced security mode
Date: 2022-08-09T16:31:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-09-microsoft-edge-deepens-defenses-against-malicious-websites-with-enhanced-security-mode

[Source](https://portswigger.net/daily-swig/microsoft-edge-deepens-defenses-against-malicious-websites-with-enhanced-security-mode){:target="_blank" rel="noopener"}

> Browser adds defense in depth to prevent abuse of unpatched vulnerabilities [...]
