Title: Update of AWS Security Reference Architecture is now available
Date: 2022-08-03T17:58:23+00:00
Author: Balu Mathew
Category: AWS Security
Tags: Advanced (300);Announcements;Security, Identity, & Compliance;AWS Security Reference Architecture;Multi-account security;Security;Security Blog;SRA
Slug: 2022-08-03-update-of-aws-security-reference-architecture-is-now-available

[Source](https://aws.amazon.com/blogs/security/update-of-aws-security-reference-architecture-is-now-available/){:target="_blank" rel="noopener"}

> We’re happy to announce that an updated version of the AWS Security Reference Architecture (AWS SRA) is now available. The AWS SRA is a holistic set of guidelines for deploying the full complement of AWS security services in a multi-account environment. You can use it to help your organization to design, implement, and manage AWS [...]
