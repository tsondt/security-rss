Title: Starlink satellite dish cracked on stage at Black Hat
Date: 2022-08-12T22:40:19+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-12-starlink-satellite-dish-cracked-on-stage-at-black-hat

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/12/starlink_terminal_hack_black_hat/){:target="_blank" rel="noopener"}

> Once the modchip plans are live, you can, too Black Hat A security researcher has shown how to, with physical access at least, fully take over a Starlink satellite terminal using a homemade modchip.... [...]
