Title: Black Hat USA: Deliberately vulnerable AWS, Azure cloud infrastructure is a pen tester’s playground
Date: 2022-08-11T13:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-11-black-hat-usa-deliberately-vulnerable-aws-azure-cloud-infrastructure-is-a-pen-testers-playground

[Source](https://portswigger.net/daily-swig/black-hat-usa-deliberately-vulnerable-aws-azure-cloud-infrastructure-is-a-pen-testers-playground){:target="_blank" rel="noopener"}

> AWSGoat and AzureGoat tools showcased in Las Vegas this week [...]
