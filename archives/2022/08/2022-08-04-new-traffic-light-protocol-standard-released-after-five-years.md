Title: New Traffic Light Protocol standard released after five years
Date: 2022-08-04T19:03:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-04-new-traffic-light-protocol-standard-released-after-five-years

[Source](https://www.bleepingcomputer.com/news/security/new-traffic-light-protocol-standard-released-after-five-years/){:target="_blank" rel="noopener"}

> The Forum of Incident Response and Security Teams (FIRST) has published TLP 2.0, a new version of its Traffic Light Protocol (TLP) standard, five years after the release of the initial version. [...]
