Title: Conti extortion gangs behind surge of BazarCall phishing attacks
Date: 2022-08-10T16:45:19-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-10-conti-extortion-gangs-behind-surge-of-bazarcall-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/conti-extortion-gangs-behind-surge-of-bazarcall-phishing-attacks/){:target="_blank" rel="noopener"}

> At least three groups split from the Conti ransomware operation have adopted BazarCall phishing tactics as the primary method to gain initial access to a victim's network. [...]
