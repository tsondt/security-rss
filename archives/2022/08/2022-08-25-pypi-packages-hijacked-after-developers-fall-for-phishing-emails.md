Title: PyPI packages hijacked after developers fall for phishing emails
Date: 2022-08-25T07:18:40-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-08-25-pypi-packages-hijacked-after-developers-fall-for-phishing-emails

[Source](https://www.bleepingcomputer.com/news/security/pypi-packages-hijacked-after-developers-fall-for-phishing-emails/){:target="_blank" rel="noopener"}

> A phishing campaign caught yesterday was seen targeting maintainers of Python packages published to the PyPI registry. Python packages 'exotel' and 'spam' are among hundreds seen laced with malware after attackers successfully compromised accounts of maintainers who fell for the phishing email. [...]
