Title: Bot army risk as 3,000+ apps found spilling Twitter API keys
Date: 2022-08-02T14:45:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-08-02-bot-army-risk-as-3000-apps-found-spilling-twitter-api-keys

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/02/cloudsek_twitter_api/){:target="_blank" rel="noopener"}

> Please stop leaving credentials where miscreants can find them Want to build your own army? Engineers at CloudSEK have published a report on how to do just that in terms of bots and Twitter, thanks to API keys leaking from applications.... [...]
