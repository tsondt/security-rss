Title: Universities Put Email Users at Cyber Risk
Date: 2022-08-02T23:02:12+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-08-02-universities-put-email-users-at-cyber-risk

[Source](https://threatpost.com/universities-email-cyber-risk/180342/){:target="_blank" rel="noopener"}

> DMARC analysis by Proofpoint shows that institutions in the U.S. have among some of the poorest protections to prevent domain spoofing and lack protections to block fraudulent emails. [...]
