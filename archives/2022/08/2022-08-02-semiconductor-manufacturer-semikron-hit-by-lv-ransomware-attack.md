Title: Semiconductor manufacturer Semikron hit by LV ransomware attack
Date: 2022-08-02T13:38:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-02-semiconductor-manufacturer-semikron-hit-by-lv-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/semiconductor-manufacturer-semikron-hit-by-lv-ransomware-attack/){:target="_blank" rel="noopener"}

> German power electronics manufacturer Semikron has disclosed that it was hit by a ransomware attack that partially encrypted the company's network. [...]
