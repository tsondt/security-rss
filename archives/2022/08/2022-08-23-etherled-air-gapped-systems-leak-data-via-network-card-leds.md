Title: ETHERLED: Air-gapped systems leak data via network card LEDs
Date: 2022-08-23T07:28:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-23-etherled-air-gapped-systems-leak-data-via-network-card-leds

[Source](https://www.bleepingcomputer.com/news/security/etherled-air-gapped-systems-leak-data-via-network-card-leds/){:target="_blank" rel="noopener"}

> Israeli researcher Mordechai Guri has discovered a new method to exfiltrate data from air-gapped systems using the LED indicators on network cards. Dubbed 'ETHERLED', the method turns the blinking lights into Morse code signals that can be decoded by an attacker. [...]
