Title: Google launches open-source software bug bounty program
Date: 2022-08-30T07:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2022-08-30-google-launches-open-source-software-bug-bounty-program

[Source](https://www.bleepingcomputer.com/news/google/google-launches-open-source-software-bug-bounty-program/){:target="_blank" rel="noopener"}

> Google will now pay security researchers to find and report bugs in the latest versions of Google-released open-source software (Google OSS). [...]
