Title: Make the most of your cloud deployment with Active Assist
Date: 2022-08-18T18:54:00+00:00
Author: Alfonso Hernandez
Category: GCP Security
Tags: Identity & Security;Developers & Practitioners
Slug: 2022-08-18-make-the-most-of-your-cloud-deployment-with-active-assist

[Source](https://cloud.google.com/blog/topics/developers-practitioners/make-most-your-cloud-deployment-active-assist/){:target="_blank" rel="noopener"}

> Why are so many companies moving to the cloud? One reason we hear quite often is cost reduction. The elasticity of cloud services, or its ability to scale up or down as needed, means paying only for what you use. Right up there on the list of reasons is security. This is because developing in the cloud enables greater visibility and governance over your deployment's resources and data. Customers also migrate to the cloud to increase reliability and performance (through cloud vendor-provided backups, disaster recovery, and SLAs). And finally, reduced maintenance and better manageability in the cloud eases the burden [...]
