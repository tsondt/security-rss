Title: Critical hole in Atlassian Bitbucket allows any miscreant to hijack servers
Date: 2022-08-29T18:08:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-29-critical-hole-in-atlassian-bitbucket-allows-any-miscreant-to-hijack-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/29/atlassian_bitbucket_critical_bug/){:target="_blank" rel="noopener"}

> Grab and deploy this backend update if you offer even repo read access A critical command-injection vulnerability in multiple API endpoints of Atlassian Bitbucket Server and Data Center could allow an unauthorized attacker to remotely execute malware, and view, change, and even delete data stored in repositories.... [...]
