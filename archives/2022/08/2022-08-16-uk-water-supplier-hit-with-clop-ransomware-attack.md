Title: U.K. Water Supplier Hit with Clop Ransomware Attack
Date: 2022-08-16T14:30:01+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Critical Infrastructure;Malware
Slug: 2022-08-16-uk-water-supplier-hit-with-clop-ransomware-attack

[Source](https://threatpost.com/water-supplier-hit-clop-ransomware/180422/){:target="_blank" rel="noopener"}

> The incident disrupted corporate IT systems at one company while attackers misidentified the victim in a post on its website that leaked stolen data. [...]
