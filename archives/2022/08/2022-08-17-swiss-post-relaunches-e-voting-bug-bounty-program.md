Title: Swiss Post relaunches e-voting bug bounty program
Date: 2022-08-17T14:28:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-17-swiss-post-relaunches-e-voting-bug-bounty-program

[Source](https://portswigger.net/daily-swig/swiss-post-relaunches-e-voting-bug-bounty-program){:target="_blank" rel="noopener"}

> Ethical hackers invited to stress test election infrastructure [...]
