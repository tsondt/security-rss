Title: Fake Chrome extension 'Internet Download Manager' has 200,000 installs
Date: 2022-08-24T05:45:26-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-08-24-fake-chrome-extension-internet-download-manager-has-200000-installs

[Source](https://www.bleepingcomputer.com/news/security/fake-chrome-extension-internet-download-manager-has-200-000-installs/){:target="_blank" rel="noopener"}

> Google Chrome extension 'Internet Download Manager' installed by more than 200,000 users is adware. The extension has been sitting on the Chrome Web Store since at least June 2019, according to the earliest reviews posted by users. [...]
