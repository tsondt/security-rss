Title: Reckon Russian spies are lurking in your inbox? Check for these IOCs, Microsoft says
Date: 2022-08-16T10:16:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-16-reckon-russian-spies-are-lurking-in-your-inbox-check-for-these-iocs-microsoft-says

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/16/microsoft_russian_spies/){:target="_blank" rel="noopener"}

> Seaborgium targeted dozens of orgs this year alone Microsoft said it disabled accounts used by Russian-linked Seaborgium troupe to phish and steal credentials from its customers as part of the cybercrime gang's illicit spying and data-stealing activities.... [...]
