Title: Don't be surprised if your organization suffers multiple cyberattacks
Date: 2022-08-11T16:15:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-11-dont-be-surprised-if-your-organization-suffers-multiple-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/11/multiple_cyberattacks_sophos/){:target="_blank" rel="noopener"}

> Failing to fix flaws, a crowded threat group scene, RaaS, and dependencies among crooks are fueling the trend Black Hat Security experts spent years warning enterprises to expect cyberattacks and to plan their defenses accordingly, now Sophos researchers are saying organizations shouldn't be surprised if they get attacked multiple times.... [...]
