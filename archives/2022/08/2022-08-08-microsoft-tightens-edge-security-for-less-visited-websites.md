Title: Microsoft tightens Edge security for less visited websites
Date: 2022-08-08T17:15:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-08-microsoft-tightens-edge-security-for-less-visited-websites

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/08/microsoft_edge_security_browsing/){:target="_blank" rel="noopener"}

> We're pretty sure that doesn't mean it's safe to click on sketchy popups Microsoft wants to make it safer for Edge users to browse and visit unfamiliar websites by automatically applying stronger security settings.... [...]
