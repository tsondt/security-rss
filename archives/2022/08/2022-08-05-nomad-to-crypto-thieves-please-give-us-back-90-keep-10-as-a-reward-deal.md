Title: Nomad to crypto thieves: Please give us back 90%, keep 10% as a reward. Deal?
Date: 2022-08-05T19:43:59+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-05-nomad-to-crypto-thieves-please-give-us-back-90-keep-10-as-a-reward-deal

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/05/nomad_white_hat_payback/){:target="_blank" rel="noopener"}

> The Feds may see things differently Cryptocurrency bridge Nomad sent a message to the looters who drained nearly $200 million in tokens from its coffers earlier this week: return at least 90 percent of the ill-gotten gains, keep 10 percent as a bounty for discovering the security flaw, and Nomad will consider this a "white-hat" hack, as opposed to plain old theft, and not take legal action.... [...]
