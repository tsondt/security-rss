Title: Hackers target hotel and travel companies with fake reservations
Date: 2022-08-21T10:12:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-21-hackers-target-hotel-and-travel-companies-with-fake-reservations

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-hotel-and-travel-companies-with-fake-reservations/){:target="_blank" rel="noopener"}

> A hacker tracked as TA558 has upped their activity this year, running phishing campaigns that target multiple hotels and firms in the hospitality and travel space. [...]
