Title: Microsoft blocks UEFI bootloaders enabling Windows Secure Boot bypass
Date: 2022-08-12T15:10:22-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-08-12-microsoft-blocks-uefi-bootloaders-enabling-windows-secure-boot-bypass

[Source](https://www.bleepingcomputer.com/news/security/microsoft-blocks-uefi-bootloaders-enabling-windows-secure-boot-bypass/){:target="_blank" rel="noopener"}

> Some signed third-party bootloaders for the Unified Extensible Firmware Interface (UEFI) used by Windows could allow attackers to execute unauthorized code in an early stage of the boot process, before the operating system loads. [...]
