Title: DuckDuckGo browser now blocks all Microsoft trackers, most of the time
Date: 2022-08-05T08:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-05-duckduckgo-browser-now-blocks-all-microsoft-trackers-most-of-the-time

[Source](https://www.bleepingcomputer.com/news/security/duckduckgo-browser-now-blocks-all-microsoft-trackers-most-of-the-time/){:target="_blank" rel="noopener"}

> DuckDuckGo announced today that they will now be blocking all third-party Microsoft tracking scripts in their privacy browser after failing to block them in the past. [...]
