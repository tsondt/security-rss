Title: You can’t choose when you’ll be hit by ransomware, but you can choose how you prepare
Date: 2022-08-03T15:46:04+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: 2022-08-03-you-cant-choose-when-youll-be-hit-by-ransomware-but-you-can-choose-how-you-prepare

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/03/you_cant_choose_when_youll/){:target="_blank" rel="noopener"}

> Without a road to recovery, you’re just going to be roadkill Sponosred Feature What sort of disaster would you rather prepare for? Hurricanes are destructive, but you know when one's coming, giving you time to take defensive action. Earthquakes vary in their destructive power, but you never know when they're going to hit, meaning your ability to recover after the impact is critical.... [...]
