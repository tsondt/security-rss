Title: Hiding a phishing attack behind the AWS cloud
Date: 2022-08-22T21:00:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-22-hiding-a-phishing-attack-behind-the-aws-cloud

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/22/aws_cloud_phishing/){:target="_blank" rel="noopener"}

> Scammers are using cloud services to create and host web pages that can be used to lure victims into handing over their credentials Criminals are slipping phishing emails past automated security scanners inside Amazon Web Services (AWS) to establish a launching pad for attacks.... [...]
