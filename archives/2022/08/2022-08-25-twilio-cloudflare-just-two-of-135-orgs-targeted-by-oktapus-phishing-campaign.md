Title: Twilio, Cloudflare just two of 135 orgs targeted by Oktapus phishing campaign
Date: 2022-08-25T22:57:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-25-twilio-cloudflare-just-two-of-135-orgs-targeted-by-oktapus-phishing-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/25/twilio_cloudflare_oktapus_phishing/){:target="_blank" rel="noopener"}

> This, this is more like what we mean by a sophisticated cyberattack Criminals behind the cyberattack attempts on Twilio and Cloudflare earlier this month had cast a much wider net in their phishing expedition, targeting as many as 135 organizations — primarily IT, software development and cloud services providers based in the US.... [...]
