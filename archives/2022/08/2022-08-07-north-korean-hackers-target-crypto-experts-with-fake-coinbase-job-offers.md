Title: North Korean hackers target crypto experts with fake Coinbase job offers
Date: 2022-08-07T11:14:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-07-north-korean-hackers-target-crypto-experts-with-fake-coinbase-job-offers

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-target-crypto-experts-with-fake-coinbase-job-offers/){:target="_blank" rel="noopener"}

> A new social engineering campaign by the notorious North Korean Lazarus hacking group has been discovered, with the hackers impersonating Coinbase to target employees in the fintech industry. [...]
