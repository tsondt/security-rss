Title: LastPass flags security incident after attackers stole source code, technical information
Date: 2022-08-26T10:52:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-26-lastpass-flags-security-incident-after-attackers-stole-source-code-technical-information

[Source](https://portswigger.net/daily-swig/lastpass-flags-security-incident-after-attackers-stole-source-code-technical-information){:target="_blank" rel="noopener"}

> Users’ master passwords are safe, thanks to company’s ‘zero knowledge’ architecture [...]
