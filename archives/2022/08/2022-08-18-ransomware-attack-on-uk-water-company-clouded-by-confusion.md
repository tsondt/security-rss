Title: Ransomware attack on UK water company clouded by confusion
Date: 2022-08-18T06:28:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-18-ransomware-attack-on-uk-water-company-clouded-by-confusion

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/18/clop_ransomware_uk_water/){:target="_blank" rel="noopener"}

> Clop gang thought it hit Thames Water – but real victim was elsewhere A water company in the drought-hit UK was recently compromised by a ransomware gang, though initially it was unclear exactly which water company was the victim.... [...]
