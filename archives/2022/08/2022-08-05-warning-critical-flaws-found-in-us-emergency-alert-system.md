Title: Warning! Critical flaws found in US Emergency Alert System
Date: 2022-08-05T18:05:55+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-05-warning-critical-flaws-found-in-us-emergency-alert-system

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/05/eas_vulnerabilities_dhs/){:target="_blank" rel="noopener"}

> DEF CON may be about to blow lid off security hole The US government is warning of critical vulnerabilities in its Emergency Alert System (EAS) systems that, if exploited, could enable intruders to send fake alerts out over television, radio, and cable networks.... [...]
