Title: Browser-powered desync: New class of HTTP request smuggling attacks showcased at Black Hat USA
Date: 2022-08-11T16:21:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-11-browser-powered-desync-new-class-of-http-request-smuggling-attacks-showcased-at-black-hat-usa

[Source](https://portswigger.net/daily-swig/browser-powered-desync-new-class-of-http-request-smuggling-attacks-showcased-at-black-hat-usa){:target="_blank" rel="noopener"}

> Renowned researcher James Kettle demonstrates his latest attack technique in Las Vegas [...]
