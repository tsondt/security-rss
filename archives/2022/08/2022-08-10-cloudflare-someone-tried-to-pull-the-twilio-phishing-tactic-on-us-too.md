Title: Cloudflare: Someone tried to pull the Twilio phishing tactic on us too
Date: 2022-08-10T14:23:11+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-08-10-cloudflare-someone-tried-to-pull-the-twilio-phishing-tactic-on-us-too

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/10/cloudflare_twilio_phishing/){:target="_blank" rel="noopener"}

> Attack was foiled by content delivery network's hardware security keys Cloudflare says it was subject to a similar attack to one made on comms company Twilio last week, but in this case it was thwarted by hardware security keys that are required to access applications and services.... [...]
