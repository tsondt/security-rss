Title: New GwisinLocker ransomware encrypts Windows and Linux ESXi servers
Date: 2022-08-06T10:05:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-06-new-gwisinlocker-ransomware-encrypts-windows-and-linux-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/new-gwisinlocker-ransomware-encrypts-windows-and-linux-esxi-servers/){:target="_blank" rel="noopener"}

> A new ransomware family called 'GwisinLocker' targets South Korean healthcare, industrial, and pharmaceutical companies with Windows and Linux encryptors, including support for encrypting VMware ESXi servers and virtual machines. [...]
