Title: LockBit gang hit by DDoS attack after threatening to leak Entrust ransomware data
Date: 2022-08-22T16:08:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-08-22-lockbit-gang-hit-by-ddos-attack-after-threatening-to-leak-entrust-ransomware-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/22/entrust_lockbit_ddos_ransomware/){:target="_blank" rel="noopener"}

> Prolific group pummeled days after claiming to be file thief behind attack on cybersecurity vendor The LockBit ransomware group last week claimed responsibility for an attack on cybersecurity vendor in June. The high-profile gang is now apparently under a distributed denial-of-service (DDoS) because of it.... [...]
