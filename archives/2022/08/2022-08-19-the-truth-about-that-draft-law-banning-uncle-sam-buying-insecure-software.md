Title: The truth about that draft law banning Uncle Sam buying insecure software
Date: 2022-08-19T02:22:32+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-19-the-truth-about-that-draft-law-banning-uncle-sam-buying-insecure-software

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/19/dod_spending_bill/){:target="_blank" rel="noopener"}

> There's always a get-out clause An attempt by lawmakers to improve parts of the US government's cybersecurity defenses has raised questions – and hackles – among infosec professionals.... [...]
