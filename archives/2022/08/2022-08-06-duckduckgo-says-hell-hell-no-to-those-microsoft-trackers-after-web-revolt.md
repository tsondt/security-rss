Title: DuckDuckGo says Hell, Hell, No to those Microsoft trackers after web revolt
Date: 2022-08-06T19:41:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-08-06-duckduckgo-says-hell-hell-no-to-those-microsoft-trackers-after-web-revolt

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/06/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: That Twitter privacy leak, scammers send Ubers for victims, critical flaw in Cisco gear, and more In brief DuckDuckGo has finally mostly cracked down on the third-party Microsoft tracking scripts that got the alternative search engine into hot water earlier this year.... [...]
