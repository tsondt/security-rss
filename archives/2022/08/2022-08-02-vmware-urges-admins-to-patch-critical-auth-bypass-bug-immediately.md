Title: VMware urges admins to patch critical auth bypass bug immediately
Date: 2022-08-02T10:51:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-02-vmware-urges-admins-to-patch-critical-auth-bypass-bug-immediately

[Source](https://www.bleepingcomputer.com/news/security/vmware-urges-admins-to-patch-critical-auth-bypass-bug-immediately/){:target="_blank" rel="noopener"}

> VMware has warned admins today to patch a critical authentication bypass security flaw affecting local domain users in multiple products and enabling unauthenticated attackers to gain admin privileges. [...]
