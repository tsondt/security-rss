Title: TikTok wants your trust around US midterm elections data
Date: 2022-08-17T16:00:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-08-17-tiktok-wants-your-trust-around-us-midterm-elections-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/17/tiktok_midterm_elections_policy/){:target="_blank" rel="noopener"}

> Misinformation's a concern, but Chinese media giant's own data privacy practices also have people worried TikTok has joined Twitter in publishing new US midterm misinformation rules, with considerable crossover in scope and style.... [...]
