Title: Snapchat, Amex sites abused in Microsoft 365 phishing attacks
Date: 2022-08-07T10:12:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-07-snapchat-amex-sites-abused-in-microsoft-365-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/snapchat-amex-sites-abused-in-microsoft-365-phishing-attacks/){:target="_blank" rel="noopener"}

> Attackers abused open redirects on the websites of Snapchat and American Express in a series of phishing attacks to steal Microsoft 365 credentials. [...]
