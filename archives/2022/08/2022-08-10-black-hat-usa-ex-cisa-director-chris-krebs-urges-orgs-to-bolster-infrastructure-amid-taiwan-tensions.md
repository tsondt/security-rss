Title: Black Hat USA: Ex-CISA director Chris Krebs urges orgs to bolster infrastructure amid Taiwan tensions
Date: 2022-08-10T21:05:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-10-black-hat-usa-ex-cisa-director-chris-krebs-urges-orgs-to-bolster-infrastructure-amid-taiwan-tensions

[Source](https://portswigger.net/daily-swig/black-hat-usa-ex-cisa-director-chris-krebs-urges-orgs-to-bolster-infrastructure-amid-taiwan-tensions){:target="_blank" rel="noopener"}

> Attack on Taiwan seemingly a case of ‘when’ not ‘if’ Chris Krebs, the former director of the US Cybersecurity and Infrastructure Security Agency (CISA), says the infosec industry is “bearish in the sh [...]
