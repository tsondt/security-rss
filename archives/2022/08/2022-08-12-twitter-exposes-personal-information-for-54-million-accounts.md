Title: Twitter Exposes Personal Information for 5.4 Million Accounts
Date: 2022-08-12T14:13:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-08-12-twitter-exposes-personal-information-for-54-million-accounts

[Source](https://www.schneier.com/blog/archives/2022/08/twitter-exposes-personal-information-for-5-4-million-accounts.html){:target="_blank" rel="noopener"}

> Twitter accidentally exposed the personal information—including phone numbers and email addresses—for 5.4 million accounts. And someone was trying to sell this information. In January 2022, we received a report through our bug bounty program of a vulnerability in Twitter’s systems. As a result of the vulnerability, if someone submitted an email address or phone number to Twitter’s systems, Twitter’s systems would tell the person what Twitter account the submitted email addresses or phone number was associated with, if any. This bug resulted from an update to our code in June 2021. When we learned about this, we immediately investigated and [...]
