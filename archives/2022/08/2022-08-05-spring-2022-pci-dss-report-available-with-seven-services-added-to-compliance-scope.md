Title: Spring 2022 PCI DSS report available with seven services added to compliance scope
Date: 2022-08-05T19:16:17+00:00
Author: Michael Oyeniya
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;Uncategorized;Compliance;Compliance reports;PCI;PCI DSS;Security Blog
Slug: 2022-08-05-spring-2022-pci-dss-report-available-with-seven-services-added-to-compliance-scope

[Source](https://aws.amazon.com/blogs/security/spring-2022-pci-dss-report-available-with-seven-services-added-to-compliance-scope/){:target="_blank" rel="noopener"}

> We’re continuing to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that seven new services have been added to the scope of our Payment Card Industry Data Security Standard (PCI DSS) certification. This provides our customers with more options to process and store their payment card [...]
