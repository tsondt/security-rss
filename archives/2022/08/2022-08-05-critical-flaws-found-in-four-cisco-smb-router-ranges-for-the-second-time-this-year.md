Title: Critical flaws found in four Cisco SMB router ranges – for the second time this year
Date: 2022-08-05T06:57:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-08-05-critical-flaws-found-in-four-cisco-smb-router-ranges-for-the-second-time-this-year

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/05/cisco_smb_routers_critical_flaws/){:target="_blank" rel="noopener"}

> At least Switchzilla thinks they're salvageable, unlike the boxes it ordered binned back in June Cisco has revealed four of its small business router ranges have critical flaws – for the second time in 2022 alone.... [...]
