Title: FTC Sues Data Broker
Date: 2022-08-30T11:58:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-08-30-ftc-sues-data-broker

[Source](https://www.schneier.com/blog/archives/2022/08/ftc-sues-data-broker.html){:target="_blank" rel="noopener"}

> This is good news: The Federal Trade Commission (FTC) has sued Kochava, a large location data provider, for allegedly selling data that the FTC says can track people at reproductive health clinics and places of worship, according to an announcement from the agency. “Defendant’s violations are in connection with acquiring consumers’ precise geolocation data and selling the data in a format that allows entities to track the consumers’ movements to and from sensitive locations, including, among others, locations associated with medical care, reproductive health, religious worship, mental health temporary shelters, such as shelters for the homeless, domestic violence survivors, or [...]
