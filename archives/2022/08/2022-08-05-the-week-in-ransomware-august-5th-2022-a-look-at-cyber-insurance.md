Title: The Week in Ransomware - August 5th 2022 - A look at cyber insurance
Date: 2022-08-05T17:35:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-05-the-week-in-ransomware-august-5th-2022-a-look-at-cyber-insurance

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-august-5th-2022-a-look-at-cyber-insurance/){:target="_blank" rel="noopener"}

> For the most part, it has been a quiet week on the ransomware front, with a few new reports, product developments, and attacks revealed. [...]
