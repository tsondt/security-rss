Title: Two years on, Apple iOS VPNs still leak IP addresses
Date: 2022-08-19T07:37:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-08-19-two-years-on-apple-ios-vpns-still-leak-ip-addresses

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/19/apple_ios_vpn/){:target="_blank" rel="noopener"}

> Privacy, it's a useful marketing term *Offer does not apply in China Apple has left a VPN bypass vulnerability in iOS unfixed for at least two years, leaving identifying IP traffic data exposed, and there's no sign of a fix.... [...]
