Title: Intel ups protection against physical chip attacks in Alder Lake
Date: 2022-08-12T15:00:08+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-08-12-intel-ups-protection-against-physical-chip-attacks-in-alder-lake

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/12/intel_ups_protection_against_chip/){:target="_blank" rel="noopener"}

> Repurposes logic originally used for spotting variations in voltage, timing in older circuits to help performance Black Hat Intel has disclosed how it may be able to protect systems against some physical threats by repurposing circuitry originally designed to counter variations in voltage and timing that may occur as silicon circuits age.... [...]
