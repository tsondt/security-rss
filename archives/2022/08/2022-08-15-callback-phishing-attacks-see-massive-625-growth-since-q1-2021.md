Title: Callback phishing attacks see massive 625% growth since Q1 2021
Date: 2022-08-15T10:32:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-15-callback-phishing-attacks-see-massive-625-growth-since-q1-2021

[Source](https://www.bleepingcomputer.com/news/security/callback-phishing-attacks-see-massive-625-percent-growth-since-q1-2021/){:target="_blank" rel="noopener"}

> Phishing is constantly evolving to bypass user training and email protections, and as threat actors adopt new tactics with better success ratios, quarterly stats reflect interesting threat trends on multiple fronts. [...]
