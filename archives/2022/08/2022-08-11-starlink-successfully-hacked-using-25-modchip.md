Title: Starlink Successfully Hacked Using $25 Modchip
Date: 2022-08-11T15:48:15+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Black Hat;IoT;News
Slug: 2022-08-11-starlink-successfully-hacked-using-25-modchip

[Source](https://threatpost.com/starlink-hack/180389/){:target="_blank" rel="noopener"}

> Belgian researcher Lennert Wouters revealed at Black Hat how he mounted a successful fault injection attack on a user terminal for SpaceX’s satellite-based internet system [...]
