Title: 77% of security leaders fear we’re in perpetual cyberwar from now on
Date: 2022-08-27T07:49:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-08-27-77-of-security-leaders-fear-were-in-perpetual-cyberwar-from-now-on

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/27/in-brief-security/){:target="_blank" rel="noopener"}

> Also, Charming Kittens from Iran scrape email inboxes, France could fine Google again, and more In brief A survey of cybersecurity decision makers found 77 percent think the world is now in a perpetual state of cyberwarfare.... [...]
