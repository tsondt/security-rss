Title: AWS CyberVadis report now available for due diligence on third-party suppliers
Date: 2022-08-17T17:36:27+00:00
Author: Andreas Terwellen
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;3P Risk;AWS security;Compliance;Cyber Risk Management;CyberVadis;Security Blog;Third Party Risk;Third-Party Risk Management;TPCRM;TPRM
Slug: 2022-08-17-aws-cybervadis-report-now-available-for-due-diligence-on-third-party-suppliers

[Source](https://aws.amazon.com/blogs/security/aws-cybervadis-report-now-available-for-due-diligence-on-third-party-suppliers/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we’re continuously expanding our compliance programs to provide you with more tools and resources to perform effective due diligence on AWS. We’re excited to announce the availability of the AWS CyberVadis report to help you reduce the burden of performing due diligence on your third-party suppliers. With the increase in [...]
