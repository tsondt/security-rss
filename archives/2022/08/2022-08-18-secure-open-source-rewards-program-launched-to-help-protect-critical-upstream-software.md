Title: Secure Open Source Rewards program launched to help protect critical upstream software
Date: 2022-08-18T12:09:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-18-secure-open-source-rewards-program-launched-to-help-protect-critical-upstream-software

[Source](https://portswigger.net/daily-swig/secure-open-source-rewards-program-launched-to-help-protect-critical-upstream-software){:target="_blank" rel="noopener"}

> SOS.dev initiative will combat software supply chain attacks by encouraging researchers to suggest security improvements to key projects [...]
