Title: A Taxonomy of Access Control
Date: 2022-08-12T11:38:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;cryptography;keys;taxonomies
Slug: 2022-08-12-a-taxonomy-of-access-control

[Source](https://www.schneier.com/blog/archives/2022/08/a-taxonomy-of-access-control.html){:target="_blank" rel="noopener"}

> My personal definition of a brilliant idea is one that is immediately obvious once it’s explained, but no one has thought of it before. I can’t believe that no one has described this taxonomy of access control before Eyal Ittay laid it out in this paper. The paper is about cryptocurrency wallet design, but the ideas are more general. Ittay points out that a key—or an account, or anything similar—can be in one of four states: safe Only the user has access, loss No one has access, leak Both the user and the adversary have access, or theft Only the [...]
