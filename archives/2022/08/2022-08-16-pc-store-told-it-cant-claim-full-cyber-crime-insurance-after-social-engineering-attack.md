Title: PC store told it can't claim full cyber-crime insurance after social-engineering attack
Date: 2022-08-16T16:43:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-08-16-pc-store-told-it-cant-claim-full-cyber-crime-insurance-after-social-engineering-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/16/social_engineering_cyber_crime_insurance/){:target="_blank" rel="noopener"}

> Two different kinds of fraud, says judge while throwing out lawsuit against insurer A Minnesota computer store suing its crime insurance provider has had its case dismissed, with the courts saying it was a clear instance of social engineering, a crime for which the insurer was only liable to cover a fraction of total losses.... [...]
