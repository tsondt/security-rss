Title: How to help ensure smooth shift handoffs in security operations
Date: 2022-08-02T16:00:00+00:00
Author: Dan Kaplan
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-08-02-how-to-help-ensure-smooth-shift-handoffs-in-security-operations

[Source](https://cloud.google.com/blog/products/identity-security/how-to-nail-smooth-shift-handoffs-in-secops/){:target="_blank" rel="noopener"}

> Editor's note : This blog was originally published by Siemplify on Oct. 29, 2019. Much the same way that nursing teams need to share patient healthcare updates when their shift ends, security operations centers (SOC) need to have smooth shift-handoff procedures in place to ensure that continuous monitoring of their networks and systems is maintained. Without proper planning, knowledge gaps can arise during the shift-change process. These include: Incomplete details : Updates, such as the work that has been done to address active incidents and the proposed duties to continue these efforts, are not thoroughly shared. Incorrect assumptions : Operating [...]
