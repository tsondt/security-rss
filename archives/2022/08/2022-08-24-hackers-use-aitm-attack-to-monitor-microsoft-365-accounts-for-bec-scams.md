Title: Hackers use AiTM attack to monitor Microsoft 365 accounts for BEC scams
Date: 2022-08-24T11:53:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-08-24-hackers-use-aitm-attack-to-monitor-microsoft-365-accounts-for-bec-scams

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-aitm-attack-to-monitor-microsoft-365-accounts-for-bec-scams/){:target="_blank" rel="noopener"}

> A new business email compromise (BEC) campaign has been discovered combining sophisticated spear-phishing with Adversary-in-The-Middle (AiTM) tactics to hack corporate executives' Microsoft 365 accounts, even those protected by MFA. [...]
