Title: How Twitter’s whistleblower could boost Elon Musk’s legal battle
Date: 2022-08-24T05:00:27+00:00
Author: Kari Paul
Category: The Guardian
Tags: Twitter;Elon Musk;Technology;US news;Data and computer security;Social media;Spam
Slug: 2022-08-24-how-twitters-whistleblower-could-boost-elon-musks-legal-battle

[Source](https://www.theguardian.com/technology/2022/aug/23/twitter-whistleblower-elon-musk-termination-penalty){:target="_blank" rel="noopener"}

> Peiter Zatko, former security chief, brought allegations of widespread security threats and spam concerns against the company New whistleblower allegations of widespread security threats and spam concerns at Twitter may give Elon Musk ammunition in his fight to back out of a deal to buy the company. On Tuesday, an 84-page complaint written by Twitter’s former security chief turned whistleblower, Peiter Zatko, alleged that Twitter prioritizes user growth over reducing spam, did not have a plan in place for major security issues, and that half the company’s servers were running out-of-date and vulnerable software. Continue reading... [...]
