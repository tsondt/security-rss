Title: Kali Linux 2022.3 adds 5 new tools, updates Linux kernel, and more
Date: 2022-08-09T15:23:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Linux
Slug: 2022-08-09-kali-linux-20223-adds-5-new-tools-updates-linux-kernel-and-more

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20223-adds-5-new-tools-updates-linux-kernel-and-more/){:target="_blank" rel="noopener"}

> Offensive Security has released ​Kali Linux 2022.3, the third version of 2022, with virtual machine improvements, Linux Kernel 5.18.5, new tools to play with, and improved ARM support. [...]
