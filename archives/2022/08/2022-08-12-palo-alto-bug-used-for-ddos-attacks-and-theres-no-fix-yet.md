Title: Palo Alto bug used for DDoS attacks and there's no fix yet
Date: 2022-08-12T23:17:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-08-12-palo-alto-bug-used-for-ddos-attacks-and-theres-no-fix-yet

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/12/palo_alto_bug/){:target="_blank" rel="noopener"}

> There goes the weekend... A high-severity Palo Alto Networks denial-of-service (DoS) vulnerability has been exploited by miscreants looking to launch DDoS attacks, and several of the affected products won't have a patch until next week.... [...]
