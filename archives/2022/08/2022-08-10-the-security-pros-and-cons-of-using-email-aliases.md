Title: The Security Pros and Cons of Using Email Aliases
Date: 2022-08-10T15:10:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Security Tools;alex holden;Allekabels;email alias;HaveIBeenPwned.com;Hold Security;RTL Nieuws;Troy Hunt
Slug: 2022-08-10-the-security-pros-and-cons-of-using-email-aliases

[Source](https://krebsonsecurity.com/2022/08/the-security-pros-and-cons-of-using-email-aliases/){:target="_blank" rel="noopener"}

> One way to tame your email inbox is to get in the habit of using unique email aliases when signing up for new accounts online. Adding a “+” character after the username portion of your email address — followed by a notation specific to the site you’re signing up at — lets you create an infinite number of unique email addresses tied to the same account. Aliases can help users detect breaches and fight spam. But not all websites allow aliases, and they can complicate account recovery. Here’s a look at the pros and cons of adopting a unique alias [...]
