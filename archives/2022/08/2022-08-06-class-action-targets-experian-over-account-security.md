Title: Class Action Targets Experian Over Account Security
Date: 2022-08-06T01:54:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm
Slug: 2022-08-06-class-action-targets-experian-over-account-security

[Source](https://krebsonsecurity.com/2022/08/class-action-targets-experian-over-account-security/){:target="_blank" rel="noopener"}

> A class action lawsuit has been filed against big-three consumer credit bureau Experian over reports that the company did little to prevent identity thieves from hijacking consumer accounts. The legal filing cites liberally from an investigation KrebsOnSecurity published in July, which found that identity thieves were able to assume control over existing Experian accounts simply by signing up for new accounts using the victim’s personal information and a different email address. The lawsuit, filed July 28, 2022 in California Central District Court, argues that Experian’s documented practice of allowing the re-registration of existing Experian accounts without first verifying that the [...]
