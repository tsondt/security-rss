Title: Friday Squid Blogging: New Squid Species
Date: 2022-08-05T21:13:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-08-05-friday-squid-blogging-new-squid-species

[Source](https://www.schneier.com/blog/archives/2022/08/friday-squid-blogging-new-squid-species.html){:target="_blank" rel="noopener"}

> Seems like they are being discovered all the time: In the past, the DEEPEND crew has discovered three new species of Bathyteuthids, a type of squid that lives in depths between 700 and 2,000 meters. The findings were validated and published in 2020. Another new squid species description is currently in review at the Bulletin of Marine Science. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
