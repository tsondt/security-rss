Title: 10 malicious Python packages exposed in latest repository attack
Date: 2022-08-09T18:01:33+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Tech;GitHub;malware;npm;pypi;python;sigstore;software supply chain attack;supply chain attack
Slug: 2022-08-09-10-malicious-python-packages-exposed-in-latest-repository-attack

[Source](https://arstechnica.com/?p=1872326){:target="_blank" rel="noopener"}

> Enlarge / Supply-chain attacks, like the latest PyPi discovery, insert malicious code into seemingly functional software packages used by developers. They're becoming increasingly common. (credit: Getty Images) Researchers have discovered yet another set of malicious packages in PyPi, the official and most popular repository for Python programs and code libraries. Those duped by the seemingly familiar packages could be subject to malware downloads or theft of user credentials and passwords. Check Point Research, which reported its findings Monday, wrote that it didn't know how many people had downloaded the 10 packages, but it noted that PyPi has 613,000 active users, [...]
