Title: XSS in Gmail’s AMP For Email earns researcher $5,000
Date: 2022-08-05T15:59:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-05-xss-in-gmails-amp-for-email-earns-researcher-5000

[Source](https://portswigger.net/daily-swig/xss-in-gmails-amp-for-email-earns-researcher-5-000){:target="_blank" rel="noopener"}

> Researcher bypasses email filter with inspired style tag trickery [...]
