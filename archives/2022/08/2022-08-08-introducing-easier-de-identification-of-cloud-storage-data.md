Title: Introducing easier de-identification of Cloud Storage data
Date: 2022-08-08T16:00:00+00:00
Author: Jordanna Chord
Category: GCP Security
Tags: Storage & Data Transfer;Data Analytics;Databases;No-code Development;Infrastructure Modernization;Identity & Security
Slug: 2022-08-08-introducing-easier-de-identification-of-cloud-storage-data

[Source](https://cloud.google.com/blog/products/identity-security/announcing-easier-de-identification-of-google-cloud-storage-data/){:target="_blank" rel="noopener"}

> De-identification of Cloud Storage just got easier Many organizations require effective processes and techniques for removing or obfuscating certain sensitive information in the data they store. An important tool to achieve this goal is de-identification. Defined by NIST as a technique that “removes identifying information from a dataset so that individual data cannot be linked with specific individuals. De-identification can reduce the privacy risk associated with collecting, processing, archiving, distributing or publishing information.” Always striving to make data security easier, today we are happy to announce the availability of a de-identification action for our Cloud Storage inspection jobs. Now, you [...]
