Title: Cisco fixes bug allowing RSA private key theft on ASA, FTD devices
Date: 2022-08-10T13:37:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-10-cisco-fixes-bug-allowing-rsa-private-key-theft-on-asa-ftd-devices

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-bug-allowing-rsa-private-key-theft-on-asa-ftd-devices/){:target="_blank" rel="noopener"}

> Cisco has addressed a high severity vulnerability affecting its Adaptive Security Appliance (ASA) and Firepower Threat Defense (FTD) software. [...]
