Title: Hackers attack UK water supplier but extort wrong company
Date: 2022-08-16T05:05:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-16-hackers-attack-uk-water-supplier-but-extort-wrong-company

[Source](https://www.bleepingcomputer.com/news/security/hackers-attack-uk-water-supplier-but-extort-wrong-company/){:target="_blank" rel="noopener"}

> South Staffordshire Water, a company supplying 330 million liters of drinking water to 1.6 consumers daily, has issued a statement confirming IT disruption from a cyberattack. [...]
