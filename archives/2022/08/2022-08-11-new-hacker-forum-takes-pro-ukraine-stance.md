Title: New Hacker Forum Takes Pro-Ukraine Stance
Date: 2022-08-11T15:14:44+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;News;ukraine
Slug: 2022-08-11-new-hacker-forum-takes-pro-ukraine-stance

[Source](https://threatpost.com/pro-ukraine-forum/180387/){:target="_blank" rel="noopener"}

> A uniquely politically motivated site called DUMPS focuses solely on threat activity directed against Russia and Belarus [...]
