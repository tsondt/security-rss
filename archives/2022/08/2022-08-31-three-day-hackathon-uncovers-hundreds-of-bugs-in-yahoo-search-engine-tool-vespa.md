Title: Three-day hackathon uncovers hundreds of bugs in Yahoo search engine tool Vespa
Date: 2022-08-31T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-31-three-day-hackathon-uncovers-hundreds-of-bugs-in-yahoo-search-engine-tool-vespa

[Source](https://portswigger.net/daily-swig/three-day-hackathon-uncovers-hundreds-of-bugs-in-yahoo-search-engine-tool-vespa){:target="_blank" rel="noopener"}

> Live event brings together bug bounty hunters from across the globe [...]
