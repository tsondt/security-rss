Title: LockBit ransomware blames Entrust for DDoS attacks on leak sites
Date: 2022-08-22T10:39:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-22-lockbit-ransomware-blames-entrust-for-ddos-attacks-on-leak-sites

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-blames-entrust-for-ddos-attacks-on-leak-sites/){:target="_blank" rel="noopener"}

> The LockBit ransomware operation's data leak sites have been shut down over the weekend due to a DDoS attack telling them to remove Entrust's allegedly stolen data. [...]
