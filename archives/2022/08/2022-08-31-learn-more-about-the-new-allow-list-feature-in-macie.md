Title: Learn more about the new allow list feature in Macie
Date: 2022-08-31T16:07:57+00:00
Author: Jonathan Nguyen
Category: AWS Security
Tags: Security, Identity, & Compliance;Technical How-to;data privacy;data security;Macie;new update scanning;Security Blog;Sensitive data
Slug: 2022-08-31-learn-more-about-the-new-allow-list-feature-in-macie

[Source](https://aws.amazon.com/blogs/security/learn-more-about-the-new-allow-list-feature-in-macie/){:target="_blank" rel="noopener"}

> Amazon Macie is a fully managed data security and data privacy service that uses machine learning and pattern matching to discover and help you protect your sensitive data in Amazon Web Services (AWS). The data that is available within your AWS account can grow rapidly, which increases your need to verify that all sensitive data [...]
