Title: Microsoft announces new external attack surface audit tool
Date: 2022-08-02T09:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-08-02-microsoft-announces-new-external-attack-surface-audit-tool

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-announces-new-external-attack-surface-audit-tool/){:target="_blank" rel="noopener"}

> Microsoft has announced a new security product allowing security teams to spot Internet-exposed resources in their organization's environment that attackers could use to breach their networks. [...]
