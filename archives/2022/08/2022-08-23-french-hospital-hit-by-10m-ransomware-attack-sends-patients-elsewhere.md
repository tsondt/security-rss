Title: French hospital hit by $10M ransomware attack, sends patients elsewhere
Date: 2022-08-23T12:23:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-23-french-hospital-hit-by-10m-ransomware-attack-sends-patients-elsewhere

[Source](https://www.bleepingcomputer.com/news/security/french-hospital-hit-by-10m-ransomware-attack-sends-patients-elsewhere/){:target="_blank" rel="noopener"}

> The Center Hospitalier Sud Francilien (CHSF), a 1000-bed hospital located 28km from the center of Paris, suffered a cyberattack on Sunday, which has resulted in the medical center referring patients to other establishments and postponing appointments for surgeries. [...]
