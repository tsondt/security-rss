Title: PayPal Phishing Scam Uses Invoices Sent Via PayPal
Date: 2022-08-18T15:27:53+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;Paypal;PayPal Business;PayPayl invoice scam;Zelle Fraud Scam
Slug: 2022-08-18-paypal-phishing-scam-uses-invoices-sent-via-paypal

[Source](https://krebsonsecurity.com/2022/08/paypal-phishing-scam-uses-invoices-sent-via-paypal/){:target="_blank" rel="noopener"}

> Scammers are using invoices sent through PayPal.com to trick recipients into calling a number to dispute a pending charge. The missives — which come from Paypal.com and include a link at Paypal.com that displays an invoice for the supposed transaction — state that the user’s account is about to be charged hundreds of dollars. Recipients who call the supplied toll-free number to contest the transaction are soon asked to download software that lets the scammers assume remote control over their computer. KrebsOnSecurity recently heard from a reader who received an email from paypal.com that he immediately suspected was phony. The [...]
