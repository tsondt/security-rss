Title: Amazon fixes Ring Android app flaw exposing camera recordings
Date: 2022-08-18T06:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-08-18-amazon-fixes-ring-android-app-flaw-exposing-camera-recordings

[Source](https://www.bleepingcomputer.com/news/security/amazon-fixes-ring-android-app-flaw-exposing-camera-recordings/){:target="_blank" rel="noopener"}

> Amazon has fixed a high-severity vulnerability in the Amazon Ring app for Android that could have allowed hackers to download customers' saved camera recordings. [...]
