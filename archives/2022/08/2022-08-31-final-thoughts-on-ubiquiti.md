Title: Final Thoughts on Ubiquiti
Date: 2022-08-31T15:14:29+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other
Slug: 2022-08-31-final-thoughts-on-ubiquiti

[Source](https://krebsonsecurity.com/2022/08/final-thoughts-on-ubiquiti/){:target="_blank" rel="noopener"}

> Last year, I posted a series of articles about a purported “breach” at Ubiquiti. My sole source for that reporting was the person who has since been indicted by federal prosecutors for his alleged wrongdoing – which includes providing false information to the press. As a result of the new information that has been provided to me, I no longer have faith in the veracity of my source or the information he provided to me. I always endeavor to ensure that my articles are properly sourced and factual. This time, I missed the mark and, as a result, I would [...]
