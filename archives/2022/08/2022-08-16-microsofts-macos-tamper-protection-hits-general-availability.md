Title: Microsoft's macOS Tamper Protection hits general availability
Date: 2022-08-16T14:03:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-08-16-microsofts-macos-tamper-protection-hits-general-availability

[Source](https://go.theregister.com/feed/www.theregister.com/2022/08/16/macos_tamper_protection/){:target="_blank" rel="noopener"}

> A boon for administrators having to deal with Apple hardware while also keeping everything secure Microsoft Defender for Endpoint's Tamper Protection in macOS has entered general availability.... [...]
