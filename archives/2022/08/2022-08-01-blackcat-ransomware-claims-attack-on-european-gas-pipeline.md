Title: BlackCat ransomware claims attack on European gas pipeline
Date: 2022-08-01T10:20:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-01-blackcat-ransomware-claims-attack-on-european-gas-pipeline

[Source](https://www.bleepingcomputer.com/news/security/blackcat-ransomware-claims-attack-on-european-gas-pipeline/){:target="_blank" rel="noopener"}

> The ransomware group known as ALPHV (aka BlackCat) has assumed over the weekend responsibility for the cyberattack that hit Creos Luxembourg last week, a natural gas pipeline and electricity network operator in the central European country. [...]
