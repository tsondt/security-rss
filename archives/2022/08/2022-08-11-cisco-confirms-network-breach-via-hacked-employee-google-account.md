Title: Cisco Confirms Network Breach Via Hacked Employee Google Account
Date: 2022-08-11T12:51:34+00:00
Author: Threatpost
Category: Threatpost
Tags: Breach;Hacks
Slug: 2022-08-11-cisco-confirms-network-breach-via-hacked-employee-google-account

[Source](https://threatpost.com/cisco-network-breach-google/180385/){:target="_blank" rel="noopener"}

> Networking giant says attackers gained initial access to an employee’s VPN client via a compromised Google account. [...]
