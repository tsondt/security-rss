Title: Microsoft disrupts Russian hackers' operation on NATO targets
Date: 2022-08-15T14:22:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-08-15-microsoft-disrupts-russian-hackers-operation-on-nato-targets

[Source](https://www.bleepingcomputer.com/news/security/microsoft-disrupts-russian-hackers-operation-on-nato-targets/){:target="_blank" rel="noopener"}

> The Microsoft Threat Intelligence Center (MSTIC) has disrupted a hacking and social engineering operation linked to a Russian threat actor tracked as SEABORGIUM that targets propland organizations in NATO countries. [...]
