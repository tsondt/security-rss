Title: GoTestWAF adds API attack testing via OpenAPI support
Date: 2022-08-12T12:29:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-08-12-gotestwaf-adds-api-attack-testing-via-openapi-support

[Source](https://portswigger.net/daily-swig/gotestwaf-adds-api-attack-testing-via-openapi-support){:target="_blank" rel="noopener"}

> CI/CD support is next for WAF security tool [...]
