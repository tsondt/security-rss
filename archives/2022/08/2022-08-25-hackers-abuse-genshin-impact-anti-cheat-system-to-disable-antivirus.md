Title: Hackers abuse Genshin Impact anti-cheat system to disable antivirus
Date: 2022-08-25T16:05:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-25-hackers-abuse-genshin-impact-anti-cheat-system-to-disable-antivirus

[Source](https://www.bleepingcomputer.com/news/security/hackers-abuse-genshin-impact-anti-cheat-system-to-disable-antivirus/){:target="_blank" rel="noopener"}

> Hackers are abusing an anti-cheat system driver for the immensely popular Genshin Impact game to disable antivirus software while conducting ransomware attacks. [...]
