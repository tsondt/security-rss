Title: Cisco fixes critical remote code execution bug in VPN routers
Date: 2022-08-03T13:26:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-03-cisco-fixes-critical-remote-code-execution-bug-in-vpn-routers

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-critical-remote-code-execution-bug-in-vpn-routers/){:target="_blank" rel="noopener"}

> Cisco has fixed critical security vulnerabilities affecting Small Business VPN routers and enabling unauthenticated, remote attackers to execute arbitrary code or commands and trigger denial of service (DoS) conditions on vulnerable devices. [...]
