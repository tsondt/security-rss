Title: Apple security updates fix 2 zero-days used to hack iPhones, Macs
Date: 2022-08-17T18:35:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Apple
Slug: 2022-08-17-apple-security-updates-fix-2-zero-days-used-to-hack-iphones-macs

[Source](https://www.bleepingcomputer.com/news/security/apple-security-updates-fix-2-zero-days-used-to-hack-iphones-macs/){:target="_blank" rel="noopener"}

> Apple has released emergency security updates today to fix two zero-day vulnerabilities previously exploited by attackers to hack iPhones, iPads, or Macs. [...]
