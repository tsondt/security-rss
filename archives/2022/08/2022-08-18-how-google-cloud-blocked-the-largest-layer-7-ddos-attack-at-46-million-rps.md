Title: How Google Cloud blocked the largest Layer 7 DDoS attack at 46 million rps
Date: 2022-08-18T16:00:00+00:00
Author: Satya Konduru
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-08-18-how-google-cloud-blocked-the-largest-layer-7-ddos-attack-at-46-million-rps

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-blocked-largest-layer-7-ddos-attack-at-46-million-rps/){:target="_blank" rel="noopener"}

> Over the past few years, Google has observed that distributed denial-of-service (DDoS) attacks are increasing in frequency and growing in size exponentially. Today’s internet-facing workloads are at constant risk of attack with impacts ranging from degraded performance and user experience for legitimate users, to increased operating and hosting costs, to full unavailability of mission critical workloads. Google Cloud customers are able to use Cloud Armor to leverage the global scale and capacity of Google’s network edge to protect their environment from some of the largest DDoS attacks ever seen. On June 1, a Google Cloud Armor customer was targeted with [...]
