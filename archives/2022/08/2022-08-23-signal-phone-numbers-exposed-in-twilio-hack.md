Title: Signal Phone Numbers Exposed in Twilio Hack
Date: 2022-08-23T11:30:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;data breaches;hacking;Signal
Slug: 2022-08-23-signal-phone-numbers-exposed-in-twilio-hack

[Source](https://www.schneier.com/blog/archives/2022/08/signal-phone-numbers-exposed-in-twilio-hack.html){:target="_blank" rel="noopener"}

> Twilio was hacked earlier this month, and the phone numbers of 1,900 Signal users were exposed : Here’s what our users need to know: All users can rest assured that their message history, contact lists, profile information, whom they’d blocked, and other personal data remain private and secure and were not affected. For about 1,900 users, an attacker could have attempted to re-register their number to another device or learned that their number was registered to Signal. This attack has since been shut down by Twilio. 1,900 users is a very small percentage of Signal’s total users, meaning that most [...]
