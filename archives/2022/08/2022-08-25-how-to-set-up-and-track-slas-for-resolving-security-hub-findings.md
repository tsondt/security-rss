Title: How to set up and track SLAs for resolving Security Hub findings
Date: 2022-08-25T15:55:45+00:00
Author: Maisie Fernandes
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Athena;Quicksight;Security;Security Blog;Security Hub;SLA
Slug: 2022-08-25-how-to-set-up-and-track-slas-for-resolving-security-hub-findings

[Source](https://aws.amazon.com/blogs/security/how-to-set-up-and-track-slas-for-resolving-security-hub-findings/){:target="_blank" rel="noopener"}

> Your organization can use AWS Security Hub to gain a comprehensive view of your security and compliance posture across your Amazon Web Services (AWS) environment. Security Hub receives security findings from AWS security services and supported third-party products and centralizes them, providing a single view for identifying and analyzing security issues. Security Hub correlates findings [...]
