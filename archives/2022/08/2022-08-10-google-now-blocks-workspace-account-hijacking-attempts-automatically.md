Title: Google now blocks Workspace account hijacking attempts automatically
Date: 2022-08-10T12:18:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-08-10-google-now-blocks-workspace-account-hijacking-attempts-automatically

[Source](https://www.bleepingcomputer.com/news/security/google-now-blocks-workspace-account-hijacking-attempts-automatically/){:target="_blank" rel="noopener"}

> Google Workspace (formerly G Suite) now comes with stronger protections for risky account actions, automatically blocking hijacking attempts with identity verification prompts and logging them for further investigation. [...]
