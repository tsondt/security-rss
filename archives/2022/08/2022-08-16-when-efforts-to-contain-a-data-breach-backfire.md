Title: When Efforts to Contain a Data Breach Backfire
Date: 2022-08-16T17:06:00+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Web Fraud 2.0;Banorte breach;CloudSecurityAlliance;CTI League;Group-IB;Kurt Seifried;Ohad Zaidenberg
Slug: 2022-08-16-when-efforts-to-contain-a-data-breach-backfire

[Source](https://krebsonsecurity.com/2022/08/when-efforts-to-contain-a-data-breach-backfire/){:target="_blank" rel="noopener"}

> Earlier this month, the administrator of the cybercrime forum Breached received a cease-and-desist letter from a cybersecurity firm. The missive alleged that an auction on the site for data stolen from 10 million customers of Mexico’s second-largest bank was fake news and harming the bank’s reputation. The administrator responded to this empty threat by purchasing the stolen banking data and leaking it on the forum for everyone to download. On August 3, 2022, someone using the alias “ Holistic-K1ller ” posted on Breached a thread selling data allegedly stolen from Grupo Financiero Banorte, Mexico’s second-biggest financial institution by total loans. [...]
