Title: Upcoming Speaking Engagements
Date: 2022-08-14T17:04:13+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2022-08-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2022/08/upcoming-speaking-engagements-22.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking as part of a Geneva Centre for Security Policy course on Cyber Security in the Context of International Security, online, on September 22, 2022. I’m speaking at IT-Security INSIDE 2022 in Zurich, Switzerland, on September 22, 2022. The list is maintained on this page. [...]
