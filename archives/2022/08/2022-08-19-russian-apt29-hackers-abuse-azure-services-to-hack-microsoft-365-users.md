Title: Russian APT29 hackers abuse Azure services to hack Microsoft 365 users
Date: 2022-08-19T11:10:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-08-19-russian-apt29-hackers-abuse-azure-services-to-hack-microsoft-365-users

[Source](https://www.bleepingcomputer.com/news/security/russian-apt29-hackers-abuse-azure-services-to-hack-microsoft-365-users/){:target="_blank" rel="noopener"}

> The state-backed Russian cyberespionage group Cozy Bear has been particularly prolific in 2022, targeting Microsoft 365 accounts in NATO countries and attempting to access foreign policy information. [...]
