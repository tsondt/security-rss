Title: Indian stock markets given ten day deadline to file infosec report, secure board signoff
Date: 2022-05-25T06:53:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-25-indian-stock-markets-given-ten-day-deadline-to-file-infosec-report-secure-board-signoff

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/25/sebi_modified_mii_infosec_rules/){:target="_blank" rel="noopener"}

> Another rush job for busy Indian IT shops Indian IT shops have been handed another extraordinarily short deadline within which to perform significant infosec work.... [...]
