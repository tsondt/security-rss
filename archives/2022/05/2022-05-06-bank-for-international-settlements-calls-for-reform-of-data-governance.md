Title: Bank for International Settlements calls for reform of data governance
Date: 2022-05-06T07:00:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-06-bank-for-international-settlements-calls-for-reform-of-data-governance

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/06/bis_data_governance_design/){:target="_blank" rel="noopener"}

> Wants Big Tech to butt out, and return control to individuals The Bank for International Settlements (BIS) – a meta bank for the world's central banks and facilitator of cross-border payments – has advocated new governance systems that promote owner control of data and transparency over its use.... [...]
