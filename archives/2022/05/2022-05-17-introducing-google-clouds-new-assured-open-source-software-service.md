Title: Introducing Google Cloud’s new Assured Open Source Software service
Date: 2022-05-17T16:00:00+00:00
Author: Andy Chang
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-05-17-introducing-google-clouds-new-assured-open-source-software-service

[Source](https://cloud.google.com/blog/products/identity-security/introducing-assured-open-source-software-service/){:target="_blank" rel="noopener"}

> There has been an increasing awareness in the developer community, enterprises, and governments of software supply chain risks. Remediation efforts for vulnerabilities like Log4j and Spring4shell, and a 650% year-over-year increase in cyberattacks aimed at open source suppliers, have sharpened focus on the critical task of bolstering the security of open source software. Governments and regulators have taken notice and action, including the White House’s Executive Order 14028 on Improving the Nation’s Cybersecurity, followed by other governments and agencies around the world asserting new requirements and standards specifically focused on the software development lifecycle and the software supply chain. Google [...]
