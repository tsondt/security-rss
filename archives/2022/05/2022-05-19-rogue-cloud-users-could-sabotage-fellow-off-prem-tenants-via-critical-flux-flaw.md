Title: Rogue cloud users could sabotage fellow off-prem tenants via critical Flux flaw
Date: 2022-05-19T12:54:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-19-rogue-cloud-users-could-sabotage-fellow-off-prem-tenants-via-critical-flux-flaw

[Source](https://portswigger.net/daily-swig/rogue-cloud-users-could-sabotage-fellow-off-prem-tenants-via-critical-flux-flaw){:target="_blank" rel="noopener"}

> Mischief-makers could ‘disrupt the availability, integrity and confidentiality’ of other tenants [...]
