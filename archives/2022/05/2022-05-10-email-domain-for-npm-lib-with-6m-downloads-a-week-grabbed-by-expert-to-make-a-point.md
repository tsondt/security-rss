Title: Email domain for NPM lib with 6m downloads a week grabbed by expert to make a point
Date: 2022-05-10T22:36:21+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-05-10-email-domain-for-npm-lib-with-6m-downloads-a-week-grabbed-by-expert-to-make-a-point

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/10/security_npm_email/){:target="_blank" rel="noopener"}

> Campaign to coax GitHub-owned outfit to improve security starts showing results Special report Security consultant Lance Vick recently acquired the expired domain used by the maintainer of a widely used NPM package to remind the JavaScript community that the NPM Registry still hasn't implemented adequate security.... [...]
