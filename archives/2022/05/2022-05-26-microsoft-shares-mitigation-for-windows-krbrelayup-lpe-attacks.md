Title: Microsoft shares mitigation for Windows KrbRelayUp LPE attacks
Date: 2022-05-26T11:46:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-05-26-microsoft-shares-mitigation-for-windows-krbrelayup-lpe-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-mitigation-for-windows-krbrelayup-lpe-attacks/){:target="_blank" rel="noopener"}

> Microsoft has shared guidance to help admins defend their Windows enterprise environments against KrbRelayUp attacks that enable attackers to gain SYSTEM privileges on Windows systems with default configurations. [...]
