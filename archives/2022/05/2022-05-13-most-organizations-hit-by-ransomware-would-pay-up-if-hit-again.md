Title: Most organizations hit by ransomware would pay up if hit again
Date: 2022-05-13T14:11:10+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-05-13-most-organizations-hit-by-ransomware-would-pay-up-if-hit-again

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/13/organizations_pay_ransomware/){:target="_blank" rel="noopener"}

> Nine out of ten organizations would do it all over again, keeping attackers in business Almost nine in 10 organizations that have suffered a ransomware attack would choose to pay the ransom if hit again, according to a new report, compared with two-thirds of those that have not experienced an attack.... [...]
