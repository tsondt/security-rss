Title: Black Hat Asia:&nbsp;‘If democracy is to survive, technology will have to be tamed’
Date: 2022-05-13T16:09:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-13-black-hat-asia-if-democracy-is-to-survive-technology-will-have-to-be-tamed

[Source](https://portswigger.net/daily-swig/black-hat-asia-nbsp-if-democracy-is-to-survive-technology-will-have-to-be-tamed){:target="_blank" rel="noopener"}

> Indian tech policy expert Samir Saran says it’s not too late to ‘course-correct’ after a ‘challenging decade’ for liberal democracies [...]
