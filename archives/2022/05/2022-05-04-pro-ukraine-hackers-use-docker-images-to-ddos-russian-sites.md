Title: Pro-Ukraine hackers use Docker images to DDoS Russian sites
Date: 2022-05-04T06:14:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-04-pro-ukraine-hackers-use-docker-images-to-ddos-russian-sites

[Source](https://www.bleepingcomputer.com/news/security/pro-ukraine-hackers-use-docker-images-to-ddos-russian-sites/){:target="_blank" rel="noopener"}

> Docker images with a download count of over 150,000 have been used to run distributed denial-of-service (DDoS) attacks against a dozen Russian and Belarusian websites managed by government, military, and news organizations. [...]
