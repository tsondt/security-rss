Title: The Week in Ransomware - May 6th 2022 - An evolving landscape
Date: 2022-05-06T18:27:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-06-the-week-in-ransomware-may-6th-2022-an-evolving-landscape

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-6th-2022-an-evolving-landscape/){:target="_blank" rel="noopener"}

> Ransomware operations continue to evolve, with new groups appearing and others quietly shutting down their operations or rebranding as new groups. [...]
