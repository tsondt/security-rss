Title: Tails 5.0 Linux users warned against using it "for sensitive information"
Date: 2022-05-25T12:41:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-25-tails-50-linux-users-warned-against-using-it-for-sensitive-information

[Source](https://www.bleepingcomputer.com/news/security/tails-50-linux-users-warned-against-using-it-for-sensitive-information/){:target="_blank" rel="noopener"}

> Tails developers have warned users to stop using the portable Debian-based Linux distro until the next release if they're entering or accessing sensitive information using the bundled Tor Browser application. [...]
