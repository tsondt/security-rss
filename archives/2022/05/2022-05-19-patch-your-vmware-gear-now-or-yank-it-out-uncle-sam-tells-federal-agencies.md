Title: Patch your VMware gear now – or yank it out, Uncle Sam tells federal agencies
Date: 2022-05-19T00:41:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-19-patch-your-vmware-gear-now-or-yank-it-out-uncle-sam-tells-federal-agencies

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/19/vmware_cisa_security_risks/){:target="_blank" rel="noopener"}

> Critical authentication bypass revealed, older flaws under active attack Uncle Sam's Cybersecurity and Infrastructure Security Agency (CISA) has issued two warnings in a single day to VMware users, as it believes the virtualization giant's products can be exploited by miscreants to gain control of systems.... [...]
