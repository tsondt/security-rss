Title: Backdoor baked into premium school management plugin for WordPress
Date: 2022-05-20T14:02:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-20-backdoor-baked-into-premium-school-management-plugin-for-wordpress

[Source](https://www.bleepingcomputer.com/news/security/backdoor-baked-into-premium-school-management-plugin-for-wordpress/){:target="_blank" rel="noopener"}

> Security researchers have discovered a backdoor in a premium WordPress plugin built as a complete management solution for schools. The malicious code enables a threat actor to execute PHP code without authenticating. [...]
