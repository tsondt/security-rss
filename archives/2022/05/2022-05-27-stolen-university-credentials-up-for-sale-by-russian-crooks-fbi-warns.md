Title: Stolen university credentials up for sale by Russian crooks, FBI warns
Date: 2022-05-27T22:34:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-27-stolen-university-credentials-up-for-sale-by-russian-crooks-fbi-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/27/fbi_warning_stolen_university_credentials/){:target="_blank" rel="noopener"}

> Forget dark-web souks, thousands of these are already being traded on public bazaars Russian crooks are selling network credentials and virtual private network access for a "multitude" of US universities and colleges on criminal marketplaces, according to the FBI.... [...]
