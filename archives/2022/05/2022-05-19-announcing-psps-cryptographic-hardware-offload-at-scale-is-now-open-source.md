Title: Announcing PSP's cryptographic hardware offload at scale is now open source
Date: 2022-05-19T16:00:00+00:00
Author: Soheil Hassas Yeganeh
Category: GCP Security
Tags: Systems;Infrastructure;Networking;Inside Google Cloud;Google Cloud;Identity & Security
Slug: 2022-05-19-announcing-psps-cryptographic-hardware-offload-at-scale-is-now-open-source

[Source](https://cloud.google.com/blog/products/identity-security/announcing-psp-security-protocol-is-now-open-source/){:target="_blank" rel="noopener"}

> Almost a decade ago, we started encrypting traffic between our data centers to help protect user privacy. Since then, we gradually rolled out changes to encrypt almost all data in transit. Our approach is described in our Encryption in Transit whitepaper. While this effort provided invaluable privacy and security benefits, software encryption came at significant cost: it took ~0.7% of Google's processing power to encrypt and decrypt RPCs, along with a corresponding amount of memory. Such costs spurred us to offload encryption to our network interface cards (NICs) using PSP (a recursive acronym for PSP Security Protocol), which we are [...]
