Title: Box, Zoom, Google Docs offer phishing boost with ‘vanity URL’ flaws
Date: 2022-05-12T12:42:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-12-box-zoom-google-docs-offer-phishing-boost-with-vanity-url-flaws

[Source](https://portswigger.net/daily-swig/box-zoom-google-docs-offer-phishing-boost-with-vanity-url-flaws){:target="_blank" rel="noopener"}

> Attack technique bypasses email filters and burnishes credibility of phishing links [...]
