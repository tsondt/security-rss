Title: CISA warns not to install May Windows updates on domain controllers
Date: 2022-05-16T13:24:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-05-16-cisa-warns-not-to-install-may-windows-updates-on-domain-controllers

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-not-to-install-may-windows-updates-on-domain-controllers/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) has removed a Windows security flaw from its catalog of known exploited vulnerabilities due to Active Directory (AD) authentication issues caused by the May 2022 updates that patch it. [...]
