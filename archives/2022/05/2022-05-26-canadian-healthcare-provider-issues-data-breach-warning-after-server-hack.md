Title: Canadian healthcare provider issues data breach warning after server hack
Date: 2022-05-26T14:11:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-26-canadian-healthcare-provider-issues-data-breach-warning-after-server-hack

[Source](https://portswigger.net/daily-swig/canadian-healthcare-provider-issues-data-breach-warning-after-server-hack){:target="_blank" rel="noopener"}

> SHN plays down concerns over medical records breach [...]
