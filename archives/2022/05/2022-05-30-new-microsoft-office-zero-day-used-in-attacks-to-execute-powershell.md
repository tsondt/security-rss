Title: New Microsoft Office zero-day used in attacks to execute PowerShell
Date: 2022-05-30T10:23:43-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-05-30-new-microsoft-office-zero-day-used-in-attacks-to-execute-powershell

[Source](https://www.bleepingcomputer.com/news/security/new-microsoft-office-zero-day-used-in-attacks-to-execute-powershell/){:target="_blank" rel="noopener"}

> Security researchers have discovered a new Microsoft Office zero-day vulnerability that is being used in attacks to execute malicious PowerShell commands via Microsoft Diagnostic Tool (MSDT) simply by opening a Word document. [...]
