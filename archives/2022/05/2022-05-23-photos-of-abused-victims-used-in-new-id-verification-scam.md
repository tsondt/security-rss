Title: Photos of abused victims used in new ID verification scam
Date: 2022-05-23T14:30:55-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-05-23-photos-of-abused-victims-used-in-new-id-verification-scam

[Source](https://www.bleepingcomputer.com/news/security/photos-of-abused-victims-used-in-new-id-verification-scam/){:target="_blank" rel="noopener"}

> Scammers are now leveraging dating apps like Tinder and Grindr to pose themselves as former victims of physical abuse to gain your trust and sympathy and sell you "ID verification" services. BleepingComputer came across multiple instances of users on online dating apps being approached by these catfishing profiles. [...]
