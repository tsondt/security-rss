Title: False-flag cyberattacks a red line for nation-states, says Mandiant boss
Date: 2022-05-07T08:53:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-07-false-flag-cyberattacks-a-red-line-for-nation-states-says-mandiant-boss

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/07/false_flag_attacks/){:target="_blank" rel="noopener"}

> NSA director says he doesn't know of a 'big one' that was successful False-flag cyberattacks represent a red line that even nation states like Russia and China don't want to cross, according to Mandiant CEO Kevin Mandia.... [...]
