Title: Cloud security unicorn cuts 20% of staff after raising $1.3b
Date: 2022-05-27T19:19:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-27-cloud-security-unicorn-cuts-20-of-staff-after-raising-13b

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/27/lacework_cuts_jobs/){:target="_blank" rel="noopener"}

> Time to play blame bingo: Markets? Profits? Too much growth? Russia? Space aliens? Cloud security company Lacework has laid off 20 percent of its employees, just months after two record-breaking funding rounds pushed its valuation to $8.3 billion.... [...]
