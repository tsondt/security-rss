Title: Canada bans Huawei and ZTE from 5G networks over security concerns
Date: 2022-05-20T06:35:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-05-20-canada-bans-huawei-and-zte-from-5g-networks-over-security-concerns

[Source](https://www.bleepingcomputer.com/news/security/canada-bans-huawei-and-zte-from-5g-networks-over-security-concerns/){:target="_blank" rel="noopener"}

> The Government of Canada announced its intention to ban the use of Huawei and ZTE telecommunications equipment and services across the country's 5G and 4G networks. [...]
