Title: Another ex-eBay exec admits cyberstalking web souk critics
Date: 2022-05-13T20:04:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-05-13-another-ex-ebay-exec-admits-cyberstalking-web-souk-critics

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/13/former_ebay_exec_pleads_guilty/){:target="_blank" rel="noopener"}

> David Harville is seventh to cop to harassment campaign David Harville, eBay's former director of global resiliency, pleaded guilty this week to five felony counts of participating in a plan to harass and intimidate journalists who were critical of the online auction business.... [...]
