Title: Check your gems: RubyGems fixes unauthorized package takeover bug
Date: 2022-05-08T16:59:59-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-05-08-check-your-gems-rubygems-fixes-unauthorized-package-takeover-bug

[Source](https://www.bleepingcomputer.com/news/security/check-your-gems-rubygems-fixes-unauthorized-package-takeover-bug/){:target="_blank" rel="noopener"}

> The RubyGems package repository has fixed a critical vulnerability that would allow anyone to unpublish ("yank") certain Ruby packages from the repository and republish their tainted or malicious versions with the same file names and version numbers. [...]
