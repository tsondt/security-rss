Title: Corporate Involvement in International Cybersecurity Treaties
Date: 2022-05-06T11:01:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;national security policy
Slug: 2022-05-06-corporate-involvement-in-international-cybersecurity-treaties

[Source](https://www.schneier.com/blog/archives/2022/05/corporate-involvement-in-international-cybersecurity-treaties.html){:target="_blank" rel="noopener"}

> The Paris Call for Trust and Stability in Cyberspace is an initiative launched by French President Emmanuel Macron during the 2018 UNESCO’s Internet Governance Forum. It’s an attempt by the world’s governments to come together and create a set of international norms and standards for a reliable, trustworthy, safe, and secure Internet. It’s not an international treaty, but it does impose obligations on the signatories. It’s a major milestone for global Internet security and safety. Corporate interests are all over this initiative, sponsoring and managing different parts of the process. As part of the Call, the French company Cigref and [...]
