Title: Biden orders new quantum push to ensure encryption isn't cracked by rivals
Date: 2022-05-05T06:57:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-05-biden-orders-new-quantum-push-to-ensure-encryption-isnt-cracked-by-rivals

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/05/us_quantum_initiatives/){:target="_blank" rel="noopener"}

> Domestic action and international collaboration to make sure you-know-who – OK, China – doesn't get ahead of the game US president Joe Biden issued two directives on Wednesday aimed at ensuring the nation – and like-minded friends – remain ahead of other countries in the field of quantum computing. Especially as applied to cryptography.... [...]
