Title: WordPress theme Jupiter patches critical privilege escalation flaw
Date: 2022-05-20T14:56:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-20-wordpress-theme-jupiter-patches-critical-privilege-escalation-flaw

[Source](https://portswigger.net/daily-swig/wordpress-theme-jupiter-patches-critical-privilege-escalation-flaw){:target="_blank" rel="noopener"}

> Users urged to update systems amid reports of active exploitation [...]
