Title: Microsoft, Apple, Google accelerate push to eliminate passwords
Date: 2022-05-05T19:06:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-05-microsoft-apple-google-accelerate-push-to-eliminate-passwords

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/05/microsoft-apple-google-fido/){:target="_blank" rel="noopener"}

> Passphrases PIP'd, FIDO and W3C projects promoted Analysis Microsoft, Apple and Google – all longtime proponents of doing away with passwords for authentication purposes – are throwing their support behind standards developed by the FIDO Alliance and the World Wide Web Consortium (W3C) that could eliminate passphrases completely.... [...]
