Title: Yahoo Japan strives for universal passwordless authentication
Date: 2022-05-11T08:19:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-11-yahoo-japan-strives-for-universal-passwordless-authentication

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/11/yahoo_japan_goes_passwordless/){:target="_blank" rel="noopener"}

> 30! million! users! already! moved! to! TXT! and/or! FIDO! Attacks! and! support! requests! both! down! Yahoo Japan has revealed that it plans to go passwordless, and that 30 million of its 50 million monthly active users have already stopped using passwords in favor of a combination of FIDO and TXT messages.... [...]
