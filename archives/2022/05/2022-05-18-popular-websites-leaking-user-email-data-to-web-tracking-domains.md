Title: Popular websites leaking user email data to web tracking domains
Date: 2022-05-18T15:21:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-18-popular-websites-leaking-user-email-data-to-web-tracking-domains

[Source](https://portswigger.net/daily-swig/popular-websites-leaking-user-email-data-to-web-tracking-domains){:target="_blank" rel="noopener"}

> Data harvested without consent and before forms are submitted in many cases, researchers claim [...]
