Title: <span>TLStorm 2.0: Millions of Aruba and Avaya network switches affected by RCE flaws</span>
Date: 2022-05-04T11:23:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-04-tlstorm-20-millions-of-aruba-and-avaya-network-switches-affected-by-rce-flaws

[Source](https://portswigger.net/daily-swig/tlstorm-2-0-millions-of-aruba-and-avaya-network-switches-affected-by-rce-flaws){:target="_blank" rel="noopener"}

> Patches issued for vulnerabilities arising from misuse of NanoSSL TLS library [...]
