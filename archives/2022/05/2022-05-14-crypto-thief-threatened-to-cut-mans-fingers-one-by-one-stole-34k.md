Title: Crypto thief threatened to cut man's fingers 'one by one,' stole £34K
Date: 2022-05-14T10:02:27-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency;Legal
Slug: 2022-05-14-crypto-thief-threatened-to-cut-mans-fingers-one-by-one-stole-34k

[Source](https://www.bleepingcomputer.com/news/security/crypto-thief-threatened-to-cut-mans-fingers-one-by-one-stole-34k/){:target="_blank" rel="noopener"}

> Online crypto scams and ponzi schemes leveraging social media platforms are hardly anything new. But, this gruesome case of a London-based crypto robber transcends the virtual realm and tells a shocking tale of real-life victims from whom the perpetrator successfully stole £34,000. [...]
