Title: Data breach at US energy supplier Riviera Utilities exposes customer information
Date: 2022-05-03T12:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-03-data-breach-at-us-energy-supplier-riviera-utilities-exposes-customer-information

[Source](https://portswigger.net/daily-swig/data-breach-at-us-energy-supplier-riviera-utilities-exposes-customer-information){:target="_blank" rel="noopener"}

> Unknown actor accessed employee emails [...]
