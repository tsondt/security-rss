Title: Elon Musk deep fakes promote new cryptocurrency scam
Date: 2022-05-22T14:22:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-22-elon-musk-deep-fakes-promote-new-cryptocurrency-scam

[Source](https://www.bleepingcomputer.com/news/security/elon-musk-deep-fakes-promote-new-cryptocurrency-scam/){:target="_blank" rel="noopener"}

> Cryptocurrency scammers are using deep fake videos of Elon Musk and other prominent cryptocurrency advocates to promote a BitVex trading platform scam that steals deposited currency. [...]
