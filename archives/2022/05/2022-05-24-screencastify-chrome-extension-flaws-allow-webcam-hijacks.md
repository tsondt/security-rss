Title: Screencastify Chrome extension flaws allow webcam hijacks
Date: 2022-05-24T12:45:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-24-screencastify-chrome-extension-flaws-allow-webcam-hijacks

[Source](https://www.bleepingcomputer.com/news/security/screencastify-chrome-extension-flaws-allow-webcam-hijacks/){:target="_blank" rel="noopener"}

> The popular Screencastify Chrome extension has fixed a vulnerability that allowed malicious sites to hijack users' webcams and steal recorded videos. However, security flaws still exist that could be exploited by unscrupulous insiders. [...]
