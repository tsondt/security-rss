Title: GM credential stuffing attack exposed car owners' personal info
Date: 2022-05-23T18:53:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-23-gm-credential-stuffing-attack-exposed-car-owners-personal-info

[Source](https://www.bleepingcomputer.com/news/security/gm-credential-stuffing-attack-exposed-car-owners-personal-info/){:target="_blank" rel="noopener"}

> US car manufacturer GM disclosed that it was the victim of a credential stuffing attack last month that exposed customer information and allowed hackers to redeem rewards points for gift cards. [...]
