Title: EU targets standardization as key to bloc-wide cyber-resilience
Date: 2022-05-10T10:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-10-eu-targets-standardization-as-key-to-bloc-wide-cyber-resilience

[Source](https://portswigger.net/daily-swig/eu-targets-standardization-as-key-to-bloc-wide-cyber-resilience){:target="_blank" rel="noopener"}

> Threat landscape’s increasing complexity adds impetus to drive for consistency across 27 member states [...]
