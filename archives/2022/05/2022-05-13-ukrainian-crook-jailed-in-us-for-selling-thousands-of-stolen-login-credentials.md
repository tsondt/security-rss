Title: Ukrainian crook jailed in US for selling thousands of stolen login credentials
Date: 2022-05-13T22:16:23+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-13-ukrainian-crook-jailed-in-us-for-selling-thousands-of-stolen-login-credentials

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/13/ukrainian_credentials_botnet/){:target="_blank" rel="noopener"}

> Touting info on 6,700 compromised systems will get you four years behind bars A Ukrainian man has been sentenced to four years in a US federal prison for selling on a dark-web marketplace stolen login credentials for more than 6,700 compromised servers.... [...]
