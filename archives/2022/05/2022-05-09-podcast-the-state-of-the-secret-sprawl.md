Title: Podcast: The State of the Secret Sprawl
Date: 2022-05-09T10:43:12+00:00
Author: Jeffrey Esposito
Category: Threatpost
Tags: Podcasts;.git directory;podcast
Slug: 2022-05-09-podcast-the-state-of-the-secret-sprawl

[Source](https://threatpost.com/gitguardian-state-of-the-secret-sprawl/179525/){:target="_blank" rel="noopener"}

> In this podcast with Mackenzie Jackson, developer advocate at GitGuardian, we dive into the report and also the issues that corporations face with public leaks from groups like Lapsus and more, as well as ways that developers can keep their code safe. [...]
