Title: Europe proposes tackling child abuse by killing privacy, strong encryption
Date: 2022-05-12T06:35:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-12-europe-proposes-tackling-child-abuse-by-killing-privacy-strong-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/12/eu_encryption_csam/){:target="_blank" rel="noopener"}

> If we're gonna go through this again, can we just literally go back in time? Proposed Europe regulations that purport to curb child abuse by imposing mass surveillance would be a "disaster" for digital privacy and strong encryption, say cybersecurity experts.... [...]
