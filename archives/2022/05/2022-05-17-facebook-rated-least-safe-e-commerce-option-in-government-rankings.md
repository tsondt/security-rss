Title: Facebook rated least safe e-commerce option in government rankings
Date: 2022-05-17T07:55:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-17-facebook-rated-least-safe-e-commerce-option-in-government-rankings

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/17/facebook_rated_least_safe_ecommerce/){:target="_blank" rel="noopener"}

> Singapore's safety scheme measures scam-combatting capability A newly implemented e-commerce rating system in the city-state of Singapore has rated Facebook's Marketplace as the least trustworthy e-commerce platform, behind Amazon and its Alibaba-owned Asian analogue Lazada.... [...]
