Title: Costa Rica May Be Pawn in Conti Ransomware Group’s Bid to Rebrand, Evade Sanctions
Date: 2022-05-31T19:57:58+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ne'er-Do-Well News;Ransomware;Russia's War on Ukraine;ADVIntel;Conti;Costa Rica;CRprensa.com;Cyberscoop;data ransom;Hive;unc1756
Slug: 2022-05-31-costa-rica-may-be-pawn-in-conti-ransomware-groups-bid-to-rebrand-evade-sanctions

[Source](https://krebsonsecurity.com/2022/05/costa-rica-may-be-pawn-in-conti-ransomware-groups-bid-to-rebrand-evade-sanctions/){:target="_blank" rel="noopener"}

> Costa Rica’s national health service was hacked sometime earlier this morning by a Russian ransomware group known as Hive. The intrusion comes just weeks after Costa Rican President Rodrigo Chaves declared a state of emergency in response to a data ransom attack from a different Russian ransomware gang — Conti. Ransomware experts say there is good reason to believe the same cybercriminals are behind both attacks, and that Hive has been helping Conti rebrand and evade international sanctions targeting extortion payouts to cybercriminals operating in Russia. The Costa Rican publication CRprensa.com reports that affected systems at the Costa Rican Social [...]
