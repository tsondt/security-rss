Title: Built with BigQuery: Material Security’s novel approach to protecting email
Date: 2022-05-20T16:00:00+00:00
Author: Abhishek Agrawal
Category: GCP Security
Tags: Partners;Data Analytics;Google Cloud;Identity & Security
Slug: 2022-05-20-built-with-bigquery-material-securitys-novel-approach-to-protecting-email

[Source](https://cloud.google.com/blog/products/identity-security/why-material-security-is-built-with-bigquery/){:target="_blank" rel="noopener"}

> Editor’s note : The post is part of a series highlighting our awesome partners, and their solutions, that are Built with BigQuery. Since the very first email was sent more than 50 years ago, the now-ubiquitous communication tool has evolved into more than just an electronic method of communication. Businesses have come to rely on it as a storage system for financial reports, legal documents, and personnel records. From daily operations to client and employee communications to the lifeblood of sales and marketing, email is still the gold standard for digital communications. But there’s a dark side to email, too: [...]
