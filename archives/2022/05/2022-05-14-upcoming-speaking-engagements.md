Title: Upcoming Speaking Engagements
Date: 2022-05-14T17:05:06+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2022-05-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2022/05/upcoming-speaking-engagements-19.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking on “Securing a World of Physically Capable Computers” at OWASP Belgium’s chapter meeting in Antwerp, Belgium, on May 17, 2022. I’m speaking at Future Summits in Antwerp, Belgium, on May 18, 2022. I’m speaking at IT-S Now 2022 in Vienna, Austria, on June 2, 2022. I’m speaking at the 14th International Conference on Cyber Conflict, CyCon 2022, in Tallinn, Estonia, on June 3, 2022. I’m speaking at the RSA Conference 2022 in San Francisco, June 6-9, 2022. I’m speaking at the Dublin Tech Summit [...]
