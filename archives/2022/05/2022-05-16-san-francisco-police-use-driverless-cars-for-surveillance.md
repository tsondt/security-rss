Title: San Francisco police use driverless cars for surveillance
Date: 2022-05-16T10:36:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-16-san-francisco-police-use-driverless-cars-for-surveillance

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/16/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Tech giants commit $30m to open-source security, miscreants breach DEA portal, and US signs cybercrime treaty In brief San Francisco police have been using driverless cars for surveillance to assist in law enforcement investigations.... [...]
