Title: Microsoft Bing censors politically sensitive Chinese terms
Date: 2022-05-20T10:37:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-20-microsoft-bing-censors-politically-sensitive-chinese-terms

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/20/citizen_lab_microsoft_bing_report/){:target="_blank" rel="noopener"}

> Research claims search engine fails to autofill certain names in Han characters, Microsoft points to technical error Microsoft search engine Bing censors terms deemed sensitive in China from its autosuggestion feature internationally, according to research from Citizen Lab.... [...]
