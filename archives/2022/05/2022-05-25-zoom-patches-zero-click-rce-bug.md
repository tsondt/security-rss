Title: Zoom Patches ‘Zero-Click’ RCE Bug
Date: 2022-05-25T13:02:37+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-05-25-zoom-patches-zero-click-rce-bug

[Source](https://threatpost.com/zoom-patches-zero-click-rce-bug/179727/){:target="_blank" rel="noopener"}

> The Google Project Zero researcher found a bug in XML parsing on the Zoom client and server. [...]
