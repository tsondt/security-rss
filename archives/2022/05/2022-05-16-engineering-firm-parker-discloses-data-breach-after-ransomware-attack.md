Title: Engineering firm Parker discloses data breach after ransomware attack
Date: 2022-05-16T10:17:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-16-engineering-firm-parker-discloses-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/engineering-firm-parker-discloses-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> The Parker-Hannifin Corporation announced a data breach exposing employees' personal information after the Conti ransomware gang began publishing allegedly stolen data last month. [...]
