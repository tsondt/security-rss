Title: NIS2: Experts share their views on the EU’s upcoming cybersecurity directive
Date: 2022-05-30T11:05:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-30-nis2-experts-share-their-views-on-the-eus-upcoming-cybersecurity-directive

[Source](https://portswigger.net/daily-swig/nis2-experts-share-their-views-on-the-eus-upcoming-cybersecurity-directive){:target="_blank" rel="noopener"}

> More organizations face incident reporting requirements under revised rules [...]
