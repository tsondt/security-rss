Title: Introducing Autonomic Security Operations for the U.S. public sector
Date: 2022-05-17T17:30:00+00:00
Author: Dan Prieto
Category: GCP Security
Tags: Google Cloud;Public Sector;Identity & Security
Slug: 2022-05-17-introducing-autonomic-security-operations-for-the-us-public-sector

[Source](https://cloud.google.com/blog/products/identity-security/introducing-autonomic-security-operations-for-the-us-public-sector/){:target="_blank" rel="noopener"}

> As sophisticated cyberattack campaigns increasingly target the U.S. public and private sectors during the COVID era, the White House and federal agencies have taken steps to protect critical infrastructure and remote-work infrastructure. These include Executive Order 14028 and the Office of Management and Budget’s Memorandum M-21-31, which recommend adopting Zero Trust policies, and span software supply chain security, cybersecurity threat management, and strengthening cyberattack detection and response. However, implementation can be a challenge for many agencies due to cost, scalability, engineering, and a lack of resources. Meeting the requirements of the EO and OMB guidance may require technology modernization and [...]
