Title: Friday Squid Blogging: Squidmobile
Date: 2022-05-13T21:10:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-05-13-friday-squid-blogging-squidmobile

[Source](https://www.schneier.com/blog/archives/2022/05/friday-squid-blogging-squidmobile.html){:target="_blank" rel="noopener"}

> The Squidmobile. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
