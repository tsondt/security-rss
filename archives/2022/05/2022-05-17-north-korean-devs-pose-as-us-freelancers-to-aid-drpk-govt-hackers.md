Title: North Korean devs pose as US freelancers to aid DRPK govt hackers
Date: 2022-05-17T18:16:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-05-17-north-korean-devs-pose-as-us-freelancers-to-aid-drpk-govt-hackers

[Source](https://www.bleepingcomputer.com/news/security/north-korean-devs-pose-as-us-freelancers-to-aid-drpk-govt-hackers/){:target="_blank" rel="noopener"}

> The U.S. government is warning that the Democratic People's Republic of Korea (DPRK) is dispatching its IT workers to get freelance jobs at companies across the world to obtain privileged access that is sometimes used to facilitate cyber intrusions. [...]
