Title: Zyxel fixes firewall flaws that could lead to hacked networks
Date: 2022-05-12T14:13:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-12-zyxel-fixes-firewall-flaws-that-could-lead-to-hacked-networks

[Source](https://www.bleepingcomputer.com/news/security/zyxel-fixes-firewall-flaws-that-could-lead-to-hacked-networks/){:target="_blank" rel="noopener"}

> Threat analysts who discovered a vulnerability affecting multiple Zyxel products report that the network equipment company fixed it via a silent update pushed out two weeks ago. [...]
