Title: Fake crypto giveaways steal millions using Elon Musk Ark Invest video
Date: 2022-05-07T16:48:48-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-05-07-fake-crypto-giveaways-steal-millions-using-elon-musk-ark-invest-video

[Source](https://www.bleepingcomputer.com/news/security/fake-crypto-giveaways-steal-millions-using-elon-musk-ark-invest-video/){:target="_blank" rel="noopener"}

> Fake cryptocurrency giveaways are stealing millions of dollars simply by replaying old Elon Musk and Jack Dorsey Ark Invest videos on YouTube. [...]
