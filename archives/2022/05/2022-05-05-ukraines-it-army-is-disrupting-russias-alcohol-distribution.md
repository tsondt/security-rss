Title: Ukraine’s IT Army is disrupting Russia's alcohol distribution
Date: 2022-05-05T14:57:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-05-ukraines-it-army-is-disrupting-russias-alcohol-distribution

[Source](https://www.bleepingcomputer.com/news/security/ukraine-s-it-army-is-disrupting-russias-alcohol-distribution/){:target="_blank" rel="noopener"}

> Hacktivists operating on the side of Ukraine have focused their DDoS attacks on a portal that is considered crucial for the distribution of alcoholic beverages in Russia. [...]
