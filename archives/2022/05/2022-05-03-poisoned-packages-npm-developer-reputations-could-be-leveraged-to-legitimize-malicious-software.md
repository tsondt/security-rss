Title: Poisoned packages: NPM developer reputations could be leveraged to legitimize malicious software
Date: 2022-05-03T10:45:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-03-poisoned-packages-npm-developer-reputations-could-be-leveraged-to-legitimize-malicious-software

[Source](https://portswigger.net/daily-swig/poisoned-packages-npm-developer-reputations-could-be-leveraged-to-legitimize-malicious-software){:target="_blank" rel="noopener"}

> Faulty invitation mechanism enabled ‘package planting’ attacks [...]
