Title: China reveals its top five sources of online fraud
Date: 2022-05-17T03:31:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-17-china-reveals-its-top-five-sources-of-online-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/17/china_internet_fraud_sources/){:target="_blank" rel="noopener"}

> 'Brushing' tops the list, as quantity of forbidden content continue to rise China’s Ministry of Public Security has revealed the five most prevalent types of fraud perpetrated online or by phone.... [...]
