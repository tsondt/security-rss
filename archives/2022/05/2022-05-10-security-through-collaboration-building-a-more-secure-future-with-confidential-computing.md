Title: Security through collaboration: Building a more secure future with Confidential Computing
Date: 2022-05-10T19:00:00+00:00
Author: Nelly Porter
Category: GCP Security
Tags: Compute;Infrastructure;Inside Google Cloud;Partners;Google Cloud;Identity & Security
Slug: 2022-05-10-security-through-collaboration-building-a-more-secure-future-with-confidential-computing

[Source](https://cloud.google.com/blog/products/identity-security/google-amd-partner-to-build-a-more-secure-future-with-confidential-computing/){:target="_blank" rel="noopener"}

> At Google Cloud, we believe that the protection of our customers’ sensitive data is paramount, and encryption is a powerful mechanism to help achieve this goal. For years, we have supported encryption in transit when our customers ingest their data to bring it to the cloud. We’ve also long supported encryption at rest, for all customer content stored in Google Cloud. To complete the full data protection lifecycle, we can protect customer data when it’s processed through our Confidential Computing portfolio. Confidential Computing products from Google Cloud protect data in use by performing computation in a hardware isolated environment that [...]
