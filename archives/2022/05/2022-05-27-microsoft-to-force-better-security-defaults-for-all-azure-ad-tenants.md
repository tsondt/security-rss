Title: Microsoft to force better security defaults for all Azure AD tenants
Date: 2022-05-27T11:59:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-05-27-microsoft-to-force-better-security-defaults-for-all-azure-ad-tenants

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-to-force-better-security-defaults-for-all-azure-ad-tenants/){:target="_blank" rel="noopener"}

> Microsoft has announced that it will force enable stricter secure default settings known as 'security defaults' on all existing Azure Active Directory (Azure AD) tenants starting in late June 2022. [...]
