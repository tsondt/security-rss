Title: Campaigners warn of legal challenge against Privacy Shield enhancements
Date: 2022-05-26T14:00:06+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-05-26-campaigners-warn-of-legal-challenge-against-privacy-shield-enhancements

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/26/privacy_shield_schrems_warning/){:target="_blank" rel="noopener"}

> Schrems III on the cards unless negotiators protect better oversight of US data access requests European privacy campaigner Max Schrems is warning that enhancements to the EU-US Privacy Shield data-sharing arrangements might face a legal challenge if negotiators don't take a new approach.... [...]
