Title: It's time to kick China off social media, says tech governance expert
Date: 2022-05-12T06:57:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-12-its-time-to-kick-china-off-social-media-says-tech-governance-expert

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/12/ban_china_from_social_media/){:target="_blank" rel="noopener"}

> 'Mischief abroad' is the Middle Kingdom's goal – without the possibility of using Chinese sites to fight back Black Hat Asia The time has come to remove Chinese voices from global social media, according to Samir Saran, president of Delhi-based think tank Observer Research Foundation (ORF), a commissioner of The Global Commission on the Stability of Cyberspace, and a member of Microsoft's Digital Peace Now Initiative.... [...]
