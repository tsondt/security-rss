Title: Apple Mail Now Blocks Email Trackers
Date: 2022-05-09T14:39:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;e-mail;privacy;tracking
Slug: 2022-05-09-apple-mail-now-blocks-email-trackers

[Source](https://www.schneier.com/blog/archives/2022/05/apple-mail-now-blocks-email-trackers.html){:target="_blank" rel="noopener"}

> Apple Mail now blocks email trackers by default. Most email newsletters you get include an invisible “image,” typically a single white pixel, with a unique file name. The server keeps track of every time this “image” is opened and by which IP address. This quirk of internet history means that marketers can track exactly when you open an email and your IP address, which can be used to roughly work out your location. So, how does Apple Mail stop this? By caching. Apple Mail downloads all images for all emails before you open them. Practically speaking, that means every message [...]
