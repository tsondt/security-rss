Title: Fake Binance NFT Mystery Box bots steal victim's crypto wallets
Date: 2022-05-13T12:24:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-13-fake-binance-nft-mystery-box-bots-steal-victims-crypto-wallets

[Source](https://www.bleepingcomputer.com/news/security/fake-binance-nft-mystery-box-bots-steal-victims-crypto-wallets/){:target="_blank" rel="noopener"}

> A new RedLine malware distribution campaign promotes fake Binance NFT mystery box bots on YouTube to lure people into infecting themselves with the information-stealing malware from GitHub repositories. [...]
