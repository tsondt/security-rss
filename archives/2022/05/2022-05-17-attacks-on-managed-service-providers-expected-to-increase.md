Title: Attacks on Managed Service Providers Expected to Increase
Date: 2022-05-17T11:10:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;advanced persistent threats;cybersecurity;infrastructure;national security policy
Slug: 2022-05-17-attacks-on-managed-service-providers-expected-to-increase

[Source](https://www.schneier.com/blog/archives/2022/05/attacks-on-managed-service-providers-expected-to-increase.html){:target="_blank" rel="noopener"}

> CISA, NSA, FBI, and similar organizations in the other Five Eyes countries are warning that attacks on MSPs — as a vector to their customers — are likely to increase. No details about what this prediction is based on. Makes sense, though. The SolarWinds attack was incredibly successful for the Russian SVR, and a blueprint for future attacks. News articles. [...]
