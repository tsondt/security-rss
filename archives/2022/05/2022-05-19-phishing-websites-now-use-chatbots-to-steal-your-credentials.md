Title: Phishing websites now use chatbots to steal your credentials
Date: 2022-05-19T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-19-phishing-websites-now-use-chatbots-to-steal-your-credentials

[Source](https://www.bleepingcomputer.com/news/security/phishing-websites-now-use-chatbots-to-steal-your-credentials/){:target="_blank" rel="noopener"}

> Threat analysts have observed a new trend in the phishing space which is to incorporate interactive chatbots on sites that guide visitors through the process of losing their sensitive data. [...]
