Title: Active attacks against VMware flaws prompts emergency update directive
Date: 2022-05-19T15:14:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-19-active-attacks-against-vmware-flaws-prompts-emergency-update-directive

[Source](https://portswigger.net/daily-swig/active-attacks-against-vmware-flaws-prompts-emergency-update-directive){:target="_blank" rel="noopener"}

> CISA orders US federal agencies to implement patches ASAP [...]
