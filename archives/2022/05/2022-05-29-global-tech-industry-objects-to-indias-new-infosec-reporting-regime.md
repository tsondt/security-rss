Title: Global tech industry objects to India’s new infosec reporting regime
Date: 2022-05-29T23:58:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-29-global-tech-industry-objects-to-indias-new-infosec-reporting-regime

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/29/global_opposition_india_infosec_plan/){:target="_blank" rel="noopener"}

> Eleven industry associations, representing every tech vendor that matters, warns of economic harm Eleven significant tech-aligned industry associations from around the world have reportedly written to India’s Computer Emergency Response Team (CERT-In) to call for revision of the nation’s new infosec reporting and data retention rules, which they criticise as inconsistent, onerous, unlikely to improve security within India, and possibly harmful to the nations economy.... [...]
