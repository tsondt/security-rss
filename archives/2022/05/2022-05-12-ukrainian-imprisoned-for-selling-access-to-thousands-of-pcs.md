Title: Ukrainian imprisoned for selling access to thousands of PCs
Date: 2022-05-12T17:10:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-12-ukrainian-imprisoned-for-selling-access-to-thousands-of-pcs

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-imprisoned-for-selling-access-to-thousands-of-pcs/){:target="_blank" rel="noopener"}

> Glib Oleksandr Ivanov-Tolpintsev, a 28-year-old from Ukraine, was sentenced today to 4 years in prison for stealing thousands of login credentials per week and selling them on a dark web marketplace. [...]
