Title: Using PowerShell to manage password resets in Windows domains
Date: 2022-05-04T10:01:02-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-05-04-using-powershell-to-manage-password-resets-in-windows-domains

[Source](https://www.bleepingcomputer.com/news/security/using-powershell-to-manage-password-resets-in-windows-domains/){:target="_blank" rel="noopener"}

> With breaches running rampant, it's common to force password resets on your Windows domain. This article shows how admins can use PowerShell to manage password resets and introduce software that makes it even easier. [...]
