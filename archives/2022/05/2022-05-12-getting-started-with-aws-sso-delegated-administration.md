Title: Getting started with AWS SSO delegated administration
Date: 2022-05-12T01:39:32+00:00
Author: Chris Mercer
Category: AWS Security
Tags: AWS Single Sign-On (SSO);Identity;Intermediate (200);Security, Identity, & Compliance;AWS Organizations;AWS SSO;Delegated administration;Security Blog
Slug: 2022-05-12-getting-started-with-aws-sso-delegated-administration

[Source](https://aws.amazon.com/blogs/security/getting-started-with-aws-sso-delegated-administration/){:target="_blank" rel="noopener"}

> Recently, AWS launched the ability to delegate administration of AWS Single Sign-On (AWS SSO) in your AWS Organizations organization to a member account (an account other than the management account). This post will show you a practical approach to using this new feature. For the documentation for this feature, see Delegated administration in the AWS [...]
