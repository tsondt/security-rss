Title: Canada bans Huawei and ZTE from 5G networks, citing national security risks
Date: 2022-05-20T05:30:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-20-canada-bans-huawei-and-zte-from-5g-networks-citing-national-security-risks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/20/canada_bans_huawei_and_zte/){:target="_blank" rel="noopener"}

> Ban on shopping from September, rip and replace order with 2024 deadline The Canadian government has joined many of its allies and banned the use of Huawei and ZTE tech in its 5G networks, as part of a new telecommunications security framework.... [...]
