Title: Attackers hijack UK NHS email accounts to steal Microsoft logins
Date: 2022-05-04T14:17:23-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-05-04-attackers-hijack-uk-nhs-email-accounts-to-steal-microsoft-logins

[Source](https://www.bleepingcomputer.com/news/security/attackers-hijack-uk-nhs-email-accounts-to-steal-microsoft-logins/){:target="_blank" rel="noopener"}

> For about half a year, work email accounts belonging to over 100 employees of the National Health System (NHS) in the U.K. were used in several phishing campaigns, some aiming to steal Microsoft logins. [...]
