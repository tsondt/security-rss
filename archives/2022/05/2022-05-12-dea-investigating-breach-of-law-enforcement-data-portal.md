Title: DEA Investigating Breach of Law Enforcement Data Portal
Date: 2022-05-12T11:00:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ne'er-Do-Well News;The Coming Storm;Department of Justice;Domain Block List;Doxbin;Drug Enforcement Administration;El Paso Intelligence Center;emergency data request;EPIC;esp.usdoj.gov;fbi;ICSI;KT;LAPSUS$;Law Enforcement Inquiry and Alerts;LEIA;National Seizure System;Nicholas Weaver;NSS;spamhaus;U.S. Drug Enforcement Agency
Slug: 2022-05-12-dea-investigating-breach-of-law-enforcement-data-portal

[Source](https://krebsonsecurity.com/2022/05/dea-investigating-breach-of-law-enforcement-data-portal/){:target="_blank" rel="noopener"}

> The U.S. Drug Enforcement Administration (DEA) says it is investigating reports that hackers gained unauthorized access to an agency portal that taps into 16 different federal law enforcement databases. KrebsOnSecurity has learned the alleged compromise is tied to a cybercrime and online harassment community that routinely impersonates police and government officials to harvest personal information on their targets. Unidentified hackers shared this screenshot of alleged access to the Drug Enforcement Administration’s intelligence sharing portal. On May 8, KrebsOnSecurity received a tip that hackers obtained a username and password for an authorized user of esp.usdoj.gov, which is the Law Enforcement Inquiry [...]
