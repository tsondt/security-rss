Title: Zyxel warns of flaws impacting firewalls, APs, and controllers
Date: 2022-05-26T10:06:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-26-zyxel-warns-of-flaws-impacting-firewalls-aps-and-controllers

[Source](https://www.bleepingcomputer.com/news/security/zyxel-warns-of-flaws-impacting-firewalls-aps-and-controllers/){:target="_blank" rel="noopener"}

> Zyxel has published a security advisory to warn admins about multiple vulnerabilities affecting a wide range of firewall, AP, and AP controller products. [...]
