Title: Introducing Open Source Insights data in BigQuery to help secure software supply chains
Date: 2022-05-12T20:45:00+00:00
Author: James Wetter
Category: GCP Security
Tags: Identity & Security
Slug: 2022-05-12-introducing-open-source-insights-data-in-bigquery-to-help-secure-software-supply-chains

[Source](https://cloud.google.com/blog/products/identity-security/announcing-open-source-insights-data-in-bigquery/){:target="_blank" rel="noopener"}

> Today we're announcing a new Google Cloud Dataset from Open Source Insights which will help developers better understand the structure and security of the software they use. This dataset provides access to critical software supply chain information for developers, maintainers and consumers of open-source software. Your users rely not only on the code you write, but also on the code your code depends on, the code that code depends on, and so on. This web of dependencies forms a dependency graph, and while each node in the graph brings useful functionality to your project, they may also introduce security vulnerabilities, [...]
