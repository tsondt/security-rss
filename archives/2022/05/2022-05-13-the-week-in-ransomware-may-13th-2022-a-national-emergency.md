Title: The Week in Ransomware - May 13th 2022 - A National Emergency
Date: 2022-05-13T16:58:23-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-13-the-week-in-ransomware-may-13th-2022-a-national-emergency

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-13th-2022-a-national-emergency/){:target="_blank" rel="noopener"}

> While ransomware attacks have slowed during Russia's invasion of Ukraine and the subsequent sanctions, the malware threat continues to affect organizations worldwide. [...]
