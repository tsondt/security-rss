Title: Hackers are after your data. So why are you making it so easy for them?
Date: 2022-05-16T17:15:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-05-16-hackers-are-after-your-data-so-why-are-you-making-it-so-easy-for-them

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/16/hackers_are_after_your_data/){:target="_blank" rel="noopener"}

> Here’s how to tailor a security suite that suits you Webinar Some cyberattackers are out to cause mayhem, but the pros are really after one thing. Your data, whether that’s through exfiltration or encryption.... [...]
