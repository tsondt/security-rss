Title: Trend Micro antivirus modified Windows registry by mistake — How to fix
Date: 2022-05-07T10:03:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Technology
Slug: 2022-05-07-trend-micro-antivirus-modified-windows-registry-by-mistake-how-to-fix

[Source](https://www.bleepingcomputer.com/news/security/trend-micro-antivirus-modified-windows-registry-by-mistake-how-to-fix/){:target="_blank" rel="noopener"}

> Trend Micro antivirus has fixed a false positive affecting its Apex One endpoint security solution that caused Microsoft Edge updates to be tagged as malware and the Windows registry to be incorrectly modified. [...]
