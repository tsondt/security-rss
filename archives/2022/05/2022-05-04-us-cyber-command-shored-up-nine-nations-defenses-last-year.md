Title: US Cyber Command shored up nine nations' defenses last year
Date: 2022-05-04T23:52:57+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-04-us-cyber-command-shored-up-nine-nations-defenses-last-year

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/04/us_cyber_hunt_forward/){:target="_blank" rel="noopener"}

> 'Hunt forward' operations push US capabilities across borders US Cyber Command chief General Paul Nakasone said has revealed the agency he leads conducted nine "hunt forward" operations last year, sending teams to different counties to help them improve their defensive security posture and hunt for cyberthreats.... [...]
