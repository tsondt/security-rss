Title: White House: Prepare for cryptography-cracking quantum computers
Date: 2022-05-05T16:01:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-05-05-white-house-prepare-for-cryptography-cracking-quantum-computers

[Source](https://www.bleepingcomputer.com/news/security/white-house-prepare-for-cryptography-cracking-quantum-computers/){:target="_blank" rel="noopener"}

> President Joe Biden signed a national security memorandum (NSM) on Thursday asking government agencies to implement a set of measures that would mitigate risks posed by quantum computers to US national cyber security. [...]
