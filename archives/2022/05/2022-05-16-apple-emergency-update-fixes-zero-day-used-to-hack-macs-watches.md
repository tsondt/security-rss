Title: Apple emergency update fixes zero-day used to hack Macs, Watches
Date: 2022-05-16T14:33:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2022-05-16-apple-emergency-update-fixes-zero-day-used-to-hack-macs-watches

[Source](https://www.bleepingcomputer.com/news/security/apple-emergency-update-fixes-zero-day-used-to-hack-macs-watches/){:target="_blank" rel="noopener"}

> Apple has released security updates to address a zero-day vulnerability that threat actors can exploit in attacks targeting Macs and Apple Watch devices. [...]
