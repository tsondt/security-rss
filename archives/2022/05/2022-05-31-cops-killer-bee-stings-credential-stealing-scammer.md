Title: Cops' Killer Bee stings credential-stealing scammer
Date: 2022-05-31T20:50:22+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-31-cops-killer-bee-stings-credential-stealing-scammer

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/31/killer_bee_interpol/){:target="_blank" rel="noopener"}

> Fraudster and two alleged accomplices nabbed in joint op An Interpol-led operation code-named Killer Bee has led to the arrest and conviction of a Nigerian man who was said to have used a remote access trojan (RAT) to reroute financial transactions and steal corporate credentials. Two suspected accomplices were also nabbed.... [...]
