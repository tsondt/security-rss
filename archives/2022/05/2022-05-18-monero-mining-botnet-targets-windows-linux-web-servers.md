Title: Monero-mining botnet targets Windows, Linux web servers
Date: 2022-05-18T07:27:05+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-18-monero-mining-botnet-targets-windows-linux-web-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/18/microsoft-cryptomining-sysrv-k/){:target="_blank" rel="noopener"}

> Sysrv-K malware infects unpatched tin, Microsoft warns The latest variant of the Sysrv botnet malware is menacing Windows and Linux systems with an expanded list of vulnerabilities to exploit, according to Microsoft.... [...]
