Title: Establishing a data perimeter on AWS
Date: 2022-05-10T21:14:07+00:00
Author: Ilya Epshteyn
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Data protection;Identity;Network security;Security Blog;service control policies
Slug: 2022-05-10-establishing-a-data-perimeter-on-aws

[Source](https://aws.amazon.com/blogs/security/establishing-a-data-perimeter-on-aws/){:target="_blank" rel="noopener"}

> For your sensitive data on AWS, you should implement security controls, including identity and access management, infrastructure security, and data protection. Amazon Web Services (AWS) recommends that you set up multiple accounts as your workloads grow to isolate applications and data that have specific security requirements. AWS tools can help you establish a data perimeter [...]
