Title: How to use new Amazon GuardDuty EKS Protection findings
Date: 2022-05-06T16:36:31+00:00
Author: Marshall Jones
Category: AWS Security
Tags: Amazon Elastic Kubernetes Service;Amazon GuardDuty;Security, Identity, & Compliance;Containers;Security Blog
Slug: 2022-05-06-how-to-use-new-amazon-guardduty-eks-protection-findings

[Source](https://aws.amazon.com/blogs/security/how-to-use-new-amazon-guardduty-eks-protection-findings/){:target="_blank" rel="noopener"}

> If you run container workloads that use Amazon Elastic Kubernetes Service (Amazon EKS), Amazon GuardDuty now has added support that will help you better protect these workloads from potential threats. Amazon GuardDuty EKS Protection can help detect threats related to user and application activity that is captured in Kubernetes audit logs. Newly-added Kubernetes threat detections [...]
