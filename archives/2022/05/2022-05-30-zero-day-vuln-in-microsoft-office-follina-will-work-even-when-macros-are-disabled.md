Title: Zero-day vuln in Microsoft Office: 'Follina' will work even when macros are disabled
Date: 2022-05-30T18:01:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-05-30-zero-day-vuln-in-microsoft-office-follina-will-work-even-when-macros-are-disabled

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/30/follina_microsoft_office_vulnerability/){:target="_blank" rel="noopener"}

> Researchers comb through code execution flaw found in malicious document Infosec researchers have idenitied a zero-day code execution vulnerability in Microsoft's ubiquitous Office software.... [...]
