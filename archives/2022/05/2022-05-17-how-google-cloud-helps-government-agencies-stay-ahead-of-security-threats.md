Title: How Google Cloud helps government agencies stay ahead of security threats
Date: 2022-05-17T16:00:00+00:00
Author: Lynn Martin
Category: GCP Security
Tags: Identity & Security;Public Sector
Slug: 2022-05-17-how-google-cloud-helps-government-agencies-stay-ahead-of-security-threats

[Source](https://cloud.google.com/blog/topics/public-sector/how-google-cloud-helps-government-agencies-stay-ahead-security-threats/){:target="_blank" rel="noopener"}

> Cybersecurity remains a top national concern, and Google Cloud is committed to providing government agencies with the security capabilities they need to achieve their missions. At the annual Google Cloud Security Summit today, we’re excited to share updates on how we’re helping governments around the world address their pressing security challenges and meet the demands of new and evolving cybersecurity mandates. Introducing Assured Open Source Software service Google Cloud is announcing its new Assured Open Source Software (OSS) service to help improve the security of the software supply chain, one of the major objectives of White House Executive Order 14028 [...]
