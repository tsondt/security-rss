Title: Vehicle owner data exposed in GM credential-stuffing attack
Date: 2022-05-25T15:41:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-25-vehicle-owner-data-exposed-in-gm-credential-stuffing-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/25/gm-credential-stuffing-attack/){:target="_blank" rel="noopener"}

> Car maker says miscreants used stolen logins to break into folks' accounts Automaker General Motors has confirmed the credential stuffing attack it suffered last month exposed customers' names, personal email addresses, and destination data, as well as usernames and phone numbers for family members tied to customer accounts.... [...]
