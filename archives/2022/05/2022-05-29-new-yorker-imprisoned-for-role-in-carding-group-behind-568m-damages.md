Title: New Yorker imprisoned for role in carding group behind $568M damages
Date: 2022-05-29T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-29-new-yorker-imprisoned-for-role-in-carding-group-behind-568m-damages

[Source](https://www.bleepingcomputer.com/news/security/new-yorker-imprisoned-for-role-in-carding-group-behind-568m-damages/){:target="_blank" rel="noopener"}

> John Telusma, a 37-year-old man from New York, was sentenced to four years in prison for selling and using stolen and compromised credit cards on the Infraud carding portal operated by the transnational cybercrime organization with the same name. [...]
