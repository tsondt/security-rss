Title: Facebook opens political ad data vaults to researchers
Date: 2022-05-24T16:30:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-05-24-facebook-opens-political-ad-data-vaults-to-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/24/facebook_political_ad_targeting_data/){:target="_blank" rel="noopener"}

> Facebook builds FORT to protect against onslaught of regulation, investigation Meta's ad transparency tools will soon reveal another treasure trove of data: advertiser targeting choices for political, election-related, and social issue spots.... [...]
