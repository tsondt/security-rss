Title: QNAP alerts NAS customers of new DeadBolt ransomware attacks
Date: 2022-05-19T06:38:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-19-qnap-alerts-nas-customers-of-new-deadbolt-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/qnap-alerts-nas-customers-of-new-deadbolt-ransomware-attacks/){:target="_blank" rel="noopener"}

> Taiwan-based network-attached storage (NAS) maker QNAP warned customers on Thursday to secure their devices against attacks pushing DeadBolt ransomware payloads. [...]
