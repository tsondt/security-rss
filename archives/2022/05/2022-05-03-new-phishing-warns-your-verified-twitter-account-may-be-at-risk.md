Title: New phishing warns: Your verified Twitter account may be at risk
Date: 2022-05-03T16:04:19-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-03-new-phishing-warns-your-verified-twitter-account-may-be-at-risk

[Source](https://www.bleepingcomputer.com/news/security/new-phishing-warns-your-verified-twitter-account-may-be-at-risk/){:target="_blank" rel="noopener"}

> Phishing emails increasingly target verified Twitter accounts with emails designed to steal their account credentials, as shown by numerous ongoing campaigns conducted by threat actors. [...]
