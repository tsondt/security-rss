Title: Putin threatens supply chains with counter-sanction order
Date: 2022-05-04T05:59:23+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-04-putin-threatens-supply-chains-with-counter-sanction-order

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/04/russian_counter_sanctions/){:target="_blank" rel="noopener"}

> ‘Certain organizations’ to be named in ten days and denied access to Russian resources Russian president Vladimir Putin has authorized retaliatory sanctions against individuals and organizations that have taken action over the illegal invasion of Ukraine.... [...]
