Title: Google Chrome updates failing on Android devices in Russia
Date: 2022-05-13T09:52:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2022-05-13-google-chrome-updates-failing-on-android-devices-in-russia

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-updates-failing-on-android-devices-in-russia/){:target="_blank" rel="noopener"}

> A growing number of Russian Chrome users on Android report getting errors when attempting to install the latest available update of the popular web browser. [...]
