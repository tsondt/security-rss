Title: Critical Jupiter WordPress plugin flaws let hackers take over sites
Date: 2022-05-18T17:12:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-18-critical-jupiter-wordpress-plugin-flaws-let-hackers-take-over-sites

[Source](https://www.bleepingcomputer.com/news/security/critical-jupiter-wordpress-plugin-flaws-let-hackers-take-over-sites/){:target="_blank" rel="noopener"}

> WordPress security analysts have discovered a set of vulnerabilities impacting the Jupiter Theme and JupiterX Core plugins for WordPress, one of which is a critical privilege escalation flaw. [...]
