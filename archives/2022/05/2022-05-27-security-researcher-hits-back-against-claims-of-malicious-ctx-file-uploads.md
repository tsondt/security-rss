Title: Security ‘researcher’ hits back against claims of malicious CTX file uploads
Date: 2022-05-27T10:46:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-27-security-researcher-hits-back-against-claims-of-malicious-ctx-file-uploads

[Source](https://portswigger.net/daily-swig/security-researcher-hits-back-against-claims-of-malicious-ctx-file-uploads){:target="_blank" rel="noopener"}

> They claim that all data received was deleted [...]
