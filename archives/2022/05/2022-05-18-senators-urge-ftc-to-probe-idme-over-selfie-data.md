Title: Senators Urge FTC to Probe ID.me Over Selfie Data
Date: 2022-05-18T16:55:40+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;biometrics;Blake Hall;Cyberscoop;facial recognition;Federal Trade Commission;FTC Chair Lina Khan;id.me;Internal Revenue Service;Sen. Alex Padilla;Sen. Cory Booker;Sen. Edward Markey;Sen. Ron Wyden;Tonya Riley
Slug: 2022-05-18-senators-urge-ftc-to-probe-idme-over-selfie-data

[Source](https://krebsonsecurity.com/2022/05/senators-urge-ftc-to-probe-id-me-over-selfie-data/){:target="_blank" rel="noopener"}

> Some of more tech-savvy Democrats in the U.S. Senate are asking the Federal Trade Commission (FTC) to investigate identity-proofing company ID.me for “deceptive statements” the company and its founder allegedly made over how they handle facial recognition data collected on behalf of the Internal Revenue Service, which until recently required anyone seeking a new IRS account online to provide a live video selfie to ID.me. In a letter to FTC Chair Lina Khan, the Senators charge that ID.me’s CEO Blake Hall has offered conflicting statements about how his company uses the facial scan data it collects on behalf of the [...]
