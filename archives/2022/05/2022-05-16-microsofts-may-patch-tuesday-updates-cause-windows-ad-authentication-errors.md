Title: Microsoft’s May Patch Tuesday Updates Cause Windows AD Authentication Errors
Date: 2022-05-16T11:46:39+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-05-16-microsofts-may-patch-tuesday-updates-cause-windows-ad-authentication-errors

[Source](https://threatpost.com/microsofts-may-patch-tuesday-updates-cause-windows-ad-authentication-errors/179631/){:target="_blank" rel="noopener"}

> Microsoft's May Patch Tuesday update is triggering authentication errors. [...]
