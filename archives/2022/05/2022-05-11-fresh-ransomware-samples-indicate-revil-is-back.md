Title: Fresh ransomware samples indicate REvil is back
Date: 2022-05-11T14:30:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-11-fresh-ransomware-samples-indicate-revil-is-back

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/11/revil-returns-secureworks-samples/){:target="_blank" rel="noopener"}

> Secureworks' investigation only the latest evidence Kaseya and JBS attackers are on the move again New ransomware samples analyzed by Secureworks' threat intelligence team are the latest indication that high-profile ransomware operation REvil is once again up and running after months of relative inactivity.... [...]
