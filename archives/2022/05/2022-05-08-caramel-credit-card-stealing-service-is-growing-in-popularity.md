Title: Caramel credit card stealing service is growing in popularity
Date: 2022-05-08T11:06:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-08-caramel-credit-card-stealing-service-is-growing-in-popularity

[Source](https://www.bleepingcomputer.com/news/security/caramel-credit-card-stealing-service-is-growing-in-popularity/){:target="_blank" rel="noopener"}

> A credit card stealing service is growing in popularity, allowing any low-skilled threat actors an easy and automated way to get started in the world of financial fraud. [...]
