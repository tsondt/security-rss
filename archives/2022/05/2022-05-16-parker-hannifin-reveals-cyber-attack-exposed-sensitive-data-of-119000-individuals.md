Title: Parker Hannifin reveals cyber-attack exposed sensitive data of 119,000 individuals
Date: 2022-05-16T10:48:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-16-parker-hannifin-reveals-cyber-attack-exposed-sensitive-data-of-119000-individuals

[Source](https://portswigger.net/daily-swig/parker-hannifin-reveals-cyber-attack-exposed-sensitive-data-of-119-000-individuals){:target="_blank" rel="noopener"}

> Data breach involves Social Security numbers and health insurance data, among other information [...]
