Title: Microsoft Defender for Endpoint gets new troubleshooting mode
Date: 2022-05-17T14:47:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-05-17-microsoft-defender-for-endpoint-gets-new-troubleshooting-mode

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-for-endpoint-gets-new-troubleshooting-mode/){:target="_blank" rel="noopener"}

> Microsoft says Defender for Endpoint now comes with a new 'troubleshooting mode' that will help Windows admins test Defender Antivirus performance and run compatibility scenarios without getting blocked by tamper protection. [...]
