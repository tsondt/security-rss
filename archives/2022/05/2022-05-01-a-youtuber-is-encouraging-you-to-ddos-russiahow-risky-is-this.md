Title: A YouTuber is encouraging you to DDoS Russia—how risky is this?
Date: 2022-05-01T10:11:16-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-05-01-a-youtuber-is-encouraging-you-to-ddos-russiahow-risky-is-this

[Source](https://www.bleepingcomputer.com/news/security/a-youtuber-is-encouraging-you-to-ddos-russia-how-risky-is-this/){:target="_blank" rel="noopener"}

> A YouTube influencer with hundreds of thousands of subscribers is encouraging everyone to conduct cyber warfare against Russia. How risky is it and can you get in trouble? [...]
