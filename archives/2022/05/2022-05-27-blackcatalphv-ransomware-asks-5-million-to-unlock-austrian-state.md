Title: BlackCat/ALPHV ransomware asks $5 million to unlock Austrian state
Date: 2022-05-27T09:23:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-27-blackcatalphv-ransomware-asks-5-million-to-unlock-austrian-state

[Source](https://www.bleepingcomputer.com/news/security/blackcat-alphv-ransomware-asks-5-million-to-unlock-austrian-state/){:target="_blank" rel="noopener"}

> Austrian federal state Carinthia has been hit by the BlackCat ransomware gang, also known as ALPHV, who demanded a $5 million to unlock the encrypted computer systems. [...]
