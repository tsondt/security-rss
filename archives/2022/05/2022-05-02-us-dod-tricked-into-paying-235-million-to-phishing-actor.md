Title: U.S. DoD tricked into paying $23.5 million to phishing actor
Date: 2022-05-02T08:45:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare;Legal
Slug: 2022-05-02-us-dod-tricked-into-paying-235-million-to-phishing-actor

[Source](https://www.bleepingcomputer.com/news/security/us-dod-tricked-into-paying-235-million-to-phishing-actor/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DoJ) has announced the conviction of Sercan Oyuntur, 40, resident of California, for multiple counts relating to a phishing operation that caused $23.5 million in damages to the U.S. Department of Defense (DoD). [...]
