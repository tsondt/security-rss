Title: Cryptocurrency laundromat Blender shredded by US Treasury in sanctions first
Date: 2022-05-06T19:43:55+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-06-cryptocurrency-laundromat-blender-shredded-by-us-treasury-in-sanctions-first

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/06/us_treasury_sanctions_blender/){:target="_blank" rel="noopener"}

> Helping North Korea? Uncle Sam would like a word The US Treasury has sanctioned cryptocurrency mixer Blender for its role in helping North Korea's Lazarus Group launder stolen digital assets.... [...]
