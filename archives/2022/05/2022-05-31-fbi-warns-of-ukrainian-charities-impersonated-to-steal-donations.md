Title: FBI warns of Ukrainian charities impersonated to steal donations
Date: 2022-05-31T15:43:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-31-fbi-warns-of-ukrainian-charities-impersonated-to-steal-donations

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-ukrainian-charities-impersonated-to-steal-donations/){:target="_blank" rel="noopener"}

> Scammers are claiming to be collecting donations to help Ukrainian refugees and war victims while impersonating legitimate Ukrainian humanitarian aid organizations, according to the Federal Bureau of Investigation (FBI). [...]
