Title: Spanish police dismantle phishing gang that emptied bank accounts
Date: 2022-05-18T17:36:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-18-spanish-police-dismantle-phishing-gang-that-emptied-bank-accounts

[Source](https://www.bleepingcomputer.com/news/security/spanish-police-dismantle-phishing-gang-that-emptied-bank-accounts/){:target="_blank" rel="noopener"}

> The Spanish police have announced the arrest of 13 people and the launch of investigations on another 7 for their participation in a phishing ring that defrauded at least 146 people. [...]
