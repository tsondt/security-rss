Title: Italy warns organizations to brace for incoming DDoS attacks
Date: 2022-05-30T14:10:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-30-italy-warns-organizations-to-brace-for-incoming-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/italy-warns-organizations-to-brace-for-incoming-ddos-attacks/){:target="_blank" rel="noopener"}

> The Computer Security Incident Response Team in Italy issued an urgent alert yesterday to raise awareness about the high risk of cyberattacks against national bodies and organizations on Monday. [...]
