Title: Chicago Public Schools data breach blamed on third-party ransomware attack
Date: 2022-05-23T15:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-23-chicago-public-schools-data-breach-blamed-on-third-party-ransomware-attack

[Source](https://portswigger.net/daily-swig/chicago-public-schools-data-breach-blamed-on-third-party-ransomware-attack){:target="_blank" rel="noopener"}

> Cybercrooks compromised server containing student course information and assessment data [...]
