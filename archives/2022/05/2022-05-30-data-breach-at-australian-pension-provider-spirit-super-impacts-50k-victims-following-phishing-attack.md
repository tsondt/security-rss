Title: Data breach at Australian pension provider Spirit Super impacts 50k victims following phishing attack
Date: 2022-05-30T16:22:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-30-data-breach-at-australian-pension-provider-spirit-super-impacts-50k-victims-following-phishing-attack

[Source](https://portswigger.net/daily-swig/data-breach-at-australian-pension-provider-spirit-super-impacts-50k-victims-following-phishing-attack){:target="_blank" rel="noopener"}

> ‘Super fund’ confirms user information has been exposed [...]
