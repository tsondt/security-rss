Title: F5 Warns of Critical Bug Allowing Remote Code Execution in BIG-IP Systems
Date: 2022-05-05T12:48:08+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-05-05-f5-warns-of-critical-bug-allowing-remote-code-execution-in-big-ip-systems

[Source](https://threatpost.com/f5-critical-bugbig-ip-systems/179514/){:target="_blank" rel="noopener"}

> The vulnerability is 'critical' with a CVSS severity rating of 9.8 out of 10. [...]
