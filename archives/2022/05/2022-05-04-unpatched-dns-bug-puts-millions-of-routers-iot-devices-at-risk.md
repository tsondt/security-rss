Title: Unpatched DNS Bug Puts Millions of Routers, IoT Devices at Risk
Date: 2022-05-04T10:27:47+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: IoT;Vulnerabilities
Slug: 2022-05-04-unpatched-dns-bug-puts-millions-of-routers-iot-devices-at-risk

[Source](https://threatpost.com/dns-bug-millions-routers-iot-risk/179478/){:target="_blank" rel="noopener"}

> A flaw in all versions of the popular C standard libraries uClibe and uClibe-ng can allow for DNS poisoning attacks against target devices. [...]
