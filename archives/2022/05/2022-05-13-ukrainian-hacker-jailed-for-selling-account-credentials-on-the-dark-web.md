Title: Ukrainian hacker jailed for selling account credentials on the dark web
Date: 2022-05-13T12:27:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-13-ukrainian-hacker-jailed-for-selling-account-credentials-on-the-dark-web

[Source](https://portswigger.net/daily-swig/ukrainian-hacker-jailed-for-selling-account-credentials-on-the-dark-web){:target="_blank" rel="noopener"}

> Botnet operator had thousands of hacked credential listings, according to the DoJ [...]
