Title: Microsoft warns partners to revoke unused authorizations that drive <em>your</em> software
Date: 2022-05-18T09:45:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-18-microsoft-warns-partners-to-revoke-unused-authorizations-that-drive-your-software

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/18/microsoft_gdap_advice/){:target="_blank" rel="noopener"}

> June debut of zero trust GDAP tool should make it harder for crims to attack through MSPs and resellers Microsoft has advised its reseller community it needs to pay attention to the debut of improve security tooling aimed at making it harder for attackers to worm their way into your systems through partners.... [...]
