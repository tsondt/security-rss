Title: The Week in Ransomware - May 20th 2022 - Another one bites the dust
Date: 2022-05-20T20:08:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-20-the-week-in-ransomware-may-20th-2022-another-one-bites-the-dust

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-20th-2022-another-one-bites-the-dust/){:target="_blank" rel="noopener"}

> Ransomware attacks continue to slow down, likely due to the invasion of Ukraine, instability in the region, and subsequent worldwide sanctions against Russia. [...]
