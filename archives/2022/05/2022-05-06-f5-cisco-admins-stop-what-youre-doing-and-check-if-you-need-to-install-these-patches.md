Title: F5, Cisco admins: Stop what you're doing and check if you need to install these patches
Date: 2022-05-06T02:06:39+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-06-f5-cisco-admins-stop-what-youre-doing-and-check-if-you-need-to-install-these-patches

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/06/cisco-f5-networking-vulnerabilities/){:target="_blank" rel="noopener"}

> BIG-IP iControl authentication bypass, NFV VM escape, and more F5 Networks and Cisco this week issued warnings about serious, and in some cases critical, security vulnerabilities in their products.... [...]
