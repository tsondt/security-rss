Title: Microsoft patches the patch that broke Windows authentication
Date: 2022-05-20T13:00:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-05-20-microsoft-patches-the-patch-that-broke-windows-authentication

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/20/microsoft_authentication_fix/){:target="_blank" rel="noopener"}

> May 10 update addressed serious vulns but also had problems of its own Microsoft has released an out-of-band patch to deal with an authentication issue that was introduced in the May 10 Windows update.... [...]
