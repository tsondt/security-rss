Title: Angry IT admin wipes employer’s databases, gets 7 years in prison
Date: 2022-05-14T11:18:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-14-angry-it-admin-wipes-employers-databases-gets-7-years-in-prison

[Source](https://www.bleepingcomputer.com/news/security/angry-it-admin-wipes-employer-s-databases-gets-7-years-in-prison/){:target="_blank" rel="noopener"}

> Han Bing, a former database administrator for Lianjia, a Chinese real-estate brokerage giant, has been sentenced to 7 years in prison for logging into corporate systems and deleting the company's data. [...]
