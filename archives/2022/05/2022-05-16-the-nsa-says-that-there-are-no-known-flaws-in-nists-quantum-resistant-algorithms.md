Title: The NSA Says that There are No Known Flaws in NIST’s Quantum-Resistant Algorithms
Date: 2022-05-16T11:34:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;encryption;NIST;NSA;quantum computing
Slug: 2022-05-16-the-nsa-says-that-there-are-no-known-flaws-in-nists-quantum-resistant-algorithms

[Source](https://www.schneier.com/blog/archives/2022/05/the-nsa-says-that-there-are-no-known-flaws-in-nists-quantum-resistant-algorithms.html){:target="_blank" rel="noopener"}

> Rob Joyce, the director of cybersecurity at the NSA, said so in an interview: The NSA already has classified quantum-resistant algorithms of its own that it developed over many years, said Joyce. But it didn’t enter any of its own in the contest. The agency’s mathematicians, however, worked with NIST to support the process, trying to crack the algorithms in order to test their merit. “Those candidate algorithms that NIST is running the competitions on all appear strong, secure, and what we need for quantum resistance,” Joyce said. “We’ve worked against all of them to make sure they are solid.” [...]
