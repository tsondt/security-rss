Title: Choosing the right certificate revocation method in ACM Private CA
Date: 2022-05-16T19:18:31+00:00
Author: Arthur Mnev
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;ACM;ACM Private CA;AWS Certificate Manager;certificate revocation;Certificates Revocation;CRL;OCSP;PCA;Security Blog
Slug: 2022-05-16-choosing-the-right-certificate-revocation-method-in-acm-private-ca

[Source](https://aws.amazon.com/blogs/security/choosing-the-right-certificate-revocation-method-in-acm-private-ca/){:target="_blank" rel="noopener"}

> AWS Certificate Manager Private Certificate Authority (ACM PCA) is a highly available, fully managed private certificate authority (CA) service that allows you to create CA hierarchies and issue X.509 certificates from the CAs you create in ACM PCA. You can then use these certificates for scenarios such as encrypting TLS communication channels, cryptographically signing code, [...]
