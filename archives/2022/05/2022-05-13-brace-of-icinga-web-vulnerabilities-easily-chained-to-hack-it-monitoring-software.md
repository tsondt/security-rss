Title: Brace of Icinga web vulnerabilities ‘easily chained’ to hack IT monitoring software
Date: 2022-05-13T13:49:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-13-brace-of-icinga-web-vulnerabilities-easily-chained-to-hack-it-monitoring-software

[Source](https://portswigger.net/daily-swig/brace-of-icinga-web-vulnerabilities-easily-chained-to-hack-it-monitoring-software){:target="_blank" rel="noopener"}

> Open source IT monitoring system gets patched [...]
