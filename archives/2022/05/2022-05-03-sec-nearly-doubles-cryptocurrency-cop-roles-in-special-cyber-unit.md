Title: SEC nearly doubles cryptocurrency cop roles in special cyber unit
Date: 2022-05-03T17:31:55+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-03-sec-nearly-doubles-cryptocurrency-cop-roles-in-special-cyber-unit

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/03/sec_doubles_crypto_cops/){:target="_blank" rel="noopener"}

> Policing digital assets sounds more Mission Impossible than NCIS The US Securities and Exchange Commission intends to fill an additional 20 positions in a special unit that polices cryptocurrency fraud and other cybercrimes.... [...]
