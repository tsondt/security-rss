Title: VMware patches critical auth bypass flaw in multiple products
Date: 2022-05-18T12:01:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-18-vmware-patches-critical-auth-bypass-flaw-in-multiple-products

[Source](https://www.bleepingcomputer.com/news/security/vmware-patches-critical-auth-bypass-flaw-in-multiple-products/){:target="_blank" rel="noopener"}

> VMware warned customers today to immediately patch a critical authentication bypass vulnerability "affecting local domain users" in multiple products that can be exploited to obtain admin privileges. [...]
