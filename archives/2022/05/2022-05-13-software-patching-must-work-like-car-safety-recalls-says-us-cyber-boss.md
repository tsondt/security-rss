Title: Software patching must work like car safety recalls, says US cyber boss
Date: 2022-05-13T16:00:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-13-software-patching-must-work-like-car-safety-recalls-says-us-cyber-boss

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/13/us_cyber_director_patching/){:target="_blank" rel="noopener"}

> Adds infosec regulation coming to more industries but with a light touch, more collaboration Black Hat Asia Software made unsafe by dependencies should be fixed without users needing to interact with the source of the problem, according to US National Cyber Director Chris Inglis, who serves in the Executive Office of the President.... [...]
