Title: Securing the human voice at scale: Pindrop partners with Google Cloud
Date: 2022-05-04T13:00:00+00:00
Author: Yariv Adan
Category: GCP Security
Tags: Identity & Security;Customers;Startups;AI & Machine Learning
Slug: 2022-05-04-securing-the-human-voice-at-scale-pindrop-partners-with-google-cloud

[Source](https://cloud.google.com/blog/products/ai-machine-learning/pindrop-delivers-secure-voice-interactions-at-scale-with-google-cloud/){:target="_blank" rel="noopener"}

> If you’ve ever used only your voice to authenticate a payment, place an order, or check an account over the phone, there is a good chance that Pindrop’s technology made it possible. Founded in Atlanta in 2011, Pindrop provides software and technology that uses machine learning, voice recognition, and behavioral analytics to help detect and prevent fraud in voice channels like AI-powered call centers and solve the long standing identity problem commonly encountered in customer care and contact center experiences. Demand for Pindrop’s technology has grown considerably over the past decade, as more organizations and people adopt voice as an [...]
