Title: RuTube hack: Russian video platform denies loss of source code following cyber-attack
Date: 2022-05-11T11:53:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-11-rutube-hack-russian-video-platform-denies-loss-of-source-code-following-cyber-attack

[Source](https://portswigger.net/daily-swig/rutube-hack-russian-video-platform-denies-loss-of-source-code-following-cyber-attack){:target="_blank" rel="noopener"}

> The ‘Russian alternative to YouTube’ has been offline since Monday [...]
