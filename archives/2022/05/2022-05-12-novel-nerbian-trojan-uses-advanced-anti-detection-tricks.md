Title: Novel ‘Nerbian’ Trojan Uses Advanced Anti-Detection Tricks
Date: 2022-05-12T10:45:06+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: 2022-05-12-novel-nerbian-trojan-uses-advanced-anti-detection-tricks

[Source](https://threatpost.com/nerbian-rat-advanced-trick/179600/){:target="_blank" rel="noopener"}

> The stealthy, feature-rich malware has multistage evasion tactics to fly under the radar of security analysis, researchers at Proofpoint have found. [...]
