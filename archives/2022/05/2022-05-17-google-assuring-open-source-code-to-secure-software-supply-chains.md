Title: Google assuring open-source code to secure software supply chains
Date: 2022-05-17T16:00:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-17-google-assuring-open-source-code-to-secure-software-supply-chains

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/17/google_assured_open_source_software/){:target="_blank" rel="noopener"}

> Java and Python packages are the first on the list Google has a plan — and a new product plus a partnership with developer-focused security shop Snyk — that attempts to make it easier for enterprises to secure their open source software dependencies.... [...]
