Title: Forging Australian Driver’s Licenses
Date: 2022-05-23T11:09:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authorization;cars;encryption;forgery
Slug: 2022-05-23-forging-australian-drivers-licenses

[Source](https://www.schneier.com/blog/archives/2022/05/forging-australian-drivers.html){:target="_blank" rel="noopener"}

> The New South Wales digital driver’s license has multiple implementation flaws that allow for easy forgeries. This file is encrypted using AES-256-CBC encryption combined with Base64 encoding. A 4-digit application PIN (which gets set during the initial onboarding when a user first instals the application) is the encryption password used to protect or encrypt the licence data. The problem here is that an attacker who has access to the encrypted licence data (whether that be through accessing a phone backup, direct access to the device or remote compromise) could easily brute-force this 4-digit PIN by using a script that would [...]
