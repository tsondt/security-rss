Title: CyberUK 2022: Global power conflicts creating ‘balkinization’ of cybersecurity tech
Date: 2022-05-11T15:53:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-11-cyberuk-2022-global-power-conflicts-creating-balkinization-of-cybersecurity-tech

[Source](https://portswigger.net/daily-swig/cyberuk-2022-global-power-conflicts-creating-balkinization-of-cybersecurity-tech){:target="_blank" rel="noopener"}

> Technology interoperability at risk from wider conflict between China and the West [...]
