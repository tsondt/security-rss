Title: Ransomware Deals Deathblow to 157-year-old College
Date: 2022-05-11T11:02:21+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2022-05-11-ransomware-deals-deathblow-to-157-year-old-college

[Source](https://threatpost.com/ransomware-deathblow-college/179574/){:target="_blank" rel="noopener"}

> Why a private college that stayed in business for 157 years had to close after the combo of COVID-19 and ransomware proved too much. [...]
