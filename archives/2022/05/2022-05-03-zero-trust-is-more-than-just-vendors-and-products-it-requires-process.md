Title: Zero trust is more than just vendors and products – it requires process
Date: 2022-05-03T16:00:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-03-zero-trust-is-more-than-just-vendors-and-products-it-requires-process

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/03/zero-trust-vendors-products-dell/){:target="_blank" rel="noopener"}

> IT orgs need to adapt their procedures to make it all work, says Dell Dell Technologies World Zero-trust architectures have become a focus for enterprises trying to figure out how to secure an IT environment where data and applications are increasingly distributed outside of the traditional perimeter defenses of central datacenters.... [...]
