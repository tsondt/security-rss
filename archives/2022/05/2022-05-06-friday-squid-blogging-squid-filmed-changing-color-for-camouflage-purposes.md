Title: Friday Squid Blogging: Squid Filmed Changing Color for Camouflage Purposes
Date: 2022-05-06T21:15:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2022-05-06-friday-squid-blogging-squid-filmed-changing-color-for-camouflage-purposes

[Source](https://www.schneier.com/blog/archives/2022/05/friday-squid-blogging-squid-filmed-changing-color-for-camouflage-purposes.html){:target="_blank" rel="noopener"}

> Video of oval squid ( Sepioteuthis lessoniana ) changing color in reaction to their background. The research paper claims this is the first time this has been documented. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
