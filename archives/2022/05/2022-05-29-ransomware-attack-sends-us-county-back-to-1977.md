Title: Ransomware attack sends US county back to 1977
Date: 2022-05-29T23:36:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-05-29-ransomware-attack-sends-us-county-back-to-1977

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/29/security_roundup/){:target="_blank" rel="noopener"}

> Also: Uni details its malware-catching AI, signs of China poking the Russian cyber-bear, and more In brief Somerset County, New Jersey, was hit by a ransomware attack this week that hobbled its ability to conduct business, and also cut off access to essential data.... [...]
