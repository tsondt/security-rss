Title: Is 100% Cybersecurity Readiness Possible? Medical Device Pros Weigh In
Date: 2022-05-25T10:00:01-04:00
Author: Sponsored by Cybellum
Category: BleepingComputer
Tags: Security
Slug: 2022-05-25-is-100-cybersecurity-readiness-possible-medical-device-pros-weigh-in

[Source](https://www.bleepingcomputer.com/news/security/is-100-percent-cybersecurity-readiness-possible-medical-device-pros-weigh-in/){:target="_blank" rel="noopener"}

> As medical devices become more connected and reliant on software, their codebase grows both in size and complexity, and they are increasingly reliant on third-party and open source software components. Learn more from 150 senior decision makers who oversee product security or cybersecurity compliance in the medical device industry, [...]
