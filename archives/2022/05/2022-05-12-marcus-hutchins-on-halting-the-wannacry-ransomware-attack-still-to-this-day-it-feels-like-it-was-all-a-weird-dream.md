Title: Marcus Hutchins on halting the WannaCry ransomware attack – ‘Still to this day it feels like it was all a weird dream’
Date: 2022-05-12T14:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-12-marcus-hutchins-on-halting-the-wannacry-ransomware-attack-still-to-this-day-it-feels-like-it-was-all-a-weird-dream

[Source](https://portswigger.net/daily-swig/marcus-hutchins-on-halting-the-wannacry-ransomware-attack-still-to-this-day-it-feels-like-it-was-all-a-weird-dream){:target="_blank" rel="noopener"}

> Five years since WannaCry exploded onto the scene, ransomware still tops global threat lists ANALYSIS Five years ago today (May 12), a ransomware attack blamed on a North Korean hacking group hit comp [...]
