Title: Industrial Spy data extortion market gets into the ransomware game
Date: 2022-05-26T08:02:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-26-industrial-spy-data-extortion-market-gets-into-the-ransomware-game

[Source](https://www.bleepingcomputer.com/news/security/industrial-spy-data-extortion-market-gets-into-the-ransomware-game/){:target="_blank" rel="noopener"}

> The Industrial Spy data extortion marketplace has now launched its own ransomware operation, where they now also encrypt victim's devices. [...]
