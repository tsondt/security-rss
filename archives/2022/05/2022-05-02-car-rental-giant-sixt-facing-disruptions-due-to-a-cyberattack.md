Title: Car rental giant Sixt facing disruptions due to a cyberattack
Date: 2022-05-02T11:44:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-02-car-rental-giant-sixt-facing-disruptions-due-to-a-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/car-rental-giant-sixt-facing-disruptions-due-to-a-cyberattack/){:target="_blank" rel="noopener"}

> ​Car rental giant Sixt was hit by a weekend cyberattack causing business disruptions at customer care centers and select branch [...]
