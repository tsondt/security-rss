Title: Third-party web trackers log what you type before submitting
Date: 2022-05-16T17:15:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-16-third-party-web-trackers-log-what-you-type-before-submitting

[Source](https://www.bleepingcomputer.com/news/security/third-party-web-trackers-log-what-you-type-before-submitting/){:target="_blank" rel="noopener"}

> An extensive study looking into the top 100k ranking websites has revealed that many are leaking information you enter in the site forms to third-party trackers before you even press submit. [...]
