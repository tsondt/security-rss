Title: Microsoft Teams, Windows 11 hacked on first day of Pwn2Own
Date: 2022-05-19T07:39:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-05-19-microsoft-teams-windows-11-hacked-on-first-day-of-pwn2own

[Source](https://www.bleepingcomputer.com/news/security/microsoft-teams-windows-11-hacked-on-first-day-of-pwn2own/){:target="_blank" rel="noopener"}

> During the first day of Pwn2Own Vancouver 2022, contestants won $800,000 after successfully exploiting 16 zero-day bugs to hack multiple products, including Microsoft's Windows 11 operating system and the Teams communication platform. [...]
