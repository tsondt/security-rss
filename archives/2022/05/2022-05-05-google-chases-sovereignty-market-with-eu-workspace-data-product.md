Title: Google chases sovereignty market with EU Workspace Data product
Date: 2022-05-05T13:30:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-05-05-google-chases-sovereignty-market-with-eu-workspace-data-product

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/05/google_eu_sovereign/){:target="_blank" rel="noopener"}

> Woos European firms who don't want their data caught in the US Cloud Act dragnet Google is joining Microsoft in its attempts to tackle EU concerns regarding data sovereignty but some privacy experts are yet to be convinced by the move.... [...]
