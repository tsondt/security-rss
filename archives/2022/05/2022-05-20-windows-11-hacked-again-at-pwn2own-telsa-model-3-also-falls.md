Title: Windows 11 hacked again at Pwn2Own, Telsa Model 3 also falls
Date: 2022-05-20T08:10:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-20-windows-11-hacked-again-at-pwn2own-telsa-model-3-also-falls

[Source](https://www.bleepingcomputer.com/news/security/windows-11-hacked-again-at-pwn2own-telsa-model-3-also-falls/){:target="_blank" rel="noopener"}

> During the second day of the Pwn2Own Vancouver 2022 hacking competition, contestants hacked Microsoft's Windows 11 OS again and demoed zero-days in Tesla Model 3's infotainment system. [...]
