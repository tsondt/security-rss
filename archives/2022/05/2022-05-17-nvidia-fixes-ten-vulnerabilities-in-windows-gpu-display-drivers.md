Title: NVIDIA fixes ten vulnerabilities in Windows GPU display drivers
Date: 2022-05-17T15:12:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-17-nvidia-fixes-ten-vulnerabilities-in-windows-gpu-display-drivers

[Source](https://www.bleepingcomputer.com/news/security/nvidia-fixes-ten-vulnerabilities-in-windows-gpu-display-drivers/){:target="_blank" rel="noopener"}

> NVIDIA has released a security update for a wide range of graphics card models, addressing four high-severity and six medium-severity vulnerabilities in its GPU drivers. [...]
