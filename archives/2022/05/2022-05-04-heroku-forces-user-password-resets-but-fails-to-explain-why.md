Title: Heroku forces user password resets but fails to explain why
Date: 2022-05-04T13:57:04-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-04-heroku-forces-user-password-resets-but-fails-to-explain-why

[Source](https://www.bleepingcomputer.com/news/security/heroku-forces-user-password-resets-but-fails-to-explain-why/){:target="_blank" rel="noopener"}

> Salesforce-owned Heroku is performing a forced password reset on a subset of user accounts in response to last month's security incident while providing no information as to why they are doing so other than vaguely mentioning it is to further secure accounts. [...]
