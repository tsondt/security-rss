Title: Google: Chinese state hackers keep targeting Russian govt agencies
Date: 2022-05-03T13:29:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-03-google-chinese-state-hackers-keep-targeting-russian-govt-agencies

[Source](https://www.bleepingcomputer.com/news/security/google-chinese-state-hackers-keep-targeting-russian-govt-agencies/){:target="_blank" rel="noopener"}

> Google said today that a Chinese-sponsored hacking group linked to China's People's Liberation Army Strategic Support Force (PLA SSF) is targeting Russian government agencies. [...]
