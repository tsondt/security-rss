Title: Vodafone plans carrier-level user tracking for targeted ads
Date: 2022-05-30T16:00:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-30-vodafone-plans-carrier-level-user-tracking-for-targeted-ads

[Source](https://www.bleepingcomputer.com/news/security/vodafone-plans-carrier-level-user-tracking-for-targeted-ads/){:target="_blank" rel="noopener"}

> Vodafone is piloting a new advertising ID system called TrustPid, which will work as a persistent user tracker at the mobile Internet Service Provider (ISP) level. [...]
