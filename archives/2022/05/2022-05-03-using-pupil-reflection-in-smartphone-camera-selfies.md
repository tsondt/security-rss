Title: Using Pupil Reflection in Smartphone Camera Selfies
Date: 2022-05-03T16:17:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;identification;smartphones
Slug: 2022-05-03-using-pupil-reflection-in-smartphone-camera-selfies

[Source](https://www.schneier.com/blog/archives/2022/05/using-pupil-reflection-in-smartphone-camera-selfies.html){:target="_blank" rel="noopener"}

> Researchers are using the reflection of the smartphone in the pupils of faces taken as selfies to infer information about how the phone is being used: For now, the research is focusing on six different ways a user can hold a device like a smartphone: with both hands, just the left, or just the right in portrait mode, and the same options in horizontal mode. It’s not a lot of information, but it’s a start. (It’ll be a while before we can reproduce these results from Blade Runner.) Research paper. [...]
