Title: U.S. DOJ will no longer prosecute ethical hackers under CFAA
Date: 2022-05-19T13:24:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-05-19-us-doj-will-no-longer-prosecute-ethical-hackers-under-cfaa

[Source](https://www.bleepingcomputer.com/news/security/us-doj-will-no-longer-prosecute-ethical-hackers-under-cfaa/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DOJ) has announced a revision of its policy on how federal prosecutors should charge violations of the Computer Fraud and Abuse Act (CFAA), carving out "good-fath" security research from being prosecuted. [...]
