Title: 15.3 Million Request-Per-Second DDoS Attack
Date: 2022-05-05T11:02:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;cyberattack;denial of service
Slug: 2022-05-05-153-million-request-per-second-ddos-attack

[Source](https://www.schneier.com/blog/archives/2022/05/15-3-million-request-per-second-ddos-attack.html){:target="_blank" rel="noopener"}

> Cloudflare is reporting a large DDoS attack against an unnamed company “operating a crypto launchpad.” While this isn’t the largest application-layer attack we’ve seen, it is the largest we’ve seen over HTTP S. HTTPS DDoS attacks are more expensive in terms of required computational resources because of the higher cost of establishing a secure TLS encrypted connection. Therefore it costs the attacker more to launch the attack, and for the victim to mitigate it. We’ve seen very large attacks in the past over (unencrypted) HTTP, but this attack stands out because of the resources it required at its scale. The [...]
