Title: Ukraine war a sorting hat for cyber-governance loyalties: Black Hat founder Jeff Moss
Date: 2022-05-12T04:59:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-12-ukraine-war-a-sorting-hat-for-cyber-governance-loyalties-black-hat-founder-jeff-moss

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/12/jeff_moss_ukraine_cyber_governance/){:target="_blank" rel="noopener"}

> Private orgs that flex with Russian bans may do more harm – to themselves – than good Black Hat Asia The war in Ukraine, and the Declaration for the Future of the Internet signed by 60 nations in late April, should be understood in the context of a global effort to recruit the nations of the world into blocs with different attitudes to internet governance.... [...]
