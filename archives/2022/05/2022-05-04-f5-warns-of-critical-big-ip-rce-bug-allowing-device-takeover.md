Title: F5 warns of critical BIG-IP RCE bug allowing device takeover
Date: 2022-05-04T18:16:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-04-f5-warns-of-critical-big-ip-rce-bug-allowing-device-takeover

[Source](https://www.bleepingcomputer.com/news/security/f5-warns-of-critical-big-ip-rce-bug-allowing-device-takeover/){:target="_blank" rel="noopener"}

> F5 has issued a security advisory warning about a flaw that may allow unauthenticated attackers with network access to execute arbitrary system commands, perform file actions, and disable services on BIG-IP. [...]
