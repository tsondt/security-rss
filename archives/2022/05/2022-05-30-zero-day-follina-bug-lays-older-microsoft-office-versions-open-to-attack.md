Title: Zero-Day ‘Follina’ Bug Lays Older Microsoft Office Versions Open to Attack
Date: 2022-05-30T14:53:18+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2022-05-30-zero-day-follina-bug-lays-older-microsoft-office-versions-open-to-attack

[Source](https://threatpost.com/zero-day-follina-bug-lays-older-microsoft-office-versions-open-to-attack/179756/){:target="_blank" rel="noopener"}

> Malware loads itself from remote servers and bypasses Microsoft's Defender AV scanner, according to reports. [...]
