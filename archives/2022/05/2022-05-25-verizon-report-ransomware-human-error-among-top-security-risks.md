Title: Verizon Report: Ransomware, Human Error Among Top Security Risks
Date: 2022-05-25T12:45:59+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Critical Infrastructure;Malware;Vulnerabilities
Slug: 2022-05-25-verizon-report-ransomware-human-error-among-top-security-risks

[Source](https://threatpost.com/verizon-dbir-report-2022/179725/){:target="_blank" rel="noopener"}

> 2022’s DBIR also highlighted the far-reaching impact of supply-chain breaches and how organizations and their employees are the reasons why incidents occur. [...]
