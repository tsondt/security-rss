Title: How to find NPM dependencies vulnerable to account hijacking
Date: 2022-05-23T07:58:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-05-23-how-to-find-npm-dependencies-vulnerable-to-account-hijacking

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/23/npm_dependencies_vulnerable/){:target="_blank" rel="noopener"}

> Security engineer outlines self-help strategy for keeping software supply chain safe Following the recent disclosure of a technique for hijacking certain NPM packages, security engineer Danish Tariq has proposed a defensive strategy for those looking to assess whether their web apps include dependencies tied to subvertable email domains.... [...]
