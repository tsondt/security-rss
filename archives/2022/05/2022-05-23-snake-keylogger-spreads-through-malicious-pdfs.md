Title: Snake Keylogger Spreads Through Malicious PDFs
Date: 2022-05-23T12:07:56+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: 2022-05-23-snake-keylogger-spreads-through-malicious-pdfs

[Source](https://threatpost.com/snake-keylogger-pdfs/179703/){:target="_blank" rel="noopener"}

> Microsoft Word also leveraged in the email campaign, which uses a 22-year-old Office RCE bug. [...]
