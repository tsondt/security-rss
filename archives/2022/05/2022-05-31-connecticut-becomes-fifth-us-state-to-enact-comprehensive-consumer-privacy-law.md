Title: Connecticut becomes fifth US state to enact comprehensive consumer privacy law
Date: 2022-05-31T14:14:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-31-connecticut-becomes-fifth-us-state-to-enact-comprehensive-consumer-privacy-law

[Source](https://portswigger.net/daily-swig/connecticut-becomes-fifth-us-state-to-enact-comprehensive-consumer-privacy-law){:target="_blank" rel="noopener"}

> The newly signed CTPA is more consumer-friendly than similar legislation in other US states [...]
