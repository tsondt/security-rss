Title: Announcing Sovereign Controls for Google Workspace
Date: 2022-05-04T07:00:00+00:00
Author: Javier Soltero
Category: GCP Security
Tags: Identity & Security;Google Workspace
Slug: 2022-05-04-announcing-sovereign-controls-for-google-workspace

[Source](https://cloud.google.com/blog/products/workspace/announcing-sovereign-controls-for-google-workspace/){:target="_blank" rel="noopener"}

> Enabling EU organizations through digital sovereignty European organizations are moving their operations and data to the cloud in increasing numbers to enable collaboration, drive business value, and transition to hybrid work. However, the cloud solutions that underpin these powerful capabilities must meet an organization’s critical requirements for security, privacy, and digital sovereignty. We often hear from European Union policymakers and business leaders that ensuring the sovereignty of their cloud data, through regionalization and additional controls over administrative access, is crucial in this evolving landscape. Today, we’re announcing Sovereign Controls for Google Workspace, which will provide digital sovereignty capabilities for organizations, [...]
