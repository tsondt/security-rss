Title: Google starts testing fenced frames to guard its Privacy Sandbox
Date: 2022-05-03T07:29:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-05-03-google-starts-testing-fenced-frames-to-guard-its-privacy-sandbox

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/03/google_fenced_frames/){:target="_blank" rel="noopener"}

> Oh, serve me ads, lots of ads, under clouded eyes above, just fence me in Google in the next few days plans to begin testing fenced frames, a proposed web API to help its Privacy Sandbox ad technologies meet commitments to privacy of a sort.... [...]
