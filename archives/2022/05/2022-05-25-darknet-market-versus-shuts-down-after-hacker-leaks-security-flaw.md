Title: Darknet market Versus shuts down after hacker leaks security flaw
Date: 2022-05-25T11:54:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-25-darknet-market-versus-shuts-down-after-hacker-leaks-security-flaw

[Source](https://www.bleepingcomputer.com/news/security/darknet-market-versus-shuts-down-after-hacker-leaks-security-flaw/){:target="_blank" rel="noopener"}

> ​The Versus Market, one of the most popular English-speaking criminal darknet markets, is shutting down after discovering a severe exploit that could have allowed access to its database and exposed the IP address of its servers. [...]
