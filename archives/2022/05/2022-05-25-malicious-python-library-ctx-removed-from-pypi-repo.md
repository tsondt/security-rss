Title: Malicious Python library CTX removed from PyPI repo
Date: 2022-05-25T10:32:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-25-malicious-python-library-ctx-removed-from-pypi-repo

[Source](https://portswigger.net/daily-swig/malicious-python-library-ctx-removed-from-pypi-repo){:target="_blank" rel="noopener"}

> A suspicious developer appears to have performed a domain hijack to take over the original project [...]
