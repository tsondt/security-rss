Title: Kali Linux 2022.2 released with 10 new tools, WSL improvements, and more
Date: 2022-05-16T12:35:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Linux
Slug: 2022-05-16-kali-linux-20222-released-with-10-new-tools-wsl-improvements-and-more

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20222-released-with-10-new-tools-wsl-improvements-and-more/){:target="_blank" rel="noopener"}

> Offensive Security has released ​Kali Linux 2022.2, the second version in 2022, with desktop enhancements, a fun April Fools screensaver, WSL GUI improvements, terminal tweaks, and best of all, new tools to play with! [...]
