Title: Google SMTP relay service abused for sending phishing emails
Date: 2022-05-02T13:51:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-02-google-smtp-relay-service-abused-for-sending-phishing-emails

[Source](https://www.bleepingcomputer.com/news/security/google-smtp-relay-service-abused-for-sending-phishing-emails/){:target="_blank" rel="noopener"}

> Phishing actors abuse Google's SMTP relay service to bypass email security products and successfully deliver malicious emails to targeted users. [...]
