Title: Google: Predator spyware infected Android devices using zero-days
Date: 2022-05-22T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-05-22-google-predator-spyware-infected-android-devices-using-zero-days

[Source](https://www.bleepingcomputer.com/news/security/google-predator-spyware-infected-android-devices-using-zero-days/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group (TAG) says that state-backed threat actors used five zero-day vulnerabilities to install Predator spyware developed by commercial surveillance developer Cytrox. [...]
