Title: Anatomy of a campaign to inject JavaScript into compromised WordPress sites
Date: 2022-05-13T04:09:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-13-anatomy-of-a-campaign-to-inject-javascript-into-compromised-wordpress-sites

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/13/wordpress-redirect-hack/){:target="_blank" rel="noopener"}

> Reverse-engineered code redirects visitors to dodgy corners of the internet A years-long campaign by miscreants to insert malicious JavaScript into vulnerable WordPress sites, so that visitors are redirected to scam websites, has been documented by reverse-engineers.... [...]
