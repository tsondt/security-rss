Title: Cyber-spies target Microsoft Exchange to steal M&amp;A info
Date: 2022-05-04T00:31:50+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-04-cyber-spies-target-microsoft-exchange-to-steal-ma-info

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/04/microsoft_exchange_mergers/){:target="_blank" rel="noopener"}

> If a network snoop probes like a Kremlin agent, exploits like a Kremlin agent, it might be... A cyber-spy group is targeting Microsoft Exchange deployments to steal data related to mergers and acquisitions and large corporate transactions, according to Mandiant.... [...]
