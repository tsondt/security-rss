Title: Mozilla: Lack of Security Protections in Mental-Health Apps Is ‘Creepy’
Date: 2022-05-03T12:42:35+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Privacy
Slug: 2022-05-03-mozilla-lack-of-security-protections-in-mental-health-apps-is-creepy

[Source](https://threatpost.com/mozilla-security-health-apps-creepy/179463/){:target="_blank" rel="noopener"}

> Popular apps to support people’s psychological and spiritual well-being can harm them by sharing their personal and sensitive data with third parties, among other privacy offenses. [...]
