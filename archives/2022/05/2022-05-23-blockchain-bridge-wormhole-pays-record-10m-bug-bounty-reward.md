Title: Blockchain bridge Wormhole pays record $10m bug bounty reward
Date: 2022-05-23T13:16:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-23-blockchain-bridge-wormhole-pays-record-10m-bug-bounty-reward

[Source](https://portswigger.net/daily-swig/blockchain-bridge-wormhole-pays-record-10m-bug-bounty-reward){:target="_blank" rel="noopener"}

> Critical security flaw patched on the same day it was submitted [...]
