Title: Cisco fixes NFVIS bugs that help gain root and hijack hosts
Date: 2022-05-04T15:58:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-04-cisco-fixes-nfvis-bugs-that-help-gain-root-and-hijack-hosts

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-nfvis-bugs-that-help-gain-root-and-hijack-hosts/){:target="_blank" rel="noopener"}

> Cisco has addressed several security flaws found in the Enterprise NFV Infrastructure Software (NFVIS), a solution that helps virtualize network services for easier management of virtual network functions (VNFs). [...]
