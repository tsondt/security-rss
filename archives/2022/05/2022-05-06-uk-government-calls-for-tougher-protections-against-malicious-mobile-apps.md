Title: UK government calls for tougher protections against malicious mobile apps
Date: 2022-05-06T12:11:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-06-uk-government-calls-for-tougher-protections-against-malicious-mobile-apps

[Source](https://portswigger.net/daily-swig/uk-government-calls-for-tougher-protections-against-malicious-mobile-apps){:target="_blank" rel="noopener"}

> NCSC proposes new code of conduct for app stores [...]
