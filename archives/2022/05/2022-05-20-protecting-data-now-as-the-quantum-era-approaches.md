Title: Protecting data now as the quantum era approaches
Date: 2022-05-20T07:30:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-20-protecting-data-now-as-the-quantum-era-approaches

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/20/quantum-security-qusecure/){:target="_blank" rel="noopener"}

> Startup QuSecure is the latest vendor to jump into the field with its as-a-service offering Analysis Startup QuSecure will this week introduce a service aimed at addressing how to safeguard cybersecurity once quantum computing renders current public key encryption technologies vulnerable.... [...]
