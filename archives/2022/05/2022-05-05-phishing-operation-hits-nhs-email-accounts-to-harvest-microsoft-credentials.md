Title: Phishing operation hits NHS email accounts to harvest Microsoft credentials
Date: 2022-05-05T07:30:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-05-phishing-operation-hits-nhs-email-accounts-to-harvest-microsoft-credentials

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/05/phishing_campaign_nhs/){:target="_blank" rel="noopener"}

> You've won $2m! Now just send me a small fee A phishing operation compromised over one hundred UK National Health Service (NHS) employees' Microsoft Exchange email accounts for credential harvesting purposes, according to email security shop Inky.... [...]
