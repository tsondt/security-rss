Title: Hackers steal WhatsApp accounts using call forwarding trick
Date: 2022-05-31T19:10:09-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-05-31-hackers-steal-whatsapp-accounts-using-call-forwarding-trick

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-whatsapp-accounts-using-call-forwarding-trick/){:target="_blank" rel="noopener"}

> There's a trick that allows attackers to hijack a victim's WhatsApp account and gain access to personal messages and contact list. [...]
