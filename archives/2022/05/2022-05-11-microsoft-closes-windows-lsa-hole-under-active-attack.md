Title: Microsoft closes Windows LSA hole under active attack
Date: 2022-05-11T01:15:43+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-11-microsoft-closes-windows-lsa-hole-under-active-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/11/microsoft_patch_tuesday/){:target="_blank" rel="noopener"}

> Plus many more flaws. And Adobe, Android, SAP join the bug-squashing frenzy Microsoft patched 74 security flaws in its May Patch Tuesday batch of updates. That's seven critical bugs, 66 deemed important, and one ranked low severity.... [...]
