Title: Spanish PM, defense minister latest Pegasus spyware victims
Date: 2022-05-02T16:00:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-02-spanish-pm-defense-minister-latest-pegasus-spyware-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/02/spain_pegasus_malware/){:target="_blank" rel="noopener"}

> Latest Spanish officials to detect Pegasus spyware on mobile devices Spain's prime minister and defense minister are the latest elected officials to detect Pegasus spyware on their mobile phones, according to multiple media reports quoting Spanish authorities.... [...]
