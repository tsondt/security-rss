Title: US revises policy regarding Computer Fraud and Abuse Act, will not prosecute good faith research
Date: 2022-05-20T11:21:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-20-us-revises-policy-regarding-computer-fraud-and-abuse-act-will-not-prosecute-good-faith-research

[Source](https://portswigger.net/daily-swig/us-revises-policy-regarding-computer-fraud-and-abuse-act-will-not-prosecute-good-faith-research){:target="_blank" rel="noopener"}

> DoJ makes long-anticipated changes to policy [...]
