Title: Tails users warned not to launch bundled Tor Browser until security fix is released
Date: 2022-05-25T13:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-25-tails-users-warned-not-to-launch-bundled-tor-browser-until-security-fix-is-released

[Source](https://portswigger.net/daily-swig/tails-users-warned-not-to-launch-bundled-tor-browser-until-security-fix-is-released){:target="_blank" rel="noopener"}

> Critical vulnerability has been fixed upstream, but Tails dev team ‘doesn’t have the capacity to publish an emergency release earlier’ [...]
