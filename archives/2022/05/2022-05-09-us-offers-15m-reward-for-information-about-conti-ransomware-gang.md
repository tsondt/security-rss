Title: US offers $15m reward for information about Conti ransomware gang
Date: 2022-05-09T16:30:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-09-us-offers-15m-reward-for-information-about-conti-ransomware-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/09/us-reward-conti-ransomware/){:target="_blank" rel="noopener"}

> The State Department notice comes in wake of the cybercrims’ attack on Costa Rican government The US government is offering up to $15 million for information about key leaders of the notorious Conti ransomware group and any individual participating in an attack using a variant of Conti's malware.... [...]
