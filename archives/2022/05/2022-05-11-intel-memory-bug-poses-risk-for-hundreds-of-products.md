Title: Intel Memory Bug Poses Risk for Hundreds of Products
Date: 2022-05-11T12:27:33+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-05-11-intel-memory-bug-poses-risk-for-hundreds-of-products

[Source](https://threatpost.com/intel-memory-bug-poses-risk-for-hundreds-of-products/179595/){:target="_blank" rel="noopener"}

> Dell and HP were among the first to release patches and fixes for the bug. [...]
