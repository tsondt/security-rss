Title: Zero Trust for Data Helps Enterprises Detect, Respond and Recover from Breaches
Date: 2022-05-23T12:47:12+00:00
Author: Threatpost
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-05-23-zero-trust-for-data-helps-enterprises-detect-respond-and-recover-from-breaches

[Source](https://threatpost.com/zero-trust-for-data/179706/){:target="_blank" rel="noopener"}

> Mohit Tiwari, CEO of Symmetry Systems, explores Zero Trust, data objects and the NIST framework for cloud and on-prem environments. [...]
