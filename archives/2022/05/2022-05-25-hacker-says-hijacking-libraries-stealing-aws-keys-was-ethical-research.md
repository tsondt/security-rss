Title: Hacker says hijacking libraries, stealing AWS keys was ethical research
Date: 2022-05-25T09:42:26-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-05-25-hacker-says-hijacking-libraries-stealing-aws-keys-was-ethical-research

[Source](https://www.bleepingcomputer.com/news/security/hacker-says-hijacking-libraries-stealing-aws-keys-was-ethical-research/){:target="_blank" rel="noopener"}

> The hacker of 'ctx' and 'PHPass' libraries has now broken silence and explained the reasons behind this hijack to BleepingComputer. According to the hacker, this was a bug bounty exercise and no malicious activity was intended. [...]
