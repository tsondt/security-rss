Title: Iran-linked Cobalt Mirage extracts money, info from US orgs – report
Date: 2022-05-13T12:11:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-13-iran-linked-cobalt-mirage-extracts-money-info-from-us-orgs-report

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/13/cobalt-mirage-ransomware/){:target="_blank" rel="noopener"}

> Khamenei, can you just not? Not right now, fam The Iran-linked Cobalt Mirage crew is running attacks against America for both financial gain and for cyber-espionage purposes, according to Secureworks' threat intelligence team.... [...]
