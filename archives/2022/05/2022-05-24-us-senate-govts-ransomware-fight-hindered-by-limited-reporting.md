Title: US Senate: Govt’s ransomware fight hindered by limited reporting
Date: 2022-05-24T13:34:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency;Government
Slug: 2022-05-24-us-senate-govts-ransomware-fight-hindered-by-limited-reporting

[Source](https://www.bleepingcomputer.com/news/security/us-senate-govt-s-ransomware-fight-hindered-by-limited-reporting/){:target="_blank" rel="noopener"}

> A report published today by U.S. Senator Gary Peters, Chairman of the Senate Homeland Security and Governmental Affairs Committee, says law enforcement and regulatory agencies lack insight into ransomware attacks to fight against them effectively. [...]
