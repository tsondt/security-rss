Title: Millions of people's info stolen from MGM Resorts dumped on Telegram for free
Date: 2022-05-25T23:44:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-25-millions-of-peoples-info-stolen-from-mgm-resorts-dumped-on-telegram-for-free

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/25/mgm_customers_data_dumped_again/){:target="_blank" rel="noopener"}

> Meanwhile, Twitter coughs up $150m after using account security contact details for advertising Miscreants have dumped on Telegram more than 142 million customer records stolen from MGM Resorts, exposing names, postal and email addresses, phone numbers, and dates of birth for any would-be identity thief.... [...]
