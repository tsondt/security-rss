Title: Tor project upgrades network speed performance with new system
Date: 2022-05-05T07:26:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-05-tor-project-upgrades-network-speed-performance-with-new-system

[Source](https://www.bleepingcomputer.com/news/security/tor-project-upgrades-network-speed-performance-with-new-system/){:target="_blank" rel="noopener"}

> The Tor Project has published details about a newly introduced system called Congestion Control that promises to eliminate speed limits on the network. [...]
