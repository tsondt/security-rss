Title: Beijing needs the ability to 'destroy' Starlink, say Chinese researchers
Date: 2022-05-25T11:01:44+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-25-beijing-needs-the-ability-to-destroy-starlink-say-chinese-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/25/beijing_starlink_takedown/){:target="_blank" rel="noopener"}

> Paper authors warn Elon Musk's 2,400 machines could be used offensively A researcher from the Beijing Institute of Tracking and Telecommunications advocated for Chinese military capability to take out Starlink satellites on the grounds of national security in a peer-reviewed domestic journal.... [...]
