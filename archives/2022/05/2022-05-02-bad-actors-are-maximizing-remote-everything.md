Title: Bad Actors Are Maximizing Remote Everything
Date: 2022-05-02T12:41:25+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-05-02-bad-actors-are-maximizing-remote-everything

[Source](https://threatpost.com/bad-actors-remote-everything/179458/){:target="_blank" rel="noopener"}

> Aamir Lakhani, global security strategist and researcher at FortiGuard Labs, zeroes in on how adversaries are targeting 'remote everything'. [...]
