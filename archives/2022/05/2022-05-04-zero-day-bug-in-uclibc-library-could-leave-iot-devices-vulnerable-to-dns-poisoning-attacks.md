Title: Zero-day bug in uClibc library could leave IoT devices vulnerable to DNS poisoning attacks
Date: 2022-05-04T14:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-04-zero-day-bug-in-uclibc-library-could-leave-iot-devices-vulnerable-to-dns-poisoning-attacks

[Source](https://portswigger.net/daily-swig/zero-day-bug-in-uclibc-library-could-leave-iot-devices-vulnerable-to-dns-poisoning-attacks){:target="_blank" rel="noopener"}

> Unpatched flaw caused by the predictability of transaction IDs [...]
