Title: Release of Technical Report into the AMD Security Processor
Date: 2022-05-10T12:00:00-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2022-05-10-release-of-technical-report-into-the-amd-security-processor

[Source](https://googleprojectzero.blogspot.com/2022/05/release-of-technical-report-into-amd.html){:target="_blank" rel="noopener"}

> Posted by James Forshaw, Google Project Zero Today, members of Project Zero and the Google Cloud security team are releasing a technical report on a security review of AMD Secure Processor (ASP). The ASP is an isolated core in AMD EPYC CPUs that adds a root of trust and controls secure system initialization. As it's a generic processor AMD can add additional security features to the firmware, but like with all complex systems it's possible these features might have security issues which could compromise the security of everything under the ASP's management. The security review undertaken was on the implementation [...]
