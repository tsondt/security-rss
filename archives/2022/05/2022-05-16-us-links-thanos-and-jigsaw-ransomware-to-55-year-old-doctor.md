Title: US links Thanos and Jigsaw ransomware to 55-year-old doctor
Date: 2022-05-16T16:46:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-16-us-links-thanos-and-jigsaw-ransomware-to-55-year-old-doctor

[Source](https://www.bleepingcomputer.com/news/security/us-links-thanos-and-jigsaw-ransomware-to-55-year-old-doctor/){:target="_blank" rel="noopener"}

> The US Department of Justice today said that Moises Luis Zagala Gonzalez (Zagala), a 55-year-old cardiologist with French and Venezuelan citizenship residing in Ciudad Bolivar, Venezuela, created and rented Jigsaw and Thanos ransomware to cybercriminals. [...]
