Title: How to reprogram Apple AirTags, play custom sounds
Date: 2022-05-27T00:52:31+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-05-27-how-to-reprogram-apple-airtags-play-custom-sounds

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/27/apple_airtag_sounds/){:target="_blank" rel="noopener"}

> Voltage glitch here, glitch there, now you can fiddle with location disc's firmware At the Workshop on Offensive Technologies 2022 (WOOT) on Thursday, security researchers demonstrated how to meddle with AirTags, Apple's coin-sized tracking devices.... [...]
