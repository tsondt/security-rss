Title: FBI: Cyber-scams cost victims $6.9b-plus worldwide in 2021
Date: 2022-05-05T22:13:19+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-05-fbi-cyber-scams-cost-victims-69b-plus-worldwide-in-2021

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/05/fbi_cyber_scams/){:target="_blank" rel="noopener"}

> Another banner year for criminals. For everyone else, not so much Cyber-scams cost victims around the globe at least $6.9 billion last year, according to the FBI's latest Internet Crime Report.... [...]
