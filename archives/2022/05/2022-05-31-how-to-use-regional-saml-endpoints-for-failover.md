Title: How to use regional SAML endpoints for failover
Date: 2022-05-31T15:53:20+00:00
Author: Jonathan VanKim
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;AWS STS;Disaster Recovery;Federation;Identity;Resilience;Security Blog;Security token service
Slug: 2022-05-31-how-to-use-regional-saml-endpoints-for-failover

[Source](https://aws.amazon.com/blogs/security/how-to-use-regional-saml-endpoints-for-failover/){:target="_blank" rel="noopener"}

> Many Amazon Web Services (AWS) customers choose to use federation with SAML 2.0 in order to use their existing identity provider (IdP) and avoid managing multiple sources of identities. Some customers have previously configured federation by using AWS Identity and Access Management (IAM) with the endpoint signin.aws.amazon.com. Although this endpoint is highly available, it is [...]
