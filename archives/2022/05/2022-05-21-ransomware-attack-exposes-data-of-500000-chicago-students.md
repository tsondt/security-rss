Title: Ransomware attack exposes data of 500,000 Chicago students
Date: 2022-05-21T13:32:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-21-ransomware-attack-exposes-data-of-500000-chicago-students

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-exposes-data-of-500-000-chicago-students/){:target="_blank" rel="noopener"}

> The Chicago Public Schools has suffered a massive data breach that exposed the data of almost 500,000 students and 60,000 employee after their vendor, Battelle for Kids, suffered a ransomware attack in December. [...]
