Title: Biden signs cybercrime tracking bill into law
Date: 2022-05-09T22:09:26+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-09-biden-signs-cybercrime-tracking-bill-into-law

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/09/biden_signs_cybercrime_tracking_bill/){:target="_blank" rel="noopener"}

> All part of a larger push by the Feds to improve cybersecurity reporting US President Joe Biden has signed into law a bill that aims to improve how the federal government tracks and prosecutes cybercrime.... [...]
