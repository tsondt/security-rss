Title: Security bug in VMWare Workspace ONE could allow access to internal, cloud networks
Date: 2022-05-02T12:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-02-security-bug-in-vmware-workspace-one-could-allow-access-to-internal-cloud-networks

[Source](https://portswigger.net/daily-swig/security-bug-in-vmware-workspace-one-could-allow-access-to-internal-cloud-networks){:target="_blank" rel="noopener"}

> Users should patch immediately [...]
