Title: Historic Hotel Stay, Complementary Emotet Exposure included
Date: 2022-05-12T10:02:01-04:00
Author: Sponsored by InQuest
Category: BleepingComputer
Tags: Security
Slug: 2022-05-12-historic-hotel-stay-complementary-emotet-exposure-included

[Source](https://www.bleepingcomputer.com/news/security/historic-hotel-stay-complementary-emotet-exposure-included/){:target="_blank" rel="noopener"}

> Historic Hotel of America serving up modern malware to their guests. Why securing your inbox with more than just anti-malware engines is needed to prevent cybercrime attacks. [...]
