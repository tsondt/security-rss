Title: Path traversal flaw found in OWASP enterprise library of security controls
Date: 2022-05-03T15:02:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-03-path-traversal-flaw-found-in-owasp-enterprise-library-of-security-controls

[Source](https://portswigger.net/daily-swig/path-traversal-flaw-found-in-owasp-enterprise-library-of-security-controls){:target="_blank" rel="noopener"}

> Difficult-to-exploit ESAPI vulnerability offers best practice lessons [...]
