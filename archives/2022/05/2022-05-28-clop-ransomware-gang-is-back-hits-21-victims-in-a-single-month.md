Title: Clop ransomware gang is back, hits 21 victims in a single month
Date: 2022-05-28T11:10:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-28-clop-ransomware-gang-is-back-hits-21-victims-in-a-single-month

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-gang-is-back-hits-21-victims-in-a-single-month/){:target="_blank" rel="noopener"}

> After effectively shutting down their entire operation for several months, between November and February, the Clop ransomware is now back according to NCC Group researchers. [...]
