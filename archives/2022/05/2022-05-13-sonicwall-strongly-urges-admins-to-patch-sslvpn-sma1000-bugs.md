Title: SonicWall ‘strongly urges’ admins to patch SSLVPN SMA1000 bugs
Date: 2022-05-13T11:38:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-13-sonicwall-strongly-urges-admins-to-patch-sslvpn-sma1000-bugs

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-strongly-urges-admins-to-patch-sslvpn-sma1000-bugs/){:target="_blank" rel="noopener"}

> SonicWall "strongly urges" customers to patch several high-risk security flaws impacting its Secure Mobile Access (SMA) 1000 Series line of products that can let attackers bypass authorization and, potentially, compromise unpatched appliances. [...]
