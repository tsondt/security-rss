Title: OAS platform vulnerable to critical RCE and API access flaws
Date: 2022-05-26T15:11:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-26-oas-platform-vulnerable-to-critical-rce-and-api-access-flaws

[Source](https://www.bleepingcomputer.com/news/security/oas-platform-vulnerable-to-critical-rce-and-api-access-flaws/){:target="_blank" rel="noopener"}

> Threat analysts have disclosed vulnerabilities affecting the Open Automation Software (OAS) platform, leading to device access, denial of service, and remote code execution. [...]
