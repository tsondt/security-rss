Title: UK cybersecurity center sent 33 million alerts to companies
Date: 2022-05-10T16:56:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-10-uk-cybersecurity-center-sent-33-million-alerts-to-companies

[Source](https://www.bleepingcomputer.com/news/security/uk-cybersecurity-center-sent-33-million-alerts-to-companies/){:target="_blank" rel="noopener"}

> The NCSC (National Cyber Security Centre) in the UK reports having served 33 million alerts to organizations signed up for its "Early Warning" service. Additionally, the government agency has dealt with a record number of online scams in 2021, removing more than 2.7 million from the internet. [...]
