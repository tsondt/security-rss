Title: Suspected phishing email crime boss cuffed in Nigeria
Date: 2022-05-26T07:25:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-26-suspected-phishing-email-crime-boss-cuffed-in-nigeria

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/26/nigerian_phishing_arrest/){:target="_blank" rel="noopener"}

> Interpol, cops swoop with intel from cybersecurity bods Interpol and cops in Africa have arrested a Nigerian man suspected of running a multi-continent cybercrime ring that specialized in phishing emails targeting businesses.... [...]
