Title: Microsoft's identity services huddle under Entra umbrella
Date: 2022-05-31T17:45:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-05-31-microsofts-identity-services-huddle-under-entra-umbrella

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/31/entra/){:target="_blank" rel="noopener"}

> Decentralized identity and knowing who needs what Microsoft has whipped out the rebranding team once more, and chosen the name "Entra" as a catch-all for the company's identity and access capabilities.... [...]
