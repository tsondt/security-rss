Title: FBI: Rise in Business Email-based Attacks is a $43B Headache
Date: 2022-05-09T17:23:35+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Hacks;Vulnerabilities
Slug: 2022-05-09-fbi-rise-in-business-email-based-attacks-is-a-43b-headache

[Source](https://threatpost.com/fbi-bec-43b/179539/){:target="_blank" rel="noopener"}

> A huge spike in fraudulent activities related to attacks leveraging business email accounts is a billion-dollar-problem. [...]
