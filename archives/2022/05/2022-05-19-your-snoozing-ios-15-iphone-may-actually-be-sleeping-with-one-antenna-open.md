Title: Your snoozing iOS 15 iPhone may actually be sleeping with one antenna open
Date: 2022-05-19T06:02:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-19-your-snoozing-ios-15-iphone-may-actually-be-sleeping-with-one-antenna-open

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/19/apple-iphone-malware/){:target="_blank" rel="noopener"}

> No, you're not really gonna be hacked. But you may be surprised Some research into the potentially exploitable low-power state of iPhones has sparked headlines this week.... [...]
