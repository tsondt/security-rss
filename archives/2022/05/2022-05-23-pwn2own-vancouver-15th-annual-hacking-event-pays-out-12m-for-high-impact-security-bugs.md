Title: Pwn2Own Vancouver: 15th annual hacking event pays out $1.2m for high-impact security bugs
Date: 2022-05-23T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-23-pwn2own-vancouver-15th-annual-hacking-event-pays-out-12m-for-high-impact-security-bugs

[Source](https://portswigger.net/daily-swig/pwn2own-vancouver-15th-annual-hacking-event-pays-out-1-2m-for-high-impact-security-bugs){:target="_blank" rel="noopener"}

> Tesla, Microsoft, and others targeted in hacking competition that saw Star Labs crowned ‘Masters of Pwn’ [...]
