Title: Patch released for cross-domain cookie leakage flaw in Guzzle
Date: 2022-05-27T14:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-27-patch-released-for-cross-domain-cookie-leakage-flaw-in-guzzle

[Source](https://portswigger.net/daily-swig/patch-released-for-cross-domain-cookie-leakage-flaw-in-guzzle){:target="_blank" rel="noopener"}

> Drupal rolls out update for issue that is contingent on cookie middleware being enabled [...]
