Title: Microsoft warns of brute-force attacks targeting MSSQL servers
Date: 2022-05-18T09:27:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-05-18-microsoft-warns-of-brute-force-attacks-targeting-mssql-servers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-brute-force-attacks-targeting-mssql-servers/){:target="_blank" rel="noopener"}

> Microsoft warned of brute-forcing attacks targeting Internet-exposed and poorly secured Microsoft SQL Server (MSSQL) database servers using weak passwords. [...]
