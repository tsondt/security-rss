Title: Privacy pathology: It's time for the users to gather a little data. Evidence
Date: 2022-05-03T08:30:04+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2022-05-03-privacy-pathology-its-time-for-the-users-to-gather-a-little-data-evidence

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/03/opinion_column_privacy/){:target="_blank" rel="noopener"}

> If Sherlock was alive today, he’d pack a Pi next to pistol and pipe Opinion Almost exactly a month ago, we noted a splendid piece of academic research into Google's data-gathering and consent practises.... [...]
