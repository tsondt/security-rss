Title: Interpol arrests alleged leader of the SilverTerrier BEC gang
Date: 2022-05-25T09:04:40-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-05-25-interpol-arrests-alleged-leader-of-the-silverterrier-bec-gang

[Source](https://www.bleepingcomputer.com/news/security/interpol-arrests-alleged-leader-of-the-silverterrier-bec-gang/){:target="_blank" rel="noopener"}

> After a year-long investigation that involved Interpol and several cybersecurity companies, the Nigeria Police Force has arrested an individual believed to be in the top ranks of a prominent business email compromise (BEC) group known as SilverTerrier or TMT. [...]
