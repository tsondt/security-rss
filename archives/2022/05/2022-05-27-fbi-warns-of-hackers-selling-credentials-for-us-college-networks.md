Title: FBI warns of hackers selling credentials for U.S. college networks
Date: 2022-05-27T16:26:39-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-05-27-fbi-warns-of-hackers-selling-credentials-for-us-college-networks

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-hackers-selling-credentials-for-us-college-networks/){:target="_blank" rel="noopener"}

> Cybercriminals are offering to sell for thousands of U.S. dollars network access credentials for higher education institutions based in the United States. [...]
