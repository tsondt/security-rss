Title: Dell brings data recovery tools to Apex and the cloud
Date: 2022-05-02T17:05:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-02-dell-brings-data-recovery-tools-to-apex-and-the-cloud

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/02/dell-apex-cyber-recovery/){:target="_blank" rel="noopener"}

> Dell shows off full stack of cyber recovery SaaS, partners with Snowflake for data analytics LAS VEGAS – Dell is giving enterprises new ways to protect the data they store in public clouds.... [...]
