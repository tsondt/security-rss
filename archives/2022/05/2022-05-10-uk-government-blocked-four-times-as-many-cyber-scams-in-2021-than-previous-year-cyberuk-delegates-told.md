Title: UK government blocked four times as many cyber-scams in 2021 than previous year, CyberUK delegates told
Date: 2022-05-10T15:19:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-10-uk-government-blocked-four-times-as-many-cyber-scams-in-2021-than-previous-year-cyberuk-delegates-told

[Source](https://portswigger.net/daily-swig/uk-government-blocked-four-times-as-many-cyber-scams-in-2021-than-previous-year-cyberuk-delegates-told){:target="_blank" rel="noopener"}

> War in Ukraine and ransomware trends top the agenda at this year’s NCSC conference [...]
