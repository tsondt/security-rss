Title: Ex-spymaster and fellow Brexiteers' emails leaked by suspected Russian op
Date: 2022-05-26T06:27:05+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-05-26-ex-spymaster-and-fellow-brexiteers-emails-leaked-by-suspected-russian-op

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/26/brexit_emails_leaked/){:target="_blank" rel="noopener"}

> A 'Very English Coop (sic) d'Etat' Emails between leading pro-Brexit figures in the UK have seemingly been stolen and leaked online by what could be a Kremlin cyberespionage team.... [...]
