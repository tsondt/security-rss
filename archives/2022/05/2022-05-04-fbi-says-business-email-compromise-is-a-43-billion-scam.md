Title: FBI says business email compromise is a $43 billion scam
Date: 2022-05-04T12:19:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-04-fbi-says-business-email-compromise-is-a-43-billion-scam

[Source](https://www.bleepingcomputer.com/news/security/fbi-says-business-email-compromise-is-a-43-billion-scam/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) said today that the amount of money lost to business email compromise (BEC) scams continues to grow each year, with a 65% increase in the identified global exposed losses between July 2019 and December 2021. [...]
