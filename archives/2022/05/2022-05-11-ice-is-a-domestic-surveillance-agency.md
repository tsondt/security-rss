Title: ICE Is a Domestic Surveillance Agency
Date: 2022-05-11T14:24:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;national security policy;privacy;reports;secrecy;surveillance
Slug: 2022-05-11-ice-is-a-domestic-surveillance-agency

[Source](https://www.schneier.com/blog/archives/2022/05/ice-is-a-domestic-surveillance-agency.html){:target="_blank" rel="noopener"}

> Georgetown has a new report on the highly secretive bulk surveillance activities of ICE in the US: When you think about government surveillance in the United States, you likely think of the National Security Agency or the FBI. You might even think of a powerful police agency, such as the New York Police Department. But unless you or someone you love has been targeted for deportation, you probably don’t immediately think of Immigration and Customs Enforcement (ICE). This report argues that you should. Our two-year investigation, including hundreds of Freedom of Information Act requests and a comprehensive review of ICE’s [...]
