Title: New ransomware strains linked to North Korean govt hackers
Date: 2022-05-03T18:31:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-03-new-ransomware-strains-linked-to-north-korean-govt-hackers

[Source](https://www.bleepingcomputer.com/news/security/new-ransomware-strains-linked-to-north-korean-govt-hackers/){:target="_blank" rel="noopener"}

> Several ransomware strains have been linked to APT38, a North Korean-sponsored hacking group known for its focus on targeting and stealing funds from financial institutions worldwide. [...]
