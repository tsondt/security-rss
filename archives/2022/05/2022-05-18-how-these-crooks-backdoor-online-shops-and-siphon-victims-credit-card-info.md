Title: How these crooks backdoor online shops and siphon victims' credit card info
Date: 2022-05-18T18:47:53+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-18-how-these-crooks-backdoor-online-shops-and-siphon-victims-credit-card-info

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/18/fbi_credit_card/){:target="_blank" rel="noopener"}

> FBI and co blow lid off latest PHP tampering scam The FBI and its friends have warned businesses of crooks scraping people's credit-card details from tampered payment pages on compromised websites.... [...]
