Title: DuckDuckGo browser allows Microsoft trackers due to search agreement
Date: 2022-05-24T18:07:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-24-duckduckgo-browser-allows-microsoft-trackers-due-to-search-agreement

[Source](https://www.bleepingcomputer.com/news/security/duckduckgo-browser-allows-microsoft-trackers-due-to-search-agreement/){:target="_blank" rel="noopener"}

> The privacy-focused DuckDuckGo browser purposely allows Microsoft trackers on third-party sites due to an agreement in their syndicated search content contract between the two companies. [...]
