Title: Microsoft: Credit card stealers are getting much stealthier
Date: 2022-05-24T14:44:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-24-microsoft-credit-card-stealers-are-getting-much-stealthier

[Source](https://www.bleepingcomputer.com/news/security/microsoft-credit-card-stealers-are-getting-much-stealthier/){:target="_blank" rel="noopener"}

> Microsoft's security researchers have observed a worrying trend in credit card skimming, where threat actors employ more advanced techniques to hide their malicious info-stealing code. [...]
