Title: Enterprise DevOps Guidebook - Chapter 1
Date: 2022-05-26T23:00:00+00:00
Author: Rob Edwards
Category: GCP Security
Tags: Application Modernization;Identity & Security;Developers & Practitioners;Google Cloud;DevOps & SRE
Slug: 2022-05-26-enterprise-devops-guidebook-chapter-1

[Source](https://cloud.google.com/blog/products/devops-sre/devops-enterprise-guidebook-chapter-1/){:target="_blank" rel="noopener"}

> The Google Cloud DORA team has been hard at work releasing our yearly Accelerate State of DevOps report. This research provides an independent view into the practices and capabilities that organizations, irrespective of their size, industry, and region, can employ to drive better performance. Year over year, the State of DevOps report helps organizations benchmark themselves against others in the industry as elite, high, medium, or low performers and provides recommendations for how organizations can continually improve. The table below highlights elite, high, medium, and low performers at a glance from the last report. To give more prescriptive advice on [...]
