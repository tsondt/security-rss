Title: Ransomware encrypts files, demands three good deeds to restore data
Date: 2022-05-26T23:20:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-26-ransomware-encrypts-files-demands-three-good-deeds-to-restore-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/26/promoting_goodwill_via_malware_extortion/){:target="_blank" rel="noopener"}

> Shut up and take... poor kids to KFC? In what is either a creepy, weird spin on Robin Hood or something from a Black Mirror episode, we're told a ransomware gang is encrypting data and then forcing each victim to perform three good deeds before they can download a decryption tool.... [...]
