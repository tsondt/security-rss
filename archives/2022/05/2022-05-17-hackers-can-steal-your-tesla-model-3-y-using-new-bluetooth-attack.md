Title: Hackers can steal your Tesla Model 3, Y using new Bluetooth attack
Date: 2022-05-17T10:30:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-17-hackers-can-steal-your-tesla-model-3-y-using-new-bluetooth-attack

[Source](https://www.bleepingcomputer.com/news/security/hackers-can-steal-your-tesla-model-3-y-using-new-bluetooth-attack/){:target="_blank" rel="noopener"}

> Security researchers at the NCC Group have developed a tool to carry out a Bluetooth Low Energy (BLE) relay attack that bypasses all existing protections to authenticate on target devices. [...]
