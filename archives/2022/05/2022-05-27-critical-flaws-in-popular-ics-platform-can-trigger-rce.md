Title: Critical Flaws in Popular ICS Platform Can Trigger RCE
Date: 2022-05-27T10:32:07+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-05-27-critical-flaws-in-popular-ics-platform-can-trigger-rce

[Source](https://threatpost.com/critical-flaws-in-popular-ics-platform-can-trigger-rce/179750/){:target="_blank" rel="noopener"}

> Cisco Talos discovered eight vulnerabilities in the Open Automation Software, two of them critical, that pose risk for critical infrastructure networks. [...]
