Title: Screencastify fixes bug that would have let rogue websites spy on webcams
Date: 2022-05-24T00:17:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-05-24-screencastify-fixes-bug-that-would-have-let-rogue-websites-spy-on-webcams

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/24/screencastify_chrome_extension_patched/){:target="_blank" rel="noopener"}

> School-friendly tool still not fully protected, privacy guru warns Screencastify, a popular Chrome extension for capturing and sharing videos from websites, was recently found to be vulnerable to a cross-site scripting (XSS) flaw that allowed arbitrary websites to dupe people into unknowingly activating their webcams.... [...]
