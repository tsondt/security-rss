Title: FBI, CISA, and NSA warn of hackers increasingly targeting MSPs
Date: 2022-05-11T08:29:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-05-11-fbi-cisa-and-nsa-warn-of-hackers-increasingly-targeting-msps

[Source](https://www.bleepingcomputer.com/news/security/fbi-cisa-and-nsa-warn-of-hackers-increasingly-targeting-msps/){:target="_blank" rel="noopener"}

> Members of the Five Eyes (FVEY) intelligence alliance today warned managed service providers (MSPs) and their customers that they're increasingly targeted by supply chain attacks. [...]
