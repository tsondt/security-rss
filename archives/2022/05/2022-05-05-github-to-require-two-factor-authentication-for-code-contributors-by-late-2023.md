Title: GitHub to require two factor authentication for code contributors by late 2023
Date: 2022-05-05T04:01:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-05-github-to-require-two-factor-authentication-for-code-contributors-by-late-2023

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/05/github_2fa_mandatory_2023/){:target="_blank" rel="noopener"}

> Code locker has figured out it's a giant honeypot for miscreants planning supply chain attacks GitHub has announced that it will require two factor authentication for users who contribute code on its service.... [...]
