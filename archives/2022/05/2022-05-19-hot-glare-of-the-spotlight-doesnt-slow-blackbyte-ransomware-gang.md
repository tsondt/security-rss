Title: Hot glare of the spotlight doesn’t slow BlackByte ransomware gang
Date: 2022-05-19T09:56:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-19-hot-glare-of-the-spotlight-doesnt-slow-blackbyte-ransomware-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/19/blackbyte-ransomware-attacks/){:target="_blank" rel="noopener"}

> Crew's raids continue worldwide, Talos team warns The US government's alert three months ago warning businesses and government agencies about the threat of BlackByte has apparently done little to slow down the ransomware group's activities.... [...]
