Title: Bluetooth Flaw Allows Remote Unlocking of Digital Locks
Date: 2022-05-20T11:02:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Bluetooth;cars;hacking;locks
Slug: 2022-05-20-bluetooth-flaw-allows-remote-unlocking-of-digital-locks

[Source](https://www.schneier.com/blog/archives/2022/05/bluetooth-flaw-allows-remote-unlocking-of-digital-locks.html){:target="_blank" rel="noopener"}

> Locks that use Bluetooth Low Energy to authenticate keys are vulnerable to remote unlocking. The research focused on Teslas, but the exploit is generalizable. In a video shared with Reuters, NCC Group researcher Sultan Qasim Khan was able to open and then drive a Tesla using a small relay device attached to a laptop which bridged a large gap between the Tesla and the Tesla owner’s phone. “This proves that any product relying on a trusted BLE connection is vulnerable to attacks even from the other side of the world,” the UK-based firm said in a statement, referring to the [...]
