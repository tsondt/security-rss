Title: Russian hackers compromise embassy emails to target governments
Date: 2022-05-01T11:06:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-01-russian-hackers-compromise-embassy-emails-to-target-governments

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-compromise-embassy-emails-to-target-governments/){:target="_blank" rel="noopener"}

> Security analysts have uncovered a recent phishing campaign from Russian hackers known as APT29 (Cozy Bear or Nobelium) targeting diplomats and government entities. [...]
