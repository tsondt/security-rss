Title: Microsoft, Apple, and Google to support FIDO passwordless logins
Date: 2022-05-05T12:19:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-05-microsoft-apple-and-google-to-support-fido-passwordless-logins

[Source](https://www.bleepingcomputer.com/news/security/microsoft-apple-and-google-to-support-fido-passwordless-logins/){:target="_blank" rel="noopener"}

> Microsoft, Apple, and Google announced today plans to support a common passwordless sign-in standard (known as passkeys) developed by the World Wide Web Consortium (W3C) and the FIDO Alliance. [...]
