Title: Friday Squid Blogging: Squid Bites Diver
Date: 2022-05-27T20:57:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-05-27-friday-squid-blogging-squid-bites-diver

[Source](https://www.schneier.com/blog/archives/2022/05/friday-squid-blogging-squid-bites-diver.html){:target="_blank" rel="noopener"}

> I agree; the diver deserved it. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
