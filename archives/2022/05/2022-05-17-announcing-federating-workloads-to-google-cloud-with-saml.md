Title: Announcing federating workloads to Google Cloud with SAML
Date: 2022-05-17T16:00:00+00:00
Author: Shaun Liu
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-05-17-announcing-federating-workloads-to-google-cloud-with-saml

[Source](https://cloud.google.com/blog/products/identity-security/introducing-federating-saml-workloads-to-google-cloud/){:target="_blank" rel="noopener"}

> At Google Cloud, we’re focused on giving customers new ways to strengthen their security posture. Many organizations use service account keys to authenticate for Google Cloud Platform API access, but managing keys can be challenging, especially in multi-cloud scenarios. To help, last year we launched Workload Identity Federation, which allows workloads running on-premises or in other clouds to federate with an external identity provider (IdP) and call Google Cloud resources without using a service account key. Initially, we supported federating workloads via the Open ID Connect (OIDC) authentication protocol. We are excited to announce today that support for SAML federation [...]
