Title: Russian Sberbank says it’s facing massive waves of DDoS attacks
Date: 2022-05-20T07:53:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-20-russian-sberbank-says-its-facing-massive-waves-of-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/russian-sberbank-says-it-s-facing-massive-waves-of-ddos-attacks/){:target="_blank" rel="noopener"}

> Sberbank's vice president and director of cybersecurity, Sergei Lebed, has told participants of the Positive Hack Days forum that the company is going through a period of unprecedented targeting by hackers. [...]
