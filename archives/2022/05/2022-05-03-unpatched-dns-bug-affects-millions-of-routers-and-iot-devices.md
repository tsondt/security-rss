Title: Unpatched DNS bug affects millions of routers and IoT devices
Date: 2022-05-03T09:18:54-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-03-unpatched-dns-bug-affects-millions-of-routers-and-iot-devices

[Source](https://www.bleepingcomputer.com/news/security/unpatched-dns-bug-affects-millions-of-routers-and-iot-devices/){:target="_blank" rel="noopener"}

> A vulnerability in the domain name system (DNS) component of a popular C standard library that is present in a wide range of IoT products may put millions of devices at DNS poisoning attack risk. [...]
