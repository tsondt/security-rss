Title: Chinese cyber-espionage group Moshen Dragon targets Asian telcos
Date: 2022-05-02T18:38:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-02-chinese-cyber-espionage-group-moshen-dragon-targets-asian-telcos

[Source](https://www.bleepingcomputer.com/news/security/chinese-cyber-espionage-group-moshen-dragon-targets-asian-telcos/){:target="_blank" rel="noopener"}

> Researchers have identified a new cluster of malicious cyber activity tracked as Moshen Dragon, targeting telecommunication service providers in Central Asia. [...]
