Title: Heroku resets user passwords after concluding April cyber-attack ran deep
Date: 2022-05-05T14:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-05-heroku-resets-user-passwords-after-concluding-april-cyber-attack-ran-deep

[Source](https://portswigger.net/daily-swig/heroku-resets-user-passwords-after-concluding-april-cyber-attack-ran-deep){:target="_blank" rel="noopener"}

> Hack investigation blames compromised token for breach [...]
