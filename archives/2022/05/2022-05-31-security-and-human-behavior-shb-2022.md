Title: Security and Human Behavior (SHB) 2022
Date: 2022-05-31T09:12:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-05-31-security-and-human-behavior-shb-2022

[Source](https://www.schneier.com/blog/archives/2022/05/security-and-human-behavior-shb-2022.html){:target="_blank" rel="noopener"}

> Today is the second day of the fifteenth Workshop on Security and Human Behavior, hosted by Ross Anderson and Alice Hutchings at the University of Cambridge. After two years of having this conference remotely on Zoom, it’s nice to be back together in person. SHB is a small, annual, invitational workshop of people studying various aspects of the human side of security, organized each year by Alessandro Acquisti, Ross Anderson, Alice Hutchings, and myself. The forty or so attendees include psychologists, economists, computer security researchers, sociologists, political scientists, criminologists, neuroscientists, designers, lawyers, philosophers, anthropologists, geographers, business school professors, and a [...]
