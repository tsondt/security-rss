Title: SSE kicks the ‘A’ out of SASE
Date: 2022-05-02T19:00:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-02-sse-kicks-the-a-out-of-sase

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/02/see-vs-sase/){:target="_blank" rel="noopener"}

> Security Service Edge separates cloud-delivered defenses from SD-WAN as debate rages Analysis The emergence of secure access service edge (SASE) dominated the networking market for the last few years as enterprises sought to address increasingly distributed IT environments.... [...]
