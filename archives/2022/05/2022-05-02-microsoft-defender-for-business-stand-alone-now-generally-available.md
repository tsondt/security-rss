Title: Microsoft Defender for Business stand-alone now generally available
Date: 2022-05-02T14:34:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-05-02-microsoft-defender-for-business-stand-alone-now-generally-available

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-for-business-stand-alone-now-generally-available/){:target="_blank" rel="noopener"}

> Microsoft says that its enterprise-grade endpoint security for small to medium-sized businesses is now generally available. [...]
