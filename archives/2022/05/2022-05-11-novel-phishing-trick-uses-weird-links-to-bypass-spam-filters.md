Title: Novel Phishing Trick Uses Weird Links to Bypass Spam Filters
Date: 2022-05-11T12:13:51+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Hacks
Slug: 2022-05-11-novel-phishing-trick-uses-weird-links-to-bypass-spam-filters

[Source](https://threatpost.com/novel-phishing-trick-uses-weird-links-to-bypass-spam-filters/179587/){:target="_blank" rel="noopener"}

> A novel form of phishing takes advantage of a disparity between how browsers and email inboxes read web domains. [...]
