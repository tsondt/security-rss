Title: Information security gets personal: How to protect yourself and your stuff
Date: 2022-05-27T17:23:42+00:00
Author: Lee Hutchinson
Category: Ars Technica
Tags: Ars Technica Videos;Biz & IT;ars frontiers;frontiers-recap;infosec
Slug: 2022-05-27-information-security-gets-personal-how-to-protect-yourself-and-your-stuff

[Source](https://arstechnica.com/?p=1856854){:target="_blank" rel="noopener"}

> Redefining privacy at Ars Frontiers. Click here for transcript. (video link) At the Ars Frontiers event in Washington, DC, I had the privilege of moderating two panels on two closely linked topics: digital privacy and information security. Despite significant attempts to improve things, conflicting priorities and inadequate policy have weakened both privacy and security. Some of the same fundamental issues underly the weaknesses in both: Digital privacy and information security are still too demanding for average people to manage, let alone master. Our privacy panel consisted of Electronic Frontier Foundation deputy executive Kurt Opsahl, security researcher Runa Sandvik, and ACLU [...]
