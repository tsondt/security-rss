Title: US charges hacker for breaching brokerage accounts, securities fraud
Date: 2022-05-11T15:51:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-05-11-us-charges-hacker-for-breaching-brokerage-accounts-securities-fraud

[Source](https://www.bleepingcomputer.com/news/security/us-charges-hacker-for-breaching-brokerage-accounts-securities-fraud/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DoJ) has charged Idris Dayo Mustapha for a range of cybercrime activities that took place between 2011 and 2018, resulting in financial losses estimated to over $5,000,000. [...]
