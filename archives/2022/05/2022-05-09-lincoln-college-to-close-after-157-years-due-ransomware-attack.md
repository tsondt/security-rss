Title: Lincoln College to close after 157 years due ransomware attack
Date: 2022-05-09T18:17:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-09-lincoln-college-to-close-after-157-years-due-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/lincoln-college-to-close-after-157-years-due-ransomware-attack/){:target="_blank" rel="noopener"}

> Lincoln College, a liberal-arts school from rural Illinois, says it will close its doors later this month, 157 years since it was founded and following a hard hit on its finances after the COVID-19 pandemic and a recent ransomware attack. [...]
