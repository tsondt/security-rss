Title: New XLoader botnet uses probability theory to hide its servers
Date: 2022-05-31T11:45:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-31-new-xloader-botnet-uses-probability-theory-to-hide-its-servers

[Source](https://www.bleepingcomputer.com/news/security/new-xloader-botnet-uses-probability-theory-to-hide-its-servers/){:target="_blank" rel="noopener"}

> Threat analysts have spotted a new version of the XLoader botnet malware that uses probability theory to hide its command and control servers, making it difficult to disrupt the malware's operation. [...]
