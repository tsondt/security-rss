Title: GitHub: Attackers stole login details of 100K npm user accounts
Date: 2022-05-27T14:40:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-27-github-attackers-stole-login-details-of-100k-npm-user-accounts

[Source](https://www.bleepingcomputer.com/news/security/github-attackers-stole-login-details-of-100k-npm-user-accounts/){:target="_blank" rel="noopener"}

> GitHub revealed today that an attacker stole the login details of roughly 100,000 npm accounts during a mid-April security breach with the help of stolen OAuth app tokens issued to Heroku and Travis-CI. [...]
