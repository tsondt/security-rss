Title: Microsoft fixes new PetitPotam Windows NTLM Relay attack vector
Date: 2022-05-14T15:39:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-14-microsoft-fixes-new-petitpotam-windows-ntlm-relay-attack-vector

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-new-petitpotam-windows-ntlm-relay-attack-vector/){:target="_blank" rel="noopener"}

> A recent security update for a Windows NTLM Relay Attack has been confirmed to be a previously unfixed vector for the PetitPotam attack. [...]
