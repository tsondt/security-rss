Title: State of internet crime in Q1 2022: Bot traffic on the rise, and more
Date: 2022-05-18T09:00:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-18-state-of-internet-crime-in-q1-2022-bot-traffic-on-the-rise-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/18/fraud_economy_booms/){:target="_blank" rel="noopener"}

> According to this cybersecurity outfit that wants your business, anyway The fraud industry, in some respects, grew in the first quarter of the year, with crooks putting more human resources into some attacks while increasingly relying on bots to carry out things like credential stuffing and fake account creation.... [...]
