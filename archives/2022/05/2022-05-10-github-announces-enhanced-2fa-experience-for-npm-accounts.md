Title: GitHub announces enhanced 2FA experience for npm accounts
Date: 2022-05-10T15:48:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-10-github-announces-enhanced-2fa-experience-for-npm-accounts

[Source](https://www.bleepingcomputer.com/news/security/github-announces-enhanced-2fa-experience-for-npm-accounts/){:target="_blank" rel="noopener"}

> Today, GitHub has launched a new public beta to notably improve the two-factor authentication (2FA) experience for all npm user accounts. [...]
