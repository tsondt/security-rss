Title: NIST refreshes software supply chain risk management guidance
Date: 2022-05-11T10:56:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-11-nist-refreshes-software-supply-chain-risk-management-guidance

[Source](https://portswigger.net/daily-swig/nist-refreshes-software-supply-chain-risk-management-guidance){:target="_blank" rel="noopener"}

> ‘A comprehensive tool that can take you from crawl to walk to run’ [...]
