Title: Low-rent RAT Worries Researchers
Date: 2022-05-10T00:24:18+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Breach;Malware
Slug: 2022-05-10-low-rent-rat-worries-researchers

[Source](https://threatpost.com/low-rent-rat-worries-researchers/179553/){:target="_blank" rel="noopener"}

> Researchers say a hacker is selling access to quality malware for chump change. [...]
