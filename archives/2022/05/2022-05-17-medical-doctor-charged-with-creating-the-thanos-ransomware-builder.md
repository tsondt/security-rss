Title: Medical doctor charged with creating the Thanos ransomware builder
Date: 2022-05-17T14:12:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-17-medical-doctor-charged-with-creating-the-thanos-ransomware-builder

[Source](https://portswigger.net/daily-swig/medical-doctor-charged-with-creating-the-thanos-ransomware-builder){:target="_blank" rel="noopener"}

> Venezuelan cardiologist allegedly tied to cybercrime scams through multiple OpSec mistakes [...]
