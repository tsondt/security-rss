Title: US recovers $15 million from global Kovter ad fraud operation
Date: 2022-05-18T14:37:51-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-18-us-recovers-15-million-from-global-kovter-ad-fraud-operation

[Source](https://www.bleepingcomputer.com/news/security/us-recovers-15-million-from-global-kovter-ad-fraud-operation/){:target="_blank" rel="noopener"}

> The US government has recovered over $15 million from Swiss bank accounts belonging to operators behind the '3ve' online advertising fraud scheme. [...]
