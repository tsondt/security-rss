Title: HP fixes bug letting attackers overwrite firmware in over 200 models
Date: 2022-05-11T11:53:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-11-hp-fixes-bug-letting-attackers-overwrite-firmware-in-over-200-models

[Source](https://www.bleepingcomputer.com/news/security/hp-fixes-bug-letting-attackers-overwrite-firmware-in-over-200-models/){:target="_blank" rel="noopener"}

> HP has released BIOS updates today to fix two high-severity vulnerabilities affecting a wide range of PC and notebook products, which might allow arbitrary code execution. [...]
