Title: 380K Kubernetes API Servers Exposed to Public Internet
Date: 2022-05-20T11:11:36+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Cloud Security
Slug: 2022-05-20-380k-kubernetes-api-servers-exposed-to-public-internet

[Source](https://threatpost.com/380k-kubernetes-api-servers-exposed-to-public-internet/179679/){:target="_blank" rel="noopener"}

> More than 380,000 of the 450,000-plus servers hosting the open-source container-orchestration engine for managing cloud deployments allow some form of access. [...]
