Title: DHS orders federal agencies to patch VMware bugs within 5 days
Date: 2022-05-18T13:38:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-18-dhs-orders-federal-agencies-to-patch-vmware-bugs-within-5-days

[Source](https://www.bleepingcomputer.com/news/security/dhs-orders-federal-agencies-to-patch-vmware-bugs-within-5-days/){:target="_blank" rel="noopener"}

> The Department of Homeland Security's cybersecurity unit ordered Federal Civilian Executive Branch (FCEB) agencies today to urgently update or remove VMware products from their networks by Monday due to an increased risk of attacks. [...]
