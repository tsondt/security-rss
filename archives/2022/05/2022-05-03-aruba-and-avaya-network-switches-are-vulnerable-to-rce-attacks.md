Title: Aruba and Avaya network switches are vulnerable to RCE attacks
Date: 2022-05-03T06:07:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-03-aruba-and-avaya-network-switches-are-vulnerable-to-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/aruba-and-avaya-network-switches-are-vulnerable-to-rce-attacks/){:target="_blank" rel="noopener"}

> Security researchers have discovered five vulnerabilities in network equipment from Aruba (owned by HP) and Avaya (owned by ExtremeNetworks), that could allow malicious actors to execute code remotely on the devices. [...]
