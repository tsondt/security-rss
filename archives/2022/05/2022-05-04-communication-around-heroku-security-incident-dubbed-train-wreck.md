Title: Communication around Heroku security incident dubbed 'train wreck'
Date: 2022-05-04T15:30:42+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-05-04-communication-around-heroku-security-incident-dubbed-train-wreck

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/04/heroku_security_communication_dubbed_complete/){:target="_blank" rel="noopener"}

> Users claim lack of transparency following compromise of Github tokens Efforts by Salesforce-owned cloud platform Heroku to manage a recent security incident are turning into a bit of a disaster, according to some users.... [...]
