Title: Popular Python and PHP libraries hijacked to steal AWS keys
Date: 2022-05-24T07:42:58-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-05-24-popular-python-and-php-libraries-hijacked-to-steal-aws-keys

[Source](https://www.bleepingcomputer.com/news/security/popular-python-and-php-libraries-hijacked-to-steal-aws-keys/){:target="_blank" rel="noopener"}

> PyPI module 'ctx' that gets downloaded over 20,000 times a week has been compromised in a software supply chain attack with malicious versions stealing the developer's environment variables. Additionally, versions of a 'phpass' fork published to the PHP/Composer package repository Packagist had been altered to steal secrets. [...]
