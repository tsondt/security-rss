Title: Malicious PyPI package opens backdoors on Windows, Linux, and Macs
Date: 2022-05-21T11:16:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Linux
Slug: 2022-05-21-malicious-pypi-package-opens-backdoors-on-windows-linux-and-macs

[Source](https://www.bleepingcomputer.com/news/security/malicious-pypi-package-opens-backdoors-on-windows-linux-and-macs/){:target="_blank" rel="noopener"}

> Yet another malicious Python package has been spotted in the PyPI registry performing supply chain attacks to drop Cobalt Strike beacons and backdoors on Windows, Linux, and macOS systems. [...]
