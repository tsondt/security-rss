Title: Path traversal flaw found in OWASP enterprise security testing library
Date: 2022-05-03T15:02:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-03-path-traversal-flaw-found-in-owasp-enterprise-security-testing-library

[Source](https://portswigger.net/daily-swig/path-traversal-flaw-found-in-owasp-enterprise-security-testing-library){:target="_blank" rel="noopener"}

> Difficult-to-exploit ESAPI vulnerability offers best practice lessons [...]
