Title: CIOs largely believe their software supply chain is vulnerable
Date: 2022-05-31T13:00:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-05-31-cios-largely-believe-their-software-supply-chain-is-vulnerable

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/31/cio_supply_chain/){:target="_blank" rel="noopener"}

> Internal beauracy and barriers hold up roll out of defenses, report finds Ask 1,000 CIOs whether they believe their organizations are vulnerable to cyberattacks targeting their software supply chains and about 82 percent can be expected to say yes.... [...]
