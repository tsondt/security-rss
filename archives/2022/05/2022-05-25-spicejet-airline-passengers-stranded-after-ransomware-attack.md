Title: SpiceJet airline passengers stranded after ransomware attack
Date: 2022-05-25T07:43:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-25-spicejet-airline-passengers-stranded-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/spicejet-airline-passengers-stranded-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Indian low-cost airline SpiceJet has informed its customers of an attempted ransomware attack that has impacted some of its systems and caused delays on flight departures today. [...]
