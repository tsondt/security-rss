Title: State Bar of Georgia reels from cyber-attack
Date: 2022-05-04T13:01:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-04-state-bar-of-georgia-reels-from-cyber-attack

[Source](https://portswigger.net/daily-swig/state-bar-of-georgia-reels-from-cyber-attack){:target="_blank" rel="noopener"}

> Bar suspends website after mystery assault [...]
