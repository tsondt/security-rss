Title: Hackers can hack your online accounts before you even register them
Date: 2022-05-23T13:02:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-23-hackers-can-hack-your-online-accounts-before-you-even-register-them

[Source](https://www.bleepingcomputer.com/news/security/hackers-can-hack-your-online-accounts-before-you-even-register-them/){:target="_blank" rel="noopener"}

> Security researchers have revealed that hackers can hijack your online accounts before you even register them by exploiting flaws that have been already been fixed on popular websites, including Instagram, LinkedIn, Zoom, WordPress, and Dropbox. [...]
