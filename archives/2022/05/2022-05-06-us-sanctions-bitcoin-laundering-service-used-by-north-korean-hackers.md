Title: US sanctions Bitcoin laundering service used by North Korean hackers
Date: 2022-05-06T11:17:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-06-us-sanctions-bitcoin-laundering-service-used-by-north-korean-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-bitcoin-laundering-service-used-by-north-korean-hackers/){:target="_blank" rel="noopener"}

> The US Department of Treasury today sanctioned cryptocurrency mixer Blender.io used last month by the North Korean-backed Lazarus hacking group to launder funds stolen from Axie Infinity's Ronin bridge. [...]
