Title: A YouTuber is promoting DDoS attacks on Russia — how legal is this?
Date: 2022-05-01T10:11:16-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-05-01-a-youtuber-is-promoting-ddos-attacks-on-russia-how-legal-is-this

[Source](https://www.bleepingcomputer.com/news/security/a-youtuber-is-promoting-ddos-attacks-on-russia-how-legal-is-this/){:target="_blank" rel="noopener"}

> A YouTube influencer with hundreds of thousands of subscribers is encouraging everyone to conduct cyber warfare against Russia. How risky is it and can you get in trouble? [...]
