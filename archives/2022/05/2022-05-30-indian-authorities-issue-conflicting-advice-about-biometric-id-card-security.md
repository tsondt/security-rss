Title: Indian authorities issue conflicting advice about biometric ID card security
Date: 2022-05-30T05:58:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-30-indian-authorities-issue-conflicting-advice-about-biometric-id-card-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/30/indian_authorities_conflicting_aadhaar_advice/){:target="_blank" rel="noopener"}

> Government authority forced to backtrack warning that photocopied Aadhaar cards represent a risk The Unique Identification Authority of India (UIDAI) has backtracked on advice about how best to secure the "Aadhaar" national identity cards that enable access to a range of government and financial serivces.... [...]
