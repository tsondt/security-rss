Title: Microsoft finds severe bugs in Android apps from large mobile providers
Date: 2022-05-27T13:06:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-05-27-microsoft-finds-severe-bugs-in-android-apps-from-large-mobile-providers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-finds-severe-bugs-in-android-apps-from-large-mobile-providers/){:target="_blank" rel="noopener"}

> Microsoft security researchers have found high severity vulnerabilities in a framework used by Android apps from multiple large international mobile service providers. [...]
