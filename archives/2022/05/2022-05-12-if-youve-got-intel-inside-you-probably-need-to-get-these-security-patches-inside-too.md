Title: If you've got Intel inside, you probably need to get these security patches inside, too
Date: 2022-05-12T21:06:29+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-12-if-youve-got-intel-inside-you-probably-need-to-get-these-security-patches-inside-too

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/12/intel_product_bugs/){:target="_blank" rel="noopener"}

> So. Many. BIOS. Bugs Intel has disclosed high-severity bugs in its firmware that's used in datacenter servers, workstations, mobile devices, storage products, and other gear. These flaws can be exploited to escalate privileges, leak information, or stop things from working.... [...]
