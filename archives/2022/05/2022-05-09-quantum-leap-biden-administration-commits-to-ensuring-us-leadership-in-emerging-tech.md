Title: Quantum leap: Biden administration commits to ensuring US leadership in emerging tech
Date: 2022-05-09T13:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-09-quantum-leap-biden-administration-commits-to-ensuring-us-leadership-in-emerging-tech

[Source](https://portswigger.net/daily-swig/quantum-leap-biden-administration-commits-to-ensuring-us-leadership-in-emerging-tech){:target="_blank" rel="noopener"}

> Government sets out plan for post-quantum encryption [...]
