Title: APTs Overwhelmingly Share Known Vulnerabilities Rather Than Attack O-Days
Date: 2022-05-18T14:01:22+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks
Slug: 2022-05-18-apts-overwhelmingly-share-known-vulnerabilities-rather-than-attack-o-days

[Source](https://threatpost.com/apts-overwhelmingly-share-known-vulnerabilities-rather-than-attack-o-days/179657/){:target="_blank" rel="noopener"}

> Research indicates that organizations should make patching existing flaws a priority to mitigate risk of compromise. [...]
