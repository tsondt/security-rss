Title: How Google Cloud monitors its Quality Management System
Date: 2022-05-25T19:00:00+00:00
Author: Rani Urbas
Category: GCP Security
Tags: Compliance;Google Cloud;Identity & Security
Slug: 2022-05-25-how-google-cloud-monitors-its-quality-management-system

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-monitors-its-quality-management-system/){:target="_blank" rel="noopener"}

> As a provider of software and services for global enterprises, Google Cloud understands that the quality and security of products is instrumental in maintaining trust among our customers. We are committed to providing products and services that help our customers meet their quality management objectives, ultimately helping organizations to meet their regulatory and customer requirements. At the heart of this commitment is our robust quality management system (QMS), a process-based approach that aims to achieve high standards of quality in all stages of the product or service lifecycle and which leverages our ISO 9001:2015 certification. In our new Quality Management [...]
