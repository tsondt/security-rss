Title: Russian hackers perform reconnaissance against Austria, Estonia
Date: 2022-05-23T09:14:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-23-russian-hackers-perform-reconnaissance-against-austria-estonia

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-perform-reconnaissance-against-austria-estonia/){:target="_blank" rel="noopener"}

> In a new reconnaissance campaign, the Russian state-sponsored hacking group Turla was observed targeting the Austrian Economic Chamber, a NATO platform, and the Baltic Defense College. [...]
