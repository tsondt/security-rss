Title: US, Europe formally blame Russia for data wiper attacks against Ukraine, Viasat
Date: 2022-05-10T20:58:44+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-10-us-europe-formally-blame-russia-for-data-wiper-attacks-against-ukraine-viasat

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/10/us_eu_russia/){:target="_blank" rel="noopener"}

> Thank goodness someone cleared that one up The US and the European Union have officially blamed Russia for a series of destructive data-wiping malware infections in Ukrainian government and private-sector networks – and said they will "take steps" to defend against and respond to Kremlin-orchestrated attacks.... [...]
