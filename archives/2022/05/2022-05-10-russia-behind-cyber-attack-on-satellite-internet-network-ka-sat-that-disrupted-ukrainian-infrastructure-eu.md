Title: Russia behind cyber-attack on satellite internet network KA-SAT that disrupted Ukrainian infrastructure – EU
Date: 2022-05-10T12:52:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-10-russia-behind-cyber-attack-on-satellite-internet-network-ka-sat-that-disrupted-ukrainian-infrastructure-eu

[Source](https://portswigger.net/daily-swig/russia-behind-cyber-attack-on-satellite-internet-network-ka-sat-that-disrupted-ukrainian-infrastructure-eu){:target="_blank" rel="noopener"}

> Suspected DDoS attack took place one hour before Russia invaded Ukraine [...]
