Title: Build a strong identity foundation that uses your existing on-premises Active Directory
Date: 2022-05-12T15:50:18+00:00
Author: Michael Miller
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Active Directory;authentication;AWS SSO;Federation;IAM
Slug: 2022-05-12-build-a-strong-identity-foundation-that-uses-your-existing-on-premises-active-directory

[Source](https://aws.amazon.com/blogs/security/build-a-strong-identity-foundation-that-uses-your-existing-on-premises-active-directory/){:target="_blank" rel="noopener"}

> This blog post outlines how to use your existing Microsoft Active Directory (AD) to reliably authenticate access to your Amazon Web Services (AWS) accounts, infrastructure running on AWS, and third-party applications. The architecture we describe is designed to be highly available and extends access to your existing AD to AWS, enabling your users to use [...]
