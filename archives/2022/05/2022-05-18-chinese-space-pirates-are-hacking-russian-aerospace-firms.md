Title: Chinese ‘Space Pirates’ are hacking Russian aerospace firms
Date: 2022-05-18T12:51:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-18-chinese-space-pirates-are-hacking-russian-aerospace-firms

[Source](https://www.bleepingcomputer.com/news/security/chinese-space-pirates-are-hacking-russian-aerospace-firms/){:target="_blank" rel="noopener"}

> A previously unknown Chinese hacking group known as 'Space Pirates' targets enterprises in the Russian aerospace industry with phishing emails to install novel malware on their systems. [...]
