Title: Russia hammered by pro-Ukrainian hackers following invasion
Date: 2022-05-06T14:30:44+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;Cyberattacks;hacking;russia;Ukraine invasion
Slug: 2022-05-06-russia-hammered-by-pro-ukrainian-hackers-following-invasion

[Source](https://arstechnica.com/?p=1852670){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) For years, Dmitriy Sergeyevich Badin sat atop the FBI’s most-wanted list. The Russian government-backed hacker has been suspected of cyberattacks on Germany’s Bundestag and the 2016 Olympics, held in Rio de Janeiro. A few weeks into Russia’s invasion of Ukraine, his own personal information—including his email and Facebook accounts and passwords, mobile phone number, and even passport details—was leaked online. Another target since the war broke out two months ago has been the All-Russia State Television and Radio Broadcasting Company, known as a voice of the Kremlin and home to Vladimir Solovyov, whose daily TV show [...]
