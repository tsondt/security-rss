Title: iPhones Vulnerable to Attack Even When Turned Off
Date: 2022-05-17T13:19:40+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Vulnerabilities
Slug: 2022-05-17-iphones-vulnerable-to-attack-even-when-turned-off

[Source](https://threatpost.com/iphones-attack-turned-off/179641/){:target="_blank" rel="noopener"}

> Wireless features Bluetooth, NFC and UWB stay on even when the device is powered down, which could allow attackers to execute pre-loaded malware. [...]
