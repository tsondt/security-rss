Title: Costa Rica declares national emergency after Conti ransomware attacks
Date: 2022-05-09T03:53:28-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-05-09-costa-rica-declares-national-emergency-after-conti-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/costa-rica-declares-national-emergency-after-conti-ransomware-attacks/){:target="_blank" rel="noopener"}

> The Costa Rican President Rodrigo Chaves has declared a national emergency following cyber attacks from Conti ransomware group. BleepingComputer also observed Conti published most of the 672 GB dump that appears to contain data belonging to the Costa Rican government agencies. [...]
