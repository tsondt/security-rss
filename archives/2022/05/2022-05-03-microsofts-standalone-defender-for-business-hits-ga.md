Title: Microsoft's standalone Defender for Business hits GA
Date: 2022-05-03T14:00:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-05-03-microsofts-standalone-defender-for-business-hits-ga

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/03/microsoft_defender_standalone/){:target="_blank" rel="noopener"}

> Security suite for the orgs unwilling to stump up for a Microsoft 365 Business Premium subscription Microsoft has made a standalone version of Microsoft Defender for Business generally available, aimed at customers not keen on paying for one of its subscriptions.... [...]
