Title: Microsoft Patch Tuesday, May 2022 Edition
Date: 2022-05-11T02:34:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;adobe;CVE-2022-26925;CVE-2022-26937;Dustin Childs;Greg Wiseman;Local Security Authortity;Microsoft Patch Tuesday May 2022;PetitPotam;Rapid7;Satnam Narang;Tenable;Trend Micro Zero Day Initiative;Windows Print Spooler
Slug: 2022-05-11-microsoft-patch-tuesday-may-2022-edition

[Source](https://krebsonsecurity.com/2022/05/microsoft-patch-tuesday-may-2022-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to fix at least 74 separate security problems in its Windows operating systems and related software. This month’s patch batch includes fixes for seven “critical” flaws, as well as a zero-day vulnerability that affects all supported versions of Windows. By all accounts, the most urgent bug Microsoft addressed this month is CVE-2022-26925, a weakness in a central component of Windows security (the “ Local Security Authority ” process within Windows). CVE-2022-26925 was publicly disclosed prior to today, and Microsoft says it is now actively being exploited in the wild. The flaw affects Windows 7 through 10 [...]
