Title: LinkedIn bug bounty program goes public with rewards of up to $18k
Date: 2022-05-26T15:26:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-26-linkedin-bug-bounty-program-goes-public-with-rewards-of-up-to-18k

[Source](https://portswigger.net/daily-swig/linkedin-bug-bounty-program-goes-public-with-rewards-of-up-to-18k){:target="_blank" rel="noopener"}

> Social media platform ends private program after paying $250,000 in rewards over eight years [...]
