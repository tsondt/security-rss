Title: New ‘Cheers’ Linux ransomware targets VMware ESXi servers
Date: 2022-05-25T15:25:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-25-new-cheers-linux-ransomware-targets-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/new-cheers-linux-ransomware-targets-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> A new ransomware named 'Cheers' has appeared in the cybercrime space and has started its operations by targeting vulnerable VMware ESXi servers. [...]
