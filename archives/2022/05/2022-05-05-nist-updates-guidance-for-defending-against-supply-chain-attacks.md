Title: NIST updates guidance for defending against supply-chain attacks
Date: 2022-05-05T14:15:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-05-nist-updates-guidance-for-defending-against-supply-chain-attacks

[Source](https://www.bleepingcomputer.com/news/security/nist-updates-guidance-for-defending-against-supply-chain-attacks/){:target="_blank" rel="noopener"}

> The National Institute of Standards and Technology (NIST) has released updated guidance on securing the supply chain against cyberattacks. [...]
