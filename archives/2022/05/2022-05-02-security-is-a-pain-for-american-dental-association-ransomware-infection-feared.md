Title: Security is a pain for American Dental Association: Ransomware infection feared
Date: 2022-05-02T19:50:32+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-02-security-is-a-pain-for-american-dental-association-ransomware-infection-feared

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/02/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Another university hit with malware, and more In brief The Black Basta crime gang has claimed it infected the American Dental Association with ransomware.... [...]
