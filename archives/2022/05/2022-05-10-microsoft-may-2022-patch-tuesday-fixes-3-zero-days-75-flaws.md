Title: Microsoft May 2022 Patch Tuesday fixes 3 zero-days, 75 flaws
Date: 2022-05-10T13:37:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-05-10-microsoft-may-2022-patch-tuesday-fixes-3-zero-days-75-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-may-2022-patch-tuesday-fixes-3-zero-days-75-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's May 2022 Patch Tuesday, and with it comes fixes for three zero-day vulnerabilities, with one actively exploited, and a total of 75 flaws. [...]
