Title: Fake crypto sites lure wannabe thieves by spamming login credentials
Date: 2022-05-18T10:54:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-05-18-fake-crypto-sites-lure-wannabe-thieves-by-spamming-login-credentials

[Source](https://www.bleepingcomputer.com/news/security/fake-crypto-sites-lure-wannabe-thieves-by-spamming-login-credentials/){:target="_blank" rel="noopener"}

> Threat actors are luring potential thieves by spamming login credentials for other people account's on fake crypto trading sites, illustrating once again, that there is no honor among thieves. [...]
