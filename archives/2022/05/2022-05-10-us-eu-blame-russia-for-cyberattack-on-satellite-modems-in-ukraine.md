Title: US, EU blame Russia for cyberattack on satellite modems in Ukraine
Date: 2022-05-10T09:47:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-10-us-eu-blame-russia-for-cyberattack-on-satellite-modems-in-ukraine

[Source](https://www.bleepingcomputer.com/news/security/us-eu-blame-russia-for-cyberattack-on-satellite-modems-in-ukraine/){:target="_blank" rel="noopener"}

> The European Union formally accused Russia of coordinating the cyberattack that hit satellite Internet modems in Ukraine on February 24, roughly one hour before Russia invaded Ukraine. [...]
