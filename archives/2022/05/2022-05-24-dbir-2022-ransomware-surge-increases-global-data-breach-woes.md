Title: DBIR 2022: Ransomware surge increases global data breach woes
Date: 2022-05-24T16:01:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-24-dbir-2022-ransomware-surge-increases-global-data-breach-woes

[Source](https://portswigger.net/daily-swig/dbir-2022-ransomware-surge-increases-global-data-breach-woes){:target="_blank" rel="noopener"}

> Verizon’s annual security report points to a double-digit rise in ransomware attacks [...]
