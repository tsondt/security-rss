Title: Australian digital driving licenses can be defaced in minutes
Date: 2022-05-30T23:31:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-05-30-australian-digital-driving-licenses-can-be-defaced-in-minutes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/30/nsw_digital_drivers_licenses_hackable/){:target="_blank" rel="noopener"}

> Brute force attack leaves the license wide open for undetectable alteration, but back end data remains unchanged An Australian digital driver's license (DDL) implementation that officials claimed is more secure than a physical license has been shown to easily defaced, but authorities insist the credential remains secure.... [...]
