Title: 'Peacetime in cyberspace is a chaotic environment' says senior US advisor
Date: 2022-05-13T13:24:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-13-peacetime-in-cyberspace-is-a-chaotic-environment-says-senior-us-advisor

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/13/cyberspace_is_first_theatre_of_war/){:target="_blank" rel="noopener"}

> The internet is now the first battleground of any new war – before the shooting starts Black Hat Asia Cyber war has become an emerged aspect of broader armed conflicts, commencing before the first shot is fired, cybersecurity expert Kenneth Geers told the audience at the Black Hat Asia conference on Friday.... [...]
