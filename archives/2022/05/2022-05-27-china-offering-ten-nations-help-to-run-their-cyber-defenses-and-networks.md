Title: China offering ten nations help to run their cyber-defenses and networks
Date: 2022-05-27T03:33:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-27-china-offering-ten-nations-help-to-run-their-cyber-defenses-and-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/27/china_south_pacific_tech_assistance/){:target="_blank" rel="noopener"}

> Sure, they’re small Pacific nations, but they’re in very strategic locations China has begun talking to ten nations in the South Pacific with an offer to help them improve their network infrastructure, cyber security, digital forensics and other capabilities – all with the help of Chinese tech vendors.... [...]
