Title: DevSecOps and cybersecurity skills are top priorities for enterprise IT – report
Date: 2022-05-18T14:11:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-18-devsecops-and-cybersecurity-skills-are-top-priorities-for-enterprise-it-report

[Source](https://portswigger.net/daily-swig/devsecops-and-cybersecurity-skills-are-top-priorities-for-enterprise-it-report){:target="_blank" rel="noopener"}

> Transparency and inter-team collaboration key amid escalating threats and compliance requirements [...]
