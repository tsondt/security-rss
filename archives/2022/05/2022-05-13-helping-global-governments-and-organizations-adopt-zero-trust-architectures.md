Title: Helping global governments and organizations adopt Zero Trust architectures
Date: 2022-05-13T13:00:00+00:00
Author: Dan Prieto
Category: GCP Security
Tags: Public Sector;Identity & Security
Slug: 2022-05-13-helping-global-governments-and-organizations-adopt-zero-trust-architectures

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-helps-governments-and-organizations-adopt-zero-trust/){:target="_blank" rel="noopener"}

> For more than a decade, Google has applied a Zero Trust approach to most aspects of our operations. Zero Trust’s core tenet–that implicit trust in any single component of a complex, interconnected system can create serious security risks–is fundamental to how we operate and build our security architecture. Early in our security journey, we realized that despite our best efforts user credentials would periodically fall into the hands of malicious actors. This is why we developed the BeyondCorp framework. We needed additional layers of defense against unauthorized access that would not impede user productivity. We also understood that software that [...]
