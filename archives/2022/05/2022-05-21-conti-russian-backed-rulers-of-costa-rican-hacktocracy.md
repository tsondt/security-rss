Title: Conti: Russian-backed rulers of Costa Rican hacktocracy?
Date: 2022-05-21T11:01:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-05-21-conti-russian-backed-rulers-of-costa-rican-hacktocracy

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/21/in_brief_security/){:target="_blank" rel="noopener"}

> Also, Chinese IT admin jailed for deleting database, and the NSA promises no more backdoors In brief The notorious Russian-aligned Conti ransomware gang has upped the ante in its attack against Costa Rica, threatening to overthrow the government if it doesn't pay a $20 million ransom.... [...]
