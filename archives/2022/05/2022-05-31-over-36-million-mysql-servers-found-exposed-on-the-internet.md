Title: Over 3.6 million MySQL servers found exposed on the Internet
Date: 2022-05-31T16:02:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-31-over-36-million-mysql-servers-found-exposed-on-the-internet

[Source](https://www.bleepingcomputer.com/news/security/over-36-million-mysql-servers-found-exposed-on-the-internet/){:target="_blank" rel="noopener"}

> ​Over 3.6 million MySQL servers are publicly exposed on the Internet and responding to queries, making them an attractive target to hackers and extortionists. [...]
