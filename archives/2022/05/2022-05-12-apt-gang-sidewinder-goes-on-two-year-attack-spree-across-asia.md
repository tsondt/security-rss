Title: APT gang 'Sidewinder' goes on two-year attack spree across Asia
Date: 2022-05-12T08:04:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-12-apt-gang-sidewinder-goes-on-two-year-attack-spree-across-asia

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/12/sidewinder_apt_attack_spree/){:target="_blank" rel="noopener"}

> Launches almost 1,000 raids, plenty with upgraded malware Black Hat Asia The advanced persistent threat gang known as SideWinder has gone on an attack spree in the last two years, conducting almost 1,000 raids and deploying increasingly sophisticated attack methods.... [...]
