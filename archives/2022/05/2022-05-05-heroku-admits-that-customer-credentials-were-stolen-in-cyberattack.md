Title: Heroku admits that customer credentials were stolen in cyberattack
Date: 2022-05-05T04:06:50-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-05-05-heroku-admits-that-customer-credentials-were-stolen-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/heroku-admits-that-customer-credentials-were-stolen-in-cyberattack/){:target="_blank" rel="noopener"}

> Heroku has now revealed that the stolen GitHub integration OAuth tokens from last month further led to the compromise of an internal customer database. The Salesforce-owned cloud platform acknowledged the same compromised token was used by attackers to exfiltrate customers' hashed and salted passwords from "a database." [...]
