Title: In record year for vulnerabilities, Microsoft actually had fewer
Date: 2022-05-25T16:11:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-05-25-in-record-year-for-vulnerabilities-microsoft-actually-had-fewer

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/25/microsoft_vulnerabilities_2021/){:target="_blank" rel="noopener"}

> Occasional gaping hole and overprivileged users still blight the Beast of Redmond Despite a record number of publicly disclosed security flaws in 2021, Microsoft managed to improve its stats, according to research from BeyondTrust.... [...]
