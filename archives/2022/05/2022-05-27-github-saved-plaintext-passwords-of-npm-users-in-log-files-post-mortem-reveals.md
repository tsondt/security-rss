Title: GitHub saved plaintext passwords of npm users in log files, post mortem reveals
Date: 2022-05-27T12:15:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-05-27-github-saved-plaintext-passwords-of-npm-users-in-log-files-post-mortem-reveals

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/27/github_publishes_a_post_mortem/){:target="_blank" rel="noopener"}

> Unrelated to the OAuth token attack, but still troubling as org reveals details of around 100,000 users were grabbed by the baddies GitHub has revealed it stored a "number of plaintext user credentials for the npm registry" in internal logs following the integration of the JavaScript package registry into GitHub's logging systems.... [...]
