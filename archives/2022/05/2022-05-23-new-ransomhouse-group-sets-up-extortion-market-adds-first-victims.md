Title: New RansomHouse group sets up extortion market, adds first victims
Date: 2022-05-23T12:26:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-23-new-ransomhouse-group-sets-up-extortion-market-adds-first-victims

[Source](https://www.bleepingcomputer.com/news/security/new-ransomhouse-group-sets-up-extortion-market-adds-first-victims/){:target="_blank" rel="noopener"}

> Yet another data-extortion cybercrime operation has appeared on the darknet named 'RansomHouse' where threat actors publish evidence of stolen files and leak data of organizations that refuse to make a ransom payment. [...]
