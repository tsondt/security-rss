Title: Why do hackers keep coming back to attack you? Because they can
Date: 2022-05-24T17:15:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-05-24-why-do-hackers-keep-coming-back-to-attack-you-because-they-can

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/24/why_do_hackers_keep_coming/){:target="_blank" rel="noopener"}

> Here’s why relying on manual tooling is like putting your hands up Webinar Hackers have a tendency to return to the scene of their crimes over and over again. But it’s not because they’re unimaginative creatures of habit. It’s because infosec teams make it so easy for them, they’d be foolish not to.... [...]
