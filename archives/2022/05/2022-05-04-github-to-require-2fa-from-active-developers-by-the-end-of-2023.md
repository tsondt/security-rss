Title: GitHub to require 2FA from active developers by the end of 2023
Date: 2022-05-04T11:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-04-github-to-require-2fa-from-active-developers-by-the-end-of-2023

[Source](https://www.bleepingcomputer.com/news/security/github-to-require-2fa-from-active-developers-by-the-end-of-2023/){:target="_blank" rel="noopener"}

> GitHub announced today that all users who contribute code on its platform (an estimated 83 million developers in total) will be required to enable two-factor authentication (2FA) on their accounts by the end of 2023. [...]
