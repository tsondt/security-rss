Title: Russia to Rent Tech-Savvy Prisoners to Corporate IT?
Date: 2022-05-02T21:29:34+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Russia's War on Ukraine
Slug: 2022-05-02-russia-to-rent-tech-savvy-prisoners-to-corporate-it

[Source](https://krebsonsecurity.com/2022/05/russia-to-rent-tech-savvy-prisoners-to-corporate-it/){:target="_blank" rel="noopener"}

> Image: Proxima Studios, via Shutterstock. Faced with a brain drain of smart people fleeing the country following its invasion of Ukraine, the Russian Federation is floating a new strategy to address a worsening shortage of qualified information technology experts: Forcing tech-savvy people within the nation’s prison population to perform low-cost IT work for domestic companies. Multiple Russian news outlets published stories on April 27 saying the Russian Federal Penitentiary Service had announced a plan to recruit IT specialists from Russian prisons to work remotely for domestic commercial companies. Russians sentenced to forced labor will serve out their time at one [...]
