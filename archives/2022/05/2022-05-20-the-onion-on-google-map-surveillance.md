Title: The Onion on Google Map Surveillance
Date: 2022-05-20T19:05:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-05-20-the-onion-on-google-map-surveillance

[Source](https://www.schneier.com/blog/archives/2022/05/the-onion-on-google-map-surveillance.html){:target="_blank" rel="noopener"}

> “ Google Maps Adds Shortcuts through Houses of People Google Knows Aren’t Home Right Now.” Excellent satire. [...]
