Title: Colonial Pipeline faces nearly $1m fine one year after ransomware attack
Date: 2022-05-09T12:15:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-09-colonial-pipeline-faces-nearly-1m-fine-one-year-after-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/09/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Unpatched DNS bug puts IoT devices at risk, SolarWinds hackers set up new digs, and a CEO faces hard time for massive mining fraud In Brief Colonial Pipeline is facing an almost $1 million fine for control room management failures after the US Department of Transportation alleged they contributed to the nation's fuel disruption in the wake of the 2021 ransomware attack.... [...]
