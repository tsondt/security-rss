Title: Websites that Collect Your Data as You Type
Date: 2022-05-19T11:23:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;data collection;key logging;privacy
Slug: 2022-05-19-websites-that-collect-your-data-as-you-type

[Source](https://www.schneier.com/blog/archives/2022/05/websites-that-collect-your-data-as-you-type.html){:target="_blank" rel="noopener"}

> A surprising number of websites include JavaScript keyloggers that collect everything you type as you type it, not just when you submit a form. Researchers from KU Leuven, Radboud University, and University of Lausanne crawled and analyzed the top 100,000 websites, looking at scenarios in which a user is visiting a site while in the European Union and visiting a site from the United States. They found that 1,844 websites gathered an EU user’s email address without their consent, and a staggering 2,950 logged a US user’s email in some form. Many of the sites seemingly do not intend to [...]
