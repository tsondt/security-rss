Title: China-linked APT Caught Pilfering Treasure Trove of IP
Date: 2022-05-04T17:32:12+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Government;Hacks
Slug: 2022-05-04-china-linked-apt-caught-pilfering-treasure-trove-of-ip

[Source](https://threatpost.com/china-linked-apt-caught-pilfering-treasure-trove-of-ip/179503/){:target="_blank" rel="noopener"}

> A state-sponsored threat actor designed a house-of-cards style infection chain to exfiltrate massive troves of highly sensitive data. [...]
