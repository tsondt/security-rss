Title: ChromeLoader Browser Hijacker Provides Gateway to Bigger Threats
Date: 2022-05-31T11:38:14+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-05-31-chromeloader-browser-hijacker-provides-gateway-to-bigger-threats

[Source](https://threatpost.com/chromeloader-hijacker-threats/179761/){:target="_blank" rel="noopener"}

> The malvertiser’s use of PowerShell could push it beyond its basic capabilities to spread ransomware, spyware or steal data from browser sessions, researchers warn. [...]
