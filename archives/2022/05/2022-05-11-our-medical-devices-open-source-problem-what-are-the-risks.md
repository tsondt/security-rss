Title: Our Medical Devices' Open Source Problem – What Are the Risks?
Date: 2022-05-11T10:01:02-04:00
Author: Sponsored by Cybellum
Category: BleepingComputer
Tags: Security
Slug: 2022-05-11-our-medical-devices-open-source-problem-what-are-the-risks

[Source](https://www.bleepingcomputer.com/news/security/our-medical-devices-open-source-problem-what-are-the-risks/){:target="_blank" rel="noopener"}

> There is no doubt that open source powers our development processes, enabling software developers to build high quality, innovative products faster than ever before. But OSS also comes with its own set of risks that device manufacturers must address while leveraging its many advantages. [...]
