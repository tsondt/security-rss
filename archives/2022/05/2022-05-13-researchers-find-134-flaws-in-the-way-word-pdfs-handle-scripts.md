Title: Researchers find 134 flaws in the way Word, PDFs, handle scripts
Date: 2022-05-13T07:54:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-13-researchers-find-134-flaws-in-the-way-word-pdfs-handle-scripts

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/13/cooperative_mutation_flaw_finder/){:target="_blank" rel="noopener"}

> ‘Cooperative mutation’ spots problems that checking scripts alone will miss Black Hat Asia Security researchers have devised a tool that detects flaws in the way apps like Microsoft Word and Adobe Acrobat process JavaScript, and it's proven so effective they've found 134 bugs – 59 of them considered worthy of a fix by vendors, 33 assigned a CVE number, and 17 producing bug bounty payments totaling $22,000.... [...]
