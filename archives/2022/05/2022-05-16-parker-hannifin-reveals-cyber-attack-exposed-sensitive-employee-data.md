Title: Parker Hannifin reveals cyber-attack exposed sensitive employee data
Date: 2022-05-16T10:48:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-16-parker-hannifin-reveals-cyber-attack-exposed-sensitive-employee-data

[Source](https://portswigger.net/daily-swig/parker-hannifin-reveals-cyber-attack-exposed-sensitive-employee-data){:target="_blank" rel="noopener"}

> Data breach involves Social Security numbers and health insurance data, among other information [...]
