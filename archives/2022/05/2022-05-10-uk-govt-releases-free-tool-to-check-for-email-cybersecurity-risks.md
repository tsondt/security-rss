Title: UK govt releases free tool to check for email cybersecurity risks
Date: 2022-05-10T12:30:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-10-uk-govt-releases-free-tool-to-check-for-email-cybersecurity-risks

[Source](https://www.bleepingcomputer.com/news/security/uk-govt-releases-free-tool-to-check-for-email-cybersecurity-risks/){:target="_blank" rel="noopener"}

> The United Kingdom's National Cyber Security Centre (NCSC) today released a new email security check service to help organizations easily identify vulnerabilities that could allow attackers to spoof emails or can lead to email privacy breaches. [...]
