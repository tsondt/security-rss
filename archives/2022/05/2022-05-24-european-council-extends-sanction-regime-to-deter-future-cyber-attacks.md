Title: European Council extends sanction regime to deter future cyber-attacks
Date: 2022-05-24T12:35:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-24-european-council-extends-sanction-regime-to-deter-future-cyber-attacks

[Source](https://portswigger.net/daily-swig/european-council-extends-sanction-regime-to-deter-future-cyber-attacks){:target="_blank" rel="noopener"}

> Strategy includes travel bans and asset freezing [...]
