Title: FBI warns of North Korean cyberspies posing as foreign IT workers
Date: 2022-05-17T22:58:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-17-fbi-warns-of-north-korean-cyberspies-posing-as-foreign-it-workers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/17/fbi_korea_freelancers/){:target="_blank" rel="noopener"}

> Looking for tech talent? Kim Jong-un's friendly freelancers, at your service Pay close attention to that resume before offering that work contract.... [...]
