Title: South Korean and US presidents gang up on North Korea's cyber-offensives
Date: 2022-05-23T05:25:02+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-23-south-korean-and-us-presidents-gang-up-on-north-koreas-cyber-offensives

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/23/s_korean_and_us_presidents/){:target="_blank" rel="noopener"}

> Less than two weeks into his new gig, Yoon cozies up to Biden as China and DPRK loom US president Biden and South Korea's new president Yoon Suk Yeol have pledged further co-operation in many technologies, including joint efforts to combat North Korea.... [...]
