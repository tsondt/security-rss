Title: Spring 2022 SOC 2 Type I Privacy report now available
Date: 2022-05-24T22:08:37+00:00
Author: Nimesh Ravasa
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS SOC Privacy Report;AWS SOC Reports;Security Blog
Slug: 2022-05-24-spring-2022-soc-2-type-i-privacy-report-now-available

[Source](https://aws.amazon.com/blogs/security/spring-2022-soc-2-type-i-privacy-report-now-available/){:target="_blank" rel="noopener"}

> Your privacy considerations are at the core of our compliance work at Amazon Web Services (AWS), and we are focused on the protection of your content while using AWS services. Our Spring 2022 SOC 2 Type I Privacy report is now available, which provides customers with a third-party attestation of our system and the suitability [...]
