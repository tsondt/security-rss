Title: Ransomware plows through farm machinery giant AGCO
Date: 2022-05-09T14:00:14+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-05-09-ransomware-plows-through-farm-machinery-giant-agco

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/09/farm_machinery_giant_agco_hit/){:target="_blank" rel="noopener"}

> John Deere rival says it may be days or 'potentially longer' before some production facilities are back in action US agricultural machinery maker AGCO is the latest high-profile organization to fall victim to ransomware, which it says affects operations at some of its worldwide production facilities.... [...]
