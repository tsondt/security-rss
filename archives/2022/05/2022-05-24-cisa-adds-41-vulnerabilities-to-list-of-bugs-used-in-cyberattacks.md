Title: CISA adds 41 vulnerabilities to list of bugs used in cyberattacks
Date: 2022-05-24T13:50:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-24-cisa-adds-41-vulnerabilities-to-list-of-bugs-used-in-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-adds-41-vulnerabilities-to-list-of-bugs-used-in-cyberattacks/){:target="_blank" rel="noopener"}

> The Cybersecurity & Infrastructure Security Agency (CISA) has added 41 vulnerabilities to its catalog of known exploited flaws over the past two days, including flaws for the Android kernel and Cisco IOS XR. [...]
