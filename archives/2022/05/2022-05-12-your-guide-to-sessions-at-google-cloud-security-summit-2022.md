Title: Your guide to sessions at Google Cloud Security Summit 2022
Date: 2022-05-12T17:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Google Cloud;Google Cloud in Asia Pacific;Google Cloud in Europe;Identity & Security
Slug: 2022-05-12-your-guide-to-sessions-at-google-cloud-security-summit-2022

[Source](https://cloud.google.com/blog/products/identity-security/your-guide-to-sessions-at-google-cloud-security-summit-2022/){:target="_blank" rel="noopener"}

> Google Cloud Security Summit is just a few days away! We have an exciting agenda with a keynote, demo, and breakout sessions across four tracks - Zero Trust, Secure Software Supply Chain, Ransomware & Emerging Threats, and Cloud Governance & Sovereignty. By attending this summit, you will be the first to learn about new products and advanced capabilities we are announcing from Google Cloud security and discover new ways to define and drive your security strategy and solve your biggest challenges. We hope you’ll join us for the Security Summit digital online event on May 17, 2022, to learn from [...]
