Title: Aligning Your Password Policy enforcement with NIST Guidelines
Date: 2022-05-31T10:06:48-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-05-31-aligning-your-password-policy-enforcement-with-nist-guidelines

[Source](https://www.bleepingcomputer.com/news/security/aligning-your-password-policy-enforcement-with-nist-guidelines/){:target="_blank" rel="noopener"}

> Although most organizations are not required by law to comply with NIST standards, it is usually in an organization's best interest to follow NIST's cybersecurity standards. This is especially true for NIST's password guidelines. [...]
