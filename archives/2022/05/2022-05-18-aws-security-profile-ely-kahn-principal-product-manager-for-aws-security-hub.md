Title: AWS Security Profile: Ely Kahn, Principal Product Manager for AWS Security Hub
Date: 2022-05-18T17:37:04+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;AWS Security Hub;Security Blog
Slug: 2022-05-18-aws-security-profile-ely-kahn-principal-product-manager-for-aws-security-hub

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-ely-kahn-principal-product-manager-for-aws-security-hub/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, I interview some of the humans who work in Amazon Web Services Security and help keep our customers safe and secure. This interview is with Ely Kahn, principal product manager for AWS Security Hub. Security Hub is a cloud security posture management service that performs security best practice checks, [...]
