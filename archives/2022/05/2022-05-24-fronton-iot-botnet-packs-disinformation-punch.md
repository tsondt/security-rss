Title: Fronton IOT Botnet Packs Disinformation Punch
Date: 2022-05-24T13:59:14+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Government;IoT;Malware
Slug: 2022-05-24-fronton-iot-botnet-packs-disinformation-punch

[Source](https://threatpost.com/fronton-botnet-disinformation/179721/){:target="_blank" rel="noopener"}

> Fronton botnet has far more ability than launching DDOS attack, can track social media trends and launch suitable propaganda. [...]
