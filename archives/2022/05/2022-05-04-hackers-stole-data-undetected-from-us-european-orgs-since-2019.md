Title: Hackers stole data undetected from US, European orgs since 2019
Date: 2022-05-04T11:46:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-04-hackers-stole-data-undetected-from-us-european-orgs-since-2019

[Source](https://www.bleepingcomputer.com/news/security/hackers-stole-data-undetected-from-us-european-orgs-since-2019/){:target="_blank" rel="noopener"}

> Cybersecurity analysts have exposed a lengthy operation attributed to the group of Chinese hackers known as "Winnti" and tracked as APT41, which focused on stealing intellectual property assets like patents, copyrights, trademarks, and other types of valuable data. [...]
