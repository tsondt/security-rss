Title: Take the 2022 Accelerate State of DevOps Survey
Date: 2022-05-26T23:00:00+00:00
Author: Claire Peters
Category: GCP Security
Tags: Application Modernization;Identity & Security;Developers & Practitioners;Google Cloud;DevOps & SRE
Slug: 2022-05-26-take-the-2022-accelerate-state-of-devops-survey

[Source](https://cloud.google.com/blog/products/devops-sre/take-the-2022-state-of-devops-survey/){:target="_blank" rel="noopener"}

> The State of DevOps report by Google Cloud and the DORA research team is the largest and longest running research of its kind with inputs from over 32,000 professionals worldwide. It provides an independent view into the practices and capabilities that organizations, irrespective of their size, industry, and region, can employ to drive better performance. Today, Google Cloud and the DORA research team are excited to announce the launch of the 2022 State of DevOps survey. For the 2022 State of DevOps report we will be focusing on a topic that has been top of mind recently: security. As technology [...]
