Title: Microsoft releases fixes for Azure flaw allowing RCE attacks
Date: 2022-05-09T13:42:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-05-09-microsoft-releases-fixes-for-azure-flaw-allowing-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-releases-fixes-for-azure-flaw-allowing-rce-attacks/){:target="_blank" rel="noopener"}

> Microsoft has released security updates to address a security flaw affecting Azure Synapse and Azure Data Factory pipelines that could let attackers execute remote commands across Integration Runtime infrastructure. [...]
