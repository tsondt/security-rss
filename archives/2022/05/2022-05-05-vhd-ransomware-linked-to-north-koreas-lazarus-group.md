Title: VHD Ransomware Linked to North Korea’s Lazarus Group
Date: 2022-05-05T12:20:10+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks
Slug: 2022-05-05-vhd-ransomware-linked-to-north-koreas-lazarus-group

[Source](https://threatpost.com/vhd-ransomware-lazarus-group/179507/){:target="_blank" rel="noopener"}

> Source code and Bitcoin transactions point to the malware, which emerged in March 2020, being the work of APT38, researchers at Trellix said. [...]
