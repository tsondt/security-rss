Title: Costa Rica’s public health agency hit by Hive ransomware
Date: 2022-05-31T13:34:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-31-costa-ricas-public-health-agency-hit-by-hive-ransomware

[Source](https://www.bleepingcomputer.com/news/security/costa-rica-s-public-health-agency-hit-by-hive-ransomware/){:target="_blank" rel="noopener"}

> All computer systems on the network of Costa Rica's public health service (known as Costa Rican Social Security Fund or CCCS) are now offline following a Hive ransomware attack that hit them this morning. [...]
