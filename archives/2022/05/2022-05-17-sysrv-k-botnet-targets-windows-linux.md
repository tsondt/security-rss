Title: Sysrv-K Botnet Targets Windows, Linux
Date: 2022-05-17T13:53:19+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Hacks;Vulnerabilities
Slug: 2022-05-17-sysrv-k-botnet-targets-windows-linux

[Source](https://threatpost.com/sysrv-k-botnet-targets-windows-linux/179646/){:target="_blank" rel="noopener"}

> Microsoft researchers say they are tracking a botnet that is leveraging bugs in the Spring Framework and WordPress plugins. [...]
