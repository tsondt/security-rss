Title: China wants its youth to stop giving livestreamers money
Date: 2022-05-09T09:59:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-09-china-wants-its-youth-to-stop-giving-livestreamers-money

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/09/china_livestreaming/){:target="_blank" rel="noopener"}

> Internet regulator puts a few practices in place – including viewing curfews and bans on tips China's internet regulator, the Cyberspace Administration of China (CAC), has published guidelines that aim to stop minors from giving tips or other forms of payment to livestreamers, watching after 10pm, or livestreaming themselves.... [...]
