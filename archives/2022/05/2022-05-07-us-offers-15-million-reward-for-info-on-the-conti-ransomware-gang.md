Title: US offers $15 million reward for info on the Conti ransomware gang
Date: 2022-05-07T07:00:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-07-us-offers-15-million-reward-for-info-on-the-conti-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/us-offers-15-million-reward-for-info-on-the-conti-ransomware-gang/){:target="_blank" rel="noopener"}

> The US Department of State is offering up to $15 million for information that helps identify and locate leadership and co-conspirators of the infamous Conti ransomware gang. [...]
