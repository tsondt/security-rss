Title: CANs Reinvent LANs for an All-Local World
Date: 2022-05-05T13:00:02+00:00
Author: David Canellos
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-05-05-cans-reinvent-lans-for-an-all-local-world

[Source](https://threatpost.com/cans-reinvent-lans-for-an-all-local-world/179518/){:target="_blank" rel="noopener"}

> A close look at a new type of network, known as a Cloud Area Network. [...]
