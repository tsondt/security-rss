Title: SEC ramps up fight on cryptocurrency fraud by doubling cyber unit
Date: 2022-05-03T15:23:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency;Government
Slug: 2022-05-03-sec-ramps-up-fight-on-cryptocurrency-fraud-by-doubling-cyber-unit

[Source](https://www.bleepingcomputer.com/news/security/sec-ramps-up-fight-on-cryptocurrency-fraud-by-doubling-cyber-unit/){:target="_blank" rel="noopener"}

> The US Securities and Exchange Commission (SEC) announced today that it will almost double the Crypto Assets and Cyber Unit to ramp up the fight against cryptocurrency fraud to protect investors from "cyber-related threats." [...]
