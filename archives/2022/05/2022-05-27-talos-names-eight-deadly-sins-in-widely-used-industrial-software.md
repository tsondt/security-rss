Title: Talos names eight deadly sins in widely used industrial software
Date: 2022-05-27T18:30:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-27-talos-names-eight-deadly-sins-in-widely-used-industrial-software

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/27/talos-aos-vulnerabilities/){:target="_blank" rel="noopener"}

> Entire swaths of gear relies on vulnerability-laden Open Automation Software (OAS) A researcher at Cisco's Talos threat intelligence team found eight vulnerabilities in the Open Automation Software (OAS) platform that, if exploited, could enable a bad actor to access a device and run code on a targeted system.... [...]
