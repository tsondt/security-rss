Title: What is ISO 27001 and Why it Matters for Compliance Standards
Date: 2022-05-17T10:01:02-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-05-17-what-is-iso-27001-and-why-it-matters-for-compliance-standards

[Source](https://www.bleepingcomputer.com/news/security/what-is-iso-27001-and-why-it-matters-for-compliance-standards/){:target="_blank" rel="noopener"}

> ISO 27001 may seem like a big undertaking, but the certification can pay off in more ways than one—including overlap with compliance regulations. Read about the benefits of ISO 27001 and how to get started. [...]
