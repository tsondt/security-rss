Title: Five Eyes turn spotlight on MSPs: Potential weak links in IT supply-chain security
Date: 2022-05-11T21:44:04+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-11-five-eyes-turn-spotlight-on-msps-potential-weak-links-in-it-supply-chain-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/11/five_eyes_msp/){:target="_blank" rel="noopener"}

> We can think of one thing the S stands for in some unfortunate cases Miscreants are targeting managed service providers (MSPs) to break into their customers' networks and deploy ransomware, steal data, and spy on them, the Five Eyes nations' cybersecurity authorities have formally warned in a joint security alert.... [...]
