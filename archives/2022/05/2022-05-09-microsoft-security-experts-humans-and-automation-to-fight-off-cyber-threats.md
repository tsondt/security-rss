Title: Microsoft Security Experts: Humans and automation to fight off cyber threats
Date: 2022-05-09T13:00:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-05-09-microsoft-security-experts-humans-and-automation-to-fight-off-cyber-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/09/microsoft_security_experts/){:target="_blank" rel="noopener"}

> "We live this fight ourselves everyday," Microsoft says of enterprise attacks Microsoft is rolling out its "Security Experts" managed service with an eye on stomping down threats and malware.... [...]
