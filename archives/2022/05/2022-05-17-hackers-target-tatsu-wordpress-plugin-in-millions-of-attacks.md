Title: Hackers target Tatsu WordPress plugin in millions of attacks
Date: 2022-05-17T07:16:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-17-hackers-target-tatsu-wordpress-plugin-in-millions-of-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-tatsu-wordpress-plugin-in-millions-of-attacks/){:target="_blank" rel="noopener"}

> Hackers are massively exploiting a remote code execution vulnerability, CVE-2021-25094, in the Tatsu Builder plugin for WordPress, which is installed on about 100,000 websites. [...]
