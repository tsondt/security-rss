Title: SharePoint RCE bug resurfaces three months after being patched by Microsoft
Date: 2022-05-16T13:38:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-16-sharepoint-rce-bug-resurfaces-three-months-after-being-patched-by-microsoft

[Source](https://portswigger.net/daily-swig/sharepoint-rce-bug-resurfaces-three-months-after-being-patched-by-microsoft){:target="_blank" rel="noopener"}

> Deserialization vulnerabilities are hard to fix [...]
