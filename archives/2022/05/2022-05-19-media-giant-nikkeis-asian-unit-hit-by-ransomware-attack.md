Title: Media giant Nikkei’s Asian unit hit by ransomware attack
Date: 2022-05-19T14:26:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-19-media-giant-nikkeis-asian-unit-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/media-giant-nikkei-s-asian-unit-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Publishing giant Nikkei disclosed that the group's headquarters in Singapore was hit by a ransomware attack almost one week ago, on May 13th. [...]
