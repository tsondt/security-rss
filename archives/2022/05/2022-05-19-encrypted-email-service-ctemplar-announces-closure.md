Title: Encrypted email service CTemplar announces closure
Date: 2022-05-19T14:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-19-encrypted-email-service-ctemplar-announces-closure

[Source](https://portswigger.net/daily-swig/encrypted-email-service-ctemplar-announces-closure){:target="_blank" rel="noopener"}

> Privacy-focused service to shut down by the end of the month [...]
