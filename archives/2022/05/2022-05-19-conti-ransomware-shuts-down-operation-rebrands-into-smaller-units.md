Title: Conti ransomware shuts down operation, rebrands into smaller units
Date: 2022-05-19T19:32:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-05-19-conti-ransomware-shuts-down-operation-rebrands-into-smaller-units

[Source](https://www.bleepingcomputer.com/news/security/conti-ransomware-shuts-down-operation-rebrands-into-smaller-units/){:target="_blank" rel="noopener"}

> The notorious Conti ransomware gang has officially shut down their operation, with infrastructure taken offline and team leaders told that the brand is no more. [...]
