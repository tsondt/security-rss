Title: Hackers target Russian govt with fake Windows updates pushing RATs
Date: 2022-05-24T15:27:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-24-hackers-target-russian-govt-with-fake-windows-updates-pushing-rats

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-russian-govt-with-fake-windows-updates-pushing-rats/){:target="_blank" rel="noopener"}

> Hackers are targeting Russian government agencies with phishing emails that pretend to be Windows security updates and other lures to install remote access malware. [...]
