Title: India slightly softens infosec incident reporting and data retention rules
Date: 2022-05-20T04:30:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-20-india-slightly-softens-infosec-incident-reporting-and-data-retention-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/20/cert_in_rules_faq/){:target="_blank" rel="noopener"}

> But also makes it plain that offshore entities must comply India has slightly softened its controversial new reporting requirements for information security incidents and made it plain they apply to multinational companies.... [...]
