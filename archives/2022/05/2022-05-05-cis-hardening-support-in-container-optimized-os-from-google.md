Title: CIS hardening support in Container-Optimized OS from Google
Date: 2022-05-05T16:00:00+00:00
Author: Anil Altinay
Category: GCP Security
Tags: Identity & Security;Google Cloud;Infrastructure Modernization
Slug: 2022-05-05-cis-hardening-support-in-container-optimized-os-from-google

[Source](https://cloud.google.com/blog/products/infrastructure-modernization/cis-compliance-support-scanning-in-container-optimized-os/){:target="_blank" rel="noopener"}

> At Google, we follow a security-first philosophy to make safeguarding our clients’ and users' data easier and more scalable, with strong security principles built into multiple layers of Google Cloud. In line with this philosophy, we want to make sure that our Container-Optimized OS adheres to industry-standard security best practices. To this end, we released a CIS benchmark for Container-Optimized OS that codifies the recommendations for hardening and security measures we have been using. Our Container-Optimized OS 97 releases now support CIS Level 1 compliance, with an option to enable support for CIS Level 2 hardening. CIS benchmarks help define [...]
