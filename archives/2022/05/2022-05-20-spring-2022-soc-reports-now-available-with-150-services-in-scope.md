Title: Spring 2022 SOC reports now available with 150 services in scope
Date: 2022-05-20T20:37:25+00:00
Author: Emma Zhang
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS SOC 1;AWS SOC 2;AWS SOC 3;AWS SOC Reports;Security Blog
Slug: 2022-05-20-spring-2022-soc-reports-now-available-with-150-services-in-scope

[Source](https://aws.amazon.com/blogs/security/spring-2022-soc-reports-now-available-with-150-services-in-scope/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we’re committed to providing our customers with continued assurance over the security, availability and confidentiality of the AWS control environment. We’re proud to deliver the Spring 2022 System and Organizational (SOC) 1, 2 and 3 reports, which cover October 1, 2021 to March 31, 2022, to support our AWS customers’ [...]
