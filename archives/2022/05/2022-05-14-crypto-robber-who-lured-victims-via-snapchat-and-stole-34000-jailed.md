Title: Crypto robber who lured victims via Snapchat and stole £34,000 jailed
Date: 2022-05-14T10:02:27-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency;Legal
Slug: 2022-05-14-crypto-robber-who-lured-victims-via-snapchat-and-stole-34000-jailed

[Source](https://www.bleepingcomputer.com/news/security/crypto-robber-who-lured-victims-via-snapchat-and-stole-34-000-jailed/){:target="_blank" rel="noopener"}

> Online crypto scams and ponzi schemes leveraging social media platforms are hardly anything new. But, this gruesome case of a London-based crypto robber transcends the virtual realm and tells a shocking tale of real-life victims from whom the perpetrator successfully stole £34,000. [...]
