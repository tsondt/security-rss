Title: Ransomware grounds some flights at Indian budget airline SpiceJet
Date: 2022-05-26T04:54:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-26-ransomware-grounds-some-flights-at-indian-budget-airline-spicejet

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/26/ransomware_attack_grounds_spicejet/){:target="_blank" rel="noopener"}

> Incident comes a week after 'SAP glitch' kept some planes on the taxiway Indian budget airline SpiceJet on Wednesday attributed delayed flights to a ransomware attack.... [...]
