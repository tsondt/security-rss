Title: Microsoft fixes new NTLM relay zero-day in all Windows versions
Date: 2022-05-10T15:04:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-05-10-microsoft-fixes-new-ntlm-relay-zero-day-in-all-windows-versions

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-new-ntlm-relay-zero-day-in-all-windows-versions/){:target="_blank" rel="noopener"}

> Microsoft has addressed an actively exploited Windows LSA spoofing zero-day that unauthenticated attackers can exploit remotely to force domain controllers to authenticate them via the Windows NT LAN Manager (NTLM) security protocol. [...]
