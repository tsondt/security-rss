Title: Iran, China-linked gangs join Putin's disinformation war online
Date: 2022-05-19T14:00:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-19-iran-china-linked-gangs-join-putins-disinformation-war-online

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/19/iran_china_disinformation_online/){:target="_blank" rel="noopener"}

> They're using the invasion 'to take aim at the usual adversaries,' Mandiant told The Reg Pro-Beijing and Iran miscreants are using the war in Ukraine to spread disinformation that supports these countries' political interests — namely, advancing anti-Western narratives – according to threat-intel experts at Mandiant.... [...]
