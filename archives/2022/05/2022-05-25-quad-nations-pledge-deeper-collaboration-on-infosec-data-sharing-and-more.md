Title: Quad nations pledge deeper collaboration on infosec, data-sharing, and more
Date: 2022-05-25T07:57:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-05-25-quad-nations-pledge-deeper-collaboration-on-infosec-data-sharing-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/25/quad_meeting_tech_agenda/){:target="_blank" rel="noopener"}

> But think tank says its past attempts at working together haven't gone well Leaders of the Quad alliance – Australia, India, Japan, and the USA – met on Tuesday and revealed initiatives to strengthen collaboration on emerging technologies and cybersecurity, with an unspoken subtext of neutralizing China.... [...]
