Title: China-linked Twisted Panda caught spying on Russian defense R&amp;D
Date: 2022-05-20T20:03:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-20-china-linked-twisted-panda-caught-spying-on-russian-defense-rd

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/20/china_twisted_panda/){:target="_blank" rel="noopener"}

> Because Beijing isn't above covert ops to accomplish its five-year goals Chinese cyberspies targeted two Russian defense institutes and possibly another research facility in Belarus, according to Check Point Research.... [...]
