Title: Your Phone May Soon Replace Many of Your Passwords
Date: 2022-05-07T13:31:17+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Security Tools;apple;FIDO Alliance;google;Johannes Ullrich;microsoft;Nicholas Weaver;Sampath Srinivas;SANS;SpyCloud;Steve Bellovin;World Wide Web Consortium
Slug: 2022-05-07-your-phone-may-soon-replace-many-of-your-passwords

[Source](https://krebsonsecurity.com/2022/05/your-phone-may-soon-replace-many-of-your-passwords/){:target="_blank" rel="noopener"}

> Apple, Google and Microsoft announced this week they will soon support an approach to authentication that avoids passwords altogether, and instead requires users to merely unlock their smartphones to sign in to websites or online services. Experts say the changes should help defeat many types of phishing attacks and ease the overall password burden on Internet users, but caution that a true passwordless future may still be years away for most websites. Image: Blog.google The tech giants are part of an industry-led effort to replace passwords, which are easily forgotten, frequently stolen by malware and phishing schemes, or leaked and [...]
