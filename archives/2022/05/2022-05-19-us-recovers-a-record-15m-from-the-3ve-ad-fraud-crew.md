Title: US recovers a record $15m from the 3ve ad-fraud crew
Date: 2022-05-19T20:30:50+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-19-us-recovers-a-record-15m-from-the-3ve-ad-fraud-crew

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/19/us_3ve_fraud/){:target="_blank" rel="noopener"}

> Swiss banks cough up around half of the proceeds of crime The US government has recovered over $15 million in proceeds from the 3ve digital advertising fraud operation that cost businesses more than $29 million for ads that were never viewed.... [...]
