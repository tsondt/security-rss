Title: US won’t prosecute ‘good faith’ security researchers under CFAA
Date: 2022-05-20T00:07:37+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-20-us-wont-prosecute-good-faith-security-researchers-under-cfaa

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/20/cfaa_rule_change/){:target="_blank" rel="noopener"}

> Well, that clears things up? Maybe not. The US Justice Department has directed prosecutors not to charge "good-faith security researchers" with violating the Computer Fraud and Abuse Act (CFAA) if their reasons for hacking are ethical — things like bug hunting, responsible vulnerability disclosure, or above-board penetration testing.... [...]
