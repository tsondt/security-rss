Title: You Can’t Eliminate Cyberattacks, So Focus on Reducing the Blast Radius
Date: 2022-05-12T11:57:42+00:00
Author: Tony Lauro
Category: Threatpost
Tags: InfoSec Insider;Vulnerabilities
Slug: 2022-05-12-you-cant-eliminate-cyberattacks-so-focus-on-reducing-the-blast-radius

[Source](https://threatpost.com/cyberattacks-blast-radius/179612/){:target="_blank" rel="noopener"}

> Tony Lauro, director of security technology and strategy at Akamai, discusses reducing your company's attack surface and the "blast radius" of a potential attack. [...]
