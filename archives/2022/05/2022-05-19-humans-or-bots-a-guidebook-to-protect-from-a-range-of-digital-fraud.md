Title: Humans or bots: a guidebook to protect from a range of digital fraud
Date: 2022-05-19T16:00:00+00:00
Author: Bhanchand Prasad
Category: GCP Security
Tags: Identity & Security;Public Sector
Slug: 2022-05-19-humans-or-bots-a-guidebook-to-protect-from-a-range-of-digital-fraud

[Source](https://cloud.google.com/blog/topics/public-sector/humans-or-bots-guidebook-protect-range-digital-fraud/){:target="_blank" rel="noopener"}

> Download our guidebook to learn how reCAPTCHA Enterprise can help strengthen your website security quickly. Cyber threats on the rise In 2021, the United States saw 40.5% of website attacks committed through “bad bots” on the internet, with 37.2% of government website traffic being bad bots, according to research by cybersecurity firm Imperva 1. As cyber threats increase, government agencies need a way to safely let constituents access digital services. Google Cloud reCAPTCHA Enterprise protects websites by distinguishing between humans and bots. Full-scale implementation of reCAPTCHA Enterprise solution expands on bot detection to protect public sector websites from a broad [...]
