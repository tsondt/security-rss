Title: Your data's auctioned off up to 987 times a day, NGO reports
Date: 2022-05-18T13:35:51+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-05-18-your-datas-auctioned-off-up-to-987-times-a-day-ngo-reports

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/18/advertisers_broadcast_pii_more_than/){:target="_blank" rel="noopener"}

> Irish Council on Civil Liberties said this is first time the scope of real-time bidding is being measured The average American has their personal information shared in an online ad bidding war 747 times a day. For the average EU citizen, that number is 376 times a day. In one year, 178 trillion instances of the same bidding war happen online in the US and EU.... [...]
