Title: Ransomware gangs rely more on weaponizing vulnerabilities
Date: 2022-05-19T05:36:56-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-05-19-ransomware-gangs-rely-more-on-weaponizing-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-rely-more-on-weaponizing-vulnerabilities/){:target="_blank" rel="noopener"}

> Security researchers are warning that external remote access services continue to be the main vector for ransomware gangs to breach company networks. [...]
