Title: Critical vulnerabilities found in 'millions of Aruba and Avaya switches'
Date: 2022-05-03T10:00:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-03-critical-vulnerabilities-found-in-millions-of-aruba-and-avaya-switches

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/03/aruba_avaya_critical_vulns/){:target="_blank" rel="noopener"}

> Airports, hospitals, hotels, and more need to deploy patches for hijack bugs Five critical remote code execution vulnerabilities in millions Aruba and Avaya devices can be exploited by cybercriminals to take full control of network switches commonly used in airports, hospitals, and hotels, according to Armis researchers.... [...]
