Title: Verizon: Ransomware sees biggest jump in five years
Date: 2022-05-26T10:04:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-26-verizon-ransomware-sees-biggest-jump-in-five-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/26/verizon-cybersecurity-report-ransomware/){:target="_blank" rel="noopener"}

> We're only here for DBIRs The cybersecurity landscape continues to expand and evolve rapidly, fueled in large part by the cat-and-mouse game between miscreants trying to get into corporate IT environments and those hired by enterprises and security vendors to keep them out.... [...]
