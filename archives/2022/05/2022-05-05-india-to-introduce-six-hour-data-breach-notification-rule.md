Title: India to introduce six-hour data breach notification rule
Date: 2022-05-05T11:55:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-05-india-to-introduce-six-hour-data-breach-notification-rule

[Source](https://portswigger.net/daily-swig/india-to-introduce-six-hour-data-breach-notification-rule){:target="_blank" rel="noopener"}

> Reporting window is 66 hours shorter than that stipulated under the EU’s GDPR [...]
