Title: HTML attachments remain popular among phishing actors in 2022
Date: 2022-05-16T18:32:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-16-html-attachments-remain-popular-among-phishing-actors-in-2022

[Source](https://www.bleepingcomputer.com/news/security/html-attachments-remain-popular-among-phishing-actors-in-2022/){:target="_blank" rel="noopener"}

> HTML files remain one of the most popular attachments used in phishing attacks for the first four months of 2022, showing that the technique remains effective against antispam engines and works well on the victims themselves. [...]
