Title: Open source 'Package Analysis' tool finds malicious npm, PyPI packages
Date: 2022-05-01T11:42:21-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-05-01-open-source-package-analysis-tool-finds-malicious-npm-pypi-packages

[Source](https://www.bleepingcomputer.com/news/security/open-source-package-analysis-tool-finds-malicious-npm-pypi-packages/){:target="_blank" rel="noopener"}

> The Open Source Security Foundation (OpenSSF), a Linux Foundation-backed initiative has released its first prototype version of the 'Package Analysis' tool that aims to catch and counter malicious attacks on open source registries. the open source tool released on GitHub was able to identify over 200 malicious npm and PyPI packages. [...]
