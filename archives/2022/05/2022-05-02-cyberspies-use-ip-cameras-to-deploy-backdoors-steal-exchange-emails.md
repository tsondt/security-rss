Title: Cyberspies use IP cameras to deploy backdoors, steal Exchange emails
Date: 2022-05-02T13:28:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-02-cyberspies-use-ip-cameras-to-deploy-backdoors-steal-exchange-emails

[Source](https://www.bleepingcomputer.com/news/security/cyberspies-use-ip-cameras-to-deploy-backdoors-steal-exchange-emails/){:target="_blank" rel="noopener"}

> A newly discovered and uncommonly stealthy Advanced Persistent Threat (APT) group is breaching corporate networks to steal Exchange (on-premise and online) emails from employees involved in corporate transactions such as mergers and acquisitions. [...]
