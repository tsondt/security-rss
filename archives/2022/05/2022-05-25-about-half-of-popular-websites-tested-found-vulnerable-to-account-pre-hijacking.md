Title: About half of popular websites tested found vulnerable to account pre-hijacking
Date: 2022-05-25T07:28:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-05-25-about-half-of-popular-websites-tested-found-vulnerable-to-account-pre-hijacking

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/25/web_pre_hijacking/){:target="_blank" rel="noopener"}

> In detail: Ocean's Eleven-grade ruse in which victims' profiles are rigged from the start Two security researchers have identified five related techniques for hijacking internet accounts by preparing them to be commandeered in advance.... [...]
