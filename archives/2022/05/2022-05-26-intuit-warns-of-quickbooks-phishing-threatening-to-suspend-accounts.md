Title: Intuit warns of QuickBooks phishing threatening to suspend accounts
Date: 2022-05-26T17:21:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-26-intuit-warns-of-quickbooks-phishing-threatening-to-suspend-accounts

[Source](https://www.bleepingcomputer.com/news/security/intuit-warns-of-quickbooks-phishing-threatening-to-suspend-accounts/){:target="_blank" rel="noopener"}

> Tax software vendor Intuit has warned that QuickBooks customers are being targeted in an ongoing series of phishing attacks impersonating the company and trying to lure them with fake account suspension warnings. [...]
