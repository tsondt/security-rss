Title: Venezuelan cardiologist charged with designing and selling ransomware
Date: 2022-05-17T05:15:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-17-venezuelan-cardiologist-charged-with-designing-and-selling-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/17/zagala_venezuelan_cardiologist_ransomware_charges/){:target="_blank" rel="noopener"}

> If his surgery was as bad as his opsec, this chap has caused a lot of trouble The US Attorney’s Office has charged a 55-year-old cardiologist with creating and selling ransomware and profiting from revenue-share agreements with criminals who deployed his product.... [...]
