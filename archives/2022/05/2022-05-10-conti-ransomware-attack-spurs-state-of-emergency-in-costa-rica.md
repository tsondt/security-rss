Title: Conti Ransomware Attack Spurs State of Emergency in Costa Rica
Date: 2022-05-10T11:54:03+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware
Slug: 2022-05-10-conti-ransomware-attack-spurs-state-of-emergency-in-costa-rica

[Source](https://threatpost.com/conti-ransomware-attack-emergency-costa-rica/179560/){:target="_blank" rel="noopener"}

> The threat group has leaked data that it claims was stolen in the breach and is promising more government-targeted attacks. [...]
