Title: The Limits of Cyber Operations in Wartime
Date: 2022-05-31T11:06:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cyberwar;Russia;Ukraine
Slug: 2022-05-31-the-limits-of-cyber-operations-in-wartime

[Source](https://www.schneier.com/blog/archives/2022/05/the-limits-of-cyber-operations-in-wartime.html){:target="_blank" rel="noopener"}

> Interesting paper by Lennart Maschmeyer: “ The Subversive Trilemma: Why Cyber Operations Fall Short of Expectations “: Abstract: Although cyber conflict has existed for thirty years, the strategic utility of cyber operations remains unclear. Many expect cyber operations to provide independent utility in both warfare and low-intensity competition. Underlying these expectations are broadly shared assumptions that information technology increases operational effectiveness. But a growing body of research shows how cyber operations tend to fall short of their promise. The reason for this shortfall is their subversive mechanism of action. In theory, subversion provides a way to exert influence at lower [...]
