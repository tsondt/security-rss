Title: How to let builders create IAM resources while improving security and agility for your organization
Date: 2022-05-06T21:07:44+00:00
Author: Jeb Benson
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance
Slug: 2022-05-06-how-to-let-builders-create-iam-resources-while-improving-security-and-agility-for-your-organization

[Source](https://aws.amazon.com/blogs/security/how-to-let-builders-create-iam-resources-while-improving-security-and-agility-for-your-organization/){:target="_blank" rel="noopener"}

> Many organizations restrict permissions to create and manage AWS Identity and Access Management (IAM) resources to a group of privileged users or a central team. This post explains how also granting these permissions to builders, that is the people who are developing, testing, launching, and managing cloud infrastructure, can speed up your development, increase your [...]
