Title: Introducing high-performance Confidential Computing with N2D and C2D VMs
Date: 2022-05-25T13:00:00+00:00
Author: Joanna Young
Category: GCP Security
Tags: Compute;Infrastructure;Partners;Google Cloud;Identity & Security
Slug: 2022-05-25-introducing-high-performance-confidential-computing-with-n2d-and-c2d-vms

[Source](https://cloud.google.com/blog/products/identity-security/introducing-confidential-computing-with-n2d-and-c2d-vms/){:target="_blank" rel="noopener"}

> We’re excited to announce Confidential Computing on the latest Google Compute Engine N2D and C2D Virtual Machines. At Google Cloud, we’re constantly striving to deliver performance improvements and feature enhancements. Last November, we announced the general availability of general-purpose N2D machine types running on 3rd Gen AMD EPYCTM processors. Then, in February, we announced the general availability of compute-optimized C2D machine types running on the same 3rd gen processors. Today, we are excited to announce that both of these new N2D and C2D machine types now offer Confidential Computing. By default, Google Cloud keeps all data encrypted, in-transit between customers [...]
