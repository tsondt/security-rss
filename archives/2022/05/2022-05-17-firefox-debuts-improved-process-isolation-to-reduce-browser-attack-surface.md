Title: Firefox debuts improved process isolation to reduce browser attack surface
Date: 2022-05-17T13:10:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-17-firefox-debuts-improved-process-isolation-to-reduce-browser-attack-surface

[Source](https://portswigger.net/daily-swig/firefox-debuts-improved-process-isolation-to-reduce-browser-attack-surface){:target="_blank" rel="noopener"}

> The goal was Win32k Lockdown – a serious step up in Windows security [...]
