Title: Europe moves closer to stricter cybersecurity standards, reporting regs
Date: 2022-05-17T07:26:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-17-europe-moves-closer-to-stricter-cybersecurity-standards-reporting-regs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/17/europe_nis2_cybersecurity_regulations/){:target="_blank" rel="noopener"}

> More types of biz fall under expanded rules – and fines for those who fall short Europe has moved closer toward new cybersecurity standards and reporting rules following a provisional network and information systems agreement dubbed NIS2 by the European Council and Parliament.... [...]
