Title: Closing the Gap Between Application Security and Observability
Date: 2022-05-20T12:42:26+00:00
Author: Threatpost
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-05-20-closing-the-gap-between-application-security-and-observability

[Source](https://threatpost.com/gap-application-security-and-observability/179684/){:target="_blank" rel="noopener"}

> Daniel Kaar, global director application security engineering at Dynatrace, highlights the newfound respect for AppSec-enabled observability in the wake of Log4Shell. [...]
