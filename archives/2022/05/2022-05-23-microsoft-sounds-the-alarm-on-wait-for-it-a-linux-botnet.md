Title: Microsoft sounds the alarm on – wait for it – a Linux botnet
Date: 2022-05-23T06:57:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-23-microsoft-sounds-the-alarm-on-wait-for-it-a-linux-botnet

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/23/microsoft_linux_botnet/){:target="_blank" rel="noopener"}

> Redmond claims the numbers are scary, but won't release them Microsoft has sounded the alarm on DDoS malware called XorDdos that targets Linux endpoints and servers.... [...]
