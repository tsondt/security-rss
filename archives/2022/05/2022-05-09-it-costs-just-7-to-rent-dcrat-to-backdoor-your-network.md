Title: It costs just $7 to rent DCRat to backdoor your network
Date: 2022-05-09T19:29:49+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-09-it-costs-just-7-to-rent-dcrat-to-backdoor-your-network

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/09/budgetfriendly_dcrat_malware/){:target="_blank" rel="noopener"}

> Budget-friendly tool breaks the you-get-what-you-pay-for rule A budget-friendly remote access trojan (RAT) that's under active development is selling on underground Russian forums for about $7 for a two-month subscription, according to BlackBerry researchers today.... [...]
