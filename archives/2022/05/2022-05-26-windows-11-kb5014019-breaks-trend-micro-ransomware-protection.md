Title: Windows 11 KB5014019 breaks Trend Micro ransomware protection
Date: 2022-05-26T15:44:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-05-26-windows-11-kb5014019-breaks-trend-micro-ransomware-protection

[Source](https://www.bleepingcomputer.com/news/security/windows-11-kb5014019-breaks-trend-micro-ransomware-protection/){:target="_blank" rel="noopener"}

> This week's Windows optional cumulative update previews have introduced a compatibility issue with some of Trend Micro's security products that breaks some of their capabilities, including the ransomware protection feature. [...]
