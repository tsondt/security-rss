Title: Ransomware the final nail in coffin for small university
Date: 2022-05-12T14:10:35+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-05-12-ransomware-the-final-nail-in-coffin-for-small-university

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/12/ransomware_dangerous_enough_to_close/){:target="_blank" rel="noopener"}

> Lincoln College shuttering after 157 years, ransomware attack from Iran final straw A December attack against a long-standing college in Illinois has pushed the institution to permanently close.... [...]
