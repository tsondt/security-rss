Title: Cheers ransomware hits VMware ESXi systems
Date: 2022-05-26T21:10:35+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-05-26-cheers-ransomware-hits-vmware-esxi-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/26/vmware-cheers-ransomware/){:target="_blank" rel="noopener"}

> Now we can say extortionware has jumped the shark Another ransomware strain is targeting VMware ESXi servers, which have been the focus of extortionists and other miscreants in recent months.... [...]
