Title: Cybergang Claims REvil is Back, Executes DDoS Attacks
Date: 2022-05-26T10:30:11+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: 2022-05-26-cybergang-claims-revil-is-back-executes-ddos-attacks

[Source](https://threatpost.com/cybergang-claims-revil-is-back-executes-ddos-attacks/179734/){:target="_blank" rel="noopener"}

> Actors claiming to be the defunct ransomware group are targeting one of Akami’s customers with a Layer 7 attack, demanding an extortion payment in Bitcoin. [...]
