Title: Yik Yak fixes information disclosure bug that leaked users’ GPS location
Date: 2022-05-23T14:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-23-yik-yak-fixes-information-disclosure-bug-that-leaked-users-gps-location

[Source](https://portswigger.net/daily-swig/yik-yak-fixes-information-disclosure-bug-that-leaked-users-gps-location){:target="_blank" rel="noopener"}

> Hairy MitM exploit independently discovered by two security researchers [...]
