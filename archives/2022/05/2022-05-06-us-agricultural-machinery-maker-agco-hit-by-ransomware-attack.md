Title: US agricultural machinery maker AGCO hit by ransomware attack
Date: 2022-05-06T12:21:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-06-us-agricultural-machinery-maker-agco-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/us-agricultural-machinery-maker-agco-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> AGCO, a leading US-based agricultural machinery producer, has announced it was hit by a ransomware attack impacting some of its production facilities. [...]
