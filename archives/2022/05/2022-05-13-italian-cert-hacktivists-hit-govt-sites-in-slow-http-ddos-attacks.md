Title: Italian CERT: Hacktivists hit govt sites in ‘Slow HTTP’ DDoS attacks
Date: 2022-05-13T14:16:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-13-italian-cert-hacktivists-hit-govt-sites-in-slow-http-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/italian-cert-hacktivists-hit-govt-sites-in-slow-http-ddos-attacks/){:target="_blank" rel="noopener"}

> Italy's Computer Security Incident Response Team (CSIRT) has published an announcement about the recent DDoS attacks that key sites in the country suffered in the last couple of days. [...]
