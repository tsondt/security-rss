Title: US brings first-of-its-kind criminal charges of Bitcoin-based sanctions-busting
Date: 2022-05-16T22:45:51+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-05-16-us-brings-first-of-its-kind-criminal-charges-of-bitcoin-based-sanctions-busting

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/16/judge_cryptocurrency_sanctions/){:target="_blank" rel="noopener"}

> Citizen allegedly moved $10m-plus in BTC into banned nation US prosecutors have accused an American citizen of illegally funneling more than $10 million in Bitcoin into an economically sanctioned country.... [...]
