Title: Let's play everyone's favorite game: REvil? Or Not REvil?
Date: 2022-05-27T07:33:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-05-27-lets-play-everyones-favorite-game-revil-or-not-revil

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/27/is_revil_trying_out_ddos/){:target="_blank" rel="noopener"}

> Another day, another DDoS attack that tries to scare the victim into paying up with mention of dreaded gang Akamai has spoken of a distributed denial of service (DDoS) assault against one of its customers during which the attackers astonishingly claimed to be associated with REvil, the notorious ransomware-as-a-service gang.... [...]
