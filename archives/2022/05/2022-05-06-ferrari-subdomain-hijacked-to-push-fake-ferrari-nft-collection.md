Title: Ferrari subdomain hijacked to push fake Ferrari NFT collection
Date: 2022-05-06T15:56:32-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-05-06-ferrari-subdomain-hijacked-to-push-fake-ferrari-nft-collection

[Source](https://www.bleepingcomputer.com/news/security/ferrari-subdomain-hijacked-to-push-fake-ferrari-nft-collection/){:target="_blank" rel="noopener"}

> One of Ferrari's subdomains was hijacked yesterday to host a scam promoting fake Ferrari NFT collection, according to researchers. The Ethereum wallet associated with the cryptocurrency scam appears to have collected a few hundred dollars before the hacked subdomain was shut down. [...]
