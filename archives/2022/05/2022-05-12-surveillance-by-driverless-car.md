Title: Surveillance by Driverless Car
Date: 2022-05-12T18:07:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;police;privacy;surveillance
Slug: 2022-05-12-surveillance-by-driverless-car

[Source](https://www.schneier.com/blog/archives/2022/05/surveillance-by-driverless-car.html){:target="_blank" rel="noopener"}

> San Francisco police are using autonomous vehicles as mobile surveillance cameras. Privacy advocates say the revelation that police are actively using AV footage is cause for alarm. “This is very concerning,” Electronic Frontier Foundation (EFF) senior staff attorney Adam Schwartz told Motherboard. He said cars in general are troves of personal consumer data, but autonomous vehicles will have even more of that data from capturing the details of the world around them. “So when we see any police department identify AVs as a new source of evidence, that’s very concerning.” [...]
