Title: Hackers display “blood is on your hands" on Russian TV, take down RuTube
Date: 2022-05-09T16:19:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-09-hackers-display-blood-is-on-your-hands-on-russian-tv-take-down-rutube

[Source](https://www.bleepingcomputer.com/news/security/hackers-display-blood-is-on-your-hands-on-russian-tv-take-down-rutube/){:target="_blank" rel="noopener"}

> ​Hackers continue to target Russia with cyberattacks, defacing Russian TV to show pro-Ukrainian messages and taking down the RuTube video streaming site. [...]
