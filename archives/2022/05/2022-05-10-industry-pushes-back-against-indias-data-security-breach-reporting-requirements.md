Title: Industry pushes back against India's data security breach reporting requirements
Date: 2022-05-10T02:47:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-10-industry-pushes-back-against-indias-data-security-breach-reporting-requirements

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/10/india_infosec_rules_criticised/){:target="_blank" rel="noopener"}

> Filling in a form at 4am improves infosec or privacy how, exactly? Opposition is building to India's recently introduced rules on reporting computer security breaches, which have come under fire for being impractical, ineffective, and impinging on privacy.... [...]
