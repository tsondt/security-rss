Title: Volatile market for stolen credit card data shaken up by sanctions against Russia
Date: 2022-05-25T14:38:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-25-volatile-market-for-stolen-credit-card-data-shaken-up-by-sanctions-against-russia

[Source](https://portswigger.net/daily-swig/volatile-market-for-stolen-credit-card-data-shaken-up-by-sanctions-against-russia){:target="_blank" rel="noopener"}

> Illicit trade still flourishing despite recent law enforcement takedowns [...]
