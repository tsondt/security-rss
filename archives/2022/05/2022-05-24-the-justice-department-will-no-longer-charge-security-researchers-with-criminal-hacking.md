Title: The Justice Department Will No Longer Charge Security Researchers with Criminal Hacking
Date: 2022-05-24T11:11:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;cybercrime;cybersecurity;national security policy;reverse engineering;vulnerabilities
Slug: 2022-05-24-the-justice-department-will-no-longer-charge-security-researchers-with-criminal-hacking

[Source](https://www.schneier.com/blog/archives/2022/05/the-justice-department-will-no-longer-charge-security-researchers-with-criminal-hacking.html){:target="_blank" rel="noopener"}

> Following a recent Supreme Court ruling, the Justice Department will no longer prosecute “good faith” security researchers with cybercrimes: The policy for the first time directs that good-faith security research should not be charged. Good faith security research means accessing a computer solely for purposes of good-faith testing, investigation, and/or correction of a security flaw or vulnerability, where such activity is carried out in a manner designed to avoid any harm to individuals or the public, and where the information derived from the activity is used primarily to promote the security or safety of the class of devices, machines, or [...]
