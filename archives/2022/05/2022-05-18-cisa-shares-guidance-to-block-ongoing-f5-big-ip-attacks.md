Title: CISA shares guidance to block ongoing F5 BIG-IP attacks
Date: 2022-05-18T11:20:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-18-cisa-shares-guidance-to-block-ongoing-f5-big-ip-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-shares-guidance-to-block-ongoing-f5-big-ip-attacks/){:target="_blank" rel="noopener"}

> In a joint advisory issued today, CISA and the Multi-State Information Sharing and Analysis Center (MS-ISAC) warned admins of active attacks targeting a critical F5 BIG-IP network security vulnerability (CVE-2022-1388). [...]
