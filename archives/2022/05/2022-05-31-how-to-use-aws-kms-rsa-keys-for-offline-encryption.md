Title: How to use AWS KMS RSA keys for offline encryption
Date: 2022-05-31T21:49:36+00:00
Author: Patrick Palmer
Category: AWS Security
Tags: Expert (400);Security, Identity, & Compliance;Asymmetric Cryptography;AWS KMS;Encryption;Offline;RSA;Security Blog
Slug: 2022-05-31-how-to-use-aws-kms-rsa-keys-for-offline-encryption

[Source](https://aws.amazon.com/blogs/security/how-to-use-aws-kms-rsa-keys-for-offline-encryption/){:target="_blank" rel="noopener"}

> This blog post discusses how you can use AWS Key Management Service (AWS KMS) RSA public keys on end clients or devices and encrypt data, then subsequently decrypt data by using private keys that are secured in AWS KMS. Asymmetric cryptography is a cryptographic system that uses key pairs. Each pair consists of a public [...]
