Title: The cyber threat isn’t going anywhere, but the fight back starts in London
Date: 2022-05-19T07:15:15+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-05-19-the-cyber-threat-isnt-going-anywhere-but-the-fight-back-starts-in-london

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/19/cyber_threat_sans/){:target="_blank" rel="noopener"}

> CyberThreat 22 returns this September Sponsored Post It might feel like you’re facing down the cyber bad guys all on your own sometimes but be assured that’s not the case. In fact, if you head to CyberThreat 22 this Autumn you can draw on the expertise of some of the world’s most experienced practitioners.... [...]
