Title: Beijing-backed gang looted IP around the world for years, claims Cybereason
Date: 2022-05-05T05:45:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-05-05-beijing-backed-gang-looted-ip-around-the-world-for-years-claims-cybereason

[Source](https://go.theregister.com/feed/www.theregister.com/2022/05/05/cybereason_china_operation_cuckoobees/){:target="_blank" rel="noopener"}

> Infosec outfit says group avoided detection by hiding payloads in undocumented Windows logs Infosec outfit Cybereason says it's discovered a multi-year – and very successful – Chinese effort to steal intellectual property.... [...]
