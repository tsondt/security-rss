Title: WordPress sites getting hacked ‘within seconds’ of TLS certificates being issued
Date: 2022-05-06T13:36:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-05-06-wordpress-sites-getting-hacked-within-seconds-of-tls-certificates-being-issued

[Source](https://portswigger.net/daily-swig/wordpress-sites-getting-hacked-within-seconds-of-tls-certificates-being-issued){:target="_blank" rel="noopener"}

> Attackers pounce before site owners can activate the installation wizard [...]
