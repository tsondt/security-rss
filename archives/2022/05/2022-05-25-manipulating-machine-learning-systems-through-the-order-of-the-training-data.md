Title: Manipulating Machine-Learning Systems through the Order of the Training Data
Date: 2022-05-25T15:30:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;machine learning;security engineering
Slug: 2022-05-25-manipulating-machine-learning-systems-through-the-order-of-the-training-data

[Source](https://www.schneier.com/blog/archives/2022/05/manipulating-machine-learning-systems-through-the-order-of-the-training-data.html){:target="_blank" rel="noopener"}

> Yet another adversarial ML attack: Most deep neural networks are trained by stochastic gradient descent. Now “stochastic” is a fancy Greek word for “random”; it means that the training data are fed into the model in random order. So what happens if the bad guys can cause the order to be not random? You guessed it— all bets are off. Suppose for example a company or a country wanted to have a credit-scoring system that’s secretly sexist, but still be able to pretend that its training was actually fair. Well, they could assemble a set of financial data that was [...]
