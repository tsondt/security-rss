Title: Windows 11 hacked three more times on last day of Pwn2Own contest
Date: 2022-05-21T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-05-21-windows-11-hacked-three-more-times-on-last-day-of-pwn2own-contest

[Source](https://www.bleepingcomputer.com/news/security/windows-11-hacked-three-more-times-on-last-day-of-pwn2own-contest/){:target="_blank" rel="noopener"}

> On the third and last day of the 2022 Pwn2Own Vancouver hacking contest, security researchers successfully hacked Microsoft's Windows 11 operating system three more times using zero-day exploits. [...]
