Title: Iranian hackers exposed in a highly targeted espionage campaign
Date: 2022-05-12T17:30:15-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-05-12-iranian-hackers-exposed-in-a-highly-targeted-espionage-campaign

[Source](https://www.bleepingcomputer.com/news/security/iranian-hackers-exposed-in-a-highly-targeted-espionage-campaign/){:target="_blank" rel="noopener"}

> Threat analysts have spotted a novel attack attributed to the Iranian hacking group known as APT34 group or Oilrig, who targeted a Jordanian diplomat with custom-crafted tools. [...]
