Title: Stealthy hackers target military and weapons contractors in recent attack
Date: 2022-09-28T12:06:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-28-stealthy-hackers-target-military-and-weapons-contractors-in-recent-attack

[Source](https://www.bleepingcomputer.com/news/security/stealthy-hackers-target-military-and-weapons-contractors-in-recent-attack/){:target="_blank" rel="noopener"}

> Security researchers have discovered a new campaign targeting multiple military contractors involved in weapon manufacturing, including an F-35 Lightning II fighter aircraft components supplier. [...]
