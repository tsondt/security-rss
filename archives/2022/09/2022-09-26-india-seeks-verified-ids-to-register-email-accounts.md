Title: India seeks verified IDs to register email accounts
Date: 2022-09-26T01:20:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-09-26-india-seeks-verified-ids-to-register-email-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/26/apac_tech_news_roundup/){:target="_blank" rel="noopener"}

> PLUS: Warnings on Chinese payment schemes; AWS brushes up its Cantonese; Hong Kong ponders digital dollar; and more! Asia In Brief India's government last week released a draft telco law that defines all over-the-top services as telecoms providers and therefore makes them subject to the same regulations imposed on carriers.... [...]
