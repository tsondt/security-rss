Title: Check out this Android spyware, says Microsoft, the home of a gazillion Windows flaws
Date: 2022-09-22T20:15:34+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-22-check-out-this-android-spyware-says-microsoft-the-home-of-a-gazillion-windows-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/22/microsoft_android_spyware_endpoint/){:target="_blank" rel="noopener"}

> While issuing an emergency patch for Endpoint Configuration Manager Data-stealing spyware disguised as a banking rewards app is targeting Android users, Microsoft's security team has warned.... [...]
