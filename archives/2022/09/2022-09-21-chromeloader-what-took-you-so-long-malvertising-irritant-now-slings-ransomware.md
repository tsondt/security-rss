Title: ChromeLoader, what took you so long? Malvertising irritant now slings ransomware
Date: 2022-09-21T09:26:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-21-chromeloader-what-took-you-so-long-malvertising-irritant-now-slings-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/21/vmware_microsoft_chromeloader_threat/){:target="_blank" rel="noopener"}

> Doesn't make cents, makes bigger bucks instead... probably ChromeLoader – the malware that exploded onto the scene this year by hijacking browsers to redirect users to pages of ads – is apparently evolving into a more significant threat by deploying malicious payloads that go beyond malvertising.... [...]
