Title: Auth0 warns that some source code repos may have been stolen
Date: 2022-09-28T14:03:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-28-auth0-warns-that-some-source-code-repos-may-have-been-stolen

[Source](https://www.bleepingcomputer.com/news/security/auth0-warns-that-some-source-code-repos-may-have-been-stolen/){:target="_blank" rel="noopener"}

> Authentication service provider and Okta subsidiary Auth0 has disclosed what it calls a "security event" involving some of its code repositories. [...]
