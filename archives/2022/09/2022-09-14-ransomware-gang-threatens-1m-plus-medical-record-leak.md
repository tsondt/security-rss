Title: Ransomware gang threatens 1m-plus medical record leak
Date: 2022-09-14T00:57:37+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-14-ransomware-gang-threatens-1m-plus-medical-record-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/14/ransomware_medical_groups/){:target="_blank" rel="noopener"}

> Criminals continue to target some of the most vulnerable Two recent ransomware attacks against healthcare systems indicate cybercriminals continue to put medical clinics and hospitals firmly in their crosshairs.... [...]
