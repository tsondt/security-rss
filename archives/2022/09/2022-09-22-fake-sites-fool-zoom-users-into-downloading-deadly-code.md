Title: Fake sites fool Zoom users into downloading deadly code
Date: 2022-09-22T13:45:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-22-fake-sites-fool-zoom-users-into-downloading-deadly-code

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/22/zoom_malware_infosteal_cyble/){:target="_blank" rel="noopener"}

> Ah, the human touch Beware the Zoom site you don't recognize, as a criminal gang is creating multiple fake versions aimed at luring users to download malware that can steal banking data, IP addresses, and other information.... [...]
