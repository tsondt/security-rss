Title: BlackCat ransomware claims attack on Italian energy agency
Date: 2022-09-02T16:05:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-02-blackcat-ransomware-claims-attack-on-italian-energy-agency

[Source](https://www.bleepingcomputer.com/news/security/blackcat-ransomware-claims-attack-on-italian-energy-agency/){:target="_blank" rel="noopener"}

> The BlackCat/ALPHV ransomware gang claimed responsibility for an attack that hit the systems of Italy's energy agency Gestore dei Servizi Energetici SpA (GSE) over the weekend. [...]
