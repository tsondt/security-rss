Title: Building the barricades against identity-based attacks
Date: 2022-09-15T13:42:14+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-09-15-building-the-barricades-against-identity-based-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/15/building_the_barricades_against_identitybased/){:target="_blank" rel="noopener"}

> Join our webinar to hear more about the value of Zero Trust unified identity protection platforms Webinar The first six months of this year have been characterized by relentless cyber security attacks whether state-induced (Russia's attacks on Ukraine), or incidents of criminal extortion and data theft. In such a threatening environment it is vital that organizations and enterprises defend themselves from internet and identity-based attacks.... [...]
