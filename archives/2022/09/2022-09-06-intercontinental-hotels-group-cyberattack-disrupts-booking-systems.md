Title: InterContinental Hotels Group cyberattack disrupts booking systems
Date: 2022-09-06T13:11:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-06-intercontinental-hotels-group-cyberattack-disrupts-booking-systems

[Source](https://www.bleepingcomputer.com/news/security/intercontinental-hotels-group-cyberattack-disrupts-booking-systems/){:target="_blank" rel="noopener"}

> Leading hospitality company InterContinental Hotels Group PLC (also known as IHG Hotels & Resorts) says its information technology (IT) systems have been disrupted since yesterday after its network was breached. [...]
