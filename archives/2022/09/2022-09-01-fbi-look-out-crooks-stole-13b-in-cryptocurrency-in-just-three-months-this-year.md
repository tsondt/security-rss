Title: FBI: Look out, crooks stole $1.3b in cryptocurrency in just three months this year
Date: 2022-09-01T02:32:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-01-fbi-look-out-crooks-stole-13b-in-cryptocurrency-in-just-three-months-this-year

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/01/fbi_cybercrime_defi_cryptocurrency/){:target="_blank" rel="noopener"}

> DeFi, as in, defying belief The FBI has urged people to be cautious and heavily research a DeFi – decentralized finance – provider before putting your money into it, after more than a billion dollars was stolen from these providers in three months.... [...]
