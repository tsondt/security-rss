Title: Google Chrome emergency update fixes new zero-day used in attacks
Date: 2022-09-02T19:29:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-09-02-google-chrome-emergency-update-fixes-new-zero-day-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-emergency-update-fixes-new-zero-day-used-in-attacks/){:target="_blank" rel="noopener"}

> Google has released Chrome 105.0.5195.102 for Windows, Mac, and Linux users to address a single high-severity security flaw, the sixth Chrome zero-day exploited in attacks patched this year. [...]
