Title: Domain shadowing becoming more popular among cybercriminals
Date: 2022-09-21T16:04:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-21-domain-shadowing-becoming-more-popular-among-cybercriminals

[Source](https://www.bleepingcomputer.com/news/security/domain-shadowing-becoming-more-popular-among-cybercriminals/){:target="_blank" rel="noopener"}

> Threat analysts at Palo Alto Networks (Unit 42) discovered that the phenomenon of 'domain shadowing' might be more prevalent than previously thought, uncovering 12,197 cases while scanning the web between April and June 2022. [...]
