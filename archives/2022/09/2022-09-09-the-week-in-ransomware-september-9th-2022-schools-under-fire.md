Title: The Week in Ransomware - September 9th 2022 - Schools under fire
Date: 2022-09-09T16:14:03-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-09-the-week-in-ransomware-september-9th-2022-schools-under-fire

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-9th-2022-schools-under-fire/){:target="_blank" rel="noopener"}

> Ransomware gangs have been busy this week, launching attacks against NAS devices, one of the largest hotel groups, IHG, and LAUSD, the second largest school district in the USA. [...]
