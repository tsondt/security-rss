Title: Dump these small-biz routers, says Cisco, because we won't patch their flawed VPN
Date: 2022-09-08T23:26:45+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-08-dump-these-small-biz-routers-says-cisco-because-we-wont-patch-their-flawed-vpn

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/08/cisco_routers_vulnerability/){:target="_blank" rel="noopener"}

> Nothing like an authentication bypass for your private IPSec network Cisco patched three security vulnerabilities in its products this week, and said it will leave unpatched a VPN-hijacking flaw that affects four small business routers.... [...]
