Title: CSRF flaw in csurf NPM package aimed at protecting against the same flaws
Date: 2022-09-02T15:11:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-02-csrf-flaw-in-csurf-npm-package-aimed-at-protecting-against-the-same-flaws

[Source](https://portswigger.net/daily-swig/csrf-flaw-in-csurf-npm-package-aimed-at-protecting-against-the-same-flaws){:target="_blank" rel="noopener"}

> Serious security prompt developers to discontinue open source package [...]
