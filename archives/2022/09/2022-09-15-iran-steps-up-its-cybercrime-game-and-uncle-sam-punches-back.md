Title: Iran steps up its cybercrime game and Uncle Sam punches back
Date: 2022-09-15T12:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-15-iran-steps-up-its-cybercrime-game-and-uncle-sam-punches-back

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/15/iran_cybercrime_indictments_sanctions/){:target="_blank" rel="noopener"}

> Criminal charges, more sanctions, and a $10m bounty, oh my The US has issued indictments against three Iranians linked to the country's Islamic Revolutionary Guard Corps (IRGC) for their alleged roles in plotting ransomware attacks against American critical infrastructure, and also sanctioned multiple individuals and two entities.... [...]
