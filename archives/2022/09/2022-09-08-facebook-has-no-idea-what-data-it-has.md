Title: Facebook Has No Idea What Data It Has
Date: 2022-09-08T15:14:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;complexity;courts;data collection;Facebook;transparency
Slug: 2022-09-08-facebook-has-no-idea-what-data-it-has

[Source](https://www.schneier.com/blog/archives/2022/09/facebook-has-no-idea-what-data-it-has.html){:target="_blank" rel="noopener"}

> This is from a court deposition : Facebook’s stonewalling has been revealing on its own, providing variations on the same theme: It has amassed so much data on so many billions of people and organized it so confusingly that full transparency is impossible on a technical level. In the March 2022 hearing, Zarashaw and Steven Elia, a software engineering manager, described Facebook as a data-processing apparatus so complex that it defies understanding from within. The hearing amounted to two high-ranking engineers at one of the most powerful and resource-flush engineering outfits in history describing their product as an unknowable machine. [...]
