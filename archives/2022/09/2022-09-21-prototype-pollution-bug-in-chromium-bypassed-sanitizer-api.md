Title: Prototype pollution bug in Chromium bypassed Sanitizer API
Date: 2022-09-21T10:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-21-prototype-pollution-bug-in-chromium-bypassed-sanitizer-api

[Source](https://portswigger.net/daily-swig/prototype-pollution-bug-in-chromium-bypassed-sanitizer-api){:target="_blank" rel="noopener"}

> Issue highlights the challenges of preventing client-side attacks [...]
