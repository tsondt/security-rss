Title: What makes Google Cloud security special: Our reflections 1 year after joining OCISO
Date: 2022-09-28T16:00:00+00:00
Author: Taylor Lehmann
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-09-28-what-makes-google-cloud-security-special-our-reflections-1-year-after-joining-ociso

[Source](https://cloud.google.com/blog/products/identity-security/what-makes-google-cloud-security-special/){:target="_blank" rel="noopener"}

> Editor's note : Google Cloud’s Office of the Chief Information Security Officer (OCISO) is an expert team of cybersecurity leaders, including established industry CISOs, initially formed in 2019. Together they have more than 500 years of combined cybersecurity experience and leadership across industries including global healthcare, finance, telecommunications and media, government and public sector, and retail industries. Their goal is to meet the customer where they are and help them take the best next steps to secure their enterprise. In this column, Taylor Lehmann, Director in OCISO, and David Stone, Security Consultant in OCISO, reflect on their first year with [...]
