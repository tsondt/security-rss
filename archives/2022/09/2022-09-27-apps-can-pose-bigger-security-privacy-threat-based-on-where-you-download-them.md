Title: Apps can pose bigger security, privacy threat based on where you download them
Date: 2022-09-27T13:53:58+00:00
Author: The Conversation
Category: Ars Technica
Tags: Biz & IT;Tech;App Store;app stores;google play;privacy;targeting
Slug: 2022-09-27-apps-can-pose-bigger-security-privacy-threat-based-on-where-you-download-them

[Source](https://arstechnica.com/?p=1884773){:target="_blank" rel="noopener"}

> Enlarge (credit: https://www.gettyimages.com/detail/news-photo/blinkee-city-rental-scooter-is-seen-in-warsaw-poland-on-news-photo/1031626648 ) Google and Apple have removed hundreds of apps from their app stores at the request of governments around the world, creating regional disparities in access to mobile apps at a time when many economies are becoming increasingly dependent on them. The mobile phone giants have removed over 200 Chinese apps, including widely downloaded apps like TikTok, at the Indian government’s request in recent years. Similarly, the companies removed LinkedIn, an essential app for professional networking, from Russian app stores at the Russian government’s request. However, access to apps is just one concern. Developers also regionalize apps, [...]
