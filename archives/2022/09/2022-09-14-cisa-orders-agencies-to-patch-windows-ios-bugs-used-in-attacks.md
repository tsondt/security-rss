Title: CISA orders agencies to patch Windows, iOS bugs used in attacks
Date: 2022-09-14T12:48:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple;Microsoft
Slug: 2022-09-14-cisa-orders-agencies-to-patch-windows-ios-bugs-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-agencies-to-patch-windows-ios-bugs-used-in-attacks/){:target="_blank" rel="noopener"}

> CISA added two new vulnerabilities to its list of security bugs exploited in the wild today, including a Windows privilege escalation vulnerability and an arbitrary code execution flaw affecting iPhones and Macs. [...]
