Title: Over 80% of the top websites leak user searches to advertisers
Date: 2022-09-08T10:27:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-08-over-80-of-the-top-websites-leak-user-searches-to-advertisers

[Source](https://www.bleepingcomputer.com/news/security/over-80-percent-of-the-top-websites-leak-user-searches-to-advertisers/){:target="_blank" rel="noopener"}

> Security researchers at Norton Labs have found that roughly eight out of ten websites featuring a search bar will leak their visitor's search terms to online advertisers like Google. [...]
