Title: US seeks standards dominance, lets Huawei access previously forbidden crypto tech
Date: 2022-09-09T03:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-09-us-seeks-standards-dominance-lets-huawei-access-previously-forbidden-crypto-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/09/bis_eases_tech_export_restrictions/){:target="_blank" rel="noopener"}

> Beijing thinks standards should include central network controls. Washington does not The US Commerce Department's Bureau of Industry and Security (BIS) has relaxed restrictions that barred export of some encryption technologies to Huawei, in the name of ensuring the United States is in a better position to negotiate global standards.... [...]
