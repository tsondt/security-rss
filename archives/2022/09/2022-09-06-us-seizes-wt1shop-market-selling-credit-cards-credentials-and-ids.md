Title: US seizes WT1SHOP market selling credit cards, credentials, and IDs
Date: 2022-09-06T18:43:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-09-06-us-seizes-wt1shop-market-selling-credit-cards-credentials-and-ids

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-wt1shop-market-selling-credit-cards-credentials-and-ids/){:target="_blank" rel="noopener"}

> An international law enforcement operation has seized the website and domains for WT1SHOP, a criminal marketplace that sold stolen credit cards, I.D. cards, and millions of login credentials. [...]
