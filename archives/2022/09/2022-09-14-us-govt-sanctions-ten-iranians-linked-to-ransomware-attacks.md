Title: US govt sanctions ten Iranians linked to ransomware attacks
Date: 2022-09-14T11:43:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-14-us-govt-sanctions-ten-iranians-linked-to-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-govt-sanctions-ten-iranians-linked-to-ransomware-attacks/){:target="_blank" rel="noopener"}

> The Treasury Department's Office of Foreign Assets Control (OFAC) announced sanctions today against ten individuals and two entities affiliated with Iran's Islamic Revolutionary Guard Corps (IRGC) for their involvement in ransomware attacks. [...]
