Title: Connect Gateway and ArgoCD: Deploy to Distributed Kubernetes
Date: 2022-09-06T16:00:00+00:00
Author: Xuebin Zhang
Category: GCP Security
Tags: Application Modernization;Hybrid & Multicloud;Identity & Security;Containers & Kubernetes
Slug: 2022-09-06-connect-gateway-and-argocd-deploy-to-distributed-kubernetes

[Source](https://cloud.google.com/blog/products/containers-kubernetes/connect-gateway-with-argocd/){:target="_blank" rel="noopener"}

> Integrating your ArgoCD deployment with Connect Gateway and Workload Identity provides a seamless path to deploy to Kubernetes on many platforms. ArgoCD can easily be configured to centrally manage various cluster platforms including GKE clusters, Anthos clusters, and many more. This promotes consistency across your fleet, saves time in onboarding a new cluster, and simplifies your distributed RBAC model for management connectivity. Skip to steps below to configure ArgoCD for your use case. Background Cloud customers who choose ArgoCD as their continuous delivery tool may opt into a centralized architecture, whereby ArgoCD is hosted in a central K8s cluster and [...]
