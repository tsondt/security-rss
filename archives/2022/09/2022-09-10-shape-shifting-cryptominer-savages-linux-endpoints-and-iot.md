Title: Shape-shifting cryptominer savages Linux endpoints and IoT
Date: 2022-09-10T11:00:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-10-shape-shifting-cryptominer-savages-linux-endpoints-and-iot

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/10/in_brief_security/){:target="_blank" rel="noopener"}

> Also, Authorities seize WT1SHOP selling 5.8m sets of PII, The North Face users face tough security hike In brief AT&T cybersecurity researchers have discovered a sneaky piece of malware targeting Linux endpoints and IoT devices in the hopes of gaining persistent access and turning victims into crypto-mining drones.... [...]
