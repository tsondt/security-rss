Title: Been hit by LockerGoga ransomware? A free fix is now out
Date: 2022-09-19T20:07:43+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-19-been-hit-by-lockergoga-ransomware-a-free-fix-is-now-out

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/19/lockergoga_ransomware_decryptor/){:target="_blank" rel="noopener"}

> Software nasty used to cause hundreds of millions of dollars in damages, cops say If you've been hit by the LockerGoga ransomware, an international law enforcement effort has publicly released a tool to fix the problem.... [...]
