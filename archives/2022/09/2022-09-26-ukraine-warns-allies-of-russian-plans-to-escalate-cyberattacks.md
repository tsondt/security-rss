Title: Ukraine warns allies of Russian plans to escalate cyberattacks
Date: 2022-09-26T11:10:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-26-ukraine-warns-allies-of-russian-plans-to-escalate-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/ukraine-warns-allies-of-russian-plans-to-escalate-cyberattacks/){:target="_blank" rel="noopener"}

> The Ukrainian military intelligence service warned today that Russia is planning "massive cyber-attacks" targeting the critical infrastructure of Ukraine and its allies. [...]
