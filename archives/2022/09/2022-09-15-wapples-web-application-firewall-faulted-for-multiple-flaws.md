Title: WAPPLES web application firewall faulted for multiple flaws
Date: 2022-09-15T14:43:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-15-wapples-web-application-firewall-faulted-for-multiple-flaws

[Source](https://portswigger.net/daily-swig/wapples-web-application-firewall-faulted-for-multiple-flaws){:target="_blank" rel="noopener"}

> Researcher uncovers RCE and undocumented backdoor risks [...]
