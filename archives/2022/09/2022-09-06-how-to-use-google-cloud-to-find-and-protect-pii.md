Title: How to use Google Cloud to find and protect PII
Date: 2022-09-06T17:30:00+00:00
Author: Nikhiel Sawhney
Category: GCP Security
Tags: Data Analytics;Business Application Platform;Open Source;Identity & Security
Slug: 2022-09-06-how-to-use-google-cloud-to-find-and-protect-pii

[Source](https://cloud.google.com/blog/products/identity-security/how-to-use-google-cloud-to-find-and-protect-pii/){:target="_blank" rel="noopener"}

> BigQuery is a leading data warehouse solution in the market today, and is valued by customers who need to gather insights and advanced analytics on their data. Many common BigQuery use cases involve the storage and processing of Personal Identifiable Information (PII)—data that needs to be protected within Google Cloud from unauthorized and malicious access. Too often, the process of finding and identifying PII in BigQuery data relies on manual PII discovery and duplication of that data. One common way to do this is by taking an extract of columns used for PII and copying them into a separate table [...]
