Title: Unpatched 15-year old Python bug allows code execution in 350k projects
Date: 2022-09-21T12:45:28-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-09-21-unpatched-15-year-old-python-bug-allows-code-execution-in-350k-projects

[Source](https://www.bleepingcomputer.com/news/security/unpatched-15-year-old-python-bug-allows-code-execution-in-350k-projects/){:target="_blank" rel="noopener"}

> A vulnerability in the Python programming language that has been overlooked for 15 years is now back in the spotlight as it likely affects more than 350,000 open-source repositories and can lead to code execution. [...]
