Title: Firmware bugs in many HP computer models left unfixed for over a year
Date: 2022-09-11T11:13:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-11-firmware-bugs-in-many-hp-computer-models-left-unfixed-for-over-a-year

[Source](https://www.bleepingcomputer.com/news/security/firmware-bugs-in-many-hp-computer-models-left-unfixed-for-over-a-year/){:target="_blank" rel="noopener"}

> A set of six high-severity firmware vulnerabilities impacting a broad range of HP devices used in enterprise environments are still waiting to be patched, although some of them were publicly disclosed since July 2021. [...]
