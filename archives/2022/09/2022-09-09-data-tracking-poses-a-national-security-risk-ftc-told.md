Title: Data tracking poses a 'national security risk' FTC told
Date: 2022-09-09T23:19:51+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-09-data-tracking-poses-a-national-security-risk-ftc-told

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/09/data_tracking_national_security_risk/){:target="_blank" rel="noopener"}

> 'We're making China's job easier' The massive amounts of digital data being bought and sold — or sometimes freely shared — poses a grave national security risk, according to a former US policymaker and diplomat.... [...]
