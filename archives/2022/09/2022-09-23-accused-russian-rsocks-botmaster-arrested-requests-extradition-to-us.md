Title: Accused Russian RSOCKS Botmaster Arrested, Requests Extradition to U.S.
Date: 2022-09-23T18:19:51+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;24chasa.bg;Denis Emelyantsev;Denis Kloster;RSOCKS botnet;RSOCKS proxy;RUSdot;Spamdot
Slug: 2022-09-23-accused-russian-rsocks-botmaster-arrested-requests-extradition-to-us

[Source](https://krebsonsecurity.com/2022/09/accused-russian-rsocks-botmaster-arrested-requests-extradition-to-u-s/){:target="_blank" rel="noopener"}

> A 36-year-old Russian man recently identified by KrebsOnSecurity as the likely proprietor of the massive RSOCKS botnet has been arrested in Bulgaria at the request of U.S. authorities. At a court hearing in Bulgaria this month, the accused hacker requested and was granted extradition to the United States, reportedly telling the judge, “America is looking for me because I have enormous information and they need it.” A copy of the passport for Denis Kloster, as posted to his Vkontakte page in 2019. On June 22, KrebsOnSecurity published Meet the Administrators of the RSOCKS Proxy Botnet, which identified Denis Kloster, a.k.a. [...]
