Title: Ukraine dismantles more bot farms spreading Russian disinformation
Date: 2022-09-07T11:47:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-07-ukraine-dismantles-more-bot-farms-spreading-russian-disinformation

[Source](https://www.bleepingcomputer.com/news/security/ukraine-dismantles-more-bot-farms-spreading-russian-disinformation/){:target="_blank" rel="noopener"}

> The Cyber Department of the Ukrainian Security Service (SSU) dismantled two more bot farms that spread Russian disinformation on social networks and messaging platforms via thousands of fake accounts. [...]
