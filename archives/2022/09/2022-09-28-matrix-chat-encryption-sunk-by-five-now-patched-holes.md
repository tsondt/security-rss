Title: Matrix chat encryption sunk by five now-patched holes
Date: 2022-09-28T21:22:54+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-28-matrix-chat-encryption-sunk-by-five-now-patched-holes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/28/matrix_encryption_flaws/){:target="_blank" rel="noopener"}

> You take the green pill, you'll spend six hours in a 'don't roll your own crypto' debate Four security researchers have identified five cryptographic vulnerabilities in code libraries that can be exploited to undermine Matrix encrypted chat clients. This includes impersonating users and sending messages as them.... [...]
