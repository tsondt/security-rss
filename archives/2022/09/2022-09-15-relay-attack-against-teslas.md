Title: Relay Attack against Teslas
Date: 2022-09-15T15:28:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;hacking;keys;vulnerabilities
Slug: 2022-09-15-relay-attack-against-teslas

[Source](https://www.schneier.com/blog/archives/2022/09/relay-attack-against-teslas.html){:target="_blank" rel="noopener"}

> Nice work : Radio relay attacks are technically complicated to execute, but conceptually easy to understand: attackers simply extend the range of your existing key using what is essentially a high-tech walkie-talkie. One thief stands near you while you’re in the grocery store, intercepting your key’s transmitted signal with a radio transceiver. Another stands near your car, with another transceiver, taking the signal from their friend and passing it on to the car. Since the car and the key can now talk, through the thieves’ range extenders, the car has no reason to suspect the key isn’t inside—and fires right [...]
