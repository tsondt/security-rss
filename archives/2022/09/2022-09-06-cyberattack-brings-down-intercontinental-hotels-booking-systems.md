Title: Cyberattack brings down InterContinental Hotels' booking systems
Date: 2022-09-06T20:42:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-06-cyberattack-brings-down-intercontinental-hotels-booking-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/06/ihg_hotels_data_breach/){:target="_blank" rel="noopener"}

> Online booking systems and other services knocked offline amid network intrusion The IT systems of InterContinental Hotels Group, the massive hospitality organization that operates 17 hotel brands around the world, have been compromised, causing ongoing disruption to the corporation's online booking systems and other services.... [...]
