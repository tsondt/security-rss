Title: Microsoft Defender falsely detects Win32/Hive.ZY in Google Chrome, Electron apps
Date: 2022-09-04T11:30:48-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-09-04-microsoft-defender-falsely-detects-win32hivezy-in-google-chrome-electron-apps

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-falsely-detects-win32-hivezy-in-google-chrome-electron-apps/){:target="_blank" rel="noopener"}

> A bad Microsoft Defender signature update mistakenly detects Google Chrome, Microsoft Edge, Discord, and other Electron apps as 'Win32/Hive.ZY' each time the apps are opened in Windows. [...]
