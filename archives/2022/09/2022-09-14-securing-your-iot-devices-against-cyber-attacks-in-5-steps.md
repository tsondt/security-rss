Title: Securing your IoT devices against cyber attacks in 5 steps
Date: 2022-09-14T10:06:03-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-09-14-securing-your-iot-devices-against-cyber-attacks-in-5-steps

[Source](https://www.bleepingcomputer.com/news/security/securing-your-iot-devices-against-cyber-attacks-in-5-steps/){:target="_blank" rel="noopener"}

> How is IoT being used in the enterprise, and how can it be secured? We will demonstrate important security best practices and how a secure password policy is paramount to the security of devices. [...]
