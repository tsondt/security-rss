Title: Damart clothing store hit by Hive ransomware, $2 million demanded
Date: 2022-09-02T12:25:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-09-02-damart-clothing-store-hit-by-hive-ransomware-2-million-demanded

[Source](https://www.bleepingcomputer.com/news/security/damart-clothing-store-hit-by-hive-ransomware-2-million-demanded/){:target="_blank" rel="noopener"}

> Damart, a French clothing company with over 130 stores across the world, is being extorted for $2 million after a cyberattack from the Hive ransomware gang. [...]
