Title: Second largest U.S. school district LAUSD hit by ransomware
Date: 2022-09-06T07:41:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-06-second-largest-us-school-district-lausd-hit-by-ransomware

[Source](https://www.bleepingcomputer.com/news/security/second-largest-us-school-district-lausd-hit-by-ransomware/){:target="_blank" rel="noopener"}

> Los Angeles Unified (LAUSD), the second largest school district in the U.S., disclosed that a ransomware attack hit its Information Technology (IT) systems over the weekend. [...]
