Title: Cisco confirms Yanluowang ransomware leaked stolen company data
Date: 2022-09-12T04:21:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-12-cisco-confirms-yanluowang-ransomware-leaked-stolen-company-data

[Source](https://www.bleepingcomputer.com/news/security/cisco-confirms-yanluowang-ransomware-leaked-stolen-company-data/){:target="_blank" rel="noopener"}

> Cisco has confirmed that the data leaked yessterday by the Yanluowang ransomware gang was stolen from the company network during a cyberattack in May. [...]
