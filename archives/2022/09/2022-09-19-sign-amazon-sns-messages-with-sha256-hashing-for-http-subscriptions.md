Title: Sign Amazon SNS messages with SHA256 hashing for HTTP subscriptions
Date: 2022-09-19T19:03:31+00:00
Author: Daniel Caminhas
Category: AWS Security
Tags: Amazon Simple Notification Service (SNS);Announcements;Intermediate (200);Security, Identity, & Compliance;Amazon SNS;Message signing;Security Blog
Slug: 2022-09-19-sign-amazon-sns-messages-with-sha256-hashing-for-http-subscriptions

[Source](https://aws.amazon.com/blogs/security/sign-amazon-sns-messages-with-sha256-hashing-for-http-subscriptions/){:target="_blank" rel="noopener"}

> Amazon Simple Notification Service (Amazon SNS) now supports message signatures based on Secure Hash Algorithm 256 (SHA256) hashing. Amazon SNS signs the messages that are delivered from your Amazon SNS topic so that subscribed HTTP endpoints can verify the authenticity of the messages. In this blog post, we will show you how to enable message [...]
