Title: NETGEAR resolves router vulnerabilities in bundled gaming component
Date: 2022-09-16T16:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-16-netgear-resolves-router-vulnerabilities-in-bundled-gaming-component

[Source](https://portswigger.net/daily-swig/netgear-resolves-router-vulnerabilities-in-bundled-gaming-component){:target="_blank" rel="noopener"}

> Silicon Valley vendor tackles command injection and MitM-to-RCE issues [...]
