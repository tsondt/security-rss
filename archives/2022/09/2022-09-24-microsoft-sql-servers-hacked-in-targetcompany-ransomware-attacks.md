Title: Microsoft SQL servers hacked in TargetCompany ransomware attacks
Date: 2022-09-24T11:12:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-24-microsoft-sql-servers-hacked-in-targetcompany-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-sql-servers-hacked-in-targetcompany-ransomware-attacks/){:target="_blank" rel="noopener"}

> Security analysts at ASEC have discovered a new wave of attacks targeting vulnerable Microsoft SQL servers, involving the deployment of a ransomware strain named FARGO. [...]
