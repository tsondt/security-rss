Title: U-Haul discloses data breach exposing customer driver licenses
Date: 2022-09-12T16:28:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-12-u-haul-discloses-data-breach-exposing-customer-driver-licenses

[Source](https://www.bleepingcomputer.com/news/security/u-haul-discloses-data-breach-exposing-customer-driver-licenses/){:target="_blank" rel="noopener"}

> Moving and storage giant U-Haul International (U-Haul) disclosed a data breach after a customer contract search tool was hacked to access customers' names and driver's license information. [...]
