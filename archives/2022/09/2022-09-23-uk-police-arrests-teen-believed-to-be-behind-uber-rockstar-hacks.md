Title: UK Police arrests teen believed to be behind Uber, Rockstar hacks
Date: 2022-09-23T13:58:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-23-uk-police-arrests-teen-believed-to-be-behind-uber-rockstar-hacks

[Source](https://www.bleepingcomputer.com/news/security/uk-police-arrests-teen-believed-to-be-behind-uber-rockstar-hacks/){:target="_blank" rel="noopener"}

> The City of London police announced on Twitter today the arrest of a British 17-year-old teen suspected of being involved in recent cyberattacks. [...]
