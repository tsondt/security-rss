Title: Optus data breach: Australians will be able to change their driver’s licence with telco to pay
Date: 2022-09-27T10:27:27+00:00
Author: Natasha May
Category: The Guardian
Tags: Australia news;Optus;Data and computer security;Telecommunications industry;Cybercrime;Identity fraud;Technology
Slug: 2022-09-27-optus-data-breach-australians-will-be-able-to-change-their-drivers-licence-with-telco-to-pay

[Source](https://www.theguardian.com/australia-news/2022/sep/27/optus-data-breach-australians-will-be-able-to-change-their-drivers-licence-with-telco-to-pay){:target="_blank" rel="noopener"}

> Federal opposition wants commonwealth to allow people to get new passports for free too – and quickly Get our free news app, morning email briefing or daily news podcast Australians caught up in a massive breach of Optus data will be able to change their driver’s licence numbers and get new cards, with the telco expected to bear the multimillion-dollar cost of the changeover. The New South Wales, Victoria, Queensland and South Australia governments on Tuesday evening began clearing the bureaucratic hurdles for anyone who can prove they are victims of the hack, which has affected millions of people. Sign [...]
