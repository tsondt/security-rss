Title: Mystery hackers are “hyperjacking” targets for insidious spying
Date: 2022-09-30T15:22:12+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;hacking;hyperjacking;syndication;virtualization;VMs
Slug: 2022-09-30-mystery-hackers-are-hyperjacking-targets-for-insidious-spying

[Source](https://arstechnica.com/?p=1885873){:target="_blank" rel="noopener"}

> Enlarge (credit: Marco Rosario Venturini Autieri/Getty Images) For decades, virtualization software has offered a way to vastly multiply computers’ efficiency, hosting entire collections of computers as “virtual machines” on just one physical machine. And for almost as long, security researchers have warned about the potential dark side of that technology: theoretical “hyperjacking” and “Blue Pill” attacks, where hackers hijack virtualization to spy on and manipulate virtual machines, with potentially no way for a targeted computer to detect the intrusion. That insidious spying has finally jumped from research papers to reality with warnings that one mysterious team of hackers has carried [...]
