Title: Apple fixes eighth zero-day used to hack iPhones and Macs this year
Date: 2022-09-12T14:20:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2022-09-12-apple-fixes-eighth-zero-day-used-to-hack-iphones-and-macs-this-year

[Source](https://www.bleepingcomputer.com/news/security/apple-fixes-eighth-zero-day-used-to-hack-iphones-and-macs-this-year/){:target="_blank" rel="noopener"}

> Apple has released security updates to address the eighth zero-day vulnerability used in attacks against iPhones and Macs since the start of the year. [...]
