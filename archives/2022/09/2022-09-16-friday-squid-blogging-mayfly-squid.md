Title: Friday Squid Blogging: Mayfly Squid
Date: 2022-09-16T21:01:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-09-16-friday-squid-blogging-mayfly-squid

[Source](https://www.schneier.com/blog/archives/2022/09/friday-squid-blogging-mayfly-squid.html){:target="_blank" rel="noopener"}

> This is surprisingly funny. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
