Title: Prompt Injection/Extraction Attacks against AI Systems
Date: 2022-09-22T11:45:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cyberattack;security engineering
Slug: 2022-09-22-prompt-injectionextraction-attacks-against-ai-systems

[Source](https://www.schneier.com/blog/archives/2022/09/prompt-injection-extraction-attacks-against-ai-systems.html){:target="_blank" rel="noopener"}

> This is an interesting attack I had not previously considered. The variants are interesting, and I think we’re just starting to understand their implications. [...]
