Title: CISA warns of critical ManageEngine RCE bug used in attacks
Date: 2022-09-22T17:43:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-22-cisa-warns-of-critical-manageengine-rce-bug-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-of-critical-manageengine-rce-bug-used-in-attacks/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has added a critical severity Java deserialization vulnerability affecting multiple Zoho ManageEngine products to its catalog of bugs exploited in the wild. [...]
