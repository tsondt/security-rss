Title: Microsoft to kill off old access rules in Exchange Online
Date: 2022-09-28T23:34:30+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-28-microsoft-to-kill-off-old-access-rules-in-exchange-online

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/28/microsoft_exchange_online_cars/){:target="_blank" rel="noopener"}

> Awoooogah – this is your one-year warning to switch over, enterprises Microsoft next month will start phasing out Client Access Rules (CARs) in Exchange Online – and will do away with this means for controlling access altogether within a year.... [...]
