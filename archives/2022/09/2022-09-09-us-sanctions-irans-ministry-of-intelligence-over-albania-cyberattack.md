Title: US sanctions Iran’s Ministry of Intelligence over Albania cyberattack
Date: 2022-09-09T12:35:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-09-us-sanctions-irans-ministry-of-intelligence-over-albania-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-iran-s-ministry-of-intelligence-over-albania-cyberattack/){:target="_blank" rel="noopener"}

> The U.S. Treasury Department announced sanctions today against Iran's Ministry of Intelligence and Security (MOIS) and its Minister of Intelligence for their role in the July cyberattack against the government of Albania, a U.S. ally and a NATO member state. [...]
