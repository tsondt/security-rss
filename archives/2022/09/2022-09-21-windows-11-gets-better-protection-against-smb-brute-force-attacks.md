Title: Windows 11 gets better protection against SMB brute-force attacks
Date: 2022-09-21T17:22:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-09-21-windows-11-gets-better-protection-against-smb-brute-force-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-11-gets-better-protection-against-smb-brute-force-attacks/){:target="_blank" rel="noopener"}

> Microsoft announced that the Windows 11 SMB server is now better protected against brute-force attacks with the release of the Insider Preview Build 25206 to the Dev Channel. [...]
