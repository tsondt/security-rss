Title: Coinbase funds lawsuit against Tornado Cash cryptomixer sanctions
Date: 2022-09-09T14:45:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Legal
Slug: 2022-09-09-coinbase-funds-lawsuit-against-tornado-cash-cryptomixer-sanctions

[Source](https://www.bleepingcomputer.com/news/security/coinbase-funds-lawsuit-against-tornado-cash-cryptomixer-sanctions/){:target="_blank" rel="noopener"}

> Coinbase announced on Tuesday that it is funding a lawsuit brought by six people in the U.S. against the Department of Treasury's for the sanctions on the Tornado Cash open-source cryptocurrency mixer platform. [...]
