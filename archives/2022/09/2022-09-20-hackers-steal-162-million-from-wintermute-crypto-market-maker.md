Title: Hackers steal $162 million from Wintermute crypto market maker
Date: 2022-09-20T11:18:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-20-hackers-steal-162-million-from-wintermute-crypto-market-maker

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-162-million-from-wintermute-crypto-market-maker/){:target="_blank" rel="noopener"}

> Digital assets trading firm Wintermute has been hacked and lost $162.2 million in DeFi operations, the company CEO, Evgeny Gaevoy, announced earlier today. [...]
