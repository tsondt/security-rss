Title: Introducing more ways to protect corporate applications with BeyondCorp Enterprise
Date: 2022-09-13T16:00:00+00:00
Author: Prashant Jain
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-09-13-introducing-more-ways-to-protect-corporate-applications-with-beyondcorp-enterprise

[Source](https://cloud.google.com/blog/products/identity-security/announcing-in-preview-new-integrations-for-beyondcorp-enterprise-access-policies/){:target="_blank" rel="noopener"}

> As part of our efforts to democratize Zero Trust, Google Cloud has designed our BeyondCorp Enterprise solution to be an extensible platform where customers can choose to integrate signals from other technology vendors and incorporate these into their Zero Trust access policies. Following our integrations announcements earlier this year, we are excited to announce a new BeyondCorp Enterprise integration with Microsoft Intune, now available in Preview. This integration allows organizations to craft Zero Trust access policies and protect private applications and SaaS applications, including Office 365, based on data collected from the Intune graph API, including device posture and other [...]
