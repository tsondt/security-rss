Title: Fake cryptocurrency giveaway sites have tripled this year
Date: 2022-09-16T05:03:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-09-16-fake-cryptocurrency-giveaway-sites-have-tripled-this-year

[Source](https://www.bleepingcomputer.com/news/security/fake-cryptocurrency-giveaway-sites-have-tripled-this-year/){:target="_blank" rel="noopener"}

> The number of websites promoting cryptocurrency giveaway scams to lure gullible victims has increased by more than 300% in the first half of this year, targeting mostly English and Spanish speakers using celebrity deepfakes. [...]
