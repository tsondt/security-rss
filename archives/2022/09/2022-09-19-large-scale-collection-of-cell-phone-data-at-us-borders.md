Title: Large-Scale Collection of Cell Phone Data at US Borders
Date: 2022-09-19T11:07:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;borders;cell phones;privacy;searches;surveillance
Slug: 2022-09-19-large-scale-collection-of-cell-phone-data-at-us-borders

[Source](https://www.schneier.com/blog/archives/2022/09/large-scale-collection-of-cell-phone-data-at-us-borders.html){:target="_blank" rel="noopener"}

> The Washington Post is reporting that the US Customs and Border Protection agency is seizing and copying cell phone, tablet, and computer data from “as many as” 10,000 phones per year, including an unspecified number of American citizens. This is done without a warrant, because “...courts have long granted an exception to border authorities, allowing them to search people’s devices without a warrant or suspicion of a crime.” CBP’s inspection of people’s phones, laptops, tablets and other electronic devices as they enter the country has long been a controversial practice that the agency has defended as a low-impact way to [...]
