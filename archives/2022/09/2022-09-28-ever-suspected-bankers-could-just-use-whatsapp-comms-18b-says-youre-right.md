Title: Ever suspected bankers could just use WhatsApp comms? $1.8b says you're right
Date: 2022-09-28T13:00:11+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-09-28-ever-suspected-bankers-could-just-use-whatsapp-comms-18b-says-youre-right

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/28/shadow_it_hedge_funds_wall_street/){:target="_blank" rel="noopener"}

> Thought shadow IT at your office was bad? Try enforcing workplace device policies on hedge fund traders Updated Ever given a colleague a quick Signal call so you can sidestep a monitored workplace app? Well, we'd hope you're not in a highly regulated industry like staff at eleven of the world's most powerful financial firms, who yesterday were fined nearly $2 billion for off-channel comms.... [...]
