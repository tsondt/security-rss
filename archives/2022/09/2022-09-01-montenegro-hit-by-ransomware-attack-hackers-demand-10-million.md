Title: Montenegro hit by ransomware attack, hackers demand $10 million
Date: 2022-09-01T13:20:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-01-montenegro-hit-by-ransomware-attack-hackers-demand-10-million

[Source](https://www.bleepingcomputer.com/news/security/montenegro-hit-by-ransomware-attack-hackers-demand-10-million/){:target="_blank" rel="noopener"}

> The government of Montenegro has admitted that its previous allegations about Russian threat actors attacking critical infrastructure in the country were false and now blames ransomware for the damage to its IT infrastructure that has caused extensive service disruptions. [...]
