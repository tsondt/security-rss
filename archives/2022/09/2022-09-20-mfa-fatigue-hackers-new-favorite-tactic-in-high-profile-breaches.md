Title: MFA Fatigue: Hackers’ new favorite tactic in high-profile breaches
Date: 2022-09-20T06:30:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-20-mfa-fatigue-hackers-new-favorite-tactic-in-high-profile-breaches

[Source](https://www.bleepingcomputer.com/news/security/mfa-fatigue-hackers-new-favorite-tactic-in-high-profile-breaches/){:target="_blank" rel="noopener"}

> Hackers are more frequently using social engineering attacks to gain access to corporate credentials and breach large networks. One component of these attacks that is becoming more popular with the rise of multi-factor authentication is a technique called MFA Fatigue. [...]
