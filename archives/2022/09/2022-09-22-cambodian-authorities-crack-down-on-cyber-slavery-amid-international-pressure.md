Title: Cambodian authorities crack down on cyber slavery amid international pressure
Date: 2022-09-22T15:15:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-09-22-cambodian-authorities-crack-down-on-cyber-slavery-amid-international-pressure

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/22/cambodian_authorities_crack_down_on/){:target="_blank" rel="noopener"}

> Lured by fake jobs, victims are isolated abroad and forced to carry out crypto and romance scams, and more Authorities in Sihanoukville, Cambodia announced on Sunday that a raid last week uncovered evidence of forced labor cybercrime syndicates that participated in human trafficking and torture.... [...]
