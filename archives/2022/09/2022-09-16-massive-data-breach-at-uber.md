Title: Massive Data Breach at Uber
Date: 2022-09-16T14:07:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;data breaches;hacking;phishing;social engineering;Uber
Slug: 2022-09-16-massive-data-breach-at-uber

[Source](https://www.schneier.com/blog/archives/2022/09/massive-data-breach-at-uber.html){:target="_blank" rel="noopener"}

> It’s big : The breach appeared to have compromised many of Uber’s internal systems, and a person claiming responsibility for the hack sent images of email, cloud storage and code repositories to cybersecurity researchers and The New York Times. “They pretty much have full access to Uber,” said Sam Curry, a security engineer at Yuga Labs who corresponded with the person who claimed to be responsible for the breach. “This is a total compromise, from what it looks like.” It looks like a pretty basic phishing attack; someone gave the hacker their login credentials. And because Uber has lousy internal [...]
