Title: Twitter whistleblower Zatko disses bird site as dysfunctional data dump
Date: 2022-09-14T00:11:57+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-14-twitter-whistleblower-zatko-disses-bird-site-as-dysfunctional-data-dump

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/14/twitter_mudge_senate/){:target="_blank" rel="noopener"}

> Mudge tells senators his former bosses are 'terrified' of the French, US regulators are toothless Twitter's former head of security Peiter "Mudge" Zatko on Tuesday told the US Senate Judiciary Committee that the social media company's lax data handling and inability to present problems to its board of directors threaten the privacy, security, and democracy for Americans.... [...]
