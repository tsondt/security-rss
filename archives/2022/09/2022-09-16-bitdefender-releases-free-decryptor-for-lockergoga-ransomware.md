Title: Bitdefender releases free decryptor for LockerGoga ransomware
Date: 2022-09-16T11:09:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-16-bitdefender-releases-free-decryptor-for-lockergoga-ransomware

[Source](https://www.bleepingcomputer.com/news/security/bitdefender-releases-free-decryptor-for-lockergoga-ransomware/){:target="_blank" rel="noopener"}

> Romanian cybersecurity firm Bitdefender has released a free decryptor to help LockerGoga ransomware victims recover their files without paying a ransom. [...]
