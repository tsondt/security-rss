Title: NSA shares guidance to help secure OT/ICS critical infrastructure
Date: 2022-09-22T14:49:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-22-nsa-shares-guidance-to-help-secure-otics-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/nsa-shares-guidance-to-help-secure-ot-ics-critical-infrastructure/){:target="_blank" rel="noopener"}

> The National Security Agency (NSA) and CISA have issued guidance on how to secure operational technology (OT) and industrial control systems (ICSs) part of U.S. critical infrastructure. [...]
