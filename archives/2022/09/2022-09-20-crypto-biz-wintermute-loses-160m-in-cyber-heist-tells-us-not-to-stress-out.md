Title: Crypto biz Wintermute loses $160m in cyber-heist, tells us not to stress out
Date: 2022-09-20T21:35:27+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-20-crypto-biz-wintermute-loses-160m-in-cyber-heist-tells-us-not-to-stress-out

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/20/wintermute_hacked_160m/){:target="_blank" rel="noopener"}

> The other Tessier-Ashpool AIs are surely disappointed Cryptocurrency market maker Wintermute says $160 million in digital assets have been stolen from it in a cyber-heist, though it assures customers that everything's fine.... [...]
