Title: Samsung sued for gobbling up too much personal info that miscreants then stole
Date: 2022-09-27T18:15:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-27-samsung-sued-for-gobbling-up-too-much-personal-info-that-miscreants-then-stole

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/27/samsung_data_theft_lawsuit/){:target="_blank" rel="noopener"}

> If you're gonna force everyone to register an account, at least protect that data, lawsuit argues A lawsuit has accused Samsung of failing to address a cyber-intrusion in early 2022, leading to the theft of US customers' personally identifiable information (PII) in a second attack months later in July.... [...]
