Title: Meta disbands Responsible Innovation team, spreads it out over Facebook and co
Date: 2022-09-09T17:28:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-09-meta-disbands-responsible-innovation-team-spreads-it-out-over-facebook-and-co

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/09/meta_disbands_responsible_innovation_team/){:target="_blank" rel="noopener"}

> Still unclear: Were members just screaming into a void for the past few years? Facebook parent Meta has disbanded its Responsible Innovation Team (RIT) that it claimed last year was a central part of efforts to "proactively surface and address potential harms to society in all that we build."... [...]
