Title: Violence-as-a-Service: Brickings, Firebombings & Shootings for Hire
Date: 2022-09-04T14:59:13+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;SIM Swapping;Web Fraud 2.0;bricking;firebombing;molotov cocktail;Patrick McGovern-Allen;SIM swapping;SWATting;Tongue
Slug: 2022-09-04-violence-as-a-service-brickings-firebombings-shootings-for-hire

[Source](https://krebsonsecurity.com/2022/09/violence-as-a-service-brickings-firebombings-shootings-for-hire/){:target="_blank" rel="noopener"}

> A 21-year-old New Jersey man has been arrested and charged with stalking in connection with a federal investigation into groups of cybercriminals who are settling scores by hiring people to carry out physical attacks on their rivals. Prosecutors say the defendant recently participated in several of these schemes — including firing a handgun into a Pennsylvania home and torching a residence in another part of the state with a Molotov Cocktail. Patrick McGovern-Allen of Egg Harbor Township, N.J. was arrested on Aug. 12 on a warrant from the U.S. Federal Bureau of Investigation. An FBI complaint alleges McGovern-Allen was part [...]
