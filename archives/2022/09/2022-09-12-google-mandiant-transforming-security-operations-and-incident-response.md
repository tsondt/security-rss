Title: Google + Mandiant: Transforming Security Operations and Incident Response
Date: 2022-09-12T13:00:00+00:00
Author: Thomas Kurian
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-09-12-google-mandiant-transforming-security-operations-and-incident-response

[Source](https://cloud.google.com/blog/products/identity-security/google-completes-acquisition-of-mandiant/){:target="_blank" rel="noopener"}

> Over the past two decades, Google has innovated to build some of the largest and most secure computing systems in the world. This scale requires us to deliver pioneering approaches to cloud security, which we pass on to our Google Cloud customers. We are committed to solving hard security problems like only Google can, as the tip of the spear of innovation and threat intelligence. Today we’re excited to share the next step in this journey with the completion of our acquisition of Mandiant, a leader in dynamic cyber defense, threat intelligence and incident response services. Mandiant shares our cybersecurity [...]
