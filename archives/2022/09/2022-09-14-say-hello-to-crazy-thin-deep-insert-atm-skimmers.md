Title: Say Hello to Crazy Thin ‘Deep Insert’ ATM Skimmers
Date: 2022-09-14T21:46:58+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: All About Skimmers;deep insert skimmers;NCR;NCR SelfServ
Slug: 2022-09-14-say-hello-to-crazy-thin-deep-insert-atm-skimmers

[Source](https://krebsonsecurity.com/2022/09/say-hello-to-crazy-thin-deep-insert-atm-skimmers/){:target="_blank" rel="noopener"}

> A number of financial institutions in and around New York City are dealing with a rash of super-thin “deep insert” skimming devices designed to fit inside the mouth of an ATM’s card acceptance slot. The card skimmers are paired with tiny pinhole cameras that are cleverly disguised as part of the cash machine. Here’s a look at some of the more sophisticated deep insert skimmer technology that fraud investigators have recently found in the wild. This ultra thin and flexible “deep insert” skimmer recently recovered from an NCR cash machine in New York is about half the height of a [...]
