Title: Noberus ransomware gets info-stealing upgrades, targets Veeam backup software
Date: 2022-09-25T08:50:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-25-noberus-ransomware-gets-info-stealing-upgrades-targets-veeam-backup-software

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/25/noberus_ransomware_symantec/){:target="_blank" rel="noopener"}

> 'One of the most dangerous and active malware developers operating at the moment' Crooks spreading the Noberus ransomware are adding weapons to their malware to steal data and credentials from compromised networks.... [...]
