Title: Trojanized versions of PuTTY utility being used to spread backdoor
Date: 2022-09-16T00:37:16+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;malware;North Korea;putty
Slug: 2022-09-16-trojanized-versions-of-putty-utility-being-used-to-spread-backdoor

[Source](https://arstechnica.com/?p=1882005){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Researchers believe hackers with connections to the North Korean government have been pushing a Trojanized version of the PuTTY networking utility in an attempt to backdoor the network of organizations they want to spy on. Researchers from security firm Mandiant said on Thursday that at least one customer it serves had an employee who installed the fake network utility by accident. The incident caused the employer to become infected with a backdoor tracked by researchers as Airdry.v2. The file was transmitted by a group Mandiant tracks as UNC4034. "Mandiant identified several overlaps between UNC4034 and [...]
