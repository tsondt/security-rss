Title: Montenegro is the Victim of a Cyberattack
Date: 2022-09-02T13:18:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-09-02-montenegro-is-the-victim-of-a-cyberattack

[Source](https://www.schneier.com/blog/archives/2022/09/montenegro-is-the-victim-of-a-cyberattack.html){:target="_blank" rel="noopener"}

> Details are few, but Montenegro has suffered a cyberattack : A combination of ransomware and distributed denial-of-service attacks, the onslaught disrupted government services and prompted the country’s electrical utility to switch to manual control. [...] But the attack against Montenegro’s infrastructure seemed more sustained and extensive, with targets including water supply systems, transportation services and online government services, among many others. Government officials in the country of just over 600,000 people said certain government services remained temporarily disabled for security reasons and that the data of citizens and businesses were not endangered. The Director of the Directorate for Information Security, [...]
