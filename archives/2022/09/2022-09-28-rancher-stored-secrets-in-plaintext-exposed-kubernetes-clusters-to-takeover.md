Title: Rancher stored secrets in plaintext, exposed Kubernetes clusters to takeover
Date: 2022-09-28T14:08:44+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-28-rancher-stored-secrets-in-plaintext-exposed-kubernetes-clusters-to-takeover

[Source](https://portswigger.net/daily-swig/rancher-stored-secrets-in-plaintext-exposed-kubernetes-clusters-to-takeover){:target="_blank" rel="noopener"}

> Maintainers patch vulnerability and offer mitigation advice over bug that affects all Kubernetes objects [...]
