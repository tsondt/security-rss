Title: Cold War Bugging of Soviet Facilities
Date: 2022-09-28T11:19:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;espionage;history of security;Russia
Slug: 2022-09-28-cold-war-bugging-of-soviet-facilities

[Source](https://www.schneier.com/blog/archives/2022/09/cold-war-bugging-of-soviet-facilities.html){:target="_blank" rel="noopener"}

> Found documents in Poland detail US spying operations against the former Soviet Union. The file details a number of bugs found at Soviet diplomatic facilities in Washington, D.C., New York, and San Francisco, as well as in a Russian government-owned vacation compound, apartments used by Russia personnel, and even Russian diplomats’ cars. And the bugs were everywhere : encased in plaster in an apartment closet; behind electrical and television outlets; bored into concrete bricks and threaded into window frames; inside wooden beams and baseboards and stashed within a building’s foundation itself; surreptitiously attached to security cameras; wired into ceiling panels [...]
