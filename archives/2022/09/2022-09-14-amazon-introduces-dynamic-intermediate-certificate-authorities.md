Title: Amazon introduces dynamic intermediate certificate authorities
Date: 2022-09-14T22:02:40+00:00
Author: Adina Lozada
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;Amazon Trust Services;Certificate Authority;Certificate pinning;Security Blog;SSL;TLS
Slug: 2022-09-14-amazon-introduces-dynamic-intermediate-certificate-authorities

[Source](https://aws.amazon.com/blogs/security/amazon-introduces-dynamic-intermediate-certificate-authorities/){:target="_blank" rel="noopener"}

> AWS Certificate Manager (ACM) is a managed service that lets you provision, manage, and deploy public and private Secure Sockets Layer/Transport Layer Security (SSL/TLS) certificates for use with Amazon Web Services (AWS) and your internal connected resources. Starting October 11, 2022, at 9:00 AM Pacific Time, public certificates obtained through ACM will be issued from [...]
