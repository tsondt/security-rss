Title: Alert: 15-year-old Python tarfile flaw lurks in 'over 350,000' code projects
Date: 2022-09-22T01:16:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-22-alert-15-year-old-python-tarfile-flaw-lurks-in-over-350000-code-projects

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/22/python_vulnerability_tarfile/){:target="_blank" rel="noopener"}

> Oh cool, a 5,500-day security hole At least 350,000 open source projects are believed to be potentially vulnerable to exploitation via a Python module flaw that has remained unfixed for 15 years.... [...]
