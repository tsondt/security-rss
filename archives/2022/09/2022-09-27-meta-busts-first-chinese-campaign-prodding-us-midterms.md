Title: Meta busts first Chinese campaign prodding US midterms
Date: 2022-09-27T15:00:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-27-meta-busts-first-chinese-campaign-prodding-us-midterms

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/27/meta_chinese_campaign/){:target="_blank" rel="noopener"}

> Russian cybercriminals were also caught targeting Europe with anti-Ukraine messages Meta says it has disrupted a misinformation network targeting US political discourse ahead of the 2022 midterm elections – and one that sought to influence public opinion in Europe about the conflict in Ukraine.... [...]
