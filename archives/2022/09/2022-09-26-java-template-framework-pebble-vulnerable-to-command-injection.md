Title: Java template framework Pebble vulnerable to command injection
Date: 2022-09-26T13:06:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-26-java-template-framework-pebble-vulnerable-to-command-injection

[Source](https://portswigger.net/daily-swig/java-template-framework-pebble-vulnerable-to-command-injection){:target="_blank" rel="noopener"}

> Issue still yet to be patched, but workarounds are available [...]
