Title: Optus faces potential class action and pledges free credit monitoring to data-breach customers
Date: 2022-09-26T06:27:35+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Optus;Telecommunications industry;Data and computer security;Australia news
Slug: 2022-09-26-optus-faces-potential-class-action-and-pledges-free-credit-monitoring-to-data-breach-customers

[Source](https://www.theguardian.com/business/2022/sep/26/optus-faces-potential-class-action-and-pledges-free-credit-monitoring-to-data-breach-customers){:target="_blank" rel="noopener"}

> Home affairs minister Clare O’Neil says company to blame and flags new laws with large fines for such breaches Follow our Australia news live blog for the latest updates Get our free news app, morning email briefing or daily news podcast Optus has agreed to provide free credit monitoring to the millions of customers caught up in its massive data breach, as the home affairs minister flags changes to law to potentially fine companies millions for similar breaches. The company on Monday said it had informed all customers via email or SMS if they had had their passport or driver’s [...]
