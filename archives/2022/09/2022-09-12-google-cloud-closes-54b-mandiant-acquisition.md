Title: Google Cloud closes $5.4b Mandiant acquisition
Date: 2022-09-12T18:15:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-12-google-cloud-closes-54b-mandiant-acquisition

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/12/google_closes_mandiant_acquisition/){:target="_blank" rel="noopener"}

> Now it's really got all eyes on you Google closed its $5.4 billion Mandiant acquisition today in a move that brings the threat intel and incident response giant under the Google Cloud umbrella.... [...]
