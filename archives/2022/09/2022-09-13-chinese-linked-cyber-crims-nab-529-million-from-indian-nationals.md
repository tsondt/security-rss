Title: Chinese-linked cyber crims nab $529 million from Indian nationals
Date: 2022-09-13T05:30:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-09-13-chinese-linked-cyber-crims-nab-529-million-from-indian-nationals

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/13/chinese_cybercrime_hits_india/){:target="_blank" rel="noopener"}

> Authorities also bust a shell company scam operation with links to the Middle Kingdom Chinese scammers have reportedly stolen a whopping $529 million dollars from Indian residents using instant lending apps, lures of part-time jobs, and bogus cryptocurrency trading schemes, according to the cyber crime unit in the state of Uttar Pradesh.... [...]
