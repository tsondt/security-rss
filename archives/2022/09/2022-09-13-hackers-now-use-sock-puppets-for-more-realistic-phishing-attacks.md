Title: Hackers now use ‘sock puppets’ for more realistic phishing attacks
Date: 2022-09-13T17:23:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-13-hackers-now-use-sock-puppets-for-more-realistic-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-now-use-sock-puppets-for-more-realistic-phishing-attacks/){:target="_blank" rel="noopener"}

> An Iranian-aligned hacking group uses a new, elaborate phishing technique involving multiple personas and email accounts to lure targets into opening malicious documents. [...]
