Title: Ransomware gangs switching to new intermittent encryption tactic
Date: 2022-09-10T10:07:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-10-ransomware-gangs-switching-to-new-intermittent-encryption-tactic

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-switching-to-new-intermittent-encryption-tactic/){:target="_blank" rel="noopener"}

> A growing number of ransomware groups are adopting a new tactic that helps them encrypt their victims' systems faster while reducing the chances of being detected and stopped. [...]
