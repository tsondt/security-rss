Title: Attorney general says FBI is working on Optus data breach – video
Date: 2022-09-27T03:21:40+00:00
Author: 
Category: The Guardian
Tags: Optus;Australia news;FBI;Australian politics;Cybercrime;Data and computer security;Data protection
Slug: 2022-09-27-attorney-general-says-fbi-is-working-on-optus-data-breach-video

[Source](https://www.theguardian.com/business/video/2022/sep/27/attorney-general-says-fbi-is-working-on-optus-data-breach-video){:target="_blank" rel="noopener"}

> Attorney general Mark Dreyfus says the FBI is working with local authorities to investigate the Optus data breach. 'The government, as well as the Australian federal police and other government agencies, are working closely together on the Optus data breach,' he said. 'The Australian federal police is taking this very seriously with a large number of officers involved, working with other federal government agencies and state and territory police, and with the FBI in the United States and with industry.' Australia politics live Purported Optus hacker releases 10,000 records including email addresses from defence and prime minister’s office Get our [...]
