Title: New EvilProxy service lets all hackers use advanced phishing tactics
Date: 2022-09-05T13:44:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-05-new-evilproxy-service-lets-all-hackers-use-advanced-phishing-tactics

[Source](https://www.bleepingcomputer.com/news/security/new-evilproxy-service-lets-all-hackers-use-advanced-phishing-tactics/){:target="_blank" rel="noopener"}

> A reverse-proxy Phishing-as-a-Service (PaaS) platform called EvilProxy has emerged, promising to steal authentication tokens to bypass multi-factor authentication (MFA) on Apple, Google, Facebook, Microsoft, Twitter, GitHub, GoDaddy, and even PyPI. [...]
