Title: Weird Fallout from Peiter Zatko’s Twitter Whistleblowing
Date: 2022-09-14T11:51:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Twitter;whistleblowers
Slug: 2022-09-14-weird-fallout-from-peiter-zatkos-twitter-whistleblowing

[Source](https://www.schneier.com/blog/archives/2022/09/weird-fallout-from-peiter-zatkos-twitter-whistleblowing.html){:target="_blank" rel="noopener"}

> People are trying to dig up dirt on Peiter Zatko, better known as Mudge. For the record, I have not been contacted. I’m not sure if I should feel slighted. [...]
