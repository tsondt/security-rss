Title: WAAP it out for application security
Date: 2022-09-21T10:11:07+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-09-21-waap-it-out-for-application-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/21/waap_it_out_for_application/){:target="_blank" rel="noopener"}

> APIs are everywhere, and WAAP can help you protect them Webinar The latest Data Breach Investigations Report (DBIR) states that applications are the 'main attack vector,' responsible for over 80 percent of breaches. Hardly welcome news since APIs are in use everywhere and have direct access to data in a way which web applications do not.... [...]
