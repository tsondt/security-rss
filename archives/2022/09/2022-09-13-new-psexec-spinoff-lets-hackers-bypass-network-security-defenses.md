Title: New PsExec spinoff lets hackers bypass network security defenses
Date: 2022-09-13T09:37:01-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-09-13-new-psexec-spinoff-lets-hackers-bypass-network-security-defenses

[Source](https://www.bleepingcomputer.com/news/security/new-psexec-spinoff-lets-hackers-bypass-network-security-defenses/){:target="_blank" rel="noopener"}

> Security researchers have developed an implementation of the Sysinternals PsExec utility that allows moving laterally in a network using a less monitored port. [...]
