Title: White House to tech world: Promise you'll write secure code – or Feds won't use it
Date: 2022-09-14T21:24:42+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-14-white-house-to-tech-world-promise-youll-write-secure-code-or-feds-wont-use-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/14/white_house_software_security_guidance/){:target="_blank" rel="noopener"}

> Developers, why not simply build flawless software, thus solving all our vulnerability worries The White House has published software security rules for federal agencies as part of a larger push to shore up America's IT supply chains.... [...]
