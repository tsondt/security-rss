Title: FBI: Hackers steal millions from healthcare payment processors
Date: 2022-09-14T18:54:39-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-09-14-fbi-hackers-steal-millions-from-healthcare-payment-processors

[Source](https://www.bleepingcomputer.com/news/security/fbi-hackers-steal-millions-from-healthcare-payment-processors/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) has issued an alert about hackers targeting healthcare payment processors to route payments to bank accounts controlled by the attacker. [...]
