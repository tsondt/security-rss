Title: GTA 6 source code and videos leaked after Rockstar Games hack
Date: 2022-09-18T16:23:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-18-gta-6-source-code-and-videos-leaked-after-rockstar-games-hack

[Source](https://www.bleepingcomputer.com/news/security/gta-6-source-code-and-videos-leaked-after-rockstar-games-hack/){:target="_blank" rel="noopener"}

> Grand Theft Auto 6 gameplay videos and source code have been leaked after a hacker allegedly breached Rockstar Game's Slack server and Confluence wiki. [...]
