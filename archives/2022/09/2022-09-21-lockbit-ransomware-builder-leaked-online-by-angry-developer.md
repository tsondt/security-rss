Title: LockBit ransomware builder leaked online by “angry developer”
Date: 2022-09-21T14:07:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-21-lockbit-ransomware-builder-leaked-online-by-angry-developer

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-builder-leaked-online-by-angry-developer-/){:target="_blank" rel="noopener"}

> The LockBit ransomware operation has suffered a breach, with an allegedly disgruntled developer leaking the builder for the gang's newest encryptor. [...]
