Title: Hackers trojanize PuTTY SSH client to backdoor media company
Date: 2022-09-15T16:45:54-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-15-hackers-trojanize-putty-ssh-client-to-backdoor-media-company

[Source](https://www.bleepingcomputer.com/news/security/hackers-trojanize-putty-ssh-client-to-backdoor-media-company/){:target="_blank" rel="noopener"}

> North Korean hackers are using trojanized versions of the PuTTY SSH client to deploy backdoors on targets' devices as part of a fake Amazon job assessment. [...]
