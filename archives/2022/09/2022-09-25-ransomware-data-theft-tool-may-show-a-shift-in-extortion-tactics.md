Title: Ransomware data theft tool may show a shift in extortion tactics
Date: 2022-09-25T11:14:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-25-ransomware-data-theft-tool-may-show-a-shift-in-extortion-tactics

[Source](https://www.bleepingcomputer.com/news/security/ransomware-data-theft-tool-may-show-a-shift-in-extortion-tactics/){:target="_blank" rel="noopener"}

> Data exfiltration malware known as Exmatter and previously linked with the BlackMatter ransomware group is now being upgraded with data corruption functionality that may indicate a new tactic that ransomware affiliates might switch to in the future. [...]
