Title: Interpol dismantles sextortion ring, warns of increased attacks
Date: 2022-09-05T13:10:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-05-interpol-dismantles-sextortion-ring-warns-of-increased-attacks

[Source](https://www.bleepingcomputer.com/news/security/interpol-dismantles-sextortion-ring-warns-of-increased-attacks/){:target="_blank" rel="noopener"}

> A transnational sextortion ring was uncovered and dismantled following a joint investigation between Interpol's cybercrime division and police in Singapore and Hong Kong. [...]
