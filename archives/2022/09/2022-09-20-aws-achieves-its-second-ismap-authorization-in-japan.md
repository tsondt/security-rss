Title: AWS achieves its second ISMAP authorization in Japan
Date: 2022-09-20T23:00:48+00:00
Author: Hidetoshi Takeuchi
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS security;Compliance;ISMAP;Japan;Security;Security Blog
Slug: 2022-09-20-aws-achieves-its-second-ismap-authorization-in-japan

[Source](https://aws.amazon.com/blogs/security/aws-achieves-its-second-ismap-authorization-in-japan/){:target="_blank" rel="noopener"}

> Earning and maintaining customer trust is an ongoing commitment at Amazon Web Services (AWS). Our customers’ security requirements drive the scope and portfolio of the compliance reports, attestations, and certifications we pursue. We’re excited to announce that AWS has achieved authorization under the Information System Security Management and Assessment Program (ISMAP) program, effective from April [...]
