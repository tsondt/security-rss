Title: Eastern European org hit by second record-smashing DDoS attack
Date: 2022-09-16T06:04:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-16-eastern-european-org-hit-by-second-record-smashing-ddos-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/16/record_european_ddos_attack_akamai/){:target="_blank" rel="noopener"}

> Cough, cough, U, cough, kraine Akamai says it has absorbed the largest-ever publicly known distributed denial of service (DDoS) attack – an assault against an unfortunate Eastern European organization that went beyond 700 million packets per second.... [...]
