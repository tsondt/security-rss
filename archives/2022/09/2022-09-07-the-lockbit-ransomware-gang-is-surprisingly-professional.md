Title: The LockBit Ransomware Gang Is Surprisingly Professional
Date: 2022-09-07T14:26:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data loss;denial of service;extortion;ransomware
Slug: 2022-09-07-the-lockbit-ransomware-gang-is-surprisingly-professional

[Source](https://www.schneier.com/blog/archives/2022/09/the-lockbit-ransomware-gang-is-surprisingly-professional.html){:target="_blank" rel="noopener"}

> This article makes LockBit sound like a legitimate organization: The DDoS attack last weekend that put a temporary stop to leaking Entrust data was seen as an opportunity to explore the triple extortion tactic to apply more pressure on victims to pay a ransom. LockBitSupp said that the ransomware operator is now looking to add DDoS as an extortion tactic on top of encrypting data and leaking it. “I am looking for dudosers [DDoSers] in the team, most likely now we will attack targets and provide triple extortion, encryption + date leak + dudos, because I have felt the power [...]
