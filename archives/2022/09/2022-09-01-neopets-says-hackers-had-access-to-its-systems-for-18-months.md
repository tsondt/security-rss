Title: Neopets says hackers had access to its systems for 18 months
Date: 2022-09-01T07:14:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-01-neopets-says-hackers-had-access-to-its-systems-for-18-months

[Source](https://www.bleepingcomputer.com/news/security/neopets-says-hackers-had-access-to-its-systems-for-18-months/){:target="_blank" rel="noopener"}

> Neopets has released details about the recently disclosed data breach incident that exposed personal information of more than 69 million members. [...]
