Title: ‘Security teams often fight against developers taking control’ of AppSec: Tanya Janca on the drive to DevSecOps adoption
Date: 2022-09-19T15:12:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-19-security-teams-often-fight-against-developers-taking-control-of-appsec-tanya-janca-on-the-drive-to-devsecops-adoption

[Source](https://portswigger.net/daily-swig/security-teams-often-fight-against-developers-taking-control-of-appsec-tanya-janca-on-the-drive-to-devsecops-adoption){:target="_blank" rel="noopener"}

> Infosec advocate speaks to The Daily Swig about the benefits of, and barriers to, ‘shifting left’ [...]
