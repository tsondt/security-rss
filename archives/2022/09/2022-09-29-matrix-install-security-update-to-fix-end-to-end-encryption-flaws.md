Title: Matrix: Install security update to fix end-to-end encryption flaws
Date: 2022-09-29T14:32:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-29-matrix-install-security-update-to-fix-end-to-end-encryption-flaws

[Source](https://www.bleepingcomputer.com/news/security/matrix-install-security-update-to-fix-end-to-end-encryption-flaws/){:target="_blank" rel="noopener"}

> Matrix decentralized communication platform has published a security warning about two critical-severity vulnerabilities that affect the end-to-end encryption in the software development kit (SDK). [...]
