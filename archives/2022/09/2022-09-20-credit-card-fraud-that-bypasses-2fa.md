Title: Credit Card Fraud That Bypasses 2FA
Date: 2022-09-20T11:29:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;banking;credit cards;fraud;smartphones;two-factor authentication
Slug: 2022-09-20-credit-card-fraud-that-bypasses-2fa

[Source](https://www.schneier.com/blog/archives/2022/09/credit-card-fraud-that-bypasses-2fa.html){:target="_blank" rel="noopener"}

> Someone in the UK is stealing smartphones and credit cards from people who have stored them in gym lockers, and is using the two items in combination to commit fraud: Phones, of course, can be made inaccessible with the use of passwords and face or fingerprint unlocking. And bank cards can be stopped. But the thief has a method which circumnavigates those basic safety protocols. Once they have the phone and the card, they register the card on the relevant bank’s app on their own phone or computer. Since it is the first time that card will have been used [...]
