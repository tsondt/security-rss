Title: Trust Update: September 2022
Date: 2022-09-07T16:00:00+00:00
Author: Tanya Popova-Jones
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-09-07-trust-update-september-2022

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-trust-update-for-september-2022/){:target="_blank" rel="noopener"}

> If you work in compliance, privacy, or risk, you know that regulatory developments have continued to accelerate this year. As part of our commitment to be the most trusted cloud, we continue to pursue global industry standards, frameworks, and codes of conduct that tackle our customers’ foundational need for a documented baseline of addressable requirements. We have seen key updates across all regions and have worked to help organizations address these new and evolving requirements. Let's look at the significant updates from around the world, hot topics, and the requirements we’ve recently addressed. Global developments: Residency, portability, and more Google [...]
