Title: Brave browser to start blocking annoying cookie consent banners
Date: 2022-09-29T11:07:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2022-09-29-brave-browser-to-start-blocking-annoying-cookie-consent-banners

[Source](https://www.bleepingcomputer.com/news/security/brave-browser-to-start-blocking-annoying-cookie-consent-banners/){:target="_blank" rel="noopener"}

> The Brave browser will soon allows users to block annoying and potentially privacy-harming cookie consent banners on all websites they visit. [...]
