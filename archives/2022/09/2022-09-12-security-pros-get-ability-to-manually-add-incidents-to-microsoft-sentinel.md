Title: Security pros get ability to manually add incidents to Microsoft Sentinel
Date: 2022-09-12T16:01:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-12-security-pros-get-ability-to-manually-add-incidents-to-microsoft-sentinel

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/12/microsoft_sentinel_manual_siem_reports/){:target="_blank" rel="noopener"}

> *Tappity tappity* Yes the NSA's on the phone. Well maybe the automated log check didn't pick it up yet, Chad! In an IT world that is increasingly automated, there are still occasions when manual operations are necessary. According to Microsoft, one of these times is when security events are reported to enterprise security operation centers (SOCs).... [...]
