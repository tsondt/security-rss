Title: Open up, it's the IRS. We're here about the crypto tax you dodged
Date: 2022-09-23T19:25:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-23-open-up-its-the-irs-were-here-about-the-crypto-tax-you-dodged

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/23/irs_cryptocurrency_income_tax/){:target="_blank" rel="noopener"}

> 'At least ten' people didn't declare coin income. Wow, what a bust The IRS has been granted a court order to collect records from a bank the agency said will help it identify US taxpayers who failed to report taxable income from crypto trades.... [...]
