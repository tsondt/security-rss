Title: Cloud CISO Perspectives: September 2022
Date: 2022-09-30T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-09-30-cloud-ciso-perspectives-september-2022

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-september-2022/){:target="_blank" rel="noopener"}

> Welcome to September’s Cloud CISO Perspectives. This month, we're focusing on Google Cloud’s acquisition of Mandiant and what it means for us and the broader cybersecurity community. Mandiant has long been recognized as a leader in dynamic cyber defense, threat intelligence, and incident response services. As I explain below, integrating their technology and intelligence with Google Cloud’s will help improve our ability to stop threats and to modernize the overall state of security operations faster than ever before. As with all Cloud CISO Perspectives, the contents of this newsletter will continue to be posted to the Google Cloud blog. If [...]
