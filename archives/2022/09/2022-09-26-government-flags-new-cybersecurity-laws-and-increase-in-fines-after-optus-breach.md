Title: Government flags new cybersecurity laws and increase in fines after Optus breach
Date: 2022-09-26T17:30:07+00:00
Author: Sarah Martin and Paul Karp
Category: The Guardian
Tags: Optus;Cybercrime;Australia news;Telecommunications industry;Data and computer security
Slug: 2022-09-26-government-flags-new-cybersecurity-laws-and-increase-in-fines-after-optus-breach

[Source](https://www.theguardian.com/business/2022/sep/27/government-flags-new-cybersecurity-laws-and-increase-in-fines-after-optus-breach){:target="_blank" rel="noopener"}

> Clare O’Neil says penalties for telcos are ‘totally inappropriate’ and data breach was ‘significant error’ Follow today’s news, live Optus data security breach: what should I do to protect myself? Get our free news app, morning email briefing or daily news podcast The Albanese government will pursue “very substantial” reforms in the wake of the massive Optus data breach, including increasing penalties under the Privacy Act that are currently capped at $2.2m. As the government flags it will push ahead with legislative changes, hundreds of public servants from the Australian Signals Directorate, the Australian Cyber Security Centre and the Australian [...]
