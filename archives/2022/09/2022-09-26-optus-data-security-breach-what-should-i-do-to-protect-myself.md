Title: Optus data security breach: what should I do to protect myself?
Date: 2022-09-26T06:44:09+00:00
Author: Natasha May
Category: The Guardian
Tags: Optus;Australia news;Identity fraud;Data and computer security;Scams;Telecoms;Consumer affairs
Slug: 2022-09-26-optus-data-security-breach-what-should-i-do-to-protect-myself

[Source](https://www.theguardian.com/business/2022/sep/26/optus-data-security-breach-what-should-i-do-to-protect-myself){:target="_blank" rel="noopener"}

> Experts say while ‘there’s no need to panic’, there are steps you can take to ensure you’re not exposed to scams or identity theft Follow our Australia news live blog for the latest updates Get our free news app, morning email briefing or daily news podcast After Optus revealed its massive data security breach on Thursday, customers started receiving emails informing them that their personal information had been accessed. The telco said that while no financial information or passwords were accessed, the breach has seen customers’ names, dates of birth, email addresses, phone numbers, addresses associated with their account, and [...]
