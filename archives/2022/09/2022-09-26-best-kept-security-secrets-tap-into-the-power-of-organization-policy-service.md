Title: Best Kept Security Secrets: Tap into the power of Organization Policy Service
Date: 2022-09-26T16:00:00+00:00
Author: Seth Rosenblatt
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-09-26-best-kept-security-secrets-tap-into-the-power-of-organization-policy-service

[Source](https://cloud.google.com/blog/products/identity-security/best-kept-security-secrets-harness-the-power-of-organization-policy-service/){:target="_blank" rel="noopener"}

> The canvas of cloud resources is vast, ready for an ambitious organization to craft their digital masterpiece (or perhaps just their business.) Yet before the first brush of paint is applied, a painter in the cloud needs to think about their frame: What shape should it take, what material is it made of, how will it look as a border against the canvas of their cloud service. Google Cloud’s Organization Policy Service is just such a frame, a broad set of tools for our customer’s security teams to set broad yet unbendable limits for engineers before they start working. Google [...]
