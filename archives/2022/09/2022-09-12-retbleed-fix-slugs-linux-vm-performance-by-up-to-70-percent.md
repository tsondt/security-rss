Title: Retbleed fix slugs Linux VM performance by up to 70 percent
Date: 2022-09-12T01:29:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-12-retbleed-fix-slugs-linux-vm-performance-by-up-to-70-percent

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/12/retbleed_slugs_vm_performance_by/){:target="_blank" rel="noopener"}

> VMware ran tests on kernel 5.19 and saw some nasty numbers. Meanwhile progress on version 6.0 is steady VMware engineers have tested the Linux kernel's fix for the Retbleed speculative execution bug, and report it can impact compute performance by a whopping 70 percent.... [...]
