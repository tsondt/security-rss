Title: Microsoft bets on hardware/software duo for Win11 security
Date: 2022-09-27T11:32:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-27-microsoft-bets-on-hardwaresoftware-duo-for-win11-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/27/microsoft_windows11_security_hardware/){:target="_blank" rel="noopener"}

> But you'll need to buy lots of new hardware to get the benefit Analysis As it rolled out the laundry list of new features in Windows 11, version 22H2 this week, Microsoft also unveiled the configuration baseline that systems will have to meet to take advantage of the latest security capabilities.... [...]
