Title: Sophisticated attack or human error? How Optus lost control of your data
Date: 2022-09-27T23:57:34+00:00
Author: Presented by Jane Lee; reported by Josh Taylor; produced by Joe Koning and Karishma Luthria; sound design and mixing by Daniel Semo. Executive producers Miles Martignoni, Gabrielle Jackson, Molly Glassey and Laura Murphy-Oates
Category: The Guardian
Tags: Australia news;Optus;Data and computer security;Privacy;Politics and technology;Cryptocurrencies;Technology
Slug: 2022-09-27-sophisticated-attack-or-human-error-how-optus-lost-control-of-your-data

[Source](https://www.theguardian.com/australia-news/audio/2022/sep/28/sophisticated-attack-or-human-error-how-optus-lost-control-of-your-data){:target="_blank" rel="noopener"}

> In the days since Optus first reported that potentially millions of its customers’ private information – from birth dates to Medicare numbers – had been breached, it has faced threats of blackmail, a potential class action and a public spat with the home affairs minister. Reporter Josh Taylor and Jane Lee discuss the fallout from the data breach and whether this was a ‘sophisticated attack’ on the telco, or a failure of the company’s own security systems Follow the day’s news, live Read more: Continue reading... [...]
