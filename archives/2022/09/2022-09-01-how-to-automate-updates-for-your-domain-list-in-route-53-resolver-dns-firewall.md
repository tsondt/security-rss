Title: How to automate updates for your domain list in Route 53 Resolver DNS Firewall
Date: 2022-09-01T18:43:07+00:00
Author: Guillaume Neau
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon Route 53;Amazon VPC;Automation;DNS firewall;firewall;network protection;rpz;Security;Security Blog;Serverless
Slug: 2022-09-01-how-to-automate-updates-for-your-domain-list-in-route-53-resolver-dns-firewall

[Source](https://aws.amazon.com/blogs/security/how-to-automate-updates-for-your-domain-list-in-route-53-resolver-dns-firewall/){:target="_blank" rel="noopener"}

> Note: This post includes links to third-party websites. AWS is not responsible for the content on those websites. Following the release of Amazon Route 53 Resolver DNS Firewall, Amazon Web Services (AWS) published several blog posts to help you protect your Amazon Virtual Private Cloud (Amazon VPC) DNS resolution, including How to Get Started with [...]
