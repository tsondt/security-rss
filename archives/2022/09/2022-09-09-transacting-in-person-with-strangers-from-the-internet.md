Title: Transacting in Person with Strangers from the Internet
Date: 2022-09-09T12:40:03+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Internet Purchase & Exchange Location;safeexchangepoint.com;safetradeoptions.com
Slug: 2022-09-09-transacting-in-person-with-strangers-from-the-internet

[Source](https://krebsonsecurity.com/2022/09/transacting-in-person-with-strangers-from-the-internet/){:target="_blank" rel="noopener"}

> Communities like Craigslist, OfferUp, Facebook Marketplace and others are great for finding low- or no-cost stuff that one can pick up directly from a nearby seller, and for getting rid of useful things that don’t deserve to end up in a landfill. But when dealing with strangers from the Internet, there is always a risk that the person you’ve agreed to meet has other intentions. Nearly all U.S. states now have designated safe trading stations — mostly at local police departments — which ensure that all transactions are handled in plain view of both the authorities and security cameras. These [...]
