Title: BlackCat ransomware’s data exfiltration tool gets an upgrade
Date: 2022-09-22T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-22-blackcat-ransomwares-data-exfiltration-tool-gets-an-upgrade

[Source](https://www.bleepingcomputer.com/news/security/blackcat-ransomware-s-data-exfiltration-tool-gets-an-upgrade/){:target="_blank" rel="noopener"}

> The BlackCat ransomware (aka ALPHV) isn't showing any signs of slowing down, and the latest example of its evolution is a new version of the gang's data exfiltration tool used for double-extortion attacks. [...]
