Title: Windows 11 now warns when typing your password in Notepad, websites
Date: 2022-09-24T12:54:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-09-24-windows-11-now-warns-when-typing-your-password-in-notepad-websites

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-11-now-warns-when-typing-your-password-in-notepad-websites/){:target="_blank" rel="noopener"}

> Windows 11 22H2 was just released, and with it comes a new security feature called Enhanced Phishing Protection that warns users when they enter their Windows password in insecure applications or on websites. [...]
