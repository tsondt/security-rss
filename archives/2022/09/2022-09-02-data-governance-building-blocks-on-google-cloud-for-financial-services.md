Title: Data governance building blocks on Google Cloud for financial services
Date: 2022-09-02T16:00:00+00:00
Author: Oscar Pulido
Category: GCP Security
Tags: Identity & Security;Financial Services;Data Analytics
Slug: 2022-09-02-data-governance-building-blocks-on-google-cloud-for-financial-services

[Source](https://cloud.google.com/blog/products/data-analytics/achieving-data-governance-for-financial-services/){:target="_blank" rel="noopener"}

> Data governance includes people, processes, and technology. Together, these principles enable organizations to validate and manage across dimensions such as: Data management, including data and pipelines lifecycle management and master data management. Data protection, spanning data access management, data masking and encryption, along with audit and compliance. Data discoverability, including data cataloging, data quality assurance, and data lineage registration and administration. Data accountability, with data user identification and policies management requirements. While prioritizing investment in their people to achieve the desired cultural transformation and processes to increase operational effectiveness and efficiency will help enterprises, the technology pillar is the critical [...]
