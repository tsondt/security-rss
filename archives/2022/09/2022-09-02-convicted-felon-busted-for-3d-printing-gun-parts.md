Title: Convicted felon busted for 3D printing gun parts
Date: 2022-09-02T20:24:52+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-02-convicted-felon-busted-for-3d-printing-gun-parts

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/02/3d_printing_guns/){:target="_blank" rel="noopener"}

> Just days after US rules tackling homemade firearms take effect A US man has admitted he broke the law when he used 3D printers to make components converting semi-automatic guns to full auto.... [...]
