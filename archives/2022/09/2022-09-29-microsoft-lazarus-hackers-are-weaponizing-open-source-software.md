Title: Microsoft: Lazarus hackers are weaponizing open-source software
Date: 2022-09-29T13:33:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-09-29-microsoft-lazarus-hackers-are-weaponizing-open-source-software

[Source](https://www.bleepingcomputer.com/news/security/microsoft-lazarus-hackers-are-weaponizing-open-source-software/){:target="_blank" rel="noopener"}

> Microsoft says the North Korean-sponsored Lazarus threat group is trojanizing legitimate open-source software and using it to backdoor organizations in many industry sectors, such as technology, defense, and media entertainment. [...]
