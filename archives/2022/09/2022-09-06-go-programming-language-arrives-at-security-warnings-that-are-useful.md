Title: Go programming language arrives at security warnings that are useful
Date: 2022-09-06T22:40:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-06-go-programming-language-arrives-at-security-warnings-that-are-useful

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/06/go_govulncheck_vulnerability_tool/){:target="_blank" rel="noopener"}

> Low-noise tool hopes to highlight vulnerabilities imported into projects The open source Go programming language, developed by Google, has added support for vulnerability management in a way designed to preserve programmers' patience.... [...]
