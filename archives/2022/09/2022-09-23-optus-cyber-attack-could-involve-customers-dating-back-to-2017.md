Title: Optus cyber-attack could involve customers dating back to 2017
Date: 2022-09-23T03:04:11+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Optus;Telecommunications industry;Business;Australia news;Data and computer security;Cybercrime;Technology
Slug: 2022-09-23-optus-cyber-attack-could-involve-customers-dating-back-to-2017

[Source](https://www.theguardian.com/business/2022/sep/23/optus-cyber-attack-hack-data-breach-hacked-could-involve-customers-dating-back-to-2017){:target="_blank" rel="noopener"}

> CEO says company has not yet confirmed how many people were affected by hack, but 9.8 million was ‘worst case scenario’ Get our free news app, morning email briefing or daily news podcast Optus customers dating as far back as 2017 could be caught up in the massive hack of the telecommunications company’s database, CEO Kelly Bayer Rosmarin has revealed. Bayer Rosmarin told reporters on Friday that the company is still not sure exactly how many customers had their personal information compromised in the attack, but that 9.8 million was the “worst case scenario”. Sign up to receive an email [...]
