Title: Reducing the risk of human error in cyber security
Date: 2022-09-28T13:56:08+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-09-28-reducing-the-risk-of-human-error-in-cyber-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/28/reducing_the_risk_of_human/){:target="_blank" rel="noopener"}

> Tips on how to turn a potential weakness into a towering strength Webinar We all make mistakes. Some happy accidents enhance the way we live. Matches were invented when scientist John Walker was cleaning his laboratory with a wooden stick coated in chemicals and it caught fire. But if you are trying to secure your data, unforced errors are the last thing you need to torch it.... [...]
