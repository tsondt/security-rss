Title: WordPress-powered sites backdoored after FishPig suffers supply chain attack
Date: 2022-09-15T02:12:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-15-wordpress-powered-sites-backdoored-after-fishpig-suffers-supply-chain-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/15/magento_wordpress_fishpig/){:target="_blank" rel="noopener"}

> And two other security snafus in this web publishing world It's only been a week or so, and obviously there are at least three critical holes in WordPress plugins and tools that are being exploited in the wild right now to compromise loads of websites.... [...]
