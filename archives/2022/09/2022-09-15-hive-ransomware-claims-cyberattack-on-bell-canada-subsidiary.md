Title: Hive ransomware claims cyberattack on Bell Canada subsidiary
Date: 2022-09-15T15:10:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-15-hive-ransomware-claims-cyberattack-on-bell-canada-subsidiary

[Source](https://www.bleepingcomputer.com/news/security/hive-ransomware-claims-cyberattack-on-bell-canada-subsidiary/){:target="_blank" rel="noopener"}

> The Hive ransomware gang claimed responsibility for an attack that hit the systems of Bell Canada subsidiary Bell Technical Solutions (BTS). [...]
