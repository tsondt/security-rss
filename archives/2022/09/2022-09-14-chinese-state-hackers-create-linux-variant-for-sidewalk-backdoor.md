Title: Chinese state hackers create Linux variant for SideWalk backdoor
Date: 2022-09-14T08:07:28-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-09-14-chinese-state-hackers-create-linux-variant-for-sidewalk-backdoor

[Source](https://www.bleepingcomputer.com/news/security/chinese-state-hackers-create-linux-variant-for-sidewalk-backdoor/){:target="_blank" rel="noopener"}

> State-backed Chinese hackers have developed a Linux variant for the SideWalk backdoor used against Windows systems belonging to targets in the academic sector. [...]
