Title: North Korean Lazarus hackers take aim at U.S. energy providers
Date: 2022-09-08T08:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-08-north-korean-lazarus-hackers-take-aim-at-us-energy-providers

[Source](https://www.bleepingcomputer.com/news/security/north-korean-lazarus-hackers-take-aim-at-us-energy-providers/){:target="_blank" rel="noopener"}

> The North Korean APT group 'Lazarus' (APT38) is exploiting VMWare Horizon servers to access the corporate networks of energy providers in the United States, Canada, and Japan. [...]
