Title: Iran blocks Whatsapp, Instagram as citizens protest death of Mahsa Amini
Date: 2022-09-23T15:24:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-09-23-iran-blocks-whatsapp-instagram-as-citizens-protest-death-of-mahsa-amini

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/23/whatsapp_and_instagram_now_restricted/){:target="_blank" rel="noopener"}

> Also: New 'magnet of threats' attackers and FBI has details on Iran's online incursion into Albania Iran is experiencing a near-total internet service disruption in the west and intermittent interruptions nationwide, with access to Instagram, Whatsapp and some mobile networks being blocked, says Netblocks.... [...]
