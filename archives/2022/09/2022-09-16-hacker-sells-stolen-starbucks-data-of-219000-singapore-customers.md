Title: Hacker sells stolen Starbucks data of 219,000 Singapore customers
Date: 2022-09-16T11:53:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-16-hacker-sells-stolen-starbucks-data-of-219000-singapore-customers

[Source](https://www.bleepingcomputer.com/news/security/hacker-sells-stolen-starbucks-data-of-219-000-singapore-customers/){:target="_blank" rel="noopener"}

> The Singapore division of Starbucks, the popular American coffeehouse chain, has admitted that it suffered a data breach incident impacting over 219,000 of its customers. [...]
