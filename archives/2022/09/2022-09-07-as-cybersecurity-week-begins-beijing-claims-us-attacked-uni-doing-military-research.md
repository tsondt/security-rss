Title: As Cybersecurity Week begins, Beijing claims US attacked Uni doing military research
Date: 2022-09-07T05:15:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-07-as-cybersecurity-week-begins-beijing-claims-us-attacked-uni-doing-military-research

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/07/china_accuses_usa_nsa_cyberattack/){:target="_blank" rel="noopener"}

> National Security Agency apparently has tools that crack Solaris boxes China has accused the United States of a savage cyber attack on a university famed for conducting aerospace research and linked to China's military.... [...]
