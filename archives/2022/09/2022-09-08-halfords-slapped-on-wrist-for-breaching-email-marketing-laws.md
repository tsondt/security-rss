Title: Halfords slapped on wrist for breaching email marketing laws
Date: 2022-09-08T09:27:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-09-08-halfords-slapped-on-wrist-for-breaching-email-marketing-laws

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/08/halfords_ico_email_breach_pecr_fine/){:target="_blank" rel="noopener"}

> Bike and car accessory slinger fined £30,000 for hitting send on more than 499k unsolicited emails Bike and car accessory retailer Halfords has found itself in the wrong lane with Britain’s data watchdog for sending hundreds of thousands of unsolicited marketing emails to members of the public.... [...]
