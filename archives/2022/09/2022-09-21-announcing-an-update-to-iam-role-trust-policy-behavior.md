Title: Announcing an update to IAM role trust policy behavior
Date: 2022-09-21T21:36:32+00:00
Author: Mark Ryland
Category: AWS Security
Tags: Announcements;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;AssumeRole;IAM;Role Trust Policy;Security Blog;STS
Slug: 2022-09-21-announcing-an-update-to-iam-role-trust-policy-behavior

[Source](https://aws.amazon.com/blogs/security/announcing-an-update-to-iam-role-trust-policy-behavior/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) is changing an aspect of how role trust policy evaluation behaves when a role assumes itself. Previously, roles implicitly trusted themselves from a role trust policy perspective if they had identity-based permissions to assume themselves. After receiving and considering feedback from customers on this topic, AWS is changing role [...]
