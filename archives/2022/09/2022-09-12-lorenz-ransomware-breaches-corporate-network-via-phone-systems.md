Title: Lorenz ransomware breaches corporate network via phone systems
Date: 2022-09-12T12:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-12-lorenz-ransomware-breaches-corporate-network-via-phone-systems

[Source](https://www.bleepingcomputer.com/news/security/lorenz-ransomware-breaches-corporate-network-via-phone-systems/){:target="_blank" rel="noopener"}

> The Lorenz ransomware gang now uses a critical vulnerability in Mitel MiVoice VOIP appliances to breach enterprises using their phone systems for initial access to their corporate networks. [...]
