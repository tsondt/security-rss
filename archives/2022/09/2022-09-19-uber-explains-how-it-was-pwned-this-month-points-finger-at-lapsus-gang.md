Title: Uber explains how it was pwned this month, points finger at Lapsus$ gang
Date: 2022-09-19T22:54:54+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-19-uber-explains-how-it-was-pwned-this-month-points-finger-at-lapsus-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/19/uber_admits_breach/){:target="_blank" rel="noopener"}

> From annoying MFA alerts to 'several internal systems' infiltrated Uber, four days after suffering a substantial cybersecurity breach, has admitted its attacker accessed "several internal systems" including the corporation's G Suite account, and downloaded internal Slack messages and a tool used by its finance department to manage "some" invoices.... [...]
