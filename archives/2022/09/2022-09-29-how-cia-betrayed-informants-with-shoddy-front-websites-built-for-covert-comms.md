Title: How CIA betrayed informants with shoddy front websites built for covert comms
Date: 2022-09-29T23:03:55+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-29-how-cia-betrayed-informants-with-shoddy-front-websites-built-for-covert-comms

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/29/us_spy_catastrophe_reported_in/){:target="_blank" rel="noopener"}

> Top tip, don't give your secret login box the HTML form type 'password' For almost a decade, the US Central Intelligence Agency communicated with informants abroad using a network of websites with hidden communications capabilities.... [...]
