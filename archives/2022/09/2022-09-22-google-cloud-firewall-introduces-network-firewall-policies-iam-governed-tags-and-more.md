Title: Google Cloud Firewall introduces Network Firewall Policies, IAM-governed Tags and more
Date: 2022-09-22T16:00:00+00:00
Author: Albert Colas Prunera
Category: GCP Security
Tags: Networking;Google Cloud;Identity & Security
Slug: 2022-09-22-google-cloud-firewall-introduces-network-firewall-policies-iam-governed-tags-and-more

[Source](https://cloud.google.com/blog/products/identity-security/announcing-new-firewall-policies-and-iam-governed-tags/){:target="_blank" rel="noopener"}

> The network security that firewalls provide is one of the basic building blocks for a secure cloud infrastructure. We are excited to announce that three new Google Cloud Firewall features are now generally available: Global Network Firewall Policies, Regional Network Firewall Policies, and IAM-governed Tags. With these enhancements, Cloud Firewall can help you more easily achieve a Zero Trust network posture with a fully distributed, cloud-native stateful inspection firewall service. In the new Global and Regional Network Firewall Policy structures, we expanded the policy structure down to the Virtual Private Cloud (VPC) level, and made it easier and more scalable [...]
