Title: Albania blames Iran for July cyberattack, severs diplomatic ties
Date: 2022-09-07T08:37:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-07-albania-blames-iran-for-july-cyberattack-severs-diplomatic-ties

[Source](https://www.bleepingcomputer.com/news/security/albania-blames-iran-for-july-cyberattack-severs-diplomatic-ties/){:target="_blank" rel="noopener"}

> Albanian Prime Minister Edi Rama announced on Wednesday that the entire staff of the Embassy of the Islamic Republic of Iran was asked to leave within 24 hours. [...]
