Title: Fired admin cripples former employer's network using old credentials
Date: 2022-09-29T16:45:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-09-29-fired-admin-cripples-former-employers-network-using-old-credentials

[Source](https://www.bleepingcomputer.com/news/security/fired-admin-cripples-former-employers-network-using-old-credentials/){:target="_blank" rel="noopener"}

> An IT system administrator of a prominent financial company based in Hawaii, U.S., used a pair of credentials that hadn't been invalidated after he was laid off to wreak havoc on his employer. [...]
