Title: San Francisco cops can use private cameras to live-monitor 'significant events'
Date: 2022-09-21T23:52:21+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-21-san-francisco-cops-can-use-private-cameras-to-live-monitor-significant-events

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/21/san_francisco_private_cameras/){:target="_blank" rel="noopener"}

> All eyes on you, and you, and you San Francisco police are now set to use non-city-owned video cameras for real-time surveillance under a rule approved by the Board of Supervisors.... [...]
