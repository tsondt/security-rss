Title: GPT-3 'prompt injection' attack causes bad bot manners
Date: 2022-09-19T13:37:53+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-19-gpt-3-prompt-injection-attack-causes-bad-bot-manners

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/19/in_brief_security/){:target="_blank" rel="noopener"}

> Also, EA goes kernel-deep to stop cheaters, PuTTY gets hijacked by North Korea, and more. In Brief OpenAI's popular natural language model GPT-3 has a problem: It can be tricked into behaving badly by doing little more than telling it to ignore its previous orders.... [...]
