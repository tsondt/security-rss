Title: Mandiant links APT42 to Iranian 'terrorist org'
Date: 2022-09-07T14:00:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-07-mandiant-links-apt42-to-iranian-terrorist-org

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/07/mandiant_apt42_irgc/){:target="_blank" rel="noopener"}

> 'It's hard to imagine a more dangerous scenario,' Mandiant Intel VP told The Reg Mandiant has named a new threat group, APT42, that it says functions as the cyberspy arm of Iran's Islamic Revolutionary Guard Corps (IRGC), which has plotted to murder US citizens including former National Security Advisor John Bolton.... [...]
