Title: Microsoft: Two New 0-Day Flaws in Exchange Server
Date: 2022-09-30T16:51:57+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Time to Patch;CVE-2022-41040;CVE-2022-41082;GTSC;Microsoft Exchange Server zero-day;Steven Adair;Volexity;Zimbra Collaboration Suite
Slug: 2022-09-30-microsoft-two-new-0-day-flaws-in-exchange-server

[Source](https://krebsonsecurity.com/2022/09/microsoft-two-new-0-day-flaws-in-exchange-server/){:target="_blank" rel="noopener"}

> Microsoft Corp. is investigating reports that attackers are exploiting two previously unknown vulnerabilities in Exchange Server, a technology many organizations rely on to send and receive email. Microsoft says it is expediting work on software patches to plug the security holes. In the meantime, it is urging a subset of Exchange customers to enable a setting that could help mitigate ongoing attacks. In customer guidance released Thursday, Microsoft said it is investigating two reported zero-day flaws affecting Microsoft Exchange Server 2013, 2016, and 2019. CVE-2022-41040, is a Server-Side Request Forgery (SSRF) vulnerability that can enable an authenticated attacker to remotely [...]
