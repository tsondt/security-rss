Title: Fake CISO Profiles on LinkedIn Target Fortune 500s
Date: 2022-09-29T20:52:43+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;The Coming Storm;Web Fraud 2.0;fake CISOs;LinkedIn;Rich Mason
Slug: 2022-09-29-fake-ciso-profiles-on-linkedin-target-fortune-500s

[Source](https://krebsonsecurity.com/2022/09/fake-ciso-profiles-on-linkedin-target-fortune-500s/){:target="_blank" rel="noopener"}

> Someone has recently created a large number of fake LinkedIn profiles for Chief Information Security Officer (CISO) roles at some of the world’s largest corporations. It’s not clear who’s behind this network of fake CISOs or what their intentions may be. But the fabricated LinkedIn identities are confusing search engine results for CISO roles at major companies, and they are being indexed as gospel by various downstream data-scraping sources. If one searches LinkedIn for the CISO of the energy giant Chevron, one might find the profile for a Victor Sites, who says he’s from Westerville, Ohio and is a graduate [...]
