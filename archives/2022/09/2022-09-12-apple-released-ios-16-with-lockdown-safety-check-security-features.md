Title: Apple released iOS 16 with Lockdown, Safety Check security features
Date: 2022-09-12T10:20:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2022-09-12-apple-released-ios-16-with-lockdown-safety-check-security-features

[Source](https://www.bleepingcomputer.com/news/apple/apple-released-ios-16-with-lockdown-safety-check-security-features/){:target="_blank" rel="noopener"}

> Apple released iOS 16 today with new features to boost iPhone users' security and privacy, including Lockdown Mode and Security Check. [...]
