Title: 'I Don't Care About Cookies' extension sold to Avast
Date: 2022-09-21T14:15:08+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-09-21-i-dont-care-about-cookies-extension-sold-to-avast

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/21/avast_buys_i_dont_care_about_cookies_addon/){:target="_blank" rel="noopener"}

> Users of cookie-warning-buster add-on already forking off due to privacy concerns The lone developer of anti-cookie-warning browser add-on "I Don't Care About Cookies" has sold it to Avast, resulting in both concern – and new forks.... [...]
