Title: Patching common vulnerabilities at scale: project promises bulk pull requests
Date: 2022-09-29T13:46:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-29-patching-common-vulnerabilities-at-scale-project-promises-bulk-pull-requests

[Source](https://portswigger.net/daily-swig/patching-common-vulnerabilities-at-scale-project-promises-bulk-pull-requests){:target="_blank" rel="noopener"}

> Automating bulk pull request generation FTW [...]
