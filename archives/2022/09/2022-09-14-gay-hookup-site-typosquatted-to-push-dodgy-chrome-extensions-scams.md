Title: Gay hookup site typosquatted to push dodgy Chrome extensions, scams
Date: 2022-09-14T14:15:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-09-14-gay-hookup-site-typosquatted-to-push-dodgy-chrome-extensions-scams

[Source](https://www.bleepingcomputer.com/news/security/gay-hookup-site-typosquatted-to-push-dodgy-chrome-extensions-scams/){:target="_blank" rel="noopener"}

> Gay hookup and cruising web app Sniffies is being impersonated by opportunistic threat actors hoping to target the website's users with many typosquatting domains that push scams and dubious Google Chrome extensions. In some cases, these illicit domains launch the Apple Music app prompting users to buy a subscription. [...]
