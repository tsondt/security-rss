Title: Implement step-up authentication with Amazon Cognito, Part 2: Deploy and test the solution
Date: 2022-09-07T17:38:54+00:00
Author: Salman Moghal
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;adaptive authentication;API Gateway;api security;Cognito;DynamoDB;lambda;Serverless;step-up authentication
Slug: 2022-09-07-implement-step-up-authentication-with-amazon-cognito-part-2-deploy-and-test-the-solution

[Source](https://aws.amazon.com/blogs/security/implement-step-up-authentication-with-amazon-cognito-part-2-deploy-and-test-the-solution/){:target="_blank" rel="noopener"}

> This solution consists of two parts. In the previous blog post Implement step-up authentication with Amazon Cognito, Part 1: Solution overview, you learned about the architecture and design of a step-up authentication solution that uses AWS services such as Amazon API Gateway, Amazon Cognito, Amazon DynamoDB, and AWS Lambda to protect privileged API operations. In [...]
