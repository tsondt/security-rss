Title: Want to sneak a RAT into Windows? Buy Quantum Builder on the dark web
Date: 2022-09-28T17:00:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-28-want-to-sneak-a-rat-into-windows-buy-quantum-builder-on-the-dark-web

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/28/quantum_builder_agent_tesla_rat/){:target="_blank" rel="noopener"}

> Beware what could be hiding in those LNK shortcuts A tool sold on the dark web that allows cybercriminals to build malicious shortcuts for delivering malware is being used in a campaign pushing a longtime.NET keylogger and remote access trojan (RAT) named Agent Tesla.... [...]
