Title: Microsoft September 2022 Patch Tuesday fixes zero-day used in attacks, 63 flaws
Date: 2022-09-13T13:36:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-09-13-microsoft-september-2022-patch-tuesday-fixes-zero-day-used-in-attacks-63-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-september-2022-patch-tuesday-fixes-zero-day-used-in-attacks-63-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's September 2022 Patch Tuesday, and with it comes fixes for an actively exploited Windows vulnerability and a total of 63 flaws. [...]
