Title: Best practices for setting up Amazon Macie with AWS Organizations
Date: 2022-09-29T20:15:54+00:00
Author: Jonathan Nguyen
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;data privacy;data security;Macie;scanning;Security Blog;Sensitive data
Slug: 2022-09-29-best-practices-for-setting-up-amazon-macie-with-aws-organizations

[Source](https://aws.amazon.com/blogs/security/best-practices-for-setting-up-amazon-macie-with-aws-organizations/){:target="_blank" rel="noopener"}

> In this post, we’ll walk through the best practices to implement before you enable Amazon Macie across all of your AWS accounts within AWS Organizations. Amazon Macie is a data classification and data protection service that uses machine learning and pattern matching to help secure your critical data in AWS. To do this, Macie first [...]
