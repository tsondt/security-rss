Title: Multi-million dollar credit card fraud operation uncovered
Date: 2022-09-23T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-23-multi-million-dollar-credit-card-fraud-operation-uncovered

[Source](https://www.bleepingcomputer.com/news/security/multi-million-dollar-credit-card-fraud-operation-uncovered/){:target="_blank" rel="noopener"}

> A massive operation that has reportedly siphoned millions of USD from credit cards since its launch in 2019 has been exposed and is considered responsible for losses for tens of thousands of victims. [...]
