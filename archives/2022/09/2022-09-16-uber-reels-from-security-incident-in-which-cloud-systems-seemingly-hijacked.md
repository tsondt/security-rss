Title: Uber reels from 'security incident' in which cloud systems seemingly hijacked
Date: 2022-09-16T03:13:43+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-16-uber-reels-from-security-incident-in-which-cloud-systems-seemingly-hijacked

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/16/uber_security_incident/){:target="_blank" rel="noopener"}

> AWS and G Suite admin accounts likely popped, HackerOne bug bounty page hit, and more Uber is tonight reeling from what looks like a substantial cybersecurity breach.... [...]
