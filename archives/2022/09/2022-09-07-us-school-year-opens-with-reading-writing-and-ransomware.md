Title: US school year opens with reading, writing, and ransomware
Date: 2022-09-07T18:00:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-07-us-school-year-opens-with-reading-writing-and-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/07/vice_society_ransomware_education/){:target="_blank" rel="noopener"}

> FBI warns that Vice Society threat group is ramping up attacks on the education sector The Vice Society threat group is ramping up ransomware attacks on US school districts just as students around the country return to the classroom, the FBI and other federal agencies are warning.... [...]
