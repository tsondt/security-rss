Title: FBI: Iranian hackers lurked in Albania’s govt network for 14 months
Date: 2022-09-21T14:44:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-21-fbi-iranian-hackers-lurked-in-albanias-govt-network-for-14-months

[Source](https://www.bleepingcomputer.com/news/security/fbi-iranian-hackers-lurked-in-albania-s-govt-network-for-14-months/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) and CISA said that one of the Iranian threat groups behind the destructive attack on the Albanian government's network in July lurked inside its systems for roughly 14 months. [...]
