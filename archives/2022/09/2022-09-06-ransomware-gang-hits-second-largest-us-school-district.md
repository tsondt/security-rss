Title: Ransomware gang hits second-largest US school district
Date: 2022-09-06T17:45:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-06-ransomware-gang-hits-second-largest-us-school-district

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/06/lausd_ransomware_fbi_cisa_los_angeles/){:target="_blank" rel="noopener"}

> FBI and CISA on-site to assist with incident response over Labor Day weekend Updated Cybercriminals hit the Los Angeles Unified School District (LAUSD) over the holiday weekend with a ransomware attack that temporarily shut down email, computer systems, and applications.... [...]
