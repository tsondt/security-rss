Title: Revealed: US telcos admit to storing, handing over location data
Date: 2022-09-02T17:15:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-02-revealed-us-telcos-admit-to-storing-handing-over-location-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/02/us_carriers_fcc_data_report/){:target="_blank" rel="noopener"}

> Letters to FCC confirm what many believed, don't address a bigger problem US mobile carriers know a lot about where their customers every move, and according to letters sent to the Federal Communications Commission (FCC), they routinely store such location data for years, willingly hand it over to law enforcement if served a proper subpoena, and say users can't opt out.... [...]
