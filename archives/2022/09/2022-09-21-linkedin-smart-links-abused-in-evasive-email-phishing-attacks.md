Title: LinkedIn Smart Links abused in evasive email phishing attacks
Date: 2022-09-21T10:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-21-linkedin-smart-links-abused-in-evasive-email-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/linkedin-smart-links-abused-in-evasive-email-phishing-attacks/){:target="_blank" rel="noopener"}

> Phishing actors are abusing LinkedIn's Smart Link feature to bypass email security products and successfully redirect targeted users to phishing pages that steal login credentials. [...]
