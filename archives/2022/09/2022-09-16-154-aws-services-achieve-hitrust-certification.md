Title: 154 AWS services achieve HITRUST certification
Date: 2022-09-16T17:39:25+00:00
Author: Sonali Vaidya
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS HITRUST;AWS security;Security Blog
Slug: 2022-09-16-154-aws-services-achieve-hitrust-certification

[Source](https://aws.amazon.com/blogs/security/154-aws-services-achieve-hitrust-certification/){:target="_blank" rel="noopener"}

> The AWS HITRUST Compliance Team is excited to announce that 154 Amazon Web Services (AWS) services are certified for the Health Information Trust Alliance (HITRUST) Common Security Framework (CSF) v9.6 for the 2022 cycle. These 154 AWS services were audited by a third-party assessor and certified under the HITRUST CSF. The full list is now [...]
