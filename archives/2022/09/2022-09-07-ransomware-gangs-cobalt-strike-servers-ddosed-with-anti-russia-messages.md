Title: Ransomware gang's Cobalt Strike servers DDoSed with anti-Russia messages
Date: 2022-09-07T09:09:23-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-09-07-ransomware-gangs-cobalt-strike-servers-ddosed-with-anti-russia-messages

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-cobalt-strike-servers-ddosed-with-anti-russia-messages/){:target="_blank" rel="noopener"}

> Someone is flooding Cobalt Strike servers operated by former members of the Conti ransomware gang with anti-Russian messages to disrupt their activity. [...]
