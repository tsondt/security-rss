Title: Emotet botnet now pushes Quantum and BlackCat ransomware
Date: 2022-09-17T11:17:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-17-emotet-botnet-now-pushes-quantum-and-blackcat-ransomware

[Source](https://www.bleepingcomputer.com/news/security/emotet-botnet-now-pushes-quantum-and-blackcat-ransomware/){:target="_blank" rel="noopener"}

> While monitoring the Emotet botnet's current activity, security researchers found that the malware is now being used by the Quantum and BlackCat ransomware gang to deploy their payloads. [...]
