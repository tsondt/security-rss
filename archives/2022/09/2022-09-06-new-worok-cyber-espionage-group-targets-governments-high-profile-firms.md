Title: New Worok cyber-espionage group targets governments, high-profile firms
Date: 2022-09-06T08:49:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-06-new-worok-cyber-espionage-group-targets-governments-high-profile-firms

[Source](https://www.bleepingcomputer.com/news/security/new-worok-cyber-espionage-group-targets-governments-high-profile-firms/){:target="_blank" rel="noopener"}

> A newly discovered cyber-espionage group has been hacking governments and high-profile companies in Asia since at least 2020 using a combination of custom and existing malicious tools. [...]
