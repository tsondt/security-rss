Title: Companies could be forced to delete customer data used to prove ID, Labor suggests
Date: 2022-09-29T02:08:00+00:00
Author: Paul Karp and Josh Taylor
Category: The Guardian
Tags: Optus;Privacy;Data and computer security;Australian politics;Australia news;Labor party;Telecommunications industry
Slug: 2022-09-29-companies-could-be-forced-to-delete-customer-data-used-to-prove-id-labor-suggests

[Source](https://www.theguardian.com/business/2022/sep/29/attorney-general-flags-urgent-privacy-law-changes-in-wake-of-optus-data-breach){:target="_blank" rel="noopener"}

> Albanese government considering sweeping overhaul of data retention and privacy laws following massive Optus cyber hack Follow our Australia news live blog for the latest updates Get our free news app, morning email briefing or daily news podcast Companies could have the right to retain customers’ data stripped back by an ambitious suite of privacy reforms proposed by the Albanese government. The attorney general, Mark Dreyfus, revealed on Thursday that in addition to completing a review of Australia’s privacy laws, the Albanese government will look to legislate “even more urgent reforms” later this year or in early 2023. Sign up [...]
