Title: Microsoft says it's boosted phishing protection in Windows 11 22H2
Date: 2022-09-27T14:00:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-27-microsoft-says-its-boosted-phishing-protection-in-windows-11-22h2

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/27/microsoft_phishing_password_protect_windows_11/){:target="_blank" rel="noopener"}

> Security tool warns admins, users when a password is used on an untrusted site or stored locally In the latest version of Windows 11, namely 22H2, Microsoft has introduced a feature in its Defender SmartScreen tool designed to, hopefully, keep passwords safer.... [...]
