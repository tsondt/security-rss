Title: Train to become an ethical hacker with this $35 course deal
Date: 2022-09-23T07:17:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-23-train-to-become-an-ethical-hacker-with-this-35-course-deal

[Source](https://www.bleepingcomputer.com/news/security/train-to-become-an-ethical-hacker-with-this-35-course-deal/){:target="_blank" rel="noopener"}

> The point is, if you want to advance your career in IT and get a better handle on cyber threat mitigation, then The 2023 Complete Cyber Security Ethical Hacking Certification Bundle is a great place to start. It's convenient, it's fun, and since it's on sale, it's very easy to afford. [...]
