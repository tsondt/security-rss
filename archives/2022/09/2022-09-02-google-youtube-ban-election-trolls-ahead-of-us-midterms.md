Title: Google, YouTube ban election trolls ahead of US midterms
Date: 2022-09-02T23:26:25+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-02-google-youtube-ban-election-trolls-ahead-of-us-midterms

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/02/google_youtube_election_misinformation/){:target="_blank" rel="noopener"}

> Plus: Truth Social barred from Play until it shows just one iota of decency Google and its YouTube subsidiary have joined other social media networks pledging to keep the 2022 US midterm elections safe and free from Russian trolls — and anyone else spewing democracy-damaging disinformation – by taking down such content.... [...]
