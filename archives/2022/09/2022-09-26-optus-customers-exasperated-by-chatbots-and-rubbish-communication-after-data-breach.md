Title: Optus customers exasperated by chatbots and ‘rubbish’ communication after data breach
Date: 2022-09-26T17:30:09+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Optus;Identity fraud;Data and computer security;Scams;Telecommunications industry;Consumer affairs
Slug: 2022-09-26-optus-customers-exasperated-by-chatbots-and-rubbish-communication-after-data-breach

[Source](https://www.theguardian.com/business/2022/sep/27/optus-customers-exasperated-by-chatbots-and-rubbish-communication-after-data-breach){:target="_blank" rel="noopener"}

> Some customers look to switch providers after puzzling responses and ‘less than helpful’ service Follow our Australia news live blog for the latest updates Get our free news app, morning email briefing or daily news podcast Optus customers say they are growing increasingly angry and frustrated at the poor communication from their mobile provider over the massive customer data breach that left millions vulnerable to identity fraud. In the four days since Optus first reported that up to 10 million customers had personal information taken in a data breach, customers have been left scratching their heads over how Optus has [...]
