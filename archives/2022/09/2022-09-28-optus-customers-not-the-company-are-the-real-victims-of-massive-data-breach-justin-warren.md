Title: Optus customers, not the company, are the real victims of massive data breach | Justin Warren
Date: 2022-09-28T04:58:55+00:00
Author: Justin Warren
Category: The Guardian
Tags: Privacy;Data and computer security
Slug: 2022-09-28-optus-customers-not-the-company-are-the-real-victims-of-massive-data-breach-justin-warren

[Source](https://www.theguardian.com/commentisfree/2022/sep/28/optus-customers-not-the-company-are-the-real-victims-of-massive-data-breach){:target="_blank" rel="noopener"}

> Optus executives are paid millions to ensure that, among other things, customer data is safe. These are the people who should be held accountable for the data breach. Follow our Australia news live blog for the latest updates Get our free news app, morning email briefing or daily news podcast The Optus data breach has brought data security into the forefront of every Australian’s mind. While it’s good people are thinking about these issues, the best time to start thinking about them was years ago. The second-best time is now. It’s important then that we analyse how Optus has handled [...]
