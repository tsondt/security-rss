Title: TeamTNT hijacking servers to run Bitcoin encryption solvers
Date: 2022-09-18T10:07:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-09-18-teamtnt-hijacking-servers-to-run-bitcoin-encryption-solvers

[Source](https://www.bleepingcomputer.com/news/security/teamtnt-hijacking-servers-to-run-bitcoin-encryption-solvers/){:target="_blank" rel="noopener"}

> Threat analysts at AquaSec have spotted signs of TeamTNT activity on their honeypots since early September, leading them to believe the notorious hacking group is back in action. [...]
