Title: Fake US govt job offers push Cobalt Strike in phishing attacks
Date: 2022-09-30T12:33:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-30-fake-us-govt-job-offers-push-cobalt-strike-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/fake-us-govt-job-offers-push-cobalt-strike-in-phishing-attacks/){:target="_blank" rel="noopener"}

> A new phishing campaign targets US and New Zealand job seekers with malicious documents installing Cobalt Strike beacons for remote access to victims' devices. [...]
