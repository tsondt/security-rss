Title: Optus tells former Virgin Mobile and Gomo customers they could also be part of data breach
Date: 2022-09-29T05:32:35+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Optus;Australia news;Telecommunications industry;Privacy;Data and computer security;Identity fraud
Slug: 2022-09-29-optus-tells-former-virgin-mobile-and-gomo-customers-they-could-also-be-part-of-data-breach

[Source](https://www.theguardian.com/business/2022/sep/29/optus-tells-former-virgin-mobile-and-gomo-customers-they-could-also-be-part-of-data-breach){:target="_blank" rel="noopener"}

> Identification repair service receives a month’s worth of complaint calls in three days as government pressures telco to pay for replacement ID documents Follow our Australia news live blog for the latest updates Get our free news app, morning email briefing or daily news podcast Former Virgin Mobile and Gomo customers are the latest to have been informed by Optus that their personal information was exposed in the company’s massive data breach, as an identification repair service reveals it has fielded a month’s worth of complaint calls in three days. It has been a week since Optus first revealed up [...]
