Title: HP fixes severe bug in pre-installed Support Assistant tool
Date: 2022-09-07T14:06:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware;Software
Slug: 2022-09-07-hp-fixes-severe-bug-in-pre-installed-support-assistant-tool

[Source](https://www.bleepingcomputer.com/news/security/hp-fixes-severe-bug-in-pre-installed-support-assistant-tool/){:target="_blank" rel="noopener"}

> HP issued a security advisory alerting users about a newly discovered vulnerability in HP Support Assistant, a software tool that comes pre-installed on all HP laptops and desktop computers, including the Omen sub-brand. [...]
