Title: Microsoft Edge’s News Feed ads abused for tech support scams
Date: 2022-09-15T14:08:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-09-15-microsoft-edges-news-feed-ads-abused-for-tech-support-scams

[Source](https://www.bleepingcomputer.com/news/security/microsoft-edge-s-news-feed-ads-abused-for-tech-support-scams/){:target="_blank" rel="noopener"}

> An ongoing malvertising campaign is injecting ads in the Microsoft Edge News Feed to redirect potential victims to websites pushing tech support scams. [...]
