Title: The Week in Ransomware - September 16th 2022 - Iranian Sanctions
Date: 2022-09-16T16:26:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-16-the-week-in-ransomware-september-16th-2022-iranian-sanctions

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-16th-2022-iranian-sanctions/){:target="_blank" rel="noopener"}

> It has been a fairly quiet week on the ransomware front, with the biggest news being US sanctions on Iranians linked to ransomware attacks. [...]
