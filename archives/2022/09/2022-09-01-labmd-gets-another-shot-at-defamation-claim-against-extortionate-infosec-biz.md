Title: LabMD gets another shot at defamation claim against 'extortionate' infosec biz
Date: 2022-09-01T03:49:38+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-01-labmd-gets-another-shot-at-defamation-claim-against-extortionate-infosec-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/01/labmd_tiversa_defamation_lawsuit/){:target="_blank" rel="noopener"}

> But keep your attorney on a 'short leash' against Tiversa, court warns LabMD, the embattled and now defunct cancer-testing company, will get another chance at suing security firm Tiversa for defamation following an appeals court ruling.... [...]
