Title: Akamai stopped new record-breaking DDoS attack in Europe
Date: 2022-09-15T14:28:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-15-akamai-stopped-new-record-breaking-ddos-attack-in-europe

[Source](https://www.bleepingcomputer.com/news/security/akamai-stopped-new-record-breaking-ddos-attack-in-europe/){:target="_blank" rel="noopener"}

> A new distributed denial-of-service (DDoS) attack that took place on Monday, September 12, has broken the previous record that Akamai recorded recently in July. [...]
