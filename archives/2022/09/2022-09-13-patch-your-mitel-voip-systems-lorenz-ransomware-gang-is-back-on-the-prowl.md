Title: Patch your Mitel VoIP systems, Lorenz ransomware gang is back on the prowl
Date: 2022-09-13T18:38:36+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-13-patch-your-mitel-voip-systems-lorenz-ransomware-gang-is-back-on-the-prowl

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/13/lorenz_ransomware_mitel_voip/){:target="_blank" rel="noopener"}

> Criminals do love that unpatched VoIP and IoT kit The Lorenz ransomware gang is exploiting a vulnerability in Mitel VoIP appliances to break corporate networks.... [...]
