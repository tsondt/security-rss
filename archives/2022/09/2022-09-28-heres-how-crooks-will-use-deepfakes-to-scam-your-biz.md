Title: Here's how crooks will use deepfakes to scam your biz
Date: 2022-09-28T07:24:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-28-heres-how-crooks-will-use-deepfakes-to-scam-your-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/28/trend_deepfake_video/){:target="_blank" rel="noopener"}

> Need deepfake tools? GitHub's got 'em All of the materials and tools needed to make deepfake videos – from source code to publicly available images and account authentication bypass services – are readily available and up for sale on the public internet and underground forums.... [...]
