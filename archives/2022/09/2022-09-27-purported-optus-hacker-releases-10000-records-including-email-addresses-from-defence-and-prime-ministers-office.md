Title: Purported Optus hacker releases 10,000 records including email addresses from defence and prime minister’s office
Date: 2022-09-27T00:49:48+00:00
Author: Natasha May and Josh Taylor
Category: The Guardian
Tags: Optus;Australia news;Data and computer security;Telecommunications industry;Cybercrime;Identity fraud;Business
Slug: 2022-09-27-purported-optus-hacker-releases-10000-records-including-email-addresses-from-defence-and-prime-ministers-office

[Source](https://www.theguardian.com/business/2022/sep/27/police-all-over-dark-web-ransom-threat-to-release-10000-customer-records-a-day-optus-ceo-says){:target="_blank" rel="noopener"}

> Optus CEO says federal police are ‘all over’ post with ultimatum demanding $1m within four days after massive data breach Purported Optus hacker releases 10,000 records including from defence and prime minister’s office Follow our Australia news live blog for the latest updates Get our free news app, morning email briefing or daily news podcast The chief executive of Optus, Kelly Bayer Rosmarin, says federal police are “all over” a post on the dark web purporting to release 10,000 customer records from the recent data breach and threatening to release more until a $1m ransom is paid. Rosmarin also told [...]
