Title: Significant customer data exposed in attack on Australian telco
Date: 2022-09-23T17:29:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-09-23-significant-customer-data-exposed-in-attack-on-australian-telco

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/23/cyberattack_optus/){:target="_blank" rel="noopener"}

> Subscribers have questions – like 'When were you going to tell us?' Updated Australian telecommunications company Optus has fallen victim to a significant cyberattack and data breach.... [...]
