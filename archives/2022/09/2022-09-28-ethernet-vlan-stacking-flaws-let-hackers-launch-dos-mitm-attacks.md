Title: Ethernet VLAN Stacking flaws let hackers launch DoS, MiTM attacks
Date: 2022-09-28T11:05:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2022-09-28-ethernet-vlan-stacking-flaws-let-hackers-launch-dos-mitm-attacks

[Source](https://www.bleepingcomputer.com/news/security/ethernet-vlan-stacking-flaws-let-hackers-launch-dos-mitm-attacks/){:target="_blank" rel="noopener"}

> Four vulnerabilities in the widely adopted 'Stacked VLAN' Ethernet feature allows attackers to perform denial-of-service (DoS) or man-in-the-middle (MitM) attacks against network targets using custom-crafted packets. [...]
