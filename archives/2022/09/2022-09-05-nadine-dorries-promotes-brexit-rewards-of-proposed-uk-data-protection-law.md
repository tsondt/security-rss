Title: Nadine Dorries promotes 'Brexit rewards' of proposed UK data protection law
Date: 2022-09-05T11:06:40+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-09-05-nadine-dorries-promotes-brexit-rewards-of-proposed-uk-data-protection-law

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/05/culture_secretary_data_bill_uk/){:target="_blank" rel="noopener"}

> Culture secretary talks up pre-Commons reading as UK waits to hear who new leader will be On the day the UK is set to appoint its new prime minister, digital and culture secretary Nadine Dorries is introducing legislation in Parliament she promises will “drop unnecessary box-ticking and measures stifling British businesses.”... [...]
