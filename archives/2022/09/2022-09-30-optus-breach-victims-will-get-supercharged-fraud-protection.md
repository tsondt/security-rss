Title: Optus breach victims will get "supercharged" fraud protection
Date: 2022-09-30T11:26:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-30-optus-breach-victims-will-get-supercharged-fraud-protection

[Source](https://www.bleepingcomputer.com/news/security/optus-breach-victims-will-get-supercharged-fraud-protection/){:target="_blank" rel="noopener"}

> The Australian Federal Police (AFP) announced today the launch of Operation Guardian which will ensure that more than 10,000 customers who had their personal info leaked in the Optus data breach will get priority protection against fraud attempts. [...]
