Title: Private equity suits at Thoma Bravo pull out of Darktrace acquisition
Date: 2022-09-08T13:00:14+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-09-08-private-equity-suits-at-thoma-bravo-pull-out-of-darktrace-acquisition

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/08/thoma_bravo_darktrace/){:target="_blank" rel="noopener"}

> 'Enterprise immune system' sees share price slump US private equity investor Thoma Bravo has pulled out of its planned takeover of Darktrace, causing shares in the UK cybersecurity company to plummet.... [...]
