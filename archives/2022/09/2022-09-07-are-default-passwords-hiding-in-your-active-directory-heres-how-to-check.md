Title: Are Default Passwords Hiding in Your Active Directory? Here's how to check
Date: 2022-09-07T10:02:04-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-09-07-are-default-passwords-hiding-in-your-active-directory-heres-how-to-check

[Source](https://www.bleepingcomputer.com/news/security/are-default-passwords-hiding-in-your-active-directory-heres-how-to-check/){:target="_blank" rel="noopener"}

> One of the biggest cybersecurity mistakes that an organization can make is failing to change a default password. The question is, how can you track down default passwords in your Windows Active Directory once they're no longer useful? [...]
