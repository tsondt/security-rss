Title: Retbleed slugs VM performance by up to 70 percent in kernel 5.19
Date: 2022-09-12T01:29:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-12-retbleed-slugs-vm-performance-by-up-to-70-percent-in-kernel-519

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/12/retbleed_slugs_vm_performance_by/){:target="_blank" rel="noopener"}

> VMware ran tests and saw some nasty numbers. Performance of next kernel otherwise uncontroversial VMware engineers have tested the Linux kernel's fix for the Retbleed speculative execution bug, and report it can impact compute performance by a whopping 70 percent.... [...]
