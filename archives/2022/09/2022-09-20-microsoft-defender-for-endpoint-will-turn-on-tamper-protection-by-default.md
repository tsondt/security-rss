Title: Microsoft Defender for Endpoint will turn on tamper protection by default
Date: 2022-09-20T08:54:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-09-20-microsoft-defender-for-endpoint-will-turn-on-tamper-protection-by-default

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-for-endpoint-will-turn-on-tamper-protection-by-default/){:target="_blank" rel="noopener"}

> Microsoft says tamper protection will soon be turned on by default for all enterprise customers in Microsoft Defender for Endpoint (MDE) for better defense against ransomware attacks. [...]
