Title: Imperva mitigated long-lasting, 25.3 billion request DDoS attack
Date: 2022-09-20T18:31:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-20-imperva-mitigated-long-lasting-253-billion-request-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/imperva-mitigated-long-lasting-253-billion-request-ddos-attack/){:target="_blank" rel="noopener"}

> Internet security company Imperva has announced its DDoS (distributed denial of service) mitigation solution has broken a new record, defending against a single attack that sent over 25.3 billion requests to one of its customers. [...]
