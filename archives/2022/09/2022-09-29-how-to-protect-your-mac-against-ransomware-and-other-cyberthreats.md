Title: How to protect your Mac against ransomware and other cyberthreats
Date: 2022-09-29T10:07:14-04:00
Author: Sponsored by Acronis
Category: BleepingComputer
Tags: Security
Slug: 2022-09-29-how-to-protect-your-mac-against-ransomware-and-other-cyberthreats

[Source](https://www.bleepingcomputer.com/news/security/how-to-protect-your-mac-against-ransomware-and-other-cyberthreats/){:target="_blank" rel="noopener"}

> A popular myth says that "Mac's don't get viruses," but that's never quite been true — and today's Mac users face more cyberthreats than ever before. If you've got a friend or family member who thinks they don't have to worry at all about cybersecurity, pass along this article. [...]
