Title: US recovers $30 million stolen from Axie Infinity by Lazarus hackers
Date: 2022-09-08T13:04:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-09-08-us-recovers-30-million-stolen-from-axie-infinity-by-lazarus-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-recovers-30-million-stolen-from-axie-infinity-by-lazarus-hackers/){:target="_blank" rel="noopener"}

> With the help of blockchain analysts and FBI agents, the U.S. government seized $30 million worth of cryptocurrency stolen by the North Korean threat group 'Lazarus' from the token-based 'play-to-earn' game Axie Infinity earlier in the year. [...]
