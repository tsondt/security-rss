Title: Attackers abuse web security flaw in Sophos Firewall
Date: 2022-09-26T14:02:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-26-attackers-abuse-web-security-flaw-in-sophos-firewall

[Source](https://portswigger.net/daily-swig/attackers-abuse-web-security-flaw-in-sophos-firewall){:target="_blank" rel="noopener"}

> Vendor patches code injection vulnerability harnessed in attacks on south Asia [...]
