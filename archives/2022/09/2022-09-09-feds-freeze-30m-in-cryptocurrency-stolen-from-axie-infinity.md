Title: Feds freeze $30m in cryptocurrency stolen from Axie Infinity
Date: 2022-09-09T22:08:58+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-09-feds-freeze-30m-in-cryptocurrency-stolen-from-axie-infinity

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/09/cryptocurrency_lazarus_axie/){:target="_blank" rel="noopener"}

> But the North Korean criminals are still over half a billion digicash dollars up Federal investigators and private companies seized $30 million in cryptocurrency stolen in March by North Korean-linked APT gang Lazarus Group from a video game developer, the latest example of the growing skills of government and cybersecurity experts to track and recover such ill-gotten gains.... [...]
