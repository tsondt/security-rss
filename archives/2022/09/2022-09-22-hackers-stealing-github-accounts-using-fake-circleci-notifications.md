Title: Hackers stealing GitHub accounts using fake CircleCI notifications
Date: 2022-09-22T09:40:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-22-hackers-stealing-github-accounts-using-fake-circleci-notifications

[Source](https://www.bleepingcomputer.com/news/security/hackers-stealing-github-accounts-using-fake-circleci-notifications/){:target="_blank" rel="noopener"}

> GitHub is warning of an ongoing phishing campaign that started on September 16 and is targeting its users with emails that impersonate the CircleCI continuous integration and delivery platform. [...]
