Title: Anthony Albanese says ‘Optus should pay’ for new passports for data breach victims
Date: 2022-09-28T09:57:13+00:00
Author: Josh Butler and Ben Butler
Category: The Guardian
Tags: Optus;Business;Australia news;Data and computer security;Telecommunications industry
Slug: 2022-09-28-anthony-albanese-says-optus-should-pay-for-new-passports-for-data-breach-victims

[Source](https://www.theguardian.com/business/2022/sep/28/anthony-albanese-says-optus-should-pay-for-new-passports-for-data-breach-victims){:target="_blank" rel="noopener"}

> Push comes day after states suggest telco will pick up multi-million dollar tab for replacing driver’s licences of affected customers Get our free news app, morning email briefing or daily news podcast The federal government has demanded Optus pay for new passports for customers caught up in the telco’s data breach, as the prime minister flagged an overhaul of laws relating to the collection of personal information. The foreign minister, Penny Wong, has written to Optus raising concerns about criminals exploiting data harvested in the hack, saying there was “no justification” for victims or taxpayers to foot the bill for [...]
