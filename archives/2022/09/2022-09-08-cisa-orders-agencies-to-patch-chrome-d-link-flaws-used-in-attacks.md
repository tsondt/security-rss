Title: CISA orders agencies to patch Chrome, D-Link flaws used in attacks
Date: 2022-09-08T15:11:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-08-cisa-orders-agencies-to-patch-chrome-d-link-flaws-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-agencies-to-patch-chrome-d-link-flaws-used-in-attacks/){:target="_blank" rel="noopener"}

> CISA has added 12 more security flaws to its list of bugs exploited in attacks, including two critical D-Link vulnerabilities and two (now-patched) zero-days in Google Chrome and the Photo Station QNAP software. [...]
