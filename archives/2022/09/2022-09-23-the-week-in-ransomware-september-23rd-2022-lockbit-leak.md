Title: The Week in Ransomware - September 23rd 2022 - LockBit leak
Date: 2022-09-23T17:25:58-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-23-the-week-in-ransomware-september-23rd-2022-lockbit-leak

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-23rd-2022-lockbit-leak/){:target="_blank" rel="noopener"}

> This week we saw some embarrassment for the LockBit ransomware operation when their programmer leaked a ransomware builder for the LockBit 3.0 encryptor. [...]
