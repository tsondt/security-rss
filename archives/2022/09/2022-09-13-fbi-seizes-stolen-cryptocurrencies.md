Title: FBI Seizes Stolen Cryptocurrencies
Date: 2022-09-13T11:51:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;FBI;North Korea;theft
Slug: 2022-09-13-fbi-seizes-stolen-cryptocurrencies

[Source](https://www.schneier.com/blog/archives/2022/09/fbi-seizes-stolen-cryptocurrencies.html){:target="_blank" rel="noopener"}

> The Wall Street Journal is reporting that the FBI has recovered over $30 million in cryptocurrency stolen by North Korean hackers earlier this year. It’s only a fraction of the $540 million stolen, but it’s something. The Axie Infinity recovery represents a shift in law enforcement’s ability to trace funds through a web of so-called crypto addresses, the virtual accounts where cryptocurrencies are stored. These addresses can be created quickly without them being linked to a cryptocurrency company that could freeze the funds. In its effort to mask the stolen crypto, Lazarus Group used more than 12,000 different addresses, according [...]
