Title: American Airlines learned it was breached from phishing targets
Date: 2022-09-24T10:06:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-24-american-airlines-learned-it-was-breached-from-phishing-targets

[Source](https://www.bleepingcomputer.com/news/security/american-airlines-learned-it-was-breached-from-phishing-targets/){:target="_blank" rel="noopener"}

> American Airlines says its Cyber Security Response Team found out about a recently disclosed data breach from the targets of a phishing campaign that was using an employee's hacked Microsoft 365 account. [...]
