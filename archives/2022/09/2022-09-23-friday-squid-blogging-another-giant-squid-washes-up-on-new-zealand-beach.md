Title: Friday Squid Blogging: Another Giant Squid Washes Up on New Zealand Beach
Date: 2022-09-23T21:32:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-09-23-friday-squid-blogging-another-giant-squid-washes-up-on-new-zealand-beach

[Source](https://www.schneier.com/blog/archives/2022/09/friday-squid-blogging-another-giant-squid-washes-up-on-new-zealand-beach.html){:target="_blank" rel="noopener"}

> This one has chewed-up tentacles. (Note that this is a different squid than the one that recently washed up on a South African beach.) As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
