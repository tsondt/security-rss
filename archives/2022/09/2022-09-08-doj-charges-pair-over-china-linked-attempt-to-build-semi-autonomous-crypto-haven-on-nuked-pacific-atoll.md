Title: DoJ charges pair over China-linked attempt to build semi-autonomous crypto haven on nuked Pacific atoll
Date: 2022-09-08T05:30:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-08-doj-charges-pair-over-china-linked-attempt-to-build-semi-autonomous-crypto-haven-on-nuked-pacific-atoll

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/08/doj_rongelap_atoll_crypto_charges/){:target="_blank" rel="noopener"}

> Yes, that’s a lot to digest: Marshall Islands legislators allegedly bribed to make it possible About halfway between The Philippines and Hawaii is a place called Rongelap Atoll that’s infamous for having been unintentionally irradiated by nuclear weapons tests conducted by America at nearby Bikini Atoll in 1954.... [...]
