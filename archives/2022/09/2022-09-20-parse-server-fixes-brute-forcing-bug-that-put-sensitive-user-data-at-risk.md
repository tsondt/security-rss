Title: Parse Server fixes brute-forcing bug that put sensitive user data at risk
Date: 2022-09-20T14:57:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-20-parse-server-fixes-brute-forcing-bug-that-put-sensitive-user-data-at-risk

[Source](https://portswigger.net/daily-swig/parse-server-fixes-brute-forcing-bug-that-put-sensitive-user-data-at-risk){:target="_blank" rel="noopener"}

> Open source project provides push notification functionality for iOS, macOS, Android, and tvOS [...]
