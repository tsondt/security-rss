Title: Optus data breach: who is affected, what has been taken and what should you do?
Date: 2022-09-22T08:31:04+00:00
Author: Ben Doherty
Category: The Guardian
Tags: Australia news;Optus;Telecommunications industry;Data and computer security;Business;Technology
Slug: 2022-09-22-optus-data-breach-who-is-affected-what-has-been-taken-and-what-should-you-do

[Source](https://www.theguardian.com/australia-news/2022/sep/22/optus-data-breach-who-is-affected-what-has-been-taken-and-what-should-you-do){:target="_blank" rel="noopener"}

> After a malicious cyber-attack, customers of Australia’s second-largest telco are advised they could be at risk of identity theft Get our free news app, morning email briefing or daily news podcast Australia’s second-largest telco, Optus, has suffered a massive data breach, with the personal information of potentially millions of customers compromised by a malicious cyber-attack. It is believed the attackers were working for a criminal or state-sponsored organisation. Sign up to receive an email with the top stories from Guardian Australia every morning Continue reading... [...]
