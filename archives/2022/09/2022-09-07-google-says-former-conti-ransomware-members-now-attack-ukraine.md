Title: Google says former Conti ransomware members now attack Ukraine
Date: 2022-09-07T07:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-07-google-says-former-conti-ransomware-members-now-attack-ukraine

[Source](https://www.bleepingcomputer.com/news/security/google-says-former-conti-ransomware-members-now-attack-ukraine/){:target="_blank" rel="noopener"}

> Google says some former Conti cybercrime gang members, now part of a threat group tracked as UAC-0098, are targeting Ukrainian organizations and European non-governmental organizations (NGOs). [...]
