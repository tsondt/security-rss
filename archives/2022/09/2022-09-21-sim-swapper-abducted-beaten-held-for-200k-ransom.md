Title: SIM Swapper Abducted, Beaten, Held for $200k Ransom
Date: 2022-09-21T16:17:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;SIM Swapping;Foreshadow;Jarik;SIM swapping;violence-as-a-service
Slug: 2022-09-21-sim-swapper-abducted-beaten-held-for-200k-ransom

[Source](https://krebsonsecurity.com/2022/09/sim-swapper-abducted-beaten-held-for-200k-ransom/){:target="_blank" rel="noopener"}

> A Florida teenager who served as a lackey for a cybercriminal group that specializes in cryptocurrency thefts was beaten and kidnapped last week by a rival cybercrime gang. The teen’s captives held guns to his head while forcing him to record a video message pleading with his crew to fork over a $200,000 ransom in exchange for his life. The youth is now reportedly cooperating with U.S. federal investigators, who are responding to an alarming number of reports of physical violence tied to certain online crime communities. The grisly kidnapping video has been circulating on a number of Telegram chat [...]
