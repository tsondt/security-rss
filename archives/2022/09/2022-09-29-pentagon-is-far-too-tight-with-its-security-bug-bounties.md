Title: Pentagon is far too tight with its security bug bounties
Date: 2022-09-29T21:27:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-29-pentagon-is-far-too-tight-with-its-security-bug-bounties

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/29/pentagon_bug_bounty/){:target="_blank" rel="noopener"}

> But overpriced, useless fighter jets? That's something we can get behind Discovering and reporting critical security flaws that could allow foreign spies to steal sensitive US government data or launch cyberattacks via the Department of Defense's IT systems doesn't carry a high reward.... [...]
