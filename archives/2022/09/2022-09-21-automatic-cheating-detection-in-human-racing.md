Title: Automatic Cheating Detection in Human Racing
Date: 2022-09-21T11:35:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cheating;sports
Slug: 2022-09-21-automatic-cheating-detection-in-human-racing

[Source](https://www.schneier.com/blog/archives/2022/09/automatic-cheating-detection-in-human-racing.html){:target="_blank" rel="noopener"}

> This is a fascinating glimpse of the future of automatic cheating detection in sports: Maybe you heard about the truly insane false-start controversy in track and field? Devon Allen—a wide receiver for the Philadelphia Eagles—was disqualified from the 110-meter hurdles at the World Athletics Championships a few weeks ago for a false start. Here’s the problem: You can’t see the false start. Nobody can see the false start. By sight, Allen most definitely does not leave before the gun. But here’s the thing: World Athletics has determined that it is not possible for someone to push off the block within [...]
