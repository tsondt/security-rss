Title: Thousands lured with blue badges in Instagram phishing attack
Date: 2022-09-01T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-01-thousands-lured-with-blue-badges-in-instagram-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/thousands-lured-with-blue-badges-in-instagram-phishing-attack/){:target="_blank" rel="noopener"}

> A new Instagram phishing campaign is underway, attempting to scam users of the popular social media platform by luring them with a blue-badge offer. [...]
