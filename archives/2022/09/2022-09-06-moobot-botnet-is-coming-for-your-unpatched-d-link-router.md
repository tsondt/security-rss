Title: Moobot botnet is coming for your unpatched D-Link router
Date: 2022-09-06T16:40:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-06-moobot-botnet-is-coming-for-your-unpatched-d-link-router

[Source](https://www.bleepingcomputer.com/news/security/moobot-botnet-is-coming-for-your-unpatched-d-link-router/){:target="_blank" rel="noopener"}

> The Mirai malware botnet variant known as 'MooBot' has re-emerged in a new attack wave that started early last month, targeting vulnerable D-Link routers with a mix of old and new exploits. [...]
