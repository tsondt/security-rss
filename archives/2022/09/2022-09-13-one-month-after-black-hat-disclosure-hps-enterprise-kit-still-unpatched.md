Title: One month after Black Hat disclosure, HP's enterprise kit still unpatched
Date: 2022-09-13T08:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-13-one-month-after-black-hat-disclosure-hps-enterprise-kit-still-unpatched

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/13/firmware_bugs_hp/){:target="_blank" rel="noopener"}

> What could go wrong with leaving firmware open after world's biggest hacker convention talk? Multiple high-severity firmware bugs in HP's business computers remain unpatched, some more than a year after Binarly security researchers disclosed the vulnerabilities to HP and then discussed them at the Black Hat security conference last month.... [...]
