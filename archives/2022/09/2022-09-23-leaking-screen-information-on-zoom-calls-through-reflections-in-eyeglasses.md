Title: Leaking Screen Information on Zoom Calls through Reflections in Eyeglasses
Date: 2022-09-23T11:43:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;side-channel attacks;videoconferencing
Slug: 2022-09-23-leaking-screen-information-on-zoom-calls-through-reflections-in-eyeglasses

[Source](https://www.schneier.com/blog/archives/2022/09/leaking-screen-information-on-zoom-calls-through-reflections-in-eyeglasses.html){:target="_blank" rel="noopener"}

> Okay, it’s an obscure threat. But people are researching it : Our models and experimental results in a controlled lab setting show it is possible to reconstruct and recognize with over 75 percent accuracy on-screen texts that have heights as small as 10 mm with a 720p webcam.” That corresponds to 28 pt, a font size commonly used for headings and small headlines. [...] Being able to read reflected headline-size text isn’t quite the privacy and security problem of being able to read smaller 9 to 12 pt fonts. But this technique is expected to provide access to smaller font [...]
