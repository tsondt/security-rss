Title: Google to test disabling Chrome Manifest V2 extensions in June 2023
Date: 2022-09-28T13:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-28-google-to-test-disabling-chrome-manifest-v2-extensions-in-june-2023

[Source](https://www.bleepingcomputer.com/news/security/google-to-test-disabling-chrome-manifest-v2-extensions-in-june-2023/){:target="_blank" rel="noopener"}

> Developers of extensions for Google Chrome can keep their hopes up that the transition from Manifest V2 to V3 will be as gradual as possible, helping to minimize the negative impact on the community of users. [...]
