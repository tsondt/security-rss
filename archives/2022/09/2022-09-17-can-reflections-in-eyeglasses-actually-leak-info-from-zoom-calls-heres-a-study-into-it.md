Title: Can reflections in eyeglasses actually leak info from Zoom calls? Here's a study into it
Date: 2022-09-17T07:32:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-17-can-reflections-in-eyeglasses-actually-leak-info-from-zoom-calls-heres-a-study-into-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/17/glasses_reflections_zoom/){:target="_blank" rel="noopener"}

> About time someone shined some light onto this Boffins at the University of Michigan in the US and Zhejiang University in China want to highlight how bespectacled video conferencing participants are inadvertently revealing sensitive on-screen information via reflections in their eyeglasses.... [...]
