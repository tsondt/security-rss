Title: Uber hack linked to hardcoded secrets spotted in powershell script
Date: 2022-09-16T15:26:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-16-uber-hack-linked-to-hardcoded-secrets-spotted-in-powershell-script

[Source](https://portswigger.net/daily-swig/uber-hack-linked-to-hardcoded-secrets-spotted-in-powershell-script){:target="_blank" rel="noopener"}

> Social engineering attack compromises internal networks and Uber’s bug bounty reports [...]
