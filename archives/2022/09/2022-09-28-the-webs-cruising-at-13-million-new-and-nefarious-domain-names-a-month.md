Title: The web's cruising at 13 million new and nefarious domain names a month
Date: 2022-09-28T20:20:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-28-the-webs-cruising-at-13-million-new-and-nefarious-domain-names-a-month

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/28/akamai_malicious_domains/){:target="_blank" rel="noopener"}

> Or so Akamai is dying to tell us Akamai reckons that, in the first half of 2022 alone, it flagged nearly 79 million newly observed domains (NODs) as malicious.... [...]
