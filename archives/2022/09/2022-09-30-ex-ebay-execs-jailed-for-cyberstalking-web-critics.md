Title: Ex-eBay execs jailed for cyberstalking web critics
Date: 2022-09-30T00:58:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-30-ex-ebay-execs-jailed-for-cyberstalking-web-critics

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/30/ebay_execs_jailed_cyberstalking/){:target="_blank" rel="noopener"}

> Still to come: Civil RICO lawsuit against eBay and former top brass Two now-former eBay executives who pleaded guilty to cyberstalking charges this year have been sent down and fined tens of thousands of dollars.... [...]
