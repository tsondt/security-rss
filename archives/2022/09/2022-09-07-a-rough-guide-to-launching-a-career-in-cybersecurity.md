Title: A rough guide to launching a career in cybersecurity
Date: 2022-09-07T13:59:44+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-07-a-rough-guide-to-launching-a-career-in-cybersecurity

[Source](https://portswigger.net/daily-swig/a-rough-guide-to-launching-a-career-in-cybersecurity){:target="_blank" rel="noopener"}

> Entry-level training courses offer paths to glory [...]
