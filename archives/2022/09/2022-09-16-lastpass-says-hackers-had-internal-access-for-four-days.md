Title: LastPass says hackers had internal access for four days
Date: 2022-09-16T15:30:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-16-lastpass-says-hackers-had-internal-access-for-four-days

[Source](https://www.bleepingcomputer.com/news/security/lastpass-says-hackers-had-internal-access-for-four-days/){:target="_blank" rel="noopener"}

> LastPass says the attacker behind the August security breach had internal access to the company's systems for four days until they were detected and evicted. [...]
