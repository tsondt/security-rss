Title: Signal calls on users to run proxies for bypassing Iran blocks
Date: 2022-09-23T11:30:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2022-09-23-signal-calls-on-users-to-run-proxies-for-bypassing-iran-blocks

[Source](https://www.bleepingcomputer.com/news/security/signal-calls-on-users-to-run-proxies-for-bypassing-iran-blocks/){:target="_blank" rel="noopener"}

> Signal is urging its global community to help people in Iran stay connected with each other and the rest of the world by volunteering proxies to bypass the aggressive restrictions imposed by the Iranian regime. [...]
