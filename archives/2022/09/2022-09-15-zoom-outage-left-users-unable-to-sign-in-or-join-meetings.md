Title: Zoom outage left users unable to sign in or join meetings
Date: 2022-09-15T11:35:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-15-zoom-outage-left-users-unable-to-sign-in-or-join-meetings

[Source](https://www.bleepingcomputer.com/news/security/zoom-outage-left-users-unable-to-sign-in-or-join-meetings/){:target="_blank" rel="noopener"}

> The Zoom video conference platform was down and experienced an outage preventing users from logging in or joining meetings. [...]
