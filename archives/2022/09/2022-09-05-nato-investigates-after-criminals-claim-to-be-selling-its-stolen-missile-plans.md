Title: NATO investigates after criminals claim to be selling its stolen missile plans
Date: 2022-09-05T13:04:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-09-05-nato-investigates-after-criminals-claim-to-be-selling-its-stolen-missile-plans

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/05/in-brief-secuirty/){:target="_blank" rel="noopener"}

> Also, Microsoft’s one-click TikTok trick, a 14-year old Aussie cracks ASD encryption in an hour, and more In brief NATO officials are investigating after criminals put up some data for sale on dark forums that they claim is "classified" information stolen from European missile maker MBDA.... [...]
