Title: IRS data leak exposes personal info of 120,000 taxpayers
Date: 2022-09-03T16:39:41-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-09-03-irs-data-leak-exposes-personal-info-of-120000-taxpayers

[Source](https://www.bleepingcomputer.com/news/security/irs-data-leak-exposes-personal-info-of-120-000-taxpayers/){:target="_blank" rel="noopener"}

> The Internal Revenue Service has accidentally leaked confidential information for approximately 120,000 taxpayers who filed a form 990-T as part of their tax returns. [...]
