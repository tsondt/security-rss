Title: Apple patches iPhone and macOS flaws under active attack
Date: 2022-09-12T23:07:44+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-12-apple-patches-iphone-and-macos-flaws-under-active-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/12/apple_patched_exploited_flaws/){:target="_blank" rel="noopener"}

> High-value targets tend to get hit Apple has pushed out five security fixes including including two vulnerabilities in its iPhones, iPads and Mac operating systems that are already being exploited.... [...]
