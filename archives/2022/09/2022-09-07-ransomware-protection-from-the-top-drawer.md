Title: Ransomware protection from the top drawer
Date: 2022-09-07T18:28:07+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-09-07-ransomware-protection-from-the-top-drawer

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/07/ransomware_protection_from_the_top/){:target="_blank" rel="noopener"}

> Why Zero Trust security needs secure infrastructure, systems, networks, users, and applications Webinar Statistics suggest that there was a ransomware attack on a company or organization every 11 seconds in 2021, but only 57 percent of the victims successfully retrieved their kidnapped data by using back up. And the 32 percent that paid a ransom only recovered 65 percent of their lost data.... [...]
