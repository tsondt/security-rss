Title: New ransomware hits Windows, Linux servers of Chile govt agency
Date: 2022-09-01T13:50:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-01-new-ransomware-hits-windows-linux-servers-of-chile-govt-agency

[Source](https://www.bleepingcomputer.com/news/security/new-ransomware-hits-windows-linux-servers-of-chile-govt-agency/){:target="_blank" rel="noopener"}

> Chile's national computer security and incident response team (CSIRT) has announced that a ransomware attack has impacted operations and online services of a government agency in the country. [...]
