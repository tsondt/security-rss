Title: Leaking Passwords through the Spellchecker
Date: 2022-09-26T11:08:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;browsers;data protection;leaks;passwords
Slug: 2022-09-26-leaking-passwords-through-the-spellchecker

[Source](https://www.schneier.com/blog/archives/2022/09/leaking-passwords-through-the-spellchecker.html){:target="_blank" rel="noopener"}

> Sometimes browser spellcheckers leak passwords : When using major web browsers like Chrome and Edge, your form data is transmitted to Google and Microsoft, respectively, should enhanced spellcheck features be enabled. Depending on the website you visit, the form data may itself include PII­—including but not limited to Social Security Numbers (SSNs)/Social Insurance Numbers (SINs), name, address, email, date of birth (DOB), contact information, bank and payment information, and so on. The solution is to only use the spellchecker options that keep the data on your computer—and don’t send it into the cloud. [...]
