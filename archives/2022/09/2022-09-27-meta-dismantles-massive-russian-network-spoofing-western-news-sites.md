Title: Meta dismantles massive Russian network spoofing Western news sites
Date: 2022-09-27T10:44:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-27-meta-dismantles-massive-russian-network-spoofing-western-news-sites

[Source](https://www.bleepingcomputer.com/news/security/meta-dismantles-massive-russian-network-spoofing-western-news-sites/){:target="_blank" rel="noopener"}

> Meta says it took down a large network of Facebook and Instagram accounts pushing disinformation published on more than 60 websites that spoofed multiple legitimate news sites across Europe. [...]
