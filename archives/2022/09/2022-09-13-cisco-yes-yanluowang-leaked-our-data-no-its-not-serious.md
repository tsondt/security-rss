Title: Cisco: Yes, Yanluowang leaked our data. No, it's not serious
Date: 2022-09-13T07:30:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-13-cisco-yes-yanluowang-leaked-our-data-no-its-not-serious

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/13/cisco_ransomware_data_leaked/){:target="_blank" rel="noopener"}

> Everything's fine! The Yanluowang ransomware group behind the May attack on Cisco Systems has publicly leaked the stolen files on the dark web over the weekend, but the networking giant says there's nothing to worry about.... [...]
