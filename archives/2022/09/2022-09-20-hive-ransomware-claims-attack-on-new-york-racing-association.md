Title: Hive ransomware claims attack on New York Racing Association
Date: 2022-09-20T16:33:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-20-hive-ransomware-claims-attack-on-new-york-racing-association

[Source](https://www.bleepingcomputer.com/news/security/hive-ransomware-claims-attack-on-new-york-racing-association/){:target="_blank" rel="noopener"}

> The Hive ransomware operation claimed responsibility for an attack on the New York Racing Association (NYRA), which previously disclosed that a cyber attack on June 30, 2022, impacted IT operations and website availability and compromised member data. [...]
