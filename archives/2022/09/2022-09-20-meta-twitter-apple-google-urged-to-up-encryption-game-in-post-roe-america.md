Title: Meta, Twitter, Apple, Google urged to up encryption game in post-Roe America
Date: 2022-09-20T19:19:24+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-20-meta-twitter-apple-google-urged-to-up-encryption-game-in-post-roe-america

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/20/encryption_abortion_data/){:target="_blank" rel="noopener"}

> Tech giants 'throwing their users to the wolves' Facebook, Twitter, Google, Apple, and others today faced renewed pressure to protect the privacy of messaging app users seeking healthcare treatment.... [...]
