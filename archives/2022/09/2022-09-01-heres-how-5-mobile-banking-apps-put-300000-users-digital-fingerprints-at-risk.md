Title: Here's how 5 mobile banking apps put 300,000 users' digital fingerprints at risk
Date: 2022-09-01T10:04:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-01-heres-how-5-mobile-banking-apps-put-300000-users-digital-fingerprints-at-risk

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/01/mobile_apps_leaked_biometrics/){:target="_blank" rel="noopener"}

> Spoiler: They used hard-coded AWS credentials Massive amounts of private data – including more than 300,000 biometric digital fingerprints used by five mobile banking apps – have been put at risk of theft due to hard-coded Amazon Web Services credentials, according to security researchers.... [...]
