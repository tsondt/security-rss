Title: China's infosec researchers obeyed Beijing and stopped reporting vulns ... or did they?
Date: 2022-09-27T06:58:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-27-chinas-infosec-researchers-obeyed-beijing-and-stopped-reporting-vulns-or-did-they

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/27/atlantic_council_china_vuln_research/){:target="_blank" rel="noopener"}

> Report finds increase in anonymous vuln reports The number of vulnerability reports provided by Chinese information security researchers has fallen sharply, according to research by think tank The Atlantic Council, which also found a strangely commensurate increase in bug reports from unknown sources.... [...]
