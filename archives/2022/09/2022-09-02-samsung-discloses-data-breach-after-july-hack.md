Title: Samsung discloses data breach after July hack
Date: 2022-09-02T13:23:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-02-samsung-discloses-data-breach-after-july-hack

[Source](https://www.bleepingcomputer.com/news/security/samsung-discloses-data-breach-after-july-hack/){:target="_blank" rel="noopener"}

> Electronics giant Samsung has confirmed a new data breach today after some of its U.S. systems were hacked to steal customer data. [...]
