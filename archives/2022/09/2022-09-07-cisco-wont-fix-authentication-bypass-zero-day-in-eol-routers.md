Title: Cisco won’t fix authentication bypass zero-day in EoL routers
Date: 2022-09-07T13:05:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-07-cisco-wont-fix-authentication-bypass-zero-day-in-eol-routers

[Source](https://www.bleepingcomputer.com/news/security/cisco-won-t-fix-authentication-bypass-zero-day-in-eol-routers/){:target="_blank" rel="noopener"}

> Cisco says that a new authentication bypass flaw affecting multiple small business VPN routers will not be patched because the devices have reached end-of-life (EoL). [...]
