Title: Microsoft 365 phishing attacks impersonate U.S. govt agencies
Date: 2022-09-19T16:28:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-19-microsoft-365-phishing-attacks-impersonate-us-govt-agencies

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-phishing-attacks-impersonate-us-govt-agencies/){:target="_blank" rel="noopener"}

> An ongoing phishing campaign targeting U.S. government contractors has expanded its operation to push higher-quality lures and better-crafted documents. [...]
