Title: New hacking group ‘Metador’ lurking in ISP networks for months
Date: 2022-09-25T10:16:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-25-new-hacking-group-metador-lurking-in-isp-networks-for-months

[Source](https://www.bleepingcomputer.com/news/security/new-hacking-group-metador-lurking-in-isp-networks-for-months/){:target="_blank" rel="noopener"}

> A previously unknown threat actor that researchers have named 'Metador' has been breaching telecommunications, internet services providers (ISPs), and universities for about two years. [...]
