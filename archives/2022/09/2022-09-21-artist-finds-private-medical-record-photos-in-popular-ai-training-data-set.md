Title: Artist finds private medical record photos in popular AI training data set
Date: 2022-09-21T15:43:09+00:00
Author: Benj Edwards
Category: Ars Technica
Tags: Biz & IT;AI;consent;Google Imagen;LAION;machine learning;privacy;Stable Diffusion
Slug: 2022-09-21-artist-finds-private-medical-record-photos-in-popular-ai-training-data-set

[Source](https://arstechnica.com/?p=1882591){:target="_blank" rel="noopener"}

> Enlarge / Censored medical images found in the LAION-5B data set used to train AI. The black bars and distortion have been added. (credit: Ars Technica) Late last week, a California-based AI artist who goes by the name Lapine discovered private medical record photos taken by her doctor in 2013 referenced in the LAION-5B image set, which is a scrape of publicly available images on the web. AI researchers download a subset of that data to train AI image synthesis models such as Stable Diffusion and Google Imagen. Lapine discovered her medical photos on a site called Have I Been [...]
