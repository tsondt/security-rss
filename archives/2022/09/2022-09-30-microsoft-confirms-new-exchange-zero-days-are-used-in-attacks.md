Title: Microsoft confirms new Exchange zero-days are used in attacks
Date: 2022-09-30T04:18:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-09-30-microsoft-confirms-new-exchange-zero-days-are-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-confirms-new-exchange-zero-days-are-used-in-attacks/){:target="_blank" rel="noopener"}

> Microsoft has confirmed that two recently reported zero-day vulnerabilities in Microsoft Exchange Server 2013, 2016, and 2019 are being exploited in the wild. [...]
