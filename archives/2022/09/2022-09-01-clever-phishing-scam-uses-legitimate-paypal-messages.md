Title: Clever Phishing Scam Uses Legitimate PayPal Messages
Date: 2022-09-01T12:18:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-09-01-clever-phishing-scam-uses-legitimate-paypal-messages

[Source](https://www.schneier.com/blog/archives/2022/09/clever-phishing-scam-uses-legitimate-paypal-messages.html){:target="_blank" rel="noopener"}

> Brian Krebs is reporting on a clever PayPal phishing scam that uses legitimate PayPal messaging. Basically, the scammers use the PayPal invoicing system to send the email. The email lists a phone number to dispute the charge, which is not PayPal and quickly turns into a request to download and install a remote-access tool. [...]
