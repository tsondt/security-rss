Title: Announcing new AWS IAM Identity Center APIs to manage users and groups at scale
Date: 2022-09-01T16:27:43+00:00
Author: Sharanya Ramakrishnan
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;Security Blog
Slug: 2022-09-01-announcing-new-aws-iam-identity-center-apis-to-manage-users-and-groups-at-scale

[Source](https://aws.amazon.com/blogs/security/announcing-new-aws-iam-identity-center-apis-to-manage-users-and-groups-at-scale/){:target="_blank" rel="noopener"}

> If you use AWS IAM Identity Center (successor to AWS Single Sign-On) as your identity source, you create and manage your users and groups manually in the IAM Identity Center console. However, you may prefer to automate this process to save time, spend less administrative effort, and to scale effectively as your organization grows. If [...]
