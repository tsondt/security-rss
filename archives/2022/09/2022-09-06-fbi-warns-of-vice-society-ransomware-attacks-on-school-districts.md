Title: FBI warns of Vice Society ransomware attacks on school districts
Date: 2022-09-06T14:37:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-06-fbi-warns-of-vice-society-ransomware-attacks-on-school-districts

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-vice-society-ransomware-attacks-on-school-districts/){:target="_blank" rel="noopener"}

> FBI, CISA, and MS-ISAC warned today of U.S. school districts being increasingly targeted by the Vice Society ransomware group, with more attacks expected after the new school year start. [...]
