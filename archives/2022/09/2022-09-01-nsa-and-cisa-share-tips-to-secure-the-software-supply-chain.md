Title: NSA and CISA share tips to secure the software supply chain
Date: 2022-09-01T11:21:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-01-nsa-and-cisa-share-tips-to-secure-the-software-supply-chain

[Source](https://www.bleepingcomputer.com/news/security/nsa-and-cisa-share-tips-to-secure-the-software-supply-chain/){:target="_blank" rel="noopener"}

> The U.S. National Security Agency (NSA) and the Cybersecurity and Infrastructure Security Agency (CISA) have released guidance today with tips on how to secure the software supply chain. [...]
