Title: IRS warns Americans of massive rise in SMS phishing attacks
Date: 2022-09-28T16:00:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-28-irs-warns-americans-of-massive-rise-in-sms-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/irs-warns-americans-of-massive-rise-in-sms-phishing-attacks/){:target="_blank" rel="noopener"}

> The Internal Revenue Service (IRS) warned Americans of an exponential rise in IRS-themed text message phishing attacks trying to steal their financial and personal information in the last few weeks. [...]
