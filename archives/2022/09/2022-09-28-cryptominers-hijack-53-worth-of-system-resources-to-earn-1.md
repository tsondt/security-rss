Title: Cryptominers hijack $53 worth of system resources to earn $1
Date: 2022-09-28T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-28-cryptominers-hijack-53-worth-of-system-resources-to-earn-1

[Source](https://www.bleepingcomputer.com/news/security/cryptominers-hijack-53-worth-of-system-resources-to-earn-1/){:target="_blank" rel="noopener"}

> Security researchers estimate that the financial impact of cryptominers infecting cloud servers costs victims about $53 for every $1 worth of cryptocurrency threat actors mine on hijacked devices. [...]
