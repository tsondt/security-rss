Title: VMware: 70% drop in Linux ESXi VM performance with Retbleed fixes
Date: 2022-09-12T11:33:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Linux;Security
Slug: 2022-09-12-vmware-70-drop-in-linux-esxi-vm-performance-with-retbleed-fixes

[Source](https://www.bleepingcomputer.com/news/linux/vmware-70-percent-drop-in-linux-esxi-vm-performance-with-retbleed-fixes/){:target="_blank" rel="noopener"}

> VMware is warning that ESXi VMs running on Linux kernel 5.19 can have up to a 70% performance drop when Retbleed mitigations are enabled compared to the Linux kernel 5.18 release. [...]
