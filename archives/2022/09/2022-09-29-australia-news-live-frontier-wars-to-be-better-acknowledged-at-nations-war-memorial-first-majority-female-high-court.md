Title: Australia news live: Frontier Wars to be better acknowledged at nation’s war memorial; first majority female high court
Date: 2022-09-29T00:58:05+00:00
Author: Natasha May
Category: The Guardian
Tags: Australia news;Australian politics;Optus;Anthony Albanese;Medicare Australia;Australia weather;Labor party;Coalition;Australian economy;Cybercrime;Data and computer security
Slug: 2022-09-29-australia-news-live-frontier-wars-to-be-better-acknowledged-at-nations-war-memorial-first-majority-female-high-court

[Source](https://www.theguardian.com/australia-news/live/2022/sep/29/australia-news-live-politics-optus-medicare-fuel-excise-anthony-albanese-labor-liberal-indigenous-voice-cost-of-living-weather){:target="_blank" rel="noopener"}

> Attorney general Mark Dreyfus has announced the appointment of justice Jayne Jagot to the high court. Follow the day’s news live Get our free news app, morning email briefing or daily news podcast Queensland seeking partnerships from the federal government in renewable plan The Queensland premier, Annastacia Palaszczuk, was asked to clarify how long the state will keep exporting coal for: There’s still going to be countries that need our coal and, of course, the metallurgical coal [that] is needed for steel production. Let’s be clear about that. Until there’s alternative to manufacturing steel, the world will still need metallurgical [...]
