Title: Tarfile path traversal bug from 2007 still present in 350k open source repos
Date: 2022-09-22T15:39:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-22-tarfile-path-traversal-bug-from-2007-still-present-in-350k-open-source-repos

[Source](https://portswigger.net/daily-swig/tarfile-path-traversal-bug-from-2007-still-present-in-350k-open-source-repos){:target="_blank" rel="noopener"}

> Warning added to Python documentation was deemed preferable to a patch [...]
