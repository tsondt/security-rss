Title: Wazuh - The free and open source XDR platform
Date: 2022-09-28T10:06:03-04:00
Author: Sponsored by Wazuh
Category: BleepingComputer
Tags: Security
Slug: 2022-09-28-wazuh-the-free-and-open-source-xdr-platform

[Source](https://www.bleepingcomputer.com/news/security/wazuh-the-free-and-open-source-xdr-platform/){:target="_blank" rel="noopener"}

> Wazuh is a free and open source security platform that provides unified SIEM and XDR protection. It protects workloads across on-premises, virtualized, containerized, and cloud-based environments. Wazuh is one of the fastest growing open source security solutions, with over 10 million downloads per year. [...]
