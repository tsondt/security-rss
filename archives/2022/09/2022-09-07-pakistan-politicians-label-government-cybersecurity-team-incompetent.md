Title: Pakistan politicians label government cybersecurity team 'incompetent'
Date: 2022-09-07T02:15:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-07-pakistan-politicians-label-government-cybersecurity-team-incompetent

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/07/pakistan_government_security_incompetent/){:target="_blank" rel="noopener"}

> MP alleges taxpayer database – which holds personal info on millions – has come under attack A Pakistani parliamentary committee has labelled its own cybersecurity agency "incompetent".... [...]
