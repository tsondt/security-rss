Title: Australia asks FBI to help find attacker who stole data from millions of users
Date: 2022-09-28T03:35:31+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-28-australia-asks-fbi-to-help-find-attacker-who-stole-data-from-millions-of-users

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/28/optus_data_breach_summary/){:target="_blank" rel="noopener"}

> Apparent perp claims to have deleted swiped info as carrier Optus struggles to get its story straight +Comment Australian authorities have asked the United States Federal Bureau of Investigation (FBI) to assist with investigations into the data breach at local telco Optus.... [...]
