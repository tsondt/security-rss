Title: New Royal Ransomware emerges in multi-million dollar attacks
Date: 2022-09-29T10:32:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-29-new-royal-ransomware-emerges-in-multi-million-dollar-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-royal-ransomware-emerges-in-multi-million-dollar-attacks/){:target="_blank" rel="noopener"}

> A new ransomware operation named Royal is quickly ramping up, targeting corporations with ransom demands ranging from $250,000 to over $2 million. [...]
