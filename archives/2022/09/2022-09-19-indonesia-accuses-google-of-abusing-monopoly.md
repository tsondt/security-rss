Title: Indonesia accuses Google of abusing monopoly
Date: 2022-09-19T00:58:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-09-19-indonesia-accuses-google-of-abusing-monopoly

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/19/indonesia_google_monopoly_investigation/){:target="_blank" rel="noopener"}

> PLUS: Qualys CEO says APAC has infosec advantages; Singapore's Sea ebbs in Americas; Toshiba's tepid takeover update; and more Asia In Brief Indonesia's competition regulator, the Komisi Pengawas Persaingan Usaha (KPPU) has alleged that Google has violated local anti-monopoly laws by abusing its dominant position for the distribution of apps and its requirement that developers must use its payment systems.... [...]
