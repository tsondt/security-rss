Title: Responsible Disclosure for Cryptocurrency Security
Date: 2022-09-09T13:33:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;blockchain;cryptocurrency;disclosure;patching;vulnerabilities
Slug: 2022-09-09-responsible-disclosure-for-cryptocurrency-security

[Source](https://www.schneier.com/blog/archives/2022/09/responsible-disclosure-for-cryptocurrency-security.html){:target="_blank" rel="noopener"}

> Stewart Baker discusses why the industry-norm responsible disclosure for software vulnerabilities fails for cryptocurrency software. Why can’t the cryptocurrency industry solve the problem the way the software and hardware industries do, by patching and updating security as flaws are found? Two reasons: First, many customers don’t have an ongoing relationship with the hardware and software providers that protect their funds­—nor do they have an incentive to update security on a regular basis. Turning to a new security provider or using updated software creates risks; leaving everything the way it was feels safer. So users won’t be rushing to pay for [...]
