Title: Nearly one in two industry pros scaled back open source use over security fears
Date: 2022-09-14T19:29:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-14-nearly-one-in-two-industry-pros-scaled-back-open-source-use-over-security-fears

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/14/snakes_on_a_plan_anaconda/){:target="_blank" rel="noopener"}

> Log4j being the main driver, this data science poll claims About 40 percent of industry professionals say their organizations have reduced their usage of open source software due to concerns about security, according to a survey conducted by data science firm Anaconda.... [...]
