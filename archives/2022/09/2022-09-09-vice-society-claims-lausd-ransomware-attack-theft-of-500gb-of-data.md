Title: Vice Society claims LAUSD ransomware attack, theft of 500GB of data
Date: 2022-09-09T10:49:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-09-vice-society-claims-lausd-ransomware-attack-theft-of-500gb-of-data

[Source](https://www.bleepingcomputer.com/news/security/vice-society-claims-lausd-ransomware-attack-theft-of-500gb-of-data/){:target="_blank" rel="noopener"}

> The Vice Society gang has claimed the ransomware attack that hit Los Angeles Unified (LAUSD), the second largest school district in the United States, over the weekend. [...]
