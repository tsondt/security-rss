Title: CI/CD servers readily breached by abusing&nbsp; SCM webhooks, researchers find
Date: 2022-09-23T13:57:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-23-cicd-servers-readily-breached-by-abusing-scm-webhooks-researchers-find

[Source](https://portswigger.net/daily-swig/ci-cd-servers-readily-breached-by-abusing-nbsp-scm-webhooks-researchers-find){:target="_blank" rel="noopener"}

> Webhook, line, and sinker [...]
