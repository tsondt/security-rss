Title: Introducing Workforce Identity Federation to easily manage workforce access to Google Cloud
Date: 2022-09-29T16:00:00+00:00
Author: Sid Mishra
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-09-29-introducing-workforce-identity-federation-to-easily-manage-workforce-access-to-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/introducing-workforce-identity-federation/){:target="_blank" rel="noopener"}

> At Google Cloud, we’re focused on giving customers new ways to strengthen their security posture. Managing identities and authorization is a core security control that underpins interactions inside and collaboration outside the organization. To address fraud, identity theft, and other security challenges associated with the proliferation of online accounts, many organizations have opted to use centralized identity provider (IdP) products that can help secure and manage identities for their users and SaaS applications, and we want to strengthen support for these solutions and the use cases they support. Today we’re pleased to announce Workforce Identity Federation in Preview. This new [...]
