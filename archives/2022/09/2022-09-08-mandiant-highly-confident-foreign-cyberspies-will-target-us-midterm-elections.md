Title: Mandiant ‘highly confident’ foreign cyberspies will target US midterm elections
Date: 2022-09-08T22:18:24+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-08-mandiant-highly-confident-foreign-cyberspies-will-target-us-midterm-elections

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/08/mandiant_cyberspies_us_elections/){:target="_blank" rel="noopener"}

> It is with a heavy heart that we must announce that the hackers are at it again Mandiant is "highly confident" that foreign cyberspies will target US election infrastructure, organizations, and individuals in the run-up to the November midterm elections.... [...]
