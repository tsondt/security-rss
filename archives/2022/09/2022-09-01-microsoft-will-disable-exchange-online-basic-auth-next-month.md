Title: Microsoft will disable Exchange Online basic auth next month
Date: 2022-09-01T13:42:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-09-01-microsoft-will-disable-exchange-online-basic-auth-next-month

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-will-disable-exchange-online-basic-auth-next-month/){:target="_blank" rel="noopener"}

> Microsoft warned customers today that it will finally disable basic authentication in random tenants worldwide to improve Exchange Online security starting October 1, 2022. [...]
