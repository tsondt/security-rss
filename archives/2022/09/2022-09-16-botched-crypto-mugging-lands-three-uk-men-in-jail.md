Title: Botched Crypto Mugging Lands Three U.K. Men in Jail
Date: 2022-09-16T17:55:25+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;SIM Swapping;The Coming Storm;brickings;Disco Dog;Discoli;Joseph James O'Connor;Leonardo Sapiano;Patrick McGovern-Allen;PlugWalkJoe;Rayhan Miah;Robert Lewis Barr;SIM swapping;SWATting;Thomas Green;violence-as-a-service
Slug: 2022-09-16-botched-crypto-mugging-lands-three-uk-men-in-jail

[Source](https://krebsonsecurity.com/2022/09/botched-crypto-mugging-lands-three-u-k-men-in-jail/){:target="_blank" rel="noopener"}

> Three men in the United Kingdom were arrested this month for attempting to assault a local man and steal his virtual currencies. The incident is the latest example of how certain cybercriminal communities are increasingly turning to physical violence to settle scores and disputes. Shortly after 11 p.m. on September 6, a resident in the Spalding Common area in the district of Lincolnshire, U.K. phoned police to say three men were acting suspiciously, and had jumped a nearby fence. “The three men made off in a VW Golf and were shortly stopped nearby,” reads a statement by the Lincolnshire Police. [...]
