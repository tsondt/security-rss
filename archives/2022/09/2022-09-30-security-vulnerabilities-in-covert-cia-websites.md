Title: Security Vulnerabilities in Covert CIA Websites
Date: 2022-09-30T14:19:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;CIA;Citizen Lab;cyberespionage;espionage;operational security
Slug: 2022-09-30-security-vulnerabilities-in-covert-cia-websites

[Source](https://www.schneier.com/blog/archives/2022/09/security-vulnerabilities-in-covert-cia-websites.html){:target="_blank" rel="noopener"}

> Back in 2018, we learned that covert system of websites that the CIA used for communications was compromised by —at least—China and Iran, and that the blunder caused a bunch of arrests, imprisonments, and executions. We’re now learning that the CIA is still “using an irresponsibly secured system for asset communication.” Citizen Lab did the research : Using only a single website, as well as publicly available material such as historical internet scanning results and the Internet Archive’s Wayback Machine, we identified a network of 885 websites and have high confidence that the United States (US) Central Intelligence Agency (CIA) [...]
