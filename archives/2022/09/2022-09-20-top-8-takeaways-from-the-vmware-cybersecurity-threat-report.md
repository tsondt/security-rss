Title: Top 8 takeaways from the VMWare Cybersecurity Threat Report
Date: 2022-09-20T10:06:03-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-09-20-top-8-takeaways-from-the-vmware-cybersecurity-threat-report

[Source](https://www.bleepingcomputer.com/news/security/top-8-takeaways-from-the-vmware-cybersecurity-threat-report/){:target="_blank" rel="noopener"}

> VMware has recently released the 2022 edition of its annual Global Incident Response Threat Report. It is critically important for IT professionals to understand these trends and what they could mean for your organization's cyber security efforts. Let's break down VMware's 8 key findings and offer meaningful insights into each. [...]
