Title: Over 1,000 iOS apps found exposing hardcoded AWS credentials
Date: 2022-09-01T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-09-01-over-1000-ios-apps-found-exposing-hardcoded-aws-credentials

[Source](https://www.bleepingcomputer.com/news/security/over-1-000-ios-apps-found-exposing-hardcoded-aws-credentials/){:target="_blank" rel="noopener"}

> Security researchers are raising the alarm about mobile app developers relying on insecure practices that expose Amazon Web Services (AWS) credentials, making the supply chain vulnerable. [...]
