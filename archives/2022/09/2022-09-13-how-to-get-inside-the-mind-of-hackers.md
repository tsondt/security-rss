Title: How to get inside the mind of hackers
Date: 2022-09-13T15:12:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-09-13-how-to-get-inside-the-mind-of-hackers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/13/how_to_get_inside_the/){:target="_blank" rel="noopener"}

> Spanish speaking SANS experts can help the LATAM cyber community detect and respond to attacks Sponsored Post No matter how hard organizations in Latin America try to stop malicious attackers from infiltrating their IT systems, breaches are inevitable – as recent events demonstrate.... [...]
