Title: Wormable Flaw, 0days Lead Sept. 2022 Patch Tuesday
Date: 2022-09-14T00:23:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;Asheer Malhotra;Cisco Talos;Dustin Childs;iOS 16;Jon Munshaw;Kevin Breen;Lily Hay Newman;Lockdown Mode;Microsoft Patch Tuesday September 2022;Pangu Lab;Safety Check;Satnam Narang;trend micro;Xinru Chi
Slug: 2022-09-14-wormable-flaw-0days-lead-sept-2022-patch-tuesday

[Source](https://krebsonsecurity.com/2022/09/wormable-flaw-0days-lead-sept-2022-patch-tuesday/){:target="_blank" rel="noopener"}

> This month’s Patch Tuesday offers a little something for everyone, including security updates for a zero-day flaw in Microsoft Windows that is under active attack, and another Windows weakness experts say could be used to power a fast-spreading computer worm. Also, Apple has also quashed a pair of zero-day bugs affecting certain macOS and iOS users, and released iOS 16, which offers a new privacy and security feature called “ Lockdown Mode.” And Adobe axed 63 vulnerabilities in a range of products. Microsoft today released software patches to plug at least 64 security holes in Windows and related products. Worst [...]
