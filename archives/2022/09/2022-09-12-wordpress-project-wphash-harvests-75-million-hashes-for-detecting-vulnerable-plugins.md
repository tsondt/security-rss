Title: WordPress project WPHash harvests 75 million hashes for detecting vulnerable plugins
Date: 2022-09-12T11:04:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-12-wordpress-project-wphash-harvests-75-million-hashes-for-detecting-vulnerable-plugins

[Source](https://portswigger.net/daily-swig/wordpress-project-wphash-harvests-75-million-hashes-for-detecting-vulnerable-plugins){:target="_blank" rel="noopener"}

> Project mission is to crowdsource the indexing and curating of plugin bug data [...]
