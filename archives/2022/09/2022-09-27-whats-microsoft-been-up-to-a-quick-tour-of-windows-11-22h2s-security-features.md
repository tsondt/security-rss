Title: What's Microsoft been up to? A quick tour of Windows 11 22H2's security features
Date: 2022-09-27T11:32:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-27-whats-microsoft-been-up-to-a-quick-tour-of-windows-11-22h2s-security-features

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/27/microsoft_windows_11_security_features/){:target="_blank" rel="noopener"}

> Some requirements to be aware of In brief As it rolled out a laundry list of features in the latest version of Windows 11, namely version 22H2, this month, Microsoft has also detailed some of the added security mechanisms.... [...]
