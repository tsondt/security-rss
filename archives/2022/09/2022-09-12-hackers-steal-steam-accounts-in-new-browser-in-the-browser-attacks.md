Title: Hackers steal Steam accounts in new Browser-in-the-Browser attacks
Date: 2022-09-12T17:42:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2022-09-12-hackers-steal-steam-accounts-in-new-browser-in-the-browser-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-steam-accounts-in-new-browser-in-the-browser-attacks/){:target="_blank" rel="noopener"}

> Hackers are launching new attacks to steal Steam credentials using a Browser-in-the-Browser phishing technique that is rising in popularity among threat actors. [...]
