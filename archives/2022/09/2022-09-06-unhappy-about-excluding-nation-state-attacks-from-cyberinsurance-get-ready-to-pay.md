Title: Unhappy about excluding nation-state attacks from cyberinsurance? Get ready to pay
Date: 2022-09-06T13:30:10+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-09-06-unhappy-about-excluding-nation-state-attacks-from-cyberinsurance-get-ready-to-pay

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/06/lloyds_cyber_insurance_policy/){:target="_blank" rel="noopener"}

> Lloyd's defends stance as critics say policy tweaks make it less worthwhile to spend on premiums Critics unhappy about insurers excluding certain nation-state attacks from cyber policies should consider the alternative: higher prices, according to Lloyd's of London.... [...]
