Title: Twitter failed to log you out of all devices after password resets
Date: 2022-09-21T15:35:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-21-twitter-failed-to-log-you-out-of-all-devices-after-password-resets

[Source](https://www.bleepingcomputer.com/news/security/twitter-failed-to-log-you-out-of-all-devices-after-password-resets/){:target="_blank" rel="noopener"}

> Twitter logged out some users after addressing a bug where some Twitter accounts remained logged on some mobile devices after voluntary password resets. [...]
