Title: Attorney general flags urgent privacy law changes after Optus data breach
Date: 2022-09-29T00:24:55+00:00
Author: Paul Karp
Category: The Guardian
Tags: Optus;Privacy;Data and computer security;Australian politics;Australia news;Labor party;Telecommunications industry
Slug: 2022-09-29-attorney-general-flags-urgent-privacy-law-changes-after-optus-data-breach

[Source](https://www.theguardian.com/business/2022/sep/29/attorney-general-flags-urgent-privacy-law-changes-in-wake-of-optus-data-breach){:target="_blank" rel="noopener"}

> Mark Dreyfus indicates potential reforms to laws regarding data breaches including higher penalties, mandatory precautions and customer notifications Follow our Australia news live blog for the latest updates Get our free news app, morning email briefing or daily news podcast Privacy law changes, including tougher penalties for data breaches, could be legislated as early as this year, the attorney general has said in the wake of the Optus breach. Mark Dreyfus revealed on Thursday that in addition to completing a review of Australia’s privacy laws the Albanese government will look to legislate “even more urgent reforms” late this year or [...]
