Title: Squiz Matrix CMS squashes admin account takeover bug
Date: 2022-09-05T16:01:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-05-squiz-matrix-cms-squashes-admin-account-takeover-bug

[Source](https://portswigger.net/daily-swig/squiz-matrix-cms-squashes-admin-account-takeover-bug){:target="_blank" rel="noopener"}

> IDOR issue meant user account privileges and contact details could be altered [...]
