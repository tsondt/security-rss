Title: Google, Microsoft can get your passwords via web browser's spellcheck
Date: 2022-09-17T14:39:16-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-09-17-google-microsoft-can-get-your-passwords-via-web-browsers-spellcheck

[Source](https://www.bleepingcomputer.com/news/security/google-microsoft-can-get-your-passwords-via-web-browsers-spellcheck/){:target="_blank" rel="noopener"}

> Enhanced Spellcheck features in Google Chrome and Microsoft Edge web browsers transmit form data, including personally identifiable information (PII) and in some cases, passwords, to Google and Microsoft respectively. [...]
