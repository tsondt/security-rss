Title: The Week in Ransomware - September 30th 2022 - Emerging from the Shadows
Date: 2022-09-30T16:48:13-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-30-the-week-in-ransomware-september-30th-2022-emerging-from-the-shadows

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-30th-2022-emerging-from-the-shadows/){:target="_blank" rel="noopener"}

> This week's news primarily revolves around LockBit, BlackMatter, and the rising enterprise-targeting Royal ransomware operation. [...]
