Title: New Report on IoT Security
Date: 2022-09-27T11:15:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;Internet of Things;reports;security engineering
Slug: 2022-09-27-new-report-on-iot-security

[Source](https://www.schneier.com/blog/archives/2022/09/new-report-on-iot-security.html){:target="_blank" rel="noopener"}

> The Atlantic Council has published a report on securing the Internet of Things: “Security in the Billions: Toward a Multinational Strategy to Better Secure the IoT Ecosystem.” The report examines the regulatory approaches taken by four countries—the US, the UK, Australia, and Singapore—to secure home, medical, and networking/telecommunications devices. The report recommends that regulators should 1) enforce minimum security standards for manufacturers of IoT devices, 2) incentivize higher levels of security through public contracting, and 3) try to align IoT standards internationally (for example, international guidance on handling connected devices that stop receiving security updates). This report looks to existing [...]
