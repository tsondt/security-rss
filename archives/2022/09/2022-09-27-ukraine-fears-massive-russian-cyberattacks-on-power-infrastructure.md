Title: Ukraine fears 'massive' Russian cyberattacks on power, infrastructure
Date: 2022-09-27T00:03:44+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-27-ukraine-fears-massive-russian-cyberattacks-on-power-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/27/russia_plans_massive_cyberattacks_ukraine/){:target="_blank" rel="noopener"}

> Will those be before or after the nuke strikes Putin keeps banging on about? Russia plans to conduct "massive cyberattacks" on Ukraine and its allies' critical infrastructure and energy sector, according to Kyiv.... [...]
