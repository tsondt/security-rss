Title: Classified NATO documents stolen from Portugal, now sold on darkweb
Date: 2022-09-08T09:49:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-08-classified-nato-documents-stolen-from-portugal-now-sold-on-darkweb

[Source](https://www.bleepingcomputer.com/news/security/classified-nato-documents-stolen-from-portugal-now-sold-on-darkweb/){:target="_blank" rel="noopener"}

> The Armed Forces General Staff agency of Portugal (EMGFA) has suffered a cyberattack that allegedly allowed the theft of classified NATO documents, which are now sold on the dark web. [...]
