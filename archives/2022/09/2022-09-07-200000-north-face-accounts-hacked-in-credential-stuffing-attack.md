Title: 200,000 North Face accounts hacked in credential stuffing attack
Date: 2022-09-07T10:40:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-07-200000-north-face-accounts-hacked-in-credential-stuffing-attack

[Source](https://www.bleepingcomputer.com/news/security/200-000-north-face-accounts-hacked-in-credential-stuffing-attack/){:target="_blank" rel="noopener"}

> Outdoor apparel brand 'The North Face' was targeted in a large-scale credential stuffing attack that has resulted in the hacking of 194,905 accounts on the thenorthface.com website. [...]
