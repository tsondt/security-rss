Title: Ukraine dismantles hacker gang that stole 30 million accounts
Date: 2022-09-23T09:24:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-23-ukraine-dismantles-hacker-gang-that-stole-30-million-accounts

[Source](https://www.bleepingcomputer.com/news/security/ukraine-dismantles-hacker-gang-that-stole-30-million-accounts/){:target="_blank" rel="noopener"}

> The cyber department of Ukraine's Security Service (SSU) has taken down a group of hackers that stole accounts of about 30 million individuals and sold them on the dark web. [...]
