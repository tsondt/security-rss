Title: Revolut hack exposes data of 50,000 users, fuels new phishing wave
Date: 2022-09-19T10:13:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-19-revolut-hack-exposes-data-of-50000-users-fuels-new-phishing-wave

[Source](https://www.bleepingcomputer.com/news/security/revolut-hack-exposes-data-of-50-000-users-fuels-new-phishing-wave/){:target="_blank" rel="noopener"}

> Revolut is sending out notices of a data breach to a small percentage of impacted users, informing them of a security incident where an unauthorized third party accessed internal data. [...]
