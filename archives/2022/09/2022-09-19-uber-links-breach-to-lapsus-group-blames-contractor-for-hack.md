Title: Uber links breach to Lapsus$ group, blames contractor for hack
Date: 2022-09-19T14:26:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-19-uber-links-breach-to-lapsus-group-blames-contractor-for-hack

[Source](https://www.bleepingcomputer.com/news/security/uber-links-breach-to-lapsus-group-blames-contractor-for-hack/){:target="_blank" rel="noopener"}

> Uber believes the hacker behind last week's breach is affiliated with the Lapsus$ extortion group, known for breaching other high-profile tech companies such as Microsoft, Cisco, Nvidia, Samsung, and Okta. [...]
