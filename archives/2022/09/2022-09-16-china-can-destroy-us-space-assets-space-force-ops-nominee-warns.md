Title: China can destroy US space assets, Space Force ops nominee warns
Date: 2022-09-16T03:59:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-16-china-can-destroy-us-space-assets-space-force-ops-nominee-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/16/china_working_to_destroy_us/){:target="_blank" rel="noopener"}

> Wants swarms of small satellites that are harder to destroy – and outsourcing to improve cybersecurity The Biden-nominated chief of space operations for the USA's Space Force (USSF) rates China his greatest challenge, as the Middle Kingdom has developed technologies to destroy space assets.... [...]
