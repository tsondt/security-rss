Title: Optus hacker apologizes and allegedly deletes all stolen data
Date: 2022-09-27T10:20:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-27-optus-hacker-apologizes-and-allegedly-deletes-all-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/optus-hacker-apologizes-and-allegedly-deletes-all-stolen-data/){:target="_blank" rel="noopener"}

> The hacker who claimed to have breached Optus and stolen the data of 11 million customers has withdrawn their extortion demands after facing increased attention by law enforcement. The threat actor also apologized to 10,200 people whose personal data was already leaked on a hacking forum. [...]
