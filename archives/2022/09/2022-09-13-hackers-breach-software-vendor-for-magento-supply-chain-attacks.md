Title: Hackers breach software vendor for Magento supply-chain attacks
Date: 2022-09-13T11:21:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-13-hackers-breach-software-vendor-for-magento-supply-chain-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-breach-software-vendor-for-magento-supply-chain-attacks/){:target="_blank" rel="noopener"}

> Hackers have injected malware in multiple extensions from FishPig, a vendor of Magento-WordPress integrations that count over 200,000 downloads. [...]
