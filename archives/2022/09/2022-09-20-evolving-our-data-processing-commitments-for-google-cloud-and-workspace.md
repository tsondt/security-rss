Title: Evolving our data processing commitments for Google Cloud and Workspace
Date: 2022-09-20T16:00:00+00:00
Author: Marc Crandall
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-09-20-evolving-our-data-processing-commitments-for-google-cloud-and-workspace

[Source](https://cloud.google.com/blog/products/identity-security/evolving-google-clouds-data-processing-commitments/){:target="_blank" rel="noopener"}

> At Google, we are constantly looking to improve our products, services, and contracts so that we can better serve our customers. To this end, we are pleased to announce that we have updated and merged our data processing terms for Google Cloud, Google Workspace (including Workspace for Education), and Cloud Identity (when purchased separately) into one combined Cloud Data Processing Addendum (the “CDPA”). The CDPA maintains the benefits of the previously separate Data Processing and Security Terms for Google Cloud customers and Data Processing Amendment for Google Workspace and Cloud Identity customers, while streamlining and strengthening Google’s data processing commitments. [...]
