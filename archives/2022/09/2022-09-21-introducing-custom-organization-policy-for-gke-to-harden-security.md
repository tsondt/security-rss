Title: Introducing Custom Organization Policy for GKE to harden security
Date: 2022-09-21T16:00:00+00:00
Author: Daniel L’Hommedieu
Category: GCP Security
Tags: Containers & Kubernetes;Identity & Security
Slug: 2022-09-21-introducing-custom-organization-policy-for-gke-to-harden-security

[Source](https://cloud.google.com/blog/products/identity-security/introducing-custom-organization-policy-for-gke-in-preview/){:target="_blank" rel="noopener"}

> Compliance officers and platform engineering teams often find it challenging to ensure security, manage consistency, and oversee governance across multiple products, environments, and teams. Google Cloud's Organization Policy Service can help tackle this challenge with a policy-based approach that simplifies policy administration across Google Cloud resources and projects. We’re excited to announce the Preview release of Custom Organization Policy, and to showcase the integration with Google Kubernetes Engine (GKE). Custom organization policy for GKE can improve security and efficiency using guardrails you define tailored to your organization's needs, and it’s offered to Google Cloud customers at no additional cost. A [...]
