Title: Newly discovered cyberspy crew targets Asian governments and corporations
Date: 2022-09-06T16:15:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-06-newly-discovered-cyberspy-crew-targets-asian-governments-and-corporations

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/06/worok_espionage_asia/){:target="_blank" rel="noopener"}

> Worok uses mix of publicly available tools, custom malware to steal info, gang active since 2020 A cyberespionage group has targeted government agencies and big-name corporations throughout Asia since at least 2020, using the notorious ProxyShell vulnerabilities in Microsoft Exchange to gain initial access.... [...]
