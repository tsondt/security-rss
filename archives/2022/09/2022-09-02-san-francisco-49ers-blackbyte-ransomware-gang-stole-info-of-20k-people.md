Title: San Francisco 49ers: Blackbyte ransomware gang stole info of 20K people
Date: 2022-09-02T09:12:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-02-san-francisco-49ers-blackbyte-ransomware-gang-stole-info-of-20k-people

[Source](https://www.bleepingcomputer.com/news/security/san-francisco-49ers-blackbyte-ransomware-gang-stole-info-of-20k-people/){:target="_blank" rel="noopener"}

> NFL's San Francisco 49ers are mailing notification letters confirming a data breach affecting more than 20,000 individuals following a ransomware attack that hit its network earlier this year. [...]
