Title: Use AWS Network Firewall to filter outbound HTTPS traffic from applications hosted on Amazon EKS and collect hostnames provided by SNI
Date: 2022-09-12T17:45:35+00:00
Author: Kirankumar Chandrashekar
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS Network Firewall;EKS;Security Blog;Terraform;VPC
Slug: 2022-09-12-use-aws-network-firewall-to-filter-outbound-https-traffic-from-applications-hosted-on-amazon-eks-and-collect-hostnames-provided-by-sni

[Source](https://aws.amazon.com/blogs/security/use-aws-network-firewall-to-filter-outbound-https-traffic-from-applications-hosted-on-amazon-eks/){:target="_blank" rel="noopener"}

> This blog post shows how to set up an Amazon Elastic Kubernetes Service (Amazon EKS) cluster such that the applications hosted on the cluster can have their outbound internet access restricted to a set of hostnames provided by the Server Name Indication (SNI) in the allow list in the AWS Network Firewall rules. For encrypted [...]
