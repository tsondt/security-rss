Title: Microsoft Exchange servers hacked via OAuth apps for phishing
Date: 2022-09-22T13:13:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-09-22-microsoft-exchange-servers-hacked-via-oauth-apps-for-phishing

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-servers-hacked-via-oauth-apps-for-phishing/){:target="_blank" rel="noopener"}

> Microsoft says a threat actor gained access to cloud tenants hosting Microsoft Exchange servers in credential stuffing attacks, with the end goal of deploying malicious OAuth applications and sending phishing emails. [...]
