Title: SQL Server admins warned about Fargo ransomware
Date: 2022-09-26T16:00:11+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-09-26-sql-server-admins-warned-about-fargo-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/26/sql_server_fargo_ransomware/){:target="_blank" rel="noopener"}

> From small town in North Dakota with a crime problem to file-scrambling nasty Organizations are being warned about a wave of attacks targeting Microsoft SQL Server with ransomware known as Fargo, which encrypts files and threatens victims that their data may be published online if they do not pay up.... [...]
