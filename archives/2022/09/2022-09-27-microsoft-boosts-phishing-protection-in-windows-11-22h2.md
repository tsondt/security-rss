Title: Microsoft boosts phishing protection in Windows 11 22H2
Date: 2022-09-27T14:00:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-09-27-microsoft-boosts-phishing-protection-in-windows-11-22h2

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/27/microsoft_phishing_password_protect_windows_11/){:target="_blank" rel="noopener"}

> Security tool warns admins and users when a password is used on an untrusted site or stored locally In the latest version of Windows 11, Microsoft is introducing a feature in its Microsoft Defender SmartScreen tool designed to keep passwords safer.... [...]
