Title: American Airlines discloses data breach after employee email compromise
Date: 2022-09-19T17:50:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-19-american-airlines-discloses-data-breach-after-employee-email-compromise

[Source](https://www.bleepingcomputer.com/news/security/american-airlines-discloses-data-breach-after-employee-email-compromise/){:target="_blank" rel="noopener"}

> American Airlines has notified customers of a recent data breach after attackers compromised an undisclosed number of employee email accounts and gained access to sensitive personal information. [...]
