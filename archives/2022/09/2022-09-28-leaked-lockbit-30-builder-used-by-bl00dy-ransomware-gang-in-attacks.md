Title: Leaked LockBit 3.0 builder used by ‘Bl00dy’ ransomware gang in attacks
Date: 2022-09-28T03:30:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-28-leaked-lockbit-30-builder-used-by-bl00dy-ransomware-gang-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/leaked-lockbit-30-builder-used-by-bl00dy-ransomware-gang-in-attacks/){:target="_blank" rel="noopener"}

> The relatively new Bl00Dy Ransomware Gang has started to use a recently leaked LockBit ransomware builder in attacks against companies. [...]
