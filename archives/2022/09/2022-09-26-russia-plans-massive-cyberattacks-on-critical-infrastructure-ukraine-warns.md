Title: Russia plans “massive cyberattacks” on critical infrastructure, Ukraine warns
Date: 2022-09-26T19:14:57+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;critical infrastrucgture;hacking;power grid;russia;Ukraine
Slug: 2022-09-26-russia-plans-massive-cyberattacks-on-critical-infrastructure-ukraine-warns

[Source](https://arstechnica.com/?p=1884593){:target="_blank" rel="noopener"}

> Enlarge (credit: gwengoat | Getty Images) The Ukrainian government on Monday warned that the Kremlin is planning to carry out “massive cyberattacks” targeting power grids and other critical infrastructure in Ukraine and in the territories of its allies. “By the cyberattacks, the enemy will try to increase the effect of missile strikes on electricity supply facilities, primarily in the eastern and southern regions of Ukraine,” an advisory warned. “The occupying command is convinced that this will slow down the offensive operations of the Ukrainian Defence Forces.” Monday’s advisory alluded to two cyberattacks the Russian government carried out—first in 2015 and [...]
