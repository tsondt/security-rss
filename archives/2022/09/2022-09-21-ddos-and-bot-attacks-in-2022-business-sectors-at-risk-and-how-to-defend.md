Title: DDoS and bot attacks in 2022: Business sectors at risk and how to defend
Date: 2022-09-21T10:05:10-04:00
Author: Sponsored by Gcore
Category: BleepingComputer
Tags: Security
Slug: 2022-09-21-ddos-and-bot-attacks-in-2022-business-sectors-at-risk-and-how-to-defend

[Source](https://www.bleepingcomputer.com/news/security/ddos-and-bot-attacks-in-2022-business-sectors-at-risk-and-how-to-defend/){:target="_blank" rel="noopener"}

> According to Gcore, in 2022, the number and volume of DDoS attacks will roughly double compared to 2021. The average attack power will grow from 150-300 Gbps to 500-700 Gbps. Andrew Slastenov, Head of Web Security, at Gcore talks to his colleagues about trends in the cybersecurity market: [...]
