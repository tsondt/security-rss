Title: Google: Former Conti cybercrime gang members now targeting Ukraine
Date: 2022-09-07T07:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-09-07-google-former-conti-cybercrime-gang-members-now-targeting-ukraine

[Source](https://www.bleepingcomputer.com/news/security/google-former-conti-cybercrime-gang-members-now-targeting-ukraine/){:target="_blank" rel="noopener"}

> Google says some former Conti ransomware gang members, now part of a threat group tracked as UAC-0098, are targeting Ukrainian organizations and European non-governmental organizations (NGOs). [...]
