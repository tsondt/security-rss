Title: TikTok denies security breach after hackers leak user data, source code
Date: 2022-09-05T09:52:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-05-tiktok-denies-security-breach-after-hackers-leak-user-data-source-code

[Source](https://www.bleepingcomputer.com/news/security/tiktok-denies-security-breach-after-hackers-leak-user-data-source-code/){:target="_blank" rel="noopener"}

> TikTok denies recent claims it was breached, and source code and user data were stolen, telling BleepingComputer that data posted to a hacking forum is "completely unrelated" to the company. [...]
