Title: Lazarus Group unleashed a MagicRAT to spy on energy providers
Date: 2022-09-08T12:00:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-08-lazarus-group-unleashed-a-magicrat-to-spy-on-energy-providers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/08/lazarus_group_energy_firms_trade_secrets/){:target="_blank" rel="noopener"}

> Cisco finds custom malware in North Korea's latest cyberespionage effort The North Korean state-sponsored crime ring Lazarus Group is behind a new cyberespionage campaign with the goal to steal data and trade secrets from energy providers across the US, Canada and Japan, according to Cisco Talos.... [...]
