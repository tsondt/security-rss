Title: Germany arrests hacker for stealing €4 million via phishing attacks
Date: 2022-09-30T10:31:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-30-germany-arrests-hacker-for-stealing-4-million-via-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/germany-arrests-hacker-for-stealing-4-million-via-phishing-attacks/){:target="_blank" rel="noopener"}

> Germany's Bundeskriminalamt (BKA), the country's federal criminal police, carried out raids on the homes of three individuals yesterday suspected of orchestrating large-scale phishing campaigns that defrauded internet users of €4,000,000. [...]
