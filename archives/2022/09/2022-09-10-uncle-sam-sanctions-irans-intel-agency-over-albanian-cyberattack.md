Title: Uncle Sam sanctions Iran's intel agency over Albanian cyberattack
Date: 2022-09-10T13:00:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-10-uncle-sam-sanctions-irans-intel-agency-over-albanian-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/10/us_sanctions_iran_mois/){:target="_blank" rel="noopener"}

> Iranians won't be terrified, but US vendors need to check their customers The US Treasury Department has issued sactions against Iran's intelligence agency in response to that country's cyberattack against Albania and other "cyber-enabled activities against the United States and its allies."... [...]
