Title: Grand Theft Auto 6 maker confirms source code, vids stolen in cyber-heist
Date: 2022-09-19T17:12:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-19-grand-theft-auto-6-maker-confirms-source-code-vids-stolen-in-cyber-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/19/grand_theft_auto_6_hacked/){:target="_blank" rel="noopener"}

> So is that three or four stars? Take-Two Interactive confirmed on Monday that its Rockstar Games subsidiary has been compromised and confidential data for Grand Theft Auto 6 has been stolen.... [...]
