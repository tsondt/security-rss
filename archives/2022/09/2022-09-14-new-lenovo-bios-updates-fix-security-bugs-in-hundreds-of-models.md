Title: New Lenovo BIOS updates fix security bugs in hundreds of models
Date: 2022-09-14T13:43:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-14-new-lenovo-bios-updates-fix-security-bugs-in-hundreds-of-models

[Source](https://www.bleepingcomputer.com/news/security/new-lenovo-bios-updates-fix-security-bugs-in-hundreds-of-models/){:target="_blank" rel="noopener"}

> Chinese computer manufacturer Lenovo has issued a security advisory to warn its clients about several high-severity vulnerabilities impacting a wide range of products in the Desktop, All in One, Notebook, ThinkPad, ThinkServer, and ThinkStation lines. [...]
