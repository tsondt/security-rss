Title: Police arrest man for laundering tens of millions in stolen crypto
Date: 2022-09-13T11:42:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Legal
Slug: 2022-09-13-police-arrest-man-for-laundering-tens-of-millions-in-stolen-crypto

[Source](https://www.bleepingcomputer.com/news/security/police-arrest-man-for-laundering-tens-of-millions-in-stolen-crypto/){:target="_blank" rel="noopener"}

> The Dutch police arrested a 39-year-old man on suspicions of laundering tens of millions of euros worth of cryptocurrency stolen in phishing attacks. [...]
