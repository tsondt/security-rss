Title: Adware on Google Play and Apple Store installed 13 million times
Date: 2022-09-26T11:33:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-26-adware-on-google-play-and-apple-store-installed-13-million-times

[Source](https://www.bleepingcomputer.com/news/security/adware-on-google-play-and-apple-store-installed-13-million-times/){:target="_blank" rel="noopener"}

> Security researchers have discovered 75 applications on Google Play and another ten on Apple's App Store engaged in ad fraud. Collectively, they add to 13 million installations. [...]
