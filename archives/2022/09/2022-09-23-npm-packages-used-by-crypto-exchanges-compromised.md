Title: npm packages used by crypto exchanges compromised
Date: 2022-09-23T12:31:54-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-09-23-npm-packages-used-by-crypto-exchanges-compromised

[Source](https://www.bleepingcomputer.com/news/security/npm-packages-used-by-crypto-exchanges-compromised/){:target="_blank" rel="noopener"}

> Multiple npm packages published by the crypto exchange, dYdX, and used by at least 44 cryptocurrency projects, appear to have been compromised. Powered by the Ethereum blockchain, dydX is a decentralized exchange platform offering perpetual trading options for over 35 popular cryptocurrencies including Bitcoin (BTC) and Ether (ETH). [...]
