Title: Web security flaw in Sophos Firewall patched
Date: 2022-09-26T14:02:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-26-web-security-flaw-in-sophos-firewall-patched

[Source](https://portswigger.net/daily-swig/web-security-flaw-in-sophos-firewall-patched){:target="_blank" rel="noopener"}

> Code injection vulnerability harnessed in attacks on south Asia [...]
