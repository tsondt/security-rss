Title: Phishing page embeds keylogger to steal passwords as you type
Date: 2022-09-14T11:30:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-14-phishing-page-embeds-keylogger-to-steal-passwords-as-you-type

[Source](https://www.bleepingcomputer.com/news/security/phishing-page-embeds-keylogger-to-steal-passwords-as-you-type/){:target="_blank" rel="noopener"}

> A novel phishing campaign is underway, targeting Greeks with phishing sites that mimic the state's official tax refund platform and steal credentials as they type them. [...]
