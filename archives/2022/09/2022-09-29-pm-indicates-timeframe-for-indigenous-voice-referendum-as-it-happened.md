Title: PM indicates timeframe for Indigenous voice referendum – as it happened
Date: 2022-09-29T08:34:12+00:00
Author: Stephanie Convery and Natasha May (earlier)
Category: The Guardian
Tags: Australia news;Australian politics;Optus;Anthony Albanese;Medicare Australia;Australia weather;Labor party;Coalition;Australian economy;Cybercrime;Data and computer security
Slug: 2022-09-29-pm-indicates-timeframe-for-indigenous-voice-referendum-as-it-happened

[Source](https://www.theguardian.com/australia-news/live/2022/sep/29/australia-news-live-politics-optus-medicare-fuel-excise-anthony-albanese-labor-liberal-indigenous-voice-cost-of-living-weather){:target="_blank" rel="noopener"}

> Australian academic Sean Turnell sentenced to three years’ jail after secret trial in Myanmar Federal government strikes deal with gas suppliers to avoid winter shortfall Get our free news app, morning email briefing or daily news podcast Queensland seeking partnerships from the federal government in renewable plan The Queensland premier, Annastacia Palaszczuk, was asked to clarify how long the state will keep exporting coal for: There’s still going to be countries that need our coal and, of course, the metallurgical coal [that] is needed for steel production. Let’s be clear about that. Until there’s alternative to manufacturing steel, the world [...]
