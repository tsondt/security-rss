Title: Ex-NSA trio who spied on Americans for UAE now banned from arms exports
Date: 2022-09-02T01:11:24+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-02-ex-nsa-trio-who-spied-on-americans-for-uae-now-banned-from-arms-exports

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/02/us_cyber_spies_debarred/){:target="_blank" rel="noopener"}

> From hero to zero-day... to plain zero Three former US government cyber-spies who, among other things, illicitly compromised and snooped on Americans' devices for the United Arab Emirates government have been banned from participating in international arms exports under a deal reached with Uncle Sam.... [...]
