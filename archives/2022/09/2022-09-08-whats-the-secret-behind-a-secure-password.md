Title: What’s the secret behind a secure password?
Date: 2022-09-08T09:30:06+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-09-08-whats-the-secret-behind-a-secure-password

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/08/whats_the_secret_behind_a/){:target="_blank" rel="noopener"}

> Intelligent, uncompromising software according to Specops Webinar Passwords are the first line of defense against bad actors gaining illegal access to data, a protective rampart that too often falls to common mistakes and increasingly sophisticated cyberattacks.... [...]
