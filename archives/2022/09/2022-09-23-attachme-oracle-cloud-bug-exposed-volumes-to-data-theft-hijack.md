Title: #AttachMe Oracle cloud bug exposed volumes to data theft, hijack
Date: 2022-09-23T09:45:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-23-attachme-oracle-cloud-bug-exposed-volumes-to-data-theft-hijack

[Source](https://portswigger.net/daily-swig/attachme-oracle-cloud-bug-exposed-volumes-to-data-theft-hijack){:target="_blank" rel="noopener"}

> Vulnerability could have been used to bypass cloud isolation protection [...]
