Title: QNAP patches zero-day used in new Deadbolt ransomware attacks
Date: 2022-09-05T11:49:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-05-qnap-patches-zero-day-used-in-new-deadbolt-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/qnap-patches-zero-day-used-in-new-deadbolt-ransomware-attacks/){:target="_blank" rel="noopener"}

> QNAP is warning customers of ongoing DeadBolt ransomware attacks that started on Saturday by exploiting a zero-day vulnerability in Photo Station. [...]
