Title: Musk seeks yet another excuse to get out of Twitter buyout: This time it's Mudge's severance check
Date: 2022-09-13T00:03:26+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-09-13-musk-seeks-yet-another-excuse-to-get-out-of-twitter-buyout-this-time-its-mudges-severance-check

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/13/twitter_says_775m_severance_payment/){:target="_blank" rel="noopener"}

> If at first you don't succeed... Elon Musk has come up with a new reason to get out of his acquisition of Twitter - a severance payment.... [...]
