Title: Using AWS Shield Advanced protection groups to improve DDoS detection and mitigation
Date: 2022-09-09T19:46:07+00:00
Author: Joe Viggiano
Category: AWS Security
Tags: Expert (400);Security, Identity, & Compliance;Technical How-to;DDoS;Protected Resources;Protection Groups;Security Blog;Shield Advanced
Slug: 2022-09-09-using-aws-shield-advanced-protection-groups-to-improve-ddos-detection-and-mitigation

[Source](https://aws.amazon.com/blogs/security/using-aws-shield-advanced-protection-groups-to-improve-ddos-detection-and-mitigation/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) customers can use AWS Shield Advanced to detect and mitigate distributed denial of service (DDoS) attacks that target their applications running on Amazon Elastic Compute Cloud (Amazon EC2), Elastic Local Balancing (ELB), Amazon CloudFront, AWS Global Accelerator, and Amazon Route 53. By using protection groups for Shield Advanced, you can logically [...]
