Title: Hacker shares how they allegedly breached Fast Company’s site
Date: 2022-09-28T16:53:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-09-28-hacker-shares-how-they-allegedly-breached-fast-companys-site

[Source](https://www.bleepingcomputer.com/news/security/hacker-shares-how-they-allegedly-breached-fast-company-s-site/){:target="_blank" rel="noopener"}

> Fast Company took its website offline after it was hacked to display stories and push out Apple News notifications containing obscene and racist comments. Today, the hacker shared how they allegedly breached the site. [...]
