Title: Ex-Broadcom engineer asks for house arrest over IP theft
Date: 2022-09-15T15:15:11+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-09-15-ex-broadcom-engineer-asks-for-house-arrest-over-ip-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/15/broadcom_engineer_ip_theft/){:target="_blank" rel="noopener"}

> Admits guilt, but claims he took files to jog his memory, afraid he'd not keep up with 'younger engineers' A former Broadcom engineer who pleaded guilty to stealing his ex-employer's trade secrets has asked the court not to give him prison time, saying he stole the files for reference, fearing he would "be unable to keep up" with "more technical and younger engineers" at a new startup.... [...]
