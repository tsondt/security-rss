Title: Microsoft: Iranian hackers encrypt Windows systems using BitLocker
Date: 2022-09-08T11:30:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-09-08-microsoft-iranian-hackers-encrypt-windows-systems-using-bitlocker

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-iranian-hackers-encrypt-windows-systems-using-bitlocker/){:target="_blank" rel="noopener"}

> Microsoft says an Iranian state-sponsored threat group it tracks as DEV-0270 (aka Nemesis Kitten) has been abusing the BitLocker Windows feature in attacks to encrypt victims' systems. [...]
