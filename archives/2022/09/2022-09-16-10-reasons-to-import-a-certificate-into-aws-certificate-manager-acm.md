Title: 10 reasons to import a certificate into AWS Certificate Manager (ACM)
Date: 2022-09-16T18:36:05+00:00
Author: Nicholas Doropoulos
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;AWS Certificate Manager;Security Blog
Slug: 2022-09-16-10-reasons-to-import-a-certificate-into-aws-certificate-manager-acm

[Source](https://aws.amazon.com/blogs/security/10-reasons-to-import-a-certificate-into-aws-certificate-manager-acm/){:target="_blank" rel="noopener"}

> AWS Certificate Manager (ACM) is a service that lets you efficiently provision, manage, and deploy public and private SSL/TLS certificates for use with AWS services and your internal connected resources. The certificates issued by ACM can then be used to secure network communications and establish the identity of websites on the internet or resources on [...]
