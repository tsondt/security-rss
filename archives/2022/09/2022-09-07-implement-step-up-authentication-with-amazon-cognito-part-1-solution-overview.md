Title: Implement step-up authentication with Amazon Cognito, Part 1: Solution overview
Date: 2022-09-07T17:36:35+00:00
Author: Salman Moghal
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;adaptive authentication;API Gateway;api security;Cognito;DynamoDB;lambda;Security Blog;Serverless;step-up authentication
Slug: 2022-09-07-implement-step-up-authentication-with-amazon-cognito-part-1-solution-overview

[Source](https://aws.amazon.com/blogs/security/implement-step-up-authentication-with-amazon-cognito-part-1-solution-overview/){:target="_blank" rel="noopener"}

> In this blog post, you’ll learn how to protect privileged business transactions that are exposed as APIs by using multi-factor authentication (MFA) or security challenges. These challenges have two components: what you know (such as passwords), and what you have (such as a one-time password token). By using these multi-factor security controls, you can implement [...]
