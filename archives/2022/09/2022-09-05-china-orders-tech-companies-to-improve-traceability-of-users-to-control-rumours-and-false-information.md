Title: China orders tech companies to 'improve traceability' of users to control 'rumours and false information'
Date: 2022-09-05T00:32:47+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-09-05-china-orders-tech-companies-to-improve-traceability-of-users-to-control-rumours-and-false-information

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/05/asia_in_brief/){:target="_blank" rel="noopener"}

> PLUS: Australia mints a physical crypto-coin; Alibaba Cloud claims world's biggest DC; India’s space airbags; and more China will conduct a three month blitz to cleanse the local internet of "rumors and false information".... [...]
