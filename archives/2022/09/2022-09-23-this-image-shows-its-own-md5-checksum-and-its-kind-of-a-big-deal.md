Title: This image shows its own MD5 checksum — and it's kind of a big deal
Date: 2022-09-23T07:32:28-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-09-23-this-image-shows-its-own-md5-checksum-and-its-kind-of-a-big-deal

[Source](https://www.bleepingcomputer.com/news/security/this-image-shows-its-own-md5-checksum-and-its-kind-of-a-big-deal/){:target="_blank" rel="noopener"}

> Generating checksums—cryptographic hashes such as MD5 or SHA-256 functions for files is hardly anything new and one of the most efficient means to ascertain the integrity of a file, or to check if two files are identical. But a researcher has generated an image that visibly contains its own MD5 hash. [...]
