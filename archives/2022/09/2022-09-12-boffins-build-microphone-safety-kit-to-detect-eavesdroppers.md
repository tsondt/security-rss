Title: Boffins build microphone safety kit to detect eavesdroppers
Date: 2022-09-12T07:30:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-12-boffins-build-microphone-safety-kit-to-detect-eavesdroppers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/12/mic_monitoring_spying/){:target="_blank" rel="noopener"}

> TickTock mic lock won't work on Apple Scientists from the National University of Singapore and Yonsei University in the Republic of Korea have developed a device for verifying whether your laptop microphone is secretly recording your conversations.... [...]
