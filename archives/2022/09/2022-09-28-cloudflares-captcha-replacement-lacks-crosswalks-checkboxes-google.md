Title: Cloudflare’s CAPTCHA replacement lacks crosswalks, checkboxes, Google
Date: 2022-09-28T20:00:18+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Tech;CAPTCHA;cloudflare;google;reCAPTCHA;web development
Slug: 2022-09-28-cloudflares-captcha-replacement-lacks-crosswalks-checkboxes-google

[Source](https://arstechnica.com/?p=1885349){:target="_blank" rel="noopener"}

> Enlarge / CAPTCHAs are meant to prevent these kinds of browsing scenarios, not train us all to better recognize vehicles and infrastructure in grainy photos. (credit: Getty Images) Cloudflare has recently made an audacious claim: We could all be doing something better with our lives than deciding which images contain crosswalks or stop lights or clicking an "I'm not a robot" checkbox. Now the cloud services company is offering up a free CAPTCHA alternative, Turnstile, available to anyone, Cloudflare customer or not, and specifically calling out Google's role in the existing "prove you're a human" hegemony. Turnstile utilizes Cloudflare's Managed [...]
