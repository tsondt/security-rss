Title: Okta: Credential stuffing accounts for 34% of all login attempts
Date: 2022-09-21T09:17:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-21-okta-credential-stuffing-accounts-for-34-of-all-login-attempts

[Source](https://www.bleepingcomputer.com/news/security/okta-credential-stuffing-accounts-for-34-percent-of-all-login-attempts/){:target="_blank" rel="noopener"}

> Credential stuffing attacks have become so prevalent in the first quarter of 2022 that their traffic surpassed that of legitimate login attempts from normal users in some countries. [...]
