Title: GIFShell attack creates reverse shell using Microsoft Teams GIFs
Date: 2022-09-08T15:28:21-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-09-08-gifshell-attack-creates-reverse-shell-using-microsoft-teams-gifs

[Source](https://www.bleepingcomputer.com/news/security/gifshell-attack-creates-reverse-shell-using-microsoft-teams-gifs/){:target="_blank" rel="noopener"}

> A new attack technique called 'GIFShell' allows threat actors to abuse Microsoft Teams for novel phishing attacks and covertly executing commands to steal data using... GIFs. [...]
