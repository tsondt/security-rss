Title: Privacy watchdog steps up fight against Europol's hoarding of personal data
Date: 2022-09-23T06:27:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-23-privacy-watchdog-steps-up-fight-against-europols-hoarding-of-personal-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/23/europol_data_privacy_legal_edps/){:target="_blank" rel="noopener"}

> If you could stop storing records on people unconnected to any crimes, that would be great An EU watchdog says rules that allow Europol cops to retain personal data on individuals with no links to criminal activity go against Europe's own data privacy protections, not to mention undermining the regulator's powers and role.... [...]
