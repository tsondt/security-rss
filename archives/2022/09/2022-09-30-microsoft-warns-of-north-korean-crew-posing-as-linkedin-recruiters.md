Title: Microsoft warns of North Korean crew posing as LinkedIn recruiters
Date: 2022-09-30T05:53:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-09-30-microsoft-warns-of-north-korean-crew-posing-as-linkedin-recruiters

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/30/microsoft_north_korea_zinc_threat/){:target="_blank" rel="noopener"}

> State-sponsored ZINC allegedly passes on malware-laden open source apps Microsoft has claimed a North Korean crew poses as LinkedIn recruiters to distribute poisoned versions of open source software packages.... [...]
