Title: Google urges open source community to fuzz test code
Date: 2022-09-08T21:00:59+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-08-google-urges-open-source-community-to-fuzz-test-code

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/08/google_fuzz_rewards/){:target="_blank" rel="noopener"}

> We'll even get our checkbook out, web giant says Google's open source security team says OSS-Fuzz, its community fuzzing service, has helped fix more than 8,000 security vulnerabilities and 26,000 other bugs in open source projects since its 2016 debut.... [...]
