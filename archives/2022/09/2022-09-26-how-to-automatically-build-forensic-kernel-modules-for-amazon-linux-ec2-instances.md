Title: How to automatically build forensic kernel modules for Amazon Linux EC2 instances
Date: 2022-09-26T16:00:07+00:00
Author: Jonathan Nguyen
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Automation;Cognito;Security Blog;WAF
Slug: 2022-09-26-how-to-automatically-build-forensic-kernel-modules-for-amazon-linux-ec2-instances

[Source](https://aws.amazon.com/blogs/security/how-to-automatically-build-forensic-kernel-modules-for-amazon-linux-ec2-instances/){:target="_blank" rel="noopener"}

> In this blog post, we will walk you through the EC2 forensic module factory solution to deploy automation to build forensic kernel modules that are required for Amazon Elastic Compute Cloud (Amazon EC2) incident response automation. When an EC2 instance is suspected to have been compromised, it’s strongly recommended to investigate what happened to the [...]
