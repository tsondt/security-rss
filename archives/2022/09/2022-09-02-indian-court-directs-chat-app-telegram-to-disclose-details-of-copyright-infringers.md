Title: Indian court directs chat app Telegram to disclose details of copyright infringers
Date: 2022-09-02T14:15:09+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-09-02-indian-court-directs-chat-app-telegram-to-disclose-details-of-copyright-infringers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/02/telegram_court_ip/){:target="_blank" rel="noopener"}

> Judge says that servers being located in Singapore is not a get-out clause A ruling handed down from the Delhi High Court this week declared that Telegram must hand over information such as IP addresses, mobile numbers, and devices used by channels on the platform involved in copyright infringement.... [...]
