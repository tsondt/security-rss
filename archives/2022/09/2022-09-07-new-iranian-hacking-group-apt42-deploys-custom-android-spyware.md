Title: New Iranian hacking group APT42 deploys custom Android spyware
Date: 2022-09-07T10:18:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Government;Mobile
Slug: 2022-09-07-new-iranian-hacking-group-apt42-deploys-custom-android-spyware

[Source](https://www.bleepingcomputer.com/news/security/new-iranian-hacking-group-apt42-deploys-custom-android-spyware/){:target="_blank" rel="noopener"}

> A new Iranian state-sponsored hacking group known as APT42 has been discovered using a custom Android malware to spy on targets of interest. [...]
