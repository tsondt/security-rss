Title: Look who's fallen foul of Europe's data retention rules. France and Germany
Date: 2022-09-21T06:32:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-09-21-look-whos-fallen-foul-of-europes-data-retention-rules-france-and-germany

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/21/eu_data_retention/){:target="_blank" rel="noopener"}

> 'Indiscriminate' preemptive harvesting of personal info a big no-no. What a novel concept On Tuesday, the European Court of Justice (ECJ) issued rulings that limit indiscriminate data retention in France and Germany.... [...]
