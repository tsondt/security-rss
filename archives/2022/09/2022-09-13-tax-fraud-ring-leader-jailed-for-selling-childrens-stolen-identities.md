Title: Tax fraud ring leader jailed for selling children’s stolen identities
Date: 2022-09-13T13:14:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-09-13-tax-fraud-ring-leader-jailed-for-selling-childrens-stolen-identities

[Source](https://www.bleepingcomputer.com/news/security/tax-fraud-ring-leader-jailed-for-selling-children-s-stolen-identities/){:target="_blank" rel="noopener"}

> The owner of a fraudulent tax preparation business, Ariel Jimenez, was sentenced to 12 years in prison for selling the stolen identities of children on welfare and helping "customers" to falsely claim tax credits, causing tens of millions of dollars in tax loss. [...]
