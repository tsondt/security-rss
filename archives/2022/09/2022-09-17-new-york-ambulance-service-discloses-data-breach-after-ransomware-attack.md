Title: New York ambulance service discloses data breach after ransomware attack
Date: 2022-09-17T10:12:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2022-09-17-new-york-ambulance-service-discloses-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/new-york-ambulance-service-discloses-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Empress EMS (Emergency Medical Services), a New York-based emergency response and ambulance service provider, has disclosed a data breach that exposed customer information. [...]
