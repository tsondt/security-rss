Title: Hacked Fast Company sends 'obscene and racist' alerts via Apple News
Date: 2022-09-28T16:30:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-28-hacked-fast-company-sends-obscene-and-racist-alerts-via-apple-news

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/28/fast_company_hack_apple_news/){:target="_blank" rel="noopener"}

> Someone going by 'Thrax' claims responsibility for 'incredibly easy' breach Apple News shut down Fast Company's news channel after "an incredibly offensive alert" was sent to subscribers following a hack of the business publication on Tuesday evening.... [...]
