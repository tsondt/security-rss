Title: School chat app Seesaw abused to send 'inappropriate image' to parents, teachers
Date: 2022-09-16T21:45:39+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-09-16-school-chat-app-seesaw-abused-to-send-inappropriate-image-to-parents-teachers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/16/seesaw_inappropriate_image/){:target="_blank" rel="noopener"}

> This is why we don't reuse passwords, kids Parents and teachers received a link to an "inappropriate image" this week via Seesaw after miscreants hijacked accounts in a credential stuffing attack against the popular school messaging app.... [...]
