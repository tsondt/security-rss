Title: How botnet attacks work and how to defend against them
Date: 2022-09-19T10:05:10-04:00
Author: Sponsored by Gcore
Category: BleepingComputer
Tags: Security
Slug: 2022-09-19-how-botnet-attacks-work-and-how-to-defend-against-them

[Source](https://www.bleepingcomputer.com/news/security/how-botnet-attacks-work-and-how-to-defend-against-them/){:target="_blank" rel="noopener"}

> Experts believe that the development of serverless technologies will further simplify the creation of botnets for DDoS attacks. Here's how Gcore can counter these threats. [...]
