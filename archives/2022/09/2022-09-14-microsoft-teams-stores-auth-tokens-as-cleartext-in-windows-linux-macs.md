Title: Microsoft Teams stores auth tokens as cleartext in Windows, Linux, Macs
Date: 2022-09-14T11:40:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-09-14-microsoft-teams-stores-auth-tokens-as-cleartext-in-windows-linux-macs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-teams-stores-auth-tokens-as-cleartext-in-windows-linux-macs/){:target="_blank" rel="noopener"}

> Security analysts have found a severe security vulnerability in the desktop app for Microsoft Teams that gives threat actors access to authentication tokens and accounts with multi-factor authentication (MFA) turned on. [...]
