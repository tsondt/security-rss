Title: Let’s Encrypt builds infrastructure to support browser-based certificate revocation revival
Date: 2022-09-13T14:39:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-13-lets-encrypt-builds-infrastructure-to-support-browser-based-certificate-revocation-revival

[Source](https://portswigger.net/daily-swig/lets-encrypt-builds-infrastructure-to-support-browser-based-certificate-revocation-revival){:target="_blank" rel="noopener"}

> CRLs are back, baby! [...]
