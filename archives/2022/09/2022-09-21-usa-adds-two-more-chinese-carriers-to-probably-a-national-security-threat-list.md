Title: USA adds two more Chinese carriers to 'probably a national security threat' list
Date: 2022-09-21T04:58:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-09-21-usa-adds-two-more-chinese-carriers-to-probably-a-national-security-threat-list

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/21/fcc_puts_pacific_network_and/){:target="_blank" rel="noopener"}

> Pacific Network Corp and China Unicom join the likes of Huawei, Hytera, Hikvision on list of dangerous suppliers The US Federal Communications Commission (FCC) has added two Chinese companies to its list of communications equipment suppliers rated a threat to national security: Pacific Network Corp, its wholly owned subsidiary ComNet (USA) LLC, and China Unicom (Americas).... [...]
