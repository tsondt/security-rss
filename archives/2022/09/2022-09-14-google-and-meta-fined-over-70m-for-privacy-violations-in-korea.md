Title: Google and Meta fined over $70m for privacy violations in Korea
Date: 2022-09-14T10:25:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-09-14-google-and-meta-fined-over-70m-for-privacy-violations-in-korea

[Source](https://go.theregister.com/feed/www.theregister.com/2022/09/14/google_and_meta_fined_over/){:target="_blank" rel="noopener"}

> Both search giant and Facebook parent claim they play by the rules, will challenge decision South Korea's Personal Information Protection Commission (PIPC) has issued two large fines for privacy violations: a $50 million penalty for Google and $22 million for Meta.... [...]
