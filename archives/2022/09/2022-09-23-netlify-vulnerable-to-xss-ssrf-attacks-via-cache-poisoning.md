Title: Netlify vulnerable to XSS, SSRF attacks via cache poisoning
Date: 2022-09-23T16:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-09-23-netlify-vulnerable-to-xss-ssrf-attacks-via-cache-poisoning

[Source](https://portswigger.net/daily-swig/netlify-vulnerable-to-xss-ssrf-attacks-via-cache-poisoning){:target="_blank" rel="noopener"}

> Issue has since been fixed [...]
