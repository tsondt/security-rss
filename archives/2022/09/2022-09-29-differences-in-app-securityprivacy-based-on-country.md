Title: Differences in App Security/Privacy Based on Country
Date: 2022-09-29T11:14:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;Android;censorship;data collection;geolocation;privacy
Slug: 2022-09-29-differences-in-app-securityprivacy-based-on-country

[Source](https://www.schneier.com/blog/archives/2022/09/differences-in-app-security-privacy-based-on-country.html){:target="_blank" rel="noopener"}

> Depending on where you are when you download your Android apps, it might collect more or less data about you. The apps we downloaded from Google Play also showed differences based on country in their security and privacy capabilities. One hundred twenty-seven apps varied in what the apps were allowed to access on users’ mobile phones, 49 of which had additional permissions deemed “dangerous” by Google. Apps in Bahrain, Tunisia and Canada requested the most additional dangerous permissions. Three VPN apps enable clear text communication in some countries, which allows unauthorized access to users’ communications. One hundred and eighteen apps [...]
