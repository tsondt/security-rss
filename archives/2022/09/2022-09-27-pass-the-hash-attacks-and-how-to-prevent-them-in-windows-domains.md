Title: Pass-the-Hash Attacks and How to Prevent them in Windows Domains
Date: 2022-09-27T10:05:10-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-09-27-pass-the-hash-attacks-and-how-to-prevent-them-in-windows-domains

[Source](https://www.bleepingcomputer.com/news/security/pass-the-hash-attacks-and-how-to-prevent-them-in-windows-domains/){:target="_blank" rel="noopener"}

> Hackers often start out with nothing more than a low-level user account and then work to gain additional privileges that will allow them to take over the network. One of the methods that is commonly used to acquire these privileges is a pass-the-hash attack. Here are five steps to prevent a pass-the-hash attack in a Windows domain. [...]
