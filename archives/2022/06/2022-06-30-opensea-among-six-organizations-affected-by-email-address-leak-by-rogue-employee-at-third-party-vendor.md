Title: OpenSea among six organizations affected by email address leak by rogue employee at third-party vendor
Date: 2022-06-30T13:10:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-30-opensea-among-six-organizations-affected-by-email-address-leak-by-rogue-employee-at-third-party-vendor

[Source](https://portswigger.net/daily-swig/opensea-among-six-organizations-affected-by-email-address-leak-by-rogue-employee-at-third-party-vendor){:target="_blank" rel="noopener"}

> All users who shared their email address with NFT marketplace told: ‘Assume you were impacted’ [...]
