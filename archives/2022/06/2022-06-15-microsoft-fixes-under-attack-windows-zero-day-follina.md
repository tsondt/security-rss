Title: Microsoft fixes under-attack Windows zero-day Follina
Date: 2022-06-15T03:02:37+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-15-microsoft-fixes-under-attack-windows-zero-day-follina

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/15/microsoft_patch_tuesday/){:target="_blank" rel="noopener"}

> Plus: Intel, AMD react to Hertzbleed data-leaking holes in CPUs Patch Tuesday Microsoft claims to have finally fixed the Follina zero-day flaw in Windows as part of its June Patch Tuesday batch, which included security updates to address 55 vulnerabilities.... [...]
