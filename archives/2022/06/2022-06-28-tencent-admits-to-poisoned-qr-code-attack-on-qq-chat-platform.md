Title: Tencent admits to poisoned QR code attack on QQ chat platform
Date: 2022-06-28T04:31:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-28-tencent-admits-to-poisoned-qr-code-attack-on-qq-chat-platform

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/28/tencent_qq_qr_code_attack/){:target="_blank" rel="noopener"}

> Could it be Beijing was right about games being bad for China? Chinese web giant Tencent has admitted to a significant account hijack attack on its QQ.com messaging and social media platform.... [...]
