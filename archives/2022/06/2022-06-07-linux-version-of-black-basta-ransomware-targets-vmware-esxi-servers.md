Title: Linux version of Black Basta ransomware targets VMware ESXi servers
Date: 2022-06-07T15:06:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-07-linux-version-of-black-basta-ransomware-targets-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-black-basta-ransomware-targets-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> Black Basta is the latest ransomware gang to add support for encrypting VMware ESXi virtual machines running on enterprise Linux servers. [...]
