Title: Bank of the West found debit card-stealing skimmers on ATMs
Date: 2022-06-27T15:44:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-27-bank-of-the-west-found-debit-card-stealing-skimmers-on-atms

[Source](https://www.bleepingcomputer.com/news/security/bank-of-the-west-found-debit-card-stealing-skimmers-on-atms/){:target="_blank" rel="noopener"}

> The Bank of the West is warning customers that their debit card numbers and PINs have been stolen by skimmers installed on several of the bank's ATMs. [...]
