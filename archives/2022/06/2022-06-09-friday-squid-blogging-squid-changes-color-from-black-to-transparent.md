Title: Friday Squid Blogging: Squid Changes Color from Black to Transparent
Date: 2022-06-09T19:33:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-06-09-friday-squid-blogging-squid-changes-color-from-black-to-transparent

[Source](https://www.schneier.com/blog/archives/2022/06/friday-squid-blogging-squid-changes-color-from-black-to-transparent.html){:target="_blank" rel="noopener"}

> Neat video. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
