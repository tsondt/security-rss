Title: Correlate IAM Access Analyzer findings with Amazon Macie
Date: 2022-06-03T16:43:04+00:00
Author: Nihar Das
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Amazon Macie;AWS;AWS Security Hub;IAM Access Analyzer;Security Blog;Sensitive data
Slug: 2022-06-03-correlate-iam-access-analyzer-findings-with-amazon-macie

[Source](https://aws.amazon.com/blogs/security/correlate-iam-access-analyzer-findings-with-amazon-macie/){:target="_blank" rel="noopener"}

> In this blog post, you’ll learn how to detect when unintended access has been granted to sensitive data in Amazon Simple Storage Service (Amazon S3) buckets in your Amazon Web Services (AWS) accounts. It’s critical for your enterprise to understand where sensitive data is stored in your organization and how and why it is shared. [...]
