Title: Ukraine's secret cyber-defense that blunts Russian attacks: excellent backups
Date: 2022-06-08T05:15:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-08-ukraines-secret-cyber-defense-that-blunts-russian-attacks-excellent-backups

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/08/silverados_alperovitch_viasat_attack/){:target="_blank" rel="noopener"}

> This is why the Viasat attack – rated one of the biggest ever – had little impact RSA Conference The Kremlin-backed cyberattack against satellite communications provider Viasat, which happened an hour before Russia invaded Ukraine, was "one of the biggest cyber events that we have seen, perhaps ever, and certainly in warfare," according to Dmitri Alperovitch, a co-founder of CrowdStrike and chair of security-centric think tank Silverado Policy Accelerator.... [...]
