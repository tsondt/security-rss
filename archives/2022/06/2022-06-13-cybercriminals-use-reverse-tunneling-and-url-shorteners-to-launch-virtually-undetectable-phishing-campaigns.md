Title: Cybercriminals use reverse tunneling and URL shorteners to launch ‘virtually undetectable’ phishing campaigns
Date: 2022-06-13T14:44:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-13-cybercriminals-use-reverse-tunneling-and-url-shorteners-to-launch-virtually-undetectable-phishing-campaigns

[Source](https://portswigger.net/daily-swig/cybercriminals-use-reverse-tunneling-and-url-shorteners-to-launch-virtually-undetectable-phishing-campaigns){:target="_blank" rel="noopener"}

> New hacking technique allows threat actors to evade some of the most effective phishing countermeasures [...]
