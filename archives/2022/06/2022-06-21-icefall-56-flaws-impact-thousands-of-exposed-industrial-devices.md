Title: Icefall: 56 flaws impact thousands of exposed industrial devices
Date: 2022-06-21T07:20:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-21-icefall-56-flaws-impact-thousands-of-exposed-industrial-devices

[Source](https://www.bleepingcomputer.com/news/security/icefall-56-flaws-impact-thousands-of-exposed-industrial-devices/){:target="_blank" rel="noopener"}

> A security report has been published on a set of 56 vulnerabilities that are collectively called Icefall and affect operational technology (OT) equipment used in various critical infrastructure environments. [...]
