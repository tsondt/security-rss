Title: Hospitals are for healing humans. But protecting and healing hospitals needs machines
Date: 2022-06-01T07:15:07+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: 2022-06-01-hospitals-are-for-healing-humans-but-protecting-and-healing-hospitals-needs-machines

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/01/hospitals_are_for_healing_humans/){:target="_blank" rel="noopener"}

> AI technology is helping hospitals repel ransomware at machine speed Sponsored Feature Browse through a selection of hospital mission statements and common themes quickly emerge: putting patients and community first, acting with integrity, pushing the bounds of medical research.... [...]
