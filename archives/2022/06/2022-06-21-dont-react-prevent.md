Title: Don’t react, prevent
Date: 2022-06-21T16:38:36+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-06-21-dont-react-prevent

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/21/dont_react_prevent/){:target="_blank" rel="noopener"}

> The art of cyber warfare needs more than just defence Webinar As general and military strategist Sun Tzu once wrote, "attack is the secret of defence; defence is the planning of attack".... [...]
