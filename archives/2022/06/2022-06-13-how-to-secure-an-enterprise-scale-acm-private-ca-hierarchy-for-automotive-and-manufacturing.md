Title: How to secure an enterprise scale ACM Private CA hierarchy for automotive and manufacturing
Date: 2022-06-13T15:38:46+00:00
Author: Anthony Pasquariello
Category: AWS Security
Tags: Expert (400);Security, Identity, & Compliance;automotive;AWS Certificate Manager;Certificate Lifecycle;certificate monitoring;Enterprise Strategy;Identity & Compliance;Manufacturing;Security;Security Blog;Supplier;Vehicle
Slug: 2022-06-13-how-to-secure-an-enterprise-scale-acm-private-ca-hierarchy-for-automotive-and-manufacturing

[Source](https://aws.amazon.com/blogs/security/how-to-secure-an-enterprise-scale-acm-private-ca-hierarchy-for-automotive-and-manufacturing/){:target="_blank" rel="noopener"}

> In this post, we show how you can use the AWS Certificate Manager Private Certificate Authority (ACM Private CA) to help follow security best practices when you build a CA hierarchy. This blog post walks through certificate authority (CA) lifecycle management topics, including an architecture overview, centralized security, separation of duties, certificate issuance auditing, and [...]
