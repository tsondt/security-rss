Title: Chinese 'Aoqin Dragon' gang runs undetected ten-year espionage spree
Date: 2022-06-10T04:58:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-10-chinese-aoqin-dragon-gang-runs-undetected-ten-year-espionage-spree

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/10/aoqin_dragon_china_apt/){:target="_blank" rel="noopener"}

> Researcher spots it targeting Asian government and telco targets, probably with Beijing's approval Threat researcher Joey Chen of Sentinel Labs says he's spotted a decade worth of cyber attacks he's happy to attribute to a single Chinese gang.... [...]
