Title: Microsoft Exchange servers worldwide hit by stealthy new backdoor
Date: 2022-06-30T21:57:06+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Uncategorized;Exchange Server;IIS;malware;microsoft
Slug: 2022-06-30-microsoft-exchange-servers-worldwide-hit-by-stealthy-new-backdoor

[Source](https://arstechnica.com/?p=1863868){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Researchers have identified stealthy new malware that threat actors have been using for the past 15 months to backdoor Microsoft Exchange servers after they have been hacked. Dubbed SessionManager, the malicious software poses as a legitimate module for Internet Information Services (IIS), the web server installed by default on Exchange servers. Organizations often deploy IIS modules to streamline specific processes on their web infrastructure. Researchers from security firm Kaspersky have identified 34 servers belonging to 24 organizations that have been infected with SessionManager since March 2021. As of earlier this month, Kaspersky said, 20 organizations remained [...]
