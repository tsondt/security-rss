Title: Novartis says no sensitive data was compromised in cyberattack
Date: 2022-06-03T15:30:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-03-novartis-says-no-sensitive-data-was-compromised-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/novartis-says-no-sensitive-data-was-compromised-in-cyberattack/){:target="_blank" rel="noopener"}

> Pharmaceutical giant Novartis says no sensitive data was compromised in a recent cyberattack by the Industrial Spy data-extortion gang. [...]
