Title: Cuba ransomware returns to extorting victims with updated encryptor
Date: 2022-06-08T10:55:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-08-cuba-ransomware-returns-to-extorting-victims-with-updated-encryptor

[Source](https://www.bleepingcomputer.com/news/security/cuba-ransomware-returns-to-extorting-victims-with-updated-encryptor/){:target="_blank" rel="noopener"}

> The Cuba ransomware operation has returned to regular operations with a new version of its malware found used in recent attacks. [...]
