Title: InQuest Labs: Man + Machine vs Business Email Compromise (BEC)
Date: 2022-06-15T10:01:02-04:00
Author: Sponsored by InQuest
Category: BleepingComputer
Tags: Security
Slug: 2022-06-15-inquest-labs-man-machine-vs-business-email-compromise-bec

[Source](https://www.bleepingcomputer.com/news/security/inquest-labs-man-plus-machine-vs-business-email-compromise-bec/){:target="_blank" rel="noopener"}

> Attackers only have to be right once while defenders need to be right 100% of the time. To help combat this asymmetric disadvantage, InQuest provides an open research portal that combines crowdsourced efforts with machine learning to combat the likes of Bumblebee and other BEC related threats. [...]
