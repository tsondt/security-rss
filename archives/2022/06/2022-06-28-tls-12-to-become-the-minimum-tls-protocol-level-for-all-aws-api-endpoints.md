Title: TLS 1.2 to become the minimum TLS protocol level for all AWS API endpoints
Date: 2022-06-28T16:57:32+00:00
Author: Janelle Hopper
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS CloudTrail;AWS Cloudwatch;FedRAMP;Security Blog;TLS;Transport Layer Security
Slug: 2022-06-28-tls-12-to-become-the-minimum-tls-protocol-level-for-all-aws-api-endpoints

[Source](https://aws.amazon.com/blogs/security/tls-1-2-required-for-aws-endpoints/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we continuously innovate to deliver you a cloud computing environment that works to help meet the requirements of the most security-sensitive organizations. To respond to evolving technology and regulatory standards for Transport Layer Security (TLS), we will be updating the TLS configuration for all AWS service API endpoints to a [...]
