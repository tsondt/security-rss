Title: Evil Corp Pivots LockBit to Dodge U.S. Sanctions
Date: 2022-06-03T12:42:41+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: 2022-06-03-evil-corp-pivots-lockbit-to-dodge-us-sanctions

[Source](https://threatpost.com/evil-corp-pivots-to-lockbit-to-dodge-u-s-sanctions/179858/){:target="_blank" rel="noopener"}

> The cybercriminal group is distancing itself from its previous branding by shifting tactics and tools once again in an aim to continue to profit from its nefarious activity. [...]
