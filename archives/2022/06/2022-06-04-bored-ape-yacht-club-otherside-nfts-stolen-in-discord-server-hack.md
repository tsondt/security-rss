Title: Bored Ape Yacht Club, Otherside NFTs stolen in Discord server hack
Date: 2022-06-04T15:23:45-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-06-04-bored-ape-yacht-club-otherside-nfts-stolen-in-discord-server-hack

[Source](https://www.bleepingcomputer.com/news/security/bored-ape-yacht-club-otherside-nfts-stolen-in-discord-server-hack/){:target="_blank" rel="noopener"}

> Hackers reportedly stole over $257,000 in Ethereum and thirty-two NFTs after the Yuga Lab's Bored Ape Yacht Club and Otherside Metaverse Discord servers were compromised to post a phishing scam. [...]
