Title: Ransomware gang now hacks corporate websites to show ransom notes
Date: 2022-06-02T15:01:51-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-02-ransomware-gang-now-hacks-corporate-websites-to-show-ransom-notes

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-now-hacks-corporate-websites-to-show-ransom-notes/){:target="_blank" rel="noopener"}

> A ransomware gang is taking extortion to a new level by publicly hacking corporate websites to publicly display ransom notes. [...]
