Title: Chinese cyber threat actors are widely abusing well-known attacks to infiltrate networks, CISA warns
Date: 2022-06-09T15:18:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-09-chinese-cyber-threat-actors-are-widely-abusing-well-known-attacks-to-infiltrate-networks-cisa-warns

[Source](https://portswigger.net/daily-swig/chinese-cyber-threat-actors-are-widely-abusing-well-known-attacks-to-infiltrate-networks-cisa-warns){:target="_blank" rel="noopener"}

> APTs hammering unpatched vulnerabilities [...]
