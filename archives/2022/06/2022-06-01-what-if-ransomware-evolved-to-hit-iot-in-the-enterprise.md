Title: What if ransomware evolved to hit IoT in the enterprise?
Date: 2022-06-01T06:34:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-01-what-if-ransomware-evolved-to-hit-iot-in-the-enterprise

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/01/ransomware_iot_devices/){:target="_blank" rel="noopener"}

> Proof-of-concept lab work demos potential future threat Forescout researchers have demonstrated how ransomware could spread through an enterprise from vulnerable Internet-of-Things gear.... [...]
