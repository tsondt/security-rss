Title: HID Mercury access control vulnerabilities leave door open to lock manipulation
Date: 2022-06-14T15:54:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-14-hid-mercury-access-control-vulnerabilities-leave-door-open-to-lock-manipulation

[Source](https://portswigger.net/daily-swig/hid-mercury-access-control-vulnerabilities-leave-door-open-to-lock-manipulation){:target="_blank" rel="noopener"}

> Manufacturer addresses threat to integrity and availability of physical access systems sold by LenelS2 [...]
