Title: AstraLocker 2.0 infects users directly from Word attachments
Date: 2022-06-30T08:12:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-30-astralocker-20-infects-users-directly-from-word-attachments

[Source](https://www.bleepingcomputer.com/news/security/astralocker-20-infects-users-directly-from-word-attachments/){:target="_blank" rel="noopener"}

> A lesser-known ransomware strain called AstraLocker has recently released its second major version, and according to threat analysts, its operators engage in rapid attacks that drop its payload directly from email attachments. [...]
