Title: New Vytal Chrome extension hides location info that your VPN can't
Date: 2022-06-12T13:25:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-06-12-new-vytal-chrome-extension-hides-location-info-that-your-vpn-cant

[Source](https://www.bleepingcomputer.com/news/security/new-vytal-chrome-extension-hides-location-info-that-your-vpn-cant/){:target="_blank" rel="noopener"}

> A new Google Chrome browser extension called Vytal prevents webpages from using programming APIs to find your geographic location leaked, even when using a VPN. [...]
