Title: Hundreds of Elasticsearch databases targeted in ransom attacks
Date: 2022-06-01T15:13:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-01-hundreds-of-elasticsearch-databases-targeted-in-ransom-attacks

[Source](https://www.bleepingcomputer.com/news/security/hundreds-of-elasticsearch-databases-targeted-in-ransom-attacks/){:target="_blank" rel="noopener"}

> A campaign targeting poorly secured Elasticsearch databases has deleted their contents and dropped ransom notes on 450 instances, demanding a payment of $620 to give them back their indexes, totaling a demand of $279,000. [...]
