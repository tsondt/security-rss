Title: Google battles bots, puts Workspace admins on alert
Date: 2022-06-30T16:00:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-30-google-battles-bots-puts-workspace-admins-on-alert

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/30/google_workspace_admins_alert/){:target="_blank" rel="noopener"}

> No security alert fatigue here Google has added API security tools and Workspace (formerly G-Suite) admin alerts about potentially risky configuration changes such as super admin passwords resets.... [...]
