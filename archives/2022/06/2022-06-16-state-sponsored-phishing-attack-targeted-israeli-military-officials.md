Title: State-Sponsored Phishing Attack Targeted Israeli Military Officials
Date: 2022-06-16T11:59:09+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Government
Slug: 2022-06-16-state-sponsored-phishing-attack-targeted-israeli-military-officials

[Source](https://threatpost.com/phishing-attack-israeli-officials/179987/){:target="_blank" rel="noopener"}

> Analysts have uncovered an Iran-linked APT sending malicious emails to top Israeli government officials. [...]
