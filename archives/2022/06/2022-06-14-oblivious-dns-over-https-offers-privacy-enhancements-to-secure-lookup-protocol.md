Title: Oblivious DNS-over-HTTPS offers privacy enhancements to secure lookup protocol
Date: 2022-06-14T14:46:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-14-oblivious-dns-over-https-offers-privacy-enhancements-to-secure-lookup-protocol

[Source](https://portswigger.net/daily-swig/oblivious-dns-over-https-offers-privacy-enhancements-to-secure-lookup-protocol){:target="_blank" rel="noopener"}

> ODoH is said to enhance user privacy without compromising performance [...]
