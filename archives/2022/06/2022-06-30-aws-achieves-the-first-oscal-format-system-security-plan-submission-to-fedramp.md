Title: AWS achieves the first OSCAL format system security plan submission to FedRAMP
Date: 2022-06-30T16:26:21+00:00
Author: Matthew Donkin
Category: AWS Security
Tags: Announcements;AWS GovCloud (US);Federal;Foundational (100);Government;Public Sector;Security, Identity, & Compliance;AWS (US) GovCloud;AWS East/West;DoD;Federal government;FedRAMP;OSCAL;Security Blog
Slug: 2022-06-30-aws-achieves-the-first-oscal-format-system-security-plan-submission-to-fedramp

[Source](https://aws.amazon.com/blogs/security/aws-achieves-the-first-oscal-format-system-security-plan-submission-to-fedramp/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is the first cloud service provider to produce an Open Security Control Assessment Language (OSCAL)–formatted system security plan (SSP) for the FedRAMP Project Management Office (PMO). OSCAL is the first step in the AWS effort to automate security documentation to simplify our customers’ journey through cloud adoption and accelerate the authorization [...]
