Title: Ransomware Risk in Healthcare Endangers Patients
Date: 2022-06-16T11:24:26+00:00
Author: Ryan Witt
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-06-16-ransomware-risk-in-healthcare-endangers-patients

[Source](https://threatpost.com/ransomware-risk-healthcare/179980/){:target="_blank" rel="noopener"}

> Ryan Witt, Proofpoint's Healthcare Cybersecurity Leader, examines the impact of ransomware on patient care. [...]
