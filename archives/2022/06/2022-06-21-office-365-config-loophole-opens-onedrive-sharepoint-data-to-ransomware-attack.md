Title: Office 365 Config Loophole Opens OneDrive, SharePoint Data to Ransomware Attack
Date: 2022-06-21T12:34:43+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Hacks
Slug: 2022-06-21-office-365-config-loophole-opens-onedrive-sharepoint-data-to-ransomware-attack

[Source](https://threatpost.com/office-365-opens-ransomware-attacks-on-onedrive-sharepoint/180010/){:target="_blank" rel="noopener"}

> A reported a "potentially dangerous piece of functionality" allows an attacker to launch an attack on cloud infrastructure and ransom files stored in SharePoint and OneDrive. [...]
