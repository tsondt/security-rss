Title: OpenSea phishing threat after rogue insider leaks customer email addresses
Date: 2022-06-30T21:20:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-30-opensea-phishing-threat-after-rogue-insider-leaks-customer-email-addresses

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/30/opensea_data_breach_phishing/){:target="_blank" rel="noopener"}

> Worse, imagine someone finding out you bought one of its NFTs The choppy waters continue at OpenSea, whose security boss this week disclosed the NFT marketplace suffered an insider attack that could lead to hundreds of thousands of people fending off phishing attempts.... [...]
