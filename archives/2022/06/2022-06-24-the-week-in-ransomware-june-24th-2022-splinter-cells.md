Title: The Week in Ransomware - June 24th 2022 - Splinter Cells
Date: 2022-06-24T18:20:35-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-24-the-week-in-ransomware-june-24th-2022-splinter-cells

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-24th-2022-splinter-cells/){:target="_blank" rel="noopener"}

> The Conti ransomware gang has finally ended their charade and turned off their Tor data leak and negotiation sites, effectively shutting down the operation. [...]
