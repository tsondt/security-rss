Title: Elusive ToddyCat APT Targets Microsoft Exchange Servers
Date: 2022-06-22T12:18:33+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Government;Malware
Slug: 2022-06-22-elusive-toddycat-apt-targets-microsoft-exchange-servers

[Source](https://threatpost.com/elusive-toddycat-apt-targets-microsoft-exchange-servers/180031/){:target="_blank" rel="noopener"}

> The threat actor targets institutions and companies in Europe and Asia. [...]
