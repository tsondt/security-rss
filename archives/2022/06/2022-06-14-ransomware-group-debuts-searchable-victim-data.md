Title: Ransomware Group Debuts Searchable Victim Data
Date: 2022-06-14T19:53:12+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ransomware;The Coming Storm;ALPHV ransomware;BlackCat ransomware;Brett Callow;Emsisoft
Slug: 2022-06-14-ransomware-group-debuts-searchable-victim-data

[Source](https://krebsonsecurity.com/2022/06/ransomware-group-debuts-searchable-victim-data/){:target="_blank" rel="noopener"}

> Cybercrime groups that specialize in stealing corporate data and demanding a ransom not to publish it have tried countless approaches to shaming their victims into paying. The latest innovation in ratcheting up the heat comes from the ALPHV/BlackCat ransomware group, which has traditionally published any stolen victim data on the Dark Web. Today, however, the group began publishing individual victim websites on the public Internet, with the leaked data made available in an easily searchable form. The ALPHV site claims to care about people’s privacy, but they let anyone view the sensitive stolen data. ALPHV recently announced on its victim [...]
