Title: Remotely Controlling Touchscreens
Date: 2022-06-02T20:59:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;malware;side-channel attacks
Slug: 2022-06-02-remotely-controlling-touchscreens

[Source](https://www.schneier.com/blog/archives/2022/06/remotely-controlling-touchscreens.html){:target="_blank" rel="noopener"}

> Researchers have demonstrated controlling touchscreens at a distance, at least in a laboratory setting: The core idea is to take advantage of the electromagnetic signals to execute basic touch events such as taps and swipes into targeted locations of the touchscreen with the goal of taking over remote control and manipulating the underlying device. The attack, which works from a distance of up to 40mm, hinges on the fact that capacitive touchscreens are sensitive to EMI, leveraging it to inject electromagnetic signals into transparent electrodes that are built into the touchscreen so as to register them as touch events. The [...]
