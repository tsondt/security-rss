Title: 7-zip now supports Windows ‘Mark-of-the-Web’ security feature
Date: 2022-06-21T17:46:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security;Software
Slug: 2022-06-21-7-zip-now-supports-windows-mark-of-the-web-security-feature

[Source](https://www.bleepingcomputer.com/news/microsoft/7-zip-now-supports-windows-mark-of-the-web-security-feature/){:target="_blank" rel="noopener"}

> 7-zip has finally added support for the long-requested 'Mark-of-the-Web' Windows security feature, providing better protection from malicious downloaded files. [...]
