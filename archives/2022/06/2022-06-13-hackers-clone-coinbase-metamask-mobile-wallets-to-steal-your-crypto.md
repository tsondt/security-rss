Title: Hackers clone Coinbase, MetaMask mobile wallets to steal your crypto
Date: 2022-06-13T15:32:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-06-13-hackers-clone-coinbase-metamask-mobile-wallets-to-steal-your-crypto

[Source](https://www.bleepingcomputer.com/news/security/hackers-clone-coinbase-metamask-mobile-wallets-to-steal-your-crypto/){:target="_blank" rel="noopener"}

> Security researchers have uncovered a large-scale malicious operation that uses trojanized mobile cryptocurrency wallet applications for Coinbase, MetaMask, TokenPocket, and imToken services. [...]
