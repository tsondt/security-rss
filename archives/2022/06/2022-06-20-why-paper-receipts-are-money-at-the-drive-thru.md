Title: Why Paper Receipts are Money at the Drive-Thru
Date: 2022-06-20T17:56:48+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Employment Fraud;Jimmy John's;Steve Saladin;U.S. Internal Revenue Service
Slug: 2022-06-20-why-paper-receipts-are-money-at-the-drive-thru

[Source](https://krebsonsecurity.com/2022/06/why-paper-receipts-are-money-at-the-drive-thru/){:target="_blank" rel="noopener"}

> Check out this handmade sign posted to the front door of a shuttered Jimmy John’s sandwich chain shop in Missouri last week. See if you can tell from the store owner’s message what happened. If you guessed that someone in the Jimmy John’s store might have fallen victim to a Business Email Compromise (BEC) or “CEO fraud” scheme — wherein the scammers impersonate company executives to steal money — you’d be in good company. In fact, that was my initial assumption when a reader in Missouri shared this photo after being turned away from his favorite local sub shop. But [...]
