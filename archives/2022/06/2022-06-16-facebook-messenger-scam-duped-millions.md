Title: Facebook Messenger Scam Duped Millions
Date: 2022-06-16T10:59:40+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Hacks;Privacy
Slug: 2022-06-16-facebook-messenger-scam-duped-millions

[Source](https://threatpost.com/acebook-messenger-scam/179977/){:target="_blank" rel="noopener"}

> One well crafted phishing message sent via Facebook Messenger ensnared 10 million Facebook users and counting. [...]
