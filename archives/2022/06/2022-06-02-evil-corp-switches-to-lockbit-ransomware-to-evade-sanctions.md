Title: Evil Corp switches to LockBit ransomware to evade sanctions
Date: 2022-06-02T16:35:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-02-evil-corp-switches-to-lockbit-ransomware-to-evade-sanctions

[Source](https://www.bleepingcomputer.com/news/security/evil-corp-switches-to-lockbit-ransomware-to-evade-sanctions/){:target="_blank" rel="noopener"}

> The Evil Corp cybercrime group has now switched to deploying LockBit ransomware on targets' networks to evade sanctions imposed by the U.S. Treasury Department's Office of Foreign Assets Control (OFAC). [...]
