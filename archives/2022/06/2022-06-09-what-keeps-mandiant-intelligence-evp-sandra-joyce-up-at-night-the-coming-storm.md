Title: What keeps Mandiant Intelligence EVP Sandra Joyce up at night? The coming storm
Date: 2022-06-09T23:00:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-09-what-keeps-mandiant-intelligence-evp-sandra-joyce-up-at-night-the-coming-storm

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/09/mandiant_intelligence_joyce/){:target="_blank" rel="noopener"}

> The next wave of security maturity is measuring effectiveness, she told The Register RSA Conference When Sandra Joyce, EVP of Mandiant Intelligence, describes the current threat landscape, it sounds like the perfect storm.... [...]
