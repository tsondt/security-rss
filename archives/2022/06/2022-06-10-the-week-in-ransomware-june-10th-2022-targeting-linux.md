Title: The Week in Ransomware - June 10th 2022 - Targeting Linux
Date: 2022-06-10T18:18:21-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-10-the-week-in-ransomware-june-10th-2022-targeting-linux

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-10th-2022-targeting-linux/){:target="_blank" rel="noopener"}

> It has been relatively quiet this week with many companies and researchers at the RSA conference. However, we still had some interesting ransomware reports released this week. [...]
