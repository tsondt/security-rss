Title: Researcher goes public with WordPress CSP bypass hack
Date: 2022-06-01T16:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-01-researcher-goes-public-with-wordpress-csp-bypass-hack

[Source](https://portswigger.net/daily-swig/researcher-goes-public-with-wordpress-csp-bypass-hack){:target="_blank" rel="noopener"}

> Technique skirts web security controls [...]
