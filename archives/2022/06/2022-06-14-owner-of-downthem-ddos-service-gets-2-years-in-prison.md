Title: Owner of ‘DownThem’ DDoS service gets 2 years in prison
Date: 2022-06-14T11:40:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-14-owner-of-downthem-ddos-service-gets-2-years-in-prison

[Source](https://www.bleepingcomputer.com/news/security/owner-of-downthem-ddos-service-gets-2-years-in-prison/){:target="_blank" rel="noopener"}

> Matthew Gatrel, 33, a citizen of Illinois, has been sentenced to two years in prison for operating platforms offering DDoS (distributed denial of service) services to subscribers. [...]
