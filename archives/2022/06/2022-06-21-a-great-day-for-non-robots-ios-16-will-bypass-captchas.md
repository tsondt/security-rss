Title: A great day for non-robots: iOS 16 will bypass CAPTCHAs
Date: 2022-06-21T11:45:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-06-21-a-great-day-for-non-robots-ios-16-will-bypass-captchas

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/21/believe_it_or_not_apple/){:target="_blank" rel="noopener"}

> A bot says what? Apple relies on IETF standards to remove annoyance, citing privacy and accessibility Apple has introduced a game-changer into its upcoming iOS 16 for those who hate CAPTCHAs, in the form of a feature called Automatic Verification.... [...]
