Title: Breaking Down the Zola Hack and Why Password Reuse is so Dangerous
Date: 2022-06-28T10:02:01-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-06-28-breaking-down-the-zola-hack-and-why-password-reuse-is-so-dangerous

[Source](https://www.bleepingcomputer.com/news/security/breaking-down-the-zola-hack-and-why-password-reuse-is-so-dangerous/){:target="_blank" rel="noopener"}

> In May of 2022, the wedding planning and registry site Zola suffered a major security breach due to a credential stuffing attack. due to password reuse. Here's what happened and what could have been done to prevent the attack. [...]
