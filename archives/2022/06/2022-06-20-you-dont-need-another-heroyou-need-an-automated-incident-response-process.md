Title: You don’t need another hero…you need an automated incident response process
Date: 2022-06-20T09:18:09+00:00
Author: Martin Courtney
Category: The Register
Tags: 
Slug: 2022-06-20-you-dont-need-another-heroyou-need-an-automated-incident-response-process

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/20/you_dont_need_another_heroyou/){:target="_blank" rel="noopener"}

> Head here to find out how to get one Webinar Have you got a few of those special people who can deal quickly and efficiently with any "incident", preventing it escalating into a full-blown crisis.... [...]
