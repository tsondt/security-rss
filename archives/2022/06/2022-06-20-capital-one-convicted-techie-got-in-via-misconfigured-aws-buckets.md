Title: Capital One: Convicted techie got in via 'misconfigured' AWS buckets
Date: 2022-06-20T13:32:25+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-06-20-capital-one-convicted-techie-got-in-via-misconfigured-aws-buckets

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/20/captial_one_wire_fraud/){:target="_blank" rel="noopener"}

> Assistant US attorney: 'She wanted data, she wanted money, and she wanted to brag' Updated A former Seattle tech worker has been convicted of wire fraud and computer intrusions in a US federal district court.... [...]
