Title: Leaky Access Tokens Exposed Amazon Photos of Users
Date: 2022-06-29T20:18:50+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Mobile Security;Privacy
Slug: 2022-06-29-leaky-access-tokens-exposed-amazon-photos-of-users

[Source](https://threatpost.com/exposed-amazon-photos/180105/){:target="_blank" rel="noopener"}

> Hackers with Amazon users’ authentication tokens could’ve stolen or encrypted personal photos and documents. [...]
