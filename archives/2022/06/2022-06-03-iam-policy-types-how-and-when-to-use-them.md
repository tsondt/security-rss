Title: IAM policy types: How and when to use them
Date: 2022-06-03T19:48:11+00:00
Author: Matt Luttrell
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;IAM;Security;Security Blog
Slug: 2022-06-03-iam-policy-types-how-and-when-to-use-them

[Source](https://aws.amazon.com/blogs/security/iam-policy-types-how-and-when-to-use-them/){:target="_blank" rel="noopener"}

> You manage access in AWS by creating policies and attaching them to AWS Identity and Access Management (IAM) principals (roles, users, or groups of users) or AWS resources. AWS evaluates these policies when an IAM principal makes a request, such as uploading an object to an Amazon Simple Storage Service (Amazon S3) bucket. Permissions in [...]
