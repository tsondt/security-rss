Title: Google showers top cloud security researchers with kudos and cash
Date: 2022-06-07T15:48:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-07-google-showers-top-cloud-security-researchers-with-kudos-and-cash

[Source](https://portswigger.net/daily-swig/google-showers-top-cloud-security-researchers-with-kudos-and-cash){:target="_blank" rel="noopener"}

> More than $300,000 was handed out in GCP prize money during 2021 [...]
