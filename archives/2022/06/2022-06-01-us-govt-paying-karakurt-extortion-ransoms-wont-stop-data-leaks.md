Title: US govt: Paying Karakurt extortion ransoms won’t stop data leaks
Date: 2022-06-01T13:09:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-01-us-govt-paying-karakurt-extortion-ransoms-wont-stop-data-leaks

[Source](https://www.bleepingcomputer.com/news/security/us-govt-paying-karakurt-extortion-ransoms-won-t-stop-data-leaks/){:target="_blank" rel="noopener"}

> Several U.S. federal agencies warned organizations today against paying ransom demands made by the Karakurt gang since that will not prevent their stolen data from being sold to others. [...]
