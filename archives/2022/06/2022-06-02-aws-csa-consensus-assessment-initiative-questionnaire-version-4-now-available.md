Title: AWS CSA Consensus Assessment Initiative Questionnaire version 4 now available
Date: 2022-06-02T20:33:28+00:00
Author: Sonali Vaidya
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS CSA STAR;AWS CSA STAR Certificates;AWS CSA STAR Level 2 Certification;AWS SCA STAR Level 2;Security Blog
Slug: 2022-06-02-aws-csa-consensus-assessment-initiative-questionnaire-version-4-now-available

[Source](https://aws.amazon.com/blogs/security/aws-csa-consensus-assessment-initiative-questionnaire-version-4-now-available/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has published an updated version of the AWS Cloud Security Alliance (CSA) Consensus Assessment Initiative Questionnaire (CAIQ). The questionnaire has been completed using the current CSA CAIQ standard, v4.0.2 (06.07.2021 update), and is now available for download. The CSA is a not-for-profit organization dedicated to “defining and raising awareness of best [...]
