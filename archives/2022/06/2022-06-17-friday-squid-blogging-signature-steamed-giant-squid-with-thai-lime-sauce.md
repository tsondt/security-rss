Title: Friday Squid Blogging: Signature Steamed Giant Squid with Thai Lime Sauce
Date: 2022-06-17T21:05:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-06-17-friday-squid-blogging-signature-steamed-giant-squid-with-thai-lime-sauce

[Source](https://www.schneier.com/blog/archives/2022/06/friday-squid-blogging-signature-steamed-giant-squid-with-thai-lime-sauce.html){:target="_blank" rel="noopener"}

> From a restaurant in Singapore. It’s not actually giant squid. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
