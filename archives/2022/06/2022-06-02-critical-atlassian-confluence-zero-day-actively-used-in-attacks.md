Title: Critical Atlassian Confluence zero-day actively used in attacks
Date: 2022-06-02T21:41:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-02-critical-atlassian-confluence-zero-day-actively-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/critical-atlassian-confluence-zero-day-actively-used-in-attacks/){:target="_blank" rel="noopener"}

> Hackers are actively exploiting a new Atlassian Confluence zero-day vulnerability tracked as CVE-2022-26134 to install web shells, with no fix available at this time. [...]
