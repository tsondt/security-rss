Title: Feds Forced Travel Firms to Share Surveillance Data on Hacker
Date: 2022-06-09T17:44:03+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Privacy
Slug: 2022-06-09-feds-forced-travel-firms-to-share-surveillance-data-on-hacker

[Source](https://threatpost.com/feds-forced-travel-firms-to-share-surveillance-data-on-hacker/179929/){:target="_blank" rel="noopener"}

> Sabre and Travelport had to report the weekly activities of former “Cardplanet” cybercriminal Aleksei Burkov for two years, info that eventually led to his arrest and prosecution. [...]
