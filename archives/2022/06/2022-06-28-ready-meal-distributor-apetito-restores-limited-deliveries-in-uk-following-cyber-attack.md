Title: Ready meal distributor Apetito restores ‘limited’ deliveries in UK following cyber-attack
Date: 2022-06-28T14:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-28-ready-meal-distributor-apetito-restores-limited-deliveries-in-uk-following-cyber-attack

[Source](https://portswigger.net/daily-swig/ready-meal-distributor-apetito-restores-limited-deliveries-in-uk-following-cyber-attack){:target="_blank" rel="noopener"}

> ‘Manual workaround’ kickstarts phased recovery after cybercrooks disrupt meal provision to vulnerable people [...]
