Title: Wave of 'Matanbuchus' spam is infecting devices with Cobalt Strike
Date: 2022-06-18T10:06:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-18-wave-of-matanbuchus-spam-is-infecting-devices-with-cobalt-strike

[Source](https://www.bleepingcomputer.com/news/security/wave-of-matanbuchus-spam-is-infecting-devices-with-cobalt-strike/){:target="_blank" rel="noopener"}

> Security researchers have noticed a new malicious spam campaign that delivers the 'Matanbuchus' malware to drop Cobalt Strike beacons on compromised machines. [...]
