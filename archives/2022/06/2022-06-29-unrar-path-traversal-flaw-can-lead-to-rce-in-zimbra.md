Title: UnRAR path traversal flaw can lead to RCE in Zimbra
Date: 2022-06-29T15:14:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-29-unrar-path-traversal-flaw-can-lead-to-rce-in-zimbra

[Source](https://portswigger.net/daily-swig/unrar-path-traversal-flaw-can-lead-to-rce-in-zimbra){:target="_blank" rel="noopener"}

> Other applications using binary to extract untrusted archives are potentially vulnerable too [...]
