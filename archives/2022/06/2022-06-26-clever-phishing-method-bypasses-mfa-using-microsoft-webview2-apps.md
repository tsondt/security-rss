Title: Clever phishing method bypasses MFA using Microsoft WebView2 apps
Date: 2022-06-26T10:12:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-26-clever-phishing-method-bypasses-mfa-using-microsoft-webview2-apps

[Source](https://www.bleepingcomputer.com/news/security/clever-phishing-method-bypasses-mfa-using-microsoft-webview2-apps/){:target="_blank" rel="noopener"}

> A clever, new phishing technique uses Microsoft Edge WebView2 applications to steal victim's authentication cookies, allowing threat actors to bypass multi-factor authentication when logging into stolen accounts. [...]
