Title: Friday Squid Blogging: More on the “Mind Boggling” Squid Genome
Date: 2022-06-03T21:03:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2022-06-03-friday-squid-blogging-more-on-the-mind-boggling-squid-genome

[Source](https://www.schneier.com/blog/archives/2022/06/friday-squid-blogging-more-on-the-mind-boggling-squid-genome.html){:target="_blank" rel="noopener"}

> Octopus and squid genes are weird. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
