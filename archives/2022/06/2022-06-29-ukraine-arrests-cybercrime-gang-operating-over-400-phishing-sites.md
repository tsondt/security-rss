Title: Ukraine arrests cybercrime gang operating over 400 phishing sites
Date: 2022-06-29T11:27:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-29-ukraine-arrests-cybercrime-gang-operating-over-400-phishing-sites

[Source](https://www.bleepingcomputer.com/news/security/ukraine-arrests-cybercrime-gang-operating-over-400-phishing-sites/){:target="_blank" rel="noopener"}

> The Ukrainian cyberpolice force arrested nine members of a criminal group that operated over 400 phishing websites crafted to appear like legitimate EU portals offering financial assistance to Ukrainians. [...]
