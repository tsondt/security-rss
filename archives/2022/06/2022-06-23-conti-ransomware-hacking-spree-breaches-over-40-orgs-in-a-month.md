Title: Conti ransomware hacking spree breaches over 40 orgs in a month
Date: 2022-06-23T06:05:37-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-06-23-conti-ransomware-hacking-spree-breaches-over-40-orgs-in-a-month

[Source](https://www.bleepingcomputer.com/news/security/conti-ransomware-hacking-spree-breaches-over-40-orgs-in-a-month/){:target="_blank" rel="noopener"}

> The Conti cybercrime syndicate runs one of the most aggressive ransomware operations and has grown highly organized, to the point that affiliates were able to hack more than 40 companies in a little over a month. [...]
