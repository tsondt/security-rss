Title: AMD targeted by RansomHouse, cybercrims claim to have '450Gb' in stolen data
Date: 2022-06-28T14:01:44+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-28-amd-targeted-by-ransomhouse-cybercrims-claim-to-have-450gb-in-stolen-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/28/amd-ransomhouse-data-extortion/){:target="_blank" rel="noopener"}

> Relative cybercrime newbies not clear on whether they're alleging to have gigabits or gigabytes of chip biz's data If claims hold true, AMD has been targeted by the extortion group RansomHouse, which says it is sitting on a trove of data stolen from the processor designer following an alleged security breach earlier this year.... [...]
