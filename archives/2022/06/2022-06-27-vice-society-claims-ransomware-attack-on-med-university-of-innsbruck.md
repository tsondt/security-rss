Title: Vice Society claims ransomware attack on Med. University of Innsbruck
Date: 2022-06-27T12:31:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-27-vice-society-claims-ransomware-attack-on-med-university-of-innsbruck

[Source](https://www.bleepingcomputer.com/news/security/vice-society-claims-ransomware-attack-on-med-university-of-innsbruck/){:target="_blank" rel="noopener"}

> The Vice Society ransomware gang has claimed responsibility for last week's cyberattack against the Medical University of Innsbruck, which caused severe IT service disruption and the alleged theft of data. [...]
