Title: California state's gun control websites expose 10 years of personal data
Date: 2022-06-30T19:08:23+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-30-california-states-gun-control-websites-expose-10-years-of-personal-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/30/california_websites_expose_personal_data/){:target="_blank" rel="noopener"}

> And some of it may have been leaked on social media A California state website exposed the personal details of anyone who applied for a concealed-and-carry weapons (CCW) permit between 2011 and 2021.... [...]
