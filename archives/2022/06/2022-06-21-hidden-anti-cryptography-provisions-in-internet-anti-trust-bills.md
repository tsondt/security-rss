Title: Hidden Anti-Cryptography Provisions in Internet Anti-Trust Bills
Date: 2022-06-21T11:34:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;laws
Slug: 2022-06-21-hidden-anti-cryptography-provisions-in-internet-anti-trust-bills

[Source](https://www.schneier.com/blog/archives/2022/06/hidden-anti-cryptography-provisions-in-internet-anti-trust-bills.html){:target="_blank" rel="noopener"}

> Two bills attempting to reduce the power of Internet monopolies are currently being debated in Congress: S. 2992, the American Innovation and Choice Online Act ; and S. 2710, the Open App Markets Act. Reducing the power to tech monopolies would do more to “fix” the Internet than any other single action, and I am generally in favor of them both. (The Center for American Progress wrote a good summary and evaluation of them. I have written in support of the bill that would force Google and Apple to give up their monopolies on their phone app stores.) There is [...]
