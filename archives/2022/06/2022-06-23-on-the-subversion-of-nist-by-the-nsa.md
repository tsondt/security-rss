Title: On the Subversion of NIST by the NSA
Date: 2022-06-23T11:05:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;algorithms;cryptography;NIST;NSA
Slug: 2022-06-23-on-the-subversion-of-nist-by-the-nsa

[Source](https://www.schneier.com/blog/archives/2022/06/on-the-subversion-of-nist-by-the-nsa.html){:target="_blank" rel="noopener"}

> Nadiya Kostyuk and Susan Landau wrote an interesting paper: “ Dueling Over DUAL_EC_DRBG: The Consequences of Corrupting a Cryptographic Standardization Process “: Abstract: In recent decades, the U.S. National Institute of Standards and Technology (NIST), which develops cryptographic standards for non-national security agencies of the U.S. government, has emerged as the de facto international source for cryptographic standards. But in 2013, Edward Snowden disclosed that the National Security Agency had subverted the integrity of a NIST cryptographic standard­the Dual_EC_DRBG­enabling easy decryption of supposedly secured communications. This discovery reinforced the desire of some public and private entities to develop their own [...]
