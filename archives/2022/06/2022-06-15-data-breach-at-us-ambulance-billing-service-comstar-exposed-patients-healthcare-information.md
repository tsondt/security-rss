Title: Data breach at US ambulance billing service Comstar exposed patients’ healthcare information
Date: 2022-06-15T10:45:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-15-data-breach-at-us-ambulance-billing-service-comstar-exposed-patients-healthcare-information

[Source](https://portswigger.net/daily-swig/data-breach-at-us-ambulance-billing-service-comstar-exposed-patients-healthcare-information){:target="_blank" rel="noopener"}

> Medical payments company admits network intrusion [...]
