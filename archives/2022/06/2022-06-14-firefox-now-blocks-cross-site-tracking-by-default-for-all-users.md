Title: Firefox now blocks cross-site tracking by default for all users
Date: 2022-06-14T11:04:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-14-firefox-now-blocks-cross-site-tracking-by-default-for-all-users

[Source](https://www.bleepingcomputer.com/news/security/firefox-now-blocks-cross-site-tracking-by-default-for-all-users/){:target="_blank" rel="noopener"}

> Mozilla says that starting today, all Firefox users will now be protected by default against cross-site tracking while browsing the Internet. [...]
