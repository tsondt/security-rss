Title: International operation takes down Russian RSOCKS botnet
Date: 2022-06-17T19:38:06+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-06-17-international-operation-takes-down-russian-rsocks-botnet

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/17/rsocks_russia_botnet/){:target="_blank" rel="noopener"}

> $200 a day buys you 90,000 victims A Russian operated botnet known as RSOCKS has been shut down by the US Department of Justice acting with law enforcement partners in Germany, the Netherlands and the UK. It is believed to have compromised millions of computers and other devices around the globe.... [...]
