Title: Hello XD ransomware now drops a backdoor while encrypting
Date: 2022-06-12T10:11:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-12-hello-xd-ransomware-now-drops-a-backdoor-while-encrypting

[Source](https://www.bleepingcomputer.com/news/security/hello-xd-ransomware-now-drops-a-backdoor-while-encrypting/){:target="_blank" rel="noopener"}

> Cybersecurity researchers report increased activity of the Hello XD ransomware, whose operators are now deploying an upgraded sample featuring stronger encryption. [...]
