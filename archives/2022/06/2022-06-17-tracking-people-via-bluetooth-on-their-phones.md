Title: Tracking People via Bluetooth on Their Phones
Date: 2022-06-17T11:06:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;Bluetooth;identification;privacy;stalking;tracking
Slug: 2022-06-17-tracking-people-via-bluetooth-on-their-phones

[Source](https://www.schneier.com/blog/archives/2022/06/tracking-people-via-bluetooth-on-their-phones.html){:target="_blank" rel="noopener"}

> We’ve always known that phones—and the people carrying them—can be uniquely identified from their Bluetooth signatures, and that we need security techniques to prevent that. This new research shows that that’s not enough. Computer scientists at the University of California San Diego proved in a study published May 24 that minute imperfections in phones caused during manufacturing create a unique Bluetooth beacon, one that establishes a digital signature or fingerprint distinct from any other device. Though phones’ Bluetooth uses cryptographic technology that limits trackability, using a radio receiver, these distortions in the Bluetooth signal can be discerned to track individual [...]
