Title: New peer-to-peer botnet infects Linux servers with cryptominers
Date: 2022-06-15T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-15-new-peer-to-peer-botnet-infects-linux-servers-with-cryptominers

[Source](https://www.bleepingcomputer.com/news/security/new-peer-to-peer-botnet-infects-linux-servers-with-cryptominers/){:target="_blank" rel="noopener"}

> A new peer-to-peer botnet named Panchan appeared in the wild around March 2022, targeting Linux servers in the education sector to mine cryptocurrency. [...]
