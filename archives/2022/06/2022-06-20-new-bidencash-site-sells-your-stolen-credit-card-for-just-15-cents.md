Title: New 'BidenCash' site sells your stolen credit card for just 15 cents
Date: 2022-06-20T11:02:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-20-new-bidencash-site-sells-your-stolen-credit-card-for-just-15-cents

[Source](https://www.bleepingcomputer.com/news/security/new-bidencash-site-sells-your-stolen-credit-card-for-just-15-cents/){:target="_blank" rel="noopener"}

> A recently launched carding site called 'BidenCash' is trying to get notoriety by leaking credit card details along with information about their owners. [...]
