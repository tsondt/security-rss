Title: New MetaMask phishing campaign uses KYC lures to steal passphrases
Date: 2022-06-23T08:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-23-new-metamask-phishing-campaign-uses-kyc-lures-to-steal-passphrases

[Source](https://www.bleepingcomputer.com/news/security/new-metamask-phishing-campaign-uses-kyc-lures-to-steal-passphrases/){:target="_blank" rel="noopener"}

> A new phishing campaign is targeting users on Microsoft 365 while spoofing the popular MetaMask cryptocurrency wallet provider and attempting to steal recovery phrases. [...]
