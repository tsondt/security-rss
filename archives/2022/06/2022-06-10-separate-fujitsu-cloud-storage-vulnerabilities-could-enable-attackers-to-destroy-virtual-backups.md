Title: Separate Fujitsu cloud storage vulnerabilities could enable attackers to destroy virtual backups
Date: 2022-06-10T12:34:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-10-separate-fujitsu-cloud-storage-vulnerabilities-could-enable-attackers-to-destroy-virtual-backups

[Source](https://portswigger.net/daily-swig/separate-fujitsu-cloud-storage-vulnerabilities-could-enable-attackers-to-destroy-virtual-backups){:target="_blank" rel="noopener"}

> Sysadmins should update their installations immediately [...]
