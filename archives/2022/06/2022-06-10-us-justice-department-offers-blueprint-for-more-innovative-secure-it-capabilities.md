Title: US Justice Department offers blueprint for more ‘innovative, secure IT capabilities’
Date: 2022-06-10T15:05:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-10-us-justice-department-offers-blueprint-for-more-innovative-secure-it-capabilities

[Source](https://portswigger.net/daily-swig/us-justice-department-offers-blueprint-for-more-innovative-secure-it-capabilities){:target="_blank" rel="noopener"}

> ‘Zero trust’ architecture and secure supply chains to the fore in new strategy [...]
