Title: There are 24.6 billion sets of credentials up for sale on the dark web
Date: 2022-06-20T12:15:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-06-20-there-are-246-billion-sets-of-credentials-up-for-sale-on-the-dark-web

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/20/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Citrix ASM has some really bad bugs, and more In brief More than half of the 24.6 billion stolen credential pairs available for sale on the dark web were exposed in the past year, the Digital Shadows Research Team has found.... [...]
