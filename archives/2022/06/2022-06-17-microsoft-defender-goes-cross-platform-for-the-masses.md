Title: Microsoft Defender goes cross-platform for the masses
Date: 2022-06-17T15:30:05+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-06-17-microsoft-defender-goes-cross-platform-for-the-masses

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/17/microsoft_defender/){:target="_blank" rel="noopener"}

> Redmond's security brand extended to multiple devices without stomping on other solutions Microsoft is extending the Defender brand with a version aimed at families and individuals.... [...]
