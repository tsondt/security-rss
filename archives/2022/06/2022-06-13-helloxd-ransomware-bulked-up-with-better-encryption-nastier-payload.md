Title: HelloXD ransomware bulked up with better encryption, nastier payload
Date: 2022-06-13T17:30:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-13-helloxd-ransomware-bulked-up-with-better-encryption-nastier-payload

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/13/helloxd-ransomware-evolving/){:target="_blank" rel="noopener"}

> Russian-based group doubles the extortion by exfiltrating the corporate data before encrypting it. Windows and Linux systems are coming under attack by new variants of the HelloXD ransomware that includes stronger encryption, improved obfuscation and an additional payload that enables threat groups to modify compromised systems, exfiltrate files and execute commands.... [...]
