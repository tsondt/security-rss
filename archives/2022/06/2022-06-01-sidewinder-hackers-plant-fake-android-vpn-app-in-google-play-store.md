Title: SideWinder hackers plant fake Android VPN app in Google Play Store
Date: 2022-06-01T09:10:12-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-06-01-sidewinder-hackers-plant-fake-android-vpn-app-in-google-play-store

[Source](https://www.bleepingcomputer.com/news/security/sidewinder-hackers-plant-fake-android-vpn-app-in-google-play-store/){:target="_blank" rel="noopener"}

> Phishing campaigns attributed to an advanced threat actor called SideWinder involved a fake VPN app for Android devices published on Google Play Store along with a custom tool that filters victims for better targeting. [...]
