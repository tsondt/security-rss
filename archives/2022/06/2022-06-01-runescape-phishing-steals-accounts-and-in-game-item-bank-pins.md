Title: RuneScape phishing steals accounts and in-game item bank PINs
Date: 2022-06-01T12:39:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-01-runescape-phishing-steals-accounts-and-in-game-item-bank-pins

[Source](https://www.bleepingcomputer.com/news/security/runescape-phishing-steals-accounts-and-in-game-item-bank-pins/){:target="_blank" rel="noopener"}

> Cybersecurity researchers have discovered a new RuneScape-themed phishing campaign, and it stands out among the various operations for being exceptionally well-crafted. [...]
