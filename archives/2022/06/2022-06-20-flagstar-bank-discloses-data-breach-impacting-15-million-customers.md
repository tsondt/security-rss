Title: Flagstar Bank discloses data breach impacting 1.5 million customers
Date: 2022-06-20T12:57:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-20-flagstar-bank-discloses-data-breach-impacting-15-million-customers

[Source](https://www.bleepingcomputer.com/news/security/flagstar-bank-discloses-data-breach-impacting-15-million-customers/){:target="_blank" rel="noopener"}

> Flagstar Bank is notifying 1.5 million customers of a data breach where hackers accessed personal data during a December cyberattack. [...]
