Title: Voicemail Scam Steals Microsoft Credentials
Date: 2022-06-21T11:20:48+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks
Slug: 2022-06-21-voicemail-scam-steals-microsoft-credentials

[Source](https://threatpost.com/voicemail-phishing-scam-steals-microsoft-credentials/180005/){:target="_blank" rel="noopener"}

> Attackers are targeting a number of key vertical markets in the U.S. with the active campaign, which impersonates the organization and Microsoft to lift Office365 and Outlook log-in details. [...]
