Title: GhostTouch: Hackers can reach your phone’s touchscreen without even touching it
Date: 2022-06-14T12:51:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-14-ghosttouch-hackers-can-reach-your-phones-touchscreen-without-even-touching-it

[Source](https://portswigger.net/daily-swig/ghosttouch-hackers-can-reach-your-phones-touchscreen-without-even-touching-it){:target="_blank" rel="noopener"}

> New research shows how electromagnetic interference can be used to trigger arbitrary behavior on mobile touchscreens, although caveats apply [...]
