Title: Ukraine targeted by almost 800 cyberattacks since the war started
Date: 2022-06-30T10:57:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-30-ukraine-targeted-by-almost-800-cyberattacks-since-the-war-started

[Source](https://www.bleepingcomputer.com/news/security/ukraine-targeted-by-almost-800-cyberattacks-since-the-war-started/){:target="_blank" rel="noopener"}

> Ukrainian government and private sector organizations have been the target of 796 cyberattacks since the start of the war on February 24, 2022, when Russia invaded Ukraine. [...]
