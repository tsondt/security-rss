Title: Google Chrome extensions can be fingerprinted to track you online
Date: 2022-06-19T13:59:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-19-google-chrome-extensions-can-be-fingerprinted-to-track-you-online

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-extensions-can-be-fingerprinted-to-track-you-online/){:target="_blank" rel="noopener"}

> A researcher has discovered how to use your installed Google Chrome extensions to generate a fingerprint of your device that can be used to track you online. [...]
