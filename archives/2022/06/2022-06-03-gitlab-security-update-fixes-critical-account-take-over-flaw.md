Title: GitLab security update fixes critical account take over flaw
Date: 2022-06-03T09:55:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-03-gitlab-security-update-fixes-critical-account-take-over-flaw

[Source](https://www.bleepingcomputer.com/news/security/gitlab-security-update-fixes-critical-account-take-over-flaw/){:target="_blank" rel="noopener"}

> GitLab has released a critical security update for multiple versions of its Community and Enterprise Edition products to address eight vulnerabilities, one of which allows account takeover. [...]
