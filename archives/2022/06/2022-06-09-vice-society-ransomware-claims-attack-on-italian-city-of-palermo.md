Title: Vice Society ransomware claims attack on Italian city of Palermo
Date: 2022-06-09T11:43:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-09-vice-society-ransomware-claims-attack-on-italian-city-of-palermo

[Source](https://www.bleepingcomputer.com/news/security/vice-society-ransomware-claims-attack-on-italian-city-of-palermo/){:target="_blank" rel="noopener"}

> The Vice Society ransomware group has claimed responsibility for the recent cyber attack on the city of Palermo in Italy, which has caused a large-scale service outage. [...]
