Title: iCloud hacker gets 9 years in prison for stealing nude photos
Date: 2022-06-16T17:51:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-16-icloud-hacker-gets-9-years-in-prison-for-stealing-nude-photos

[Source](https://www.bleepingcomputer.com/news/security/icloud-hacker-gets-9-years-in-prison-for-stealing-nude-photos/){:target="_blank" rel="noopener"}

> A California man who hacked thousands of Apple iCloud accounts was sentenced to 8 years in prison after pleading guilty to conspiracy and computer fraud in October 2021. [...]
