Title: RubyGems trials 2FA-by-default in code repo’s latest security effort
Date: 2022-06-17T12:17:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-17-rubygems-trials-2fa-by-default-in-code-repos-latest-security-effort

[Source](https://portswigger.net/daily-swig/rubygems-trials-2fa-by-default-in-code-repos-latest-security-effort){:target="_blank" rel="noopener"}

> Move intended to help prevent Ruby packages from being used in supply chain attacks [...]
