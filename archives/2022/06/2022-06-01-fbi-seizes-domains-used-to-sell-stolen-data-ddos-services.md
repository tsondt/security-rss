Title: FBI seizes domains used to sell stolen data, DDoS services
Date: 2022-06-01T14:46:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-01-fbi-seizes-domains-used-to-sell-stolen-data-ddos-services

[Source](https://www.bleepingcomputer.com/news/security/fbi-seizes-domains-used-to-sell-stolen-data-ddos-services/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) and the U.S. Department of Justice announced today the seizure of three domains used by cybercriminals to sell personal info stolen in data breaches and to provide DDoS attack services. [...]
