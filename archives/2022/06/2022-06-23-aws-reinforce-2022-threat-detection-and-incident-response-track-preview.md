Title: AWS re:Inforce 2022: Threat detection and incident response track preview
Date: 2022-06-23T15:49:55+00:00
Author: Celeste Bishop
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Events;Security, Identity, & Compliance;AWS security;Cloud security;cloud security conference;Incident response;Live Events;Security Blog;threat detection
Slug: 2022-06-23-aws-reinforce-2022-threat-detection-and-incident-response-track-preview

[Source](https://aws.amazon.com/blogs/security/aws-reinforce-2022-threat-detection-and-incident-response-track-preview/){:target="_blank" rel="noopener"}

> Register now with discount code SALXTDVaB7y to get $150 off your full conference pass to AWS re:Inforce. For a limited time only and while supplies last. Today we’re going to highlight just some of the sessions focused on threat detection and incident response that are planned for AWS re:Inforce 2022. AWS re:Inforce is a learning [...]
