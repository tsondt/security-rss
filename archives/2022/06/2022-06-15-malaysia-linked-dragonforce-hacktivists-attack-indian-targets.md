Title: Malaysia-linked DragonForce hacktivists attack Indian targets
Date: 2022-06-15T04:44:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-15-malaysia-linked-dragonforce-hacktivists-attack-indian-targets

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/15/dragonforce_malaysia_india_attacks/){:target="_blank" rel="noopener"}

> Just what we needed: a threat to rival Anonymous A Malaysia-linked hacktivist group has attacked targets in India, seemingly in reprisal for a representative of the ruling Bharatiya Janata Party (BJP) making remarks felt to be insulting to the prophet Muhammad.... [...]
