Title: Password recovery from beyond the grave
Date: 2022-06-17T08:03:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-06-17-password-recovery-from-beyond-the-grave

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/17/on_call/){:target="_blank" rel="noopener"}

> Does your disaster recovery plan include a mysterious missive at a funeral? On Call Every disaster recovery plan needs to contain the "hit by a bus" scenario. But have you ever retrieved a password from beyond the grave? One Register reader has. Welcome to On Call.... [...]
