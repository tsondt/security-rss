Title: Google has more reasons why it doesn't like antitrust law that affects Google
Date: 2022-06-08T17:45:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-06-08-google-has-more-reasons-why-it-doesnt-like-antitrust-law-that-affects-google

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/08/google_antitrust_legislation/){:target="_blank" rel="noopener"}

> It'll ruin Gmail, claims web ads giant Google has a fresh list of reasons why it opposes tech antitrust legislation making its way through Congress but, like others who've expressed discontent, the ad giant's complaints leave out mention of portions of the proposed law that address said gripes.... [...]
