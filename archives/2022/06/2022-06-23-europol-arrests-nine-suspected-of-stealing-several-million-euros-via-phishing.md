Title: Europol arrests nine suspected of stealing 'several million' euros via phishing
Date: 2022-06-23T06:29:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-23-europol-arrests-nine-suspected-of-stealing-several-million-euros-via-phishing

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/23/dutch_europol_arrest_phishing/){:target="_blank" rel="noopener"}

> Victims lured into handing over online banking logins, police say Europol cops have arrested nine suspected members of a cybercrime ring involved in phishing, internet scams, and money laundering.... [...]
