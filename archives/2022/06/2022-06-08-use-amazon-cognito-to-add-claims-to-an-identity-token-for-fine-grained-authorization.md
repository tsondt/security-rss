Title: Use Amazon Cognito to add claims to an identity token for fine-grained authorization
Date: 2022-06-08T17:55:48+00:00
Author: Ajit Ambike
Category: AWS Security
Tags: Expert (400);Security, Identity, & Compliance;Amazon Cognito;authorization;Claims;Identity Token;Security Blog
Slug: 2022-06-08-use-amazon-cognito-to-add-claims-to-an-identity-token-for-fine-grained-authorization

[Source](https://aws.amazon.com/blogs/security/use-amazon-cognito-to-add-claims-to-an-identity-token-for-fine-grained-authorization/){:target="_blank" rel="noopener"}

> With Amazon Cognito, you can quickly add user sign-up, sign-in, and access control to your web and mobile applications. After a user signs in successfully, Cognito generates an identity token for user authorization. The service provides a pre token generation trigger, which you can use to customize identity token claims before token generation. In this [...]
