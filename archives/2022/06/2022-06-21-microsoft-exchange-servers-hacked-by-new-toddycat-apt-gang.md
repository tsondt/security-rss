Title: Microsoft Exchange servers hacked by new ToddyCat APT gang
Date: 2022-06-21T07:46:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-21-microsoft-exchange-servers-hacked-by-new-toddycat-apt-gang

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-servers-hacked-by-new-toddycat-apt-gang/){:target="_blank" rel="noopener"}

> An advanced persistent threat (APT) group dubbed ToddyCat has been targeting Microsoft Exchange servers throughout Asia and Europe for more than a year, since at least December 2020. [...]
