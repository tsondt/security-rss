Title: Feeling highly stressed about your job? You must be a CISO
Date: 2022-06-04T07:49:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-04-feeling-highly-stressed-about-your-job-you-must-be-a-ciso

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/04/cybersecurity_csuite_stress/){:target="_blank" rel="noopener"}

> 'The attack surface has expanded exponentially' during the work-from-home pandemic, says one Almost all cybersecurity professionals are stressed, and nearly half (46 percent) have considered leaving the industry altogether, according to a DeepInstinct survey.... [...]
