Title: BSides Cleveland organizer steps down after controversial guest added as ‘surprise’ speaker
Date: 2022-06-24T11:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-24-bsides-cleveland-organizer-steps-down-after-controversial-guest-added-as-surprise-speaker

[Source](https://portswigger.net/daily-swig/bsides-cleveland-organizer-steps-down-after-controversial-guest-added-as-surprise-speaker){:target="_blank" rel="noopener"}

> Fury among online community over decision to include presenter [...]
