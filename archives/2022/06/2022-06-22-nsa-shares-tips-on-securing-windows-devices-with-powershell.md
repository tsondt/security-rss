Title: NSA shares tips on securing Windows devices with PowerShell
Date: 2022-06-22T18:10:37-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-06-22-nsa-shares-tips-on-securing-windows-devices-with-powershell

[Source](https://www.bleepingcomputer.com/news/security/nsa-shares-tips-on-securing-windows-devices-with-powershell/){:target="_blank" rel="noopener"}

> The National Security Agency (NSA) and cybersecurity partner agencies issued an advisory today recommending system administrators to use PowerShell to prevent and detect malicious activity on Windows machines. [...]
