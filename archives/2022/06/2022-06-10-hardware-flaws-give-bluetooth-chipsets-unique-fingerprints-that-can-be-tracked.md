Title: Hardware flaws give Bluetooth chipsets unique fingerprints that can be tracked
Date: 2022-06-10T04:17:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-10-hardware-flaws-give-bluetooth-chipsets-unique-fingerprints-that-can-be-tracked

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/10/smartphone-bluetooth-tracking/){:target="_blank" rel="noopener"}

> While this poses a privacy and security threat, an attacker's ability to exploit it may come down to luck Researchers at the University of California San Diego have shown for the first time that Bluetooth signals each have an individual, trackable, fingerprint.... [...]
