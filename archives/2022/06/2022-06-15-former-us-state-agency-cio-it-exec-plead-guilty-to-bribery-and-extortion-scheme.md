Title: Former US state agency CIO, IT exec plead guilty to bribery and extortion scheme
Date: 2022-06-15T00:37:27+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-06-15-former-us-state-agency-cio-it-exec-plead-guilty-to-bribery-and-extortion-scheme

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/15/maryland_bribery_extortion_charges/){:target="_blank" rel="noopener"}

> Pair's multimillion-dollar contract caper unraveled A former Maryland Cabinet-level official and a former IT executive have pleaded guilty to involvement in a bribery and extortion scheme related to technology contracts about a decade ago.... [...]
