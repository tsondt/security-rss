Title: Meet the Administrators of the RSOCKS Proxy Botnet
Date: 2022-06-22T13:06:34+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;79136334444;Constella Intelligence;Denis "Neo" Kloster;exploit;Internet Advertising Omsk;istanx@gmail.com;RSOCKS botnet;RUSdot;SL MobPartners;Spamdot;Stanx;stanx@rusdot.com;U.S. Department of Justice;Verified
Slug: 2022-06-22-meet-the-administrators-of-the-rsocks-proxy-botnet

[Source](https://krebsonsecurity.com/2022/06/meet-the-administrators-of-the-rsocks-proxy-botnet/){:target="_blank" rel="noopener"}

> Authorities in the United States, Germany, the Netherlands and the U.K. last week said they dismantled the “ RSOCKS ” botnet, a collection of millions of hacked devices that were sold as “proxies” to cybercriminals looking for ways to route their malicious traffic through someone else’s computer. While the coordinated action did not name the Russian hackers allegedly behind RSOCKS, KrebsOnSecurity has identified its owner as a 35-year-old Russian man living abroad who also runs the world’s top spam forum. The RUSdot mailer, the email spamming tool made and sold by the administrator of RSOCKS. According to a statement by [...]
