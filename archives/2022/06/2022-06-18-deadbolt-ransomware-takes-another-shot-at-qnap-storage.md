Title: DeadBolt ransomware takes another shot at QNAP storage
Date: 2022-06-18T00:48:00+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-18-deadbolt-ransomware-takes-another-shot-at-qnap-storage

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/18/deadlbolt-ransomware-qnap-nas/){:target="_blank" rel="noopener"}

> Keep boxes updated and protected to avoid a NAS-ty shock QNAP is warning users about another wave of DeadBolt ransomware attacks against its network-attached storage (NAS) devices – and urged customers to update their devices' QTS or QuTS hero operating systems to the latest versions.... [...]
