Title: Phishing gang behind millions in losses dismantled by police
Date: 2022-06-21T12:50:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-21-phishing-gang-behind-millions-in-losses-dismantled-by-police

[Source](https://www.bleepingcomputer.com/news/security/phishing-gang-behind-millions-in-losses-dismantled-by-police/){:target="_blank" rel="noopener"}

> Members of a phishing gang behind millions of euros in losses were arrested today following a law enforcement operation coordinated by the Europol. [...]
