Title: Infrastructure Security in Google Cloud
Date: 2022-06-08T20:48:00+00:00
Author: Priyanka Vergadia
Category: GCP Security
Tags: Google Cloud;Identity & Security;Developers & Practitioners
Slug: 2022-06-08-infrastructure-security-in-google-cloud

[Source](https://cloud.google.com/blog/topics/developers-practitioners/infrastructure-security-in-google-cloud/){:target="_blank" rel="noopener"}

> The security of the infrastructure that runs your applications is one of the most important considerations in choosing a cloud vendor. Google Cloud’s approach to infrastructure security is unique. Google doesn’t rely on any single technology to secure its infrastructure. Rather, it has built security through progressive layers that deliver defense in depth. Defense in depth at scale Data center physical security - Google data centers feature layered security with custom-designed electronic access cards, alarms, vehicle access barriers, perimeter fencing, metal detectors, biometrics, and laser beam intrusion detection. They are monitored 24/7 by high-resolution cameras that can detect and track [...]
