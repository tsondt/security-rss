Title: New PACMAN hardware attack targets Macs with Apple M1 CPUs
Date: 2022-06-10T15:15:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple;Hardware
Slug: 2022-06-10-new-pacman-hardware-attack-targets-macs-with-apple-m1-cpus

[Source](https://www.bleepingcomputer.com/news/security/new-pacman-hardware-attack-targets-macs-with-apple-m1-cpus/){:target="_blank" rel="noopener"}

> A new hardware attack targeting Pointer Authentication in Apple M1 CPUs with speculative execution enables attackers to gain arbitrary code execution on Mac systems. [...]
