Title: Don't ditch PowerShell to improve security, say infosec agencies from UK, US, and NZ
Date: 2022-06-23T07:58:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-23-dont-ditch-powershell-to-improve-security-say-infosec-agencies-from-uk-us-and-nz

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/23/keep_poewrshell_security_advice/){:target="_blank" rel="noopener"}

> Use it sensibly instead – which means turning on the useful bits Microsoft doesn't enable by default Windows PowerShell is enormously useful, extremely prevalent, and often targeted by crooks because it offers an express route into the heart of Windows servers and networks.... [...]
