Title: Cloud CISO Perspectives: May 2022
Date: 2022-06-02T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-06-02-cloud-ciso-perspectives-may-2022

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-may-2022/){:target="_blank" rel="noopener"}

> May was another big month for us, even as we get ready for more industry work and engagement at the RSA Security Conference in San Francisco. At our Security Summit and throughout the past month, we continued to launch new security products and features, and increased service and support for all our Google Cloud and Google Workspace customers. Google Cloud’s Security Summit 2022 Our second annual Security Summit held on May 17 was a great success. In the days leading up to the Summit, we discussed how we are working to bring Zero Trust policies to government agencies, and we [...]
