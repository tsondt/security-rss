Title: $6b mega contract electronics vendor Sanmina jumps into zero trust
Date: 2022-06-23T15:30:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-23-6b-mega-contract-electronics-vendor-sanmina-jumps-into-zero-trust

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/23/sanmina-zero-trust-zscaler/){:target="_blank" rel="noopener"}

> Company was an early adopter of Google Cloud, which led to a search for a new security architecture Matt Ramberg is the vice president of information security at Sanmina, a sprawling electronics manufacturer with close to 60 facilities in 20 countries on six continents and some 35,000 employees spread across the world.... [...]
