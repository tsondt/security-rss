Title: Microsoft: June Windows Server updates may cause backup issues
Date: 2022-06-15T08:34:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-06-15-microsoft-june-windows-server-updates-may-cause-backup-issues

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-june-windows-server-updates-may-cause-backup-issues/){:target="_blank" rel="noopener"}

> Microsoft says that some applications might fail to backup data using Volume Shadow Copy Service (VSS) after applying the June 2022 Patch Tuesday Windows updates. [...]
