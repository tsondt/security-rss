Title: Introducing a new AWS whitepaper: Does data localization cause more problems than it solves?
Date: 2022-06-10T17:38:44+00:00
Author: Jana Kay
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Technical How-to;data localization;data residency;data sovereignty;Security Blog;Whitepaper
Slug: 2022-06-10-introducing-a-new-aws-whitepaper-does-data-localization-cause-more-problems-than-it-solves

[Source](https://aws.amazon.com/blogs/security/introducing-a-new-aws-whitepaper-does-data-localization-cause-more-problems-than-it-solves/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) recently released a new whitepaper, Does data localization cause more problems than it solves?, as part of the AWS Innovating Securely briefing series. The whitepaper draws on research from Emily Wu’s paper Sovereignty and Data Localization, published by Harvard University’s Belfer Center, and describes how countries can realize similar data localization [...]
