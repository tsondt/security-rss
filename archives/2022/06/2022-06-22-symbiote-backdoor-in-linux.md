Title: Symbiote Backdoor in Linux
Date: 2022-06-22T11:07:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;Linux;malware
Slug: 2022-06-22-symbiote-backdoor-in-linux

[Source](https://www.schneier.com/blog/archives/2022/06/symbiote-backdoor-in-linux.html){:target="_blank" rel="noopener"}

> Interesting : What makes Symbiote different from other Linux malware that we usually come across, is that it needs to infect other running processes to inflict damage on infected machines. Instead of being a standalone executable file that is run to infect a machine, it is a shared object (SO) library that is loaded into all running processes using LD_PRELOAD (T1574.006), and parasitically infects the machine. Once it has infected all the running processes, it provides the threat actor with rootkit functionality, the ability to harvest credentials, and remote access capability. News article : Researchers have unearthed a discovery that [...]
