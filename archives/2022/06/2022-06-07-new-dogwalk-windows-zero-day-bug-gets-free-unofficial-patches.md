Title: New ‘DogWalk’ Windows zero-day bug gets free unofficial patches
Date: 2022-06-07T12:59:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-06-07-new-dogwalk-windows-zero-day-bug-gets-free-unofficial-patches

[Source](https://www.bleepingcomputer.com/news/security/new-dogwalk-windows-zero-day-bug-gets-free-unofficial-patches/){:target="_blank" rel="noopener"}

> Free unofficial patches for a new Windows zero-day vulnerability in the Microsoft Support Diagnostic Tool (MSDT) have been released today through the 0patch platform. [...]
