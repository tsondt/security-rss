Title: Taming the Digital Asset Tsunami
Date: 2022-06-08T13:36:22+00:00
Author: Rob N. Gurzeev
Category: Threatpost
Tags: InfoSec Insider;IoT
Slug: 2022-06-08-taming-the-digital-asset-tsunami

[Source](https://threatpost.com/digital-asset-tsunami/179917/){:target="_blank" rel="noopener"}

> Rob Gurzeev, CEO and Co-Founder of CyCognito, explores external attack surface soft spots tied to an ever-expanding number of digital assets companies too often struggle to keep track of and manage effectively. [...]
