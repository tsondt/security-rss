Title: ‘Killnet’ Adversary Pummels Lithuania with DDoS Attacks Over Blockade
Date: 2022-06-28T12:17:05+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Critical Infrastructure;Government;Hacks
Slug: 2022-06-28-killnet-adversary-pummels-lithuania-with-ddos-attacks-over-blockade

[Source](https://threatpost.com/killnet-pummels-lithuania/180075/){:target="_blank" rel="noopener"}

> Cyber collective Killnet claims it won’t let up until the Baltic country opens trade routes to and from the Russian exclave of Kaliningrad. [...]
