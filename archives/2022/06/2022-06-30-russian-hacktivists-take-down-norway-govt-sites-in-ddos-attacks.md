Title: Russian hacktivists take down Norway govt sites in DDoS attacks
Date: 2022-06-30T10:31:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-30-russian-hacktivists-take-down-norway-govt-sites-in-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/russian-hacktivists-take-down-norway-govt-sites-in-ddos-attacks/){:target="_blank" rel="noopener"}

> Norway's National Security Authority (NSM) published a statement yesterday warning that some of the country's most important websites and online services are being rendered inaccessible due to distributed denial of service (DDoS) attacks. [...]
