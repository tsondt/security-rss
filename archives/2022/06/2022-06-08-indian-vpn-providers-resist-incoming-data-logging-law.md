Title: Indian VPN providers resist incoming data-logging law
Date: 2022-06-08T10:28:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-08-indian-vpn-providers-resist-incoming-data-logging-law

[Source](https://portswigger.net/daily-swig/indian-vpn-providers-resist-incoming-data-logging-law){:target="_blank" rel="noopener"}

> Privacy concerns raised over mandate to retain customer records [...]
