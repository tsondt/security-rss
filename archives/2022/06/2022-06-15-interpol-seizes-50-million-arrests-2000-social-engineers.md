Title: Interpol seizes $50 million, arrests 2000 social engineers
Date: 2022-06-15T10:26:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-06-15-interpol-seizes-50-million-arrests-2000-social-engineers

[Source](https://www.bleepingcomputer.com/news/security/interpol-seizes-50-million-arrests-2000-social-engineers/){:target="_blank" rel="noopener"}

> An international law enforcement operation, codenamed 'First Light 2022,' has seized 50 million dollars and arrested thousands of people involved in social engineering scams worldwide. [...]
