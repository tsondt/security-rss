Title: The Week in Ransomware - June 17th 2022 - Have I Been Ransomed?
Date: 2022-06-17T17:11:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-17-the-week-in-ransomware-june-17th-2022-have-i-been-ransomed

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-17th-2022-have-i-been-ransomed/){:target="_blank" rel="noopener"}

> Ransomware operations are constantly evolving their tactics to pressure victims to pay. For example, this week, we saw a new extortion tactic come into play with the creation of dedicated websites to extort victims with searchable data. [...]
