Title: Built with BigQuery: How Exabeam delivers a petabyte-scale cybersecurity solution
Date: 2022-06-30T16:00:00+00:00
Author: Sanjay Chaudhary
Category: GCP Security
Tags: Data Analytics;Partners;Google Cloud;Identity & Security
Slug: 2022-06-30-built-with-bigquery-how-exabeam-delivers-a-petabyte-scale-cybersecurity-solution

[Source](https://cloud.google.com/blog/products/identity-security/how-bigquery-helps-exabeam-deliver-cybersecurity-at-petabyte-scale/){:target="_blank" rel="noopener"}

> Editor’s note : The post is part of a series highlighting our awesome partners, and their solutions, that are Built with BigQuery. Exabeam, a leader in SIEM and XDR, provides security operations teams with end-to-end Threat Detection, Investigation, and Response (TDIR) by leveraging a combination of user and entity behavioral analytics (UEBA) and security orchestration, automation, and response (SOAR) to allow organizations to quickly resolve cybersecurity threats. As the company looked to take its cybersecurity solution to the next level, Exabeam partnered with Google Cloud to unlock its ability to scale for storage, ingestion, and analysis of security data. Harnessing [...]
