Title: Unpatched Exchange server, stolen RDP logins... How miscreants get BlackCat ransomware on your network
Date: 2022-06-15T03:40:44+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-15-unpatched-exchange-server-stolen-rdp-logins-how-miscreants-get-blackcat-ransomware-on-your-network

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/15/blackcat-ransomware-microsoft/){:target="_blank" rel="noopener"}

> Microsoft details this ransomware-as-a-service Two of the more prolific cybercriminal groups, which in the past have deployed such high-profile ransomware families as Conti, Ryuk, REvil and Hive, have started adopting the BlackCat ransomware-as-as-service (RaaS) offering.... [...]
