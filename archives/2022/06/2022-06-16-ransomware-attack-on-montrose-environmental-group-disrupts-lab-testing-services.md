Title: Ransomware attack on Montrose Environmental Group disrupts lab testing services
Date: 2022-06-16T14:09:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-16-ransomware-attack-on-montrose-environmental-group-disrupts-lab-testing-services

[Source](https://portswigger.net/daily-swig/ransomware-attack-on-montrose-environmental-group-disrupts-lab-testing-services){:target="_blank" rel="noopener"}

> Some lab results will be delayed, company warns [...]
