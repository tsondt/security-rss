Title: Conducting Modern Insider Risk Investigations
Date: 2022-06-07T12:45:27+00:00
Author: Sponsored Content
Category: Threatpost
Tags: Sponsored
Slug: 2022-06-07-conducting-modern-insider-risk-investigations

[Source](https://threatpost.com/conducting-modern-insider-risk-investigations/179869/){:target="_blank" rel="noopener"}

> Insider Risk Management requires a different approach than to those from external threats. IRM is unique from other domains of security in that the data sources which serve as inputs are as often people as they are tools. Shifting the analyst‘s mindset when handling risks presented by insiders requires us to move through the stages of inquiry, investigation, and determining outcomes. [...]
