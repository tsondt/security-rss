Title: Inside the RSAC expo: Buzzword bingo and the bear in the room
Date: 2022-06-14T07:28:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-14-inside-the-rsac-expo-buzzword-bingo-and-the-bear-in-the-room

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/14/rsa_2020/){:target="_blank" rel="noopener"}

> We mingle with the vendors so you don't have to RSA Conference Your humble vulture never liked conference expos – even before finding myself on the show floor during a global pandemic. Expo halls are a necessary evil that are predominatly visited to find gifts to bring home to the kids.... [...]
