Title: Announcing general availability of reCAPTCHA Enterprise password leak detection
Date: 2022-06-13T16:00:00+00:00
Author: Aaron Malenfant
Category: GCP Security
Tags: Retail;Public Sector;Google Cloud;Identity & Security
Slug: 2022-06-13-announcing-general-availability-of-recaptcha-enterprise-password-leak-detection

[Source](https://cloud.google.com/blog/products/identity-security/announcing-recaptcha-enterprise-password-leak-detection-in-ga/){:target="_blank" rel="noopener"}

> As long as passwords remain an incredibly common form of account authentication, password reuse attacks—which take advantage of people reusing the same password across multiple services—will be one of the most common ways for malicious hackers to hijack user accounts. Password reuse is such a serious problem that more than 52% of users admitted to reusing their password on some sites, and 13% confessed that they use the same password on all their accounts, according to a Google/Harris Poll conducted in 2019. When malicious hackers steal passwords in data breaches, they’re looking to exploit password reuse and increase the chances [...]
