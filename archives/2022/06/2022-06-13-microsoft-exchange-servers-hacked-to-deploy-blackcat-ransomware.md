Title: Microsoft: Exchange servers hacked to deploy BlackCat ransomware
Date: 2022-06-13T13:14:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-06-13-microsoft-exchange-servers-hacked-to-deploy-blackcat-ransomware

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-servers-hacked-to-deploy-blackcat-ransomware/){:target="_blank" rel="noopener"}

> Microsoft says BlackCat ransomware affiliates are now attacking Microsoft Exchange servers using exploits targeting unpatched vulnerabilities. [...]
