Title: DragonForce Gang Unleash Hacks Against Govt. of India
Date: 2022-06-15T13:59:37+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Government;Hacks
Slug: 2022-06-15-dragonforce-gang-unleash-hacks-against-govt-of-india

[Source](https://threatpost.com/hackers-india-government/179968/){:target="_blank" rel="noopener"}

> In response to a comment about the Prophet Mohammed, a hacktivist group in Malaysia has unleashed a wave of cyber attacks in India. [...]
