Title: Israel plans ‘Cyber-Dome’ to defeat digital attacks from Iran and others
Date: 2022-06-30T02:15:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-30-israel-plans-cyber-dome-to-defeat-digital-attacks-from-iran-and-others

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/30/israel_cyber_dome/){:target="_blank" rel="noopener"}

> Already has 'Iron Dome' – does it need another hero? The new head of Israel's National Cyber Directorate (INCD) has announced the nation intends to build a "Cyber-Dome" – a national defense system to fend off digital attacks.... [...]
