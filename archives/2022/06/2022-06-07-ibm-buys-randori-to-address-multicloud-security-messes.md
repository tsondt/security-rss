Title: IBM buys Randori to address multicloud security messes
Date: 2022-06-07T01:30:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-07-ibm-buys-randori-to-address-multicloud-security-messes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/07/ibm_buys_randori_rsa_news/){:target="_blank" rel="noopener"}

> Big Blue joins the hot market for infosec investment RSA Conference IBM has expanded its extensive cybersecurity portfolio by acquiring Randori – a four-year-old startup that specializes in helping enterprises manage their attack surface by identifying and prioritizing their external-facing on-premises and cloud assets.... [...]
