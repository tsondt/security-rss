Title: New AWS whitepaper: AWS User Guide to Financial Services Regulations and Guidelines in New Zealand
Date: 2022-06-21T17:11:03+00:00
Author: Julian Busic
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;banking;Financial Services;Guidance on Cyber Resilience;New Zealand;Reserve Bank of New Zealand;Security Blog
Slug: 2022-06-21-new-aws-whitepaper-aws-user-guide-to-financial-services-regulations-and-guidelines-in-new-zealand

[Source](https://aws.amazon.com/blogs/security/new-aws-whitepaper-aws-user-guide-to-financial-services-regulations-and-guidelines-in-new-zealand/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has released a new whitepaper to help financial services customers in New Zealand accelerate their use of the AWS Cloud. The new AWS User Guide to Financial Services Regulations and Guidelines in New Zealand—along with the existing AWS Workbook for the RBNZ’s Guidance on Cyber Resilience—continues our efforts to help AWS [...]
