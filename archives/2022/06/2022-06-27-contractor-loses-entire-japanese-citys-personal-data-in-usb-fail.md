Title: Contractor loses entire Japanese city's personal data in USB fail
Date: 2022-06-27T10:44:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-06-27-contractor-loses-entire-japanese-citys-personal-data-in-usb-fail

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/27/security_in_brief/){:target="_blank" rel="noopener"}

> Also, Chrome add-ons are great for fingerprinting, and hacked hot tubs splurge details In brief A Japanese contractor working in the city of Amagasaki, near Osaka, reportedly mislaid a USB drive containing personal data on the metropolis's 460,000 residents.... [...]
