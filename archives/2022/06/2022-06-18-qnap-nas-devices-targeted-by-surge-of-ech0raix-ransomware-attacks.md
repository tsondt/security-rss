Title: QNAP NAS devices targeted by surge of eCh0raix ransomware attacks
Date: 2022-06-18T13:06:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-18-qnap-nas-devices-targeted-by-surge-of-ech0raix-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/qnap-nas-devices-targeted-by-surge-of-ech0raix-ransomware-attacks/){:target="_blank" rel="noopener"}

> This week a new series of ech0raix ransomware has started targeting vulnerable QNAP Network Attached Storage (NAS) devices according to user reports and sample submissions on the ID-Ransomware platform. [...]
