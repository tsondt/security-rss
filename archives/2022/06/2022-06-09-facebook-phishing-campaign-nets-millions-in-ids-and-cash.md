Title: Facebook phishing campaign nets millions in IDs and cash
Date: 2022-06-09T17:46:40+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-06-09-facebook-phishing-campaign-nets-millions-in-ids-and-cash

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/09/facebook_phishing_campaign/){:target="_blank" rel="noopener"}

> Hundreds of millions of stolen credentials and a cool $59 million An ongoing phishing campaign targeting Facebook users may have already netted hundreds of millions of credentials and a claimed $59 million, and it's only getting bigger.... [...]
