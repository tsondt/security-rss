Title: Business email platform Zimbra patches memcached injection flaw that imperils user credentials
Date: 2022-06-16T11:04:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-16-business-email-platform-zimbra-patches-memcached-injection-flaw-that-imperils-user-credentials

[Source](https://portswigger.net/daily-swig/business-email-platform-zimbra-patches-memcached-injection-flaw-that-imperils-user-credentials){:target="_blank" rel="noopener"}

> Attackers could also potentially gain access to various internal services, researcher warns [...]
