Title: OpenSSL 3.0.5 awaits release to fix potential worse-than-Heartbleed flaw
Date: 2022-06-27T23:30:34+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-06-27-openssl-305-awaits-release-to-fix-potential-worse-than-heartbleed-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/27/openssl_304_memory_corruption_bug/){:target="_blank" rel="noopener"}

> Though severity up for debate, and limited chips affected, broken tests hold back previous patch from distribution The latest version of OpenSSL v3, a widely used open-source library for secure networking using the Transport Layer Security (TLS) protocol, contains a memory corruption vulnerability that imperils x64 systems with Intel's Advanced Vector Extensions 512 (AVX512).... [...]
