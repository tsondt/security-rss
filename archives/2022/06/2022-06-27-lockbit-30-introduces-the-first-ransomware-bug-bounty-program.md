Title: LockBit 3.0 introduces the first ransomware bug bounty program
Date: 2022-06-27T11:09:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-27-lockbit-30-introduces-the-first-ransomware-bug-bounty-program

[Source](https://www.bleepingcomputer.com/news/security/lockbit-30-introduces-the-first-ransomware-bug-bounty-program/){:target="_blank" rel="noopener"}

> The LockBit ransomware operation has released 'LockBit 3.0,' introducing the first ransomware bug bounty program and leaking new extortion tactics and Zcash cryptocurrency payment options. [...]
