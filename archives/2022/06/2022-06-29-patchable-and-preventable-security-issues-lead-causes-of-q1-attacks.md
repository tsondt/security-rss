Title: Patchable and Preventable Security Issues Lead Causes of Q1 Attacks
Date: 2022-06-29T13:00:49+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Breach;Vulnerabilities;Web Security
Slug: 2022-06-29-patchable-and-preventable-security-issues-lead-causes-of-q1-attacks

[Source](https://threatpost.com/lead-causes-of-q1-attacks/180096/){:target="_blank" rel="noopener"}

> Attacks against U.S. companies spike in Q1 2022 with patchable and preventable external vulnerabilities responsible for bulk of attacks. [...]
