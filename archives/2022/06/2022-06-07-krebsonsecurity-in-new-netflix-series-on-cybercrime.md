Title: KrebsOnSecurity in New Netflix Series on Cybercrime
Date: 2022-06-07T14:58:56+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Web Fraud 2.0;Netflix;SWATting;Web of Make Believe and Lies
Slug: 2022-06-07-krebsonsecurity-in-new-netflix-series-on-cybercrime

[Source](https://krebsonsecurity.com/2022/06/krebsonsecurity-in-new-netflix-series-on-cybercrime/){:target="_blank" rel="noopener"}

> Netflix has a new documentary series airing next week — “ Web of Make Believe: Death, Lies & the Internet ” — in which Yours Truly apparently has a decent amount of screen time. The debut episode explores the far-too-common harassment tactic of “ swatting ” — wherein fake bomb threats or hostage situations are phoned in to police as part of a scheme to trick them into visiting potentially deadly force on a target’s address. Image: Netflix.com The producers of the Netflix show said footage from an interview I sat for in early 2020 on swatting and other threats [...]
