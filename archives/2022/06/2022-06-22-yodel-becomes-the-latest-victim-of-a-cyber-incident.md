Title: Yodel becomes the latest victim of a cyber 'incident'
Date: 2022-06-22T09:15:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-06-22-yodel-becomes-the-latest-victim-of-a-cyber-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/22/yodel/){:target="_blank" rel="noopener"}

> British parcel delivery firm 'working around the clock' to get systems back and running Delivery company Yodel has found itself the latest victim of a cyber "incident" that has disrupted services.... [...]
