Title: Cisco EVP: We need to lift everyone above the cybersecurity poverty line
Date: 2022-06-06T22:50:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-06-cisco-evp-we-need-to-lift-everyone-above-the-cybersecurity-poverty-line

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/06/cisco_security_rsa/){:target="_blank" rel="noopener"}

> It's going to become a human-rights issue, Jeetu Patel tells The Register RSA Conference Exclusive Establishing some level of cybersecurity measures across all organizations will soon reach human-rights issue status, according to Jeetu Patel, Cisco EVP for security and collaboration.... [...]
