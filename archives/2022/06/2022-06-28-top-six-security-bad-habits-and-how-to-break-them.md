Title: Top Six Security Bad Habits, and How to Break Them
Date: 2022-06-28T13:05:00+00:00
Author: Infosec Contributor
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-06-28-top-six-security-bad-habits-and-how-to-break-them

[Source](https://threatpost.com/six-bad-habits-break/180082/){:target="_blank" rel="noopener"}

> Shrav Mehta, CEO, Secureframe, outlines the top six bad habits security teams need to break to prevent costly breaches, ransomware attacks and prevent phishing-based endpoint attacks. [...]
