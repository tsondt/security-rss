Title: US ran offensive cyber ops to support Ukraine, says general
Date: 2022-06-02T01:01:39+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-02-us-ran-offensive-cyber-ops-to-support-ukraine-says-general

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/02/nakasone_us_hacking_russia/){:target="_blank" rel="noopener"}

> Public acknowledgement 'unusual', one cybersec exec tells us America's military conducted offensive cyber operations to support Ukraine in its response to Russia's illegal invasion, US Cyber Command chief General Paul Nakasone has said.... [...]
