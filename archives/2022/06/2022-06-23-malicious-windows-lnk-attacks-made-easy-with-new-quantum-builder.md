Title: Malicious Windows 'LNK' attacks made easy with new Quantum builder
Date: 2022-06-23T11:04:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-23-malicious-windows-lnk-attacks-made-easy-with-new-quantum-builder

[Source](https://www.bleepingcomputer.com/news/security/malicious-windows-lnk-attacks-made-easy-with-new-quantum-builder/){:target="_blank" rel="noopener"}

> Malware researchers have noticed a new tool that helps cybercriminals build malicious.LNK files to deliver payloads for the initial stages of an attack. [...]
