Title: Feds raid dark web market selling data on 24 million Americans
Date: 2022-06-08T14:30:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-06-08-feds-raid-dark-web-market-selling-data-on-24-million-americans

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/08/us_feds_raid_dark_web/){:target="_blank" rel="noopener"}

> SSNDOB sold email addresses, passwords, credit card numbers, SSNs and more US law enforcement has shut down another dark web market, seizing and dismantling SSNDOB, a site dealing in stolen personal information.... [...]
