Title: How Google Cloud can help secure your software supply chain
Date: 2022-06-08T16:00:00+00:00
Author: Damith Karunaratne
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-06-08-how-google-cloud-can-help-secure-your-software-supply-chain

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-can-help-secure-your-software-supply-chain/){:target="_blank" rel="noopener"}

> With the recent announcement of Assured Open Source Software service, Google Cloud can help customers secure their open source software by providing them with the same open source packages that Google uses. By getting security assurances from using these open source packages, Google Cloud customers can enhance their security posture and build their own software using the same tools that we use such as Cloud Build, Artifact Registry and Container/Artifact Analysis. Here’s how Assured OSS can be incorporated into your software supply chain to provide additional software security assurances during the software development and delivery process. Building security into your [...]
