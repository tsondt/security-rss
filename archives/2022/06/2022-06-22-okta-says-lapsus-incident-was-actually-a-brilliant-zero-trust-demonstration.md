Title: Okta says Lapsus$ incident was actually a brilliant zero trust demonstration
Date: 2022-06-22T07:58:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-22-okta-says-lapsus-incident-was-actually-a-brilliant-zero-trust-demonstration

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/22/okta_lapsus_zero_trust_explanation/){:target="_blank" rel="noopener"}

> Once former supplier Sitel coughed up its logs, it became apparent the attacker was hemmed in Okta has completed its analysis of the March 2022 incident that saw The Lapsus$ extortion crew get a glimpse at some customer information, and concluded that its implementation of zero trust techniques foiled the attack.... [...]
