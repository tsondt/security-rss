Title: Being prepared for adversarial attacks
Date: 2022-06-02T10:20:25+00:00
Author: Jeffrey Esposito
Category: Threatpost
Tags: Podcasts;Sponsored;derek manky;Fortinet;Fortinet FortiGuard Labs;podcast;threatpost
Slug: 2022-06-02-being-prepared-for-adversarial-attacks

[Source](https://threatpost.com/threatpost-manky-fortinet/179821/){:target="_blank" rel="noopener"}

> There is no question that the level of threats facing today’s businesses continues to change on a daily basis. So what are the trends that CISOs need to be on the lookout for? For this episode of the Threatpost podcast, I am joined by Derek Manky, Chief Security Strategist & VP Global Threat Intelligence, Fortinet’s [...]
