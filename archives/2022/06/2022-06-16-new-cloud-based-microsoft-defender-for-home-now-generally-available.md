Title: New cloud-based Microsoft Defender for home now generally available
Date: 2022-06-16T11:14:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-06-16-new-cloud-based-microsoft-defender-for-home-now-generally-available

[Source](https://www.bleepingcomputer.com/news/microsoft/new-cloud-based-microsoft-defender-for-home-now-generally-available/){:target="_blank" rel="noopener"}

> Microsoft has announced today the general availability of Microsoft Defender for individuals, the company's new security solution for personal phones and computers. [...]
