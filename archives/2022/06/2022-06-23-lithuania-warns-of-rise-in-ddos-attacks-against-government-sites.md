Title: Lithuania warns of rise in DDoS attacks against government sites
Date: 2022-06-23T12:00:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-23-lithuania-warns-of-rise-in-ddos-attacks-against-government-sites

[Source](https://www.bleepingcomputer.com/news/security/lithuania-warns-of-rise-in-ddos-attacks-against-government-sites/){:target="_blank" rel="noopener"}

> The National Cyber Security Center (NKSC) of Lithuania has issued a public warning about a steep increase in distributed denial of service (DDoS) attacks directed against public authorities in the country. [...]
