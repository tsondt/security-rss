Title: Conti ransomware targeted Intel firmware for stealthy attacks
Date: 2022-06-02T09:22:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-02-conti-ransomware-targeted-intel-firmware-for-stealthy-attacks

[Source](https://www.bleepingcomputer.com/news/security/conti-ransomware-targeted-intel-firmware-for-stealthy-attacks/){:target="_blank" rel="noopener"}

> Researchers analyzing the leaked chats of the notorious Conti ransomware operation have discovered that teams inside the Russian cybercrime group were actively developing firmware hacks. [...]
