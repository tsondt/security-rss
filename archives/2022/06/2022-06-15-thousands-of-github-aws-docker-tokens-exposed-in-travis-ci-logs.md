Title: Thousands of GitHub, AWS, Docker tokens exposed in Travis CI logs
Date: 2022-06-15T03:21:23-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-06-15-thousands-of-github-aws-docker-tokens-exposed-in-travis-ci-logs

[Source](https://www.bleepingcomputer.com/news/security/thousands-of-github-aws-docker-tokens-exposed-in-travis-ci-logs/){:target="_blank" rel="noopener"}

> For a second time in less than a year, the Travis CI platform for software development and testing has exposed user data containing authentication tokens that could give access to developers' accounts on GitHub, Amazon Web Services, and Docker Hub. [...]
