Title: Automotive hose maker Nichirin hit by ransomware attack
Date: 2022-06-23T10:04:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-23-automotive-hose-maker-nichirin-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/automotive-hose-maker-nichirin-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Nichirin-Flex U.S.A, a subsidiary of the Japanese car and motorcycle hose maker Nichirin, has been hit by a ransomware attack causing the company to take the network offline. [...]
