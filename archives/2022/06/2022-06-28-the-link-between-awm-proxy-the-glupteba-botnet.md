Title: The Link Between AWM Proxy & the Glupteba Botnet
Date: 2022-06-28T18:33:31+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;Web Fraud 2.0;alureon;AWM Proxy;Constella Intelligence;dennstr;Dmitry Starovikov;domaintools;ESET;Glupteba botnet;google;Kaspersky Lab;lycefer;Meris;pay-per-install;Riley Kilmer;rootkit;RSOCKS botnet;spur.us;TDL-4;tdss
Slug: 2022-06-28-the-link-between-awm-proxy-the-glupteba-botnet

[Source](https://krebsonsecurity.com/2022/06/the-link-between-awm-proxy-the-glupteba-botnet/){:target="_blank" rel="noopener"}

> On December 7, 2021, Google announced it was suing two Russian men allegedly responsible for operating the Glupteba botnet, a global malware menace that has infected millions of computers over the past decade. That same day, AWM Proxy — a 14-year-old anonymity service that rents hacked PCs to cybercriminals — suddenly went offline. Security experts had long seen a link between Glupteba and AWM Proxy, but new research shows AWM Proxy’s founder is one of the men being sued by Google. AWMproxy, the storefront for renting access to infected PCs, circa 2011. Launched in March 2008, AWM Proxy quickly became [...]
