Title: Microsoft Office 365 feature can help cloud ransomware attacks
Date: 2022-06-16T06:07:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-16-microsoft-office-365-feature-can-help-cloud-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-office-365-feature-can-help-cloud-ransomware-attacks/){:target="_blank" rel="noopener"}

> Security researchers are warning that threat actors could hijack Office 365 accounts to encrypt for a ransom the files stored in SharePoint and OneDrive services that companies use for cloud-based collaboration, document management and storage. [...]
