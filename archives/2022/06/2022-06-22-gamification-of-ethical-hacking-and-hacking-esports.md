Title: Gamification of Ethical Hacking and Hacking Esports
Date: 2022-06-22T12:49:26+00:00
Author: Joseph Carson
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-06-22-gamification-of-ethical-hacking-and-hacking-esports

[Source](https://threatpost.com/gamification-of-ethical-hacking-and-hacking-esports/180039/){:target="_blank" rel="noopener"}

> Joseph Carson, Chief Security Scientist and Advisory CISO at Delinea, explores why gamified platforms and hacking esports are the future. [...]
