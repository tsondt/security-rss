Title: Over 900,000 Kubernetes instances found exposed online
Date: 2022-06-28T06:39:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-28-over-900000-kubernetes-instances-found-exposed-online

[Source](https://www.bleepingcomputer.com/news/security/over-900-000-kubernetes-instances-found-exposed-online/){:target="_blank" rel="noopener"}

> Over 900,000 misconfigured Kubernetes clusters were found exposed on the Internet to potentially malicious scans, some even vulnerable to data-exposing cyberattacks. [...]
