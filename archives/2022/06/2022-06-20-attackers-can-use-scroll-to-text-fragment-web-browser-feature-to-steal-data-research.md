Title: Attackers can use ‘Scroll to Text Fragment’ web browser feature to steal data – research
Date: 2022-06-20T13:07:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-20-attackers-can-use-scroll-to-text-fragment-web-browser-feature-to-steal-data-research

[Source](https://portswigger.net/daily-swig/attackers-can-use-scroll-to-text-fragment-web-browser-feature-to-steal-data-research){:target="_blank" rel="noopener"}

> In some scenarios, CSS style specifications can be manipulated to cause browsers to send data to an attacker-controlled server [...]
