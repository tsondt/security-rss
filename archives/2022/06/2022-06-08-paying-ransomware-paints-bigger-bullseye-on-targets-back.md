Title: Paying Ransomware Paints Bigger Bullseye on Target’s Back
Date: 2022-06-08T13:05:39+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Malware
Slug: 2022-06-08-paying-ransomware-paints-bigger-bullseye-on-targets-back

[Source](https://threatpost.com/paying-ransomware-bullseye-back/179915/){:target="_blank" rel="noopener"}

> Ransomware attackers often strike targets twice, regardless of whether the ransom was paid. [...]
