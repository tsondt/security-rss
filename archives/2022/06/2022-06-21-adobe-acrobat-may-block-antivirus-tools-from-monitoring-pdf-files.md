Title: Adobe Acrobat may block antivirus tools from monitoring PDF files
Date: 2022-06-21T14:44:06-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-06-21-adobe-acrobat-may-block-antivirus-tools-from-monitoring-pdf-files

[Source](https://www.bleepingcomputer.com/news/security/adobe-acrobat-may-block-antivirus-tools-from-monitoring-pdf-files/){:target="_blank" rel="noopener"}

> Security researchers found that Adobe Acrobat is trying to block security software from having visibility into the PDF files it opens, creating a security risk for the users. [...]
