Title: Interpol anti-fraud operation busts call centers behind business email scams
Date: 2022-06-17T02:58:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-17-interpol-anti-fraud-operation-busts-call-centers-behind-business-email-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/17/interpol_operation_first_light_fraud_scam/){:target="_blank" rel="noopener"}

> 1,770 premises raided, 2,000 arrested, $50m seized Law enforcement agencies around the world have arrested about 2,000 people and seized $50 million in a sweeping operation crackdown of social engineering and other scam operations around the globe.... [...]
