Title: The Week in Ransomware - June 3rd 2022 - Evading sanctions
Date: 2022-06-03T16:41:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-03-the-week-in-ransomware-june-3rd-2022-evading-sanctions

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-3rd-2022-evading-sanctions/){:target="_blank" rel="noopener"}

> Ransomware gangs continue to evolve their operations as victims refuse to pay ransoms due to sanctions or other reasons. [...]
