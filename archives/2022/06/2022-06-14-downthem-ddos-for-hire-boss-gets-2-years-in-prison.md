Title: “Downthem” DDoS-for-Hire Boss Gets 2 Years in Prison
Date: 2022-06-14T00:09:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: DDoS-for-Hire;Ne'er-Do-Well News;ampnode;booter;ddos-for-hire;downthem;Juan "Severon" Martinez;Matthew Gatrel;stresser
Slug: 2022-06-14-downthem-ddos-for-hire-boss-gets-2-years-in-prison

[Source](https://krebsonsecurity.com/2022/06/downthem-ddos-for-hire-boss-gets-2-years-in-prison/){:target="_blank" rel="noopener"}

> A 33-year-old Illinois man was sentenced to two years in prison today following his conviction last year for operating services that allowed paying customers to launch powerful distributed denial-of-service (DDoS) attacks against hundreds of thousands of Internet users and websites. The user interface for Downthem[.]org. Matthew Gatrel of St. Charles, Ill. was found guilty for violations of the Computer Fraud and Abuse Act (CFAA) related to his operation of downthem[.]org and ampnode[.]com, two DDoS-for-hire services that had thousands of customers who paid to launch more than 200,000 attacks. Despite admitting to FBI agents that he ran these so-called “ booter [...]
