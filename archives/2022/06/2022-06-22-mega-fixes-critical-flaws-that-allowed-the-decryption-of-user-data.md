Title: MEGA fixes critical flaws that allowed the decryption of user data
Date: 2022-06-22T11:00:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-22-mega-fixes-critical-flaws-that-allowed-the-decryption-of-user-data

[Source](https://www.bleepingcomputer.com/news/security/mega-fixes-critical-flaws-that-allowed-the-decryption-of-user-data/){:target="_blank" rel="noopener"}

> MEGA has released a security update to address a set of severe vulnerabilities that could have exposed user data, even if the data had been stored in encrypted form. [...]
