Title: MetaMask, Phantom warn of flaw that could steal your crypto wallets
Date: 2022-06-16T10:19:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-06-16-metamask-phantom-warn-of-flaw-that-could-steal-your-crypto-wallets

[Source](https://www.bleepingcomputer.com/news/security/metamask-phantom-warn-of-flaw-that-could-steal-your-crypto-wallets/){:target="_blank" rel="noopener"}

> MetaMask and Phantom are warning of a new 'Demonic' vulnerability that could expose a crypto wallet's secret recovery phrase, allowing attackers to steal NFTs and cryptocurrency stored within it. [...]
