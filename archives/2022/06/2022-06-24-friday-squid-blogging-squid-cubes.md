Title: Friday Squid Blogging: Squid Cubes
Date: 2022-06-24T21:04:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-06-24-friday-squid-blogging-squid-cubes

[Source](https://www.schneier.com/blog/archives/2022/06/friday-squid-blogging-squid-cubes.html){:target="_blank" rel="noopener"}

> Researchers thaw squid frozen into a cube and often make interesting discoveries. (Okay, this is a weird story.) As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
