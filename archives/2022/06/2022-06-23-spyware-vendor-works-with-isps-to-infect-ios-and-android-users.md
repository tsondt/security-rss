Title: Spyware vendor works with ISPs to infect iOS and Android users
Date: 2022-06-23T13:07:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-23-spyware-vendor-works-with-isps-to-infect-ios-and-android-users

[Source](https://www.bleepingcomputer.com/news/security/spyware-vendor-works-with-isps-to-infect-ios-and-android-users/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group (TAG) revealed today that RCS Labs, an Italian spyware vendor, has received help from some Internet service providers (ISPs) to infect Android and iOS users in Italy and Kazakhstan with commercial surveillance tools. [...]
