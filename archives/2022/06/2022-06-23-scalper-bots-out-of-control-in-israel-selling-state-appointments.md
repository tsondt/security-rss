Title: Scalper bots out of control in Israel, selling state appointments
Date: 2022-06-23T16:47:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-23-scalper-bots-out-of-control-in-israel-selling-state-appointments

[Source](https://www.bleepingcomputer.com/news/security/scalper-bots-out-of-control-in-israel-selling-state-appointments/){:target="_blank" rel="noopener"}

> Out-of-control scalper bots have created havoc in Israel by registering public service appointments for various government services and then offering to sell them to disgruntled citizens. [...]
