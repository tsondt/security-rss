Title: Cloudflare mitigates record-breaking HTTPS DDoS attack
Date: 2022-06-14T10:31:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-14-cloudflare-mitigates-record-breaking-https-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-mitigates-record-breaking-https-ddos-attack/){:target="_blank" rel="noopener"}

> Internet infrastructure firm Cloudflare said today that it mitigated a 26 million request per second distributed denial-of-service (DDoS) attack, the largest HTTPS DDoS attack detected to date. [...]
