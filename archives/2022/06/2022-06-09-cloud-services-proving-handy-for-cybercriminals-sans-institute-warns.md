Title: Cloud services proving handy for cybercriminals, SANS Institute warns
Date: 2022-06-09T21:30:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-09-cloud-services-proving-handy-for-cybercriminals-sans-institute-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/09/criminals_cloud_sans/){:target="_blank" rel="noopener"}

> Flying horses, gonna pwn me away... RSA Conference Living off the land is so 2021. These days, cybercriminals are living off the cloud, according to Katie Nickels, director of intelligence for Red Canary and a SANS Certified Instructor.... [...]
