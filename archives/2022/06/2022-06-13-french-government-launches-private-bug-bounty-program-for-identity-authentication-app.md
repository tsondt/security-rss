Title: French government launches private bug bounty program for identity authentication app
Date: 2022-06-13T13:24:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-13-french-government-launches-private-bug-bounty-program-for-identity-authentication-app

[Source](https://portswigger.net/daily-swig/french-government-launches-private-bug-bounty-program-for-identity-authentication-app){:target="_blank" rel="noopener"}

> Cryptographic skillset favored during hacker selection process [...]
