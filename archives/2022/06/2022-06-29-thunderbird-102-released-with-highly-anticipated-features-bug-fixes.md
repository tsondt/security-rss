Title: Thunderbird 102 released with highly anticipated features, bug fixes
Date: 2022-06-29T12:00:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Software;Security
Slug: 2022-06-29-thunderbird-102-released-with-highly-anticipated-features-bug-fixes

[Source](https://www.bleepingcomputer.com/news/software/thunderbird-102-released-with-highly-anticipated-features-bug-fixes/){:target="_blank" rel="noopener"}

> Mozilla has announced the release of Thunderbird 102, one of the world's most popular open-source email clients with an estimated userbase of over 25 million. [...]
