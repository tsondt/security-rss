Title: Hacking Tesla’s Remote Key Cards
Date: 2022-06-14T12:19:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-06-14-hacking-teslas-remote-key-cards

[Source](https://www.schneier.com/blog/archives/2022/06/hacking-teslas-remote-key-cards.html){:target="_blank" rel="noopener"}

> Interesting vulnerability in Tesla’s NFC key cards: Martin Herfurt, a security researcher in Austria, quickly noticed something odd about the new feature: Not only did it allow the car to automatically start within 130 seconds of being unlocked with the NFC card, but it also put the car in a state to accept entirely new keys­with no authentication required and zero indication given by the in-car display. “The authorization given in the 130-second interval is too general... [it’s] not only for drive,” Herfurt said in an online interview. “This timer has been introduced by Tesla... in order to make the [...]
