Title: Google Warns Spyware Being Deployed Against Android, iOS Users
Date: 2022-06-24T11:02:00+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Privacy
Slug: 2022-06-24-google-warns-spyware-being-deployed-against-android-ios-users

[Source](https://threatpost.com/google-hermit-spyware-android-ios/180062/){:target="_blank" rel="noopener"}

> The company is warning victims in Italy and Kazakhstan that they have been targeted by the malware from Italian firm RCS Labs. [...]
