Title: Microsoft fixes bug that let hackers hijack Azure Linux clusters
Date: 2022-06-29T06:48:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-06-29-microsoft-fixes-bug-that-let-hackers-hijack-azure-linux-clusters

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-bug-that-let-hackers-hijack-azure-linux-clusters/){:target="_blank" rel="noopener"}

> Microsoft has fixed a container escape vulnerability in the Service Fabric (SF) application hosting platform that would allow threat actors to escalate privileges to root, gain control of the host node, and compromise the entire SF Linux cluster. [...]
