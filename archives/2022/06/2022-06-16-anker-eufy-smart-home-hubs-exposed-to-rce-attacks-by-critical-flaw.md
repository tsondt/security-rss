Title: Anker Eufy smart home hubs exposed to RCE attacks by critical flaw
Date: 2022-06-16T13:38:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2022-06-16-anker-eufy-smart-home-hubs-exposed-to-rce-attacks-by-critical-flaw

[Source](https://www.bleepingcomputer.com/news/security/anker-eufy-smart-home-hubs-exposed-to-rce-attacks-by-critical-flaw/){:target="_blank" rel="noopener"}

> Anker's central smart home device hub, Eufy Homebase 2, was vulnerable to three vulnerabilities, one of which is a critical remote code execution (RCE) flaw. [...]
