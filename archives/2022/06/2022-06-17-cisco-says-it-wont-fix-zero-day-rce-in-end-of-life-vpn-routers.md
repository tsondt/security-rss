Title: Cisco says it won’t fix zero-day RCE in end-of-life VPN routers
Date: 2022-06-17T13:13:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-17-cisco-says-it-wont-fix-zero-day-rce-in-end-of-life-vpn-routers

[Source](https://www.bleepingcomputer.com/news/security/cisco-says-it-won-t-fix-zero-day-rce-in-end-of-life-vpn-routers/){:target="_blank" rel="noopener"}

> Cisco advises owners of end-of-life Small Business RV routers to upgrade to newer models after disclosing a remote code execution vulnerability that will not be patched. [...]
