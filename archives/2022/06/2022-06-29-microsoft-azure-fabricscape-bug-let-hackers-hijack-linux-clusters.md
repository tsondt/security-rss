Title: Microsoft Azure FabricScape bug let hackers hijack Linux clusters
Date: 2022-06-29T06:48:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux;Microsoft
Slug: 2022-06-29-microsoft-azure-fabricscape-bug-let-hackers-hijack-linux-clusters

[Source](https://www.bleepingcomputer.com/news/security/microsoft-azure-fabricscape-bug-let-hackers-hijack-linux-clusters/){:target="_blank" rel="noopener"}

> Microsoft has fixed a container escape bug dubbed FabricScape in the Service Fabric (SF) application hosting platform that let threat actors escalate privileges to root, gain control of the host node, and compromise the entire SF Linux cluster. [...]
