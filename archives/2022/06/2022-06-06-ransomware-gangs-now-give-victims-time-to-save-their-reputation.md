Title: Ransomware gangs now give victims time to save their reputation
Date: 2022-06-06T12:56:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-06-ransomware-gangs-now-give-victims-time-to-save-their-reputation

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-now-give-victims-time-to-save-their-reputation/){:target="_blank" rel="noopener"}

> Threat analysts have observed an unusual trend in ransomware group tactics, reporting that initial phases of victim extortion are becoming less open to the public as the actors tend to use hidden or anonymous entries. [...]
