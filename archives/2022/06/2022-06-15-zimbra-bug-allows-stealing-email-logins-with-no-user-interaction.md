Title: Zimbra bug allows stealing email logins with no user interaction
Date: 2022-06-15T14:01:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-15-zimbra-bug-allows-stealing-email-logins-with-no-user-interaction

[Source](https://www.bleepingcomputer.com/news/security/zimbra-bug-allows-stealing-email-logins-with-no-user-interaction/){:target="_blank" rel="noopener"}

> Zimbra and SonarSource proceeded to the coordinated disclosure of a high-severity vulnerability that allows unauthenticated attackers to steal cleartext credentials from Zimbra without any user interaction. [...]
