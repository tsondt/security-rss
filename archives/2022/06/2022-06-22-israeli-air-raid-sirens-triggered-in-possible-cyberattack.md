Title: Israeli air raid sirens triggered in possible cyberattack
Date: 2022-06-22T18:00:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-06-22-israeli-air-raid-sirens-triggered-in-possible-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/22/israeli_air_raid_sirens_iran/){:target="_blank" rel="noopener"}

> Source remains unclear, plenty suspect Iran Air raid sirens sounded for over an hour in parts of Jerusalem and southern Israel on Sunday evening – but bombs never fell, leading some to blame Iran for compromising the alarms.... [...]
