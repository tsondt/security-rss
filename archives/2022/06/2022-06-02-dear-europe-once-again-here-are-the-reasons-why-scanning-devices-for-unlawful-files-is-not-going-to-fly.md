Title: Dear Europe, once again here are the reasons why scanning devices for unlawful files is not going to fly
Date: 2022-06-02T11:29:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-06-02-dear-europe-once-again-here-are-the-reasons-why-scanning-devices-for-unlawful-files-is-not-going-to-fly

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/02/eu_child_protection/){:target="_blank" rel="noopener"}

> Antivirus-but-for-pictures would trample rights, not even work as expected, say academics While Apple has, temporarily at least, backed away from last year's plan to run client-side scanning (CSS) software on customers' iPhones to detect and report child sexual abuse material (CSAM) to authorities, European officials in May proposed rules to protect children that involve the same highly criticized approach.... [...]
