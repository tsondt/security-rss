Title: Cisco warns of security holes in its security appliances
Date: 2022-06-22T20:16:35+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-22-cisco-warns-of-security-holes-in-its-security-appliances

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/22/cisco_bug_bundle/){:target="_blank" rel="noopener"}

> Bugs potentially useful for rogue insiders, admin account hijackers Cisco has alerted customers to another four vulnerabilities in its products, including a high-severity flaw in its email and web security appliances.... [...]
