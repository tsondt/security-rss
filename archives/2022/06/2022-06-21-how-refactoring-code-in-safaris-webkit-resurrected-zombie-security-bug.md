Title: How refactoring code in Safari's WebKit resurrected 'zombie' security bug
Date: 2022-06-21T08:31:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-21-how-refactoring-code-in-safaris-webkit-resurrected-zombie-security-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/21/apple-safari-zombie-exploit/){:target="_blank" rel="noopener"}

> Fixed in 2013, reinstated in 2016, exploited in the wild this year A security flaw in Apple's Safari web browser that was patched nine years ago was exploited in the wild again some months ago – a perfect example of a "zombie" vulnerability.... [...]
