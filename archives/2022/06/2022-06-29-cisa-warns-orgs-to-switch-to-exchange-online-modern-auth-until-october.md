Title: CISA warns orgs to switch to Exchange Online Modern Auth until October
Date: 2022-06-29T09:06:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-29-cisa-warns-orgs-to-switch-to-exchange-online-modern-auth-until-october

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-orgs-to-switch-to-exchange-online-modern-auth-until-october/){:target="_blank" rel="noopener"}

> CISA has urged government agencies and private sector organizations using Microsoft's Exchange cloud email platform to expedite the switch from Basic Authentication legacy authentication methods without multifactor authentication (MFA) support to Modern Authentication alternatives. [...]
