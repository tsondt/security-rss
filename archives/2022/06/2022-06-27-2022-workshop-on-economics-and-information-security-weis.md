Title: 2022 Workshop on Economics and Information Security (WEIS)
Date: 2022-06-27T11:42:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;conferences;security conferences
Slug: 2022-06-27-2022-workshop-on-economics-and-information-security-weis

[Source](https://www.schneier.com/blog/archives/2022/06/2022-workshop-on-economics-and-information-security-weis.html){:target="_blank" rel="noopener"}

> I did not attend WEIS this year, but Ross Anderson was there and liveblogged all the talks. [...]
