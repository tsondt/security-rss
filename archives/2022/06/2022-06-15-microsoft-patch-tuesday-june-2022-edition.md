Title: Microsoft Patch Tuesday, June 2022 Edition
Date: 2022-06-15T04:52:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;Amit Yoran;Azure Synapse;CVE-2022-30190;Follina;Kevin Beaumont;Mayuresh Dani;Microsoft Patch Tuesday June 2022;Microsoft Support Diagnostics Tool;MSDT;Orca Security;Tenable;Trend Micro Zero Day Initiative
Slug: 2022-06-15-microsoft-patch-tuesday-june-2022-edition

[Source](https://krebsonsecurity.com/2022/06/microsoft-patch-tuesday-june-2022-edition/){:target="_blank" rel="noopener"}

> Microsoft on Tuesday released software updates to fix 60 security vulnerabilities in its Windows operating systems and other software, including a zero-day flaw in all supported Microsoft Office versions on all flavors of Windows that’s seen active exploitation for at least two months now. On a lighter note, Microsoft is officially retiring its Internet Explorer (IE) web browser, which turns 27 years old this year. Three of the bugs tackled this month earned Microsoft’s most dire “critical” label, meaning they can be exploited remotely by malware or miscreants to seize complete control over a vulnerable system. On top of the [...]
