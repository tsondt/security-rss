Title: Intel offers 'server on a card' reference design for network security
Date: 2022-06-08T13:30:06+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-06-08-intel-offers-server-on-a-card-reference-design-for-network-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/08/intel_security_reference_design/){:target="_blank" rel="noopener"}

> OEMs thrown a NetSec Accelerator that plugs into server PCIe slots RSA Conference Intel has released a reference design for a plug-in security card aimed at delivering improved network and security processing without requiring the additional rackspace a discrete appliance would need.... [...]
