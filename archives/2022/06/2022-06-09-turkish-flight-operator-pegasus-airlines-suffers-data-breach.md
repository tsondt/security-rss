Title: Turkish flight operator Pegasus Airlines suffers data breach
Date: 2022-06-09T12:29:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-09-turkish-flight-operator-pegasus-airlines-suffers-data-breach

[Source](https://portswigger.net/daily-swig/turkish-flight-operator-pegasus-airlines-suffers-data-breach){:target="_blank" rel="noopener"}

> Data protection regulator confirms sensitive information was leaked [...]
