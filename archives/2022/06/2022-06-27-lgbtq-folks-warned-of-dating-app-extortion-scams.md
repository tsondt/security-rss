Title: LGBTQ+ folks warned of dating app extortion scams
Date: 2022-06-27T21:37:34+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-27-lgbtq-folks-warned-of-dating-app-extortion-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/27/ftc-lgbtq-extortion/){:target="_blank" rel="noopener"}

> Uncle Sam tells of crooks exploiting Pride Month The FTC is warning members of the LGBTQ+ community about online extortion via dating apps such as Grindr and Feeld.... [...]
