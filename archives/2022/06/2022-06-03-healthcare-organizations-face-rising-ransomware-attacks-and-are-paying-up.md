Title: Healthcare organizations face rising ransomware attacks – and are paying up
Date: 2022-06-03T11:03:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-03-healthcare-organizations-face-rising-ransomware-attacks-and-are-paying-up

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/03/healthcare-ransomware-pay-sophos/){:target="_blank" rel="noopener"}

> Via their insurance companies, natch Healthcare organizations, already an attractive target for ransomware given the highly sensitive data they hold, saw such attacks almost double between 2020 and 2021, according to a survey released this week by Sophos.... [...]
