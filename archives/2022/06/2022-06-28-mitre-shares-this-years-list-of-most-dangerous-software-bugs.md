Title: MITRE shares this year's list of most dangerous software bugs
Date: 2022-06-28T12:29:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-28-mitre-shares-this-years-list-of-most-dangerous-software-bugs

[Source](https://www.bleepingcomputer.com/news/security/mitre-shares-this-years-list-of-most-dangerous-software-bugs/){:target="_blank" rel="noopener"}

> MITRE shared this year's top 25 most common and dangerous weaknesses impacting software throughout the previous two calendar years. [...]
