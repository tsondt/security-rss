Title: 730K WordPress sites force-updated to patch critical plugin bug
Date: 2022-06-16T14:58:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-16-730k-wordpress-sites-force-updated-to-patch-critical-plugin-bug

[Source](https://www.bleepingcomputer.com/news/security/730k-wordpress-sites-force-updated-to-patch-critical-plugin-bug/){:target="_blank" rel="noopener"}

> WordPress sites using Ninja Forms, a forms builder plugin with more than 1 million installations, have been force-updated en masse this week to a new build that addresses a critical security vulnerability likely exploited in the wild. [...]
