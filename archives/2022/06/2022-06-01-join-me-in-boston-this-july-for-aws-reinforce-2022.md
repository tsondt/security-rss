Title: Join me in Boston this July for AWS re:Inforce 2022
Date: 2022-06-01T20:49:01+00:00
Author: CJ Moses
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Security, Identity, & Compliance;authentication;AWS security;Cloud security;cloud security conference;data privacy;Data protection;Identity and Access Management;Security Blog;threat detection and incident response
Slug: 2022-06-01-join-me-in-boston-this-july-for-aws-reinforce-2022

[Source](https://aws.amazon.com/blogs/security/join-me-in-boston-this-july-for-aws-reinforce-2022/){:target="_blank" rel="noopener"}

> I’d like to personally invite you to attend the Amazon Web Services (AWS) security conference, AWS re:Inforce 2022, in Boston, MA on July 26–27. This event offers interactive educational content to address your security, compliance, privacy, and identity management needs. Join security experts, customers, leaders, and partners from around the world who are committed to [...]
