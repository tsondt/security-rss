Title: AMD investigates RansomHouse hack claims, theft of 450GB data
Date: 2022-06-28T13:18:14-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-28-amd-investigates-ransomhouse-hack-claims-theft-of-450gb-data

[Source](https://www.bleepingcomputer.com/news/security/amd-investigates-ransomhouse-hack-claims-theft-of-450gb-data/){:target="_blank" rel="noopener"}

> Chip manufacturer AMD says they are investigating a cyberattack after threat actors claimed to have stolen 450 GB of data from the company last year. [...]
