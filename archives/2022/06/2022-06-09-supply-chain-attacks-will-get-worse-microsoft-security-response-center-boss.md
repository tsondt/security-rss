Title: Supply chain attacks will get worse: Microsoft Security Response Center boss
Date: 2022-06-09T02:30:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-09-supply-chain-attacks-will-get-worse-microsoft-security-response-center-boss

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/09/microsoft_supply_chain_attacks/){:target="_blank" rel="noopener"}

> Do you know all of your software dependencies? Spoiler alert: hardly anybody is on top of it RSA Conference Major supply-chain attacks of recent years – we're talking about SolarWinds, Kaseya and Log4j to name a few – are "just the tip of the iceberg at this point," according to Aanchal Gupta, who leads Microsoft's Security Response Center.... [...]
