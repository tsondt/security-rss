Title: Announcing MITRE ATT&CK mappings for Google Cloud security capabilities
Date: 2022-06-28T15:00:00+00:00
Author: Iman Ghanizada
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-06-28-announcing-mitre-attck-mappings-for-google-cloud-security-capabilities

[Source](https://cloud.google.com/blog/products/identity-security/announcing-mitre-attck-mappings-released-for-google-cloud-security-capabilities/){:target="_blank" rel="noopener"}

> The adoption of Autonomic Security Operations (ASO) requires the ability to use threat informed decision making throughout the continuous detection and continuous response (CD/CR) workflow. We are excited to facilitate this process by mapping native security capabilities of Google Cloud to MITRE ATT&CK® through our research partnership with the MITRE Engenuity Center for Threat-Informed Defense. As a result, Google Cloud users can now evaluate the effectiveness of native security controls against specific ATT&CK® techniques. These mappings can increase your ability to develop better use cases and response playbooks, and identify how to improve security across your Google Cloud workloads. Application [...]
