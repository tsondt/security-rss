Title: WiFi probing exposes smartphone users to tracking, info leaks
Date: 2022-06-11T11:46:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-11-wifi-probing-exposes-smartphone-users-to-tracking-info-leaks

[Source](https://www.bleepingcomputer.com/news/security/wifi-probing-exposes-smartphone-users-to-tracking-info-leaks/){:target="_blank" rel="noopener"}

> Researchers at the University of Hamburg in Germany have conducted a field experiment capturing hundreds of thousands of passersby's WiFi connection probe requests to determine the type of data transmitted without the device owners realizing it. [...]
