Title: New Hertzbleed side-channel attack affects Intel, AMD CPUs
Date: 2022-06-14T15:55:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-14-new-hertzbleed-side-channel-attack-affects-intel-amd-cpus

[Source](https://www.bleepingcomputer.com/news/security/new-hertzbleed-side-channel-attack-affects-intel-amd-cpus/){:target="_blank" rel="noopener"}

> A new side-channel attack known as Hertzbleed allows remote attackers to steal full cryptographic keys by observing variations in CPU frequency enabled by dynamic voltage and frequency scaling (DVFS). [...]
