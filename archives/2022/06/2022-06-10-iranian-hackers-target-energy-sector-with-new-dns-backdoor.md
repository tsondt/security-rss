Title: Iranian hackers target energy sector with new DNS backdoor
Date: 2022-06-10T14:06:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-10-iranian-hackers-target-energy-sector-with-new-dns-backdoor

[Source](https://www.bleepingcomputer.com/news/security/iranian-hackers-target-energy-sector-with-new-dns-backdoor/){:target="_blank" rel="noopener"}

> The Iranian Lycaeum APT hacking group uses a new.NET-based DNS backdoor to conduct attacks on companies in the energy and telecommunication sectors. [...]
