Title: OMIGOD: Cloud providers still using secret middleware
Date: 2022-06-11T11:00:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-06-11-omigod-cloud-providers-still-using-secret-middleware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/11/in-brief-security/){:target="_blank" rel="noopener"}

> All the news you may have missed from RSA this week RSA Conference in brief Researchers from Wiz, who previously found a series of four serious flaws in Azure's Open Management Infrastructure (OMI) agent dubbed "OMIGOD," presented some related news at RSA: Pretty much every cloud provider is installing similar software "without customer's awareness or explicit consent."... [...]
