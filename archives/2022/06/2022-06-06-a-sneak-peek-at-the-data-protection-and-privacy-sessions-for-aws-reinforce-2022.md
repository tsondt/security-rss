Title: A sneak peek at the data protection and privacy sessions for AWS re:Inforce 2022
Date: 2022-06-06T15:16:19+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Events;Security, Identity, & Compliance;AWS security;Cloud security;cloud security conference;data privacy;Data protection;Live Events;Privacy;re:Inforce;Security Blog
Slug: 2022-06-06-a-sneak-peek-at-the-data-protection-and-privacy-sessions-for-aws-reinforce-2022

[Source](https://aws.amazon.com/blogs/security/a-sneak-peek-at-the-data-protection-and-privacy-sessions-for-reinforce-2022/){:target="_blank" rel="noopener"}

> Register now with discount code SALUZwmdkJJ to get $150 off your full conference pass to AWS re:Inforce. For a limited time only and while supplies last. Today we want to tell you about some of the engaging data protection and privacy sessions planned for AWS re:Inforce. AWS re:Inforce is a learning conference where you can [...]
