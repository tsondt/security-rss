Title: Automotive fabric supplier TB Kawashima announces cyberattack
Date: 2022-06-25T09:12:06-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-06-25-automotive-fabric-supplier-tb-kawashima-announces-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/automotive-fabric-supplier-tb-kawashima-announces-cyberattack/){:target="_blank" rel="noopener"}

> TB Kawashima, part of the Japanese automotive component manufacturer Toyota Boshoku of the Toyota Group of companies, announced that one of its subsidiaries has been hit by a cyberattack. [...]
