Title: Atlassian: Unpatched critical flaw under attack right now to hijack Confluence
Date: 2022-06-03T00:28:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-03-atlassian-unpatched-critical-flaw-under-attack-right-now-to-hijack-confluence

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/03/atlassian_confluence_critical_flaw_attacked/){:target="_blank" rel="noopener"}

> One suggested option: Turn the thing off until it can be fixed Atlassian has warned users of its Confluence collaboration tool that they should either restrict internet access to the software, or disable it, in light of a critical-rated unauthenticated remote-code-execution flaw in the product that is actively under attack.... [...]
