Title: Microsoft postpones shift to New Commerce Experience subscriptions
Date: 2022-06-29T06:47:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-29-microsoft-postpones-shift-to-new-commerce-experience-subscriptions

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/29/microsoft_nce_indefinite_extension/){:target="_blank" rel="noopener"}

> The whiff of rebellion among Cloud Solution Providers is getting stronger Microsoft has indefinitely postponed the date on which its Cloud Solution Providers (CSPs) will be required to sell software and services licences on new terms.... [...]
