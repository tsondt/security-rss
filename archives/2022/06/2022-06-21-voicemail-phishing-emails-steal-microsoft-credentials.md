Title: Voicemail phishing emails steal Microsoft credentials
Date: 2022-06-21T00:36:46+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-21-voicemail-phishing-emails-steal-microsoft-credentials

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/21/phishing-voicemail-microsoft-zscaler/){:target="_blank" rel="noopener"}

> As always, check that O365 login page is actually O365 Someone is trying to steal people's Microsoft 365 and Outlook credentials by sending them phishing emails disguised as voicemail notifications.... [...]
