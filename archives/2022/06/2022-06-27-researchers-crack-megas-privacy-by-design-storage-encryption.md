Title: Researchers crack MEGA’s ‘privacy by design’ storage, encryption
Date: 2022-06-27T16:34:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-27-researchers-crack-megas-privacy-by-design-storage-encryption

[Source](https://portswigger.net/daily-swig/researchers-crack-megas-privacy-by-design-storage-encryption){:target="_blank" rel="noopener"}

> ETH Zurich finds flaws in the firm’s cryptographic infrastructure [...]
