Title: Statutory defense for ethical hacking under UK Computer Misuse Act tabled
Date: 2022-06-23T14:06:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-23-statutory-defense-for-ethical-hacking-under-uk-computer-misuse-act-tabled

[Source](https://portswigger.net/daily-swig/statutory-defense-for-ethical-hacking-under-uk-computer-misuse-act-tabled){:target="_blank" rel="noopener"}

> Amendment applies to bill related to 5G rollout and connected products [...]
