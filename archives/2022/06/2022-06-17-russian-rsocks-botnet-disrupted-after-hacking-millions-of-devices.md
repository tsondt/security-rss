Title: Russian RSocks botnet disrupted after hacking millions of devices
Date: 2022-06-17T10:17:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-06-17-russian-rsocks-botnet-disrupted-after-hacking-millions-of-devices

[Source](https://www.bleepingcomputer.com/news/security/russian-rsocks-botnet-disrupted-after-hacking-millions-of-devices/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice has announced the disruption of the Russian RSocks malware botnet used to hijack millions of computers, Android smartphones, and IoT (Internet of Things) devices worldwide for use as proxy servers. [...]
