Title: China lured graduate jobseekers into digital espionage
Date: 2022-06-30T13:49:56+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;Policy;china;espionage;hacking
Slug: 2022-06-30-china-lured-graduate-jobseekers-into-digital-espionage

[Source](https://arstechnica.com/?p=1863684){:target="_blank" rel="noopener"}

> Enlarge (credit: FT montage | Getty Images | Dreamstime ) Chinese university students have been lured to work at a secretive technology company that masked the true nature of their jobs: researching Western targets for spying and translating hacked documents as part of Beijing’s industrial-scale intelligence regime. The Financial Times has identified and contacted 140 potential translators, mostly recent graduates who have studied English at public universities in Hainan, Sichuan and Xi’an. They had responded to job advertisements at Hainan Xiandun, a company that was located in the tropical southern island of Hainan. The application process included translation tests on [...]
