Title: When and where to use IAM permissions boundaries
Date: 2022-06-01T17:09:13+00:00
Author: Umair Rehmat
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;IAM;Identity;permissions boundaries;Security;Security Blog
Slug: 2022-06-01-when-and-where-to-use-iam-permissions-boundaries

[Source](https://aws.amazon.com/blogs/security/when-and-where-to-use-iam-permissions-boundaries/){:target="_blank" rel="noopener"}

> Customers often ask for guidance on permissions boundaries in AWS Identity and Access Management (IAM) and when, where, and how to use them. A permissions boundary is an IAM feature that helps your centralized cloud IAM teams to safely empower your application developers to create new IAM roles and policies in Amazon Web Services (AWS). [...]
