Title: US senators seek ban on sale of health location data
Date: 2022-06-17T20:29:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-17-us-senators-seek-ban-on-sale-of-health-location-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/17/bill-location-data-ban-roe/){:target="_blank" rel="noopener"}

> With Supreme Court set to overturn Roe v Wade, privacy is key A group of senators wants to make it illegal for data brokers to sell sensitive location and health information of individuals' medical treatment.... [...]
