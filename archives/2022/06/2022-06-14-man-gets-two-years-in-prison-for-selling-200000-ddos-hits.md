Title: Man gets two years in prison for selling 200,000 DDoS hits
Date: 2022-06-14T19:29:24+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-14-man-gets-two-years-in-prison-for-selling-200000-ddos-hits

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/14/man-sentenced-ddos-attacks/){:target="_blank" rel="noopener"}

> Over 2,000 customers with malice on their minds A 33-year-old Illinois man has been sentenced to two years in prison for running websites that paying customers used to launch more than 200,000 distributed denial-of-services (DDoS) attacks.... [...]
