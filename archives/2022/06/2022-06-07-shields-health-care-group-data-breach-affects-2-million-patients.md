Title: Shields Health Care Group data breach affects 2 million patients
Date: 2022-06-07T10:53:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2022-06-07-shields-health-care-group-data-breach-affects-2-million-patients

[Source](https://www.bleepingcomputer.com/news/security/shields-health-care-group-data-breach-affects-2-million-patients/){:target="_blank" rel="noopener"}

> Shields Health Care Group (Shields) suffered a data breach that exposed the data of approximately 2,000,000 people in the United States after hackers breached their network and stole data. [...]
