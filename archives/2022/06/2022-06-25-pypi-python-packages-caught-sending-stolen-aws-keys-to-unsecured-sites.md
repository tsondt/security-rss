Title: PyPi python packages caught sending stolen AWS keys to unsecured sites
Date: 2022-06-25T11:32:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-25-pypi-python-packages-caught-sending-stolen-aws-keys-to-unsecured-sites

[Source](https://www.bleepingcomputer.com/news/security/pypi-python-packages-caught-sending-stolen-aws-keys-to-unsecured-sites/){:target="_blank" rel="noopener"}

> Multiple malicious Python packages available on the PyPI repository were caught stealing sensitive information like AWS credentials and transmitting it to publicly exposed endpoints accessible by anyone. [...]
