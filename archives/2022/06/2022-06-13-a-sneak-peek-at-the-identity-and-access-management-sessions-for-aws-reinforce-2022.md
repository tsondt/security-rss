Title: A sneak peek at the identity and access management sessions for AWS re:Inforce 2022
Date: 2022-06-13T19:31:17+00:00
Author: Ilya Epshteyn
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Events;Security, Identity, & Compliance;AWS security;Cloud security;cloud security conference;IAM;Identity and Access Management;Live Events;Security Blog
Slug: 2022-06-13-a-sneak-peek-at-the-identity-and-access-management-sessions-for-aws-reinforce-2022

[Source](https://aws.amazon.com/blogs/security/a-sneak-peek-at-the-identity-and-access-management-sessions-for-aws-reinforce-2022/){:target="_blank" rel="noopener"}

> Register now with discount code SALFNj7FaRe to get $150 off your full conference pass to AWS re:Inforce. For a limited time only and while supplies last. AWS re:Inforce 2022 will take place in-person in Boston, MA, on July 26 and 27 and will include some exciting identity and access management sessions. AWS re:Inforce 2022 features [...]
