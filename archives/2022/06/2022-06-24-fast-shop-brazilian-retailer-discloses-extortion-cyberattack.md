Title: Fast Shop Brazilian retailer discloses "extortion" cyberattack
Date: 2022-06-24T11:53:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-24-fast-shop-brazilian-retailer-discloses-extortion-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/fast-shop-brazilian-retailer-discloses-extortion-cyberattack/){:target="_blank" rel="noopener"}

> Fast Shop, one of Brazil's largest retailers, has suffered an 'extortion' cyberattack that led to network disruption and the temporary closure of its online store. [...]
