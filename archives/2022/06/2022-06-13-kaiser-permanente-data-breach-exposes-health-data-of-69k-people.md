Title: Kaiser Permanente data breach exposes health data of 69K people
Date: 2022-06-13T18:54:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-13-kaiser-permanente-data-breach-exposes-health-data-of-69k-people

[Source](https://www.bleepingcomputer.com/news/security/kaiser-permanente-data-breach-exposes-health-data-of-69k-people/){:target="_blank" rel="noopener"}

> Kaiser Permanente, one of America's leading not-for-profit health plans and health care providers, has recently disclosed a data breach that exposed the health information of more than 69,000 individuals. [...]
