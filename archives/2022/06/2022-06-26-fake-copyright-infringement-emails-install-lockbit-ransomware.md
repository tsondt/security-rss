Title: Fake copyright infringement emails install LockBit ransomware
Date: 2022-06-26T11:05:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-26-fake-copyright-infringement-emails-install-lockbit-ransomware

[Source](https://www.bleepingcomputer.com/news/security/fake-copyright-infringement-emails-install-lockbit-ransomware/){:target="_blank" rel="noopener"}

> LockBit ransomware affiliates are using an interesting trick to get people into infecting their devices by disguising their malware as copyright claims. [...]
