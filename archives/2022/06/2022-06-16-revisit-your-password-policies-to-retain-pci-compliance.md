Title: Revisit Your Password Policies to Retain PCI Compliance
Date: 2022-06-16T10:02:01-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-06-16-revisit-your-password-policies-to-retain-pci-compliance

[Source](https://www.bleepingcomputer.com/news/security/revisit-your-password-policies-to-retain-pci-compliance/){:target="_blank" rel="noopener"}

> Organizations that are subject to the PCI regulations must carefully consider how best to address these new requirements. Some of the requirements are relatively easy to address. Even so, some of the new requirements go beyond what Windows native security mechanisms are capable of. Here is what you need to know. [...]
