Title: Potent Emotet Variant Spreads Via Stolen Email Credentials
Date: 2022-06-10T11:02:29+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: 2022-06-10-potent-emotet-variant-spreads-via-stolen-email-credentials

[Source](https://threatpost.com/potent-emotet-variant-spreads-via-stolen-email-credentials/179932/){:target="_blank" rel="noopener"}

> The dangerous malware appears to be well and truly back in action, sporting new variants and security-dodging behaviors in a wave of recent phishing campaigns. [...]
