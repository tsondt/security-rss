Title: Americans report losing over $1 billion to cryptocurrency scams
Date: 2022-06-03T13:24:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-06-03-americans-report-losing-over-1-billion-to-cryptocurrency-scams

[Source](https://www.bleepingcomputer.com/news/security/americans-report-losing-over-1-billion-to-cryptocurrency-scams/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) says over 46,000 people Americans have reported losing more than $1 billion worth of cryptocurrency to scams between January 2021 and March 2022. [...]
