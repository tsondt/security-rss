Title: Metasploit 6.2.0 improves credential theft, SMB support features, more
Date: 2022-06-13T14:15:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-13-metasploit-620-improves-credential-theft-smb-support-features-more

[Source](https://www.bleepingcomputer.com/news/security/metasploit-620-improves-credential-theft-smb-support-features-more/){:target="_blank" rel="noopener"}

> ​Metasploit 6.2.0 has been released with 138 new modules, 148 new improvements/features, and 156 bug fixes since version 6.1.0 was released in August 2021. [...]
