Title: HTTP/3 evolves into RFC 9114 – a security advantage, but not without challenges
Date: 2022-06-07T13:38:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-07-http3-evolves-into-rfc-9114-a-security-advantage-but-not-without-challenges

[Source](https://portswigger.net/daily-swig/http-3-evolves-into-rfc-9114-a-security-advantage-but-not-without-challenges){:target="_blank" rel="noopener"}

> The backbone of the web has received a major upgrade [...]
