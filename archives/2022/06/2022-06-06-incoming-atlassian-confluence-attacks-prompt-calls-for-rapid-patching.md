Title: Incoming! Atlassian Confluence attacks prompt calls for rapid patching
Date: 2022-06-06T12:53:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-06-incoming-atlassian-confluence-attacks-prompt-calls-for-rapid-patching

[Source](https://portswigger.net/daily-swig/incoming-atlassian-confluence-attacks-prompt-calls-for-rapid-patching){:target="_blank" rel="noopener"}

> China suspected in assaults against enterprises running collaboration platform [...]
