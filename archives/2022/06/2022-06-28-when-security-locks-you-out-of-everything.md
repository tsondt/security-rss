Title: When Security Locks You Out of Everything
Date: 2022-06-28T11:22:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;cybersecurity;passwords;two-factor authentication
Slug: 2022-06-28-when-security-locks-you-out-of-everything

[Source](https://www.schneier.com/blog/archives/2022/06/__trashed-2.html){:target="_blank" rel="noopener"}

> Thought experiment story of someone of someone who lost everything in a house fire, and now can’t log into anything: But to get into my cloud, I need my password and 2FA. And even if I could convince the cloud provider to bypass that and let me in, the backup is secured with a password which is stored in—you guessed it—my Password Manager. I am in cyclic dependency hell. To get my passwords, I need my 2FA. To get my 2FA, I need my passwords. It’s a one-in-a-million story, and one that’s hard to take into account in system design. [...]
