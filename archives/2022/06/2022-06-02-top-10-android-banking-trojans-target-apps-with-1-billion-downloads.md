Title: Top 10 Android banking trojans target apps with 1 billion downloads
Date: 2022-06-02T17:09:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2022-06-02-top-10-android-banking-trojans-target-apps-with-1-billion-downloads

[Source](https://www.bleepingcomputer.com/news/security/top-10-android-banking-trojans-target-apps-with-1-billion-downloads/){:target="_blank" rel="noopener"}

> The ten most prolific Android mobile banking trojans target 639 financial applications that collectively have over one billion downloads on the Google Play Store. [...]
