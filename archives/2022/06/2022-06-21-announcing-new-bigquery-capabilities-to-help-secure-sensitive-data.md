Title: Announcing new BigQuery capabilities to help secure sensitive data
Date: 2022-06-21T16:00:00+00:00
Author: Deepti Mandava
Category: GCP Security
Tags: Google Cloud;Data Analytics;Identity & Security
Slug: 2022-06-21-announcing-new-bigquery-capabilities-to-help-secure-sensitive-data

[Source](https://cloud.google.com/blog/products/identity-security/announcing-new-bigquery-capabilities-to-help-secure-sensitive-data/){:target="_blank" rel="noopener"}

> In order to better serve their customers and users, digital applications and platforms continue to store and use sensitive data such as Personally Identifiable Information (PII), genetic and biometric information, and credit card information. Many organizations that provide data for analytics use cases face evolving regulatory and privacy mandates, ongoing risks from data breaches and data leakage, and a growing need to control data access. Data access control and masking of sensitive information is even more complex for large enterprises that are building massive data ecosystems. Copies of datasets often are created to manage access to different groups. Sometimes, copies [...]
