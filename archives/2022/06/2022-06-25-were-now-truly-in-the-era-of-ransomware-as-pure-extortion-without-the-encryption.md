Title: We're now truly in the era of ransomware as pure extortion without the encryption
Date: 2022-06-25T10:41:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-25-were-now-truly-in-the-era-of-ransomware-as-pure-extortion-without-the-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/25/ransomware_gangs_extortion_feature/){:target="_blank" rel="noopener"}

> Why screw around with cryptography and keys when just stealing the info is good enough Feature US and European cops, prosecutors, and NGOs recently convened a two-day workshop in the Hague to discuss how to respond to the growing scourge of ransomware.... [...]
