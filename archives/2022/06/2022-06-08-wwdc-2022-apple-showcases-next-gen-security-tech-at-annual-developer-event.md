Title: WWDC 2022: Apple showcases next-gen security tech at annual developer event
Date: 2022-06-08T13:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-08-wwdc-2022-apple-showcases-next-gen-security-tech-at-annual-developer-event

[Source](https://portswigger.net/daily-swig/wwdc-2022-apple-showcases-next-gen-security-tech-at-annual-developer-event){:target="_blank" rel="noopener"}

> Passkeys, Safety Check, and Private Access Tokens demonstrated during week-long virtual conference [...]
