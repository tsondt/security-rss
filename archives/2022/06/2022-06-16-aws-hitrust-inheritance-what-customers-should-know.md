Title: AWS HITRUST Inheritance: What customers should know
Date: 2022-06-16T19:36:31+00:00
Author: Sonali Vaidya
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS HITRUST;AWS HITRUST Inheritance;AWS security;Security Blog
Slug: 2022-06-16-aws-hitrust-inheritance-what-customers-should-know

[Source](https://aws.amazon.com/blogs/security/aws-hitrust-csf-certification-is-available-for-customer-inheritance/){:target="_blank" rel="noopener"}

> As an Amazon Web Services (AWS) customer, you don’t have to assess the controls that you inherit from the AWS HITRUST Validated Assessment Questionnaire, because AWS already has completed HITRUST assessment using version 9.4 in 2021. You can deploy your environments onto AWS and inherit our HITRUST CSF certification, provided that you use only in-scope [...]
