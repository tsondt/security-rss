Title: A Guide to Surviving a Ransomware Attack
Date: 2022-06-30T11:59:34+00:00
Author: Oliver Tavakoli
Category: Threatpost
Tags: InfoSec Insider;Malware
Slug: 2022-06-30-a-guide-to-surviving-a-ransomware-attack

[Source](https://threatpost.com/a-guide-to-surviving-a-ransomware-attack/180110/){:target="_blank" rel="noopener"}

> Oliver Tavakoli, CTO at Vectra AI, gives us hope that surviving a ransomware attack is possible, so long as we apply preparation and intentionality to our defense posture. [...]
