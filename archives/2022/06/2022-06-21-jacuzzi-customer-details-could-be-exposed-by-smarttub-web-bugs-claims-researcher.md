Title: Jacuzzi customer details could be exposed by SmartTub web bugs, claims researcher
Date: 2022-06-21T15:10:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-21-jacuzzi-customer-details-could-be-exposed-by-smarttub-web-bugs-claims-researcher

[Source](https://portswigger.net/daily-swig/jacuzzi-customer-details-could-be-exposed-by-smarttub-web-bugs-claims-researcher){:target="_blank" rel="noopener"}

> Iconic hot tub manufacturer addresses flaws that also apparently exposed numerous backend services [...]
