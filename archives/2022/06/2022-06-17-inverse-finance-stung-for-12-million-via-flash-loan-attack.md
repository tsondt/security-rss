Title: Inverse Finance stung for $1.2 million via flash loan attack
Date: 2022-06-17T21:34:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-06-17-inverse-finance-stung-for-12-million-via-flash-loan-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/17/inverse_finance_heist/){:target="_blank" rel="noopener"}

> Just cryptocurrency things A decentralized autonomous organization (DAO) called Inverse Finance has been robbed of cryptocurrency somehow exchangeable for $1.2 million, just two months after being taken for $15.6 million.... [...]
