Title: Leaking Military Secrets on Gaming Discussion Boards
Date: 2022-06-08T11:17:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;games;leaks;military;operational security;secrecy
Slug: 2022-06-08-leaking-military-secrets-on-gaming-discussion-boards

[Source](https://www.schneier.com/blog/archives/2022/06/leaking-military-secrets-on-gaming-discussion-boards.html){:target="_blank" rel="noopener"}

> People are leaking classified military information on discussion boards for the video game War Thunder to win arguments — repeatedly. [...]
