Title: India extends deadline for compliance with infosec logging rules by 90 days
Date: 2022-06-28T02:02:50+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-28-india-extends-deadline-for-compliance-with-infosec-logging-rules-by-90-days

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/28/india_directions_deadline_logging/){:target="_blank" rel="noopener"}

> Helpfully announced extension on deadline day India's Ministry of Electronics and Information Technology (MeitY) and the local Computer Emergency Response Team (CERT-In) have extended the deadline for compliance with the Cyber Security Directions introduced on April 28, which were due to take effect yesterday.... [...]
