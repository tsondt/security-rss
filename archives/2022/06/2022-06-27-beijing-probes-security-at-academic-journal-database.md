Title: Beijing probes security at academic journal database
Date: 2022-06-27T05:30:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-06-27-beijing-probes-security-at-academic-journal-database

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/27/beijing_investigates_cnki_security/){:target="_blank" rel="noopener"}

> It's easy to see why – the question is, why now? China's internet regulator has launched an investigation into the security regime protecting academic journal database China National Knowledge Infrastructure (CNKI), citing national security concerns.... [...]
