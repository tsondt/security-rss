Title: One in every 13 incidents blamed on API insecurity – report
Date: 2022-06-22T16:41:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-22-one-in-every-13-incidents-blamed-on-api-insecurity-report

[Source](https://portswigger.net/daily-swig/one-in-every-13-incidents-blamed-on-api-insecurity-report){:target="_blank" rel="noopener"}

> Larger organizations are statistically more at risk, warns Imperva [...]
