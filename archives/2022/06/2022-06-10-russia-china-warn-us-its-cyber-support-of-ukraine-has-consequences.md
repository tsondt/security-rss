Title: Russia, China, warn US its cyber support of Ukraine has consequences
Date: 2022-06-10T03:16:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-10-russia-china-warn-us-its-cyber-support-of-ukraine-has-consequences

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/10/russia_china_usa_ukraine_cyberdefense/){:target="_blank" rel="noopener"}

> Countries that accept US infosec help told they could pay a price too Russia and China have each warned the United States that the offensive cyber-ops it ran to support Ukraine were acts of aggression that invite reprisal.... [...]
