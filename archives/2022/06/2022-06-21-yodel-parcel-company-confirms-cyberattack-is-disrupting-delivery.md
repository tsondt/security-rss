Title: Yodel parcel company confirms cyberattack is disrupting delivery
Date: 2022-06-21T18:43:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-06-21-yodel-parcel-company-confirms-cyberattack-is-disrupting-delivery

[Source](https://www.bleepingcomputer.com/news/security/yodel-parcel-company-confirms-cyberattack-is-disrupting-delivery/){:target="_blank" rel="noopener"}

> Services for the U.K.-based Yodel delivery service company have been disrupted due to a cyberattack that caused delays in parcel distribution and tracking orders online. [...]
