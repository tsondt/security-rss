Title: Kaiser Permanente data breach exposed healthcare records of 70,000 patients
Date: 2022-06-13T15:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-13-kaiser-permanente-data-breach-exposed-healthcare-records-of-70000-patients

[Source](https://portswigger.net/daily-swig/kaiser-permanente-data-breach-exposed-healthcare-records-of-70-000-patients){:target="_blank" rel="noopener"}

> Health plan provider plays down ID theft fears after breach at Washington state division [...]
