Title: Foxconn confirms ransomware attack disrupted production in Mexico
Date: 2022-06-02T04:20:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-02-foxconn-confirms-ransomware-attack-disrupted-production-in-mexico

[Source](https://www.bleepingcomputer.com/news/security/foxconn-confirms-ransomware-attack-disrupted-production-in-mexico/){:target="_blank" rel="noopener"}

> Foxconn electronics manufacturer has confirmed that one of its Mexico-based production plants has been impacted by a ransomware attack in late May. [...]
