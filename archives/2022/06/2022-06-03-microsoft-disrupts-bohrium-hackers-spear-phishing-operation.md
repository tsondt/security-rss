Title: Microsoft disrupts Bohrium hackers’ spear-phishing operation
Date: 2022-06-03T11:24:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-06-03-microsoft-disrupts-bohrium-hackers-spear-phishing-operation

[Source](https://www.bleepingcomputer.com/news/security/microsoft-disrupts-bohrium-hackers-spear-phishing-operation/){:target="_blank" rel="noopener"}

> The Microsoft Digital Crimes Unit (DCU) has disrupted a spear-phishing operation linked to an Iranian threat actor tracked as Bohrium that targeted customers in the U.S., Middle East, and India. [...]
