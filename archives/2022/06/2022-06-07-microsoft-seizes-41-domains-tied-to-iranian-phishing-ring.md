Title: Microsoft seizes 41 domains tied to 'Iranian phishing ring'
Date: 2022-06-07T00:04:18+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-07-microsoft-seizes-41-domains-tied-to-iranian-phishing-ring

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/07/microsoft_bohrium_domains/){:target="_blank" rel="noopener"}

> Windows giant gets court order to take over dot-coms and more Microsoft has obtained a court order to seize 41 domains used by what the Windows giant said was an Iranian cybercrime group that ran a spear-phishing operation targeting organizations in the US, Middle East, and India.... [...]
