Title: PyPI package 'keep' mistakenly included a password stealer
Date: 2022-06-12T15:03:44-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-06-12-pypi-package-keep-mistakenly-included-a-password-stealer

[Source](https://www.bleepingcomputer.com/news/security/pypi-package-keep-mistakenly-included-a-password-stealer/){:target="_blank" rel="noopener"}

> PyPI packages 'keep,' 'pyanxdns,' 'api-res-py' were found to contain a password-stealer and a backdoor due to the presence of malicious 'request' dependency within some versions. [...]
