Title: New Firefox privacy feature strips URLs of tracking parameters
Date: 2022-06-28T17:11:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Software
Slug: 2022-06-28-new-firefox-privacy-feature-strips-urls-of-tracking-parameters

[Source](https://www.bleepingcomputer.com/news/security/new-firefox-privacy-feature-strips-urls-of-tracking-parameters/){:target="_blank" rel="noopener"}

> Mozilla Firefox 102 was released today with a new privacy feature that strips parameters from URLs that are used to track you around the web. [...]
