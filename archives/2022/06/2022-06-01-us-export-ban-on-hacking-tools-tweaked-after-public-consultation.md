Title: US export ban on hacking tools tweaked after public consultation
Date: 2022-06-01T15:54:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-01-us-export-ban-on-hacking-tools-tweaked-after-public-consultation

[Source](https://portswigger.net/daily-swig/us-export-ban-on-hacking-tools-tweaked-after-public-consultation){:target="_blank" rel="noopener"}

> Government has sought to allay misgivings of cybersecurity industry [...]
