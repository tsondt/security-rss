Title: ExpressVPN moves servers out of India to escape customer data retention law
Date: 2022-06-02T05:58:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-02-expressvpn-moves-servers-out-of-india-to-escape-customer-data-retention-law

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/02/expressvpnservers_out_of_india/){:target="_blank" rel="noopener"}

> Privacy service will keep working, just beyond the reach of India's government Virtual private network operator ExpressVPN will pull its servers from India, citing the impossibility of complying with the nation's incoming requirement to record users' identities and activities.... [...]
