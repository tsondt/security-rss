Title: Unpatched bug chain poses ‘mass account takeover’ threat to Yunmai weight monitoring app
Date: 2022-06-06T14:20:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-06-unpatched-bug-chain-poses-mass-account-takeover-threat-to-yunmai-weight-monitoring-app

[Source](https://portswigger.net/daily-swig/unpatched-bug-chain-poses-mass-account-takeover-threat-to-yunmai-weight-monitoring-app){:target="_blank" rel="noopener"}

> User data related to at least 500,000 Android accounts at risk [...]
