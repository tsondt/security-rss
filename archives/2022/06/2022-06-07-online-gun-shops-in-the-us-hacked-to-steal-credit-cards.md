Title: Online gun shops in the US hacked to steal credit cards
Date: 2022-06-07T12:27:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-07-online-gun-shops-in-the-us-hacked-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/online-gun-shops-in-the-us-hacked-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> Rainier Arms and Numrich Gun Parts, two American gun shops that operate e-commerce sites on rainierarms.com and gunpartscorp.com, have disclosed data breach incidents resulting from card skimmer infections on their sites. [...]
