Title: Old Hacks Die Hard: Ransomware, Social Engineering Top Verizon DBIR Threats – Again
Date: 2022-06-03T13:46:55+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2022-06-03-old-hacks-die-hard-ransomware-social-engineering-top-verizon-dbir-threats-again

[Source](https://threatpost.com/old-hacks-die-hard-ransomware-social-engineering-top-verizon-dbir-threats-again/179864/){:target="_blank" rel="noopener"}

> Deja-Vu data from this year's DBIR report feels like we are stuck in the movie 'Groundhog Day.' [...]
