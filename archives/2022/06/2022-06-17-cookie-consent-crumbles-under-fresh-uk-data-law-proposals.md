Title: Cookie consent crumbles under fresh UK data law proposals
Date: 2022-06-17T12:15:06+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-06-17-cookie-consent-crumbles-under-fresh-uk-data-law-proposals

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/17/cookies_crumble_in_uk_data/){:target="_blank" rel="noopener"}

> Campaigners fear erosion of rights as narrowing of law proposed as well as political control over independent watchdog The UK government has published its plans for reforming local data protection law which includes removing the requirement for consent for all website cookies – akin to the situation across much of the US.... [...]
