Title: New DFSCoerce NTLM Relay attack allows Windows domain takeover
Date: 2022-06-20T16:35:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-06-20-new-dfscoerce-ntlm-relay-attack-allows-windows-domain-takeover

[Source](https://www.bleepingcomputer.com/news/microsoft/new-dfscoerce-ntlm-relay-attack-allows-windows-domain-takeover/){:target="_blank" rel="noopener"}

> A new Windows NTLM relay attack called DFSCoerce has been discovered that uses MS-DFSNM, Microsoft's Distributed File System, to completely take over a Windows domain. [...]
