Title: Atlassian: Unpatched critical flaw under attack right now to hijack Confluence - and it's been there since 2013
Date: 2022-06-03T00:28:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-03-atlassian-unpatched-critical-flaw-under-attack-right-now-to-hijack-confluence-and-its-been-there-since-2013

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/03/atlassian_confluence_critical_flaw_attacked/){:target="_blank" rel="noopener"}

> CISA's suggested action is to take the thing offline until it can be fixed, Atlassian has added a possible defence Updated Atlassian has warned users of its Confluence collaboration tool that they should either restrict internet access to the software, or disable it, in light of a critical-rated unauthenticated remote-code-execution flaw in the product that is actively under attack.... [...]
