Title: Cyber Risk Retainers: Not Another Insurance Policy
Date: 2022-06-07T13:25:11+00:00
Author: Matt Dunn
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-06-07-cyber-risk-retainers-not-another-insurance-policy

[Source](https://threatpost.com/cyber-risk-retainers-not-another-insurance-policy/179895/){:target="_blank" rel="noopener"}

> The costs associated with a cyberattack can be significant, especially if a company does not have an Incident Response plan that addresses risk. [...]
