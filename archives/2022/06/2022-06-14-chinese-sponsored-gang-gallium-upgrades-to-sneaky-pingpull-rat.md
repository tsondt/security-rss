Title: Chinese-sponsored gang Gallium upgrades to sneaky PingPull RAT
Date: 2022-06-14T06:27:05+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-14-chinese-sponsored-gang-gallium-upgrades-to-sneaky-pingpull-rat

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/14/gallium-pingpull-rat/){:target="_blank" rel="noopener"}

> Broadens targets from telecoms to finance and government orgs The Gallium group, believed to be a Chinese state-sponsored team, is going on the warpath with an upgraded remote access trojan (RAT) that threat hunters say is difficult to detect.... [...]
