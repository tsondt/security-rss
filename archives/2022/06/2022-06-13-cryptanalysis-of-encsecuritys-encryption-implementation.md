Title: Cryptanalysis of ENCSecurity’s Encryption Implementation
Date: 2022-06-13T11:48:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-06-13-cryptanalysis-of-encsecuritys-encryption-implementation

[Source](https://www.schneier.com/blog/archives/2022/06/cryptanalysis-of-encsecuritys-encryption-implementation.html){:target="_blank" rel="noopener"}

> ENCSecurity markets a file encryption system, and it’s used by SanDisk, Sony, Lexar, and probably others. Despite it using AES as its algorithm, it’s implementation is flawed in multiple ways—and breakable. The moral is, as it always is, that implementing cryptography securely is hard. Don’t roll your own anything if you can help it. [...]
