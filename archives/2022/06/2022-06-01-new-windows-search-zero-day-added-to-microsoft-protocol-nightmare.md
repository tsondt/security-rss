Title: New Windows Search zero-day added to Microsoft protocol nightmare
Date: 2022-06-01T18:06:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-01-new-windows-search-zero-day-added-to-microsoft-protocol-nightmare

[Source](https://www.bleepingcomputer.com/news/security/new-windows-search-zero-day-added-to-microsoft-protocol-nightmare/){:target="_blank" rel="noopener"}

> A new Windows Search zero-day vulnerability can be used to automatically open a search window containing remotely-hosted malware executables simply by launching a Word document. [...]
