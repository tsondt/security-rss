Title: The curious tale of a fake Carrier.app
Date: 2022-06-23T09:01:00.001000-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2022-06-23-the-curious-tale-of-a-fake-carrierapp

[Source](https://googleprojectzero.blogspot.com/2022/06/curious-case-carrier-app.html){:target="_blank" rel="noopener"}

> Posted by Ian Beer, Google Project Zero NOTE: This issue was CVE-2021-30983 was fixed in iOS 15.2 in December 2021. Towards the end of 2021 Google's Threat Analysis Group (TAG) shared an iPhone app with me: App splash screen showing the Vodafone carrier logo and the text " My Vodafone " (not the legitimate Vodadone app) Although this looks like the real My Vodafone carrier app available in the App Store, it didn't come from the App Store and is not the real application from Vodafone. TAG suspects that a target receives a link to this app in an SMS, [...]
