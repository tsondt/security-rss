Title: Why Netflix isn't the Only One Bummed About Password Sharing
Date: 2022-06-07T10:01:02-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-06-07-why-netflix-isnt-the-only-one-bummed-about-password-sharing

[Source](https://www.bleepingcomputer.com/news/security/why-netflix-isnt-the-only-one-bummed-about-password-sharing/){:target="_blank" rel="noopener"}

> Carnegie Mellen found that as much as 28% of end-users willingly share passwords with others, and a Specops study found that of those who share passwords 21% of people don't know who else their password has been shared with. That's a lot of sharing going on. [...]
