Title: AWS HITRUST Shared Responsibility Matrix version 1.2 now available
Date: 2022-06-07T18:26:00+00:00
Author: Sonali Vaidya
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS Compliance;AWS HITRUST;AWS HITRUST Inheritance;AWS security
Slug: 2022-06-07-aws-hitrust-shared-responsibility-matrix-version-12-now-available

[Source](https://aws.amazon.com/blogs/security/aws-hitrust-shared-responsibility-matrix-version-1-2-now-available/){:target="_blank" rel="noopener"}

> The latest version of the AWS HITRUST Shared Responsibility Matrix is now available to download. Version 1.2 is based on HITRUST MyCSF version 9.4[r2] and was released by HITRUST on April 20, 2022. AWS worked with HITRUST to update the Shared Responsibility Matrix and to add new controls based on MyCSF v9.4[r2]. You don’t have [...]
