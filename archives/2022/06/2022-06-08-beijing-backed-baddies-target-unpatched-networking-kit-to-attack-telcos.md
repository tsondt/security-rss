Title: Beijing-backed baddies target unpatched networking kit to attack telcos
Date: 2022-06-08T07:56:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-08-beijing-backed-baddies-target-unpatched-networking-kit-to-attack-telcos

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/08/cisa_fbi_nsa_china_attack_advisory/){:target="_blank" rel="noopener"}

> NSA, FBI and CISA issue joint advisory that suggests China hardly has to work for this – flaws revealed in 2017 are among their entry points State-sponsored Chinese attackers are actively exploiting old vulnerabilities to "establish a broad network of compromised infrastructure" then using it to attack telcos and network services providers.... [...]
