Title: Smartphones and Civilians in Wartime
Date: 2022-06-09T11:22:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;laws;sensors;smartphones;war
Slug: 2022-06-09-smartphones-and-civilians-in-wartime

[Source](https://www.schneier.com/blog/archives/2022/06/smartphones-and-civilians-in-wartime.html){:target="_blank" rel="noopener"}

> Interesting article about civilians using smartphones to assist their militaries in wartime, and how that blurs the important legal distinction between combatants and non-combatants: The principle of distinction between the two roles is a critical cornerstone of international humanitarian law­—the law of armed conflict, codified by decades of customs and laws such as the Geneva Conventions. Those considered civilians and civilian targets are not to be attacked by military forces; as they are not combatants, they should be spared. At the same time, they also should not act as combatants—­if they do, they may lose this status. The conundrum, then, [...]
