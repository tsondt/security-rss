Title: On the Dangers of Cryptocurrencies and the Uselessness of Blockchain
Date: 2022-06-24T11:13:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bitcoin;blockchain;cryptocurrency;essays
Slug: 2022-06-24-on-the-dangers-of-cryptocurrencies-and-the-uselessness-of-blockchain

[Source](https://www.schneier.com/blog/archives/2022/06/on-the-dangers-of-cryptocurrencies-and-the-uselessness-of-blockchain.html){:target="_blank" rel="noopener"}

> Earlier this month, I and others wrote a letter to Congress, basically saying that cryptocurrencies are an complete and total disaster, and urging them to regulate the space. Nothing in that letter is out of the ordinary, and is in line with what I wrote about blockchain in 2019. In response, Matthew Green has written —not really a rebuttal—but a “a general response to some of the more common spurious objections...people make to public blockchain systems.” In it, he makes several broad points: Yes, current proof-of-work blockchains like bitcoin are terrible for the environment. But there are other modes like [...]
