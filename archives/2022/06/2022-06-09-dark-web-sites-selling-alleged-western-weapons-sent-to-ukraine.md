Title: Dark web sites selling alleged Western weapons sent to Ukraine
Date: 2022-06-09T08:30:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-09-dark-web-sites-selling-alleged-western-weapons-sent-to-ukraine

[Source](https://www.bleepingcomputer.com/news/security/dark-web-sites-selling-alleged-western-weapons-sent-to-ukraine/){:target="_blank" rel="noopener"}

> Several weapon marketplaces on the dark web have listed military-grade firearms allegedly coming from Western countries that sent them to support the Ukrainian army in its fight against the Russian invaders. [...]
