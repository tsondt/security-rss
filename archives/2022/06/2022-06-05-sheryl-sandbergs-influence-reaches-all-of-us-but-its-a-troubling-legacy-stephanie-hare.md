Title: Sheryl Sandberg’s influence reaches all of us. But it’s a troubling legacy | Stephanie Hare
Date: 2022-06-05T07:00:07+00:00
Author: Stephanie Hare
Category: The Guardian
Tags: Sheryl Sandberg;Facebook;Privacy;Data and computer security;Internet safety;Meta;US news;World news;Social networking;Media;Technology
Slug: 2022-06-05-sheryl-sandbergs-influence-reaches-all-of-us-but-its-a-troubling-legacy-stephanie-hare

[Source](https://www.theguardian.com/commentisfree/2022/jun/05/sheryl-sandberg-influence-reaches-us-all-but-its-troubling-legacy){:target="_blank" rel="noopener"}

> From epic data mining to shocking failures of content moderation, Meta’s COO passes on a vast clean-up job If you are reading this, odds are that you are one of the 2.87 billion daily users of the products offered by Meta, the parent company of Facebook, Instagram, Facebook Messenger and WhatsApp. If you are not using any of these products, you are connected to people who do use them. And this connects you to Sheryl Sandberg, who resigned last week from her role as Meta’s chief operating officer. Even if you have never met her, interacted directly with her or [...]
