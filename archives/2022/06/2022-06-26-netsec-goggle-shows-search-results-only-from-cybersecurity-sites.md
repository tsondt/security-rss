Title: NetSec Goggle shows search results only from cybersecurity sites
Date: 2022-06-26T09:14:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-26-netsec-goggle-shows-search-results-only-from-cybersecurity-sites

[Source](https://www.bleepingcomputer.com/news/security/netsec-goggle-shows-search-results-only-from-cybersecurity-sites/){:target="_blank" rel="noopener"}

> A new Brave Search Goggle modifies Brave Search results to only show reputable cybersecurity sites, making it easier to search for and find security information. [...]
