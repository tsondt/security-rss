Title: Severe Parse Server bug impacts Apple Game Center
Date: 2022-06-22T14:08:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-22-severe-parse-server-bug-impacts-apple-game-center

[Source](https://portswigger.net/daily-swig/severe-parse-server-bug-impacts-apple-game-center){:target="_blank" rel="noopener"}

> Fake certificates could be used to bypass authentication controls [...]
