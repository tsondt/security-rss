Title: WatchDog hacking group launches new Docker cryptojacking campaign
Date: 2022-06-03T13:50:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-03-watchdog-hacking-group-launches-new-docker-cryptojacking-campaign

[Source](https://www.bleepingcomputer.com/news/security/watchdog-hacking-group-launches-new-docker-cryptojacking-campaign/){:target="_blank" rel="noopener"}

> ​The WatchDog hacking group is conducting a new cryptojacking campaign with advanced techniques for intrusion, worm-like propagation, and evasion of security software. [...]
