Title: Dark web awash with breached credentials, study finds
Date: 2022-06-15T14:26:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-15-dark-web-awash-with-breached-credentials-study-finds

[Source](https://portswigger.net/daily-swig/dark-web-awash-with-breached-credentials-study-finds){:target="_blank" rel="noopener"}

> Many consumers still relying on easy-to-crack passwords, warns Digital Shadows [...]
