Title: Google Workspace now alerts of critical changes to admin accounts
Date: 2022-06-29T08:30:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-29-google-workspace-now-alerts-of-critical-changes-to-admin-accounts

[Source](https://www.bleepingcomputer.com/news/security/google-workspace-now-alerts-of-critical-changes-to-admin-accounts/){:target="_blank" rel="noopener"}

> Google Workspace (formerly G Suite) has been updated to notify admins of highly sensitive changes to configurations, including those made to single sign-on (SSO) profiles and admin accounts. [...]
