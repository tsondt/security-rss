Title: UK health privacy watchdog still in talks over who is accessing country's COVID data store
Date: 2022-06-14T10:13:04+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-06-14-uk-health-privacy-watchdog-still-in-talks-over-who-is-accessing-countrys-covid-data-store

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/14/ndg_covid_data_store/){:target="_blank" rel="noopener"}

> Over a year after discussions began, National Data Guardian continues to pursue transparency in health data use More than two years after England launched a COVID data store, keeping details of National Health Service (NHS) patients, the country's National Data Guardian (NDG) remains unsatisfied with who is accessing the data.... [...]
