Title: Telegram’s blogging platform abused in phishing attacks
Date: 2022-06-01T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-01-telegrams-blogging-platform-abused-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/telegram-s-blogging-platform-abused-in-phishing-attacks/){:target="_blank" rel="noopener"}

> Telegram's anonymous blogging platform, Telegraph, is being actively exploited by phishing actors who take advantage of the platform's lax policies to set up interim landing pages that lead to the theft of account credentials. [...]
