Title: US: Chinese govt hackers breached telcos to snoop on network traffic
Date: 2022-06-07T18:43:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-07-us-chinese-govt-hackers-breached-telcos-to-snoop-on-network-traffic

[Source](https://www.bleepingcomputer.com/news/security/us-chinese-govt-hackers-breached-telcos-to-snoop-on-network-traffic/){:target="_blank" rel="noopener"}

> Several US federal agencies today revealed that Chinese-backed threat actors have targeted and compromised major telecommunications companies and network service providers to steal credentials and harvest data. [...]
