Title: Russian botnet ‘RSOCKS’ dismantled after hacking millions of devices
Date: 2022-06-17T17:55:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-17-russian-botnet-rsocks-dismantled-after-hacking-millions-of-devices

[Source](https://portswigger.net/daily-swig/russian-botnet-rsocks-dismantled-after-hacking-millions-of-devices){:target="_blank" rel="noopener"}

> Sock it to ‘em [...]
