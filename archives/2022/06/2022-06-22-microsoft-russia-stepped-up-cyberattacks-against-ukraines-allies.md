Title: Microsoft: Russia stepped up cyberattacks against Ukraine’s allies
Date: 2022-06-22T13:59:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-06-22-microsoft-russia-stepped-up-cyberattacks-against-ukraines-allies

[Source](https://www.bleepingcomputer.com/news/security/microsoft-russia-stepped-up-cyberattacks-against-ukraine-s-allies/){:target="_blank" rel="noopener"}

> Microsoft said today that Russian intelligence agencies have stepped up cyberattacks against governments of countries that have allied themselves with Ukraine after Russia's invasion. [...]
