Title: Announcing Apigee Advanced API Security for Google Cloud
Date: 2022-06-30T16:00:00+00:00
Author: Vikas Anand
Category: GCP Security
Tags: Infrastructure Modernization;Google Cloud;Identity & Security
Slug: 2022-06-30-announcing-apigee-advanced-api-security-for-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/announcing-apigee-advanced-api-security-for-google-cloud/){:target="_blank" rel="noopener"}

> Organizations in every region and industry are developing APIs to enable easier and more standardized delivery of services and data for digital experiences. This increasing shift to digital experiences has grown API usage and traffic volumes. However, as malicious API attacks also have grown, API security has become an important battleground over business risk. To help customers more easily address their growing API security needs, Google Cloud is announcing today the Preview of Advanced API Security, a comprehensive set of API security capabilities built on Apigee, our API management platform. Advanced API Security enables organizations to more easily detect security [...]
