Title: Get more out of service control policies in a multi-account environment
Date: 2022-06-06T19:03:18+00:00
Author: Omar Haq
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;AWS Organizations;Policies;Security Blog;service control policies
Slug: 2022-06-06-get-more-out-of-service-control-policies-in-a-multi-account-environment

[Source](https://aws.amazon.com/blogs/security/get-more-out-of-service-control-policies-in-a-multi-account-environment/){:target="_blank" rel="noopener"}

> Many of our customers use AWS Organizations to manage multiple Amazon Web Services (AWS) accounts. There are many benefits to using multiple accounts in your organization, such as grouping workloads with a common business purpose, complying with regulatory frameworks, and establishing strong isolation barriers between applications based on ownership. Customers are even using distinct accounts [...]
