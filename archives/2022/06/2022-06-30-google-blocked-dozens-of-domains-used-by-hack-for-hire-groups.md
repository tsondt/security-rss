Title: Google blocked dozens of domains used by hack-for-hire groups
Date: 2022-06-30T08:19:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-30-google-blocked-dozens-of-domains-used-by-hack-for-hire-groups

[Source](https://www.bleepingcomputer.com/news/security/google-blocked-dozens-of-domains-used-by-hack-for-hire-groups/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group (TAG) has blocked dozens of malicious domains and websites used by hack-for-hire groups in attacks targeting high-risk targets worldwide. [...]
