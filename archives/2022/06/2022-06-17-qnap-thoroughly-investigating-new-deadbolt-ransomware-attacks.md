Title: QNAP 'thoroughly investigating' new DeadBolt ransomware attacks
Date: 2022-06-17T05:52:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-17-qnap-thoroughly-investigating-new-deadbolt-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/qnap-thoroughly-investigating-new-deadbolt-ransomware-attacks/){:target="_blank" rel="noopener"}

> Network-attached storage (NAS) vendor QNAP once again warned customers on Friday to secure their devices against a new campaign of attacks pushing DeadBolt ransomware. [...]
