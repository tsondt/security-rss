Title: Citrix warns critical bug can let attackers reset admin passwords
Date: 2022-06-15T10:46:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-15-citrix-warns-critical-bug-can-let-attackers-reset-admin-passwords

[Source](https://www.bleepingcomputer.com/news/security/citrix-warns-critical-bug-can-let-attackers-reset-admin-passwords/){:target="_blank" rel="noopener"}

> Citrix warned customers to deploy security updates that address a critical Citrix Application Delivery Management (ADM) vulnerability that can let attackers reset admin passwords. [...]
