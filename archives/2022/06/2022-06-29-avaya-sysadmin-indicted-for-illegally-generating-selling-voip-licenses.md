Title: Avaya sysadmin indicted for illegally generating, selling VoIP licenses
Date: 2022-06-29T14:44:56-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-29-avaya-sysadmin-indicted-for-illegally-generating-selling-voip-licenses

[Source](https://www.bleepingcomputer.com/news/security/avaya-sysadmin-indicted-for-illegally-generating-selling-voip-licenses/){:target="_blank" rel="noopener"}

> Three defendants who allegedly sold over $88 million worth of software licenses belonging to Avaya Holdings Corporation have been charged in Oklahoma, U.S., facing 14 counts of wire fraud and money laundering. [...]
