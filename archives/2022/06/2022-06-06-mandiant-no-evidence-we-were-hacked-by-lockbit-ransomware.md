Title: Mandiant: “No evidence” we were hacked by LockBit ransomware
Date: 2022-06-06T15:54:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-06-mandiant-no-evidence-we-were-hacked-by-lockbit-ransomware

[Source](https://www.bleepingcomputer.com/news/security/mandiant-no-evidence-we-were-hacked-by-lockbit-ransomware/){:target="_blank" rel="noopener"}

> American cybersecurity firm Mandiant is investigating LockBit ransomware gang's claims that they hacked the company's network and stole data. [...]
