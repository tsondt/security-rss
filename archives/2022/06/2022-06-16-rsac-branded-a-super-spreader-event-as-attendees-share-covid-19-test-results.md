Title: RSAC branded a 'super spreader event' as attendees share COVID-19 test results
Date: 2022-06-16T21:56:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-16-rsac-branded-a-super-spreader-event-as-attendees-share-covid-19-test-results

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/16/rsa_covid_risk/){:target="_blank" rel="noopener"}

> That, and Black Hat, are about to reveal risk assessment skills of our cyber-risk experts RSA Conference Quick show of hands: who came home from this year's RSA Conference without COVID-19?... [...]
