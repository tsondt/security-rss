Title: Clipminer rakes in $1.7m in crypto hijacking scam
Date: 2022-06-03T12:30:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-03-clipminer-rakes-in-17m-in-crypto-hijacking-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/03/clipminer-cryptocurrency-millions/){:target="_blank" rel="noopener"}

> Crooks divert transactions to own wallets while running mining on the side A crew using malware that performs cryptomining and clipboard-hacking operations have made off with at least $1.7 million in stolen cryptocurrency.... [...]
