Title: Even Russia's Evil Corp now favors software-as-a-service
Date: 2022-06-03T22:55:42+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-03-even-russias-evil-corp-now-favors-software-as-a-service

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/03/evil-corp-ransomware-sanctions/){:target="_blank" rel="noopener"}

> Albeit to avoid US sanctions hitting it in the wallet The Russian-based Evil Corp is jumping from one malware strain to another in hopes of evading sanctions placed on it by the US government in 2019.... [...]
