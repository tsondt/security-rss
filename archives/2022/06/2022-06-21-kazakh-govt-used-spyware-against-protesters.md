Title: Kazakh Govt. Used Spyware Against Protesters
Date: 2022-06-21T12:48:21+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2022-06-21-kazakh-govt-used-spyware-against-protesters

[Source](https://threatpost.com/kazakh-govt-used-spyware-against-protesters/180016/){:target="_blank" rel="noopener"}

> Researchers have discovered that a Kazakhstan government entity deployed sophisticated Italian spyware within its borders. [...]
