Title: LGBTQ+ community warned of extortionists abusing dating apps
Date: 2022-06-26T12:04:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-26-lgbtq-community-warned-of-extortionists-abusing-dating-apps

[Source](https://www.bleepingcomputer.com/news/security/lgbtq-plus-community-warned-of-extortionists-abusing-dating-apps/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) has warned this week of extortion scammers targeting the LGBTQ+ community by abusing online dating apps like Grindr and Feeld. [...]
