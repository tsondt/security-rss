Title: Jenkins warns of security holes in these 25 plugins
Date: 2022-06-30T20:22:43+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-06-30-jenkins-warns-of-security-holes-in-these-25-plugins

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/30/jenkins_plugins_security_advisories/){:target="_blank" rel="noopener"}

> Relax, most of the vulnerabilities so far have, er, no fix Jenkins, an open-source automation server for continuous integration and delivery (CI/CD), has published 34 security advisories covering 25 plugins used to extend the software.... [...]
