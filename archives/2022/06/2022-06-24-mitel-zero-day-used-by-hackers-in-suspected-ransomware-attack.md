Title: Mitel zero-day used by hackers in suspected ransomware attack
Date: 2022-06-24T13:13:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-24-mitel-zero-day-used-by-hackers-in-suspected-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/mitel-zero-day-used-by-hackers-in-suspected-ransomware-attack/){:target="_blank" rel="noopener"}

> Hackers used a zero-day exploit on Linux-based Mitel MiVoice VOIP appliances for initial access in what is believed to be the beginning of a ransomware attack. [...]
