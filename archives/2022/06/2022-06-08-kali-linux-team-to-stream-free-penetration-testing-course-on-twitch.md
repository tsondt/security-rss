Title: Kali Linux team to stream free penetration testing course on Twitch
Date: 2022-06-08T17:26:23-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-08-kali-linux-team-to-stream-free-penetration-testing-course-on-twitch

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-team-to-stream-free-penetration-testing-course-on-twitch/){:target="_blank" rel="noopener"}

> Offensive Security, the creators of Kali Linux, announced today that they would be offering free access to their live-streamed 'Penetration Testing with Kali Linux (PEN-200/PWK)' training course later this month. [...]
