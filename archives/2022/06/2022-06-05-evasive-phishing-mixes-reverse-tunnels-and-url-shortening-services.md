Title: Evasive phishing mixes reverse tunnels and URL shortening services
Date: 2022-06-05T11:06:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-05-evasive-phishing-mixes-reverse-tunnels-and-url-shortening-services

[Source](https://www.bleepingcomputer.com/news/security/evasive-phishing-mixes-reverse-tunnels-and-url-shortening-services/){:target="_blank" rel="noopener"}

> Security researchers are seeing an uptick in the use of reverse tunnel services along with URL shorteners​​​​​​​ for large-scale phishing campaigns, making the malicious activity more difficult to stop. [...]
