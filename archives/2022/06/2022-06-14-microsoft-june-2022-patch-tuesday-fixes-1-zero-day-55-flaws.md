Title: Microsoft June 2022 Patch Tuesday fixes 1 zero-day, 55 flaws
Date: 2022-06-14T13:45:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-06-14-microsoft-june-2022-patch-tuesday-fixes-1-zero-day-55-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-june-2022-patch-tuesday-fixes-1-zero-day-55-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's June 2022 Patch Tuesday, and with it comes fixes for 55 vulnerabilities, including fixes for the Windows MSDT 'Follina' zero-day vulnerability and new Intel MMIO flaws. [...]
