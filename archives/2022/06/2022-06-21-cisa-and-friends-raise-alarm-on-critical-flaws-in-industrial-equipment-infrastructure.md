Title: CISA and friends raise alarm on critical flaws in industrial equipment, infrastructure
Date: 2022-06-21T04:01:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-21-cisa-and-friends-raise-alarm-on-critical-flaws-in-industrial-equipment-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/21/56_vulnerabilities_critical_industrial/){:target="_blank" rel="noopener"}

> Nearly 60 holes found affecting 'more than 30,000' machines worldwide Fifty-six vulnerabilities – some deemed critical – have been found in industrial operational technology (OT) systems from ten global manufacturers including Honeywell, Ericsson, Motorola, and Siemens, putting more than 30,000 devices worldwide at risk, according to the US government's CISA and private security researchers.... [...]
