Title: Evilnum hackers return in new operation targeting migration orgs
Date: 2022-06-28T17:49:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-28-evilnum-hackers-return-in-new-operation-targeting-migration-orgs

[Source](https://www.bleepingcomputer.com/news/security/evilnum-hackers-return-in-new-operation-targeting-migration-orgs/){:target="_blank" rel="noopener"}

> The Evilnum hacking group is showing renewed signs of malicious activity, targeting European organizations that are involved in international migration. [...]
