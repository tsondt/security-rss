Title: Ecuador’s Attempt to Resettle Edward Snowden
Date: 2022-06-29T11:19:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Edward Snowden;NSA;whistleblowers
Slug: 2022-06-29-ecuadors-attempt-to-resettle-edward-snowden

[Source](https://www.schneier.com/blog/archives/2022/06/ecuadors-attempt-to-resettle-edward-snowden.html){:target="_blank" rel="noopener"}

> Someone hacked the Ecuadorian embassy in Moscow and found a document related to Ecuador’s 2013 efforts to bring Edward Snowden there. If you remember, Snowden was traveling from Hong Kong to somewhere when the US revoked his passport, stranding him in Russia. In the document, Ecuador asks Russia to provide Snowden with safe passage to come to Ecuador. It’s hard to believe this all happened almost ten years ago. [...]
