Title: Travel-related Cybercrime Takes Off as Industry Rebounds
Date: 2022-06-15T13:37:23+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Breach;Hacks
Slug: 2022-06-15-travel-related-cybercrime-takes-off-as-industry-rebounds

[Source](https://threatpost.com/travel-related-cybercrime-takes-off/179962/){:target="_blank" rel="noopener"}

> Upsurge in the tourism industry after the COVID-19 pandemic grabs the attention of cybercriminals to scam the tourists. [...]
