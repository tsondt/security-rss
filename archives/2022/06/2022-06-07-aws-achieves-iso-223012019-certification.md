Title: AWS achieves ISO 22301:2019 certification
Date: 2022-06-07T16:55:39+00:00
Author: Sonali Vaidya
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS ISO;AWS ISO Certificates;AWS ISO22301;AWS ISO27001;AWS ISO27017;AWS ISO27018;AWS ISO9001;Security Blog
Slug: 2022-06-07-aws-achieves-iso-223012019-certification

[Source](https://aws.amazon.com/blogs/security/aws-achieves-iso-223012019-certification/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has successfully achieved ISO 22301:2019 certification without audit findings. ISO 22301:2019 is a rigorous third-party independent assessment of the international standard for Business Continuity Management (BCM). Published by the International Organization for Standardization (ISO), ISO 22301:2019 is designed to help organizations prevent, prepare for, respond to, [...]
