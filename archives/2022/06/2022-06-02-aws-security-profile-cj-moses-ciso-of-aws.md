Title: AWS Security Profile: CJ Moses, CISO of AWS
Date: 2022-06-02T15:42:20+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Thought Leadership;AWS Security Profile;Security Blog
Slug: 2022-06-02-aws-security-profile-cj-moses-ciso-of-aws

[Source](https://aws.amazon.com/blogs/security/aws_security_profile_cj_moses_ciso_of_aws/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, I interview the people who work in Amazon Web Services (AWS) Security and help keep our customers safe and secure. This interview is with CJ Moses—previously the AWS Deputy Chief Information Security Officer (CISO), he began his role as CISO of AWS in February of 2022. How did you [...]
