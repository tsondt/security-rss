Title: US seizes SSNDOB market for selling personal info of 24 million people
Date: 2022-06-07T19:47:45-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-07-us-seizes-ssndob-market-for-selling-personal-info-of-24-million-people

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-ssndob-market-for-selling-personal-info-of-24-million-people/){:target="_blank" rel="noopener"}

> SSNDOB, an online marketplace that sold the names, social security numbers, and dates of birth of approximately 24 million US people, has been taken offline following an international law enforcement operation. [...]
