Title: Hertzbleed: A New Side-Channel Attack
Date: 2022-06-20T11:23:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;keys;side-channel attacks
Slug: 2022-06-20-hertzbleed-a-new-side-channel-attack

[Source](https://www.schneier.com/blog/archives/2022/06/hertzbleed-a-new-side-channel-attack.html){:target="_blank" rel="noopener"}

> Hertzbleed is a new side-channel attack that works against a variety of microprocressors. Deducing cryptographic keys by analyzing power consumption has long been an attack, but it’s not generally viable because measuring power consumption is often hard. This new attack measures power consumption by measuring time, making it easier to exploit. The team discovered that dynamic voltage and frequency scaling (DVFS)—a power and thermal management feature added to every modern CPU—allows attackers to deduce the changes in power consumption by monitoring the time it takes for a server to respond to specific carefully made queries. The discovery greatly reduces what’s [...]
