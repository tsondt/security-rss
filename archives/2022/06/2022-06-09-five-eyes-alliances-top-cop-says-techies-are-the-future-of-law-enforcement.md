Title: Five Eyes alliance’s top cop says techies are the future of law enforcement
Date: 2022-06-09T05:06:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-09-five-eyes-alliances-top-cop-says-techies-are-the-future-of-law-enforcement

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/09/five_eyes_chair_tech_talk/){:target="_blank" rel="noopener"}

> Crims have weaponized tech and certain States let them launder the proceeds Australian Federal Police (AFP) commissioner Reece Kershaw has accused un-named nations of helping organized criminals to use technology to commit and launder the proceeds of crime, and called for international collaboration to developer technologies that counter the threats that behaviour creates.... [...]
