Title: U.S. Water Utilities Prime Cyberattack Target, Experts
Date: 2022-06-10T13:27:12+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Critical Infrastructure;Government;IoT
Slug: 2022-06-10-us-water-utilities-prime-cyberattack-target-experts

[Source](https://threatpost.com/water-cyberattack-target/179935/){:target="_blank" rel="noopener"}

> Environmentalists and policymakers warn water treatment plants are ripe for attack. [...]
