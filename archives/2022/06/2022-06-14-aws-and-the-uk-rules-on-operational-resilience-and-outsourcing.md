Title: AWS and the UK rules on operational resilience and outsourcing
Date: 2022-06-14T19:20:31+00:00
Author: Arvind Kannan
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS Financial Services Guide;Bank of England;FCA;operational resilience;Outsourcing Guidelines;PRA;Security Blog;UK;United Kingdom
Slug: 2022-06-14-aws-and-the-uk-rules-on-operational-resilience-and-outsourcing

[Source](https://aws.amazon.com/blogs/security/aws-and-the-uk-rules-on-operational-resilience-and-outsourcing/){:target="_blank" rel="noopener"}

> Financial institutions across the globe use Amazon Web Services (AWS) to transform the way they do business. Regulations continue to evolve in this space, and we’re working hard to help customers proactively respond to new rules and guidelines. In many cases, the AWS Cloud makes it simpler than ever before to assist customers with their compliance [...]
