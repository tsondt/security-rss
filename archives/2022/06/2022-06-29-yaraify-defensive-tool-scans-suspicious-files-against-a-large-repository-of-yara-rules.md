Title: YARAify: Defensive tool scans suspicious files against a large repository of YARA rules
Date: 2022-06-29T11:03:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-29-yaraify-defensive-tool-scans-suspicious-files-against-a-large-repository-of-yara-rules

[Source](https://portswigger.net/daily-swig/yaraify-defensive-tool-scans-suspicious-files-against-a-large-repository-of-yara-rules){:target="_blank" rel="noopener"}

> Team behind Abuse.ch and ThreatFox launch new hub for scanning and hunting files using YARA [...]
