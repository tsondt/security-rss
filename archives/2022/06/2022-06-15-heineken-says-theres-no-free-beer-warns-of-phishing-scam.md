Title: Heineken says there’s no free beer, warns of phishing scam
Date: 2022-06-15T19:24:43+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-15-heineken-says-theres-no-free-beer-warns-of-phishing-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/15/heineken_phishing_scam/){:target="_blank" rel="noopener"}

> WhatsApp messages possibly the worst Father's Day present in the world There's no such thing as free beer for Father's Day — at least not from Heineken. The brewing giant confirmed that a contest circulating on WhatsApp, which promises a chance to win one of 5,000 coolers full of green-bottled lager, is a frothy fraud.... [...]
