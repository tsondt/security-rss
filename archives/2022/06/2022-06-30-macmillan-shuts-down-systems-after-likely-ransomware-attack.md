Title: Macmillan shuts down systems after likely ransomware attack
Date: 2022-06-30T14:04:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-30-macmillan-shuts-down-systems-after-likely-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/macmillan-shuts-down-systems-after-likely-ransomware-attack/){:target="_blank" rel="noopener"}

> Publishing giant Macmillan was forced to shut down their network and offices while recovering from a security incident that appears to be a ransomware attack. [...]
