Title: Ransomware market evolution results in fewer variants, but rise in off-the-shelf cybercrime kits continues
Date: 2022-06-28T12:26:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-28-ransomware-market-evolution-results-in-fewer-variants-but-rise-in-off-the-shelf-cybercrime-kits-continues

[Source](https://portswigger.net/daily-swig/ransomware-market-evolution-results-in-fewer-variants-but-rise-in-off-the-shelf-cybercrime-kits-continues){:target="_blank" rel="noopener"}

> RaaS model continues to be adopted by criminals looking to maximize their ROI, new study indicates [...]
