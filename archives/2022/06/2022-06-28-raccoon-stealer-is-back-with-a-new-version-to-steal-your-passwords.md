Title: Raccoon Stealer is back with a new version to steal your passwords
Date: 2022-06-28T09:39:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-28-raccoon-stealer-is-back-with-a-new-version-to-steal-your-passwords

[Source](https://www.bleepingcomputer.com/news/security/raccoon-stealer-is-back-with-a-new-version-to-steal-your-passwords/){:target="_blank" rel="noopener"}

> The Raccoon Stealer malware is back with a second major version circulating on cybercrime forums, offering hackers elevated password-stealing functionality and upgraded operational capacity. [...]
