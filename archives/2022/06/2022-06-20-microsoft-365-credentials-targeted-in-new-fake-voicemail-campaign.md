Title: Microsoft 365 credentials targeted in new fake voicemail campaign
Date: 2022-06-20T10:06:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-20-microsoft-365-credentials-targeted-in-new-fake-voicemail-campaign

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-credentials-targeted-in-new-fake-voicemail-campaign/){:target="_blank" rel="noopener"}

> A new phishing campaign has been targeting U.S. organizations in the military, security software, manufacturing supply chain, healthcare and pharmaceutical sectors to steal Microsoft Office 365 and Outlook credentials. [...]
