Title: NSO claims 'more than 5' EU states use Pegasus spyware
Date: 2022-06-24T06:22:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-24-nso-claims-more-than-5-eu-states-use-pegasus-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/24/nso_customers_eu_pegasus/){:target="_blank" rel="noopener"}

> And it's like, what... 12, 13,000 total targets a year max, exec says NSO Group told European lawmakers this week that "under 50" customers use its notorious Pegasus spyware, though these customers include "more than five" European Union member states.... [...]
