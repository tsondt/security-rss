Title: FBI, CISA: Don't get caught in Karakurt's extortion web
Date: 2022-06-03T00:01:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-03-fbi-cisa-dont-get-caught-in-karakurts-extortion-web

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/03/fbi_cisa_warn_karakurt_extortion/){:target="_blank" rel="noopener"}

> Is this gang some sort of Conti side hustle? The answer may be yes The Feds have warned organizations about a lesser-known extortion gang Karakurt, which demands ransoms as high as $13 million and, some cybersecurity folks say, may be linked to the notorious Conti crew.... [...]
