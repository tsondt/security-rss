Title: Apple blocked 1.6 millions apps from defrauding users in 2021
Date: 2022-06-04T11:05:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2022-06-04-apple-blocked-16-millions-apps-from-defrauding-users-in-2021

[Source](https://www.bleepingcomputer.com/news/security/apple-blocked-16-millions-apps-from-defrauding-users-in-2021/){:target="_blank" rel="noopener"}

> Apple said this week that it blocked more than 343,000 iOS apps were blocked by the App Store App Review team for privacy violations last year, while another 157,000 were rejected for attempting to mislead or spamming iOS users. [...]
