Title: Introducing new Cloud Armor features including rate limiting, adaptive protection, and bot defense
Date: 2022-06-28T16:00:00+00:00
Author: Navya Dwarakanath
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-06-28-introducing-new-cloud-armor-features-including-rate-limiting-adaptive-protection-and-bot-defense

[Source](https://cloud.google.com/blog/products/identity-security/announcing-new-cloud-armor-rate-limiting-adaptive-protection-and-bot-defense/){:target="_blank" rel="noopener"}

> As cyberattacks grow in complexity and intensity against cloud customers, they need their cloud providers to play an even more active role in the resiliency of their web applications and APIs. Attacks have evolved from isolated DDoS attempts to far more comprehensive and coordinated techniques, including volumetric flood DDoS attacks, bot attacks, and API abuse. Google Cloud Armor can help our customers counter these growing security threats to web-applications and services by empowering defenders to deploy a defense-in-depth strategy. Today, we are proud to announce the General Availability of new capabilities in Cloud Armor that can greatly improve the security, [...]
