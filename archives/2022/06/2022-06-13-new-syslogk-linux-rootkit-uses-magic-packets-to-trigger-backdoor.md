Title: New Syslogk Linux rootkit uses magic packets to trigger backdoor
Date: 2022-06-13T11:13:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-13-new-syslogk-linux-rootkit-uses-magic-packets-to-trigger-backdoor

[Source](https://www.bleepingcomputer.com/news/security/new-syslogk-linux-rootkit-uses-magic-packets-to-trigger-backdoor/){:target="_blank" rel="noopener"}

> A new rootkit malware named 'Syslogk' has been spotted in the wild, and it features advanced process and file hiding techniques that make detection highly unlikely. [...]
