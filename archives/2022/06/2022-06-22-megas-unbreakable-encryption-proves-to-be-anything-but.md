Title: Mega's unbreakable encryption proves to be anything but
Date: 2022-06-22T20:58:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-06-22-megas-unbreakable-encryption-proves-to-be-anything-but

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/22/megas_encryption_broken/){:target="_blank" rel="noopener"}

> Boffins devise five attacks to expose private files Mega, the New Zealand-based file-sharing biz co-founded a decade ago by Kim Dotcom, promotes its "privacy by design" and user-controlled encryption keys to claim that data stored on Mega's servers can only be accessed by customers, even if its main system is taken over by law enforcement or others.... [...]
