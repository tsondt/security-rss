Title: Elasticsearch server with no password or encryption leaks a million records
Date: 2022-06-16T08:13:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-16-elasticsearch-server-with-no-password-or-encryption-leaks-a-million-records

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/16/storehub_data_leak/){:target="_blank" rel="noopener"}

> POS and online ordering vendor StoreHub offered free Asian info takeaways Researchers at security product recommendation service Safety Detectives claim they’ve found almost a million customer records wide open on an Elasticsearch server run by Malaysian point-of-sale software vendor StoreHub.... [...]
