Title: Microsoft Exchange bug abused to hack building automation systems
Date: 2022-06-27T11:39:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-27-microsoft-exchange-bug-abused-to-hack-building-automation-systems

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-bug-abused-to-hack-building-automation-systems/){:target="_blank" rel="noopener"}

> A Chinese-speaking threat actor has hacked into the building automation systems (used to control HVAC, fire, and security functions) of several Asian organizations to backdoor their networks and gain access to more secured areas in their networks. [...]
