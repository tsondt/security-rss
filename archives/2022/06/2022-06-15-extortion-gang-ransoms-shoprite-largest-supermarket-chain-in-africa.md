Title: Extortion gang ransoms Shoprite, largest supermarket chain in Africa
Date: 2022-06-15T12:28:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2022-06-15-extortion-gang-ransoms-shoprite-largest-supermarket-chain-in-africa

[Source](https://www.bleepingcomputer.com/news/security/extortion-gang-ransoms-shoprite-largest-supermarket-chain-in-africa/){:target="_blank" rel="noopener"}

> Shoprite Holdings, Africa's largest supermarket chain that operates almost three thousand stores across twelve countries in the continent, has been hit by a ransomware attack. [...]
