Title: Google: How we tackled this iPhone, Android spyware
Date: 2022-06-24T10:46:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-24-google-how-we-tackled-this-iphone-android-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/24/spyware_iphones_android_isp/){:target="_blank" rel="noopener"}

> Watching people's every move and collecting their info – not on our watch, says web ads giant Spyware developed by Italian firm RCS Labs was used to target cellphones in Italy and Kazakhstan — in some cases with an assist from the victims' cellular network providers, according to Google's Threat Analysis Group (TAG).... [...]
