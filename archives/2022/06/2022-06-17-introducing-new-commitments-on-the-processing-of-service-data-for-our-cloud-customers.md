Title: Introducing new commitments on the processing of service data for our cloud customers
Date: 2022-06-17T10:00:00+00:00
Author: Marc Crandall
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-06-17-introducing-new-commitments-on-the-processing-of-service-data-for-our-cloud-customers

[Source](https://cloud.google.com/blog/products/identity-security/introducing-new-commitments-on-the-processing-of-service-data-for-our-cloud-customers/){:target="_blank" rel="noopener"}

> At Google, we engage regularly with customers, regulators, policymakers, and other stakeholders to provide transparency into our operations, policies, and practices and to further strengthen our commitment to privacy compliance. One such engagement is our ongoing work with the Dutch government regarding its Data Protection Impact Assessment (DPIA) of Google Workspace and Workspace for Education. As a result of that engagement, today Google is announcing our intention to offer new contractual privacy commitments for service data 1 that align with the commitments we offer for customer data. 2 Once those new commitments become generally available, we will process service data [...]
