Title: Critical PHP flaw exposes QNAP NAS devices to RCE attacks
Date: 2022-06-22T06:20:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-22-critical-php-flaw-exposes-qnap-nas-devices-to-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/critical-php-flaw-exposes-qnap-nas-devices-to-rce-attacks/){:target="_blank" rel="noopener"}

> QNAP has warned customers today that many of its Network Attached Storage (NAS) devices are vulnerable to attacks that would exploit a three-year-old critical PHP vulnerability allowing remote code execution. [...]
