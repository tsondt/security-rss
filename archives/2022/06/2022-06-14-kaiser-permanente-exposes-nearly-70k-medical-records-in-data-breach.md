Title: Kaiser Permanente Exposes Nearly 70K Medical Records in Data Breach
Date: 2022-06-14T11:08:27+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Privacy
Slug: 2022-06-14-kaiser-permanente-exposes-nearly-70k-medical-records-in-data-breach

[Source](https://threatpost.com/kaiser-permanente-breach/179949/){:target="_blank" rel="noopener"}

> Attackers gained access to private account details through an email compromise incident that occurred in April. [...]
