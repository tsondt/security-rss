Title: Halfords suffers a puncture in the customer details department
Date: 2022-06-23T08:30:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-06-23-halfords-suffers-a-puncture-in-the-customer-details-department

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/23/halfords_data_leak_vulnerability/){:target="_blank" rel="noopener"}

> I like driving in my car, hope my data's not gone far UK automobile service and parts seller Halfords has shared the details of its customers a little too freely, according to the findings of a security researcher.... [...]
