Title: Singapore promises 'brutal and unrelentingly hard' action on dodgy crypto players
Date: 2022-06-27T00:30:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-06-27-singapore-promises-brutal-and-unrelentingly-hard-action-on-dodgy-crypto-players

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/27/singapore_cbdc_crypto_policy/){:target="_blank" rel="noopener"}

> But welcomes fast cross-border payments in central bank digital currencies In the same week that it welcomed the launch of a local center of excellence focused on crypto-inspired central bank digital currencies, Singapore's Monetary Authority (MAS) has warned crypto cowboys they face a rough ride in the island nation.... [...]
