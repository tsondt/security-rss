Title: Wickr for Government achieves FedRAMP Ready designation
Date: 2022-06-20T17:21:08+00:00
Author: Anne Grahn
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS security;data privacy;Data protection;Encryption;FedRAMP;Public Sector;Security Blog
Slug: 2022-06-20-wickr-for-government-achieves-fedramp-ready-designation

[Source](https://aws.amazon.com/blogs/security/wickr-for-government-achieves-fedramp-ready-designation/){:target="_blank" rel="noopener"}

> AWS is pleased to announce that Wickr for Government (WickrGov) has achieved Federal Risk and Authorization Management Program (FedRAMP) Ready status at the Moderate Impact Level, and is actively working toward FedRAMP Authorized status. FedRAMP is a US government-wide program that promotes the adoption of secure cloud services across the federal government by providing a [...]
