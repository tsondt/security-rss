Title: Watch out for phishing emails that inject spyware trio
Date: 2022-06-01T10:02:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-01-watch-out-for-phishing-emails-that-inject-spyware-trio

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/01/phishing-rat-bitrat-fortinet/){:target="_blank" rel="noopener"}

> You wait for one infection and then three come along at once An emailed report seemingly about a payment will, when opened in Excel on a Windows system, attempt to inject three pieces of file-less malware that steal sensitive information.... [...]
