Title: Start using Modern Auth now for Exchange Online
Date: 2022-06-29T22:59:50+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-29-start-using-modern-auth-now-for-exchange-online

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/29/cisa-microsoft-modern-auth/){:target="_blank" rel="noopener"}

> Before Microsoft shutters basic logins in a few months The US government is pushing federal agencies and private corporations to adopt the Modern Authentication method in Exchange Online before Microsoft starts shutting down Basic Authentication from the first day of October.... [...]
