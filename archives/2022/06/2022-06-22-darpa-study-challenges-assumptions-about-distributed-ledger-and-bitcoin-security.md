Title: DARPA study challenges assumptions about distributed ledger (and Bitcoin) security
Date: 2022-06-22T13:30:12+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-06-22-darpa-study-challenges-assumptions-about-distributed-ledger-and-bitcoin-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/22/research_challenges_assumptions_about_distributed/){:target="_blank" rel="noopener"}

> Blockchain not as decentralised as many assume, finds Pentagon sponsored research US government sponsored research is casting new light on the security of blockchain technology, including the assertion that a subset of a distributed ledger's participants can gain control over the entire system.... [...]
