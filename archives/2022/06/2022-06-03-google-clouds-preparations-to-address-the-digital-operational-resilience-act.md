Title: Google Cloud’s preparations to address the Digital Operational Resilience Act
Date: 2022-06-03T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Financial Services;Google Cloud;Identity & Security
Slug: 2022-06-03-google-clouds-preparations-to-address-the-digital-operational-resilience-act

[Source](https://cloud.google.com/blog/products/identity-security/what-google-cloud-is-doing-to-prepare-for-dora/){:target="_blank" rel="noopener"}

> European legislators came to an inter-institutional agreement on the Digital Operational Resilience Act (DORA) in May 2022. This is a major milestone in the adoption of new rules designed to ensure financial entities can withstand, respond to and recover from all types of ICT-related disruptions and threats, including increasingly sophisticated cyberattacks. DORA will harmonize how financial entities must report cybersecurity incidents, test their digital operational resilience, and manage ICT third-party risk across the financial services sector and European Union (EU) member states. In addition to establishing clear expectations for the role of ICT providers, DORA will also allow financial regulators [...]
