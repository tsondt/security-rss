Title: Cybercriminals Expand Attack Radius and Ransomware Pain Points
Date: 2022-06-02T13:08:55+00:00
Author: Threatpost
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-06-02-cybercriminals-expand-attack-radius-and-ransomware-pain-points

[Source](https://threatpost.com/criminals-expand-attack-radius/179832/){:target="_blank" rel="noopener"}

> Melissa Bischoping, security researcher with Tanium and Infosec Insiders columnist, urges firms to consider the upstream and downstream impact of "triple extortion" ransomware attacks. [...]
