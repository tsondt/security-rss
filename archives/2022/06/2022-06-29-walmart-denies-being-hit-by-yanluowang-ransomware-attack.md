Title: Walmart denies being hit by Yanluowang ransomware attack
Date: 2022-06-29T17:23:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-29-walmart-denies-being-hit-by-yanluowang-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/walmart-denies-being-hit-by-yanluowang-ransomware-attack/){:target="_blank" rel="noopener"}

> American retailer Walmart has denied being hit with a ransomware attack by the Yanluowang gang after the hackers claimed to encrypt thousands of computers. [...]
