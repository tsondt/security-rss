Title: Cloudflare says it thwarted record-breaking HTTPS DDoS flood
Date: 2022-06-14T23:44:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-14-cloudflare-says-it-thwarted-record-breaking-https-ddos-flood

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/14/cloudflare-record-ddos-attack/){:target="_blank" rel="noopener"}

> 26m requests a second? Not legit traffic, not even Bill Gates doing $1m giveaways could manage that Cloudflare said it this month staved off another record-breaking HTTPS-based distributed denial-of-service attack, this one significantly larger than the previous largest DDoS attack that occurred only two months ago.... [...]
