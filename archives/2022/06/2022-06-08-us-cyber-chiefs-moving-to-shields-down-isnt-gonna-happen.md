Title: US cyber chiefs: Moving to Shields Down isn't gonna happen
Date: 2022-06-08T06:58:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-08-us-cyber-chiefs-moving-to-shields-down-isnt-gonna-happen

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/08/us_shields_down/){:target="_blank" rel="noopener"}

> Promises new alert notices but warn 'we can sometimes predict thunderstorms but not lightning strikes' RSA Conference A heightened state of defensive cyber security posture is the new normal, according to federal cyber security chiefs speaking at the RSA Conference on Tuesday. This requires greater transparency and threat intel sharing between the government and private sector, they added.... [...]
