Title: Beijing-backed attackers use ransomware as a decoy while they conduct espionage
Date: 2022-06-24T07:04:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-06-24-beijing-backed-attackers-use-ransomware-as-a-decoy-while-they-conduct-espionage

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/24/ransomware_as_espionage_distraction/){:target="_blank" rel="noopener"}

> They're not lying when they say 'We stole your data' – the lie is about which data they lifted A state-sponsored Chinese threat actor has used ransomware as a distraction to help it conduct electronic espionage, according to security software vendor Secureworks.... [...]
