Title: More than $100m in cryptocurrency stolen from blockchain biz
Date: 2022-06-24T21:46:38+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-06-24-more-than-100m-in-cryptocurrency-stolen-from-blockchain-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/24/harmony_100m_cryptocurrency_theft/){:target="_blank" rel="noopener"}

> 'A humbling and unfortunate reminder' that monsters lurk under bridges Blockchain venture Harmony offers bridge services for transferring crypto coins across different blockchains, but something has gone badly wrong.... [...]
