Title: Cisco Secure Email bug can let attackers bypass authentication
Date: 2022-06-15T14:24:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-15-cisco-secure-email-bug-can-let-attackers-bypass-authentication

[Source](https://www.bleepingcomputer.com/news/security/cisco-secure-email-bug-can-let-attackers-bypass-authentication/){:target="_blank" rel="noopener"}

> Cisco notified customers this week to patch a critical vulnerability that could allow attackers to bypass authentication and login into the web management interface of Cisco email gateway appliances with non-default configurations. [...]
