Title: Microsoft Releases Workaround for ‘One-Click’ 0Day Under Active Attack
Date: 2022-06-01T10:38:37+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Vulnerabilities
Slug: 2022-06-01-microsoft-releases-workaround-for-one-click-0day-under-active-attack

[Source](https://threatpost.com/microsoft-workaround-0day-attack/179776/){:target="_blank" rel="noopener"}

> Threat actors already are exploiting vulnerability, dubbed ‘Follina’ and originally identified back in April, to target organizations in Russia and Tibet, researchers said. [...]
