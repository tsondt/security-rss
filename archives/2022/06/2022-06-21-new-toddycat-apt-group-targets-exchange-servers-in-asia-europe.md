Title: New ToddyCat APT group targets Exchange servers in Asia, Europe
Date: 2022-06-21T07:46:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-21-new-toddycat-apt-group-targets-exchange-servers-in-asia-europe

[Source](https://www.bleepingcomputer.com/news/security/new-toddycat-apt-group-targets-exchange-servers-in-asia-europe/){:target="_blank" rel="noopener"}

> An advanced persistent threat (APT) group dubbed ToddyCat has been targeting Microsoft Exchange servers throughout Asia and Europe for more than a year, since at least December 2020. [...]
