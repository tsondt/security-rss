Title: FBI warning: Crooks are using deepfake videos in interviews for remote gigs
Date: 2022-06-29T06:16:49+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-06-29-fbi-warning-crooks-are-using-deepfake-videos-in-interviews-for-remote-gigs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/29/fbi_deepfake_job_applicant_warning/){:target="_blank" rel="noopener"}

> Yes. Of course I human. Why asking? Also, when you give passwords to database? The US FBI issued a warning on Tuesday that it was has received increasing numbers of complaints relating to the use of deepfake videos during interviews for tech jobs that involve access to sensitive systems and information.... [...]
