Title: Bluetooth Signals Can Be Used to Track Smartphones, Say Researchers
Date: 2022-06-13T12:36:20+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Hacks;Privacy
Slug: 2022-06-13-bluetooth-signals-can-be-used-to-track-smartphones-say-researchers

[Source](https://threatpost.com/bluetooth-signals-track-smartphones/179937/){:target="_blank" rel="noopener"}

> Researchers demonstrated a possible way to track individuals via Bluetooth signals. [...]
