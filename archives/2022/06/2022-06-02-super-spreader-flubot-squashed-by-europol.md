Title: Super-spreader FluBot squashed by Europol
Date: 2022-06-02T08:03:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-02-super-spreader-flubot-squashed-by-europol

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/02/flubot_malware_squashed/){:target="_blank" rel="noopener"}

> Your package is delayed. Click this innocent-looking link to reschedule FluBot, the super-spreader Android malware that infected tens of thousands of phones globally, has been reportedly squashed by an international law enforcement operation.... [...]
