Title: Threat and risk specialists signal post-COVID conference season is back on
Date: 2022-06-10T19:25:51+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-06-10-threat-and-risk-specialists-signal-post-covid-conference-season-is-back-on

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/10/rsa-conference-covid/){:target="_blank" rel="noopener"}

> Well, we'll see in a week or so RSA Conference For the first time in over two years the streets of San Francisco have been filled by attendees at the RSA Conference and it seems that the days of physical cons are back on.... [...]
