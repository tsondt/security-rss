Title: Announcing general availability of Confidential GKE Nodes
Date: 2022-06-16T16:00:00+00:00
Author: Joanna Young
Category: GCP Security
Tags: Containers & Kubernetes;Google Cloud;Compute;Identity & Security
Slug: 2022-06-16-announcing-general-availability-of-confidential-gke-nodes

[Source](https://cloud.google.com/blog/products/identity-security/announcing-general-availability-of-confidential-gke-nodes/){:target="_blank" rel="noopener"}

> Today, we’re excited to announce the general availability of Confidential GKE Nodes. Many organizations have made Google Kubernetes Engine (GKE) the foundation of their modern application architectures. While the benefits of containers and Kubernetes can outweigh that of traditional architectures, moving to and running those apps in the cloud often entails careful planning to minimize risk and potential data exposure. To help increase security of your GKE clusters, Confidential GKE Nodes can be used. Part of the growing Confidential Computing product portfolio, Confidential GKE Nodes leverage hardware to make sure your data is encrypted in memory. The GKE workloads you [...]
