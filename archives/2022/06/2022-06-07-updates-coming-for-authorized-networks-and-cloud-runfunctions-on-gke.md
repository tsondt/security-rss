Title: Updates coming for Authorized Networks and Cloud Run/Functions on GKE
Date: 2022-06-07T23:00:00+00:00
Author: Greg Castle
Category: GCP Security
Tags: GKE;Containers & Kubernetes;Identity & Security
Slug: 2022-06-07-updates-coming-for-authorized-networks-and-cloud-runfunctions-on-gke

[Source](https://cloud.google.com/blog/products/identity-security/updates-coming-for-authorized-networks-and-cloud-runfunctions-on-gke/){:target="_blank" rel="noopener"}

> We recently received helpful information through the Vulnerability Rewards Program for Authorized Networks and Cloud Run/Functions on Google Kubernetes Engine (GKE). Based on that information, we updated our product documentation and prioritized a plan to make engineering changes to GKE to restrict access to only GKE-related services. Those changes will roll out automatically to over 99% of our GKE customers by late August, and we will proactively reach out to the remaining customers to work on migration issues together. Our existing firewall rules allow the Kubernetes API server’s IP address to be reachable from the Cloud Run and Cloud Functions [...]
