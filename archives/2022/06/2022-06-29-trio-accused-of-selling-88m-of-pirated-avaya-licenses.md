Title: Trio accused of selling $88m of pirated Avaya licenses
Date: 2022-06-29T03:31:49+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-06-29-trio-accused-of-selling-88m-of-pirated-avaya-licenses

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/29/avaya_piracy_doj_fbi/){:target="_blank" rel="noopener"}

> Rogue insider generated keys, resold them to blow the cash on gold, crypto, and more, prosecutors say Three people accused of selling pirate software licenses worth more than $88 million have been charged with fraud.... [...]
