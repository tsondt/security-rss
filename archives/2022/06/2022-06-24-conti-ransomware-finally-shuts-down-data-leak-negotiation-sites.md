Title: Conti ransomware finally shuts down data leak, negotiation sites
Date: 2022-06-24T10:35:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-24-conti-ransomware-finally-shuts-down-data-leak-negotiation-sites

[Source](https://www.bleepingcomputer.com/news/security/conti-ransomware-finally-shuts-down-data-leak-negotiation-sites/){:target="_blank" rel="noopener"}

> The Conti ransomware operation has finally shut down its last public-facing infrastructure, consisting of two Tor servers used to leak data and negotiate with victims, closing the final chapter of the notorious cybercrime brand. [...]
