Title: Ransomware gang creates site for employees to search for their stolen data
Date: 2022-06-14T19:03:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-06-14-ransomware-gang-creates-site-for-employees-to-search-for-their-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-creates-site-for-employees-to-search-for-their-stolen-data/){:target="_blank" rel="noopener"}

> The ALPHV ransomware gang, aka BlackCat, has brought extortion to a new level by creating a dedicated website that allows the customers and employees of their victim to check if their data was stolen in an attack [...]
