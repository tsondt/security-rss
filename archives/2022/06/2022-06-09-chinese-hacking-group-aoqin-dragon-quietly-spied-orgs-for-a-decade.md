Title: Chinese hacking group Aoqin Dragon quietly spied orgs for a decade
Date: 2022-06-09T07:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-09-chinese-hacking-group-aoqin-dragon-quietly-spied-orgs-for-a-decade

[Source](https://www.bleepingcomputer.com/news/security/chinese-hacking-group-aoqin-dragon-quietly-spied-orgs-for-a-decade/){:target="_blank" rel="noopener"}

> A previously unknown Chinese-speaking threat actor has been uncovered by threat analysts SentinelLabs who were able to link it to malicious activity going as far back as 2013. [...]
