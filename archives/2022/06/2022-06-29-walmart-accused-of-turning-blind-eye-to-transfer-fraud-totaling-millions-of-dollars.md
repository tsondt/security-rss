Title: Walmart accused of turning blind eye to transfer fraud totaling millions of dollars
Date: 2022-06-29T00:36:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-29-walmart-accused-of-turning-blind-eye-to-transfer-fraud-totaling-millions-of-dollars

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/29/walmart_ftc_money_transfer/){:target="_blank" rel="noopener"}

> Store giant brands watchdog's lawsuit 'factually misguided, legally flawed' The FTC has sued Walmart, claiming it turned a blind eye to fraudsters using its money transfer services to con folks out of "hundreds of millions of dollars."... [...]
