Title: Chromium browsers vulnerable to dangling markup injection
Date: 2022-06-30T10:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-30-chromium-browsers-vulnerable-to-dangling-markup-injection

[Source](https://portswigger.net/daily-swig/chromium-browsers-vulnerable-to-dangling-markup-injection){:target="_blank" rel="noopener"}

> Fixed bug could allow attackers to extract sensitive information [...]
