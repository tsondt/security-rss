Title: Ransomware attacks need less than four days to encrypt systems
Date: 2022-06-01T07:32:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-01-ransomware-attacks-need-less-than-four-days-to-encrypt-systems

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attacks-need-less-than-four-days-to-encrypt-systems/){:target="_blank" rel="noopener"}

> The duration of ransomware attacks in 2021 averaged 92.5 hours, measured from initial network access to payload deployment. In 2020, ransomware actors spent an average of 230 hours to complete their attacks and 1637.6 hours in 2019. [...]
