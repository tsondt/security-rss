Title: QBot now pushes Black Basta ransomware in bot-powered attacks
Date: 2022-06-06T17:01:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-06-qbot-now-pushes-black-basta-ransomware-in-bot-powered-attacks

[Source](https://www.bleepingcomputer.com/news/security/qbot-now-pushes-black-basta-ransomware-in-bot-powered-attacks/){:target="_blank" rel="noopener"}

> The Black Basta ransomware gang has partnered with the QBot malware operation to gain spread laterally through hacked corporate environments. [...]
