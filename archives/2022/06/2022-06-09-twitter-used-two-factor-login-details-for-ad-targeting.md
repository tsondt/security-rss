Title: Twitter Used Two-Factor Login Details for Ad Targeting
Date: 2022-06-09T14:30:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-06-09-twitter-used-two-factor-login-details-for-ad-targeting

[Source](https://www.schneier.com/blog/archives/2022/06/twitter-used-two-factor-login-details-for-ad-targeting.html){:target="_blank" rel="noopener"}

> Twitter was fined $150 million for using phone numbers and email addresses collected for two-factor authentication for ad targeting. [...]
