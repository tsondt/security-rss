Title: World Economic Forum wants a global map of online crime
Date: 2022-06-10T21:27:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-10-world-economic-forum-wants-a-global-map-of-online-crime

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/10/atlas_wef_rsa/){:target="_blank" rel="noopener"}

> Will cyber crimes shrug off Atlas Initiative? Objectively, yes RSA Conference An ambitious project spearheaded by the World Economic Forum (WEF) is working to develop a map of the cybercrime ecosystem using open source information.... [...]
