Title: How SLSA and SBOM can help healthcare's cybersecurity resiliency
Date: 2022-06-23T19:00:00+00:00
Author: Seth Rosenblatt
Category: GCP Security
Tags: Healthcare & Life Sciences;Google Cloud;Identity & Security
Slug: 2022-06-23-how-slsa-and-sbom-can-help-healthcares-cybersecurity-resiliency

[Source](https://cloud.google.com/blog/products/identity-security/how-slsa-and-sbom-can-help-healthcare-resiliency/){:target="_blank" rel="noopener"}

> Taking prescription medication at the direction of anyone other than a trained physician is very risky—and the same could be said for selecting technology used to run a hospital, to manage a drug manufacturing facility and, increasingly, to treat a patient for a medical condition. To pick the right medication, physicians need to carefully consider its ingredients, the therapeutic value they collectively provide, and the patient’s condition. Healthcare cybersecurity leaders similarly need to know what goes into the technology their organization's use to manage patient medical records, manufacture compound drugs, and treat patients in order to keep them safe from [...]
