Title: Single largest disclosure for vulnerabilities in industrial control security reveals 56 flaws
Date: 2022-06-21T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-21-single-largest-disclosure-for-vulnerabilities-in-industrial-control-security-reveals-56-flaws

[Source](https://portswigger.net/daily-swig/single-largest-disclosure-for-vulnerabilities-in-industrial-control-security-reveals-56-flaws){:target="_blank" rel="noopener"}

> Scores of security issues in industrial control systems unveiled [...]
