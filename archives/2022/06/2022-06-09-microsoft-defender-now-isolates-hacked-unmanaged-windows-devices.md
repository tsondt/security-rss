Title: Microsoft Defender now isolates hacked, unmanaged Windows devices
Date: 2022-06-09T12:35:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-06-09-microsoft-defender-now-isolates-hacked-unmanaged-windows-devices

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-now-isolates-hacked-unmanaged-windows-devices/){:target="_blank" rel="noopener"}

> Microsoft has announced a new feature for Microsoft Defender for Endpoint (MDE) to help organizations prevent attackers and malware from using compromised unmanaged devices to move laterally through the network. [...]
