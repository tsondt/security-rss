Title: Yandex CEO Arkady Volozh resigns after being added to EU sanctions list
Date: 2022-06-06T01:59:26+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-06-yandex-ceo-arkady-volozh-resigns-after-being-added-to-eu-sanctions-list

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/06/yandex_ceo_arkady_volozh_resigns/){:target="_blank" rel="noopener"}

> Russia's top tech CEO accused of material support to Moscow Arkady Volozh, CEO of Russia's biggest internet company Yandex, has resigned after being added to the European Union's list of individuals sanctioned as part of its response to the illegal invasion of Ukraine.... [...]
