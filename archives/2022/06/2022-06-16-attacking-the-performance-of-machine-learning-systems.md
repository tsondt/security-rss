Title: Attacking the Performance of Machine Learning Systems
Date: 2022-06-16T11:02:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cyberattack;machine learning
Slug: 2022-06-16-attacking-the-performance-of-machine-learning-systems

[Source](https://www.schneier.com/blog/archives/2022/06/attacking-the-performance-of-machine-learning-systems.html){:target="_blank" rel="noopener"}

> Interesting research: “ Sponge Examples: Energy-Latency Attacks on Neural Networks “: Abstract: The high energy costs of neural network training and inference led to the use of acceleration hardware such as GPUs and TPUs. While such devices enable us to train large-scale neural networks in datacenters and deploy them on edge devices, their designers’ focus so far is on average-case performance. In this work, we introduce a novel threat vector against neural networks whose energy consumption or decision latency are critical. We show how adversaries can exploit carefully-crafted sponge examples, which are inputs designed to maximise energy consumption and latency, [...]
