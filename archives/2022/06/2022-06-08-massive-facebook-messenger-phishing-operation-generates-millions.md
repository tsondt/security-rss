Title: Massive Facebook Messenger phishing operation generates millions
Date: 2022-06-08T14:54:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-08-massive-facebook-messenger-phishing-operation-generates-millions

[Source](https://www.bleepingcomputer.com/news/security/massive-facebook-messenger-phishing-operation-generates-millions/){:target="_blank" rel="noopener"}

> Researchers have uncovered a large-scale phishing operation that abused Facebook and Messenger to lure millions of users to phishing pages, tricking them into entering their account credentials and seeing advertisements. [...]
