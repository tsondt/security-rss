Title: Scammers Target NFT Discord Channel
Date: 2022-06-02T11:44:57+00:00
Author: Sagar Tiwari
Category: Threatpost
Tags: Hacks;Vulnerabilities
Slug: 2022-06-02-scammers-target-nft-discord-channel

[Source](https://threatpost.com/scammers-target-nft-discord-channel/179827/){:target="_blank" rel="noopener"}

> Hackers escalate phishing and scamming attacks to exploit popular Discord bot and persuade users to click on the malicious links. [...]
