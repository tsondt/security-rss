Title: What Counts as “Good Faith Security Research?”
Date: 2022-06-03T19:33:03+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;computer fraud and abuse act;Department of Justice;Deputy Attorney General Lisa O. Monaco;Digital Millennium Copyright Act;good faith security research;Orin Kerr;University of California Berkley;Van Buren v. United States
Slug: 2022-06-03-what-counts-as-good-faith-security-research

[Source](https://krebsonsecurity.com/2022/06/what-counts-as-good-faith-security-research/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DOJ) recently revised its policy on charging violations of the Computer Fraud and Abuse Act (CFAA), a 1986 law that remains the primary statute by which federal prosecutors pursue cybercrime cases. The new guidelines state that prosecutors should avoid charging security researchers who operate in “good faith” when finding and reporting vulnerabilities. But legal experts continue to advise researchers to proceed with caution, noting the new guidelines can’t be used as a defense in court, nor are they any kind of shield against civil prosecution. In a statemen t about the changes, Deputy Attorney General [...]
