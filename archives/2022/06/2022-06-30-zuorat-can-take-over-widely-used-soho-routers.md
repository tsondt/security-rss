Title: ZuoRAT Can Take Over Widely Used SOHO Routers
Date: 2022-06-30T17:20:30+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2022-06-30-zuorat-can-take-over-widely-used-soho-routers

[Source](https://threatpost.com/zuorat-soho-routers/180113/){:target="_blank" rel="noopener"}

> Devices from Cisco, Netgear and others at risk from the multi-stage malware, which has been active since April 2020 and shows the work of a sophisticated threat actor. [...]
