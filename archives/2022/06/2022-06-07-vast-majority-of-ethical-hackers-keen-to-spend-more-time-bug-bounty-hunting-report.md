Title: Vast majority of ethical hackers keen to spend more time bug bounty hunting – report
Date: 2022-06-07T10:25:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-07-vast-majority-of-ethical-hackers-keen-to-spend-more-time-bug-bounty-hunting-report

[Source](https://portswigger.net/daily-swig/vast-majority-of-ethical-hackers-keen-to-spend-more-time-bug-bounty-hunting-report){:target="_blank" rel="noopener"}

> Bounties and greater independence are prime motives for hackers hoping to do more freelance bug hunting [...]
