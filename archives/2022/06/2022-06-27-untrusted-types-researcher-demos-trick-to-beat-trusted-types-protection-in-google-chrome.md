Title: Untrusted types: Researcher demos trick to beat Trusted Types protection in Google Chrome
Date: 2022-06-27T15:25:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-27-untrusted-types-researcher-demos-trick-to-beat-trusted-types-protection-in-google-chrome

[Source](https://portswigger.net/daily-swig/untrusted-types-researcher-demos-trick-to-beat-trusted-types-protection-in-google-chrome){:target="_blank" rel="noopener"}

> Flaws in protection mechanism leaves websites more exposed to DOM XSS-based attacks [...]
