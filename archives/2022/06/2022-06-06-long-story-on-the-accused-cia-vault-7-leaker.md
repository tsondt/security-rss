Title: Long Story on the Accused CIA Vault 7 Leaker
Date: 2022-06-06T15:33:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;CIA;leaks;WikiLeaks
Slug: 2022-06-06-long-story-on-the-accused-cia-vault-7-leaker

[Source](https://www.schneier.com/blog/archives/2022/06/long-story-on-the-accused-cia-vault-7-leaker.html){:target="_blank" rel="noopener"}

> Long article about Joshua Schulte, the accused leaker of the WikiLeaks Vault 7 and Vault 8 CIA data. Well worth reading. [...]
