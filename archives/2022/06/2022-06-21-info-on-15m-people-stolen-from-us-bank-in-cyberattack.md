Title: Info on 1.5m people stolen from US bank in cyberattack
Date: 2022-06-21T20:53:56+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-21-info-on-15m-people-stolen-from-us-bank-in-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/21/flagstar_bank_breached_ssn/){:target="_blank" rel="noopener"}

> Time to rethink that cybersecurity strategy? A US bank has said at least the names and social security numbers of more than 1.5 million of its customers were stolen from its computers in December.... [...]
