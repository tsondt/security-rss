Title: Confluence servers hacked to deploy AvosLocker, Cerber2021 ransomware
Date: 2022-06-11T10:31:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-11-confluence-servers-hacked-to-deploy-avoslocker-cerber2021-ransomware

[Source](https://www.bleepingcomputer.com/news/security/confluence-servers-hacked-to-deploy-avoslocker-cerber2021-ransomware/){:target="_blank" rel="noopener"}

> Ransomware gangs are now targeting a recently patched and actively exploited remote code execution (RCE) vulnerability affecting Atlassian Confluence Server and Data Center instances for initial access to corporate networks. [...]
