Title: Malicious Messenger chatbots used to steal Facebook accounts
Date: 2022-06-28T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-28-malicious-messenger-chatbots-used-to-steal-facebook-accounts

[Source](https://www.bleepingcomputer.com/news/security/malicious-messenger-chatbots-used-to-steal-facebook-accounts/){:target="_blank" rel="noopener"}

> A new phishing attack is using Facebook Messenger chatbots to impersonate the company's support team and steal credentials used to manage Facebook pages. [...]
