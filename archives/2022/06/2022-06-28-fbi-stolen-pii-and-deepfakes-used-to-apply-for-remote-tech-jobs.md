Title: FBI: Stolen PII and deepfakes used to apply for remote tech jobs
Date: 2022-06-28T10:41:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-28-fbi-stolen-pii-and-deepfakes-used-to-apply-for-remote-tech-jobs

[Source](https://www.bleepingcomputer.com/news/security/fbi-stolen-pii-and-deepfakes-used-to-apply-for-remote-tech-jobs/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warns of an increase in complaints that cybercriminals are using Americans' stolen Personally Identifiable Information (PII) and deepfakes to apply for remote work positions. [...]
