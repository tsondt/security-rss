Title: Microsoft continues cyber security spending spree with Miburo buy
Date: 2022-06-15T15:30:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-06-15-microsoft-continues-cyber-security-spending-spree-with-miburo-buy

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/15/miburo/){:target="_blank" rel="noopener"}

> Brains to be added to the Customer Security and Trust in defense against 'foreign adversaries' Microsoft has opened its wallet once more to pick up New York-based cyber-threat analyst Miburo.... [...]
