Title: Costa Rican government held up by ransomware … again
Date: 2022-06-06T03:46:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-06-06-costa-rican-government-held-up-by-ransomware-again

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/06/security_in_brief/){:target="_blank" rel="noopener"}

> Also US warns of voting machine flaws and Google pays out $100 million to Illinois In brief Last month the notorious Russian ransomware gang Conti threatened to overthrow Costa Rica's government if a ransom wasn't paid. This month, another band of extortionists has attacked the nation.... [...]
