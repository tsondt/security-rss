Title: Chinese hackers target script kiddies with info-stealer trojan
Date: 2022-06-22T14:28:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-22-chinese-hackers-target-script-kiddies-with-info-stealer-trojan

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-target-script-kiddies-with-info-stealer-trojan/){:target="_blank" rel="noopener"}

> Cybersecurity researchers have discovered a new campaign attributed to the Chinese "Tropic Trooper" hacking group, which employs a novel loader called Nimbda and a new variant of the Yahoyah trojan. [...]
