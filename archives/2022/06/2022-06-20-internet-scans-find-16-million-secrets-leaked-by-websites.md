Title: Internet scans find 1.6 million secrets leaked by websites
Date: 2022-06-20T14:11:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-20-internet-scans-find-16-million-secrets-leaked-by-websites

[Source](https://portswigger.net/daily-swig/internet-scans-find-1-6-million-secrets-leaked-by-websites){:target="_blank" rel="noopener"}

> Probe surfaces ‘alarmingly huge’ number of unredacted tokens and keys [...]
