Title: Carnival Cruises torpedoed by US states, agrees to pay $6m after waves of cyberattacks
Date: 2022-06-28T02:58:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-06-28-carnival-cruises-torpedoed-by-us-states-agrees-to-pay-6m-after-waves-of-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/28/carnival-cybersecurity-fines/){:target="_blank" rel="noopener"}

> Now those are some phishing boats Carnival Cruise Lines will cough up more than $6 million to end two separate lawsuits filed by 46 states in the US after sensitive, personal information on customers and employees was accessed in a string of cyberattacks.... [...]
