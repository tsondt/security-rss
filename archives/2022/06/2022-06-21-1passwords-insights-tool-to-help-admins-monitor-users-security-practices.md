Title: 1Password's Insights tool to help admins monitor users' security practices
Date: 2022-06-21T13:00:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-06-21-1passwords-insights-tool-to-help-admins-monitor-users-security-practices

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/21/1password_trots_out_insights_tool/){:target="_blank" rel="noopener"}

> Find the clown who choses 'password' as a password and make things right 1Password, the Toronto-based maker of the identically named password manager, is adding a security analysis and advice tool called Insights from 1Password to its business-oriented product.... [...]
