Title: Announcing general availability of Cloud Armor’s new edge security policies, and support for proxy load balancers
Date: 2022-06-28T16:00:00+00:00
Author: Gregory M. Lebovitz
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-06-28-announcing-general-availability-of-cloud-armors-new-edge-security-policies-and-support-for-proxy-load-balancers

[Source](https://cloud.google.com/blog/products/identity-security/cloud-armor-adds-more-edge-security-policies-proxy-load-balancers/){:target="_blank" rel="noopener"}

> Whether workloads are deployed in public clouds, on-premises, or other infrastructure providers, DDoS and Layer 7 attacks target all web applications, APIs, and services. That’s why Google Cloud continues to expand our scope of DDoS and web application firewall (WAF) protection for web applications, APIs, and services with Google Cloud Armor, so customers can defend internet-facing workloads no matter which architecture they choose. Today, we are announcing two major capabilities that expand Cloud Armor’s coverage to more types of workloads. First, we are launching edge security policies to help defend workloads using Cloud CDN, Media CDN, and Cloud Storage and [...]
