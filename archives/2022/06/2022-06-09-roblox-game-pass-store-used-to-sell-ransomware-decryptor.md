Title: Roblox Game Pass store used to sell ransomware decryptor
Date: 2022-06-09T15:29:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2022-06-09-roblox-game-pass-store-used-to-sell-ransomware-decryptor

[Source](https://www.bleepingcomputer.com/news/security/roblox-game-pass-store-used-to-sell-ransomware-decryptor/){:target="_blank" rel="noopener"}

> A new ransomware is taking the unusual approach of selling its decryptor on the Roblox gaming platform using the service's in-game Robux currency. [...]
