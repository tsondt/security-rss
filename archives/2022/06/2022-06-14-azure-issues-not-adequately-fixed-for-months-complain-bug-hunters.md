Title: Azure issues not adequately fixed for months, complain bug hunters
Date: 2022-06-14T13:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-14-azure-issues-not-adequately-fixed-for-months-complain-bug-hunters

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/14/security_azure_patch/){:target="_blank" rel="noopener"}

> Redmond kicks off Patch Tuesday with a months-old flaw fix Two security vendors – Orca Security and Tenable – have accused Microsoft of unnecessarily putting customers' data and cloud environments at risk by taking far too long to fix critical vulnerabilities in Azure.... [...]
