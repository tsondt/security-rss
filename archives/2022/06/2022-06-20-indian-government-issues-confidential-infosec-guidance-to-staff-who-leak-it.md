Title: Indian government issues confidential infosec guidance to staff – who leak it
Date: 2022-06-20T03:32:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-06-20-indian-government-issues-confidential-infosec-guidance-to-staff-who-leak-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/20/indian_government_infosec_guidance_leaks/){:target="_blank" rel="noopener"}

> Bans VPNs, Dropbox, and more India's government last week issued confidential information security guidelines that calls on the 30 million plus workers it employs to adopt better work practices – and as if to prove a point, the document quickly leaked on a government website.... [...]
