Title: Security researcher receives legal threat over patched Powertek data center vulnerabilities
Date: 2022-06-15T12:33:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-15-security-researcher-receives-legal-threat-over-patched-powertek-data-center-vulnerabilities

[Source](https://portswigger.net/daily-swig/security-researcher-receives-legal-threat-over-patched-powertek-data-center-vulnerabilities){:target="_blank" rel="noopener"}

> Vendor threatened legal action following disclosure and fixes being issued, bug hunter claims [...]
