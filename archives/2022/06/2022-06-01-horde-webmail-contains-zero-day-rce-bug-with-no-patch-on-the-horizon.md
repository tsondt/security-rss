Title: Horde Webmail contains zero-day RCE bug with no patch on the horizon
Date: 2022-06-01T14:34:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-06-01-horde-webmail-contains-zero-day-rce-bug-with-no-patch-on-the-horizon

[Source](https://portswigger.net/daily-swig/horde-webmail-contains-zero-day-rce-bug-with-no-patch-on-the-horizon){:target="_blank" rel="noopener"}

> CSRF exploit requires user to open malicious email [...]
