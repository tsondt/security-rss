Title: Microsoft blocks Polonium hackers from using OneDrive in attacks
Date: 2022-06-02T13:36:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-06-02-microsoft-blocks-polonium-hackers-from-using-onedrive-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-blocks-polonium-hackers-from-using-onedrive-in-attacks/){:target="_blank" rel="noopener"}

> Microsoft said it blocked a Lebanon-based hacking group it tracks as Polonium from using the OneDrive cloud storage platform for data exfiltration and command and control while targeting and compromising Israelian organizations. [...]
