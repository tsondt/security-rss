Title: Bizarre ransomware sells decryptor on Roblox Game Pass store
Date: 2022-06-09T15:29:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2022-06-09-bizarre-ransomware-sells-decryptor-on-roblox-game-pass-store

[Source](https://www.bleepingcomputer.com/news/security/bizarre-ransomware-sells-decryptor-on-roblox-game-pass-store/){:target="_blank" rel="noopener"}

> A new ransomware is taking the unusual approach of selling its decryptor on the Roblox gaming platform using the service's in-game Robux currency. [...]
