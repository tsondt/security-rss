Title: OpenSea discloses data breach, warns users of phishing attacks
Date: 2022-06-30T06:05:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-30-opensea-discloses-data-breach-warns-users-of-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/opensea-discloses-data-breach-warns-users-of-phishing-attacks/){:target="_blank" rel="noopener"}

> OpenSea, the largest non-fungible token (NFT) marketplace, disclosed a data breach on Wednesday and warned users of phishing attacks that could target them in the coming days. [...]
