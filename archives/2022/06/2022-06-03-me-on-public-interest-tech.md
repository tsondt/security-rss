Title: Me on Public-Interest Tech
Date: 2022-06-03T19:01:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;public interest;Schneier news;video
Slug: 2022-06-03-me-on-public-interest-tech

[Source](https://www.schneier.com/blog/archives/2022/06/me-on-public-interest-tech.html){:target="_blank" rel="noopener"}

> Back in November 2020, in the middle of the COVID-19 pandemic, I gave a virtual talk at the International Symposium on Technology and Society: “ The Story of the Internet and How it Broke Bad: A Call for Public-Interest Technologists.” It was something I was really proud of, and it’s finally up on the net. [...]
