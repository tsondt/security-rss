Title: CafePress fined $500,000 for breach affecting 23 million users
Date: 2022-06-24T12:48:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-24-cafepress-fined-500000-for-breach-affecting-23-million-users

[Source](https://www.bleepingcomputer.com/news/security/cafepress-fined-500-000-for-breach-affecting-23-million-users/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) today ordered Residual Pumpkin Entity, the former owner of the CafePress t-shirt and merchandise site, to pay a $500,000 fine for attempting to cover up a major data breach impacting more than 23 million customers and failing to protect their data. [...]
