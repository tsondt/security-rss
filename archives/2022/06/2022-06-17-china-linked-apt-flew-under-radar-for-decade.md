Title: China-linked APT Flew Under Radar for Decade
Date: 2022-06-17T13:34:04+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Government;Malware;Vulnerabilities
Slug: 2022-06-17-china-linked-apt-flew-under-radar-for-decade

[Source](https://threatpost.com/apt-flew-under-radar-decade/179995/){:target="_blank" rel="noopener"}

> Evidence suggests that a just-discovered APT has been active since 2013. [...]
