Title: Gone in 130 seconds: New Tesla hack gives thieves their own personal key
Date: 2022-06-08T20:21:29+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;hacking;NFC;paak;phone as a key;Tesla
Slug: 2022-06-08-gone-in-130-seconds-new-tesla-hack-gives-thieves-their-own-personal-key

[Source](https://arstechnica.com/?p=1859753){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Last year, Tesla issued an update that made its vehicles easier to start after being unlocked with their NFC key cards. Now, a researcher has shown how the feature can be exploited to steal cars. For years, drivers who used their Tesla NFC key card to unlock their cars had to place the card on the center console to begin driving. Following the update, which was reported here last August, drivers could operate their cars immediately after unlocking them with the card. The NFC card is one of three means for unlocking a Tesla; a key [...]
