Title: Italian city of Palermo shuts down all systems to fend off cyberattack
Date: 2022-06-06T10:13:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-06-italian-city-of-palermo-shuts-down-all-systems-to-fend-off-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/italian-city-of-palermo-shuts-down-all-systems-to-fend-off-cyberattack/){:target="_blank" rel="noopener"}

> The municipality of Palermo in Southern Italy suffered a cyberattack on Friday, which appears to have had a massive impact on a broad range of operations and services to both citizens and visiting tourists. [...]
