Title: 'Prolific' NetWalker extortionist pleads guilty to ransomware charges
Date: 2022-06-29T19:04:35+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-06-29-prolific-netwalker-extortionist-pleads-guilty-to-ransomware-charges

[Source](https://go.theregister.com/feed/www.theregister.com/2022/06/29/netwalker_extortionist_pleaded_guilty/){:target="_blank" rel="noopener"}

> Canadian stole $21.5m from dozens of companies worldwide A former Canadian government employee has pleaded guilty in a US court to several charges related to his involvement with the NetWalker ransomware gang.... [...]
