Title: Users of biggest NFT marketplace warned over phishing after data leak
Date: 2022-06-30T16:29:44+00:00
Author: Dan Milmo and agency
Category: The Guardian
Tags: Non-fungible tokens (NFTs);Technology sector;Technology;Hacking;Data and computer security;Data protection;Business;World news
Slug: 2022-06-30-users-of-biggest-nft-marketplace-warned-over-phishing-after-data-leak

[Source](https://www.theguardian.com/technology/2022/jun/30/users-opensea-nft-marketplace-warned-over-phishing-after-data-leak){:target="_blank" rel="noopener"}

> OpenSea tells customers and subscribers not to open emails and files ‘sent by strangers’ after revealing breach The world’s biggest marketplace for non-fungible tokens (NFTs) has warned its users to be on the alert for email phishing attacks following a massive data leak. OpenSea, where traders exchange the crypto assets, told customers and newsletter subscribers not to open emails and files “sent by strangers” after revealing the breach. Continue reading... [...]
