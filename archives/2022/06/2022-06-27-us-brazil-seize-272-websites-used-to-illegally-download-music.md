Title: US, Brazil seize 272 websites used to illegally download music
Date: 2022-06-27T14:00:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-06-27-us-brazil-seize-272-websites-used-to-illegally-download-music

[Source](https://www.bleepingcomputer.com/news/security/us-brazil-seize-272-websites-used-to-illegally-download-music/){:target="_blank" rel="noopener"}

> The domains of six websites that streamed and provided illegal downloads of copyrighted music were seized by U.S. Homeland Security Investigations (HSI) and the Department of Justice. [...]
