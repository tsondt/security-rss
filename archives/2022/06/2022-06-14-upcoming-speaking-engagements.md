Title: Upcoming Speaking Engagements
Date: 2022-06-14T17:01:46+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-06-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2022/06/upcoming-speaking-engagements-20.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at the Dublin Tech Summit in Dublin, Ireland, June 15-16, 2022. The list is maintained on this page. [...]
