Title: Adconion Execs Plead Guilty in Federal Anti-Spam Case
Date: 2022-06-11T00:04:22+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Adconion Direct;AFRINIC;Amobee;ARIN;CAN-SPAM Act;Daniel Dye;Inspiring Networks;Jacob Bychak;Maikel Uerlings;Mark Manoogian;Media Breakaway;Micfo;Mohammed Abdul Qayyum;Mybroadband.co.za;Optinrealbig;Petr Pacas;Scott Richter
Slug: 2022-06-11-adconion-execs-plead-guilty-in-federal-anti-spam-case

[Source](https://krebsonsecurity.com/2022/06/adconion-execs-plead-guilty-in-federal-anti-spam-case/){:target="_blank" rel="noopener"}

> At the outset of their federal criminal trial for hijacking vast swaths of Internet addresses for use in large-scale email spam campaigns, three current or former executives at online advertising firm Adconion Direct (now Amobee ) have pleaded guilty to lesser misdemeanor charges of fraud and misrepresentation via email. In October 2018, prosecutors in the Southern District of California named four Ad employees — Jacob Bychak, Mark Manoogian, Petr Pacas, and Mohammed Abdul Qayyum — in a ten-count indictment (PDF) on felony charges of conspiracy, wire fraud, and electronic mail fraud. The government alleged that between December 2010 and September [...]
