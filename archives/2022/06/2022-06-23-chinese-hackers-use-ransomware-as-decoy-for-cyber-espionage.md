Title: Chinese hackers use ransomware as decoy for cyber espionage
Date: 2022-06-23T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-06-23-chinese-hackers-use-ransomware-as-decoy-for-cyber-espionage

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-use-ransomware-as-decoy-for-cyber-espionage/){:target="_blank" rel="noopener"}

> Two Chinese hacking groups conducting cyber espionage and stealing intellectual property from Japanese and western companies are deploying ransomware as a decoy to cover up their malicious activities. [...]
