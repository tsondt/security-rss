Title: Discovery of 56 OT Device Flaws Blamed on Lackluster Security Culture
Date: 2022-06-22T12:34:57+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-06-22-discovery-of-56-ot-device-flaws-blamed-on-lackluster-security-culture

[Source](https://threatpost.com/discovery-of-56-ot-device-flaws-blamed-on-lackluster-security-culture/180035/){:target="_blank" rel="noopener"}

> Culture of ‘insecure-by-design’ security is cited in discovery of bug-riddled operational technology devices. [...]
