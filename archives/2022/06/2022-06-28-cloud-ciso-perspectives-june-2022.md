Title: Cloud CISO Perspectives: June 2022
Date: 2022-06-28T19:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-06-28-cloud-ciso-perspectives-june-2022

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-june-2022/){:target="_blank" rel="noopener"}

> June saw the in-person return of the RSA Conference in San Francisco, one of the largest cybersecurity enterprise conferences in the world. It was great to meet with so many of you at many of our Google Cloud events, at our panel hosted in partnership with Cyversity, and throughout the conference. At RSA we focused on our industry-leading security products, but even more importantly on our goal to make (and encourage others to make) more secure products, not just security products. And remember, we make this newsletter available on the Google Cloud blog and by email—you can subscribe here. RSA [...]
