Title: Zendesk Explore flaws opened the door to account pillage
Date: 2022-11-15T16:10:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-15-zendesk-explore-flaws-opened-the-door-to-account-pillage

[Source](https://portswigger.net/daily-swig/zendesk-explore-flaws-opened-the-door-to-account-pillage){:target="_blank" rel="noopener"}

> Patched SQLi and logical access vulnerabilities posed serious risk [...]
