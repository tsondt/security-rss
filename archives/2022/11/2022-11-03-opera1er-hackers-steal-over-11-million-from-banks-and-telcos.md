Title: OPERA1ER hackers steal over $11 million from banks and telcos
Date: 2022-11-03T11:14:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-03-opera1er-hackers-steal-over-11-million-from-banks-and-telcos

[Source](https://www.bleepingcomputer.com/news/security/opera1er-hackers-steal-over-11-million-from-banks-and-telcos/){:target="_blank" rel="noopener"}

> A threat group that researchers call OPERA1ER has stolen at least $11 million from banks and telecommunication service providers in Africa using off-the-shelf hacking tools. [...]
