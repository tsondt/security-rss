Title: Million-dollar bug bounties: The rise of record-breaking payouts
Date: 2022-11-29T15:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-29-million-dollar-bug-bounties-the-rise-of-record-breaking-payouts

[Source](https://portswigger.net/daily-swig/million-dollar-bug-bounties-the-rise-of-record-breaking-payouts){:target="_blank" rel="noopener"}

> As seven-figure vulnerability rewards continue to hit headlines, what is driving bug bounty inflation? [...]
