Title: U.S. govt employees exposed to mobile attacks from outdated Android, iOS
Date: 2022-11-02T11:11:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government;Mobile
Slug: 2022-11-02-us-govt-employees-exposed-to-mobile-attacks-from-outdated-android-ios

[Source](https://www.bleepingcomputer.com/news/security/us-govt-employees-exposed-to-mobile-attacks-from-outdated-android-ios/){:target="_blank" rel="noopener"}

> Roughly half of all Android-based mobile phones used by state and local government employees are running outdated versions of the operating system, exposing them to hundreds of vulnerabilities threat actors can leverage to perform cyberattacks. [...]
