Title: Meta fined €265M for not protecting Facebook users' data from scrapers
Date: 2022-11-28T10:15:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-28-meta-fined-265m-for-not-protecting-facebook-users-data-from-scrapers

[Source](https://www.bleepingcomputer.com/news/security/meta-fined-265m-for-not-protecting-facebook-users-data-from-scrapers/){:target="_blank" rel="noopener"}

> Meta has been fined €265 million ($275.5 million) by the Irish data protection commission (DPC) for a massive 2021 Facebook data leak exposing the information of hundreds of million users worldwide. [...]
