Title: Google wins lawsuit against alleged Russian botnet herders
Date: 2022-11-17T15:00:07+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-11-17-google-wins-lawsuit-against-alleged-russian-botnet-herders

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/17/google_botnet_default_judgment/){:target="_blank" rel="noopener"}

> Judge tells tale of two men, their lawyer, and a 'willful campaign... to mislead the court' A New York judge has issued a default judgment against two Russian nationals who are alleged to have helped create the "Glupteba" botnet, sold fraudulent credit card information, and generated cryptocurrency using the network.... [...]
