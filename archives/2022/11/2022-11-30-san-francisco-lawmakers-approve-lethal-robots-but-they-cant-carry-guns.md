Title: San Francisco lawmakers approve lethal robots, but they can't carry guns
Date: 2022-11-30T21:30:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-30-san-francisco-lawmakers-approve-lethal-robots-but-they-cant-carry-guns

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/30/san_francisco_killer_robots_ordinance/){:target="_blank" rel="noopener"}

> Rise of the explosive machines San Francisco police can deploy so-called "killer robots" following a Board of Supervisors' vote on Tuesday, clearing the cops to use robots equipped with explosives in extreme situations.... [...]
