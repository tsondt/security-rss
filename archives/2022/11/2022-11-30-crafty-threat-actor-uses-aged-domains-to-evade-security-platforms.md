Title: Crafty threat actor uses 'aged' domains to evade security platforms
Date: 2022-11-30T03:05:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-30-crafty-threat-actor-uses-aged-domains-to-evade-security-platforms

[Source](https://www.bleepingcomputer.com/news/security/crafty-threat-actor-uses-aged-domains-to-evade-security-platforms/){:target="_blank" rel="noopener"}

> A sophisticated threat actor named 'CashRewindo' has been using aged domains in global malvertising campaigns that lead to investment scam sites. [...]
