Title: Unofficial fix emerges for Windows bug abused to infect home PCs with ransomware
Date: 2022-11-01T03:48:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-01-unofficial-fix-emerges-for-windows-bug-abused-to-infect-home-pcs-with-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/01/microsoft_motw_malware_flaw/){:target="_blank" rel="noopener"}

> Broken code signature? LGTM, says Microsoft OS A cybersecurity firm has issued another unofficial patch to squash a bug in Windows that Microsoft has yet to fix, with this hole being actively exploited to spread ransomware.... [...]
