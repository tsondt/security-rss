Title: Husband and wife nuclear warship 'spy' team get 20 years each
Date: 2022-11-10T17:14:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-10-husband-and-wife-nuclear-warship-spy-team-get-20-years-each

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/10/husband_and_wife_spy_team/){:target="_blank" rel="noopener"}

> The Toebbes tried selling US Navy secrets, but handed them right to the FBI A woman and her husband, who both copped to trying to sell nuclear warship secrets to a foreign government, have been sentenced to prison, with each set to spend around two decades behind bars.... [...]
