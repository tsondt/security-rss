Title: FBI: Hive ransomware extorted $100M from over 1,300 victims
Date: 2022-11-17T14:46:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-17-fbi-hive-ransomware-extorted-100m-from-over-1300-victims

[Source](https://www.bleepingcomputer.com/news/security/fbi-hive-ransomware-extorted-100m-from-over-1-300-victims/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) said today that the notorious Hive ransomware gang has successfully extorted roughly $100 million from over a thousand companies since June 2021. [...]
