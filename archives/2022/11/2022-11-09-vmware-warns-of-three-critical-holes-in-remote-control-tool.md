Title: VMware warns of three critical holes in remote-control tool
Date: 2022-11-09T01:16:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-09-vmware-warns-of-three-critical-holes-in-remote-control-tool

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/09/vmware_workspace_one_assist_critical_flaws/){:target="_blank" rel="noopener"}

> Anyone can pretend to be your Windows IT support and take command of staff devices VMware has revealed a terrible trio of critical-rated flaws in Workspace ONE Assist for Windows – a product used by IT and help desk staff to remotely take over and manage employees' devices.... [...]
