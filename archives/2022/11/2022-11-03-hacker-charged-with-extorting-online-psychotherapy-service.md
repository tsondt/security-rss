Title: Hacker Charged With Extorting Online Psychotherapy Service
Date: 2022-11-03T14:43:22+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Atti Kurritu;hack the planet;Julius Kivimäki;ransom_man;Vastaamo;Ville Tapio;William Ralston;wired;Zbot;Zeekill
Slug: 2022-11-03-hacker-charged-with-extorting-online-psychotherapy-service

[Source](https://krebsonsecurity.com/2022/11/hacker-charged-with-extorting-online-psychotherapy-service/){:target="_blank" rel="noopener"}

> A 25-year-old Finnish man has been charged with extorting a once popular and now-bankrupt online psychotherapy company and its patients. Finnish authorities rarely name suspects in an investigation, but they were willing to make an exception for Julius “Zeekill” Kivimaki, a notorious hacker who — at the tender age of 17 — had been convicted of more than 50,000 cybercrimes, including data breaches, payment fraud, operating botnets, and calling in bomb threats. In late October 2022, Kivimaki was charged (and arrested in absentia, according to the Finns) with attempting to extort money from the Vastaamo Psychotherapy Center. On October 21, [...]
