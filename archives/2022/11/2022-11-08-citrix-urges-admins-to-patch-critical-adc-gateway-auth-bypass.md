Title: Citrix urges admins to patch critical ADC, Gateway auth bypass
Date: 2022-11-08T12:03:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-08-citrix-urges-admins-to-patch-critical-adc-gateway-auth-bypass

[Source](https://www.bleepingcomputer.com/news/security/citrix-urges-admins-to-patch-critical-adc-gateway-auth-bypass/){:target="_blank" rel="noopener"}

> Citrix is urging customers to install security updates for a critical authentication bypass vulnerability in Citrix ADC and Citrix Gateway. [...]
