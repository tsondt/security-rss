Title: Shocker: EV charging infrastructure is seriously insecure
Date: 2022-11-15T21:30:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-15-shocker-ev-charging-infrastructure-is-seriously-insecure

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/15/ev_charging_infrastructure_sandia/){:target="_blank" rel="noopener"}

> What did we learn from the IoT days? Apparently nothing. If you've noticed car charging stations showing up in your area, congratulations! You're part of a growing network of systems so poorly secured they could one day be used to destabilize entire electrical grids, and which contain enough security issues to be problematic today.... [...]
