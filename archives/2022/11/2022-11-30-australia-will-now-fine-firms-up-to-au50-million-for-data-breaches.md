Title: Australia will now fine firms up to AU$50 million for data breaches
Date: 2022-11-30T12:26:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-11-30-australia-will-now-fine-firms-up-to-au50-million-for-data-breaches

[Source](https://www.bleepingcomputer.com/news/security/australia-will-now-fine-firms-up-to-au50-million-for-data-breaches/){:target="_blank" rel="noopener"}

> The Australian parliament has approved a bill to amend the country's privacy legislation, significantly increasing the maximum penalties to AU$50 million for companies and data controllers who suffered large-scale data breaches. [...]
