Title: DuckDuckGo now lets all Android users block trackers in their apps
Date: 2022-11-16T08:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2022-11-16-duckduckgo-now-lets-all-android-users-block-trackers-in-their-apps

[Source](https://www.bleepingcomputer.com/news/security/duckduckgo-now-lets-all-android-users-block-trackers-in-their-apps/){:target="_blank" rel="noopener"}

> DuckDuckGo for Android's 'App Tracking Protection' feature has reached open beta, allowing all Android users to block third-party trackers across all their installed apps. [...]
