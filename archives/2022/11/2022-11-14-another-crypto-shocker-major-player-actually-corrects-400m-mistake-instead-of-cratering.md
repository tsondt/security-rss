Title: Another crypto shocker: Major player actually corrects $400m mistake instead of cratering
Date: 2022-11-14T12:30:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-11-14-another-crypto-shocker-major-player-actually-corrects-400m-mistake-instead-of-cratering

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/14/cryptodotcom_gateio_400m/){:target="_blank" rel="noopener"}

> Fellow crypto-exchange Gate.io spots error, returns funds Over the weekend it was revealed that cryptocurrency exchange company Crypto.com accidentally sent over $400 million to another cryptocurrency exchange and was miraculously able to get it back.... [...]
