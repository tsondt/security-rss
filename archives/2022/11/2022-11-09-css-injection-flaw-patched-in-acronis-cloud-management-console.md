Title: CSS injection flaw patched in Acronis cloud management console
Date: 2022-11-09T14:43:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-09-css-injection-flaw-patched-in-acronis-cloud-management-console

[Source](https://portswigger.net/daily-swig/css-injection-flaw-patched-in-acronis-cloud-management-console){:target="_blank" rel="noopener"}

> CSRF attacks could be triggered to access and exfiltrate information [...]
