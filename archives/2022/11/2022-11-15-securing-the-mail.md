Title: Securing the mail
Date: 2022-11-15T13:30:05+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2022-11-15-securing-the-mail

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/15/securing_the_mail/){:target="_blank" rel="noopener"}

> Making the business case for email encryption Webinar Every now and again the dangers of using personal and unencrypted email services makes it to the top of the news agenda. It happened to Hilary Clinton in the States, and it's been all over the front pages in the UK following the resignation of British Home Secretary Suella Braverman after she used her personal email account six times for government business.... [...]
