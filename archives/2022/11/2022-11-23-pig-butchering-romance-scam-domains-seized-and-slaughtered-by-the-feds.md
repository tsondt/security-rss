Title: 'Pig butchering' romance scam domains seized and slaughtered by the Feds
Date: 2022-11-23T00:30:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-23-pig-butchering-romance-scam-domains-seized-and-slaughtered-by-the-feds

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/23/pig_butchering_domains_seized/){:target="_blank" rel="noopener"}

> 'We allege these fraudsters bled dry each of their victims' of $10m The US government seized seven domain names used in so-called "pig butchering" scams that netted criminals more than $10 million.... [...]
