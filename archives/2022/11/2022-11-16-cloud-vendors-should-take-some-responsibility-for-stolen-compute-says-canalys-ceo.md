Title: Cloud vendors should take some responsibility for stolen compute, says Canalys CEO
Date: 2022-11-16T14:45:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-11-16-cloud-vendors-should-take-some-responsibility-for-stolen-compute-says-canalys-ceo

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/16/clouds_stolen_compute_canalys/){:target="_blank" rel="noopener"}

> Crypto winter also attributed to semiconductor slumps in recent quarters Canalys Forums APAC Canalys CEO Steve Brazier has proposed that cloud vendors should have similar accountability to credit card companies when accounts are hacked and used to mine cryptocurrency.... [...]
