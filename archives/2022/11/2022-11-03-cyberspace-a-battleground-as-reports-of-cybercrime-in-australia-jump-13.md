Title: Cyberspace ‘a battleground’ as reports of cybercrime in Australia jump 13%
Date: 2022-11-03T14:01:16+00:00
Author: Daniel Hurst
Category: The Guardian
Tags: Australia news;Data and computer security;Technology;Cybercrime
Slug: 2022-11-03-cyberspace-a-battleground-as-reports-of-cybercrime-in-australia-jump-13

[Source](https://www.theguardian.com/australia-news/2022/nov/04/cyberspace-a-battleground-as-reports-of-cybercrime-in-australia-jump-13){:target="_blank" rel="noopener"}

> Fraud, online shopping and banking among most commonly reported crimes, but ransomware ‘most destructive’, ASD says Follow out Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast The number of reports of cybercrime in Australia had shot up by 13% to 76,000 in a year, or one every seven minutes, even before a series of high-profile privacy breaches hit the headlines. These threats are imposing an increasingly heavy cost on businesses, with the average loss per cybercrime rising by 14% to $39,000 for a small business and $62,000 [...]
