Title: I am a Medibank customer. Am I affected by the cyber-attack? What can I do to protect myself?
Date: 2022-11-11T06:08:21+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Cybercrime;Australia news;Technology;Data protection;Data and computer security;Medibank
Slug: 2022-11-11-i-am-a-medibank-customer-am-i-affected-by-the-cyber-attack-what-can-i-do-to-protect-myself

[Source](https://www.theguardian.com/technology/2022/oct/26/i-am-a-medibank-customer-am-i-affected-by-the-cyber-attack-what-can-i-do-to-protect-myself){:target="_blank" rel="noopener"}

> Experts suggest using multifactor authentication and telling your bank to put extra security checks in place Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Millions of Medibank’s current and former customers have had their personal information, including health claims, exposed in a hack of the company’s customer database. Here’s what we know so far, and what you can do. Name Address Date of birth Gender Email address Medicare card number (in some cases) Health claims made with Medibank (in some cases) Financial support for customers [...]
