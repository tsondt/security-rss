Title: US offshore oil and gas installation at 'increasing' risk of cyberattack
Date: 2022-11-21T16:02:11+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-11-21-us-offshore-oil-and-gas-installation-at-increasing-risk-of-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/21/us_oil_gas_cyber_threats/){:target="_blank" rel="noopener"}

> GAO says 2010 Deepwater Horizon disaster will look like a walk in the park The US Government Accountability Office (GAO) has warned that the time to act on securing the US's offshore oil and natural gas installations is now because they are under "increasing" and "significant risk" of cyberattack.... [...]
