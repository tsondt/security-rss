Title: Mind the Gap
Date: 2022-11-22T13:05:00-08:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2022-11-22-mind-the-gap

[Source](https://googleprojectzero.blogspot.com/2022/11/mind-the-gap.html){:target="_blank" rel="noopener"}

> By Ian Beer, Project Zero Note: The vulnerabilities discussed in this blog post (CVE-2022-33917) are fixed by the upstream vendor, but at the time of publication, t hese fixes have not yet made it downstream to affected Android devices (including Pixel, Samsung, Xiaomi, Oppo and others). Devices with a Mali GPU are currently vulnerable. Introduction In June 2022, Project Zero researcher Maddie Stone gave a talk at FirstCon22 titled 0-day In-the-Wild Exploitation in 2022...so far. A key takeaway was that approximately 50% of the observed 0-days in the first half of 2022 were variants of previously patched vulnerabilities. This finding [...]
