Title: You can now assign multiple MFA devices in IAM
Date: 2022-11-16T23:17:05+00:00
Author: Liam Wadman
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Technical How-to;credentials;FIDO;Google Authenticator;IAM;MFA;multi-factor authentication;Security;Security Blog;totp;u2f;WebAuthn;YubiKey
Slug: 2022-11-16-you-can-now-assign-multiple-mfa-devices-in-iam

[Source](https://aws.amazon.com/blogs/security/you-can-now-assign-multiple-mfa-devices-in-iam/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), security is our top priority, and configuring multi-factor authentication (MFA) on accounts is an important step in securing your organization. Now, you can add multiple MFA devices to AWS account root users and AWS Identity and Access Management (IAM) users in your AWS accounts. This helps you to raise the [...]
