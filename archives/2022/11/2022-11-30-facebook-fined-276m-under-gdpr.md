Title: Facebook Fined $276M under GDPR
Date: 2022-11-30T12:00:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Facebook;GDPR;law enforcement;leaks
Slug: 2022-11-30-facebook-fined-276m-under-gdpr

[Source](https://www.schneier.com/blog/archives/2022/11/facebook-fined-276m-under-gdpr.html){:target="_blank" rel="noopener"}

> Facebook—Meta—was just fined $276 million (USD) for a data leak that included full names, birth dates, phone numbers, and location. Meta’s total fine by the Data Protection Commission is over $700 million. Total GDPR fines are over €2 billion (EUR) since 2018. [...]
