Title: QBot phishing abuses Windows Control Panel EXE to infect devices
Date: 2022-11-17T13:19:17-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-17-qbot-phishing-abuses-windows-control-panel-exe-to-infect-devices

[Source](https://www.bleepingcomputer.com/news/security/qbot-phishing-abuses-windows-control-panel-exe-to-infect-devices/){:target="_blank" rel="noopener"}

> Phishing emails distributing the QBot malware are using a DLL hijacking flaw in the Windows 10 Control Panel to infect computers, likely as an attempt to evade detection by security software. [...]
