Title: The Conviction of Uber’s Chief Security Officer
Date: 2022-11-07T12:17:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;cover-ups;cyberattack;cybersecurity;data breaches;data protection;Uber
Slug: 2022-11-07-the-conviction-of-ubers-chief-security-officer

[Source](https://www.schneier.com/blog/archives/2022/11/the-conviction-of-ubers-chief-security-officer.html){:target="_blank" rel="noopener"}

> I have been meaning to write about Joe Sullivan, Uber’s former Chief Security Officer. He was convicted of crimes related to covering up a cyberattack against Uber. It’s a complicated case, and I’m not convinced that he deserved a guilty ruling or that it’s a good thing for the industry. I may still write something, but until then, this essay on the topic is worth reading. [...]
