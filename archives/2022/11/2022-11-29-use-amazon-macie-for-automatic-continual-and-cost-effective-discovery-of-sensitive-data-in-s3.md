Title: Use Amazon Macie for automatic, continual, and cost-effective discovery of sensitive data in S3
Date: 2022-11-29T20:11:55+00:00
Author: Jonathan Nguyen
Category: AWS Security
Tags: Security, Identity, & Compliance;Technical How-to;Macie;S3;Security Blog;Sensitive data
Slug: 2022-11-29-use-amazon-macie-for-automatic-continual-and-cost-effective-discovery-of-sensitive-data-in-s3

[Source](https://aws.amazon.com/blogs/security/use-amazon-macie-for-automatic-continual-and-cost-effective-discovery-of-sensitive-data-in-s3/){:target="_blank" rel="noopener"}

> Customers have an increasing need to collect, store, and process data within their AWS environments for application modernization, reporting, and predictive analytics. AWS Well-Architected security pillar, general data privacy and compliance regulations require that you appropriately identify and secure sensitive information. Knowing where your data is allows you to implement the appropriate security controls which [...]
