Title: India's Home Ministry cracks down on predatory lending apps following suicides
Date: 2022-11-01T03:15:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-01-indias-home-ministry-cracks-down-on-predatory-lending-apps-following-suicides

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/01/india_lending_app_crackdown_ordered/){:target="_blank" rel="noopener"}

> Local media say they're China backed, Ministry only mentions organized crime India's Home Ministry has asked state governments to crack down on illegal lending apps it says have led to "multiple suicides by citizens owing to harassment, blackmail, and harsh recovery methods."... [...]
