Title: Microsoft hits the switch on password-free smartphone authentication
Date: 2022-11-07T17:30:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-07-microsoft-hits-the-switch-on-password-free-smartphone-authentication

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/07/microsoft_azure_phishing_mfa/){:target="_blank" rel="noopener"}

> No more MF phish on this MFA cellphone as Azure AD CBA + YubiKey hits preview Microsoft is rolling out another way for smartphone and tablet users to protect themselves from phishing attacks as post-pandemic hybrid work pulls more and more workers under bring-your-own-device (BYOD) policies.... [...]
