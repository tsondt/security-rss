Title: Detect and block advanced bot traffic
Date: 2022-11-10T17:44:38+00:00
Author: Etienne Munnich
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Technical How-to;block bots;bot management;bot mitigation;bot protection;filter bots;rate limit bots;Security Blog;stop bots
Slug: 2022-11-10-detect-and-block-advanced-bot-traffic

[Source](https://aws.amazon.com/blogs/security/detect-and-block-advanced-bot-traffic/){:target="_blank" rel="noopener"}

> Automated scripts, known as bots, can generate significant volumes of traffic to your mobile applications, websites, and APIs. Targeted bots take this a step further by targeting website content, such as product availability or pricing. Traffic from targeted bots can result in a poor user experience by competing against legitimate user traffic for website access [...]
