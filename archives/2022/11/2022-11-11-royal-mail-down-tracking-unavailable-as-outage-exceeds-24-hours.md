Title: Royal Mail down: Tracking unavailable as outage exceeds 24 hours
Date: 2022-11-11T03:33:55-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-11-11-royal-mail-down-tracking-unavailable-as-outage-exceeds-24-hours

[Source](https://www.bleepingcomputer.com/news/security/royal-mail-down-tracking-unavailable-as-outage-exceeds-24-hours/){:target="_blank" rel="noopener"}

> Royal Mail, UK's leading mail and parcel delivery service, has been experiencing ongoing outages with its online tracking services down for more than 24 hours at the time of writing. [...]
