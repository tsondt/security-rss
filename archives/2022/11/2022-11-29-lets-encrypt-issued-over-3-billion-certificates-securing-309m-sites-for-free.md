Title: Let’s Encrypt issued over 3 billion certificates, securing 309M sites for free
Date: 2022-11-29T17:03:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-29-lets-encrypt-issued-over-3-billion-certificates-securing-309m-sites-for-free

[Source](https://www.bleepingcomputer.com/news/security/let-s-encrypt-issued-over-3-billion-certificates-securing-309m-sites-for-free/){:target="_blank" rel="noopener"}

> Internet Security Research Group (ISRG), the nonprofit behind Let's Encrypt, says the open certificate authority (CA) has issued its three billionth certificate this year. [...]
