Title: Pro-Russian hacktivists take down EU Parliament site in DDoS attack
Date: 2022-11-23T12:21:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-23-pro-russian-hacktivists-take-down-eu-parliament-site-in-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/pro-russian-hacktivists-take-down-eu-parliament-site-in-ddos-attack/){:target="_blank" rel="noopener"}

> The website of the European Parliament has been taken down following a DDoS (Distributed Denial of Service) attack claimed by Anonymous Russia, part of the pro-Russian hacktivist group Killnet. [...]
