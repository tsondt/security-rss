Title: Prototype pollution bug exposed Ember.js applications to XSS
Date: 2022-11-08T12:16:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-08-prototype-pollution-bug-exposed-emberjs-applications-to-xss

[Source](https://portswigger.net/daily-swig/prototype-pollution-bug-exposed-ember-js-applications-to-xss){:target="_blank" rel="noopener"}

> Unsanitized user input risk spotted in JavaScript framework [...]
