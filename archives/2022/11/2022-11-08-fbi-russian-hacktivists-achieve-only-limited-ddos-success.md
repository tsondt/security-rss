Title: FBI: Russian hacktivists achieve only 'limited' DDoS success
Date: 2022-11-08T02:31:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-08-fbi-russian-hacktivists-achieve-only-limited-ddos-success

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/08/fbi_hacktivists_useless/){:target="_blank" rel="noopener"}

> OK, so you've got a botnet. That don't impress me much Pro-Russia hacktivists' recent spate of network-flooding bot traffic aimed at US critical infrastructure targets, while annoying, have had "limited success," according to the FBI.... [...]
