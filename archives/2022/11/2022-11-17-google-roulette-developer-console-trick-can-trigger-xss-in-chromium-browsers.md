Title: Google Roulette: Developer console trick can trigger XSS in Chromium browsers
Date: 2022-11-17T13:16:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-17-google-roulette-developer-console-trick-can-trigger-xss-in-chromium-browsers

[Source](https://portswigger.net/daily-swig/google-roulette-developer-console-trick-can-trigger-xss-in-chromium-browsers){:target="_blank" rel="noopener"}

> A case study on the complexity of browser security [...]
