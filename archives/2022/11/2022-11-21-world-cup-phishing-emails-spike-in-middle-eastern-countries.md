Title: World Cup phishing emails spike in Middle Eastern countries
Date: 2022-11-21T20:49:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-21-world-cup-phishing-emails-spike-in-middle-eastern-countries

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/21/world_cup_phishing_emails/){:target="_blank" rel="noopener"}

> That's where the money is Phishing attempts targeting victims in the Middle East increased 100 percent last month in the lead up to the World Cup in Qatar, according to security shop Trellix.... [...]
