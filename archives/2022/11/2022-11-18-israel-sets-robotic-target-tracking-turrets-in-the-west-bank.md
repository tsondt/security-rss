Title: Israel sets robotic target-tracking turrets in the West Bank
Date: 2022-11-18T06:30:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-18-israel-sets-robotic-target-tracking-turrets-in-the-west-bank

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/18/israel_sets_robotic_targettracking_turrets/){:target="_blank" rel="noopener"}

> Military says they'll save lives on both sides as tensions escalate Israeli fortifications in the West Bank are becoming a bit more faceless, as the military has reportedly deployed robotic turrets capable of firing stun grenades, less-than-lethal bullets, and tear gas at Palestinians protesting their presence.... [...]
