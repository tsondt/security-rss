Title: Swiss Re wants government bail out as cybercrime insurance costs spike
Date: 2022-11-08T20:30:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-08-swiss-re-wants-government-bail-out-as-cybercrime-insurance-costs-spike

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/08/government_cyber_insurance/){:target="_blank" rel="noopener"}

> Giant forecasts premiums rising to $23b by 2025 As insurance companies struggle to stay afloat amid rising cyber claims, Swiss Re has recommended a public-private partnership insurance scheme with one option being a government-backed fund to help fill the coverage gap.... [...]
