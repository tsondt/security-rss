Title: Experian, T-Mobile US settle data spills for mere $16m
Date: 2022-11-08T17:00:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-08-experian-t-mobile-us-settle-data-spills-for-mere-16m

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/08/experian_tmobile_fined_over_2012/){:target="_blank" rel="noopener"}

> Two breaches: one in 2012, another in 2015 – saw 18m folks' records stolen Experian and T-Mobile US have reached separate settlements with 40 states in America following a pair of data security breaches in 2012 and 2015. The settlement will net authorities $16 million, along with assurances it won't happen again.... [...]
