Title: Ransomware cost US banks $1.2 billion last year
Date: 2022-11-02T16:30:14+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-11-02-ransomware-cost-us-banks-12-billion-last-year

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/02/ransomware_cost_us_banks/){:target="_blank" rel="noopener"}

> Up 188% on 2020 but could be because financial institutions were encouraged to report incidents Banks in the US paid out nearly $1.2 billion in 2021 as a result of ransomware attacks, a marked rise over the year before though it may simply be due to more financial institutions being asked to report incidents.... [...]
