Title: Making Cobalt Strike harder for threat actors to abuse
Date: 2022-11-17T17:00:00+00:00
Author: Greg Sinclair
Category: GCP Security
Tags: Identity & Security
Slug: 2022-11-17-making-cobalt-strike-harder-for-threat-actors-to-abuse

[Source](https://cloud.google.com/blog/products/identity-security/making-cobalt-strike-harder-for-threat-actors-to-abuse/){:target="_blank" rel="noopener"}

> Cobalt Strike, the popular tool used by red teams to test the resilience of their cyber defenses, has seen many iterations and improvements over the last decade. First released in 2012, it was originally the commercial spinoff of the open-source Armitage project that added a graphical user interface (GUI) to the Metasploit framework to help security practitioners detect software vulnerabilities more quickly. It has since matured into a point-and-click system for the deployment of the Swiss Army Knife of remote access tools onto targeted assets. While the intention of Cobalt Strike is to emulate a real cyber threat, malicious actors [...]
