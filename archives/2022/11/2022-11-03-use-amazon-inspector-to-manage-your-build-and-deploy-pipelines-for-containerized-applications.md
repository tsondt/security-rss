Title: Use Amazon Inspector to manage your build and deploy pipelines for containerized applications
Date: 2022-11-03T16:50:23+00:00
Author: Scott Ward
Category: AWS Security
Tags: Amazon Elastic Container Registry;Amazon Inspector;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Containers;DevSecOps;Security Blog
Slug: 2022-11-03-use-amazon-inspector-to-manage-your-build-and-deploy-pipelines-for-containerized-applications

[Source](https://aws.amazon.com/blogs/security/use-amazon-inspector-to-manage-your-build-and-deploy-pipelines-for-containerized-applications/){:target="_blank" rel="noopener"}

> Amazon Inspector is an automated vulnerability management service that continually scans Amazon Web Services (AWS) workloads for software vulnerabilities and unintended network exposure. Amazon Inspector currently supports vulnerability reporting for Amazon Elastic Compute Cloud (Amazon EC2) instances and container images stored in Amazon Elastic Container Registry (Amazon ECR). With the emergence of Docker in 2013, [...]
