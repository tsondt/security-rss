Title: F5 fixes high severity RCE bug in BIG-IP, BIG-IQ devices
Date: 2022-11-16T15:02:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-16-f5-fixes-high-severity-rce-bug-in-big-ip-big-iq-devices

[Source](https://portswigger.net/daily-swig/f5-fixes-high-severity-rce-bug-in-big-ip-big-iq-devices){:target="_blank" rel="noopener"}

> Widespread exploitation deemed ‘unlikely’ given hurdles [...]
