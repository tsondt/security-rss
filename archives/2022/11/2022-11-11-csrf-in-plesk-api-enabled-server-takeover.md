Title: CSRF in Plesk API enabled server takeover
Date: 2022-11-11T11:31:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-11-csrf-in-plesk-api-enabled-server-takeover

[Source](https://portswigger.net/daily-swig/csrf-in-plesk-api-enabled-server-takeover){:target="_blank" rel="noopener"}

> Bugs in programming interfaces of web hosting admin tool patched [...]
