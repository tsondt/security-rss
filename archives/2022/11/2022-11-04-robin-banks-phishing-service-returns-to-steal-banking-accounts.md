Title: Robin Banks phishing service returns to steal banking accounts
Date: 2022-11-04T11:48:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-04-robin-banks-phishing-service-returns-to-steal-banking-accounts

[Source](https://www.bleepingcomputer.com/news/security/robin-banks-phishing-service-returns-to-steal-banking-accounts/){:target="_blank" rel="noopener"}

> The Robin Banks phishing-as-a-service (PhaaS) platform is back in action with infrastructure hosted by a Russian internet company that offers protection against distributed denial-of-service (DDoS) attacks. [...]
