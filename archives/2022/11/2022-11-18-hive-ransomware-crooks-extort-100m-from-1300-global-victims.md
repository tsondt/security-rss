Title: Hive ransomware crooks extort $100m from 1,300 global victims
Date: 2022-11-18T20:35:16+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-18-hive-ransomware-crooks-extort-100m-from-1300-global-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/18/hive_ransomware_fbi/){:target="_blank" rel="noopener"}

> FBI, CISA sound the alarm and detail IOCs Hive ransomware criminals have hit more than 1,300 companies globally, extorting about $100 million from its victims over the last 18 months, according to the FBI.... [...]
