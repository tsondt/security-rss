Title: How to migrate on-premises Active Directory users to Google Cloud Managed Microsoft AD
Date: 2022-11-15T21:00:00+00:00
Author: Muthu Ganesh A
Category: GCP Security
Tags: Identity & Security
Slug: 2022-11-15-how-to-migrate-on-premises-active-directory-users-to-google-cloud-managed-microsoft-ad

[Source](https://cloud.google.com/blog/products/identity-security/introducing-managed-microsoft-ad-migration-support-for-active-directory-users/){:target="_blank" rel="noopener"}

> One benefit of cloud migration is having access to managed services, which can reduce operational overhead. For organizations operating in Microsoft-centered environments, Google Cloud offers a highly-available, hardened Managed Service for Microsoft Active Directory running on Windows virtual machines. Managed Microsoft AD provides benefits such as automated AD server updates, maintenance, default security configurations, and needs no hardware management or patching. Most organizations adopting Managed Microsoft AD will be migrating from an existing on-premises AD deployment. Typically, when migrating Active Directory objects, existing users cannot continue to access resources in the new domain unless security identifier (SID) history is preserved. [...]
