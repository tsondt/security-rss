Title: Mali GPU ‘patch gap’ leaves Android users vulnerable to attacks
Date: 2022-11-23T10:59:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-11-23-mali-gpu-patch-gap-leaves-android-users-vulnerable-to-attacks

[Source](https://www.bleepingcomputer.com/news/security/mali-gpu-patch-gap-leaves-android-users-vulnerable-to-attacks/){:target="_blank" rel="noopener"}

> A set of five exploitable vulnerabilities in Arm's Mali GPU driver remain unfixed months after the chip maker patched them, leaving potentially millions of Android devices exposed to attacks. [...]
