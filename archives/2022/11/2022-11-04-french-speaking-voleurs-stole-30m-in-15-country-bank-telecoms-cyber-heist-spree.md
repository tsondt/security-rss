Title: French-speaking voleurs stole $30m in 15-country bank, telecoms cyber-heist spree
Date: 2022-11-04T06:22:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-04-french-speaking-voleurs-stole-30m-in-15-country-bank-telecoms-cyber-heist-spree

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/04/french_opera1er_group_ib/){:target="_blank" rel="noopener"}

> Smooth 'OPERA1ER' hit orgs around the world over four or more years A French-speaking criminal group codenamed OPERA1ER has pulled off more than 30 cyber-heists against telecom organizations and banks across Africa, Asia, and Latin America, stealing upwards of $30 million over four years, according to security researchers.... [...]
