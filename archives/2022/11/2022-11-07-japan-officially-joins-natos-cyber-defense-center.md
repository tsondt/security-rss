Title: Japan officially joins NATO's cyber defense center
Date: 2022-11-07T11:32:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-11-07-japan-officially-joins-natos-cyber-defense-center

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/07/japan_joins_nato_cyber_defence/){:target="_blank" rel="noopener"}

> Already red-teaming and blue teaming in the international Locked Shields contest every year Japan’s Ministry of Defence (JMOD) announced on Friday that it has formally joined NATO’s Cooperative Cyber Defense Centre of Excellence (CCDCOE).... [...]
