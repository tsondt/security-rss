Title: Swiss bankers warn: Three quarters of retail Bitcoin investors are in the red
Date: 2022-11-16T08:30:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-11-16-swiss-bankers-warn-three-quarters-of-retail-bitcoin-investors-are-in-the-red

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/16/bitcoin_investors_lose/){:target="_blank" rel="noopener"}

> Little fish lured into the market help whales cash out Somewhere between 73 and 81 percent of retail Bitcoin buyers are likely to be into the negative on their investment, according to research published Monday by the Bank of International Settlements (BIS).... [...]
