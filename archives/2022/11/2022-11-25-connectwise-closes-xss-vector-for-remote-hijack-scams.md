Title: ConnectWise closes XSS vector for remote hijack scams
Date: 2022-11-25T15:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-25-connectwise-closes-xss-vector-for-remote-hijack-scams

[Source](https://portswigger.net/daily-swig/connectwise-closes-xss-vector-for-remote-hijack-scams){:target="_blank" rel="noopener"}

> Researchers also applaud abandonment of customization feature abused by scammers [...]
