Title: Introducing reCAPTCHA Enterprise’s Mobile SDK to help protect iOS, Android apps
Date: 2022-11-01T16:00:00+00:00
Author: Mark Corner
Category: GCP Security
Tags: Retail;Public Sector;Financial Services;Gaming;Identity & Security
Slug: 2022-11-01-introducing-recaptcha-enterprises-mobile-sdk-to-help-protect-ios-android-apps

[Source](https://cloud.google.com/blog/products/identity-security/introducing-recaptcha-enterprises-mobile-sdk-to-help-protect-ios-android-apps/){:target="_blank" rel="noopener"}

> reCAPTCHA Enterprise is Google’s online fraud detection service that leverages more than a decade of experience defending the internet. reCAPTCHA Enterprise can be used to prevent fraud and attacks perpetrated by scripts, bot software, and humans. When installed inside a mobile app at the point of action, such as login, purchase, or account creation, reCAPTCHA Enterprise can block fake users and bots while allowing legitimate users to proceed. To provide more complete coverage for native mobile iOS and Android applications, we’re announcing the general availability of the reCAPTCHA Enterprise Mobile SDK. Designed with digital-first and mobile-first organizations in mind, the [...]
