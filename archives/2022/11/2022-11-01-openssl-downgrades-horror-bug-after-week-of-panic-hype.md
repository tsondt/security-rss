Title: OpenSSL downgrades horror bug after week of panic, hype
Date: 2022-11-01T21:39:28+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-01-openssl-downgrades-horror-bug-after-week-of-panic-hype

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/01/openssl_downgrades_bugs/){:target="_blank" rel="noopener"}

> Relax, there's more chance of Babbage coming back to life to hack your system than this flaw being exploited OpenSSL today issued a fix for a critical-turned-high-severity vulnerability that project maintainers warned about last week.... [...]
