Title: Urlscan.io API unwittingly leaks sensitive URLs, data
Date: 2022-11-02T14:38:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-02-urlscanio-api-unwittingly-leaks-sensitive-urls-data

[Source](https://portswigger.net/daily-swig/urlscan-io-api-unwittingly-leaks-sensitive-urls-data){:target="_blank" rel="noopener"}

> Public listings have made sensitive data searchable due to misconfigured third-party services [...]
