Title: Microsoft confirms gaming performance issues on Windows 11 22H2
Date: 2022-11-11T09:58:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-11-microsoft-confirms-gaming-performance-issues-on-windows-11-22h2

[Source](https://www.bleepingcomputer.com/news/security/microsoft-confirms-gaming-performance-issues-on-windows-11-22h2/){:target="_blank" rel="noopener"}

> Microsoft is working on a fix for a new known issue behind lower-than-expected performance or stuttering in some games on systems running Windows 11 22H2. [...]
