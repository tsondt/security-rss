Title: NSA urges orgs to use memory-safe programming languages
Date: 2022-11-11T11:35:19+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-11-11-nsa-urges-orgs-to-use-memory-safe-programming-languages

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/11/nsa_urges_orgs_to_use/){:target="_blank" rel="noopener"}

> C/C++ on the bench, as US snoop HQ puts its trust in Rust, C#, Go, Java, Ruby, Swift The NSA has released guidance encouraging organizations to shift programming languages from the likes of C and C++ to memory-safe alternatives – namely C#, Rust, Go, Java, Ruby or Swift.... [...]
