Title: British govt is scanning all Internet devices hosted in UK
Date: 2022-11-04T15:22:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-04-british-govt-is-scanning-all-internet-devices-hosted-in-uk

[Source](https://www.bleepingcomputer.com/news/security/british-govt-is-scanning-all-internet-devices-hosted-in-uk/){:target="_blank" rel="noopener"}

> The United Kingdom's National Cyber Security Centre (NCSC), the government agency that leads the country's cyber security mission, is now scanning all Internet-exposed devices hosted in the UK for vulnerabilities. [...]
