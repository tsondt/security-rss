Title: Mastodon users vulnerable to password-stealing attacks
Date: 2022-11-15T15:39:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-15-mastodon-users-vulnerable-to-password-stealing-attacks

[Source](https://portswigger.net/daily-swig/mastodon-users-vulnerable-to-password-stealing-attacks){:target="_blank" rel="noopener"}

> Patched bug could have leaked credentials [...]
