Title: Friday Squid Blogging: Squid Brains
Date: 2022-11-18T22:12:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-11-18-friday-squid-blogging-squid-brains

[Source](https://www.schneier.com/blog/archives/2022/11/friday-squid-blogging-squid-brains.html){:target="_blank" rel="noopener"}

> Researchers have new evidence of how squid brains develop : Researchers from the FAS Center for Systems Biology describe how they used a new live-imaging technique to watch neurons being created in the embryo in almost real-time. They were then able to track those cells through the development of the nervous system in the retina. What they saw surprised them. The neural stem cells they tracked behaved eerily similar to the way these cells behave in vertebrates during the development of their nervous system. It suggests that vertebrates and cephalopods, despite diverging from each other 500 million years ago, not [...]
