Title: US Treasury thwarts DDoS attack from Russian Killnet group
Date: 2022-11-02T20:45:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-02-us-treasury-thwarts-ddos-attack-from-russian-killnet-group

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/02/killnet_us_treasury_ddos/){:target="_blank" rel="noopener"}

> Yet another pathetic 'stunt' from pro-Kremlin criminals The US Treasury Department has thwarted a distributed denial of service (DDoS) attack that officials attributed to Russian hacktivist group Killnet.... [...]
