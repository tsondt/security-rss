Title: An $8 mess — Twitter Blue 'verified' accounts push crypto scams
Date: 2022-11-10T07:33:40-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-11-10-an-8-mess-twitter-blue-verified-accounts-push-crypto-scams

[Source](https://www.bleepingcomputer.com/news/security/an-8-mess-twitter-blue-verified-accounts-push-crypto-scams/){:target="_blank" rel="noopener"}

> Twitter has officially rolled out its Twitter Blue program for an $8 monthly fee that confers upon the Tweeter multiple benefits, including the much-sought blue badge. But, all this has led to its own set of problems, such as threat actors now impersonating famous people and still being granted a "verified" status. [...]
