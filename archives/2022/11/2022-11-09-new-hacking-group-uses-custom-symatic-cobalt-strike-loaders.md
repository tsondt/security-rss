Title: New hacking group uses custom 'Symatic' Cobalt Strike loaders
Date: 2022-11-09T14:15:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-09-new-hacking-group-uses-custom-symatic-cobalt-strike-loaders

[Source](https://www.bleepingcomputer.com/news/security/new-hacking-group-uses-custom-symatic-cobalt-strike-loaders/){:target="_blank" rel="noopener"}

> A previously unknown Chinese APT (advanced persistent threat) hacking group dubbed 'Earth Longzhi' targets organizations in East Asia, Southeast Asia, and Ukraine. [...]
