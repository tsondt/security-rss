Title: Elon Musk to abused Twitter users: Your tormentors are coming back
Date: 2022-11-25T05:16:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-25-elon-musk-to-abused-twitter-users-your-tormentors-are-coming-back

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/25/twitter_suspeded_account_amnesty/){:target="_blank" rel="noopener"}

> Promises restoration of suspended accounts, despite previous pledge to do no such thing Twitter CEO Elon Musk has decided to allow suspended accounts back onto the micro-blogging service.... [...]
