Title: International summit agrees crack down on crypto to combat ransomware
Date: 2022-11-03T16:45:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-03-international-summit-agrees-crack-down-on-crypto-to-combat-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/03/ransomware_summit_cryptocurrency/){:target="_blank" rel="noopener"}

> Commitments include international wallet info sharing, KYC requirements, and an AML crackdown The White House's second International Counter Ransomware Initiative summit has concluded, and this year the 36-nation group has made clear it intends to crack down on how cryptocurrencies are used to finance ransomware operations.... [...]
