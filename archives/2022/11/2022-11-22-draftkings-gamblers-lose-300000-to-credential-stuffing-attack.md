Title: DraftKings gamblers lose $300,000 to credential stuffing attack
Date: 2022-11-22T23:30:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-22-draftkings-gamblers-lose-300000-to-credential-stuffing-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/22/draftkings_credential_stuffing_attack/){:target="_blank" rel="noopener"}

> Users of the sports betting site rolled the dice on reusing passwords and lost A credential stuffing attack over the weekend that affected sports betting biz DraftKings resulted in as much as $300,000 being stolen from customer accounts.... [...]
