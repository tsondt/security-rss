Title: Microsoft's attempts to harden Kerberos authentication broke it on Windows Servers
Date: 2022-11-21T23:00:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-21-microsofts-attempts-to-harden-kerberos-authentication-broke-it-on-windows-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/21/microsoft_kerberos_fix_windows/){:target="_blank" rel="noopener"}

> Emergency out-of-band updates to the rescue Microsoft is rolling out fixes for problems with the Kerberos network authentication protocol on Windows Server after it was broken by November Patch Tuesday updates.... [...]
