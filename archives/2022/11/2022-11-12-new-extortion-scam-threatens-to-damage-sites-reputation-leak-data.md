Title: New extortion scam threatens to damage sites’ reputation, leak data
Date: 2022-11-12T11:10:20-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-12-new-extortion-scam-threatens-to-damage-sites-reputation-leak-data

[Source](https://www.bleepingcomputer.com/news/security/new-extortion-scam-threatens-to-damage-sites-reputation-leak-data/){:target="_blank" rel="noopener"}

> An active extortion scam is targeting website owners and admins worldwide, claiming to have hacked their servers and demanding $2,500 not to leak data. [...]
