Title: Influencer 'Hushpuppi' gets 11 years in prison for cyber fraud
Date: 2022-11-08T09:39:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-11-08-influencer-hushpuppi-gets-11-years-in-prison-for-cyber-fraud

[Source](https://www.bleepingcomputer.com/news/security/influencer-hushpuppi-gets-11-years-in-prison-for-cyber-fraud/){:target="_blank" rel="noopener"}

> An Instagram influencer known as 'Hushpuppi' has been sentenced to 11 years in prison for conspiring to launder tens of millions of USD from business email compromise (BEC) scams and various cyber schemes. [...]
