Title: Reducing the risk of cloud attack
Date: 2022-11-04T12:28:06+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2022-11-04-reducing-the-risk-of-cloud-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/04/reducing_the_risk_of_cloud/){:target="_blank" rel="noopener"}

> Exploring the top five cloud threats and how to make them evaporate Webinar The Charles Dickens novel "A Tale of Two Cities" famously begins with the line 'It was the best of times, it was the worst of times.' It's a quotation which could easily be applied to the rapid growth of cloud application and service provision, and the accompanying threats to cyber security which have expanded in parallel.... [...]
