Title: Multi-factor auth fatigue is real – and it's why you may be in the headlines next
Date: 2022-11-03T20:45:15+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-03-multi-factor-auth-fatigue-is-real-and-its-why-you-may-be-in-the-headlines-next

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/03/mfa_fatigue_enterprise_threat/){:target="_blank" rel="noopener"}

> Overwhelmed by waves of push notifications, worn-down users inadvertently let the bad guys in Analysis The September cyberattack on ride-hailing service Uber began when a criminal bought the stolen credentials of a company contractor on the dark web.... [...]
