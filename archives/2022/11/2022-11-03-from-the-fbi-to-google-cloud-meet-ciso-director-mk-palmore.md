Title: From the FBI to Google Cloud, meet CISO Director MK Palmore
Date: 2022-11-03T16:00:00+00:00
Author: Google Cloud Content & Editorial
Category: GCP Security
Tags: Identity & Security;Inside Google Cloud
Slug: 2022-11-03-from-the-fbi-to-google-cloud-meet-ciso-director-mk-palmore

[Source](https://cloud.google.com/blog/topics/inside-google-cloud/meet-the-people-of-google-cloud-mk-palmore/){:target="_blank" rel="noopener"}

> Editor's note : MK Palmore is a director in Google Cloud's Office of the Chief Information Security Officer, or CISO, a group that works with private- and public-sector organizations to understand and guard against cybersecurity threats. He came to Google Cloud in 2021 from Palo Alto Networks, which he joined following three decades in the Marines and the FBI. Do you see a common thread in your career? I've always wanted to be a part of something bigger than myself. I grew up in Washington, D.C., where I rode the Metro bus daily and had a front row seat to [...]
