Title: TikTok NSFW if you work for the South Dakota government
Date: 2022-11-30T11:31:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-11-30-tiktok-nsfw-if-you-work-for-the-south-dakota-government

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/30/tiktok_nsfw_if_you_work/){:target="_blank" rel="noopener"}

> Governor bans platform and website from all state-owned devices that can connect to the internet The governor of South Dakota issued an executive order on Tuesday banning the use of Chinese social media platform TikTok for state government agencies, employees and contractors on state devices.... [...]
