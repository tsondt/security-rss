Title: Google to roll out Privacy Sandbox on Android 13 starting early 2023
Date: 2022-11-15T13:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-11-15-google-to-roll-out-privacy-sandbox-on-android-13-starting-early-2023

[Source](https://www.bleepingcomputer.com/news/security/google-to-roll-out-privacy-sandbox-on-android-13-starting-early-2023/){:target="_blank" rel="noopener"}

> Google announced today that they will begin rolling out the Privacy Sandbox system on a limited number of Android 13 devices starting in early 2023. [...]
