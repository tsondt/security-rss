Title: Dropbox admits 130 of its private GitHub repos were copied after phishing attack
Date: 2022-11-01T23:52:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-01-dropbox-admits-130-of-its-private-github-repos-were-copied-after-phishing-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/01/dropbox_phishing_code_leak/){:target="_blank" rel="noopener"}

> Personal info and data safe, stolen code not critical, apparently Dropbox has said it was successfully phished, resulting in someone copying 130 of its private GitHub code repositories and swiping some of its secret API credentials.... [...]
