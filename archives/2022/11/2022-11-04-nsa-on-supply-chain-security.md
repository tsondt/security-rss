Title: NSA on Supply Chain Security
Date: 2022-11-04T14:16:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;infrastructure;NSA;operational security;reports;supply chain
Slug: 2022-11-04-nsa-on-supply-chain-security

[Source](https://www.schneier.com/blog/archives/2022/11/nsa-on-supply-chain-security.html){:target="_blank" rel="noopener"}

> The NSA (together with CISA) has published a long report on supply-chain security: “ Securing the Software Supply Chain: Recommended Practices Guide for Suppliers. “: Prevention is often seen as the responsibility of the software developer, as they are required to securely develop and deliver code, verify third party components, and harden the build environment. But the supplier also holds a critical responsibility in ensuring the security and integrity of our software. After all, the software vendor is responsible for liaising between the customer and software developer. It is through this relationship that additional security features can be applied via [...]
