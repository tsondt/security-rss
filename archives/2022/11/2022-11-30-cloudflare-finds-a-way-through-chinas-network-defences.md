Title: Cloudflare finds a way through China's network defences
Date: 2022-11-30T04:58:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-30-cloudflare-finds-a-way-through-chinas-network-defences

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/30/cloudflare_china_networking/){:target="_blank" rel="noopener"}

> Teams with locals to allow consistent security policy to make it through the Great Firewall Cloudflare has found a way to extend some of its services across the Great Firewall and into mainland China.... [...]
