Title: Using Wi-FI to See through Walls
Date: 2022-11-08T12:15:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;drones;privacy;surveillance;Wi-Fi
Slug: 2022-11-08-using-wi-fi-to-see-through-walls

[Source](https://www.schneier.com/blog/archives/2022/11/using-wi-fi-to-see-through-walls.html){:target="_blank" rel="noopener"}

> This technique measures device response time to determine distance: The scientists tested the exploit by modifying an off-the-shelf drone to create a flying scanning device, the Wi-Peep. The robotic aircraft sends several messages to each device as it flies around, establishing the positions of devices in each room. A thief using the drone could find vulnerable areas in a home or office by checking for the absence of security cameras and other signs that a room is monitored or occupied. It could also be used to follow a security guard, or even to help rival hotels spy on each other [...]
