Title: U.S. charges Russian suspects with operating Z-Library e-Book site
Date: 2022-11-17T08:04:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-11-17-us-charges-russian-suspects-with-operating-z-library-e-book-site

[Source](https://www.bleepingcomputer.com/news/security/us-charges-russian-suspects-with-operating-z-library-e-book-site/){:target="_blank" rel="noopener"}

> Anton Napolsky (33) and Valeriia Ermakova (27), two Russian nationals, were charged with intellectual property crimes linked to Z-Library, a pirate online eBook repository. [...]
