Title: Export historical Security Hub findings to an S3 bucket to enable complex analytics
Date: 2022-11-01T18:58:06+00:00
Author: Jonathan Nguyen
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;EventBridge;lambda;S3;Security Blog;securityhub;stepfunctions
Slug: 2022-11-01-export-historical-security-hub-findings-to-an-s3-bucket-to-enable-complex-analytics

[Source](https://aws.amazon.com/blogs/security/export-historical-security-hub-findings-to-an-s3-bucket-to-enable-complex-analytics/){:target="_blank" rel="noopener"}

> AWS Security Hub is a cloud security posture management service that you can use to perform security best practice checks, aggregate alerts, and automate remediation. Security Hub has out-of-the-box integrations with many AWS services and over 60 partner products. Security Hub centralizes findings across your AWS accounts and supported AWS Regions into a single delegated [...]
