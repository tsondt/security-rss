Title: Top Zeus Botnet Suspect “Tank” Arrested in Geneva
Date: 2022-11-15T15:38:20+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;JabberZeuS;tank;Vyacheslav Igorevich Penchukov
Slug: 2022-11-15-top-zeus-botnet-suspect-tank-arrested-in-geneva

[Source](https://krebsonsecurity.com/2022/11/top-zeus-botnet-suspect-tank-arrested-in-geneva/){:target="_blank" rel="noopener"}

> Vyacheslav “Tank” Penchukov, the accused 40-year-old Ukrainian leader of a prolific cybercriminal group that stole tens of millions of dollars from small to mid-sized businesses in the United States and Europe, has been arrested in Switzerland, according to multiple sources. Wanted Ukrainian cybercrime suspect Vyacheslav “Tank” Penchukov (right) was arrested in Geneva, Switzerland. Tank was the day-to-day manager of a cybercriminal group that stole tens of millions of dollars from small to mid-sized businesses. Penchukov was named in a 2014 indictment by the U.S. Department of Justice as a top figure in the JabberZeus Crew, a small but potent cybercriminal [...]
