Title: Android phone owner accidentally finds a way to bypass lock screen
Date: 2022-11-12T10:07:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-11-12-android-phone-owner-accidentally-finds-a-way-to-bypass-lock-screen

[Source](https://www.bleepingcomputer.com/news/security/android-phone-owner-accidentally-finds-a-way-to-bypass-lock-screen/){:target="_blank" rel="noopener"}

> Cybersecurity researcher David Schütz accidentally found a way to bypass the lock screen on his fully patched Google Pixel 6 and Pixel 5 smartphones, enabling anyone with physical access to the device to unlock it. [...]
