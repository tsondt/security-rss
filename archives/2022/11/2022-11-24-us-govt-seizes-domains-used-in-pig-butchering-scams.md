Title: U.S. govt seizes domains used in 'pig butchering' scams
Date: 2022-11-24T08:12:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-11-24-us-govt-seizes-domains-used-in-pig-butchering-scams

[Source](https://www.bleepingcomputer.com/news/security/us-govt-seizes-domains-used-in-pig-butchering-scams/){:target="_blank" rel="noopener"}

> For the first time, the U.S. Department of Justice seized seven domains that hosted websites linked to "pig butchering" scams, where fraudsters trick victims of romance scams into investing in cryptocurrency via fake investment platforms. [...]
