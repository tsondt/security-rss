Title: Feds find Silk Road thief's $1b+ Bitcoin stash in popcorn tin, hidden safe
Date: 2022-11-07T22:28:44+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-07-feds-find-silk-road-thiefs-1b-bitcoin-stash-in-popcorn-tin-hidden-safe

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/07/us_bitcoin_silk_road/){:target="_blank" rel="noopener"}

> Uncle Sam follows the money... all the way to a single-board computer A crook who stole more than 50,000 Bitcoins from the dark web souk Silk Road in 2012 has pleaded guilty and lost the lot, with a stretch behind bars likely ahead of him.... [...]
