Title: Russian LockBit ransomware operator arrested in Canada
Date: 2022-11-10T10:31:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-10-russian-lockbit-ransomware-operator-arrested-in-canada

[Source](https://www.bleepingcomputer.com/news/security/russian-lockbit-ransomware-operator-arrested-in-canada/){:target="_blank" rel="noopener"}

> Europol has announced today the arrest of a Russian national linked to LockBit ransomware attacks targeting critical infrastructure organizations and high-profile companies worldwide. [...]
