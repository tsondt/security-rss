Title: AWS Digital Sovereignty Pledge: Control without compromise
Date: 2022-11-27T23:04:23+00:00
Author: Matt Garman
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Digital Sovereignty Pledge;AWS Nitro System;Data protection;Digital Sovereignty;EU Data Protection;Security Blog;Sovereign-by-design
Slug: 2022-11-27-aws-digital-sovereignty-pledge-control-without-compromise

[Source](https://aws.amazon.com/blogs/security/aws-digital-sovereignty-pledge-control-without-compromise/){:target="_blank" rel="noopener"}

> French | German | Italian | Japanese | Korean We’ve always believed that for the cloud to realize its full potential it would be essential that customers have control over their data. Giving customers this sovereignty has been a priority for AWS since the very beginning when we were the only major cloud provider to [...]
