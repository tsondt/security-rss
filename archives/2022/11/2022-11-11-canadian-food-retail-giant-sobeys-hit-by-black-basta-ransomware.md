Title: Canadian food retail giant Sobeys hit by Black Basta ransomware
Date: 2022-11-11T13:38:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-11-canadian-food-retail-giant-sobeys-hit-by-black-basta-ransomware

[Source](https://www.bleepingcomputer.com/news/security/canadian-food-retail-giant-sobeys-hit-by-black-basta-ransomware/){:target="_blank" rel="noopener"}

> Grocery stores and pharmacies belonging to Canadian food retail giant Sobeys have been experiencing IT systems issues since last weekend. [...]
