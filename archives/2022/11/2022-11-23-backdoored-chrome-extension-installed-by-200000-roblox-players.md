Title: Backdoored Chrome extension installed by 200,000 Roblox players
Date: 2022-11-23T06:07:16-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-11-23-backdoored-chrome-extension-installed-by-200000-roblox-players

[Source](https://www.bleepingcomputer.com/news/security/backdoored-chrome-extension-installed-by-200-000-roblox-players/){:target="_blank" rel="noopener"}

> Chrome browser extension 'SearchBlox' installed by more than 200,000 users has been discovered to contain a backdoor that can steal your Roblox credentials as well as your assets on Rolimons, a Roblox trading platform. [...]
