Title: Google Cloud completes Korea Financial Security Institute audit
Date: 2022-11-15T17:00:00+00:00
Author: Rani Urbas
Category: GCP Security
Tags: Identity & Security
Slug: 2022-11-15-google-cloud-completes-korea-financial-security-institute-audit

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-completes-korea-financial-security-institute-audit/){:target="_blank" rel="noopener"}

> Financial Services institutions (FSIs) are among the most heavily regulated enterprises, and often have explicit and detailed obligations to assess and audit activities they outsource. For FSIs, due diligence of a cloud provider’s controls is often an essential first step in adopting cloud services. At Google Cloud, we work closely with our customers, their regulators, and appointed independent auditors who want to verify the security, privacy, and compliance of our platform. One example is how we support our FSI customers in South Korea. The IT outsourcing regulations issued by the Korean Financial Services Commission (FSC) provide specific guidance on risk [...]
