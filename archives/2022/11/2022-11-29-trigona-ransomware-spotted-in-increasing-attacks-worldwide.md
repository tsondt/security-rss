Title: Trigona ransomware spotted in increasing attacks worldwide
Date: 2022-11-29T17:57:18-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-29-trigona-ransomware-spotted-in-increasing-attacks-worldwide

[Source](https://www.bleepingcomputer.com/news/security/trigona-ransomware-spotted-in-increasing-attacks-worldwide/){:target="_blank" rel="noopener"}

> A previously unnamed ransomware has rebranded under the name 'Trigona,' launching a new Tor negotiation site where they accept Monero as ransom payments. [...]
