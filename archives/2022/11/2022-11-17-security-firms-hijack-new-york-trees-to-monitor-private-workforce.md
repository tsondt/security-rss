Title: Security firms hijack New York trees to monitor private workforce
Date: 2022-11-17T23:09:01+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-11-17-security-firms-hijack-new-york-trees-to-monitor-private-workforce

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/17/security_firms_nyc_trees/){:target="_blank" rel="noopener"}

> Employee management tech raises eyebrows in the Big Apple Private security firms in New York City have co-opted public resources – specifically trees – to track their guards as they make their rounds.... [...]
