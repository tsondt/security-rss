Title: Charles V of Spain Secret Code Cracked
Date: 2022-11-29T12:19:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptanalysis;encryption;history of cryptography;Spain
Slug: 2022-11-29-charles-v-of-spain-secret-code-cracked

[Source](https://www.schneier.com/blog/archives/2022/11/charles-v-of-spain-secret-code-cracked.html){:target="_blank" rel="noopener"}

> Diplomatic code cracked after 500 years: In painstaking work backed by computers, Pierrot found “distinct families” of about 120 symbols used by Charles V. “Whole words are encrypted with a single symbol” and the emperor replaced vowels coming after consonants with marks, she said, an inspiration probably coming from Arabic. In another obstacle, he used meaningless symbols to mislead any adversary trying to decipher the message. The breakthrough came in June when Pierrot managed to make out a phrase in the letter, and the team then cracked the code with the help of Camille Desenclos, a historian. “It was painstaking [...]
