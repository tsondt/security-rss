Title: Russian cybergangs stole over 50 million passwords this year
Date: 2022-11-23T06:02:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-23-russian-cybergangs-stole-over-50-million-passwords-this-year

[Source](https://www.bleepingcomputer.com/news/security/russian-cybergangs-stole-over-50-million-passwords-this-year/){:target="_blank" rel="noopener"}

> At least 34 distinct Russian-speaking cybercrime groups using info-stealing malware like Raccoon and Redline have collectively stolen 50,350,000 account passwords from over 896,000 individual infections from January to July 2022. [...]
