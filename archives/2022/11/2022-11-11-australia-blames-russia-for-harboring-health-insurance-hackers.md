Title: Australia blames Russia for harboring health insurance hackers
Date: 2022-11-11T05:30:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-11-australia-blames-russia-for-harboring-health-insurance-hackers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/11/russia_named_medibank_hack_source/){:target="_blank" rel="noopener"}

> Crims accessed 10 million customer records and are releasing intimate medical details The Australian Federal Police (AFP) has pointed to Russia as the location of the attackers who breached local health insurer Medibank, accessed almost ten million customer records, and in recent days dumped some customer data onto the dark web.... [...]
