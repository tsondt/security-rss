Title: A roadmap to better cyber security training
Date: 2022-11-10T09:00:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-11-10-a-roadmap-to-better-cyber-security-training

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/10/a_roadmap_to_better_cyber/){:target="_blank" rel="noopener"}

> SANS courses show you what’s useful and NICE Sponsored Post It's a common problem when it comes to finding a new job or landing that all important promotion. You need to upgrade your CV to show some knowledge and experience of systems, tools and frameworks that your current role doesn't require but the next step up the ladder does. But how do you learn what you need if you're current role focuses on a different set of priorities, or even know what areas of speciality different organisations prize most highly in the first place?... [...]
