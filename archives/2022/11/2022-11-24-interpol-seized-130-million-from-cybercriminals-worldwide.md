Title: Interpol seized $130 million from cybercriminals worldwide
Date: 2022-11-24T11:11:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-24-interpol-seized-130-million-from-cybercriminals-worldwide

[Source](https://www.bleepingcomputer.com/news/security/interpol-seized-130-million-from-cybercriminals-worldwide/){:target="_blank" rel="noopener"}

> INTERPOL has announced the seizure of $130,000,000 million worth of money and virtual assets linked to various cybercrimes and money laundering operations. [...]
