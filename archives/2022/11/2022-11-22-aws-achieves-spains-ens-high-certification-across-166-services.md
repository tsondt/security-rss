Title: AWS achieves Spain’s ENS High certification across 166 services
Date: 2022-11-22T16:11:10+00:00
Author: Daniel Fuertes
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS security;CCN;Certificate;Certification;Compliance;cybersecurity;ENS;España;Esquema Nacional de Seguridad;High;Public Sector;report;Security Blog;Spain
Slug: 2022-11-22-aws-achieves-spains-ens-high-certification-across-166-services

[Source](https://aws.amazon.com/blogs/security/aws-achieves-spains-ens-high-certification-across-166-services/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is committed to bringing additional services and AWS Regions into the scope of our Esquema Nacional de Seguridad (ENS) High certification to help customers meet their regulatory needs. ENS is Spain’s National Security Framework. The ENS certification is regulated under the Spanish Royal Decree 3/2010 and is a compulsory requirement for [...]
