Title: Google looking outside the usual channels to fix security skills gap
Date: 2022-11-20T09:01:27+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-20-google-looking-outside-the-usual-channels-to-fix-security-skills-gap

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/20/google_cisco_diversity_inclusion/){:target="_blank" rel="noopener"}

> 'If your input continues to be monoculture, you can expect the same outcomes' Cybersecurity moves fast. New and bigger threats emerge all the time across an ever-expanding attack surface and there's not enough people to fill vacant jobs.... [...]
