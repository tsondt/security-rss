Title: Ransomware detection with Wazuh SIEM and XDR platform
Date: 2022-11-29T10:05:10-05:00
Author: Sponsored by Wazuh
Category: BleepingComputer
Tags: Security
Slug: 2022-11-29-ransomware-detection-with-wazuh-siem-and-xdr-platform

[Source](https://www.bleepingcomputer.com/news/security/ransomware-detection-with-wazuh-siem-and-xdr-platform/){:target="_blank" rel="noopener"}

> Wazuh is a free, open source SIEM/XDR solution with more than 10 million annual downloads. Learn more about how Wazuh can help protect your organization against the ever-evolving tactics of ransomware. [...]
