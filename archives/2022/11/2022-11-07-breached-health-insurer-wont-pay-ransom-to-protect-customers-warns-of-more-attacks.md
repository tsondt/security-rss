Title: Breached health insurer won't pay ransom to protect customers, warns of more attacks
Date: 2022-11-07T01:45:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-07-breached-health-insurer-wont-pay-ransom-to-protect-customers-warns-of-more-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/07/medibank_breach_n0_ransom_payment/){:target="_blank" rel="noopener"}

> Australia's Medibank uses a government-approved Band-Aid to cover a gaping 10-milion-record wound Australian health insurer Medibank – which spent October discovering a security incident was worse than it first thought – has announced it will not pay a ransom to attackers that made off with personal info describing nearly ten million customers.... [...]
