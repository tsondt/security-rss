Title: International cops arrest hundreds of fraudsters, money launderers and cocaine kingpins
Date: 2022-11-29T06:01:22+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-29-international-cops-arrest-hundreds-of-fraudsters-money-launderers-and-cocaine-kingpins

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/29/europol_fraud_drug_arrests/){:target="_blank" rel="noopener"}

> $155,000-a-month lifestyle ends in cuffs for suspected crim Europol has arrested hundreds of fraudsters, money launderers and cocaine kingpins, and shut down thousands of websites selling pirated and counterfeit products in a series of raids over the past month.... [...]
