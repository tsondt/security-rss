Title: Google Chrome extension used to steal cryptocurrency, passwords
Date: 2022-11-21T13:24:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Google
Slug: 2022-11-21-google-chrome-extension-used-to-steal-cryptocurrency-passwords

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-extension-used-to-steal-cryptocurrency-passwords/){:target="_blank" rel="noopener"}

> An information-stealing Google Chrome browser extension named 'VenomSoftX' is being deployed by Windows malware to steal cryptocurrency and clipboard contents as users browse the web. [...]
