Title: Considerations for security operations in the cloud
Date: 2022-11-18T21:42:59+00:00
Author: Stuart Gregg
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;architect;centralization;decentralization;operations;SecOps;Security;Security Blog;Securityoperations;tools
Slug: 2022-11-18-considerations-for-security-operations-in-the-cloud

[Source](https://aws.amazon.com/blogs/security/considerations-for-security-operations-in-the-cloud/){:target="_blank" rel="noopener"}

> Cybersecurity teams are often made up of different functions. Typically, these can include Governance, Risk & Compliance (GRC), Security Architecture, Assurance, and Security Operations, to name a few. Each function has its own specific tasks, but works towards a common goal—to partner with the rest of the business and help teams ship and run workloads [...]
