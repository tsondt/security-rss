Title: Cloud CISO Perspectives: November 2022
Date: 2022-11-30T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Identity & Security
Slug: 2022-11-30-cloud-ciso-perspectives-november-2022

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-november-2022/){:target="_blank" rel="noopener"}

> Welcome to November’s Cloud CISO Perspectives. I’d like to celebrate the first year of the Google Cybersecurity Action Team (GCAT) and look ahead to the team’s goals for 2023. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. GCAT, one year later We launched the Google Cybersecurity Action Team in October 2021 as a premier security advisory team, with the singular mission of supporting the security and digital transformation of governments, critical [...]
