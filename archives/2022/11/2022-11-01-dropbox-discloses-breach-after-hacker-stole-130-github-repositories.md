Title: Dropbox discloses breach after hacker stole 130 GitHub repositories
Date: 2022-11-01T17:15:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-01-dropbox-discloses-breach-after-hacker-stole-130-github-repositories

[Source](https://www.bleepingcomputer.com/news/security/dropbox-discloses-breach-after-hacker-stole-130-github-repositories/){:target="_blank" rel="noopener"}

> Dropbox disclosed a security breach after threat actors stole 130 code repositories after gaining access to one of its GitHub accounts using employee credentials stolen in a phishing attack. [...]
