Title: 42,000 sites used to trap users in brand impersonation scheme
Date: 2022-11-14T11:59:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-11-14-42000-sites-used-to-trap-users-in-brand-impersonation-scheme

[Source](https://www.bleepingcomputer.com/news/security/42-000-sites-used-to-trap-users-in-brand-impersonation-scheme/){:target="_blank" rel="noopener"}

> A malicious for-profit group named 'Fangxiao' has created a massive network of over 42,000 web domains that impersonate well-known brands to redirect users to sites promoting adware apps, dating sites, or 'free' giveaways. [...]
