Title: New ransomware attacks in Ukraine linked to Russian Sandworm hackers
Date: 2022-11-25T13:01:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-25-new-ransomware-attacks-in-ukraine-linked-to-russian-sandworm-hackers

[Source](https://www.bleepingcomputer.com/news/security/new-ransomware-attacks-in-ukraine-linked-to-russian-sandworm-hackers/){:target="_blank" rel="noopener"}

> New ransomware attacks targeting organizations in Ukraine first detected this Monday have been linked to the notorious Russian military threat group known as Sandworm. [...]
