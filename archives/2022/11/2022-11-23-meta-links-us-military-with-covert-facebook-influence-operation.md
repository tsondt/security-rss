Title: Meta links U.S. military with covert Facebook influence operation
Date: 2022-11-23T14:38:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-23-meta-links-us-military-with-covert-facebook-influence-operation

[Source](https://www.bleepingcomputer.com/news/security/meta-links-us-military-with-covert-facebook-influence-operation/){:target="_blank" rel="noopener"}

> Meta has removed several accounts on Facebook and Instagram associated with the U.S. military, saying they were used as part of covert influence operations targeting the Middle East and Russia. [...]
