Title: Europe calls for joint cyber defense to ward off Russia
Date: 2022-11-11T07:34:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-11-europe-calls-for-joint-cyber-defense-to-ward-off-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/11/eu_joint_cyber_defense/){:target="_blank" rel="noopener"}

> EC veep: 'Cyber is the new domain in warfare' The European Commission on Thursday proposed a cyber defense policy in response to Europe's "deteriorating security environment" since Russia illegally invaded Ukraine earlier this year.... [...]
