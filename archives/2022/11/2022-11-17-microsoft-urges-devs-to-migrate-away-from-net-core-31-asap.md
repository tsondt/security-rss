Title: Microsoft urges devs to migrate away from .NET Core 3.1 ASAP
Date: 2022-11-17T09:14:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-17-microsoft-urges-devs-to-migrate-away-from-net-core-31-asap

[Source](https://www.bleepingcomputer.com/news/security/microsoft-urges-devs-to-migrate-away-from-net-core-31-asap/){:target="_blank" rel="noopener"}

> Microsoft has urged developers still using the long-term support (LTS) release of.NET Core 3.1 to migrate to the latest.NET Core versions until it reaches the end of support (EOS) next month. [...]
