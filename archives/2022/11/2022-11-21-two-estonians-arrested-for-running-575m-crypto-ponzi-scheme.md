Title: Two Estonians arrested for running $575M crypto Ponzi scheme
Date: 2022-11-21T18:37:41-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-11-21-two-estonians-arrested-for-running-575m-crypto-ponzi-scheme

[Source](https://www.bleepingcomputer.com/news/security/two-estonians-arrested-for-running-575m-crypto-ponzi-scheme/){:target="_blank" rel="noopener"}

> Two Estonian nationals were arrested in Tallinn, Estonia, on Sunday after being indicted in the U.S. for running a massive cryptocurrency Ponzi scheme that led to losses of more than $575 million. [...]
