Title: Researchers secretly helped decrypt Zeppelin ransomware for 2 years
Date: 2022-11-18T14:54:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-18-researchers-secretly-helped-decrypt-zeppelin-ransomware-for-2-years

[Source](https://www.bleepingcomputer.com/news/security/researchers-secretly-helped-decrypt-zeppelin-ransomware-for-2-years/){:target="_blank" rel="noopener"}

> Security researchers found vulnerabilities in the encryption mechanism of the Zeppelin ransomware and exploited them to create a working decryptor they used since 2020 to help victim companies recover files without paying the attackers. [...]
