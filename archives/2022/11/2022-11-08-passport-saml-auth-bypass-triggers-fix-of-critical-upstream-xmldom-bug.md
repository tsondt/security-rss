Title: Passport-SAML auth bypass triggers fix of critical, upstream XMLDOM bug
Date: 2022-11-08T16:33:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-08-passport-saml-auth-bypass-triggers-fix-of-critical-upstream-xmldom-bug

[Source](https://portswigger.net/daily-swig/passport-saml-auth-bypass-triggers-fix-of-critical-upstream-xmldom-bug){:target="_blank" rel="noopener"}

> Rapid remedy follows reawakening of long-dormant bug threat [...]
