Title: All the US midterm-related lies to expect when you're electing
Date: 2022-11-07T21:30:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-07-all-the-us-midterm-related-lies-to-expect-when-youre-electing

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/07/us_midterms_misinformation/){:target="_blank" rel="noopener"}

> Don't like the results? The election must have been rigged Misinformation related to tomorrow's US midterm elections hasn't slowed, according to security researchers.... [...]
