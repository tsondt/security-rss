Title: Ransomware gang threatens to release stolen Medibank data
Date: 2022-11-07T12:50:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-07-ransomware-gang-threatens-to-release-stolen-medibank-data

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-threatens-to-release-stolen-medibank-data/){:target="_blank" rel="noopener"}

> A ransomware gang that some believe is a relaunch of REvil and others track as BlogXX has claimed responsibility for last month's ransomware attack against Australian health insurance provider Medibank Private Limited. [...]
