Title: Prototype pollution project yields another Parse Server RCE
Date: 2022-11-11T15:37:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-11-prototype-pollution-project-yields-another-parse-server-rce

[Source](https://portswigger.net/daily-swig/prototype-pollution-project-yields-another-parse-server-rce){:target="_blank" rel="noopener"}

> Bug emerges from ambition to find ‘end-to-end exploits beyond DoS’ [...]
