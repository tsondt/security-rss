Title: Medibank warns customers their data was leaked by ransomware gang
Date: 2022-11-09T11:43:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-09-medibank-warns-customers-their-data-was-leaked-by-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/medibank-warns-customers-their-data-was-leaked-by-ransomware-gang/){:target="_blank" rel="noopener"}

> Australian health insurance giant Medibank has warned customers that the ransomware group behind last month's breach has started to leak data stolen from its systems. [...]
