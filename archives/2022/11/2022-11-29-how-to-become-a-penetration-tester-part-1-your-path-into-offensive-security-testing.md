Title: How to become a penetration tester: Part 1 – your path into offensive security testing
Date: 2022-11-29T15:13:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-29-how-to-become-a-penetration-tester-part-1-your-path-into-offensive-security-testing

[Source](https://portswigger.net/daily-swig/how-to-become-a-penetration-tester-part-1-your-path-into-offensive-security-testing){:target="_blank" rel="noopener"}

> Fancy a career in what one practitioner described as the ‘best job in the world’? Read on to find out how... [...]
