Title: Vice Society ransomware claims attack on Cincinnati State college
Date: 2022-11-25T12:18:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-25-vice-society-ransomware-claims-attack-on-cincinnati-state-college

[Source](https://www.bleepingcomputer.com/news/security/vice-society-ransomware-claims-attack-on-cincinnati-state-college/){:target="_blank" rel="noopener"}

> The Vice Society ransomware operation has claimed responsibility for a cyberattack on Cincinnati State Technical and Community College, with the threat actors now leaking data allegedly stolen during the attack. [...]
