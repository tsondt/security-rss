Title: LockBit suspect cuffed after ransomware forces emergency services to use pen and paper
Date: 2022-11-12T08:57:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-12-lockbit-suspect-cuffed-after-ransomware-forces-emergency-services-to-use-pen-and-paper

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/12/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: CISA has a flowchart for patching, privacy campaign goes after face search engine In Brief A suspected member of the notorious international LockBit ransomware mob has been arrested – and could spend several years behind bars if convicted.... [...]
