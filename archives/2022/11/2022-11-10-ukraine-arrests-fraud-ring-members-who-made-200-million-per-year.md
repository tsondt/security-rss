Title: Ukraine arrests fraud ring members who made €200 million per year
Date: 2022-11-10T11:02:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Government
Slug: 2022-11-10-ukraine-arrests-fraud-ring-members-who-made-200-million-per-year

[Source](https://www.bleepingcomputer.com/news/security/ukraine-arrests-fraud-ring-members-who-made-200-million-per-year/){:target="_blank" rel="noopener"}

> Ukraine's cyber police and Europol have identified and arrested five key members of an international investment fraud ring estimated to have caused losses of over €200 million per year. [...]
