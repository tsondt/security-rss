Title: Russian military hackers linked to ransomware attacks in Ukraine
Date: 2022-11-10T14:47:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-10-russian-military-hackers-linked-to-ransomware-attacks-in-ukraine

[Source](https://www.bleepingcomputer.com/news/security/russian-military-hackers-linked-to-ransomware-attacks-in-ukraine/){:target="_blank" rel="noopener"}

> A series of attacks targeting transportation and logistics organizations in Ukraine and Poland with Prestige ransomware since October have been linked to an elite Russian military cyberespionage group. [...]
