Title: US bans sales of Huawei, Hikvision, ZTE, and Dahua equipment
Date: 2022-11-28T09:18:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-11-28-us-bans-sales-of-huawei-hikvision-zte-and-dahua-equipment

[Source](https://www.bleepingcomputer.com/news/security/us-bans-sales-of-huawei-hikvision-zte-and-dahua-equipment/){:target="_blank" rel="noopener"}

> The United States government, through the Federal Communications Commission (FCC), has banned the sale of equipment from Chinese telecommunications and video surveillance vendor Huawei, ZTE, Hytera, Hikvision, and Dahua due "unacceptable risks to national security". [...]
