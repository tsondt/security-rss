Title: Securing tomorrow today: Why Google now protects its internal communications from quantum threats
Date: 2022-11-18T17:00:00+00:00
Author: ISE Crypto PQC working group
Category: GCP Security
Tags: Identity & Security
Slug: 2022-11-18-securing-tomorrow-today-why-google-now-protects-its-internal-communications-from-quantum-threats

[Source](https://cloud.google.com/blog/products/identity-security/why-google-now-uses-post-quantum-cryptography-for-internal-comms/){:target="_blank" rel="noopener"}

> Editor's note : The ISE Crypto PQC working group is comprised of Google Senior Cryptography Engineers Stefan Kölbl, Rafael Misoczki, and Sophie Schmieg. When you visit a website and the URL starts with HTTPS, you’re relying on a secure public key cryptographic protocol to shield the information you share with the site from casual eavesdroppers. Public key cryptography underpins most secure communication protocols, including those we use internally at Google as part of our mission to protect our assets and our users’ data against threats. Our own internal encryption-in-transit protocol, Application Layer Transport Security (ALTS), uses public key cryptography algorithms [...]
