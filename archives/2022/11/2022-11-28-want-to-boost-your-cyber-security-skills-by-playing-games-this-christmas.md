Title: Want to boost your cyber security skills by playing games this Christmas?
Date: 2022-11-28T13:06:14+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-11-28-want-to-boost-your-cyber-security-skills-by-playing-games-this-christmas

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/28/want_to_boost_your_cyber/){:target="_blank" rel="noopener"}

> Register for this free SANS Holiday Hack Challenge to find out how Sponsored Post Christmas is a time for gift giving and spending time with your friends and family – but that doesn't have to be all. What if you could add to the fun by taking part in an entertaining free holiday-themed cyber security event that both builds your skills and gives you the chance of adding a stellar prize to the pile of gifts under your tree?... [...]
