Title: Sandworm gang launches Monster ransomware attacks on Ukraine
Date: 2022-11-29T08:30:15+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-29-sandworm-gang-launches-monster-ransomware-attacks-on-ukraine

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/29/russia_ransomboggs_ransomware_ukraine/){:target="_blank" rel="noopener"}

> The RansomBoggs campaign is the Russia-linked group’s latest assault on the smaller country The Russian criminal crew Sandworm is launching another attack against organizations in Ukraine, using a ransomware that analysts at Slovakian software company ESET are calling RansomBoggs.... [...]
