Title: Fall 2022 SOC 2 Type 2 Privacy report now available
Date: 2022-11-17T16:13:53+00:00
Author: Nimesh Ravasa
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Privacy Report;AWS SOC Reports;Security Blog
Slug: 2022-11-17-fall-2022-soc-2-type-2-privacy-report-now-available

[Source](https://aws.amazon.com/blogs/security/fall-2022-soc-2-type-2-privacy-report-now-available/){:target="_blank" rel="noopener"}

> Your privacy considerations are at the core of our compliance work at Amazon Web Services (AWS), and we are focused on the protection of your content while using AWS services. We are happy to announce that our Fall 2022 SOC 2 Type 2 Privacy report is now available. The report provides a third-party attestation of [...]
