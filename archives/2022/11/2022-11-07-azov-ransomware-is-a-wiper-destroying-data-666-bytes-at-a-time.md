Title: Azov Ransomware is a wiper, destroying data 666 bytes at a time
Date: 2022-11-07T18:13:42-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-07-azov-ransomware-is-a-wiper-destroying-data-666-bytes-at-a-time

[Source](https://www.bleepingcomputer.com/news/security/azov-ransomware-is-a-wiper-destroying-data-666-bytes-at-a-time/){:target="_blank" rel="noopener"}

> The Azov Ransomware continues to be heavily distributed worldwide, now proven to be a data wiper that intentionally destroys victims' data and infects other programs. [...]
