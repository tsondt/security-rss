Title: Using Regex to Implement Passphrases in Your Active Directory
Date: 2022-11-01T10:06:12-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-11-01-using-regex-to-implement-passphrases-in-your-active-directory

[Source](https://www.bleepingcomputer.com/news/microsoft/using-regex-to-implement-passphrases-in-your-active-directory/){:target="_blank" rel="noopener"}

> Passphrases provide a superior type of password for authentication as they allow you to create strong passwords you can remember. Furthermore, you can use regex (regular expression) to effectively help develop solid passphrases and ensure these do not contain weak elements. Let's see how. [...]
