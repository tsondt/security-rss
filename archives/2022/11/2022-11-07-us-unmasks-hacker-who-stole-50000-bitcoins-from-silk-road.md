Title: U.S. unmasks hacker who stole 50,000 bitcoins from Silk Road
Date: 2022-11-07T15:23:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Government
Slug: 2022-11-07-us-unmasks-hacker-who-stole-50000-bitcoins-from-silk-road

[Source](https://www.bleepingcomputer.com/news/security/us-unmasks-hacker-who-stole-50-000-bitcoins-from-silk-road/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice has announced today the conviction of James Zhong, a mysterious hacker who stole 50,000 bitcoins from the 'Silk Road' dark net marketplace. [...]
