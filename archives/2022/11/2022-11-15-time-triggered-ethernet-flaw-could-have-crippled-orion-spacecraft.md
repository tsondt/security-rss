Title: Time-Triggered Ethernet flaw could have crippled Orion spacecraft
Date: 2022-11-15T23:45:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-15-time-triggered-ethernet-flaw-could-have-crippled-orion-spacecraft

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/15/pcspoof_attack_nasa/){:target="_blank" rel="noopener"}

> Houston, we have a PCspooF problem A vulnerability in a networking technology widely used in space and aircraft could, if successfully exploited, have disastrous effects on these critical systems including thwarting NASA missions, according to researchers.... [...]
