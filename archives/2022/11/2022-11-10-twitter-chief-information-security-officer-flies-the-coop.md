Title: Twitter Chief Information Security Officer flies the coop
Date: 2022-11-10T16:34:09+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-11-10-twitter-chief-information-security-officer-flies-the-coop

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/10/twitter_ciso_quits/){:target="_blank" rel="noopener"}

> As social media giant grapples with Musk takeover, a safe pair of hands reaches for the door Troubled social media giant Twitter has lost the services of its chief information and security officer to cap off another chaotic week following its acquisition by Elon Musk.... [...]
