Title: How advances in email encryption bring all-out security success
Date: 2022-11-25T12:50:07+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2022-11-25-how-advances-in-email-encryption-bring-all-out-security-success

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/25/how_advances_in_email_encryption/){:target="_blank" rel="noopener"}

> Listen in to our webinar on 30th November to find out Webinar Email provides us with an infinite number of possible exchanges. We send approximately 332 billion messages a day but having so much convenience and flexibility at our fingertips also brings security risks.... [...]
