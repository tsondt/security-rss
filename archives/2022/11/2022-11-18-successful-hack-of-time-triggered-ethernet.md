Title: Successful Hack of Time-Triggered Ethernet
Date: 2022-11-18T15:04:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;embedded systems;hardware;spoofing
Slug: 2022-11-18-successful-hack-of-time-triggered-ethernet

[Source](https://www.schneier.com/blog/archives/2022/11/successful-hack-of-time-triggered-ethernet.html){:target="_blank" rel="noopener"}

> Time-triggered Ethernet (TTE) is used in spacecraft, basically to use the same hardware to process traffic with different timing and criticality. Researchers have defeated it : On Tuesday, researchers published findings that, for the first time, break TTE’s isolation guarantees. The result is PCspooF, an attack that allows a single non-critical device connected to a single plane to disrupt synchronization and communication between TTE devices on all planes. The attack works by exploiting a vulnerability in the TTE protocol. The work was completed by researchers at the University of Michigan, the University of Pennsylvania, and NASA’s Johnson Space Center. “Our [...]
