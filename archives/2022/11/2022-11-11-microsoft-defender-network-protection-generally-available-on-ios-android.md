Title: Microsoft Defender network protection generally available on iOS, Android
Date: 2022-11-11T15:01:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-11-microsoft-defender-network-protection-generally-available-on-ios-android

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-network-protection-generally-available-on-ios-android/){:target="_blank" rel="noopener"}

> Microsoft announced that the Mobile Network Protection feature is generally available to help organizations detect network weaknesses affecting Android and iOS devices running Microsoft's Defender for Endpoint (MDE) enterprise endpoint security platform. [...]
