Title: European Parliament Putin things back together after cyber attack
Date: 2022-11-24T06:03:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-24-european-parliament-putin-things-back-together-after-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/24/european_parliament_russia_ddos/){:target="_blank" rel="noopener"}

> DDoS started not long after Russia was declared a state sponsor of terrorism The European Parliament has experienced a cyber attack that started not long after it declared Russia to be a state sponsor of terrorism.... [...]
