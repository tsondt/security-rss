Title: Z-Library operators arrested, charged with criminal copyright infringement
Date: 2022-11-18T08:30:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-18-z-library-operators-arrested-charged-with-criminal-copyright-infringement

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/18/zlibrary_operators_arrested/){:target="_blank" rel="noopener"}

> There's a legal line between book borrowing and piracy Two Russian nationals accused of operating Z-Library – one of the largest online book piracy websites – have been charged with criminal copyright infringement, wire fraud and money laundering.... [...]
