Title: Fake MSI Afterburner targets Windows gamers with miners, info-stealers
Date: 2022-11-23T13:56:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-23-fake-msi-afterburner-targets-windows-gamers-with-miners-info-stealers

[Source](https://www.bleepingcomputer.com/news/security/fake-msi-afterburner-targets-windows-gamers-with-miners-info-stealers/){:target="_blank" rel="noopener"}

> Windows gamers and power users are being targeted by fake MSI Afterburner download portals to infect users with cryptocurrency miners and the RedLine information-stealing malware. [...]
