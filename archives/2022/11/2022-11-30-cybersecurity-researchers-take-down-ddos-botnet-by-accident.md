Title: Cybersecurity researchers take down DDoS botnet by accident
Date: 2022-11-30T15:12:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-30-cybersecurity-researchers-take-down-ddos-botnet-by-accident

[Source](https://www.bleepingcomputer.com/news/security/cybersecurity-researchers-take-down-ddos-botnet-by-accident/){:target="_blank" rel="noopener"}

> While analyzing its capabilities, Akamai researchers have accidentally taken down a cryptomining botnet that was also used for distributed denial-of-service (DDoS) attacks. [...]
