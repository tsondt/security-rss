Title: Announcing Assured Workloads for Israel in Preview
Date: 2022-11-09T07:00:00+00:00
Author: Ben Hagai
Category: GCP Security
Tags: Identity & Security
Slug: 2022-11-09-announcing-assured-workloads-for-israel-in-preview

[Source](https://cloud.google.com/blog/products/identity-security/introducing-assured-workloads-for-israel-in-preview/){:target="_blank" rel="noopener"}

> In October, we announced the opening of a new Google Cloud region in Israel. The region, me-west1, joins our global network of cloud regions delivering high-performance, low-latency access to cloud services for customers of all sizes and across industries. As we support customers in this new region, we want to help provide the confidence that when you use our services, you can have control, transparency, and are able to support your compliance and residency requirements. Today, at the Israel Cloud Summit, we are excited to announce the public Preview of Assured Workloads for Israel to help address these needs. Assured [...]
