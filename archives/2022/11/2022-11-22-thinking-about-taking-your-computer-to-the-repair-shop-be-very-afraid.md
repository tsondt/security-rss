Title: Thinking about taking your computer to the repair shop? Be very afraid
Date: 2022-11-22T20:51:52+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;computer repairs;data security;privacy
Slug: 2022-11-22-thinking-about-taking-your-computer-to-the-repair-shop-be-very-afraid

[Source](https://arstechnica.com/?p=1899664){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) If you’ve ever worried about the privacy of your sensitive data when seeking a computer or phone repair, a new study suggests you have good reason. It found that privacy violations occurred at least 50 percent of the time, not surprisingly with female customers bearing the brunt. Researchers at University of Guelph in Ontario, Canada, recovered logs from laptops after receiving overnight repairs from 12 commercial shops. The logs showed that technicians from six of the locations had accessed personal data and that two of those shops also copied data onto a personal device. Devices belonging [...]
