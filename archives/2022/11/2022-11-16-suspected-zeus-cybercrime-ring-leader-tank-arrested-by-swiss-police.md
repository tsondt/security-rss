Title: Suspected Zeus cybercrime ring leader ‘Tank’ arrested by Swiss police
Date: 2022-11-16T14:33:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-16-suspected-zeus-cybercrime-ring-leader-tank-arrested-by-swiss-police

[Source](https://www.bleepingcomputer.com/news/security/suspected-zeus-cybercrime-ring-leader-tank-arrested-by-swiss-police/){:target="_blank" rel="noopener"}

> Vyacheslav Igorevich Penchukov, also known as Tank and one of the leaders of the notorious JabberZeus cybercrime gang, was arrested in Geneva last month. [...]
