Title: All Day DevOps: Third of Log4j downloads still pull vulnerable version despite threat of supply chain attacks
Date: 2022-11-14T16:16:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-14-all-day-devops-third-of-log4j-downloads-still-pull-vulnerable-version-despite-threat-of-supply-chain-attacks

[Source](https://portswigger.net/daily-swig/all-day-devops-third-of-log4j-downloads-still-pull-vulnerable-version-despite-threat-of-supply-chain-attacks){:target="_blank" rel="noopener"}

> AppSec engineer keynote says Log4j revealed lessons were not learned from the Equifax breach [...]
