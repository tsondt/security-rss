Title: NordVPN Black Friday deal: Up to 68% off a 27-month VPN subscription
Date: 2022-11-23T09:57:45-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-23-nordvpn-black-friday-deal-up-to-68-off-a-27-month-vpn-subscription

[Source](https://www.bleepingcomputer.com/news/security/nordvpn-black-friday-deal-up-to-68-percent-off-a-27-month-vpn-subscription/){:target="_blank" rel="noopener"}

> NordVPN's Black Friday deal is live with up to 68% off and 3 extra months for free on 1-year or 2-year subscriptions to the NordVPN VPN service. [...]
