Title: Red Cross seeks digital equivalent of its emblems to mark some tech as off-limits in war
Date: 2022-11-07T06:01:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-07-red-cross-seeks-digital-equivalent-of-its-emblems-to-mark-some-tech-as-off-limits-in-war

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/07/red_cross_digital_emblem/){:target="_blank" rel="noopener"}

> Suggests tweaks to IP semantics as one way to identify protected tech and traffic The International Committee of the Red Cross (ICRC) wants to devise a digital equivalent of its emblems (the red cross and red crescent), to signify that certain digital resources are protected and must not be targeted during cyberwarfare.... [...]
