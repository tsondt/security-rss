Title: Apps with over 3 million installs leak 'Admin' search API keys
Date: 2022-11-21T10:04:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-11-21-apps-with-over-3-million-installs-leak-admin-search-api-keys

[Source](https://www.bleepingcomputer.com/news/security/apps-with-over-3-million-installs-leak-admin-search-api-keys/){:target="_blank" rel="noopener"}

> Researchers discovered 1,550 mobile apps leaking Algolia API keys, risking the exposure of sensitive internal services and stored user information. [...]
