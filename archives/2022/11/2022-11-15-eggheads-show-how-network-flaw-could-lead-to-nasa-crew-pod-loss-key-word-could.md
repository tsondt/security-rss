Title: Eggheads show how network flaw could lead to NASA crew pod loss. Key word: Could
Date: 2022-11-15T23:45:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-15-eggheads-show-how-network-flaw-could-lead-to-nasa-crew-pod-loss-key-word-could

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/15/pcspoof_tte_flaw/){:target="_blank" rel="noopener"}

> Houston, we have a PCspooF problem A vulnerability in network technology widely used in space and aircraft could, if successfully exploited, have disastrous effects on those critical systems, according to academics.... [...]
