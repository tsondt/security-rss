Title: Microsoft sued for open-source piracy through GitHub Copilot
Date: 2022-11-05T10:07:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-05-microsoft-sued-for-open-source-piracy-through-github-copilot

[Source](https://www.bleepingcomputer.com/news/security/microsoft-sued-for-open-source-piracy-through-github-copilot/){:target="_blank" rel="noopener"}

> Programmer and lawyer Matthew Butterick has sued Microsoft, GitHub, and OpenAI, alleging that GitHub's Copilot violates the terms of open-source licenses and infringes the rights of code authors. [...]
