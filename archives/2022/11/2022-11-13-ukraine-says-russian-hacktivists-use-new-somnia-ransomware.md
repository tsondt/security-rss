Title: Ukraine says Russian hacktivists use new Somnia ransomware
Date: 2022-11-13T10:06:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-13-ukraine-says-russian-hacktivists-use-new-somnia-ransomware

[Source](https://www.bleepingcomputer.com/news/security/ukraine-says-russian-hacktivists-use-new-somnia-ransomware/){:target="_blank" rel="noopener"}

> Russian hacktivists have infected multiple organizations in Ukraine with a new ransomware strain called 'Somnia,' encrypting their systems and causing operational problems. [...]
