Title: Google pushes emergency Chrome update to fix 8th zero-day in 2022
Date: 2022-11-25T02:28:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-11-25-google-pushes-emergency-chrome-update-to-fix-8th-zero-day-in-2022

[Source](https://www.bleepingcomputer.com/news/security/google-pushes-emergency-chrome-update-to-fix-8th-zero-day-in-2022/){:target="_blank" rel="noopener"}

> Google has released an emergency security update for the desktop version of the Chrome web browser, addressing the eighth zero-day vulnerability exploited in attacks this year. [...]
