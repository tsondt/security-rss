Title: Introducing new, faster search and investigative experience in Chronicle Security Operations
Date: 2022-11-16T17:00:00+00:00
Author: Spencer Lichtenstein
Category: GCP Security
Tags: Identity & Security
Slug: 2022-11-16-introducing-new-faster-search-and-investigative-experience-in-chronicle-security-operations

[Source](https://cloud.google.com/blog/products/identity-security/introducing-new-search-and-investigative-experience-in-chronicle-security-operations/){:target="_blank" rel="noopener"}

> In cybersecurity, speed matters. Whether a security analyst is trying to understand the details of an alert that was triggered by an indicator of compromise (IoC), or find additional context for a suspicious asset, speed is often the critical factor that will help thwart a cyberattack before threat actors are able to inflict damage. With speed in mind, we are pleased to announce the general availability of our new investigative experience in Chronicle Security Operations. We are continuing to deliver on our mission to bring the power of Google to security operations and are raising the bar for search and [...]
