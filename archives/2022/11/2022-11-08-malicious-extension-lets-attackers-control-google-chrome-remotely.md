Title: Malicious extension lets attackers control Google Chrome remotely
Date: 2022-11-08T16:37:27-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-08-malicious-extension-lets-attackers-control-google-chrome-remotely

[Source](https://www.bleepingcomputer.com/news/security/malicious-extension-lets-attackers-control-google-chrome-remotely/){:target="_blank" rel="noopener"}

> A new Chrome browser botnet named 'Cloud9' has been discovered in the wild using malicious extensions to steal online accounts, log keystrokes, inject ads and malicious JS code, and enlist the victim's browser in DDoS attacks. [...]
