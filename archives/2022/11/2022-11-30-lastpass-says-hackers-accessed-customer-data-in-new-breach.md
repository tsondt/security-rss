Title: Lastpass says hackers accessed customer data in new breach
Date: 2022-11-30T16:24:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-30-lastpass-says-hackers-accessed-customer-data-in-new-breach

[Source](https://www.bleepingcomputer.com/news/security/lastpass-says-hackers-accessed-customer-data-in-new-breach/){:target="_blank" rel="noopener"}

> LastPass says unknown attackers breached its cloud storage using information stolen during a previous security incident from August 2022. [...]
