Title: Government by Gmail catches up with UK minister... who is reappointed anyway
Date: 2022-11-01T14:30:05+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-11-01-government-by-gmail-catches-up-with-uk-minister-who-is-reappointed-anyway

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/01/suella_braverman_gmail_breach/){:target="_blank" rel="noopener"}

> Home Secretary 'nominally in charge' of nation's security apologizes for breach of tech protocols The UK's Home Secretary – the minister in charge of policing and internal security – has been forced to apologize for breaching IT security protocols in government.... [...]
