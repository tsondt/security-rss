Title: Double-check demand payment emails from law firms: Convincing fakes surface
Date: 2022-11-04T18:30:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-04-double-check-demand-payment-emails-from-law-firms-convincing-fakes-surface

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/04/crimson_kingsnake_bec_scam/){:target="_blank" rel="noopener"}

> Crimson Kingsnake impersonates legit attorneys, fakes email threads from your colleagues in far-reaching BEC campaign A new threat group called Crimson Kingsnake is impersonating real law companies and debt recovery services to intimidate businessess into paying bogus overdue invoices.... [...]
