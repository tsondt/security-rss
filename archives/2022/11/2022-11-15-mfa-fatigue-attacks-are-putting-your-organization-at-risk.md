Title: MFA Fatigue attacks are putting your organization at risk
Date: 2022-11-15T10:07:14-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2022-11-15-mfa-fatigue-attacks-are-putting-your-organization-at-risk

[Source](https://www.bleepingcomputer.com/news/security/mfa-fatigue-attacks-are-putting-your-organization-at-risk/){:target="_blank" rel="noopener"}

> A common threat targeting businesses is MFA fatigue attacks—a technique where a cybercriminal attempts to gain access to a corporate network by bombarding a user with MFA prompts. This article includes some measures you can implement to prevent these types of attacks. [...]
