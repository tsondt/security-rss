Title: Attackers bypass Coinbase and MetaMask 2FA via TeamViewer, fake support chat
Date: 2022-11-21T17:16:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-21-attackers-bypass-coinbase-and-metamask-2fa-via-teamviewer-fake-support-chat

[Source](https://www.bleepingcomputer.com/news/security/attackers-bypass-coinbase-and-metamask-2fa-via-teamviewer-fake-support-chat/){:target="_blank" rel="noopener"}

> A crypto-stealing phishing campaign is underway to bypass multi-factor authentication and gain access to accounts on Coinbase, MetaMask, Crypto.com, and KuCoin and steal cryptocurrency. [...]
