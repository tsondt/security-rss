Title: Enhance your privacy with this second phone number app deal
Date: 2022-11-08T07:21:42-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-08-enhance-your-privacy-with-this-second-phone-number-app-deal

[Source](https://www.bleepingcomputer.com/news/security/enhance-your-privacy-with-this-second-phone-number-app-deal/){:target="_blank" rel="noopener"}

> Protecting your privacy while staying in touch can be a difficult problem to solve. This second-phone app helps you solve it with a lifetime subscription for $24.99, 83% off the $150 MSRP. [...]
