Title: New ebook: CJ Moses’ Security Predictions in 2023 and Beyond
Date: 2022-11-16T21:05:07+00:00
Author: CJ Moses
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;eBook;Security;Security Blog
Slug: 2022-11-16-new-ebook-cj-moses-security-predictions-in-2023-and-beyond

[Source](https://aws.amazon.com/blogs/security/new-ebook-cj-moses-security-predictions-in-2023-and-beyond/){:target="_blank" rel="noopener"}

> As we head into 2023, it’s time to think about lessons from this year and incorporate them into planning for the next year and beyond. At AWS, we continually learn from our customers, who influence the best practices that we share and the security services that we offer. We heard that you’re looking for more [...]
