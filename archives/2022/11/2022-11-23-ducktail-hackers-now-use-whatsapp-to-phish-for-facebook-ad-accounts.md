Title: Ducktail hackers now use WhatsApp to phish for Facebook Ad accounts
Date: 2022-11-23T04:47:48-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-11-23-ducktail-hackers-now-use-whatsapp-to-phish-for-facebook-ad-accounts

[Source](https://www.bleepingcomputer.com/news/security/ducktail-hackers-now-use-whatsapp-to-phish-for-facebook-ad-accounts/){:target="_blank" rel="noopener"}

> A cybercriminal operation tracked as Ducktail has been hijacking Facebook Business accounts causing losses of up to $600,000 in advertising credits. [...]
