Title: Meta links US military to fake social media influence campaigns
Date: 2022-11-24T12:15:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-24-meta-links-us-military-to-fake-social-media-influence-campaigns

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/24/meta_us_military_influence/){:target="_blank" rel="noopener"}

> Didn't say they were good, though – covert ops apparently got 'little to no engagement' from targets In its latest quarterly threat report, Meta said it had detected and disrupted influence operations originating in the US, and it calls out those it believes are responsible: the American military.... [...]
