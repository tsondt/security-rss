Title: Another Event-Related Spyware App
Date: 2022-11-15T12:16:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberweapons;Egypt;smartphones;spyware
Slug: 2022-11-15-another-event-related-spyware-app

[Source](https://www.schneier.com/blog/archives/2022/11/another-event-related-spyware-app.html){:target="_blank" rel="noopener"}

> Last month, we were warned not to install Qatar’s World Cup app because it was spyware. This month, it’s Egypt’s COP27 Summit app : The app is being promoted as a tool to help attendees navigate the event. But it risks giving the Egyptian government permission to read users’ emails and messages. Even messages shared via encrypted services like WhatsApp are vulnerable, according to POLITICO’s technical review of the application, and two of the outside experts. The app also provides Egypt’s Ministry of Communications and Information Technology, which created it, with other so-called backdoor privileges, or the ability to scan [...]
