Title: Magento stores targeted in massive surge of TrojanOrders attacks
Date: 2022-11-16T11:14:57-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-16-magento-stores-targeted-in-massive-surge-of-trojanorders-attacks

[Source](https://www.bleepingcomputer.com/news/security/magento-stores-targeted-in-massive-surge-of-trojanorders-attacks/){:target="_blank" rel="noopener"}

> At least seven hacking groups are behind a massive surge in 'TrojanOrders' attacks targeting Magento 2 websites, exploiting a vulnerability that allows the threat actors to compromise vulnerable servers. [...]
