Title: Patch Tuesday, November 2022 Election Edition
Date: 2022-11-09T01:50:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;AskWoody;CVE-2022-41073;CVE-2022-41080;CVE-2022-41082;CVE-2022-41091;CVE-2022-41125;CVE-2022-41128;Immersive Labs;Kevin Breen;microsoft;Microsoft Patch Tuesday November 2022;sans internet storm center;Satnam Narang;Tenable;windows;Windows Print Spooler
Slug: 2022-11-09-patch-tuesday-november-2022-election-edition

[Source](https://krebsonsecurity.com/2022/11/patch-tuesday-november-2022-election-edition/){:target="_blank" rel="noopener"}

> Let’s face it: Having “2022 election” in the headline above is probably the only reason anyone might read this story today. Still, while most of us here in the United States are anxiously awaiting the results of how well we’ve patched our Democracy, it seems fitting that Microsoft Corp. today released gobs of security patches for its ubiquitous Windows operating systems. November’s patch batch includes fixes for a whopping six zero-day security vulnerabilities that miscreants and malware are already exploiting in the wild. Probably the scariest of the zero-day flaws is CVE-2022-41128, a “critical” weakness in the Windows scripting languages [...]
