Title: How to evaluate and use ECDSA certificates in AWS Certificate Manager
Date: 2022-11-08T17:37:21+00:00
Author: Zachary Miller
Category: AWS Security
Tags: AWS Certificate Manager;Foundational (100);Security, Identity, & Compliance;Technical How-to;ACM;certificate management;certificates;cryptography;ECDSA;PKI;Security Blog;SSL;TLS
Slug: 2022-11-08-how-to-evaluate-and-use-ecdsa-certificates-in-aws-certificate-manager

[Source](https://aws.amazon.com/blogs/security/how-to-evaluate-and-use-ecdsa-certificates-in-aws-certificate-manager/){:target="_blank" rel="noopener"}

> AWS Certificate Manager (ACM) is a managed service that enables you to provision, manage, and deploy public and private SSL/TLS certificates that you can use to securely encrypt network traffic. You can now use ACM to request Elliptic Curve Digital Signature Algorithm (ECDSA) certificates and associate the certificates with AWS services like Application Load Balancer (ALB) [...]
