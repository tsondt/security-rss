Title: Windows Server domain controllers may stop, restart after recent updates
Date: 2022-11-28T15:46:52+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-28-windows-server-domain-controllers-may-stop-restart-after-recent-updates

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/28/microsoft_windows_server_lsass/){:target="_blank" rel="noopener"}

> Microsoft outlines a workaround while pulling together a fix to LSASS memory leak Updates to Windows Server released as part of this month's Patch Tuesday onslaught might cause some domain controllers to stop working or automatically restart, according to Microsoft.... [...]
