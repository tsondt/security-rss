Title: Hackers breach energy orgs via bugs in discontinued web server
Date: 2022-11-22T14:55:29-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-11-22-hackers-breach-energy-orgs-via-bugs-in-discontinued-web-server

[Source](https://www.bleepingcomputer.com/news/security/hackers-breach-energy-orgs-via-bugs-in-discontinued-web-server/){:target="_blank" rel="noopener"}

> Microsoft said today that security vulnerabilities found to impact a web server discontinued since 2005 have been used to target and compromise organizations in the energy sector. [...]
