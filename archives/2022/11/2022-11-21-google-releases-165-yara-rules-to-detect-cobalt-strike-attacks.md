Title: Google releases 165 YARA rules to detect Cobalt Strike attacks
Date: 2022-11-21T11:32:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-11-21-google-releases-165-yara-rules-to-detect-cobalt-strike-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-releases-165-yara-rules-to-detect-cobalt-strike-attacks/){:target="_blank" rel="noopener"}

> The Google Cloud Threat Intelligence team has open-sourced YARA Rules and a VirusTotal Collection of indicators of compromise (IOCs) to help defenders detect Cobalt Strike components in their networks. [...]
