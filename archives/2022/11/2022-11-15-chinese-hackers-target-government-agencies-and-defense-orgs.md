Title: Chinese hackers target government agencies and defense orgs
Date: 2022-11-15T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-15-chinese-hackers-target-government-agencies-and-defense-orgs

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-target-government-agencies-and-defense-orgs/){:target="_blank" rel="noopener"}

> The Chinese espionage APT (advanced persistent threat), tracked as 'Billbug' (aka Thrip, or Lotus Blossom), is currently running a 2022 campaign targeting government agencies and defense organizations in multiple Asian countries. [...]
