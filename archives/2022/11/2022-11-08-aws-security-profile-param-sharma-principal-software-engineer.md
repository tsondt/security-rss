Title: AWS Security Profile: Param Sharma, Principal Software Engineer
Date: 2022-11-08T20:30:49+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Announcements;AWS re:Invent;Events;Security, Identity, & Compliance;AWS Security Profile;Live Events;Re:Invent 2022;reInvent;Security Blog
Slug: 2022-11-08-aws-security-profile-param-sharma-principal-software-engineer

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-param-sharma/){:target="_blank" rel="noopener"}

> In the weeks leading up to AWS re:Invent 2022, I’m interviewing some of the humans who work in AWS Security, help keep our customers safe and secure, and also happen to be speaking at re:Invent. This interview is with Param Sharma, principal software engineer for AWS Private Certificate Authority (AWS Private CA). AWS Private CA enables [...]
