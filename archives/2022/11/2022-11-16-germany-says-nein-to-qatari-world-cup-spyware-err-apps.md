Title: Germany says nein to Qatari World Cup spyware, err, apps
Date: 2022-11-16T21:30:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-16-germany-says-nein-to-qatari-world-cup-spyware-err-apps

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/16/germany_world_cup_apps/){:target="_blank" rel="noopener"}

> Norway, France also sound data privacy alarms World Cup apps from the Qatari government collect more personal information than they need to, according to Germany's data protection agency, which this week warned football fans to only install the two apps "if it is absolutely necessary." Also: consider using a burner phone.... [...]
