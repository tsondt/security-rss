Title: Microsoft fixes critical RCE flaw affecting Azure Cosmos DB
Date: 2022-11-01T09:44:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-01-microsoft-fixes-critical-rce-flaw-affecting-azure-cosmos-db

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-critical-rce-flaw-affecting-azure-cosmos-db/){:target="_blank" rel="noopener"}

> Analysts at Orca Security have found a critical vulnerability affecting Azure Cosmos DB that allowed unauthenticated read and write access to containers. [...]
