Title: New SandStrike spyware infects Android devices via malicious VPN app
Date: 2022-11-01T11:29:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-01-new-sandstrike-spyware-infects-android-devices-via-malicious-vpn-app

[Source](https://www.bleepingcomputer.com/news/security/new-sandstrike-spyware-infects-android-devices-via-malicious-vpn-app/){:target="_blank" rel="noopener"}

> Threat actors are using a newly discovered spyware known as SandStrike and delivered via a malicious VPN application to target Persian-speaking Android users. [...]
