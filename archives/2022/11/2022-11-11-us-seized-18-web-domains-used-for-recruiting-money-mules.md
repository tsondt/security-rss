Title: U.S. seized 18 web domains used for recruiting money mules
Date: 2022-11-11T13:05:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-11-us-seized-18-web-domains-used-for-recruiting-money-mules

[Source](https://www.bleepingcomputer.com/news/security/us-seized-18-web-domains-used-for-recruiting-money-mules/){:target="_blank" rel="noopener"}

> The FBI and U.S. Postal Inspection Service have seized eighteen web domains used to recruit money mules for work-from-home and reshipping scams. [...]
