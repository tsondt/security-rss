Title: Lenovo fixes flaws that can be used to disable UEFI Secure Boot
Date: 2022-11-09T11:03:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-09-lenovo-fixes-flaws-that-can-be-used-to-disable-uefi-secure-boot

[Source](https://www.bleepingcomputer.com/news/security/lenovo-fixes-flaws-that-can-be-used-to-disable-uefi-secure-boot/){:target="_blank" rel="noopener"}

> Lenovo has fixed two high-severity vulnerabilities impacting various ThinkBook, IdeaPad, and Yoga laptop models that could allow an attacker to deactivate UEFI Secure Boot. [...]
