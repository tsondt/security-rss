Title: Ransomware gang targets Belgian municipality, hits police instead
Date: 2022-11-26T10:06:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-26-ransomware-gang-targets-belgian-municipality-hits-police-instead

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-targets-belgian-municipality-hits-police-instead/){:target="_blank" rel="noopener"}

> The Ragnar Locker ransomware gang has published stolen data from what they thought was the municipality of Zwijndrecht, but turned out to be stolen from Zwijndrecht police, a local police unit in Antwerp, Belgium. [...]
