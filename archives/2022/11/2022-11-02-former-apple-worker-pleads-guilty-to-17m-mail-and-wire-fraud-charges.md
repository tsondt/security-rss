Title: Former Apple worker pleads guilty to $17m mail and wire fraud charges
Date: 2022-11-02T13:00:51+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-11-02-former-apple-worker-pleads-guilty-to-17m-mail-and-wire-fraud-charges

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/02/apple_buyer_wire_money_fraud/){:target="_blank" rel="noopener"}

> Nefarious schemes included harvesting motherboard components and selling them back to Apple A one-time Apple employee working as a buyer within the iGiant's supply chain department has pleaded guilty to mail and wire fraud charges spanning multiple years, ultimately costing the company $17 million.... [...]
