Title: US Health Dept warns of Venus ransomware targeting healthcare orgs
Date: 2022-11-10T16:50:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-10-us-health-dept-warns-of-venus-ransomware-targeting-healthcare-orgs

[Source](https://www.bleepingcomputer.com/news/security/us-health-dept-warns-of-venus-ransomware-targeting-healthcare-orgs/){:target="_blank" rel="noopener"}

> The U.S. Department of Health and Human Services (HHS) warned today that Venus ransomware attacks are also targeting the country's healthcare organizations. [...]
