Title: New Mac app wants to record everything you do—so you can “rewind” it later
Date: 2022-11-02T21:33:00+00:00
Author: Benj Edwards
Category: Ars Technica
Tags: Biz & IT;Tech;AI;compression;ocr;privacy;Rewind;Rewind AI;speech recognition
Slug: 2022-11-02-new-mac-app-wants-to-record-everything-you-doso-you-can-rewind-it-later

[Source](https://arstechnica.com/?p=1894475){:target="_blank" rel="noopener"}

> Enlarge / Rewind reportedly lets you search your Mac's usage history for what you've seen, said, or heard. (credit: Rewind AI) Yesterday, a company called Rewind AI announced a self-titled software product for Macs with Apple Silicon that reportedly keeps a highly compressed, searchable record of everything you do locally on your Mac and lets you "rewind" time to see it later. If you forget something you've "seen, said, or heard," Rewind wants to help you find it easily. Rewind AI claims its product stores all recording data locally on your machine and does not require cloud integration. Among its [...]
