Title: Country that still uses fax machines wants to lead the world on data standards at G7
Date: 2022-11-15T09:43:30+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-11-15-country-that-still-uses-fax-machines-wants-to-lead-the-world-on-data-standards-at-g7

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/15/japan_g7_data_standards/){:target="_blank" rel="noopener"}

> Aiming for somewhere between US 'Wild West' and EU's strict GDPR Even though Japan lags behind the rest of the developed world in digital transformation, it hopes to create global data flow standards for discussion at next year's G7 meetings.... [...]
