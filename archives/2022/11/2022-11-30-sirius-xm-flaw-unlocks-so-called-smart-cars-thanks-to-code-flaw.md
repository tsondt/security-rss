Title: Sirius XM flaw unlocks so-called smart cars thanks to code flaw
Date: 2022-11-30T23:30:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-30-sirius-xm-flaw-unlocks-so-called-smart-cars-thanks-to-code-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/30/siriusxm_connected_cars_hacking/){:target="_blank" rel="noopener"}

> Telematics program doesn't just give you music, but a big security flaw Sirius XM's Connected Vehicle Services has fixed an authorization flaw that would have allowed an attacker to remotely unlock doors and start engines on connected cars knowing only the vehicle identification number (VIN).... [...]
