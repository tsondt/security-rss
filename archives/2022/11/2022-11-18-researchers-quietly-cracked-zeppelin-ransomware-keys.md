Title: Researchers Quietly Cracked Zeppelin Ransomware Keys
Date: 2022-11-18T02:30:26+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ransomware;Security Tools;Cybersecurity & Infrastructure Security Agency;Digital Ocean;fbi;Joel Lathrop;Lance James;Unit 221B;Zeppelin decryptor;Zeppelin ransomware
Slug: 2022-11-18-researchers-quietly-cracked-zeppelin-ransomware-keys

[Source](https://krebsonsecurity.com/2022/11/researchers-quietly-cracked-zeppelin-ransomware-keys/){:target="_blank" rel="noopener"}

> Peter is an IT manager for a technology manufacturer that got hit with a Russian ransomware strain called “ Zeppelin ” in May 2020. He’d been on the job less than six months, and because of the way his predecessor architected things, the company’s data backups also were encrypted by Zeppelin. After two weeks of stalling their extortionists, Peter’s bosses were ready to capitulate and pay the ransom demand. Then came the unlikely call from an FBI agent. “Don’t pay,” the agent said. “We’ve found someone who can crack the encryption.” Peter, who spoke candidly about the attack on condition [...]
