Title: Acer fixes UEFI bugs that can be used to disable Secure Boot
Date: 2022-11-28T18:31:17-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-28-acer-fixes-uefi-bugs-that-can-be-used-to-disable-secure-boot

[Source](https://www.bleepingcomputer.com/news/security/acer-fixes-uefi-bugs-that-can-be-used-to-disable-secure-boot/){:target="_blank" rel="noopener"}

> Acer has fixed a high-severity vulnerability affecting multiple laptop models that could enable local attackers to deactivate UEFI Secure Boot security feature. [...]
