Title: Hackers steal $300,000 in DraftKings credential stuffing attack
Date: 2022-11-21T15:17:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-21-hackers-steal-300000-in-draftkings-credential-stuffing-attack

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-300-000-in-draftkings-credential-stuffing-attack/){:target="_blank" rel="noopener"}

> Sports betting company DraftKings said today that it would make whole customers affected by a credential stuffing attack that led to losses of up to $300,000. [...]
