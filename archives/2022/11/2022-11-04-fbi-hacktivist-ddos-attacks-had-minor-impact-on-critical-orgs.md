Title: FBI: Hacktivist DDoS attacks had minor impact on critical orgs
Date: 2022-11-04T16:29:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-04-fbi-hacktivist-ddos-attacks-had-minor-impact-on-critical-orgs

[Source](https://www.bleepingcomputer.com/news/security/fbi-hacktivist-ddos-attacks-had-minor-impact-on-critical-orgs/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) said on Friday that distributed denial-of-service (DDoS) attacks coordinated by hacktivist groups have little to no impact. [...]
