Title: First Review of A Hacker’s Mind
Date: 2022-11-18T18:08:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;Schneier news
Slug: 2022-11-18-first-review-of-a-hackers-mind

[Source](https://www.schneier.com/blog/archives/2022/11/first-review-of-a-hackers-mind.html){:target="_blank" rel="noopener"}

> Kirkus reviews A Hacker’s Mind : A cybersecurity expert examines how the powerful game whatever system is put before them, leaving it to others to cover the cost. Schneier, a professor at Harvard Kennedy School and author of such books as Data and Goliath and Click Here To Kill Everybody, regularly challenges his students to write down the first 100 digits of pi, a nearly impossible task­—but not if they cheat, concerning which he admonishes, “Don’t get caught.” Not getting caught is the aim of the hackers who exploit the vulnerabilities of systems of all kinds. Consider right-wing venture capitalist [...]
