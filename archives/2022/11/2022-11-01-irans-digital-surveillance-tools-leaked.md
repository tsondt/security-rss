Title: Iran’s Digital Surveillance Tools Leaked
Date: 2022-11-01T11:24:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Iran;leaks;privacy;surveillance
Slug: 2022-11-01-irans-digital-surveillance-tools-leaked

[Source](https://www.schneier.com/blog/archives/2022/11/irans-digital-surveillance-tools-leaked.html){:target="_blank" rel="noopener"}

> It’s Iran’s turn to have its digital surveillance tools leaked : According to these internal documents, SIAM is a computer system that works behind the scenes of Iranian cellular networks, providing its operators a broad menu of remote commands to alter, disrupt, and monitor how customers use their phones. The tools can slow their data connections to a crawl, break the encryption of phone calls, track the movements of individuals or large groups, and produce detailed metadata summaries of who spoke to whom, when, and where. Such a system could help the government invisibly quash the ongoing protests ­—or those [...]
