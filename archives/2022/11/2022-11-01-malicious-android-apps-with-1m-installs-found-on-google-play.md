Title: Malicious Android apps with 1M+ installs found on Google Play
Date: 2022-11-01T16:03:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-11-01-malicious-android-apps-with-1m-installs-found-on-google-play

[Source](https://www.bleepingcomputer.com/news/security/malicious-android-apps-with-1m-plus-installs-found-on-google-play/){:target="_blank" rel="noopener"}

> A set of four malicious applications currently available in Google Play, the official store for the Android system, are directing users sites that steal sensitive information or generate 'pay-per-click' revenue for the operators. [...]
