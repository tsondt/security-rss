Title: Google Search results poisoned with torrent sites via Data Studio
Date: 2022-11-18T09:03:43-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-11-18-google-search-results-poisoned-with-torrent-sites-via-data-studio

[Source](https://www.bleepingcomputer.com/news/security/google-search-results-poisoned-with-torrent-sites-via-data-studio/){:target="_blank" rel="noopener"}

> Threat actors are abusing Google's Looker Studio (formerly Google Data Studio) to boost search engine rankings for their illicit websites that promote spam, torrents, and pirated content. [...]
