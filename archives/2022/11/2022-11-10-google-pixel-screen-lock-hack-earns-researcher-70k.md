Title: Google Pixel screen-lock hack earns researcher $70k
Date: 2022-11-10T16:14:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-10-google-pixel-screen-lock-hack-earns-researcher-70k

[Source](https://portswigger.net/daily-swig/google-pixel-screen-lock-hack-earns-researcher-70k){:target="_blank" rel="noopener"}

> Android security pwned by PUK reset trick [...]
