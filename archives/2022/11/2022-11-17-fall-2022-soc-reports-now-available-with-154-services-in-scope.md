Title: Fall 2022 SOC reports now available with 154 services in scope
Date: 2022-11-17T16:16:21+00:00
Author: Andrew Najjar
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC 1;AWS SOC 2;AWS SOC 3;AWS SOC Reports;Security Blog
Slug: 2022-11-17-fall-2022-soc-reports-now-available-with-154-services-in-scope

[Source](https://aws.amazon.com/blogs/security/fall-2022-soc-reports-now-available-with-154-services-in-scope/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we’re committed to providing customers with continued assurance over the security, availability, and confidentiality of the AWS control environment. We’re proud to deliver the Fall 2022 System and Organizational Controls (SOC) 1, 2, and 3 reports, which cover April 1–September 30, 2022, to support our customers’ confidence in AWS services. [...]
