Title: Lawsuit Seeks Food Benefits Stolen By Skimmers
Date: 2022-11-10T18:11:10+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;All About Skimmers;Deborah Harris;Supplemental Nutrition and Assistance Program;The Center for Law and Social Policy;The Massachusetts Law Reform Institute;U.S. Department of Agriculture
Slug: 2022-11-10-lawsuit-seeks-food-benefits-stolen-by-skimmers

[Source](https://krebsonsecurity.com/2022/11/lawsuit-seeks-food-benefits-stolen-by-skimmers/){:target="_blank" rel="noopener"}

> A nonprofit organization is suing the state of Massachusetts on behalf of thousands of low-income families who were collectively robbed of more than a $1 million in food assistance benefits by card skimming devices secretly installed at cash machines and grocery store checkout lanes across the state. Federal law bars states from replacing these benefits using federal funds, and a recent rash of skimming incidents nationwide has disproportionately affected those receiving food assistance via state-issued prepaid debit cards. The Massachusetts SNAP benefits card looks more like a library card than a payment card. On Nov. 4, The Massachusetts Law Reform [...]
