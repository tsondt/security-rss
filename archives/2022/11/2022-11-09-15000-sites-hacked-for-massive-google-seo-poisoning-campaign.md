Title: 15,000 sites hacked for massive Google SEO poisoning campaign
Date: 2022-11-09T13:08:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-11-09-15000-sites-hacked-for-massive-google-seo-poisoning-campaign

[Source](https://www.bleepingcomputer.com/news/security/15-000-sites-hacked-for-massive-google-seo-poisoning-campaign/){:target="_blank" rel="noopener"}

> Hackers are conducting a massive black hat search engine optimization (SEO) campaign by compromising almost 15,000 websites to redirect visitors to fake Q&A discussion forums. [...]
