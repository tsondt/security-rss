Title: Get the best out of Amazon Verified Permissions by using fine-grained authorization methods
Date: 2022-11-29T18:38:32+00:00
Author: Jeff Lombardo
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Technical How-to;custom application access;fine-grained application permissions;fine-grained authorization;policy-based access control;Security Blog
Slug: 2022-11-29-get-the-best-out-of-amazon-verified-permissions-by-using-fine-grained-authorization-methods

[Source](https://aws.amazon.com/blogs/security/get-the-best-out-of-amazon-verified-permissions-by-using-fine-grained-authorization-methods/){:target="_blank" rel="noopener"}

> With the release of Amazon Verified Permissions, developers of custom applications can implement access control logic based on caller and resource information; group membership, hierarchy, and relationship; and session context, such as device posture, location, time, or method of authentication. With Amazon Verified Permissions, you can focus on building simple authorization policies and your applications—instead [...]
