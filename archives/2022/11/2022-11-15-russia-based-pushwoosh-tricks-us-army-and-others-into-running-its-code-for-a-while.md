Title: Russia-based Pushwoosh tricks US Army and others into running its code – for a while
Date: 2022-11-15T01:30:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-15-russia-based-pushwoosh-tricks-us-army-and-others-into-running-its-code-for-a-while

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/15/russia_pushwoosh_us_army/){:target="_blank" rel="noopener"}

> Russian data trackers... what could possibly go wrong? Updated US government agencies including the Army and Centers for Disease Control and Prevention pulled apps running Pushwoosh code after learning the software company – which presents itself as American – is actually Russian, according to Reuters.... [...]
