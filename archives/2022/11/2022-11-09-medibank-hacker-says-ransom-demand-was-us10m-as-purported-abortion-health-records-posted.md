Title: Medibank hacker says ransom demand was US$10m as purported abortion health records posted
Date: 2022-11-09T22:38:47+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Australia news;Cybercrime;Health insurance;Hacking;Internet;Data and computer security
Slug: 2022-11-09-medibank-hacker-says-ransom-demand-was-us10m-as-purported-abortion-health-records-posted

[Source](https://www.theguardian.com/australia-news/2022/nov/10/medibank-hacker-says-ransom-demand-was-us10m-as-purported-abortion-health-records-posted){:target="_blank" rel="noopener"}

> Post on blog linked to Russian ransomware group says it offered ‘discount’ ransom to health insurer of US$9.7m, or $1 for each customer’s data Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast The hacker behind the cyber-attack on Medibank set a US$10m price on not releasing the data, they claimed, alongside a new leak of apparently hacked records that purports to contain abortion health information. In the early hours of Thursday on a dark web blog linked to the REvil Russian ransomware group, the attacker [...]
