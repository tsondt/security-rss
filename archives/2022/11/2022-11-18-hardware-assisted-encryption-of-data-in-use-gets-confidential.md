Title: Hardware-assisted encryption of data in use gets confidential
Date: 2022-11-18T12:03:09+00:00
Author: John Malachy
Category: The Register
Tags: 
Slug: 2022-11-18-hardware-assisted-encryption-of-data-in-use-gets-confidential

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/18/hardwareassisted_encryption_of_data_in/){:target="_blank" rel="noopener"}

> Our poll reveals how much organisations rely on the compliant storage and hosting sensitive data in their data centres Reader Survey Results Data protection is a top priority for organisations tasked with protecting the integrity of not just their own data, but also the personally identifiable information (PII) they store and process on behalf of their business partners and customers. Not doing it properly risks losing their trust and falling foul of increasingly stringent data protection regulation. So what can be done to toughen up your defences?... [...]
