Title: Keralty ransomware attack impacts Colombia's health care system
Date: 2022-11-30T18:25:53-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2022-11-30-keralty-ransomware-attack-impacts-colombias-health-care-system

[Source](https://www.bleepingcomputer.com/news/security/keralty-ransomware-attack-impacts-colombias-health-care-system/){:target="_blank" rel="noopener"}

> The Keralty multinational healthcare organization suffered a RansomHouse ransomware attack on Sunday, disrupting the websites and operations of the company and its subsidiaries. [...]
