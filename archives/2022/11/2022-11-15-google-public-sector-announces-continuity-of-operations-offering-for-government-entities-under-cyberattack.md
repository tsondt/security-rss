Title: Google Public Sector announces continuity-of-operations offering for government entities under cyberattack
Date: 2022-11-15T11:00:00+00:00
Author: Jeanette Manfra
Category: GCP Security
Tags: Google Cloud;Productivity & Collaboration;Identity & Security;Public Sector
Slug: 2022-11-15-google-public-sector-announces-continuity-of-operations-offering-for-government-entities-under-cyberattack

[Source](https://cloud.google.com/blog/topics/public-sector/google-workspace-makes-the-government-agencies-more-resilient/){:target="_blank" rel="noopener"}

> Cyberattacks that target our government are all too common these days. From SolarWinds, to hacks against widely used email servers, to attacks against the defense industrial base, we know that cyberattacks against the public and private sectors continue to be an issue. Our latest VirusTotal malware trends report illustrates this point as well, with findings that governmental domains are among the top categories used by attackers in 2022 to distribute malicious content. Given the external environment, government agencies in particular need reliable continuity plans in the event of an attack. In fact, two policy directives were recently issued to ensure [...]
