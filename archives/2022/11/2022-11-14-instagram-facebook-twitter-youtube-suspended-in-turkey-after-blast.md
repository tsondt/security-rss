Title: Instagram, Facebook, Twitter, YouTube suspended in Turkey after blast
Date: 2022-11-14T04:17:28-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-11-14-instagram-facebook-twitter-youtube-suspended-in-turkey-after-blast

[Source](https://www.bleepingcomputer.com/news/security/instagram-facebook-twitter-youtube-suspended-in-turkey-after-blast/){:target="_blank" rel="noopener"}

> Following yesterday's deadly blast on İstiklal Avenue in Istanbul, Turkish authorities began restricting access to social media including Instagram, Facebook, Twitter, YouTube and Telegram. [...]
