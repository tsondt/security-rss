Title: New Crimson Kingsnake gang impersonates law firms in BEC attacks
Date: 2022-11-03T14:33:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-03-new-crimson-kingsnake-gang-impersonates-law-firms-in-bec-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-crimson-kingsnake-gang-impersonates-law-firms-in-bec-attacks/){:target="_blank" rel="noopener"}

> A business email compromise (BEC) group named 'Crimson Kingsnake' has emerged, impersonating well-known international law firms to trick recipients into approving overdue invoice payments. [...]
