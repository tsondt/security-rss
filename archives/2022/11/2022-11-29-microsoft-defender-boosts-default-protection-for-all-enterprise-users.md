Title: Microsoft Defender boosts default protection for all enterprise users
Date: 2022-11-29T09:59:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-11-29-microsoft-defender-boosts-default-protection-for-all-enterprise-users

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-boosts-default-protection-for-all-enterprise-users/){:target="_blank" rel="noopener"}

> Microsoft announced that built-in protection is generally available for all devices onboarded to Defender for Endpoint, the company's endpoint security platform. [...]
