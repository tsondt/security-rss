Title: China is likely stockpiling and deploying vulnerabilities, says Microsoft
Date: 2022-11-07T07:56:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-11-07-china-is-likely-stockpiling-and-deploying-vulnerabilities-says-microsoft

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/07/china_stockpiles_vulnerabilities_microsoft_asserts/){:target="_blank" rel="noopener"}

> Increase in espionage and cyberattacks since law requiring vulnerabilities first be reported to Beijing Microsoft has asserted that China's offensive cyber capabilities have improved, thanks to a law that has allowed Beijing to create an arsenal of unreported software vulnerabilities.... [...]
