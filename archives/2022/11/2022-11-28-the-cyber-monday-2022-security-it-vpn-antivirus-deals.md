Title: The Cyber Monday 2022 Security, IT, VPN, & Antivirus Deals
Date: 2022-11-28T09:43:53-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-28-the-cyber-monday-2022-security-it-vpn-antivirus-deals

[Source](https://www.bleepingcomputer.com/news/security/the-cyber-monday-2022-security-it-vpn-and-antivirus-deals/){:target="_blank" rel="noopener"}

> Cyber Monday is here, and great deals are live in computer security, software, online courses, system admin services, antivirus, and VPN software. [...]
