Title: AWS Security Profile: Reef D’Souza, Principal Solutions Architect
Date: 2022-11-17T20:33:06+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Announcements;AWS re:Invent;Security, Identity, & Compliance;AWS Security Profile;Re:Invent 2022;Security Blog
Slug: 2022-11-17-aws-security-profile-reef-dsouza-principal-solutions-architect

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-reef-dsouza-principal-solutions-architect/){:target="_blank" rel="noopener"}

> In the weeks leading up to AWS re:invent 2022, I’ll share conversations I’ve had with some of the humans who work in AWS Security who will be presenting at the conference, and get a sneak peek at their work and sessions. In this profile, I interviewed Reef D’Souza, Principal Solutions Architect. How long have you [...]
