Title: New clipboard hijacker replaces crypto wallet addresses with lookalikes
Date: 2022-11-03T09:10:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-11-03-new-clipboard-hijacker-replaces-crypto-wallet-addresses-with-lookalikes

[Source](https://www.bleepingcomputer.com/news/security/new-clipboard-hijacker-replaces-crypto-wallet-addresses-with-lookalikes/){:target="_blank" rel="noopener"}

> A new clipboard stealer called Laplas Clipper spotted in the wild is using cryptocurrency wallet addresses that look like the address of the victim's intended recipient. [...]
