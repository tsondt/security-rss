Title: HackerOne encourages customers to adopt standard policy to protect hackers from legal problems
Date: 2022-11-17T15:27:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-17-hackerone-encourages-customers-to-adopt-standard-policy-to-protect-hackers-from-legal-problems

[Source](https://portswigger.net/daily-swig/hackerone-encourages-customers-to-adopt-standard-policy-to-protect-hackers-from-legal-problems){:target="_blank" rel="noopener"}

> ‘Short, broad, easily-understood safe harbor statement’ offered [...]
