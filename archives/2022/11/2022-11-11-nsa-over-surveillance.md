Title: NSA Over-surveillance
Date: 2022-11-11T12:25:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;NSA;privacy;surveillance
Slug: 2022-11-11-nsa-over-surveillance

[Source](https://www.schneier.com/blog/archives/2022/11/nsa-over-surveillance.html){:target="_blank" rel="noopener"}

> Here in 2022, we have a newly declassified 2016 Inspector General report—”Misuse of Sigint Systems”—about a 2013 NSA program that resulted in the unauthorized (that is, illegal) targeting of Americans. Given all we learned from Edward Snowden, this feels like a minor coda. There’s nothing really interesting in the IG document, which is heavily redacted. News story. [...]
