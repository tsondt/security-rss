Title: Atlassian fixes critical command injection bug in Bitbucket Server
Date: 2022-11-18T06:59:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-18-atlassian-fixes-critical-command-injection-bug-in-bitbucket-server

[Source](https://www.bleepingcomputer.com/news/security/atlassian-fixes-critical-command-injection-bug-in-bitbucket-server/){:target="_blank" rel="noopener"}

> Atlassian has released updates to address critical-severity updates in its centralized identity management platform, Crowd Server and Data Center, and in Bitbucket Server and Data Center, the company's solution for Git repository management. [...]
