Title: Docker Hub repositories hide over 1,650 malicious containers
Date: 2022-11-24T12:16:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-24-docker-hub-repositories-hide-over-1650-malicious-containers

[Source](https://www.bleepingcomputer.com/news/security/docker-hub-repositories-hide-over-1-650-malicious-containers/){:target="_blank" rel="noopener"}

> Over 1,600 publicly available Docker Hub images hide malicious behavior, including cryptocurrency miners, embedded secrets that can be used as backdoors, DNS hijackers, and website redirectors. [...]
