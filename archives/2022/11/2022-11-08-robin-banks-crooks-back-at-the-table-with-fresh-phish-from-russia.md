Title: Robin Banks crooks back at the table with fresh phish from Russia
Date: 2022-11-08T17:45:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-08-robin-banks-crooks-back-at-the-table-with-fresh-phish-from-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/08/robin_banks_phishing_service/){:target="_blank" rel="noopener"}

> Phishing-as-a-service group's toolset now includes ways to get around MFA Robin Banks, the phishing-as-a-service (PHaaS) platform that was kicked off Cloudflare for malicious activity, is back in action with a Russian service provider and new tools to make it easier to bypass security measures.... [...]
