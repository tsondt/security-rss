Title: Royal Mail customer data leak shutters online Click and Drop
Date: 2022-11-03T08:29:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-03-royal-mail-customer-data-leak-shutters-online-click-and-drop

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/03/royal_mail_customer_data_leak/){:target="_blank" rel="noopener"}

> Customers complain of exposed order info, multiple charges — but still no postage A technical SNAFU shut down the UK's Royal Mail Click and Drop website on Tuesday after a security "issue" allowed some customers to see others' order information.... [...]
