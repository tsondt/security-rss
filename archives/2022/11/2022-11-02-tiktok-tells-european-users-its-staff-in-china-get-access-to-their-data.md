Title: TikTok tells European users its staff in China get access to their data
Date: 2022-11-02T15:56:11+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: TikTok;China;Privacy;Social media;Apps;Data and computer security;European Union;Digital media;Internet;Technology;Business;Asia Pacific;US news;Singapore;Europe;World news;Media
Slug: 2022-11-02-tiktok-tells-european-users-its-staff-in-china-get-access-to-their-data

[Source](https://www.theguardian.com/technology/2022/nov/02/tiktok-tells-european-users-its-staff-in-china-get-access-to-their-data){:target="_blank" rel="noopener"}

> Privacy policy update confirms data of continent’s users available to range of TikTok bases including in Brazil, Israel and US TikTok is spelling out to its European users that their data can be accessed by employees outside the continent, including in China, amid political and regulatory concerns about Chinese access to user information on the platform. The Chinese-owned social video app is updating its privacy policy to confirm that staff in countries, including China, are allowed to access user data to ensure their experience of the platform is “consistent, enjoyable and safe”. Continue reading... [...]
