Title: Couple sentenced to prison for trying to sell nuclear warship secrets
Date: 2022-11-09T17:39:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-09-couple-sentenced-to-prison-for-trying-to-sell-nuclear-warship-secrets

[Source](https://www.bleepingcomputer.com/news/security/couple-sentenced-to-prison-for-trying-to-sell-nuclear-warship-secrets/){:target="_blank" rel="noopener"}

> A Navy nuclear engineer and his wife were sentenced to over 19 years and more than 21 years in prison for attempting to sell nuclear warship design secrets to what they believed was a foreign power agent. [...]
