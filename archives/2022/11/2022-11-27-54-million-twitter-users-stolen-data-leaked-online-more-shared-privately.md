Title: 5.4 million Twitter users' stolen data leaked online — more shared privately
Date: 2022-11-27T13:31:01-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-27-54-million-twitter-users-stolen-data-leaked-online-more-shared-privately

[Source](https://www.bleepingcomputer.com/news/security/54-million-twitter-users-stolen-data-leaked-online-more-shared-privately/){:target="_blank" rel="noopener"}

> Over 5.4 million Twitter user records containing non-public information stolen using an API vulnerability fixed in January have been shared for free on a hacker forum. Another massive, potentially more significant, data dump of millions of Twitter records has also been disclosed by a security researcher. [...]
