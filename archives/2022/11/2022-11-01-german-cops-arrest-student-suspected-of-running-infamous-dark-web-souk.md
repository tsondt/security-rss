Title: German cops arrest student suspected of running infamous dark-web souk
Date: 2022-11-01T05:28:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-01-german-cops-arrest-student-suspected-of-running-infamous-dark-web-souk

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/01/germany_deutschland_dark_web/){:target="_blank" rel="noopener"}

> Deutschland im Deep Web destroyed A 22-year-old student German federal police believe to be the administrator of one of the largest German-speaking, dark-web forums has been arrested.... [...]
