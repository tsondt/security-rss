Title: Android and iOS apps with 15 million installs extort loan seekers
Date: 2022-11-30T08:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Google;Mobile
Slug: 2022-11-30-android-and-ios-apps-with-15-million-installs-extort-loan-seekers

[Source](https://www.bleepingcomputer.com/news/security/android-and-ios-apps-with-15-million-installs-extort-loan-seekers/){:target="_blank" rel="noopener"}

> Over 280 Android and iOS apps on the Google Play and the Apple App stores trapped users in loan schemes with misleading terms and employed various methods to extort and harass borrowers. [...]
