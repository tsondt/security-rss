Title: AWS Security Profile: Jonathan “Koz” Kozolchyk, GM of Certificate Services
Date: 2022-11-18T17:38:45+00:00
Author: Roger Park
Category: AWS Security
Tags: Announcements;AWS re:Invent;Security, Identity, & Compliance;AWS Re:Invent;AWS Security Profile;Live Events;Re:Invent 2022;Security Blog
Slug: 2022-11-18-aws-security-profile-jonathan-koz-kozolchyk-gm-of-certificate-services

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-jonathan-koz-kozolchyk-gm-of-certificate-services/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, we interview AWS thought leaders who help keep our customers safe and secure. This interview features Jonathan “Koz” Kozolchyk, GM of Certificate Services, PKI Systems. Koz shares his insights on the current certificate landscape, his career at Amazon and within the security space, what he’s excited about for the [...]
