Title: How data embassies can strengthen resiliency with sovereignty
Date: 2022-11-11T17:00:00+00:00
Author: Thiébaut Meyer
Category: GCP Security
Tags: Identity & Security
Slug: 2022-11-11-how-data-embassies-can-strengthen-resiliency-with-sovereignty

[Source](https://cloud.google.com/blog/products/identity-security/data-embassies-strengthening-resiliency-with-sovereignty/){:target="_blank" rel="noopener"}

> Embassies have been havens for the people of countries they represent for hundreds of years, helping reduce some risks that their citizens can face when traveling and living abroad. The concept of reducing risk with an embassy has been extended to data in the digital world, made possible by the flexible, distributed nature of the cloud. Small countries around the world are turning to the concept of “ data embassies ” because they are in need of sovereign and resilient infrastructure. Nevertheless, they realize that keeping data localized within a single facility or a set geographical boundary could pose a [...]
