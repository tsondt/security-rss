Title: Still using a discontinued Boa web server? Microsoft warns of supply chain attacks
Date: 2022-11-23T19:00:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-23-still-using-a-discontinued-boa-web-server-microsoft-warns-of-supply-chain-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/23/microsoft_boa_web_server/){:target="_blank" rel="noopener"}

> Flaws in the open-source tool exploited – and India's power grid was a target Microsoft is warning that systems using the long-discontinued Boa web server could be at risk of attacks after a series of intrusion attempts of power grid operations in India likely included exploiting security flaws in the technology.... [...]
