Title: Lockheed Martin's Army cyber training platform goes civilian
Date: 2022-11-29T17:45:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-29-lockheed-martins-army-cyber-training-platform-goes-civilian

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/29/lockheed_martin_cyber_training/){:target="_blank" rel="noopener"}

> Army civilian employees, that is, but aerospace biz says it could be used in the private sector, too Locheed Martin has bagged a government contract to train 17,000 remote US Army civilian employees on security readiness, and wants to also extend the offer to private entities.... [...]
