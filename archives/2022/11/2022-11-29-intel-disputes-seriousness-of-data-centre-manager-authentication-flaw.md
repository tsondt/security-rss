Title: Intel disputes seriousness of Data Centre Manager authentication flaw
Date: 2022-11-29T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-29-intel-disputes-seriousness-of-data-centre-manager-authentication-flaw

[Source](https://portswigger.net/daily-swig/intel-disputes-seriousness-of-data-centre-manager-authentication-flaw){:target="_blank" rel="noopener"}

> Security researcher scores $10K bug bounty [...]
