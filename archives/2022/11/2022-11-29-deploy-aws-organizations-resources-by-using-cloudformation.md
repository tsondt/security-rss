Title: Deploy AWS Organizations resources by using CloudFormation
Date: 2022-11-29T01:01:51+00:00
Author: Matt Luttrell
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS CloudFormation;AWS Organizations;Security Blog
Slug: 2022-11-29-deploy-aws-organizations-resources-by-using-cloudformation

[Source](https://aws.amazon.com/blogs/security/deploy-aws-organizations-resources-by-using-cloudformation/){:target="_blank" rel="noopener"}

> AWS recently announced that AWS Organizations now supports AWS CloudFormation. This feature allows you to create and update AWS accounts, organizational units (OUs), and policies within your organization by using CloudFormation templates. With this latest integration, you can efficiently codify and automate the deployment of your resources in AWS Organizations. You can now manage your AWS organization [...]
