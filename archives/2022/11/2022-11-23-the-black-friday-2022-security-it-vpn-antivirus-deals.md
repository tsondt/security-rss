Title: The Black Friday 2022 Security, IT, VPN, & Antivirus Deals
Date: 2022-11-23T11:23:56-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-23-the-black-friday-2022-security-it-vpn-antivirus-deals

[Source](https://www.bleepingcomputer.com/news/security/the-black-friday-2022-security-it-vpn-and-antivirus-deals/){:target="_blank" rel="noopener"}

> Black Friday is almost here, and great deals are already live today for computer security, software, online courses, system admin services, antivirus, and VPN software. [...]
