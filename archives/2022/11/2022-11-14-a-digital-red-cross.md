Title: A Digital Red Cross
Date: 2022-11-14T12:38:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;healthcare;infrastructure;medicine;ransomware
Slug: 2022-11-14-a-digital-red-cross

[Source](https://www.schneier.com/blog/archives/2022/11/a-digital-red-cross.html){:target="_blank" rel="noopener"}

> The International Committee of the Red Cross wants some digital equivalent to the iconic red cross, to alert would-be hackers that they are accessing a medical network. The emblem wouldn’t provide technical cybersecurity protection to hospitals, Red Cross infrastructure or other medical providers, but it would signal to hackers that a cyberattack on those protected networks during an armed conflict would violate international humanitarian law, experts say, Tilman Rodenhäuser, a legal adviser to the International Committee of the Red Cross, said at a panel discussion hosted by the organization on Thursday. I can think of all sorts of problems with [...]
