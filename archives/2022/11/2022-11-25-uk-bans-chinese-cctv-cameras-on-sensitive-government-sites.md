Title: UK bans Chinese CCTV cameras on 'sensitive' government sites
Date: 2022-11-25T00:30:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-25-uk-bans-chinese-cctv-cameras-on-sensitive-government-sites

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/25/uk_government_china_cctv_ban_/){:target="_blank" rel="noopener"}

> Agencies told to rip 'em off core networks and replace 'em whenever and wherever possible The United Kingdom has decided Chinese video cameras have no place in government facilities.... [...]
