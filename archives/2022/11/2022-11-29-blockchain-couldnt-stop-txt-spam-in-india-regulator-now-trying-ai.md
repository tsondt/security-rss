Title: Blockchain couldn't stop TXT spam in India, regulator now trying AI
Date: 2022-11-29T02:29:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-29-blockchain-couldnt-stop-txt-spam-in-india-regulator-now-trying-ai

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/29/india_txt_spam_crackdown/){:target="_blank" rel="noopener"}

> Maybe – just maybe – messages and calls from +91 might become more trustworthy India's Telecom Regulatory Authority (TRAI) has announced a fresh crackdown on TXT spam – this time using artificial intelligence, after a previous blockchain-powered effort delivered mixed results.... [...]
