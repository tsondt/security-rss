Title: Vodafone Italy discloses data breach after reseller hacked
Date: 2022-11-02T13:05:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-02-vodafone-italy-discloses-data-breach-after-reseller-hacked

[Source](https://www.bleepingcomputer.com/news/security/vodafone-italy-discloses-data-breach-after-reseller-hacked/){:target="_blank" rel="noopener"}

> Vodafone Italia is sending customers notices of a data breach, informing them that one of its commercial partners, FourB S.p.A., who operates as a reseller of the telco's services in the country, has fallen victim to a cyberattack. [...]
