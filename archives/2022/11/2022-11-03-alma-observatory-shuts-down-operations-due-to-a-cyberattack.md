Title: ALMA Observatory shuts down operations due to a cyberattack
Date: 2022-11-03T10:46:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-03-alma-observatory-shuts-down-operations-due-to-a-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/alma-observatory-shuts-down-operations-due-to-a-cyberattack/){:target="_blank" rel="noopener"}

> The Atacama Large Millimeter Array (ALMA) Observatory in Chile has suspended all astronomical observation operations and taken its public website offline following a cyberattack on Saturday, October 29, 2022. [...]
