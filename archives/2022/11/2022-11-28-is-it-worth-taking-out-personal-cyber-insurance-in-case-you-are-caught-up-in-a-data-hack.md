Title: Is it worth taking out personal cyber insurance in case you are caught up in a data hack?
Date: 2022-11-28T23:30:11+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Cybercrime;Hacking;Australia news;Data and computer security;Technology;Optus;Medibank;Internet
Slug: 2022-11-28-is-it-worth-taking-out-personal-cyber-insurance-in-case-you-are-caught-up-in-a-data-hack

[Source](https://www.theguardian.com/technology/2022/nov/29/is-it-worth-taking-out-personal-cyber-insurance-in-case-you-are-caught-up-in-a-data-hack){:target="_blank" rel="noopener"}

> Experts say investing in identity theft protection may provide peace of mind, but won’t help recover lost information Get our morning and afternoon news emails, free app or daily news podcast The recent Optus and Medibank data breaches in which thousands of Australians had their personal information stolen have heightened public consciousness of the threat of identity fraud. Information including names, dates of birth, addresses, phone numbers, passport and Medicare numbers, and even healthcare claims have been posted online in the past few months as a result of the high profile breaches. Sign up for Guardian Australia’s free morning and [...]
