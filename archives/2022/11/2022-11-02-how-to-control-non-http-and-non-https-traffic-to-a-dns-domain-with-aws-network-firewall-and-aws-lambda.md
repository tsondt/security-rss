Title: How to control non-HTTP and non-HTTPS traffic to a DNS domain with AWS Network Firewall and AWS Lambda
Date: 2022-11-02T18:15:31+00:00
Author: Tyler Applebaum
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;AWS Lambda;AWS Network Firewall;DNS;Security Blog
Slug: 2022-11-02-how-to-control-non-http-and-non-https-traffic-to-a-dns-domain-with-aws-network-firewall-and-aws-lambda

[Source](https://aws.amazon.com/blogs/security/how-to-control-non-http-and-non-https-traffic-to-a-dns-domain-with-aws-network-firewall-and-aws-lambda/){:target="_blank" rel="noopener"}

> Security and network administrators can control outbound access from a virtual private cloud (VPC) to specific destinations by using a service like AWS Network Firewall. You can use stateful rule groups to control outbound access to domains for HTTP and HTTPS by default in Network Firewall. In this post, we’ll walk you through how to [...]
