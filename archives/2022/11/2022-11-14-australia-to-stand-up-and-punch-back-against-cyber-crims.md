Title: Australia to 'stand up and punch back' against cyber crims
Date: 2022-11-14T01:15:18+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-14-australia-to-stand-up-and-punch-back-against-cyber-crims

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/14/australia_offensive_ops_against_ransomware/){:target="_blank" rel="noopener"}

> Creates 100-strong squad comprising cops and spooks with remit to disrupt ransomware ops Australia's government has declared the nation is planning to go on the offensive against international cyber crooks following recent high-profile attacks on local health insurer Medibank and telco Optus.... [...]
