Title: Kioxia warns of potential cost of US chip policy over China
Date: 2022-11-01T10:30:11+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-11-01-kioxia-warns-of-potential-cost-of-us-chip-policy-over-china

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/01/kioxia_china_us_policy/){:target="_blank" rel="noopener"}

> Nice NAND industry you have there, would be a shame if something happened to it Attempts to reorganize supply chains to cut out China and foil its attempts to build a high-tech chip industry will be costly and may simply cause the Middle Kingdom to redouble its efforts, says memory maker Kioxia.... [...]
