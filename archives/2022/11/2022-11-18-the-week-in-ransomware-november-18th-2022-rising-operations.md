Title: The Week in Ransomware - November 18th 2022 - Rising Operations
Date: 2022-11-18T17:13:26-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-18-the-week-in-ransomware-november-18th-2022-rising-operations

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-18th-2022-rising-operations/){:target="_blank" rel="noopener"}

> There have been some interesting developments in ransomware this week, with the arrest of a cybercrime ring leader and reports shedding light on two new, but up-and-coming, ransomware operations. [...]
