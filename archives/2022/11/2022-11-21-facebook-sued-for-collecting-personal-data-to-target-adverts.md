Title: Facebook sued for collecting personal data to target adverts
Date: 2022-11-21T14:33:59+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: Meta;Facebook;Data protection;Social networking;Privacy;Data and computer security;UK news
Slug: 2022-11-21-facebook-sued-for-collecting-personal-data-to-target-adverts

[Source](https://www.theguardian.com/technology/2022/nov/21/woman-launches-high-court-challenge-of-facebook-use-of-personal-data-for-ads){:target="_blank" rel="noopener"}

> In high court case that could set precedent for millions, Tanya O’Carroll alleges owner Meta is breaking UK data laws A human rights campaigner is suing Facebook’s owner in the high court, claiming the company is disregarding her right to object against the collection of her personal data. Tanya O’Carroll has launched a lawsuit against Mark Zuckerberg’s Meta alleging it has breached UK data laws by failing to respect her right to demand Facebook stop collecting and processing her data. Facebook generates revenue from building profiles of users and matching them with advertisers who direct ads at people targeting their [...]
