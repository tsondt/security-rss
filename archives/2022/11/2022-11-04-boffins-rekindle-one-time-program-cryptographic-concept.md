Title: Boffins rekindle one-time program cryptographic concept
Date: 2022-11-04T14:13:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-04-boffins-rekindle-one-time-program-cryptographic-concept

[Source](https://portswigger.net/daily-swig/boffins-rekindle-one-time-program-cryptographic-concept){:target="_blank" rel="noopener"}

> Authentication idea advanced but not yet fulfilled [...]
