Title: 'iSpoof' service dismantled, main operator and 145 users arrested
Date: 2022-11-24T06:04:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-24-ispoof-service-dismantled-main-operator-and-145-users-arrested

[Source](https://www.bleepingcomputer.com/news/security/ispoof-service-dismantled-main-operator-and-145-users-arrested/){:target="_blank" rel="noopener"}

> The 'iSpoof' online spoofing service has been dismantled following an international law enforcement investigation that also led to the arrest of 146 people, including the suspected mastermind of the operation. [...]
