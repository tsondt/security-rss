Title: Notorious Emotet botnet returns after a few months off
Date: 2022-11-17T08:30:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-11-17-notorious-emotet-botnet-returns-after-a-few-months-off

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/17/emotet_botnet_returns/){:target="_blank" rel="noopener"}

> And it's been sending out hundreds of thousands of malicious emails a day The Emotet malware-delivery botnet is back after a short hiatus, quickly ramping up the number of malicious emails it's sending and sporting additional capabilities, including changes to its binary and delivering a new version of the IcedID malware dropper.... [...]
