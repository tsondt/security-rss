Title: Tailscale VPN nodes vulnerable to DNS rebinding, RCE
Date: 2022-11-30T13:28:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-30-tailscale-vpn-nodes-vulnerable-to-dns-rebinding-rce

[Source](https://portswigger.net/daily-swig/tailscale-vpn-nodes-vulnerable-to-dns-rebinding-rce){:target="_blank" rel="noopener"}

> Users should manually update to the latest version now [...]
