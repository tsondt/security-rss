Title: Can confidential computing stop the next crypto heist?
Date: 2022-11-07T13:30:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-07-can-confidential-computing-stop-the-next-crypto-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/07/confidential_computing_crypto_heists/){:target="_blank" rel="noopener"}

> Tech giants and startups rush into the next big thing in security The theft of billions of dollars in cryptocurrency over recent months could have been prevented, and confidential computing is a key to the security fix.... [...]
