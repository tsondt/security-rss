Title: For a successful cloud transformation, change your culture first
Date: 2022-11-07T17:30:00+00:00
Author: Tony Richards
Category: GCP Security
Tags: Identity & Security
Slug: 2022-11-07-for-a-successful-cloud-transformation-change-your-culture-first

[Source](https://cloud.google.com/blog/products/identity-security/culture-comes-first-in-cloud-transformations/){:target="_blank" rel="noopener"}

> Twenty years ago, organizations discovered the magic of server virtualization. Among the many benefits of virtualization, it heralded the end of the need for one physical server per application and the beginning of managing software-defined infrastructure. It helped automate building and installing systems, and it helped lay foundations for today’s Continuous Integration/Continuous Delivery mechanisms and DevOps practices. However, the adoption of virtualization was not always smooth and several hurdles stood in its path. Today’s organizations are facing similar obstacles in their journey to the cloud. Organizations that wanted to virtualize their server environment two decades ago faced a significant challenge [...]
