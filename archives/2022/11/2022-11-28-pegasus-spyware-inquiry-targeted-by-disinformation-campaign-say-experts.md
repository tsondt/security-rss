Title: Pegasus spyware inquiry targeted by disinformation campaign, say experts
Date: 2022-11-28T12:14:29+00:00
Author: Stephanie Kirchgaessner in Washington and Sam Jones in Madrid
Category: The Guardian
Tags: Surveillance;Espionage;European Union;Spain;Hacking;Israel;Technology;Europe;World news;Malware;Data and computer security
Slug: 2022-11-28-pegasus-spyware-inquiry-targeted-by-disinformation-campaign-say-experts

[Source](https://www.theguardian.com/world/2022/nov/28/pegasus-eu-parliament-spyware-inquiry-targeted-disinformation-campaign){:target="_blank" rel="noopener"}

> European parliament is investigating powerful surveillance tool used by governments around the world Victims of spyware and a group of security experts have privately warned that a European parliament investigatory committee risks being thrown off course by an alleged “disinformation campaign”. The warning, contained in a letter to MEPs signed by the victims, academics and some of the world’s most renowned surveillance experts, followed news last week that two individuals accused of trying to discredit widely accepted evidence in spyware cases in Spain had been invited to appear before the committee investigating abuse of hacking software. Continue reading... [...]
