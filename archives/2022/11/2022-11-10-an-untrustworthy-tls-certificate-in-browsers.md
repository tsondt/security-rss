Title: An Untrustworthy TLS Certificate in Browsers
Date: 2022-11-10T15:18:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;browsers;certificates;TLS;trust
Slug: 2022-11-10-an-untrustworthy-tls-certificate-in-browsers

[Source](https://www.schneier.com/blog/archives/2022/11/an-untrustworthy-tls-certificate-in-browsers.html){:target="_blank" rel="noopener"}

> The major browsers natively trust a whole bunch of certificate authorities, and some of them are really sketchy : Google’s Chrome, Apple’s Safari, nonprofit Firefox and others allow the company, TrustCor Systems, to act as what’s known as a root certificate authority, a powerful spot in the internet’s infrastructure that guarantees websites are not fake, guiding users to them seamlessly. The company’s Panamanian registration records show that it has the identical slate of officers, agents and partners as a spyware maker identified this year as an affiliate of Arizona-based Packet Forensics, which public contracting records and company documents show has [...]
