Title: Previously unidentified ARCrypter ransomware expands worldwide
Date: 2022-11-17T15:07:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-17-previously-unidentified-arcrypter-ransomware-expands-worldwide

[Source](https://www.bleepingcomputer.com/news/security/previously-unidentified-arcrypter-ransomware-expands-worldwide/){:target="_blank" rel="noopener"}

> A previously unknown 'ARCrypter' ransomware that compromised key organizations in Latin America is now expanding its attacks worldwide. [...]
