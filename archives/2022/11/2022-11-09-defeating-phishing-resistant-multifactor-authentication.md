Title: Defeating Phishing-Resistant Multifactor Authentication
Date: 2022-11-09T12:18:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;phishing;two-factor authentication
Slug: 2022-11-09-defeating-phishing-resistant-multifactor-authentication

[Source](https://www.schneier.com/blog/archives/2022/11/defeating-phishing-resistant-multifactor-authentication.html){:target="_blank" rel="noopener"}

> CISA is now pushing phishing-resistant multifactor authentication. Roger Grimes has an excellent post reminding everyone that “phishing-resistant” is not “phishing proof,” and that everyone needs to stop pretending otherwise. His list of different attacks is particularly useful. [...]
