Title: New Book: A Hacker’s Mind
Date: 2022-11-11T20:11:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;books;Schneier news
Slug: 2022-11-11-new-book-a-hackers-mind

[Source](https://www.schneier.com/blog/archives/2022/11/new-book-a-hackers-mind.html){:target="_blank" rel="noopener"}

> I have a new book coming out in February. It’s about hacking. A Hacker’s Mind: How the Powerful Bend Society’s Rules, and How to Bend them Back isn’t about hacking computer systems; it’s about hacking more general economic, political, and social systems. It generalizes the term hack as a means of subverting a system’s rules in unintended ways. What sorts of system? Any system of rules, really. Take the tax code, for example. It’s not computer code, but it’s a series of algorithms—supposedly deterministic—that take a bunch of inputs about your income and produce an output that’s the amount of [...]
