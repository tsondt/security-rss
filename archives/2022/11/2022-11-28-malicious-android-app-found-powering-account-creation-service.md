Title: Malicious Android app found powering account creation service
Date: 2022-11-28T17:52:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2022-11-28-malicious-android-app-found-powering-account-creation-service

[Source](https://www.bleepingcomputer.com/news/security/malicious-android-app-found-powering-account-creation-service/){:target="_blank" rel="noopener"}

> ​A fake Android SMS application, with 100,000 downloads on the Google Play store, has been discovered to secretly act as an SMS relay for an account creation service for sites like Microsoft, Google, Instagram, Telegram, and Facebook [...]
