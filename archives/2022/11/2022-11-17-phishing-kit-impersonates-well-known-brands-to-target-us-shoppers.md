Title: Phishing kit impersonates well-known brands to target US shoppers
Date: 2022-11-17T18:44:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-17-phishing-kit-impersonates-well-known-brands-to-target-us-shoppers

[Source](https://www.bleepingcomputer.com/news/security/phishing-kit-impersonates-well-known-brands-to-target-us-shoppers/){:target="_blank" rel="noopener"}

> A sophisticated phishing kit has been targeting North Americans since mid-September, using lures focused on holidays like Labor Day and Halloween. [...]
