Title: Maple Leaf Foods suffers outage following weekend cyberattack
Date: 2022-11-07T12:59:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-07-maple-leaf-foods-suffers-outage-following-weekend-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/maple-leaf-foods-suffers-outage-following-weekend-cyberattack/){:target="_blank" rel="noopener"}

> Maple Leaf Foods confirmed on Sunday that it experienced a cybersecurity incident causing a system outage and disruption of operations. [...]
