Title: OpenSSL 3 patch, once Heartbleed-level “critical,” arrives as a lesser “high”
Date: 2022-11-01T18:05:11+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Tech;heartbleed;openssl;patch;ssl;vulnerability
Slug: 2022-11-01-openssl-3-patch-once-heartbleed-level-critical-arrives-as-a-lesser-high

[Source](https://arstechnica.com/?p=1894214){:target="_blank" rel="noopener"}

> Enlarge / The fallout of an OpenSSL vulnerability, initially listed as "critical," should be much less severe than that of the last critical OpenSSL bug, Heartbleed. An OpenSSL vulnerability once signaled as the first critical-level patch since the Internet-reshaping Heartbleed bug has just been patched. It ultimately arrived as a "high" security fix for a buffer overflow, one that affects all OpenSSL 3.x installations, but is unlikely to lead to remote code execution. OpenSSL version 3.0.7 was announced last week as a critical security fix release. The specific vulnerabilities (now CVE-2022-37786 and CVE-2022-3602 ) had been largely unknown until today, [...]
