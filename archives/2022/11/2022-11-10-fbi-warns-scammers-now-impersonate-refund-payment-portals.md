Title: FBI warns scammers now impersonate refund payment portals
Date: 2022-11-10T12:09:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-10-fbi-warns-scammers-now-impersonate-refund-payment-portals

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-scammers-now-impersonate-refund-payment-portals/){:target="_blank" rel="noopener"}

> The FBI warns that tech support scammers are now impersonating financial institutions' refund payment portals to harvest victims' sensitive information and add legitimacy. [...]
