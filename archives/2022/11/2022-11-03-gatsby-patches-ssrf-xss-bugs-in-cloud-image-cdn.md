Title: Gatsby patches SSRF, XSS bugs in Cloud Image CDN
Date: 2022-11-03T13:22:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-11-03-gatsby-patches-ssrf-xss-bugs-in-cloud-image-cdn

[Source](https://portswigger.net/daily-swig/gatsby-patches-ssrf-xss-bugs-in-cloud-image-cdn){:target="_blank" rel="noopener"}

> Remediation compared to ‘changing the tires on a car while in motion’ [...]
