Title: Wells Fargo, Zelle slammed by Liz Warren over rampant online banking fraud
Date: 2022-11-09T21:15:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-09-wells-fargo-zelle-slammed-by-liz-warren-over-rampant-online-banking-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/09/warren_wells_fargo_zelle_fraud/){:target="_blank" rel="noopener"}

> Customers 'more than twice' as likely to be hit by scams, says Dem Senator Wells Fargo customers who use Zelle to send and request payments suffer more than twice the rate of fraud and other online scams as people using other big banks, according to US Senator Elizabeth Warren (D-MA).... [...]
