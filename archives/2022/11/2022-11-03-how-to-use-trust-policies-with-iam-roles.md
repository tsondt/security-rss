Title: How to use trust policies with IAM roles
Date: 2022-11-03T17:31:04+00:00
Author: Jonathan Jenkyn
Category: AWS Security
Tags: Advanced (300);AWS Identity and Access Management (IAM);Security, Identity, & Compliance;AWS IAM;Privilege escalation;roles;Security Blog;Trust policy
Slug: 2022-11-03-how-to-use-trust-policies-with-iam-roles

[Source](https://aws.amazon.com/blogs/security/how-to-use-trust-policies-with-iam-roles/){:target="_blank" rel="noopener"}

> November 3, 2022: We updated this post to fix some syntax errors in the policy statements and to add additional use cases. August 30, 2021: This post is currently being updated. We will post another note when it’s complete. AWS Identity and Access Management (IAM) roles are a significant component of the way that customers [...]
