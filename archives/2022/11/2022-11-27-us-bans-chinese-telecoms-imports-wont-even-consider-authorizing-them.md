Title: US bans Chinese telecoms imports – won't even consider authorizing them
Date: 2022-11-27T22:32:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-11-27-us-bans-chinese-telecoms-imports-wont-even-consider-authorizing-them

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/27/fcc_china_equipment_authorization_ban/){:target="_blank" rel="noopener"}

> Part bureaucratic box ticking, part crackdown that makes even Wi-Fi routers and smartphones off limits The United States' Federal Communications Commission (FCC) has barred itself from authorizing the import or sale of Chinese telecoms and video surveillance products from Huawei, ZTE, Hytera Communications, Hikvision, and Dahua, on national security grounds.... [...]
