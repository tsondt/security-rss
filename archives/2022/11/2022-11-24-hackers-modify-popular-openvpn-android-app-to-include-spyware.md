Title: Hackers modify popular OpenVPN Android app to include spyware
Date: 2022-11-24T10:29:49-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-11-24-hackers-modify-popular-openvpn-android-app-to-include-spyware

[Source](https://www.bleepingcomputer.com/news/security/hackers-modify-popular-openvpn-android-app-to-include-spyware/){:target="_blank" rel="noopener"}

> A threat actor associated with cyberespionage operations since at least 2017 has been luring victims with fake VPN software for Android that is a trojanized version of legitimate software SoftVPN and OpenVPN. [...]
