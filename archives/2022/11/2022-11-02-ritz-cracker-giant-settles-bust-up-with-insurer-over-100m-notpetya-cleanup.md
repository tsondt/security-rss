Title: Ritz cracker giant settles bust-up with insurer over $100m+ NotPetya cleanup
Date: 2022-11-02T07:29:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-02-ritz-cracker-giant-settles-bust-up-with-insurer-over-100m-notpetya-cleanup

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/02/mondelez_zurich_notpetya_settlement/){:target="_blank" rel="noopener"}

> Deal could 'upend the entire cyber-insurance ecosystem and make it almost impossible to get meaningful cyber coverage' Mondelez International has settled its lawsuit against Zurich American Insurance Company, which it brought because the insurer refused to cover the snack giant's $100-million-plus cleanup bill following the 2017 NotPetya outbreak.... [...]
