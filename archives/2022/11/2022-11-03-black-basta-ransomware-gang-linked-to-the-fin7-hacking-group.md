Title: Black Basta ransomware gang linked to the FIN7 hacking group
Date: 2022-11-03T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-03-black-basta-ransomware-gang-linked-to-the-fin7-hacking-group

[Source](https://www.bleepingcomputer.com/news/security/black-basta-ransomware-gang-linked-to-the-fin7-hacking-group/){:target="_blank" rel="noopener"}

> Security researchers at Sentinel Labs have uncovered evidence that links the Black Basta ransomware gang to the financially motivated hacking group FIN7, also known as "Carbanak." [...]
