Title: LinkedIn Adds Verified Emails, Profile Creation Dates
Date: 2022-11-04T21:09:52+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Employment Fraud;Security Tools
Slug: 2022-11-04-linkedin-adds-verified-emails-profile-creation-dates

[Source](https://krebsonsecurity.com/2022/11/linkedin-adds-verified-emails-profile-creation-dates/){:target="_blank" rel="noopener"}

> Responding to a recent surge in AI-generated bot accounts, LinkedIn is rolling out new features that it hopes will help users make more informed decisions about with whom they choose to connect. Many LinkedIn profiles now display a creation date, and the company is expanding its domain validation offering, which allows users to publicly confirm that they can reply to emails at the domain of their stated current employer. LinkedIn’s new “About This Profile” section — which is visible by clicking the “More” button at the top of a profile — includes the year the account was created, the last [...]
