Title: See yourself in cyber: Highlights from Cybersecurity Awareness Month
Date: 2022-11-02T22:44:22+00:00
Author: CJ Moses
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Cybersecurity awareness;MFA;Security;Security Blog;Training
Slug: 2022-11-02-see-yourself-in-cyber-highlights-from-cybersecurity-awareness-month

[Source](https://aws.amazon.com/blogs/security/see-yourself-in-cyber-highlights-from-cybersecurity-awareness-month/){:target="_blank" rel="noopener"}

> As Cybersecurity Awareness Month comes to a close, we want to share some of the work we’ve done and made available to you throughout October. Over the last four weeks, we have shared insights and resources aligned with this year’s theme—”See Yourself in Cyber”—to help advance awareness training, and inspire people to join the rapidly [...]
