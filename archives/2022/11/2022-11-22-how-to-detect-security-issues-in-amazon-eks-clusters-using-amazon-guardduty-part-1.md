Title: How to detect security issues in Amazon EKS clusters using Amazon GuardDuty – Part 1
Date: 2022-11-22T18:39:46+00:00
Author: Marshall Jones
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Containers;Detective;EKS;GuardDuty;Incident response;Kubernetes;Security;Security Blog;threat detection
Slug: 2022-11-22-how-to-detect-security-issues-in-amazon-eks-clusters-using-amazon-guardduty-part-1

[Source](https://aws.amazon.com/blogs/security/how-to-detect-security-issues-in-amazon-eks-clusters-using-amazon-guardduty-part-1/){:target="_blank" rel="noopener"}

> In this two-part blog post, we’ll discuss how to detect and investigate security issues in an Amazon Elastic Kubernetes Service (Amazon EKS) cluster with Amazon GuardDuty and Amazon Detective. Amazon Elastic Kubernetes Service (Amazon EKS) is a managed service that you can use to run and scale container workloads by using Kubernetes in the AWS [...]
