Title: As Twitter brings on $8 fee, phishing emails target verified accounts
Date: 2022-11-04T05:55:20-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-11-04-as-twitter-brings-on-8-fee-phishing-emails-target-verified-accounts

[Source](https://www.bleepingcomputer.com/news/security/as-twitter-brings-on-8-fee-phishing-emails-target-verified-accounts/){:target="_blank" rel="noopener"}

> As Twitter announces plans to charge users $8 a month for Twitter Blue and verification under Elon Musk's management, BleepingComputer has come across several phishing emails targeting verified users. [...]
