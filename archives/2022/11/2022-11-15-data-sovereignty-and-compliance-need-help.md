Title: Data sovereignty and compliance need help
Date: 2022-11-15T09:00:08+00:00
Author: Dave Cartwright
Category: The Register
Tags: 
Slug: 2022-11-15-data-sovereignty-and-compliance-need-help

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/15/data_sovereignty_and_compliance_need/){:target="_blank" rel="noopener"}

> It’s a critical issue which our poll suggests influences the choice of on and off prem hosting platforms Reader Survey Results Back in September, we asked readers of The Register about data sovereignty. It's a concept about which we see more and more conversation among businesses, and increased awareness is also bringing corresponding concerns about the perils and pitfalls of not taking it seriously.... [...]
