Title: Friday Squid Blogging: Squid Purse
Date: 2022-11-11T22:18:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-11-11-friday-squid-blogging-squid-purse

[Source](https://www.schneier.com/blog/archives/2022/11/friday-squid-blogging-squid-purse.html){:target="_blank" rel="noopener"}

> Perfect for an evening out. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
