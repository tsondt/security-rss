Title: OpenSSL fixes two high severity vulnerabilities, what you need to know
Date: 2022-11-01T12:39:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-01-openssl-fixes-two-high-severity-vulnerabilities-what-you-need-to-know

[Source](https://www.bleepingcomputer.com/news/security/openssl-fixes-two-high-severity-vulnerabilities-what-you-need-to-know/){:target="_blank" rel="noopener"}

> The OpenSSL Project has patched two high-severity security flaws in its open-source cryptographic library used to encrypt communication channels and HTTPS connections. [...]
