Title: New ransomware encrypts files, then steals your Discord account
Date: 2022-11-20T10:07:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-20-new-ransomware-encrypts-files-then-steals-your-discord-account

[Source](https://www.bleepingcomputer.com/news/security/new-ransomware-encrypts-files-then-steals-your-discord-account/){:target="_blank" rel="noopener"}

> The new 'AXLocker' ransomware family is not only encrypting victims' files and demanding a ransom payment but also stealing the Discord accounts of infected users. [...]
