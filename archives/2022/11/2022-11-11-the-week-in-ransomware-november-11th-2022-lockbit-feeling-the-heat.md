Title: The Week in Ransomware - November 11th 2022 - LockBit feeling the heat
Date: 2022-11-11T17:25:04-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-11-the-week-in-ransomware-november-11th-2022-lockbit-feeling-the-heat

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-11th-2022-lockbit-feeling-the-heat/){:target="_blank" rel="noopener"}

> This 'Week in Ransomware' covers the last two weeks of ransomware news, with new information on attacks, arrests, data wipers, and reports shared by cybersecurity firms and researchers. [...]
