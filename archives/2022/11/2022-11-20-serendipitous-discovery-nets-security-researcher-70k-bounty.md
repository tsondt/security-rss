Title: Serendipitous discovery nets security researcher $70k bounty
Date: 2022-11-20T09:00:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-20-serendipitous-discovery-nets-security-researcher-70k-bounty

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/20/in_brief_security/){:target="_blank" rel="noopener"}

> Also, a phishing gang goes Royal, while another employee at Snowden's old haunt gets caught nabbing data In brief A security researcher whose Google Pixel battery died while sending a text is probably thankful for the interruption - powering it back up led to a discovery that netted him a $70,000 bounty from Google for a lock screen bypass bug.... [...]
