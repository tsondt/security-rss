Title: US charges BEC suspects with targeting federal health care programs
Date: 2022-11-18T12:26:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-18-us-charges-bec-suspects-with-targeting-federal-health-care-programs

[Source](https://www.bleepingcomputer.com/news/security/us-charges-bec-suspects-with-targeting-federal-health-care-programs/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DOJ) has charged ten defendants for their alleged involvement in business email compromise (BEC) schemes targeting numerous victims across the country, including U.S. federal funding programs like Medicare and Medicaid. [...]
