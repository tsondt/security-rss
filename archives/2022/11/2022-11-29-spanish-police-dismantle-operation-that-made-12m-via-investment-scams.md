Title: Spanish police dismantle operation that made €12M via investment scams
Date: 2022-11-29T11:46:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-29-spanish-police-dismantle-operation-that-made-12m-via-investment-scams

[Source](https://www.bleepingcomputer.com/news/security/spanish-police-dismantle-operation-that-made-12m-via-investment-scams/){:target="_blank" rel="noopener"}

> Spanish National Police have dismantled a cybercrime organization that used fake investment sites to defraud over €12.3 million ($12.8 million) from 300 victims across Europe. [...]
