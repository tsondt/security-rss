Title: Instagram star gets 11 years for $300m email scam plot
Date: 2022-11-10T20:46:31+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-11-10-instagram-star-gets-11-years-for-300m-email-scam-plot

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/10/instagram_star_bec_prison/){:target="_blank" rel="noopener"}

> Hushpuppi swaps private jet, Dubai penthouse for prison duds and $1.7m to victims An international cyber-scammer and Instagram star who plotted to launder more than $300 million over the course of 18 months was this week jailed – and he must pay back more than $1.7 million to his victims.... [...]
