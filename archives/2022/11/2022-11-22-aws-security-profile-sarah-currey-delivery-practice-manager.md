Title: AWS Security Profile: Sarah Currey, Delivery Practice Manager
Date: 2022-11-22T21:19:22+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Security Profile;Re:Invent 2022;reInvent;Security Blog
Slug: 2022-11-22-aws-security-profile-sarah-currey-delivery-practice-manager

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-sarah-currey-delivery-practice-manager/){:target="_blank" rel="noopener"}

> In the weeks leading up to AWS re:invent 2022, I’ll share conversations I’ve had with some of the humans who work in AWS Security who will be presenting at the conference, and get a sneak peek at their work and sessions. In this profile, I interviewed Sarah Currey, Delivery Practice Manager in World Wide Professional [...]
