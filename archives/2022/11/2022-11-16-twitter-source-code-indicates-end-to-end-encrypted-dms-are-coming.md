Title: Twitter source code indicates end-to-end encrypted DMs are coming
Date: 2022-11-16T11:55:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-16-twitter-source-code-indicates-end-to-end-encrypted-dms-are-coming

[Source](https://www.bleepingcomputer.com/news/security/twitter-source-code-indicates-end-to-end-encrypted-dms-are-coming/){:target="_blank" rel="noopener"}

> Twitter is reportedly working on finally adding end-to-end encryption (E2EE) for direct messages (DMs) exchanged between users on the social media platform. [...]
