Title: How to configure rotation and rotation windows for secrets stored in AWS Secrets Manager
Date: 2022-11-21T21:46:13+00:00
Author: Faith Isichei
Category: AWS Security
Tags: Announcements;Intermediate (200);Learning Levels;Security, Identity, & Compliance;AWS Secrets Manager;Cloud security;rotation;Security Blog
Slug: 2022-11-21-how-to-configure-rotation-and-rotation-windows-for-secrets-stored-in-aws-secrets-manager

[Source](https://aws.amazon.com/blogs/security/how-to-configure-rotation-windows-for-secrets-stored-in-aws-secrets-manager/){:target="_blank" rel="noopener"}

> November 21, 2022: We updated this post to reflect the fact that AWS Secrets Manager now supports rotating secrets as often as every four hours. AWS Secrets Manager helps you manage, retrieve, and rotate database credentials, API keys, and other secrets throughout their lifecycles. You can specify a rotation window for your secrets, allowing you [...]
