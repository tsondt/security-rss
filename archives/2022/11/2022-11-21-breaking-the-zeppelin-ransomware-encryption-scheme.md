Title: Breaking the Zeppelin Ransomware Encryption Scheme
Date: 2022-11-21T12:08:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptanalysis;cybersecurity;encryption;ransomware
Slug: 2022-11-21-breaking-the-zeppelin-ransomware-encryption-scheme

[Source](https://www.schneier.com/blog/archives/2022/11/breaking-the-zeppelin-ransomware-encryption-scheme.html){:target="_blank" rel="noopener"}

> Brian Krebs writes about how the Zeppelin ransomware encryption scheme was broken: The researchers said their break came when they understood that while Zeppelin used three different types of encryption keys to encrypt files, they could undo the whole scheme by factoring or computing just one of them: An ephemeral RSA-512 public key that is randomly generated on each machine it infects. “If we can recover the RSA-512 Public Key from the registry, we can crack it and get the 256-bit AES Key that encrypts the files!” they wrote. “The challenge was that they delete the [public key] once the [...]
