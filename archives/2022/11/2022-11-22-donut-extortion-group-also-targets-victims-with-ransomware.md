Title: Donut extortion group also targets victims with ransomware
Date: 2022-11-22T15:53:01-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-22-donut-extortion-group-also-targets-victims-with-ransomware

[Source](https://www.bleepingcomputer.com/news/security/donut-extortion-group-also-targets-victims-with-ransomware/){:target="_blank" rel="noopener"}

> The Donut (D0nut) extortion group has been confirmed to deploy ransomware in double-extortion attacks on the enterprise. [...]
