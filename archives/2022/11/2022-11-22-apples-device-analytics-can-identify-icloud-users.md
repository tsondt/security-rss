Title: Apple’s Device Analytics Can Identify iCloud Users
Date: 2022-11-22T15:28:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;cloud computing;courts;identification;iOS;privacy;tracking
Slug: 2022-11-22-apples-device-analytics-can-identify-icloud-users

[Source](https://www.schneier.com/blog/archives/2022/11/apples-device-analytics-can-identify-icloud-users.html){:target="_blank" rel="noopener"}

> Researchers claim that supposedly anonymous device analytics information can identify users: On Twitter, security researchers Tommy Mysk and Talal Haj Bakry have found that Apple’s device analytics data includes an iCloud account and can be linked directly to a specific user, including their name, date of birth, email, and associated information stored on iCloud. Apple has long claimed otherwise: On Apple’s device analytics and privacy legal page, the company says no information collected from a device for analytics purposes is traceable back to a specific user. “iPhone Analytics may include details about hardware and operating system specifications, performance statistics, and [...]
