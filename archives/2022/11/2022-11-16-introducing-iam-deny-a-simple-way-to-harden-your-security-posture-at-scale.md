Title: Introducing IAM Deny, a simple way to harden your security posture at scale
Date: 2022-11-16T18:00:00+00:00
Author: Ravi Shah
Category: GCP Security
Tags: Identity & Security
Slug: 2022-11-16-introducing-iam-deny-a-simple-way-to-harden-your-security-posture-at-scale

[Source](https://cloud.google.com/blog/products/identity-security/introducing-iam-deny/){:target="_blank" rel="noopener"}

> At Google Cloud, we’re focused on making it easy for organizations to build solutions quickly and securely. Identity and Access Management (IAM) is the core security control for establishing who has access to which cloud resources and making sure access permissions are aligned to your company’s business and security policies. We are excited to announce the general availability of IAM Deny policies. This new capability helps you easily create access guardrails for Google Cloud resources. With IAM Deny policies, you can create rules that broadly restrict resource access. It provides a powerful, coarse-grained access control to help implement security policies [...]
