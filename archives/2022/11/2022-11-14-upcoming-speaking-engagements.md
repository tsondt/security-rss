Title: Upcoming Speaking Engagements
Date: 2022-11-14T17:01:47+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2022-11-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2022/11/upcoming-speaking-engagements-25.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at the 24th International Information Security Conference in Madrid, Spain, on November 17, 2022. The list is maintained on this page. [...]
