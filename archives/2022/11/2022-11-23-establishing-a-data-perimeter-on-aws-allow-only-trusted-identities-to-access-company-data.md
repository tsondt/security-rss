Title: Establishing a data perimeter on AWS: Allow only trusted identities to access company data
Date: 2022-11-23T17:28:01+00:00
Author: Tatyana Yatskevich
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Data protection;Identity;Network security;Security Blog;service control policies
Slug: 2022-11-23-establishing-a-data-perimeter-on-aws-allow-only-trusted-identities-to-access-company-data

[Source](https://aws.amazon.com/blogs/security/establishing-a-data-perimeter-on-aws-allow-only-trusted-identities-to-access-company-data/){:target="_blank" rel="noopener"}

> As described in an earlier blog post, Establishing a data perimeter on AWS, Amazon Web Services (AWS) offers a set of capabilities you can use to implement a data perimeter to help prevent unintended access. One type of unintended access that companies want to prevent is access to corporate data by users who do not [...]
