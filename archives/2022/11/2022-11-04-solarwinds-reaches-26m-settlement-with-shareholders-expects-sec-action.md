Title: SolarWinds reaches $26m settlement with shareholders, expects SEC action
Date: 2022-11-04T21:59:59+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-04-solarwinds-reaches-26m-settlement-with-shareholders-expects-sec-action

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/04/solarwinds_settlement_sec_enforcement/){:target="_blank" rel="noopener"}

> One 8-K filing, two bombshells SolarWinds has agreed to pay $26 million to settle a shareholder lawsuit, and it's also expecting to be slapped with an enforcement action by Uncle Sam – both related to its infamous 2020 supply chain security fiasco, according to the software maker's most recent US regulatory filing.... [...]
