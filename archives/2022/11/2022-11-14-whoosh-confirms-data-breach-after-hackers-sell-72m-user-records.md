Title: Whoosh confirms data breach after hackers sell 7.2M user records
Date: 2022-11-14T13:19:27-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-14-whoosh-confirms-data-breach-after-hackers-sell-72m-user-records

[Source](https://www.bleepingcomputer.com/news/security/whoosh-confirms-data-breach-after-hackers-sell-72m-user-records/){:target="_blank" rel="noopener"}

> The Russian scooter-sharing service Whoosh has confirmed a data breach after hackers started to sell a database containing the details of 7.2 million customers on a hacking forum. [...]
