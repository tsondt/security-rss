Title: Russian Software Company Pretending to Be American
Date: 2022-11-16T11:03:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Russia;smartphones;supply chain
Slug: 2022-11-16-russian-software-company-pretending-to-be-american

[Source](https://www.schneier.com/blog/archives/2022/11/russian-software-company-pretending-to-be-american.html){:target="_blank" rel="noopener"}

> Computer code developed by a company called Pushwoosh is in about 8,000 Apple and Google smartphone apps. The company pretends to be American when it is actually Russian. According to company documents publicly filed in Russia and reviewed by Reuters, Pushwoosh is headquartered in the Siberian town of Novosibirsk, where it is registered as a software company that also carries out data processing. It employs around 40 people and reported revenue of 143,270,000 rubles ($2.4 mln) last year. Pushwoosh is registered with the Russian government to pay taxes in Russia. On social media and in US regulatory filings, however, it [...]
