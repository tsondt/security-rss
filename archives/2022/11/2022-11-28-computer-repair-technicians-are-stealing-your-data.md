Title: Computer Repair Technicians Are Stealing Your Data
Date: 2022-11-28T15:44:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;computer security;privacy
Slug: 2022-11-28-computer-repair-technicians-are-stealing-your-data

[Source](https://www.schneier.com/blog/archives/2022/11/computer-repair-technicians-are-stealing-your-data.html){:target="_blank" rel="noopener"}

> Laptop technicians routinely violate the privacy of the people whose computers they repair: Researchers at University of Guelph in Ontario, Canada, recovered logs from laptops after receiving overnight repairs from 12 commercial shops. The logs showed that technicians from six of the locations had accessed personal data and that two of those shops also copied data onto a personal device. Devices belonging to females were more likely to be snooped on, and that snooping tended to seek more sensitive data, including both sexually revealing and non-sexual pictures, documents, and financial information. [...] In three cases, Windows Quick Access or Recently [...]
