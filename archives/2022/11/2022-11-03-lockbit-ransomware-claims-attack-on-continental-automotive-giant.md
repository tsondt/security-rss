Title: LockBit ransomware claims attack on Continental automotive giant
Date: 2022-11-03T14:25:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-03-lockbit-ransomware-claims-attack-on-continental-automotive-giant

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-claims-attack-on-continental-automotive-giant/){:target="_blank" rel="noopener"}

> The LockBit ransomware gang has claimed responsibility for a cyberattack against the German multinational automotive group Continental. [...]
