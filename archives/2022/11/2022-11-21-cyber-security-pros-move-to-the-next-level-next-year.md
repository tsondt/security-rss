Title: Cyber security pros: move to the next level next year
Date: 2022-11-21T12:55:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-11-21-cyber-security-pros-move-to-the-next-level-next-year

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/21/cyber_security_pros_move_to/){:target="_blank" rel="noopener"}

> SANS’ 2023 cyber security training conferences provide the springboard to a new set of skills Sponsored Post Your skills as a cyber security professional are only as up to date as the threats designed to test them, so it's a good idea to stay ahead of the game and keep refreshing them as often as possible. That's what SANS cyber security training events, held across the US in 2023, are here to help you do.... [...]
