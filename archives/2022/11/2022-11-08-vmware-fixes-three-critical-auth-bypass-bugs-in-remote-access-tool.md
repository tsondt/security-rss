Title: VMware fixes three critical auth bypass bugs in remote access tool
Date: 2022-11-08T15:24:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-11-08-vmware-fixes-three-critical-auth-bypass-bugs-in-remote-access-tool

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-three-critical-auth-bypass-bugs-in-remote-access-tool/){:target="_blank" rel="noopener"}

> VMware has released security updates to address three critical severity vulnerabilities in the Workspace ONE Assist solution that enable remote attackers to bypass authentication and elevate privileges to admin. [...]
