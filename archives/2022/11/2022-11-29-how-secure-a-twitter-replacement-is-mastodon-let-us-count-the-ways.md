Title: How secure a Twitter replacement is Mastodon? Let us count the ways
Date: 2022-11-29T22:05:19+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;mastodon;Twitter;vulnerabilities
Slug: 2022-11-29-how-secure-a-twitter-replacement-is-mastodon-let-us-count-the-ways

[Source](https://arstechnica.com/?p=1900717){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) As Elon Musk critics flee from Twitter, Mastodon seems to be the most common replacement. In the last month, the number of monthly active users on Mastodon has rocketed more than threefold, from about 1 million to 3.5 million, while total number of users jumped from about 6.5 million to 8.7 million. This substantial increase raises important questions about the security of this new platform, and for good reason. Unlike the centralized model of Twitter and virtually every other social media platform, Mastodon is built on a federated model of independent servers, known as instances. In [...]
