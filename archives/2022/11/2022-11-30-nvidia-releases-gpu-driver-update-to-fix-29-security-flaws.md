Title: NVIDIA releases GPU driver update to fix 29 security flaws
Date: 2022-11-30T11:27:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-30-nvidia-releases-gpu-driver-update-to-fix-29-security-flaws

[Source](https://www.bleepingcomputer.com/news/security/nvidia-releases-gpu-driver-update-to-fix-29-security-flaws/){:target="_blank" rel="noopener"}

> NVIDIA has released a security update for its GPU display driver for Windows, containing a fix for a high-severity flaw that threat actors can exploit to perform, among other things, code execution and privilege escalation. [...]
