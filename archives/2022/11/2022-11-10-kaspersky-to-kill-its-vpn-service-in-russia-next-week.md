Title: Kaspersky to kill its VPN service in Russia next week
Date: 2022-11-10T12:42:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2022-11-10-kaspersky-to-kill-its-vpn-service-in-russia-next-week

[Source](https://www.bleepingcomputer.com/news/security/kaspersky-to-kill-its-vpn-service-in-russia-next-week/){:target="_blank" rel="noopener"}

> Kaspersky is stopping the operation and sales of its VPN product, Kaspersky Secure Connection, in the Russian Federation, with the free version to be suspended as early as November 15, 2022. [...]
