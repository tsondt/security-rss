Title: GoTo says hackers breached its dev environment, cloud storage
Date: 2022-11-30T19:14:39-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-11-30-goto-says-hackers-breached-its-dev-environment-cloud-storage

[Source](https://www.bleepingcomputer.com/news/security/goto-says-hackers-breached-its-dev-environment-cloud-storage/){:target="_blank" rel="noopener"}

> Remote access and collaboration company GoTo disclosed today that they suffered a security breach where threat actors gained access to their development environment and third-party cloud storage service. [...]
