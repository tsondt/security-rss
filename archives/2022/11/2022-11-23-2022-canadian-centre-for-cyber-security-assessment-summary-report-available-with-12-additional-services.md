Title: 2022 Canadian Centre for Cyber Security Assessment Summary report available with 12 additional services
Date: 2022-11-23T20:33:40+00:00
Author: Naranjan Goklani
Category: AWS Security
Tags: Announcements;AWS Artifact;Compliance;Federal;Foundational (100);Government;Public Sector;Security;Security, Identity, & Compliance;State or Local Government;AWS Canada (Central) Region;AWS Shared Responsibility Model;Canada;CCCS;CCCS Assessment;classification;Cloud security;Cyber Security;cybersecurity;Federal Government Canada;Government of Canada (GC);ITSG-33;Medium Cloud Security Profile;PBMM;Protected B;Security Blog
Slug: 2022-11-23-2022-canadian-centre-for-cyber-security-assessment-summary-report-available-with-12-additional-services

[Source](https://aws.amazon.com/blogs/security/2022-canadian-centre-for-cyber-security-assessment-summary-report-available-with-12-additional-services/){:target="_blank" rel="noopener"}

> We are pleased to announce the availability of the 2022 Canadian Centre for Cyber Security (CCCS) assessment summary report for Amazon Web Services (AWS). This assessment will bring the total to 132 AWS services and features assessed in the Canada (Central) AWS Region, including 12 additional AWS services. A copy of the summary assessment report is available for [...]
