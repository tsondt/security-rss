Title: Failures in Twitter’s Two-Factor Authentication System
Date: 2022-11-17T10:53:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;cybersecurity;passwords;SMS;Twitter;two-factor authentication;vulnerabilities
Slug: 2022-11-17-failures-in-twitters-two-factor-authentication-system

[Source](https://www.schneier.com/blog/archives/2022/11/failures-in-twitters-two-factor-authentication-system.html){:target="_blank" rel="noopener"}

> Twitter is having intermittent problems with its two-factor authentication system: Not all users are having problems receiving SMS authentication codes, and those who rely on an authenticator app or physical authentication token to secure their Twitter account may not have reason to test the mechanism. But users have been self-reporting issues on Twitter since the weekend, and WIRED confirmed that on at least some accounts, authentication texts are hours delayed or not coming at all. The meltdown comes less than two weeks after Twitter laid off about half of its workers, roughly 3,700 people. Since then, engineers, operations specialists, IT [...]
