Title: When should I use Cloud Armor?
Date: 2022-11-07T17:00:00+00:00
Author: Ammett Williams
Category: GCP Security
Tags: Identity & Security;Developers & Practitioners
Slug: 2022-11-07-when-should-i-use-cloud-armor

[Source](https://cloud.google.com/blog/topics/developers-practitioners/when-should-i-use-cloud-armor/){:target="_blank" rel="noopener"}

> Google Cloud Armor is a well known enterprise-grade DDoS defense and web application firewall service that provides additional security for your applications and websites running on Google Cloud, on-prem or on other platforms. Cloud Armor helps protect against broken access controls, security misconfigurations, cryptographic failures and more. Cloud Armor supports hybrid and multi cloud deployments, out of the box pre-defined OWASP based WAF rules and integration with Security Command Center (SCC), Cloud Monitoring, Cloud Logging and Recaptcha Enterprise. Cloud Armor also uses machine learning to automatically detect threats at layer 7. Cloud Armor works at the network and application layer(s), [...]
