Title: Critical RCE bugs in Android remote keyboard apps with 2M installs
Date: 2022-11-30T18:14:17-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-11-30-critical-rce-bugs-in-android-remote-keyboard-apps-with-2m-installs

[Source](https://www.bleepingcomputer.com/news/security/critical-rce-bugs-in-android-remote-keyboard-apps-with-2m-installs/){:target="_blank" rel="noopener"}

> Three Android applications that allow users to use devices as remote keyboards for their computers have critical vulnerabilities that could expose key presses and enable remote code execution. [...]
