Title: The US Has a Shortage of Bomb-Sniffing Dogs
Date: 2022-11-23T16:23:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bombs;COVID-19;national security policy
Slug: 2022-11-23-the-us-has-a-shortage-of-bomb-sniffing-dogs

[Source](https://www.schneier.com/blog/archives/2022/11/the-us-has-a-shortage-of-bomb-sniffing-dogs.html){:target="_blank" rel="noopener"}

> Nothing beats a dog’s nose for detecting explosives. Unfortunately, there aren’t enough dogs : Last month, the US Government Accountability Office (GAO) released a nearly 100-page report about working dogs and the need for federal agencies to better safeguard their health and wellness. The GOA says that as of February the US federal government had approximately 5,100 working dogs, including detection dogs, across three federal agencies. Another 420 dogs “served the federal government in 24 contractor-managed programs within eight departments and two independent agencies,” the GAO report says. The report also underscores the demands placed on detection dogs and the [...]
