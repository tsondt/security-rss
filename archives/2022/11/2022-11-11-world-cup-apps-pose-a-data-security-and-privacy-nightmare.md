Title: World Cup apps pose a data security and privacy nightmare
Date: 2022-11-11T20:06:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-11-11-world-cup-apps-pose-a-data-security-and-privacy-nightmare

[Source](https://go.theregister.com/feed/www.theregister.com/2022/11/11/world_cup_security/){:target="_blank" rel="noopener"}

> Unless you're fine with Qatar snoops remotely accessing your phone With mandated spyware downloads to tens of thousands of surveillance cameras equipped with facial-recognition technology, the World Cup in Qatar next month is looking more like a data security and privacy nightmare than a celebration of the beautiful game.... [...]
