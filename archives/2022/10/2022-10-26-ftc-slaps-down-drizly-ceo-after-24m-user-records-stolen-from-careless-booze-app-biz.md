Title: FTC slaps down Drizly CEO after 2.4m user records stolen from 'careless' booze app biz
Date: 2022-10-26T00:07:39+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-26-ftc-slaps-down-drizly-ceo-after-24m-user-records-stolen-from-careless-booze-app-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/26/ftc_blames_ceo_drizly_breach/){:target="_blank" rel="noopener"}

> At least this'll give some ammo to CISOs dying for stronger IT defenses Analysis Drizly CEO James Cory Rellas is in the firing line after his company exposed about 2.5 million customers' personal information in a computer security blunder.... [...]
