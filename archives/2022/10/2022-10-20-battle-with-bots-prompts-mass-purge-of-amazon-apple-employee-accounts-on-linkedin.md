Title: Battle with Bots Prompts Mass Purge of Amazon, Apple Employee Accounts on LinkedIn
Date: 2022-10-20T17:07:34+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Employment Fraud;Web Fraud 2.0;Amazon;Anastacia Brown;apple;Binance;Indeed;Jay Pinho;LinkedIn;Mandiant;Nicholas Weaver;SignalHire
Slug: 2022-10-20-battle-with-bots-prompts-mass-purge-of-amazon-apple-employee-accounts-on-linkedin

[Source](https://krebsonsecurity.com/2022/10/battle-with-bots-prompts-mass-purge-of-amazon-apple-employee-accounts-on-linkedin/){:target="_blank" rel="noopener"}

> On October 10, 2022, there were 576,562 LinkedIn accounts that listed their current employer as Apple Inc. The next day, half of those profiles no longer existed. A similarly dramatic drop in the number of LinkedIn profiles claiming employment at Amazon comes as LinkedIn is struggling to combat a significant uptick in the creation of fake employee accounts that pair AI-generated profile photos with text lifted from legitimate users. Jay Pinho is a developer who is working on a product that tracks company data, including hiring. Pinho has been using LinkedIn to monitor daily employee headcounts at several dozen large [...]
