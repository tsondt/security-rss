Title: Introducing Confidential Space to help unlock the value of secure data collaboration
Date: 2022-10-11T11:58:00+00:00
Author: Nelly Porter
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-10-11-introducing-confidential-space-to-help-unlock-the-value-of-secure-data-collaboration

[Source](https://cloud.google.com/blog/products/identity-security/announcing-confidential-space/){:target="_blank" rel="noopener"}

> Business partnerships across many industries strain under rules and requirements that prevent them from sharing sensitive data. Organizations also recognize that collaboration can accelerate innovation, but meaningful collaboration can be limited or even prevented by the need to protect intellectual property or regulated data. Rising to meet today’s business challenges can require companies to collaborate across internal company silos, with outside organizations, and across geographies, while pooling and enriching joint data sets in a secure and trusted way. Today at Google Cloud Next, we are announcing Confidential Space, the next solution in our groundbreaking Confidential Computing portfolio. Organizations can perform [...]
