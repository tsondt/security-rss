Title: Can IAM help save on cyber insurance?
Date: 2022-10-11T08:10:08+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2022-10-11-can-iam-help-save-on-cyber-insurance

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/11/can_iam_help_save_on/){:target="_blank" rel="noopener"}

> Demonstrating a robust defense can help underwrite cyber risk for customers and providers, says One Identity Sponsored Feature Underwriters are continuing to feel the pinch as cyber insurance claims mount. That means customers are hurting too, with policies becoming more costly and insurers demanding more proof of cybersecurity. So how do organizations make better use of identity and access management to demonstrate their competency in protecting people's sensitive personal and financial data?... [...]
