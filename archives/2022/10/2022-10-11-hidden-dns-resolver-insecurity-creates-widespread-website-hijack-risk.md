Title: Hidden DNS resolver insecurity creates widespread website hijack risk
Date: 2022-10-11T10:51:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-11-hidden-dns-resolver-insecurity-creates-widespread-website-hijack-risk

[Source](https://portswigger.net/daily-swig/hidden-dns-resolver-insecurity-creates-widespread-website-hijack-risk){:target="_blank" rel="noopener"}

> WordPress installations exposed to spoofed password reset vis cache poisoning threat [...]
