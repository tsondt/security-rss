Title: JavaScript sandbox vm2 remediates remote code execution risk
Date: 2022-10-04T12:48:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-04-javascript-sandbox-vm2-remediates-remote-code-execution-risk

[Source](https://portswigger.net/daily-swig/javascript-sandbox-vm2-remediates-remote-code-execution-risk){:target="_blank" rel="noopener"}

> Affected firms alerted to bug whose potential impact is heightened by vm2’s use in production environments [...]
