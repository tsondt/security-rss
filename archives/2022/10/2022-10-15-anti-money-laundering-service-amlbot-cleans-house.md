Title: Anti-Money Laundering Service AMLBot Cleans House
Date: 2022-10-15T14:08:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Web Fraud 2.0;AMLBot;Antinalysis;Incognito Chain;Nick Bax;Polina Smoliar
Slug: 2022-10-15-anti-money-laundering-service-amlbot-cleans-house

[Source](https://krebsonsecurity.com/2022/10/anti-money-laundering-service-amlbot-cleans-house/){:target="_blank" rel="noopener"}

> AMLBot, a service that helps businesses avoid transacting with cryptocurrency wallets that have been sanctioned for cybercrime activity, said an investigation published by KrebsOnSecurity last year helped it shut down three dark web services that secretly resold its technology to help cybercrooks avoid detection by anti-money laundering systems. Antinalysis, as it existed in 2021. In August 2021, KrebsOnSecurity published “ New Anti Anti-Money Laundering Services for Crooks,” which examined Antinalysis, a service marketed on cybercrime forums that purported to offer a glimpse of how one’s payment activity might be flagged by law enforcement agencies and private companies that track and [...]
