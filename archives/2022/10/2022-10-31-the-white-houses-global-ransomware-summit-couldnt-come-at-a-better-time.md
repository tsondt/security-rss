Title: The White House's global ransomware summit couldn't come at a better time
Date: 2022-10-31T17:30:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-10-31-the-white-houses-global-ransomware-summit-couldnt-come-at-a-better-time

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/31/white_house_ransomware_summit/){:target="_blank" rel="noopener"}

> As cyber threats ramp up, businesses and organizations will be hoping for more than platitudes The White House has begun its second annual International Counter Ransomware Summit in which Biden administration officials will convene with representatives of three dozen nations, the EU, and private business to discuss the growing threat posed by data-destroying cyber attacks.... [...]
