Title: Loads of PostgreSQL systems are sitting on the internet without SSL encryption
Date: 2022-10-07T10:48:08+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-10-07-loads-of-postgresql-systems-are-sitting-on-the-internet-without-ssl-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/07/postgresql_no_ssl/){:target="_blank" rel="noopener"}

> They probably shouldn't be connected in the first place, says database expert Only a third of PostgreSQL databases connected to the internet use SSL for encrypted messaging, according to a cloud database provider.... [...]
