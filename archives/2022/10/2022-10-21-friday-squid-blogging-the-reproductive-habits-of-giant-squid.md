Title: Friday Squid Blogging: The Reproductive Habits of Giant Squid
Date: 2022-10-21T20:12:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-10-21-friday-squid-blogging-the-reproductive-habits-of-giant-squid

[Source](https://www.schneier.com/blog/archives/2022/10/friday-squid-blogging-the-reproductive-habits-of-giant-squid.html){:target="_blank" rel="noopener"}

> Interesting : A recent study on giant squid that have washed ashore along the Sea of Japan coast has raised the possibility that the animal has a different reproductive method than many other types of squid. Almost all squid and octopus species are polygamous, with multiple males passing sperm to a single female. Giant squids were thought to have a similar form reproduction. However, a group led by Professor Noritaka Hirohashi, 57, a professor of reproductive biology in the Faculty of Life and Environmental Sciences at Shimane University suspects differently. They examined 66 sperm “bags” attached to five different locations [...]
