Title: Toyota dev left key to customer info on public GitHub page for five years
Date: 2022-10-11T01:06:59+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-11-toyota-dev-left-key-to-customer-info-on-public-github-page-for-five-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/11/toyota_source_code_email_leak/){:target="_blank" rel="noopener"}

> 'Oh what a feeling' when your contractor leaks site source code Toyota has admitted it put 296,019 email addresses and customer management numbers of folks who signed up for its T-Connect assistance website at risk of online theft by bungling its security.... [...]
