Title: Japan to citizens: Get a digital ID or health insurance gets harder
Date: 2022-10-27T03:57:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-10-27-japan-to-citizens-get-a-digital-id-or-health-insurance-gets-harder

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/27/japan_digital_id_push/){:target="_blank" rel="noopener"}

> Risk of death is certainly one way to get the populace on board Japan's plan to phase out public health insurance cards in favor of linking the services to a digital ID card could compel those who oppose the digitization to sign up.... [...]
