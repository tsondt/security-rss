Title: Biden now wants to toughen up chemical sector's cybersecurity
Date: 2022-10-27T22:36:31+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-27-biden-now-wants-to-toughen-up-chemical-sectors-cybersecurity

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/27/biden_chemical_cybersecurity_sprint/){:target="_blank" rel="noopener"}

> Control panels facing the internet? Data stolen? You gotta keep an ion this stuff The White House is adding the chemical sector to a program launched last year to improve cybersecurity capabilities within America's critical infrastructure industries.... [...]
