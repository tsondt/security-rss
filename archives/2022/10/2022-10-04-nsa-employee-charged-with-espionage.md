Title: NSA Employee Charged with Espionage
Date: 2022-10-04T11:30:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;cyberespionage;espionage;FBI;NSA;undercover
Slug: 2022-10-04-nsa-employee-charged-with-espionage

[Source](https://www.schneier.com/blog/archives/2022/10/nsa-employee-charged-with-espionage.html){:target="_blank" rel="noopener"}

> An ex-NSA employee has been charged with trying to sell classified data to the Russians (but instead actually talking to an undercover FBI agent). It’s a weird story, and the FBI affidavit raises more questions than it answers. The employee only worked for the NSA for three weeks—which is weird in itself. I can’t figure out how he linked up with the undercover FBI agent. It’s not clear how much of this was the employee’s idea, and whether he was goaded by the FBI agent. Still, hooray for not leaking NSA secrets to the Russians. (And, almost ten years after [...]
