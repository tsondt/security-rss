Title: Cyber-snoops broke into US military contractor, stole data, hid for months
Date: 2022-10-05T19:27:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-05-cyber-snoops-broke-into-us-military-contractor-stole-data-hid-for-months

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/05/military_contractor_hack/){:target="_blank" rel="noopener"}

> Tell us it’s Russia without telling us it’s Russia Spies for months hid inside a US military contractor's enterprise network and stole sensitive data, according to a joint alert from the US government's Cybersecurity and Infrastructure Security Agency (CISA), the FBI, and NSA.... [...]
