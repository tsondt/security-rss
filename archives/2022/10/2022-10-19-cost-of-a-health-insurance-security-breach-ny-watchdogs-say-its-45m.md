Title: Cost of a health insurance security breach? NY watchdogs say it's $4.5m
Date: 2022-10-19T23:54:44+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-19-cost-of-a-health-insurance-security-breach-ny-watchdogs-say-its-45m

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/19/eyemed_data_breach_settlement/){:target="_blank" rel="noopener"}

> Hundreds of thousands of people's sensitive info poorly protected New York regulators continue turning the screws on organizations with slapdash computer security.... [...]
