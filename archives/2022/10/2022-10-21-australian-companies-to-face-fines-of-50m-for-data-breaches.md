Title: Australian companies to face fines of $50m for data breaches
Date: 2022-10-21T19:00:48+00:00
Author: Amy Remeikis and Paul Karp
Category: The Guardian
Tags: Australian politics;Anthony Albanese;Australia news;Cybercrime;Data and computer security
Slug: 2022-10-21-australian-companies-to-face-fines-of-50m-for-data-breaches

[Source](https://www.theguardian.com/australia-news/2022/oct/22/australian-companies-to-face-fines-of-50m-for-data-breaches){:target="_blank" rel="noopener"}

> In wake of Optus and Medibank leaks, serious or repeated breaches of customer information will attract heavy penalties under new legislation Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Companies that fail to adequately protect people’s data could face fines of $50m or more under new legislation to be introduced next week. After Optus and Medibank reported significant breaches of customer data, including sensitive health information, the Albanese government was now moving to increase penalties for serious or repeated breaches of customer data. Sign up [...]
