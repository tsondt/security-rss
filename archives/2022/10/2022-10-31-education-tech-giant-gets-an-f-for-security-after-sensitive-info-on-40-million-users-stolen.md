Title: Education tech giant gets an F for security after sensitive info on 40 million users stolen
Date: 2022-10-31T22:54:49+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-31-education-tech-giant-gets-an-f-for-security-after-sensitive-info-on-40-million-users-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/31/chegg_ftc_order/){:target="_blank" rel="noopener"}

> Chegg it out: Four blunders in four years Sloppy data security at education tech giant Chegg exposed students and workers' personal information not once but four times in various ways over four years, according to the FTC.... [...]
