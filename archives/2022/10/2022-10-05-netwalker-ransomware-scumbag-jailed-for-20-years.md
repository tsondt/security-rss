Title: NetWalker ransomware scumbag jailed for 20 years
Date: 2022-10-05T22:54:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-05-netwalker-ransomware-scumbag-jailed-for-20-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/05/netwalker_ransomware_crook_sentenced/){:target="_blank" rel="noopener"}

> And note to his crime pals – he said he would sing like a canary An ex-Canadian government worker who extorted tens of millions of dollars from organizations worldwide using the NetWalker ransomware has been sent down for 20 years.... [...]
