Title: South Korea cancels passport of Terraform Lab's Do Kwon
Date: 2022-10-06T16:02:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-10-06-south-korea-cancels-passport-of-terraform-labs-do-kwon

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/06/south_korea_to_invalidate_passport/){:target="_blank" rel="noopener"}

> Whereabouts of wanted cryptobro unknown, but he's reliably on Twitter South Korea issued a publicly available notice on Wednesday to wanted man and Terraform Labs founder Do Kwon, demanding he return his passport.... [...]
