Title: Mastercard moves to protect 'risky and frisky' crypto transactions
Date: 2022-10-10T06:57:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-10-mastercard-moves-to-protect-risky-and-frisky-crypto-transactions

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/10/mastercard_crypto_secure/){:target="_blank" rel="noopener"}

> Expands into a sector so toxic many won't touch it Supposedly ingenious schemes to revolutionize the finance industry with crypto are not hard to find – nor are their failures. And scarcely a day passes on which a cryptocurrency venture's infosec is not found wanting. That sad situation is causing financial institutions sufficient pain that Mastercard thinks the time is ripe for a service that helps lenders to understand if their customers' crypto purchases are dangerous.... [...]
