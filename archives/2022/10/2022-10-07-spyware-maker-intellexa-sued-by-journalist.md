Title: Spyware Maker Intellexa Sued by Journalist
Date: 2022-10-07T11:13:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;cyberweapons;Greece;Israel;privacy;spyware;surveillance
Slug: 2022-10-07-spyware-maker-intellexa-sued-by-journalist

[Source](https://www.schneier.com/blog/archives/2022/10/spyware-maker-intellexa-sued-by-journalist.html){:target="_blank" rel="noopener"}

> The Greek journalist Thanasis Koukakis was spied on by his own government, with a commercial spyware product called “Predator.” That product is sold by a company in North Macedonia called Cytrox, which is in turn owned by an Israeli company called Intellexa. Koukakis is suing Intellexa. The lawsuit filed by Koukakis takes aim at Intellexa and its executive, alleging a criminal breach of privacy and communication laws, reports Haaretz. The founder of Intellexa, a former Israeli intelligence commander named Taj Dilian, is listed as one of the defendants in the suit, as is another shareholder, Sara Hemo, and the firm [...]
