Title: Apache Commons Text RCE: Resemblance to Log4Shell but exposure risk is ‘much lower’
Date: 2022-10-19T10:35:25+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-19-apache-commons-text-rce-resemblance-to-log4shell-but-exposure-risk-is-much-lower

[Source](https://portswigger.net/daily-swig/apache-commons-text-rce-resemblance-to-log4shell-but-exposure-risk-is-much-lower){:target="_blank" rel="noopener"}

> Log4Shell-like bug is serious but less dangerous than notorious Log4j vulnerability [...]
