Title: Singtel confirms digital burglary at Dialog subsidiary
Date: 2022-10-10T10:47:12+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-10-10-singtel-confirms-digital-burglary-at-dialog-subsidiary

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/10/singtel_dialog_breach/){:target="_blank" rel="noopener"}

> Second of Singapore telco's Australian businesses to be prised open by criminals in weeks Singtel has confirmed that another Australian business it owns, consulting unit Dialog, has fallen victim to a cyber burglary just weeks after the mammoth data leak at telco Optus was revealed.... [...]
