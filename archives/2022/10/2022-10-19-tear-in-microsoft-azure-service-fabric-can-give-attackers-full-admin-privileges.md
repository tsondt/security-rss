Title: Tear in Microsoft Azure Service Fabric can give attackers full admin privileges
Date: 2022-10-19T13:05:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-19-tear-in-microsoft-azure-service-fabric-can-give-attackers-full-admin-privileges

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/19/azure_service_fabric_vulnerability/){:target="_blank" rel="noopener"}

> Orca Security disclosed the bug, and older versions remain vulnerable A proof-of-concept exploit has been published detailing a spoofing vulnerability in Microsoft Azure Service Fabric. The flaw allows attackers to gain full administrator permissions and then perform any manner of malicious activity.... [...]
