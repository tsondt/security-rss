Title: Museum Security
Date: 2022-10-19T11:16:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;interviews;physical security;security analysis
Slug: 2022-10-19-museum-security

[Source](https://www.schneier.com/blog/archives/2022/10/museum-security.html){:target="_blank" rel="noopener"}

> Interesting interview : Banks don’t take millions of dollars and put them in plastic bags and hang them on the wall so everybody can walk right up to them. But we do basically the same thing in museums and hang the assets right out on the wall. So it’s our job, then, to either use technology or develop technology that protects the art, to hire honest guards that are trainable and able to meet the challenge and alert and so forth. And we have to keep them alert because it’s the world’s most boring job. It might be great for [...]
