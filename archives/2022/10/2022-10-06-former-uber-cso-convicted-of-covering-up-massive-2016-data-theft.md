Title: Former Uber CSO convicted of covering up massive 2016 data theft
Date: 2022-10-06T00:33:21+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-10-06-former-uber-cso-convicted-of-covering-up-massive-2016-data-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/06/uber_cso_sullivan_guilty/){:target="_blank" rel="noopener"}

> Passing off a ransom payment as a bug bounty? That's obstruction of justice Joe Sullivan, Uber's former chief security officer, has been found guilty of illegally covering up the theft of Uber drivers and customers' personal information.... [...]
