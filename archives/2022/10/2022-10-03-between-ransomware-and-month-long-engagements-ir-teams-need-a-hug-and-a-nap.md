Title: Between ransomware and month-long engagements, IR teams need a hug – and a nap
Date: 2022-10-03T10:00:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-03-between-ransomware-and-month-long-engagements-ir-teams-need-a-hug-and-a-nap

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/03/ibm_incident_reponder_survey/){:target="_blank" rel="noopener"}

> Here's what 1,100 incident responders say about their jobs, just in time for NSCAM Remember the good old days of cyber-incident response, when the job involved digital forensics and lots of stolen credit cards, as opposed to power-grid-breaking malware and multi-million-dollar ransom demands?... [...]
