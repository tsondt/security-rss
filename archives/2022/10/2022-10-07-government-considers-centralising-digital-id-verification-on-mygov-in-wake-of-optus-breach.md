Title: Government considers centralising digital ID verification on myGov in wake of Optus breach
Date: 2022-10-07T03:32:34+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Data and computer security;Privacy;Australia news;Bill Shorten;Australian politics;Technology
Slug: 2022-10-07-government-considers-centralising-digital-id-verification-on-mygov-in-wake-of-optus-breach

[Source](https://www.theguardian.com/technology/2022/oct/07/government-considers-centralising-digital-id-verification-on-mygov-in-wake-of-optus-breach){:target="_blank" rel="noopener"}

> Experts warn using any single system could have its own cybersecurity weaknesses leaving data vulnerable to misuse Follow our Australia news live blog for the latest updates ​​Get our free news app, morning email briefing and daily news podcast The Australian government is considering using myGov or its myGovID system to centralise digital identity authentication in the wake of the Optus data breach, but critics warn any single system could have its own cybersecurity weaknesses. The former Telstra chief executive David Thodey was recruited to audit myGov when the Albanese government came into power, and his review would now examine [...]
