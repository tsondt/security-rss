Title: Mozilla Firefox fixes freezes caused by new Windows 11 feature
Date: 2022-10-31T15:19:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-10-31-mozilla-firefox-fixes-freezes-caused-by-new-windows-11-feature

[Source](https://www.bleepingcomputer.com/news/security/mozilla-firefox-fixes-freezes-caused-by-new-windows-11-feature/){:target="_blank" rel="noopener"}

> Mozilla has fixed a known issue causing the Firefox web browser to freeze when copying text on Windows 11 devices where the Suggested Actions clipboard feature is enabled. [...]
