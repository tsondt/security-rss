Title: Online age-verification system could create ‘honeypot’ of personal data and pornography-viewing habits, privacy groups warn
Date: 2022-10-30T14:00:31+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Technology;Australia news;Labor party;Internet safety;Data and computer security
Slug: 2022-10-30-online-age-verification-system-could-create-honeypot-of-personal-data-and-pornography-viewing-habits-privacy-groups-warn

[Source](https://www.theguardian.com/technology/2022/oct/31/online-age-verification-system-could-create-honeypot-of-personal-data-and-pornography-viewing-habits-privacy-groups-warn){:target="_blank" rel="noopener"}

> As the government develops online safety guidelines, digital rights groups says any approach requiring the use of ID is ‘invasive and risky’ Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast In the wake of the Optus and Medibank data breaches, digital rights groups are urging the federal government to rule out requiring identification documents as part of any online age-verification system, warning it could create a honeypot of people’s personal information and pornography-viewing habits. The eSafety commissioner, Julie Inman Grant, is developing an online safety [...]
