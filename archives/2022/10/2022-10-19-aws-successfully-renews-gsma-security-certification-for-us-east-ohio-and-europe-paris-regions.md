Title: AWS successfully renews GSMA security certification for US East (Ohio) and Europe (Paris) Regions
Date: 2022-10-19T18:40:09+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;audit;AWS security;Compliance;GSM;Security;Security Blog;Telecom
Slug: 2022-10-19-aws-successfully-renews-gsma-security-certification-for-us-east-ohio-and-europe-paris-regions

[Source](https://aws.amazon.com/blogs/security/aws-successfully-renews-gsma-security-certification-for-us-east-ohio-and-europe-paris-regions/){:target="_blank" rel="noopener"}

> Amazon Web Services is pleased to announce that our US East (Ohio) and Europe (Paris) Regions have been re-certified through October 2023 by the GSM Association (GSMA) under its Security Accreditation Scheme Subscription Management (SAS-SM) with scope Data Centre Operations and Management (DCOM). The US East (Ohio) and Europe (Paris) Regions first obtained GSMA certification [...]
