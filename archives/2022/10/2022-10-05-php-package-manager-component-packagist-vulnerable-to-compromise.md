Title: PHP package manager component Packagist vulnerable to compromise
Date: 2022-10-05T14:38:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-05-php-package-manager-component-packagist-vulnerable-to-compromise

[Source](https://portswigger.net/daily-swig/php-package-manager-component-packagist-vulnerable-to-compromise){:target="_blank" rel="noopener"}

> Argument injection bug posed RCE risk [...]
