Title: Learning from real life situations
Date: 2022-10-06T09:00:11+00:00
Author: Guy Matthews
Category: The Register
Tags: 
Slug: 2022-10-06-learning-from-real-life-situations

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/06/learning_from_real_life_situations/){:target="_blank" rel="noopener"}

> How about some cyber security education that’s actually delivered by people with genuine everyday experience? Sponsored Post There's nothing much to be said in favour of cybercrime. It ruins legitimate endeavours and wrecks livelihoods. It does, though, build a sense togetherness among the people whose job is to stop it.... [...]
