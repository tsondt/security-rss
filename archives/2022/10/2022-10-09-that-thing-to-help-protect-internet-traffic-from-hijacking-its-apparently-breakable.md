Title: That thing to help protect internet traffic from hijacking? It's apparently breakable
Date: 2022-10-09T19:31:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-10-09-that-thing-to-help-protect-internet-traffic-from-hijacking-its-apparently-breakable

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/09/internet_traffic_routing_defense/){:target="_blank" rel="noopener"}

> RPKI is supposed to verify network routes. Here's how it could be subverted – in theory An internet security mechanism called Resource Public Key Infrastructure (RPKI), intended to safeguard the routing of data traffic, can be broken.... [...]
