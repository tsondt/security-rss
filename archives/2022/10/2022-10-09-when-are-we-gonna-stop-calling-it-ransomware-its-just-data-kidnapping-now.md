Title: When are we gonna stop calling it ransomware? It's just data kidnapping now
Date: 2022-10-09T08:12:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-09-when-are-we-gonna-stop-calling-it-ransomware-its-just-data-kidnapping-now

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/09/extortion_ransomware_threats_category/){:target="_blank" rel="noopener"}

> It's not like the good old days with iffy cryptography and begging for keys Comment It's getting difficult these days to find a ransomware group that doesn't steal data and promise not to sell it if a ransom is paid off. What's more, these criminals are going down the extortion-only route, and not even bothering to scramble your files with encryption.... [...]
