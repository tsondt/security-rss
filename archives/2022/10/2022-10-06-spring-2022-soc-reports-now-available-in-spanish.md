Title: Spring 2022 SOC reports now available in Spanish
Date: 2022-10-06T18:44:38+00:00
Author: Rodrigo Fiuza
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS SOC 1;AWS SOC 2;AWS SOC 3;AWS SOC Reports;Security Blog
Slug: 2022-10-06-spring-2022-soc-reports-now-available-in-spanish

[Source](https://aws.amazon.com/blogs/security/spring-2022-soc-reports-now-available-in-spanish/){:target="_blank" rel="noopener"}

> English We continue to listen to our customers, regulators, and stakeholders to understand their needs regarding audit, assurance, certification, and attestation programs at Amazon Web Services (AWS). We are pleased to announce that Spring 2022 SOC 1, SOC 2, and SOC 3 reports are now available in Spanish. These translated reports will help drive greater [...]
