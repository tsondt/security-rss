Title: The Week in Ransomware - October 7th 2022 - A 20 year sentence
Date: 2022-10-07T18:14:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-10-07-the-week-in-ransomware-october-7th-2022-a-20-year-sentence

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-7th-2022-a-20-year-sentence/){:target="_blank" rel="noopener"}

> It was a very quiet week regarding ransomware news, with the most significant news being the sentencing of a Netwalker affiliate to 20-years in prison. [...]
