Title: Singapore hosts ICS/OT cybersecurity training extravaganza
Date: 2022-10-31T03:00:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-10-31-singapore-hosts-icsot-cybersecurity-training-extravaganza

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/31/singapore_hosts_icsot_cybersecurity_training/){:target="_blank" rel="noopener"}

> Two great SANS events for APAC cyber security professionals to boost their ICS knowledge and skills Sponsored Post Cybercriminals generally respect no limits or boundaries, but there is evidence to suggest that they are singling out industrial control systems (ICS) and operational technology (OT) systems such as supervisory control and data acquisition (SCADA) platforms in the Asia Pacific region which may represent easier targets for their attention.... [...]
