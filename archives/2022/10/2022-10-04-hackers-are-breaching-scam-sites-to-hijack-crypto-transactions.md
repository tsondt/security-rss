Title: Hackers are breaching scam sites to hijack crypto transactions
Date: 2022-10-04T14:20:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-10-04-hackers-are-breaching-scam-sites-to-hijack-crypto-transactions

[Source](https://www.bleepingcomputer.com/news/security/hackers-are-breaching-scam-sites-to-hijack-crypto-transactions/){:target="_blank" rel="noopener"}

> In a perfect example of there being no honor among thieves, a threat actor named 'Water Labbu' is hacking into cryptocurrency scam sites to inject malicious JavaScript that steals funds from the scammer's victims. [...]
