Title: Pro-Putin goons claim responsibility for blowing US airport websites offline
Date: 2022-10-10T18:12:28+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-10-pro-putin-goons-claim-responsibility-for-blowing-us-airport-websites-offline

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/10/ddos_us_airport_websites/){:target="_blank" rel="noopener"}

> How's that boot taste? Updated Russian miscreants claimed responsibility for knocking more than a dozen US airports' websites offline on Monday morning in what appeared to be a large-scale, distributed-denial-of-service (DDoS) attack.... [...]
