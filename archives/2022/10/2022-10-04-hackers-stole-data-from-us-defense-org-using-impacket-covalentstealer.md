Title: Hackers stole data from US defense org using Impacket, CovalentStealer
Date: 2022-10-04T19:08:56-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-10-04-hackers-stole-data-from-us-defense-org-using-impacket-covalentstealer

[Source](https://www.bleepingcomputer.com/news/security/hackers-stole-data-from-us-defense-org-using-impacket-covalentstealer/){:target="_blank" rel="noopener"}

> The U.S. Government today released an alert about state-backed hackers using a custom CovalentStealer malware and the Impacket framework to steal sensitive data from a U.S. organization in the Defense Industrial Base (DIB) sector. [...]
