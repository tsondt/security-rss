Title: IAM Access Analyzer makes it simpler to author and validate role trust policies
Date: 2022-10-03T22:43:21+00:00
Author: Mathangi Ramesh
Category: AWS Security
Tags: AWS IAM Access Analyzer;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Technical How-to;Access management;AWS CloudTrail;AWS IAM;AWS Lambda;least privilege;Policies;Security;Security Blog
Slug: 2022-10-03-iam-access-analyzer-makes-it-simpler-to-author-and-validate-role-trust-policies

[Source](https://aws.amazon.com/blogs/security/iam-access-analyzer-makes-it-simpler-to-author-and-validate-role-trust-policies/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) Access Analyzer provides many tools to help you set, verify, and refine permissions. One part of IAM Access Analyzer—policy validation—helps you author secure and functional policies that grant the intended permissions. Now, I’m excited to announce that AWS has updated the IAM console experience for role trust policies to [...]
