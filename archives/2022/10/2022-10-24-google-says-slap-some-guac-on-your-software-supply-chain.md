Title: Google says slap some GUAC on your software supply chain
Date: 2022-10-24T12:30:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-10-24-google-says-slap-some-guac-on-your-software-supply-chain

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/24/security_in_brief/){:target="_blank" rel="noopener"}

> Also: Iranian election hackers are back, the TSA gets regulatory on train cybersecurity, and more In brief Google has released a new open source software tool to help businesses better understand the risks to their software supply chains by aggregating security metadata into a queryable, standardized database.... [...]
