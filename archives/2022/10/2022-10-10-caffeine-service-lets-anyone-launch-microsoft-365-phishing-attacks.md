Title: Caffeine service lets anyone launch Microsoft 365 phishing attacks
Date: 2022-10-10T17:47:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-10-caffeine-service-lets-anyone-launch-microsoft-365-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/caffeine-service-lets-anyone-launch-microsoft-365-phishing-attacks/){:target="_blank" rel="noopener"}

> A phishing-as-a-service (PhaaS) platform named 'Caffeine' makes it easy for threat actors to launch attacks, featuring an open registration process allowing anyone to jump in and start their own phishing campaigns. [...]
