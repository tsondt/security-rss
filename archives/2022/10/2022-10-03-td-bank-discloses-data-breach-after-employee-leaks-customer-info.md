Title: TD Bank discloses data breach after employee leaks customer info
Date: 2022-10-03T18:42:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-03-td-bank-discloses-data-breach-after-employee-leaks-customer-info

[Source](https://www.bleepingcomputer.com/news/security/td-bank-discloses-data-breach-after-employee-leaks-customer-info/){:target="_blank" rel="noopener"}

> TD Bank has disclosed a data breach affecting an undisclosed number of customers whose personal information was stolen by a former employee and used to conduct financial fraud. [...]
