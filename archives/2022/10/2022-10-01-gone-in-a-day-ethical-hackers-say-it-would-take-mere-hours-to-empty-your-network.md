Title: Gone in a day: Ethical hackers say it would take mere hours to empty your network
Date: 2022-10-01T09:57:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-01-gone-in-a-day-ethical-hackers-say-it-would-take-mere-hours-to-empty-your-network

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/01/ethical_hackers_sans_survey/){:target="_blank" rel="noopener"}

> 300 red teamers walk into a bar... Once they've broken into an IT environment, most intruders need less than five hours to collect and steal sensitive data, according to a SANS Institute survey of more than 300 ethical hackers.... [...]
