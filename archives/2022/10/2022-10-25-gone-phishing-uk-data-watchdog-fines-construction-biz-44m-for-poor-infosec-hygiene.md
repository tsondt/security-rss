Title: Gone phishing: UK data watchdog fines construction biz £4.4m for poor infosec hygiene
Date: 2022-10-25T08:30:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-10-25-gone-phishing-uk-data-watchdog-fines-construction-biz-44m-for-poor-infosec-hygiene

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/25/gone_phishing_uk_data_watchdog/){:target="_blank" rel="noopener"}

> Staff member bit on lure, ultimately exposed up to 113,000 colleagues' personal information Britain's data watchdog has slapped construction business Interserve Group with a potential £4.4 million ($4.98M) fine after a successful phishing attack by criminals exposed the personal data of up to 113,000 employees.... [...]
