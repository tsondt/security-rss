Title: Uncle Sam says Chinese agents tried to interfere with Huawei criminal case in US
Date: 2022-10-24T23:40:32+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-10-24-uncle-sam-says-chinese-agents-tried-to-interfere-with-huawei-criminal-case-in-us

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/24/justice_department_china_charges/){:target="_blank" rel="noopener"}

> Beijing also sought to recruit academics and officials in America, and more claimed American prosecutors on Monday accused 13 people of committing espionage-linked crimes in the US on behalf of the Chinese government.... [...]
