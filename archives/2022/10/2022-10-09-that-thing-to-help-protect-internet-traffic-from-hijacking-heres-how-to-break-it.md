Title: That thing to help protect internet traffic from hijacking? Here's how to break it
Date: 2022-10-09T19:31:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-10-09-that-thing-to-help-protect-internet-traffic-from-hijacking-heres-how-to-break-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/09/internet_traffic_routing_defense/){:target="_blank" rel="noopener"}

> RPKI is supposed to verify network routes. Cyber-researchers suggest ways to potentially defeat it An internet security mechanism called Resource Public Key Infrastructure (RPKI), intended to safeguard the routing of data traffic, can be broken.... [...]
