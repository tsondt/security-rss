Title: Confidentiality in the cloud: the delicate bargain of trust
Date: 2022-10-20T06:32:14+00:00
Author: John Malachy
Category: The Register
Tags: 
Slug: 2022-10-20-confidentiality-in-the-cloud-the-delicate-bargain-of-trust

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/20/confidentiality_in_the_cloud_the/){:target="_blank" rel="noopener"}

> How hardware-assisted data security can boost the integrity of sensitive data sets stored in cloud environments Sponsored Feature The concept behind Confidential Computing isn't new – organisations have been using hardware-assisted technology to encrypt and decrypt data for a while now. But fresh impetus from the Confidential Computing Consortium, new technology, and greater reliance on off prem public clouds to host and process sensitive information is prompting a more widespread re-evaluation of its benefits.... [...]
