Title: Steganography alert: Backdoor spyware stashed in Microsoft logo
Date: 2022-10-02T12:56:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-02-steganography-alert-backdoor-spyware-stashed-in-microsoft-logo

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/02/witchetty_windows_logo_spyware/){:target="_blank" rel="noopener"}

> Now that's sticker shock Internet snoops have been caught concealing spyware in an old Windows logo in an attack on governments in the Middle East.... [...]
