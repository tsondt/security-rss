Title: Optus data breach prompts pincer movement of twin regulatory probes
Date: 2022-10-11T04:57:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-11-optus-data-breach-prompts-pincer-movement-of-twin-regulatory-probes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/11/optus_acma_oaic_dual_probes/){:target="_blank" rel="noopener"}

> Data retention requirements to be considered alongside infosec failings Australian carrier Optus's recent data breach will be investigated by two regulators, the double trouble likely an indicator of the nation's displeasure at the incident – which saw almost ten million locals' personal data exposed online.... [...]
