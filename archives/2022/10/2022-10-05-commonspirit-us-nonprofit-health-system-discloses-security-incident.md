Title: CommonSpirit US nonprofit health system discloses security incident
Date: 2022-10-05T11:37:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-10-05-commonspirit-us-nonprofit-health-system-discloses-security-incident

[Source](https://www.bleepingcomputer.com/news/security/commonspirit-us-nonprofit-health-system-discloses-security-incident/){:target="_blank" rel="noopener"}

> CommonSpirit Health, one of the largest nonprofit health systems in the United States, says it took down some of its IT systems because of a security incident that has impacted multiple facilities. [...]
