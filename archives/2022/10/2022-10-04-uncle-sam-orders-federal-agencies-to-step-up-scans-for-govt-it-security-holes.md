Title: Uncle Sam orders federal agencies to step up scans for govt IT security holes
Date: 2022-10-04T22:26:46+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-04-uncle-sam-orders-federal-agencies-to-step-up-scans-for-govt-it-security-holes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/04/cisa_software_vulnerability_directive/){:target="_blank" rel="noopener"}

> Good time to be selling automation tools The US government's Cybersecurity and Infrastructure Security Agency (CISA) has ordered federal civilian agencies to scan for and report software vulnerabilities in their IT systems more frequently under a directive issued this week.... [...]
