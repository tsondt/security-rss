Title: How do you protect your online systems? Cultivate an insider threat
Date: 2022-10-10T08:30:11+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2022-10-10-how-do-you-protect-your-online-systems-cultivate-an-insider-threat

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/10/opinion_infosec/){:target="_blank" rel="noopener"}

> Challenge your people to try to break into your systems, and see how interesting life gets for your colleagues Opinion People are the biggest problem in corporate infosec. Make them the biggest asset.... [...]
