Title: Giveaways for every security professional
Date: 2022-10-04T03:00:14+00:00
Author: Guy Matthews
Category: The Register
Tags: 
Slug: 2022-10-04-giveaways-for-every-security-professional

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/04/giveaways_for_every_security_professional/){:target="_blank" rel="noopener"}

> Don’t chuck money away before you’ve checked SANS free educational content Sponsored Post Fighting cybercrime is an expensive business. If your cyber defences fail, then the cost can be measured in many ways. There's the price of repairing damaged infrastructure, retrieving lost data, and paying regulatory penalties. And the cost in reputational terms with customers simply has no metric.... [...]
