Title: If someone tries ransacking your Windows network, it's a bit easier now to grok in Microsoft 365 Defender
Date: 2022-10-26T04:27:05+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-26-if-someone-tries-ransacking-your-windows-network-its-a-bit-easier-now-to-grok-in-microsoft-365-defender

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/26/microsoft_365_identity_protection/){:target="_blank" rel="noopener"}

> Blinking, beeping, and flashing lights, blinking and beeping and flashing... Microsoft is bringing Azure Active Directory Identity Protection alerts to Microsoft 365 Defender to seemingly help IT folks thwart criminals infiltrating corporate networks via compromised users.... [...]
