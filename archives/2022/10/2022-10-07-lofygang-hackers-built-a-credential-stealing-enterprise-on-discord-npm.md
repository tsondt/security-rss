Title: LofyGang hackers built a credential-stealing enterprise on Discord, NPM
Date: 2022-10-07T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-07-lofygang-hackers-built-a-credential-stealing-enterprise-on-discord-npm

[Source](https://www.bleepingcomputer.com/news/security/lofygang-hackers-built-a-credential-stealing-enterprise-on-discord-npm/){:target="_blank" rel="noopener"}

> A threat group using the name 'LofyGang', operating since 2020, is considered responsible for creating and distributing over 200 malicious packages on multiple code hosting platforms, including GitHub and NPM. [...]
