Title: 2K Games warns users their stolen data is now up for sale online
Date: 2022-10-07T12:47:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-10-07-2k-games-warns-users-their-stolen-data-is-now-up-for-sale-online

[Source](https://www.bleepingcomputer.com/news/security/2k-games-warns-users-their-stolen-data-is-now-up-for-sale-online/){:target="_blank" rel="noopener"}

> Video game publisher 2K emailed users on Thursday to warn that some of their personal info was stolen and put up for sale online following a September 19 security breach. [...]
