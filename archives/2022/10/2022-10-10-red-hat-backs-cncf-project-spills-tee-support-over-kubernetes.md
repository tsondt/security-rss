Title: Red Hat backs CNCF project, spills TEE support over Kubernetes
Date: 2022-10-10T16:00:06+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-10-10-red-hat-backs-cncf-project-spills-tee-support-over-kubernetes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/10/confidential_containers_encrypted_k8s/){:target="_blank" rel="noopener"}

> Keeping the contents of your clusters secure from whoever's hosting them Red Hat is backing a Cloud Native Computing Foundation (CNCF) project that aims to improve the security of containers in Kubernetes clusters by running them inside hardware-enforced enclaves.... [...]
