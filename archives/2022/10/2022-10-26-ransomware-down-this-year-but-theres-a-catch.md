Title: Ransomware down this year – but there's a catch
Date: 2022-10-26T05:28:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-26-ransomware-down-this-year-but-theres-a-catch

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/26/sonicwall_ransomware_raas/){:target="_blank" rel="noopener"}

> 2021 was such a banner year for extortionists, 2022 is gonna look rosy in comparison The number of ransomware attacks worldwide dropped 31 percent year-over-year during the first nine of months 2022, at least as far as SonicWall has observed. But don't get too excited.... [...]
