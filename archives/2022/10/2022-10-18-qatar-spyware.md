Title: Qatar Spyware
Date: 2022-10-18T11:57:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;Qatar;sports;spyware
Slug: 2022-10-18-qatar-spyware

[Source](https://www.schneier.com/blog/archives/2022/10/qatar-spyware.html){:target="_blank" rel="noopener"}

> Everyone visiting Qatar for the World Cup needs to install spyware on their phone. Everyone travelling to Qatar during the football World Cup will be asked to download two apps called Ehteraz and Hayya. Briefly, Ehteraz is an covid-19 tracking app, while Hayya is an official World Cup app used to keep track of match tickets and to access the free Metro in Qatar. In particular, the covid-19 app Ehteraz asks for access to several rights on your mobile., like access to read, delete or change all content on the phone, as well as access to connect to WiFi and [...]
