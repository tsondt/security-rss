Title: BlackByte ransomware abuses legit driver to disable security products
Date: 2022-10-05T15:44:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-05-blackbyte-ransomware-abuses-legit-driver-to-disable-security-products

[Source](https://www.bleepingcomputer.com/news/security/blackbyte-ransomware-abuses-legit-driver-to-disable-security-products/){:target="_blank" rel="noopener"}

> The BlackByte ransomware gang is using a new technique that researchers are calling "Bring Your Own Driver," which enables bypassing protections by disabling more than 1,000 drivers used by various security solutions. [...]
