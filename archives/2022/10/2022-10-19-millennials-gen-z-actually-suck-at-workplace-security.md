Title: Millennials, Gen Z actually suck at workplace security
Date: 2022-10-19T16:45:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-10-19-millennials-gen-z-actually-suck-at-workplace-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/19/millennials_gen_z/){:target="_blank" rel="noopener"}

> OK, boomer – how do I turn off cookies? It's just as you suspected: your Gen Z and millennial coworkers just aren't taking cybersecurity at work seriously enough.... [...]
