Title: The Week in Ransomware - October 28th 2022 - Healthcare leaks
Date: 2022-10-28T16:08:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-10-28-the-week-in-ransomware-october-28th-2022-healthcare-leaks

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-28th-2022-healthcare-leaks/){:target="_blank" rel="noopener"}

> This week, we learned of healthcare data leaks out of Australia, information about existing attacks, and reports on how ransomware gangs operate and partner with malware developers for initial access. [...]
