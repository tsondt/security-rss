Title: Hardening data security in the cloud
Date: 2022-10-07T08:29:13+00:00
Author: John Malachy
Category: The Register
Tags: 
Slug: 2022-10-07-hardening-data-security-in-the-cloud

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/07/hardening_data_security_in_the/){:target="_blank" rel="noopener"}

> How Intel’s SGX hardware helps safeguard applications in multi-tenant environments Sponsored Feature As enterprises continue to migrate applications into the cloud, security concerns about the data those workloads store and process are inevitable. But how can IT departments be certain that sensitive information covered by stringent data protection laws hosted in public, private and hybrid cloud environments spanning multiple servers and locations is adequately protected from both internal and external threats?... [...]
