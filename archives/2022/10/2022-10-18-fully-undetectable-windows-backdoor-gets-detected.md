Title: 'Fully undetectable' Windows backdoor gets detected
Date: 2022-10-18T20:14:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-10-18-fully-undetectable-windows-backdoor-gets-detected

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/18/fully_undetectable_windows_powershell_backdoor/){:target="_blank" rel="noopener"}

> SafeBreach supposedly spots somewhat stealthy subversive software SafeBreach Labs says it has detected a novel fully undetectable (FUD) PowerShell backdoor, which calls into question the accuracy of threat naming.... [...]
