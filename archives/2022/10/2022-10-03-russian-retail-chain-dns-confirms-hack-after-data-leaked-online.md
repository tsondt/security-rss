Title: Russian retail chain 'DNS' confirms hack after data leaked online
Date: 2022-10-03T14:35:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-03-russian-retail-chain-dns-confirms-hack-after-data-leaked-online

[Source](https://www.bleepingcomputer.com/news/security/russian-retail-chain-dns-confirms-hack-after-data-leaked-online/){:target="_blank" rel="noopener"}

> Russian retail chain 'DNS' (Digital Network System) disclosed yesterday that they suffered a data breach that allegedly exposed the personal information of 16 million customers and employees. [...]
