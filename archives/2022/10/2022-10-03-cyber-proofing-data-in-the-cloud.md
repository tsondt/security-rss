Title: Cyber-proofing data in the cloud
Date: 2022-10-03T13:24:06+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-10-03-cyber-proofing-data-in-the-cloud

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/03/cyberproofing_data_in_the_cloud/){:target="_blank" rel="noopener"}

> How to reduce the risk and impact of ransomware attacks on AWS data and applications Webinar Ransomware has a longer history than you might imagine. The very first recognized attack was at the World Health Organization in 1989 when the AIDS Trojan was distributed to 20,000 attendees via floppy disc.... [...]
