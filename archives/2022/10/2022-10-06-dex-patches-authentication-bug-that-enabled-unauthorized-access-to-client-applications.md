Title: Dex patches authentication bug that enabled unauthorized access to client applications
Date: 2022-10-06T16:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-06-dex-patches-authentication-bug-that-enabled-unauthorized-access-to-client-applications

[Source](https://portswigger.net/daily-swig/dex-patches-authentication-bug-that-enabled-unauthorized-access-to-client-applications){:target="_blank" rel="noopener"}

> With 35.6 million downloads the OAuth 2.0 protocol provider has serious downstream attack surface [...]
