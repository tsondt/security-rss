Title: As Russia wages disinfo war, Ukraine's cyber chief calls for global anti-fake news fight
Date: 2022-10-22T22:53:26+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-22-as-russia-wages-disinfo-war-ukraines-cyber-chief-calls-for-global-anti-fake-news-fight

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/22/ukraine_cybersecurity_chief_mwise/){:target="_blank" rel="noopener"}

> 'Completely new approaches should be developed to prevent the influence of this propaganda' As a hybrid offline and online war wages on in Ukraine, Viktor Zhora, who leads the country's cybersecurity agency, has had a front-row seat of it all.... [...]
