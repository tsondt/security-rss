Title: GitLab patches RCE bug in GitHub import function
Date: 2022-10-13T14:27:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-13-gitlab-patches-rce-bug-in-github-import-function

[Source](https://portswigger.net/daily-swig/gitlab-patches-rce-bug-in-github-import-function){:target="_blank" rel="noopener"}

> Data importation mechanism failed to sanitize imports [...]
