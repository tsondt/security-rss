Title: Lloyd's of London reboots after dodgy network activity detected
Date: 2022-10-07T00:13:23+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-07-lloyds-of-london-reboots-after-dodgy-network-activity-detected

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/07/lloyds_london_security_incident/){:target="_blank" rel="noopener"}

> Is it Putin? Is it the Norks? Is it a bored teenager? Roll the dice Lloyd's of London has reset its IT systems and is probing a possible cyberattack against it after detecting worrisome network behavior this week.... [...]
