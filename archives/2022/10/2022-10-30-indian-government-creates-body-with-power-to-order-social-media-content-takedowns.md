Title: Indian government creates body with power to order social media content takedowns
Date: 2022-10-30T23:32:24+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-10-30-indian-government-creates-body-with-power-to-order-social-media-content-takedowns

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/30/asia_in_brief/){:target="_blank" rel="noopener"}

> PLUS: China’s digital currency surges; Infosys tax portals wobble again; Singapore crypto protections; and more Asia In Brief India's government has given itself the power to compel social networks to take down content.... [...]
