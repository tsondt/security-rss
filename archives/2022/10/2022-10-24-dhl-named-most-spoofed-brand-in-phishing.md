Title: DHL named most-spoofed brand in phishing
Date: 2022-10-24T18:42:43+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-24-dhl-named-most-spoofed-brand-in-phishing

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/24/dhl_phishing_scams/){:target="_blank" rel="noopener"}

> With Microsoft and LinkedIn close on shipping giant's heels DHL is the most spoofed brand when it comes to phishing emails, according to Check Point.... [...]
