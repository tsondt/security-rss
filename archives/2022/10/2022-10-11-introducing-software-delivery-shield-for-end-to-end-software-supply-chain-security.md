Title: Introducing Software Delivery Shield for end-to-end software supply chain security
Date: 2022-10-11T12:07:00+00:00
Author: Michael McGrath
Category: GCP Security
Tags: Application Development;Next;Identity & Security;DevOps & SRE
Slug: 2022-10-11-introducing-software-delivery-shield-for-end-to-end-software-supply-chain-security

[Source](https://cloud.google.com/blog/products/devops-sre/introducing-software-delivery-shield-from-google-cloud/){:target="_blank" rel="noopener"}

> Organizations and their software delivery pipelines are continually exposed to growing cyberattack vectors. Coupled with the massive adoption of open source software, which now helps power nearly all of our public infrastructure and is highly prevalent in most proprietary software, businesses around the world are more vulnerable than ever. Today’s organizations need to be more vigilant in protecting their software development infrastructure and processes. Development and IT teams are all asking for a better way to secure the software supply chain across the code, people, systems, and processes that contribute to development and delivery of the software. For many years, [...]
