Title: New open-source tool scans public AWS S3 buckets for secrets
Date: 2022-10-29T11:12:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-29-new-open-source-tool-scans-public-aws-s3-buckets-for-secrets

[Source](https://www.bleepingcomputer.com/news/security/new-open-source-tool-scans-public-aws-s3-buckets-for-secrets/){:target="_blank" rel="noopener"}

> A new open-source 'S3crets Scanner' scanner allows researchers and red-teamers to search for 'secrets' mistakenly stored in publicly exposed or company's Amazon AWS S3 storage buckets. [...]
