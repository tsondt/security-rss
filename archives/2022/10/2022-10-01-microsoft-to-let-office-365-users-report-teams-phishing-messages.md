Title: Microsoft to let Office 365 users report Teams phishing messages
Date: 2022-10-01T11:06:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-10-01-microsoft-to-let-office-365-users-report-teams-phishing-messages

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-to-let-office-365-users-report-teams-phishing-messages/){:target="_blank" rel="noopener"}

> Microsoft is working on updating Microsoft Defender for Office 365 to allow Microsoft Teams users to alert their organization's security team of any dodgy messages they receive. [...]
