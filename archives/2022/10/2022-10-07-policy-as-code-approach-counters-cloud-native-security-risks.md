Title: Policy-as-code approach counters ‘cloud native’ security risks
Date: 2022-10-07T15:24:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-07-policy-as-code-approach-counters-cloud-native-security-risks

[Source](https://portswigger.net/daily-swig/policy-as-code-approach-counters-cloud-native-security-risks){:target="_blank" rel="noopener"}

> Research suggests that automation can cut down on cloud control plane compromises [...]
