Title: Outsourcer Interserve fined £4.4m for failing to stop cyber-attack
Date: 2022-10-23T23:01:50+00:00
Author: Mark Sweney
Category: The Guardian
Tags: Interserve;Information commissioner;Data and computer security;Cybercrime;Data protection;Business;Technology;Internet;UK news
Slug: 2022-10-23-outsourcer-interserve-fined-44m-for-failing-to-stop-cyber-attack

[Source](https://www.theguardian.com/business/2022/oct/24/outsourcer-interserve-fined-4-point-4m-cyber-attack-failings-data-breach-personal-information){:target="_blank" rel="noopener"}

> Watchdog says phishing email enabled hackers to steal personal information of 113,000 employees Britain’s data watchdog has fined the construction group Interserve £4.4m after a cyber-attack that enabled hackers to steal the personal and financial information of up to 113,000 employees. The attack occurred when Interserve ran an outsourcing business and was designated a “strategic supplier to the government with clients including the Ministry of Defence”. Bank account details, national insurance numbers, ethnic origin, sexual orientation and religion were among the personal information compromised. Continue reading... [...]
