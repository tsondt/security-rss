Title: Modified version of Tor Browser spies on Chinese users
Date: 2022-10-05T11:32:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-10-05-modified-version-of-tor-browser-spies-on-chinese-users

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/05/tor_browser_china_spy_kasperksy_research/){:target="_blank" rel="noopener"}

> Patiently gathers data that can be used to identify the victims, says Kaspersky Cybersecurity biz Kaspersky has spotted a modified version of the Tor Browser it says collects sensitive data on Chinese users.... [...]
