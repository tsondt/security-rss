Title: FBI warns of disinformation threats before 2022 midterm elections
Date: 2022-10-06T17:35:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-10-06-fbi-warns-of-disinformation-threats-before-2022-midterm-elections

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-disinformation-threats-before-2022-midterm-elections/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned today of foreign influence operations that might spread disinformation to affect the results of this year's midterm elections. [...]
