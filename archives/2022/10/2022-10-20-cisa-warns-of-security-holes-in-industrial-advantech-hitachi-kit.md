Title: CISA warns of security holes in industrial Advantech, Hitachi kit
Date: 2022-10-20T00:35:59+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-20-cisa-warns-of-security-holes-in-industrial-advantech-hitachi-kit

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/20/cisa_flaws_advantech_hitachi/){:target="_blank" rel="noopener"}

> When we concede that everything has bugs, we wish it wasn't quite everything This week, the US government's Cybersecurity and Infrastructure Security Agency (CISA) expanded its ever-growing list of vulnerability in industrial control systems (ICS) and critical infrastructure technology.... [...]
