Title: Critical flaw in open source WebPageTest remains unpatched
Date: 2022-10-07T11:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-07-critical-flaw-in-open-source-webpagetest-remains-unpatched

[Source](https://portswigger.net/daily-swig/critical-flaw-in-open-source-webpagetest-remains-unpatched){:target="_blank" rel="noopener"}

> Public disclosure, a talk, and a blog post later, the RCE exploit remains unresolved [...]
