Title: Japanese sushi chain boss resigns amid accusation of improper data access
Date: 2022-10-04T05:56:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-10-04-japanese-sushi-chain-boss-resigns-amid-accusation-of-improper-data-access

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/04/hama_sushi_data_theft_allegations/){:target="_blank" rel="noopener"}

> Data theft stinks, says victim. Alleged perp claims he's getting a raw deal The president of casual Japanese chain restaurant Kappa Sushi resigned yesterday in the wake of a data-theft scandal that has rocked the world of sushi trains.... [...]
