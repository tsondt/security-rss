Title: NSA shares supply chain security tips for software suppliers
Date: 2022-10-31T12:54:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-10-31-nsa-shares-supply-chain-security-tips-for-software-suppliers

[Source](https://www.bleepingcomputer.com/news/security/nsa-shares-supply-chain-security-tips-for-software-suppliers/){:target="_blank" rel="noopener"}

> NSA, CISA, and the Office of the Director of National Intelligence (ODNI) have shared a new set of suggested practices that software suppliers (vendors) can follow to secure the supply chain. [...]
