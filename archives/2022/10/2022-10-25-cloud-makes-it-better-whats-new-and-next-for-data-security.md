Title: Cloud makes it better: What's new and next for data security
Date: 2022-10-25T16:00:00+00:00
Author: Jodie Garner
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-10-25-cloud-makes-it-better-whats-new-and-next-for-data-security

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-brings-the-insight-and-partnerships-that-transform-security/){:target="_blank" rel="noopener"}

> Today’s digital economy offers a wealth of opportunities, but those opportunities come with growing risks. It has become increasingly important to manage the risks posed by the intersection of digital resilience and today's risk landscape. Organizations around the world should be asking themselves: If a risk becomes a material threat, how can we help our employees continue to get work done efficiently and securely despite unexpected disruption, no matter where they are? This new era of “anywhere work” is not only a technology issue, but one that encompasses leadership support and cultural shifts. In a recent webinar, Heidi Shey, principal [...]
