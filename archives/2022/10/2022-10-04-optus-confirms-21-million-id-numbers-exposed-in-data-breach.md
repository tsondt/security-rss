Title: Optus confirms 2.1 million ID numbers exposed in data breach
Date: 2022-10-04T12:43:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-10-04-optus-confirms-21-million-id-numbers-exposed-in-data-breach

[Source](https://www.bleepingcomputer.com/news/security/optus-confirms-21-million-id-numbers-exposed-in-data-breach/){:target="_blank" rel="noopener"}

> Optus confirmed yesterday that 2.1 million customers had government identification numbers compromised during a cyberattack last month. [...]
