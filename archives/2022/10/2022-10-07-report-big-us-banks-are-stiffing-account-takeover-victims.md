Title: Report: Big U.S. Banks Are Stiffing Account Takeover Victims
Date: 2022-10-07T18:46:12+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Aite-Novarica;Bank of America;Biocatch;capital one;JPMorgan Chase;PNC Bank;Sen. Elizabeth Warren;Seth Rudin;Trace Fooshee;Truist;U.S. Bank;Wells Fargo;Zelle
Slug: 2022-10-07-report-big-us-banks-are-stiffing-account-takeover-victims

[Source](https://krebsonsecurity.com/2022/10/report-big-u-s-banks-are-stiffing-account-takeover-victims/){:target="_blank" rel="noopener"}

> When U.S. consumers have their online bank accounts hijacked and plundered by hackers, U.S. financial institutions are legally obligated to reverse any unauthorized transactions as long as the victim reports the fraud in a timely manner. But new data released this week suggests that for some of the nation’s largest banks, reimbursing account takeover victims has become more the exception than the rule. The findings came in a report released by Sen. Elizabeth Warren (D-Mass.), who in April 2022 opened an investigation into fraud tied to Zelle, the “peer-to-peer” digital payment service used by many financial institutions that allows customers [...]
