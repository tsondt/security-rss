Title: Biden's Privacy Shield 2.0 order may not satisfy Europe
Date: 2022-10-08T10:56:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-10-08-bidens-privacy-shield-20-order-may-not-satisfy-europe

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/08/in_brief_security/){:target="_blank" rel="noopener"}

> Also, Albania almost called in NATO over cyber attacks, and Facebook warns of account-stealing mobile apps In brief An executive order signed by President Biden on Friday to setting out fresh rules on how the US and Europe share people's private personal info may still fall short of the EU's wishes, says the privacy advocate who defeated the previous regulations in court.... [...]
