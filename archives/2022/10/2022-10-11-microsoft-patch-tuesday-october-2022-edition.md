Title: Microsoft Patch Tuesday, October 2022 Edition
Date: 2022-10-11T21:06:23+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Time to Patch;Caitlin Condon;CVE-2022-37968;CVE-2022-41033;Immersive Labs;Kevin Breen;Microsoft Patch Tuesday October 2022;Rapid7;Satnam Narang;Tenable
Slug: 2022-10-11-microsoft-patch-tuesday-october-2022-edition

[Source](https://krebsonsecurity.com/2022/10/microsoft-patch-tuesday-october-2022-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to fix at least 85 security holes in its Windows operating systems and related software, including a new zero-day vulnerability in all supported versions of Windows that is being actively exploited. However, noticeably absent from this month’s Patch Tuesday are any updates to address a pair of zero-day flaws being exploited this past month in Microsoft Exchange Server. The new zero-day flaw– CVE-2022-41033 — is an “elevation of privilege” bug in the Windows COM+ event service, which provides system notifications when users logon or logoff. Microsoft says the flaw is being actively exploited, and that it [...]
