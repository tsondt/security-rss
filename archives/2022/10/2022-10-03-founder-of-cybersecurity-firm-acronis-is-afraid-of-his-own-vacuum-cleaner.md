Title: Founder of cybersecurity firm Acronis is afraid of his own vacuum cleaner
Date: 2022-10-03T10:46:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-10-03-founder-of-cybersecurity-firm-acronis-is-afraid-of-his-own-vacuum-cleaner

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/03/acronis_founder_smart_devices/){:target="_blank" rel="noopener"}

> It is the exponential changes in the course of human history that worry Serg Bell Acronis founder Serg Bell is afraid of his own vacuum cleaner, he told The Register in Singapore last week.... [...]
