Title: New AWS whitepaper: Using AWS in the Context of Canada’s Controlled Goods Program (CGP)
Date: 2022-10-17T22:29:10+00:00
Author: Michael Davie
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;AWS security;Canada;Compliance;Controlled Goods Program;Security;Security Blog
Slug: 2022-10-17-new-aws-whitepaper-using-aws-in-the-context-of-canadas-controlled-goods-program-cgp

[Source](https://aws.amazon.com/blogs/security/new-aws-whitepaper-using-aws-in-the-context-of-canadas-controlled-goods-program-cgp/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has released a new whitepaper to help Canadian defense and security customers accelerate their use of the AWS Cloud. The new guide, Using AWS in the Context of Canada’s Controlled Goods Program (CGP), continues our efforts to help AWS customers navigate the regulatory expectations of the Government of Canada’s Controlled Goods [...]
