Title: Friday Squid Blogging: Chinese Squid Fishing
Date: 2022-10-28T20:57:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;squid
Slug: 2022-10-28-friday-squid-blogging-chinese-squid-fishing

[Source](https://www.schneier.com/blog/archives/2022/10/friday-squid-blogging-chinese-squid-fishing.html){:target="_blank" rel="noopener"}

> China claims that it is “engaging in responsible squid fishing”: Chen Xinjun, dean of the College of Marine Sciences at Shanghai Ocean University, made the remarks in response to recent accusations by foreign reporters and actor Leonardo DiCaprio that China is depleting its own fish stock and that Chinese boats have sailed to other waters to continue deep-sea fishing, particularly near Ecuador, affecting local fish stocks in the South American nation. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
