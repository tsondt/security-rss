Title: Binance robbed of $600 million in crypto-tokens
Date: 2022-10-07T19:40:00+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-10-07-binance-robbed-of-600-million-in-crypto-tokens

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/07/binance_hack_566m/){:target="_blank" rel="noopener"}

> How's your day going? Cryptocurrency exchange Binance temporarily halted its blockchain network on Thursday in response to a cyberattack that led to the theft of two million BNB tokens, notionally exchangeable for $566 million in fiat currency.... [...]
