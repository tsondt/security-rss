Title: Best kept security secrets: How Cloud EKM can help resolve the cloud trust paradox
Date: 2022-10-17T16:00:00+00:00
Author: Seth Rosenblatt
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-10-17-best-kept-security-secrets-how-cloud-ekm-can-help-resolve-the-cloud-trust-paradox

[Source](https://cloud.google.com/blog/products/identity-security/how-cloud-ekm-can-help-resolve-the-cloud-trust-paradox/){:target="_blank" rel="noopener"}

> Whether driven by government policy, industry regulation, or geo-political considerations, the evolution of cloud computing has led organizations to want even more control over their data and more transparency from their cloud services. At Google Cloud, one of the best tools for achieving that level of control and transparency is a bit of technological magic we call Cloud External Key Manager (EKM). Cloud EKM can help you protect your cloud data at rest with encryption keys which are stored and managed in a third-party key management system that’s outside Google Cloud’s infrastructure, and ultimately outside Google's control. This can help [...]
