Title: New Year, new cyber security career
Date: 2022-10-26T09:00:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-10-26-new-year-new-cyber-security-career

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/26/new_year_new_cyber_security/){:target="_blank" rel="noopener"}

> Say hello to SANS 2023 training events and the new job that will inevitably follow Sponsored Post The turn of the year is always a good time to take stock and think about where you are heading. Many hard working cybersecurity professionals will be keeping as close an eye on the calendar as they are on cyber criminals.... [...]
