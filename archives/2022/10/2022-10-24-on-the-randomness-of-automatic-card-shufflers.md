Title: On the Randomness of Automatic Card Shufflers
Date: 2022-10-24T11:37:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;gambling;loopholes;random numbers
Slug: 2022-10-24-on-the-randomness-of-automatic-card-shufflers

[Source](https://www.schneier.com/blog/archives/2022/10/on-the-randomness-of-automatic-card-shufflers.html){:target="_blank" rel="noopener"}

> Many years ago, Matt Blaze and I talked about getting our hands on a casino-grade automatic shuffler and looking for vulnerabilities. We never did it—I remember that we didn’t even try very hard—but this article shows that we probably would have found non-random properties:...the executives had recently discovered that one of their machines had been hacked by a gang of hustlers. The gang used a hidden video camera to record the workings of the card shuffler through a glass window. The images, transmitted to an accomplice outside in the casino parking lot, were played back in slow motion to figure [...]
