Title: Hacker steals $566 million worth of crypto from Binance Bridge
Date: 2022-10-06T20:30:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-10-06-hacker-steals-566-million-worth-of-crypto-from-binance-bridge

[Source](https://www.bleepingcomputer.com/news/security/hacker-steals-566-million-worth-of-crypto-from-binance-bridge/){:target="_blank" rel="noopener"}

> Hackers have reportedly stolen 2 million Binance Coins (BNB), worth $566 million, from the Binance Bridge. [...]
