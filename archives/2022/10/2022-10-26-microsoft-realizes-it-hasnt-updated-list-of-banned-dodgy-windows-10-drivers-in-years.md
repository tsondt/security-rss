Title: Microsoft realizes it hasn't updated list of banned dodgy Windows 10 drivers in years
Date: 2022-10-26T18:45:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-26-microsoft-realizes-it-hasnt-updated-list-of-banned-dodgy-windows-10-drivers-in-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/26/microsoft_windows_driver_hvci_blocked_list/){:target="_blank" rel="noopener"}

> Hope no one was relying on that to block threats, er, yeah? Microsoft appears to have woken up and realized it may have left certain Windows Server and Windows 10 systems exposed to exploitable drivers for years.... [...]
