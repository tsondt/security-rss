Title: Cloud migration and the cyber skills shortage
Date: 2022-10-20T17:51:08+00:00
Author: Guy Matthews
Category: The Register
Tags: 
Slug: 2022-10-20-cloud-migration-and-the-cyber-skills-shortage

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/20/cloud_migration_and_the_cyber/){:target="_blank" rel="noopener"}

> Protecting applications off prem demands a fresh wave of security talent Sponsored Post Shifting workloads and applications to the cloud is on every forward-thinking CIO's wish list. It is also their worst nightmare. If they get it right, they've helped to transform and modernize their organization's operations and everyone's happy. If they get it wrong, it's a different story, made much worse if a seriously expensive data breach is involved.... [...]
