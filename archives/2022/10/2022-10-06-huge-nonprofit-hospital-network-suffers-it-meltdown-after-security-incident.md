Title: Huge nonprofit hospital network suffers IT meltdown after 'security incident'
Date: 2022-10-06T21:55:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-06-huge-nonprofit-hospital-network-suffers-it-meltdown-after-security-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/06/commonspirit_health_cyberattack/){:target="_blank" rel="noopener"}

> Ambulances diverted, patient records frozen, rhymes with handsome wear America's second-largest nonprofit healthcare org is suffering a security "issue" that has diverted ambulances and shut down electronic records systems at hospitals around the country.... [...]
