Title: Chegg sued by FTC after suffering four data breaches within 3 years
Date: 2022-10-31T14:07:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-10-31-chegg-sued-by-ftc-after-suffering-four-data-breaches-within-3-years

[Source](https://www.bleepingcomputer.com/news/security/chegg-sued-by-ftc-after-suffering-four-data-breaches-within-3-years/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) has sued education technology company Chegg after it exposed the sensitive information of tens of millions of customers and employees in four data breaches suffered since 2017. [...]
