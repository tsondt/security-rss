Title: Oops, web trackers may have leaked 3 million patients' info
Date: 2022-10-20T23:42:48+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-10-20-oops-web-trackers-may-have-leaked-3-million-patients-info

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/20/health_group_says_tracking_pixel/){:target="_blank" rel="noopener"}

> Scream with us: Aaaaaa-AAH A hospital network in Wisconsin and Illinois fears visitor tracking code on its websites may have transmitted personal information on as many as 3 million patients to Meta, Google, and other third parties.... [...]
