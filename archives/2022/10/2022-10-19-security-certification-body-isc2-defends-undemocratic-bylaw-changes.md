Title: Security certification body (ISC)² defends ‘undemocratic’ bylaw changes
Date: 2022-10-19T15:11:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-19-security-certification-body-isc2-defends-undemocratic-bylaw-changes

[Source](https://portswigger.net/daily-swig/security-certification-body-isc-defends-undemocratic-bylaw-changes){:target="_blank" rel="noopener"}

> Former chair bemoans ‘coup by governance’ [...]
