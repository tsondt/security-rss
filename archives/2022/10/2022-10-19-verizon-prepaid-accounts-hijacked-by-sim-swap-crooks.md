Title: Verizon prepaid accounts hijacked by SIM swap crooks
Date: 2022-10-19T22:04:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-19-verizon-prepaid-accounts-hijacked-by-sim-swap-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/19/verizon_breach_sim_swap/){:target="_blank" rel="noopener"}

> Nightmare for those with one-time security codes texted to their phones Verizon has notified some prepaid customers that their accounts were compromised and their phone numbers potentially hijacked by crooks via SIM swaps.... [...]
