Title: Hacktivists say they stole 100,000 emails from Iran's nuclear energy agency
Date: 2022-10-24T02:30:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-24-hacktivists-say-they-stole-100000-emails-from-irans-nuclear-energy-agency

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/24/black_reward_iran_nuclear_leak/){:target="_blank" rel="noopener"}

> Tehran laughs it off as foreign psyop or media stunt. Just don't remind them about Stuxnet, OK? Iran's Atomic Energy Organization has laughed off claims that the email systems of a subsidiary were compromised, revealing important operational data about a nuclear power plant.... [...]
