Title: FBI: Cyberattacks targeting election systems unlikely to affect results
Date: 2022-10-05T17:49:54-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-05-fbi-cyberattacks-targeting-election-systems-unlikely-to-affect-results

[Source](https://www.bleepingcomputer.com/news/security/fbi-cyberattacks-targeting-election-systems-unlikely-to-affect-results/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) and the Cybersecurity and Infrastructure Security Agency (CISA) in a public service announcement says that cyber activity attempting to compromise election infrastructure is unlikely to cause a massive disruption or prevent voting. [...]
