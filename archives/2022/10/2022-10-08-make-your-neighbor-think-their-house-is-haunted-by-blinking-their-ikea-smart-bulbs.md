Title: Make your neighbor think their house is haunted by blinking their Ikea smart bulbs
Date: 2022-10-08T08:08:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-08-make-your-neighbor-think-their-house-is-haunted-by-blinking-their-ikea-smart-bulbs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/08/buggy_ikea_smart_bulbs/){:target="_blank" rel="noopener"}

> Radio comms vulnerabilities detailed A couple of vulnerabilities in Ikea smart lighting systems can be exploited to make lights annoyingly flicker for hours.... [...]
