Title: Alert: This ransomware preys on healthcare orgs via weak-ass VPN servers
Date: 2022-10-24T17:00:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-24-alert-this-ransomware-preys-on-healthcare-orgs-via-weak-ass-vpn-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/24/cisa_fbi_daixin_ransomware/){:target="_blank" rel="noopener"}

> FBI, CISA warn of Daixin gang after OakBend Medical Center hit Federal agencies are warning of a threat group called Daixin Team that is using ransomware and data extortion tactics to target US healthcare organizations.... [...]
