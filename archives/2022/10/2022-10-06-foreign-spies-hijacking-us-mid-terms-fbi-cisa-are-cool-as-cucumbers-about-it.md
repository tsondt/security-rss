Title: Foreign spies hijacking US mid-terms? FBI, CISA are cool as cucumbers about it
Date: 2022-10-06T17:30:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-10-06-foreign-spies-hijacking-us-mid-terms-fbi-cisa-are-cool-as-cucumbers-about-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/06/us_midterm_election_cyberattack_unlikely/){:target="_blank" rel="noopener"}

> I think we can handle one little Russia. We sent two units, they're bringing any attempts down now The FBI and the US government's Cybersecurity and Infrastructure Security Agency (CISA) claim any foreign interference in the 2022 US midterm elections is unlikely to disrupt or prevent voting, compromise ballot integrity, or manipulate votes at scale.... [...]
