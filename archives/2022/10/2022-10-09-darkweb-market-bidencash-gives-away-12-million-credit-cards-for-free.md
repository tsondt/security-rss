Title: Darkweb market BidenCash gives away 1.2 million credit cards for free
Date: 2022-10-09T11:12:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-09-darkweb-market-bidencash-gives-away-12-million-credit-cards-for-free

[Source](https://www.bleepingcomputer.com/news/security/darkweb-market-bidencash-gives-away-12-million-credit-cards-for-free/){:target="_blank" rel="noopener"}

> A dark web carding market named 'BidenCash' has released a massive dump of 1,221,551 credit cards to promote their marketplace, allowing anyone to download them for free to conduct financial fraud. [...]
