Title: DoJ ‘very disappointed’ with probation sentence for Capital One hacker Paige Thompson
Date: 2022-10-05T05:31:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-05-doj-very-disappointed-with-probation-sentence-for-capital-one-hacker-paige-thompson

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/05/paige_thompson_sentence_doj_unhappy/){:target="_blank" rel="noopener"}

> ‘This is not what justice looks like’ says US attorney of sanction for leak of 100 million records Convicted wire fraud perpetrator Paige Thompson (aka "erratic") has been sentenced to time served and five years of probation with location and computer monitoring, prompting U.S. Attorney Nick Brown to label the sanctions unsatisfactory.... [...]
