Title: Purpleurchin cryptocurrency miners spotted scouring free GitHub, Heroku accounts
Date: 2022-10-27T07:27:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-27-purpleurchin-cryptocurrency-miners-spotted-scouring-free-github-heroku-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/27/purpleurchin_cryptomining_github_accounts/){:target="_blank" rel="noopener"}

> This is why we can't have nice things A stealthy cryptocurrency mining operation has been spotted using thousands of free accounts on GitHub, Heroku and other DevOps outfits to craft digital tokens. GitHub, for one, forbids the mining of coins using its cloud resources.... [...]
