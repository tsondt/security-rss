Title: Fortinet warns admins to patch critical auth bypass bug immediately
Date: 2022-10-07T09:04:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-10-07-fortinet-warns-admins-to-patch-critical-auth-bypass-bug-immediately

[Source](https://www.bleepingcomputer.com/news/security/fortinet-warns-admins-to-patch-critical-auth-bypass-bug-immediately/){:target="_blank" rel="noopener"}

> Fortinet has warned administrators to update FortiGate firewalls and FortiProxy web proxies to the latest versions, which address a critical severity vulnerability. [...]
