Title: October Is Cybersecurity Awareness Month
Date: 2022-10-05T19:07:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;history of security;security education
Slug: 2022-10-05-october-is-cybersecurity-awareness-month

[Source](https://www.schneier.com/blog/archives/2022/10/october-is-cybersecurity-awareness-month.html){:target="_blank" rel="noopener"}

> For the past nineteen years, October has been Cybersecurity Awareness Month here in the US, and that event that has always been part advice and part ridicule. I tend to fall on the apathy end of the spectrum; I don’t think I’ve ever mentioned it before. But the memes can be funny. Here’s a decent rundown of some of the chatter. [...]
