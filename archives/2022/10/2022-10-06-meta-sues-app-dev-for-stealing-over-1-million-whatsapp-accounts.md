Title: Meta sues app dev for stealing over 1 million WhatsApp accounts
Date: 2022-10-06T14:03:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-10-06-meta-sues-app-dev-for-stealing-over-1-million-whatsapp-accounts

[Source](https://www.bleepingcomputer.com/news/security/meta-sues-app-dev-for-stealing-over-1-million-whatsapp-accounts/){:target="_blank" rel="noopener"}

> Meta has sued several Chinese companies doing business as HeyMods, Highlight Mobi, and HeyWhatsApp for developing and allegedly using "unofficial" WhatsApp Android apps to steal over one million WhatsApp accounts starting May 2022. [...]
