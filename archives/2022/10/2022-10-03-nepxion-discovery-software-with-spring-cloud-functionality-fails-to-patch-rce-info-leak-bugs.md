Title: Nepxion Discovery software with Spring Cloud functionality fails to patch RCE, info leak bugs
Date: 2022-10-03T11:20:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-03-nepxion-discovery-software-with-spring-cloud-functionality-fails-to-patch-rce-info-leak-bugs

[Source](https://portswigger.net/daily-swig/nepxion-discovery-software-with-spring-cloud-functionality-fails-to-patch-rce-info-leak-bugs){:target="_blank" rel="noopener"}

> Maintainer of Chinese project closes public issue apparently without issuing a fix [...]
