Title: Online romance scamlord who netted $9.5m jailed for 25 years
Date: 2022-10-03T22:15:47+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-03-online-romance-scamlord-who-netted-95m-jailed-for-25-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/03/romance_scammer_sentenced/){:target="_blank" rel="noopener"}

> Hello, love, I need $32k to fix my oil rig A man in the US has been jailed for 25 years after using dating websites, email scams, and other online swindles to steal more than $9.5 million from companies and individuals.... [...]
