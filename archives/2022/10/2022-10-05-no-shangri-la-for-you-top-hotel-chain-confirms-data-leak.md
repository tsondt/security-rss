Title: No Shangri-La for you: Top hotel chain confirms data leak
Date: 2022-10-05T02:15:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-05-no-shangri-la-for-you-top-hotel-chain-confirms-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/05/shangri_data_leak/){:target="_blank" rel="noopener"}

> In Xanadu did Kubla Khan a stately pleasure-dome decree Hotel chain Shangri-La Group has admitted to its systems being attacked, and personal data describing guests accessed by unknown parties, over a timeframe that includes the dates on which a high-level international defence conference was staged at one of its Singapore properties.... [...]
