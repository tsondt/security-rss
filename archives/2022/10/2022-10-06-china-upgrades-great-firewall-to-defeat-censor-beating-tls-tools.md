Title: China upgrades Great Firewall to defeat censor-beating TLS tools
Date: 2022-10-06T03:31:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-06-china-upgrades-great-firewall-to-defeat-censor-beating-tls-tools

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/06/great_firewall_of_china_upgrades/){:target="_blank" rel="noopener"}

> Just in time to ensure nobody can disagree that giving Xi five more years as president is the best idea ever China appears to have upgraded its Great Firewall, the instrument of pervasive real-time censorship it uses to ensure that ideas its government doesn’t like don’t reach China’s citizens.... [...]
