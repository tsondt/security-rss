Title: Intel Alder Lake BIOS code leak may contain vital secrets
Date: 2022-10-10T16:45:13+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-10-10-intel-alder-lake-bios-code-leak-may-contain-vital-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/10/alder_lake_bios_code_leaked/){:target="_blank" rel="noopener"}

> Gurus say source includes secret hardware info, private signing key for Boot Guard protection Source code for the BIOS used with Intel's 12th-gen Core processors has been leaked online, possibly including details of undocumented model-specific registers (MSRs) and even the private signing key for Intel's Boot Guard security technology.... [...]
