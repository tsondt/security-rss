Title: Matrix address flaws that break message encryption assurances
Date: 2022-10-04T14:49:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-04-matrix-address-flaws-that-break-message-encryption-assurances

[Source](https://portswigger.net/daily-swig/matrix-address-flaws-that-break-message-encryption-assurances){:target="_blank" rel="noopener"}

> Confidentiality and authentication flaws uncovered by researchers [...]
