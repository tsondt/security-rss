Title: Jira Align flaws enabled malicious users to gain super admin privileges
Date: 2022-10-26T16:00:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-26-jira-align-flaws-enabled-malicious-users-to-gain-super-admin-privileges

[Source](https://portswigger.net/daily-swig/jira-align-flaws-enabled-malicious-users-to-gain-super-admin-privileges){:target="_blank" rel="noopener"}

> Super admins can, among other things, modify Jira connections, reset user accounts, and modify security settings [...]
