Title: Upcoming ‘critical’ OpenSSL update prompts feverish speculation
Date: 2022-10-28T14:31:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-28-upcoming-critical-openssl-update-prompts-feverish-speculation

[Source](https://portswigger.net/daily-swig/upcoming-critical-openssl-update-prompts-feverish-speculation){:target="_blank" rel="noopener"}

> Is it the new Heartbleed or just a bleeding distraction? [...]
