Title: A sneak peek at the security, identity, and compliance sessions for re:Invent 2022
Date: 2022-10-20T17:05:28+00:00
Author: Katie Collins
Category: AWS Security
Tags: Announcements;AWS re:Invent;Events;Security, Identity, & Compliance;AWS Re:Invent;AWS security;Re:Invent 2022;Security Blog;Security education
Slug: 2022-10-20-a-sneak-peek-at-the-security-identity-and-compliance-sessions-for-reinvent-2022

[Source](https://aws.amazon.com/blogs/security/a-sneak-peek-at-the-security-identity-and-compliance-sessions-for-reinvent-2022/){:target="_blank" rel="noopener"}

> AWS re:Invent 2022 is fast approaching, and this post can help you plan your agenda with a look at the sessions in the security track. AWS re:Invent, your opportunity to catch up on the latest technologies in cloud computing, will take place in person in Las Vegas, NV, from November 28 – December 2, 2022. [...]
