Title: Microsoft Exchange server zero-day mitigation can be bypassed
Date: 2022-10-03T10:21:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-10-03-microsoft-exchange-server-zero-day-mitigation-can-be-bypassed

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-server-zero-day-mitigation-can-be-bypassed/){:target="_blank" rel="noopener"}

> Microsoft has shared mitigations for two new Microsoft Exchange zero-day vulnerabilities tracked as CVE-2022-41040 and CVE-2022-41082, but researchers warn that the mitigation for on-premise servers is far from enough. [...]
