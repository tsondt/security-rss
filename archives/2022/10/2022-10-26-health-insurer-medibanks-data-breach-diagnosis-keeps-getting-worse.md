Title: Health insurer Medibank's data breach diagnosis keeps getting worse
Date: 2022-10-26T03:45:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-26-health-insurer-medibanks-data-breach-diagnosis-keeps-getting-worse

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/26/medibank_breach_update/){:target="_blank" rel="noopener"}

> All four million customers at risk of having records of medical treatments exposed Australian health insurer Medibank's data breach was today revealed to be even worse than first thought, with a regulatory filing stating that info describing all four million customers has been accessed.... [...]
