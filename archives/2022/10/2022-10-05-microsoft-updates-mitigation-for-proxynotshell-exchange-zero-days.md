Title: Microsoft updates mitigation for ProxyNotShell Exchange zero days
Date: 2022-10-05T08:58:16-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-10-05-microsoft-updates-mitigation-for-proxynotshell-exchange-zero-days

[Source](https://www.bleepingcomputer.com/news/security/microsoft-updates-mitigation-for-proxynotshell-exchange-zero-days/){:target="_blank" rel="noopener"}

> Microsoft has updated the mitigation for the latest Exchange zero-day vulnerabilities tracked as CVE-2022-41040 and CVE-2022-41082, also referred to ProxyNotShell. [...]
