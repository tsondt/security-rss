Title: Federal bans aren't stopping US states from buying forbidden Chinese kit
Date: 2022-10-28T17:32:06+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-10-28-federal-bans-arent-stopping-us-states-from-buying-forbidden-chinese-kit

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/28/federal_bans_china_law/){:target="_blank" rel="noopener"}

> Report claims thousands of orgs are still happily writing checks Only a "handful" of US states have stopped buying Chinese technologies deemed by the government to pose security threats, according to a report from a Washington policy research group.... [...]
