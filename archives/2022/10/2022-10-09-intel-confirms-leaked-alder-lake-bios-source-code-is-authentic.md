Title: Intel confirms leaked Alder Lake BIOS Source Code is authentic
Date: 2022-10-09T20:53:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2022-10-09-intel-confirms-leaked-alder-lake-bios-source-code-is-authentic

[Source](https://www.bleepingcomputer.com/news/security/intel-confirms-leaked-alder-lake-bios-source-code-is-authentic/){:target="_blank" rel="noopener"}

> Intel has confirmed that a source code leak for the UEFI BIOS of Alder Lake CPUs is authentic and has been released by a third party. [...]
