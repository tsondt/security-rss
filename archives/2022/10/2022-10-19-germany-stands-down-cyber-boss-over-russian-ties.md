Title: Germany stands down cyber boss over Russian ties
Date: 2022-10-19T07:30:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-19-germany-stands-down-cyber-boss-over-russian-ties

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/19/germany_stands_down_cyber_boss/){:target="_blank" rel="noopener"}

> Involvement with lobby group that welcomed Putin's pals presses buttons Germany's government has stood down the president of its Federal Office for Information Security, Arne Schönbohm, over his links to Russia.... [...]
