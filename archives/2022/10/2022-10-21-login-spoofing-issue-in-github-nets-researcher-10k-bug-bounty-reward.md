Title: Login spoofing issue in GitHub nets researcher $10k bug bounty reward
Date: 2022-10-21T14:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-21-login-spoofing-issue-in-github-nets-researcher-10k-bug-bounty-reward

[Source](https://portswigger.net/daily-swig/login-spoofing-issue-in-github-nets-researcher-10k-bug-bounty-reward){:target="_blank" rel="noopener"}

> Platform pays high reward for bug reported as ‘low severity’ [...]
