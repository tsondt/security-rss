Title: FBI: Looking for Biden's student loan forgiveness? Watch out for these scams
Date: 2022-10-19T01:20:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-10-19-fbi-looking-for-bidens-student-loan-forgiveness-watch-out-for-these-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/19/fbi_warns_student_loan_forgiveness/){:target="_blank" rel="noopener"}

> You really think someone would do that? Just go on the internet and steal identities? In what can only be described as inevitable, the FBI is warning those eligible for student loan debt relief to keep an eye out for scammers trying to take advantage of President's Biden program.... [...]
