Title: Announcing new GKE functionality for streamlined security management
Date: 2022-10-26T16:00:00+00:00
Author: Victor Szalvay
Category: GCP Security
Tags: Containers & Kubernetes;Identity & Security
Slug: 2022-10-26-announcing-new-gke-functionality-for-streamlined-security-management

[Source](https://cloud.google.com/blog/products/identity-security/introducing-new-gke-interface-for-streamlined-security-management/){:target="_blank" rel="noopener"}

> At Google Cloud, we’re driven by a vision of invisible security, where advanced security capabilities are engineered into our platforms, operations are simplified, and stronger security outcomes can be achieved. As we pursue this ideal, we want to help make security easier to use and manage. Our new built-in Google Kubernetes Engine (GKE) security posture dashboard (now available in Preview) does both, with opinionated guidance for customers that can help improve the security posture of your GKE clusters and containerized workloads. It also includes insights into vulnerabilities and workload configuration checks, and offers integrated event logging so you can subscribe [...]
