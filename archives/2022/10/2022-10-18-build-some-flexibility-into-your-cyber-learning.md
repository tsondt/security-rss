Title: Build some flexibility into your cyber learning
Date: 2022-10-18T21:00:15+00:00
Author: Guy Matthews
Category: The Register
Tags: 
Slug: 2022-10-18-build-some-flexibility-into-your-cyber-learning

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/18/build_some_flexibility_into_your/){:target="_blank" rel="noopener"}

> Training should bend around the many moving parts in your daily schedule, not the other way around. Sponsored Post We're all looking for a way to get the best cyber security training on the market, so we can push ahead in our careers. But we want to do it at our own pace, and in a location that suits us.... [...]
