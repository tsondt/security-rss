Title: Netwalker ransomware affiliate sentenced to 20 years in prison
Date: 2022-10-04T16:10:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-10-04-netwalker-ransomware-affiliate-sentenced-to-20-years-in-prison

[Source](https://www.bleepingcomputer.com/news/security/netwalker-ransomware-affiliate-sentenced-to-20-years-in-prison/){:target="_blank" rel="noopener"}

> Former Netwalker ransomware affiliate Sebastien Vachon-Desjardins has been sentenced to 20 years in prison and demanded to forfeit $21.5 million for his attacks on a Tampa company and other entities. [...]
