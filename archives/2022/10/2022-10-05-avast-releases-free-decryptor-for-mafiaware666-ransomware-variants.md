Title: Avast releases free decryptor for MafiaWare666 ransomware variants
Date: 2022-10-05T13:46:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-05-avast-releases-free-decryptor-for-mafiaware666-ransomware-variants

[Source](https://www.bleepingcomputer.com/news/security/avast-releases-free-decryptor-for-mafiaware666-ransomware-variants/){:target="_blank" rel="noopener"}

> ​Avast has released a decryptor for variants of the MafiaWare666 ransomware known as 'Jcrypt', 'RIP Lmao', and 'BrutusptCrypt,' allowing victims to recover their files for free. [...]
