Title: RC4 Is Still Considered Harmful
Date: 2022-10-27T12:48:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2022-10-27-rc4-is-still-considered-harmful

[Source](https://googleprojectzero.blogspot.com/2022/10/rc4-is-still-considered-harmful.html){:target="_blank" rel="noopener"}

> By James Forshaw, Project Zero I've been spending a lot of time researching Windows authentication implementations, specifically Kerberos. In June 2022 I found an interesting issue number 2310 with the handling of RC4 encryption that allowed you to authenticate as another user if you could either interpose on the Kerberos network traffic to and from the KDC or directly if the user was configured to disable typical pre-authentication requirements. This blog post goes into more detail on how this vulnerability works and how I was able to exploit it with only a bare minimum of brute forcing required. Note, I'm [...]
