Title: Australia Increases Fines for Massive Data Breaches
Date: 2022-10-26T11:13:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Australia;cyberattack;data breaches;incentives
Slug: 2022-10-26-australia-increases-fines-for-massive-data-breaches

[Source](https://www.schneier.com/blog/archives/2022/10/australia-increases-fines-for-massive-data-breaches.html){:target="_blank" rel="noopener"}

> After suffering two large, and embarrassing, data breaches in recent weeks, the Australian government increased the fine for serious data breaches from $2.2 million to a minimum of $50 million. (That’s $50 million AUD, or $32 million USD.) This is a welcome change. The problem is one of incentives, and Australia has now increased the incentive for companies to secure the personal data or their users and customers. [...]
