Title: Utility security is so bad, US DoE offers rate cuts to improve it
Date: 2022-10-07T15:15:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-10-07-utility-security-is-so-bad-us-doe-offers-rate-cuts-to-improve-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/07/utility_security/){:target="_blank" rel="noopener"}

> New hardware? Consultants? You tell us because your infosec is off the grid The US Department of Energy has proposed regulations to financially reward cybersecurity modernization at power plants by offering rate deals for everything from buying new hardware to paying for outside help.... [...]
