Title: Introducing Assured Workloads in Canada and Australia, and new features for all
Date: 2022-10-27T16:00:00+00:00
Author: Collin Frierson
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-10-27-introducing-assured-workloads-in-canada-and-australia-and-new-features-for-all

[Source](https://cloud.google.com/blog/products/identity-security/introducing-assured-workloads-in-canada-and-australia/){:target="_blank" rel="noopener"}

> At Google Cloud, we continue to invest in our vision of invisible security where advanced capabilities are engineered into our platforms, operations are simplified, and stronger outcomes can be achieved. Assured Workloads is a Google Cloud service that helps customers create and maintain controlled environments that accelerate running more secure and compliant workloads, including enforcement of data residency, administrative and personnel controls, and managing encryption keys. Today, we are extending these capabilities to more regions. Assured Workloads for Canada is now generally available, and Assured Workloads for Australia is now available in preview. We are also making it easier for [...]
