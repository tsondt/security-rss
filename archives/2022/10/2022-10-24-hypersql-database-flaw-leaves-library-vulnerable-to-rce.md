Title: HyperSQL DataBase flaw leaves library vulnerable to RCE
Date: 2022-10-24T14:46:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-24-hypersql-database-flaw-leaves-library-vulnerable-to-rce

[Source](https://portswigger.net/daily-swig/hypersql-database-flaw-leaves-library-vulnerable-to-rce){:target="_blank" rel="noopener"}

> Mishandling of untrusted input issue resolved by developers [...]
