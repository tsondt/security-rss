Title: Cisco AnyConnect Windows client under active attack
Date: 2022-10-26T20:31:23+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-26-cisco-anyconnect-windows-client-under-active-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/26/cisco_vpn_bugs_exploited/){:target="_blank" rel="noopener"}

> Make sure you're patched – and update VMware Cloud Foundation, too, by the way Cisco says miscreants are exploiting two vulnerabilities in its AnyConnect Secure Mobility Client for Windows, which is supposed to ensure safe VPN access for remote workers.... [...]
