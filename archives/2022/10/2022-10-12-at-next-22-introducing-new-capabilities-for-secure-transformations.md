Title: At Next ’22, introducing new capabilities for secure transformations
Date: 2022-10-12T16:00:00+00:00
Author: Jeff Reed
Category: GCP Security
Tags: Google Cloud;Next;Identity & Security
Slug: 2022-10-12-at-next-22-introducing-new-capabilities-for-secure-transformations

[Source](https://cloud.google.com/blog/products/identity-security/introducing-new-capabilities-for-secure-transformations/){:target="_blank" rel="noopener"}

> Organizations large and small are realizing that digital transformation and the changing threat landscape require a grounds up effort to transform security. At Google Cloud, we continue to invest in our vision of invisible security where advanced capabilities are engineered into our platforms, operations are simplified, and stronger security outcomes can be achieved. We made five major security announcements yesterday at Google Cloud Next : Introducing Chronicle Security Operations, to help detect, investigate, and respond to cyberthreats with the speed, scale, and intelligence of Google Introducing Confidential Space, to help unlock the value of secure data collaboration Advancing digital sovereignty [...]
