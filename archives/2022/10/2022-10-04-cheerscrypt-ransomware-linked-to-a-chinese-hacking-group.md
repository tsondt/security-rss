Title: Cheerscrypt ransomware linked to a Chinese hacking group
Date: 2022-10-04T11:46:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-04-cheerscrypt-ransomware-linked-to-a-chinese-hacking-group

[Source](https://www.bleepingcomputer.com/news/security/cheerscrypt-ransomware-linked-to-a-chinese-hacking-group/){:target="_blank" rel="noopener"}

> The Cheerscrypt ransomware has been linked to a Chinese hacking group named 'Emperor Dragonfly,' known to frequently switch between ransomware families to evade attribution. [...]
