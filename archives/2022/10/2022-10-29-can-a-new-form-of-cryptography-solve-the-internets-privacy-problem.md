Title: Can a new form of cryptography solve the internet’s privacy problem?
Date: 2022-10-29T14:00:02+00:00
Author: Alex Bellos
Category: The Guardian
Tags: Data protection;Privacy;Data and computer security;Technology;Internet
Slug: 2022-10-29-can-a-new-form-of-cryptography-solve-the-internets-privacy-problem

[Source](https://www.theguardian.com/technology/2022/oct/29/privacy-problem-tech-enhancing-data-political-legal){:target="_blank" rel="noopener"}

> Techniques which allow the sharing of data whilst keeping it secure may revolutionise fields from healthcare to law enforcement Rachel is a student at a US university who was sexually assaulted on campus. She decided against reporting it ( fewer than 10% of survivors do). What she did, however, was register the assault on a website that is using novel ideas from cryptography to help catch serial sexual predators. The organisation Callisto lets a survivor enter their name in a database, together with identifying details of their assailant, such as social media handle or phone number. These details are encrypted, [...]
