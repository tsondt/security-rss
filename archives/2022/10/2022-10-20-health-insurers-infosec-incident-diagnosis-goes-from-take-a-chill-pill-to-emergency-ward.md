Title: Health insurer's infosec incident diagnosis goes from 'take a chill pill' to emergency ward
Date: 2022-10-20T01:34:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-20-health-insurers-infosec-incident-diagnosis-goes-from-take-a-chill-pill-to-emergency-ward

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/20/medibank_data_breach_worsens/){:target="_blank" rel="noopener"}

> Australia's Medibank says it's been shown stolen data that includes details of treatments administered to customers Updated Australian health insurer Medibank has revealed it's been contacted by a group that claims to have its customers' data and is threatening to distribute it.... [...]
