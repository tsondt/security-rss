Title: National Cybersecurity Awareness program 18 years on: Don't click that
Date: 2022-10-03T17:30:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-03-national-cybersecurity-awareness-program-18-years-on-dont-click-that

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/03/cybersecurity_awareness_month/){:target="_blank" rel="noopener"}

> Technology is addressing many of the cyberthreats, but the human element will always be a factor If you've ever found yourself in an interminable meeting listening to the CISO ramble on about the important role you play in protecting yourself and the company from cyberthreats, you could probably point an accusatory finger in large part at the National Cybersecurity Awareness Month (NCSAM) program.... [...]
