Title: The point solution IAM evolution under reform
Date: 2022-10-27T13:01:57+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2022-10-27-the-point-solution-iam-evolution-under-reform

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/27/the_point_solution_iam_evolution/){:target="_blank" rel="noopener"}

> A consolidation of IAM tools, suppliers and managed services providers is changing the default approach Sponsored Feature The inexorable pace of technological innovation in response to the unrelenting growth of cyber attacks has led to fragmentation within cyber security provision. Things generally follow a common pattern, starting with a new security requirement being identified, whether a response to a novel threat, or a compliance or regulation challenge. This leads buyers to specialized tools, usually from smaller vendors that do one thing well. But inevitably over time, buyers end up using a mishmash of systems and tools, each with its own [...]
