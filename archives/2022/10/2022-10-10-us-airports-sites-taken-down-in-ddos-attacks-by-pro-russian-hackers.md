Title: US airports' sites taken down in DDoS attacks by pro-Russian hackers
Date: 2022-10-10T10:15:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-10-us-airports-sites-taken-down-in-ddos-attacks-by-pro-russian-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-airports-sites-taken-down-in-ddos-attacks-by-pro-russian-hackers/){:target="_blank" rel="noopener"}

> The pro-Russian hacktivist group 'KillNet' is claiming large-scale distributed denial-of-service (DDoS) attacks against the websites of several major airports in the U.S., making them unaccessible. [...]
