Title: A year of SANS security summits
Date: 2022-10-24T09:11:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-10-24-a-year-of-sans-security-summits

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/24/a_year_of_sans_security/){:target="_blank" rel="noopener"}

> A mixture of free online events and in-person conferences that put you at the heart of the cyber security industry Sponsored Post Where do the world's cyber security professionals get an opportunity to mingle and swap tips with their global peers while engaging in interactive, hands-on learning exercises that will help them stop cyber criminals in their tracks?... [...]
