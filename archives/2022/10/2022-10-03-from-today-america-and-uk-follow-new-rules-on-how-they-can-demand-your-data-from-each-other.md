Title: From today, America and UK follow new rules on how they can demand your data from each other
Date: 2022-10-03T19:11:42+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-10-03-from-today-america-and-uk-follow-new-rules-on-how-they-can-demand-your-data-from-each-other

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/03/us_uk_data_access_agreement/){:target="_blank" rel="noopener"}

> Cops and Feds get easier info sharing, Britain benefits most The Data Access Agreement (DAA), by which the US and UK have agreed how one country can respond to lawful data demands from police and investigators in the other, took effect on Monday.... [...]
