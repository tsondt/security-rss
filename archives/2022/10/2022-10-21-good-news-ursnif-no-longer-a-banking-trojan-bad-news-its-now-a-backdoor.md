Title: Good news, URSNIF no longer a banking trojan. Bad news, it's now a backdoor
Date: 2022-10-21T10:28:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-21-good-news-ursnif-no-longer-a-banking-trojan-bad-news-its-now-a-backdoor

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/21/ursnif_trojan_shift_ransomware/){:target="_blank" rel="noopener"}

> And one designed to slip ransomware and data-stealing code onto infected machines URSNIF, the malware also known as Gozi that attempts to steal online banking credentials from victims' Windows PCs, is evolving to support extortionware.... [...]
