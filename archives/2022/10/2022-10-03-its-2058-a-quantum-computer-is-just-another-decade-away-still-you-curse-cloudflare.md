Title: It's 2058. A quantum computer is just another decade away. Still, you curse Cloudflare
Date: 2022-10-03T18:22:38+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-03-its-2058-a-quantum-computer-is-just-another-decade-away-still-you-curse-cloudflare

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/03/cloudflare_postquantum_cryptography/){:target="_blank" rel="noopener"}

> Assuming this Kyber TLS stuff works as expected Cloudflare is the first major internet infrastructure provider to support post-quantum cryptography for all customers, which, in theory, should protect data if quantum computing ever manages to break today's encryption technologies.... [...]
