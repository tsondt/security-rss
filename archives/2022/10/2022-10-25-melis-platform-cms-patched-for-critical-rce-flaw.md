Title: Melis Platform CMS patched for critical RCE flaw
Date: 2022-10-25T15:20:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-25-melis-platform-cms-patched-for-critical-rce-flaw

[Source](https://portswigger.net/daily-swig/melis-platform-cms-patched-for-critical-rce-flaw){:target="_blank" rel="noopener"}

> POP chain crafted to demonstrate exploitability [...]
