Title: Hackers selling access to 576 corporate networks for $4 million
Date: 2022-10-31T14:45:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-31-hackers-selling-access-to-576-corporate-networks-for-4-million

[Source](https://www.bleepingcomputer.com/news/security/hackers-selling-access-to-576-corporate-networks-for-4-million/){:target="_blank" rel="noopener"}

> A new report shows that hackers are selling access to 576 corporate networks worldwide for a total cumulative sales price of $4,000,000, fueling attacks on the enterprise. [...]
