Title: Binance blockchain suffers $570 million hack
Date: 2022-10-07T13:49:06+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;Policy;Binance;blockchain;cryptocurrency;hacking;syndication
Slug: 2022-10-07-binance-blockchain-suffers-570-million-hack

[Source](https://arstechnica.com/?p=1887987){:target="_blank" rel="noopener"}

> Enlarge / CHINA - 2022/07/25: In this photo illustration, the cryptocurrency exchange trading platform Binance logo is displayed on a smartphone screen. (Photo Illustration by Budrul Chukrut/SOPA Images/LightRocket via Getty Images) (credit: SOPA Images ) Hackers have stolen around $570 million in tokens from Binance, in a rare blow to the world’s biggest crypto exchange and another dent to the troubled digital assets industry struggling to regain trust after a collapse in prices. Binance initially estimated on Friday that tokens worth about $100 million to $110 million had been taken, pausing the operation of the affected blockchain for approximately eight [...]
