Title: How to secure APIs against fraud and abuse with reCAPTCHA Enterprise and Apigee X
Date: 2022-10-07T17:00:00+00:00
Author: Nandan Sridhar
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-10-07-how-to-secure-apis-against-fraud-and-abuse-with-recaptcha-enterprise-and-apigee-x

[Source](https://cloud.google.com/blog/products/identity-security/how-to-secure-apis-against-fraud-and-abuse-with-recaptcha-enterprise-and-apigee-x/){:target="_blank" rel="noopener"}

> A comprehensive API security strategy requires protection from fraud and abuse. To better protect our publicly-facing APIs from malicious software that engages in abusive activities, we can deploy CAPTCHAs to disrupt abuse patterns. Developers can prevent attacks, reduce their API security surface area, and minimize disruption to users by implementing Google Cloud's reCAPTCHA Enterprise and Apigee X solutions. As Google Cloud's API management platform, Apigee X can help protect APIs using a reverse-proxy approach to HTTP requests and responses. One important feature of Apigee X is the ability to include a reCAPTCHA Enterprise challenge in the authentication (AuthN) stage of [...]
