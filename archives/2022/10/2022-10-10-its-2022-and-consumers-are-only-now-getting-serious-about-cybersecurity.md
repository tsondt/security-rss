Title: It’s 2022 and consumers are only now getting serious about cybersecurity
Date: 2022-10-10T12:30:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-10-its-2022-and-consumers-are-only-now-getting-serious-about-cybersecurity

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/10/users_security_privacy_tools/){:target="_blank" rel="noopener"}

> US consumers start to get the message about protecting themselves online End users, often viewed by infosec specialists as a corporation's weakest link, appear to be finally understanding the importance of good security and privacy practices.... [...]
