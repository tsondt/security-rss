Title: Friday Squid Blogging: Emotional Support Squid
Date: 2022-10-07T21:05:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2022-10-07-friday-squid-blogging-emotional-support-squid

[Source](https://www.schneier.com/blog/archives/2022/10/friday-squid-blogging-emotional-support-squid.html){:target="_blank" rel="noopener"}

> The Monterey Bay Aquarium has a video—” 2 Hours Of Squid To Relax/Study/Work To “—with 2.4 million views. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
