Title: Interview with Signal’s New President
Date: 2022-10-20T11:47:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Facebook;interviews;metadata;privacy;Signal;WhatsApp
Slug: 2022-10-20-interview-with-signals-new-president

[Source](https://www.schneier.com/blog/archives/2022/10/interview-with-signals-new-president.html){:target="_blank" rel="noopener"}

> Long and interesting interview with Signal’s new president, Meredith Whittaker: WhatsApp uses the Signal encryption protocol to provide encryption for its messages. That was absolutely a visionary choice that Brian and his team led back in the day ­- and big props to them for doing that. But you can’t just look at that and then stop at message protection. WhatsApp does not protect metadata the way that Signal does. Signal knows nothing about who you are. It doesn’t have your profile information and it has introduced group encryption protections. We don’t know who you are talking to or who [...]
