Title: Analyze Amazon Cognito advanced security intelligence to improve visibility and protection
Date: 2022-10-17T17:03:19+00:00
Author: Diana Alvarado
Category: AWS Security
Tags: Best Practices;Security, Identity, & Compliance;Automation;Cognito;Security Blog;WAF
Slug: 2022-10-17-analyze-amazon-cognito-advanced-security-intelligence-to-improve-visibility-and-protection

[Source](https://aws.amazon.com/blogs/security/analyze-amazon-cognito-advanced-security-intelligence-to-improve-visibility-and-protection/){:target="_blank" rel="noopener"}

> As your organization looks to improve your security posture and practices, early detection and prevention of unauthorized activity quickly becomes one of your main priorities. The behaviors associated with unauthorized activity commonly follow patterns that you can analyze in order to create specific mitigations or feed data into your security monitoring systems. This post shows [...]
