Title: Avast releases free decryptor for Hades ransomware variants
Date: 2022-10-05T13:46:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-05-avast-releases-free-decryptor-for-hades-ransomware-variants

[Source](https://www.bleepingcomputer.com/news/security/avast-releases-free-decryptor-for-hades-ransomware-variants/){:target="_blank" rel="noopener"}

> Avast has released a decryptor for variants of the Hades ransomware known as 'MafiaWare666', 'Jcrypt', 'RIP Lmao', and 'BrutusptCrypt,' allowing victims to recover their files for free. [...]
