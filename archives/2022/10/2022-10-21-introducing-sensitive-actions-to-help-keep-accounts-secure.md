Title: Introducing Sensitive Actions to help keep accounts secure
Date: 2022-10-21T17:00:00+00:00
Author: Rosemary McCloskey
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-10-21-introducing-sensitive-actions-to-help-keep-accounts-secure

[Source](https://cloud.google.com/blog/products/identity-security/announcing-sensitive-actions-to-help-keep-accounts-secure/){:target="_blank" rel="noopener"}

> At Google Cloud, we operate in a shared fate model, working in concert with our customers to help achieve stronger security outcomes. One of the ways we do this is to identify potentially risky behavior to help customers determine if action is appropriate. To this end, we now provide insights on what we are calling Sensitive Actions. Sensitive Actions, now available in Preview, are focused on understanding IAM account, or user account, behavior. They are changes made in a Google Cloud environment that are security relevant — and therefore important to be aware of and evaluate — because they may [...]
