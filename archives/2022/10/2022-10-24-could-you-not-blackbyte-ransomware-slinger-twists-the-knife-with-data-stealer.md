Title: Could you not? BlackByte ransomware slinger twists the knife with data stealer
Date: 2022-10-24T07:40:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-24-could-you-not-blackbyte-ransomware-slinger-twists-the-knife-with-data-stealer

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/24/blackbyte_ransomware_exbyte_extortion/){:target="_blank" rel="noopener"}

> Your IT storage may go from terabytes to Exbytes At least one affiliate of the high-profile ransomware-as-a-service (RaaS) group BlackByte is using a custom tool to exfiltrate files from a victim's network, a key step in the fast-growing business of double-extortion.... [...]
