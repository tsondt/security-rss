Title: Russians dodging mobilization behind flourishing scam market
Date: 2022-10-02T11:07:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-02-russians-dodging-mobilization-behind-flourishing-scam-market

[Source](https://www.bleepingcomputer.com/news/security/russians-dodging-mobilization-behind-flourishing-scam-market/){:target="_blank" rel="noopener"}

> Ever since Russian president Vladimir Putin ordered partial mobilization after facing setbacks on the Ukrainian front, men in Russia and the state's conscript officers are playing a 'cat and mouse' game involving technology and cybercrime services. [...]
