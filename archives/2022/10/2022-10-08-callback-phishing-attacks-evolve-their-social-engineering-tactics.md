Title: Callback phishing attacks evolve their social engineering tactics
Date: 2022-10-08T10:11:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-08-callback-phishing-attacks-evolve-their-social-engineering-tactics

[Source](https://www.bleepingcomputer.com/news/security/callback-phishing-attacks-evolve-their-social-engineering-tactics/){:target="_blank" rel="noopener"}

> The BazarCall malicious operation has evolved its social engineering methods, keeping the old fake charges lure for the first phase of the attack but then switching to pretending to help the victim deal with an infection or hack. [...]
