Title: Adobe patches critical Magento XSS that puts sites at takeover risk
Date: 2022-10-14T11:11:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-14-adobe-patches-critical-magento-xss-that-puts-sites-at-takeover-risk

[Source](https://portswigger.net/daily-swig/adobe-patches-critical-magento-xss-that-puts-sites-at-takeover-risk){:target="_blank" rel="noopener"}

> E-commerce platform admins should update ASAP [...]
