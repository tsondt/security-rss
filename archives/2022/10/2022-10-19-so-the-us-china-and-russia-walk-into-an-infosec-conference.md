Title: So, the US, China, and Russia walk into an infosec conference
Date: 2022-10-19T14:30:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-10-19-so-the-us-china-and-russia-walk-into-an-infosec-conference

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/19/singapore_international_cyber_week/){:target="_blank" rel="noopener"}

> Suffice to say things got a little awkward Cyber-diplomats from around the world say they want the internet to be safe, secure, and free of interference. Of course, they believe it's the fault of other nations that the internet is not safe, secure or free of interference.... [...]
