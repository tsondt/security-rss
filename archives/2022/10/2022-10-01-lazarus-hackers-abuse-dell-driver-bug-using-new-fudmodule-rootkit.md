Title: Lazarus hackers abuse Dell driver bug using new FudModule rootkit
Date: 2022-10-01T10:05:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-01-lazarus-hackers-abuse-dell-driver-bug-using-new-fudmodule-rootkit

[Source](https://www.bleepingcomputer.com/news/security/lazarus-hackers-abuse-dell-driver-bug-using-new-fudmodule-rootkit/){:target="_blank" rel="noopener"}

> The notorious North Korean hacking group 'Lazarus' was seen installing a Windows rootkit that abuses a Dell hardware driver in a Bring Your Own Vulnerable Driver attack. [...]
