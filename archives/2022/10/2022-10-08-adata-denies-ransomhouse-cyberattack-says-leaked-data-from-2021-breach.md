Title: ADATA denies RansomHouse cyberattack, says leaked data from 2021 breach
Date: 2022-10-08T11:18:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-10-08-adata-denies-ransomhouse-cyberattack-says-leaked-data-from-2021-breach

[Source](https://www.bleepingcomputer.com/news/security/adata-denies-ransomhouse-cyberattack-says-leaked-data-from-2021-breach/){:target="_blank" rel="noopener"}

> Taiwanese chip maker ADATA denies claims of a RansomHouse cyberattack after the threat actors began posting the company's stolen files on their data leak site. [...]
