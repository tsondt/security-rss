Title: Researchers find 633% increase in cyber-attacks aimed at open source repositories
Date: 2022-10-18T15:21:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-18-researchers-find-633-increase-in-cyber-attacks-aimed-at-open-source-repositories

[Source](https://portswigger.net/daily-swig/researchers-find-633-increase-in-cyber-attacks-aimed-at-open-source-repositories){:target="_blank" rel="noopener"}

> Attack surge blamed on ‘avoidable’ bugs [...]
