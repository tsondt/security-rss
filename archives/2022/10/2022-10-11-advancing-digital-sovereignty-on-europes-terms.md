Title: Advancing digital sovereignty on Europe's terms
Date: 2022-10-11T12:04:00+00:00
Author: Adaire Fox-Martin
Category: GCP Security
Tags: Google Cloud;Next;Identity & Security
Slug: 2022-10-11-advancing-digital-sovereignty-on-europes-terms

[Source](https://cloud.google.com/blog/products/identity-security/advancing-digital-sovereignty-on-europes-terms/){:target="_blank" rel="noopener"}

> In September 2021, we unveiled “ Cloud. On Europe’s Terms,” an ambitious commitment to deliver cloud services that provide the highest levels of digital sovereignty while enabling the next wave of growth and transformation for European organizations. We’ve since seen increasing demand from customers and policymakers for digital sovereignty solutions. Working closely with our European customers, partners, policy makers, and governments, today at Google Cloud Next we’re pleased to share that we’ve delivered and are continuing to develop a broad portfolio of Sovereign Solutions that can support European customers’ current and emerging sovereignty needs as they progress their digital transformation. [...]
