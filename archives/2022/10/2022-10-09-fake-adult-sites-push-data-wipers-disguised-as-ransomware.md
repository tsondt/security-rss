Title: Fake adult sites push data wipers disguised as ransomware
Date: 2022-10-09T10:16:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-09-fake-adult-sites-push-data-wipers-disguised-as-ransomware

[Source](https://www.bleepingcomputer.com/news/security/fake-adult-sites-push-data-wipers-disguised-as-ransomware/){:target="_blank" rel="noopener"}

> Malicious adult websites push fake ransomware which, in reality, acts as a wiper that quietly tries to delete almost all of the data on your device. [...]
