Title: City of Tucson discloses data breach affecting over 125,000 people
Date: 2022-10-05T13:21:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-10-05-city-of-tucson-discloses-data-breach-affecting-over-125000-people

[Source](https://www.bleepingcomputer.com/news/security/city-of-tucson-discloses-data-breach-affecting-over-125-000-people/){:target="_blank" rel="noopener"}

> The City of Tucson, Arizona, has disclosed a data breach affecting the personal information of more than 125,000 individuals. [...]
