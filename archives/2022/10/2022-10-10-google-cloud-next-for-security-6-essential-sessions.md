Title: Google Cloud Next for security: 6 essential sessions
Date: 2022-10-10T16:00:00+00:00
Author: Google Cloud Content & Editorial
Category: GCP Security
Tags: Next;Google Cloud;Identity & Security
Slug: 2022-10-10-google-cloud-next-for-security-6-essential-sessions

[Source](https://cloud.google.com/blog/products/identity-security/security-breakout-sessions-at-google-cloud-next22/){:target="_blank" rel="noopener"}

> Zero Trust. Securing the software supply chain. Policy controls. And what about Mandiant? We have so many cool and interesting security sessions coming this week at Google Cloud Next, it can be hard to know where to start. Fortunately, we’ve got you covered with these six must-watch security sessions to add to your playlist : 1. SEC101 What’s next for security professionals? Kick off your Next Security journey with Mandiant’s Sandra Joyce, Charles Schwab’s Bashar Abouseido, and our own Sunil Potti on Google Cloud’s vision for security. Here you’ll get a first look at many of our latest innovations, and [...]
