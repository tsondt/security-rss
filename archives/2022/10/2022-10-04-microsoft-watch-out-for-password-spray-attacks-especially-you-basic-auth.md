Title: Microsoft: Watch out for password spray attacks – especially you, Basic Auth
Date: 2022-10-04T16:15:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-04-microsoft-watch-out-for-password-spray-attacks-especially-you-basic-auth

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/04/microsoft_exchange_password_spray/){:target="_blank" rel="noopener"}

> Exchange Online users should have authentication policies in place Microsoft is warning Exchange Online users about a rise in password spray attacks, urging those that have yet to disable Basic Authentication to at least set up authentication policies to protect their users and data.... [...]
