Title: Apple Only Commits to Patching Latest OS Version
Date: 2022-10-31T11:29:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;iOS;patching
Slug: 2022-10-31-apple-only-commits-to-patching-latest-os-version

[Source](https://www.schneier.com/blog/archives/2022/10/apple-only-commits-to-patching-latest-os-version.html){:target="_blank" rel="noopener"}

> People have suspected this for a while, but Apple has made it official. It only commits to fully patching the latest version of its OS, even though it claims to support older versions. From ArsTechnica : In other words, while Apple will provide security-related updates for older versions of its operating systems, only the most recent upgrades will receive updates for every security problem Apple knows about. Apple currently provides security updates to macOS 11 Big Sur and macOS 12 Monterey alongside the newly released macOS Ventura, and in the past, it has released security updates for older iOS versions [...]
