Title: BlueBleed: Microsoft customer data leak claimed to be 'one of the largest' in years
Date: 2022-10-20T15:00:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-20-bluebleed-microsoft-customer-data-leak-claimed-to-be-one-of-the-largest-in-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/20/microsoft_data_leak_socradar/){:target="_blank" rel="noopener"}

> SOCRadar says sensitive info from 150,000 orgs was exposed, Redmond disputes findings Microsoft has confirmed one of its own misconfigured cloud systems led to customer information being exposed to the internet, though it disputes the extent of the leak.... [...]
