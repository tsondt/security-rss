Title: Toyota discloses data leak after access key exposed on GitHub
Date: 2022-10-10T13:50:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-10-toyota-discloses-data-leak-after-access-key-exposed-on-github

[Source](https://www.bleepingcomputer.com/news/security/toyota-discloses-data-leak-after-access-key-exposed-on-github/){:target="_blank" rel="noopener"}

> Toyota Motor Corporation is warning that customers' personal information may have been exposed after an access key was publicly available on GitHub for almost five years. [...]
