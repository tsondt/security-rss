Title: A real estate agent data breach would be devastating for renters. They collect too much personal information | Samantha Floreani
Date: 2022-10-03T16:30:12+00:00
Author: Samantha Floreani
Category: The Guardian
Tags: Data and computer security;Housing;Australia news
Slug: 2022-10-03-a-real-estate-agent-data-breach-would-be-devastating-for-renters-they-collect-too-much-personal-information-samantha-floreani

[Source](https://www.theguardian.com/commentisfree/2022/oct/04/telcos-arent-alone-in-collecting-too-much-of-our-personal-information){:target="_blank" rel="noopener"}

> Does a breach need to happen before we see regulatory change? Thanks to Optus, millions of people are now acutely aware of what can happen when companies don’t take privacy and security seriously. But telcos aren’t alone in collecting and storing too much of our personal information. The real estate industry is often overlooked in conversations about data security, but it is one of the most invasive, with potentially devastating consequences for renters across the country. If you’ve ever been a renter, this is probably a familiar story: you’re searching for somewhere to live, rents are high, competition is stiff, [...]
