Title: How Chrome supports today’s workforce with secure enterprise browsing
Date: 2022-10-05T16:00:00+00:00
Author: Parisa Tabriz
Category: GCP Security
Tags: Identity & Security;Chrome Enterprise
Slug: 2022-10-05-how-chrome-supports-todays-workforce-with-secure-enterprise-browsing

[Source](https://cloud.google.com/blog/products/chrome-enterprise/how-chrome-supports-todays-workforce-secure-enterprise-browsing/){:target="_blank" rel="noopener"}

> When I first started at Google 15 years ago, I was brought on as a “ hired hacker ” on Google’s security team. I hunted for software vulnerabilities and helped product teams fix and mitigate them. Over the years, I’ve worked on initiatives to ensure security is built-in by default and user-friendly, to detect and mitigate exploits, and to advance security across the broader tech ecosystem, such as our recently announced Open Source Software Vulnerability Rewards Program. As you might expect, security has a very special place in my heart, and now that I’m leading the Chrome browser team, security [...]
