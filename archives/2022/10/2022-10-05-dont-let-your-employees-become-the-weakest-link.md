Title: Don’t let your employees become the weakest link
Date: 2022-10-05T12:59:10+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-10-05-dont-let-your-employees-become-the-weakest-link

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/05/watch_our_webinar_to_learn/){:target="_blank" rel="noopener"}

> Watch our webinar to learn the best way to keep data protected from human error Webinar "You are the weakest link, goodbye!". One of the most famous catchphrases in television history. Popularized by the BBC gameshow and delivered by caustic TV presenter Anne Robinson, it is still the ultimate put down.... [...]
