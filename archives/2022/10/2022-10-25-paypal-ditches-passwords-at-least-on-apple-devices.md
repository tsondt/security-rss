Title: PayPal ditches passwords, at least on Apple devices
Date: 2022-10-25T19:30:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-25-paypal-ditches-passwords-at-least-on-apple-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/25/paypal_ditches_passwords/){:target="_blank" rel="noopener"}

> No more reusing, recycling passwords PayPal has added passkeys for passwordless login to accounts across Apple devices.... [...]
