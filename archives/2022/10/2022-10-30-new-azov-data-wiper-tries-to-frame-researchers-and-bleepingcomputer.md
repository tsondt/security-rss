Title: New Azov data wiper tries to frame researchers and BleepingComputer
Date: 2022-10-30T20:26:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-10-30-new-azov-data-wiper-tries-to-frame-researchers-and-bleepingcomputer

[Source](https://www.bleepingcomputer.com/news/security/new-azov-data-wiper-tries-to-frame-researchers-and-bleepingcomputer/){:target="_blank" rel="noopener"}

> A new and destructive 'Azov Ransomware' data wiper is being heavily distributed through pirated software, key generators, and adware bundles, trying to frame well-known security researchers by claiming they are behind the attack. [...]
