Title: Complex Impersonation Story
Date: 2022-10-10T11:09:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;fraud;impersonation;scams
Slug: 2022-10-10-complex-impersonation-story

[Source](https://www.schneier.com/blog/archives/2022/10/complex-impersonation-story.html){:target="_blank" rel="noopener"}

> This is a story of one piece of what is probably a complex employment scam. Basically, real programmers are having their resumes copied and co-opted by scammers, who apply for jobs (or, I suppose, get recruited from various job sites), then hire other people with Western looks and language skills are to impersonate those first people on Zoom job interviews. Presumably, sometimes the scammers get hired and...I suppose...collect paychecks for a while until they get found out and fired. But that requires a bunch of banking fraud as well, so I don’t know. [...]
