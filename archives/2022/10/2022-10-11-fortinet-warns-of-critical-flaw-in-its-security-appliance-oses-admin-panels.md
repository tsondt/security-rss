Title: Fortinet warns of critical flaw in its security appliance OSes, admin panels
Date: 2022-10-11T10:32:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-10-11-fortinet-warns-of-critical-flaw-in-its-security-appliance-oses-admin-panels

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/11/fortinet_critical_flaw/){:target="_blank" rel="noopener"}

> Naturally, they're already under attack – so you know what to do next Security appliance vendor Fortinet has become the subject of a bug report by its own FortiGuard Labs after the discovery of a critical-rated flaw in three of its products.... [...]
