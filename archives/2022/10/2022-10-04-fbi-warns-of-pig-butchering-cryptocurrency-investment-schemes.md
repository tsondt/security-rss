Title: FBI warns of "Pig Butchering" cryptocurrency investment schemes
Date: 2022-10-04T09:59:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-10-04-fbi-warns-of-pig-butchering-cryptocurrency-investment-schemes

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-pig-butchering-cryptocurrency-investment-schemes/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warns of a rise in 'Pig Butchering' cryptocurrency scams used to steal ever-increasing amounts of crypto from unsuspecting investors. [...]
