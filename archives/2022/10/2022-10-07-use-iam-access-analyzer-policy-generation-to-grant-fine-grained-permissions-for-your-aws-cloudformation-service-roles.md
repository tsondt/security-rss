Title: Use IAM Access Analyzer policy generation to grant fine-grained permissions for your AWS CloudFormation service roles
Date: 2022-10-07T19:19:32+00:00
Author: Joel Knight
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Access management;AWS CloudTrail;AWS IAM;AWS Lambda;least privilege;Policies;Security;Security Blog
Slug: 2022-10-07-use-iam-access-analyzer-policy-generation-to-grant-fine-grained-permissions-for-your-aws-cloudformation-service-roles

[Source](https://aws.amazon.com/blogs/security/use-iam-access-analyzer-policy-generation-to-grant-fine-grained-permissions-for-your-aws-cloudformation-service-roles/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) Access Analyzer provides tools to simplify permissions management by making it simpler for you to set, verify, and refine permissions. One such tool is IAM Access Analyzer policy generation, which creates fine-grained policies based on your AWS CloudTrail access activity—for example, the actions you use with Amazon Elastic Compute [...]
