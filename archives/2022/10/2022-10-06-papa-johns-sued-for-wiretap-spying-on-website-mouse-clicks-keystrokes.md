Title: Papa John's sued for 'wiretap' spying on website mouse clicks, keystrokes
Date: 2022-10-06T20:20:49+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-10-06-papa-johns-sued-for-wiretap-spying-on-website-mouse-clicks-keystrokes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/06/papa_johns_spying_lawsuit/){:target="_blank" rel="noopener"}

> When the tracking hits your eye like a big pizza pie, that's a priori Papa John's is being sued by a customer – not for its pizza but for allegedly breaking the US Wiretap Act by snooping on the way he browsed the pie-slinger's website.... [...]
