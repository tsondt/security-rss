Title: Pro-China crew ramps up disinfo ahead of US midterms. Not that anyone's falling for it
Date: 2022-10-27T00:31:44+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-10-27-pro-china-crew-ramps-up-disinfo-ahead-of-us-midterms-not-that-anyones-falling-for-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/27/chinese_misinformation_dragonbridge_elections/){:target="_blank" rel="noopener"}

> Hey, Xi, 滚开 The prolific pro-Beijing Dragonbridge crew has apparently stepped up its activity ahead of the US 2022 midterms by trying to discourage Americans from voting as well as pinning the Nord Stream pipeline explosion on Uncle Sam.... [...]
