Title: EnergyAustralia latest to be hit by cyber-attack as details of hundreds of customers exposed
Date: 2022-10-21T08:40:05+00:00
Author: Mostafa Rachwani
Category: The Guardian
Tags: Australia news;Data and computer security;Cybercrime
Slug: 2022-10-21-energyaustralia-latest-to-be-hit-by-cyber-attack-as-details-of-hundreds-of-customers-exposed

[Source](https://www.theguardian.com/australia-news/2022/oct/21/energyaustralia-latest-to-be-hit-by-cyber-attack-as-details-of-hundreds-of-customers-exposed){:target="_blank" rel="noopener"}

> Electricity company says attack accessed information on 323 customers but ‘no evidence’ data was transferred elsewhere Get our morning and afternoon news emails, free app or daily news podcast EnergyAustralia has become the latest company to be targeted by a cyber-attack, with hundreds of customers’ details exposed. In a statement released late on Friday, the electricity company said 323 residential and small business customers were affected by unauthorised access to their online platform, My Account. Sign up for our free morning newsletter and afternoon email to get your daily news roundup Continue reading... [...]
