Title: Glut of Fake LinkedIn Profiles Pits HR Against the Bots
Date: 2022-10-05T21:20:53+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Web Fraud 2.0;Hamish Taylor;ISOutsource;Jason Lathrop;Linkedin bots;Mark Miller
Slug: 2022-10-05-glut-of-fake-linkedin-profiles-pits-hr-against-the-bots

[Source](https://krebsonsecurity.com/2022/10/glut-of-fake-linkedin-profiles-pits-hr-against-the-bots/){:target="_blank" rel="noopener"}

> A recent proliferation of phony executive profiles on LinkedIn is creating something of an identity crisis for the business networking site, and for companies that rely on it to hire and screen prospective employees. The fabricated LinkedIn identities — which pair AI-generated profile photos with text lifted from legitimate accounts — are creating major headaches for corporate HR departments and for those managing invite-only LinkedIn groups. Some of the fake profiles flagged by the co-administrator of a popular sustainability group on LinkedIn. Last week, KrebsOnSecurity examined a flood of inauthentic LinkedIn profiles all claiming Chief Information Security Officer (CISO) roles [...]
