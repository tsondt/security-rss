Title: ConnectWise fixes RCE bug exposing thousands of servers to attacks
Date: 2022-10-28T18:30:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-10-28-connectwise-fixes-rce-bug-exposing-thousands-of-servers-to-attacks

[Source](https://www.bleepingcomputer.com/news/security/connectwise-fixes-rce-bug-exposing-thousands-of-servers-to-attacks/){:target="_blank" rel="noopener"}

> ConnectWise has released security updates to address a critical vulnerability in the ConnectWise Recover and R1Soft Server Backup Manager (SBM) secure backup solutions. [...]
