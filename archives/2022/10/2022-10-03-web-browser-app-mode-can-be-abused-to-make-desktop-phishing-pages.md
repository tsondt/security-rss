Title: Web browser app mode can be abused to make desktop phishing pages
Date: 2022-10-03T12:35:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-10-03-web-browser-app-mode-can-be-abused-to-make-desktop-phishing-pages

[Source](https://www.bleepingcomputer.com/news/security/web-browser-app-mode-can-be-abused-to-make-desktop-phishing-pages/){:target="_blank" rel="noopener"}

> The app mode in Chromium-based browsers like Google Chrome and Microsoft Edge can be abused to create realistic-looking login screens that appear as desktop apps. [...]
