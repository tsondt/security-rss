Title: How Card Skimming Disproportionally Affects Those Most In Need
Date: 2022-10-19T01:28:46+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: All About Skimmers
Slug: 2022-10-19-how-card-skimming-disproportionally-affects-those-most-in-need

[Source](https://krebsonsecurity.com/2022/10/how-card-skimming-disproportionally-affects-those-most-in-need/){:target="_blank" rel="noopener"}

> When people banking in the United States lose money because their payment card got skimmed at an ATM, gas pump or grocery store checkout terminal, they may face hassles or delays in recovering any lost funds, but they are almost always made whole by their financial institution. Yet, one class of Americans — those receiving food assistance benefits via state-issued prepaid debit cards — are particularly exposed to losses from skimming scams, and usually have little recourse to do anything about it. California’s EBT card does not currently include a chip. That silver square is a hologram. Over the past [...]
