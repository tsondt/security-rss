Title: Top of the Pops: US authorities list the 20 hottest vulns that China's hackers love to hit
Date: 2022-10-07T05:28:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-10-07-top-of-the-pops-us-authorities-list-the-20-hottest-vulns-that-chinas-hackers-love-to-hit

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/07/us_spooks_reckon_these_are/){:target="_blank" rel="noopener"}

> Microsoft has four entries on list of shame, Log4j tops the chart Three US national security agencies - CISA, the FBI and the NSA - on Thursday issued a joint advisory naming the 20 infosec exploited by state-sponsored Chinese threat actors since 2020.... [...]
