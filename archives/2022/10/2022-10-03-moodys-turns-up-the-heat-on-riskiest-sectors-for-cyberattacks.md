Title: Moody's turns up the heat on 'riskiest' sectors for cyberattacks
Date: 2022-10-03T06:33:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-03-moodys-turns-up-the-heat-on-riskiest-sectors-for-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/03/moodys_cyber_risk_ratings/){:target="_blank" rel="noopener"}

> $22 trillion of global rated debt has 'high' or 'very high' cyber-risk exposure About $22 trillion of global debt rated by Moody's Investors Service has "high," or "very high" cyber-risk exposure, with electric, gas and water utilities, as well as hospitals, among the sectors facing the highest risk of cyberattacks.... [...]
