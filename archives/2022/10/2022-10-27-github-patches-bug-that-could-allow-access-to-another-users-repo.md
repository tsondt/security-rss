Title: GitHub patches bug that could allow access to another user’s repo
Date: 2022-10-27T14:15:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-10-27-github-patches-bug-that-could-allow-access-to-another-users-repo

[Source](https://portswigger.net/daily-swig/github-patches-bug-that-could-allow-access-to-another-users-repo){:target="_blank" rel="noopener"}

> Renaming accounts opened the door to hijacking [...]
