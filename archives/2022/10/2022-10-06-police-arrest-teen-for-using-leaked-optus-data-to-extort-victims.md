Title: Police arrest teen for using leaked Optus data to extort victims
Date: 2022-10-06T02:44:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-10-06-police-arrest-teen-for-using-leaked-optus-data-to-extort-victims

[Source](https://www.bleepingcomputer.com/news/security/police-arrest-teen-for-using-leaked-optus-data-to-extort-victims/){:target="_blank" rel="noopener"}

> The AFP (Australian Federal Police) have arrested a 19-year-old man in Sydney and charged him for allegedly using leaked Optus customer data for extortion. [...]
