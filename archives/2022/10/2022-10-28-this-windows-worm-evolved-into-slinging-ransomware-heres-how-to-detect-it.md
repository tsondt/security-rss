Title: This Windows worm evolved into slinging ransomware. Here's how to detect it
Date: 2022-10-28T22:11:04+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-28-this-windows-worm-evolved-into-slinging-ransomware-heres-how-to-detect-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/28/microsoft_raspberry_robin_malware/){:target="_blank" rel="noopener"}

> Raspberry Robin hits 1,000 orgs in just one month Raspberry Robin, a worm that spreads through Windows systems via USB drives, has rapidly evolved: now backdoor access is being sold or offered to infected machines so that ransomware, among other code, can be installed by cybercriminals.... [...]
