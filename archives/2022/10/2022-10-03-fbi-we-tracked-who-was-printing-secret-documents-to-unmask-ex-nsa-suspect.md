Title: FBI: We tracked who was printing secret documents to unmask ex-NSA suspect
Date: 2022-10-03T17:00:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-10-03-fbi-we-tracked-who-was-printing-secret-documents-to-unmask-ex-nsa-suspect

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/03/nsa_worker_fbi_espionage/){:target="_blank" rel="noopener"}

> Infosec systems designer alleged to have chatted with undercover agent A 30-year-old ex-NSA employee was accused by the FBI of trying to sell classified US information to a foreign government – after the Feds said they linked him to the printing of secret documents.... [...]
