Title: Ransomware gang leaks data stolen from LAUSD school system
Date: 2022-10-02T17:51:39-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-10-02-ransomware-gang-leaks-data-stolen-from-lausd-school-system

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-leaks-data-stolen-from-lausd-school-system/){:target="_blank" rel="noopener"}

> The Vice Society Ransomware gang published data and documents Sunday morning that were stolen from the Los Angeles Unified School District during a cyberattack earlier this month. [...]
