Title: That thing to help protect internet traffic from hijacking? It's broken
Date: 2022-10-09T19:31:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-10-09-that-thing-to-help-protect-internet-traffic-from-hijacking-its-broken

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/09/internet_traffic_routing_defense/){:target="_blank" rel="noopener"}

> RPKI is supposed to verify network routes. Instead, here's how it could be subverted An internet security mechanism called Resource Public Key Infrastructure (RPKI), intended to safeguard the routing of data traffic, is broken, according to security experts from Germany's ATHENE, the National Research Center for Applied Cybersecurity.... [...]
