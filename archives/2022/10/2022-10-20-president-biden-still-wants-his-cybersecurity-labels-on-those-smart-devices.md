Title: President Biden still wants his cybersecurity labels on those smart devices
Date: 2022-10-20T09:30:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-10-20-president-biden-still-wants-his-cybersecurity-labels-on-those-smart-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/20/biden_administration_iot_security_labels/){:target="_blank" rel="noopener"}

> May follow Finland and Germany in adopting Singapore's standard The Biden administration is pushing ahead with its drive to add cyber security labeling to consumer Internet of Things (IoT) devices, and may join other nations in adopting the scheme pioneered by Singapore.... [...]
