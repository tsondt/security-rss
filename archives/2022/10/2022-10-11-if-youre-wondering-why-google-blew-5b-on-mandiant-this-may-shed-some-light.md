Title: If you're wondering why Google blew $5b on Mandiant, this may shed some light
Date: 2022-10-11T12:00:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-10-11-if-youre-wondering-why-google-blew-5b-on-mandiant-this-may-shed-some-light

[Source](https://go.theregister.com/feed/www.theregister.com/2022/10/11/google_mandiant_brain/){:target="_blank" rel="noopener"}

> Automating infosec knowhow, essentially Mandiant, now officially owned by Google, has the scale (not to mention the deep pockets) to be the "brain" across organizations' myriad security products and automate protection on top of these controls, according to the security shop's CEO Kevin Mandia.... [...]
