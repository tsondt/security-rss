Title: Devil-may-care Lapsus$ gang is not the aspirational brand infosec needs
Date: 2022-03-17T03:58:20+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-17-devil-may-care-lapsus-gang-is-not-the-aspirational-brand-infosec-needs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/17/lapsus-larger-companies/){:target="_blank" rel="noopener"}

> Hitting big targets, untouchable, technically proficient. Who will it inspire next? Analysis The Lapsus$ cyber-crime gang, believed to be based in Brazil, until recently was best known for attacks on that country's Ministry of Health and Portuguese media outlets SIC Noticias and Expresso.... [...]
