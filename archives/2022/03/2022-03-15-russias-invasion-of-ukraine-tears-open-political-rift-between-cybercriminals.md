Title: Russia's invasion of Ukraine tears open political rift between cybercriminals
Date: 2022-03-15T01:02:21+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-15-russias-invasion-of-ukraine-tears-open-political-rift-between-cybercriminals

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/cyberciminals_russia_ukraine/){:target="_blank" rel="noopener"}

> Is the West OK when the gun points the other way? Cybercriminals are taking sides over Russia's deadly invasion of Ukraine, putting either the West or Moscow in their sights, according to Accenture.... [...]
