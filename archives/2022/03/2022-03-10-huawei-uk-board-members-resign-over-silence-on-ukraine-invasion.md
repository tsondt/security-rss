Title: Huawei UK board members resign over silence on Ukraine invasion
Date: 2022-03-10T05:59:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-03-10-huawei-uk-board-members-resign-over-silence-on-ukraine-invasion

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/10/huawei_uk_board_members_resign/){:target="_blank" rel="noopener"}

> Knights depart Chinese mega-corp's round table Updated Multiple reports have surfaced claiming that two members of Huawei's UK board have resigned over the company's stance – or lack thereof – on Russia's illegal invasion of the Ukraine.... [...]
