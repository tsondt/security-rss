Title: Ukrainian ISP used by military disrupted by ‘powerful’ cyber-attack
Date: 2022-03-29T10:33:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-29-ukrainian-isp-used-by-military-disrupted-by-powerful-cyber-attack

[Source](https://portswigger.net/daily-swig/ukrainian-isp-used-by-military-disrupted-by-powerful-cyber-attack){:target="_blank" rel="noopener"}

> Russia blamed after internet knocked offline for many across the country [...]
