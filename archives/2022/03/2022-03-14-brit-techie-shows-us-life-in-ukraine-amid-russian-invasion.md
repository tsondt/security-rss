Title: Brit techie shows us life in Ukraine amid Russian invasion
Date: 2022-03-14T11:15:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-14-brit-techie-shows-us-life-in-ukraine-amid-russian-invasion

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/14/lviv_ukraine_pics_from_ground/){:target="_blank" rel="noopener"}

> Martial law, no booze sales, big queues for trains westwards Pics British infosec pro Vic Harkness traveled to Ukraine to offer humanitarian help – and while taking a break in the western city of Lviv she described to The Register what it's like in the war-torn country.... [...]
