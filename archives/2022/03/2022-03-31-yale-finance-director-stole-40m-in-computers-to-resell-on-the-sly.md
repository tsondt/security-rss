Title: Yale finance director stole $40m in computers to resell on the sly
Date: 2022-03-31T01:28:45+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-03-31-yale-finance-director-stole-40m-in-computers-to-resell-on-the-sly

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/31/yale_electronics_fraud/){:target="_blank" rel="noopener"}

> Ill-gotten gains bankrolled swish life of flash cars and real estate A now-former finance director stole tablet computers and other equipment worth $40 million from the Yale University School of Medicine, and resold them for a profit.... [...]
