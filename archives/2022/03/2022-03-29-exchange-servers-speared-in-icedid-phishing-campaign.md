Title: Exchange Servers Speared in IcedID Phishing Campaign
Date: 2022-03-29T14:02:41+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-03-29-exchange-servers-speared-in-icedid-phishing-campaign

[Source](https://threatpost.com/exchange-servers-speared-in-icedid-phishing-campaign/179137/){:target="_blank" rel="noopener"}

> The ever-evolving malware shows off new tactics that use email thread hijacking and other obfuscation techniques to provide advanced evasion techniques. [...]
