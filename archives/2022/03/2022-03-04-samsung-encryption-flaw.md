Title: Samsung Encryption Flaw
Date: 2022-03-04T12:19:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;Android;cryptanalysis;cryptography;encryption;Samsung;smartphones;vulnerabilities
Slug: 2022-03-04-samsung-encryption-flaw

[Source](https://www.schneier.com/blog/archives/2022/03/samsung-encryption-flaw.html){:target="_blank" rel="noopener"}

> Researchers have found a major encryption flaw in 100 million Samsung Galaxy phones. From the abstract: In this work, we expose the cryptographic design and implementation of Android’s Hardware-Backed Keystore in Samsung’s Galaxy S8, S9, S10, S20, and S21 flagship devices. We reversed-engineered and provide a detailed description of the cryptographic design and code structure, and we unveil severe design flaws. We present an IV reuse attack on AES-GCM that allows an attacker to extract hardware-protected key material, and a downgrade attack that makes even the latest Samsung devices vulnerable to the IV reuse attack. We demonstrate working key extraction [...]
