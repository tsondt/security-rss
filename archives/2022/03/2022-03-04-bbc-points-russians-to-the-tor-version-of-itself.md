Title: BBC points Russians to the Tor version of itself
Date: 2022-03-04T15:30:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-04-bbc-points-russians-to-the-tor-version-of-itself

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/04/russia_splinternet_tor_rumours/){:target="_blank" rel="noopener"}

> Back to the future with short wave radio, plus Russia drop internet Iron Curtain Russia has reportedly blocked access to Western media outlets including the BBC to netizens within its borders, as suspicions rise that the country has begun implementing a "splinternet" plan to seal itself off from the wider internet.... [...]
