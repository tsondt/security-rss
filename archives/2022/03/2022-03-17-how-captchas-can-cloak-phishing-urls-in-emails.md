Title: How CAPTCHAs can cloak phishing URLs in emails
Date: 2022-03-17T13:00:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-03-17-how-captchas-can-cloak-phishing-urls-in-emails

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/17/captcha_phishinbg_url/){:target="_blank" rel="noopener"}

> Select all images of you being duped into providing your credentials CAPTCHA puzzles, designed to distinguish people from computer code, are being used to separate people from their login credentials.... [...]
