Title: CrowdStrike offers fully managed identity-threat-detection-as-a-service
Date: 2022-03-03T00:39:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-03-crowdstrike-offers-fully-managed-identity-threat-detection-as-a-service

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/03/crowdstrike-identity-threat-service/){:target="_blank" rel="noopener"}

> The further you move from the office, the more wild the product descriptions CrowdStrike is bringing its identity threat prevention technology to its managed detection and response (MDR) service, giving enterprises a chance to blunt the growing threat of identity-based attacks that has accelerated during the COVID-19 pandemic.... [...]
