Title: Insurance giant Aon confirms it has suffered 'cyber incident'
Date: 2022-03-01T14:13:50+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-03-01-insurance-giant-aon-confirms-it-has-suffered-cyber-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/01/aon_cyber_incident/){:target="_blank" rel="noopener"}

> Oh the irony! Insurance companies, even those selling cyber insurance, are attack targets Aon, the British-American provider of insurance and pension administration, has brought in external specialists to help probe a "cyber incident".... [...]
