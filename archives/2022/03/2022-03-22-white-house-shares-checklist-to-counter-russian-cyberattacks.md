Title: White House shares checklist to counter Russian cyberattacks
Date: 2022-03-22T19:03:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-03-22-white-house-shares-checklist-to-counter-russian-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/white-house-shares-checklist-to-counter-russian-cyberattacks/){:target="_blank" rel="noopener"}

> The White House is urging U.S. organizations to shore up their cybersecurity defenses after new intelligence suggests that Russia is preparing to conduct cyberattacks in the near future. [...]
