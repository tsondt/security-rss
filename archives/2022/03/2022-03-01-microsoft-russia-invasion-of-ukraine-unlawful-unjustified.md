Title: Microsoft: Russia invasion of Ukraine ‘unlawful, unjustified’
Date: 2022-03-01T00:00:56+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-01-microsoft-russia-invasion-of-ukraine-unlawful-unjustified

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/01/microsoft-russia-ukraine/){:target="_blank" rel="noopener"}

> Windows giant says it detected 'destructive cyberattacks', DDoS malware aimed at now occupied nation Microsoft is decrying what it calls the "tragic, unlawful and unjustified invasion of Ukraine" by Russia, and vowed to continue protecting the country from cyberattacks and state-sponsored disinformation campaigns.... [...]
