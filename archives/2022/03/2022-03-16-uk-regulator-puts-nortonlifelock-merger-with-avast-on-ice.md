Title: UK regulator puts NortonLifeLock merger with Avast on ice
Date: 2022-03-16T11:33:47+00:00
Author: Richard Currie
Category: The Register
Tags: 
Slug: 2022-03-16-uk-regulator-puts-nortonlifelock-merger-with-avast-on-ice

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/16/cma_nortonlifelock_avast/){:target="_blank" rel="noopener"}

> Security vendors now have 5 working days to explain to the Competition and Markets Authority why it's wrong The UK Competition and Markets Authority (CMA) merger inquiry into NortonLifeLock's proposed $8bn acquisition of rival antivirus provider Avast has now closed, with the regulator concluding that a tie-up could indeed reduce competition in the marketplace.... [...]
