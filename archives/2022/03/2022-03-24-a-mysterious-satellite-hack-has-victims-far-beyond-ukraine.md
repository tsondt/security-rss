Title: A mysterious satellite hack has victims far beyond Ukraine
Date: 2022-03-24T14:20:17+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;broadband;hacking;Internet;russian invasion;satellite broadband;satellite Internet;Ukraine
Slug: 2022-03-24-a-mysterious-satellite-hack-has-victims-far-beyond-ukraine

[Source](https://arstechnica.com/?p=1843265){:target="_blank" rel="noopener"}

> Enlarge (credit: bjdlzx | Getty Images) More than 22,000 miles above Earth, the KA-SAT is locked in orbit. Traveling at 7,000 miles per hour, in sync with the planet’s rotation, the satellite beams high-speed Internet down to people across Europe. Since 2011, it has helped homeowners, businesses, and militaries get online. However, as Russian troops moved into Ukraine during the early hours of February 24, satellite Internet connections were disrupted. A mysterious cyberattack against the satellite’s ground infrastructure—not the satellite itself—plunged tens of thousands of people into Internet darkness. Among them were parts of Ukraine’s defenses. “It was a really [...]
