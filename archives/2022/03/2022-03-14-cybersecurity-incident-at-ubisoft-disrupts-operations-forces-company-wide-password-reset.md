Title: ‘Cybersecurity incident’ at Ubisoft disrupts operations, forces company-wide password reset
Date: 2022-03-14T13:46:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-14-cybersecurity-incident-at-ubisoft-disrupts-operations-forces-company-wide-password-reset

[Source](https://portswigger.net/daily-swig/cybersecurity-incident-at-ubisoft-disrupts-operations-forces-company-wide-password-reset){:target="_blank" rel="noopener"}

> Lapsu$ threat actors have been linked to the cyber-attack [...]
