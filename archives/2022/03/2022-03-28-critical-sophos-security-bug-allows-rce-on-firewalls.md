Title: Critical Sophos Security Bug Allows RCE on Firewalls
Date: 2022-03-28T17:33:43+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-03-28-critical-sophos-security-bug-allows-rce-on-firewalls

[Source](https://threatpost.com/critical-sophos-security-bug-rce-firewalls/179127/){:target="_blank" rel="noopener"}

> The security vendor's appliance suffers from an authentication-bypass issue. [...]
