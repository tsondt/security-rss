Title: Zoom agrees privacy conditions, gets low-risk rating from Netherlands
Date: 2022-03-21T12:30:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-03-21-zoom-agrees-privacy-conditions-gets-low-risk-rating-from-netherlands

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/21/zoom_dpia/){:target="_blank" rel="noopener"}

> Warn users there's no E2EE when using it via the browser, DPIA tells institutions Hot on the heels of Microsoft's report card from the Dutch department of Justice and Security comes news of rival messaging platform Zoom receiving a nod via a renewed Data Protection Impact Assessment (DPIA).... [...]
