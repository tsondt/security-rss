Title: Lapsus$ extortionists dump data online as Samsung admits breach
Date: 2022-03-07T17:11:51+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-07-lapsus-extortionists-dump-data-online-as-samsung-admits-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/07/samsung_lapsus_data_theft/){:target="_blank" rel="noopener"}

> 190GB worth of internal files include 'some source codes relating to the operation of Galaxy devices' but Chaebol says customer data is safe Updated Samsung has acknowledged its data was stolen after the Lapsus$ extortion gang deposited what appears to be 190GB of the company's stolen internal files online.... [...]
