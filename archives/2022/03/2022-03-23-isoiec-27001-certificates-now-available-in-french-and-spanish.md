Title: ISO/IEC 27001 certificates now available in French and Spanish
Date: 2022-03-23T20:15:39+00:00
Author: Rodrigo Fiuza
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS ISO;AWS ISO Certificates;ISO 27001;Security Blog
Slug: 2022-03-23-isoiec-27001-certificates-now-available-in-french-and-spanish

[Source](https://aws.amazon.com/blogs/security/iso-iec-27001-certificates-now-available-in-french-and-spanish/){:target="_blank" rel="noopener"}

> French version Spanish version We continue to listen to our customers, regulators, and stakeholders to understand their needs regarding audit, assurance, certification, and attestation programs at Amazon Web Services (AWS). We are pleased to announce that ISO/IEC 27001 certificates for AWS are now available in French and Spanish on AWS Artifact. These translated reports will [...]
