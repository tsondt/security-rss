Title: <span>ENISA urges data-handling innovation amid growing tide of healthcare breaches</span>
Date: 2022-03-28T15:11:25+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-28-enisa-urges-data-handling-innovation-amid-growing-tide-of-healthcare-breaches

[Source](https://portswigger.net/daily-swig/enisa-urges-data-handling-innovation-amid-growing-tide-of-healthcare-breaches){:target="_blank" rel="noopener"}

> Infosec agency sets out use cases for clinical trials, data exchange, and connected devices [...]
