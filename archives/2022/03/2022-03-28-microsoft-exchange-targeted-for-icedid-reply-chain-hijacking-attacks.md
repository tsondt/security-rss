Title: Microsoft Exchange targeted for IcedID reply-chain hijacking attacks
Date: 2022-03-28T09:32:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-28-microsoft-exchange-targeted-for-icedid-reply-chain-hijacking-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-targeted-for-icedid-reply-chain-hijacking-attacks/){:target="_blank" rel="noopener"}

> The distribution of the IcedID malware has returned to notable numbers thanks to a new campaign that hijacks existing email conversations threads and injects payloads that are hard to spot as malicious. [...]
