Title: Skyhigh Security rises from McAfee-FireEye’s SSE
Date: 2022-03-22T04:01:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-22-skyhigh-security-rises-from-mcafee-fireeyes-sse

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/22/skyhigh_security_mcafee_fireeye_sse/){:target="_blank" rel="noopener"}

> CEO chats to us about zero trust, data protection, and more Skyhigh Security, formed from the Secure Service Edge (SSE) pieces of McAfee Enterprise and FireEye, today announced its name and data-guarding portfolio.... [...]
