Title: Developer Sabotages Open-Source Software Package
Date: 2022-03-21T15:22:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;malware;open source;Russia
Slug: 2022-03-21-developer-sabotages-open-source-software-package

[Source](https://www.schneier.com/blog/archives/2022/03/developer-sabotages-open-source-software-package.html){:target="_blank" rel="noopener"}

> This is a big deal : A developer has been caught adding malicious code to a popular open-source package that wiped files on computers located in Russia and Belarus as part of a protest that has enraged many users and raised concerns about the safety of free and open source software. The application, node-ipc, adds remote interprocess communication and neural networking capabilities to other open source code libraries. As a dependency, node-ipc is automatically downloaded and incorporated into other libraries, including ones like Vue.js CLI, which has more than 1 million weekly downloads. [...] The node-ipc update is just one [...]
