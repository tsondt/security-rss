Title: Gus Simmons’s Memoir
Date: 2022-03-25T11:14:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;books;history of cryptography
Slug: 2022-03-25-gus-simmonss-memoir

[Source](https://www.schneier.com/blog/archives/2022/03/gus-simmonss-memoir.html){:target="_blank" rel="noopener"}

> Gus Simmons is an early pioneer in cryptography and computer security. I know him best for his work on authentication and covert channels, specifically as related to nuclear treaty verification. His work is cited extensively in Applied Cryptography. He has written a memoir of growing up dirt-poor in 1930s rural West Virginia. I’m in the middle of reading it, and it’s fascinating. More blog posts. [...]
