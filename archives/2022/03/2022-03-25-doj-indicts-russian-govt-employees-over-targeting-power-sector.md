Title: DOJ Indicts Russian Gov’t Employees Over Targeting Power Sector
Date: 2022-03-25T21:25:17+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Critical Infrastructure;Government;Hacks;Malware;Vulnerabilities;Web Security
Slug: 2022-03-25-doj-indicts-russian-govt-employees-over-targeting-power-sector

[Source](https://threatpost.com/doj-indicts-russian-govt-employees-over-targeting-power-sector/179108/){:target="_blank" rel="noopener"}

> The supply-chain attack on the U.S. energy sector targeted thousands of computers at hundreds of organizations, including at least one nuclear power plant. [...]
