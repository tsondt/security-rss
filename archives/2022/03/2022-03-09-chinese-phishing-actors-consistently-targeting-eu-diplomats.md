Title: Chinese phishing actors consistently targeting EU diplomats
Date: 2022-03-09T02:02:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-09-chinese-phishing-actors-consistently-targeting-eu-diplomats

[Source](https://www.bleepingcomputer.com/news/security/chinese-phishing-actors-consistently-targeting-eu-diplomats/){:target="_blank" rel="noopener"}

> The China-aligned group tracked as TA416 (aka Mustang Panda) has been consistently targeting European diplomats since August 2020, with the most recent activity involving refreshed lures to coincide with the Russian invasion of Ukraine. [...]
