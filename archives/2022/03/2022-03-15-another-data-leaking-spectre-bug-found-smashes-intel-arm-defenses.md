Title: Another data-leaking Spectre bug found, smashes Intel, Arm defenses
Date: 2022-03-15T09:22:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-03-15-another-data-leaking-spectre-bug-found-smashes-intel-arm-defenses

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/spectre_bti_intel_amd_arm/){:target="_blank" rel="noopener"}

> Your processor design fell off the vulnerability tree and hit every branch on the way down Analysis Intel this month published an advisory to address a novel Spectre v2 vulnerability in its processors that can be exploited by malware to steal data from memory that should otherwise be off limits.... [...]
