Title: Japanese beauty retailer Acro blames third-party hack for breach of 100k payment cards
Date: 2022-03-04T15:57:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-04-japanese-beauty-retailer-acro-blames-third-party-hack-for-breach-of-100k-payment-cards

[Source](https://portswigger.net/daily-swig/japanese-beauty-retailer-acro-blames-third-party-hack-for-breach-of-100k-payment-cards){:target="_blank" rel="noopener"}

> Company traces compromise to vulnerability in payment processor’s systems [...]
