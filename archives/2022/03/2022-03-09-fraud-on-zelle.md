Title: Fraud on Zelle
Date: 2022-03-09T12:00:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;banking;fraud;scams
Slug: 2022-03-09-fraud-on-zelle

[Source](https://www.schneier.com/blog/archives/2022/03/fraud-on-zelle.html){:target="_blank" rel="noopener"}

> Zelle is rife with fraud : Zelle’s immediacy has also made it a favorite of fraudsters. Other types of bank transfers or transactions involving payment cards typically take at least a day to clear. But once crooks scare or trick victims into handing over money via Zelle, they can siphon away thousands of dollars in seconds. There’s no way for customers — and in many cases, the banks themselves — to retrieve the money. [...] It’s not clear who is legally liable for such losses. Banks say that returning money to defrauded customers is not their responsibility, since the federal [...]
