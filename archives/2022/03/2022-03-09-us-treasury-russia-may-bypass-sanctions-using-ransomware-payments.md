Title: US Treasury: Russia may bypass sanctions using ransomware payments
Date: 2022-03-09T11:41:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency;Government
Slug: 2022-03-09-us-treasury-russia-may-bypass-sanctions-using-ransomware-payments

[Source](https://www.bleepingcomputer.com/news/security/us-treasury-russia-may-bypass-sanctions-using-ransomware-payments/){:target="_blank" rel="noopener"}

> The Treasury Department's Financial Crimes Enforcement Network (FinCEN) warned U.S. financial institutions this week to keep an eye out for attempts to evade sanctions and US-imposed restrictions following Russia's invasion of Ukraine. [...]
