Title: Russia creates its own TLS certificate authority to bypass sanctions
Date: 2022-03-10T11:06:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-10-russia-creates-its-own-tls-certificate-authority-to-bypass-sanctions

[Source](https://www.bleepingcomputer.com/news/security/russia-creates-its-own-tls-certificate-authority-to-bypass-sanctions/){:target="_blank" rel="noopener"}

> Russia has created its own trusted TLS certificate authority (CA) to solve website access problems that have been piling up after sanctions prevent certificate renewals. [...]
