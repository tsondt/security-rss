Title: How to use AWS Security Hub and Amazon OpenSearch Service for SIEM
Date: 2022-03-21T16:21:20+00:00
Author: Ely Kahn
Category: AWS Security
Tags: Advanced (300);AWS Security Hub;Security, Identity, & Compliance;Uncategorized;Amazon Elasticsearch Service;SIEM
Slug: 2022-03-21-how-to-use-aws-security-hub-and-amazon-opensearch-service-for-siem

[Source](https://aws.amazon.com/blogs/security/how-to-use-aws-security-hub-and-amazon-opensearch-service-for-siem/){:target="_blank" rel="noopener"}

> AWS Security Hub provides you with a consolidated view of your security posture in Amazon Web Services (AWS) and helps you check your environment against security standards and current AWS security recommendations. Although Security Hub has some similarities to security information and event management (SIEM) tools, it is not designed as standalone a SIEM replacement. [...]
