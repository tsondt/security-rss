Title: Upcoming Speaking Events
Date: 2022-03-14T14:57:17+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-03-14-upcoming-speaking-events

[Source](https://www.schneier.com/blog/archives/2022/03/upcoming-speaking-events.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m participating in an online panel discussion on “ Ukraine and Russia: The Online War,” hosted by UMass Amherst, at 5:00 PM Eastern on March 31, 2022. I’m speaking at Future Summits in Antwerp, Belgium on May 18, 2022. I’m speaking at IT-S Now 2022 in Vienna on June 2, 2022. I’m speaking at the 14th International Conference on Cyber Conflict, CyCon 2022, in Tallinn, Estonia on June 3, 2022. I’m speaking at the RSA Conference 2022 in San Francisco, June 6-9, 2022. I’m speaking at [...]
