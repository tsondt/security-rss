Title: UK spy boss warns China hopes Russia will help it take over tech standards
Date: 2022-03-31T04:01:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-31-uk-spy-boss-warns-china-hopes-russia-will-help-it-take-over-tech-standards

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/31/gchq_sir_jeremy_fleming_speech/){:target="_blank" rel="noopener"}

> Speech also alleges Russian troops in Ukraine have mutinied, shot down own plane The director of UK intelligence agency Government Communications Headquarters (GCHQ), Sir Jeremy Fleming, has warned that China is trying to introduce "undemocratic values as the default for vast swathes of future tech and the standards that govern it."... [...]
