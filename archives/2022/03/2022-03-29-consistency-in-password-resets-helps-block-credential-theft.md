Title: Consistency in password resets helps block credential theft
Date: 2022-03-29T10:01:02-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-03-29-consistency-in-password-resets-helps-block-credential-theft

[Source](https://www.bleepingcomputer.com/news/security/consistency-in-password-resets-helps-block-credential-theft/){:target="_blank" rel="noopener"}

> As important as end user training and message filtering may be, there is a third method that tip the odds in their favor. Because phishing attacks often come disguised as password reset emails, it is important to handle password resets in a way that makes it obvious that email messages are not part of the password reset process. [...]
