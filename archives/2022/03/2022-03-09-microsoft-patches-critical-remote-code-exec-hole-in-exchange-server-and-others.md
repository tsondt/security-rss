Title: Microsoft patches critical remote-code-exec hole in Exchange Server and others
Date: 2022-03-09T01:32:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-09-microsoft-patches-critical-remote-code-exec-hole-in-exchange-server-and-others

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/09/microsoft_patch_tuesday/){:target="_blank" rel="noopener"}

> And Adobe, SAP, Intel, AMD, Cisco, Google join in Patch Tuesday Microsoft has addressed 71 security flaws, including three critical remote code execution vulnerabilities, in its monthly Patch Tuesday update. The IT giant is confident none of the bugs have been actively exploited.... [...]
