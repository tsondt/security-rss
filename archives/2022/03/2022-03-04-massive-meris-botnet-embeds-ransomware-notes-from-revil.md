Title: Massive Meris Botnet Embeds Ransomware Notes from REvil
Date: 2022-03-04T22:46:59+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;IoT;Malware;Web Security
Slug: 2022-03-04-massive-meris-botnet-embeds-ransomware-notes-from-revil

[Source](https://threatpost.com/massive-meris-botnet-embeds-ransomware-notes-revil/178769/){:target="_blank" rel="noopener"}

> Notes threatening to tank targeted companies' stock price were embedded into the DDoS ransomware attacks as a string_of_text directed to CEOs and webops_geeks in the URL. [...]
