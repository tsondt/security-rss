Title: Samsung Confirms Lapsus$ Ransomware Hit, Source Code Leak
Date: 2022-03-07T19:28:36+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks
Slug: 2022-03-07-samsung-confirms-lapsus-ransomware-hit-source-code-leak

[Source](https://threatpost.com/samsung-lapsus-ransomware-source-code/178791/){:target="_blank" rel="noopener"}

> The move comes just a week after GPU-maker NVIDIA was hit by Lapsus$ and every employee credential was leaked. [...]
