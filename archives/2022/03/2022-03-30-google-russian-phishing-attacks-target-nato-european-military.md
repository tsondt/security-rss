Title: Google: Russian phishing attacks target NATO, European military
Date: 2022-03-30T13:44:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-03-30-google-russian-phishing-attacks-target-nato-european-military

[Source](https://www.bleepingcomputer.com/news/security/google-russian-phishing-attacks-target-nato-european-military/){:target="_blank" rel="noopener"}

> The Google Threat Analysis Group (TAG) says more and more threat actors are now using Russia's war in Ukraine to target Eastern European and NATO countries, including Ukraine, in phishing and malware attacks. [...]
