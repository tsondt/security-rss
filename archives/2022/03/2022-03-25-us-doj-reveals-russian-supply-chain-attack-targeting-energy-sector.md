Title: US DoJ reveals Russian supply chain attack targeting energy sector
Date: 2022-03-25T06:45:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-25-us-doj-reveals-russian-supply-chain-attack-targeting-energy-sector

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/25/us_indicts_russian_state_hackers/){:target="_blank" rel="noopener"}

> Poisoned SCADA apps could have disrupted power supply – perhaps even at nuclear plants The United States Department of Justice has unsealed a pair of indictments that detail alleged Russian government hackers' efforts to use supply chain attacks and malware in an attempt to compromise and control critical infrastructure around the world – including at least one nuclear power plant.... [...]
