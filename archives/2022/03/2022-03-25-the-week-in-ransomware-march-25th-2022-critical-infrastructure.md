Title: The Week in Ransomware - March 25th 2022 - Critical infrastructure
Date: 2022-03-25T17:06:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-25-the-week-in-ransomware-march-25th-2022-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-25th-2022-critical-infrastructure/){:target="_blank" rel="noopener"}

> With the US providing military aid to Ukraine and its sanctions damaging the Russian economy, the US government disclosed this week that there is intelligence that Russia is preparing for potential cyberattacks against US interests. [...]
