Title: Has Trickbot gang hijacked your router? This scanner may have an answer
Date: 2022-03-17T20:51:28+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-17-has-trickbot-gang-hijacked-your-router-this-scanner-may-have-an-answer

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/17/microsoft_trickbot_scanner/){:target="_blank" rel="noopener"}

> Thanks... Micro... soft... OK, there, we said it Microsoft has published a tool that scans for and detects MikroTik-powered Internet-of-Things devices that have been hijacked by the Trickbot gang.... [...]
