Title: WhatsApp emits extension to detect tampering with desktop web apps
Date: 2022-03-10T21:04:43+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-03-10-whatsapp-emits-extension-to-detect-tampering-with-desktop-web-apps

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/10/whatsapp_cloudflare_code/){:target="_blank" rel="noopener"}

> Code Verify tool confers with Cloudflare to warn of any shenanigans WhatsApp and Cloudflare have teamed up to provide desktop users of WhatsApp's web client with a browser extension called Code Verify that checks the integrity of the software running in their browser.... [...]
