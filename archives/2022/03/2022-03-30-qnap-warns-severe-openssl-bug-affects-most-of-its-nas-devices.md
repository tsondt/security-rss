Title: QNAP warns severe OpenSSL bug affects most of its NAS devices
Date: 2022-03-30T12:39:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-30-qnap-warns-severe-openssl-bug-affects-most-of-its-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-severe-openssl-bug-affects-most-of-its-nas-devices/){:target="_blank" rel="noopener"}

> Taiwan-based network-attached storage (NAS) maker QNAP warned on Tuesday that most of its NAS devices are impacted by a high severity OpenSSL bug disclosed two weeks ago. [...]
