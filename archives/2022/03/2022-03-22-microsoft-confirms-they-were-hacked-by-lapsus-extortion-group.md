Title: Microsoft confirms they were hacked by Lapsus$ extortion group
Date: 2022-03-22T20:13:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-03-22-microsoft-confirms-they-were-hacked-by-lapsus-extortion-group

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-confirms-they-were-hacked-by-lapsus-extortion-group/){:target="_blank" rel="noopener"}

> Microsoft has confirmed that one of their employees was compromised by the Lapsus$ hacking group, allowing the threat actors to access and steal portions of their source code. [...]
