Title: Breaking RSA through Insufficiently Random Primes
Date: 2022-03-16T16:35:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptanalysis;keys;PGP;random numbers;RSA
Slug: 2022-03-16-breaking-rsa-through-insufficiently-random-primes

[Source](https://www.schneier.com/blog/archives/2022/03/breaking-rsa-through-insufficiently-random-primes.html){:target="_blank" rel="noopener"}

> Basically, the SafeZone library doesn’t sufficiently randomize the two prime numbers it used to generate RSA keys. They’re too close to each other, which makes them vulnerable to recovery. There aren’t many weak keys out there, but there are some: So far, Böck has identified only a handful of keys in the wild that are vulnerable to the factorization attack. Some of the keys are from printers from two manufacturers, Canon and Fujifilm (originally branded as Fuji Xerox). Printer users can use the keys to generate a Certificate Signing Request. The creation date for the all the weak keys was [...]
