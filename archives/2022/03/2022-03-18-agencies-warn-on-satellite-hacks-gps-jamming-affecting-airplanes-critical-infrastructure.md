Title: Agencies Warn on Satellite Hacks & GPS Jamming Affecting Airplanes, Critical Infrastructure
Date: 2022-03-18T20:05:36+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Web Security
Slug: 2022-03-18-agencies-warn-on-satellite-hacks-gps-jamming-affecting-airplanes-critical-infrastructure

[Source](https://threatpost.com/agencies-satellite-hacks-gps-jamming-airplanes-critical-infrastructure/178993/){:target="_blank" rel="noopener"}

> The Russian invasion of Ukraine has coincided with the jamming of airplane navigation systems and hacks on the SATCOM networks that empower critical infrastructure. [...]
