Title: Samsung confirms hackers stole Galaxy devices source code
Date: 2022-03-07T11:29:06-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-07-samsung-confirms-hackers-stole-galaxy-devices-source-code

[Source](https://www.bleepingcomputer.com/news/security/samsung-confirms-hackers-stole-galaxy-devices-source-code/){:target="_blank" rel="noopener"}

> Samsung Electronics confirmed on Monday that its network was breached and the hackers stole confidential information, including source code present in Galaxy smartphones. [...]
