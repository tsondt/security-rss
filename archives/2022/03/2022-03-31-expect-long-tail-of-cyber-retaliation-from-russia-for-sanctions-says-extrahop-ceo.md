Title: Expect 'long tail of cyber retaliation' from Russia for sanctions, says ExtraHop CEO
Date: 2022-03-31T09:32:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-31-expect-long-tail-of-cyber-retaliation-from-russia-for-sanctions-says-extrahop-ceo

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/31/extrahop_russian_sanctions/){:target="_blank" rel="noopener"}

> 'We have this small moment in time where we can make improvements in our defensive posture' The US and its NATO allies should expect a "long tail of retaliation," in the form of cyberattacks, for the sanctions imposed on Russia, says cloud security shop ExtraHop's CEO Patrick Dennis.... [...]
