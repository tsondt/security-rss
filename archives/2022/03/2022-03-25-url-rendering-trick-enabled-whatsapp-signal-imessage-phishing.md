Title: URL rendering trick enabled WhatsApp, Signal, iMessage phishing
Date: 2022-03-25T11:51:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-25-url-rendering-trick-enabled-whatsapp-signal-imessage-phishing

[Source](https://www.bleepingcomputer.com/news/security/url-rendering-trick-enabled-whatsapp-signal-imessage-phishing/){:target="_blank" rel="noopener"}

> A set of flaws affecting the world's leading messaging and email platforms, including Instagram, iMessage, WhatsApp, Signal, and Facebook Messenger, has allowed threat actors to create legitimate-looking phishing URLs for the past three years. [...]
