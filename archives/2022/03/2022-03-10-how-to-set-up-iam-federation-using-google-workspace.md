Title: How to set up IAM federation using Google Workspace
Date: 2022-03-10T18:06:32+00:00
Author: Wei Chen
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Federation;IAM;SSO
Slug: 2022-03-10-how-to-set-up-iam-federation-using-google-workspace

[Source](https://aws.amazon.com/blogs/security/how-to-set-up-federated-single-sign-on-to-aws-using-google-workspace/){:target="_blank" rel="noopener"}

> March 16, 2022: The title and the opening section of this blog post has been updated. Federating your external identity provider (IdP) to AWS is a best practice. The simplest way to federate into AWS is with AWS Single Sign-On (AWS SSO). With AWS SSO, you configure federation once and manage access to all of your AWS accounts [...]
