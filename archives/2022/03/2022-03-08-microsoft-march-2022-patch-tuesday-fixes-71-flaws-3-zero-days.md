Title: Microsoft March 2022 Patch Tuesday fixes 71 flaws, 3 zero-days
Date: 2022-03-08T13:28:54-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-03-08-microsoft-march-2022-patch-tuesday-fixes-71-flaws-3-zero-days

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-march-2022-patch-tuesday-fixes-71-flaws-3-zero-days/){:target="_blank" rel="noopener"}

> Today is Microsoft's March 2022 Patch Tuesday, and with it comes fixes for three zero-day vulnerabilities and a total of 71 flaws. [...]
