Title: Ukrainian sites saw a 10x increase in attacks when invasion started
Date: 2022-03-02T18:46:57-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-02-ukrainian-sites-saw-a-10x-increase-in-attacks-when-invasion-started

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-sites-saw-a-10x-increase-in-attacks-when-invasion-started/){:target="_blank" rel="noopener"}

> Internet security companies have recorded a massive wave of attacks against Ukrainian WordPress sites since Russia invaded Ukraine, aiming to take down the websites and cause general demoralization. [...]
