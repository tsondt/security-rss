Title: Detailed: Critical hijacking bugs that took months to patch in Microsoft Azure Defender for IoT
Date: 2022-03-30T02:18:28+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-30-detailed-critical-hijacking-bugs-that-took-months-to-patch-in-microsoft-azure-defender-for-iot

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/30/sentinelone_microsoft_azure_iot/){:target="_blank" rel="noopener"}

> SQL injection, race condition, bad cryptographic check pave way for infrastructure network takeovers SentinelOne this week detailed a handful of bugs, including two critical remote code execution vulnerabilities, it found in Microsoft Azure Defender for IoT.... [...]
