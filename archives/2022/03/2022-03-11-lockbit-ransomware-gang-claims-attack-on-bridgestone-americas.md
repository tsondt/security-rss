Title: LockBit ransomware gang claims attack on Bridgestone Americas
Date: 2022-03-11T16:28:15-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-11-lockbit-ransomware-gang-claims-attack-on-bridgestone-americas

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-gang-claims-attack-on-bridgestone-americas/){:target="_blank" rel="noopener"}

> A cyberattack on Bridgestone Americas, one of the largest manufacturers of tires in the world, has been claimed by the LockBit ransomware gang. [...]
