Title: Racing against the clock -- hitting a tiny kernel race window
Date: 2022-03-24T13:51:00.001000-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2022-03-24-racing-against-the-clock-hitting-a-tiny-kernel-race-window

[Source](https://googleprojectzero.blogspot.com/2022/03/racing-against-clock-hitting-tiny.html){:target="_blank" rel="noopener"}

> TL;DR: How to make a tiny kernel race window really large even on kernels without CONFIG_PREEMPT : use a cache miss to widen the race window a little bit make a timerfd expire in that window (which will run in an interrupt handler - in other words, in hardirq context) make sure that the wakeup triggered by the timerfd has to churn through 50000 waitqueue items created by epoll Racing one thread against a timer also avoids accumulating timing variations from two threads in each race attempt - hence the title. On the other hand, it also means you now [...]
