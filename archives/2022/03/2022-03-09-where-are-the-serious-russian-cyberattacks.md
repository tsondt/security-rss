Title: Where are the (serious) Russian cyberattacks?
Date: 2022-03-09T10:29:08+00:00
Author: Steven J. Vaughan-Nichols
Category: The Register
Tags: 
Slug: 2022-03-09-where-are-the-serious-russian-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/09/where_are_the_russian_cyberattacks/){:target="_blank" rel="noopener"}

> Sure, HermeticWiper and IssacWiper are bad, but they're not BAD in capital letters Column I'm heartsick over Russia's invasion of Ukraine. But, before it began, I'd been really worried about Russian cyberattacks, which would overrun Ukraine and flood into the West's infrastructure.... [...]
