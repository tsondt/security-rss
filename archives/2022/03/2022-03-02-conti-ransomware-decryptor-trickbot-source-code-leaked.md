Title: Conti Ransomware Decryptor, TrickBot Source Code Leaked
Date: 2022-03-02T18:14:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-03-02-conti-ransomware-decryptor-trickbot-source-code-leaked

[Source](https://threatpost.com/conti-ransomware-decryptor-trickbot-source-code-leaked/178727/){:target="_blank" rel="noopener"}

> The decryptor spilled by ContiLeaks won’t work with recent victims. Conti couldn't care less: It's still operating just fine. Still, the dump is a bouquet’s worth of intel. [...]
