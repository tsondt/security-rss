Title: Russia, Iran, Saudi Arabia are top sources of online misinformation
Date: 2022-03-31T02:30:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-03-31-russia-iran-saudi-arabia-are-top-sources-of-online-misinformation

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/31/russia_iran_and_saudi_arabia/){:target="_blank" rel="noopener"}

> Think tank fears future studies of this sort may be harder as social networks withdraw data Russia, Iran and Saudi Arabia are the top three proliferators of state-linked Twitter misinformation campaigns, according to a report released Wednesday by the Australian Strategic Policy Institute (ASPI).... [...]
