Title: Patch now: RCE Spring4shell hits Java Spring framework
Date: 2022-03-31T15:00:04+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-03-31-patch-now-rce-spring4shell-hits-java-spring-framework

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/31/spring_vuln/){:target="_blank" rel="noopener"}

> You didn't have any plans for the weekend anyway, did you? Another Java Remote Code Execution vulnerability has reared its head, this time in the popular Spring Framework and, goodness, it's a nasty one.... [...]
