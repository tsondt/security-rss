Title: China: attacks from US IP addresses hit us, moved on to Russia and Ukraine
Date: 2022-03-14T06:58:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-14-china-attacks-from-us-ip-addresses-hit-us-moved-on-to-russia-and-ukraine

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/14/china_attackers_with_us_ips/){:target="_blank" rel="noopener"}

> Offers list of IP addresses that look like they're hosted at carriers and colos – hardly the stuff of super-spies China's Cyberspace Administration has claimed that "since late February" it has observed continuous attacks on the Chinese internet and local computers by actors who used the resources they co-opted to target Russia, Belarus, and Ukraine.... [...]
