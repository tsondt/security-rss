Title: Honda bug lets a hacker unlock and start your car via replay attack
Date: 2022-03-25T03:28:03-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-25-honda-bug-lets-a-hacker-unlock-and-start-your-car-via-replay-attack

[Source](https://www.bleepingcomputer.com/news/security/honda-bug-lets-a-hacker-unlock-and-start-your-car-via-replay-attack/){:target="_blank" rel="noopener"}

> Researchers have disclosed a 'replay attack' vulnerability affecting select Honda and Acura car models, that allows a nearby hacker to unlock and start your car wirelessly. Honda has no plans to fix the issue in older models at this time. [...]
