Title: Lockbit wins ransomware speed test, encrypts 25,000 files per minute
Date: 2022-03-23T12:01:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-23-lockbit-wins-ransomware-speed-test-encrypts-25000-files-per-minute

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/23/ransomware_encryption_speed/){:target="_blank" rel="noopener"}

> Aim for prevention rather than outrunning this malware Ransomware moves more quickly than most organizations can respond. Though knowing they have a specific limited window should help inform where to put their defenses, according to security data shop Splunk.... [...]
