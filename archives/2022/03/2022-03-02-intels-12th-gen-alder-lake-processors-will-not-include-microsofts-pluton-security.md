Title: Intel's 12th-gen Alder Lake processors will not include Microsoft's Pluton security
Date: 2022-03-02T09:10:05+00:00
Author: Agam Shah
Category: The Register
Tags: 
Slug: 2022-03-02-intels-12th-gen-alder-lake-processors-will-not-include-microsofts-pluton-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/02/microsoft_pluton_chip/){:target="_blank" rel="noopener"}

> We can still hear the echoes of the launch fanfare from 2020 Microsoft's attempt to put its homegrown Pluton security processor architecture into third-party Windows 11 PCs is right now more work-in-progress than the slam dunk its publicity would have you believe.... [...]
