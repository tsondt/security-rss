Title: New Phishing toolkit lets anyone create fake Chrome browser windows
Date: 2022-03-19T11:16:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-19-new-phishing-toolkit-lets-anyone-create-fake-chrome-browser-windows

[Source](https://www.bleepingcomputer.com/news/security/new-phishing-toolkit-lets-anyone-create-fake-chrome-browser-windows/){:target="_blank" rel="noopener"}

> A phishing kit has been released that allows red teamers and wannabe cybercriminals to create effective single sign-on phishing login forms using fake Chrome browser windows. [...]
