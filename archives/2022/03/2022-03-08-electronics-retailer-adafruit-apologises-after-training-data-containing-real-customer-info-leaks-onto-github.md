Title: Electronics retailer Adafruit apologises after training data containing real customer info leaks onto GitHub
Date: 2022-03-08T17:32:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-08-electronics-retailer-adafruit-apologises-after-training-data-containing-real-customer-info-leaks-onto-github

[Source](https://portswigger.net/daily-swig/electronics-retailer-adafruit-apologises-after-training-data-containing-real-customer-info-leaks-onto-github){:target="_blank" rel="noopener"}

> IoT hardware vendor promises to tighten up procedures [...]
