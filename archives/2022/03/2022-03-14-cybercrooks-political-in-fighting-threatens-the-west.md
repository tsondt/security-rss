Title: Cybercrooks’ Political In-Fighting Threatens the West
Date: 2022-03-14T13:52:37+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Critical Infrastructure;Government;Malware;Web Security
Slug: 2022-03-14-cybercrooks-political-in-fighting-threatens-the-west

[Source](https://threatpost.com/cybercrooks-political-in-fighting-threatens-the-west/178899/){:target="_blank" rel="noopener"}

> They’re choosing sides in the Russia-Ukraine war, beckoning previously shunned ransomware groups and thereby reinvigorating those groups' once-diminished power. [...]
