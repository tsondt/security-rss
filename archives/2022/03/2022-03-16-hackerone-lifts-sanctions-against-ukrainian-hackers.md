Title: HackerOne lifts ‘sanctions’&nbsp;against Ukrainian hackers
Date: 2022-03-16T14:39:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-16-hackerone-lifts-sanctions-against-ukrainian-hackers

[Source](https://portswigger.net/daily-swig/hackerone-lifts-sanctions-nbsp-against-ukrainian-hackers){:target="_blank" rel="noopener"}

> Platform apologizes for ‘poor communication’ over bug bounty payouts [...]
