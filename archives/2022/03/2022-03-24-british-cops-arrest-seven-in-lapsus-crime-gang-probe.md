Title: British cops arrest seven in Lapsus$ crime gang probe
Date: 2022-03-24T22:13:19+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-24-british-cops-arrest-seven-in-lapsus-crime-gang-probe

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/24/police_lapsus_arrests/){:target="_blank" rel="noopener"}

> Bitcoin millionaire teen said to be among those detained British cops investigating a cyber-crime group have made a string of arrests.... [...]
