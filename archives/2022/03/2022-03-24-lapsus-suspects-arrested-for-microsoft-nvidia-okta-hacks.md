Title: Lapsus$ suspects arrested for Microsoft, Nvidia, Okta hacks
Date: 2022-03-24T16:25:21-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-24-lapsus-suspects-arrested-for-microsoft-nvidia-okta-hacks

[Source](https://www.bleepingcomputer.com/news/security/lapsus-suspects-arrested-for-microsoft-nvidia-okta-hacks/){:target="_blank" rel="noopener"}

> As Lapsus$ data extortion gang announced that several of its members are taking a vacation, the City of London Police say they have arrested seven individuals connected to the gang. [...]
