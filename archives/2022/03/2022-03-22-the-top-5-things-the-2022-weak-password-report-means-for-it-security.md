Title: The top 5 things the 2022 Weak Password Report means for IT security
Date: 2022-03-22T10:00:00-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-03-22-the-top-5-things-the-2022-weak-password-report-means-for-it-security

[Source](https://www.bleepingcomputer.com/news/security/the-top-5-things-the-2022-weak-password-report-means-for-it-security/){:target="_blank" rel="noopener"}

> Given that passwords have had such unprecedented longevity, it would seem that password security best practices would be refined to the point of perfection. Even so, Specops Software's first annual Weak Password Report has yielded some interesting results that may cause you to rethink the way that your organization manages passwords. [...]
