Title: US national emergency extended due to elevated malicious cyber activity
Date: 2022-03-30T16:34:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-03-30-us-national-emergency-extended-due-to-elevated-malicious-cyber-activity

[Source](https://www.bleepingcomputer.com/news/security/us-national-emergency-extended-due-to-elevated-malicious-cyber-activity/){:target="_blank" rel="noopener"}

> US President Joe Biden today has extended the state of national emergency declared to deal with increasingly prevalent and severe malicious cyber threats to the United States national security, foreign policy, and economy. [...]
