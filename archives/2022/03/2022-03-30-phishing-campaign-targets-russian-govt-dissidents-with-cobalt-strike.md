Title: Phishing campaign targets Russian govt dissidents with Cobalt Strike
Date: 2022-03-30T09:05:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-30-phishing-campaign-targets-russian-govt-dissidents-with-cobalt-strike

[Source](https://www.bleepingcomputer.com/news/security/phishing-campaign-targets-russian-govt-dissidents-with-cobalt-strike/){:target="_blank" rel="noopener"}

> A new spear phishing campaign is taking place in Russia targeting dissenters with opposing views to those promoted by the state and national media about the war against Ukraine. [...]
