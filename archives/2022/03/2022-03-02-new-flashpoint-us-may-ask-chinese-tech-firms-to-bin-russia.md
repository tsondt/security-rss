Title: New flashpoint: US may ask Chinese tech firms to bin Russia
Date: 2022-03-02T06:58:49+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-02-new-flashpoint-us-may-ask-chinese-tech-firms-to-bin-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/02/china_us_russia_sanctions_tangle/){:target="_blank" rel="noopener"}

> Beijing and Moscow are suddenly BFFs, and that's already seen one Chinese firm reverse a Russia ban As big tech companies from the West swiftly and happily comply with new rules that prohibit interactions with Russia, Chinese companies will soon feel pressure to do likewise – and counter-pressure to resist such calls.... [...]
