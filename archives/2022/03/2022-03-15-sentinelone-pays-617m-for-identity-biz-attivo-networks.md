Title: SentinelOne pays $617m for identity biz Attivo Networks
Date: 2022-03-15T18:45:40+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-15-sentinelone-pays-617m-for-identity-biz-attivo-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/sentinelone_attivo_617m/){:target="_blank" rel="noopener"}

> Security consolidation du jour SentinelOne reached a $616.5m deal to buy identity security vendor Attivo Networks, the companies announced today.... [...]
