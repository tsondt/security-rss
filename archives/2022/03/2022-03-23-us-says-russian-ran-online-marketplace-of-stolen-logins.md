Title: US says Russian ran online marketplace of stolen logins
Date: 2022-03-23T22:44:36+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-03-23-us-says-russian-ran-online-marketplace-of-stolen-logins

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/23/russian_indicted_by_us/){:target="_blank" rel="noopener"}

> Cyber-souk offered bundle deals of account access and credit card info, says Uncle Sam A Russian national was indicted in the US on Tuesday for allegedly running an online marketplace selling access to credit card, shopping, and web payment accounts belonging to tens of thousands of victims.... [...]
