Title: Hackers fork open-source reverse tunneling tool for persistence
Date: 2022-03-09T13:24:17-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-09-hackers-fork-open-source-reverse-tunneling-tool-for-persistence

[Source](https://www.bleepingcomputer.com/news/security/hackers-fork-open-source-reverse-tunneling-tool-for-persistence/){:target="_blank" rel="noopener"}

> Security experts have spotted an interesting case of a suspected ransomware attack that employed custom-made tools typically used by APT (advanced persistent threat) groups. [...]
