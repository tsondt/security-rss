Title: Well done patching Log4j. Now, are you ready for the next zero day disaster?
Date: 2022-03-22T18:00:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-03-22-well-done-patching-log4j-now-are-you-ready-for-the-next-zero-day-disaster

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/22/software_pipeline_log4j/){:target="_blank" rel="noopener"}

> Join us online to learn how to keep your software pipeline flowing Webinar If you breathed a sigh of relief after dealing with the Log4j vulnerability last year, here's some bad news. There are further equally nasty zero day vulnerabilities to come, so now is not the time to relax.... [...]
