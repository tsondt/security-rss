Title: FBI warns election officials of credential phishing attacks
Date: 2022-03-29T12:52:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-29-fbi-warns-election-officials-of-credential-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-election-officials-of-credential-phishing-attacks/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned US election officials on Tuesday of an ongoing and widespread phishing campaign trying to steal their credentials since at least October 2021. [...]
