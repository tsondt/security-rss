Title: Ransomware mitigation: Using Amazon WorkDocs to protect end-user data
Date: 2022-03-18T19:53:00+00:00
Author: James Perry
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance
Slug: 2022-03-18-ransomware-mitigation-using-amazon-workdocs-to-protect-end-user-data

[Source](https://aws.amazon.com/blogs/security/ransomware-mitigation-using-amazon-workdocs-to-protect-end-user-data/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has published whitepapers, blog articles, and videos with prescriptive guidance to assist you in developing an enterprise strategy to mitigate risks associated with ransomware and other destructive events. We also announced a strategic partnership with CrowdStrike and Presidio where together we developed a Ransomware Risk Mitigation Kit, and a Quick-Start engagement [...]
