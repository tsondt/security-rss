Title: Global consultancies quit Russia
Date: 2022-03-07T06:02:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-07-global-consultancies-quit-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/07/dxc_pwc_kpmg_accenture_quit_russia/){:target="_blank" rel="noopener"}

> KPMG and PwC follow DXC and Accenture, leaving 14,500 staff – probably – without a gig Four top global consultancies, all with big IT practices, have quit Russia.... [...]
