Title: Conti ransomware gang's source code leaked
Date: 2022-03-02T17:35:05+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-02-conti-ransomware-gangs-source-code-leaked

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/02/conti-source-code-leaked/){:target="_blank" rel="noopener"}

> Latest info dump days after anonymous outing of 60,000 messages Infamous ransomware group Conti is now the target of cyberattacks in the wake of its announcement late last week that it fully supports Russia's ongoing invasion of neighboring Ukraine, with the latest hit being the leaking of its source code for the public to see.... [...]
