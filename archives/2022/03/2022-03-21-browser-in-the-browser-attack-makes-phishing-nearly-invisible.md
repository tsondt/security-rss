Title: Browser-in-the-Browser Attack Makes Phishing Nearly Invisible
Date: 2022-03-21T23:57:04+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;News;Web Security
Slug: 2022-03-21-browser-in-the-browser-attack-makes-phishing-nearly-invisible

[Source](https://threatpost.com/browser-in-the-browser-attack-makes-phishing-nearly-invisible/179014/){:target="_blank" rel="noopener"}

> Can we trust web browsers to protect us, even if they say “https?” Not with the novel BitB attack, which fakes popup SSO windows to phish away credentials for Google, Facebook and Microsoft, et al. [...]
