Title: ‘Fox guarding the henhouse’ – Founder of cyber-fraud prevention company pleads guilty to defrauding investors
Date: 2022-03-17T12:09:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-17-fox-guarding-the-henhouse-founder-of-cyber-fraud-prevention-company-pleads-guilty-to-defrauding-investors

[Source](https://portswigger.net/daily-swig/fox-guarding-the-henhouse-founder-of-cyber-fraud-prevention-company-pleads-guilty-to-defrauding-investors){:target="_blank" rel="noopener"}

> Adam Rogas has been charged with using fraudulent financial data to secure more than $100m in funding [...]
