Title: Four Russian government employees charged over hacking campaigns on critical infrastructure
Date: 2022-03-25T14:39:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-25-four-russian-government-employees-charged-over-hacking-campaigns-on-critical-infrastructure

[Source](https://portswigger.net/daily-swig/four-russian-government-employees-charged-over-hacking-campaigns-on-critical-infrastructure){:target="_blank" rel="noopener"}

> Historical crimes unsealed by US courts [...]
