Title: FIDO Alliance says it has finally killed the password
Date: 2022-03-21T15:00:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-03-21-fido-alliance-says-it-has-finally-killed-the-password

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/21/fido_password_killer/){:target="_blank" rel="noopener"}

> Conceptually. It's OEMs who'll do the work, and you'll just have to trust them There's a new proposal on eliminating passwords, but it relies on putting a lot of security eggs into OEM security baskets.... [...]
