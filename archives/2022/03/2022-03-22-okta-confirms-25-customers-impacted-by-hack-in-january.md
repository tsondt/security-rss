Title: Okta confirms 2.5% customers impacted by hack in January
Date: 2022-03-22T18:52:20-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-22-okta-confirms-25-customers-impacted-by-hack-in-january

[Source](https://www.bleepingcomputer.com/news/security/okta-confirms-25-percent-customers-impacted-by-hack-in-january/){:target="_blank" rel="noopener"}

> Okta, a major provider of access management systems, says that 2.5%, or approximately 375 customers, were impacted by a cyberattack claimed by the Lapsus$ data extortion group. [...]
