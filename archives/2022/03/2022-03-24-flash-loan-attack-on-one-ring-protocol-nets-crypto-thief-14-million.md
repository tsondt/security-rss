Title: Flash loan attack on One Ring protocol nets crypto-thief $1.4 million
Date: 2022-03-24T11:53:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-24-flash-loan-attack-on-one-ring-protocol-nets-crypto-thief-14-million

[Source](https://portswigger.net/daily-swig/flash-loan-attack-on-one-ring-protocol-nets-crypto-thief-1-4-million){:target="_blank" rel="noopener"}

> Price manipulation of LP tokens ejected OShare tokens from protocol [...]
