Title: Customers can now request the AWS CyberGRX report for their third-party supplier due diligence
Date: 2022-03-07T21:50:38+00:00
Author: Niyaz Noor
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;3P Risk;AWS security;Compliance;Cyber Risk Management;CyberGRX;CyberGRX Assessment;Third Party Risk;Third-Party Risk Management;TPCRM;TPRM
Slug: 2022-03-07-customers-can-now-request-the-aws-cybergrx-report-for-their-third-party-supplier-due-diligence

[Source](https://aws.amazon.com/blogs/security/customers-can-now-request-the-aws-cybergrx-report-for-their-third-party-supplier-due-diligence/){:target="_blank" rel="noopener"}

> Gaining and maintaining customer trust is an ongoing commitment at Amazon Web Services (AWS). We are continuously expanding our compliance programs to provide customers with more tools and resources to be able to perform effective due diligence on AWS. We are excited to announce the availability of the AWS CyberGRX report for our customers. With the [...]
