Title: Prison service for England and Wales recorded more than 2,000 data breaches over 12 months
Date: 2022-03-14T16:01:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-14-prison-service-for-england-and-wales-recorded-more-than-2000-data-breaches-over-12-months

[Source](https://portswigger.net/daily-swig/prison-service-for-england-and-wales-recorded-more-than-2-000-data-breaches-over-12-months){:target="_blank" rel="noopener"}

> Ministry of Justice said information commissioner ‘satisfied’ with response to one particularly contentious breach [...]
