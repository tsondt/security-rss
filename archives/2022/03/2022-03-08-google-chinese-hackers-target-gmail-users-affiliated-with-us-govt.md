Title: Google: Chinese hackers target Gmail users affiliated with US govt
Date: 2022-03-08T11:58:56-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-08-google-chinese-hackers-target-gmail-users-affiliated-with-us-govt

[Source](https://www.bleepingcomputer.com/news/security/google-chinese-hackers-target-gmail-users-affiliated-with-us-govt/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group has warned multiple Gmail users that they were targeted in phishing attacks conducted by a Chinese-backed hacking group tracked as APT31. [...]
