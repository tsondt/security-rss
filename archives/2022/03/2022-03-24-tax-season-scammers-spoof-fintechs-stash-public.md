Title: Tax-Season Scammers Spoof Fintechs Stash, Public
Date: 2022-03-24T13:00:16+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-03-24-tax-season-scammers-spoof-fintechs-stash-public

[Source](https://threatpost.com/tax-season-scammers-spoof-fintechs-stash-public/179071/){:target="_blank" rel="noopener"}

> Threat actors are impersonating such wildly popular personal-finance apps (which are used more than social media or streaming services) to try to fool people into giving up their credentials. [...]
