Title: CISOs face 'perfect storm' of ransomware and state-supported cybercrime
Date: 2022-03-18T13:14:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-18-cisos-face-perfect-storm-of-ransomware-and-state-supported-cybercrime

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/18/ciso_security_storm/){:target="_blank" rel="noopener"}

> As some nations turn a blind eye, defense becomes life-or-death matter With not just ransomware gangs raiding network after network, but nation states consciously turning a blind eye to it, today's chief information security officers are caught in a "perfect storm," says Cybereason CSO Sam Curry.... [...]
