Title: AvosLocker group is targeting US critical infrastructure, FBI says
Date: 2022-03-21T14:00:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-21-avoslocker-group-is-targeting-us-critical-infrastructure-fbi-says

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/21/avoslocker-ransomware-critical-infrastructure-fbi/){:target="_blank" rel="noopener"}

> Ransomware affiliates threaten to publish stolen data or launch DDoS attacks if victims don’t pay A ransomware-as-a-service (RaaS) group that has been around since last summer is targeting critical infrastructure in the United States, according to federal law enforcement agencies.... [...]
