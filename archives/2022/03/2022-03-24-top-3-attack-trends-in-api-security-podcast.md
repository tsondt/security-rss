Title: Top 3 Attack Trends in API Security – Podcast
Date: 2022-03-24T13:00:59+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Podcasts;Web Security
Slug: 2022-03-24-top-3-attack-trends-in-api-security-podcast

[Source](https://threatpost.com/top-3-attack-trends-in-api-security-podcast/179064/){:target="_blank" rel="noopener"}

> Bots & automated attacks have exploded, with attackers and developers alike in love with APIs, according to a new Cequence Security report. Hacker-in-residence Jason Kent explains the latest. [...]
