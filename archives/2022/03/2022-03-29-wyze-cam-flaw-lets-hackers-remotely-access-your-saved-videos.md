Title: Wyze Cam flaw lets hackers remotely access your saved videos
Date: 2022-03-29T11:28:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-29-wyze-cam-flaw-lets-hackers-remotely-access-your-saved-videos

[Source](https://www.bleepingcomputer.com/news/security/wyze-cam-flaw-lets-hackers-remotely-access-your-saved-videos/){:target="_blank" rel="noopener"}

> A Wyze Cam internet camera vulnerability allows unauthenticated, remote access to videos and images stored on local memory cards and has remained unfixed for almost three years. [...]
