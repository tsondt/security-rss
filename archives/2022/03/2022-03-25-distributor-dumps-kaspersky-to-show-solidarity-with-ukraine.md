Title: Distributor dumps Kaspersky to show solidarity with Ukraine
Date: 2022-03-25T04:04:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-25-distributor-dumps-kaspersky-to-show-solidarity-with-ukraine

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/25/distributor_dicker_data_dumps_kaspsersky/){:target="_blank" rel="noopener"}

> Security software vendor saddened but says its channel is holding firm Australian technology distributor Dicker Data has decided to end its commercial relationship with Russian security software vendor Kaspersky.... [...]
