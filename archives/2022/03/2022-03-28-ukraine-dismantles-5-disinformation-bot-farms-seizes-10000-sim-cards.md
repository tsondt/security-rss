Title: Ukraine dismantles 5 disinformation bot farms, seizes 10,000 SIM cards
Date: 2022-03-28T16:23:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-28-ukraine-dismantles-5-disinformation-bot-farms-seizes-10000-sim-cards

[Source](https://www.bleepingcomputer.com/news/security/ukraine-dismantles-5-disinformation-bot-farms-seizes-10-000-sim-cards/){:target="_blank" rel="noopener"}

> The Ukrainian Security Service (SSU) has announced that since the start of the war with Russia, it has discovered and shut down five bot farms with over 100,000 fake social media accounts spreading fake news. [...]
