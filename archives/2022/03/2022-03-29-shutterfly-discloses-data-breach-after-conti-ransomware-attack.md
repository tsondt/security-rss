Title: Shutterfly discloses data breach after Conti ransomware attack
Date: 2022-03-29T14:32:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-29-shutterfly-discloses-data-breach-after-conti-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/shutterfly-discloses-data-breach-after-conti-ransomware-attack/){:target="_blank" rel="noopener"}

> Online retail and photography manufacturing platform Shutterfly has disclosed a data breach that exposed employee information after threat actors stole data during a Conti ransomware attack. [...]
