Title: A Better Grasp of Cyber Attack Tactics Can Stop Criminals Faster
Date: 2022-03-24T10:01:02-04:00
Author: Sponsored by FortiGuard Labs
Category: BleepingComputer
Tags: Security
Slug: 2022-03-24-a-better-grasp-of-cyber-attack-tactics-can-stop-criminals-faster

[Source](https://www.bleepingcomputer.com/news/security/a-better-grasp-of-cyber-attack-tactics-can-stop-criminals-faster/){:target="_blank" rel="noopener"}

> Recently, FortiGuard Labs released the latest Global Threat Landscape Report for the second half of 2021. There is a ton of data in it and several key takeaways. The main themes that weave through this report are about the increase in cybercriminal sophistication as well as speed. [...]
