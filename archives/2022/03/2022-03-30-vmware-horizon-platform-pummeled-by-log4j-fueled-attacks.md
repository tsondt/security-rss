Title: VMware Horizon platform pummeled by Log4j-fueled attacks
Date: 2022-03-30T15:30:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-30-vmware-horizon-platform-pummeled-by-log4j-fueled-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/30/log4j-malware-sophos-vmware-horizon/){:target="_blank" rel="noopener"}

> Miscreants deployed cryptominers, backdoors since late December, Sophos says VMware's Horizon virtualization platform has become an ongoing target of attackers exploiting the high-profile Log4j flaw to install backdoors and cryptomining malware.... [...]
