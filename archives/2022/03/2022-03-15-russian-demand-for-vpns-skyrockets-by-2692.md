Title: Russian demand for VPNs skyrockets by 2,692%
Date: 2022-03-15T15:30:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-03-15-russian-demand-for-vpns-skyrockets-by-2692

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/russian_demand_for_vpns/){:target="_blank" rel="noopener"}

> Virtual iron curtains are a lot harder to keep free of holes Research by Top10VPN, which regularly publishes data on virtual private newtork (VPN) usage around the world, has highlighted unprecedented demand in Russia and Ukraine.... [...]
