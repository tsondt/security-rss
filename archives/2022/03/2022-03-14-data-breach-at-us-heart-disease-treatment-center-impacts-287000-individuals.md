Title: Data breach at US heart disease treatment center impacts 287,000 individuals
Date: 2022-03-14T14:55:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-14-data-breach-at-us-heart-disease-treatment-center-impacts-287000-individuals

[Source](https://portswigger.net/daily-swig/data-breach-at-us-heart-disease-treatment-center-impacts-287-000-individuals){:target="_blank" rel="noopener"}

> South Denver Cardiology Associates admits hack [...]
