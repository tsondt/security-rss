Title: Cyberattackers Target UPS Backup Power Devices in Mission-Critical Environments
Date: 2022-03-30T17:14:57+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;IoT;Vulnerabilities;Web Security
Slug: 2022-03-30-cyberattackers-target-ups-backup-power-devices-in-mission-critical-environments

[Source](https://threatpost.com/cyberattackers-ups-backup-power-critical-environments/179169/){:target="_blank" rel="noopener"}

> The active attacks could result in critical-infrastructure damage, business disruption, lateral movement and more. [...]
