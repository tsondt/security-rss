Title: Brit data regulator fines five cold-calling fiends £405k
Date: 2022-03-17T11:45:09+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-03-17-brit-data-regulator-fines-five-cold-calling-fiends-405k

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/17/ico_cold_calling_fines/){:target="_blank" rel="noopener"}

> Businesses targeted elderly, made 750,000 calls to people on Telephone Preference System Five British companies are collectively nursing a £405,000 fine from the UK's data watchdog for making a combined total of 750,000 unsolicited marketing calls targeting vulnerable elderly people.... [...]
