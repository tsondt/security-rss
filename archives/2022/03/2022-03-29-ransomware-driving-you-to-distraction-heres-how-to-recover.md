Title: Ransomware driving you to distraction? Here’s how to recover
Date: 2022-03-29T16:15:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-03-29-ransomware-driving-you-to-distraction-heres-how-to-recover

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/29/ransomeware_recovery/){:target="_blank" rel="noopener"}

> This session will help restore your peace of mind Webinar What does peace of mind look like when you’re a security pro faced with a relentless onslaught of ransomware?... [...]
