Title: Huge DDoS attack temporarily kicks Israeli government sites offline
Date: 2022-03-15T17:12:19+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-15-huge-ddos-attack-temporarily-kicks-israeli-government-sites-offline

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/ddos-attack-israel-government-iran/){:target="_blank" rel="noopener"}

> A state of emergency is declared as officials assess the damage and look for culprits A massive distributed denial-of-service (DDoS) attack forced Israeli officials Monday to temporarily take down several government websites and to declare a state of online emergency to assess the damage and begin investigating who was behind the incident.... [...]
