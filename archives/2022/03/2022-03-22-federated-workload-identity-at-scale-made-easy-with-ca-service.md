Title: Federated workload identity at scale made easy with CA Service
Date: 2022-03-22T18:00:00+00:00
Author: Anoosh Saboori
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-03-22-federated-workload-identity-at-scale-made-easy-with-ca-service

[Source](https://cloud.google.com/blog/products/identity-security/unify-workload-certs-using-certificate-authority-service/){:target="_blank" rel="noopener"}

> At the end of 2021, we announced the ability for Google Cloud Certificate Authority (CA) Service to issue certificates for workloads reflecting their federated identities, even if the workloads are hosted on-premises or in other clouds. We are excited to announce this capability is now generally available, advancing our work to support customers’ implementation of zero trust strategies across all their IT environments. At the core of a zero trust approach to security is the idea that trust needs to be established via multiple mechanisms and continuously verified. A zero trust approach to end user access (such as Google’s BeyondCorp [...]
