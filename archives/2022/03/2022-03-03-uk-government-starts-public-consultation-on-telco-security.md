Title: UK government starts public consultation on telco security
Date: 2022-03-03T09:30:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-03-uk-government-starts-public-consultation-on-telco-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/03/telco_security_regulations_dcms_consultation/){:target="_blank" rel="noopener"}

> Potential mass internet surveillance idea dropped after ISP pushback, uncertainty about final measures linger While the world watches Ukraine, the British government has quietly dropped a requirement for mass surveillance of UK internet users by their service providers.... [...]
