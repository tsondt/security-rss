Title: Ukraine invasion: This may be the quiet before the cyber-storm, IT staff warned
Date: 2022-03-09T23:46:22+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-09-ukraine-invasion-this-may-be-the-quiet-before-the-cyber-storm-it-staff-warned

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/09/ukraine_russia_cyberattacks/){:target="_blank" rel="noopener"}

> Now is the time to be a prepper – the computer security kind As the invasion of Ukraine heads into its third week with NATO allies ratcheting up sanctions against Russia, infosec vendors have urged Western governments and businesses to prepare for retaliatory cyberattacks.... [...]
