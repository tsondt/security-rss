Title: US charges 4 Russian govt employees with critical infrastructure hacks
Date: 2022-03-24T17:57:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-24-us-charges-4-russian-govt-employees-with-critical-infrastructure-hacks

[Source](https://www.bleepingcomputer.com/news/security/us-charges-4-russian-govt-employees-with-critical-infrastructure-hacks/){:target="_blank" rel="noopener"}

> The U.S. has indicted four Russian government employees for their involvement in hacking campaigns targeting hundreds of companies and organizations from the global energy sector between 2012 and 2018. [...]
