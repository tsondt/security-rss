Title: Nearly 30% of critical WordPress plugin bugs don't get a patch
Date: 2022-03-09T16:33:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-09-nearly-30-of-critical-wordpress-plugin-bugs-dont-get-a-patch

[Source](https://www.bleepingcomputer.com/news/security/nearly-30-percent-of-critical-wordpress-plugin-bugs-dont-get-a-patch/){:target="_blank" rel="noopener"}

> Patchstack, a leader in WordPress security and threat intelligence, has released a whitepaper to present the state of WordPress security in 2021, and the report paints a dire picture. [...]
