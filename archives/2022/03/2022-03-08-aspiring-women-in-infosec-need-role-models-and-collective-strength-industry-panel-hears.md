Title: Aspiring women in infosec need role models and collective strength, industry panel hears
Date: 2022-03-08T16:24:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-08-aspiring-women-in-infosec-need-role-models-and-collective-strength-industry-panel-hears

[Source](https://portswigger.net/daily-swig/aspiring-women-in-infosec-need-role-models-and-collective-strength-industry-panel-hears){:target="_blank" rel="noopener"}

> Another panelist urged young security pros to consider starting out as generalists before specializing [...]
