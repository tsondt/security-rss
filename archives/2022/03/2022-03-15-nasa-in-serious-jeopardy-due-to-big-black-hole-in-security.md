Title: NASA in 'serious jeopardy' due to big black hole in security
Date: 2022-03-15T06:15:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-15-nasa-in-serious-jeopardy-due-to-big-black-hole-in-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/nasa_insider_threat_audit/){:target="_blank" rel="noopener"}

> Auditor finds space agency defends Classified info well, isn't paying attention to valuable Unclassified data An audit of NASA's infosec preparedness against insider threats has warned it faces "serious jeopardy to operations" due to lack of protection for Unclassified information.... [...]
