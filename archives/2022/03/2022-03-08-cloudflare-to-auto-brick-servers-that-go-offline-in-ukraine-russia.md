Title: Cloudflare to auto-brick servers that go offline in Ukraine, Russia
Date: 2022-03-08T11:31:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-08-cloudflare-to-auto-brick-servers-that-go-offline-in-ukraine-russia

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-to-auto-brick-servers-that-go-offline-in-ukraine-russia/){:target="_blank" rel="noopener"}

> Cloudflare announced that it is taking drastic measures to protect data of customers in Eastern Europe under current conditions of the Russian invasion of Ukraine. [...]
