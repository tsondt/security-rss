Title: Authentication oufit Okta investigating Lapsus$ breach report
Date: 2022-03-22T13:00:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-03-22-authentication-oufit-okta-investigating-lapsus-breach-report

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/22/okta_lapsus/){:target="_blank" rel="noopener"}

> Cloudflare takes no chances, hits the identity reset button The Lapsus$ extortion crew has turned its attention to identity platform Okta and published screenshots purportedly showing the group gaining access to the company's internals.... [...]
