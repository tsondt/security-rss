Title: VMware fixes command injection, file upload flaws in Carbon Black security tool
Date: 2022-03-23T23:30:31+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-23-vmware-fixes-command-injection-file-upload-flaws-in-carbon-black-security-tool

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/23/critical_bugs_vmware_carbon_black/){:target="_blank" rel="noopener"}

> Miscreants can exploit these to make a bad situation much worse VMware has patched two security flaws, an OS command injection vulnerability and a file upload hole, in its Carbon Black App Control security product running on Windows.... [...]
