Title: Cloud CISO Perspectives: March 2022
Date: 2022-03-31T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-03-31-cloud-ciso-perspectives-march-2022

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-march-2022/){:target="_blank" rel="noopener"}

> Two themes have been resonating for me across the security industry over the last month. The first is a topic from my personal blog that I wrote more than two years ago: Resilience is about Capabilities not Plans. Collectively, organizations have proven their ability to be resilient in light of many disruptive events like a pandemic, natural disasters, and cyber conflicts. Our resilience will only continue to be tested in existing or new ways into the future. Organizations that prioritize testing and re-testing capabilities across their people, process and technology vs. plans alone will continue to be the most resilient. [...]
