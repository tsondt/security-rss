Title: Bug in the Linux Kernel Allows Privilege Escalation, Container Escape
Date: 2022-03-08T14:52:05+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2022-03-08-bug-in-the-linux-kernel-allows-privilege-escalation-container-escape

[Source](https://threatpost.com/bug-linux-kernel-privilege-escalation-container-escape/178808/){:target="_blank" rel="noopener"}

> A missing check allows unprivileged attackers to escape containers and execute arbitrary commands in the kernel. [...]
