Title: HackerOne kicks Kaspersky’s bug bounty program off its platform
Date: 2022-03-25T12:16:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-25-hackerone-kicks-kasperskys-bug-bounty-program-off-its-platform

[Source](https://www.bleepingcomputer.com/news/security/hackerone-kicks-kaspersky-s-bug-bounty-program-off-its-platform/){:target="_blank" rel="noopener"}

> Bug bounty platform HackerOne disabled Kaspersky's bug bounty program on Friday following sanctions imposed on Russia and Belarus after the invasion of Ukraine. [...]
