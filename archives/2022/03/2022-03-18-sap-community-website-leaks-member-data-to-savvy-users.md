Title: SAP community website leaks member data to savvy users
Date: 2022-03-18T11:49:06+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-03-18-sap-community-website-leaks-member-data-to-savvy-users

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/18/sap_customer_influence_leak/){:target="_blank" rel="noopener"}

> Database dump accessible via the OData protocol, but software giant says it's working as intended A website for SAP's Customer Influence programs is exposing member data, creating the possibility for targeted social-engineering attacks.... [...]
