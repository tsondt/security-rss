Title: REvil ransomware member extradited to U.S. to stand trial for Kaseya attack
Date: 2022-03-10T13:23:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security;Government
Slug: 2022-03-10-revil-ransomware-member-extradited-to-us-to-stand-trial-for-kaseya-attack

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-member-extradited-to-us-to-stand-trial-for-kaseya-attack/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice announced that alleged REvil ransomware affiliate, Yaroslav Vasinskyi, was extradited to the United States last week to stand trial for the Kaseya cyberattack. [...]
