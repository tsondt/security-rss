Title: Hackers use modified MFA tool against Indian govt employees
Date: 2022-03-29T12:29:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-29-hackers-use-modified-mfa-tool-against-indian-govt-employees

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-modified-mfa-tool-against-indian-govt-employees/){:target="_blank" rel="noopener"}

> A new campaign from the hacking group tracked as APT36, aka 'Transparent Tribe' or' Mythic Leopard,' has been discovered using new custom malware and entry vectors in attacks against the Indian government. [...]
