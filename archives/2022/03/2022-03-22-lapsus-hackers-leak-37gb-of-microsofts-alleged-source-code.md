Title: Lapsus$ hackers leak 37GB of Microsoft's alleged source code
Date: 2022-03-22T02:27:21-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-03-22-lapsus-hackers-leak-37gb-of-microsofts-alleged-source-code

[Source](https://www.bleepingcomputer.com/news/microsoft/lapsus-hackers-leak-37gb-of-microsofts-alleged-source-code/){:target="_blank" rel="noopener"}

> The Lapsus$ hacking group claims to have leaked the source code for Bing, Cortana, and other projects stolen from Microsoft's internal Azure DevOps server. [...]
