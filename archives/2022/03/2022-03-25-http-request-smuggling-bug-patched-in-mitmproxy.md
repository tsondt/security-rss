Title: HTTP request smuggling bug patched in mitmproxy
Date: 2022-03-25T11:58:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-25-http-request-smuggling-bug-patched-in-mitmproxy

[Source](https://portswigger.net/daily-swig/http-request-smuggling-bug-patched-in-mitmproxy){:target="_blank" rel="noopener"}

> Bug exploited inconsistencies between intermediary and backend servers [...]
