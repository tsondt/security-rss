Title: Critical Axeda vulnerabilities pose takeover risk to hundreds of IoT devices
Date: 2022-03-09T15:35:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-09-critical-axeda-vulnerabilities-pose-takeover-risk-to-hundreds-of-iot-devices

[Source](https://portswigger.net/daily-swig/critical-axeda-vulnerabilities-pose-takeover-risk-to-hundreds-of-iot-devices){:target="_blank" rel="noopener"}

> Serious supply chain threat posed to downstream medical devices in particular [...]
