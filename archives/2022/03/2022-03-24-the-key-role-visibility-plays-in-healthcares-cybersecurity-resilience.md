Title: The key role ‘visibility’ plays in healthcare’s cybersecurity resilience
Date: 2022-03-24T16:00:00+00:00
Author: Seth Rosenblatt
Category: GCP Security
Tags: Healthcare & Life Sciences;Identity & Security
Slug: 2022-03-24-the-key-role-visibility-plays-in-healthcares-cybersecurity-resilience

[Source](https://cloud.google.com/blog/products/identity-security/improve-visibility-to-make-healthcare-more-secure/){:target="_blank" rel="noopener"}

> When technology just works, it's easy to trust. But too often, we place our trust in technology that doesn’t deserve it. When we do this with technology to provide healthcare, we put the safety of patients and the security and reliability of our global healthcare system at risk. The institutions that make up our global healthcare system also place their trust in cybersecurity measures and technology to keep their systems running and repelling the unceasing wave of attacks they face. We often hear about the institutions that succumb to cyberattacks, but we don’t read much about the institutions that have [...]
