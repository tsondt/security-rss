Title: Ukraine says local govt sites hacked to push fake capitulation news
Date: 2022-03-03T13:45:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-03-ukraine-says-local-govt-sites-hacked-to-push-fake-capitulation-news

[Source](https://www.bleepingcomputer.com/news/security/ukraine-says-local-govt-sites-hacked-to-push-fake-capitulation-news/){:target="_blank" rel="noopener"}

> The Security Service of Ukraine (SSU) said today "enemy" hackers are using compromised local government and regional authorities' websites to push rumors that Ukraine surrendered and signed a peace treaty with Russia. [...]
