Title: Apple Safari empowers developers to mitigate web flaws with WebKit CSP enhancements
Date: 2022-03-18T11:53:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-18-apple-safari-empowers-developers-to-mitigate-web-flaws-with-webkit-csp-enhancements

[Source](https://portswigger.net/daily-swig/apple-safari-empowers-developers-to-mitigate-web-flaws-with-webkit-csp-enhancements){:target="_blank" rel="noopener"}

> Apple praised for changes that ‘allow developers to build safe web applications’ [...]
