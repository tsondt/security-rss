Title: A Detailed Look at the Conti Ransomware Gang
Date: 2022-03-29T11:02:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;leaks;ransomware;Russia;Ukraine
Slug: 2022-03-29-a-detailed-look-at-the-conti-ransomware-gang

[Source](https://www.schneier.com/blog/archives/2022/03/a-detailed-look-at-the-conti-ransomware-gang.html){:target="_blank" rel="noopener"}

> Based on two years of leaked messages, 60,000 in all: The Conti ransomware gang runs like any number of businesses around the world. It has multiple departments, from HR and administrators to coders and researchers. It has policies on how its hackers should process their code, and shares best practices to keep the group’s members hidden from law enforcement. [...]
