Title: Where’s the Russia-Ukraine Cyberwar?
Date: 2022-03-10T12:06:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberwar;Russia;Ukraine
Slug: 2022-03-10-wheres-the-russia-ukraine-cyberwar

[Source](https://www.schneier.com/blog/archives/2022/03/wheres-the-russia-ukraine-cyberwar.html){:target="_blank" rel="noopener"}

> It has been interesting to notice how unimportant and ineffective cyber operations have been in the Russia-Ukraine war. Russia launched a wiper against Ukraine at the beginning, but it was found and neutered. Near as I can tell, the only thing that worked was the disabling of regional KA-SAT SATCOM terminals. It’s probably too early to reach any conclusions, but people are starting to write about this, with varying theories. I want to write about this, too, but I’m waiting for things to progress more. [...]
