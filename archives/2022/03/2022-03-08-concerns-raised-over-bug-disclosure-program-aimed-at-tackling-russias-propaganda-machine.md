Title: Concerns raised over bug disclosure program aimed at tackling Russia’s ‘propaganda machine’
Date: 2022-03-08T14:07:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-08-concerns-raised-over-bug-disclosure-program-aimed-at-tackling-russias-propaganda-machine

[Source](https://portswigger.net/daily-swig/concerns-raised-over-bug-disclosure-program-aimed-at-tackling-russias-propaganda-machine){:target="_blank" rel="noopener"}

> Some cybersecurity professionals express unease about ‘red team’ VDP launched alongside defense-focused program [...]
