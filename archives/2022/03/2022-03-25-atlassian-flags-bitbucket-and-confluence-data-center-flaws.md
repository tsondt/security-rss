Title: Atlassian flags Bitbucket and Confluence Data Center flaws
Date: 2022-03-25T15:45:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-03-25-atlassian-flags-bitbucket-and-confluence-data-center-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/25/atlassian_hazelcast/){:target="_blank" rel="noopener"}

> Cluster tech vulnerability means either patching or port tinkering could be on the cards Atlassian has demonstrated the interconnectedness of all things with a warning that some versions of Bitbucket Data Center and Confluence Data Center require patching courtesy of the Hazelcast Java deserialization vulnerability.... [...]
