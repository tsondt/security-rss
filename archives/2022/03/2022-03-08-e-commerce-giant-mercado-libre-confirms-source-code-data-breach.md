Title: E-commerce giant Mercado Libre confirms source code data breach
Date: 2022-03-08T06:51:45-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-08-e-commerce-giant-mercado-libre-confirms-source-code-data-breach

[Source](https://www.bleepingcomputer.com/news/security/e-commerce-giant-mercado-libre-confirms-source-code-data-breach/){:target="_blank" rel="noopener"}

> E-commerce giant Mercado Libre has confirmed "unauthorized access" to a part of its source code this week. Mercado additionally says data of around 300,000 of its users was accessed by threat actors. [...]
