Title: Zlib crash-an-app bug finally squashed, 17 years later
Date: 2022-03-30T23:33:52+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-30-zlib-crash-an-app-bug-finally-squashed-17-years-later

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/30/zlib_data_bug/){:target="_blank" rel="noopener"}

> Better late than never The widely used Zlib data-compression library finally has a patch to close a vulnerability that could be exploited to crash applications and services — four years after the vulnerability was first discovered but effectively left unfixed.... [...]
