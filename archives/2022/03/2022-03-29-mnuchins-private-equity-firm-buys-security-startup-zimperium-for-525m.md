Title: Mnuchin's private equity firm buys security startup Zimperium for $525m
Date: 2022-03-29T16:00:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-03-29-mnuchins-private-equity-firm-buys-security-startup-zimperium-for-525m

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/29/mnuchin_equity_firm_zimperium/){:target="_blank" rel="noopener"}

> Mnuchin to be board chair of Softbank-backed security startup Zimperium Former US Treasury secretary Steve Mnuchin's private equity firm has announced its plans to buy a controlling stake in a mobile cybersecurity company for more than half a billion dollars.... [...]
