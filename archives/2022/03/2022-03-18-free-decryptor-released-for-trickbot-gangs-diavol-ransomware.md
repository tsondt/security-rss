Title: Free decryptor released for TrickBot gang's Diavol ransomware
Date: 2022-03-18T15:35:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-18-free-decryptor-released-for-trickbot-gangs-diavol-ransomware

[Source](https://www.bleepingcomputer.com/news/security/free-decryptor-released-for-trickbot-gangs-diavol-ransomware/){:target="_blank" rel="noopener"}

> Cybersecurity firm Emsisoft has released a free decryption tool to help Diavol ransomware victims recover their files without paying a ransom. [...]
