Title: Zero trust? Not yet a must for most IT departments
Date: 2022-03-03T11:26:10+00:00
Author: Dave Cartwright
Category: The Register
Tags: 
Slug: 2022-03-03-zero-trust-not-yet-a-must-for-most-it-departments

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/03/reg_reader_survey_zero_trust_results/){:target="_blank" rel="noopener"}

> One-in-ten respondents hadn't even heard of it Reader survey results When we published the questions for this survey, our view was that zero trust, or ZT, has finally begun to become a thing – as a real technology in real companies.... [...]
