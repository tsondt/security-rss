Title: Cryptomining groups fight fiercely for cloud resources
Date: 2022-03-31T06:27:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-31-cryptomining-groups-fight-fiercely-for-cloud-resources

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/31/cryptomining_cloud_trend/){:target="_blank" rel="noopener"}

> Oh look, someone else who thinks on-prem is old hat Cryptocurrency mining groups that typically have targeted on-premises servers are now competing fiercely for servers in the cloud.... [...]
