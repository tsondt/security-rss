Title: Hackers Gaining Power of Subpoena Via Fake “Emergency Data Requests”
Date: 2022-03-29T14:07:27+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;The Coming Storm;Web Fraud 2.0;Anitsu;data extortion;Discord;Doxbin;emergency data request;Everlynn;fake EDR;fbi;KT;LAPSUS$;mark rasch;microsoft;Miku;Oklaqq;Palo Alto Networks;pompompurin;Recursion Team;Unit 221B;White;WhiteDoxbin
Slug: 2022-03-29-hackers-gaining-power-of-subpoena-via-fake-emergency-data-requests

[Source](https://krebsonsecurity.com/2022/03/hackers-gaining-power-of-subpoena-via-fake-emergency-data-requests/){:target="_blank" rel="noopener"}

> There is a terrifying and highly effective “method” that criminal hackers are now using to harvest sensitive customer data from Internet service providers, phone companies and social media firms. It involves compromising email accounts and websites tied to police departments and government agencies, and then sending unauthorized demands for subscriber data while claiming the information being requested can’t wait for a court order because it relates to an urgent matter of life and death. In the United States, when federal, state or local law enforcement agencies wish to obtain information about who owns an account at a social media firm, [...]
