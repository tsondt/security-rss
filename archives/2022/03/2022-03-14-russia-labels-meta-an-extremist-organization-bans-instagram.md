Title: Russia labels Meta an 'extremist' organization, bans Instagram
Date: 2022-03-14T05:59:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-14-russia-labels-meta-an-extremist-organization-bans-instagram

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/14/russia_meta/){:target="_blank" rel="noopener"}

> As Ukraine calls for big tech to end support for its products in Russia Russia's Investigative Committee, the nation's peak criminal and anti-corruption investigation body, has opened a probe into whether Meta is an extremist organization.... [...]
