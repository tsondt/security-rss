Title: A Blockchain Primer and a Bored Ape Headscratcher – Podcast
Date: 2022-03-31T13:00:09+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;News;Podcasts;Web Security
Slug: 2022-03-31-a-blockchain-primer-and-a-bored-ape-headscratcher-podcast

[Source](https://threatpost.com/a-blockchain-primer-and-a-bored-ape-headscratcher-podcast/179179/){:target="_blank" rel="noopener"}

> Mystified? Now’s the time to learn about cryptocurrency-associated risks: Listen to KnowBe4’s Dr. Lydia Kostopoulos explain blockchain, NFTs and how to stay safe. [...]
