Title: Okta confirms support engineer's laptop was hacked in January
Date: 2022-03-22T18:52:20-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-22-okta-confirms-support-engineers-laptop-was-hacked-in-january

[Source](https://www.bleepingcomputer.com/news/security/okta-confirms-support-engineers-laptop-was-hacked-in-january/){:target="_blank" rel="noopener"}

> Okta, a major provider of access management systems, has completed its investigation into a breach incident claimed by the Lapsus$ data extortion group. [...]
