Title: Reporting Mandates to Clear Up Feds’ Hazy Look into Threat Landscape – Podcast
Date: 2022-03-17T13:00:38+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Critical Infrastructure;Government;IoT;Malware;Podcasts;Vulnerabilities;Web Security
Slug: 2022-03-17-reporting-mandates-to-clear-up-feds-hazy-look-into-threat-landscape-podcast

[Source](https://threatpost.com/reporting-mandates-to-clear-up-feds-hazy-look-into-threat-landscape-podcast/178947/){:target="_blank" rel="noopener"}

> It’s about time, AttackIQ’s Jonathan Reiber said about 24H/72H report deadlines mandated in the new spending bill. As it is, visibility into adversary behavior has been muck. [...]
