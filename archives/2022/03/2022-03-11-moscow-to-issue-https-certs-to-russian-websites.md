Title: Moscow to issue HTTPS certs to Russian websites
Date: 2022-03-11T04:55:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-11-moscow-to-issue-https-certs-to-russian-websites

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/11/russian_ca/){:target="_blank" rel="noopener"}

> Meanwhile, Anonymous claims it's popped Putin's comms regulator Moscow has set up its own certificate authority to issue TLS certs to Russians affected by sanctions or otherwise punished for president Putin's invasion of Ukraine.... [...]
