Title: Sophos fixes critical hijack flaw in firewall offering
Date: 2022-03-28T19:56:54+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-28-sophos-fixes-critical-hijack-flaw-in-firewall-offering

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/28/sophos-firewall-rce-vulnerability/){:target="_blank" rel="noopener"}

> Authentication bypass followed by remote-code execution at the network boundary Sophos has patched a remote code execution (RCE) vulnerability in its firewall gear that was disclosed via its bug-bounty program.... [...]
