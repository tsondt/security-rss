Title: Network cavity blamed for data breach at Japanese candy maker Morinaga
Date: 2022-03-29T15:27:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-29-network-cavity-blamed-for-data-breach-at-japanese-candy-maker-morinaga

[Source](https://portswigger.net/daily-swig/network-cavity-blamed-for-data-breach-at-japanese-candy-maker-morinaga){:target="_blank" rel="noopener"}

> More than 1.6m affected by suspected compromise that ‘locked up’ servers [...]
