Title: Estonian ransomware operator sentenced to 66 months in prison
Date: 2022-03-25T14:02:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-25-estonian-ransomware-operator-sentenced-to-66-months-in-prison

[Source](https://www.bleepingcomputer.com/news/security/estonian-ransomware-operator-sentenced-to-66-months-in-prison/){:target="_blank" rel="noopener"}

> Maksim Berezan, an Estonian man linked to multimillion-dollar ransomware attacks, was sentenced on Friday to 66 months in prison for his involvement in online fraud schemes. [...]
