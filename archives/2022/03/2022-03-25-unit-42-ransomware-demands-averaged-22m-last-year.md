Title: Unit 42: Ransomware demands averaged $2.2m last year
Date: 2022-03-25T19:50:19+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-25-unit-42-ransomware-demands-averaged-22m-last-year

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/25/ransomware_unit_42_report/){:target="_blank" rel="noopener"}

> Conti, REvil declared most active criminal gangs The average ransom demand hit $2.2 million in 2021, a 144 percent rise from the year prior, according to Palo Alto Networks' Unit 42 consultants, while the average ransom payment grew 78 percent to $541,010.... [...]
