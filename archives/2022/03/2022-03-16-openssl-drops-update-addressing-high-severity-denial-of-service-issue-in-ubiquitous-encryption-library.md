Title: OpenSSL drops update addressing ‘high severity’ denial of service issue in ubiquitous encryption library
Date: 2022-03-16T10:22:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-16-openssl-drops-update-addressing-high-severity-denial-of-service-issue-in-ubiquitous-encryption-library

[Source](https://portswigger.net/daily-swig/openssl-drops-update-addressing-high-severity-denial-of-service-issue-in-ubiquitous-encryption-library){:target="_blank" rel="noopener"}

> The race is on for maintainers of downstream applications [...]
