Title: RagnarLocker ransomware struck 52 critical infrastructure entities within two years – FBI
Date: 2022-03-10T15:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-10-ragnarlocker-ransomware-struck-52-critical-infrastructure-entities-within-two-years-fbi

[Source](https://portswigger.net/daily-swig/ragnarlocker-ransomware-struck-52-critical-infrastructure-entities-within-two-years-fbi){:target="_blank" rel="noopener"}

> Agency issues mitigation advice to help organizations tighten network defenses [...]
