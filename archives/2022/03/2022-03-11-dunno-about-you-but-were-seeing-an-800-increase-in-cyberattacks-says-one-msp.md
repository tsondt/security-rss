Title: Dunno about you, but we're seeing an 800% increase in cyberattacks, says one MSP
Date: 2022-03-11T17:40:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-11-dunno-about-you-but-were-seeing-an-800-increase-in-cyberattacks-says-one-msp

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/11/russia-invasion-cyber-war-rages/){:target="_blank" rel="noopener"}

> Cyberwarfare escalates for some as Russia continues to invade Ukraine Revenge and inflation are key drivers behind an 800 percent increase in cyberattacks seen by a managed services provider since the days before the onset of Russia's invasion of Ukraine last month, according to the company's top executive.... [...]
