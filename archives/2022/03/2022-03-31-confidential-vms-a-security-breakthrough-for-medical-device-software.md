Title: Confidential VMs - a security breakthrough for medical device software
Date: 2022-03-31T16:30:00+00:00
Author: Jerry Valentine
Category: GCP Security
Tags: Compute;Infrastructure;Identity & Security
Slug: 2022-03-31-confidential-vms-a-security-breakthrough-for-medical-device-software

[Source](https://cloud.google.com/blog/products/identity-security/securing-medical-device-software-with-google-confidential-vm/){:target="_blank" rel="noopener"}

> Editor's note: This is a guest blog by Idea Evolver and AstraZeneca Delivering better healthcare is increasingly dependent on technology. Recently, there has been movement towards self-managed healthcare via remote technology – a trend that accelerated during the COVID-19 pandemic. Examples of this are initiatives that directly empower consumers to obtain preventive therapies without a physician’s prescription. AstraZeneca, a global, science-led, biopharmaceutical company, is currently recruiting for the Technology-Assisted Cholesterol Trial in Consumers (TACTiC). TACTiC is a Software as a Medical Device (SaMD) application designed to ensure that only the candidates in the trial with an appropriate level of risk [...]
