Title: Automotive giant DENSO hit by new Pandora ransomware gang
Date: 2022-03-14T10:26:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-14-automotive-giant-denso-hit-by-new-pandora-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/automotive-giant-denso-hit-by-new-pandora-ransomware-gang/){:target="_blank" rel="noopener"}

> DENSO has published an announcement to confirm that its German business computer network was accessed by an unauthorized third party on March 10, 2022, resulting in a data breach. [...]
