Title: VPN provider bans BitTorrent after getting sued by film studios
Date: 2022-03-12T11:01:01-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-12-vpn-provider-bans-bittorrent-after-getting-sued-by-film-studios

[Source](https://www.bleepingcomputer.com/news/security/vpn-provider-bans-bittorrent-after-getting-sued-by-film-studios/){:target="_blank" rel="noopener"}

> "No logs" VPN provider TorGuard has reached a legal settlement with over two dozen movie studios that sued the company for encouraging piracy and copyright infringement. In the settlement, TorGuard has agreed to block BitTorrent traffic for its users. [...]
