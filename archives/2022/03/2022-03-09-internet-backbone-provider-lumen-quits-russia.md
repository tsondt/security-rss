Title: Internet backbone provider Lumen quits Russia
Date: 2022-03-09T06:01:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-09-internet-backbone-provider-lumen-quits-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/09/lumen_quits_russia/){:target="_blank" rel="noopener"}

> Disconnects small group of customers to protect 'integrity of the global internet' Lumen Technologies, the internet backbone provider formerly known as CenturyLink, has quit Russia.... [...]
