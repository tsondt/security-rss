Title: FBI: Avoslocker ransomware targets US critical infrastructure
Date: 2022-03-19T10:07:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-19-fbi-avoslocker-ransomware-targets-us-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/fbi-avoslocker-ransomware-targets-us-critical-infrastructure/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warns of AvosLocker ransomware being used in attacks targeting multiple US critical infrastructure sectors. [...]
