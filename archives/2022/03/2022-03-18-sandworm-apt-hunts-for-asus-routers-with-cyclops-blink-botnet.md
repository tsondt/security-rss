Title: Sandworm APT Hunts for ASUS Routers with Cyclops Blink Botnet
Date: 2022-03-18T17:17:17+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;IoT;Malware;Vulnerabilities;Web Security
Slug: 2022-03-18-sandworm-apt-hunts-for-asus-routers-with-cyclops-blink-botnet

[Source](https://threatpost.com/sandworm-asus-routers-cyclops-blink-botnet/178986/){:target="_blank" rel="noopener"}

> The Russian-speaking APT behind the NotPetya attacks and the Ukrainian power grid takedown could be setting up for additional sinister attacks, researchers said. [...]
