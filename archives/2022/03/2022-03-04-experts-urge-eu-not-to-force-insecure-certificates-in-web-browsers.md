Title: Experts urge EU not to force insecure certificates in web browsers
Date: 2022-03-04T15:00:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-03-04-experts-urge-eu-not-to-force-insecure-certificates-in-web-browsers

[Source](https://www.bleepingcomputer.com/news/security/experts-urge-eu-not-to-force-insecure-certificates-in-web-browsers/){:target="_blank" rel="noopener"}

> A group of 38 cybersecurity professors and IT experts worldwide, together with the Electronic Frontier Foundation (EFF), have cosigned a letter to EU regulators that warns of a proposal that could expose internet users to cybercrime. [...]
