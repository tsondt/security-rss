Title: An update on Google Cloud’s commitments to E.U. businesses in light of the new E.U.-U.S. data transfer framework
Date: 2022-03-28T23:00:00+00:00
Author: Nathaly Rey
Category: GCP Security
Tags: Compliance;Google Cloud in Europe;Identity & Security
Slug: 2022-03-28-an-update-on-google-clouds-commitments-to-eu-businesses-in-light-of-the-new-eu-us-data-transfer-framework

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-helps-eu-companies-under-new-data-transfer-rules/){:target="_blank" rel="noopener"}

> Last week, the European Commission and U.S. Government agreed on a new E.U.-U.S. data transfer framework. Earlier today, Google shared that it welcomes these efforts by the U.S. government to enhance privacy protections for E.U. data and facilitate trusted transatlantic data flows. For our Google Cloud customers, we intend to make the protections offered by this new framework available once it is implemented. Last year, we reaffirmed our commitment to E.U. businesses after the European Data Protection Board (EDPB) issued its Recommendations on Supplementary Measures, following Europe’s top court ruling invalidating the E.U.-U.S. Privacy Shield Framework and upholding the E.U. [...]
