Title: The Week in Ransomware - March 4th 2022 - The Conti Leaks
Date: 2022-03-04T18:46:31-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-04-the-week-in-ransomware-march-4th-2022-the-conti-leaks

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-4th-2022-the-conti-leaks/){:target="_blank" rel="noopener"}

> This week's biggest story is the massive data leak from the Conti ransomware operation, including over 160,000 internal messages between members and source code for the ransomware and TrickBot operation. [...]
