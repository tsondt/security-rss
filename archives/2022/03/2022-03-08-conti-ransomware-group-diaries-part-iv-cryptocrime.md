Title: Conti Ransomware Group Diaries, Part IV: Cryptocrime
Date: 2022-03-08T01:38:36+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Ransomware;Athens University School of Information Sciences and Technolog;Begemot;Bloodrush;Chainalysis;Conti ransomware;DDoS;Demon;Ghost;Gizmodo;Jeffrey Ladish;Mango;Matt Novak;pump-and-dump;SQUID;Stern;Van
Slug: 2022-03-08-conti-ransomware-group-diaries-part-iv-cryptocrime

[Source](https://krebsonsecurity.com/2022/03/conti-ransomware-group-diaries-part-iv-cryptocrime/){:target="_blank" rel="noopener"}

> Three stories here last week pored over several years’ worth of internal chat records stolen from the Conti ransomware group, the most profitable ransomware gang in operation today. The candid messages revealed how Conti evaded law enforcement and intelligence agencies, what it was like on a typical day at the Conti office, and how Conti secured the digital weaponry used in their attacks. This final post on the Conti conversations explores different schemes that Conti pursued to invest in and steal cryptocurrencies. When you’re perhaps the most successful ransomware group around — Conti made $180 million last year in extortion [...]
