Title: EU, US close to replacing defunct Privacy Shield II
Date: 2022-03-02T16:40:09+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-03-02-eu-us-close-to-replacing-defunct-privacy-shield-ii

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/02/new_hope_for_privacy_shield/){:target="_blank" rel="noopener"}

> Fresh data transfer pact in the works for spring The State of the Net conference in Washington, DC, has heard officials representing the EU and the US say they believe they are close to reaching a data-sharing agreement to replace Privacy Shield.... [...]
