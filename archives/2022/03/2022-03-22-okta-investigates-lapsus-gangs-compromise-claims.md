Title: Okta investigates LAPSUS$ gang’s compromise claims
Date: 2022-03-22T17:17:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-22-okta-investigates-lapsus-gangs-compromise-claims

[Source](https://portswigger.net/daily-swig/okta-investigates-lapsus-gangs-compromise-claims){:target="_blank" rel="noopener"}

> Attackers purportedly said ‘focus was only on Okta customers’ [...]
