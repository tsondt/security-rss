Title: Infosys, Wipro silent on their Russian operations
Date: 2022-03-11T08:01:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-03-11-infosys-wipro-silent-on-their-russian-operations

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/11/infosys_wipro_russia_silence/){:target="_blank" rel="noopener"}

> IT giants reportedly contemplating relocation of Eastern European ops Indian IT services giants Infosys and Wipro both operate offices in Russia - and neither is saying what will become of them.... [...]
