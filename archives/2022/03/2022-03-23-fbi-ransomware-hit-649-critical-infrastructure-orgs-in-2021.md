Title: FBI: Ransomware hit 649 critical infrastructure orgs in 2021
Date: 2022-03-23T15:00:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-23-fbi-ransomware-hit-649-critical-infrastructure-orgs-in-2021

[Source](https://www.bleepingcomputer.com/news/security/fbi-ransomware-hit-649-critical-infrastructure-orgs-in-2021/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) says ransomware gangs have breached the networks of at least 649 organizations from multiple US critical infrastructure sectors last year, according to the Internet Crime Complaint Center (IC3) 2021 Internet Crime Report. [...]
