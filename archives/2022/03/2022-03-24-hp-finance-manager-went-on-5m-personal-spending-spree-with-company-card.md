Title: HP finance manager went on $5m personal spending spree with company card
Date: 2022-03-24T19:11:40+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-03-24-hp-finance-manager-went-on-5m-personal-spending-spree-with-company-card

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/24/hp_finance_manager_expense_fraud/){:target="_blank" rel="noopener"}

> Tesla, Porche, jewelry, bags, purses... not quite enough for ink, though A now-former HP finance planning manager pleaded guilty on Wednesday to charges of wire fraud, money laundering, and filing false tax returns that follow from the misappropriation of company funds.... [...]
