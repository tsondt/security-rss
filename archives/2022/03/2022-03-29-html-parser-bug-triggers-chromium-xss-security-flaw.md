Title: HTML parser bug triggers Chromium XSS security flaw
Date: 2022-03-29T13:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-29-html-parser-bug-triggers-chromium-xss-security-flaw

[Source](https://portswigger.net/daily-swig/html-parser-bug-triggers-chromium-xss-security-flaw){:target="_blank" rel="noopener"}

> Websites thought to be XSS-protected could have been unintentionally exposed to XSS attacks in Chrome sessions [...]
