Title: Conti Ransomware Group Diaries, Part II: The Office
Date: 2022-03-02T17:49:52+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;The Coming Storm;Bentley;Bleeping Computer;Conti;Emotet;Hof;Lawrence Abrams;Mango;Palo Alto Networks;ransomware;Reverse;Ryuk;Stern;trickbot
Slug: 2022-03-02-conti-ransomware-group-diaries-part-ii-the-office

[Source](https://krebsonsecurity.com/2022/03/conti-ransomware-group-diaries-part-ii-the-office/){:target="_blank" rel="noopener"}

> Earlier this week, a Ukrainian security researcher leaked almost two years’ worth of internal chat logs from Conti, one of the more rapacious and ruthless ransomware gangs in operation today. Tuesday’s story examined how Conti dealt with its own internal breaches and attacks from private security firms and governments. In Part II of this series we’ll explore what it’s like to work for Conti, as described by the Conti employees themselves. The Conti group’s chats reveal a great deal about its internal structure and hierarchy. Conti maintains many of the same business units as a legitimate, small- to medium-sized enterprise, [...]
