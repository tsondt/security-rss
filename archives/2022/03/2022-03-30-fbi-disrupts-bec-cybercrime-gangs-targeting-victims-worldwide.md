Title: FBI disrupts BEC cybercrime gangs targeting victims worldwide
Date: 2022-03-30T12:13:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2022-03-30-fbi-disrupts-bec-cybercrime-gangs-targeting-victims-worldwide

[Source](https://www.bleepingcomputer.com/news/legal/fbi-disrupts-bec-cybercrime-gangs-targeting-victims-worldwide/){:target="_blank" rel="noopener"}

> A coordinated operation conducted by the FBI and its international law enforcement partners has resulted in disrupting business email compromise (BEC) schemes in several countries. [...]
