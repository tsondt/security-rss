Title: US and Canada reinstate cybercrime forum to prevent Russian cyber-attacks
Date: 2022-03-23T13:57:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-23-us-and-canada-reinstate-cybercrime-forum-to-prevent-russian-cyber-attacks

[Source](https://portswigger.net/daily-swig/us-and-canada-reinstate-cybercrime-forum-to-prevent-russian-cyber-attacks){:target="_blank" rel="noopener"}

> North American nations agree to share information on key security issues [...]
