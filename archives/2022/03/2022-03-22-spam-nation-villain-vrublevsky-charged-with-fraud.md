Title: ‘Spam Nation’ Villain Vrublevsky Charged With Fraud
Date: 2022-03-22T16:33:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Web Fraud 2.0;Aleksandr "Nastra" Zhukov;chronopay;Dmitry Artimovich;Igor Artimovich;pavel vrublevsky;redeye;Vladislav Horohorin
Slug: 2022-03-22-spam-nation-villain-vrublevsky-charged-with-fraud

[Source](https://krebsonsecurity.com/2022/03/spam-nation-villain-vrublevsky-charged-with-fraud/){:target="_blank" rel="noopener"}

> Pavel Vrublevsky, founder of the Russian payment technology firm ChronoPay and the antagonist in my 2014 book “ Spam Nation,” was arrested in Moscow this month and charged with fraud. Russian authorities allege Vrublevsky operated several fraudulent SMS-based payment schemes, and facilitated money laundering for Hydra, the largest Russian darknet market. But according to information obtained by KrebsOnSecurity, it is equally likely Vrublevsky was arrested thanks to his propensity for carefully documenting the links between Russia’s state security services and the cybercriminal underground. An undated photo of Vrublevsky at his ChronoPay office in Moscow. ChronoPay specializes in providing access to [...]
