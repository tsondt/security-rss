Title: Details of '120,000 Russian soldiers' leaked by Ukrainian media
Date: 2022-03-02T19:59:53+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-02-details-of-120000-russian-soldiers-leaked-by-ukrainian-media

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/02/russian_soldier_leaks/){:target="_blank" rel="noopener"}

> Verification hit and miss so far Ukrainian news website Ukrainska Pravda says the nation's Centre for Defence Strategies think tank has obtained the personal details of 120,000 Russian servicemen fighting in Ukraine. The publication has now shared this data freely on its website.... [...]
