Title: OpenSSL patches crash-me bug triggered by rogue certs
Date: 2022-03-15T20:40:18+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-03-15-openssl-patches-crash-me-bug-triggered-by-rogue-certs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/openssl_bug_dos/){:target="_blank" rel="noopener"}

> Bad data can throw vulnerable apps and services for an infinite loop A bug in OpenSSL certificate parsing leaves systems open to denial-of-service attacks from anyone wielding an explicit curve.... [...]
