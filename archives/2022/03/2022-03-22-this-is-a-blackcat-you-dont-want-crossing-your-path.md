Title: This is a BlackCat you don't want crossing your path
Date: 2022-03-22T05:29:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-22-this-is-a-blackcat-you-dont-want-crossing-your-path

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/22/talos-ransomware-blackcat/){:target="_blank" rel="noopener"}

> Plus: Android trojan in 100,000+ app installs, Solaris malware Cybersecurity researchers with Cisco have outlined probable links between the BlackMatter/DarkSide ransomware ring responsible for last year's high-profile raid on the Colonial Pipeline, and an emerging ransomware-as-a-service product dubbed BlackCat.... [...]
