Title: DDoS attacks now use new record-breaking amplification vector
Date: 2022-03-08T10:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-08-ddos-attacks-now-use-new-record-breaking-amplification-vector

[Source](https://www.bleepingcomputer.com/news/security/ddos-attacks-now-use-new-record-breaking-amplification-vector/){:target="_blank" rel="noopener"}

> A new reflection/amplification DDoS vector has been spotted in the wild, offering threat actors a record-breaking amplification ratio of almost 4.3 billion to 1. [...]
