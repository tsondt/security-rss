Title: 1Password increases bug bounty reward to $1 million
Date: 2022-03-10T14:45:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-10-1password-increases-bug-bounty-reward-to-1-million

[Source](https://portswigger.net/daily-swig/1password-increases-bug-bounty-reward-to-1-million){:target="_blank" rel="noopener"}

> Researchers offered record incentive for vulnerabilities found on Bugcrowd programs [...]
