Title: Bridgestone Americas confirms ransomware attack, LockBit leaks data
Date: 2022-03-11T16:28:15-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-11-bridgestone-americas-confirms-ransomware-attack-lockbit-leaks-data

[Source](https://www.bleepingcomputer.com/news/security/bridgestone-americas-confirms-ransomware-attack-lockbit-leaks-data/){:target="_blank" rel="noopener"}

> A cyberattack on Bridgestone Americas, one of the largest manufacturers of tires in the world, has been claimed by the LockBit ransomware gang. [...]
