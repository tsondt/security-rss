Title: Microsoft investigating claims of hacked source code repositories
Date: 2022-03-21T10:34:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-21-microsoft-investigating-claims-of-hacked-source-code-repositories

[Source](https://www.bleepingcomputer.com/news/security/microsoft-investigating-claims-of-hacked-source-code-repositories/){:target="_blank" rel="noopener"}

> Microsoft says they are investigating claims that the Lapsus$ data extortion hacking group breached their internal Azure DevOps source code repositories and stolen data. [...]
