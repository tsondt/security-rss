Title: Hacking Alexa through Alexa’s Speech
Date: 2022-03-07T12:20:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;Amazon;hacking;voice recognition
Slug: 2022-03-07-hacking-alexa-through-alexas-speech

[Source](https://www.schneier.com/blog/archives/2022/03/hacking-alexa-through-alexas-speech.html){:target="_blank" rel="noopener"}

> An Alexa can respond to voice commands it issues. This can be exploited : The attack works by using the device’s speaker to issue voice commands. As long as the speech contains the device wake word (usually “Alexa” or “Echo”) followed by a permissible command, the Echo will carry it out, researchers from Royal Holloway University in London and Italy’s University of Catania found. Even when devices require verbal confirmation before executing sensitive commands, it’s trivial to bypass the measure by adding the word “yes” about six seconds after issuing the command. Attackers can also exploit what the researchers call [...]
