Title: UK ferry operator Wightlink flags potential data breach after ‘highly sophisticated’ cyber-attack
Date: 2022-03-11T13:56:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-11-uk-ferry-operator-wightlink-flags-potential-data-breach-after-highly-sophisticated-cyber-attack

[Source](https://portswigger.net/daily-swig/uk-ferry-operator-wightlink-flags-potential-data-breach-after-highly-sophisticated-cyber-attack){:target="_blank" rel="noopener"}

> Personal data potentially compromised, but English Channel crossings unaffected [...]
