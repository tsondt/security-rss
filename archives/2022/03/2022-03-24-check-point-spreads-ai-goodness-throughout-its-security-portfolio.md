Title: Check Point spreads AI goodness throughout its security portfolio
Date: 2022-03-24T06:43:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-24-check-point-spreads-ai-goodness-throughout-its-security-portfolio

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/24/checkpoint_ai_nvidia_cybersecurity/){:target="_blank" rel="noopener"}

> Evolving threat environment means preventative AI – and SmartNICs – are needed to improve protection GTC Check Point Software has put Nvidia GPUs and artificial intelligence techniques to work across its broad portfolio of security tools in order to address and adapt better to an increasingly sophisticated and rapidly changing threat environment.... [...]
