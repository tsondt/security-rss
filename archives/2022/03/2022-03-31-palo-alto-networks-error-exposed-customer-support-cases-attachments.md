Title: Palo Alto Networks error exposed customer support cases, attachments
Date: 2022-03-31T09:00:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-31-palo-alto-networks-error-exposed-customer-support-cases-attachments

[Source](https://www.bleepingcomputer.com/news/security/palo-alto-networks-error-exposed-customer-support-cases-attachments/){:target="_blank" rel="noopener"}

> EXCLUSIVE: A bug in the support dashboard of Palo Alto Networks (PAN) exposed thousands of customer support tickets to an unauthorized individual, BleepingComputer has learned. The exposed information included, customer names, contact information, conversations between staff and customers, firewall logs and configuration dumps. [...]
