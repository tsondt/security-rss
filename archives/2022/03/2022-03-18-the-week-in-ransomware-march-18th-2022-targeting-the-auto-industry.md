Title: The Week in Ransomware - March 18th 2022 - Targeting the auto industry
Date: 2022-03-18T16:11:57-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-18-the-week-in-ransomware-march-18th-2022-targeting-the-auto-industry

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-18th-2022-targeting-the-auto-industry/){:target="_blank" rel="noopener"}

> This week, the automotive industry has been under attack, with numerous companies exhibiting signs of breaches or ransomware activity. [...]
