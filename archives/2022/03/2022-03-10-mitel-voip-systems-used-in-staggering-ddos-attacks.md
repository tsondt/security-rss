Title: Mitel VoIP systems used in staggering DDoS attacks
Date: 2022-03-10T12:28:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-10-mitel-voip-systems-used-in-staggering-ddos-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/10/mitel_amplification_ddos_attack/){:target="_blank" rel="noopener"}

> One malicious network packet can theoretically spark billions more against a victim Miscreants have launched massive, amplified distributed denial-of-service attacks by exploiting a vulnerability in Mitel collaboration systems.... [...]
