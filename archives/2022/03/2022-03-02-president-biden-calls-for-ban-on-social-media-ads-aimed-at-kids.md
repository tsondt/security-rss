Title: President Biden calls for ban on social media ads aimed at kids
Date: 2022-03-02T04:59:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-02-president-biden-calls-for-ban-on-social-media-ads-aimed-at-kids

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/02/state_of_the_union_2022_tech_policy/){:target="_blank" rel="noopener"}

> State of the Union features call for Congress to pass law that could see Intel spend $100B on chip factories United States president Joe Biden has used his first State of the Union speech to call for a ban on social networks serving ads targeted at children.... [...]
