Title: Phishing kits constantly evolve to evade security software
Date: 2022-03-24T19:16:56-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-24-phishing-kits-constantly-evolve-to-evade-security-software

[Source](https://www.bleepingcomputer.com/news/security/phishing-kits-constantly-evolve-to-evade-security-software/){:target="_blank" rel="noopener"}

> Modern phishing kits sold on cybercrime forums as off-the-shelve packages feature multiple and sophisticated detection avoidance and traffic filtering systems to ensure that internet security solutions won't mark them as a threat. [...]
