Title: Cybercrooks target students with fake job opportunities
Date: 2022-03-29T10:45:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-03-29-cybercrooks-target-students-with-fake-job-opportunities

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/29/student_scams/){:target="_blank" rel="noopener"}

> Legit employers don't normally send a check before you've started – or ask you to send money to a Bitcoin address Scammers appear to be targeting university students looking to kickstart their careers, according to research from cybersecurity biz Proofpoint.... [...]
