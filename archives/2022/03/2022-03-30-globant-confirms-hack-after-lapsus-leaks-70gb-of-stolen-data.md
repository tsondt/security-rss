Title: Globant confirms hack after Lapsus$ leaks 70GB of stolen data
Date: 2022-03-30T14:47:51-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-30-globant-confirms-hack-after-lapsus-leaks-70gb-of-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/globant-confirms-hack-after-lapsus-leaks-70gb-of-stolen-data/){:target="_blank" rel="noopener"}

> IT and software consultancy firm Globant has confirmed that they were breached by the Lapsus$ data extortion group, where data consisting of administrator credentials and source code was leaked by the threat actors. [...]
