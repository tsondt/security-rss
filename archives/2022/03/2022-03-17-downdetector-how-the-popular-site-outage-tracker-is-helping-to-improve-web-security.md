Title: Downdetector: How the popular site outage tracker is helping to improve web security
Date: 2022-03-17T15:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-17-downdetector-how-the-popular-site-outage-tracker-is-helping-to-improve-web-security

[Source](https://portswigger.net/daily-swig/downdetector-how-the-popular-site-outage-tracker-is-helping-to-improve-web-security){:target="_blank" rel="noopener"}

> ‘Minutes matter, and being able to get that additional feed can give infosec teams the edge’ [...]
