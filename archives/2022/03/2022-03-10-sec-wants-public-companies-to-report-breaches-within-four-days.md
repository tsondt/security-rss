Title: SEC wants public companies to report breaches within four days
Date: 2022-03-10T13:03:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-03-10-sec-wants-public-companies-to-report-breaches-within-four-days

[Source](https://www.bleepingcomputer.com/news/security/sec-wants-public-companies-to-report-breaches-within-four-days/){:target="_blank" rel="noopener"}

> The US Securities and Exchange Commission (SEC) has proposed rule amendments to require publicly traded companies to report data breaches and other cybersecurity incidents within four days after they're determined as being a material incident (one that shareholders would likely consider important). [...]
