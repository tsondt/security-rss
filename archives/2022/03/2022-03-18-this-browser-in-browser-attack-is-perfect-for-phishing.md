Title: This browser-in-browser attack is perfect for phishing
Date: 2022-03-18T20:56:23+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-03-18-this-browser-in-browser-attack-is-perfect-for-phishing

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/18/browser_in_browser_phishing/){:target="_blank" rel="noopener"}

> If you're involved in malvertising, please don't read this. We don't want to give you ideas A novel way of tricking people out of their passwords has left us wondering if there's a need to rethink how much we trust our web browsers to protect us and to accelerate efforts to close web security gaps.... [...]
