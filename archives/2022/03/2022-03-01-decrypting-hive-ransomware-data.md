Title: Decrypting Hive Ransomware Data
Date: 2022-03-01T12:06:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cryptanalysis;encryption;ransomware
Slug: 2022-03-01-decrypting-hive-ransomware-data

[Source](https://www.schneier.com/blog/archives/2022/03/decrypting-hive-ransomware-data.html){:target="_blank" rel="noopener"}

> Nice piece of research : Abstract: Among the many types of malicious codes, ransomware poses a major threat. Ransomware encrypts data and demands a ransom in exchange for decryption. As data recovery is impossible if the encryption key is not obtained, some companies suffer from considerable damage, such as the payment of huge amounts of money or the loss of important data. In this paper, we analyzed Hive ransomware, which appeared in June 2021. Hive ransomware has caused immense harm, leading the FBI to issue an alert about it. To minimize the damage caused by Hive Ransomware and to help [...]
