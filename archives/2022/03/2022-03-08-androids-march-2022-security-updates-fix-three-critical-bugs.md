Title: Android's March 2022 security updates fix three critical bugs
Date: 2022-03-08T16:35:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2022-03-08-androids-march-2022-security-updates-fix-three-critical-bugs

[Source](https://www.bleepingcomputer.com/news/security/androids-march-2022-security-updates-fix-three-critical-bugs/){:target="_blank" rel="noopener"}

> Google has released the March 2022 security updates for Android 10, 11, and 12, addressing three critical severity flaws, one of which affects all devices running the latest version of the mobile OS. [...]
