Title: IT security is at crisis point – so what are you going to do about it?
Date: 2022-03-08T17:00:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-03-08-it-security-is-at-crisis-point-so-what-are-you-going-to-do-about-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/08/it_security_crisis_point/){:target="_blank" rel="noopener"}

> Clue: It should involve next-generation threat detection tools Webinar There’s one thing worse than having to face down the onslaught of cyber-attacks today’s security pros face.... [...]
