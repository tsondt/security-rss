Title: Stats widget hacked in attempt to breach Russian government agency websites
Date: 2022-03-11T16:06:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-11-stats-widget-hacked-in-attempt-to-breach-russian-government-agency-websites

[Source](https://portswigger.net/daily-swig/stats-widget-hacked-in-attempt-to-breach-russian-government-agency-websites){:target="_blank" rel="noopener"}

> The software was reportedly used as part of a short-lived software supply chain attack [...]
