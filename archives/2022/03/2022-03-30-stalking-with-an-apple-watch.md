Title: Stalking with an Apple Watch
Date: 2022-03-30T11:29:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;geolocation;stalking;surveillance;tracking
Slug: 2022-03-30-stalking-with-an-apple-watch

[Source](https://www.schneier.com/blog/archives/2022/03/stalking-with-an-apple-watch.html){:target="_blank" rel="noopener"}

> The malicious uses of these technologies are scary : Police reportedly arrived on the scene last week and found the man crouched beside the woman’s passenger side door. According to the police, the man had, at some point, wrapped his Apple Watch across the spokes of the woman’s passenger side front car wheel and then used the Watch to track her movements. When police eventually confronted him, he admitted the Watch was his. Now, he’s reportedly being charged with attaching an electronic tracking device to the woman’s vehicle. [...]
