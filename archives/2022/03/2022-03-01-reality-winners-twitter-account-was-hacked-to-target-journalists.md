Title: Reality Winner's Twitter account was hacked to target journalists
Date: 2022-03-01T05:46:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-01-reality-winners-twitter-account-was-hacked-to-target-journalists

[Source](https://www.bleepingcomputer.com/news/security/reality-winners-twitter-account-was-hacked-to-target-journalists/){:target="_blank" rel="noopener"}

> Twitter account of former intelligence specialist, Reality Winner was hacked over the weekend by threat actors looking to target journalists at prominent media organizations. After taking over Winner's verified Twitter account, hackers changed the profile name to "Feedback Team" to impersonate Twitter staff and began sending out DMs. [...]
