Title: Conti Ransomware Group Diaries, Part III: Weaponry
Date: 2022-03-04T20:20:29+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Ransomware;alarm;Bentley;Bio;Bloodrush;Chainalysis;Cobalt Strike;Conti;Grant;Kaktus;LeMans Corporation;PIN;ransomware;Reshaev;Revers;Salamandra;Skippy;The Spaniard;Tramp;Trickbotleaks;trump
Slug: 2022-03-04-conti-ransomware-group-diaries-part-iii-weaponry

[Source](https://krebsonsecurity.com/2022/03/conti-ransomware-group-diaries-part-iii-weaponry/){:target="_blank" rel="noopener"}

> Part I of this series examined newly-leaked internal chats from the Conti ransomware group, and how the crime gang dealt with its own internal breaches. Part II explored what it’s like to be an employee of Conti’s sprawling organization. Today’s Part III looks at how Conti abused a panoply of popular commercial security services to undermine the security of their targets, as well as how the team’s leaders strategized for the upper hand in ransom negotiations with victims. Conti is by far the most aggressive and profitable ransomware group in operation today. Image: Chainalysis Conti is by far the most [...]
