Title: Ukraine Hit with Novel ‘FoxBlade’ Trojan Hours Before Invasion
Date: 2022-03-01T16:55:47+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Malware;Web Security
Slug: 2022-03-01-ukraine-hit-with-novel-foxblade-trojan-hours-before-invasion

[Source](https://threatpost.com/microsoft-ukraine-foxblade-trojan-hours-before-russian-invasion/178702/){:target="_blank" rel="noopener"}

> Microsoft detected cyberattacks launched against Ukraine hours before Russia’s tanks and missiles began to pummel the country last week. [...]
