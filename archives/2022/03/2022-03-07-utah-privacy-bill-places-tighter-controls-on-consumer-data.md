Title: Utah privacy bill places tighter controls on consumer data
Date: 2022-03-07T14:37:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-07-utah-privacy-bill-places-tighter-controls-on-consumer-data

[Source](https://portswigger.net/daily-swig/utah-privacy-bill-places-tighter-controls-on-consumer-data){:target="_blank" rel="noopener"}

> Policymakers move forward with new data privacy legislation [...]
