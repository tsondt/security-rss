Title: New Windows security feature blocks vulnerable drivers
Date: 2022-03-28T14:02:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-03-28-new-windows-security-feature-blocks-vulnerable-drivers

[Source](https://www.bleepingcomputer.com/news/microsoft/new-windows-security-feature-blocks-vulnerable-drivers/){:target="_blank" rel="noopener"}

> Microsoft will allow Windows users to block drivers with known vulnerabilities with the help of Windows Defender Application Control (WDAC) and a vulnerable driver blocklist. [...]
