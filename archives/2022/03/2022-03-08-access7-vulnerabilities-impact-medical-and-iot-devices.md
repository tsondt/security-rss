Title: Access:7 vulnerabilities impact medical and IoT devices
Date: 2022-03-08T00:00:01-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-08-access7-vulnerabilities-impact-medical-and-iot-devices

[Source](https://www.bleepingcomputer.com/news/security/access-7-vulnerabilities-impact-medical-and-iot-devices/){:target="_blank" rel="noopener"}

> A set of seven vulnerabilities collectively tracked as Access:7 have been found in PTC's Axeda agent, a solution used for remote access and management of over 150 connected devices from more than 100 vendors. [...]
