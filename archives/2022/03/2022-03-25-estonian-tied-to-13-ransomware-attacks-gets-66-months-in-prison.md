Title: Estonian Tied to 13 Ransomware Attacks Gets 66 Months in Prison
Date: 2022-03-25T17:10:34+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;Albanec;cashout;DirectConnection;Maksim Berezan;U.S. Department of Justice;unlimited cashout
Slug: 2022-03-25-estonian-tied-to-13-ransomware-attacks-gets-66-months-in-prison

[Source](https://krebsonsecurity.com/2022/03/estonian-tied-to-13-ransomware-attacks-gets-66-months-in-prison/){:target="_blank" rel="noopener"}

> An Estonian man was sentenced today to more than five years in a U.S. prison for his role in at least 13 ransomware attacks that caused losses of approximately $53 million. Prosecutors say the accused also enjoyed a lengthy career of “cashing out” access to hacked bank accounts worldwide. Maksim Berezan, 37, is an Estonian national who was arrested nearly two years ago in Latvia. U.S. authorities alleged Berezan was a longtime member of DirectConnection, a closely-guarded Russian cybercriminal forum that existed until 2015. Berezan’s indictment (PDF) says he used his status at DirectConnection to secure cashout jobs from other [...]
