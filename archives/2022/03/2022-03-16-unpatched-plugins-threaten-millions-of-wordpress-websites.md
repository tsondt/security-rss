Title: Unpatched plugins threaten millions of WordPress websites
Date: 2022-03-16T16:20:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-16-unpatched-plugins-threaten-millions-of-wordpress-websites

[Source](https://portswigger.net/daily-swig/unpatched-plugins-threaten-millions-of-wordpress-websites){:target="_blank" rel="noopener"}

> Number of vulnerabilities found in WordPress plugins and themes jumped 150% between 2020 and 2021 [...]
