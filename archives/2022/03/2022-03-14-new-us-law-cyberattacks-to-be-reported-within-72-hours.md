Title: New US law: Cyberattacks to be reported within 72 hours
Date: 2022-03-14T12:47:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-14-new-us-law-cyberattacks-to-be-reported-within-72-hours

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/14/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Criminals use contact forms to spread BazarBackdoor, ServiceNow leaks, and more In brief A US bill that would require critical infrastructure operators to report cyberattacks within 72 hours is headed to President Joe Biden's desk to be signed into law.... [...]
