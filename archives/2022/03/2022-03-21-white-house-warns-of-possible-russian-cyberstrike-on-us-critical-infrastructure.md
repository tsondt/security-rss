Title: White House warns of possible Russian cyberstrike on US critical infrastructure
Date: 2022-03-21T20:35:36+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;critical infrastructure;hacking;russia
Slug: 2022-03-21-white-house-warns-of-possible-russian-cyberstrike-on-us-critical-infrastructure

[Source](https://arstechnica.com/?p=1842620){:target="_blank" rel="noopener"}

> Enlarge / US Deputy National Security Advisor for Cyber and Emerging Technology Anne Neuberger speaking during a March 21 White House daily press briefing. (credit: Getty Images ) The Biden administration on Monday warned that it believes Russian state hackers may step up a cyber offensive that targets US organizations, particularly organizations in the private sector providing critical infrastructure. Administration officials stressed that they have yet to unearth any evidence of specific cyberattack plans. But in recent weeks, officials have said Kremlin-sponsored strikes on US-based computers and networks was a distinct possibility that security defenders should prepare for. As the [...]
