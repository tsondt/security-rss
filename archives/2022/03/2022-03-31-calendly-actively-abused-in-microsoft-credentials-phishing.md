Title: Calendly actively abused in Microsoft credentials phishing
Date: 2022-03-31T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-31-calendly-actively-abused-in-microsoft-credentials-phishing

[Source](https://www.bleepingcomputer.com/news/security/calendly-actively-abused-in-microsoft-credentials-phishing/){:target="_blank" rel="noopener"}

> Phishing actors are actively abusing Calendly to kick off a clever sequence to trick targets into entering their email account credentials on the phishing page. [...]
