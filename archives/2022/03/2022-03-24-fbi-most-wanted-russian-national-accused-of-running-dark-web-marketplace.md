Title: FBI Most Wanted Russian national accused of running dark web marketplace
Date: 2022-03-24T13:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-24-fbi-most-wanted-russian-national-accused-of-running-dark-web-marketplace

[Source](https://portswigger.net/daily-swig/fbi-most-wanted-russian-national-accused-of-running-dark-web-marketplace){:target="_blank" rel="noopener"}

> The 23-year-old has been indicted for operating a successful carding ring [...]
