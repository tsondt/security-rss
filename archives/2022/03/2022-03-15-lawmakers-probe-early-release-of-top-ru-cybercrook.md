Title: Lawmakers Probe Early Release of Top RU Cybercrook
Date: 2022-03-15T15:37:43+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Aleksei Burkov;DirectConnection;Jake Sullivan;K0pa;Mazafaka;Naama Issachar;Vladimir Putin
Slug: 2022-03-15-lawmakers-probe-early-release-of-top-ru-cybercrook

[Source](https://krebsonsecurity.com/2022/03/lawmakers-probe-early-release-of-top-ru-cybercrook/){:target="_blank" rel="noopener"}

> Aleksei Burkov, seated second from right, attends a hearing in Jerusalem in 2015. Image: Andrei Shirokov / Tass via Getty Images. Aleksei Burkov, a cybercriminal who long operated two of Russia’s most exclusive underground hacking forums, was arrested in 2015 by Israeli authorities. The Russian government fought Burkov’s extradition to the U.S. for four years — even arresting and jailing an Israeli woman to force a prisoner swap. That effort failed: Burkov was sent to America, pleaded guilty, and was sentenced to nine years in prison. But a little more than a year later, he was quietly released and deported [...]
