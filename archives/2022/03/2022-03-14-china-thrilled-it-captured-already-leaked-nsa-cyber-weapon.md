Title: China thrilled it captured already-leaked NSA cyber-weapon
Date: 2022-03-14T19:28:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-14-china-thrilled-it-captured-already-leaked-nsa-cyber-weapon

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/14/china_nsa_nopen/){:target="_blank" rel="noopener"}

> Not now with your mischief, Beijing China claims it has obtained malware used by the NSA to steal files, monitor and redirect network traffic, and remotely control computers to spy on foreign targets.... [...]
