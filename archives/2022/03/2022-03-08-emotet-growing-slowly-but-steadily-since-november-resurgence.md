Title: Emotet growing slowly but steadily since November resurgence
Date: 2022-03-08T10:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-08-emotet-growing-slowly-but-steadily-since-november-resurgence

[Source](https://www.bleepingcomputer.com/news/security/emotet-growing-slowly-but-steadily-since-november-resurgence/){:target="_blank" rel="noopener"}

> The notorious Emotet botnet is still being distributed steadily in the wild, having now infected 130,000 systems in 179 countries. [...]
