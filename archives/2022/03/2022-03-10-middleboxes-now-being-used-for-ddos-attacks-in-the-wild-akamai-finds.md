Title: Middleboxes now being used for DDoS attacks in the wild, Akamai finds
Date: 2022-03-10T12:22:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-10-middleboxes-now-being-used-for-ddos-attacks-in-the-wild-akamai-finds

[Source](https://portswigger.net/daily-swig/middleboxes-now-being-used-for-ddos-attacks-in-the-wild-akamai-finds){:target="_blank" rel="noopener"}

> Malicious actors are starting to add TCP middlebox reflection to their arsenal [...]
