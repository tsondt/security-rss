Title: FBI: Ransomware gang breached 52 US critical infrastructure orgs
Date: 2022-03-07T15:16:33-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-07-fbi-ransomware-gang-breached-52-us-critical-infrastructure-orgs

[Source](https://www.bleepingcomputer.com/news/security/fbi-ransomware-gang-breached-52-us-critical-infrastructure-orgs/){:target="_blank" rel="noopener"}

> The US Federal Bureau of Investigation (FBI) says the Ragnar Locker ransomware group has breached the networks of at least 52 organizations from multiple US critical infrastructure sectors. [...]
