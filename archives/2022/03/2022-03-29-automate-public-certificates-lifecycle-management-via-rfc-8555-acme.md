Title: Automate Public Certificates Lifecycle Management via RFC 8555 (ACME)
Date: 2022-03-29T16:00:00+00:00
Author: Babi Seal
Category: GCP Security
Tags: Developers & Practitioners;Networking;DevOps & SRE;Identity & Security
Slug: 2022-03-29-automate-public-certificates-lifecycle-management-via-rfc-8555-acme

[Source](https://cloud.google.com/blog/products/identity-security/automate-public-certificate-lifecycle-management-via--acme-client-api/){:target="_blank" rel="noopener"}

> We’re excited to announce an enhancement of our preview of Certificate Manager which allows Google Cloud customers to acquire public certificates for their workloads that terminate TLS directly or for their cross-cloud and on-premise workloads. This is accomplished via the Automatic Certificate Management Environment ( ACME ) protocol which is the same protocol used by Certificate Authorities to enable seamless automatic lifecycle management of TLS certificates. These certificates come from Google Trust Services, the same Certificate Authority (CA) we use by default when we manage certificates on your behalf with the Global External HTTPS Load Balancer. By using the same [...]
