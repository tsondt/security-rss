Title: Fake Emergency Search Warrants Draw Scrutiny from Capitol Hill
Date: 2022-03-31T22:54:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;apple;Bloomberg;Bug;Discord;Facebook;Instagram;LAPSUS$;Meta;Sen. Ron Wyden;Snapchat;twitter
Slug: 2022-03-31-fake-emergency-search-warrants-draw-scrutiny-from-capitol-hill

[Source](https://krebsonsecurity.com/2022/03/fake-emergency-search-warrants-draw-scrutiny-from-capitol-hill/){:target="_blank" rel="noopener"}

> On Tuesday, KrebsOnSecurity warned that hackers increasingly are using compromised government and police department email accounts to obtain sensitive customer data from mobile providers, ISPs and social media companies. Today, one of the U.S. Senate’s most tech-savvy lawmakers said he was troubled by the report and is now asking technology companies and federal agencies for information about the frequency of such schemes. At issue are forged “emergency data requests,” (EDRs) sent through hacked police or government agency email accounts. Tech companies usually require a search warrant or subpoena before providing customer or user data, but any police jurisdiction can use [...]
