Title: UK Cops Collar 7 Suspected Lapsus$ Gang Members
Date: 2022-03-24T21:23:30+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-03-24-uk-cops-collar-7-suspected-lapsus-gang-members

[Source](https://threatpost.com/uk-cops-collar-7-suspected-lapsus-gang-members/179098/){:target="_blank" rel="noopener"}

> London Police can't say if they nabbed the 17-year-old suspected mastermind & multimillionaire – but researchers say they’ve been tracking an Oxford teen since mid-2021. [...]
