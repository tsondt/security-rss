Title: New Spring Java framework zero-day allows remote code execution
Date: 2022-03-30T16:16:13-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-30-new-spring-java-framework-zero-day-allows-remote-code-execution

[Source](https://www.bleepingcomputer.com/news/security/new-spring-java-framework-zero-day-allows-remote-code-execution/){:target="_blank" rel="noopener"}

> A new zero-day vulnerability in the Spring Core Java framework called 'Spring4Shell' has been publicly disclosed, allowing unauthenticated remote code execution on applications. [...]
