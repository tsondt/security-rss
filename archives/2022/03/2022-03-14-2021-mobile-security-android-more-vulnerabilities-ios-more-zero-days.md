Title: 2021 mobile security: Android more vulnerabilities, iOS more zero-days
Date: 2022-03-14T15:46:15-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-03-14-2021-mobile-security-android-more-vulnerabilities-ios-more-zero-days

[Source](https://www.bleepingcomputer.com/news/security/2021-mobile-security-android-more-vulnerabilities-ios-more-zero-days/){:target="_blank" rel="noopener"}

> Mobile security company Zimperium has released its annual mobile threat report where security trends and discoveries in the year that passed lay the groundwork for predicting what's coming in 2022. [...]
