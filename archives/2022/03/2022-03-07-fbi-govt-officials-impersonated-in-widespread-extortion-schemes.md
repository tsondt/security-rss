Title: FBI: Govt officials impersonated in widespread extortion schemes
Date: 2022-03-07T12:47:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-07-fbi-govt-officials-impersonated-in-widespread-extortion-schemes

[Source](https://www.bleepingcomputer.com/news/security/fbi-govt-officials-impersonated-in-widespread-extortion-schemes/){:target="_blank" rel="noopener"}

> Scammers are impersonating government officials and law enforcement in active and rampant extortion schemes targeting Americans' money or personally identifiable information (PII). [...]
