Title: Fresh concerns about 'indefinite' UK government access to doctors' patient data
Date: 2022-03-23T09:30:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-03-23-fresh-concerns-about-indefinite-uk-government-access-to-doctors-patient-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/23/uk_government_gp_data/){:target="_blank" rel="noopener"}

> As England's controversial scheme to grab 55 million citizen's medical data is withdrawn, emergency powers are extended Concerns are being raised over UK government proposals to extend emergency powers introduced during the pandemic, giving it access to patient data held by general practitioners (GPs).... [...]
