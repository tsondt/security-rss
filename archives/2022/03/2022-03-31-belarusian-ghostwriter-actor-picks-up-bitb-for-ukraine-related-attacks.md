Title: Belarusian ‘Ghostwriter’ Actor Picks Up BitB for Ukraine-Related Attacks
Date: 2022-03-31T18:09:07+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Government;Hacks;Malware;Web Security
Slug: 2022-03-31-belarusian-ghostwriter-actor-picks-up-bitb-for-ukraine-related-attacks

[Source](https://threatpost.com/belarusian-ghostwriter-actor-picks-up-bitb-for-ukraine-related-attacks/179210/){:target="_blank" rel="noopener"}

> Ghostwriter is one of 3 campaigns using war-themed attacks, with cyber-fire coming in from government-backed actors in China, Iran, North Korea & Russia. [...]
