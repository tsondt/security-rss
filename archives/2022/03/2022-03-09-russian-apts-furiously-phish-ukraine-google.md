Title: Russian APTs Furiously Phish Ukraine – Google
Date: 2022-03-09T14:07:55+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-03-09-russian-apts-furiously-phish-ukraine-google

[Source](https://threatpost.com/russian-apts-phishing-ukraine-google/178819/){:target="_blank" rel="noopener"}

> Also on the rise: DDoS attacks against Ukrainian sites and phishing activity capitalizing on the conflict, with China's Mustang Panda targeting Europe. [...]
