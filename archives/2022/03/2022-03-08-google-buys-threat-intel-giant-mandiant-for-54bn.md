Title: Google buys threat intel giant Mandiant for $5.4bn
Date: 2022-03-08T14:30:34+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-08-google-buys-threat-intel-giant-mandiant-for-54bn

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/08/google_buys_mandiant_5_4bn_dollars/){:target="_blank" rel="noopener"}

> Artist formerly known as FireEye to boost security for Alphabet's cloudy arm Google is buying preeminent threat intel firm Mandiant for $5.4bn, the two companies announced this morning.... [...]
