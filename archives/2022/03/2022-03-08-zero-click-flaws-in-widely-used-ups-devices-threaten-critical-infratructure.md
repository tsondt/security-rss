Title: Zero-Click Flaws in Widely Used UPS Devices Threaten Critical Infratructure
Date: 2022-03-08T15:14:09+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Critical Infrastructure;Vulnerabilities
Slug: 2022-03-08-zero-click-flaws-in-widely-used-ups-devices-threaten-critical-infratructure

[Source](https://threatpost.com/zero-click-flaws-ups-critical-infratructure/178810/){:target="_blank" rel="noopener"}

> The 'TLStorm' vulnerabilities, found in APC Smart-UPS products, could allow attackers to cause both cyber and physical damage by taking down critical infrastructure. [...]
