Title: Hundreds of HP printer models vulnerable to remote code execution
Date: 2022-03-22T09:18:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-22-hundreds-of-hp-printer-models-vulnerable-to-remote-code-execution

[Source](https://www.bleepingcomputer.com/news/security/hundreds-of-hp-printer-models-vulnerable-to-remote-code-execution/){:target="_blank" rel="noopener"}

> HP has published security advisories for three critical-severity vulnerabilities affecting hundreds of its LaserJet Pro, Pagewide Pro, OfficeJet, Enterprise, Large Format, and DeskJet printer models. [...]
