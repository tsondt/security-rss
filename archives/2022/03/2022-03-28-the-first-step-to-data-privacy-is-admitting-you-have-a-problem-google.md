Title: The first step to data privacy is admitting you have a problem, Google
Date: 2022-03-28T10:17:07+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2022-03-28-the-first-step-to-data-privacy-is-admitting-you-have-a-problem-google

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/28/google_data_privacy/){:target="_blank" rel="noopener"}

> Android messaging heist isn't the action of a well company. Let us help Opinion One of the joys of academic research is that if you do it right, you can prove the truth. In the case of computer science professor Douglas Leith, this truth is that Google has been taking detailed notes of every telephone call and SMS message made and received on the default Android apps.... [...]
