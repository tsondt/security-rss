Title: In case you missed it: Google Cloud Security Talks, Threat Detection & Response Edition
Date: 2022-03-22T16:00:00+00:00
Author: Lorenz Jakober
Category: GCP Security
Tags: Events;Identity & Security
Slug: 2022-03-22-in-case-you-missed-it-google-cloud-security-talks-threat-detection-response-edition

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-security-talks-threat-detection-response-edition/){:target="_blank" rel="noopener"}

> We recently held our first Google Cloud Security Talks event of 2022. The event focused on all things security operations (SecOps) across on-premises, cloud and hybrid environments. We shared product updates across the portfolio and talked about how threat detection, investigation and response fits into our invisible security vision. In case you missed the event, we’ve put together a brief recap below. Of course, we encourage you to check out the on-demand sessions for all the details! Our opening keynote provided an overview of how to transform SecOps and build a modern threat detection and response program that takes advantage [...]
