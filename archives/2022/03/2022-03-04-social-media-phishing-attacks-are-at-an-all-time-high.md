Title: Social media phishing attacks are at an all time high
Date: 2022-03-04T11:09:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-04-social-media-phishing-attacks-are-at-an-all-time-high

[Source](https://www.bleepingcomputer.com/news/security/social-media-phishing-attacks-are-at-an-all-time-high/){:target="_blank" rel="noopener"}

> Phishing campaigns continue to focus on social media, ramping up efforts to target users for the third consecutive year as the medium becomes increasingly used worldwide for communication, news, and entertainment. [...]
