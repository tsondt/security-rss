Title: RCE Bugs in Hugely Popular VoIP Apps: Patch Now!
Date: 2022-03-01T21:44:32+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-03-01-rce-bugs-in-hugely-popular-voip-apps-patch-now

[Source](https://threatpost.com/rce-bugs-popular-voip-apps-patch-now/178719/){:target="_blank" rel="noopener"}

> The flaws are in the ubiquitous open-source PJSIP multimedia communication library, used by the Asterisk PBX toolkit that's found in a massive number of VoIP implementations. [...]
