Title: Russia Leaks Data From a Thousand Cuts–Podcast
Date: 2022-03-03T16:31:36+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Podcasts
Slug: 2022-03-03-russia-leaks-data-from-a-thousand-cutspodcast

[Source](https://threatpost.com/russia-leaks-data-thousand-cuts-podcast/178749/){:target="_blank" rel="noopener"}

> It’s not just Ukraine: There's a flood of intel on Russian military, nukes and crooks, says dark-web intel expert Vinny Troia, even with the Conti ransomware gang shuttering its leaking Jabber chat server. [...]
