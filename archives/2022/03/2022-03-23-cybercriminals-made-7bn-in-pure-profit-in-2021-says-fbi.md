Title: Cybercriminals made $7bn in pure profit in 2021, says FBI
Date: 2022-03-23T15:59:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-03-23-cybercriminals-made-7bn-in-pure-profit-in-2021-says-fbi

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/23/cybercriminals_made_7bn_2021/){:target="_blank" rel="noopener"}

> Another year, another batch of record-setting cybercrime losses The FBI's latest yearly cybercrime report is bad news for those of us trying to stay safe: The criminals continue to have a leg up, leading to record financial losses.... [...]
