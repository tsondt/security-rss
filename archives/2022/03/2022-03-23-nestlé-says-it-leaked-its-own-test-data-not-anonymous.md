Title: Nestlé says it leaked its own test data, not Anonymous
Date: 2022-03-23T21:35:21+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-23-nestlé-says-it-leaked-its-own-test-data-not-anonymous

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/23/nestle_russia_anonymous/){:target="_blank" rel="noopener"}

> Have a break, don't have any more KitKat, junk food giant also tells Russia Nestlé, which is to stop selling KitKats and other brands in Russia, says corporate data leaked online this week by Anonymous was not stolen nor all that useful.... [...]
