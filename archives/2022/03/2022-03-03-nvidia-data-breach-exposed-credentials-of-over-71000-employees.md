Title: NVIDIA data breach exposed credentials of over 71,000 employees
Date: 2022-03-03T16:59:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-03-nvidia-data-breach-exposed-credentials-of-over-71000-employees

[Source](https://www.bleepingcomputer.com/news/security/nvidia-data-breach-exposed-credentials-of-over-71-000-employees/){:target="_blank" rel="noopener"}

> More than 71,000 employee credentials were stolen and leaked online following a data breach suffered by US chipmaker giant Nvidia last month. [...]
