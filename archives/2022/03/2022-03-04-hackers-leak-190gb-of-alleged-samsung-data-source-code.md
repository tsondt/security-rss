Title: Hackers leak 190GB of alleged Samsung data, source code
Date: 2022-03-04T17:15:04-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-04-hackers-leak-190gb-of-alleged-samsung-data-source-code

[Source](https://www.bleepingcomputer.com/news/security/hackers-leak-190gb-of-alleged-samsung-data-source-code/){:target="_blank" rel="noopener"}

> The Lapsus$ data extortion group leaked today a huge collection of confidential data they claim to be from Samsung Electronics, the South Korean giant consumer electronics company. [...]
