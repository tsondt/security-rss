Title: Multi-Ransomwared Victims Have It Coming–Podcast
Date: 2022-03-10T14:00:32+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Podcasts;Web Security
Slug: 2022-03-10-multi-ransomwared-victims-have-it-comingpodcast

[Source](https://threatpost.com/blaming-ransomware-victims-podcast/178799/){:target="_blank" rel="noopener"}

> Let's blame the victim. IT decision makers' confidence about security doesn't jibe with their concession that repeated incidents are their own fault, says ExtraHop's Jamie Moles. [...]
