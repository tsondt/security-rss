Title: Ubiquiti sues Krebs on Security for defamation
Date: 2022-03-30T19:46:24+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-03-30-ubiquiti-sues-krebs-on-security-for-defamation

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/30/ubiquiti_brian_krebs/){:target="_blank" rel="noopener"}

> Network equipment maker insists it acted responsibly following intrusion Network equipment maker Ubiquiti on Tuesday filed a lawsuit against infosec journalist Brian Krebs, alleging he defamed the company by falsely accusing the firm of covering up a cyber-attack.... [...]
