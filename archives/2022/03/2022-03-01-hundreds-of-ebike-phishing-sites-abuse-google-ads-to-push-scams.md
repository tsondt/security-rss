Title: Hundreds of eBike phishing sites abuse Google Ads to push scams
Date: 2022-03-01T10:00:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-01-hundreds-of-ebike-phishing-sites-abuse-google-ads-to-push-scams

[Source](https://www.bleepingcomputer.com/news/security/hundreds-of-ebike-phishing-sites-abuse-google-ads-to-push-scams/){:target="_blank" rel="noopener"}

> A large-scale campaign involving over 200 phishing and scam sites has tricked users into giving their personal data to fake investments schemes impersonating genuine brands. [...]
