Title: Chinese hacking group uses new 'Fire Chili' Windows rootkit
Date: 2022-03-31T13:11:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-31-chinese-hacking-group-uses-new-fire-chili-windows-rootkit

[Source](https://www.bleepingcomputer.com/news/security/chinese-hacking-group-uses-new-fire-chili-windows-rootkit/){:target="_blank" rel="noopener"}

> The Chinese APT group known as Deep Panda has been spotted in a recent campaign targeting VMware Horizon servers with the Log4Shell exploit to deploy a novel rootkit named 'Fire Chili'. [...]
