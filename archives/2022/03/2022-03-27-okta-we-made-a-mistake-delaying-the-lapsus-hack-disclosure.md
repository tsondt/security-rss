Title: Okta: "We made a mistake" delaying the Lapsus$ hack disclosure
Date: 2022-03-27T07:00:21-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-27-okta-we-made-a-mistake-delaying-the-lapsus-hack-disclosure

[Source](https://www.bleepingcomputer.com/news/security/okta-we-made-a-mistake-delaying-the-lapsus-hack-disclosure/){:target="_blank" rel="noopener"}

> Okta has admitted that it made a mistake delaying the disclosure hack from the Lapsus$ data extortion group that took place in January. Additionally, the company has provided a detailed timeline of the incident and its investigation activities. [...]
