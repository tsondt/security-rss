Title: The Uncertain Future of IT Automation
Date: 2022-03-08T15:56:36+00:00
Author: Chris Hass
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;InfoSec Insider;IoT;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2022-03-08-the-uncertain-future-of-it-automation

[Source](https://threatpost.com/uncertain-future-it-automation/178709/){:target="_blank" rel="noopener"}

> While IT automation is growing, big challenges remain. Chris Hass, director of information security and research at Automox, discusses how the future looks. [...]
