Title: DeadBolt Ransomware Resurfaces to Hit QNAP Again
Date: 2022-03-23T15:43:49+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-03-23-deadbolt-ransomware-resurfaces-to-hit-qnap-again

[Source](https://threatpost.com/deadbolt-ransomware-qnap-again/179057/){:target="_blank" rel="noopener"}

> A new steady stream of attacks against network-attached storage devices from the Taiwan-based vendor is similar to a wave that occurred in January. [...]
