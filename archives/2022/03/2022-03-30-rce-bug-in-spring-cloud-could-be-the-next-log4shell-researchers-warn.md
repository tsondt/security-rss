Title: RCE Bug in Spring Cloud Could Be the Next Log4Shell, Researchers Warn
Date: 2022-03-30T18:04:11+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-03-30-rce-bug-in-spring-cloud-could-be-the-next-log4shell-researchers-warn

[Source](https://threatpost.com/critical-rce-bug-spring-log4shell/179173/){:target="_blank" rel="noopener"}

> The so-called 'Spring4Shell' bug has cropped up, so to speak, and could be lurking in any number of Java applications. [...]
