Title: Fake antivirus updates used to deploy Cobalt Strike in Ukraine
Date: 2022-03-14T17:52:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-14-fake-antivirus-updates-used-to-deploy-cobalt-strike-in-ukraine

[Source](https://www.bleepingcomputer.com/news/security/fake-antivirus-updates-used-to-deploy-cobalt-strike-in-ukraine/){:target="_blank" rel="noopener"}

> Ukraine's Computer Emergency Response Team is warning that threat actors are distributing fake Windows antivirus updates that install Cobalt Strike and other malware. [...]
