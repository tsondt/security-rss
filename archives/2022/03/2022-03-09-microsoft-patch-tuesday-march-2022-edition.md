Title: Microsoft Patch Tuesday, March 2022 Edition
Date: 2022-03-09T16:22:12+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;CVE-2022-21967;CVE-2022-21990;CVE-2022-23277;CVE-2022-23285;CVE-2022-24459;CVE-2022-24503;CVE-2022-24508;CVE-2022-24512;Dustin Childs;Greg Wiseman;Immersive Labs;Kevin Breen;Microsoft Exchange Server;Microsoft Patch Tuesday March 2022;Rapid7;Remote Desktop;Trend Micro Zero Day Initiative;Windows SMBv3
Slug: 2022-03-09-microsoft-patch-tuesday-march-2022-edition

[Source](https://krebsonsecurity.com/2022/03/microsoft-patch-tuesday-march-2022-edition/){:target="_blank" rel="noopener"}

> Microsoft on Tuesday released software updates to plug at least 70 security holes in its Windows operating systems and related software. For the second month running, there are no scary zero-day threats looming for Windows users, and relatively few “critical” fixes. And yet we know from experience that attackers are already trying to work out how to turn these patches into a roadmap for exploiting the flaws they fix. Here’s a look at the security weaknesses Microsoft says are most likely to be targeted first. Greg Wiseman, product manager at Rapid7, notes that three vulnerabilities fixed this month have been [...]
