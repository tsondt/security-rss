Title: CISA updates Conti ransomware alert with nearly 100 domain names
Date: 2022-03-09T19:31:29-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-09-cisa-updates-conti-ransomware-alert-with-nearly-100-domain-names

[Source](https://www.bleepingcomputer.com/news/security/cisa-updates-conti-ransomware-alert-with-nearly-100-domain-names/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) has updated the alert on Conti ransomware with indicators of compromise (IoCs) consisting of close to 100 domain names used in malicious operations. [...]
