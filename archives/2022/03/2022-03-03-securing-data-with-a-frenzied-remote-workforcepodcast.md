Title: Securing Data With a Frenzied Remote Workforce–Podcast
Date: 2022-03-03T14:00:53+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Podcasts;Sponsored;Web Security
Slug: 2022-03-03-securing-data-with-a-frenzied-remote-workforcepodcast

[Source](https://threatpost.com/securing-data-frenzied-remote-workforce-podcast/178742/){:target="_blank" rel="noopener"}

> Stock the liquor cabinet and take a shot whenever you hear GitLab Staff Security Researcher Mark Loveless say “Zero Trust.” [...]
