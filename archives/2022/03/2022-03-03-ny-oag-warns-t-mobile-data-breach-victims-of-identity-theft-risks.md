Title: NY OAG warns T-Mobile data breach victims of identity theft risks
Date: 2022-03-03T22:17:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-03-ny-oag-warns-t-mobile-data-breach-victims-of-identity-theft-risks

[Source](https://www.bleepingcomputer.com/news/security/ny-oag-warns-t-mobile-data-breach-victims-of-identity-theft-risks/){:target="_blank" rel="noopener"}

> The New York State Office of the Attorney General (NY OAG) warned victims of the August 2021 T-Mobile data breach that they faced identity theft risks after some of the stolen information ended up for sale on the dark web. [...]
