Title: Powering Security Operations with context-aware detections, alert prioritization and risk scoring in Google Chronicle
Date: 2022-03-18T16:00:00+00:00
Author: Travis Lanham
Category: GCP Security
Tags: Events;Identity & Security
Slug: 2022-03-18-powering-security-operations-with-context-aware-detections-alert-prioritization-and-risk-scoring-in-google-chronicle

[Source](https://cloud.google.com/blog/products/identity-security/powering-security-operations-google-chronicle/){:target="_blank" rel="noopener"}

> With today’s rapidly escalating threat landscape, it is important that our customers have access to relevant context across their entire IT stack, whether it’s in the cloud, on-premise, or a combination of both, while responding to malicious threats. An alert in isolation does not provide sufficient information - associated metadata, context, and asset information is fundamental to an effective threat response strategy. Additionally, with prevalent alert fatigue, security teams lack the ability to prioritize which critical threats to address first. In order to alleviate these challenges, we are thrilled to announce the public preview of context-aware detections in Google Chronicle. [...]
