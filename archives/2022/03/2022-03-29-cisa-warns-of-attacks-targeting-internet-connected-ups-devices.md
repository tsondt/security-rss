Title: CISA warns of attacks targeting Internet-connected UPS devices
Date: 2022-03-29T11:55:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-29-cisa-warns-of-attacks-targeting-internet-connected-ups-devices

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-of-attacks-targeting-internet-connected-ups-devices/){:target="_blank" rel="noopener"}

> In a joint advisory with the Department of Energy, the Cybersecurity and Infrastructure Security Agency (CISA) warned U.S. organizations today to secure Internet-connected UPS devices from ongoing attacks. [...]
