Title: Pandora Ransomware Hits Giant Automotive Supplier Denso
Date: 2022-03-15T12:58:59+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;News;Web Security
Slug: 2022-03-15-pandora-ransomware-hits-giant-automotive-supplier-denso

[Source](https://threatpost.com/pandora-ransomware-hits-giant-automotive-supplier-denso/178911/){:target="_blank" rel="noopener"}

> Denso confirmed that cybercriminals leaked stolen, classified information from the Japan-based car-components manufacturer after an attack on one of its offices in Germany. [...]
