Title: DarkHotel APT Targets Wynn, Macao Hotels to Rip Off Guest Data
Date: 2022-03-18T18:53:40+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2022-03-18-darkhotel-apt-targets-wynn-macao-hotels-to-rip-off-guest-data

[Source](https://threatpost.com/darkhotel-apt-wynn-macao-hotels/178989/){:target="_blank" rel="noopener"}

> A DarkHotel phishing campaign breached luxe hotel networks, including Wynn Palace and the Grand Coloane Resort in Macao, a new report says. [...]
