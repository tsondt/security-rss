Title: Dozens of COVID passport apps put user's privacy at risk
Date: 2022-03-07T13:36:48-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-07-dozens-of-covid-passport-apps-put-users-privacy-at-risk

[Source](https://www.bleepingcomputer.com/news/security/dozens-of-covid-passport-apps-put-users-privacy-at-risk/){:target="_blank" rel="noopener"}

> Roughly two-thirds of test digital vaccination applications commonly used today as safe passes and travel passports exhibit behavior that may put users' privacy at risk. [...]
