Title: Google: Russia, China, Belarus state hackers target Ukraine, Europe
Date: 2022-03-08T06:21:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-08-google-russia-china-belarus-state-hackers-target-ukraine-europe

[Source](https://www.bleepingcomputer.com/news/security/google-russia-china-belarus-state-hackers-target-ukraine-europe/){:target="_blank" rel="noopener"}

> Google says Russian, Belarusian, and Chinese threat actors targeted Ukrainian and European government and military organizations, as well as individuals, in sweeping phishing campaigns and DDoS attacks. [...]
