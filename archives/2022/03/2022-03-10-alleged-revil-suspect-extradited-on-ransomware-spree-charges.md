Title: Alleged REvil suspect extradited on ransomware spree charges
Date: 2022-03-10T15:55:33+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-10-alleged-revil-suspect-extradited-on-ransomware-spree-charges

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/10/revil_suspect_ukrainian_arraigned_us_court/){:target="_blank" rel="noopener"}

> Little doubt about US federal court outcome A Ukrainian national alleged to be a member of the REvil ransomware gang has been extradited to the US and charged with multiple criminal offences.... [...]
