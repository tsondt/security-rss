Title: Lapsus$ back? Researchers claim extortion gang attacked software consultancy Globant
Date: 2022-03-30T05:58:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-30-lapsus-back-researchers-claim-extortion-gang-attacked-software-consultancy-globant

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/30/lapsus_return_okta_fallout/){:target="_blank" rel="noopener"}

> Meanwhile, Okta squirms as further details of slow hack response emerge Extortion gang Lapsus$ may to be back at work, despite the arrest of seven alleged operatives.... [...]
