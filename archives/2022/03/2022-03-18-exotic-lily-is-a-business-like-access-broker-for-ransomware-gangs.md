Title: Exotic Lily is a business-like access broker for ransomware gangs
Date: 2022-03-18T16:00:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-18-exotic-lily-is-a-business-like-access-broker-for-ransomware-gangs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/18/exotic_lily_iab_google/){:target="_blank" rel="noopener"}

> Google's TAG details operations of prolific group, including 9-to-5 workdays A group with links to high-profile ransomware crews Conti and Diavol is working as an internet access broker (IAB) for a Russia-linked cybercriminal gang, according to Google's Threat Analysis Group (TAG).... [...]
