Title: Bridgestone Hit as Ransomware Torches Toyota Supply Chain
Date: 2022-03-21T14:22:53+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-03-21-bridgestone-hit-as-ransomware-torches-toyota-supply-chain

[Source](https://threatpost.com/bridgestone-hit-as-ransomware-torches-toyota-supply-chain/178998/){:target="_blank" rel="noopener"}

> A ransomware attack struck Bridgestone Americas, weeks after another Toyota supplier experienced the same and a third reported some kind of cyber hit. [...]
