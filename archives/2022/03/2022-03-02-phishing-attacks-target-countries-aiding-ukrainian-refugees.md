Title: Phishing attacks target countries aiding Ukrainian refugees
Date: 2022-03-02T08:35:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-02-phishing-attacks-target-countries-aiding-ukrainian-refugees

[Source](https://www.bleepingcomputer.com/news/security/phishing-attacks-target-countries-aiding-ukrainian-refugees/){:target="_blank" rel="noopener"}

> A spear-phishing campaign likely coordinated by a state-backed threat actor has been targeting European government personnel providing logistics support to Ukrainian refugees. [...]
