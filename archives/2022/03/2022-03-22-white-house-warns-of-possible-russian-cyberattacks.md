Title: White House Warns of Possible Russian Cyberattacks
Date: 2022-03-22T14:57:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cyberwar;infrastructure;Russia
Slug: 2022-03-22-white-house-warns-of-possible-russian-cyberattacks

[Source](https://www.schneier.com/blog/archives/2022/03/white-house-warns-of-possible-russian-cyberattacks.html){:target="_blank" rel="noopener"}

> News : The White House has issued its starkest warning that Russia may be planning cyberattacks against critical-sector U.S. companies amid the Ukraine invasion. [...] Context: The alert comes after Russia has lobbed a series of digital attacks at the Ukrainian government and critical industry sectors. But there’s been no sign so far of major disruptive hacks against U.S. targets even as the government has imposed increasingly harsh sanctions that have battered the Russian economy. The public alert followed classified briefings government officials conducted last week for more than 100 companies in sectors at the highest risk of Russian hacks, [...]
