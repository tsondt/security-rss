Title: Introducing Community Security Analytics
Date: 2022-03-10T17:30:00+00:00
Author: Iman Ghanizada
Category: GCP Security
Tags: Data Analytics;Identity & Security
Slug: 2022-03-10-introducing-community-security-analytics

[Source](https://cloud.google.com/blog/products/identity-security/introducing-community-security-analytics/){:target="_blank" rel="noopener"}

> As more organizations embrace the principles of Autonomic Security Operations, we continue to research and develop new initiatives that can simplify the adoption of a continuous detection and continuous response (CD/CR) workflow for Security Operations teams. To this end, we’re excited to announce Community Security Analytics (CSA), a set of open-sourced queries and rules designed for self-service security analytics designed to help detect common cloud-based threats. We believe that fostering a community around standardizing and sharing cloud security analytics across our portfolio of offerings can help improve detective capabilities – giving threat researchers, threat hunters, security analysts, and data governance [...]
