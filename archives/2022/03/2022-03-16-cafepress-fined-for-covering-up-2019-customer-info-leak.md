Title: CafePress fined for covering up 2019 customer info leak
Date: 2022-03-16T22:23:23+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-16-cafepress-fined-for-covering-up-2019-customer-info-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/16/ftc_cafepress_settlement/){:target="_blank" rel="noopener"}

> Watchdog demands $500,000 after millions of people's info stolen and sold The FTC wants the former owner of CafePress to cough up $500,000 after the customizable merch bazaar not only tried to cover up a major computer security breach involving millions of netizens, it failed to safeguard customers' personal information.... [...]
