Title: Free HermeticRansom Ransomware Decryptor Released
Date: 2022-03-04T16:56:27+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cryptography;Malware;Web Security
Slug: 2022-03-04-free-hermeticransom-ransomware-decryptor-released

[Source](https://threatpost.com/free-hermeticransom-ransomware-decryptor-released/178762/){:target="_blank" rel="noopener"}

> Cruddy cryptography means victims whose files have been encrypted by the Ukraine-tormenting ransomware can break the chains without paying extortionists. [...]
