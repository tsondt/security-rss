Title: Google Cloud’s security and resiliency measures for customers and partners
Date: 2022-03-02T22:30:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Identity & Security
Slug: 2022-03-02-google-clouds-security-and-resiliency-measures-for-customers-and-partners

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-is-helping-those-affected-by-war-in-ukraine/){:target="_blank" rel="noopener"}

> As the tragic Russian invasion of Ukraine continues, we want to provide customers and partners with an update on how Google Cloud is providing resources for organizations, protecting against potential cyberattacks, and delivering network resiliency. We continue to work closely with U.S. and international officials in our approach—and have undertaken several steps across Google to bolster humanitarian efforts in the region, counter disinformation, and protect local Ukrainian citizens. Support and resources for organizations We’ve expanded eligibility for Project Shield, which provides free, unlimited protection against Distributed Denial of Service (DDoS) attacks. The service is now available to certain public sector [...]
