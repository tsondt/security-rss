Title: Western Digital tells EdgeRover users to patch app again
Date: 2022-03-21T15:30:13+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-03-21-western-digital-tells-edgerover-users-to-patch-app-again

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/21/wd_edgerover_app_flaw/){:target="_blank" rel="noopener"}

> Critical vulnerability may have allowed an attacker to escalate local privileges Users of Western Digital's EdgeRover app for Windows and Mac are advised to download an updated version to avoid a security flaw that might allow an attacker unauthorized access to directories and files.... [...]
