Title: Cow-counting app abused by China 'to spy on US states'
Date: 2022-03-09T00:08:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-09-cow-counting-app-abused-by-china-to-spy-on-us-states

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/09/china_apt41_mandiant_usaherds/){:target="_blank" rel="noopener"}

> Beijing-linked Dropbox-using PlugX RAT crew not sitting still, either Beijing's spies compromised government computer networks in six US states by exploiting, among other flaws, a vulnerability in a cattle-counting system, according to Mandiant.... [...]
