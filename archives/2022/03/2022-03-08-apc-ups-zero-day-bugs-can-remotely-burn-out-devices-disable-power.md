Title: APC UPS zero-day bugs can remotely burn out devices, disable power
Date: 2022-03-08T19:08:59-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-08-apc-ups-zero-day-bugs-can-remotely-burn-out-devices-disable-power

[Source](https://www.bleepingcomputer.com/news/security/apc-ups-zero-day-bugs-can-remotely-burn-out-devices-disable-power/){:target="_blank" rel="noopener"}

> A set of three critical zero-day vulnerabilities now tracked as TLStorm could let hackers take control of uninterruptible power supply (UPS) devices from APC, a subsidiary of Schneider Electric. [...]
