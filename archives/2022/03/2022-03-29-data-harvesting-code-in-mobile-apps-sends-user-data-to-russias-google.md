Title: Data-harvesting code in mobile apps sends user data to “Russia’s Google”
Date: 2022-03-29T14:18:09+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;android;apple;data harvesting;google;iOS;yandex
Slug: 2022-03-29-data-harvesting-code-in-mobile-apps-sends-user-data-to-russias-google

[Source](https://arstechnica.com/?p=1844088){:target="_blank" rel="noopener"}

> Enlarge (credit: Kirill Kudryavtsev | Getty Images) Russia’s biggest Internet company has embedded code into apps found on mobile devices that allows information about millions of users to be sent to servers located in its home country. The revelation relates to software created by Yandex that permits developers to create apps for devices running Apple’s iOS and Google’s Android, systems that run the vast majority of the world’s smartphones. Yandex collects user data harvested from mobile phones before sending the information to servers in Russia. Researchers have raised concerns the same “metadata” may then be accessed by the Kremlin and [...]
