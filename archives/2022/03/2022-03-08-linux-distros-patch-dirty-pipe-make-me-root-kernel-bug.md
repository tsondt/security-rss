Title: Linux distros patch 'Dirty Pipe' make-me-root kernel bug
Date: 2022-03-08T04:26:36+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-03-08-linux-distros-patch-dirty-pipe-make-me-root-kernel-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/08/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Adafruit customer data leak fallout, infosec burnout, and more In brief A Linux local privilege escalation flaw dubbed Dirty Pipe has been discovered and disclosed along with proof-of-concept exploit code.... [...]
