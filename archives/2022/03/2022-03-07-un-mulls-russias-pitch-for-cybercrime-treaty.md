Title: UN mulls Russia's pitch for cybercrime treaty
Date: 2022-03-07T19:20:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-07-un-mulls-russias-pitch-for-cybercrime-treaty

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/07/russia-un-cybercrime-treaty/){:target="_blank" rel="noopener"}

> US, EU, human-rights bods say proposal would allow more nation-state abuses As Russia's invasion of Ukraine rolls through its second week, a United Nations committee has begun hearings on a proposed new cybercrime treaty Russia has been pushing. The proposal has been heavily criticized by the United States, the European Union and other Western countries.... [...]
