Title: Phishing Campaign Targeted Those Aiding Ukraine Refugees
Date: 2022-03-03T17:18:44+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-03-03-phishing-campaign-targeted-those-aiding-ukraine-refugees

[Source](https://threatpost.com/phishing-campaign-targeted-those-aiding-ukraine-refugees/178752/){:target="_blank" rel="noopener"}

> A military email address was used to distribute malicious email macros among EU personnel helping Ukrainians. [...]
