Title: Using Radar to Read Body Language
Date: 2022-03-08T12:01:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Internet of Things;machine learning;privacy;surveillance
Slug: 2022-03-08-using-radar-to-read-body-language

[Source](https://www.schneier.com/blog/archives/2022/03/using-radar-to-read-body-language.html){:target="_blank" rel="noopener"}

> Yet another method of surveillance : Radar can detect you moving closer to a computer and entering its personal space. This might mean the computer can then choose to perform certain actions, like booting up the screen without requiring you to press a button. This kind of interaction already exists in current Google Nest smart displays, though instead of radar, Google employs ultrasonic sound waves to measure a person’s distance from the device. When a Nest Hub notices you’re moving closer, it highlights current reminders, calendar events, or other important notifications. Proximity alone isn’t enough. What if you just ended [...]
