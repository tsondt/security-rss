Title: Scammers have 2 clever new ways to install malicious apps on iOS devices
Date: 2022-03-16T21:20:32+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;App Store;apple;iOS;malware;testflight;webclips
Slug: 2022-03-16-scammers-have-2-clever-new-ways-to-install-malicious-apps-on-ios-devices

[Source](https://arstechnica.com/?p=1841674){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Scammers pushing iOS malware are stepping up their game by abusing two legitimate Apple features to bypass App Store vetting requirements and trick people into installing malicious apps. Apple has long required that apps pass a security review and be admitted to the App Store before they can be installed on iPhones and iPads. The vetting prevents malicious apps from making their way onto the devices, where they can then steal cryptocurrency and passwords or carry out other nefarious activities. A post published Wednesday by security firm Sophos sheds light on two newer methods being [...]
