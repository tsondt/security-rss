Title: Okta acknowledges 'mistake' in handling of Lapsus$ attack
Date: 2022-03-28T04:14:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-28-okta-acknowledges-mistake-in-handling-of-lapsus-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/28/okta_lapsuss_faq_admits_mistake/){:target="_blank" rel="noopener"}

> Changes story again to say customers weren't in danger, admits it waited for incident report instead of asking tough questions Identity-management-as-a-service outfit Okta has acknowledged that it made an important mistake in its handling of the attack on a supplier by extortion gang Lapsus$.... [...]
