Title: Linux Improves Its Random Number Generator
Date: 2022-03-24T11:38:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Linux;random numbers
Slug: 2022-03-24-linux-improves-its-random-number-generator

[Source](https://www.schneier.com/blog/archives/2022/03/linux-improves-its-random-number-generator.html){:target="_blank" rel="noopener"}

> In kernel version 5.17, both /dev/random and /dev/urandom have been replaced with a new — identical — algorithm based on the BLAKE2 hash function, which is an excellent security improvement. [...]
