Title: LokiLocker ransomware family spotted with built-in wiper
Date: 2022-03-16T21:00:34+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-16-lokilocker-ransomware-family-spotted-with-built-in-wiper

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/16/blackberry_lokilocker_ransomware/){:target="_blank" rel="noopener"}

> BlackBerry says extortionists erase documents if ransom unpaid BlackBerry security researchers have identified a ransomware family targeting English-speaking victims that is capable of erasing all non-system files from infected Windows PCs.... [...]
