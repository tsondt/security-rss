Title: Western Digital app bug gives elevated privileges in Windows, macOS
Date: 2022-03-20T10:11:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-20-western-digital-app-bug-gives-elevated-privileges-in-windows-macos

[Source](https://www.bleepingcomputer.com/news/security/western-digital-app-bug-gives-elevated-privileges-in-windows-macos/){:target="_blank" rel="noopener"}

> Western Digital's EdgeRover desktop app for both Windows and Mac are vulnerable to local privilege escalation and sandboxing escape bugs that could allow the disclosure of sensitive information or denial of service (DoS) attacks. [...]
