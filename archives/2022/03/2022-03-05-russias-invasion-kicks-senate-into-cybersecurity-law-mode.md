Title: Russia’s invasion kicks Senate into cybersecurity law mode
Date: 2022-03-05T00:40:19+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-05-russias-invasion-kicks-senate-into-cybersecurity-law-mode

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/05/senate-cyber-bill/){:target="_blank" rel="noopener"}

> Critical infrastructure, federal agencies must report intrusions, ransomware payments within hours, draft rules state Russia's invasion of Ukraine, and the possibility that the Kremlin may escalate its cyberespionage against the West after being heavily sanctioned, has convinced the US Senate to unanimously pass a bipartisan cybersecurity bill.... [...]
