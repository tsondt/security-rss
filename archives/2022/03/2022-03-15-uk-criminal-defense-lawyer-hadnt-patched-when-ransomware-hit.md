Title: UK criminal defense lawyer hadn't patched when ransomware hit
Date: 2022-03-15T13:30:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-03-15-uk-criminal-defense-lawyer-hadnt-patched-when-ransomware-hit

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/brit_solicitor_fined_for_failing/){:target="_blank" rel="noopener"}

> Brit solicitor fined after admitting it took 5 months to install critical update Criminal defense law firm Tuckers Solicitors is facing a fine from the UK's data watchdog for failing to properly secure data that included information on case proceedings which was scooped up in a ransomware attack in 2020.... [...]
