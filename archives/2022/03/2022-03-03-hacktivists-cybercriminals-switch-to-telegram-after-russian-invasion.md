Title: Hacktivists, cybercriminals switch to Telegram after Russian invasion
Date: 2022-03-03T12:40:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-03-hacktivists-cybercriminals-switch-to-telegram-after-russian-invasion

[Source](https://www.bleepingcomputer.com/news/security/hacktivists-cybercriminals-switch-to-telegram-after-russian-invasion/){:target="_blank" rel="noopener"}

> Telegram, the free instant messaging service that promises secure end-to-end communications, has assumed a pivotal role in the ongoing conflict between Russia and Ukraine, as it's being massively used by hacktivists and cyber-criminals alike. [...]
