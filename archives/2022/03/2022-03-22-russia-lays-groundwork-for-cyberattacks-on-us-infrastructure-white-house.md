Title: Russia Lays Groundwork for Cyberattacks on US Infrastructure – White House
Date: 2022-03-22T16:31:18+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Malware;Vulnerabilities;Web Security
Slug: 2022-03-22-russia-lays-groundwork-for-cyberattacks-on-us-infrastructure-white-house

[Source](https://threatpost.com/russia-cyberattacks-us-infrastructure/179037/){:target="_blank" rel="noopener"}

> "Evolving intelligence" shows Russia amping up for cyber-war in response to Ukraine-related sanctions, the White House said -- but researchers warn that many orgs are not prepared. [...]
