Title: Dev Sabotages Popular NPM Package to Protest Russian Invasion
Date: 2022-03-17T19:21:09+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-03-17-dev-sabotages-popular-npm-package-to-protest-russian-invasion

[Source](https://threatpost.com/dev-sabotages-popular-npm-package-protest-russian-invasion/178972/){:target="_blank" rel="noopener"}

> In the latest software supply-chain attack, the code maintainer added malicious code to the hugely popular node-ipc library to replace files with a heart emoji and a peacenotwar module. [...]
