Title: Friday Squid Blogging: Unexpectedly Low Squid Population in the Arctic
Date: 2022-03-25T21:07:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2022-03-25-friday-squid-blogging-unexpectedly-low-squid-population-in-the-arctic

[Source](https://www.schneier.com/blog/archives/2022/03/friday-squid-blogging-unexpectedly-low-squid-population-in-the-arctic.html){:target="_blank" rel="noopener"}

> Research : Abstract: The retreating ice cover of the Central Arctic Ocean (CAO) fuels speculations on future fisheries. However, very little is known about the existence of harvestable fish stocks in this 3.3 million­–square kilometer ecosystem around the North Pole. Crossing the Eurasian Basin, we documented an uninterrupted 3170-kilometer-long deep scattering layer (DSL) with zooplankton and small fish in the Atlantic water layer at 100- to 500-meter depth. Diel vertical migration of this central Arctic DSL was lacking most of the year when daily light variation was absent. Unexpectedly, the DSL also contained low abundances of Atlantic cod, along with [...]
