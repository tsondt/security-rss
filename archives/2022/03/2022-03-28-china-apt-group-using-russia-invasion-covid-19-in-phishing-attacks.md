Title: China APT group using Russia invasion, COVID-19 in phishing attacks
Date: 2022-03-28T16:30:37+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-28-china-apt-group-using-russia-invasion-covid-19-in-phishing-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/28/mustang-panda-korplug-variant/){:target="_blank" rel="noopener"}

> Mustang Panda deploys variant of Korplug malware to target European officials and ISPs A China-based threat group is likely running a month-long campaign using a variant of the Korplug malware and targeting European diplomats, internet service providers (ISPs) and research institutions via phishing lures that refer to Russia's invasion of Ukraine and COVID-19 travel restrictions.... [...]
