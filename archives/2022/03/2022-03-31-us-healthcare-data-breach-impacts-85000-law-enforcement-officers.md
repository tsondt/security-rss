Title: US healthcare data breach impacts 85,000 law enforcement officers
Date: 2022-03-31T10:46:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-31-us-healthcare-data-breach-impacts-85000-law-enforcement-officers

[Source](https://portswigger.net/daily-swig/us-healthcare-data-breach-impacts-85-000-law-enforcement-officers){:target="_blank" rel="noopener"}

> Law Enforcement Health Benefits was hit by a ransomware attack last year [...]
