Title: $620 million in crypto stolen from Axie Infinity's Ronin bridge
Date: 2022-03-29T15:38:51-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency;Security
Slug: 2022-03-29-620-million-in-crypto-stolen-from-axie-infinitys-ronin-bridge

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/620-million-in-crypto-stolen-from-axie-infinitys-ronin-bridge/){:target="_blank" rel="noopener"}

> A hacker has stolen almost $620 million in Ethereum and USDC tokens from Axie Infinity's Ronin network bridge, making it possibly the largest crypto hack in history. [...]
