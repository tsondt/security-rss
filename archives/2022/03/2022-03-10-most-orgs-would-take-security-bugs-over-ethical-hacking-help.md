Title: Most Orgs Would Take Security Bugs Over Ethical Hacking Help
Date: 2022-03-10T15:30:19+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Bug Bounty;Vulnerabilities
Slug: 2022-03-10-most-orgs-would-take-security-bugs-over-ethical-hacking-help

[Source](https://threatpost.com/orgs-security-bugs-ethical-hacking-help/178862/){:target="_blank" rel="noopener"}

> A new survey suggests that security is becoming more important for enterprises, but they’re still falling back on old "security by obscurity" ways. [...]
