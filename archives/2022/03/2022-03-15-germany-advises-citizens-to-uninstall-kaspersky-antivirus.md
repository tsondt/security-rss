Title: Germany advises citizens to uninstall Kaspersky antivirus
Date: 2022-03-15T16:00:22+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-15-germany-advises-citizens-to-uninstall-kaspersky-antivirus

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/kaspersky_germany_antivirus/){:target="_blank" rel="noopener"}

> Russian biz founder calls it 'an insult' Germany's BSI federal cybersecurity agency has warned the country's citizens not to install Russian-owned Kaspersky antivirus, saying it has "doubts about the reliability of the manufacturer."... [...]
