Title: Adafruit discloses data leak from ex-employee's GitHub repo
Date: 2022-03-06T06:16:34-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-06-adafruit-discloses-data-leak-from-ex-employees-github-repo

[Source](https://www.bleepingcomputer.com/news/security/adafruit-discloses-data-leak-from-ex-employees-github-repo/){:target="_blank" rel="noopener"}

> Adafruit has disclosed a data leak that occurred due to a publicly-viewable GitHub repository. The company suspects this could have allowed "unauthorized access" to information about certain users on or before 2019. [...]
