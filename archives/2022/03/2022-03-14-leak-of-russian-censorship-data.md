Title: Leak of Russian Censorship Data
Date: 2022-03-14T11:09:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Anonymous;censorship;databases;Russia
Slug: 2022-03-14-leak-of-russian-censorship-data

[Source](https://www.schneier.com/blog/archives/2022/03/leak-of-russian-censorship-data.html){:target="_blank" rel="noopener"}

> The transparency organization Distributed Denial of Secrets has released 800GB of data from Roskomnadzor, the Russian government censorship organization. Specifically, Distributed Denial of Secrets says the data comes from the Roskomnadzor of the Republic of Bashkortostan. The Republic of Bashkortostan is in the west of the country. [...] The data is split into two main categories: a series of over 360,000 files totalling in at 526.9GB and which date up to as recently as March 5, and then two databases that are 290.6GB in size, according to Distributed Denial of Secrets’ website. [...]
