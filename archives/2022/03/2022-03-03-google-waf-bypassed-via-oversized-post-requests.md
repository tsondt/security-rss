Title: Google WAF bypassed via oversized POST requests
Date: 2022-03-03T18:12:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-03-google-waf-bypassed-via-oversized-post-requests

[Source](https://portswigger.net/daily-swig/google-waf-bypassed-via-oversized-post-requests){:target="_blank" rel="noopener"}

> Security research highlights web application firewall security risk [...]
