Title: Lapsus$ ‘Back from Vacation’
Date: 2022-03-30T16:29:10+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;Malware;News;Web Security
Slug: 2022-03-30-lapsus-back-from-vacation

[Source](https://threatpost.com/lapsus-back-from-vacation/179156/){:target="_blank" rel="noopener"}

> Lapsus$ added IT giant Globant plus 70GB of leaked data – including admin credentials for scads of customers' DevOps platforms – to its hit list. [...]
