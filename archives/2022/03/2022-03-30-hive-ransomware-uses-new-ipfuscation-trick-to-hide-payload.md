Title: Hive ransomware uses new 'IPfuscation' trick to hide payload
Date: 2022-03-30T10:12:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2022-03-30-hive-ransomware-uses-new-ipfuscation-trick-to-hide-payload

[Source](https://www.bleepingcomputer.com/news/security/hive-ransomware-uses-new-ipfuscation-trick-to-hide-payload/){:target="_blank" rel="noopener"}

> Threat analysts have discovered a new obfuscation technique used by the Hive ransomware gang, involving IPv4 addresses and a series of conversions that eventually lead to downloading Cobalt Strike beacons. [...]
