Title: Microsoft investigates after Lapsus$ gang brags of Bing, Cortana code heist
Date: 2022-03-21T19:53:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-21-microsoft-investigates-after-lapsus-gang-brags-of-bing-cortana-code-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/21/microsoft_lapsus_breach_probe/){:target="_blank" rel="noopener"}

> If boasts are legit, Windows giant will join Nvidia, Samsung, others as victims The Lapsus$ extortion gang briefly alleged over the weekend it had compromised Microsoft.... [...]
