Title: New Mustang Panda hacking campaign targets diplomats, ISPs
Date: 2022-03-23T15:13:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-23-new-mustang-panda-hacking-campaign-targets-diplomats-isps

[Source](https://www.bleepingcomputer.com/news/security/new-mustang-panda-hacking-campaign-targets-diplomats-isps/){:target="_blank" rel="noopener"}

> An ongoing Mustang Panda campaign that has started at least eight months ago has been uncovered by threat analysts who also managed to sample and analyze custom malware loaders and a new Korplug variant. [...]
