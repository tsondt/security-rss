Title: Equifax data breach: Consumers unlikely to benefit financially from final settlement
Date: 2022-03-04T11:52:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-04-equifax-data-breach-consumers-unlikely-to-benefit-financially-from-final-settlement

[Source](https://portswigger.net/daily-swig/equifax-data-breach-consumers-unlikely-to-benefit-financially-from-final-settlement){:target="_blank" rel="noopener"}

> Potential claimants would face an ‘uphill battle in order to establish standing’, says US privacy law expert [...]
