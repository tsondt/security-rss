Title: Rompetrol gas station network hit by Hive ransomware
Date: 2022-03-07T10:25:22-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-07-rompetrol-gas-station-network-hit-by-hive-ransomware

[Source](https://www.bleepingcomputer.com/news/security/rompetrol-gas-station-network-hit-by-hive-ransomware/){:target="_blank" rel="noopener"}

> Romania's Rompetrol gas station network has been hit by a ransomware attack. Rompetrol, owned by KMG International announced today that it was battling a "complex cyberattack." BleepingComputer has learned that the Hive ransomware gang is behind this attack. [...]
