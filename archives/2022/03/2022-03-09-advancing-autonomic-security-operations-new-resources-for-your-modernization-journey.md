Title: Advancing Autonomic Security Operations: New resources for your modernization journey
Date: 2022-03-09T17:00:00+00:00
Author: Anton Chuvakin
Category: GCP Security
Tags: Identity & Security
Slug: 2022-03-09-advancing-autonomic-security-operations-new-resources-for-your-modernization-journey

[Source](https://cloud.google.com/blog/products/identity-security/new-resources-to-advance-your-autonomic-security-operations-modernization-journey/){:target="_blank" rel="noopener"}

> We launched our Autonomic Security Operations solution with a mission to accelerate the world's adoption of more modern threat management strategies. Ultimately, the goal is to shift the industry away from a product-centric approach to detection and response to a more principled approach focused on modernizing your Security Operations workflows, personnel, and underlying technologies to achieve an autonomic state of existence – where your threat management function can scale with your business - and with the threats your face. This effort is grounded in research of Google’s own practices, as well as methodologies like DevOps, SRE, and Agile that have [...]
