Title: Log4JShell Used to Swarm VMware Servers with Miners, Backdoors
Date: 2022-03-29T20:33:08+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-03-29-log4jshell-used-to-swarm-vmware-servers-with-miners-backdoors

[Source](https://threatpost.com/log4jshell-swarm-vmware-servers-miners-backdoors/179142/){:target="_blank" rel="noopener"}

> Researchers have found three backdoors and four miners in attacks exploiting the Log4Shell vulnerability, some of which are still ongoing. [...]
