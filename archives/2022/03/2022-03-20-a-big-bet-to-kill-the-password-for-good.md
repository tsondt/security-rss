Title: A big bet to kill the password for good
Date: 2022-03-20T10:37:19+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;biometrics;Fido;hardware tokens;passwords
Slug: 2022-03-20-a-big-bet-to-kill-the-password-for-good

[Source](https://arstechnica.com/?p=1842190){:target="_blank" rel="noopener"}

> Enlarge (credit: Elena Lacey) After years of tantalizing hints that a passwordless future is just around the corner, you're probably still not feeling any closer to that digital unshackling. Ten years into working on the issue, though, the FIDO Alliance, an industry association that specifically works on secure authentication, thinks it has finally identified the missing piece of the puzzle. On Thursday, the organization published a white paper that lays out FIDO's vision for solving the usability issues that have dogged passwordless features and, seemingly, kept them from achieving broad adoption. FIDO's members collaborated to produce the paper, and they [...]
