Title: Conti Ransomware source code leaked by Ukrainian researcher
Date: 2022-03-01T17:24:25-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-01-conti-ransomware-source-code-leaked-by-ukrainian-researcher

[Source](https://www.bleepingcomputer.com/news/security/conti-ransomware-source-code-leaked-by-ukrainian-researcher/){:target="_blank" rel="noopener"}

> A Ukrainian researcher continues to deal devastating blows to the Conti ransomware operation, leaking further internal conversations, as well as the source for their ransomware, administrative panels, and more. [...]
