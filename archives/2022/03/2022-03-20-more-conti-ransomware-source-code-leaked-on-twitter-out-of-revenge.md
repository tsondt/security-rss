Title: More Conti ransomware source code leaked on Twitter out of revenge
Date: 2022-03-20T19:20:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-20-more-conti-ransomware-source-code-leaked-on-twitter-out-of-revenge

[Source](https://www.bleepingcomputer.com/news/security/more-conti-ransomware-source-code-leaked-on-twitter-out-of-revenge/){:target="_blank" rel="noopener"}

> A Ukrainian security researcher has leaked newer malware source code from the Conti ransomware operation in revenge for the cybercriminals siding with Russia on the invasion of Ukraine. [...]
