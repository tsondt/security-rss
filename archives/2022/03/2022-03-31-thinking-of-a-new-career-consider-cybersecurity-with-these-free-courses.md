Title: Thinking of a new career? Consider Cybersecurity with these free courses
Date: 2022-03-31T10:03:06-04:00
Author: Sponsored by Fortinet
Category: BleepingComputer
Tags: Security
Slug: 2022-03-31-thinking-of-a-new-career-consider-cybersecurity-with-these-free-courses

[Source](https://www.bleepingcomputer.com/news/security/thinking-of-a-new-career-consider-cybersecurity-with-these-free-courses/){:target="_blank" rel="noopener"}

> Curiosity and a love of learning are definite advantages in the cybersecurity field, and reading and learning more about the subject is just a few clicks away. The world needs more people out there fighting cybercrime. Perhaps one of them could be you. [...]
