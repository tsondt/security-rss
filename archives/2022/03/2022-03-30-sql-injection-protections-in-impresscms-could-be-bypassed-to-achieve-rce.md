Title: SQL injection protections in ImpressCMS could be bypassed to achieve RCE
Date: 2022-03-30T15:07:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-30-sql-injection-protections-in-impresscms-could-be-bypassed-to-achieve-rce

[Source](https://portswigger.net/daily-swig/sql-injection-protections-in-impresscms-could-be-bypassed-to-achieve-rce){:target="_blank" rel="noopener"}

> Features designed to protect against SQL injection could be abused and turned against the host application [...]
