Title: FORCEDENTRY: Sandbox Escape
Date: 2022-03-31T09:00:00-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2022-03-31-forcedentry-sandbox-escape

[Source](https://googleprojectzero.blogspot.com/2022/03/forcedentry-sandbox-escape.html){:target="_blank" rel="noopener"}

> Posted by Ian Beer & Samuel Groß of Google Project Zero We want to thank Citizen Lab for sharing a sample of the FORCEDENTRY exploit with us, and Apple’s Security Engineering and Architecture (SEAR) group for collaborating with us on the technical analysis. Any editorial opinions reflected below are solely Project Zero’s and do not necessarily reflect those of the organizations we collaborated with during this research. Late last year we published a writeup of the initial remote code execution stage of FORCEDENTRY, the zero-click iMessage exploit attributed by Citizen Lab to NSO. By sending a.gif iMessage attachment (which was [...]
