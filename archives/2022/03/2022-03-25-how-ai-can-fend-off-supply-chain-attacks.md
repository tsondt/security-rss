Title: How AI can fend off supply-chain attacks
Date: 2022-03-25T07:46:05+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: 2022-03-25-how-ai-can-fend-off-supply-chain-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/25/ai_supply_chain_attacks/){:target="_blank" rel="noopener"}

> Vendor email compromise is big risk, says Darktrace Paid feature Of all the things that have value in business, trust is one of the most important. We tend to trust those that we work with closely, giving them easier access to our precious infrastructure.... [...]
