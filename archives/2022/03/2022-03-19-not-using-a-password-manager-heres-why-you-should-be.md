Title: Not using a password manager? Here’s why you should be…
Date: 2022-03-19T17:00:32+00:00
Author: Kate O'Flaherty
Category: The Guardian
Tags: Data and computer security;Hacking;Privacy;Internet;Apps;Technology
Slug: 2022-03-19-not-using-a-password-manager-heres-why-you-should-be

[Source](https://www.theguardian.com/technology/2022/mar/19/not-using-password-manager-why-you-should-online-security){:target="_blank" rel="noopener"}

> Experts recommend password managers for convenience and enhanced online safety, yet few of us use them In a competitive field, passwords are one of the worst things about the internet. Long and complex passwords are more secure but difficult to remember, leaving many people using weak and easy-to-guess credentials. One study by the UK’s National Cyber Security Centre (NCSC) revealed how millions are using their pet’s name, football team names, ‘password’ and “123456” to access online services. But this leaves you wide open to attack: cybercriminals can crack weak passwords in seconds using automated tools. “A hacker needs roughly two [...]
