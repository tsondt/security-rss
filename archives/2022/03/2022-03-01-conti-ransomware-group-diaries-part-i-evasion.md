Title: Conti Ransomware Group Diaries, Part I: Evasion
Date: 2022-03-01T20:50:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;The Coming Storm;alex holden;Alla Witte;Conti;Conti breach;Conti ransomware;Contileaks;Emercoin;EmerDNS;Hof;Hold Security;REvil ransomware;Stern;trickbot;Ukraine
Slug: 2022-03-01-conti-ransomware-group-diaries-part-i-evasion

[Source](https://krebsonsecurity.com/2022/03/conti-ransomware-group-diaries-part-i-evasion/){:target="_blank" rel="noopener"}

> A Ukrainian security researcher this week leaked several years of internal chat logs and other sensitive data tied to Conti, an aggressive and ruthless Russian cybercrime group that focuses on deploying its ransomware to companies with more than $100 million in annual revenue. The chat logs offer a fascinating glimpse into the challenges of running a sprawling criminal enterprise with more than 100 salaried employees. The records also provide insight into how Conti has dealt with its own internal breaches and attacks from private security firms and foreign governments. Conti’s threatening message this week regarding international interference in Ukraine. Conti [...]
