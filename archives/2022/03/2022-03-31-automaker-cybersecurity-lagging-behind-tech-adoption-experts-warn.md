Title: Automaker Cybersecurity Lagging Behind Tech Adoption, Experts Warn
Date: 2022-03-31T14:49:47+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;IoT;Vulnerabilities
Slug: 2022-03-31-automaker-cybersecurity-lagging-behind-tech-adoption-experts-warn

[Source](https://threatpost.com/automaker-cybersecurity-lagging-tech-adoption/179204/){:target="_blank" rel="noopener"}

> A bug in Honda is indicative of the sprawling car-attack surface that could give cyberattackers easy access to victims, as global use of ‘smart car tech’ and EVs surges. [...]
