Title: Microsoft Azure developers targeted by 200-plus data-stealing npm packages
Date: 2022-03-24T23:26:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-03-24-microsoft-azure-developers-targeted-by-200-plus-data-stealing-npm-packages

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/24/developers_using_microsoft_azure_targeted/){:target="_blank" rel="noopener"}

> Another day, another attack on the software supply chain A group of more than 200 malicious npm packages targeting developers who use Microsoft Azure has been removed two days after they were made available to the public.... [...]
