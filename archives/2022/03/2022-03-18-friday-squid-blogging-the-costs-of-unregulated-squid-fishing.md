Title: Friday Squid Blogging: The Costs of Unregulated Squid Fishing
Date: 2022-03-18T21:15:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;reports;squid
Slug: 2022-03-18-friday-squid-blogging-the-costs-of-unregulated-squid-fishing

[Source](https://www.schneier.com/blog/archives/2022/03/friday-squid-blogging-the-costs-of-unregulated-squid-fishing.html){:target="_blank" rel="noopener"}

> Greenpeace has published a report, “ Squids in the Spotlight,” on the extent and externalities of global squid fishing. News article. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
