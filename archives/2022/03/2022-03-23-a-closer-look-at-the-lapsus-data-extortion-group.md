Title: A Closer Look at the LAPSUS$ Data Extortion Group
Date: 2022-03-23T22:00:43+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;SIM Swapping
Slug: 2022-03-23-a-closer-look-at-the-lapsus-data-extortion-group

[Source](https://krebsonsecurity.com/2022/03/a-closer-look-at-the-lapsus-data-extortion-group/){:target="_blank" rel="noopener"}

> Microsoft and identity management platform Okta both this week disclosed breaches involving LAPSUS$, a relatively new cybercrime group that specializes in stealing data from big companies and threatening to publish it unless a ransom demand is paid. Here’s a closer look at LAPSUS$, and some of the low-tech but high-impact methods the group uses to gain access to targeted organizations. First surfacing in December 2021 with an extortion demand on Brazil’s Ministry of Health, LAPSUS$ made headlines more recently for posting screenshots of internal tools tied to a number of major corporations, including NVIDIA, Samsung, and Vodafone. On Tuesday, LAPSUS$ [...]
