Title: UK Ministry of Defence takes recruitment system offline, confirms data leak
Date: 2022-03-24T11:01:09+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-03-24-uk-ministry-of-defence-takes-recruitment-system-offline-confirms-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/24/ministry_of_defence/){:target="_blank" rel="noopener"}

> Info of those signing up to be soldiers leaked, as sources finger Capita-run system The UK Ministry of Defence has suspended online application and support services for the British Army's Capita-run Defence Recruitment System and confirmed to us that digital intruders compromised some data held on would-be soldiers.... [...]
