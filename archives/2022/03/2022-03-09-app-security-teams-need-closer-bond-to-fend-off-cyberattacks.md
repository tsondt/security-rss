Title: App, security teams need closer bond to fend off cyberattacks
Date: 2022-03-09T19:00:05+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-09-app-security-teams-need-closer-bond-to-fend-off-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/09/app_cybersecurity_alliance_immersive_labs/){:target="_blank" rel="noopener"}

> Enterprises should shift left to protect themselves, says Immersive Labs Enterprises need to create a more strategic alliance between their application security and cybersecurity teams if they are going to better protect themselves against cyberthreats.... [...]
