Title: Fresh flaws in Facebook Canvas earn bug bounty hunter a second payday
Date: 2022-03-07T17:00:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-07-fresh-flaws-in-facebook-canvas-earn-bug-bounty-hunter-a-second-payday

[Source](https://portswigger.net/daily-swig/fresh-flaws-in-facebook-canvas-earn-bug-bounty-hunter-a-second-payday){:target="_blank" rel="noopener"}

> Next-level account takeover [...]
