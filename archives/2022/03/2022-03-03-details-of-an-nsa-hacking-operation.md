Title: Details of an NSA Hacking Operation
Date: 2022-03-03T12:32:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;doxing;hacking;NSA;Russia
Slug: 2022-03-03-details-of-an-nsa-hacking-operation

[Source](https://www.schneier.com/blog/archives/2022/03/details-of-an-nsa-hacking-operation.html){:target="_blank" rel="noopener"}

> Pangu Lab in China just published a report of a hacking operation by the Equation Group (aka the NSA). It noticed the hack in 2013, and was able to map it with Equation Group tools published by the Shadow Brokers (aka some Russian group)....the scope of victims exceeded 287 targets in 45 countries, including Russia, Japan, Spain, Germany, Italy, etc. The attack lasted for over 10 years. Moreover, one victim in Japan is used as a jump server for further attack. News article. [...]
