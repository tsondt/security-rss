Title: Satellite comms networks on alert after US govt warning
Date: 2022-03-21T14:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-21-satellite-comms-networks-on-alert-after-us-govt-warning

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/21/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Security teams burning out, more Conti leaks analysis, and Log4j still plagues enterprises In Brief US federal agencies have warned of possible threats to American and international satellite communication (SATCOM) networks that could affect customers.... [...]
