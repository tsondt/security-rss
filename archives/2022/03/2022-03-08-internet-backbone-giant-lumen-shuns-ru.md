Title: Internet Backbone Giant Lumen Shuns .RU
Date: 2022-03-08T23:35:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Russia's War on Ukraine;Cogent;Doug Madory;ICANN;Kentik;Lumen Technologies;Neil MacFarquhar;Sergey Vovnenko
Slug: 2022-03-08-internet-backbone-giant-lumen-shuns-ru

[Source](https://krebsonsecurity.com/2022/03/internet-backbone-giant-lumen-shuns-ru/){:target="_blank" rel="noopener"}

> Lumen Technologies, an American company that operates one of the largest Internet backbones and carries a significant percentage of the world’s Internet traffic, said today it will stop routing traffic for organizations based in Russia. Lumen’s decision comes just days after a similar exit by backbone provider Cogent, and amid a news media crackdown in Russia that has already left millions of Russians in the dark about what is really going on with their president’s war in Ukraine. Monroe, La. based Lumen [ NYSE: LUMN ] (formerly CenturyLink ) initially said it would halt all new business with organizations based [...]
