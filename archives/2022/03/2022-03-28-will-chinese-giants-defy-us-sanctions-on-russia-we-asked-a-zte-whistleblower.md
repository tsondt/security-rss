Title: Will Chinese giants defy US sanctions on Russia? We asked a ZTE whistleblower
Date: 2022-03-28T07:00:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-28-will-chinese-giants-defy-us-sanctions-on-russia-we-asked-a-zte-whistleblower

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/28/ashley_yablon_zte_whistleblower_interview/){:target="_blank" rel="noopener"}

> Just look at Ericsson and ISIS, Ashley Yablon says If ZTE and other Chinese giants defy bans on selling American technology to Russia, it will be because they can't help but chase the revenue, says Ashley Yablon, the whistleblower whose evidence led to ZTE being fined for willfully ignoring the US ban on exports to Iran.... [...]
