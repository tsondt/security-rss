Title: Private chat? Chrome Skype extension with 9m installs found to be leaking user info
Date: 2022-03-01T14:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-01-private-chat-chrome-skype-extension-with-9m-installs-found-to-be-leaking-user-info

[Source](https://portswigger.net/daily-swig/private-chat-chrome-skype-extension-with-9m-installs-found-to-be-leaking-user-info){:target="_blank" rel="noopener"}

> Microsoft addresses issue at eleventh hour, as researcher publicly discloses ‘trivial’ privacy bug in browser plugin [...]
