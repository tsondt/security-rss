Title: Ragnar ransomware gang hit 52 critical US orgs, says FBI
Date: 2022-03-09T02:05:41+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-09-ragnar-ransomware-gang-hit-52-critical-us-orgs-says-fbi

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/09/fbi_says_ragnar_locker_ransomware/){:target="_blank" rel="noopener"}

> Energy biz, financial services, governments, and IT outfits targeted The Ragnar Locker ransomware gang has so far infected at least 52 critical infrastructure organizations in America across sectors including manufacturing, energy, financial services, government, and information technology, according to an FBI alert this week.... [...]
