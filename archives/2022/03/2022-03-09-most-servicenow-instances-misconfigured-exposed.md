Title: Most ServiceNow Instances Misconfigured, Exposed
Date: 2022-03-09T16:00:32+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Web Security
Slug: 2022-03-09-most-servicenow-instances-misconfigured-exposed

[Source](https://threatpost.com/most-servicenow-instances-misconfigured-exposed/178827/){:target="_blank" rel="noopener"}

> Customers aren't locking down access correctly, leading to ~70 percent of ServiceNow implementations tested by AppOmni being vulnerable to malicious data extraction. [...]
