Title: Brave takes on the creepy websites that override your privacy settings
Date: 2022-03-09T22:30:06+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;brave;browsers;privacy;third party cookies
Slug: 2022-03-09-brave-takes-on-the-creepy-websites-that-override-your-privacy-settings

[Source](https://arstechnica.com/?p=1839951){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Some websites just can't take "no" for an answer. Instead of respecting visitors' choice to block third-party cookies—the identifiers that track browsing activity as a user moves from site to site—they find sneaky ways to bypass those settings. Now, makers of the Brave browser are taking action. Earlier this week, Brave Nightly—the testing and development version of the browser—rolled out a feature that's designed to prevent what's known as bounce tracking. The new feature, known as unlinkable bouncing, will roll out for general release in Brave version 1.37 slated for March 29. Overriding privacy Bounce tracking [...]
