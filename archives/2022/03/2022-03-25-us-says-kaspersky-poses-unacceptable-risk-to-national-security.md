Title: US says Kaspersky poses unacceptable risk to national security
Date: 2022-03-25T17:17:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-03-25-us-says-kaspersky-poses-unacceptable-risk-to-national-security

[Source](https://www.bleepingcomputer.com/news/security/us-says-kaspersky-poses-unacceptable-risk-to-national-security/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) added Russian cybersecurity firm Kaspersky to its Covered List, saying it poses unacceptable risks to U.S. national security. [...]
