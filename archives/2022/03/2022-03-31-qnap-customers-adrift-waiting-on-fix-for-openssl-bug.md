Title: QNAP Customers Adrift, Waiting on Fix for OpenSSL Bug
Date: 2022-03-31T13:22:49+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-03-31-qnap-customers-adrift-waiting-on-fix-for-openssl-bug

[Source](https://threatpost.com/qnap-customers-adrift-fix-openssl-bug/179197/){:target="_blank" rel="noopener"}

> QNAP is warning clients that a recently disclosed vulnerability affects most of its NAS devices, with no mitigation available while the vendor readies a patch. [...]
