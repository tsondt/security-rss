Title: Russian government sites hacked in supply chain attack
Date: 2022-03-09T09:52:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-03-09-russian-government-sites-hacked-in-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/russian-government-sites-hacked-in-supply-chain-attack/){:target="_blank" rel="noopener"}

> Russia says some of its federal agencies' websites were compromised on Tuesday after unknown attackers hacked the stats widget used to track the number of visitors by multiple government agencies. [...]
