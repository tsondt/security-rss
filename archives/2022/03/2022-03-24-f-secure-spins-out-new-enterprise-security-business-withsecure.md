Title: F-Secure spins out new enterprise security business: WithSecure
Date: 2022-03-24T01:57:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-24-f-secure-spins-out-new-enterprise-security-business-withsecure

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/24/fsecure_withsecure/){:target="_blank" rel="noopener"}

> CEO tells The Reg of new branding ahead of Finnish vendor's corporate split F-Secure's enterprise-facing business will have a new brand – WithSecure – and a sharpened focus when the company splits into two independent operations.... [...]
