Title: Critical SonicWall firewall patch not released for all devices
Date: 2022-03-28T15:47:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-28-critical-sonicwall-firewall-patch-not-released-for-all-devices

[Source](https://www.bleepingcomputer.com/news/security/critical-sonicwall-firewall-patch-not-released-for-all-devices/){:target="_blank" rel="noopener"}

> Security hardware manufacturer SonicWall has fixed a critical vulnerability in the SonicOS security operating system that allows denial of service (DoS) attacks and could lead to remote code execution (RCE). [...]
