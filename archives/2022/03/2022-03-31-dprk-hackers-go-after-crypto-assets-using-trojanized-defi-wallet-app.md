Title: DPRK hackers go after crypto assets using trojanized DeFi Wallet app
Date: 2022-03-31T08:05:55-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-31-dprk-hackers-go-after-crypto-assets-using-trojanized-defi-wallet-app

[Source](https://www.bleepingcomputer.com/news/security/dprk-hackers-go-after-crypto-assets-using-trojanized-defi-wallet-app/){:target="_blank" rel="noopener"}

> Hackers associated with the North Korean government have been distributing a trojanized version of the DeFi Wallet for storing cryptocurrency assets to gain access to the systems of cryptocurrency users and investors. [...]
