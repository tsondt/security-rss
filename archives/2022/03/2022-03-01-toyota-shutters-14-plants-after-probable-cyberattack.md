Title: Toyota shutters 14 plants after probable cyberattack
Date: 2022-03-01T01:50:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-01-toyota-shutters-14-plants-after-probable-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/01/toyota_shuts_plants/){:target="_blank" rel="noopener"}

> Incident struck supplier day after officials warned of massive spike in efforts to launch Emotet malware Toyota has closed all 14 plants it operates in Japan due to what it has described as a “system failure” at Kojima Industries Corporation – and local media report the cause of the failure is a cyberattack.... [...]
