Title: Attackers can force Amazon Echos to hack themselves with self-issued commands
Date: 2022-03-06T13:00:21+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;amazon echo;eavesdropping;exploits;vulnerabilities
Slug: 2022-03-06-attackers-can-force-amazon-echos-to-hack-themselves-with-self-issued-commands

[Source](https://arstechnica.com/?p=1838449){:target="_blank" rel="noopener"}

> Enlarge / A group of Amazon Echo smart speakers, including Echo Studio, Echo, and Echo Dot models. (Photo by Neil Godwin/Future Publishing via Getty Images) (credit: T3 Magazine/Getty Images) Academic researchers have devised a new working exploit that commandeers Amazon Echo smart speakers and forces them to unlock doors, make phone calls and unauthorized purchases, and control furnaces, microwave ovens, and other smart appliances. The attack works by using the device’s speaker to issue voice commands. As long as the speech contains the device wake word (usually “Alexa” or “Echo”) followed by a permissible command, the Echo will carry it [...]
