Title: Azure flaw allowed users to control others' accounts
Date: 2022-03-08T06:01:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-08-azure-flaw-allowed-users-to-control-others-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/08/azure_autowarp_flaw/){:target="_blank" rel="noopener"}

> AutoWarp security hole wasn't exploited – though researchers saw a way into a bank and a telco Microsoft has acknowledged the existence of a flaw in its Azure cloud computing service that allowed users full access to other users' accounts.... [...]
