Title: “Change Password”
Date: 2022-03-17T11:16:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;passwords
Slug: 2022-03-17-change-password

[Source](https://www.schneier.com/blog/archives/2022/03/change-password.html){:target="_blank" rel="noopener"}

> Oops : Instead of telling you when it’s safe to cross the street, the walk signs in Crystal City, VA are just repeating ‘CHANGE PASSWORD.’ Something’s gone terribly wrong here. [...]
