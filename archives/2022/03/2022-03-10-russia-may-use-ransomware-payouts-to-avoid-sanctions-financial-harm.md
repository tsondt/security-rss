Title: Russia May Use Ransomware Payouts to Avoid Sanctions’ Financial Harm
Date: 2022-03-10T14:10:04+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Web Security
Slug: 2022-03-10-russia-may-use-ransomware-payouts-to-avoid-sanctions-financial-harm

[Source](https://threatpost.com/russia-ransomware-payouts-avoid-sanctions/178854/){:target="_blank" rel="noopener"}

> FinCEN warns financial institutions to be ware of unusual cryptocurrency payments or illegal transactions Russia may use to ease financial hurt from Ukraine-linked sanctions. [...]
