Title: Attacks abusing programming APIs grew over 600% in 2021
Date: 2022-03-02T11:28:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-02-attacks-abusing-programming-apis-grew-over-600-in-2021

[Source](https://www.bleepingcomputer.com/news/security/attacks-abusing-programming-apis-grew-over-600-percent-in-2021/){:target="_blank" rel="noopener"}

> Security analysts warn of a sharp rise in API attacks over the past year, with most companies still following inadequate practices to tackle the problem. [...]
