Title: ‘Dangerous’ EU web authentication plan threatens to undercut browser-led certification system, detractors claim
Date: 2022-03-30T14:15:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-30-dangerous-eu-web-authentication-plan-threatens-to-undercut-browser-led-certification-system-detractors-claim

[Source](https://portswigger.net/daily-swig/dangerous-eu-web-authentication-plan-threatens-to-undercut-browser-led-certification-system-detractors-claim){:target="_blank" rel="noopener"}

> Signatories to a letter criticizing EU scheme share their misgivings with The Daily Swig [...]
