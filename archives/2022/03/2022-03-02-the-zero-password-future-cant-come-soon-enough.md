Title: The zero-password future can't come soon enough
Date: 2022-03-02T15:00:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-02-the-zero-password-future-cant-come-soon-enough

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/02/passwords-weak-security-link/){:target="_blank" rel="noopener"}

> SpyCloud highlights poor password hygiene of consumers and the threat to enterprises Passwords, long a weakness in the tapestry of defenses designed to keep enterprises and individuals more secure, continue to be a problem due in large part to the same issue that has haunted them for years: the users themselves.... [...]
