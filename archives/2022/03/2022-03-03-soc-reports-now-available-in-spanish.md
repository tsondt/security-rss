Title: SOC reports now available in Spanish
Date: 2022-03-03T19:36:41+00:00
Author: Rodrigo Fiuza
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC 1;AWS SOC 2;AWS SOC 3;AWS SOC Reports
Slug: 2022-03-03-soc-reports-now-available-in-spanish

[Source](https://aws.amazon.com/blogs/security/soc-reports-now-available-in-spanish/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we continue to listen to our customers, regulators, and stakeholders to understand their needs regarding audit, assurance, certification, and attestation programs. We are pleased to announce that Fall 2021 AWS SOC 1, SOC 2 and SOC 3 reports are now available in Spanish. These translated reports will help drive greater [...]
