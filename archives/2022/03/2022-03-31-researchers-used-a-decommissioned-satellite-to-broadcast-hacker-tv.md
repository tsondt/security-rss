Title: Researchers used a decommissioned satellite to broadcast hacker TV
Date: 2022-03-31T15:56:11+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;hacking;satellite
Slug: 2022-03-31-researchers-used-a-decommissioned-satellite-to-broadcast-hacker-tv

[Source](https://arstechnica.com/?p=1844763){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images | 3DSculptor) Independent researchers and the United States military have become increasingly focused on orbiting satellites' potential security vulnerabilities in recent years. These devices, which are built primarily with durability, reliability, and longevity in mind, were largely never intended to be ultra-secure. But at the ShmooCon security conference in Washington, DC, on Friday, embedded device security researcher Karl Koscher raised questions about a different phase of a satellite's life cycle: What happens when an old satellite is being decommissioned and transitioning to a “ graveyard orbit ”? Koscher and his colleagues received permission last year to [...]
