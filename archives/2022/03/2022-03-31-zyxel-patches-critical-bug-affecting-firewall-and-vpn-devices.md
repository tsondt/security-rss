Title: Zyxel patches critical bug affecting firewall and VPN devices
Date: 2022-03-31T15:02:03-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-31-zyxel-patches-critical-bug-affecting-firewall-and-vpn-devices

[Source](https://www.bleepingcomputer.com/news/security/zyxel-patches-critical-bug-affecting-firewall-and-vpn-devices/){:target="_blank" rel="noopener"}

> Network equipment company Zyxel has updated the firmware of several of its business-grade firewall and VPN products to address a critical-severity vulnerability that could give attackers administrator-level access to affected devices. [...]
