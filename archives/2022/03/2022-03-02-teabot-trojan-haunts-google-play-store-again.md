Title: TeaBot Trojan Haunts Google Play Store, Again
Date: 2022-03-02T22:50:09+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Malware;Mobile Security;Vulnerabilities
Slug: 2022-03-02-teabot-trojan-haunts-google-play-store-again

[Source](https://threatpost.com/teabot-trojan-haunts-google-play-store/178738/){:target="_blank" rel="noopener"}

> Malicious Google Play apps have circumvented censorship by hiding trojans in software updates. [...]
