Title: Hive ransomware ports its Linux VMware ESXi encryptor to Rust
Date: 2022-03-27T15:18:58-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Linux
Slug: 2022-03-27-hive-ransomware-ports-its-linux-vmware-esxi-encryptor-to-rust

[Source](https://www.bleepingcomputer.com/news/security/hive-ransomware-ports-its-linux-vmware-esxi-encryptor-to-rust/){:target="_blank" rel="noopener"}

> The Hive ransomware operation has converted their VMware ESXi Linux encryptor to the Rust programming language and added new features to make it harder for security researchers to snoop on victim's ransom negotiations. [...]
