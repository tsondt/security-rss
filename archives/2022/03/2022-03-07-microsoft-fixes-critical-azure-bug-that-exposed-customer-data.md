Title: Microsoft fixes critical Azure bug that exposed customer data
Date: 2022-03-07T11:09:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-03-07-microsoft-fixes-critical-azure-bug-that-exposed-customer-data

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-critical-azure-bug-that-exposed-customer-data/){:target="_blank" rel="noopener"}

> Microsoft has addressed a critical vulnerability in the Azure Automation service that could have allowed attackers to take full control over other Azure customers' data. [...]
