Title: Hackers stoke pandemonium amid Russia’s war in Ukraine
Date: 2022-03-05T11:30:00+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Policy;Cyberattacks;Fancy Bear;hacking;russia;Ukraine;war
Slug: 2022-03-05-hackers-stoke-pandemonium-amid-russias-war-in-ukraine

[Source](https://arstechnica.com/?p=1838378){:target="_blank" rel="noopener"}

> Enlarge (credit: Elena Lacey | Getty Images) On Thursday, hackers defaced a Russian Space Research Institute website and leaked files that they allege are stolen from Roscosmos, the Russian space agency. Their message ? “Leave Ukraine alone else Anonymous will f*ck you up even more.” Meanwhile a DDoS attack pummeled Russia's.ru “top level domain,” with the aim of essentially cutting off access to all URLs that end in.ru. These are just the latest incidents in a surge of hacktivism in support of Ukraine. Protests against Russia’s war of choice with Ukraine have been held around the world, including in 48 [...]
