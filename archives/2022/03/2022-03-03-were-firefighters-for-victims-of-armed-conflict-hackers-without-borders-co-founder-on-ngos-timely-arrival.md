Title: ‘We’re firefighters for victims of armed conflict’ – Hackers Without Borders co-founder on NGO’s timely arrival
Date: 2022-03-03T12:52:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-03-were-firefighters-for-victims-of-armed-conflict-hackers-without-borders-co-founder-on-ngos-timely-arrival

[Source](https://portswigger.net/daily-swig/were-firefighters-for-victims-of-armed-conflict-hackers-without-borders-co-founder-on-ngos-timely-arrival){:target="_blank" rel="noopener"}

> ‘We had NGOs for press, medical staff, and mental health issues, but not for cyber-attack victims’ [...]
