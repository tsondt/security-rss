Title: APT41 Spies Broke Into 6 US State Networks via a Livestock App
Date: 2022-03-09T21:10:20+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Vulnerabilities;Web Security
Slug: 2022-03-09-apt41-spies-broke-into-6-us-state-networks-via-a-livestock-app

[Source](https://threatpost.com/apt41-spies-broke-into-6-us-state-networks-via-livestock-app/178838/){:target="_blank" rel="noopener"}

> The China-affiliated state-sponsored threat actor used Log4j and zero-day bugs in the USAHerds animal-tracking software to hack into multiple government networks. [...]
