Title: Ten notorious ransomware strains put to the encryption speed test
Date: 2022-03-23T10:53:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-23-ten-notorious-ransomware-strains-put-to-the-encryption-speed-test

[Source](https://www.bleepingcomputer.com/news/security/ten-notorious-ransomware-strains-put-to-the-encryption-speed-test/){:target="_blank" rel="noopener"}

> Researchers have conducted a technical experiment, testing ten ransomware variants to determine how fast they encrypt files and evaluate how feasible it would be to timely respond to their attacks. [...]
