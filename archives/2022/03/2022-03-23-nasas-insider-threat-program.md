Title: NASA’s Insider Threat Program
Date: 2022-03-23T11:16:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;insiders;national security policy;reports
Slug: 2022-03-23-nasas-insider-threat-program

[Source](https://www.schneier.com/blog/archives/2022/03/nasas-insider-threat-program.html){:target="_blank" rel="noopener"}

> The Office of Inspector General has audited NASA’s insider threat program: While NASA has a fully operational insider threat program for its classified systems, the vast majority of the Agency’s information technology (IT) systems — including many containing high-value assets or critical infrastructure — are unclassified and are therefore not covered by its current insider threat program. Consequently, the Agency may be facing a higher-than-necessary risk to its unclassified systems and data. While NASA’s exclusion of unclassified systems from its insider threat program is common among federal agencies, adding those systems to a multi-faceted security program could provide an additional [...]
