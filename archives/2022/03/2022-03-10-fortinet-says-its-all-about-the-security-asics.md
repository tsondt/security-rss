Title: Fortinet says it’s all about the security ASICs
Date: 2022-03-10T22:21:02+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-10-fortinet-says-its-all-about-the-security-asics

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/10/fortinet_security_asics/){:target="_blank" rel="noopener"}

> Xie claims his custom chips lower infosec computing costs by up to 10x As security and networking converge, Fortinet CEO Ken Xie believes the company he co-founded will win this particular $200bn market with its custom application-specific ICs, or ASIC chips.... [...]
