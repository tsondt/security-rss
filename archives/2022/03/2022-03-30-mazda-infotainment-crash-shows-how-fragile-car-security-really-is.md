Title: Mazda Infotainment Crash Shows How Fragile Car Security Really Is
Date: 2022-03-30T10:02:04-04:00
Author: Sponsored by Cybellum
Category: BleepingComputer
Tags: Security
Slug: 2022-03-30-mazda-infotainment-crash-shows-how-fragile-car-security-really-is

[Source](https://www.bleepingcomputer.com/news/security/mazda-infotainment-crash-shows-how-fragile-car-security-really-is/){:target="_blank" rel="noopener"}

> Automated product security helps teams address automotive security vulnerabilities and bugs before - not after - they land companies in the headlines. [...]
