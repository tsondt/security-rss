Title: Biden says Russia exploring revenge cyberattacks
Date: 2022-03-22T08:01:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-22-biden-says-russia-exploring-revenge-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/22/biden_cybersecurity_statement_warning/){:target="_blank" rel="noopener"}

> Several hundred US orgs given classified briefings as critical infrastructure felt to be at risk United States President Joe Biden has revealed "evolving intelligence that the Russian Government is exploring options for potential cyber attacks" and that the risks posed to critical infrastructure are so significant that hundreds of US organizations have been given classified briefings on the matter.... [...]
