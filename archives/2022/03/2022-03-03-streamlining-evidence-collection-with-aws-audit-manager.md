Title: Streamlining evidence collection with AWS Audit Manager
Date: 2022-03-03T16:37:46+00:00
Author: Nicholas Parks
Category: AWS Security
Tags: AWS Audit Manager;Intermediate (200);Security, Identity, & Compliance;API;assessment;audit;Automation;CLI;cloud;Compliance;controls;Devops;evidence;framework;Integration;lambda;sdlc
Slug: 2022-03-03-streamlining-evidence-collection-with-aws-audit-manager

[Source](https://aws.amazon.com/blogs/security/streamlining-evidence-collection-with-aws-audit-manager/){:target="_blank" rel="noopener"}

> In this post, we will show you how to deploy a solution into your Amazon Web Services (AWS) account that enables you to simply attach manual evidence to controls using AWS Audit Manager. Making evidence-collection as seamless as possible minimizes audit fatigue and helps you maintain a strong compliance posture. As an AWS customer, you [...]
