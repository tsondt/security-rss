Title: Leaked ransomware documents show Conti helping Putin from the shadows
Date: 2022-03-19T10:45:49+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;conti;Fancy Bear;hacking;ransomware;russia;state sponsored hacking
Slug: 2022-03-19-leaked-ransomware-documents-show-conti-helping-putin-from-the-shadows

[Source](https://arstechnica.com/?p=1842163){:target="_blank" rel="noopener"}

> Enlarge (credit: Wired | Getty Images) For years, Russia’s cybercrime groups have acted with relative impunity. The Kremlin and local law enforcement have largely turned a blind eye to disruptive ransomware attacks as long as they didn’t target Russian companies. Despite direct pressure on Vladimir Putin to tackle ransomware groups, they’re still intimately tied to Russia’s interests. A recent leak from one of the most notorious such groups provides a glimpse into the nature of those ties—and just how tenuous they may be. A cache of 60,000 leaked chat messages and files from the notorious Conti ransomware group provides glimpses [...]
