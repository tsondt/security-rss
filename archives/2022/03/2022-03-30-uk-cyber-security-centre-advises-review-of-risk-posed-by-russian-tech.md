Title: UK Cyber Security Centre advises review of risk posed by Russian tech
Date: 2022-03-30T06:51:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-30-uk-cyber-security-centre-advises-review-of-risk-posed-by-russian-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/30/uk_ncsc_russia_tech_advice/){:target="_blank" rel="noopener"}

> Suggests it's prudent to plan for Putin weaponizing Russian products The UK's National Cyber Security Centre (NCSC) has advised users of Russian technology products to reassess the risks it presents.... [...]
