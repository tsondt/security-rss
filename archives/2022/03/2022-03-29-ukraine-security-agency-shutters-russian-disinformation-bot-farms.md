Title: Ukraine security agency shutters Russian disinformation bot farms
Date: 2022-03-29T15:00:04+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-29-ukraine-security-agency-shutters-russian-disinformation-bot-farms

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/29/ukriane-russia-bot-farm-disinformation/){:target="_blank" rel="noopener"}

> Operators used social media to distort status of fighting, instill panic in residents Ukraine's security agency has shut down five bot farms since the start of Russia's invasion of the country almost five weeks ago, slowing down a Russian operation designed to spread disinformation in the war-torn country and to sow panic among its frightened residents.... [...]
