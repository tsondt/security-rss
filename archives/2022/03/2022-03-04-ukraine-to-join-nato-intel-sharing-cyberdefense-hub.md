Title: Ukraine to join NATO intel-sharing cyberdefense hub
Date: 2022-03-04T17:59:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Government;Security
Slug: 2022-03-04-ukraine-to-join-nato-intel-sharing-cyberdefense-hub

[Source](https://www.bleepingcomputer.com/news/government/ukraine-to-join-nato-intel-sharing-cyberdefense-hub/){:target="_blank" rel="noopener"}

> While Ukraine is yet to become a member of the North Atlantic Treaty Organization (NATO), the country has been accepted as a contributing participant to the NATO Cooperative Cyber Defence Centre of Excellence (CCDCOE). [...]
