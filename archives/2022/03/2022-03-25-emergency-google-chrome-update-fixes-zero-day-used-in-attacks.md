Title: Emergency Google Chrome update fixes zero-day used in attacks
Date: 2022-03-25T15:10:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-03-25-emergency-google-chrome-update-fixes-zero-day-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/emergency-google-chrome-update-fixes-zero-day-used-in-attacks/){:target="_blank" rel="noopener"}

> Google has released Chrome 99.0.4844.84 for Windows, Mac, and Linux users to address a high-severity zero-day bug exploited in the wild. [...]
