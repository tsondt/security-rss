Title: HubSpot Data Breach Ripples Through Crytocurrency Industry
Date: 2022-03-24T17:11:40+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks;News;Web Security
Slug: 2022-03-24-hubspot-data-breach-ripples-through-crytocurrency-industry

[Source](https://threatpost.com/hubspot-data-breach-crytocurrency-industry/179086/){:target="_blank" rel="noopener"}

> ~30 crypto companies were affected, including BlockFi, Swan Bitcoin and NYDIG, providing an uncomfortable reminder about how much data CRM systems snarf up. [...]
