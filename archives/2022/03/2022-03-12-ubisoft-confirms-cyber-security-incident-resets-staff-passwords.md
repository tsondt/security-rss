Title: Ubisoft confirms 'cyber security incident', resets staff passwords
Date: 2022-03-12T01:16:34-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-12-ubisoft-confirms-cyber-security-incident-resets-staff-passwords

[Source](https://www.bleepingcomputer.com/news/security/ubisoft-confirms-cyber-security-incident-resets-staff-passwords/){:target="_blank" rel="noopener"}

> Video game developer Ubisoft has confirmed that it suffered a 'cyber security incident' that caused disruption to some of its services. Data extortion group LAPSUS$, who has claimed responsibility for hacking Samsung, NVIDIA, and Mercado Libre thus far, also appears to be behind Ubisoft incident. [...]
