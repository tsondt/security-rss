Title: Toyota shuts down production after ‘cyber-attack’ on supplier
Date: 2022-03-01T16:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-01-toyota-shuts-down-production-after-cyber-attack-on-supplier

[Source](https://portswigger.net/daily-swig/toyota-shuts-down-production-after-cyber-attack-on-supplier){:target="_blank" rel="noopener"}

> JITter in the supply chain [...]
