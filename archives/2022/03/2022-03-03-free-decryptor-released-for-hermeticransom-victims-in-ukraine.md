Title: Free decryptor released for HermeticRansom victims in Ukraine
Date: 2022-03-03T11:30:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-03-free-decryptor-released-for-hermeticransom-victims-in-ukraine

[Source](https://www.bleepingcomputer.com/news/security/free-decryptor-released-for-hermeticransom-victims-in-ukraine/){:target="_blank" rel="noopener"}

> Avast Threat Labs has released a decryptor for the HermeticRansom ransomware strain used predominately in targeted attacks against Ukrainian systems in the past ten days. [...]
