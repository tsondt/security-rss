Title: Dell opts out of Microsoft's Pluton security for Windows
Date: 2022-03-09T18:30:09+00:00
Author: Agam Shah
Category: The Register
Tags: 
Slug: 2022-03-09-dell-opts-out-of-microsofts-pluton-security-for-windows

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/09/dell_pluton_microsoft/){:target="_blank" rel="noopener"}

> This doesn't align with our approach, PC giant tells us Yet another top-tier PC maker seemingly isn't interested right now in Microsoft's vision of hardware-level security for Windows 11 systems.... [...]
