Title: Google Blows Lid Off Conti, Diavol Ransomware Access-Broker Ops
Date: 2022-03-18T14:49:01+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Hacks;Malware;Web Security
Slug: 2022-03-18-google-blows-lid-off-conti-diavol-ransomware-access-broker-ops

[Source](https://threatpost.com/google-conti-diavol-ransomware-access-broker/178981/){:target="_blank" rel="noopener"}

> Researchers have exposed the work of Exotic Lily, a full-time cybercriminal initial-access group that uses phishing to infiltrate organizations’ networks for further malicious activity. [...]
