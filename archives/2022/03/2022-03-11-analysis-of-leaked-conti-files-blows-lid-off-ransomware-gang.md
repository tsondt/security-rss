Title: Analysis of leaked Conti files blows lid off ransomware gang
Date: 2022-03-11T00:30:30+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-11-analysis-of-leaked-conti-files-blows-lid-off-ransomware-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/11/conti_leaks_code/){:target="_blank" rel="noopener"}

> Not only is this payback sweet, it gives network defenders valuable intelligence It was a Ukrainian security specialist who apparently turned the tables on the notorious Russia-based Conti, and leaked the ransomware gang's source code, chat logs, and tons of other sensitive data about the gang's operations, tools, and costs.... [...]
