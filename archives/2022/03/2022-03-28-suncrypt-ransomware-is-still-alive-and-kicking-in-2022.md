Title: SunCrypt ransomware is still alive and kicking in 2022
Date: 2022-03-28T14:35:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-28-suncrypt-ransomware-is-still-alive-and-kicking-in-2022

[Source](https://www.bleepingcomputer.com/news/security/suncrypt-ransomware-is-still-alive-and-kicking-in-2022/){:target="_blank" rel="noopener"}

> SunCrypt, a ransomware as service (RaaS) operation that reached prominence in mid-2020, is reportedly still active, even if barely, as its operators continue to work on giving its strain new capabilities. [...]
