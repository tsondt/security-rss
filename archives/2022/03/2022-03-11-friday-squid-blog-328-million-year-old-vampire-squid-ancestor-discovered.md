Title: Friday Squid Blog: 328-million-year-old Vampire Squid Ancestor Discovered
Date: 2022-03-11T22:01:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2022-03-11-friday-squid-blog-328-million-year-old-vampire-squid-ancestor-discovered

[Source](https://www.schneier.com/blog/archives/2022/03/friday-squid-blog-328-million-year-old-vampire-squid-ancestor-discovered.html){:target="_blank" rel="noopener"}

> A fossilized ancestor of the vampire squid — with ten arms — was discovered and named Syllipsimopodi bideni after President Biden. Here’s the research paper. Note: Vampire squids are not squids. (Yes, it’s weird.) As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
