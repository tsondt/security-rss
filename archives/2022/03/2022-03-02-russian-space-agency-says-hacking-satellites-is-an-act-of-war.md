Title: Russian space agency says hacking satellites is an act of war
Date: 2022-03-02T15:58:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-02-russian-space-agency-says-hacking-satellites-is-an-act-of-war

[Source](https://www.bleepingcomputer.com/news/security/russian-space-agency-says-hacking-satellites-is-an-act-of-war/){:target="_blank" rel="noopener"}

> Russia will consider any cyberattacks targeting Russian satellite infrastructure an act of war, as the country's space agency director said in a TV interview. [...]
