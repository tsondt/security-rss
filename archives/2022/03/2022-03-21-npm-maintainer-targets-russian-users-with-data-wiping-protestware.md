Title: NPM maintainer targets Russian users with data-wiping ‘protestware’
Date: 2022-03-21T17:07:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-21-npm-maintainer-targets-russian-users-with-data-wiping-protestware

[Source](https://portswigger.net/daily-swig/npm-maintainer-targets-russian-users-with-data-wiping-protestware){:target="_blank" rel="noopener"}

> GUI-rilla warfare [...]
