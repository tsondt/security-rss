Title: Hackers claim to breach TransUnion South Africa with 'Password' password
Date: 2022-03-18T11:32:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-18-hackers-claim-to-breach-transunion-south-africa-with-password-password

[Source](https://www.bleepingcomputer.com/news/security/hackers-claim-to-breach-transunion-south-africa-with-password-password/){:target="_blank" rel="noopener"}

> TransUnion South Africa has disclosed that hackers breached one of their servers using stolen credentials and demanded a extortion demand not to release stolen data. [...]
