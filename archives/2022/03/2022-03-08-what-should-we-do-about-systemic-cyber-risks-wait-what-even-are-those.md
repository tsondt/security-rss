Title: What should we do about 'systemic' cyber risks? Wait, what even are those
Date: 2022-03-08T18:45:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-03-08-what-should-we-do-about-systemic-cyber-risks-wait-what-even-are-those

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/08/system-cyber-risk-rising/){:target="_blank" rel="noopener"}

> Complexity and scale of the internet hold back our ability to tackle disaster Analysis In a report published this week addressing "systemic" cybersecurity risks, several infosec experts noted that as the number of significant network intrusions rises, an understanding of the problem and the ability to address the larger issues remain lacking.... [...]
