Title: Washington residents’ medical data exposed by phishing attack on Spokane Regional Health District
Date: 2022-03-25T14:21:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-25-washington-residents-medical-data-exposed-by-phishing-attack-on-spokane-regional-health-district

[Source](https://portswigger.net/daily-swig/washington-residents-medical-data-exposed-by-phishing-attack-on-spokane-regional-health-district){:target="_blank" rel="noopener"}

> Medications and test results among data potentially ‘previewed’ by attacker [...]
