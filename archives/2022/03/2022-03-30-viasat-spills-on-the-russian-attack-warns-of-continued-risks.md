Title: Viasat spills on the Russian attack, warns of continued risks
Date: 2022-03-30T16:45:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-03-30-viasat-spills-on-the-russian-attack-warns-of-continued-risks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/30/viasat_spills_on_russian_attack/){:target="_blank" rel="noopener"}

> A misconfigured VPN appliance is to blame It turns out the only thing Russian forces needed to knock thousands of Ukrainian satellite broadband customers offline was a misconfigured VPN.... [...]
