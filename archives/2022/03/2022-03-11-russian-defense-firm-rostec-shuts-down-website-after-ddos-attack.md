Title: Russian defense firm Rostec shuts down website after DDoS attack
Date: 2022-03-11T09:50:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-11-russian-defense-firm-rostec-shuts-down-website-after-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/russian-defense-firm-rostec-shuts-down-website-after-ddos-attack/){:target="_blank" rel="noopener"}

> Rostec, a Russian state-owned aerospace and defense conglomerate, said its website was taken down today following what it described as a "cyberattack." [...]
