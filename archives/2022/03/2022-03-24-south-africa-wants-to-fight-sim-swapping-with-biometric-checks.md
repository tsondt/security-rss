Title: South Africa wants to fight SIM swapping with biometric checks
Date: 2022-03-24T13:50:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-03-24-south-africa-wants-to-fight-sim-swapping-with-biometric-checks

[Source](https://www.bleepingcomputer.com/news/security/south-africa-wants-to-fight-sim-swapping-with-biometric-checks/){:target="_blank" rel="noopener"}

> The independent communications authority of South Africa (ICASA) has submitted a radical proposal to tackle the problem of SIM swapping attacks in the country, suggesting that local service providers should keep biometric data of cellphone number owners. [...]
