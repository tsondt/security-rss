Title: Microsoft Azure DevOps revives TLS 1.0/1.1 with rollback
Date: 2022-03-15T19:24:00+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-03-15-microsoft-azure-devops-revives-tls-1011-with-rollback

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/microsoft_azure_tls/){:target="_blank" rel="noopener"}

> Planned deprecation didn't go as planned, cloud biz aims to try again at the end of March Microsoft's Azure DevOps team has undone the deprecation of outdated Transport Layer Security (TLS) that occurred at the end of January because of unspecified "unexpected issues" that arose following the change.... [...]
