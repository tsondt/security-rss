Title: How the tech community has rallied to Ukraine’s cyber-defence | Joyce Hakmeh and Esther Naylor
Date: 2022-03-07T13:52:52+00:00
Author: Joyce Hakmeh and Esther Naylor
Category: The Guardian
Tags: Cyberwar;Hacking;Ukraine;Internet;Technology;World news;Russia;Malware;Data and computer security
Slug: 2022-03-07-how-the-tech-community-has-rallied-to-ukraines-cyber-defence-joyce-hakmeh-and-esther-naylor

[Source](https://www.theguardian.com/commentisfree/2022/mar/07/tech-community-rallied-ukraine-cyber-defence-eu-nato){:target="_blank" rel="noopener"}

> From an army of volunteers to EU and Nato teams, the variety of online actors working for the cause is unprecedented As the conflict in Ukraine escalates, expert cyber-watchers have been speculating about the kind of cyber-attacks that Russia might conduct. Will the Kremlin turn off Ukraine’s power grid, dismantle Ukraine’s transport system, cut off the water supply or target the health system? Or would cybercriminals operating from Russia, who could act as proxies for the Russian regime, conduct these activities? Over the past decade, Ukraine has experienced many major cyber-attacks, most of which have been attributed to Russia. From [...]
