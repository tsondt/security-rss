Title: Critical Firefox Zero-Day Bugs Allow RCE, Sandbox Escape
Date: 2022-03-07T16:19:15+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-03-07-critical-firefox-zero-day-bugs-allow-rce-sandbox-escape

[Source](https://threatpost.com/firefox-zero-day-bugs-rce-sandbox-escape/178779/){:target="_blank" rel="noopener"}

> Both vulnerabilities are use-after-free issues in Mozilla's popular web browser. [...]
