Title: Reg reader rages over Virgin Media's email password policy
Date: 2022-03-10T10:29:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-10-reg-reader-rages-over-virgin-medias-email-password-policy

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/10/virgin_media_email_password_security/){:target="_blank" rel="noopener"}

> No more than 10 alphanumerics, no special characters – in 2022? A Register reader has raised concerns over UK ISP Virgin Media's password policies after discovering he couldn't set a password longer than 10 characters or one that includes non-alphanumeric characters.... [...]
