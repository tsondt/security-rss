Title: Microsoft: Lapsus$ Used Employee Account to Steal Source Code
Date: 2022-03-23T15:28:03+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;Malware;Web Security
Slug: 2022-03-23-microsoft-lapsus-used-employee-account-to-steal-source-code

[Source](https://threatpost.com/microsoft-lapsus-compromised-one-employees-account/179048/){:target="_blank" rel="noopener"}

> The data-extortion gang got at Microsoft's Azure DevOps server. Meanwhile, fellow Lapsus$ victim and authentication firm Okta said 2.5 percent of customers were affected in its own Lapsus$ attack. [...]
