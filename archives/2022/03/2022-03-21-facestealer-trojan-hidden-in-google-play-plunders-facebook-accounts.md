Title: Facestealer Trojan Hidden in Google Play Plunders Facebook Accounts
Date: 2022-03-21T19:18:32+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2022-03-21-facestealer-trojan-hidden-in-google-play-plunders-facebook-accounts

[Source](https://threatpost.com/facestealer-trojan-google-play-facebook/179015/){:target="_blank" rel="noopener"}

> The trojanized Craftsart Cartoon Photo Tools app is available in the official Android app store, but it's actually spyware capable of stealing any and all information from victims' social-media accounts. [...]
