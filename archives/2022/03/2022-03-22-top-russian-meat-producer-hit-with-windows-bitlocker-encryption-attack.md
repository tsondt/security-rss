Title: Top Russian meat producer hit with Windows BitLocker encryption attack
Date: 2022-03-22T08:43:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-22-top-russian-meat-producer-hit-with-windows-bitlocker-encryption-attack

[Source](https://www.bleepingcomputer.com/news/security/top-russian-meat-producer-hit-with-windows-bitlocker-encryption-attack/){:target="_blank" rel="noopener"}

> Moscow-based meat producer and distributor Miratorg Agribusiness Holding has suffered a major cyberattack that encrypted its IT systems, according to a report from Rosselkhoznadzor - the Russian federal veterinary and phytosanitary supervision service. [...]
