Title: FIDO: Here’s Another Knife to Help Murder Passwords
Date: 2022-03-22T15:42:39+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Web Security
Slug: 2022-03-22-fido-heres-another-knife-to-help-murder-passwords

[Source](https://threatpost.com/fido-knife-murder-passwords/179031/){:target="_blank" rel="noopener"}

> After years of promising a passwordless future – really, any day now! – FIDO is proposing tweaks to WebAuthn that could put us out of password misery. Experts aren’t so sure. [...]
