Title: Chrome Zero-Day from North Korea
Date: 2022-03-31T11:13:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Chrome;hacking;North Korea;zero-day
Slug: 2022-03-31-chrome-zero-day-from-north-korea

[Source](https://www.schneier.com/blog/archives/2022/03/chrome-zero-day-from-north-korea.html){:target="_blank" rel="noopener"}

> North Korean hackers have been exploiting a zero-day in Chrome. The flaw, tracked as CVE-2022-0609, was exploited by two separate North Korean hacking groups. Both groups deployed the same exploit kit on websites that either belonged to legitimate organizations and were hacked or were set up for the express purpose of serving attack code on unsuspecting visitors. One group was dubbed Operation Dream Job, and it targeted more than 250 people working for 10 different companies. The other group, known as AppleJeus, targeted 85 users. Details : The attackers made use of an exploit kit that contained multiple stages and [...]
