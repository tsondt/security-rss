Title: Western Digital fixes critical bug giving root on My Cloud NAS devices
Date: 2022-03-26T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-26-western-digital-fixes-critical-bug-giving-root-on-my-cloud-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/western-digital-fixes-critical-bug-giving-root-on-my-cloud-nas-devices/){:target="_blank" rel="noopener"}

> Western Digital has fixed a critical severity vulnerability in the Samba vfs_fruit VFS module that enabled attackers to gain remote code execution with root privileges on unpatched My Cloud OS 5 devices. [...]
