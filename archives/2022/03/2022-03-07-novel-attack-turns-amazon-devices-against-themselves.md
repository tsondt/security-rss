Title: Novel Attack Turns Amazon Devices Against Themselves
Date: 2022-03-07T21:30:12+00:00
Author: Nate Nelson
Category: Threatpost
Tags: IoT;Vulnerabilities
Slug: 2022-03-07-novel-attack-turns-amazon-devices-against-themselves

[Source](https://threatpost.com/attack-amazon-devices-against-themselves/178797/){:target="_blank" rel="noopener"}

> Researchers have discovered how to remotely manipulate the Amazon Echo through its own speakers. [...]
