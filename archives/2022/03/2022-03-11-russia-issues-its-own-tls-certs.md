Title: Russia Issues Its Own TLS Certs
Date: 2022-03-11T18:34:34+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Web Security
Slug: 2022-03-11-russia-issues-its-own-tls-certs

[Source](https://threatpost.com/russia-issues-its-own-tls-certs/178891/){:target="_blank" rel="noopener"}

> The country’s citizens are being blocked from the internet because foreign certificate authorities can't accept payments due to Ukraine-related sanctions, so it created its own CA. [...]
