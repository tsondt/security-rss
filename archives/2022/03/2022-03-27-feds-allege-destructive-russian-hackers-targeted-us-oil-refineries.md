Title: Feds allege destructive Russian hackers targeted US oil refineries
Date: 2022-03-27T09:14:07+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Policy;hacking;Infrastructure;malware;oil;refineries;russia;triton
Slug: 2022-03-27-feds-allege-destructive-russian-hackers-targeted-us-oil-refineries

[Source](https://arstechnica.com/?p=1843669){:target="_blank" rel="noopener"}

> Enlarge / Critical infrastructure sites such as this oil refinery in Port Arthur, Texas, rely on safety systems. (credit: IIP Photo Archive ) For years, the hackers behind the malware known as Triton or Trisis have stood out as a uniquely dangerous threat to critical infrastructure: a group of digital intruders who attempted to sabotage industrial safety systems, with physical, potentially catastrophic results. Now the US Department of Justice has put a name to one of the hackers in that group—and confirmed the hackers' targets included a US company that owns multiple oil refineries. On Thursday, just days after the [...]
