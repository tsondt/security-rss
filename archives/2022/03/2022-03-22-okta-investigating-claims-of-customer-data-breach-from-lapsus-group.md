Title: Okta investigating claims of customer data breach from Lapsus$ group
Date: 2022-03-22T03:15:48-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-22-okta-investigating-claims-of-customer-data-breach-from-lapsus-group

[Source](https://www.bleepingcomputer.com/news/security/okta-investigating-claims-of-customer-data-breach-from-lapsus-group/){:target="_blank" rel="noopener"}

> Okta, a leading provider of authentication services and Identity and access management (IAM) solutions says it is investigating claims of data breach. [...]
