Title: FBI adds Russian cybercrime market owner to most wanted list
Date: 2022-03-23T19:05:46-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-23-fbi-adds-russian-cybercrime-market-owner-to-most-wanted-list

[Source](https://www.bleepingcomputer.com/news/security/fbi-adds-russian-cybercrime-market-owner-to-most-wanted-list/){:target="_blank" rel="noopener"}

> A Russian national has been indicted by the US DOJ and added to the FBI's Cyber Most Wanted list for allegedly creating and managing a cybercrime marketplace. [...]
