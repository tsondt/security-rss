Title: Amazon Alexa can be hijacked via commands from own speaker
Date: 2022-03-03T18:31:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-03-amazon-alexa-can-be-hijacked-via-commands-from-own-speaker

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/03/amazon_alexa_speaker_vuln/){:target="_blank" rel="noopener"}

> This isn't the artificial intelligence we were promised Without a critical update, Amazon Alexa devices could wake themselves up and start executing audio commands issued by a remote attacker, according to infosec researchers at Royal Holloway, University of London.... [...]
