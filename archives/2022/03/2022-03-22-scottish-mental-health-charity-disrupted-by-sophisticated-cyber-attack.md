Title: Scottish mental health charity disrupted by ‘sophisticated’&nbsp;cyber-attack
Date: 2022-03-22T15:50:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-22-scottish-mental-health-charity-disrupted-by-sophisticated-cyber-attack

[Source](https://portswigger.net/daily-swig/scottish-mental-health-charity-disrupted-by-sophisticated-nbsp-cyber-attack){:target="_blank" rel="noopener"}

> Organization is ‘devastated’ over security incident [...]
