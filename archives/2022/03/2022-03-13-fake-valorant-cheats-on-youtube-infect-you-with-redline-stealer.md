Title: Fake Valorant cheats on YouTube infect you with RedLine stealer
Date: 2022-03-13T10:06:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-13-fake-valorant-cheats-on-youtube-infect-you-with-redline-stealer

[Source](https://www.bleepingcomputer.com/news/security/fake-valorant-cheats-on-youtube-infect-you-with-redline-stealer/){:target="_blank" rel="noopener"}

> Korean security analysts have spotted a malware distribution campaign that uses Valorant cheat lures on YouTube to trick players into downloading RedLine, a powerful information stealer. [...]
