Title: Israeli government websites temporarily knocked offline by ‘massive’ cyber-attack
Date: 2022-03-15T11:55:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-15-israeli-government-websites-temporarily-knocked-offline-by-massive-cyber-attack

[Source](https://portswigger.net/daily-swig/israeli-government-websites-temporarily-knocked-offline-by-massive-cyber-attack){:target="_blank" rel="noopener"}

> DDoS assault blamed on Iran, local media reports [...]
