Title: Kaspersky, China Telecom, China Mobile named 'threats to US national security'
Date: 2022-03-28T00:14:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-28-kaspersky-china-telecom-china-mobile-named-threats-to-us-national-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/28/fcc_kaspersky_china_telecom_china_mobile_national_security/){:target="_blank" rel="noopener"}

> First Russian addition to FCC's Very Naughty List apparently unconnected to illegal invasion of Ukraine The United Stations Federal Communications Commission (FCC) has labelled Kaspersky, China Mobile, and China Telecom as threats to national security.... [...]
