Title: How to set up federated single sign-on to AWS using Google Workspace
Date: 2022-03-10T18:06:32+00:00
Author: Wei Chen
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Federation;IAM;SSO
Slug: 2022-03-10-how-to-set-up-federated-single-sign-on-to-aws-using-google-workspace

[Source](https://aws.amazon.com/blogs/security/how-to-set-up-federated-single-sign-on-to-aws-using-google-workspace/){:target="_blank" rel="noopener"}

> Organizations who want to federate their external identity provider (IdP) to AWS will typically do it through AWS Single Sign-On (AWS SSO), AWS Identity and Access Management (IAM), or use both. With AWS SSO, you configure federation once and manage access to all of your AWS accounts centrally. With AWS IAM, you configure federation to [...]
