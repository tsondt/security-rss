Title: Cisco joins long list of security companies supporting Ukraine
Date: 2022-03-04T12:39:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-04-cisco-joins-long-list-of-security-companies-supporting-ukraine

[Source](https://www.bleepingcomputer.com/news/security/cisco-joins-long-list-of-security-companies-supporting-ukraine/){:target="_blank" rel="noopener"}

> Cisco has joined the growing list of security and technology companies that no longer offer services in Russia after their invasion of Ukraine. [...]
