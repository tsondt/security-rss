Title: US Critical Infrastructure Companies Will Have to Report When They Are Hacked
Date: 2022-03-15T11:01:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cyberespionage;cybersecurity;defense;espionage;hacking;infrastructure;laws;ransomware
Slug: 2022-03-15-us-critical-infrastructure-companies-will-have-to-report-when-they-are-hacked

[Source](https://www.schneier.com/blog/archives/2022/03/us-critical-infrastructure-companies-will-have-to-report-when-they-are-hacked.html){:target="_blank" rel="noopener"}

> This will be law soon: Companies critical to U.S. national interests will now have to report when they’re hacked or they pay ransomware, according to new rules approved by Congress. [...] The reporting requirement legislation was approved by the House and the Senate on Thursday and is expected to be signed into law by President Joe Biden soon. It requires any entity that’s considered part of the nation’s critical infrastructure, which includes the finance, transportation and energy sectors, to report any “substantial cyber incident” to the government within three days and any ransomware payment made within 24 hours. Even better [...]
