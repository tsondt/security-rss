Title: China's annual parliament gives tech industry much to ponder
Date: 2022-03-08T07:53:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-08-chinas-annual-parliament-gives-tech-industry-much-to-ponder

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/08/china_two_sessions_2022/){:target="_blank" rel="noopener"}

> More digitization, more R&D, and more provocation of tech-producing neighbours China is this week staging its annual "Two Sessions" meetings, which see its pair of top decision-making bodies meet to set the agenda for the coming year. As usual there is plenty of material that touches on tech.... [...]
