Title: Microsoft Azure Developers Awash in PII-Stealing npm Packages
Date: 2022-03-24T20:21:02+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Malware;Web Security
Slug: 2022-03-24-microsoft-azure-developers-awash-in-pii-stealing-npm-packages

[Source](https://threatpost.com/microsoft-azure-developers-pii-stealing-npm-packages/179096/){:target="_blank" rel="noopener"}

> A large-scale, automated typosquatting attack saw 200+ malicious packages flood the npm code repository, targeting popular Azure scopes. [...]
