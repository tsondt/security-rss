Title: Viasat shares details on KA-SAT satellite service cyberattack
Date: 2022-03-30T09:50:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-30-viasat-shares-details-on-ka-sat-satellite-service-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/viasat-shares-details-on-ka-sat-satellite-service-cyberattack/){:target="_blank" rel="noopener"}

> US satellite communications provider Viasat has shared an incident report regarding the cyberattack that affected its KA-SAT consumer-oriented satellite broadband service on February 24, the day Russia invaded Ukraine. [...]
