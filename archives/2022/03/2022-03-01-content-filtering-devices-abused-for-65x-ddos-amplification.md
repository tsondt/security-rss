Title: Content filtering devices abused for 65x DDoS amplification
Date: 2022-03-01T11:06:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-01-content-filtering-devices-abused-for-65x-ddos-amplification

[Source](https://www.bleepingcomputer.com/news/security/content-filtering-devices-abused-for-65x-ddos-amplification/){:target="_blank" rel="noopener"}

> Researchers have identified an alarming new trend in DDoS attacks that target middlebox devices to attain enormous 6,533% amplification levels. With such an amplification level, threat actors can launch catastrophic attacks with limited bandwidth/equipment. [...]
