Title: NVIDIA confirms data was stolen in recent cyberattack
Date: 2022-03-01T11:46:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-01-nvidia-confirms-data-was-stolen-in-recent-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/nvidia-confirms-data-was-stolen-in-recent-cyberattack/){:target="_blank" rel="noopener"}

> Chipmaker giant Nvidia confirms that its network was breached in a cyberattack last week, giving intruders access to proprietary information data and employee login data. [...]
