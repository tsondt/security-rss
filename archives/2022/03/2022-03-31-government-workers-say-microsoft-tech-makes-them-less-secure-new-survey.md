Title: Government workers say Microsoft tech makes them less secure: new survey
Date: 2022-03-31T16:00:00+00:00
Author: Jeanette Manfra
Category: GCP Security
Tags: Google Workspace;Public Sector;Identity & Security
Slug: 2022-03-31-government-workers-say-microsoft-tech-makes-them-less-secure-new-survey

[Source](https://cloud.google.com/blog/products/identity-security/government-workers-say-microsoft-tech-makes-them-less-secure-new-survey/){:target="_blank" rel="noopener"}

> Prior to joining Google Cloud, I spent 20 years in the public sector serving in various security roles, most recently as the head of the cybersecurity division at the newly established Cybersecurity and Infrastructure Security Agency (CISA). I was responsible for delivering services and capabilities to about 100 civilian agencies, as well as our critical infrastructure for state and local partners across the country. Throughout my time serving in the U.S. government, I spent a great deal of energy working to keep bad actors out of our systems. What I eventually realized was that we were at a disadvantage. We [...]
