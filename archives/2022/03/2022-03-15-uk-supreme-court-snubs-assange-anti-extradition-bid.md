Title: UK Supreme Court snubs Assange anti-extradition bid
Date: 2022-03-15T18:02:46+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-15-uk-supreme-court-snubs-assange-anti-extradition-bid

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/15/assange_uk_extradition/){:target="_blank" rel="noopener"}

> Home Secretary ponders putting WikiLeaker on one-way US flight Julian Assange has all but lost his fight against extradition from Britain to America after the UK Supreme Court said his case "did not raise an arguable point of law."... [...]
