Title: Europol dismantles massive call center investment scam operation
Date: 2022-03-29T09:48:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-03-29-europol-dismantles-massive-call-center-investment-scam-operation

[Source](https://www.bleepingcomputer.com/news/security/europol-dismantles-massive-call-center-investment-scam-operation/){:target="_blank" rel="noopener"}

> Europol has announced the arrest of 108 people suspected of being involved in an international call center operation that tricked victims into investment scams. [...]
