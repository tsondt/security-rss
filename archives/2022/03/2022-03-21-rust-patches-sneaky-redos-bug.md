Title: Rust patches sneaky ReDoS bug
Date: 2022-03-21T13:12:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-21-rust-patches-sneaky-redos-bug

[Source](https://portswigger.net/daily-swig/rust-patches-sneaky-redos-bug){:target="_blank" rel="noopener"}

> Regex defenses restored to thwart resource consumption trap [...]
