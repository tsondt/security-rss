Title: LockBit victim estimates cost of ransomware attack to be $42 million
Date: 2022-03-31T09:30:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-31-lockbit-victim-estimates-cost-of-ransomware-attack-to-be-42-million

[Source](https://www.bleepingcomputer.com/news/security/lockbit-victim-estimates-cost-of-ransomware-attack-to-be-42-million/){:target="_blank" rel="noopener"}

> Atento has published its 2021 financial performance results, which have a massive $42.1 million dent from a ransomware attack the firm suffered in October 2021. [...]
