Title: NHS Digital's demise bad for 55 million patients' privacy – ex-chairman
Date: 2022-03-04T11:53:26+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-03-04-nhs-digitals-demise-bad-for-55-million-patients-privacy-ex-chairman

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/04/nhs_digital_privacy_bmj_article/){:target="_blank" rel="noopener"}

> IT and data arm now part of NHS England, which could be pressured into data sharing without proper oversight Ten months after attempts first began to extract the medical information of 55 million citizens in England, NHS Digital's former chairman is warning the merger of the agency with NHS England threatens the privacy of people's personal data.... [...]
