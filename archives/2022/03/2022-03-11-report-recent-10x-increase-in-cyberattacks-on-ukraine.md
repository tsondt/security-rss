Title: Report: Recent 10x Increase in Cyberattacks on Ukraine
Date: 2022-03-11T16:50:11+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Russia's War on Ukraine;The Coming Storm;anycast;Bill Woodcock;DNS;John Todd;LINX;London Internet Exchange;MegaFon;Packet Clearing House;Quad9;Rostelecom
Slug: 2022-03-11-report-recent-10x-increase-in-cyberattacks-on-ukraine

[Source](https://krebsonsecurity.com/2022/03/report-recent-10x-increase-in-cyberattacks-on-ukraine/){:target="_blank" rel="noopener"}

> As their cities suffered more intense bombardment by Russian military forces this week, Ukrainian Internet users came under renewed cyberattacks, with one Internet company providing service there saying they blocked ten times the normal number of phishing and malware attacks targeting Ukrainians. John Todd is general manager of Quad9, a free “anycast” DNS platform. DNS stands for Domain Name System, which is like a globally distributed phone book for the Internet that maps human-friendly website names (example.com) to numeric Internet addresses (8.8.4.4.) that are easier for computers to manage. Your computer or mobile device generates DNS lookups each time you [...]
