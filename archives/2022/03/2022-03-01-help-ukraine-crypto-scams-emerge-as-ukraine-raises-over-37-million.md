Title: 'Help Ukraine' crypto scams emerge as Ukraine raises over $37 million
Date: 2022-03-01T07:51:31-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-03-01-help-ukraine-crypto-scams-emerge-as-ukraine-raises-over-37-million

[Source](https://www.bleepingcomputer.com/news/security/help-ukraine-crypto-scams-emerge-as-ukraine-raises-over-37-million/){:target="_blank" rel="noopener"}

> Scammers are now targeting unsuspecting users via phishing webpages, forum posts, and email links enticing users to "help Ukraine" by donating cryptocurrency. The development follows Ukraine's successful effort of raising over $37 million in crypto donations from all around the world amid the country's ongoing Russian invasion. [...]
