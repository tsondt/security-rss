Title: Windows zero-day flaw giving admin rights gets unofficial patch, again
Date: 2022-03-21T13:48:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-03-21-windows-zero-day-flaw-giving-admin-rights-gets-unofficial-patch-again

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-zero-day-flaw-giving-admin-rights-gets-unofficial-patch-again/){:target="_blank" rel="noopener"}

> A Windows local privilege escalation zero-day vulnerability that Microsoft has failed to fully address for several months now, allows users to gain administrative privileges in Windows 10, Windows 11, and Windows Server. [...]
