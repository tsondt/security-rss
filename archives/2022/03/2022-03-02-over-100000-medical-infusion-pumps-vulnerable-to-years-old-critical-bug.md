Title: Over 100,000 medical infusion pumps vulnerable to years old critical bug
Date: 2022-03-02T18:27:45-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-03-02-over-100000-medical-infusion-pumps-vulnerable-to-years-old-critical-bug

[Source](https://www.bleepingcomputer.com/news/security/over-100-000-medical-infusion-pumps-vulnerable-to-years-old-critical-bug/){:target="_blank" rel="noopener"}

> Data collected from more than 200,000 network-connected medical infusion pumps used to deliver medication and fluids to patients shows that 75% of them are running with known security issues that attackers could exploit. [...]
