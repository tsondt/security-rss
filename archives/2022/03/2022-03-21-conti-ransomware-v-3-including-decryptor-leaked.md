Title: Conti Ransomware V. 3, Including Decryptor, Leaked
Date: 2022-03-21T17:48:51+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Malware;Web Security
Slug: 2022-03-21-conti-ransomware-v-3-including-decryptor-leaked

[Source](https://threatpost.com/conti-ransomware-v-3-including-decryptor-leaked/179006/){:target="_blank" rel="noopener"}

> The latest is a fresher version of the ransomware pro-Ukraine researcher ContiLeaks already released, but it’s reportedly clunkier code. [...]
