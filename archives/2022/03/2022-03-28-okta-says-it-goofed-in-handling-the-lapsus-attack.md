Title: Okta Says It Goofed in Handling the Lapsus$ Attack
Date: 2022-03-28T18:28:34+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks;Web Security
Slug: 2022-03-28-okta-says-it-goofed-in-handling-the-lapsus-attack

[Source](https://threatpost.com/okta-goofed-lapsus-attack/179129/){:target="_blank" rel="noopener"}

> "We made a mistake," Okta said, owning up to its responsibility for security incidents that hit its service providers and potentially its own customers. [...]
