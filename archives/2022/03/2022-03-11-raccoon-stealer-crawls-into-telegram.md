Title: Raccoon Stealer Crawls Into Telegram
Date: 2022-03-11T15:03:20+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-03-11-raccoon-stealer-crawls-into-telegram

[Source](https://threatpost.com/raccoon-stealer-telegram/178881/){:target="_blank" rel="noopener"}

> The credential-stealing trash panda is using the chat app to store and update C2 addresses as crooks find creative new ways to distribute the malware. [...]
