Title: Why Vaccine Cards Are So Easily Forged
Date: 2022-03-18T11:12:05+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cheating;COVID-19;essays;forgery;security theater;vulnerabilities
Slug: 2022-03-18-why-vaccine-cards-are-so-easily-forged

[Source](https://www.schneier.com/blog/archives/2022/03/why-vaccine-cards-are-so-easily-forged.html){:target="_blank" rel="noopener"}

> My proof of COVID-19 vaccination is recorded on an easy-to-forge paper card. With little trouble, I could print a blank form, fill it out, and snap a photo. Small imperfections wouldn’t pose any problem; you can’t see whether the paper’s weight is right in a digital image. When I fly internationally, I have to show a negative COVID-19 test result. That, too, would be easy to fake. I could change the date on an old test, or put my name on someone else’s test, or even just make something up on my computer. After all, there’s no standard format for [...]
