Title: Millions of APC Smart-UPS devices vulnerable to TLStorm
Date: 2022-03-09T12:29:32+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-03-09-millions-of-apc-smart-ups-devices-vulnerable-to-tlstorm

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/09/tlstorm_apc_ups_critical_zero_days/){:target="_blank" rel="noopener"}

> Critical zero-day vulns spotted in popular Schneider kit If you're managing a smart model from ubiquitous uninterrupted power supply (UPS) device brand APC, you need to apply updates now – a set of three critical zero-day vulnerabilities are making Smart-UPS devices a possible entry point for network infiltration.... [...]
