Title: Switzerland's SWIFT data centre under guard after Russian banks excluded
Date: 2022-03-03T17:30:40+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-03-03-switzerlands-swift-data-centre-under-guard-after-russian-banks-excluded

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/03/swift_data_centre_under_guard/){:target="_blank" rel="noopener"}

> Local police want to head off any worries about possible sabotage A Swiss data centre operated by financial messaging service SWIFT is under guard by police following the exclusion of key Russian banks from the system.... [...]
