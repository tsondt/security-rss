Title: Microsoft Addresses 3 Zero-Days & 3 Critical Bugs for March Patch Tuesday
Date: 2022-03-08T21:42:06+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;IoT;Vulnerabilities;Web Security
Slug: 2022-03-08-microsoft-addresses-3-zero-days-3-critical-bugs-for-march-patch-tuesday

[Source](https://threatpost.com/microsoft-zero-days-critical-bugsmarch-patch-tuesday/178817/){:target="_blank" rel="noopener"}

> The computing giant patched 71 security vulnerabilities in an uncharacteristically light scheduled update, including its first Xbox bug. [...]
