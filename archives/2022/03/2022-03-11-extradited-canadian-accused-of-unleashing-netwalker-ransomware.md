Title: Extradited Canadian accused of unleashing NetWalker ransomware
Date: 2022-03-11T01:46:42+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-03-11-extradited-canadian-accused-of-unleashing-netwalker-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/11/canadian_netwalker_ransomware_extradited_america/){:target="_blank" rel="noopener"}

> More than $28m in crypto-coins found in home, it is claimed US prosecutors on Thursday said they have extradited a Canadian man to America to face charges that he conspired to distribute ransomware.... [...]
