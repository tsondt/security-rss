Title: Coinbase blocks over 25,000 Russian-linked crypto addresses
Date: 2022-03-07T13:49:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-03-07-coinbase-blocks-over-25000-russian-linked-crypto-addresses

[Source](https://www.bleepingcomputer.com/news/security/coinbase-blocks-over-25-000-russian-linked-crypto-addresses/){:target="_blank" rel="noopener"}

> Coinbase, one of the most popular cryptocurrency exchange platforms, announced today that it's blocking access to more than 25,000 blockchain addresses linked to Russian individuals and entities. [...]
