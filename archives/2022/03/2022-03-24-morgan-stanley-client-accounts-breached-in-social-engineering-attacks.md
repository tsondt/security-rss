Title: Morgan Stanley client accounts breached in social engineering attacks
Date: 2022-03-24T18:47:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-24-morgan-stanley-client-accounts-breached-in-social-engineering-attacks

[Source](https://www.bleepingcomputer.com/news/security/morgan-stanley-client-accounts-breached-in-social-engineering-attacks/){:target="_blank" rel="noopener"}

> Morgan Stanley Wealth Management, the wealth and asset management division of Morgan Stanley, says some of its customers had their accounts compromised following vishing attacks. [...]
