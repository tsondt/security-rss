Title: Attackers getting faster at latching onto unpatched vulnerabilities for stealth hacking campaigns – report
Date: 2022-03-28T16:00:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-28-attackers-getting-faster-at-latching-onto-unpatched-vulnerabilities-for-stealth-hacking-campaigns-report

[Source](https://portswigger.net/daily-swig/attackers-getting-faster-at-latching-onto-unpatched-vulnerabilities-for-stealth-hacking-campaigns-report){:target="_blank" rel="noopener"}

> Enterprises need to be ready with ‘battle-tested incident response procedures’ as zero-day exploitation ramps up [...]
