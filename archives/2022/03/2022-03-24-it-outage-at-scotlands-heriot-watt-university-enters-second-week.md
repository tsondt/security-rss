Title: IT outage at Scotland's Heriot-Watt University enters second week
Date: 2022-03-24T09:30:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-03-24-it-outage-at-scotlands-heriot-watt-university-enters-second-week

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/24/heriot_watt_outage/){:target="_blank" rel="noopener"}

> Multiple systems remain down as spokesperson confirms 'security incident' was cyber attack Edinburgh's Heriot-Watt University has entered a second week of woe following a vist by an infosec nasty.... [...]
