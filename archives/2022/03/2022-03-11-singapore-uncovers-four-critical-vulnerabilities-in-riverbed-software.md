Title: Singapore uncovers four critical vulnerabilities in Riverbed software
Date: 2022-03-11T22:49:25+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-03-11-singapore-uncovers-four-critical-vulnerabilities-in-riverbed-software

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/11/riverbed_vulnerabilities/){:target="_blank" rel="noopener"}

> Details emerge of the now-patched flaws Singapore's Cyber Security Group, an agency charged with securing the nation's cyberspace, has uncovered four critical flaws in code from network software company Riverbed.... [...]
