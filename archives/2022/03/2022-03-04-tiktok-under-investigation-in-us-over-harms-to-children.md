Title: TikTok under investigation in US over harms to children
Date: 2022-03-04T11:07:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-03-04-tiktok-under-investigation-in-us-over-harms-to-children

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/04/tiktok_harms_to_children/){:target="_blank" rel="noopener"}

> Probe builds on one already in process for Instagram Reports that ByteDance-owned social media platform TikTok is harmful to children are under investigation by a number of US attorneys general.... [...]
