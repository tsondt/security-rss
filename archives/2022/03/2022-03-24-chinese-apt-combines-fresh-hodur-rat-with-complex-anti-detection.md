Title: Chinese APT Combines Fresh Hodur RAT with Complex Anti-Detection
Date: 2022-03-24T14:08:06+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Government;Malware;Web Security
Slug: 2022-03-24-chinese-apt-combines-fresh-hodur-rat-with-complex-anti-detection

[Source](https://threatpost.com/chinese-apt-combines-fresh-hodur-rat-with-complex-anti-detection/179084/){:target="_blank" rel="noopener"}

> Mustang Panda's already sophisticated cyberespionage campaign has matured even further with the introduction of a brand-new PlugX RAT variant. [...]
