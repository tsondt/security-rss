Title: Lapsus$ Data Kidnappers Claim Snatches From Microsoft, Okta
Date: 2022-03-22T22:14:40+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks;Malware;Web Security
Slug: 2022-03-22-lapsus-data-kidnappers-claim-snatches-from-microsoft-okta

[Source](https://threatpost.com/lapsus-data-kidnappers-claim-snatches-from-microsoft-okta/179041/){:target="_blank" rel="noopener"}

> Lapsus$ shared screenshots of internal Okta systems and 40Gb of purportedly stolen Microsoft data on Bing, Bing Maps and Cortana. [...]
