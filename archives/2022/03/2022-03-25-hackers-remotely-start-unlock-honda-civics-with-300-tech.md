Title: Hackers remotely start, unlock Honda Civics with $300 tech
Date: 2022-03-25T15:00:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-03-25-hackers-remotely-start-unlock-honda-civics-with-300-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/25/honda_civic_hack/){:target="_blank" rel="noopener"}

> Any models made between 2016 and 2020 can have key fob codes sniffed and re-transmitted If you're driving a Honda Civic manufactured between 2016 and 2020, this newly reported key fob hijack should start your worry engine.... [...]
