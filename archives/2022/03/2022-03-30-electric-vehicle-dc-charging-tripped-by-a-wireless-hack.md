Title: Electric Vehicle DC charging tripped by a wireless hack
Date: 2022-03-30T11:31:50+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-03-30-electric-vehicle-dc-charging-tripped-by-a-wireless-hack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/30/brokenwire/){:target="_blank" rel="noopener"}

> No EVs were damaged in the making of this report Researchers from the University of Oxford published details of a vulnerability in the Combined Charging System that has the potential to abort charging.... [...]
