Title: Okta now says: Lapsus$ may in fact have accessed customer info
Date: 2022-03-23T04:14:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-03-23-okta-now-says-lapsus-may-in-fact-have-accessed-customer-info

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/23/rere/){:target="_blank" rel="noopener"}

> Plus: Microsoft reveals gang pulled off limited source code heist after single account compromised Identity management as-a-service platform Okta says the Lapsus$ extortion gang may in fact have managed to see some of its customers' data, and Microsoft has admitted the crew got its grubby paws on some source code.... [...]
