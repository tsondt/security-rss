Title: Microsoft rolling out new endpoint security solution for SMBs
Date: 2022-03-01T14:36:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-03-01-microsoft-rolling-out-new-endpoint-security-solution-for-smbs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-rolling-out-new-endpoint-security-solution-for-smbs/){:target="_blank" rel="noopener"}

> Microsoft has started rolling out its new endpoint security solution for small and medium-sized businesses (SMBs) known as Microsoft Defender for Business to Microsoft 365 Business Premium customers worldwide starting today, March 1st. [...]
