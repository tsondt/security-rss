Title: Nvidia DGX systems prone to side channel, covert attacks
Date: 2022-03-31T13:43:51+00:00
Author: Nicole Hemsoth
Category: The Register
Tags: 
Slug: 2022-03-31-nvidia-dgx-systems-prone-to-side-channel-covert-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/31/dgx-security-attack-vulnerability/){:target="_blank" rel="noopener"}

> Reverse engineering yields sticky microarchitectural vulnerabilities Nvidia's ultra-dense GPU-driven AI training and inference systems are prone to covert and side channel attacks, according to research just published from a team led by Pacific Northwest National Laboratory (PNNL). This might be less concerning for those with on-prem DGX systems, but for cloud vendors selling time on the AI training boxes, the vulnerabilities are worth noting.... [...]
