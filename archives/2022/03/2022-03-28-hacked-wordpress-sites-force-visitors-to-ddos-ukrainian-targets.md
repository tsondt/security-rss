Title: Hacked WordPress sites force visitors to DDoS Ukrainian targets
Date: 2022-03-28T17:55:23-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-03-28-hacked-wordpress-sites-force-visitors-to-ddos-ukrainian-targets

[Source](https://www.bleepingcomputer.com/news/security/hacked-wordpress-sites-force-visitors-to-ddos-ukrainian-targets/){:target="_blank" rel="noopener"}

> Hackers are compromising WordPress sites to insert a malicious script that uses visitors' browsers to perform distributed denial-of-service attacks on Ukrainian websites. [...]
