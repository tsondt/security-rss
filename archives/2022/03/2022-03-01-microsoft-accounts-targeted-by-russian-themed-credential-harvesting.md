Title: Microsoft Accounts Targeted by Russian-Themed Credential Harvesting
Date: 2022-03-01T10:57:23+00:00
Author: Tara Seals
Category: Threatpost
Tags: Web Security
Slug: 2022-03-01-microsoft-accounts-targeted-by-russian-themed-credential-harvesting

[Source](https://threatpost.com/microsoft-accounts-targeted-russian-credential-harvesting/178698/){:target="_blank" rel="noopener"}

> Malicious emails warning Microsoft users of "unusual sign-on activity" from Russia are looking to capitalizing on the Ukrainian crisis. [...]
