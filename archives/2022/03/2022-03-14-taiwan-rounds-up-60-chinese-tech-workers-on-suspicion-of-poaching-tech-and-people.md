Title: Taiwan rounds up 60 Chinese tech workers on suspicion of poaching tech and people
Date: 2022-03-14T04:59:04+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-03-14-taiwan-rounds-up-60-chinese-tech-workers-on-suspicion-of-poaching-tech-and-people

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/14/taiwan_china_tech_worker_raids/){:target="_blank" rel="noopener"}

> The fight against economic espionage and skullduggery continues Taiwan's Ministry of Justice has tasked its Investigation Bureau to conduct a series of raids around the island and hauled in 60 Chinese nationals suspected of lifting trade secrets or poaching talent from China-owned firms.... [...]
