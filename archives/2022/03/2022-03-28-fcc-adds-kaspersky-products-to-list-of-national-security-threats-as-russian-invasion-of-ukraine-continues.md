Title: FCC adds Kaspersky products to list of national security threats as Russian invasion of Ukraine continues
Date: 2022-03-28T14:02:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-28-fcc-adds-kaspersky-products-to-list-of-national-security-threats-as-russian-invasion-of-ukraine-continues

[Source](https://portswigger.net/daily-swig/fcc-adds-kaspersky-products-to-list-of-national-security-threats-as-russian-invasion-of-ukraine-continues){:target="_blank" rel="noopener"}

> Russian antivirus vendor cited in expanded guidance [...]
