Title: QNAP warns severe Linux bug affects most of its NAS devices
Date: 2022-03-14T12:09:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux
Slug: 2022-03-14-qnap-warns-severe-linux-bug-affects-most-of-its-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-severe-linux-bug-affects-most-of-its-nas-devices/){:target="_blank" rel="noopener"}

> Taiwanese hardware vendor QNAP warns most of its Network Attached Storage (NAS) devices are impacted by a high severity Linux vulnerability dubbed 'Dirty Pipe' that allows attackers with local access to gain root privileges. [...]
