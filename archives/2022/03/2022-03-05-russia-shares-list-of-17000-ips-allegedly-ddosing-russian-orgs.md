Title: Russia shares list of 17,000 IPs allegedly DDoSing Russian orgs
Date: 2022-03-05T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-05-russia-shares-list-of-17000-ips-allegedly-ddosing-russian-orgs

[Source](https://www.bleepingcomputer.com/news/security/russia-shares-list-of-17-000-ips-allegedly-ddosing-russian-orgs/){:target="_blank" rel="noopener"}

> The Russian government shared a list of 17,576 IP addresses allegedly used to launch distributed denial-of-service (DDoS) attacks targeting Russian organizations and their networks. [...]
