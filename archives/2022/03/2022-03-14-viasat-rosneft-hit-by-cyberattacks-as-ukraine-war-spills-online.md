Title: Viasat, Rosneft hit by cyberattacks as Ukraine war spills online
Date: 2022-03-14T17:02:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-03-14-viasat-rosneft-hit-by-cyberattacks-as-ukraine-war-spills-online

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/14/viasat_rosneft_ukraine_cyberattacks/){:target="_blank" rel="noopener"}

> One shows signs of a state-sponsored intrusion, the other potentially not Signs of Russian cyberattacks on Western-owned digital systems have begun to emerge – even as the German arm of Russian oil company Rosneft said it was breached over the weekend.... [...]
