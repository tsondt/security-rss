Title: Ukraine invasion: WordPress-hosted university websites hacked in ‘targeted attacks’
Date: 2022-03-02T14:48:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-02-ukraine-invasion-wordpress-hosted-university-websites-hacked-in-targeted-attacks

[Source](https://portswigger.net/daily-swig/ukraine-invasion-wordpress-hosted-university-websites-hacked-in-targeted-attacks){:target="_blank" rel="noopener"}

> Education institutions hit by more than 100,000 attacks in 24 hours [...]
