Title: Spring Cloud framework commits patch for code injection flaw
Date: 2022-03-30T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-30-spring-cloud-framework-commits-patch-for-code-injection-flaw

[Source](https://portswigger.net/daily-swig/spring-cloud-framework-commits-patch-for-code-injection-flaw){:target="_blank" rel="noopener"}

> A fix appears to have been pushed but is not available in a stable release yet [...]
