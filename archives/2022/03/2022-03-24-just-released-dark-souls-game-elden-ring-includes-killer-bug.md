Title: Just-Released Dark Souls Game, Elden Ring, Includes Killer Bug
Date: 2022-03-24T19:23:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-03-24-just-released-dark-souls-game-elden-ring-includes-killer-bug

[Source](https://threatpost.com/dark-souls-game-elden-ring-killer-bug/179090/){:target="_blank" rel="noopener"}

> A patch fixes exploit hidden in Elden Ring that traps PC players in a ‘death loop.’ [...]
