Title: SEC proposes four-day rule for public companies to report cyberattacks
Date: 2022-03-09T21:16:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-03-09-sec-proposes-four-day-rule-for-public-companies-to-report-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/03/09/sec_cyberattack_disclosure/){:target="_blank" rel="noopener"}

> And it'll be in an 8-K for all to see A new rule proposed by the US Securities and Exchange Commission (SEC) would force public companies to disclose cyberattacks within four days along with periodic reports about their cyber-risk management plans.... [...]
