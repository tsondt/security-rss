Title: ProtonMail urges Russian users to renew as payment options dry up
Date: 2022-03-08T11:46:49-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-03-08-protonmail-urges-russian-users-to-renew-as-payment-options-dry-up

[Source](https://www.bleepingcomputer.com/news/security/protonmail-urges-russian-users-to-renew-as-payment-options-dry-up/){:target="_blank" rel="noopener"}

> ProtonMail is urging its Russian user base to hurry up and renew their subscriptions before it is too late, as multiple payment processing services like Mastercard, Visa, and PayPal are exiting the Russian market. ProtonMail is a provider of privacy-centric and end-to-end encrypted email services to millions around the world. [...]
