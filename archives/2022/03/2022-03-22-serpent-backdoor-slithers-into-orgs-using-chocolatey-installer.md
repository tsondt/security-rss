Title: Serpent Backdoor Slithers into Orgs Using Chocolatey Installer
Date: 2022-03-22T14:21:42+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2022-03-22-serpent-backdoor-slithers-into-orgs-using-chocolatey-installer

[Source](https://threatpost.com/serpent-backdoor-chocolatey-installer/179027/){:target="_blank" rel="noopener"}

> An unusual attack using an open-source Python package installer called Chocolatey, steganography and Scheduled Tasks is stealthily delivering spyware to companies. [...]
