Title: Protect your users’ accounts with reCAPTCHA Enterprise’s account defender
Date: 2022-03-10T17:00:00+00:00
Author: Aaron Malenfant
Category: GCP Security
Tags: Retail;Public Sector;Identity & Security
Slug: 2022-03-10-protect-your-users-accounts-with-recaptcha-enterprises-account-defender

[Source](https://cloud.google.com/blog/products/identity-security/use-account-defender-in-recaptcha-enterprise-to-protect-accounts/){:target="_blank" rel="noopener"}

> reCAPTCHA Enterprise is Google’s online fraud detection service that leverages over a decade of experience defending the internet. reCAPTCHA Enterprise can be used to prevent fraud and attacks perpetrated by scripts, bot software, or humans. When installed on a web page at the point of action, such as login, purchase, or account creation, reCAPTCHA Enterprise provides a frictionless user experience that allows legitimate users to proceed while fake users and bots are blocked. As part of our continued investment to offer more tools to fight online fraud targeting your accounts and payments, we are pleased to announce account defender in [...]
