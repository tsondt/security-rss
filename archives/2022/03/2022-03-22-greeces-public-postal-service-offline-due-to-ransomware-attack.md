Title: Greece's public postal service offline due to ransomware attack
Date: 2022-03-22T10:05:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-22-greeces-public-postal-service-offline-due-to-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/greeces-public-postal-service-offline-due-to-ransomware-attack/){:target="_blank" rel="noopener"}

> ELTA, the state-owned provider of postal services in Greece, has disclosed a ransomware incident detected on Sunday that is still keeping most of the organizations services offline. [...]
