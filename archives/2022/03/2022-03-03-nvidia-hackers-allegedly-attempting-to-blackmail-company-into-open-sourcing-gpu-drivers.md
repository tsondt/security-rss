Title: Nvidia hackers allegedly attempting to blackmail company into open-sourcing GPU drivers
Date: 2022-03-03T16:05:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-03-03-nvidia-hackers-allegedly-attempting-to-blackmail-company-into-open-sourcing-gpu-drivers

[Source](https://portswigger.net/daily-swig/nvidia-hackers-allegedly-attempting-to-blackmail-company-into-open-sourcing-gpu-drivers){:target="_blank" rel="noopener"}

> Unusual demand follows request that hardware firm removes mining hashrate limiters on GPUs [...]
