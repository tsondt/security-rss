Title: Staff Think Conti Group Is a Legit Employer – Podcast
Date: 2022-03-14T21:50:45+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Podcasts;Web Security
Slug: 2022-03-14-staff-think-conti-group-is-a-legit-employer-podcast

[Source](https://threatpost.com/staff-think-conti-group-legit-employer-podcast/178903/){:target="_blank" rel="noopener"}

> The ransomware group’s benefits – bonuses, employee of the month, performance reviews & top-notch training – might be better than yours, says BreachQuest’s Marco Figueroa. [...]
