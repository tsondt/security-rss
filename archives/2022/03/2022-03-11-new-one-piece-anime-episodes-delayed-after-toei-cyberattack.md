Title: New ONE PIECE anime episodes delayed after Toei cyberattack
Date: 2022-03-11T10:09:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-03-11-new-one-piece-anime-episodes-delayed-after-toei-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/new-one-piece-anime-episodes-delayed-after-toei-cyberattack/){:target="_blank" rel="noopener"}

> Anime giant Toei suffered a weekend cyberattack causing delays in airing new episodes of popular anime series, including ONE PIECE and Delicious Party Precure. [...]
