Title: Apple emergency update fixes zero-days used to hack iPhones, Macs
Date: 2022-03-31T14:16:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-03-31-apple-emergency-update-fixes-zero-days-used-to-hack-iphones-macs

[Source](https://www.bleepingcomputer.com/news/security/apple-emergency-update-fixes-zero-days-used-to-hack-iphones-macs/){:target="_blank" rel="noopener"}

> Apple has released security updates on Thursday to address two zero-day vulnerabilities exploited by attackers to hack iPhones, iPads, and Macs. [...]
