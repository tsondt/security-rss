Title: Misconfigured Firebase Databases Exposing Data in Mobile Apps
Date: 2022-03-17T14:36:04+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: 2022-03-17-misconfigured-firebase-databases-exposing-data-in-mobile-apps

[Source](https://threatpost.com/misconfigured-firebase-databases-exposing-data-mobile-apps/178965/){:target="_blank" rel="noopener"}

> Five percent of the databases are vulnerable to threat actors: It's a gold mine of exploit opportunity in thousands of mobile apps, researchers say. [...]
