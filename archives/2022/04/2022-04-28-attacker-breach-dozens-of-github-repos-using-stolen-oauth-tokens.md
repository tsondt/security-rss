Title: Attacker Breach ‘Dozens’ of GitHub Repos Using Stolen OAuth Tokens
Date: 2022-04-28T13:14:10+00:00
Author: Threatpost
Category: Threatpost
Tags: Breach;Vulnerabilities
Slug: 2022-04-28-attacker-breach-dozens-of-github-repos-using-stolen-oauth-tokens

[Source](https://threatpost.com/github-repos-stolen-oauth-tokens/179427/){:target="_blank" rel="noopener"}

> GitHub shared the timeline of breaches in April 2022, this timeline encompasses the information related to when a threat actor gained access and stole private repositories belonging to dozens of organizations. [...]
