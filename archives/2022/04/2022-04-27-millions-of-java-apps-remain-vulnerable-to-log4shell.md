Title: Millions of Java Apps Remain Vulnerable to Log4Shell
Date: 2022-04-27T12:11:25+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-04-27-millions-of-java-apps-remain-vulnerable-to-log4shell

[Source](https://threatpost.com/java-apps-vulnerable-log4shell/179397/){:target="_blank" rel="noopener"}

> Four months after the critical flaw was discovered, attackers have a massive attack surface from which they can exploit the flaw and take over systems, researchers found. [...]
