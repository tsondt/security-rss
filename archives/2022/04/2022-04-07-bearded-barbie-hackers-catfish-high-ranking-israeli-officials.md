Title: Bearded Barbie hackers catfish high ranking Israeli officials
Date: 2022-04-07T10:44:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-07-bearded-barbie-hackers-catfish-high-ranking-israeli-officials

[Source](https://www.bleepingcomputer.com/news/security/bearded-barbie-hackers-catfish-high-ranking-israeli-officials/){:target="_blank" rel="noopener"}

> The Hamas-backed hacking group tracked as 'APT-C-23' was found catfishing Israeli officials working in defense, law, enforcement, and government agencies, ultimately leading to the deployment of new malware. [...]
