Title: Instagram beyond pics: Sexual harassers, crypto crooks, ID thieves
Date: 2022-04-14T09:04:56-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-04-14-instagram-beyond-pics-sexual-harassers-crypto-crooks-id-thieves

[Source](https://www.bleepingcomputer.com/news/security/instagram-beyond-pics-sexual-harassers-crypto-crooks-id-thieves/){:target="_blank" rel="noopener"}

> A platform for everyone to seamlessly share their best moments online, Instagram is slowly turning into a mecca for the undesirables—from sexual harassers to crypto "investors" helping you "get rich fast." How do you keep yourself safe against such profiles? [...]
