Title: Wake-up call: Is the infosec skills gap causing a mental health crisis?
Date: 2022-04-07T13:55:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-07-wake-up-call-is-the-infosec-skills-gap-causing-a-mental-health-crisis

[Source](https://portswigger.net/daily-swig/wake-up-call-is-the-infosec-skills-gap-causing-a-mental-health-crisis){:target="_blank" rel="noopener"}

> Increasing workloads are causing depression and anxiety among frontline security staff, report claims [...]
