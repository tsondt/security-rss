Title: Karakurt Ensnares Conti, Diavol Ransomware Groups in Its Web
Date: 2022-04-15T17:34:48+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-04-15-karakurt-ensnares-conti-diavol-ransomware-groups-in-its-web

[Source](https://threatpost.com/karakurt-conti-diavol-ransomware/179317/){:target="_blank" rel="noopener"}

> Connections that show the cybercriminal teams are working together signal shifts in their respective tactics and an expansion of opportunities to target victims. [...]
