Title: Google Chrome emergency update fixes zero-day used in attacks
Date: 2022-04-14T17:36:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-04-14-google-chrome-emergency-update-fixes-zero-day-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-emergency-update-fixes-zero-day-used-in-attacks/){:target="_blank" rel="noopener"}

> Google has released Chrome 100.0.4896.127 for Windows, Mac, and Linux, to fix a high-severity zero-day vulnerability actively used by threat actors in attacks. [...]
