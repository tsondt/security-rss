Title: SSRF Flaw in Fintech Platform Allowed for Compromise of Bank Accounts
Date: 2022-04-07T13:46:17+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-04-07-ssrf-flaw-in-fintech-platform-allowed-for-compromise-of-bank-accounts

[Source](https://threatpost.com/ssrf-flaw-fintech-bank-accounts/179247/){:target="_blank" rel="noopener"}

> Researchers discovered the vulnerability in an API already integrated into many bank systems, which could have defrauded millions of users by giving attackers access to their funds. [...]
