Title: The Week in Ransomware - April 1st 2022 - 'I can fight with a keyboard'
Date: 2022-04-01T19:07:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-01-the-week-in-ransomware-april-1st-2022-i-can-fight-with-a-keyboard

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-1st-2022-i-can-fight-with-a-keyboard/){:target="_blank" rel="noopener"}

> While ransomware is still conducting attacks and all companies must stay alert, ransomware news has been relatively slow this week. However, there were still some interesting stories that we outline below. [...]
