Title: The Week in Ransomware - April 29th 2022 - New operations emerge
Date: 2022-04-29T18:29:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-29-the-week-in-ransomware-april-29th-2022-new-operations-emerge

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-29th-2022-new-operations-emerge/){:target="_blank" rel="noopener"}

> This week we have discovered numerous new ransomware operations that have begun operating, with one appearing to be a rebrand of previous operations. [...]
