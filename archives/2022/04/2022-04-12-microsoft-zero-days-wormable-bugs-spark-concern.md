Title: Microsoft Zero-Days, Wormable Bugs Spark Concern
Date: 2022-04-12T20:00:54+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Vulnerabilities;Web Security
Slug: 2022-04-12-microsoft-zero-days-wormable-bugs-spark-concern

[Source](https://threatpost.com/microsoft-zero-days-wormable-bugs/179273/){:target="_blank" rel="noopener"}

> For April Patch Tuesday, the computing giant addressed a zero-day under active attack and several critical security vulnerabilities, including three that allow self-propagating exploits. [...]
