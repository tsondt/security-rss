Title: Critical bug in Android could allow access to users' media files
Date: 2022-04-21T11:35:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-04-21-critical-bug-in-android-could-allow-access-to-users-media-files

[Source](https://www.bleepingcomputer.com/news/security/critical-bug-in-android-could-allow-access-to-users-media-files/){:target="_blank" rel="noopener"}

> Security analysts have found that Android devices running on Qualcomm and MediaTek chipsets were vulnerable to remote code execution due to a flaw in the implementation of the Apple Lossless Audio Codec (ALAC). [...]
