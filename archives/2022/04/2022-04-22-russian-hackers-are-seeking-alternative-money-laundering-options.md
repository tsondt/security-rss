Title: Russian hackers are seeking alternative money-laundering options
Date: 2022-04-22T14:33:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-22-russian-hackers-are-seeking-alternative-money-laundering-options

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-are-seeking-alternative-money-laundering-options/){:target="_blank" rel="noopener"}

> The Russian cybercrime community, one of the most active and prolific in the world, is turning to alternative money-laundering methods due to sanctions on Russia and law enforcement actions against dark web markets. [...]
