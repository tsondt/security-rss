Title: Zero days are for life, not just for Christmas. Here’s how to deal with them
Date: 2022-04-08T17:15:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-04-08-zero-days-are-for-life-not-just-for-christmas-heres-how-to-deal-with-them

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/08/zero_days_are_for_life/){:target="_blank" rel="noopener"}

> Learn from the best in this session Webinar The Log4j vulnerability put everyone in cybersecurity through the mill last December. So, is it OK to relax now?... [...]
