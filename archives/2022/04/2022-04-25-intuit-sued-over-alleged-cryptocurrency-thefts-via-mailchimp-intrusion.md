Title: Intuit sued over alleged cryptocurrency thefts via Mailchimp intrusion
Date: 2022-04-25T22:15:45+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-25-intuit-sued-over-alleged-cryptocurrency-thefts-via-mailchimp-intrusion

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/25/intuit-mailchimp-cryptocurrency/){:target="_blank" rel="noopener"}

> Financial software giant slammed for 'poor security practices' Intuit is being sued in the US after a security failure at its Mailchimp email marketing business allegedly led to the theft of cryptocurrency from one or more digital wallets.... [...]
