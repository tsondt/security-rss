Title: The Week in Ransomware - April 15th 2022 - Encrypting Russia
Date: 2022-04-15T17:19:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-15-the-week-in-ransomware-april-15th-2022-encrypting-russia

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-15th-2022-encrypting-russia/){:target="_blank" rel="noopener"}

> While countries worldwide have been the frequent target of ransomware attacks, Russia and CIS countries have been avoided by threat actors. The tables have turned with the NB65 hacking group modifying the leaked Conti ransomware to use in attacks on Russian entities. [...]
