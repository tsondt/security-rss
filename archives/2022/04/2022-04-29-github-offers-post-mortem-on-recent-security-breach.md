Title: GitHub offers post-mortem on recent security breach
Date: 2022-04-29T10:44:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-29-github-offers-post-mortem-on-recent-security-breach

[Source](https://portswigger.net/daily-swig/github-offers-post-mortem-on-recent-security-breach){:target="_blank" rel="noopener"}

> Tokens stollen and abused but problem has been contained [...]
