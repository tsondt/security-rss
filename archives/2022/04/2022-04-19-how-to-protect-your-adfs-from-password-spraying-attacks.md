Title: How to protect your ADFS from password spraying attacks
Date: 2022-04-19T10:00:00-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-04-19-how-to-protect-your-adfs-from-password-spraying-attacks

[Source](https://www.bleepingcomputer.com/news/security/how-to-protect-your-adfs-from-password-spraying-attacks/){:target="_blank" rel="noopener"}

> Microsoft recommends a multi-tiered approach for securing your ADFS environment from password attacks. Learn how Specops can fill in the gaps to add further protection against password sprays and other password attacks. [...]
