Title: Friday Squid Blogging: Ten-Foot Long Squid Washed onto  Japanese Shore — ALIVE
Date: 2022-04-29T21:08:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-04-29-friday-squid-blogging-ten-foot-long-squid-washed-onto-japanese-shore-alive

[Source](https://www.schneier.com/blog/archives/2022/04/friday-squid-blogging-ten-foot-long-squid-washed-onto-japanese-shore-alive.html){:target="_blank" rel="noopener"}

> This is rare : An about 3-meter-long giant squid was found stranded on a beach here on April 20, in what local authorities said was a rare occurrence. At around 10 a.m., a nearby resident spotted the squid at Ugu beach in Obama, Fukui Prefecture, on the Sea of Japan coast. According to the Obama Municipal Government, the squid was still alive when it was found. It is unusual for a giant squid to be washed ashore alive, officials said. The deep-sea creature will be transported to Echizen Matsushima Aquarium in the prefectural city of Sakai. Sadly, I do not [...]
