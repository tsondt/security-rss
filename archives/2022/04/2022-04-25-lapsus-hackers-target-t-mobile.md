Title: Lapsus$ Hackers Target T-Mobile
Date: 2022-04-25T13:32:43+00:00
Author: Threatpost
Category: Threatpost
Tags: Breach
Slug: 2022-04-25-lapsus-hackers-target-t-mobile

[Source](https://threatpost.com/lapsus-hackers-target-t-mobile/179384/){:target="_blank" rel="noopener"}

> No government and customer data was accessed. [...]
