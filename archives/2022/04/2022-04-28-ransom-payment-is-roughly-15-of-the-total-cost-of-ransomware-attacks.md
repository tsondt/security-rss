Title: Ransom payment is roughly 15% of the total cost of ransomware attacks
Date: 2022-04-28T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-28-ransom-payment-is-roughly-15-of-the-total-cost-of-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/ransom-payment-is-roughly-15-percent-of-the-total-cost-of-ransomware-attacks/){:target="_blank" rel="noopener"}

> Researchers analyzing the collateral consequences of a ransomware attack include costs that are roughly seven times higher than the ransom demanded by the threat actors. [...]
