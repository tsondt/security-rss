Title: QNAP warns users to disable AFP until it fixes critical bugs
Date: 2022-04-27T16:21:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-27-qnap-warns-users-to-disable-afp-until-it-fixes-critical-bugs

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-users-to-disable-afp-until-it-fixes-critical-bugs/){:target="_blank" rel="noopener"}

> Taiwanese corporation QNAP has asked customers this week to disable the AFP file service protocol on their network-attached storage (NAS) appliances until it fixes multiple critical Netatalk vulnerabilities. [...]
