Title: Long Article on NSO Group
Date: 2022-04-21T12:16:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-04-21-long-article-on-nso-group

[Source](https://www.schneier.com/blog/archives/2022/04/long-article-on-nso-group.html){:target="_blank" rel="noopener"}

> Ronan Farrow has a long article in The New Yorker on NSO Group, which includes the news that someone — probably Spain — used the software to spy on domestic Catalonian sepratists. [...]
