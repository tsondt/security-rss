Title: US State Department opens cybersecurity policy bureau
Date: 2022-04-05T21:23:23+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-05-us-state-department-opens-cybersecurity-policy-bureau

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/05/us_cybersecurity_cdp/){:target="_blank" rel="noopener"}

> Some in tech industry are ecstatic: 'A historic step forward' and timing 'couldn't be any better' The US State Department this week launched an agency responsible for developing online defense and privacy-protection policies and direction as the Biden administration seeks to integrate cybersecurity into America's foreign relations.... [...]
