Title: US warns of Lazarus hackers using malicious cryptocurrency apps
Date: 2022-04-18T17:47:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-04-18-us-warns-of-lazarus-hackers-using-malicious-cryptocurrency-apps

[Source](https://www.bleepingcomputer.com/news/security/us-warns-of-lazarus-hackers-using-malicious-cryptocurrency-apps/){:target="_blank" rel="noopener"}

> CISA, the FBI, and the US Treasury Department warned today that the North Korean Lazarus hacking group is targeting organizations in the cryptocurrency and blockchain industries with trojanized cryptocurrency applications. [...]
