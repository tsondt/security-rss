Title: Third npm protestware: 'event-source-polyfill' calls Russia out
Date: 2022-04-11T17:02:23-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-04-11-third-npm-protestware-event-source-polyfill-calls-russia-out

[Source](https://www.bleepingcomputer.com/news/security/third-npm-protestware-event-source-polyfill-calls-russia-out/){:target="_blank" rel="noopener"}

> Developers are increasingly voicing their opinions through their open source projects in active use by thousands of software applications and organizations. Most recently, the developer of the 'event-source-polyfill' npm package peacefully protested Russia's "unreasonable invasion" of Ukraine, to Russian consumers. [...]
