Title: LGPD workbook for AWS customers managing personally identifiable information in Brazil
Date: 2022-04-25T22:25:35+00:00
Author: Rodrigo Fiuza
Category: AWS Security
Tags: Advanced (300);Announcements;Expert (400);Security, Identity, & Compliance;Compliance;Conformidade;GDPR;LGPD;Security;Segurança
Slug: 2022-04-25-lgpd-workbook-for-aws-customers-managing-personally-identifiable-information-in-brazil

[Source](https://aws.amazon.com/blogs/security/lgpd-workbook-for-aws-customers-managing-personally-identifiable-information-in-brazil/){:target="_blank" rel="noopener"}

> Portuguese version AWS is pleased to announce the publication of the Brazil General Data Protection Law Workbook. The General Data Protection Law (LGPD) in Brazil was first published on 14 August 2018, and started its applicability on 18 August 2020. Companies that manage personally identifiable information (PII) in Brazil as defined by LGPD will have [...]
