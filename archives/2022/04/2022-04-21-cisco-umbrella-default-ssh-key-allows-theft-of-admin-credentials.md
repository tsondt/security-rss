Title: Cisco Umbrella default SSH key allows theft of admin credentials
Date: 2022-04-21T04:16:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-21-cisco-umbrella-default-ssh-key-allows-theft-of-admin-credentials

[Source](https://www.bleepingcomputer.com/news/security/cisco-umbrella-default-ssh-key-allows-theft-of-admin-credentials/){:target="_blank" rel="noopener"}

> Cisco has released security updates to address a high severity vulnerability in the Cisco Umbrella Virtual Appliance (VA), allowing unauthenticated attackers to steal admin credentials remotely. [...]
