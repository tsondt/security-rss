Title: Menswear Brand Zegna Reveals Ransomware Attack
Date: 2022-04-12T17:22:38+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Malware
Slug: 2022-04-12-menswear-brand-zegna-reveals-ransomware-attack

[Source](https://threatpost.com/menswear-zegna-ransomware/179266/){:target="_blank" rel="noopener"}

> Accounting materials from the Italy-based luxury fashion house were leaked online by RansomExx because the company refused to pay. [...]
