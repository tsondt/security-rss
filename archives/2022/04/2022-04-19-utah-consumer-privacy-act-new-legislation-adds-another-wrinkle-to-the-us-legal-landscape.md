Title: Utah Consumer Privacy Act: New legislation adds another wrinkle to the US legal landscape
Date: 2022-04-19T13:20:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-19-utah-consumer-privacy-act-new-legislation-adds-another-wrinkle-to-the-us-legal-landscape

[Source](https://portswigger.net/daily-swig/utah-consumer-privacy-act-new-legislation-adds-another-wrinkle-to-the-us-legal-landscape){:target="_blank" rel="noopener"}

> Soon to be enacted law provides further governance for citizens’ data [...]
