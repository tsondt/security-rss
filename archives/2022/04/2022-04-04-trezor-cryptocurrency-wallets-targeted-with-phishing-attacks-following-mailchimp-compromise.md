Title: Trezor cryptocurrency wallets targeted with phishing attacks following Mailchimp compromise
Date: 2022-04-04T14:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-04-trezor-cryptocurrency-wallets-targeted-with-phishing-attacks-following-mailchimp-compromise

[Source](https://portswigger.net/daily-swig/trezor-cryptocurrency-wallets-targeted-with-phishing-attacks-following-mailchimp-compromise){:target="_blank" rel="noopener"}

> Company claims false data breach emails were spread via newsletters [...]
