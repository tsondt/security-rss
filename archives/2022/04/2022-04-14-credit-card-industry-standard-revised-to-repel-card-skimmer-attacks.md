Title: Credit card industry standard revised to repel card-skimmer attacks
Date: 2022-04-14T14:10:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-14-credit-card-industry-standard-revised-to-repel-card-skimmer-attacks

[Source](https://portswigger.net/daily-swig/credit-card-industry-standard-revised-to-repel-card-skimmer-attacks){:target="_blank" rel="noopener"}

> PCI DSS v4.0 encourages better defenses against Magecart-style assaults [...]
