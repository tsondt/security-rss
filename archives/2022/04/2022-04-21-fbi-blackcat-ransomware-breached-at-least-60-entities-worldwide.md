Title: FBI: BlackCat ransomware breached at least 60 entities worldwide
Date: 2022-04-21T03:21:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-21-fbi-blackcat-ransomware-breached-at-least-60-entities-worldwide

[Source](https://www.bleepingcomputer.com/news/security/fbi-blackcat-ransomware-breached-at-least-60-entities-worldwide/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) says the Black Cat ransomware gang, also known as ALPHV, has breached the networks of at least 60 organizations worldwide, between November 2021 and March 2022. [...]
