Title: Microsoft Issues Report of Russian Cyberattacks against Ukraine
Date: 2022-04-28T14:15:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cyberespionage;cyberwar;espionage;reports;Russia;Ukraine
Slug: 2022-04-28-microsoft-issues-report-of-russian-cyberattacks-against-ukraine

[Source](https://www.schneier.com/blog/archives/2022/04/microsoft-issues-report-of-russian-cyberattacks-against-ukraine.html){:target="_blank" rel="noopener"}

> Microsoft has a comprehensive report on the dozens of cyberattacks — and even more espionage operations — Russia has conducted against Ukraine as part of this war: At least six Russian Advanced Persistent Threat (APT) actors and other unattributed threats, have conducted destructive attacks, espionage operations, or both, while Russian military forces attack the country by land, air, and sea. It is unclear whether computer network operators and physical forces are just independently pursuing a common set of priorities or actively coordinating. However, collectively, the cyber and kinetic actions work to disrupt or degrade Ukrainian government and military functions and [...]
