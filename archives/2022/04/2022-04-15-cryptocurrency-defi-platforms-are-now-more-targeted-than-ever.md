Title: Cryptocurrency DeFi platforms are now more targeted than ever
Date: 2022-04-15T12:33:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-04-15-cryptocurrency-defi-platforms-are-now-more-targeted-than-ever

[Source](https://www.bleepingcomputer.com/news/security/cryptocurrency-defi-platforms-are-now-more-targeted-than-ever/){:target="_blank" rel="noopener"}

> Hackers are increasingly targeting DeFi (Decentralized Finance) cryptocurrency platforms, with Q1 2022 data showing that more platforms are being targeted than ever before. [...]
