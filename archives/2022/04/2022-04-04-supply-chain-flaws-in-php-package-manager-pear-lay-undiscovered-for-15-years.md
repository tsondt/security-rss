Title: Supply chain flaws in PHP package manager PEAR lay undiscovered for 15 years
Date: 2022-04-04T15:30:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-04-supply-chain-flaws-in-php-package-manager-pear-lay-undiscovered-for-15-years

[Source](https://portswigger.net/daily-swig/supply-chain-flaws-in-php-package-manager-pear-lay-undiscovered-for-15-years){:target="_blank" rel="noopener"}

> PEAR was ripe for exploitation via cryptographic flaw and bug in outdated dependency [...]
