Title: Australia warns of money recovery phishing luring past victims
Date: 2022-04-05T19:35:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-05-australia-warns-of-money-recovery-phishing-luring-past-victims

[Source](https://www.bleepingcomputer.com/news/security/australia-warns-of-money-recovery-phishing-luring-past-victims/){:target="_blank" rel="noopener"}

> The Australian Competition & Consumer Commission has published an announcement to raise awareness about a spike in money recovery scams. [...]
