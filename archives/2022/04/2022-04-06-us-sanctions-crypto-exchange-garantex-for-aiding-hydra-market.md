Title: U.S. sanctions crypto-exchange Garantex for aiding Hydra Market
Date: 2022-04-06T08:49:54-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Legal
Slug: 2022-04-06-us-sanctions-crypto-exchange-garantex-for-aiding-hydra-market

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-crypto-exchange-garantex-for-aiding-hydra-market/){:target="_blank" rel="noopener"}

> The U.S. Department of the Treasury's Office has announced sanctions against the cryptocurrency exchange Garantex, which has been linked to illegal transactions for Hydra Market. [...]
