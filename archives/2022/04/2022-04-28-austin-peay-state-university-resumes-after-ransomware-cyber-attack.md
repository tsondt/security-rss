Title: Austin Peay State University resumes after ransomware cyber attack
Date: 2022-04-28T05:04:58-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-04-28-austin-peay-state-university-resumes-after-ransomware-cyber-attack

[Source](https://www.bleepingcomputer.com/news/security/austin-peay-state-university-resumes-after-ransomware-cyber-attack/){:target="_blank" rel="noopener"}

> Austin Peay State University (APSU) confirmed yesterday that it had been a victim of a ransomware attack. The university, located in Clarksville, Tennessee advised students, staff, and faculty to disconnect their computers and devices from the university network immediately as a precaution. [...]
