Title: How to protect HMACs inside AWS KMS
Date: 2022-04-19T22:57:30+00:00
Author: Jeremy Stieglitz
Category: AWS Security
Tags: Announcements;Intermediate (200);Learning Levels;Security;Security, Identity, & Compliance;Technical How-to;AWS KMS;digital signature;HMAC;JWT;Security Blog
Slug: 2022-04-19-how-to-protect-hmacs-inside-aws-kms

[Source](https://aws.amazon.com/blogs/security/how-to-protect-hmacs-inside-aws-kms/){:target="_blank" rel="noopener"}

> Today AWS Key Management Service (AWS KMS) is introducing new APIs to generate and verify hash-based message authentication codes (HMACs) using the Federal Information Processing Standard (FIPS) 140-2 validated hardware security modules (HSMs) in AWS KMS. HMACs are a powerful cryptographic building block that incorporate secret key material in a hash function to create a [...]
