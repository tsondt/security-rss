Title: Cooler heads needed in heated E2EE debate, says think tank
Date: 2022-04-05T14:30:29+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-05-cooler-heads-needed-in-heated-e2ee-debate-says-think-tank

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/05/e2ee_rusi/){:target="_blank" rel="noopener"}

> RUSI argues for collaboration, while others note all 'scans' compromise secure encryption End-to-end encryption (E2EE) has become a global flashpoint in the ongoing debate between the security of private communications versus the need of law enforcement agencies to protect the public from criminals.... [...]
