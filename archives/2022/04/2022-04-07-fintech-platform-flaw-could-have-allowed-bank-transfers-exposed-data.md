Title: Fintech platform flaw could have allowed bank transfers, exposed data
Date: 2022-04-07T15:30:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-07-fintech-platform-flaw-could-have-allowed-bank-transfers-exposed-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/07/salt-fintech-platform-vulnerability-ssrf/){:target="_blank" rel="noopener"}

> Fintech provider flaw could have hit dozens of U.S. banks, says Salt Security Salt Security spotted a vulnerability in a large fintech company's digital platform that would have granted attackers admin access to banking systems in addition to allowing them to transfer funds to their own accounts.... [...]
