Title: Flaw could have granted criminals control over Ever Surf crypto wallets
Date: 2022-04-25T15:30:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-25-flaw-could-have-granted-criminals-control-over-ever-surf-crypto-wallets

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/25/ever-surf-wallet-vulnerability/){:target="_blank" rel="noopener"}

> Check Point uncovers web vulnerability that could have led to cryptocurrency theft A flaw detected in the browser version of the Ever Surf cryptocurrency wallet could have given hackers who exploited it full control over a targeted user's wallet, say threat hunters at Check Point Research.... [...]
