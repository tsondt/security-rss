Title: Microsoft takes down APT28 domains used in attacks against Ukraine
Date: 2022-04-07T18:52:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-04-07-microsoft-takes-down-apt28-domains-used-in-attacks-against-ukraine

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-takes-down-apt28-domains-used-in-attacks-against-ukraine/){:target="_blank" rel="noopener"}

> Microsoft has successfully disrupted attacks against Ukrainian targets coordinated by the Russian APT28 hacking group after taking down seven domains used as attack infrastructure. [...]
