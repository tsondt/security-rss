Title: Microsoft adds on-premises Exchange, SharePoint to bug bounty program
Date: 2022-04-05T11:53:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-04-05-microsoft-adds-on-premises-exchange-sharepoint-to-bug-bounty-program

[Source](https://www.bleepingcomputer.com/news/security/microsoft-adds-on-premises-exchange-sharepoint-to-bug-bounty-program/){:target="_blank" rel="noopener"}

> Microsoft has announced that Exchange, SharePoint, and Skype for Business on-premises are now part of the Applications and On-Premises Servers Bounty Program starting today. [...]
