Title: Java encryption implementation error made it trivial to forge credentials
Date: 2022-04-20T19:00:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-20-java-encryption-implementation-error-made-it-trivial-to-forge-credentials

[Source](https://portswigger.net/daily-swig/java-encryption-implementation-error-made-it-trivial-to-forge-credentials){:target="_blank" rel="noopener"}

> Bundled math in code issue created security trap [...]
