Title: Enemybot botnet uses Gafgyt source code with a sprinkling of Mirai
Date: 2022-04-13T14:00:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-13-enemybot-botnet-uses-gafgyt-source-code-with-a-sprinkling-of-mirai

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/13/enemy-botnet-uses-gafgyt-mirai/){:target="_blank" rel="noopener"}

> Keksec malware used for DDoS attacks, may spread to cryptomining, Fortinet says A prolific threat group known for deploying distributed denial-of-service (DDoS) and cryptomining attacks is running a new botnet that is built using the Linux-based Gafgyt source code along with some code from the Mirai botnet malware.... [...]
