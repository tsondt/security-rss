Title: Best practices: Securing your Amazon Location Service resources
Date: 2022-04-01T17:12:28+00:00
Author: David Bailey
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Amazon Cognito;Amazon Location Service;AWS IAM;AWS KMS;Compliance;location;PII;Security
Slug: 2022-04-01-best-practices-securing-your-amazon-location-service-resources

[Source](https://aws.amazon.com/blogs/security/best-practices-securing-your-amazon-location-service-resources/){:target="_blank" rel="noopener"}

> Location data is subjected to heavy scrutiny by security experts. Knowing the current position of a person, vehicle, or asset can provide industries with many benefits, whether to understand where a current delivery is, how many people are inside a venue, or to optimize routing for a fleet of vehicles. This blog post explains how [...]
