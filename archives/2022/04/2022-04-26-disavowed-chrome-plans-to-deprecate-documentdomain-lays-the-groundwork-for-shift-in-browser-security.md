Title: Disavowed: Chrome plans to deprecate ‘document.domain’ lays the groundwork for shift in browser security
Date: 2022-04-26T17:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-26-disavowed-chrome-plans-to-deprecate-documentdomain-lays-the-groundwork-for-shift-in-browser-security

[Source](https://portswigger.net/daily-swig/disavowed-chrome-plans-to-deprecate-document-domain-lays-the-groundwork-for-shift-in-browser-security){:target="_blank" rel="noopener"}

> Making document.domain immutable [...]
