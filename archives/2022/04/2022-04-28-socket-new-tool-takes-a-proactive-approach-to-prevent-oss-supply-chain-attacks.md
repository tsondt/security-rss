Title: Socket: New tool takes a proactive approach to prevent OSS supply chain attacks
Date: 2022-04-28T14:15:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-28-socket-new-tool-takes-a-proactive-approach-to-prevent-oss-supply-chain-attacks

[Source](https://portswigger.net/daily-swig/socket-new-tool-takes-a-proactive-approach-to-prevent-oss-supply-chain-attacks){:target="_blank" rel="noopener"}

> Signal detector aims to help developers to stay ahead of threats [...]
