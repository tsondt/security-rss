Title: Singaporean cybersecurity agency launches certification scheme for businesses
Date: 2022-04-05T11:17:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-05-singaporean-cybersecurity-agency-launches-certification-scheme-for-businesses

[Source](https://portswigger.net/daily-swig/singaporean-cybersecurity-agency-launches-certification-scheme-for-businesses){:target="_blank" rel="noopener"}

> Program comprises separate security marks aimed at SMEs and enterprises [...]
