Title: Testing, testing, testing: Why Red Teaming is a must for every CISO
Date: 2022-04-05T07:15:14+00:00
Author: The Masked CISO
Category: The Register
Tags: 
Slug: 2022-04-05-testing-testing-testing-why-red-teaming-is-a-must-for-every-ciso

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/05/why_red_teaming_is_a_must/){:target="_blank" rel="noopener"}

> Our Vectra Masked CISO series tackles some of the biggest issues in security and how to overcome them Advertorial Security must be tried, tested, and tested again. It’s become easier than ever for cybercriminals to find stealthy new ways to gain access to and navigate laterally across systems, exploit gaps in hybrid workflows, or hijack cloud accounts to gain access to a target. It is vital that every CISO can offer a clear picture of how their security is really holding up against the latest tactics, techniques, and procedures.... [...]
