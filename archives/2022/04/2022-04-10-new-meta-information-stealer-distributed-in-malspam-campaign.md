Title: New Meta information stealer distributed in malspam campaign
Date: 2022-04-10T11:12:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-10-new-meta-information-stealer-distributed-in-malspam-campaign

[Source](https://www.bleepingcomputer.com/news/security/new-meta-information-stealer-distributed-in-malspam-campaign/){:target="_blank" rel="noopener"}

> Independent analyst Brand Duncan has spotted a malspam campaign delivering META, a new info-stealer malware that appears to be rising in popularity among cybercriminals. [...]
