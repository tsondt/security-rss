Title: Five Eyes nations fear wave of Russian attacks against critical infrastructure
Date: 2022-04-21T02:02:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-21-five-eyes-nations-fear-wave-of-russian-attacks-against-critical-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/21/five_eyes_russia/){:target="_blank" rel="noopener"}

> If this is surprising to operators, we are doomed The Five Eyes nations' cybersecurity agencies this week urged critical infrastructure to be ready for attacks by crews backed by or sympathetic to the Kremlin amid strong Western opposition to Russia's invasion of Ukraine.... [...]
