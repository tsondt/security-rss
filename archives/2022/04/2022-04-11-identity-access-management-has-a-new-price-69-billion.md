Title: Identity access management has a new price: $6.9 billion
Date: 2022-04-11T15:30:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-04-11-identity-access-management-has-a-new-price-69-billion

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/11/identity_access_management/){:target="_blank" rel="noopener"}

> That's what Thoma Bravo is paying for SailPoint in a mid-pandemic market A $6.9 billion acquisition is putting a hard number on the value of Identity and Access Management (IAM).... [...]
