Title: Beanstalk DeFi platform loses $182 million in flash-loan attack
Date: 2022-04-18T10:05:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-04-18-beanstalk-defi-platform-loses-182-million-in-flash-loan-attack

[Source](https://www.bleepingcomputer.com/news/security/beanstalk-defi-platform-loses-182-million-in-flash-loan-attack/){:target="_blank" rel="noopener"}

> The decentralized, credit-based finance system Beanstalk disclosed on Sunday that it suffered a security breach that resulted in financial losses of $182 million, the attacker stealing $80 million in crypto assets. [...]
