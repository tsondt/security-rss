Title: European officials reportedly targeted by NSO spyware
Date: 2022-04-11T22:22:39+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-11-european-officials-reportedly-targeted-by-nso-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/11/nso_spyware_eu/){:target="_blank" rel="noopener"}

> Pegasus software maker faces mounting lawsuits, investigations in the US and EU Someone at least tried to use NSO Group's surveillance software to spy on European Commission officials last year, according to a Reuters report.... [...]
