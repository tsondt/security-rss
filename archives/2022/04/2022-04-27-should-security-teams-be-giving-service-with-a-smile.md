Title: Should security teams be giving service with a smile?
Date: 2022-04-27T07:15:14+00:00
Author: The Masked CISO
Category: The Register
Tags: 
Slug: 2022-04-27-should-security-teams-be-giving-service-with-a-smile

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/27/should_security_teams_be_giving/){:target="_blank" rel="noopener"}

> Our Vectra Masked CISO series tackles some of the biggest issues in security and how to overcome them Advertorial As security professionals, we aren’t known for our levity. True, we’re often fire-fighting serious incidents with potentially profound consequences for the organisation, and our career prospects. But our relationships with others are usually characterised by policing and enforcement rather than engagement and support.... [...]
