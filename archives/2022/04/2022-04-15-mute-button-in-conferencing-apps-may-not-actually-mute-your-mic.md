Title: 'Mute' button in conferencing apps may not actually mute your mic
Date: 2022-04-15T11:05:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software;Technology
Slug: 2022-04-15-mute-button-in-conferencing-apps-may-not-actually-mute-your-mic

[Source](https://www.bleepingcomputer.com/news/security/mute-button-in-conferencing-apps-may-not-actually-mute-your-mic/){:target="_blank" rel="noopener"}

> A new study shows that pressing the mute button on popular video conferencing apps (VCA) may not actually work like you think it should, with apps still listening in on your microphone. [...]
