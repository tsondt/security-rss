Title: Cybercriminals do their homework for latest banking scam
Date: 2022-04-15T15:30:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-04-15-cybercriminals-do-their-homework-for-latest-banking-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/15/the_latest_scam_pay_yourself/){:target="_blank" rel="noopener"}

> What could be safer than sending money to yourself through your own bank? A new social engineering scam is making the rounds, and this one is particularly insidious: It tricks users into sending money to what they think is their own account to reverse a fraudulent charge.... [...]
