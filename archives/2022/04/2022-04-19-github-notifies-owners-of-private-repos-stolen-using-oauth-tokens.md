Title: GitHub notifies owners of private repos stolen using OAuth tokens
Date: 2022-04-19T12:55:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-19-github-notifies-owners-of-private-repos-stolen-using-oauth-tokens

[Source](https://www.bleepingcomputer.com/news/security/github-notifies-owners-of-private-repos-stolen-using-oauth-tokens/){:target="_blank" rel="noopener"}

> GitHub says it notified all organizations believed to have had data stolen from their private repositories by attackers abusing compromised OAuth user tokens issued to Heroku and Travis-CI. [...]
