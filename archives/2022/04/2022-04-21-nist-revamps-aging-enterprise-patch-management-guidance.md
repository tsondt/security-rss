Title: NIST revamps aging enterprise patch management guidance
Date: 2022-04-21T11:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-21-nist-revamps-aging-enterprise-patch-management-guidance

[Source](https://portswigger.net/daily-swig/nist-revamps-aging-enterprise-patch-management-guidance){:target="_blank" rel="noopener"}

> US agency highlights ‘divide’ between security teams and their colleagues about the value of patching [...]
