Title: Hackers use Conti's leaked ransomware to attack Russian companies
Date: 2022-04-09T14:30:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-09-hackers-use-contis-leaked-ransomware-to-attack-russian-companies

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-contis-leaked-ransomware-to-attack-russian-companies/){:target="_blank" rel="noopener"}

> A hacking group used the Conti's leaked ransomware source code to create their own ransomware to use in cyberattacks against Russian organizations. [...]
