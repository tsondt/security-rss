Title: SpringShell attacks target about one in six vulnerable orgs
Date: 2022-04-05T13:36:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-05-springshell-attacks-target-about-one-in-six-vulnerable-orgs

[Source](https://www.bleepingcomputer.com/news/security/springshell-attacks-target-about-one-in-six-vulnerable-orgs/){:target="_blank" rel="noopener"}

> Roughly one out of six organizations worldwide that are impacted by the Spring4Shell zero-day vulnerability have already been targeted by threat actors, according to statistics from one cybersecurity company. [...]
