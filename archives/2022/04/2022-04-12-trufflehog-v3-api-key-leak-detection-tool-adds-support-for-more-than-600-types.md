Title: TruffleHog v3: API key leak detection tool adds support for more than 600 types
Date: 2022-04-12T09:59:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-12-trufflehog-v3-api-key-leak-detection-tool-adds-support-for-more-than-600-types

[Source](https://portswigger.net/daily-swig/trufflehog-v3-api-key-leak-detection-tool-adds-support-for-more-than-600-types){:target="_blank" rel="noopener"}

> Third version of the open source software comes with significant upgrades [...]
