Title: GitHub: How stolen OAuth tokens helped breach dozens of orgs
Date: 2022-04-27T17:04:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-27-github-how-stolen-oauth-tokens-helped-breach-dozens-of-orgs

[Source](https://www.bleepingcomputer.com/news/security/github-how-stolen-oauth-tokens-helped-breach-dozens-of-orgs/){:target="_blank" rel="noopener"}

> GitHub has shared a timeline of this month's security breach when a threat actor gained access to and stole private repositories belonging to dozens of organizations. [...]
