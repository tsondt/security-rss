Title: GitLab issues critical update after hard-coding passwords into accounts
Date: 2022-04-01T19:21:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-04-01-gitlab-issues-critical-update-after-hard-coding-passwords-into-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/01/gitlab_security_advisory/){:target="_blank" rel="noopener"}

> Fixed passphrases for OmniAuth users not such a great idea GitLab on Thursday issued security updates for three versions of GitLab Community Edition (CE) and Enterprise Edition (EE) software that address, among other flaws, a critical hard-coded password bug.... [...]
