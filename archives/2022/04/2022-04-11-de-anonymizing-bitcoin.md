Title: De-anonymizing Bitcoin
Date: 2022-04-11T11:04:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bitcoin;child pornography;cryptocurrency;de-anonymization;tracing
Slug: 2022-04-11-de-anonymizing-bitcoin

[Source](https://www.schneier.com/blog/archives/2022/04/de-anonymizing-bitcoin.html){:target="_blank" rel="noopener"}

> Andy Greenberg wrote a long article — an excerpt from his new book — on how law enforcement de-anonymized bitcoin transactions to take down a global child porn ring. Within a few years of Bitcoin’s arrival, academic security researchers — and then companies like Chainalysis — began to tear gaping holes in the masks separating Bitcoin users’ addresses and their real-world identities. They could follow bitcoins on the blockchain as they moved from address to address until they reached one that could be tied to a known identity. In some cases, an investigator could learn someone’s Bitcoin addresses by transacting [...]
