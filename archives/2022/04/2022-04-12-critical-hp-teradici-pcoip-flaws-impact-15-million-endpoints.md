Title: Critical HP Teradici PCoIP flaws impact 15 million endpoints
Date: 2022-04-12T12:40:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-12-critical-hp-teradici-pcoip-flaws-impact-15-million-endpoints

[Source](https://www.bleepingcomputer.com/news/security/critical-hp-teradici-pcoip-flaws-impact-15-million-endpoints/){:target="_blank" rel="noopener"}

> HP is warning of new critical security vulnerabilities in the Teradici PCoIP client and agent for Windows, Linux, and macOS that impact 15 million endpoints. [...]
