Title: Funky Pigeon pauses all orders after 'security incident'
Date: 2022-04-19T12:45:05+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-04-19-funky-pigeon-pauses-all-orders-after-security-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/19/funky_pigeon_security_incident/){:target="_blank" rel="noopener"}

> Parent WH Smith says no customer payment data exposed, according to current investigations British retailer WH Smith has confirmed that Funky Pigeon, its online greetings card and gift subsidiary, has halted all further orders after a "security incident."... [...]
