Title: Third member of FIN7 cybercrime gang jailed over card skimming scheme
Date: 2022-04-08T14:20:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-08-third-member-of-fin7-cybercrime-gang-jailed-over-card-skimming-scheme

[Source](https://portswigger.net/daily-swig/third-member-of-fin7-cybercrime-gang-jailed-over-card-skimming-scheme){:target="_blank" rel="noopener"}

> US authorities sentence pen tester to five years in prison [...]
