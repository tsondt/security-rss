Title: Emma Sleep Company admits checkout cyber attack
Date: 2022-04-04T10:29:11+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-04-04-emma-sleep-company-admits-checkout-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/04/emma_the_sleep_company_admits/){:target="_blank" rel="noopener"}

> Customers wake to a nightmare as payment data pilfered from UK website Emma Sleep Company has confirmed to The Reg that it suffered a Magecart attack which enabled ne'er-do-wells to skim customers' credit or debit card data from its website.... [...]
