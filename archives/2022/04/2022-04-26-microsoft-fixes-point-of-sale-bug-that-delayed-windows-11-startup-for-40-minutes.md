Title: Microsoft fixes Point of Sale bug that delayed Windows 11 startup for 40 minutes
Date: 2022-04-26T11:32:26+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-04-26-microsoft-fixes-point-of-sale-bug-that-delayed-windows-11-startup-for-40-minutes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/26/windows_11_patch/){:target="_blank" rel="noopener"}

> You thought hunting for discount vouchers took a while? That's nothing compared to Windows booting on a till A fresh Windows 11 patch slipped out overnight as an optional update, but contains an impressively long list of fixes for Microsoft's flagship operating system.... [...]
