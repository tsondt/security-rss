Title: Redis, MongoDB, and Elastic: 2022’s top exposed databases
Date: 2022-04-27T04:29:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;Software
Slug: 2022-04-27-redis-mongodb-and-elastic-2022s-top-exposed-databases

[Source](https://www.bleepingcomputer.com/news/security/redis-mongodb-and-elastic-2022-s-top-exposed-databases/){:target="_blank" rel="noopener"}

> Security researchers have noticed an increase in the number of databases publicly exposed to the Internet, with 308,000 identified in 2021. The growth continued quarter over quarter, peaking in the first months of this year. [...]
