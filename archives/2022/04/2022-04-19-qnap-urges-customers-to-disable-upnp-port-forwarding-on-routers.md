Title: QNAP urges customers to disable UPnP port forwarding on routers
Date: 2022-04-19T15:38:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-19-qnap-urges-customers-to-disable-upnp-port-forwarding-on-routers

[Source](https://www.bleepingcomputer.com/news/security/qnap-urges-customers-to-disable-upnp-port-forwarding-on-routers/){:target="_blank" rel="noopener"}

> Taiwanese hardware vendor QNAP urged customers on Monday to disable Universal Plug and Play (UPnP) port forwarding on their routers to prevent exposing their network-attached storage (NAS) devices to attacks from the Internet. [...]
