Title: SMS Phishing Attacks are on the Rise
Date: 2022-04-25T10:18:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-04-25-sms-phishing-attacks-are-on-the-rise

[Source](https://www.schneier.com/blog/archives/2022/04/sms-phishing-attacks-are-on-the-rise.html){:target="_blank" rel="noopener"}

> SMS phishing attacks — annoyingly called “smishing” — are becoming more common. I know that I have been receiving a lot of phishing SMS messages over the past few months. I am not getting the “Fedex package delivered” messages the article talks about. Mine are usually of the form: “thank you for paying your bill, here’s a free gift for you.” [...]
