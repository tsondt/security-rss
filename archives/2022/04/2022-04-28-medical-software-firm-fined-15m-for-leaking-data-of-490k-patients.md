Title: Medical software firm fined €1.5M for leaking data of 490k patients
Date: 2022-04-28T12:17:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-04-28-medical-software-firm-fined-15m-for-leaking-data-of-490k-patients

[Source](https://www.bleepingcomputer.com/news/security/medical-software-firm-fined-15m-for-leaking-data-of-490k-patients/){:target="_blank" rel="noopener"}

> The French data protection authority (CNIL) fined medical software vendor Dedalus Biology with EUR 1.5 million for violating three articles of the GDPR (General Data Protection Regulation). [...]
