Title: PHP bug allows attackers to bypass domain filters, stage DoS attacks against servers
Date: 2022-04-01T11:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-01-php-bug-allows-attackers-to-bypass-domain-filters-stage-dos-attacks-against-servers

[Source](https://portswigger.net/daily-swig/php-bug-allows-attackers-to-bypass-domain-filters-stage-dos-attacks-against-servers){:target="_blank" rel="noopener"}

> Filter bypass flaw is triggered only on very large user input, which puts restrictions on its exploitability [...]
