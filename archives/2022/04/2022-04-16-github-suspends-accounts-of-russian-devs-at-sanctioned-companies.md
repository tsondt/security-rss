Title: GitHub suspends accounts of Russian devs at sanctioned companies
Date: 2022-04-16T10:04:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-16-github-suspends-accounts-of-russian-devs-at-sanctioned-companies

[Source](https://www.bleepingcomputer.com/news/security/github-suspends-accounts-of-russian-devs-at-sanctioned-companies/){:target="_blank" rel="noopener"}

> Russian software developers are reporting that their GitHub accounts are being suspended without warning if they work for or previously worked for companies under US sanctions. [...]
