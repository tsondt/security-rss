Title: Fake Trezor data breach emails used to steal cryptocurrency wallets
Date: 2022-04-03T12:03:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-04-03-fake-trezor-data-breach-emails-used-to-steal-cryptocurrency-wallets

[Source](https://www.bleepingcomputer.com/news/security/fake-trezor-data-breach-emails-used-to-steal-cryptocurrency-wallets/){:target="_blank" rel="noopener"}

> A compromised Trezor hardware wallet mailing list was used to send fake data breach notifications to steal cryptocurrency wallets and the assets stored within them. [...]
