Title: Fighting Fake EDRs With ‘Credit Ratings’ for Police
Date: 2022-04-27T14:27:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Web Fraud 2.0;apple;AT&T;Coinbase;Discord;emergency data request;fbi;GitHub;google;Kodex;LinkedIn;Matt Donahue;Meta;microsoft;Snapchat;T-Mobile;Tiktok;Twilio;twitter;Verizon
Slug: 2022-04-27-fighting-fake-edrs-with-credit-ratings-for-police

[Source](https://krebsonsecurity.com/2022/04/fighting-fake-edrs-with-credit-ratings-for-police/){:target="_blank" rel="noopener"}

> When KrebsOnSecurity recently explored how cybercriminals were using hacked email accounts at police departments worldwide to obtain warrantless Emergency Data Requests (EDRs) from social media firms and technology providers, many security experts called it a fundamentally unfixable problem. But don’t tell that to Matt Donahue, a former FBI agent who recently quit the agency to launch a startup that aims to help tech companies do a better job screening out phony law enforcement data requests — in part by assigning trustworthiness or “credit ratings” to law enforcement authorities worldwide. A sample Kodex dashboard. Image: Kodex.us. Donahue is co-founder of Kodex, [...]
