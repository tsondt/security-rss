Title: Leaked Chats Show LAPSUS$ Stole T-Mobile Source Code
Date: 2022-04-22T13:09:39+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;SIM Swapping;Amtrak;apple;Bitbucket;Dan Goodin;Doxbin;Electronic Arts;emergency data request;Everlynn;Flashpoint;Genesis;Globant;Iqor;KT;LAPSUS$;Lapsus$ Jobs;Michelin;microsoft;Mobile Device Management;Mox;NVIDIA;Recursion Team;Russian Market;Samsung;SASCAR;SIM swapping;Slack;source code theft;SWATting;T-Mobile;T-Mobile Atlas;WhiteDoxbin
Slug: 2022-04-22-leaked-chats-show-lapsus-stole-t-mobile-source-code

[Source](https://krebsonsecurity.com/2022/04/leaked-chats-show-lapsus-stole-t-mobile-source-code/){:target="_blank" rel="noopener"}

> KrebsOnSecurity recently reviewed a copy of the private chat messages between members of the LAPSUS$ cybercrime group in the week leading up to the arrest of its most active members last month. The logs show LAPSUS$ breached T-Mobile multiple times in March, stealing source code for a range of company projects. T-Mobile says no customer or government information was stolen in the intrusion. LAPSUS$ is known for stealing data and then demanding a ransom not to publish or sell it. But the leaked chats indicate this mercenary activity was of little interest to the tyrannical teenage leader of LAPSUS$, whose [...]
