Title: Apache says Struts 2 security bug wasn't fully fixed in 2020
Date: 2022-04-13T21:30:01+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-13-apache-says-struts-2-security-bug-wasnt-fully-fixed-in-2020

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/13/apache_struts_bug_new_patch/){:target="_blank" rel="noopener"}

> But this time the patch should do the trick Apache has taken another shot at fixing a critical remote code execution vulnerability in its Struts 2 framework for Java applications – because the first patch, issued in 2020, didn't fully do the trick.... [...]
