Title: Russian hacktivists launch DDoS attacks on Romanian govt sites
Date: 2022-04-29T10:47:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-29-russian-hacktivists-launch-ddos-attacks-on-romanian-govt-sites

[Source](https://www.bleepingcomputer.com/news/security/russian-hacktivists-launch-ddos-attacks-on-romanian-govt-sites/){:target="_blank" rel="noopener"}

> The Romanian national cyber security and incident response team, DNSC, has issued a statement about a series of distributed denial-of-service (DDoS) attacks targeting several public websites managed by the state entities. [...]
