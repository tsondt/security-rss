Title: Karakurt revealed as data extortion arm of Conti cybercrime syndicate
Date: 2022-04-15T09:28:07-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-04-15-karakurt-revealed-as-data-extortion-arm-of-conti-cybercrime-syndicate

[Source](https://www.bleepingcomputer.com/news/security/karakurt-revealed-as-data-extortion-arm-of-conti-cybercrime-syndicate/){:target="_blank" rel="noopener"}

> After breaching servers managed by the cybercriminals, security researchers found a connection between Conti ransomware and the recently emerged Karakurt data extortion group, showing that the two gangs are part of the same operation. [...]
