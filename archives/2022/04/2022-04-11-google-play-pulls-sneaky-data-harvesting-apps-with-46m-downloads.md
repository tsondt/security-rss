Title: Google Play pulls sneaky data-harvesting apps with 46m+ downloads
Date: 2022-04-11T11:01:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-11-google-play-pulls-sneaky-data-harvesting-apps-with-46m-downloads

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/11/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Fox News learns to use database passwords, Autodesk patches high-severity bugs, and CISA says retire old D-Link routers In brief Google pulled a slew of Android apps with more than 46 million downloads from its Google Play Store after security researchers notified the cloud giant that the code contained some sneaky data-harvesting code.... [...]
