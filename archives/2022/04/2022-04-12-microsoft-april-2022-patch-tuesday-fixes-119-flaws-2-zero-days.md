Title: Microsoft April 2022 Patch Tuesday fixes 119 flaws, 2 zero-days
Date: 2022-04-12T13:40:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-04-12-microsoft-april-2022-patch-tuesday-fixes-119-flaws-2-zero-days

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-april-2022-patch-tuesday-fixes-119-flaws-2-zero-days/){:target="_blank" rel="noopener"}

> Today is Microsoft's April 2022 Patch Tuesday, and with it comes fixes for two zero-day vulnerabilities and a total of 119 flaws. [...]
