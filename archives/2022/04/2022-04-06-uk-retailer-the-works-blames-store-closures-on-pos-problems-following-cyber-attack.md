Title: UK retailer The Works blames store closures on POS problems following cyber-attack
Date: 2022-04-06T12:18:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-06-uk-retailer-the-works-blames-store-closures-on-pos-problems-following-cyber-attack

[Source](https://portswigger.net/daily-swig/uk-retailer-the-works-blames-store-closures-on-pos-problems-following-cyber-attack){:target="_blank" rel="noopener"}

> Discount chain is working to restore stock deliveries [...]
