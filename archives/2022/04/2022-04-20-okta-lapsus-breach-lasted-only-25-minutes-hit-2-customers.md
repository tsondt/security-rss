Title: Okta: Lapsus$ breach lasted only 25 minutes, hit 2 customers
Date: 2022-04-20T12:48:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-20-okta-lapsus-breach-lasted-only-25-minutes-hit-2-customers

[Source](https://www.bleepingcomputer.com/news/security/okta-lapsus-breach-lasted-only-25-minutes-hit-2-customers/){:target="_blank" rel="noopener"}

> Identity and access management firm Okta says an investigation into the January Lapsus$ breach concluded the incident's impact was significantly smaller than expected. [...]
