Title: Russia (still) trying to weaponize Facebook for spying, Ukraine-war disinfo
Date: 2022-04-07T21:08:49+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-07-russia-still-trying-to-weaponize-facebook-for-spying-ukraine-war-disinfo

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/07/russia_weaponize_facebook/){:target="_blank" rel="noopener"}

> Plus: More financially motivated miscreants try to monetize invasion Facebook is fighting a surge in cyber-espionage attempts and misinformation campaigns related to the Russian invasion of Ukraine, according to a new report by parent group Meta.... [...]
