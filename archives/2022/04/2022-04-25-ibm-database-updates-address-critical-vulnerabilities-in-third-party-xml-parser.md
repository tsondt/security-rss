Title: IBM database updates address critical vulnerabilities in third-party XML parser
Date: 2022-04-25T15:39:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-25-ibm-database-updates-address-critical-vulnerabilities-in-third-party-xml-parser

[Source](https://portswigger.net/daily-swig/ibm-database-updates-address-critical-vulnerabilities-in-third-party-xml-parser){:target="_blank" rel="noopener"}

> Flaws in popular parser prompt updates from numerous downstream vendors [...]
