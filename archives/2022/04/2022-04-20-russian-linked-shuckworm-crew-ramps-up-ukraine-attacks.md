Title: Russian-linked Shuckworm crew ramps up Ukraine attacks
Date: 2022-04-20T16:04:05+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-20-russian-linked-shuckworm-crew-ramps-up-ukraine-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/20/shuckworm-attack-ukraine-symantec/){:target="_blank" rel="noopener"}

> Cyber-espionage gang using multiple variants of its custom backdoor to ensure persistence, Symantec warns A Russian-linked threat group that has almost exclusively targeted Ukraine since it first appeared on the scene in 2014 is deploying multiple variants of its malware payload on systems within the country.... [...]
