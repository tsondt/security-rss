Title: Friday Squid Blogging: Do Squid Have Emotions?
Date: 2022-04-08T21:12:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-04-08-friday-squid-blogging-do-squid-have-emotions

[Source](https://www.schneier.com/blog/archives/2022/04/friday-squid-blogging-do-squid-have-emotions.html){:target="_blank" rel="noopener"}

> Scientists are now debating whether octopuses, squid, and crabs have emotions. Short answer: we don’t know, but can’t rule it out. There may be a point when humans can no longer assume that crayfish, shrimp, and other invertebrates don’t feel pain and other emotions. “If they can no longer be considered immune to felt pain, invertebrate experiences will need to become part of our species’ moral landscape,” she says. “But pain is just one morally relevant emotion. Invertebrates such as octopuses may experience other emotions such as curiosity in exploration, affection for individuals, or excitement in anticipation of a future [...]
