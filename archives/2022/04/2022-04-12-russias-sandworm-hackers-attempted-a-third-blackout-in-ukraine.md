Title: Russia’s Sandworm hackers attempted a third blackout in Ukraine
Date: 2022-04-12T21:02:52+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;electrical transmission;hacking;malware;russia;sandworm;Ukraine
Slug: 2022-04-12-russias-sandworm-hackers-attempted-a-third-blackout-in-ukraine

[Source](https://arstechnica.com/?p=1847592){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images | Sundry Photography) More than half a decade has passed since the notorious Russian hackers known as Sandworm targeted an electrical transmission station north of Kyiv a week before Christmas in 2016, using a unique, automated piece of code to interact directly with the station's circuit breakers and turn off the lights to a fraction of Ukraine's capital. That unprecedented specimen of industrial control system malware has never been seen again—until now: In the midst of Russia's brutal invasion of Ukraine, Sandworm appears to be pulling out its old tricks. On Tuesday, the Ukrainian Computer Emergency [...]
