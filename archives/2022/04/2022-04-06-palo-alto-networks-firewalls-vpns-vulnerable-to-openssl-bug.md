Title: Palo Alto Networks firewalls, VPNs vulnerable to OpenSSL bug
Date: 2022-04-06T17:37:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-06-palo-alto-networks-firewalls-vpns-vulnerable-to-openssl-bug

[Source](https://www.bleepingcomputer.com/news/security/palo-alto-networks-firewalls-vpns-vulnerable-to-openssl-bug/){:target="_blank" rel="noopener"}

> American cybersecurity company Palo Alto Networks warned customers on Wednesday that some of its firewall, VPN, and XDR products are vulnerable to a high severity OpenSSL infinite loop bug disclosed three weeks ago [...]
