Title: VMware patches Spring4Shell RCE flaw in multiple products
Date: 2022-04-04T12:08:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-04-vmware-patches-spring4shell-rce-flaw-in-multiple-products

[Source](https://www.bleepingcomputer.com/news/security/vmware-patches-spring4shell-rce-flaw-in-multiple-products/){:target="_blank" rel="noopener"}

> ​​​​​​​VMWare has published a security advisory for the critical remote code execution vulnerability known as Spring4Shell, which impacts multiple of its cloud computing and virtualization products. [...]
