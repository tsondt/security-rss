Title: American Dental Association hit by new Black Basta ransomware
Date: 2022-04-26T14:42:33-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-26-american-dental-association-hit-by-new-black-basta-ransomware

[Source](https://www.bleepingcomputer.com/news/security/american-dental-association-hit-by-new-black-basta-ransomware/){:target="_blank" rel="noopener"}

> The American Dental Association (ADA) was hit by a weekend cyberattack, causing them to shut down portions of their network while investigating the attack. [...]
