Title: Microsoft dogs Strontium domains to stop attacks on Ukraine
Date: 2022-04-08T20:29:32+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-08-microsoft-dogs-strontium-domains-to-stop-attacks-on-ukraine

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/08/microsoft-russia-stronium-domains/){:target="_blank" rel="noopener"}

> Software giant sinkholes systems used by Russian gang Microsoft this week seized seven internet domains run by Russia-linked threat group Strontium, which was using the infrastructure to target Ukrainian institutions as well as think tanks in the US and EU, apparently to support Russian's invasion of its neighbor.... [...]
