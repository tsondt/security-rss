Title: Hamas-linked cyber-spies 'target high-ranking Israelis'
Date: 2022-04-06T20:24:37+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-06-hamas-linked-cyber-spies-target-high-ranking-israelis

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/06/apt-israeli-officials/){:target="_blank" rel="noopener"}

> Sensitive info swiped from Windows and Android devices, according to report A prolific Middle East team with links to Hamas is said to be using malware and infrastructure to target high-ranking Israeli officials and steal sensitive data from Windows and Android devices.... [...]
