Title: US government launches Bureau of Cyberspace and Digital Policy to enhance cybersecurity across nation
Date: 2022-04-05T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-05-us-government-launches-bureau-of-cyberspace-and-digital-policy-to-enhance-cybersecurity-across-nation

[Source](https://portswigger.net/daily-swig/us-government-launches-bureau-of-cyberspace-and-digital-policy-to-enhance-cybersecurity-across-nation){:target="_blank" rel="noopener"}

> Department will be tasked with addressing the security challenges and opportunities associated with cyberspace [...]
