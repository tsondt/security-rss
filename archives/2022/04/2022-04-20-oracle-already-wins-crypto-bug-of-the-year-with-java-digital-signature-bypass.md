Title: Oracle already wins 'crypto bug of the year' with Java digital signature bypass
Date: 2022-04-20T20:11:10+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-04-20-oracle-already-wins-crypto-bug-of-the-year-with-java-digital-signature-bypass

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/20/java_authentication_bug/){:target="_blank" rel="noopener"}

> Whole new meaning for zero consequences Java versions 15 to 18 contain a flaw in its ECDSA signature validation that makes it trivial for miscreants to digitally sign files and other data as if they were legit organizations.... [...]
