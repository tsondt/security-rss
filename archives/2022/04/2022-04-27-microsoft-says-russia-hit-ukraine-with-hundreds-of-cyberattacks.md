Title: Microsoft says Russia hit Ukraine with hundreds of cyberattacks
Date: 2022-04-27T14:09:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-27-microsoft-says-russia-hit-ukraine-with-hundreds-of-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-says-russia-hit-ukraine-with-hundreds-of-cyberattacks/){:target="_blank" rel="noopener"}

> Microsoft has revealed the true scale of Russian-backed cyberattacks against Ukraine since the invasion, with hundreds of attempts from multiple Russian hacking groups targeting the country's infrastructure and Ukrainian citizens. [...]
