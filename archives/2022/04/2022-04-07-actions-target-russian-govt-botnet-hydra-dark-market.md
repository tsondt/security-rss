Title: Actions Target Russian Govt. Botnet, Hydra Dark Market
Date: 2022-04-07T22:03:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;Web Fraud 2.0;Ars Technica;ASUS;Beserk Bear;Cyclops Blink;Dan Goodin;Dragonfly 2.0;fbi;Federal Security Service;Garantex;German Federal Criminal Police Office;GRU;Hydra Market;Main Intelligence Directorate;NotPetya;Russian FSB;Sandworm;Trisis;Triton;U.S. Department of Justice;U.S. Department of Treasury;Voodoo Bear;VPNFilter;WatchGuard
Slug: 2022-04-07-actions-target-russian-govt-botnet-hydra-dark-market

[Source](https://krebsonsecurity.com/2022/04/actions-target-russian-govt-botnet-hydra-dark-market/){:target="_blank" rel="noopener"}

> The U.S. Federal Bureau of Investigation (FBI) says it has disrupted a giant botnet built and operated by a Russian government intelligence unit known for launching destructive cyberattacks against energy infrastructure in the United States and Ukraine. Separately, law enforcement agencies in the U.S. and Germany moved to decapitate “ Hydra,” a billion-dollar Russian darknet drug bazaar that also helped to launder the profits of multiple Russian ransomware groups. FBI officials said Wednesday they disrupted “ Cyclops Blink,” a collection of compromised networking devices managed by hackers working with the Russian Federation’s Main Intelligence Directorate (GRU). A statement from the [...]
