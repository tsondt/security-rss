Title: Ransom DDoS attacks have dropped to record lows this year
Date: 2022-04-12T14:51:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-12-ransom-ddos-attacks-have-dropped-to-record-lows-this-year

[Source](https://www.bleepingcomputer.com/news/security/ransom-ddos-attacks-have-dropped-to-record-lows-this-year/){:target="_blank" rel="noopener"}

> Extortion denial-of-service activity, the so-called RDDoS (ransom distributed denial-of-service) attacks have taken a tumble in the first quarter of the year, according to recent statistics from Cloudflare. [...]
