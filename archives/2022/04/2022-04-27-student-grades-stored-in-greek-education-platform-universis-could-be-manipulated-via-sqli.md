Title: Student grades stored in Greek education platform UniverSIS could be manipulated via SQLi
Date: 2022-04-27T11:30:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-27-student-grades-stored-in-greek-education-platform-universis-could-be-manipulated-via-sqli

[Source](https://portswigger.net/daily-swig/student-grades-stored-in-greek-education-platform-universis-could-be-manipulated-via-sqli){:target="_blank" rel="noopener"}

> Maintainers promptly patch issue that could also leak sensitive personal data [...]
