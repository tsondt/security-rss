Title: Critical flaw in Elementor WordPress plugin may affect 500k sites
Date: 2022-04-13T10:51:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-13-critical-flaw-in-elementor-wordpress-plugin-may-affect-500k-sites

[Source](https://www.bleepingcomputer.com/news/security/critical-flaw-in-elementor-wordpress-plugin-may-affect-500k-sites/){:target="_blank" rel="noopener"}

> The authors of the Elementor Website Builder plugin for WordPress have just released version 3.6.3 to address a critical remote code execution flaw that may impact as many as 500,000 websites. [...]
