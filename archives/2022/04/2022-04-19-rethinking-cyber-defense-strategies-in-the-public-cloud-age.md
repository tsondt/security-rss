Title: Rethinking Cyber-Defense Strategies in the Public-Cloud Age
Date: 2022-04-19T17:29:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2022-04-19-rethinking-cyber-defense-strategies-in-the-public-cloud-age

[Source](https://threatpost.com/cyber-defense-public-cloud/179342/){:target="_blank" rel="noopener"}

> Exploring what's next for public-cloud security, including top risks and how to implement better risk management. [...]
