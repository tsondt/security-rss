Title: ESET uncovers vulnerabilities in Lenovo laptops
Date: 2022-04-19T15:00:04+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-04-19-eset-uncovers-vulnerabilities-in-lenovo-laptops

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/19/eset_lenovo/){:target="_blank" rel="noopener"}

> Firmware updates incoming in response to UEFI threats Updated Got a Lenovo laptop? You might need to do a swift bit of patching judging by the latest set of vulnerabilities uncovered by security researchers at ESET.... [...]
