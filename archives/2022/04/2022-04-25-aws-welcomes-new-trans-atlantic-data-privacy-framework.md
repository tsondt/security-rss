Title: AWS welcomes new Trans-Atlantic Data Privacy Framework
Date: 2022-04-25T17:45:40+00:00
Author: Michael Punke
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;data privacy;GDPR
Slug: 2022-04-25-aws-welcomes-new-trans-atlantic-data-privacy-framework

[Source](https://aws.amazon.com/blogs/security/aws-welcomes-new-trans-atlantic-data-privacy-framework/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) welcomes the new Trans-Atlantic Data Privacy Framework (Data Privacy Framework) that was agreed to, in principle, between the European Union (EU) and the United States (US) last month. This announcement demonstrates the common will between the US and EU to strengthen privacy protections in trans-Atlantic data flows, and will supplement the [...]
