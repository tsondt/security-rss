Title: Emotet botnet switches to 64-bit modules, increases activity
Date: 2022-04-19T15:57:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-19-emotet-botnet-switches-to-64-bit-modules-increases-activity

[Source](https://www.bleepingcomputer.com/news/security/emotet-botnet-switches-to-64-bit-modules-increases-activity/){:target="_blank" rel="noopener"}

> The Emotet malware is having a burst in distribution and is likely to soon switch to new payloads that are currently detected by fewer antivirus engines. [...]
