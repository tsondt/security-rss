Title: Hardware-assisted security poised for growth, says Intel
Date: 2022-04-12T17:30:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-04-12-hardware-assisted-security-poised-for-growth-says-intel

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/12/hardware_assisted_security_poised_for_growth/){:target="_blank" rel="noopener"}

> Only 36% use it now, but an additional 47% plan to adopt HAS in the next year An Intel study finds that businesses are eager for cybersecurity and are keen to see how security can be baked into devices.... [...]
