Title: <span>Microsoft report unmasks at least six Russian nation-state actors responsible for cyber-attacks against Ukraine</span>
Date: 2022-04-28T15:31:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-28-microsoft-report-unmasks-at-least-six-russian-nation-state-actors-responsible-for-cyber-attacks-against-ukraine

[Source](https://portswigger.net/daily-swig/microsoft-report-unmasks-at-least-six-russian-nation-state-actors-responsible-for-cyber-attacks-against-ukraine){:target="_blank" rel="noopener"}

> Kremlin-linked actors have launched multiple assaults since invasion began [...]
