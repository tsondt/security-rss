Title: Chinese state-backed hackers now target Russian state officers
Date: 2022-04-27T08:38:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-27-chinese-state-backed-hackers-now-target-russian-state-officers

[Source](https://www.bleepingcomputer.com/news/security/chinese-state-backed-hackers-now-target-russian-state-officers/){:target="_blank" rel="noopener"}

> Security researchers analyzing a phishing campaign targeting Russian officials found evidence that points to the China-based threat actor tracked as Mustang Panda (also known as HoneyMyte and Bronze President). [...]
