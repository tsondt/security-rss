Title: FBI warns of ransomware attacks targeting US agriculture sector
Date: 2022-04-20T15:13:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-20-fbi-warns-of-ransomware-attacks-targeting-us-agriculture-sector

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-ransomware-attacks-targeting-us-agriculture-sector/){:target="_blank" rel="noopener"}

> The US Federal Bureau of Investigation (FBI) warned Food and Agriculture (FA) sector organizations today of an increased risk that ransomware gangs "may be more likely" to attack them during the harvest and planting seasons. [...]
