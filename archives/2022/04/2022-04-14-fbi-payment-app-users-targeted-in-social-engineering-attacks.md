Title: FBI: Payment app users targeted in social engineering attacks
Date: 2022-04-14T17:53:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-14-fbi-payment-app-users-targeted-in-social-engineering-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-payment-app-users-targeted-in-social-engineering-attacks/){:target="_blank" rel="noopener"}

> Cybercriminals are attempting to trick American users of digital payment apps into making instant money transfers in social engineering attacks using text messages with fake bank fraud alerts. [...]
