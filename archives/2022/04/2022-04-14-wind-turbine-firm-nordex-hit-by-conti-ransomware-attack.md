Title: Wind turbine firm Nordex hit by Conti ransomware attack
Date: 2022-04-14T21:54:48-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-14-wind-turbine-firm-nordex-hit-by-conti-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/wind-turbine-firm-nordex-hit-by-conti-ransomware-attack/){:target="_blank" rel="noopener"}

> The Conti ransomware operation has claimed responsibility for a cyberattack on wind turbine giant Nordex, which was forced to shut down IT systems and remote access to the managed turbines earlier this month. [...]
