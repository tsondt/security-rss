Title: Zero-Day Vulnerabilities Are on the Rise
Date: 2022-04-27T18:40:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hacking;malware;ransomware;vulnerabilities;zero-day
Slug: 2022-04-27-zero-day-vulnerabilities-are-on-the-rise

[Source](https://www.schneier.com/blog/archives/2022/04/zero-day-vulnerabilities-are-on-the-rise.html){:target="_blank" rel="noopener"}

> Both Google and Mandiant are reporting a significant increase in the number of zero-day vulnerabilities reported in 2021. Google: 2021 included the detection and disclosure of 58 in-the-wild 0-days, the most ever recorded since Project Zero began tracking in mid-2014. That’s more than double the previous maximum of 28 detected in 2015 and especially stark when you consider that there were only 25 detected in 2020. We’ve tracked publicly known in-the-wild 0-day exploits in this spreadsheet since mid-2014. While we often talk about the number of 0-day exploits used in-the-wild, what we’re actually discussing is the number of 0-day exploits [...]
