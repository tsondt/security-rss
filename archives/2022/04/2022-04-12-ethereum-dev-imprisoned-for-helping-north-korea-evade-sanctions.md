Title: Ethereum dev imprisoned for helping North Korea evade sanctions
Date: 2022-04-12T17:42:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-12-ethereum-dev-imprisoned-for-helping-north-korea-evade-sanctions

[Source](https://www.bleepingcomputer.com/news/security/ethereum-dev-imprisoned-for-helping-north-korea-evade-sanctions/){:target="_blank" rel="noopener"}

> Virgil Griffith, a US cryptocurrency expert, was sentenced on Tuesday to 63 months in prison after pleading guilty to assisting the Democratic People's Republic of Korea (DPRK) with technical info on how to evade sanctions. [...]
