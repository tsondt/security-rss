Title: New Black Basta ransomware springs into action with a dozen breaches
Date: 2022-04-27T17:46:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-27-new-black-basta-ransomware-springs-into-action-with-a-dozen-breaches

[Source](https://www.bleepingcomputer.com/news/security/new-black-basta-ransomware-springs-into-action-with-a-dozen-breaches/){:target="_blank" rel="noopener"}

> A new ransomware gang known as Black Basta has quickly catapulted into operation this month, claiming to have breached over twelve companies in just a few weeks. [...]
