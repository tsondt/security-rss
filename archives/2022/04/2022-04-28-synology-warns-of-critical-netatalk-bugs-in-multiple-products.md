Title: Synology warns of critical Netatalk bugs in multiple products
Date: 2022-04-28T14:55:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-28-synology-warns-of-critical-netatalk-bugs-in-multiple-products

[Source](https://www.bleepingcomputer.com/news/security/synology-warns-of-critical-netatalk-bugs-in-multiple-products/){:target="_blank" rel="noopener"}

> Synology has warned customers that some of its network-attached storage (NAS) appliances are exposed to attacks exploiting multiple critical Netatalk vulnerabilities. [...]
