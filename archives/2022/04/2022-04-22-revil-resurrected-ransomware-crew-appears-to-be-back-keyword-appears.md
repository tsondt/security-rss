Title: REvil resurrected? Ransomware crew appears to be back. Keyword: Appears
Date: 2022-04-22T06:24:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-22-revil-resurrected-ransomware-crew-appears-to-be-back-keyword-appears

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/22/revil_ransomware_returns/){:target="_blank" rel="noopener"}

> Months after arrests, gang – or someone mimicking them – now active The notorious REvil ransomware gang appears to have returned from the bowels of the dark web, three months after the arrest of 14 of its suspected members, with its old website forwarding to a new operation that lists both previous and fresh victims.... [...]
