Title: Real-time voice concealment algorithm blocks microphone spying
Date: 2022-04-19T13:37:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-19-real-time-voice-concealment-algorithm-blocks-microphone-spying

[Source](https://www.bleepingcomputer.com/news/security/real-time-voice-concealment-algorithm-blocks-microphone-spying/){:target="_blank" rel="noopener"}

> Columbia University researchers have developed a novel algorithm that can block rogue audio eavesdropping via microphones in smartphones, voice assistants, and IoTs in general. [...]
