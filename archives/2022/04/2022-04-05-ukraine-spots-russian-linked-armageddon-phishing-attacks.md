Title: Ukraine spots Russian-linked 'Armageddon' phishing attacks
Date: 2022-04-05T09:10:56-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-05-ukraine-spots-russian-linked-armageddon-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/ukraine-spots-russian-linked-armageddon-phishing-attacks/){:target="_blank" rel="noopener"}

> The Computer Emergency Response Team of Ukraine (CERT-UA) has spotted new phishing attempts attributed to the Russian threat group tracked as Armageddon (Gamaredon). [...]
