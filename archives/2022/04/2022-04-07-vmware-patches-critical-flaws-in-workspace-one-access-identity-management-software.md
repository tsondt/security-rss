Title: VMware patches critical flaws in Workspace ONE Access identity management software
Date: 2022-04-07T15:29:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-07-vmware-patches-critical-flaws-in-workspace-one-access-identity-management-software

[Source](https://portswigger.net/daily-swig/vmware-patches-critical-flaws-in-workspace-one-access-identity-management-software){:target="_blank" rel="noopener"}

> Virtual reality [...]
