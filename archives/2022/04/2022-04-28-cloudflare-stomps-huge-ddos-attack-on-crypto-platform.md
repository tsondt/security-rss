Title: Cloudflare stomps huge DDoS attack on crypto platform
Date: 2022-04-28T15:30:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-28-cloudflare-stomps-huge-ddos-attack-on-crypto-platform

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/28/cloudflare-largest-ddos-attack-/){:target="_blank" rel="noopener"}

> At 15.3 million requests per second, the assault was the largest HTTPS blitz on record lasting 15 seconds Cloudflare this month halted a massive distributed denial-of-service (DDoS) attack on a cryptocurrency platform that not only was unusual in its sheer size but also because it was launched over HTTPS and primarily originated from cloud datacenters rather than residential internet service providers (ISPs).... [...]
