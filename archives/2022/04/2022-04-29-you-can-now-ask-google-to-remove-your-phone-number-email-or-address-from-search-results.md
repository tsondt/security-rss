Title: You Can Now Ask Google to Remove Your Phone Number, Email or Address from Search Results
Date: 2022-04-29T19:25:49+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;briansclub;google;Michelle Chang;Techcrunch
Slug: 2022-04-29-you-can-now-ask-google-to-remove-your-phone-number-email-or-address-from-search-results

[Source](https://krebsonsecurity.com/2022/04/you-can-now-ask-google-to-remove-your-phone-number-email-or-address-from-search-results/){:target="_blank" rel="noopener"}

> Google said this week it is expanding the types of data people can ask to have removed from search results, to include personal contact information like your phone number, email address or physical address. The move comes just months after Google rolled out a new policy enabling people under the age of 18 (or a parent/guardian) to request removal of their images from Google search results. Google has for years accepted requests to remove certain sensitive data such as bank account or credit card numbers from search results. In a blog post on Wednesday, Google’s Michelle Chang wrote that the [...]
