Title: VMware warns of critical vulnerabilities in multiple products
Date: 2022-04-06T14:01:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-06-vmware-warns-of-critical-vulnerabilities-in-multiple-products

[Source](https://www.bleepingcomputer.com/news/security/vmware-warns-of-critical-vulnerabilities-in-multiple-products/){:target="_blank" rel="noopener"}

> VMware has warned customers to immediately patch critical vulnerabilities in multiple products that could be used by threat actors to launch remote code execution attacks. [...]
