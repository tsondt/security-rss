Title: Feds take down Kremlin-backed Cyclops Blink botnet
Date: 2022-04-06T19:24:35+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-06-feds-take-down-kremlin-backed-cyclops-blink-botnet

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/06/takedown_cyclops_blink/){:target="_blank" rel="noopener"}

> Control systems scrubbed, hijacked network devices need to be patched and cleaned The US Justice Department today revealed details of a court-authorized take-down of command-and-control systems the Sandworm cyber-crime ring used to direct network devices infected by its Cyclops Blink malware.... [...]
