Title: Internal AWS credentials swiped by researcher via SQL payload
Date: 2022-04-12T15:47:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-12-internal-aws-credentials-swiped-by-researcher-via-sql-payload

[Source](https://portswigger.net/daily-swig/internal-aws-credentials-swiped-by-researcher-via-sql-payload){:target="_blank" rel="noopener"}

> Amazon cloud service acts quickly to close security hole in RDS [...]
