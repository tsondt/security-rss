Title: There are few guarantees when it comes to ransomware, except that you’re a target
Date: 2022-04-11T16:15:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-04-11-there-are-few-guarantees-when-it-comes-to-ransomware-except-that-youre-a-target

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/11/there_are_few_guarantees_when/){:target="_blank" rel="noopener"}

> Learn how to live with that by joining this session Webinar Are there any cast iron guarantees when it comes to ransomware? Well, you can guarantee that your organization will come under attack sooner or later. Probably sooner.... [...]
