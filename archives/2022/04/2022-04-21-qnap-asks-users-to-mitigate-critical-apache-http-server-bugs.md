Title: QNAP asks users to mitigate critical Apache HTTP Server bugs
Date: 2022-04-21T13:03:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-21-qnap-asks-users-to-mitigate-critical-apache-http-server-bugs

[Source](https://www.bleepingcomputer.com/news/security/qnap-asks-users-to-mitigate-critical-apache-http-server-bugs/){:target="_blank" rel="noopener"}

> QNAP has asked customers to apply mitigation measures to block attempts to exploit Apache HTTP Server security vulnerabilities impacting their network-attached storage (NAS) devices. [...]
