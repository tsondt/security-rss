Title: Snap-on discloses data breach claimed by Conti ransomware gang
Date: 2022-04-08T16:35:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-08-snap-on-discloses-data-breach-claimed-by-conti-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/snap-on-discloses-data-breach-claimed-by-conti-ransomware-gang/){:target="_blank" rel="noopener"}

> American automotive tools manufacturer Snap-on announced a data breach exposing associate and franchisee data after the Conti ransomware gang began leaking the company's data in March. [...]
