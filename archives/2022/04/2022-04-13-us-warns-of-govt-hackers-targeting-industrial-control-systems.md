Title: US warns of govt hackers targeting industrial control systems
Date: 2022-04-13T13:53:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-13-us-warns-of-govt-hackers-targeting-industrial-control-systems

[Source](https://www.bleepingcomputer.com/news/security/us-warns-of-govt-hackers-targeting-industrial-control-systems/){:target="_blank" rel="noopener"}

> A joint cybersecurity advisory issued by CISA, NSA, FBI, and the Department of Energy (DOE) warns of government-backed hacking groups being able to hijack multiple industrial devices using a new ICS-focused malware toolkit. [...]
