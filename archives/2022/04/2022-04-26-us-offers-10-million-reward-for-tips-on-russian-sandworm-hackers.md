Title: US offers $10 million reward for tips on Russian Sandworm hackers
Date: 2022-04-26T17:20:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-26-us-offers-10-million-reward-for-tips-on-russian-sandworm-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-offers-10-million-reward-for-tips-on-russian-sandworm-hackers/){:target="_blank" rel="noopener"}

> The U.S. is offering up to $10 million to identify or locate six Russian GRU hackers who are part of the notorious Sandworm hacking group. [...]
