Title: China accused of cyberattacks on Indian power grid
Date: 2022-04-08T07:58:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-04-08-china-accused-of-cyberattacks-on-indian-power-grid

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/08/china_sponsored_attacks_india_ukraine/){:target="_blank" rel="noopener"}

> Beijing may have had a hand in attacks in Ukraine, too China has been accused of conducting a long-term cyber attack on India's power grid, and has been implicated in cyber attacks against targets in Ukraine.... [...]
