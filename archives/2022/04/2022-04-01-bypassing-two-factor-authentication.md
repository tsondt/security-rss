Title: Bypassing Two-Factor Authentication
Date: 2022-04-01T11:12:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;computer security;passwords;two-factor authentication
Slug: 2022-04-01-bypassing-two-factor-authentication

[Source](https://www.schneier.com/blog/archives/2022/04/bypassing-two-factor-authentication.html){:target="_blank" rel="noopener"}

> These techniques are not new, but they’re increasingly popular :...some forms of MFA are stronger than others, and recent events show that these weaker forms aren’t much of a hurdle for some hackers to clear. In the past few months, suspected script kiddies like the Lapsus$ data extortion gang and elite Russian-state threat actors (like Cozy Bear, the group behind the SolarWinds hack) have both successfully defeated the protection. [...] Methods include: Sending a bunch of MFA requests and hoping the target finally accepts one to make the noise stop. Sending one or two prompts per day. This method often [...]
