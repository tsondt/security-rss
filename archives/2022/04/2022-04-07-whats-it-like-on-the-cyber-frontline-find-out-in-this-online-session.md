Title: What’s it like on the cyber frontline? Find out in this online session
Date: 2022-04-07T23:16:04+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-04-07-whats-it-like-on-the-cyber-frontline-find-out-in-this-online-session

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/07/cyber_frontline/){:target="_blank" rel="noopener"}

> UK NCSC founder in conversation with Rubrik EMEA CTO Paid post When’s the ideal time to reexamine your cybersecurity and data-protection guidelines?... [...]
