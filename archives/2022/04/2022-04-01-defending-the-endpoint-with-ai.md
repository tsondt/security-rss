Title: Defending the endpoint with AI
Date: 2022-04-01T07:00:10+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2022-04-01-defending-the-endpoint-with-ai

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/01/defending_the_endpoint_with_ai/){:target="_blank" rel="noopener"}

> Darktrace Enterprise Immune System collates and crunches network traffic patterns Paid feature Remember the good old days, when the only devices a company had to worry about were the PCs on its own network? Today, security teams must yearn for those times as they struggle to protect endpoint devices everywhere.... [...]
