Title: RaidForums hacking forum seized by police, owner arrested
Date: 2022-04-12T10:51:28-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-04-12-raidforums-hacking-forum-seized-by-police-owner-arrested

[Source](https://www.bleepingcomputer.com/news/security/raidforums-hacking-forum-seized-by-police-owner-arrested/){:target="_blank" rel="noopener"}

> The RaidForums hacker forum, used mainly for trading and selling stolen databases, has been shut down and its domain seized by U.S. law enforcement during Operation TOURNIQUET, an action coordinated by Europol that involved law enforcement agencies in several countries. [...]
