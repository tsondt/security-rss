Title: Microsoft disables SMB1 by default for Windows 11 Home Insiders
Date: 2022-04-19T14:30:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-04-19-microsoft-disables-smb1-by-default-for-windows-11-home-insiders

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-disables-smb1-by-default-for-windows-11-home-insiders/){:target="_blank" rel="noopener"}

> Microsoft announced today that the 30-year-old SMBv1 file-sharing protocol is now disabled by default on Windows systems running the latest Windows 11 Home Dev channel builds, the last editions of Windows or Windows Server that still came with SMBv1 enabled. [...]
