Title: Friday Squid Blogging: Squid Skin–Inspired Insulating Material
Date: 2022-04-22T21:04:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2022-04-22-friday-squid-blogging-squid-skininspired-insulating-material

[Source](https://www.schneier.com/blog/archives/2022/04/friday-squid-blogging-squid-skin-inspired-insulating-material.html){:target="_blank" rel="noopener"}

> Interesting : Drawing inspiration from cephalopod skin, engineers at the University of California, Irvine invented an adaptive composite material that can insulate beverage cups, restaurant to-go bags, parcel boxes and even shipping containers. [...] “The metal islands in our composite material are next to one another when the material is relaxed and become separated when the material is stretched, allowing for control of the reflection and transmission of infrared light or heat dissipation,” said Gorodetsky. “The mechanism is analogous to chromatophore expansion and contraction in a squid’s skin, which alters the reflection and transmission of visible light.” Chromatophore size changes [...]
