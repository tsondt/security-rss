Title: Microsoft's huge Patch Tuesday includes fix for bug under attack
Date: 2022-04-13T01:36:32+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-13-microsofts-huge-patch-tuesday-includes-fix-for-bug-under-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/13/microsoft_patch_tuesday/){:target="_blank" rel="noopener"}

> April bundle addresses 100-plus vulnerabilities including 10 critical RCEs Microsoft's massive April Patch Tuesday includes one bug that has already been exploited in the wild and a second that has been publicly disclosed.... [...]
