Title: Crooks use fake emergency data requests to get personal info out of Big Tech – report
Date: 2022-04-02T15:11:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-02-crooks-use-fake-emergency-data-requests-to-get-personal-info-out-of-big-tech-report

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/02/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Hive hits health-care org, law enforcement ransomware response is lacking, and orgs can't meet new disclosure rules In Brief Cybercriminals have used fake emergency data requests (EDRs) to steal sensitive customer data from service providers and social media firms. At least one report suggests Apple, and Facebook's parent company Meta, were victims of this fraud.... [...]
