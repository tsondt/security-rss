Title: China turns cyber-espionage eyes to Russia as Ukraine invasion grinds on
Date: 2022-04-27T14:00:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-27-china-turns-cyber-espionage-eyes-to-russia-as-ukraine-invasion-grinds-on

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/27/china-bronze-president-malware-russia/){:target="_blank" rel="noopener"}

> State-sponsored Bronze President group launches cyber-espionage malware campaign against notional ally China appears to be entering a raging cyber-espionage battle that's grown in line with Russia's unprovoked attack on Ukraine, deploying advanced malware on the computer systems of Russian officials.... [...]
