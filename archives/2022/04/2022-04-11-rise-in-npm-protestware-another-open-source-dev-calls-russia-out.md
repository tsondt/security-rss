Title: Rise in npm protestware: another open source dev calls Russia out
Date: 2022-04-11T17:02:23-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-04-11-rise-in-npm-protestware-another-open-source-dev-calls-russia-out

[Source](https://www.bleepingcomputer.com/news/security/rise-in-npm-protestware-another-open-source-dev-calls-russia-out/){:target="_blank" rel="noopener"}

> Developers are increasingly voicing their opinions through their open source projects in active use by thousands of software applications and organizations. Most recently, the developer of the 'event-source-polyfill' npm package peacefully protested Russia's "unreasonable invasion" of Ukraine, to Russian consumers. [...]
