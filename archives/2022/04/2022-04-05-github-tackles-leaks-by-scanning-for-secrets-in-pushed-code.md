Title: GitHub tackles leaks by scanning for secrets in pushed code
Date: 2022-04-05T16:00:08+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-04-05-github-tackles-leaks-by-scanning-for-secrets-in-pushed-code

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/05/github_prevents_leaks_by_scanning/){:target="_blank" rel="noopener"}

> Repo updates inspected for security blunders before some git can exploit them GitHub is aiming to help users avoid inadvertent leaks of confidential objects like access tokens by scanning repository content for such secrets before a git push is allowed to complete.... [...]
