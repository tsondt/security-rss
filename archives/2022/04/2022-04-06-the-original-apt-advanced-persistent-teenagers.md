Title: The Original APT: Advanced Persistent Teenagers
Date: 2022-04-06T17:55:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;The Coming Storm;Advanced Persistent Teenagers;Amit Yoran;APT;CISA;fbi;LAPSUS$;microsoft;NVIDIA;Okta;Samsung;Tenable;Twitter hack;vishing;voice phishing;wired
Slug: 2022-04-06-the-original-apt-advanced-persistent-teenagers

[Source](https://krebsonsecurity.com/2022/04/the-original-apt-advanced-persistent-teenagers/){:target="_blank" rel="noopener"}

> Many organizations are already struggling to combat cybersecurity threats from ransomware purveyors and state-sponsored hacking groups, both of which tend to take days or weeks to pivot from an opportunistic malware infection to a full blown data breach. But few organizations have a playbook for responding to the kinds of virtual “smash and grab” attacks we’ve seen recently from LAPSUS$, a juvenile data extortion group whose short-lived, low-tech and remarkably effective tactics have put some of the world’s biggest corporations on edge. Since surfacing in late 2021, LAPSUS$ has gained access to the networks or contractors for some of the [...]
