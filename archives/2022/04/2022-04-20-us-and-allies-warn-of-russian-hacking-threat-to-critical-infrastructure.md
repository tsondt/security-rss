Title: US and allies warn of Russian hacking threat to critical infrastructure
Date: 2022-04-20T13:59:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-20-us-and-allies-warn-of-russian-hacking-threat-to-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/us-and-allies-warn-of-russian-hacking-threat-to-critical-infrastructure/){:target="_blank" rel="noopener"}

> Today, Five Eyes cybersecurity authorities warned critical infrastructure network defenders of an increased risk that Russia-backed hacking groups could target organizations within and outside Ukraine's borders. [...]
