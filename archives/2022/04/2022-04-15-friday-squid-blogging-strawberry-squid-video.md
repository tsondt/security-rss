Title: Friday Squid Blogging: Strawberry Squid Video
Date: 2022-04-15T21:07:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-04-15-friday-squid-blogging-strawberry-squid-video

[Source](https://www.schneier.com/blog/archives/2022/04/friday-squid-blogging-strawberry-squid-video.html){:target="_blank" rel="noopener"}

> Beautiful video shot off the California coast. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
