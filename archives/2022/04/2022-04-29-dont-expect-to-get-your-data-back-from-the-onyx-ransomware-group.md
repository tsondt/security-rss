Title: Don’t expect to get your data back from the Onyx ransomware group
Date: 2022-04-29T15:00:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-29-dont-expect-to-get-your-data-back-from-the-onyx-ransomware-group

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/29/onyx-ransomware-destroy-files/){:target="_blank" rel="noopener"}

> The cybercriminals trash files larger than 2MB, forever losing them to the void Ransomware groups in recent years have ramped up the threats against victims to incentivize them to pay the ransom in return for their stolen and encrypted data. But a new crew is essentially destroying files larger than 2MB, so data in those files is lost even if the ransom is paid.... [...]
