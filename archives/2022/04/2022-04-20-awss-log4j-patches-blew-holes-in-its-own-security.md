Title: AWS's Log4j patches blew holes in its own security
Date: 2022-04-20T21:51:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-20-awss-log4j-patches-blew-holes-in-its-own-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/20/aws_log4j_patches/){:target="_blank" rel="noopener"}

> Remote code exec is so 2014. Have this container escape and privilege escalation, instead Amazon Web Services has updated its Log4j security patches after it was discovered the original fixes made customer deployments vulnerable to container escape and privilege escalation.... [...]
