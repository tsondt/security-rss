Title: Cash App notifies 8.2 million US customers about data breach
Date: 2022-04-05T16:09:14-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-05-cash-app-notifies-82-million-us-customers-about-data-breach

[Source](https://www.bleepingcomputer.com/news/security/cash-app-notifies-82-million-us-customers-about-data-breach/){:target="_blank" rel="noopener"}

> Cash App is notifying 8.2 million current and former US customers of a data breach after a former employee accessed their account information. [...]
