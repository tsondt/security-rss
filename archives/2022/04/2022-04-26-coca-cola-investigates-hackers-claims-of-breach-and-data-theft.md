Title: Coca-Cola investigates hackers' claims of breach and data theft
Date: 2022-04-26T14:20:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2022-04-26-coca-cola-investigates-hackers-claims-of-breach-and-data-theft

[Source](https://www.bleepingcomputer.com/news/security/coca-cola-investigates-hackers-claims-of-breach-and-data-theft/){:target="_blank" rel="noopener"}

> Coca-Cola, the world's largest soft drinks maker, has confirmed in a statement to BleepingComputer that it is aware of the reports about a cyberattack on its network and is currently investigating the claims. [...]
