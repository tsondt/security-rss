Title: Cyberespionage APT Now Identified as Three Separate Actors
Date: 2022-04-29T11:51:05+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks
Slug: 2022-04-29-cyberespionage-apt-now-identified-as-three-separate-actors

[Source](https://threatpost.com/apt-id-3-separate-actors/179435/){:target="_blank" rel="noopener"}

> The threat group known as TA410 that wields the sophisticated FlowCloud RAT actually has three subgroups operating globally, each with their own toolsets and targets. [...]
