Title: LinkedIn brand takes lead as most impersonated in phishing attacks
Date: 2022-04-19T06:14:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-19-linkedin-brand-takes-lead-as-most-impersonated-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/linkedin-brand-takes-lead-as-most-impersonated-in-phishing-attacks/){:target="_blank" rel="noopener"}

> Security researchers are warning that LinkedIn has become the most spoofed brand in phishing attacks, accounting for more than 52% of all such incidents at a global level. [...]
