Title: Mandiant shareholder sues to block $5.4b Google deal
Date: 2022-04-04T20:31:24+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-04-mandiant-shareholder-sues-to-block-54b-google-deal

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/04/mandiant_google_lawsuit/){:target="_blank" rel="noopener"}

> Investors given 'materially incomplete and misleading' info, it is claimed A Mandiant shareholder has launched a legal challenge to block Google's $5.4 billion takeover of the threat intelligence firm.... [...]
