Title: Feds offer big rewards for info on suspected Russian Sandworm intel officers
Date: 2022-04-27T17:46:55+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-27-feds-offer-big-rewards-for-info-on-suspected-russian-sandworm-intel-officers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/27/feds_10m_reward_sandworm/){:target="_blank" rel="noopener"}

> A different type of bug bounty Uncle Sam will dole out up to $10 million for vital information on each of six Russian GRU officers linked to the Kremlin-backed Sandworm gang, who, according to the Feds, have plotted to carry out destructive cyber-attacks against American critical infrastructure.... [...]
