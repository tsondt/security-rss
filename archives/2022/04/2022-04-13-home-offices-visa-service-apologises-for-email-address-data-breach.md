Title: Home Office’s visa service apologises for email address data breach
Date: 2022-04-13T16:34:43+00:00
Author: Diane Taylor
Category: The Guardian
Tags: Data protection;Home Office;UK news;Data and computer security;Technology;GDPR;Email
Slug: 2022-04-13-home-offices-visa-service-apologises-for-email-address-data-breach

[Source](https://www.theguardian.com/technology/2022/apr/13/home-offices-visa-service-apologises-for-email-address-data-breach){:target="_blank" rel="noopener"}

> Private contractor running service sent email to applicants containing more than 170 email addresses The Home Office’s visa service has apologised for a data breach in which the email addresses of more than 170 people were mistakenly copied into an email circulated last week. More than 170 email addresses were accidentally copied into a message on 7 April 2022 about the change of location for a visa appointment with the UK Visa and Citizenship Application Service. The UKVCAS is run on behalf of the Home Office by the private contractor Sopra Steria. Some of the email addresses appeared to be [...]
