Title: Microsoft increases awards for high-impact Microsoft 365 bugs
Date: 2022-04-14T15:10:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-04-14-microsoft-increases-awards-for-high-impact-microsoft-365-bugs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-increases-awards-for-high-impact-microsoft-365-bugs/){:target="_blank" rel="noopener"}

> Microsoft has increased the maximum awards for high-impact security flaws reported through the Microsoft 365 and the Dynamics 365 / Power Platform bug bounty programs. [...]
