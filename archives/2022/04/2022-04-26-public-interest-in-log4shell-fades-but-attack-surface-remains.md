Title: Public interest in Log4Shell fades but attack surface remains
Date: 2022-04-26T10:59:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-26-public-interest-in-log4shell-fades-but-attack-surface-remains

[Source](https://www.bleepingcomputer.com/news/security/public-interest-in-log4shell-fades-but-attack-surface-remains/){:target="_blank" rel="noopener"}

> It's been four months since Log4Shell, a critical zero-day vulnerability in the ubiquitous Apache Log4j library, was discovered, and threat analysts warn that the application of the available fixes is still way behind. [...]
