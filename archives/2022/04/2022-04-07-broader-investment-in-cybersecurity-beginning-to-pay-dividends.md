Title: Broader investment in cybersecurity beginning to pay dividends
Date: 2022-04-07T14:30:07+00:00
Author: Richard Currie
Category: The Register
Tags: 
Slug: 2022-04-07-broader-investment-in-cybersecurity-beginning-to-pay-dividends

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/07/security_driving_down_ransomware_payments/){:target="_blank" rel="noopener"}

> Improved defenses give organizations more room to negotiate but won't protect from lawsuits, says law firm An increased willingness on the part of enterprises to invest in cybersecurity may finally be starting to make a difference, according to US law giant BakerHostetler.... [...]
