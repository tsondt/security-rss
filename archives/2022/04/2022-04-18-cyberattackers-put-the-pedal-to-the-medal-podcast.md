Title: Cyberattackers Put the Pedal to the Medal: Podcast
Date: 2022-04-18T13:00:30+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Malware;Podcasts;Sponsored;Vulnerabilities;Web Security
Slug: 2022-04-18-cyberattackers-put-the-pedal-to-the-medal-podcast

[Source](https://threatpost.com/cyberattackers-speed-fortinet-podcast/179294/){:target="_blank" rel="noopener"}

> Fortinet's Derek Manky discusses the exponential increase in the speed that attackers weaponize fresh vulnerabilities, where botnets and offensive automation fit in, and the ramifications for security teams. [...]
