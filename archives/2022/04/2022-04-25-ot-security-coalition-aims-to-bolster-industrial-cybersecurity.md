Title: OT security coalition aims to bolster industrial cybersecurity
Date: 2022-04-25T13:21:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-25-ot-security-coalition-aims-to-bolster-industrial-cybersecurity

[Source](https://portswigger.net/daily-swig/ot-security-coalition-aims-to-bolster-industrial-cybersecurity){:target="_blank" rel="noopener"}

> Operational Technology Cybersecurity Coalition to tackle infrastructure security issues [...]
