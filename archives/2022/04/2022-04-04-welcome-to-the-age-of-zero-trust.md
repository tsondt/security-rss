Title: Welcome to the Age of Zero Trust
Date: 2022-04-04T17:15:06+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2022-04-04-welcome-to-the-age-of-zero-trust

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/04/darktrace_zero_trust/){:target="_blank" rel="noopener"}

> Rules-based systems don't work, says Darktrace Paid Feature What's highly valuable, often sensitive, and possibly dangerous when it trickles out of your business into the wrong hands? That's right – it's your workforce. Companies are struggling to retain employees and hire new ones as workers seek new opportunities elsewhere. That causes headaches for more than just the HR department. It should be keeping IT security teams awake at night too.... [...]
