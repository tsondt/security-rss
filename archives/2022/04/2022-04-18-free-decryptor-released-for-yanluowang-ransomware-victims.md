Title: Free decryptor released for Yanluowang ransomware victims
Date: 2022-04-18T17:00:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-18-free-decryptor-released-for-yanluowang-ransomware-victims

[Source](https://www.bleepingcomputer.com/news/security/free-decryptor-released-for-yanluowang-ransomware-victims/){:target="_blank" rel="noopener"}

> Kaspersky today revealed it found a vulnerability in Yanluowang ransomware's encryption algorithm, which makes it possible to recover files it encrypts. [...]
