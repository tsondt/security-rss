Title: Apple paid out $36,000 bug bounty for HTTP request smuggling flaws on core web apps – research
Date: 2022-04-07T12:54:48+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-07-apple-paid-out-36000-bug-bounty-for-http-request-smuggling-flaws-on-core-web-apps-research

[Source](https://portswigger.net/daily-swig/apple-paid-out-36-000-bug-bounty-for-http-request-smuggling-flaws-on-core-web-apps-research){:target="_blank" rel="noopener"}

> Queue poisoning attacks allegedly put accounts at risk of takeover [...]
