Title: Amazon Web Services fixes container escape in Log4Shell hotfix
Date: 2022-04-20T04:45:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-20-amazon-web-services-fixes-container-escape-in-log4shell-hotfix

[Source](https://www.bleepingcomputer.com/news/security/amazon-web-services-fixes-container-escape-in-log4shell-hotfix/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has fixed four security issues in its hot patch from December that addressed the critical Log4Shell vulnerability (CVE-2021-44228) affecting cloud or on-premise environments running Java applications with a vulnerable version of the Log4j logging library or containers. [...]
