Title: Quantum ransomware seen deployed in rapid network attacks
Date: 2022-04-25T08:03:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-25-quantum-ransomware-seen-deployed-in-rapid-network-attacks

[Source](https://www.bleepingcomputer.com/news/security/quantum-ransomware-seen-deployed-in-rapid-network-attacks/){:target="_blank" rel="noopener"}

> The Quantum ransomware, a strain first discovered in August 2021, were seen carrying out speedy attacks that escalate quickly, leaving defenders little time to react. [...]
