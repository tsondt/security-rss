Title: Conti’s Ransomware Toll on the Healthcare Industry
Date: 2022-04-18T20:41:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Ransomware;Conti;Emotet;Emsisoft;Errol Weiss;fbi;H-ISAC;Health Information Sharing & Analysis Center;Healthcare Information and Management Systems Society;microsoft;proofpoint;Ryuk;sophos;U.S. Cybersecurity & Infrastructure Security Agency;Zloader
Slug: 2022-04-18-contis-ransomware-toll-on-the-healthcare-industry

[Source](https://krebsonsecurity.com/2022/04/contis-ransomware-toll-on-the-healthcare-industry/){:target="_blank" rel="noopener"}

> Conti — one of the most ruthless and successful Russian ransomware groups — publicly declared during the height of the COVID-19 pandemic that it would refrain from targeting healthcare providers. But new information confirms this pledge was always a lie, and that Conti has launched more than 200 attacks against hospitals and other healthcare facilities since first surfacing in 2018 under its earlier name, “ Ryuk.” On April 13, Microsoft said it executed a legal sneak attack against Zloader, a remote access trojan and malware platform that multiple ransomware groups have used to deploy their malware inside victim networks. More [...]
