Title: Authorities seize Hydra servers in bust against darknet cybercrime marketplace
Date: 2022-04-06T14:44:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-06-authorities-seize-hydra-servers-in-bust-against-darknet-cybercrime-marketplace

[Source](https://portswigger.net/daily-swig/authorities-seize-hydra-servers-in-bust-against-darknet-cybercrime-marketplace){:target="_blank" rel="noopener"}

> Wretched hive of villainy shut down [...]
