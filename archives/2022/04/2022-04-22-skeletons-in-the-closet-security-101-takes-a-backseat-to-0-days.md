Title: Skeletons in the Closet: Security 101 Takes a Backseat to 0-days
Date: 2022-04-22T10:56:16+00:00
Author: Nate Warfield
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-04-22-skeletons-in-the-closet-security-101-takes-a-backseat-to-0-days

[Source](https://threatpost.com/security-101-takes-a-backseat-to-0-days/179374/){:target="_blank" rel="noopener"}

> Nate Warfield, CTO at Prevailion, discusses the dangers of focusing on zero-day security vulnerabilities, and how security teams are being distracted from the day-to-day work that prevents most breaches. [...]
