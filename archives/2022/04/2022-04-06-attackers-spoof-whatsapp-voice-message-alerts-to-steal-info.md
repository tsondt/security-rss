Title: Attackers Spoof WhatsApp Voice-Message Alerts to Steal Info
Date: 2022-04-06T12:37:47+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Mobile Security
Slug: 2022-04-06-attackers-spoof-whatsapp-voice-message-alerts-to-steal-info

[Source](https://threatpost.com/attackers-whatsapp-voice-message/179244/){:target="_blank" rel="noopener"}

> Threat actors target Office 365 and Google Workspace in a new campaign, which uses a legitimate domain associated with a road-safety center in Moscow to send messages. [...]
