Title: Cyberattacks Rage in Ukraine, Support Military Operations
Date: 2022-04-28T12:46:12+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks
Slug: 2022-04-28-cyberattacks-rage-in-ukraine-support-military-operations

[Source](https://threatpost.com/cyberwar-ukraine-military/179421/){:target="_blank" rel="noopener"}

> At least five APTs are believed involved with attacks tied ground campaigns and designed to damage Ukraine's digital infrastructure. [...]
