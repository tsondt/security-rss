Title: US govt grants academics $12M to develop cyberattack defense tools
Date: 2022-04-22T12:33:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-22-us-govt-grants-academics-12m-to-develop-cyberattack-defense-tools

[Source](https://www.bleepingcomputer.com/news/security/us-govt-grants-academics-12m-to-develop-cyberattack-defense-tools/){:target="_blank" rel="noopener"}

> The US Department of Energy (DOE) has announced that it will provide $12 million in funding to six university teams to develop defense and mitigation tools to protect US energy delivery systems from cyberattacks. [...]
