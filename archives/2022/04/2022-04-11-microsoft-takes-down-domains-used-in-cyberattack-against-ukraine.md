Title: Microsoft Takes Down Domains Used in Cyberattack Against Ukraine
Date: 2022-04-11T17:26:25+00:00
Author: Threatpost
Category: Threatpost
Tags: Malware
Slug: 2022-04-11-microsoft-takes-down-domains-used-in-cyberattack-against-ukraine

[Source](https://threatpost.com/microsoft-takedown-domains-ukraine/179257/){:target="_blank" rel="noopener"}

> The APT28 (Advanced persistence threat) is operating since 2009, this group has worked under different names such as Sofacy, Sednit, Strontium Storm, Fancy Bear, Iron Twilight, and Pawn. [...]
