Title: US warns North Korean Lazarus gang rises against cryptocurrency outfits
Date: 2022-04-20T10:14:04+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-20-us-warns-north-korean-lazarus-gang-rises-against-cryptocurrency-outfits

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/20/lazarus-targets-digital-assets/){:target="_blank" rel="noopener"}

> Malware-laced recruitment emails are more Kim job ill than Kim Jong-un The North Korean-based criminal group Lazarus is expanding its attacks into the blockchain and crypto space, three agencies of the US government have warned.... [...]
