Title: Video Conferencing Apps Sometimes Ignore the Mute Button
Date: 2022-04-29T14:18:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;privacy;videoconferencing
Slug: 2022-04-29-video-conferencing-apps-sometimes-ignore-the-mute-button

[Source](https://www.schneier.com/blog/archives/2022/04/video-conferencing-apps-sometimes-ignore-the-mute-button.html){:target="_blank" rel="noopener"}

> New research: “ Are You Really Muted?: A Privacy Analysis of Mute Buttons in Video Conferencing Apps “: Abstract: In the post-pandemic era, video conferencing apps (VCAs) have converted previously private spaces — bedrooms, living rooms, and kitchens — into semi-public extensions of the office. And for the most part, users have accepted these apps in their personal space, without much thought about the permission models that govern the use of their personal data during meetings. While access to a device’s video camera is carefully controlled, little has been done to ensure the same level of privacy for accessing the [...]
