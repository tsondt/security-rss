Title: North Korea's Lazarus cyber-gang caught 'spying' on chemical sector companies
Date: 2022-04-15T02:30:24+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-15-north-koreas-lazarus-cyber-gang-caught-spying-on-chemical-sector-companies

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/15/lazarus_chemical_korea/){:target="_blank" rel="noopener"}

> Crypto-coin theft isn't enough to keep these miscreants busy North Korea's Lazarus cybercrime gang is now breaking into chemical sector companies' networks to spy on them, according to Symantec's threat intel team.... [...]
