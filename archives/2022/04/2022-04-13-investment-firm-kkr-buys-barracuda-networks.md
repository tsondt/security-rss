Title: Investment firm KKR buys Barracuda Networks
Date: 2022-04-13T05:43:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-04-13-investment-firm-kkr-buys-barracuda-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/13/kkr_buys_barracuda/){:target="_blank" rel="noopener"}

> Plans to take a bigger bite of the SME security market by swimming towards SASE Investment firm KKR has acquired Barracuda Networks from private equity firm Thoma Bravo.... [...]
