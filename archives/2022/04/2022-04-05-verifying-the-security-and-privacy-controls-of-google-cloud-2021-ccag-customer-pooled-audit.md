Title: Verifying the security and privacy controls of Google Cloud: 2021 CCAG customer pooled audit
Date: 2022-04-05T16:00:00+00:00
Author: Rani Urbas
Category: GCP Security
Tags: Compliance;Google Cloud in Europe;Identity & Security
Slug: 2022-04-05-verifying-the-security-and-privacy-controls-of-google-cloud-2021-ccag-customer-pooled-audit

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-completed-the-ccag-pooled-audit-for-2021/){:target="_blank" rel="noopener"}

> Earning the role as our customers’ most trusted cloud requires commitment to ongoing transparency, collaboration and assurance. Our products regularly undergo independent verification, achieving certifications or attestations of compliance against global regulatory requirements, frameworks, and guidelines. At Google Cloud we work closely with our customers, their regulators, and appointed independent auditors who want to verify the security and privacy of our platform. One example of how the Google Cybersecurity Action Team supports customers’ risk management efforts is our annual audit with the Collaborative Cloud Audit Group (CCAG). In 2020, faced with the global COVID-19 pandemic and the demands for teleworking, [...]
