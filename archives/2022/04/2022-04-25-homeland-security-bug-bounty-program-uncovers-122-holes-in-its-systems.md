Title: Homeland Security bug bounty program uncovers 122 holes in its systems
Date: 2022-04-25T19:55:34+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-25-homeland-security-bug-bounty-program-uncovers-122-holes-in-its-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/25/dhs_bug_bounty/){:target="_blank" rel="noopener"}

> Thinking of another word for this US govt department's name The first bug bounty program by America's Homeland Security has led to the discovery and disclosure of 122 vulnerabilities, 27 of which were deemed critical.... [...]
