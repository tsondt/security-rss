Title: Windows 10 KB5012636 cumulative update fixes freezing issues
Date: 2022-04-22T09:06:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-22-windows-10-kb5012636-cumulative-update-fixes-freezing-issues

[Source](https://www.bleepingcomputer.com/news/security/windows-10-kb5012636-cumulative-update-fixes-freezing-issues/){:target="_blank" rel="noopener"}

> Microsoft has released the optional KB5012636 cumulative update preview for Windows 10 1809 and Windows Server 2019, with fixes for system freezing issues affecting client and server systems. [...]
