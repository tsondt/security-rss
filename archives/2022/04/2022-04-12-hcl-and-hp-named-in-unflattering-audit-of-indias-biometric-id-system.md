Title: HCL and HP named in unflattering audit of India’s biometric ID system
Date: 2022-04-12T06:57:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-04-12-hcl-and-hp-named-in-unflattering-audit-of-indias-biometric-id-system

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/12/aadhaar_uadai_audit/){:target="_blank" rel="noopener"}

> Same biometric used for different people, no archives, lousy infosec among the issues India’s Comptroller and Auditor General has published a performance audit of the nation’s Unique Identification Authority and found big IT problems – some attributable to Indian services giant HCL and to HP, but others due to poor government decisions.... [...]
