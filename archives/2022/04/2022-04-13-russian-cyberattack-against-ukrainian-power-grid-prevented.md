Title: Russian Cyberattack against Ukrainian Power Grid Prevented
Date: 2022-04-13T11:32:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cyberwar;cyberweapons;malware;Russia;Ukraine
Slug: 2022-04-13-russian-cyberattack-against-ukrainian-power-grid-prevented

[Source](https://www.schneier.com/blog/archives/2022/04/russian-cyberattack-against-ukrainian-power-grid-prevented.html){:target="_blank" rel="noopener"}

> A Russian cyberweapon, similar to the one used in 2016, was detected and removed before it could be used. Key points: ESET researchers collaborated with CERT-UA to analyze the attack against the Ukrainian energy company The destructive actions were scheduled for 2022-04-08 but artifacts suggest that the attack had been planned for at least two weeks The attack used ICS-capable malware and regular disk wipers for Windows, Linux and Solaris operating systems We assess with high confidence that the attackers used a new version of the Industroyer malware, which was used in 2016 to cut power in Ukraine We assess [...]
