Title: GitHub: Attacker breached dozens of orgs using stolen OAuth tokens
Date: 2022-04-15T19:09:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-15-github-attacker-breached-dozens-of-orgs-using-stolen-oauth-tokens

[Source](https://www.bleepingcomputer.com/news/security/github-attacker-breached-dozens-of-orgs-using-stolen-oauth-tokens/){:target="_blank" rel="noopener"}

> GitHub revealed today that an attacker is using stolen OAuth user tokens (issued to Heroku and Travis-CI) to download data from private repositories. [...]
