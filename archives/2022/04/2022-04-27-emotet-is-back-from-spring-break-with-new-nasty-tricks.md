Title: Emotet is Back From ‘Spring Break’ With New Nasty Tricks
Date: 2022-04-27T19:53:37+00:00
Author: Threatpost
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2022-04-27-emotet-is-back-from-spring-break-with-new-nasty-tricks

[Source](https://threatpost.com/emotet-back-new-tricks/179410/){:target="_blank" rel="noopener"}

> The Botnet appears to use a new delivery method for compromising Windows systems after Microsoft disables VBA macros by default. [...]
