Title: Friday Squid Blogging: Squid Migration and Climate Change
Date: 2022-04-01T21:06:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2022-04-01-friday-squid-blogging-squid-migration-and-climate-change

[Source](https://www.schneier.com/blog/archives/2022/04/friday-squid-blogging-squid-migration-and-climate-change.html){:target="_blank" rel="noopener"}

> New research on the changing migration of the Doryteuthis opalescens as a result of climate change. News article : Stanford researchers have solved a mystery about why a species of squid native to California has been found thriving in the Gulf of Alaska about 1,800 miles north of its expected range: climate change. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
