Title: Hive ransomware affiliate zeros in on Exchange servers
Date: 2022-04-22T16:00:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-22-hive-ransomware-affiliate-zeros-in-on-exchange-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/22/hive_ransomware_microsoft_exchange/){:target="_blank" rel="noopener"}

> Threat actor exploited known vulnerabilities in the Microsoft software to compromise multiple systems An affiliate of the aggressive Hive ransomware group is exploiting known vulnerabilities in Microsoft Exchange servers to encrypt and exfiltrate data and threaten to publicly disclose the information if the ransom isn't paid.... [...]
