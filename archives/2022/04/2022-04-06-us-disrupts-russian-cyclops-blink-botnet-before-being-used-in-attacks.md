Title: US disrupts Russian Cyclops Blink botnet before being used in attacks
Date: 2022-04-06T11:46:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-06-us-disrupts-russian-cyclops-blink-botnet-before-being-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-disrupts-russian-cyclops-blink-botnet-before-being-used-in-attacks/){:target="_blank" rel="noopener"}

> US government officials announced today the disruption of the Cyclops Blink botnet controlled by the Russian-backed Sandworm hacking group before being used in attacks. [...]
