Title: The More You Know, The More You Know You Don’t Know
Date: 2022-04-19T09:06:00-07:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2022-04-19-the-more-you-know-the-more-you-know-you-dont-know

[Source](https://googleprojectzero.blogspot.com/2022/04/the-more-you-know-more-you-know-you.html){:target="_blank" rel="noopener"}

> A Year in Review of 0-days Used In-the-Wild in 2021 Posted by Maddie Stone, Google Project Zero This is our third annual year in review of 0-days exploited in-the-wild [ 2020, 2019 ]. Each year we’ve looked back at all of the detected and disclosed in-the-wild 0-days as a group and synthesized what we think the trends and takeaways are. The goal of this report is not to detail each individual exploit, but instead to analyze the exploits from the year as a group, looking for trends, gaps, lessons learned, successes, etc. If you’re interested in the analysis of individual [...]
