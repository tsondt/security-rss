Title: Russian govt impersonators target telcos in phishing attacks
Date: 2022-04-27T11:32:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-27-russian-govt-impersonators-target-telcos-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/russian-govt-impersonators-target-telcos-in-phishing-attacks/){:target="_blank" rel="noopener"}

> A previously unknown and financially motivated hacking group is impersonating a Russian agency in a phishing campaign targeting entities in Eastern European countries. [...]
