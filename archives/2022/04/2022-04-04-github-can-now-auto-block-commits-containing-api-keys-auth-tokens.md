Title: GitHub can now auto-block commits containing API keys, auth tokens
Date: 2022-04-04T15:32:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-04-github-can-now-auto-block-commits-containing-api-keys-auth-tokens

[Source](https://www.bleepingcomputer.com/news/security/github-can-now-auto-block-commits-containing-api-keys-auth-tokens/){:target="_blank" rel="noopener"}

> GitHub announced on Monday that it expanded its code hosting platform's secrets scanning capabilities for GitHub Advanced Security customers to automatically block secret leaks. [...]
