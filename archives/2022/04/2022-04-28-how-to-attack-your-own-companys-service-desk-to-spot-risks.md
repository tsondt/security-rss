Title: How to Attack Your Own Company's Service Desk to spot risks
Date: 2022-04-28T10:01:02-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-04-28-how-to-attack-your-own-companys-service-desk-to-spot-risks

[Source](https://www.bleepingcomputer.com/news/security/how-to-attack-your-own-companys-service-desk-to-spot-risks/){:target="_blank" rel="noopener"}

> Specops Secure Service Desk is an excellent tool for keeping a help desk safe from social engineering attacks. Although Specops Secure Service Desk offers numerous features, there are three capabilities that are especially useful for thwarting social engineering attacks. [...]
