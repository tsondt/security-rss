Title: DDoS attacks at an all-time-high in Q1 2022, says Kaspersky
Date: 2022-04-26T14:30:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-04-26-ddos-attacks-at-an-all-time-high-in-q1-2022-says-kaspersky

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/26/kaspersky_ddos_up/){:target="_blank" rel="noopener"}

> More attacks and more targeted attacks than ever before. What could have happened to cause that uptick? Kaspersky has released a report showing Distributed Denial of Service (DDoS) attacks hit an all-time-high in the first quarter of 2022.... [...]
