Title: Raspberry Pi removes default user to hinder brute-force attacks
Date: 2022-04-08T09:00:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-08-raspberry-pi-removes-default-user-to-hinder-brute-force-attacks

[Source](https://www.bleepingcomputer.com/news/security/raspberry-pi-removes-default-user-to-hinder-brute-force-attacks/){:target="_blank" rel="noopener"}

> An update to Raspberry Pi OS Bullseye has removed the default 'pi' user to make it harder for attackers to find and compromise Internet-exposed Raspberry Pi devices using default credentials. [...]
