Title: Interpol: We can't arrest our way out of cybercrime
Date: 2022-04-29T12:15:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-29-interpol-we-cant-arrest-our-way-out-of-cybercrime

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/29/interpol_cybercrime_partnerships/){:target="_blank" rel="noopener"}

> Especially when gangs are better funded than local police As cybercriminals become more sophisticated and their attacks more destructive and costly, private security firms and law enforcement need to work together, according to Interpol's Doug Witschi.... [...]
