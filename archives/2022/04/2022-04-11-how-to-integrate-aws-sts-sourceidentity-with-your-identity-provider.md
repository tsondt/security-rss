Title: How to integrate AWS STS SourceIdentity with your identity provider
Date: 2022-04-11T20:19:22+00:00
Author: Keith Joelner
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;AWS STS;Federation;Identity;Security Blog;Security token service;Sessions;Tracing
Slug: 2022-04-11-how-to-integrate-aws-sts-sourceidentity-with-your-identity-provider

[Source](https://aws.amazon.com/blogs/security/how-to-integrate-aws-sts-sourceidentity-with-your-identity-provider/){:target="_blank" rel="noopener"}

> You can use third-party identity providers (IdPs) such as Okta, Ping, or OneLogin to federate with the AWS Identity and Access Management (IAM) service using SAML 2.0, allowing your workforce to configure services by providing authorization access to the AWS Management Console or Command Line Interface (CLI). When you federate to AWS, you assume a [...]
