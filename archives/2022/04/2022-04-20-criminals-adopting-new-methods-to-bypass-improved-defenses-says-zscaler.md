Title: Criminals adopting new methods to bypass improved defenses, says Zscaler
Date: 2022-04-20T12:15:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-20-criminals-adopting-new-methods-to-bypass-improved-defenses-says-zscaler

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/20/phishing-attempts-on-rise-zscaler/){:target="_blank" rel="noopener"}

> PhaaS, SMiShing, and remote work drive increase in phishing attacks The number of phishing attacks worldwide jumped 29 percent last year as threat actors countered stronger enterprise defenses with newer methods, according to researchers with Zscaler's ThreatLabz research team.... [...]
