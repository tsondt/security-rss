Title: Number of publicly exposed database instances hits new record
Date: 2022-04-27T04:29:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;Software
Slug: 2022-04-27-number-of-publicly-exposed-database-instances-hits-new-record

[Source](https://www.bleepingcomputer.com/news/security/number-of-publicly-exposed-database-instances-hits-new-record/){:target="_blank" rel="noopener"}

> Security researchers have noticed an increase in the number of databases publicly exposed to the Internet, with 308,000 identified in 2021. The growth continued quarter over quarter, peaking in the first months of this year. [...]
