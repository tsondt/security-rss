Title: FIN7 crime-gang pen tester headed to US prison for five years
Date: 2022-04-07T23:06:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-07-fin7-crime-gang-pen-tester-headed-to-us-prison-for-five-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/07/fin7_pen_tester/){:target="_blank" rel="noopener"}

> He's the third gangster in crew to face time behind bars Another member of notorious cybercrime ring FIN7 is headed to jail after the gang breached major companies' networks across the US and stole more than $1 billion from these businesses' customers.... [...]
