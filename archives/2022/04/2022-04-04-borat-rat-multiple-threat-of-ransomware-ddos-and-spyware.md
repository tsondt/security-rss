Title: Borat RAT: Multiple threat of ransomware, DDoS and spyware
Date: 2022-04-04T16:30:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-04-borat-rat-multiple-threat-of-ransomware-ddos-and-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/04/borat-rat-ransomware-ddos/){:target="_blank" rel="noopener"}

> Thought Sacha Baron Cohen was a terrible threat actor? Get a load of this: encrypts/steals data, records audio/video and controls keyboard A new remote access trojan (RAT) dubbed "Borat" doesn't come with many laughs but offers bad actors a menu of cyberthreats to choose from.... [...]
