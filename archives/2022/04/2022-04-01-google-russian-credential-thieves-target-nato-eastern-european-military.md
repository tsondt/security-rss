Title: Google: Russian credential thieves target NATO, Eastern European military
Date: 2022-04-01T10:20:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-01-google-russian-credential-thieves-target-nato-eastern-european-military

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/01/russian_credential_phishing/){:target="_blank" rel="noopener"}

> Also: Belarusian miscreants pivot to browser-in-the-browser attacks A Russian cybercrime gang has lately sent credential-phishing emails to the military of Eastern European countries and a NATO Center of Excellence, according to a Google threat report this week.... [...]
