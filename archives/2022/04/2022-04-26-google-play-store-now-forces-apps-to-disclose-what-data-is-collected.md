Title: Google Play Store now forces apps to disclose what data is collected
Date: 2022-04-26T11:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-04-26-google-play-store-now-forces-apps-to-disclose-what-data-is-collected

[Source](https://www.bleepingcomputer.com/news/security/google-play-store-now-forces-apps-to-disclose-what-data-is-collected/){:target="_blank" rel="noopener"}

> Google is rolling out a new Data Safety section on the Play Store, Android's official app repository, where developers must declare what data their software collects from users of their apps. [...]
