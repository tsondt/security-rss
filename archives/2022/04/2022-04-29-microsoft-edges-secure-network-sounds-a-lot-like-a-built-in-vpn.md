Title: Microsoft Edge's 'Secure Network' sounds a lot like a built-in VPN
Date: 2022-04-29T16:00:04+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-04-29-microsoft-edges-secure-network-sounds-a-lot-like-a-built-in-vpn

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/29/edge_vpn/){:target="_blank" rel="noopener"}

> Only works with signed-in users, but could lure more into using the browser Microsoft appears to be planning a VPN-like solution for its Edge browser judging by a support page for the upcoming feature.... [...]
