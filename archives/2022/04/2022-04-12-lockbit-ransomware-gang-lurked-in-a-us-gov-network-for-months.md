Title: LockBit ransomware gang lurked in a U.S. gov network for months
Date: 2022-04-12T10:15:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-12-lockbit-ransomware-gang-lurked-in-a-us-gov-network-for-months

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-gang-lurked-in-a-us-gov-network-for-months/){:target="_blank" rel="noopener"}

> Threat analysts have found evidence of malicious actors using the LockBit ransomware strain lingering in the network of a regional U.S. government agency for at least five months. [...]
