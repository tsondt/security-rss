Title: Can we solve the zero-day threat once and for all? No, but here’s what we can do
Date: 2022-04-12T17:15:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-04-12-can-we-solve-the-zero-day-threat-once-and-for-all-no-but-heres-what-we-can-do

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/12/can_we_solve_the_zero/){:target="_blank" rel="noopener"}

> This online session shows you what constant vigilance should look like Webinar Last December’s Log4j crisis brought the danger of zero day vulnerabilities to the front pages. But while one key flaw has been put under the microscope, does that mean the problem is over?... [...]
