Title: Hack Me, I’m Famous: Bug bounty hackathon nets security researcher €10,000 overnight
Date: 2022-04-21T14:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-21-hack-me-im-famous-bug-bounty-hackathon-nets-security-researcher-10000-overnight

[Source](https://portswigger.net/daily-swig/hack-me-im-famous-bug-bounty-hackathon-nets-security-researcher-10-000-overnight){:target="_blank" rel="noopener"}

> European event saw 40 researchers team up to find bugs [...]
