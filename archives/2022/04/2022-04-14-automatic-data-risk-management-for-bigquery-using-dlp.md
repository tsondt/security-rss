Title: Automatic data risk management for BigQuery using DLP
Date: 2022-04-14T16:00:00+00:00
Author: Scott Ellis
Category: GCP Security
Tags: Databases;Data Analytics;Compliance;Cloud Migration;Identity & Security
Slug: 2022-04-14-automatic-data-risk-management-for-bigquery-using-dlp

[Source](https://cloud.google.com/blog/products/identity-security/google-launches-automatic-dlp-for-bigquery/){:target="_blank" rel="noopener"}

> Protecting sensitive data and preventing unintended data exposure is critical for businesses. However, many organizations lack the tools to stay on top of where sensitive data resides across their enterprise. It’s particularly concerning when sensitive data shows up in unexpected places – for example, in logs that services generate, when customers inadvertently send it in a customer support chat, or when managing unstructured analytical workloads. This is where Automatic Data Loss Prevention (DLP) for BigQuery can help. Data discovery and classification is often implemented as a manual, on-demand process, and as a result happens less frequently than many organizations would [...]
