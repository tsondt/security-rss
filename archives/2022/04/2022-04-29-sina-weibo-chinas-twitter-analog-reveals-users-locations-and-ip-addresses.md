Title: Sina Weibo, China's Twitter analog, reveals users' locations and IP addresses
Date: 2022-04-29T08:02:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-04-29-sina-weibo-chinas-twitter-analog-reveals-users-locations-and-ip-addresses

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/29/weibo_location_services_default/){:target="_blank" rel="noopener"}

> Sssshhhh! Nobody tell Elon Musk To the surprise of many users, China's largest Twitter-esque microblogging website, Sina Weibo, announced on Thursday that it will publish users' IP addresses and location data in an effort to keep their content honest and nice.... [...]
