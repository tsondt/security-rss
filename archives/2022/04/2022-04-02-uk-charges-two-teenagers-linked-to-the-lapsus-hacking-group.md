Title: UK charges two teenagers linked to the Lapsus$ hacking group
Date: 2022-04-02T10:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-02-uk-charges-two-teenagers-linked-to-the-lapsus-hacking-group

[Source](https://www.bleepingcomputer.com/news/security/uk-charges-two-teenagers-linked-to-the-lapsus-hacking-group/){:target="_blank" rel="noopener"}

> Two teenagers from the UK charged with helping the Lapsus$ extortion gang have been released on bail after appearing in the Highbury Corner Magistrates Court court on Friday morning. [...]
