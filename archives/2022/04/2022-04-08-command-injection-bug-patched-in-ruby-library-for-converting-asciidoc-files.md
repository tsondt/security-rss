Title: Command injection bug patched in Ruby library for converting AsciiDoc files
Date: 2022-04-08T11:13:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-08-command-injection-bug-patched-in-ruby-library-for-converting-asciidoc-files

[Source](https://portswigger.net/daily-swig/command-injection-bug-patched-in-ruby-library-for-converting-asciidoc-files){:target="_blank" rel="noopener"}

> Ruby server RCE bug gets quashed [...]
