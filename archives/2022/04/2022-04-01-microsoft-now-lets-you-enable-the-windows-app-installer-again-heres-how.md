Title: Microsoft now lets you enable the Windows App Installer again, here's how
Date: 2022-04-01T16:23:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-04-01-microsoft-now-lets-you-enable-the-windows-app-installer-again-heres-how

[Source](https://www.bleepingcomputer.com/news/security/microsoft-now-lets-you-enable-the-windows-app-installer-again-heres-how/){:target="_blank" rel="noopener"}

> Microsoft now allows enterprise admins to re-enable the MSIX ms-appinstaller protocol handler disabled after Emotet abused it to deliver malicious Windows App Installer packages. [...]
