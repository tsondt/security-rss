Title: Security Turbulence in the Cloud: Survey Says…
Date: 2022-04-29T12:33:51+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2022-04-29-security-turbulence-in-the-cloud-survey-says

[Source](https://threatpost.com/security-turbulence-in-the-cloud-survey-says/179437/){:target="_blank" rel="noopener"}

> Exclusive Threatpost research examines organizations’ top cloud security concerns, attitudes towards zero-trust and DevSecOps. [...]
