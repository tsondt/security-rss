Title: USA's plan to decouple its tech with China lacks a strategy – report
Date: 2022-04-26T16:30:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-04-26-usas-plan-to-decouple-its-tech-with-china-lacks-a-strategy-report

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/26/usas_plan_to_decouple_its/){:target="_blank" rel="noopener"}

> The Schmidt hits the fan The USA's policy of decoupling its technology industries from China lacks a strategy, a theory of success, and an understanding of how to achieve its ill-defined goals, according to a new paper by Jon Bateman from the thinktank Carnegie Endowment for International Peace (CEIP).... [...]
