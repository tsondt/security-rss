Title: Finnish govt websites knocked down as Ukraine President addresses MPs
Date: 2022-04-09T01:09:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-09-finnish-govt-websites-knocked-down-as-ukraine-president-addresses-mps

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/09/dos_attacks_finland_russia/){:target="_blank" rel="noopener"}

> Online attacks follow suspected airspace violation by Russian aircraft Cyberattacks took down Finnish government websites on Friday while Ukrainian President Volodymyr Zelenskyy addressed Finland's members of parliament (MPs).... [...]
