Title: Coca-Cola probes pro-Kremlin gang's claims of 161GB data theft
Date: 2022-04-26T18:58:46+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-26-coca-cola-probes-pro-kremlin-gangs-claims-of-161gb-data-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/26/cocacola_ransomware_attack/){:target="_blank" rel="noopener"}

> Life tastes not so good right now Coca-Cola confirmed it's probing a possible network intrusion after the Stormous cybercrime gang claimed it stole 161GB of data from the beverage giant.... [...]
