Title: Undetectable Backdoors in Machine-Learning Models
Date: 2022-04-19T20:12:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-04-19-undetectable-backdoors-in-machine-learning-models

[Source](https://www.schneier.com/blog/archives/2022/04/undetectable-backdoors-in-machine-learning-models.html){:target="_blank" rel="noopener"}

> New paper: “ Planting Undetectable Backdoors in Machine Learning Models : Abstract : Given the computational cost and technical expertise required to train machine learning models, users may delegate the task of learning to a service provider. We show how a malicious learner can plant an undetectable backdoor into a classifier. On the surface, such a backdoored classifier behaves normally, but in reality, the learner maintains a mechanism for changing the classification of any input, with only a slight perturbation. Importantly, without the appropriate “backdoor key”, the mechanism is hidden and cannot be detected by any computationally-bounded observer. We demonstrate [...]
