Title: Git security vulnerabilities prompt updates
Date: 2022-04-13T14:32:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-13-git-security-vulnerabilities-prompt-updates

[Source](https://portswigger.net/daily-swig/git-security-vulnerabilities-prompt-updates){:target="_blank" rel="noopener"}

> Windows users at highest risk from security bugs in software development tool [...]
