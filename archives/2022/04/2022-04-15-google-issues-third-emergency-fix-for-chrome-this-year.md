Title: Google issues third emergency fix for Chrome this year
Date: 2022-04-15T12:49:37+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-15-google-issues-third-emergency-fix-for-chrome-this-year

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/15/google-third-fix-chrome-vulnerability/){:target="_blank" rel="noopener"}

> The latest patch is aimed at a type confusion vulnerability that is actively being exploited Google is issuing fixes for two vulnerabilities in its Chrome web browser, including one flaw that is already being exploited in the wild.... [...]
