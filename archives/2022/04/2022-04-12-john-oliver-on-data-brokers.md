Title: John Oliver on Data Brokers
Date: 2022-04-12T14:25:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data collection;national security policy;privacy;video
Slug: 2022-04-12-john-oliver-on-data-brokers

[Source](https://www.schneier.com/blog/archives/2022/04/john-oliver-on-data-brokers.html){:target="_blank" rel="noopener"}

> John Oliver has an excellent segment on data brokers and surveillance capitalism. [...]
