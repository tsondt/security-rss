Title: New IDC whitepaper released – Trusted Cloud: Overcoming the Tension Between Data Sovereignty and Accelerated Digital Transformation
Date: 2022-04-27T21:26:54+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Cloud Adoption;Foundational (100);Security, Identity, & Compliance;cloud adoption;Data protection;EU Data Protection;GDPR;IDC;Privacy
Slug: 2022-04-27-new-idc-whitepaper-released-trusted-cloud-overcoming-the-tension-between-data-sovereignty-and-accelerated-digital-transformation

[Source](https://aws.amazon.com/blogs/security/new-idc-whitepaper-released-trusted-cloud-overcoming-the-tension-between-data-sovereignty-and-accelerated-digital-transformation/){:target="_blank" rel="noopener"}

> A new International Data Corporation (IDC) whitepaper sponsored by AWS, Trusted Cloud: Overcoming the Tension Between Data Sovereignty and Accelerated Digital Transformation, examines the importance of the cloud in building the future of digital EU organizations. IDC predicts that 70% of CEOs of large European organizations will be incentivized to generate at least 40% of [...]
