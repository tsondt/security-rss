Title: Facebook's Meta, tracking code, and the student financial aid website
Date: 2022-04-30T11:00:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-04-30-facebooks-meta-tracking-code-and-the-student-financial-aid-website

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/30/meta_student_data/){:target="_blank" rel="noopener"}

> Also: Occulus virtual reality apps fail to detail info collection Meta's Facebook subsidiary has been collecting hashed personal data from students seeking US government financial aid, even from those without a Facebook account and those not logged into the student aid website, according to a research study published this week.... [...]
