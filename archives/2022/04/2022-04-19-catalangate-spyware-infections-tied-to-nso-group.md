Title: ‘CatalanGate’ Spyware Infections Tied to NSO Group
Date: 2022-04-19T16:04:33+00:00
Author: Threatpost
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2022-04-19-catalangate-spyware-infections-tied-to-nso-group

[Source](https://threatpost.com/catalangate-spyware/179336/){:target="_blank" rel="noopener"}

> Citizen Lab uncovers multi-year campaign targeting autonomous region of Spain, called Catalonia. [...]
