Title: Hackers Using Fake Police Data Requests against Tech Companies
Date: 2022-04-05T11:04:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;extortion;forgery;hacking;police;scams
Slug: 2022-04-05-hackers-using-fake-police-data-requests-against-tech-companies

[Source](https://www.schneier.com/blog/archives/2022/04/hackers-using-fake-police-data-requests-against-tech-companies.html){:target="_blank" rel="noopener"}

> Brian Krebs has a detailed post about hackers using fake police data requests to trick companies into handing over data. Virtually all major technology companies serving large numbers of users online have departments that routinely review and process such requests, which are typically granted as long as the proper documents are provided and the request appears to come from an email address connected to an actual police department domain name. But in certain circumstances ­– such as a case involving imminent harm or death –­ an investigating authority may make what’s known as an Emergency Data Request (EDR), which largely [...]
