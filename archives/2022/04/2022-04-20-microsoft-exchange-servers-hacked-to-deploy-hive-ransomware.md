Title: Microsoft Exchange servers hacked to deploy Hive ransomware
Date: 2022-04-20T17:03:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-20-microsoft-exchange-servers-hacked-to-deploy-hive-ransomware

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-servers-hacked-to-deploy-hive-ransomware/){:target="_blank" rel="noopener"}

> A Hive ransomware affiliate has been targeting Microsoft Exchange servers vulnerable to ProxyShell security issues to deploy various backdoors, including Cobalt Strike beacon. [...]
