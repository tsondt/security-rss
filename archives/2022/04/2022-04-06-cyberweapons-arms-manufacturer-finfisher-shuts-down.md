Title: Cyberweapons Arms Manufacturer FinFisher Shuts Down
Date: 2022-04-06T14:38:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberweapons;privacy;spyware;surveillance
Slug: 2022-04-06-cyberweapons-arms-manufacturer-finfisher-shuts-down

[Source](https://www.schneier.com/blog/archives/2022/04/cyberweapons-arms-manufacturer-finfisher-shuts-down.html){:target="_blank" rel="noopener"}

> FinFisher has shut down operations. This is the spyware company whose products were used, among other things, to spy on Turkish and Bahraini political opposition. [...]
