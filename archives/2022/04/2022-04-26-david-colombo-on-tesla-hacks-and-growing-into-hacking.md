Title: David Colombo on Tesla Hacks and Growing into Hacking
Date: 2022-04-26T10:00:00-04:00
Author: Sponsored by Cybellum
Category: BleepingComputer
Tags: Security
Slug: 2022-04-26-david-colombo-on-tesla-hacks-and-growing-into-hacking

[Source](https://www.bleepingcomputer.com/news/security/david-colombo-on-tesla-hacks-and-growing-into-hacking/){:target="_blank" rel="noopener"}

> Cybellum interviewed David Colombo, the cyber boy wonder of Germany, and founder of Colombo Technologies for our podcast, Left to Our Own Devices. Not yet 20 years old, the prolific cyber researcher already has to his credit the exposure of numerous critical vulnerabilities, including the honor of hacking his way into Tesla vehicles. [...]
