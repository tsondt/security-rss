Title: Fake Windows 10 updates infect you with Magniber ransomware
Date: 2022-04-30T10:18:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-30-fake-windows-10-updates-infect-you-with-magniber-ransomware

[Source](https://www.bleepingcomputer.com/news/security/fake-windows-10-updates-infect-you-with-magniber-ransomware/){:target="_blank" rel="noopener"}

> Fake Windows 10 updates on crack sites are being used to distribute the Magniber ransomware in a massive campaign that started earlier this month. [...]
