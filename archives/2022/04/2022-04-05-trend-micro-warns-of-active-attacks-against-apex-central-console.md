Title: Trend Micro warns of active attacks against Apex Central console
Date: 2022-04-05T14:32:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-05-trend-micro-warns-of-active-attacks-against-apex-central-console

[Source](https://portswigger.net/daily-swig/trend-micro-warns-of-active-attacks-against-apex-central-console){:target="_blank" rel="noopener"}

> Scramble to patch security dashboard [...]
