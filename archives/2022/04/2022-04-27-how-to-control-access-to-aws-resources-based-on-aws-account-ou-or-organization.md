Title: How to control access to AWS resources based on AWS account, OU, or organization
Date: 2022-04-27T17:54:12+00:00
Author: Rishi Mehrotra
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Uncategorized;AWS Organizations;IAM Condition Keys;IAM policies;Identity and Access Management;New Launch;Security
Slug: 2022-04-27-how-to-control-access-to-aws-resources-based-on-aws-account-ou-or-organization

[Source](https://aws.amazon.com/blogs/security/how-to-control-access-to-aws-resources-based-on-aws-account-ou-or-organization/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) recently launched new condition keys to make it simpler to control access to your resources along your Amazon Web Services (AWS) organizational boundaries. AWS recommends that you set up multiple accounts as your workloads grow, and you can use multiple AWS accounts to isolate workloads or applications that have [...]
