Title: Amazon Cognito launches support for in-Region integration with Amazon SES and Amazon SNS
Date: 2022-04-12T20:07:27+00:00
Author: Amit Jha
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Cognito;Customer Identity;Regions;SNS
Slug: 2022-04-12-amazon-cognito-launches-support-for-in-region-integration-with-amazon-ses-and-amazon-sns

[Source](https://aws.amazon.com/blogs/security/amazon-cognito-launches-support-for-in-region-integration-with-amazon-ses-and-amazon-sns/){:target="_blank" rel="noopener"}

> We are pleased to announce that in all AWS Regions that support Amazon Cognito, you can now integrate Amazon Cognito with Amazon Simple Email Service (Amazon SES) and Amazon Simple Notification Service (Amazon SNS) in the same Region. By integrating these services in the same Region, you can more easily achieve lower latency, and remove [...]
