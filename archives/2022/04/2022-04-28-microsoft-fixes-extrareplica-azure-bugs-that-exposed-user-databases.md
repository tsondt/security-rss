Title: Microsoft fixes ExtraReplica Azure bugs that exposed user databases
Date: 2022-04-28T13:34:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-04-28-microsoft-fixes-extrareplica-azure-bugs-that-exposed-user-databases

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-extrareplica-azure-bugs-that-exposed-user-databases/){:target="_blank" rel="noopener"}

> Microsoft has addressed a chain of critical vulnerabilities found in the Azure Database for PostgreSQL Flexible Server that could let malicious users escalate privileges and gain access to other customers' databases after bypassing authentication. [...]
