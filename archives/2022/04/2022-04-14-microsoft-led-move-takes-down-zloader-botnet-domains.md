Title: Microsoft-led move takes down ZLoader botnet domains
Date: 2022-04-14T19:45:26+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-14-microsoft-led-move-takes-down-zloader-botnet-domains

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/14/microsoftled_zloader_botnet/){:target="_blank" rel="noopener"}

> That should keep the criminals offline for, well, weeks probably Microsoft has announced a months-long effort to take control of 65 domains that the ZLoader criminal botnet gang has been using as command-and-control servers.... [...]
