Title: FBI links largest crypto hack ever to North Korean hackers
Date: 2022-04-14T13:40:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-04-14-fbi-links-largest-crypto-hack-ever-to-north-korean-hackers

[Source](https://www.bleepingcomputer.com/news/security/fbi-links-largest-crypto-hack-ever-to-north-korean-hackers/){:target="_blank" rel="noopener"}

> The Treasury Department's Office of Foreign Assets Control (OFAC) has sanctioned the address that received the cryptocurrency stolen in the largest cryptocurrency hack ever, the hack of Axie Infinity's Ronin network bridge. [...]
