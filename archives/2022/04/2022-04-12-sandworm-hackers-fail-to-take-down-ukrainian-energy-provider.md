Title: Sandworm hackers fail to take down Ukrainian energy provider
Date: 2022-04-12T08:03:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-12-sandworm-hackers-fail-to-take-down-ukrainian-energy-provider

[Source](https://www.bleepingcomputer.com/news/security/sandworm-hackers-fail-to-take-down-ukrainian-energy-provider/){:target="_blank" rel="noopener"}

> The Russian state-sponsored hacking group known as Sandworm tried on Friday to take down a large Ukrainian energy provider by disconnecting its electrical substations with a new variant of the Industroyer malware for industrial control systems (ICS) and a new version of the CaddyWiper data destruction malware. [...]
