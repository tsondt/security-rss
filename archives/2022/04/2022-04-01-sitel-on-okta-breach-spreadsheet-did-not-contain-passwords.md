Title: Sitel on Okta breach: "spreadsheet" did not contain passwords
Date: 2022-04-01T03:55:52-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-04-01-sitel-on-okta-breach-spreadsheet-did-not-contain-passwords

[Source](https://www.bleepingcomputer.com/news/security/sitel-on-okta-breach-spreadsheet-did-not-contain-passwords/){:target="_blank" rel="noopener"}

> Okta's outsourced provider of support services, Sitel (Sykes) has shared more information this week in response to the leaked documents that detailed the various incident response tasks carried out by Sitel after the Lapsus$ hack. [...]
