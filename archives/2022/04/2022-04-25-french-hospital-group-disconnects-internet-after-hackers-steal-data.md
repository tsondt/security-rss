Title: French hospital group disconnects Internet after hackers steal data
Date: 2022-04-25T10:48:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-25-french-hospital-group-disconnects-internet-after-hackers-steal-data

[Source](https://www.bleepingcomputer.com/news/security/french-hospital-group-disconnects-internet-after-hackers-steal-data/){:target="_blank" rel="noopener"}

> The GHT Coeur Grand Est. Hospitals and Health Care group comprising nine establishments with 3,370 beds across Northeast France has disclosed a cyberattack that resulted in the theft of sensitive administrative and patient data. [...]
