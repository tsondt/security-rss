Title: Extend your pre-commit hooks with AWS CloudFormation Guard
Date: 2022-04-26T16:36:24+00:00
Author: Joaquin Manuel Rinaudo
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Technical How-to;AWS CloudFormation;Management Tools;Technical How-To
Slug: 2022-04-26-extend-your-pre-commit-hooks-with-aws-cloudformation-guard

[Source](https://aws.amazon.com/blogs/security/extend-your-pre-commit-hooks-with-aws-cloudformation-guard/){:target="_blank" rel="noopener"}

> Git hooks are scripts that extend Git functionality when certain events and actions occur during code development. Developer teams often use Git hooks to perform quality checks before they commit their code changes. For example, see the blog post Use Git pre-commit hooks to avoid AWS CloudFormation errors for a description of how the AWS [...]
