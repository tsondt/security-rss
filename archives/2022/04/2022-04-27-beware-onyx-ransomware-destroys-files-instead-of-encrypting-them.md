Title: Beware: Onyx ransomware destroys files instead of encrypting them
Date: 2022-04-27T20:16:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-27-beware-onyx-ransomware-destroys-files-instead-of-encrypting-them

[Source](https://www.bleepingcomputer.com/news/security/beware-onyx-ransomware-destroys-files-instead-of-encrypting-them/){:target="_blank" rel="noopener"}

> A new Onyx ransomware operation is destroying large files instead of encrypting them, preventing those files from being decrypted even if a ransom is paid. [...]
