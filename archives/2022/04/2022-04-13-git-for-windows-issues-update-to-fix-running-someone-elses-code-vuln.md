Title: Git for Windows issues update to fix running-someone-else’s-code vuln
Date: 2022-04-13T13:00:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-04-13-git-for-windows-issues-update-to-fix-running-someone-elses-code-vuln

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/13/git_vuln/){:target="_blank" rel="noopener"}

> Running a multi-user Windows environment and Git? Time to patch After a hefty Patch Tuesday comes news of an update for Git to deal with a vulnerability for the source shack when run on Microsoft's Windows.... [...]
