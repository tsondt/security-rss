Title: T-Mobile confirms Lapsus$ hackers breached internal systems
Date: 2022-04-22T11:19:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-22-t-mobile-confirms-lapsus-hackers-breached-internal-systems

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-confirms-lapsus-hackers-breached-internal-systems/){:target="_blank" rel="noopener"}

> T-Mobile has confirmed that the Lapsus$ extortion gang breached its network "several weeks ago" using stolen credentials and gained access to internal systems. [...]
