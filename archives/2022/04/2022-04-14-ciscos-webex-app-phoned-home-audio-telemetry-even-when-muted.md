Title: Cisco's Webex app phoned home audio telemetry even when muted
Date: 2022-04-14T20:55:01+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-04-14-ciscos-webex-app-phoned-home-audio-telemetry-even-when-muted

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/14/muting_ciscos_webex_app_doesnt/){:target="_blank" rel="noopener"}

> Study finds turning sound off in a range of applications doesn't always cut the mic Boffins at two US universities have found that muting popular native video-conferencing apps fails to disable device microphones – and that these apps have the ability to access audio data when muted, or actually do so.... [...]
