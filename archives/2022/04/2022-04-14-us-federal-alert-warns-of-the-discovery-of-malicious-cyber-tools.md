Title: US federal alert warns of the discovery of malicious cyber tools
Date: 2022-04-14T01:12:58+00:00
Author: Associated Press
Category: The Guardian
Tags: US national security;Cyberwar;Technology;Data and computer security;Russia;FBI;US news
Slug: 2022-04-14-us-federal-alert-warns-of-the-discovery-of-malicious-cyber-tools

[Source](https://www.theguardian.com/us-news/2022/apr/13/us-alert-malicious-cyber-tools-russia){:target="_blank" rel="noopener"}

> Cybersecurity officials said the evidence suggests Russia is behind the tools – configured to target North American energy concerns Multiple US government agencies issued a joint alert Wednesday warning of the discovery of malicious cyber tools created by unnamed advanced threat actors that they said were capable of gaining “full system access” to multiple industrial control systems. The public alert from the Energy and Homeland Security departments, the FBI and National Security Agency did not name the actors or offer details on the find. But their private sector cybersecurity partners said the evidence suggests Russia is behind the tools – [...]
