Title: GitLab addresses critical account hijack bug
Date: 2022-04-01T13:36:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-01-gitlab-addresses-critical-account-hijack-bug

[Source](https://portswigger.net/daily-swig/gitlab-addresses-critical-account-hijack-bug){:target="_blank" rel="noopener"}

> Monthly release also addresses pair of stored XSS flaws [...]
