Title: Microsoft Defender flags Google Chrome updates as suspicious
Date: 2022-04-20T11:02:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-04-20-microsoft-defender-flags-google-chrome-updates-as-suspicious

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-flags-google-chrome-updates-as-suspicious/){:target="_blank" rel="noopener"}

> Microsoft Defender for Endpoint has been tagging Google Chrome updates delivered via Google Update as suspicious activity due to a false positive issue. [...]
