Title: Android apps with 45 million installs used data harvesting SDK
Date: 2022-04-07T10:06:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2022-04-07-android-apps-with-45-million-installs-used-data-harvesting-sdk

[Source](https://www.bleepingcomputer.com/news/security/android-apps-with-45-million-installs-used-data-harvesting-sdk/){:target="_blank" rel="noopener"}

> Mobile malware analysts warn about a set of applications available on the Google Play Store, which collected sensitive user data from over 45 million devices. [...]
