Title: Clever Cryptocurrency Theft
Date: 2022-04-20T13:57:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-04-20-clever-cryptocurrency-theft

[Source](https://www.schneier.com/blog/archives/2022/04/clever-cryptocurrency-theft.html){:target="_blank" rel="noopener"}

> Beanstalk Farms is a decentralized finance project that has a majority stake governance system: basically people have proportiona votes based on the amount of currency they own. A clever hacker used a “flash loan” feature of another decentralized finance project to borrow enough of the currency to give himself a controlling stake, and then approved a $182 million transfer to his own wallet. It is insane to me that cryptocurrencies are still a thing. [...]
