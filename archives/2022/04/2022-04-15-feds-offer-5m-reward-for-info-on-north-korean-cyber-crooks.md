Title: Feds offer $5m reward for info on North Korean cyber crooks
Date: 2022-04-15T23:24:47+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-15-feds-offer-5m-reward-for-info-on-north-korean-cyber-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/15/feds_north_korea_bounty/){:target="_blank" rel="noopener"}

> Meanwhile: Caltech grad earns five years in prison for heping Kim's coders The US government offered a reward up to $5 million for information that helps disrupt North Korea's cryptocurrency theft, cyber-espionage, and other illicit state-backed activities.... [...]
