Title: Data breach at US healthcare provider ARcare impacts 345,000 individuals
Date: 2022-04-29T12:45:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-29-data-breach-at-us-healthcare-provider-arcare-impacts-345000-individuals

[Source](https://portswigger.net/daily-swig/data-breach-at-us-healthcare-provider-arcare-impacts-345-000-individuals){:target="_blank" rel="noopener"}

> Sensitive medical and other personal data was potentially exposed [...]
