Title: Crooks steal NFTs worth '$3m' in Bored Ape Yacht Club heist
Date: 2022-04-26T01:00:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-26-crooks-steal-nfts-worth-3m-in-bored-ape-yacht-club-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/26/nft_theft_bored_ape_yacht_club/){:target="_blank" rel="noopener"}

> More news that three years ago would sound like a fever dream Crooks stole non-fungible tokens (NFTs) said to be worth about $3 million after breaking into the Bored Ape Yacht Club's Instagram account and posting a link to a copycat website that sought to harvest marks' assets.... [...]
