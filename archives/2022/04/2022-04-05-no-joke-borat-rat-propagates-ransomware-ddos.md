Title: No-Joke Borat RAT Propagates Ransomware, DDoS
Date: 2022-04-05T13:30:50+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: 2022-04-05-no-joke-borat-rat-propagates-ransomware-ddos

[Source](https://threatpost.com/borat-rat-ransomware-ddos/179233/){:target="_blank" rel="noopener"}

> This fresh malware strain extends the functionality of typical trojans with advanced functionality and a series of modules for launching various types of threat activity. [...]
