Title: Microsoft Patch Tuesday, April 2022 Edition
Date: 2022-04-13T15:01:24+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Security Tools;Time to Patch
Slug: 2022-04-13-microsoft-patch-tuesday-april-2022-edition

[Source](https://krebsonsecurity.com/2022/04/microsoft-patch-tuesday-april-2022-edition/){:target="_blank" rel="noopener"}

> Microsoft on Tuesday released updates to fix roughly 120 security vulnerabilities in its Windows operating systems and other software. Two of the flaws have been publicly detailed prior to this week, and one is already seeing active exploitation, according to a report from the U.S. National Security Agency (NSA). Of particular concern this month is CVE-2022-24521, which is a “privilege escalation” vulnerability in the Windows common log file system driver. In its advisory, Microsoft said it received a report from the NSA that the flaw is under active attack. “It’s not stated how widely the exploit is being used in [...]
