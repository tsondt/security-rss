Title: Ukraine targeted by DDoS attacks from compromised WordPress sites
Date: 2022-04-28T11:38:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-28-ukraine-targeted-by-ddos-attacks-from-compromised-wordpress-sites

[Source](https://www.bleepingcomputer.com/news/security/ukraine-targeted-by-ddos-attacks-from-compromised-wordpress-sites/){:target="_blank" rel="noopener"}

> Ukraine's computer emergency response team (CERT-UA) has published an announcement warning of ongoing DDoS (distributed denial of service) attacks targeting pro-Ukraine sites and the government web portal. [...]
