Title: NPM flaw let attackers add anyone as maintainer to malicious packages
Date: 2022-04-28T07:19:29-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-04-28-npm-flaw-let-attackers-add-anyone-as-maintainer-to-malicious-packages

[Source](https://www.bleepingcomputer.com/news/security/npm-flaw-let-attackers-add-anyone-as-maintainer-to-malicious-packages/){:target="_blank" rel="noopener"}

> A logical flaw in the npm registry, dubbed 'package planting' let authors of malicious packages quietly add anyone and any number of users as 'maintainers' to their packages in an attempt to boost the trust in their package. [...]
