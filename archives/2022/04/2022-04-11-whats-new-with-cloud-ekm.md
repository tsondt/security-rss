Title: What's new with Cloud EKM
Date: 2022-04-11T16:00:00+00:00
Author: Jasika Bawa
Category: GCP Security
Tags: Identity & Security
Slug: 2022-04-11-whats-new-with-cloud-ekm

[Source](https://cloud.google.com/blog/products/identity-security/whats-new-with-cloud-ekm/){:target="_blank" rel="noopener"}

> Google Cloud External Key Manager (Cloud EKM) lets you protect your cloud data with encryption keys that are stored and managed in a third-party key management system outside Google Cloud’s infrastructure. This allows you to achieve full separation between your encryption keys and your data stored in the cloud, making you the ultimate arbiter of access to your data. We are continuously innovating and developing the functionality of Cloud EKM, so let's explore some recent updates we’ve made. New functionality Available today, we have added several much-anticipated features to Cloud EKM to help meet customer requirements: Cloud EKM over VPC [...]
