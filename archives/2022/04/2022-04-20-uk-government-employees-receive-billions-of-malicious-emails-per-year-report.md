Title: UK government employees receive ‘billions’ of malicious emails per year – report
Date: 2022-04-20T13:31:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-20-uk-government-employees-receive-billions-of-malicious-emails-per-year-report

[Source](https://portswigger.net/daily-swig/uk-government-employees-receive-billions-of-malicious-emails-per-year-report){:target="_blank" rel="noopener"}

> Phishing, malware, and spam are popular techniques deployed by attackers [...]
