Title: Kaspersky cracks Yanluowang ransomware, offers free decryptor
Date: 2022-04-19T19:59:20+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-19-kaspersky-cracks-yanluowang-ransomware-offers-free-decryptor

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/19/kaspersky_yanluowang_ransomware/){:target="_blank" rel="noopener"}

> Step one, get some scrambled files back. Steps two through 37... Kaspersky has found a vulnerability in the Yanluowang ransomware encryption algorithm and, as a result, released a free decryptor tool to help victims of this software nasty recover their files.... [...]
