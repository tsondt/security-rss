Title: YouTube terminates account for Hong Kong's presumed next head of government
Date: 2022-04-21T13:31:16+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-04-21-youtube-terminates-account-for-hong-kongs-presumed-next-head-of-government

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/21/youtube_blocks_hong_kong_candidate/){:target="_blank" rel="noopener"}

> Google cites US sanctions while Beijing and John Lee Ka-chiu are miffed YouTube has blocked the campaign account of Hong Kong's only candidate for the Special Administrative Region's (SAR) head of government, John Lee Ka-chiu, citing US sanctions.... [...]
