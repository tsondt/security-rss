Title: FIN7 hacking group 'pen tester' sentenced to 5 years in prison
Date: 2022-04-07T17:00:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-07-fin7-hacking-group-pen-tester-sentenced-to-5-years-in-prison

[Source](https://www.bleepingcomputer.com/news/security/fin7-hacking-group-pen-tester-sentenced-to-5-years-in-prison/){:target="_blank" rel="noopener"}

> Denys Iarmak, a Ukrainian member and a "pen tester for the FIN7 financially-motivated hacking group, was sentenced on Thursday to 5 years in prison for breaching victims' networks and stealing credit card information for roughly two years, between November 2016 and November 2018. [...]
