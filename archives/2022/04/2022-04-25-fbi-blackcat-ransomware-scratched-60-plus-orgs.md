Title: FBI: BlackCat ransomware scratched 60-plus orgs
Date: 2022-04-25T06:42:04+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-25-fbi-blackcat-ransomware-scratched-60-plus-orgs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/25/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Cisco Umbrella flaw patched, lid blown off TeamTNT, and ICS security folks join JCDC party In brief The BlackCat ransomware gang, said to be the first-known ransomware group to successfully break into networks with Rust-written malware, has attacked at least 60 organizations globally as of March, according to the FBI.... [...]
