Title: Your iOS app may still be covertly tracking you, despite what Apple says
Date: 2022-04-18T21:10:19+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;apple;apps;iOS;privacy;tracking
Slug: 2022-04-18-your-ios-app-may-still-be-covertly-tracking-you-despite-what-apple-says

[Source](https://arstechnica.com/?p=1848980){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Last year, Apple enacted App Tracking Transparency, a mandatory policy that forbids app makers from tracking user activity across other apps without first receiving those users’ explicit permission. Privacy advocates praised the initiative, and Facebook warned it would spell certain doom for companies that rely on targeted advertising. However, research published last week suggests that ATT, as it’s usually abbreviated, doesn’t always curb the surreptitious collection of personal data or the fingerprinting of users. At the heart of ATT is the requirement that users must click an “allow” button that appears when an app is installed. [...]
