Title: More charged in UK Lapsus$ investigation
Date: 2022-04-01T13:30:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-04-01-more-charged-in-uk-lapsus-investigation

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/01/lapsus_uk_charges/){:target="_blank" rel="noopener"}

> Two teenagers arrested as part of police probe into extortion group British police have charged two teenagers as part of an international investigation into the Lapsus$ cyber extortion gang.... [...]
