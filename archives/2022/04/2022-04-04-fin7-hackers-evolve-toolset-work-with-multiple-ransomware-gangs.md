Title: FIN7 hackers evolve toolset, work with multiple ransomware gangs
Date: 2022-04-04T10:02:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-04-fin7-hackers-evolve-toolset-work-with-multiple-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/fin7-hackers-evolve-toolset-work-with-multiple-ransomware-gangs/){:target="_blank" rel="noopener"}

> Threat analysts have compiled a detailed technical report on FIN7 operations from late 2021 to early 2022, showing that the actor is still very active, evolving, and trying new monetization methods. [...]
