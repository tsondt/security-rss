Title: New Fodcha DDoS botnet targets over 100 victims every day
Date: 2022-04-13T16:11:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-13-new-fodcha-ddos-botnet-targets-over-100-victims-every-day

[Source](https://www.bleepingcomputer.com/news/security/new-fodcha-ddos-botnet-targets-over-100-victims-every-day/){:target="_blank" rel="noopener"}

> A rapidly growing botnet is ensnaring routers, DVRs, and servers across the Internet to target more than 100 victims every day in distributed denial-of-service (DDoS) attacks. [...]
