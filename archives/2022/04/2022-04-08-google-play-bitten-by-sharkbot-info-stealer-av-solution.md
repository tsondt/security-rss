Title: Google Play Bitten by Sharkbot Info-stealer ‘AV Solution’
Date: 2022-04-08T16:06:29+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2022-04-08-google-play-bitten-by-sharkbot-info-stealer-av-solution

[Source](https://threatpost.com/google-play-bitten-sharkbot/179252/){:target="_blank" rel="noopener"}

> Google removed six different malicious Android applications targeting mainly users in the U.K. and Italy that were installed about 15,000 times. [...]
