Title: Hackers breach MailChimp's internal tools to target crypto customers
Date: 2022-04-04T10:53:14-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-04-hackers-breach-mailchimps-internal-tools-to-target-crypto-customers

[Source](https://www.bleepingcomputer.com/news/security/hackers-breach-mailchimps-internal-tools-to-target-crypto-customers/){:target="_blank" rel="noopener"}

> Email marketing firm MailChimp disclosed on Sunday that they had been hit by hackers who gained access to internal customer support and account management tools to steal audience data and conduct phishing attacks. [...]
