Title: Taiwan, China square off over chip tech espionage laws
Date: 2022-04-13T15:30:09+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-04-13-taiwan-china-square-off-over-chip-tech-espionage-laws

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/13/taiwan_and_china_new_chip_espionage_laws/){:target="_blank" rel="noopener"}

> Tightening of IP laws to prevent poaching seen by Beijing as 'provocative smear' Trouble is brewing over moves by Taiwan to prevent China from gaining access to its chip technology, as the island nation proposes tougher laws to deter the leaking of trade secrets outside the country.... [...]
