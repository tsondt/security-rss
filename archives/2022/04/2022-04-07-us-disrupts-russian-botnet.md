Title: US Disrupts Russian Botnet
Date: 2022-04-07T14:31:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;botnets;FBI;malware;national security policy;Russia
Slug: 2022-04-07-us-disrupts-russian-botnet

[Source](https://www.schneier.com/blog/archives/2022/04/us-disrupts-russian-botnet.html){:target="_blank" rel="noopener"}

> The Justice Department announced the disruption of a Russian GRU-controlled botnet: The Justice Department today announced a court-authorized operation, conducted in March 2022, to disrupt a two-tiered global botnet of thousands of infected network hardware devices under the control of a threat actor known to security researchers as Sandworm, which the U.S. government has previously attributed to the Main Intelligence Directorate of the General Staff of the Armed Forces of the Russian Federation (the GRU). The operation copied and removed malware from vulnerable internet-connected firewall devices that Sandworm used for command and control (C2) of the underlying botnet. Although the [...]
