Title: Tearing down red flags: Women in CyberSecurity’s Lynn Dohm on tackling the high exit rate of female infosec pros
Date: 2022-04-15T13:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-15-tearing-down-red-flags-women-in-cybersecuritys-lynn-dohm-on-tackling-the-high-exit-rate-of-female-infosec-pros

[Source](https://portswigger.net/daily-swig/tearing-down-red-flags-women-in-cybersecuritys-lynn-dohm-on-tackling-the-high-exit-rate-of-female-infosec-pros){:target="_blank" rel="noopener"}

> Infosec leader on why training, mutual support, and career opportunities are needed to keep women in their roles [...]
