Title: Luxury fashion house Zegna confirms August ransomware attack
Date: 2022-04-11T14:32:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-11-luxury-fashion-house-zegna-confirms-august-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/luxury-fashion-house-zegna-confirms-august-ransomware-attack/){:target="_blank" rel="noopener"}

> The Italian luxury fashion company Ermenegildo Zegna has disclosed a ransomware incident from August 2021 that has resulted in an extensive IT systems outage. [...]
