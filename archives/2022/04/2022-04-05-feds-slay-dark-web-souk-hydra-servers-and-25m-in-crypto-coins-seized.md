Title: Feds slay dark-web souk Hydra: Servers and $25m in crypto-coins seized
Date: 2022-04-05T23:12:52+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-05-feds-slay-dark-web-souk-hydra-servers-and-25m-in-crypto-coins-seized

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/05/us_germany_hydra/){:target="_blank" rel="noopener"}

> US also charges Russia-based web host owner regarding cyber-crime market US and German federal agencies came down hard on Hydra, the longest-running known dark-web marketplace trafficking in illegal drugs and money-laundering services, with a multi-pronged attack that aimed to cut off multiple heads of the nefarious online beast.... [...]
