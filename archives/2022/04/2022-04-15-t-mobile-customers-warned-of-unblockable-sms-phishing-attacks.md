Title: T-Mobile customers warned of unblockable SMS phishing attacks
Date: 2022-04-15T14:14:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-15-t-mobile-customers-warned-of-unblockable-sms-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-customers-warned-of-unblockable-sms-phishing-attacks/){:target="_blank" rel="noopener"}

> An ongoing phishing campaign targets T-Mobile customers with malicious links using unblockable texts sent via SMS (Short Message Service) group messages. [...]
