Title: Google boosts Android security with new set of dev policy changes
Date: 2022-04-07T15:41:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2022-04-07-google-boosts-android-security-with-new-set-of-dev-policy-changes

[Source](https://www.bleepingcomputer.com/news/security/google-boosts-android-security-with-new-set-of-dev-policy-changes/){:target="_blank" rel="noopener"}

> Google has announced several key policy changes for Android application developers that will increase the security of users, Google Play, and the apps offered by the service. [...]
