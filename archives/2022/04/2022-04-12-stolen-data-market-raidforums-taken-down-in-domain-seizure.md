Title: Stolen-data market RaidForums taken down in domain seizure
Date: 2022-04-12T23:51:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-04-12-stolen-data-market-raidforums-taken-down-in-domain-seizure

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/12/raidforums_market_arrest/){:target="_blank" rel="noopener"}

> Suspected admin who went by 'Omnipotent' awaits UK decision on extradition to US After at least six years of peddling pilfered personal information, the infamous stolen-data market RaidForums has been shut down following the arrest of suspected founder and admin Diogo Santos Coelho in the UK earlier this year.... [...]
