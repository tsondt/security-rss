Title: OpenSSH takes aim at 'capture now, decrypt later' quantum attacks
Date: 2022-04-12T14:00:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-04-12-openssh-takes-aim-at-capture-now-decrypt-later-quantum-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/12/openssh_9/){:target="_blank" rel="noopener"}

> Guarding against the forever almost-here crypto-cracking tech OpenSSH 9 is here, with updates aimed at dealing with cryptographically challenging quantum computers.... [...]
