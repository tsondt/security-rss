Title: Block claims ex-employee downloaded customer data after leaving firm
Date: 2022-04-06T15:02:51+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-06-block-claims-ex-employee-downloaded-customer-data-after-leaving-firm

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/06/ex-block-employee-takes-customer-data/){:target="_blank" rel="noopener"}

> Leak highlights worker offboarding policies as SaaS use grows A former employee with Block used the digital financial services firm's Cash App products to access and download personal information about US customers in December 2021, the firm has claimed.... [...]
