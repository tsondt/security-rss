Title: GitHub can now alert of supply-chain bugs in new dependencies
Date: 2022-04-08T14:00:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-08-github-can-now-alert-of-supply-chain-bugs-in-new-dependencies

[Source](https://www.bleepingcomputer.com/news/security/github-can-now-alert-of-supply-chain-bugs-in-new-dependencies/){:target="_blank" rel="noopener"}

> GitHub can now block and alert you of pull requests that introduce new dependencies impacted by known supply chain vulnerabilities. [...]
