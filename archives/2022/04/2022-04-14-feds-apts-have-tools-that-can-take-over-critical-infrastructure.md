Title: Feds: APTs Have Tools That Can Take Over Critical Infrastructure
Date: 2022-04-14T15:57:20+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Critical Infrastructure;Malware
Slug: 2022-04-14-feds-apts-have-tools-that-can-take-over-critical-infrastructure

[Source](https://threatpost.com/feds-apts-critical-infrastructure/179291/){:target="_blank" rel="noopener"}

> Threat actors have developed custom modules to compromise various ICS devices as well as Windows workstations that pose an imminent threat, particularly to energy providers. [...]
