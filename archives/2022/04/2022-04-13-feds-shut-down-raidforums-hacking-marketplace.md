Title: Feds Shut Down RaidForums Hacking Marketplace
Date: 2022-04-13T15:01:36+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Web Security
Slug: 2022-04-13-feds-shut-down-raidforums-hacking-marketplace

[Source](https://threatpost.com/shut-down-raidforums-hacking-marketplace/179279/){:target="_blank" rel="noopener"}

> The DoJ is charging its founder, 21-year-old Portuguese citizen Diogo Santos Coelho, on six criminal counts, including conspiracy, access device fraud and aggravated identity theft. [...]
