Title: Apple Rushes Out Patches for 0-Days in MacOS, iOS
Date: 2022-04-01T13:02:17+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;Web Security
Slug: 2022-04-01-apple-rushes-out-patches-for-0-days-in-macos-ios

[Source](https://threatpost.com/apple-rushes-out-patches-0-days-macos-ios/179222/){:target="_blank" rel="noopener"}

> The vulnerabilities could allow threat actors to disrupt or access kernel activity and may be under active exploit. [...]
