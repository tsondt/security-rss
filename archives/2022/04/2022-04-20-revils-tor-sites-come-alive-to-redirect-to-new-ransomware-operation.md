Title: REvil's TOR sites come alive to redirect to new ransomware operation
Date: 2022-04-20T17:29:18-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-04-20-revils-tor-sites-come-alive-to-redirect-to-new-ransomware-operation

[Source](https://www.bleepingcomputer.com/news/security/revils-tor-sites-come-alive-to-redirect-to-new-ransomware-operation/){:target="_blank" rel="noopener"}

> REvil ransomware's servers in the TOR network are back up after months of inactivity and redirect to a new operation that appears to have started since at least mid-December last year. [...]
