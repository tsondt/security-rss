Title: US uncovers “Swiss Army knife” for hacking industrial control systems
Date: 2022-04-14T20:52:36+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;hacking;industrial control;infrastructure attacks;malware;power grid;utilities
Slug: 2022-04-14-us-uncovers-swiss-army-knife-for-hacking-industrial-control-systems

[Source](https://arstechnica.com/?p=1848127){:target="_blank" rel="noopener"}

> Enlarge (credit: cravetiger | Getty Images) Malware designed to target industrial control systems like power grids, factories, water utilities, and oil refineries represents a rare species of digital badness. So when the United States government warns of a piece of code built to target not just one of those industries, but potentially all of them, critical infrastructure owners worldwide should take notice. On Wednesday, the Department of Energy, the Cybersecurity and Infrastructure Security Agency, the NSA, and the FBI jointly released an advisory about a new hacker toolset potentially capable of meddling with a wide range of industrial control system [...]
