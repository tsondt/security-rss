Title: Huawei reportedly furloughs Russian staff and stops taking orders
Date: 2022-04-13T05:03:55+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-04-13-huawei-reportedly-furloughs-russian-staff-and-stops-taking-orders

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/13/huawei_may_quit_russia/){:target="_blank" rel="noopener"}

> Chinese giant still hiring in Moscow – for some very interesting gigs Updated Chinese telecom giant Huawei has issued a mandatory month-long furlough to some of its Russia-based staff and suspended new orders, according to Russian media.... [...]
