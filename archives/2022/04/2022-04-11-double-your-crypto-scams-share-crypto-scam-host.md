Title: Double-Your-Crypto Scams Share Crypto Scam Host
Date: 2022-04-11T15:26:40+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;+7.9914500893;ARKinvest;crypto investment scams;Cryptohost;FinTelegram;lightning-network;Mark Tepterev;Polkadot;Sergei Orlovets;SmartApe;ulanikirill52@gmail.com
Slug: 2022-04-11-double-your-crypto-scams-share-crypto-scam-host

[Source](https://krebsonsecurity.com/2022/04/double-your-crypto-scams-share-crypto-scam-host/){:target="_blank" rel="noopener"}

> Online scams that try to separate the unwary from their cryptocurrency are a dime a dozen, but a great many seemingly disparate crypto scam websites tend to rely on the same dodgy infrastructure providers to remain online in the face of massive fraud and abuse complaints from their erstwhile customers. Here’s a closer look at hundreds of phony crypto investment schemes that are all connected through a hosting provider which caters to people running crypto scams. A security researcher recently shared with KrebsOnSecurity an email he received from someone who said they foolishly invested an entire bitcoin (currently worth ~USD [...]
