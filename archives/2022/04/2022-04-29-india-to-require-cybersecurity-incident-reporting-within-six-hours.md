Title: India to require cybersecurity incident reporting within six hours
Date: 2022-04-29T11:07:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-04-29-india-to-require-cybersecurity-incident-reporting-within-six-hours

[Source](https://www.bleepingcomputer.com/news/security/india-to-require-cybersecurity-incident-reporting-within-six-hours/){:target="_blank" rel="noopener"}

> The Indian government has issued new directives requiring organizations to report cybersecurity incidents to CERT-IN within six hours, even if those incidents are port or vulnerability scans of computer systems. [...]
