Title: Pwn2Own Miami: Hackers earn $400,000 by cracking ICS platforms
Date: 2022-04-22T15:06:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-22-pwn2own-miami-hackers-earn-400000-by-cracking-ics-platforms

[Source](https://portswigger.net/daily-swig/pwn2own-miami-hackers-earn-400-000-by-cracking-ics-platforms){:target="_blank" rel="noopener"}

> Industrial control insecurity laid bare during competition [...]
