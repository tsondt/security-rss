Title: Introducing SWIFT on Google Cloud
Date: 2022-04-28T13:00:00+00:00
Author: Chris Page
Category: GCP Security
Tags: Application Modernization;Identity & Security;Google Cloud;Financial Services
Slug: 2022-04-28-introducing-swift-on-google-cloud

[Source](https://cloud.google.com/blog/topics/financial-services/introducing-swift-on-google-cloud/){:target="_blank" rel="noopener"}

> It is hard to understand global payments without understanding SWIFT. For over 40 years, SWIFT, Society for Worldwide Interbank Financial Telecommunication, has secured financial messaging for banks, corporates, brokers, and treasuries in over 200 countries. For example, if you have ever requested a funds transfer from your local bank's branch or website to a friend or relative who has a different bank in another country, chances are those payment messages went through the SWIFT network. Today, I would like to talk about how Google Cloud is collaborating with SWIFT for SWIFT to offer our joint customers the SWIFT Alliance Connect [...]
