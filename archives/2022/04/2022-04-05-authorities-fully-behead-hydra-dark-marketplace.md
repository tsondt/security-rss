Title: Authorities Fully Behead Hydra Dark Marketplace
Date: 2022-04-05T17:53:47+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Web Security
Slug: 2022-04-05-authorities-fully-behead-hydra-dark-marketplace

[Source](https://threatpost.com/authorities-hydra-dark-marketplace/179240/){:target="_blank" rel="noopener"}

> The popular underground market traded in drugs, stolen data, forged documents and more -- raking in billions in Bitcoin. [...]
