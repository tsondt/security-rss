Title: India gives local techies 60 days to hit 6-hour deadline for infosec incident reporting
Date: 2022-04-29T10:46:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-04-29-india-gives-local-techies-60-days-to-hit-6-hour-deadline-for-infosec-incident-reporting

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/29/cert_in_directive/){:target="_blank" rel="noopener"}

> Customer data collection and retention requirements also increased, including for crypto operators India's Computer Emergency Response Team (CERT-In) has given many of the nation's IT shops a big job that needs to be done in a hurry: complying with a new set of rules that require organizations to report 20 different types of infosec incidents within six hours of detection, be they a ransomware attack or mere compromise of a social media account.... [...]
