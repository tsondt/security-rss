Title: US DOJ probes Google's $5.4b Mandiant acquisition
Date: 2022-04-22T20:52:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-22-us-doj-probes-googles-54b-mandiant-acquisition

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/22/doj-google-mandiant/){:target="_blank" rel="noopener"}

> Not a social network or an instant-chat app used by tens of millions, so scrutiny it is, then Federal regulators are taking a closer look at Google's planned $5.4 billion acquisition of Mandiant, a deal designed to boost the web giant's public cloud's cybersecurity capabilities.... [...]
