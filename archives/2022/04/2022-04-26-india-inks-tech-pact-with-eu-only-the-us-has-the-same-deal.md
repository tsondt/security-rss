Title: India inks tech pact with EU – only the US has the same deal
Date: 2022-04-26T07:32:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-04-26-india-inks-tech-pact-with-eu-only-the-us-has-the-same-deal

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/26/india_eu_trade_and_technology_council/){:target="_blank" rel="noopener"}

> Meanwhile, UK and India finally explain Cyber Security Partnership agreed to in May 2021 India's government and the European Union have signed up to create a "Trade and Technology Council" – an entity the EU has previously only created to enhance its relationship with the United States.... [...]
