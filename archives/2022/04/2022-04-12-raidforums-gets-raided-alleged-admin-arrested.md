Title: RaidForums Gets Raided, Alleged Admin Arrested
Date: 2022-04-12T17:29:33+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Diogo Santos Coelho;Europol;fbi;National Crime Agency;Omnipotent;Operation Tourniquet;RaidForums seizure;SubVirt;T-Mobile breach;U.S. Department of Justice;unrivaled@pm.me
Slug: 2022-04-12-raidforums-gets-raided-alleged-admin-arrested

[Source](https://krebsonsecurity.com/2022/04/raidforums-get-raided-alleged-admin-arrested/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DOJ) said today it seized the website and user database for RaidForums, an extremely popular English-language cybercrime forum that sold access to more than 10 billion consumer records stolen in some of the world’s largest data breaches since 2015. The DOJ also charged the alleged administrator of RaidForums — 21-year-old Diogo Santos Coelho, of Portugal — with six criminal counts, including conspiracy, access device fraud and aggravated identity theft. The “raid” in RaidForums is a nod to the community’s humble beginnings in 2015, when it was primarily an online venue for organizing and supporting various [...]
