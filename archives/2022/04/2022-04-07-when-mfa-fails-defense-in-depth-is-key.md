Title: When MFA fails, defense in depth is key
Date: 2022-04-07T07:45:11+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2022-04-07-when-mfa-fails-defense-in-depth-is-key

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/07/mfa_defense_in_depth/){:target="_blank" rel="noopener"}

> It started with a phish - never thought it would come to this Paid feature There are no silver bullets in cyber security. Vendors would like to create hacker-proof defenses, but attackers always find a sneaky way through.... [...]
