Title: Flaw in Rarible NFT market allowed tricky crypto asset transfers
Date: 2022-04-14T06:23:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-04-14-flaw-in-rarible-nft-market-allowed-tricky-crypto-asset-transfers

[Source](https://www.bleepingcomputer.com/news/security/flaw-in-rarible-nft-market-allowed-tricky-crypto-asset-transfers/){:target="_blank" rel="noopener"}

> A security flaw in the Rarible NFT (non-fungible token) marketplace allowed threat actors to use a relatively simple attack vector to steal digital assets from the target's accounts and transfer them directly to their wallets. [...]
