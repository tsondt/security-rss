Title: Chinese drone-maker DJI suspends ops in Russia, Ukraine
Date: 2022-04-27T08:15:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-04-27-chinese-drone-maker-dji-suspends-ops-in-russia-ukraine

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/27/dji_suspends_russia_and_ukraine/){:target="_blank" rel="noopener"}

> First Middle Kingdom company to take a stance says it doesn't want anyone weaponizing its flying machines In a first for a major Chinese tech company, drone-maker DJI Technologies announced on Tuesday that it will temporarily suspend business in both Russia and Ukraine.... [...]
