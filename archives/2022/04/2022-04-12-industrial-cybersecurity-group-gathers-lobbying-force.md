Title: Industrial cybersecurity group gathers lobbying force
Date: 2022-04-12T16:30:12+00:00
Author: Nicole Hemsoth
Category: The Register
Tags: 
Slug: 2022-04-12-industrial-cybersecurity-group-gathers-lobbying-force

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/12/industrial_cybersecurity_group_gathers_lobbying/){:target="_blank" rel="noopener"}

> Industrial giants, cybersec vendors collect under OTCSA banner A number of the world's largest manufacturing and cybersecurity companies are getting behind a new consortium aimed at protecting industrial systems from threats.... [...]
