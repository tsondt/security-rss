Title: New EnemyBot DDoS botnet recruits routers and IoTs into its army
Date: 2022-04-13T12:00:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-13-new-enemybot-ddos-botnet-recruits-routers-and-iots-into-its-army

[Source](https://www.bleepingcomputer.com/news/security/new-enemybot-ddos-botnet-recruits-routers-and-iots-into-its-army/){:target="_blank" rel="noopener"}

> A new Mirai-based botnet malware named Enemybot has been observed growing its army of infected devices through vulnerabilities in modems, routers, and IoT devices, with the threat actor operating it known as Keksec. [...]
