Title: UK Prime Minister, Catalan groups 'targeted by NSO Pegasus spyware'
Date: 2022-04-18T20:17:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-04-18-uk-prime-minister-catalan-groups-targeted-by-nso-pegasus-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/18/uk_catalan_spyware/){:target="_blank" rel="noopener"}

> UAE reportedly using 'legal' malware on erstwhile allies Citizen Lab has reported finding suspected surveillance software on devices associated with both the UK Prime Minister's Office and what was formerly called the British Foreign and Commonwealth Office.... [...]
