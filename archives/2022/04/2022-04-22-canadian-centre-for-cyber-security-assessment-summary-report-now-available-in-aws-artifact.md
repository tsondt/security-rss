Title: Canadian Centre for Cyber Security Assessment Summary report now available in AWS Artifact
Date: 2022-04-22T18:45:07+00:00
Author: Rob Samuel
Category: AWS Security
Tags: Announcements;AWS Artifact;Compliance;Federal;Foundational (100);Government;Public Sector;Security;Security, Identity, & Compliance;State or Local Government;AWS Canada (Central) Region;AWS Shared Responsibility Model;Canada;CCCS;CCCS Assessment;classification;Cloud security;Cyber Security;cybersecurity;Federal Government Canada;Government of Canada (GC);ITSG-33;PBMM
Slug: 2022-04-22-canadian-centre-for-cyber-security-assessment-summary-report-now-available-in-aws-artifact

[Source](https://aws.amazon.com/blogs/security/canadian-centre-for-cyber-security-assessment-summary-report-now-available-in-aws-artifact/){:target="_blank" rel="noopener"}

> French version At Amazon Web Services (AWS), we are committed to providing continued assurance to our customers through assessments, certifications, and attestations that support the adoption of AWS services. We are pleased to announce the availability of the Canadian Centre for Cyber Security (CCCS) assessment summary report for AWS, which you can view and download [...]
