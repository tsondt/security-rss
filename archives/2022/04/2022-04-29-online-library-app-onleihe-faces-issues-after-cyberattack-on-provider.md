Title: Online library app Onleihe faces issues after cyberattack on provider
Date: 2022-04-29T14:01:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-29-online-library-app-onleihe-faces-issues-after-cyberattack-on-provider

[Source](https://www.bleepingcomputer.com/news/security/online-library-app-onleihe-faces-issues-after-cyberattack-on-provider/){:target="_blank" rel="noopener"}

> Library lending app Onleihe announced problems lending several media formats offered on the platform, like audio, video, and e-book files, after a cyberattack targeted their vendor. [...]
