Title: Microsoft announces new Windows 11 security, encryption features
Date: 2022-04-05T11:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-04-05-microsoft-announces-new-windows-11-security-encryption-features

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-announces-new-windows-11-security-encryption-features/){:target="_blank" rel="noopener"}

> Microsoft says that Windows 11 will get more security improvements in upcoming releases, which will add more protection against cybersecurity threats, offer better encryption, and block malicious apps and drivers. [...]
