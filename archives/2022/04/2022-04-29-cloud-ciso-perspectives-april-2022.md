Title: Cloud CISO Perspectives: April 2022
Date: 2022-04-29T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-04-29-cloud-ciso-perspectives-april-2022

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-april-2022/){:target="_blank" rel="noopener"}

> This month marks one year of our Cloud CISO Perspectives Series ! Over the past year, we’ve discussed many milestones and challenges across our industry. I’m most proud of the work our collective security teams at Google Cloud are doing everyday to help improve security for our customers and society at large through the cloud. Below, catch up on the latest updates from our Google Cybersecurity Action Team, open source software security progress, and don’t forget to register for our Google Cloud Security Summit... Google Cloud Security Summit On Tuesday, May 17, we will host our second annual Google Cloud [...]
