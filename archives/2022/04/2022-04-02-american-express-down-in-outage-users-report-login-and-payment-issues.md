Title: American Express down in outage: users report login and payment issues
Date: 2022-04-02T03:55:42-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-04-02-american-express-down-in-outage-users-report-login-and-payment-issues

[Source](https://www.bleepingcomputer.com/news/security/american-express-down-in-outage-users-report-login-and-payment-issues/){:target="_blank" rel="noopener"}

> Yesterday, American Express users across the world including US, UK, and Europe, experienced widespread outages lasting hours, and some users continue to. BleepingComputer was able to briefly reproduce issues right before Amex confirmed partially restoring services. [...]
