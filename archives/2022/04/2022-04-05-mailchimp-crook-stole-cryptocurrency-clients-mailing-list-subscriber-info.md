Title: Mailchimp: Crook stole cryptocurrency clients' mailing-list subscriber info
Date: 2022-04-05T01:11:49+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-05-mailchimp-crook-stole-cryptocurrency-clients-mailing-list-subscriber-info

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/05/mailchimp_confirms_breach/){:target="_blank" rel="noopener"}

> Staff socially engineered into handing over internal system credentials Mailchimp has confirmed a miscreant gained access to one of its internal tools and used it to steal data belonging to 100-plus high-value customers.... [...]
