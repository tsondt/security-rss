Title: Don't let ransomware crooks spend months in your network – like this govt agency did
Date: 2022-04-14T00:12:27+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-14-dont-let-ransomware-crooks-spend-months-in-your-network-like-this-govt-agency-did

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/14/ransomware_gang_network/){:target="_blank" rel="noopener"}

> Miscreants Googled for post-intrusion tools before downloading them onto servers, PCs Lockbit ransomware operators spent nearly six months in a government agency's network, deleting logs and using Chrome to download hacking tools, before eventually deploying extortionware, according to Sophos threat researchers.... [...]
