Title: Critical bug allows attacker to remotely control medical robot
Date: 2022-04-12T11:00:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-12-critical-bug-allows-attacker-to-remotely-control-medical-robot

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/12/critical_vuln_hospital_robots/){:target="_blank" rel="noopener"}

> CVSS 9.8 flaws are not what you want in a hospital robot Mobile robot maker Aethon has fixed a series of vulnerabilities in its Tug hospital robots that, if exploited, could allow a cybercriminal to remotely control thousands of medical machines.... [...]
