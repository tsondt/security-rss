Title: GitHub restores popular Python repo hit by bogus DMCA takedown
Date: 2022-04-21T10:26:03-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-04-21-github-restores-popular-python-repo-hit-by-bogus-dmca-takedown

[Source](https://www.bleepingcomputer.com/news/security/github-restores-popular-python-repo-hit-by-bogus-dmca-takedown/){:target="_blank" rel="noopener"}

> Yesterday, following a DMCA complaint, GitHub took down a repository that hosts the official SymPy project documentation website. It turns out the DMCA notice filed by HackerRank's representatives was sent out in error and generated much backlash from the open source community. The DMCA notice has since been rescinded. [...]
