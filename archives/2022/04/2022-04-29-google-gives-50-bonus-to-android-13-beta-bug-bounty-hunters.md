Title: Google gives 50% bonus to Android 13 Beta bug bounty hunters
Date: 2022-04-29T13:48:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-04-29-google-gives-50-bonus-to-android-13-beta-bug-bounty-hunters

[Source](https://www.bleepingcomputer.com/news/security/google-gives-50-percent-bonus-to-android-13-beta-bug-bounty-hunters/){:target="_blank" rel="noopener"}

> Google has announced that all security researchers who report Android 13 Beta vulnerabilities through its Vulnerability Rewards Program (VRP) will get a 50% bonus on top of the standard reward until May 26th, 2022. [...]
