Title: Hackers steal $655K after picking MetaMask seed from iCloud backup
Date: 2022-04-18T14:12:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-04-18-hackers-steal-655k-after-picking-metamask-seed-from-icloud-backup

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-655k-after-picking-metamask-seed-from-icloud-backup/){:target="_blank" rel="noopener"}

> MetaMask has published a warning for their iOS users about the seeds of cryptocurrency wallets being stored in Apple's iCloud if app data backup is active. [...]
