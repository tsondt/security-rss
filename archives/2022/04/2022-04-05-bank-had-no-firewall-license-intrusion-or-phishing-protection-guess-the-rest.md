Title: Bank had no firewall license, intrusion or phishing protection – guess the rest
Date: 2022-04-05T05:15:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-04-05-bank-had-no-firewall-license-intrusion-or-phishing-protection-guess-the-rest

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/05/mahesh_bank_no_firewall_attack/){:target="_blank" rel="noopener"}

> Crooks used RAT to hijack superusers at India's Mahesh Bank, stole millions An Indian bank that did not have a valid firewall license, had not employed phishing protection, lacked an intrusion detection system and eschewed use of any intrusion prevention system has, shockingly, been compromised by criminals who made off with millions of rupees.... [...]
