Title: How do China's cyber-spies snoop on governments, NGOs? Probably like this
Date: 2022-04-07T09:45:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-07-how-do-chinas-cyber-spies-snoop-on-governments-ngos-probably-like-this

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/07/china-espionage-campaign/){:target="_blank" rel="noopener"}

> Cicada's months-long global espionage campaign marks an expansion of team's capabilities A China-backed crew is said to be running a global espionage campaign against governments, religious groups, and non-governmental organizations (NGOs) by, in some cases, possibly exploiting a vulnerability in Microsoft Exchange servers.... [...]
