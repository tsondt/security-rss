Title: UK spy agencies sharing bulk personal data with foreign allies was legal, says court
Date: 2022-04-06T08:33:04+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-04-06-uk-spy-agencies-sharing-bulk-personal-data-with-foreign-allies-was-legal-says-court

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/06/privacy_international_vs_ipt/){:target="_blank" rel="noopener"}

> Yes, that thing they've never publicly admitted they do A privacy rights org this week lost an appeal [PDF] in a case about the sharing of Bulk Personal Datasets (BPDs) by MI5, MI6, and GCHQ with foreign intelligence agencies.... [...]
