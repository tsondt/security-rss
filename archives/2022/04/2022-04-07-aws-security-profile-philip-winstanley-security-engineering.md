Title: AWS Security Profile: Philip Winstanley, Security Engineering
Date: 2022-04-07T17:22:01+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;AWS Security and Compliance;AWS Security Profiles
Slug: 2022-04-07-aws-security-profile-philip-winstanley-security-engineering

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-philip-winstanley-security-engineering/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, I interview some of the humans who work in Amazon Web Services (AWS) Security and help keep our customers safe and secure. This interview is with Philip Winstanley, a security engineer and AWS Guardian. The Guardians program identifies and develops security experts within engineering teams across AWS, enabling these [...]
