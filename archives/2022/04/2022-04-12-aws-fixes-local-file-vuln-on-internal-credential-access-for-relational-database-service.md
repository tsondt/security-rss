Title: AWS fixes local file vuln on internal credential access for Relational Database Service
Date: 2022-04-12T18:05:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-04-12-aws-fixes-local-file-vuln-on-internal-credential-access-for-relational-database-service

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/12/aws_rds_vuln/){:target="_blank" rel="noopener"}

> Lightspin threat researchers discovered the bug, which AWS fixed A local file read vulnerability in Amazon's Relational Database Service (RDS) could have been exploited by an attacker to gain access to internal AWS credentials, the cloud behemoth has confirmed.... [...]
