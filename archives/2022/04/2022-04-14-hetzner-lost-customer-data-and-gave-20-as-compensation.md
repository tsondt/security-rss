Title: Hetzner lost customer data and gave 20€ as compensation
Date: 2022-04-14T10:12:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2022-04-14-hetzner-lost-customer-data-and-gave-20-as-compensation

[Source](https://www.bleepingcomputer.com/news/security/hetzner-lost-customer-data-and-gave-20-as-compensation/){:target="_blank" rel="noopener"}

> Hetzner Online GmbH, a German cloud services provider, told some customers this week that their data had been irreversibly lost and were provided a 20€ compensation in online credit. [...]
