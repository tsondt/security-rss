Title: Lenovo UEFI firmware driver bugs affect over 100 notebook models
Date: 2022-04-19T09:01:06-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-04-19-lenovo-uefi-firmware-driver-bugs-affect-over-100-notebook-models

[Source](https://www.bleepingcomputer.com/news/security/lenovo-uefi-firmware-driver-bugs-affect-over-100-notebook-models/){:target="_blank" rel="noopener"}

> Lenovo has published a security advisory on vulnerabilities that impact its Unified Extensible Firmware Interface (UEFI) loaded on at least 100 of its laptop models. [...]
