Title: New Industrial Spy stolen data market promoted through cracks, adware
Date: 2022-04-16T12:50:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-04-16-new-industrial-spy-stolen-data-market-promoted-through-cracks-adware

[Source](https://www.bleepingcomputer.com/news/security/new-industrial-spy-stolen-data-market-promoted-through-cracks-adware/){:target="_blank" rel="noopener"}

> Threat actors have launched a new marketplace called Industrial Spy that sells stolen data from breached companies, promoting the site through adware and software cracks. [...]
