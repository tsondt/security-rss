Title: Study: How Amazon uses Echo smart speaker conversations to target ads
Date: 2022-04-27T06:52:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-04-27-study-how-amazon-uses-echo-smart-speaker-conversations-to-target-ads

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/27/amazon_audio_data/){:target="_blank" rel="noopener"}

> Web giant milks advertisers with data harvested from digital assistant Amazon and third-party services have been using smart speaker interaction data for ad targeting, in violation of privacy commitments, according to researchers at four US universities.... [...]
