Title: PacketStreamer: New tool can aid research by revealing potential hacking behaviors
Date: 2022-04-11T10:52:44+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-11-packetstreamer-new-tool-can-aid-research-by-revealing-potential-hacking-behaviors

[Source](https://portswigger.net/daily-swig/packetstreamer-new-tool-can-aid-research-by-revealing-potential-hacking-behaviors){:target="_blank" rel="noopener"}

> Utility can be used to “indicate the presence of an adversary or the progress of an attack” [...]
