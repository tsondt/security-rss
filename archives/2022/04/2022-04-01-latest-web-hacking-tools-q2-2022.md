Title: Latest web hacking tools – Q2 2022
Date: 2022-04-01T09:37:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-01-latest-web-hacking-tools-q2-2022

[Source](https://portswigger.net/daily-swig/latest-web-hacking-tools-q2-2022){:target="_blank" rel="noopener"}

> We take a look at the latest additions to security researchers’ armory [...]
