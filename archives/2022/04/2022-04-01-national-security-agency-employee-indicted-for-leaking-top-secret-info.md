Title: National Security Agency employee indicted for 'leaking top secret info'
Date: 2022-04-01T05:33:33+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-04-01-national-security-agency-employee-indicted-for-leaking-top-secret-info

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/01/nsa_employee_secret_data_leak/){:target="_blank" rel="noopener"}

> Managed to send material from his private email address, it is claimed The United States Department of Justice (DoJ) has accused an NSA employee of sharing top-secret national security information with an unnamed person who worked in the private sector.... [...]
