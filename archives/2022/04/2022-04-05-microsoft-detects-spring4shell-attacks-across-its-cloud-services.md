Title: Microsoft detects Spring4Shell attacks across its cloud services
Date: 2022-04-05T12:46:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-04-05-microsoft-detects-spring4shell-attacks-across-its-cloud-services

[Source](https://www.bleepingcomputer.com/news/security/microsoft-detects-spring4shell-attacks-across-its-cloud-services/){:target="_blank" rel="noopener"}

> Microsoft said that it's currently tracking a "low volume of exploit attempts" targeting the critical Spring4Shell (aka SpringShell) remote code execution (RCE) vulnerability across its cloud services. [...]
