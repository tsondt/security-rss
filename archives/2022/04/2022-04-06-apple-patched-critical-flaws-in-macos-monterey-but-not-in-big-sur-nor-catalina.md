Title: Apple patched critical flaws in macOS Monterey but not in Big Sur nor Catalina
Date: 2022-04-06T07:40:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-04-06-apple-patched-critical-flaws-in-macos-monterey-but-not-in-big-sur-nor-catalina

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/06/apple_patched_zerodays_in_macos/){:target="_blank" rel="noopener"}

> About 35-40 per cent of iGiant's desktop OS installs potentially vulnerable, says Intego Apple last week patched two actively exploited vulnerabilities in macOS Monterey yet has left users of older supported versions of its desktop operating system unprotected.... [...]
