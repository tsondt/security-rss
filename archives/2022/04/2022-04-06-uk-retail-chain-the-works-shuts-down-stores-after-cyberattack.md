Title: UK retail chain The Works shuts down stores after cyberattack
Date: 2022-04-06T14:22:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-04-06-uk-retail-chain-the-works-shuts-down-stores-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/uk-retail-chain-the-works-shuts-down-stores-after-cyberattack/){:target="_blank" rel="noopener"}

> British retail chain The Works announced it was forced to shut down several stores due to till issues caused by a cyber-security incident involving unauthorized access to its computer systems. [...]
