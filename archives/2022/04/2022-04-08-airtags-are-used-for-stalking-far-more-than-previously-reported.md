Title: AirTags Are Used for Stalking Far More than Previously Reported
Date: 2022-04-08T11:06:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;police;stalking;tracking
Slug: 2022-04-08-airtags-are-used-for-stalking-far-more-than-previously-reported

[Source](https://www.schneier.com/blog/archives/2022/04/airtags-are-used-for-stalking-far-more-than-previously-reported.html){:target="_blank" rel="noopener"}

> Ever since Apple introduced AirTags, security people have warned that they could be used for stalking. But while there have been a bunch of anecdotal stories, this is the first vaguely scientific survey: Motherboard requested records mentioning AirTags in a recent eight month period from dozens of the country’s largest police departments. We obtained records from eight police departments. Of the 150 total police reports mentioning AirTags, in 50 cases women called the police because they started getting notifications that their whereabouts were being tracked by an AirTag they didn’t own. Of those, 25 could identify a man in their [...]
