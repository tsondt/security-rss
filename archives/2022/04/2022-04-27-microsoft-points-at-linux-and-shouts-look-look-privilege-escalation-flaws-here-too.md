Title: Microsoft points at Linux and shouts: Look, look! Privilege-escalation flaws here, too!
Date: 2022-04-27T22:15:57+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-04-27-microsoft-points-at-linux-and-shouts-look-look-privilege-escalation-flaws-here-too

[Source](https://go.theregister.com/feed/www.theregister.com/2022/04/27/microsoft-linux-vulnerability/){:target="_blank" rel="noopener"}

> Will Redmond start code-naming Windows make-me-admin bugs? Flaws in networkd-dispatcher, a service used in some parts of the Linux world, can be exploited by a rogue logged-in user or application to escalate their privileges to root level, allowing the box to be commandeered, Microsoft researchers said Wednnesday.... [...]
