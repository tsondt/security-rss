Title: OpenSSH 9.0 bakes in post-quantum cryptography to future proof against attacks
Date: 2022-04-12T14:32:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-04-12-openssh-90-bakes-in-post-quantum-cryptography-to-future-proof-against-attacks

[Source](https://portswigger.net/daily-swig/openssh-9-0-bakes-in-post-quantum-cryptography-to-future-proof-against-attacks){:target="_blank" rel="noopener"}

> Protection offered against ‘capture now, decrypt later’ attacks [...]
