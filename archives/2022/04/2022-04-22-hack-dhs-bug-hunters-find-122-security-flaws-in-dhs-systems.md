Title: 'Hack DHS' bug hunters find 122 security flaws in DHS systems
Date: 2022-04-22T16:05:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-04-22-hack-dhs-bug-hunters-find-122-security-flaws-in-dhs-systems

[Source](https://www.bleepingcomputer.com/news/security/hack-dhs-bug-hunters-find-122-security-flaws-in-dhs-systems/){:target="_blank" rel="noopener"}

> The Department of Homeland Security (DHS) today revealed that bug bounty hunters enrolled in its 'Hack DHS' bug bounty program have found 122 security vulnerabilities in external DHS systems, 27 of them rated critical severity. [...]
