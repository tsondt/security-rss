Title: Critical Samba flaw presents code execution threat
Date: 2022-02-01T16:20:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-01-critical-samba-flaw-presents-code-execution-threat

[Source](https://portswigger.net/daily-swig/critical-samba-flaw-presents-code-execution-threat){:target="_blank" rel="noopener"}

> Urgent patching of file-sharing technology urged [...]
