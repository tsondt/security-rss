Title: Interview with the Head of the NSA’s Research Directorate
Date: 2022-02-03T12:01:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data collection;interviews;NSA
Slug: 2022-02-03-interview-with-the-head-of-the-nsas-research-directorate

[Source](https://www.schneier.com/blog/archives/2022/02/interview-with-the-head-of-the-nsas-research-directorate.html){:target="_blank" rel="noopener"}

> MIT Technology Review published an interview with Gil Herrera, the new head of the NSA’s Research Directorate. There’s a lot of talk about quantum computing, monitoring 5G networks, and the problems of big data: The math department, often in conjunction with the computer science department, helps tackle one of NSA’s most interesting problems: big data. Despite public reckoning over mass surveillance, NSA famously faces the challenge of collecting such extreme quantities of data that, on top of legal and ethical problems, it can be nearly impossible to sift through all of it to find everything of value. NSA views the [...]
