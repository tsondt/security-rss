Title: NetWalker ransomware affiliate sentenced to 80 months in prison
Date: 2022-02-08T07:45:04-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-08-netwalker-ransomware-affiliate-sentenced-to-80-months-in-prison

[Source](https://www.bleepingcomputer.com/news/security/netwalker-ransomware-affiliate-sentenced-to-80-months-in-prison/){:target="_blank" rel="noopener"}

> Sebastien Vachon-Desjardins, a Canadian man charged by the US for his involvement in NetWalker ransomware attacks, was sentenced to 6 years and 8 months in prison after pleading guilty before an Ontario judge to multiple offenses linked to attacks on 17 Canadian victims. [...]
