Title: Labour reminds UK.gov that it's supposed to be reforming the Computer Misuse Act
Date: 2022-02-08T10:56:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-08-labour-reminds-ukgov-that-its-supposed-to-be-reforming-the-computer-misuse-act

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/08/labour_computer_misuse_act_reform_questions/){:target="_blank" rel="noopener"}

> Shadow foreign secretary says work is visibly overdue The shadow foreign secretary for UK's opposition Labour party, David Lammy MP, has asked why the reform of the Computer Misuse Act appears to have stalled in an open letter to government.... [...]
