Title: Microsoft offers defense against 'ice phishing' crypto scammers
Date: 2022-02-18T11:17:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-18-microsoft-offers-defense-against-ice-phishing-crypto-scammers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/18/microsoft_ice_phishing/){:target="_blank" rel="noopener"}

> All right stop, collaborate and listen, 'soft is back with some theft protection Microsoft has some advice on how to defend against "ice phishing" and other novel attacks that aim to empty cryptocurrency wallets, for those not already abstaining.... [...]
