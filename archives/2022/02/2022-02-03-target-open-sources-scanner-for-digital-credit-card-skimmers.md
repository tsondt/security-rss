Title: Target open sources scanner for digital credit card skimmers
Date: 2022-02-03T12:27:48-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2022-02-03-target-open-sources-scanner-for-digital-credit-card-skimmers

[Source](https://www.bleepingcomputer.com/news/security/target-open-sources-scanner-for-digital-credit-card-skimmers/){:target="_blank" rel="noopener"}

> Target, one of the largest American department store chains and e-commerce retailers, has open sourced 'Merry Maker' - its years-old proprietary scanner for payment card skimming. [...]
