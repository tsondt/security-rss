Title: White House Denies Mulling Massive Cyberattacks Against Russia
Date: 2022-02-25T00:29:32+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Web Security
Slug: 2022-02-25-white-house-denies-mulling-massive-cyberattacks-against-russia

[Source](https://threatpost.com/white-house-denies-mulling-massive-cyberattacks-against-russia/178658/){:target="_blank" rel="noopener"}

> The options reportedly included tampering with trains, electric service and internet connectivity, hampering Russia's military operations in Ukraine. [...]
