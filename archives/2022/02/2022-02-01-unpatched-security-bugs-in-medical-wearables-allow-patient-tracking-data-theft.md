Title: Unpatched Security Bugs in Medical Wearables Allow Patient Tracking, Data Theft
Date: 2022-02-01T21:32:13+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Critical Infrastructure;IoT;Vulnerabilities
Slug: 2022-02-01-unpatched-security-bugs-in-medical-wearables-allow-patient-tracking-data-theft

[Source](https://threatpost.com/unpatched-security-bugs-medical-wearables-patient-tracking-data-theft/178150/){:target="_blank" rel="noopener"}

> Rising critical unpatched vulnerabilities and a lack of encryption leave medical device data defenseless, researcher warn. [...]
