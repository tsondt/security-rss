Title: Breaking 256-bit Elliptic Curve Encryption with a Quantum Computer
Date: 2022-02-09T12:25:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cryptography;IBM;quantum computing
Slug: 2022-02-09-breaking-256-bit-elliptic-curve-encryption-with-a-quantum-computer

[Source](https://www.schneier.com/blog/archives/2022/02/breaking-245-bit-elliptic-curve-encryption-with-a-quantum-computer.html){:target="_blank" rel="noopener"}

> Researchers have calculated the quantum computer size necessary to break 256-bit elliptic curve public-key cryptography: Finally, we calculate the number of physical qubits required to break the 256-bit elliptic curve encryption of keys in the Bitcoin network within the small available time frame in which it would actually pose a threat to do so. It would require 317 × 10 6 physical qubits to break the encryption within one hour using the surface code, a code cycle time of 1 μ s, a reaction time of 10 μ s, and a physical gate error of 10 -3. To instead break [...]
