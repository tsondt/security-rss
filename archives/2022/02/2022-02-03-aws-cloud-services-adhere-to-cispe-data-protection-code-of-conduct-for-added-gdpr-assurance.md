Title: AWS cloud services adhere to CISPE Data Protection Code of Conduct for added GDPR assurance
Date: 2022-02-03T15:09:07+00:00
Author: Chad Woolf
Category: AWS Security
Tags: Announcements;Foundational (100);Learning Levels;CISPE;Data protection;EU Data Protection
Slug: 2022-02-03-aws-cloud-services-adhere-to-cispe-data-protection-code-of-conduct-for-added-gdpr-assurance

[Source](https://aws.amazon.com/blogs/security/aws-cloud-services-adhere-to-cispe-data-protection-code-of-conduct/){:target="_blank" rel="noopener"}

> French version German version I’m happy to announce that AWS has declared 52 services under the Cloud Infrastructure Service Providers Europe Data Protection Code of Conduct (CISPE Code). This provides an independent verification and an added level of assurance to our customers that our cloud services can be used in compliance with the General Data [...]
