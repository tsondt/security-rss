Title: Sharp SIM-Swapping Spike Causes $68M in Losses
Date: 2022-02-10T22:13:33+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Mobile Security;Privacy;Web Security
Slug: 2022-02-10-sharp-sim-swapping-spike-causes-68m-in-losses

[Source](https://threatpost.com/sharp-sim-swapping-spike-losses/178358/){:target="_blank" rel="noopener"}

> The attacks, which lead to 2FA defeat and account takeover, have accelerated by several hundred percent in one year, leading to thousands of drained bank accounts. [...]
