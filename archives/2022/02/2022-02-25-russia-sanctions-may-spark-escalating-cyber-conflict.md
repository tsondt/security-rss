Title: Russia Sanctions May Spark Escalating Cyber Conflict
Date: 2022-02-25T19:10:04+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm
Slug: 2022-02-25-russia-sanctions-may-spark-escalating-cyber-conflict

[Source](https://krebsonsecurity.com/2022/02/russia-sanctions-may-spark-escalating-cyber-conflict/){:target="_blank" rel="noopener"}

> President Biden joined European leaders this week in enacting economic sanctions against Russia in response to its invasion of Ukraine. The West has promised tougher sanctions are coming, but experts warn these will almost certainly trigger a Russian retaliation against America and its allies, which could escalate into cyber attacks on Western financial institutions and energy infrastructure. Michael Daniel is a former cybersecurity advisor to the White House during the Obama administration who now heads the Cyber Threat Alliance, an industry group focused on sharing threat intelligence among members. Daniel said there are two primary types of cyber threats the [...]
