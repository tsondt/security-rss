Title: Russian nation-state hackers targeting US contractors for sensitive defense information, FBI warns
Date: 2022-02-17T13:48:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-17-russian-nation-state-hackers-targeting-us-contractors-for-sensitive-defense-information-fbi-warns

[Source](https://portswigger.net/daily-swig/russian-nation-state-hackers-targeting-us-contractors-for-sensitive-defense-information-fbi-warns){:target="_blank" rel="noopener"}

> Cybersecurity and military secrets among documents accessed [...]
