Title: Ubuntu applies security fixes for all versions back to 14.04
Date: 2022-02-23T15:29:08+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-02-23-ubuntu-applies-security-fixes-for-all-versions-back-to-1404

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/23/ubuntu_kernel_updates/){:target="_blank" rel="noopener"}

> Update those boxes pronto – yes, we're looking at you, users of Trusty Tahr and Xenial Xerus Ubuntu has issued a batch of updates that cover the default as well as the AWS and KVM flavours for the current short-term release 21.10, both the original 5.04 and OEM 5.14 builds for the current 20.04 LTS release, as well as 18.04, and, surprisingly, even 16.04 and 14.04.... [...]
