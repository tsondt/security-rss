Title: DPD Group parcel tracking flaw may have exposed customer data
Date: 2022-02-07T17:30:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-07-dpd-group-parcel-tracking-flaw-may-have-exposed-customer-data

[Source](https://www.bleepingcomputer.com/news/security/dpd-group-parcel-tracking-flaw-may-have-exposed-customer-data/){:target="_blank" rel="noopener"}

> An unauthenticated API call vulnerability in DPD Group's package tracking system could have been exploited to access the personally identifiable details of its clients. [...]
