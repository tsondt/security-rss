Title: Internet 'spy system' delayed because nation can't get the equipment
Date: 2022-02-16T03:45:00+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-16-internet-spy-system-delayed-because-nation-cant-get-the-equipment

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/16/cambodia_internet_gateway_delayed/){:target="_blank" rel="noopener"}

> Quick, blame COVID-19 The government of Cambodia has delayed implementation of its National Internet Gateway – because it is yet to acquire the equipment needed to operate the service.... [...]
