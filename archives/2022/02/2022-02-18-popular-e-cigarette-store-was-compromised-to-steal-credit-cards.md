Title: Popular e-cigarette store was compromised to steal credit cards
Date: 2022-02-18T05:14:55-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-02-18-popular-e-cigarette-store-was-compromised-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/popular-e-cigarette-store-was-compromised-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> BleepingComputer has confirmed Element Vape, a prominent online seller of e-cigarettes and vaping kits was serving a credit card skimmer on its live site, likely after getting hacked. Element Vape has a presence across the U.S. and Canada and sells products in both retail outlets and on their online store. [...]
