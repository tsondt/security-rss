Title: Adobe: Zero-Day Magento 2 RCE Bug Under Active Attack
Date: 2022-02-14T16:48:50+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-02-14-adobe-zero-day-magento-2-rce-bug-under-active-attack

[Source](https://threatpost.com/adobe-zero-day-magento-rce-attack/178407/){:target="_blank" rel="noopener"}

> The vendor issued an emergency fix on Sunday, and eCommerce websites should update ASAP to avoid Magecart card-skimming attacks and other problems. [...]
