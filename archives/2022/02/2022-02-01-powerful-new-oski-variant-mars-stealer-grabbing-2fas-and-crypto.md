Title: Powerful new Oski variant ‘Mars Stealer’ grabbing 2FAs and crypto
Date: 2022-02-01T13:41:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-01-powerful-new-oski-variant-mars-stealer-grabbing-2fas-and-crypto

[Source](https://www.bleepingcomputer.com/news/security/powerful-new-oski-variant-mars-stealer-grabbing-2fas-and-crypto/){:target="_blank" rel="noopener"}

> A new and powerful malware named 'Mars Stealer' has appeared in the wild, and appears to be a redesign of the Oski malware that shut down development abruptly in the summer of 2020. [...]
