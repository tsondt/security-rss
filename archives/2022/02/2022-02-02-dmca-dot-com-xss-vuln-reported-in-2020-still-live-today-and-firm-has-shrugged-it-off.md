Title: DMCA-dot-com XSS vuln reported in 2020 still live today and firm has shrugged it off
Date: 2022-02-02T10:15:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-02-dmca-dot-com-xss-vuln-reported-in-2020-still-live-today-and-firm-has-shrugged-it-off

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/02/dmca_com_live_xss_flaw/){:target="_blank" rel="noopener"}

> Researcher tells world after being stonewalled There is a live cross-site scripting (XSS) vulnerability in takedowns website DMCA-dot-com's user interface. It's existed for more than a year and the site's operators don't appear to be interested in fixing it.... [...]
