Title: Bittersweet Symfony: Devs accidentally turn off CSRF protection in PHP framework
Date: 2022-02-03T16:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-03-bittersweet-symfony-devs-accidentally-turn-off-csrf-protection-in-php-framework

[Source](https://portswigger.net/daily-swig/bittersweet-symfony-devs-accidentally-turn-off-csrf-protection-in-php-framework){:target="_blank" rel="noopener"}

> Inadvertent defense downgrade quickly reverted [...]
