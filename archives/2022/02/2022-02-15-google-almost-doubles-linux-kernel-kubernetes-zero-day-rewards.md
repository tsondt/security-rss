Title: Google almost doubles Linux Kernel, Kubernetes zero-day rewards
Date: 2022-02-15T15:38:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Linux;Security
Slug: 2022-02-15-google-almost-doubles-linux-kernel-kubernetes-zero-day-rewards

[Source](https://www.bleepingcomputer.com/news/google/google-almost-doubles-linux-kernel-kubernetes-zero-day-rewards/){:target="_blank" rel="noopener"}

> Google says it bumped up rewards for reports of Linux Kernel, Kubernetes, Google Kubernetes Engine (GKE), or kCTF vulnerabilities by adding bigger bonuses for zero-day bugs and exploits using unique exploitation techniques. [...]
