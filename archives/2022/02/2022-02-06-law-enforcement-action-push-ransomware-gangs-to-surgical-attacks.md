Title: Law enforcement action push ransomware gangs to surgical attacks
Date: 2022-02-06T10:17:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-06-law-enforcement-action-push-ransomware-gangs-to-surgical-attacks

[Source](https://www.bleepingcomputer.com/news/security/law-enforcement-action-push-ransomware-gangs-to-surgical-attacks/){:target="_blank" rel="noopener"}

> The numerous law enforcement operations leading to the arrests and takedown of ransomware operations in 2021 have forced threat actors to narrow their targeting scope and maximize the efficiency of their operations. [...]
