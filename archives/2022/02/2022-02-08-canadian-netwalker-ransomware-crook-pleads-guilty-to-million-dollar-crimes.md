Title: Canadian Netwalker ransomware crook pleads guilty to million-dollar crimes
Date: 2022-02-08T20:16:49+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-08-canadian-netwalker-ransomware-crook-pleads-guilty-to-million-dollar-crimes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/08/netwalker_ransomware_jailed/){:target="_blank" rel="noopener"}

> Crim has 80 months to think on choices made in life A Canadian who used the Netwalker ransomware to attack 17 organisations and had C$30m (US$23.6m) in cash and Bitcoin when police raided his house has been jailed for more than six years.... [...]
