Title: 2022 may be the year cybercrime returns its focus to consumers
Date: 2022-02-27T10:11:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-27-2022-may-be-the-year-cybercrime-returns-its-focus-to-consumers

[Source](https://www.bleepingcomputer.com/news/security/2022-may-be-the-year-cybercrime-returns-its-focus-to-consumers/){:target="_blank" rel="noopener"}

> Threat analysts expect 2022 to be the tipping point for a shift in the focus of hackers from large companies back to consumers. [...]
