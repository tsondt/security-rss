Title: Cyberattackers Leverage DocuSign to Steal Microsoft Outlook Logins
Date: 2022-02-24T15:08:17+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Cloud Security;Web Security
Slug: 2022-02-24-cyberattackers-leverage-docusign-to-steal-microsoft-outlook-logins

[Source](https://threatpost.com/cyberattackers-docusign-steal-microsoft-outlook-logins/178613/){:target="_blank" rel="noopener"}

> A targeted phishing attack takes aim at a major U.S. payments company. [...]
