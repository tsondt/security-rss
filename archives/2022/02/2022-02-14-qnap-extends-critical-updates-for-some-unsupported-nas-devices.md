Title: QNAP extends critical updates for some unsupported NAS devices
Date: 2022-02-14T13:03:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-14-qnap-extends-critical-updates-for-some-unsupported-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-extends-critical-updates-for-some-unsupported-nas-devices/){:target="_blank" rel="noopener"}

> QNAP has extended support and will keep issuing security updates for some end-of-life (EOL) network-attached storage (NAS) devices until October 2022. [...]
