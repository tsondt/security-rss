Title: Hackers use simple methods to target orgs in the aviation sector
Date: 2022-02-15T07:28:39-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-02-15-hackers-use-simple-methods-to-target-orgs-in-the-aviation-sector

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-simple-methods-to-target-orgs-in-the-aviation-sector/){:target="_blank" rel="noopener"}

> For years, a low-skilled attacker has been using off-the-shelf malware in malicious campaigns aimed at companies in the aviation sector as well as in other sensitive industries. [...]
