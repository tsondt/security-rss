Title: Microsoft is making it harder to steal Windows passwords from memory
Date: 2022-02-13T15:00:50-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-13-microsoft-is-making-it-harder-to-steal-windows-passwords-from-memory

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-is-making-it-harder-to-steal-windows-passwords-from-memory/){:target="_blank" rel="noopener"}

> Microsoft is enabling an 'Attack Surface Reduction' security feature rule by default to block hackers' attempts to steal Windows credentials from the LSASS process. [...]
