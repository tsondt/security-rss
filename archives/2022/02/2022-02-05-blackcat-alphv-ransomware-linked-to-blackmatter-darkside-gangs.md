Title: BlackCat (ALPHV) ransomware linked to BlackMatter, DarkSide gangs
Date: 2022-02-05T17:29:54-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-05-blackcat-alphv-ransomware-linked-to-blackmatter-darkside-gangs

[Source](https://www.bleepingcomputer.com/news/security/blackcat-alphv-ransomware-linked-to-blackmatter-darkside-gangs/){:target="_blank" rel="noopener"}

> The Black Cat ransomware gang, also known as ALPHV, has confirmed they are former members of the notorious BlackMatter/DarkSide ransomware operation. [...]
