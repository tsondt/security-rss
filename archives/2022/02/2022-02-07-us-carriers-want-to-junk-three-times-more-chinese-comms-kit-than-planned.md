Title: US carriers want to junk three times more Chinese comms kit than planned
Date: 2022-02-07T03:32:17+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-07-us-carriers-want-to-junk-three-times-more-chinese-comms-kit-than-planned

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/07/secure_and_trusted_communications_reimbursement_program_overrun/){:target="_blank" rel="noopener"}

> FCC budget to rip and replace Huawei and ZTE kit was $1.9B. It received $5.6B of applications The United States Federal Communications Commission has revealed that carriers have applied for $5.6 billion in funding to rip and replace China-made communications kit.... [...]
