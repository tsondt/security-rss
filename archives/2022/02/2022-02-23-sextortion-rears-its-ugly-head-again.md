Title: Sextortion Rears Its Ugly Head Again
Date: 2022-02-23T17:20:41+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2022-02-23-sextortion-rears-its-ugly-head-again

[Source](https://threatpost.com/sextortion-rears-its-ugly-head-again/178595/){:target="_blank" rel="noopener"}

> Attackers are sending email blasts with malware links in embedded PDFs as a way to evade email filters, lying about having fictional "video evidence." [...]
