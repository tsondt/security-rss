Title: Kill Cloud Risk: Get Everybody to Stop Fighting Over App Security – Podcast
Date: 2022-02-17T14:00:14+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Podcasts;Web Security
Slug: 2022-02-17-kill-cloud-risk-get-everybody-to-stop-fighting-over-app-security-podcast

[Source](https://threatpost.com/killing-cloud-risk-bulletproofing-app-security-podcast/178486/){:target="_blank" rel="noopener"}

> When it comes to ensuring safe cloud app rollouts, there’s flat-out animosity between business shareholders. HackerOne’s Alex Rice and GitLab’s Johnathan Hunt share tips on quashing all the squabbling. [...]
