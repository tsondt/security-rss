Title: How Pure Storage helps customers guard against ransomware
Date: 2022-02-11T08:30:11+00:00
Author: Chris Mellor
Category: The Register
Tags: 
Slug: 2022-02-11-how-pure-storage-helps-customers-guard-against-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/11/pure_storage_ransomware_defences/){:target="_blank" rel="noopener"}

> Don't panic! Paid Feature Ransomware is one of the great scourges of our time. And it's getting worse. We talked to Shawn Rosemarin, global vice president for emerging technology solution sales at Pure Storage, about how the company can help customers protect themselves... [...]
