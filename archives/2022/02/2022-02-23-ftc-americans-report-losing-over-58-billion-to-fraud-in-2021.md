Title: FTC: Americans report losing over $5.8 billion to fraud in 2021
Date: 2022-02-23T08:44:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-23-ftc-americans-report-losing-over-58-billion-to-fraud-in-2021

[Source](https://www.bleepingcomputer.com/news/security/ftc-americans-report-losing-over-58-billion-to-fraud-in-2021/){:target="_blank" rel="noopener"}

> The US Federal Trade Commission (FTC) said today that Americans reported losses of more than $5.8 billion to fraud during last year, a massive total increase of over 70% compared to the losses reported in 2020. [...]
