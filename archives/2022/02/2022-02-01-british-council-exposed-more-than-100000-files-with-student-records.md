Title: British Council exposed more than 100,000 files with student records
Date: 2022-02-01T08:24:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-02-01-british-council-exposed-more-than-100000-files-with-student-records

[Source](https://www.bleepingcomputer.com/news/security/british-council-exposed-more-than-100-000-files-with-student-records/){:target="_blank" rel="noopener"}

> More than 100,000 files with student records belonging to British Council were found exposed online. An unsecured Microsoft Azure blob found on the internet by cybersecurity firm revealed student IDs, names, usernames and email addresses, and other personal information. [...]
