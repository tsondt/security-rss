Title: Living Off the Land: How to Defend Against Malicious Use of Legitimate Utilities
Date: 2022-02-01T14:00:08+00:00
Author: Threatpost
Category: Threatpost
Tags: Sponsored;Uptycs
Slug: 2022-02-01-living-off-the-land-how-to-defend-against-malicious-use-of-legitimate-utilities

[Source](https://threatpost.com/living-off-the-land-malicious-use-legitimate-utilities/177762/){:target="_blank" rel="noopener"}

> LOLBins help attackers become invisible to security platforms. Uptycs provides a rundown of the most commonly abused native utilities for Windows, Linux and macOS – and advice for protection. [...]
