Title: Ukrainian government and banks once again hit by DDoS attacks
Date: 2022-02-23T12:25:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-23-ukrainian-government-and-banks-once-again-hit-by-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-government-and-banks-once-again-hit-by-ddos-attacks/){:target="_blank" rel="noopener"}

> The sites of several Ukrainian government agencies (including the Ministries of Foreign Affairs, Defense, and Internal Affairs, the Security Service, and the Cabinet of Ministers), and of the two largest state-owned banks are again targeted by Distributed Denial-of-Service (DDoS) attacks. [...]
