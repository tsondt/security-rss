Title: US to attack cyber criminals first, ask questions later – if it protects victims
Date: 2022-02-21T04:59:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-21-us-to-attack-cyber-criminals-first-ask-questions-later-if-it-protects-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/21/doj_cyber_offensive_policy/){:target="_blank" rel="noopener"}

> DoJ also creates two teams to prevent abuse of cryptocurrency – who knew that happens? The United States Department of Justice (DoJ) has revealed new policies that may see it undertake pre-emptive action against cyber threats.... [...]
