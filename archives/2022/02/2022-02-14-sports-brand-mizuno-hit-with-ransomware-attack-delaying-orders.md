Title: Sports brand Mizuno hit with ransomware attack delaying orders
Date: 2022-02-14T14:31:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-14-sports-brand-mizuno-hit-with-ransomware-attack-delaying-orders

[Source](https://www.bleepingcomputer.com/news/security/sports-brand-mizuno-hit-with-ransomware-attack-delaying-orders/){:target="_blank" rel="noopener"}

> Sports equipment and sportswear brand Mizuno is affected by phone outages and order delays after being hit by ransomware, BleepingComputer has learned from sources familiar with the attack. [...]
