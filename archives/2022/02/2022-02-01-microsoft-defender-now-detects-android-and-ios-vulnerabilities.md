Title: Microsoft Defender now detects Android and iOS vulnerabilities
Date: 2022-02-01T14:21:47-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-01-microsoft-defender-now-detects-android-and-ios-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-now-detects-android-and-ios-vulnerabilities/){:target="_blank" rel="noopener"}

> Microsoft says threat and vulnerability management support for Android and iOS has reached general availability in Microsoft Defender for Endpoint, the company's enterprise endpoint security platform. [...]
