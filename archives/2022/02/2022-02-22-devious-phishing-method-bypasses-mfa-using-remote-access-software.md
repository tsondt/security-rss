Title: Devious phishing method bypasses MFA using remote access software
Date: 2022-02-22T16:57:16-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-22-devious-phishing-method-bypasses-mfa-using-remote-access-software

[Source](https://www.bleepingcomputer.com/news/security/devious-phishing-method-bypasses-mfa-using-remote-access-software/){:target="_blank" rel="noopener"}

> A devious new phishing technique allows attackers to bypass MFA by secretly having victims log in to their accounts directly on attacker-controlled servers using VNC. [...]
