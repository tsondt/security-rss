Title: Axis Communications shares details on disruptive cyberattack
Date: 2022-02-28T18:20:25-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-28-axis-communications-shares-details-on-disruptive-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/axis-communications-shares-details-on-disruptive-cyberattack/){:target="_blank" rel="noopener"}

> Axis Communications has published a post mortem about a cyberattack that caused severe disruption in their systems, with some systems still partially offline. [...]
