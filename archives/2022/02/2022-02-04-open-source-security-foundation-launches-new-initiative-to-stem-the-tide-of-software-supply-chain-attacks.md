Title: Open Source Security Foundation launches new initiative to stem the tide of software supply chain attacks
Date: 2022-02-04T11:40:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-04-open-source-security-foundation-launches-new-initiative-to-stem-the-tide-of-software-supply-chain-attacks

[Source](https://portswigger.net/daily-swig/open-source-security-foundation-launches-new-initiative-to-stem-the-tide-of-software-supply-chain-attacks){:target="_blank" rel="noopener"}

> Alpha-Omega Project aims to improve software supply chain security for 10,000 OSS projects [...]
