Title: Dutch govt issues data protection report card for Microsoft
Date: 2022-02-23T11:04:05+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-02-23-dutch-govt-issues-data-protection-report-card-for-microsoft

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/23/dpia_microsoft/){:target="_blank" rel="noopener"}

> You need to switch on E2EE in group meetings, watch out for US Cloud Act, warns impact assessment A Data Protection Impact Assessment (DPIA) has been published by a Dutch ministry, noting that Microsoft still has work to do if the country's institutions are to use the company's products without all manner of mitigations.... [...]
