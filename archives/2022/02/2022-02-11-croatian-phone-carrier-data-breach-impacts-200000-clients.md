Title: Croatian phone carrier data breach impacts 200,000 clients
Date: 2022-02-11T14:29:48-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-11-croatian-phone-carrier-data-breach-impacts-200000-clients

[Source](https://www.bleepingcomputer.com/news/security/croatian-phone-carrier-data-breach-impacts-200-000-clients/){:target="_blank" rel="noopener"}

> Croatian phone carrier 'A1 Hrvatska' has disclosed a data breach exposing the personal information of 10% of its customers, roughly 200,000 people. [...]
