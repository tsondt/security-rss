Title: NFL's San Francisco 49ers hit by Blackbyte ransomware attack
Date: 2022-02-13T08:22:11-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-13-nfls-san-francisco-49ers-hit-by-blackbyte-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/nfls-san-francisco-49ers-hit-by-blackbyte-ransomware-attack/){:target="_blank" rel="noopener"}

> The NFL's San Francisco 49ers team is recovering from a cyberattack by the BlackByte ransomware gang who claims to have stolen data from the American football organization. [...]
