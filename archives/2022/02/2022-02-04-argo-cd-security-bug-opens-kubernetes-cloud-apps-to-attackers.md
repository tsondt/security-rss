Title: Argo CD Security Bug Opens Kubernetes Cloud Apps to Attackers
Date: 2022-02-04T18:26:07+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2022-02-04-argo-cd-security-bug-opens-kubernetes-cloud-apps-to-attackers

[Source](https://threatpost.com/argo-cd-security-bug-kubernetes-cloud-apps/178239/){:target="_blank" rel="noopener"}

> The popular continuous-delivery platform has a path-traversal bug (CVE-2022-24348) that could allow cyberattackers to hop from one application ecosystem to another. [...]
