Title: Cloud SQL launches support for IAM Conditions and Tags
Date: 2022-02-17T17:00:00+00:00
Author: Akhil Jariwala
Category: GCP Security
Tags: Identity & Security;Databases
Slug: 2022-02-17-cloud-sql-launches-support-for-iam-conditions-and-tags

[Source](https://cloud.google.com/blog/products/databases/cloud-sql-launches-iam-conditions-and-tags/){:target="_blank" rel="noopener"}

> If you are responsible for administering Cloud SQL instances, securing your databases is one of your top priorities. However, as the number of users using your Cloud SQL instances grows, you may find yourself challenged to keep access locked down. More Cloud SQL users means more work ensuring that the right users have the appropriate access to the proper database instances. In the past, you may have addressed this challenge by separating Cloud SQL instances into distinct projects, isolating access along project boundaries. However, this approach can lead to a sprawling number of projects that can be burdensome to manage. [...]
