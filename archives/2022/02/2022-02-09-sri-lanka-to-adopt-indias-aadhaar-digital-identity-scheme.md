Title: Sri Lanka to adopt India’s Aadhaar digital identity scheme
Date: 2022-02-09T07:03:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-09-sri-lanka-to-adopt-indias-aadhaar-digital-identity-scheme

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/09/sri_lanka_to_adopt_indias/){:target="_blank" rel="noopener"}

> Biometric IDs for all, cross-border interoperability not on the table Sri Lanka has decided to adopt a national digital identity framework based on biometric data and will ask India if it can implement that nation’s Aadhaar scheme.... [...]
