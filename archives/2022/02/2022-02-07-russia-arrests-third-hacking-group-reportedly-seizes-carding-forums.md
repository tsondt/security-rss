Title: Russia arrests third hacking group, reportedly seizes carding forums
Date: 2022-02-07T16:39:38-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-07-russia-arrests-third-hacking-group-reportedly-seizes-carding-forums

[Source](https://www.bleepingcomputer.com/news/security/russia-arrests-third-hacking-group-reportedly-seizes-carding-forums/){:target="_blank" rel="noopener"}

> Russia arrested six people today, allegedly part of a hacking group that was involved in the theft and selling of stolen credit cards. [...]
