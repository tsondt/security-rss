Title: Critical Cisco Bugs Open VPN Routers to Cyberattacks
Date: 2022-02-03T20:15:54+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-02-03-critical-cisco-bugs-open-vpn-routers-to-cyberattacks

[Source](https://threatpost.com/critical-cisco-bugs-vpn-routers-cyberattacks/178199/){:target="_blank" rel="noopener"}

> The company's RV line of small-business routers contains 15 different security vulnerabilities that could enable everything from RCE to corporate network access and denial-of-service – and many have exploits circulating. [...]
