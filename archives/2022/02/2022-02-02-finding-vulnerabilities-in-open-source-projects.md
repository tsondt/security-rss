Title: Finding Vulnerabilities in Open Source Projects
Date: 2022-02-02T15:58:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Google;Microsoft;open source;vulnerabilities
Slug: 2022-02-02-finding-vulnerabilities-in-open-source-projects

[Source](https://www.schneier.com/blog/archives/2022/02/finding-vulnerabilities-in-open-source-projects.html){:target="_blank" rel="noopener"}

> The Open Source Security Foundation announced $10 million in funding from a pool of tech and financial companies, including $5 million from Microsoft and Google, to find vulnerabilities in open source projects: The “Alpha” side will emphasize vulnerability testing by hand in the most popular open-source projects, developing close working relationships with a handful of the top 200 projects for testing each year. “Omega” will look more at the broader landscape of open source, running automated testing on the top 10,000. This is an excellent idea. This code ends up in all sorts of critical applications. Log4j would be a [...]
