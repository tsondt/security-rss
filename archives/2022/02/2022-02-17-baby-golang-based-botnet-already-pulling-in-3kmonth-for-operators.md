Title: Baby Golang-Based Botnet Already Pulling in $3K/Month for Operators
Date: 2022-02-17T17:28:02+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-02-17-baby-golang-based-botnet-already-pulling-in-3kmonth-for-operators

[Source](https://threatpost.com/golang-botnet-pulling-in-3k-month/178509/){:target="_blank" rel="noopener"}

> Newborn as it is, the Kraken botnet has already spread like wildfire, thanks to the malware's author tinkering away over the past few months, adding more infostealers and backdoors. [...]
