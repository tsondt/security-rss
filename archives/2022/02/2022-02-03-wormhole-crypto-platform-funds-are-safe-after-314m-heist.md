Title: Wormhole Crypto Platform: ‘Funds Are Safe’ After $314M Heist
Date: 2022-02-03T18:28:14+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security
Slug: 2022-02-03-wormhole-crypto-platform-funds-are-safe-after-314m-heist

[Source](https://threatpost.com/wormhole-crypto-funds-safe-heist/178189/){:target="_blank" rel="noopener"}

> The popular bridge, which connects Ethereum, Solana blockchain & more, was shelled out by it's-not-saying. Wormhole is trying to negotiate with the attacker. [...]
