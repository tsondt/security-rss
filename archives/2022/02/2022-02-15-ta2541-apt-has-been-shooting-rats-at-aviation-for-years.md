Title: TA2541: APT Has Been Shooting RATs at Aviation for Years
Date: 2022-02-15T14:02:07+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;News;Web Security
Slug: 2022-02-15-ta2541-apt-has-been-shooting-rats-at-aviation-for-years

[Source](https://threatpost.com/ta2541-apt-rats-aviation/178422/){:target="_blank" rel="noopener"}

> Since 2017, the attacker has flung simple off-the-shelf malware in malicious email campaigns aimed at aviation, aerospace, transportation and defense. [...]
