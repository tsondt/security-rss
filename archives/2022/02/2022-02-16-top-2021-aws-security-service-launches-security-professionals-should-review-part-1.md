Title: Top 2021 AWS Security service launches security professionals should review – Part 1
Date: 2022-02-16T20:14:20+00:00
Author: Ryan Holland
Category: AWS Security
Tags: Foundational (100);Security;Security, Identity, & Compliance;Thought Leadership;2021 recap;Amazon CodeGuru;Amazon Detective;Amazon GuardDuty;Amazon Inspector;Amazon Macie;AWS Key Management Service;AWS KMS;AWS Secrets Manager;AWS security;AWS Security Hub;AWS Security Team
Slug: 2022-02-16-top-2021-aws-security-service-launches-security-professionals-should-review-part-1

[Source](https://aws.amazon.com/blogs/security/top-2021-aws-security-service-launches-part-1/){:target="_blank" rel="noopener"}

> Given the speed of Amazon Web Services (AWS) innovation, it can sometimes be challenging to keep up with AWS Security service and feature launches. To help you stay current, here’s an overview of some of the most important 2021 AWS Security launches that security professionals should be aware of. This is the first of two [...]
