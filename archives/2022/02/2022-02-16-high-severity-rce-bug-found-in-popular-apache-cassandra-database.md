Title: High-Severity RCE Bug Found in Popular Apache Cassandra Database
Date: 2022-02-16T16:03:32+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-02-16-high-severity-rce-bug-found-in-popular-apache-cassandra-database

[Source](https://threatpost.com/high-severity-rce-bug-found-in-popular-apache-cassandra-database/178464/){:target="_blank" rel="noopener"}

> On the plus side, only instances with non-standard not recommended configurations are vulnerable. On the downside, those configurations aren't easy to track down, and it's easy as pie to exploit. [...]
