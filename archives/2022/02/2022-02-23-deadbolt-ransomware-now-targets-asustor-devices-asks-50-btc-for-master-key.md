Title: DeadBolt ransomware now targets ASUSTOR devices, asks 50 BTC for master key
Date: 2022-02-23T12:57:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-23-deadbolt-ransomware-now-targets-asustor-devices-asks-50-btc-for-master-key

[Source](https://www.bleepingcomputer.com/news/security/deadbolt-ransomware-now-targets-asustor-devices-asks-50-btc-for-master-key/){:target="_blank" rel="noopener"}

> The DeadBolt ransomware is now targeting ASUSTOR NAS devices by encrypting files and demanding a $1,150 ransom in bitcoins. [...]
