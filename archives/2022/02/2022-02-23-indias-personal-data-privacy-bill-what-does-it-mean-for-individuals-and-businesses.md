Title: <span>India’s Personal Data Privacy Bill: What does it mean for individuals and businesses?</span>
Date: 2022-02-23T13:44:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-23-indias-personal-data-privacy-bill-what-does-it-mean-for-individuals-and-businesses

[Source](https://portswigger.net/daily-swig/indias-personal-data-privacy-bill-what-does-it-mean-for-individuals-and-businesses){:target="_blank" rel="noopener"}

> New legislation sets out to bring India in line with international best practice, but what will this look like in action? [...]
