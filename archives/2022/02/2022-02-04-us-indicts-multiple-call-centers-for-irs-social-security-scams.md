Title: US indicts multiple call centers for IRS, Social Security scams
Date: 2022-02-04T12:02:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government;Legal
Slug: 2022-02-04-us-indicts-multiple-call-centers-for-irs-social-security-scams

[Source](https://www.bleepingcomputer.com/news/security/us-indicts-multiple-call-centers-for-irs-social-security-scams/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice has announced the indictment of several India-based call centers and their directors for targeting Americans with Social Security, IRS, and loan phone call scams. [...]
