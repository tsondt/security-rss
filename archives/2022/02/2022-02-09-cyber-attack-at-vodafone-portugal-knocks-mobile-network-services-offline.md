Title: Cyber-attack at Vodafone Portugal knocks mobile network services offline
Date: 2022-02-09T14:05:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-09-cyber-attack-at-vodafone-portugal-knocks-mobile-network-services-offline

[Source](https://portswigger.net/daily-swig/cyber-attack-at-vodafone-portugal-knocks-mobile-network-services-offline){:target="_blank" rel="noopener"}

> No customer data was accessed, company claims [...]
