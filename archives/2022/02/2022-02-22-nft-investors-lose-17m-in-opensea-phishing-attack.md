Title: NFT Investors Lose $1.7M in OpenSea Phishing Attack
Date: 2022-02-22T03:12:30+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2022-02-22-nft-investors-lose-17m-in-opensea-phishing-attack

[Source](https://threatpost.com/nft-investors-lose-1-7m-in-opensea-phishing-attack/178558/){:target="_blank" rel="noopener"}

> Attackers took advantage of a smart-contract migration to swindle 17 users. [...]
