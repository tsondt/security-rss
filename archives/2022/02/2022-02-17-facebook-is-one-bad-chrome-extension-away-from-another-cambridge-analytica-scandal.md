Title: Facebook is one bad Chrome extension away from another Cambridge Analytica scandal
Date: 2022-02-17T09:28:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-17-facebook-is-one-bad-chrome-extension-away-from-another-cambridge-analytica-scandal

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/17/chrome_meta_token/){:target="_blank" rel="noopener"}

> Meta says it's on top of access token abuse but also needs Google's help Analysis Multiple Chrome browser extensions make use of a session token for Meta's Facebook that grants access to signed-in users' social network data in a way that violates the company's policies and leaves users open to potential privacy violations.... [...]
