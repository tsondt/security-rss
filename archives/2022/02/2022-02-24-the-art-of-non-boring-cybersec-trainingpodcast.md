Title: The Art of Non-boring Cybersec Training–Podcast
Date: 2022-02-24T14:00:50+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Hacks;Malware;Mobile Security;Podcasts;Vulnerabilities;Web Security
Slug: 2022-02-24-the-art-of-non-boring-cybersec-trainingpodcast

[Source](https://threatpost.com/the-art-of-non-boring-cybersec-training-podcast/178594/){:target="_blank" rel="noopener"}

> With human error being the common factor in most cyberattacks, employee training has got to get better. To that end, Trustwave cybersec training expert Darren Van Booven explains the importance of fish stress balls and management buy-in. [...]
