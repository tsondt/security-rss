Title: No Critical Bugs for Microsoft February 2022 Patch Tuesday, 1 Zero-Day
Date: 2022-02-08T20:24:17+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-02-08-no-critical-bugs-for-microsoft-february-2022-patch-tuesday-1-zero-day

[Source](https://threatpost.com/microsoft-february-patch-tuesday-zero-day/178286/){:target="_blank" rel="noopener"}

> This batch had zero critical CVEs, which is unheard of. Most (50) of the patches are labeled Important, so don't delay to apply the patches, security experts said. [...]
