Title: KP Snacks giant hit by Conti ransomware, deliveries disrupted
Date: 2022-02-02T11:49:49-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-02-02-kp-snacks-giant-hit-by-conti-ransomware-deliveries-disrupted

[Source](https://www.bleepingcomputer.com/news/security/kp-snacks-giant-hit-by-conti-ransomware-deliveries-disrupted/){:target="_blank" rel="noopener"}

> KP Snacks, a major producer of popular British snacks has been hit by the Conti ransomware group affecting distribution to leading supermarkets. [...]
