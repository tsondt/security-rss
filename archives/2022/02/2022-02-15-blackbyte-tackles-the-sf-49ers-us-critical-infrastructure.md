Title: BlackByte Tackles the SF 49ers & US Critical Infrastructure
Date: 2022-02-15T02:04:36+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;News;Web Security
Slug: 2022-02-15-blackbyte-tackles-the-sf-49ers-us-critical-infrastructure

[Source](https://threatpost.com/blackbyte-tackles-the-sf-49ers-us-critical-infrastructure/178416/){:target="_blank" rel="noopener"}

> Hours before the Superbowl and two days after the FBI warned about the ransomware gang, BlackByte leaked what are purportedly the NFL team's files. [...]
