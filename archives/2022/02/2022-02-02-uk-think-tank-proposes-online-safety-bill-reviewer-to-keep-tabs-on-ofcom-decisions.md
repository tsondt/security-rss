Title: UK think tank proposes Online Safety Bill reviewer to keep tabs on Ofcom decisions
Date: 2022-02-02T16:09:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-02-uk-think-tank-proposes-online-safety-bill-reviewer-to-keep-tabs-on-ofcom-decisions

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/02/iea_online_safety_bill_reviewer_proposal/){:target="_blank" rel="noopener"}

> Terror watchdog is a bad model to follow, though Even think tanks with close links to the UK's Conservative government are now criticising the Online Safety Bill, with the Institute of Economic Affairs (IEA) describing it today as "a significant threat to freedom of speech, privacy and innovation."... [...]
