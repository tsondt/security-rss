Title: UK, US, Australia issue joint advisory: Ransomware on the loose, critical national infrastructure affected
Date: 2022-02-09T16:28:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-09-uk-us-australia-issue-joint-advisory-ransomware-on-the-loose-critical-national-infrastructure-affected

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/09/uk_us_au_ransomware_warning/){:target="_blank" rel="noopener"}

> Don't pay extortionists? Firms shelled out $5bn in Bitcoin in 6 months Ransomware attacks are proliferating as criminals turn to gangs providing turnkey post-compromise services, Britain's National Cyber Security Centre (NCSC) has warned.... [...]
