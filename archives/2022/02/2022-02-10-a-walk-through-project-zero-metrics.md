Title: A walk through Project Zero metrics
Date: 2022-02-10T08:58:00-08:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2022-02-10-a-walk-through-project-zero-metrics

[Source](https://googleprojectzero.blogspot.com/2022/02/a-walk-through-project-zero-metrics.html){:target="_blank" rel="noopener"}

> Posted by Ryan Schoen, Project Zero tl;dr In 2021, vendors took an average of 52 days to fix security vulnerabilities reported from Project Zero. This is a significant acceleration from an average of about 80 days 3 years ago. In addition to the average now being well below the 90-day deadline, we have also seen a dropoff in vendors missing the deadline (or the additional 14-day grace period ). In 2021, only one bug exceeded its fix deadline, though 14% of bugs required the grace period. Differences in the amount of time it takes a vendor/product to ship a fix [...]
