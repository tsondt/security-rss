Title: Free Android app lets users detect Apple AirTag tracking
Date: 2022-02-26T10:07:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-02-26-free-android-app-lets-users-detect-apple-airtag-tracking

[Source](https://www.bleepingcomputer.com/news/security/free-android-app-lets-users-detect-apple-airtag-tracking/){:target="_blank" rel="noopener"}

> A small team of researchers at the Darmstadt University in Germany have published a report illustrating how their AirGuard app for Android provides better protection from stealthy AirTag stalking than other apps. [...]
