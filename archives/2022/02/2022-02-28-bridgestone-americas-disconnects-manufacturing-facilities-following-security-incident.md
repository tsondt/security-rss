Title: Bridgestone Americas ‘disconnects’ manufacturing facilities following ‘security incident’
Date: 2022-02-28T12:26:30+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-28-bridgestone-americas-disconnects-manufacturing-facilities-following-security-incident

[Source](https://portswigger.net/daily-swig/bridgestone-americas-disconnects-manufacturing-facilities-following-security-incident){:target="_blank" rel="noopener"}

> World’s biggest tire manufacturer yet to determine ‘scope or nature of any potential incident’ [...]
