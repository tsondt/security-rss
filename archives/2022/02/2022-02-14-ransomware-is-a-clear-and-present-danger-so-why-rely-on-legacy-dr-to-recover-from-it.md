Title: Ransomware is a clear and present danger. So why rely on legacy DR to recover from it?
Date: 2022-02-14T18:00:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-02-14-ransomware-is-a-clear-and-present-danger-so-why-rely-on-legacy-dr-to-recover-from-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/14/ransomware_legacy_dr/){:target="_blank" rel="noopener"}

> Find out right here how zero trust and modern disaster recovery can save you Webinar If disaster strikes your organization, you’ll want to restore your data to precisely where it was seconds before. So, why would you rely on data protection tooling designed for the early 21st century?... [...]
