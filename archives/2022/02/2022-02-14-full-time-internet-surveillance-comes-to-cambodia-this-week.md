Title: Full-time internet surveillance comes to Cambodia this week
Date: 2022-02-14T05:57:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-14-full-time-internet-surveillance-comes-to-cambodia-this-week

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/14/cambodia_national_internet_gateway/){:target="_blank" rel="noopener"}

> Locals fear sharing their views on new National Internet Gateway Cambodia’s National Internet Gateway comes online this Wednesday, exposing all traffic within the country to pervasive government surveillance.... [...]
