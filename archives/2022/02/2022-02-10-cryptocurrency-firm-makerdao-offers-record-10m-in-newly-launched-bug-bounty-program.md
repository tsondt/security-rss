Title: Cryptocurrency firm MakerDAO offers record $10m in newly launched bug bounty program
Date: 2022-02-10T19:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-10-cryptocurrency-firm-makerdao-offers-record-10m-in-newly-launched-bug-bounty-program

[Source](https://portswigger.net/daily-swig/cryptocurrency-firm-makerdao-offers-record-10m-in-newly-launched-bug-bounty-program){:target="_blank" rel="noopener"}

> Chance to become an instant multimillionaire via flaws in DAI smart contracts, websites, and apps [...]
