Title: CISA warns of hybrid operations threat to US critical infrastructure
Date: 2022-02-19T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-19-cisa-warns-of-hybrid-operations-threat-to-us-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-of-hybrid-operations-threat-to-us-critical-infrastructure/){:target="_blank" rel="noopener"}

> CISA urged leaders of U.S. critical infrastructure organizations on Friday to increase their orgs' resilience against a growing risk of being targeted by foreign influence operations using misinformation, disinformation, and malformation (MDM) tactics. [...]
