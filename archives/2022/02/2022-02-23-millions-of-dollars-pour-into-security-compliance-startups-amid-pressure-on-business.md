Title: Millions of dollars pour into security compliance startups amid pressure on business
Date: 2022-02-23T22:04:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-02-23-millions-of-dollars-pour-into-security-compliance-startups-amid-pressure-on-business

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/23/secureframe_security_compliance_investment/){:target="_blank" rel="noopener"}

> There's gold in them thar forms Government agencies and industry groups are putting increasing pressure on enterprises to ensure their systems, and the vast amounts of data they are holding, are protected against the growing threat of ransomware and others cyber-attacks.... [...]
