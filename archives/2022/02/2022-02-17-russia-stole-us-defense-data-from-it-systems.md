Title: Russia 'stole US defense data' from IT systems
Date: 2022-02-17T01:50:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-17-russia-stole-us-defense-data-from-it-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/17/cisa_russian_attacks/){:target="_blank" rel="noopener"}

> Clearly no need for leet zero-day hax when you can spearphish and exploit months-old vulnerabilities A two-year campaign by state-sponsored Russian entities to siphon information from US defense contractors worked, it is claimed.... [...]
