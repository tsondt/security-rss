Title: Experience is really everything with SASE
Date: 2022-02-08T07:30:11+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: 2022-02-08-experience-is-really-everything-with-sase

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/08/trustmarque_sase_profile/){:target="_blank" rel="noopener"}

> Don’t just worry about today, get ready for tomorrow Paid Feature Ask a Network or Security Manager if their network is secure, and the answer - typically - is “yes, of course”. So says Andrew Want, Chief Technologist at Trustmarque, the UK value-add services firm.... [...]
