Title: How Phishers Are Slinking Their Links Into LinkedIn
Date: 2022-02-03T18:49:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;Amazon;Avanan;Check Point;Fortinet;irs;Jeremy Fuchs;LinkedIn;microsoft;Paypal;Slinks
Slug: 2022-02-03-how-phishers-are-slinking-their-links-into-linkedin

[Source](https://krebsonsecurity.com/2022/02/how-phishers-are-slinking-their-links-into-linkedin/){:target="_blank" rel="noopener"}

> If you received a link to LinkedIn.com via email, SMS or instant message, would you click it? Spammers, phishers and other ne’er-do-wells are hoping you will, because they’ve long taken advantage of a marketing feature on the business networking site which lets them create a LinkedIn.com link that bounces your browser to other websites, such as phishing pages that mimic top online brands (but chiefly Linkedin’s parent firm Microsoft ). At issue is a “redirect” feature available to businesses that chose to market through LinkedIn.com. The LinkedIn redirect links allow customers to track the performance of ad campaigns, while promoting [...]
