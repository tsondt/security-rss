Title: KP Snacks hit by ransomware: Crisps and nuts firm KO'd by modern scourge
Date: 2022-02-03T17:17:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-03-kp-snacks-hit-by-ransomware-crisps-and-nuts-firm-kod-by-modern-scourge

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/03/kp_snacks_ransomware/){:target="_blank" rel="noopener"}

> Firm doesn't know when it'll restart salty goodness deliveries Some of Britain's favourite pub munch could end up in short supply after KP Snacks, makers of nuts and crisps, suffered a ransomware attack.... [...]
