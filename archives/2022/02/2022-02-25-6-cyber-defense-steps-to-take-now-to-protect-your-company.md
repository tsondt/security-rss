Title: 6 Cyber-Defense Steps to Take Now to Protect Your Company
Date: 2022-02-25T18:49:10+00:00
Author: Daniel Spicer
Category: Threatpost
Tags: InfoSec Insider;Malware
Slug: 2022-02-25-6-cyber-defense-steps-to-take-now-to-protect-your-company

[Source](https://threatpost.com/latest-insights-ransomware-threats/178391/){:target="_blank" rel="noopener"}

> Ransomware is getting worse, but Daniel Spicer, chief security officer at Ivanti, offers a checklist for choosing defense solutions to meet the challenge. [...]
