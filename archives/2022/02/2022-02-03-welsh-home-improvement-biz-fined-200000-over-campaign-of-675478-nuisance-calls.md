Title: Welsh home improvement biz fined £200,000 over campaign of 675,478 nuisance calls
Date: 2022-02-03T10:27:56+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-02-03-welsh-home-improvement-biz-fined-200000-over-campaign-of-675478-nuisance-calls

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/03/ico_home2sense_fine/){:target="_blank" rel="noopener"}

> ICO says Home2Sense showed 'complete disregard for people's privacy' Home2Sense Ltd, a home improvement biz, is nursing a £200,000 financial penalty from the UK's data watchdog for making well over half a million marketing calls to people that registered to opt out of such botheration.... [...]
