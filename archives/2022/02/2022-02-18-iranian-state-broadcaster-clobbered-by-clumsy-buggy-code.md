Title: Iranian State Broadcaster Clobbered by ‘Clumsy, Buggy’ Code
Date: 2022-02-18T13:46:04+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks;Malware;Web Security
Slug: 2022-02-18-iranian-state-broadcaster-clobbered-by-clumsy-buggy-code

[Source](https://threatpost.com/iranian-state-broadcaster-clumsy-buggy-code/178524/){:target="_blank" rel="noopener"}

> Researchers said a Jan. 27 attack that aired footage of opposition leaders calling for assassination of Iran’s Supreme Leader was a clumsy and unsophisticated wiper attack. [...]
