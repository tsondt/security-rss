Title: When you’re fending off cyber-attackers, it is possible to be just too tooled up
Date: 2022-02-18T07:30:09+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-02-18-when-youre-fending-off-cyber-attackers-it-is-possible-to-be-just-too-tooled-up

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/18/fending_off_cyber_attackers/){:target="_blank" rel="noopener"}

> This webinar will help you put insight first Webinar Organizations face more cyber-threats than ever before, leading many security teams to implement increasing numbers of tools and controls in response.... [...]
