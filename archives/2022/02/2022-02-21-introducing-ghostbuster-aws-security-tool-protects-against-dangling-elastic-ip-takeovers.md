Title: <span>Introducing Ghostbuster – AWS security tool protects against dangling elastic IP takeovers&nbsp;</span>
Date: 2022-02-21T15:00:05+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-21-introducing-ghostbuster-aws-security-tool-protects-against-dangling-elastic-ip-takeovers

[Source](https://portswigger.net/daily-swig/introducing-ghostbuster-aws-security-tool-protects-against-dangling-elastic-ip-takeovers-nbsp){:target="_blank" rel="noopener"}

> New defense against attacks that can cause more damage than other flavors of subdomain takeover [...]
