Title: Critical 'remote escalation' flaw in Android 12 fixed in Feb security patch batch
Date: 2022-02-09T08:28:11+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-02-09-critical-remote-escalation-flaw-in-android-12-fixed-in-feb-security-patch-batch

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/09/android_security_bulletin/){:target="_blank" rel="noopener"}

> This is the final software update from Google for the Pixel 3, 3 XL, too The February edition of Google's monthly Android security update tackles, among other vulnerabilities, an eyebrow-raising critical flaw in Android 12.... [...]
