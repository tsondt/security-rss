Title: Cisco bug can let hackers crash Cisco Secure Email gateways
Date: 2022-02-17T11:26:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-17-cisco-bug-can-let-hackers-crash-cisco-secure-email-gateways

[Source](https://www.bleepingcomputer.com/news/security/cisco-bug-can-let-hackers-crash-cisco-secure-email-gateways/){:target="_blank" rel="noopener"}

> Cisco has addressed a high severity vulnerability that could allow remote attackers to crash Cisco Secure Email appliances using maliciously crafted email messages. [...]
