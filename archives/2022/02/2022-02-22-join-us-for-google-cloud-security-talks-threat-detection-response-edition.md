Title: Join us for Google Cloud Security Talks: Threat Detection & Response Edition
Date: 2022-02-22T17:00:00+00:00
Author: Lorenz Jakober
Category: GCP Security
Tags: Events;Identity & Security
Slug: 2022-02-22-join-us-for-google-cloud-security-talks-threat-detection-response-edition

[Source](https://cloud.google.com/blog/products/identity-security/join-us-for-google-cloud-security-talks-threat-detection-response-edition/){:target="_blank" rel="noopener"}

> Learn about everything related to threat detection, investigation and response in our Q1 2022 Google Cloud Security Talks Join us for our first Google Cloud Security Talks of 2022, a live online event on March 9th, where we’ll focus on all things SecOps, and threat detection, investigation and response across on-premise, cloud, and hybrid environments. During this multi-session digital event, learn how you can modernize SecOps in your organization and enhance your approach to threat detection, investigation and response, with sessions including: A keynote from Jess Leroy and Amos Stern to kick off Security Talks in 2022. They will provide [...]
