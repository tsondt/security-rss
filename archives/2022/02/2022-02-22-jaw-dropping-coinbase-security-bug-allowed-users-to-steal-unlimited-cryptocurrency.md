Title: Jaw-dropping Coinbase security bug allowed users to steal unlimited cryptocurrency
Date: 2022-02-22T11:37:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-22-jaw-dropping-coinbase-security-bug-allowed-users-to-steal-unlimited-cryptocurrency

[Source](https://portswigger.net/daily-swig/jaw-dropping-coinbase-security-bug-allowed-users-to-steal-unlimited-cryptocurrency){:target="_blank" rel="noopener"}

> Researcher nets $250,000 for ‘potentially market-nuking’ vulnerability [...]
