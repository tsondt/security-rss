Title: Supply-Chain Security Is Not a Problem…It’s a Predicament
Date: 2022-02-02T19:23:41+00:00
Author: Sounil Yu
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Mobile Security;Vulnerabilities;Web Security
Slug: 2022-02-02-supply-chain-security-is-not-a-problemits-a-predicament

[Source](https://threatpost.com/supply-chain-security-predicament/178166/){:target="_blank" rel="noopener"}

> Despite what security vendors might say, there is no way to comprehensively solve our supply-chain security challenges, posits JupiterOne CISO Sounil Yu. We can only manage them. [...]
