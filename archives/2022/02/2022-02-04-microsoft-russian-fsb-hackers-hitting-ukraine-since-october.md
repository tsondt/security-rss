Title: Microsoft: Russian FSB hackers hitting Ukraine since October
Date: 2022-02-04T15:17:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-04-microsoft-russian-fsb-hackers-hitting-ukraine-since-october

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-russian-fsb-hackers-hitting-ukraine-since-october/){:target="_blank" rel="noopener"}

> Microsoft said today that a Russian hacking group known as Gamaredon has been behind a streak of spear-phishing emails targeting Ukrainian entities and organizations related to Ukrainian affairs since October 2021. [...]
