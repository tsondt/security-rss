Title: Researchers find threat group that has been active for 5 years
Date: 2022-02-15T18:23:25+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;data theft;malware;trojans
Slug: 2022-02-15-researchers-find-threat-group-that-has-been-active-for-5-years

[Source](https://arstechnica.com/?p=1834480){:target="_blank" rel="noopener"}

> Enlarge / Warning: Data transfer in progress (credit: Yuri_Arcurs/Getty Images) Researchers on Tuesday revealed a new threat actor that over the past five years has blasted thousands of organizations with an almost endless stream of malicious messages designed to infect systems with data-stealing malware. TA2541, as security firm Proofpoint has named the hacking group, has been active since at least 2017, when company researchers started tracking it. The group uses relatively crude tactics, techniques, and procedures, or TTPs, to target organizations in the aviation, aerospace, transportation, manufacturing, and defense industries. These TTPs include the use of malicious Google Drive links [...]
