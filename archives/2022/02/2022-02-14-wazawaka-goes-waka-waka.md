Title: Wazawaka Goes Waka Waka
Date: 2022-02-14T18:22:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Ransomware;Babuk;Babuk ransomware;Biba99;Boriselcin;Cisco Talos;CVE-2021-20028;Dmitry Smilyanets;Groove ransom;Mikhail Pavlovich Matveev;Orange;RaidForums;RAMP;SonicWall VPN;teresacox19963@gmail.com;ToX;Verified;Washington Metropolitan Police Department;Wazawaka
Slug: 2022-02-14-wazawaka-goes-waka-waka

[Source](https://krebsonsecurity.com/2022/02/wazawaka-goes-waka-waka/){:target="_blank" rel="noopener"}

> In January, KrebsOnSecurity examined clues left behind by “ Wazawaka,” the hacker handle chosen by a major ransomware criminal in the Russian-speaking cybercrime scene. Wazawaka has since “lost his mind” according to his erstwhile colleagues, creating a Twitter account to drop exploit code for a widely-used virtual private networking (VPN) appliance, and publishing bizarre selfie videos taunting security researchers and journalists. Wazawaka, a.k.a. Mikhail P. Matveev, a.k.a. “Orange,” a.k.a. “Boriselcin,” showing off his missing ring finger. In last month’s story, we explored clues that led from Wazawaka’s multitude of monikers, email addresses, and passwords to a 30-something father in Abakan, [...]
