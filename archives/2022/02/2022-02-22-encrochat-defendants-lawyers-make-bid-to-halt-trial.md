Title: EncroChat defendants' lawyers make bid to halt trial
Date: 2022-02-22T10:29:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-22-encrochat-defendants-lawyers-make-bid-to-halt-trial

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/22/encrochat_lawyers_complain_unfair_trials/){:target="_blank" rel="noopener"}

> Good luck finding public support for that one Lawyers for EncroChat encrypted phone users have begged the EU to halt court cases using evidence from the compromised mobile network, saying evidence disclosure breaches the political bloc's laws.... [...]
