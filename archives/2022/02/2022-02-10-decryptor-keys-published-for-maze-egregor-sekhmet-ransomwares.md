Title: Decryptor Keys Published for Maze, Egregor, Sekhmet Ransomwares
Date: 2022-02-10T23:16:44+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-02-10-decryptor-keys-published-for-maze-egregor-sekhmet-ransomwares

[Source](https://threatpost.com/decryptor-keys-maze-egregor-sekhmet-ransomwares/178363/){:target="_blank" rel="noopener"}

> The Maze gang are purportedly never going back to ransomware and have destroyed all of their ransomware source code, said somebody claiming to be the developer. [...]
