Title: Cyberattackers Cook Up Employee Personal Data Heist for Meyer
Date: 2022-02-22T20:41:48+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Malware
Slug: 2022-02-22-cyberattackers-cook-up-employee-personal-data-heist-for-meyer

[Source](https://threatpost.com/cyberattackers-employee-personal-data-meyer/178570/){:target="_blank" rel="noopener"}

> The Conti gang breached the cookware giant's network, prepping thousands of employees’ personal data for consumption by cybercrooks. [...]
