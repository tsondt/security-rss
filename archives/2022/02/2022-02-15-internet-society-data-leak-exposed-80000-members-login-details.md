Title: <span>Internet Society data leak exposed 80,000 members’ login details</span>
Date: 2022-02-15T12:54:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-15-internet-society-data-leak-exposed-80000-members-login-details

[Source](https://portswigger.net/daily-swig/internet-society-data-leak-exposed-80-000-members-login-details){:target="_blank" rel="noopener"}

> Champion of an open, secure internet blames breach on third-party oversight The Internet Society (ISOC), a non-profit dedicated to keeping the internet open and secure, has blamed the inadvertent expo [...]
