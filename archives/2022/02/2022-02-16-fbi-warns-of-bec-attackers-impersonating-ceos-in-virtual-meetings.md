Title: FBI warns of BEC attackers impersonating CEOs in virtual meetings
Date: 2022-02-16T13:09:29-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-16-fbi-warns-of-bec-attackers-impersonating-ceos-in-virtual-meetings

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-bec-attackers-impersonating-ceos-in-virtual-meetings/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned today that US organizations and individuals are being increasingly targeted in BEC (business email compromise) attacks on virtual meeting platforms. [...]
