Title: FBI: Use a Burner Phone at the Olympics
Date: 2022-02-01T23:06:53+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Mobile Security;Web Security
Slug: 2022-02-01-fbi-use-a-burner-phone-at-the-olympics

[Source](https://threatpost.com/fbi-burner-phone-olympics-beijing/178153/){:target="_blank" rel="noopener"}

> The warning follows a Citizen Lab report that found the official, mandatory app has an encryption flaw that "can be trivially sidestepped." Besides burners, here are more tips on staying cyber-safe at the Games. [...]
