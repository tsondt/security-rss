Title: Adobe warns of second critical security hole in Adobe Commerce, Magento
Date: 2022-02-18T19:20:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-18-adobe-warns-of-second-critical-security-hole-in-adobe-commerce-magento

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/18/adobe_magento_patch/){:target="_blank" rel="noopener"}

> As sanctioned Russian infosec firm says it has working exploit code Adobe has put out a warning about another critical security bug affecting its Magento/Adobe Commerce product – and IT pros need to install a second patch after an initial update earlier this week failed to fully plug the first one.... [...]
