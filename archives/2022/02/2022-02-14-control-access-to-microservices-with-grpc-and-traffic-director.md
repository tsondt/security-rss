Title: Control access to microservices with gRPC and Traffic Director
Date: 2022-02-14T17:00:00+00:00
Author: Eric Anderson
Category: GCP Security
Tags: Identity & Security;Networking
Slug: 2022-02-14-control-access-to-microservices-with-grpc-and-traffic-director

[Source](https://cloud.google.com/blog/products/networking/client-authorization-with-grpc-and-traffic-director/){:target="_blank" rel="noopener"}

> We are excited to announce Traffic Director’s general availability of client authorization by proxyless gRPC services. This release, in conjunction with Traffic Director’s capability for managing mutual TLS (mTLS) credentials for Google Kubernetes Engine (GKE), enables customers to centrally manage access between workloads using Traffic Director. With the new authorization support, you can explicitly permit access to servers from specific clients and ensure that non-authorized clients will be denied access. You can match clients by their identity as verified with mTLS and limit clients to certain IP addresses. You can also match against an HTTP header for custom approaches. Check [...]
