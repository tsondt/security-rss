Title: We get the privacy we deserve from our behavior
Date: 2022-02-16T10:45:06+00:00
Author: Team Register
Category: The Register
Tags: 
Slug: 2022-02-16-we-get-the-privacy-we-deserve-from-our-behavior

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/16/encryption_debate_for2_weds/){:target="_blank" rel="noopener"}

> How many websites are dancing with your data like no one is watching? Register Debate Welcome to the latest Register Debate in which writers discuss technology topics, and you the reader choose the winning argument. The format is simple: we propose a motion, the arguments for the motion will run this Monday and Wednesday, and the arguments against on Tuesday and Thursday. During the week you can cast your vote on which side you support using the poll embedded below, choosing whether you're in favor or against the motion. The final score will be announced on Friday, revealing whether the [...]
