Title: KP Snacks Left with Crumbs After Ransomware Attack
Date: 2022-02-02T22:25:35+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Malware;Vulnerabilities;Web Security
Slug: 2022-02-02-kp-snacks-left-with-crumbs-after-ransomware-attack

[Source](https://threatpost.com/kp-snacks-crumbs-ransomware-attack/178176/){:target="_blank" rel="noopener"}

> The Conti gang strikes again, disrupting the nom-merchant's supply chain and threatening empty supermarket shelves lasting for weeks. [...]
