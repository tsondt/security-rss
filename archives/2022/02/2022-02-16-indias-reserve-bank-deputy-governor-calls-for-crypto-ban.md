Title: India's Reserve Bank deputy governor calls for crypto ban
Date: 2022-02-16T05:58:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-16-indias-reserve-bank-deputy-governor-calls-for-crypto-ban

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/16/india_cryptocurrency_ban_call/){:target="_blank" rel="noopener"}

> Labels digital currencies wreckers of sovereignty, worse than a Ponzi scheme The deputy governor of the Reserve Bank of India, T Rabi Sankar, has delivered an extremely unflattering assessment of cryptocurrencies – worse than Ponzi schemes, wreckers of economies, and richly deserving of a ban within India.... [...]
