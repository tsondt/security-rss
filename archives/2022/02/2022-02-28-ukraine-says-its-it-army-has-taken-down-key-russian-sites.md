Title: Ukraine says its 'IT Army' has taken down key Russian sites
Date: 2022-02-28T12:28:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-28-ukraine-says-its-it-army-has-taken-down-key-russian-sites

[Source](https://www.bleepingcomputer.com/news/security/ukraine-says-its-it-army-has-taken-down-key-russian-sites/){:target="_blank" rel="noopener"}

> Key Russian websites and state online portals have been taken offline by attacks claimed by the Ukrainian cyber police force, which now openly engages in cyber-warfare. [...]
