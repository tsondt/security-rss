Title: Data stolen from Nvidia, blueprints leak threatened
Date: 2022-02-26T00:39:29+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-02-26-data-stolen-from-nvidia-blueprints-leak-threatened

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/26/nvidia_security_breach/){:target="_blank" rel="noopener"}

> Also don't try to unlock your GPU cards with fake mining tool, and more In brief Nvidia is probing a cyberattack that caused outages within its internal network.... [...]
