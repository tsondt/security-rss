Title: FritzFrog botnet grows 10x, hits healthcare, edu, and govt systems
Date: 2022-02-10T09:08:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-10-fritzfrog-botnet-grows-10x-hits-healthcare-edu-and-govt-systems

[Source](https://www.bleepingcomputer.com/news/security/fritzfrog-botnet-grows-10x-hits-healthcare-edu-and-govt-systems/){:target="_blank" rel="noopener"}

> The FritzFrog botnet that's been active for more than two years has resurfaced with an alarming infection rate, growing ten times in just a month of hitting healthcare, education, and government systems with an exposed SSH server. [...]
