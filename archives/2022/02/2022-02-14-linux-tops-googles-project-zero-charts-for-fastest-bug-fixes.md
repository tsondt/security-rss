Title: Linux tops Google's Project Zero charts for fastest bug fixes
Date: 2022-02-14T13:04:03+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-02-14-linux-tops-googles-project-zero-charts-for-fastest-bug-fixes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/14/in_brief_security/){:target="_blank" rel="noopener"}

> Plus new breach disclosure rules for US investment firms In brief The bug hunters at Google's Project Zero team have released their latest time-to-fix data and Linux is smashing the opposition.... [...]
