Title: White House pins Ukraine DDoS attacks on Russian GRU hackers
Date: 2022-02-18T16:33:38-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-18-white-house-pins-ukraine-ddos-attacks-on-russian-gru-hackers

[Source](https://www.bleepingcomputer.com/news/security/white-house-pins-ukraine-ddos-attacks-on-russian-gru-hackers/){:target="_blank" rel="noopener"}

> Today, the White House has linked the recent DDoS attacks that knocked down the sites of Ukrainian banks and defense agencies to Russia's Main Directorate of the General Staff of the Armed Forces (also known as GRU). [...]
