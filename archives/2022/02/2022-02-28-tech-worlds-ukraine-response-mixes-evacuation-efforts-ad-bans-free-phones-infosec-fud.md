Title: Tech world's Ukraine response mixes evacuation efforts, ad bans, free phones, infosec FUD
Date: 2022-02-28T05:15:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-28-tech-worlds-ukraine-response-mixes-evacuation-efforts-ad-bans-free-phones-infosec-fud

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/28/tech_response_to_ukraine/){:target="_blank" rel="noopener"}

> And personal sorrow, as the horror of Russian invasion hits home As Russia's invasion of Ukraine continues, the technology industry is trying to use its services to make a difference – and to keep those services available as the war makes it harder to operate.... [...]
