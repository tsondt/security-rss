Title: Millions of WordPress sites get forced update to patch critical plugin flaw
Date: 2022-02-18T21:08:00+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;content management systems;databases;hacking;vulnerabilities;wordpress
Slug: 2022-02-18-millions-of-wordpress-sites-get-forced-update-to-patch-critical-plugin-flaw

[Source](https://arstechnica.com/?p=1835318){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Millions of WordPress sites have received a forced update over the past day to fix a critical vulnerability in a plugin called UpdraftPlus. The mandatory patch came at the request of UpdraftPlus developers because of the severity of the vulnerability, which allows untrusted subscribers, customers, and others to download the site’s private database as long as they have an account on the vulnerable site. Databases frequently include sensitive information about customers or the site’s security settings, leaving millions of sites susceptible to serious data breaches that spill passwords, user names, IP addresses, and more. Bad outcomes, [...]
