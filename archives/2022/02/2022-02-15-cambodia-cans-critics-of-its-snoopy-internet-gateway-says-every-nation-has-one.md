Title: Cambodia cans critics of its snoopy Internet Gateway, says every nation has one
Date: 2022-02-15T06:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-15-cambodia-cans-critics-of-its-snoopy-internet-gateway-says-every-nation-has-one

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/15/cambodia_clarifies_internet_gateway/){:target="_blank" rel="noopener"}

> Government says it’s just collecting tax, United Nations points to data harvesting and chilling effects on free speech Cambodia's Ministry of Foreign Affairs and International Cooperation has issued a clarification about the role of the "National Internet Gateway" that will commence operations tomorrow, stating that descriptions of it as an instrument of pervasive surveillance are "unfounded."... [...]
