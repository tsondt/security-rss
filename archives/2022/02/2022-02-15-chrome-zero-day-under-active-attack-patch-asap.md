Title: Chrome Zero-Day Under Active Attack: Patch ASAP
Date: 2022-02-15T18:33:28+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-02-15-chrome-zero-day-under-active-attack-patch-asap

[Source](https://threatpost.com/google-chrome-zero-day-under-attack/178428/){:target="_blank" rel="noopener"}

> The year's 1st Chrome zero-day can lead to all sorts of misery, ranging from data corruption to the execution of arbitrary code on vulnerable systems. [...]
