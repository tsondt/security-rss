Title: What are real organisations doing with zero trust?
Date: 2022-02-02T07:26:10+00:00
Author: Dave Cartwright
Category: The Register
Tags: 
Slug: 2022-02-02-what-are-real-organisations-doing-with-zero-trust

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/02/reg_reader_survey_zero_trusts/){:target="_blank" rel="noopener"}

> Take part in this short survey and let's find out together Reg Reader Survey Like many concepts in cyber-security, Zero Trust (hereafter "ZT") has come to prominence recently. The concept is reckoned to have first been used in the mid-1990s, though it came to prominence around 2010 and has really started to take off in the past three years or so.... [...]
