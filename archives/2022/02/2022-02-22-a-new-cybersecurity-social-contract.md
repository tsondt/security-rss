Title: A New Cybersecurity “Social Contract”
Date: 2022-02-22T15:28:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;national security policy;public interest
Slug: 2022-02-22-a-new-cybersecurity-social-contract

[Source](https://www.schneier.com/blog/archives/2022/02/a-new-cybersecurity-social-contract.html){:target="_blank" rel="noopener"}

> The US National Cyber Director Chris Inglis wrote an essay outlining a new social contract for the cyber age: The United States needs a new social contract for the digital age — one that meaningfully alters the relationship between public and private sectors and proposes a new set of obligations for each. Such a shift is momentous but not without precedent. From the Pure Food and Drug Act of 1906 to the Clean Air Act of 1963 and the public-private revolution in airline safety in the 1990s, the United States has made important adjustments following profound changes in the economy [...]
