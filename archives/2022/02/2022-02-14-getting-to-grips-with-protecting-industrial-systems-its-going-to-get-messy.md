Title: Getting to grips with protecting industrial systems? It’s going to get messy
Date: 2022-02-14T23:00:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-02-14-getting-to-grips-with-protecting-industrial-systems-its-going-to-get-messy

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/14/protecting_industrial_systems/){:target="_blank" rel="noopener"}

> Join this webcast and learn how to get your hands dirty Paid Post Getting to really know industrial systems means getting hands-on. Literally. The same applies to securing them, which is more important than ever in the wake of attacks such as last year’s Colonial Pipeline debacle.... [...]
