Title: The Week in Ransomware - February 11th 2022 - Maze, Egregor decryptors
Date: 2022-02-11T16:57:54-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-11-the-week-in-ransomware-february-11th-2022-maze-egregor-decryptors

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-february-11th-2022-maze-egregor-decryptors/){:target="_blank" rel="noopener"}

> We saw the Maze ransomware developers reemerge briefly this week as they shared the master decryption keys for the Egregor, Maze, and Sekhmet ransomware operations. [...]
