Title: Microsoft manages a mere 51 security fixes for February update bundle
Date: 2022-02-09T00:30:43+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-09-microsoft-manages-a-mere-51-security-fixes-for-february-update-bundle

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/09/microsoft_patch_tuesday/){:target="_blank" rel="noopener"}

> Excitement this month can be found in SAP code, with critical Log4j repairs and a CISA warning Patch Tuesday Microsoft for its February Patch Tuesday gave Windows admins just 51 fixes to apply, the smallest number of patches since the meager ration of 44 in August 2021.... [...]
