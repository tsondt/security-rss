Title: US says Russian state hackers breached defense contractors
Date: 2022-02-16T12:05:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-16-us-says-russian-state-hackers-breached-defense-contractors

[Source](https://www.bleepingcomputer.com/news/security/us-says-russian-state-hackers-breached-defense-contractors/){:target="_blank" rel="noopener"}

> Russian-backed hackers have been targeting and compromising U.S. cleared defense contractors (CDCs) since at least January 2020 to gain access to and steal sensitive info that gives insight into U.S. defense and intelligence programs and capabilities. [...]
