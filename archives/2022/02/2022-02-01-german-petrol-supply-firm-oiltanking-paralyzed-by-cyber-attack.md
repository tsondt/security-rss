Title: German petrol supply firm Oiltanking paralyzed by cyber attack
Date: 2022-02-01T07:27:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-01-german-petrol-supply-firm-oiltanking-paralyzed-by-cyber-attack

[Source](https://www.bleepingcomputer.com/news/security/german-petrol-supply-firm-oiltanking-paralyzed-by-cyber-attack/){:target="_blank" rel="noopener"}

> Oiltanking GmbH, a German petrol distributor who supplies Shell gas stations in the country, has fallen victim to a cyberattack that severely impacted its operations. [...]
