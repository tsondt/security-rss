Title: Low-Detection Phishing Kits Increasingly Bypass MFA
Date: 2022-02-03T22:10:32+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Mobile Security;Web Security
Slug: 2022-02-03-low-detection-phishing-kits-increasingly-bypass-mfa

[Source](https://threatpost.com/low-detection-phishing-kits-bypass-mfa/178208/){:target="_blank" rel="noopener"}

> A growing class of phishing kits – transparent reverse proxy kits – are being used to get past multi-factor authentication using MiTM tactics. [...]
