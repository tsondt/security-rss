Title: Emergency Adobe Commerce, Magento patches follow ‘limited’ in-the-wild attacks on vulnerable deployments
Date: 2022-02-16T12:24:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-16-emergency-adobe-commerce-magento-patches-follow-limited-in-the-wild-attacks-on-vulnerable-deployments

[Source](https://portswigger.net/daily-swig/emergency-adobe-commerce-magento-patches-follow-limited-in-the-wild-attacks-on-vulnerable-deployments){:target="_blank" rel="noopener"}

> Web admins urged to update now [...]
