Title: Massive LinkedIn Phishing, Bot Attacks Feed on the Job-Hungry
Date: 2022-02-16T21:15:47+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-02-16-massive-linkedin-phishing-bot-attacks-feed-on-the-job-hungry

[Source](https://threatpost.com/massive-linkedin-phishing-bot-attacks-hungry-job-seekers/178476/){:target="_blank" rel="noopener"}

> The phishing attacks are spoofing LinkedIn to target ‘Great Resignation’ job hunters, who are also being preyed on by huge data-scraping bot attacks. [...]
