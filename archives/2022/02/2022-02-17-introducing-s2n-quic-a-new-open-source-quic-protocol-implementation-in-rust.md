Title: Introducing s2n-quic, a new open-source QUIC protocol implementation in Rust
Date: 2022-02-17T20:05:43+00:00
Author: Panos Kampanakis
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;Cryptographic library;Encryption;QUIC;QUIC protocol;s2n;s2n-quic;Security Blog
Slug: 2022-02-17-introducing-s2n-quic-a-new-open-source-quic-protocol-implementation-in-rust

[Source](https://aws.amazon.com/blogs/security/introducing-s2n-quic-open-source-protocol-rust/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), security, high performance, and strong encryption for everyone are top priorities for all our services. With these priorities in mind, less than a year after QUIC ratification in the Internet Engineering Task Force (IETF), we are introducing support for the QUIC protocol which can boost performance for web applications that [...]
