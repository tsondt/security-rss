Title: Microsoft blocked billions of brute-force and phishing attacks last year
Date: 2022-02-03T11:35:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-03-microsoft-blocked-billions-of-brute-force-and-phishing-attacks-last-year

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-blocked-billions-of-brute-force-and-phishing-attacks-last-year/){:target="_blank" rel="noopener"}

> Office 365 and Azure Active Directory (Azure AD) customers were the targets of billions of phishing emails and brute force attacks successfully blocked last year by Microsoft. [...]
