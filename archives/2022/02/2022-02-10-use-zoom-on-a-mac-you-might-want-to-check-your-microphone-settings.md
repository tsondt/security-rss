Title: Use Zoom on a Mac? You might want to check your microphone settings
Date: 2022-02-10T14:07:02+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-02-10-use-zoom-on-a-mac-you-might-want-to-check-your-microphone-settings

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/10/zoom_mac_microphone/){:target="_blank" rel="noopener"}

> Big Zoomer is listening to us, complain users Apple Mac users running the Zoom meetings app are reporting that it's keeping their computer's microphone on when they aren't using it.... [...]
