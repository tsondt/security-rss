Title: Strengthening our European data sovereignty offerings with Assured Workloads for EU
Date: 2022-02-02T14:00:00+00:00
Author: Bryce Buffaloe
Category: GCP Security
Tags: Google Cloud;Google Cloud in Europe;Compliance;Identity & Security
Slug: 2022-02-02-strengthening-our-european-data-sovereignty-offerings-with-assured-workloads-for-eu

[Source](https://cloud.google.com/blog/products/identity-security/meet-data-sovereignty-requirements-with-assured-workloads-for-eu-on-google-cloud/){:target="_blank" rel="noopener"}

> European organizations, both public and private, are migrating their operations and data to the cloud in increasing numbers. In doing so, they need confidence they can meet their unique needs for security, privacy, and digital sovereignty. Key requirements include the ability to store data within a European geographic region, to ensure that support is provided by EU personnel, and the ability to control administrative access to their customer data and encryption keys used to protect that data. To help meet these needs for customers using Google Cloud Platform, we are pleased to announce the general availability of Assured Workloads for [...]
