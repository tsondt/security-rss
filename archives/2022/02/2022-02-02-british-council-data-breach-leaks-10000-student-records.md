Title: British Council data breach leaks 10,000 student records
Date: 2022-02-02T12:44:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-02-british-council-data-breach-leaks-10000-student-records

[Source](https://portswigger.net/daily-swig/british-council-data-breach-leaks-10-000-student-records){:target="_blank" rel="noopener"}

> Researchers say 144,000 files were exposed [...]
