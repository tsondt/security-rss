Title: Critical MQTT-Related Bugs Open Industrial Networks to RCE Via Moxa
Date: 2022-02-11T21:51:28+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Vulnerabilities
Slug: 2022-02-11-critical-mqtt-related-bugs-open-industrial-networks-to-rce-via-moxa

[Source](https://threatpost.com/critical-mqtt-bugs-industrial-rce-moxa/178399/){:target="_blank" rel="noopener"}

> A collection of five security vulnerabilities with a collective CVSS score of 10 out of 10 threaten critical infrastructure environments that use Moxa MXview. [...]
