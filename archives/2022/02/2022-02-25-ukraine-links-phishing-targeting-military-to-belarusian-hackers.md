Title: Ukraine links phishing targeting military to Belarusian hackers
Date: 2022-02-25T09:18:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-25-ukraine-links-phishing-targeting-military-to-belarusian-hackers

[Source](https://www.bleepingcomputer.com/news/security/ukraine-links-phishing-targeting-military-to-belarusian-hackers/){:target="_blank" rel="noopener"}

> The Computer Emergency Response Team of Ukraine (CERT-UA) warned today of a spearphishing campaign targeting private email accounts belonging to Ukrainian armed forces personnel. [...]
