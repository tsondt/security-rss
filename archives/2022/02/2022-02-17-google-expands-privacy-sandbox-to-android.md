Title: Google expands Privacy Sandbox to Android
Date: 2022-02-17T03:01:29+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-17-google-expands-privacy-sandbox-to-android

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/17/google_privacy_sandbox/){:target="_blank" rel="noopener"}

> Web giant shocked, shocked, to find misuse of data going on in here Google plans to extend its rework of web ad technology – the optimistically named Privacy Sandbox – to Android devices in an effort to limit the misuse of data in its mobile ecosystem.... [...]
