Title: TrickBot Takes a Break, Leaving Researchers Scratching Their Heads
Date: 2022-02-25T21:32:15+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Malware
Slug: 2022-02-25-trickbot-takes-a-break-leaving-researchers-scratching-their-heads

[Source](https://threatpost.com/trickbot-break-researchers-scratching-heads/178678/){:target="_blank" rel="noopener"}

> The infamous trojan is likely making some major operational changes, researchers believe. [...]
