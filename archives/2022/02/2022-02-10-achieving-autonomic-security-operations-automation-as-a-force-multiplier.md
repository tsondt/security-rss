Title: Achieving Autonomic Security Operations: Automation as a Force Multiplier
Date: 2022-02-10T17:00:00+00:00
Author: Iman Ghanizada
Category: GCP Security
Tags: Identity & Security
Slug: 2022-02-10-achieving-autonomic-security-operations-automation-as-a-force-multiplier

[Source](https://cloud.google.com/blog/products/identity-security/security-automation-lessons-from-site-reliability-engineering-for-security-operations-center/){:target="_blank" rel="noopener"}

> As we discussed in “Achieving Autonomic Security Operations: Reducing toil”, your Security Operations Center (SOC) can learn lessons from Site Reliability Engineering (SRE) This means that applying software engineering practices to security operations challenges can radically improve an organization’s security. In this post, we discuss how you can leverage another core principle of SRE – automation - as a means to achieve better outcomes in your SOC. Let’s make it very clear – a fully automated Security Operations Center that requires no human involvement is not possible today. The essence of Autonomic Security Operations is the belief that organizations need [...]
