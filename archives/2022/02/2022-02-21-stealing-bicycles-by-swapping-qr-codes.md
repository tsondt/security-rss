Title: Stealing Bicycles by Swapping QR Codes
Date: 2022-02-21T12:31:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;scams;theft
Slug: 2022-02-21-stealing-bicycles-by-swapping-qr-codes

[Source](https://www.schneier.com/blog/archives/2022/02/stealing-bicycles-by-swapping-qr-codes.html){:target="_blank" rel="noopener"}

> This is a clever hack against those bike-rental kiosks: They’re stealing Citi Bikes by switching the QR scan codes on two bicycles near each other at a docking station, then waiting for an unsuspecting cyclist to try to unlock a bike with his or her smartphone app. The app doesn’t work for the rider but does free up the nearby Citi Bike with the switched code, where a thief is waiting, jumps on the bicycle and rides off. Presumably they’re using camera, printers, and stickers to swap the codes on the bikes. And presumably the victim is charged for not [...]
