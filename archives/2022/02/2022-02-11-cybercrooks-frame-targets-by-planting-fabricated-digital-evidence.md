Title: Cybercrooks Frame Targets by Planting Fabricated Digital Evidence
Date: 2022-02-11T19:57:34+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2022-02-11-cybercrooks-frame-targets-by-planting-fabricated-digital-evidence

[Source](https://threatpost.com/cybercrooks-frame-targets-plant-incriminating-evidence/178384/){:target="_blank" rel="noopener"}

> The ‘ModifiedElephant’ threat actors are technically unimpressive, but they’ve evaded detection for a decade, hacking human rights advocates' systems with dusty old keyloggers and off-the-shelf RATs. [...]
