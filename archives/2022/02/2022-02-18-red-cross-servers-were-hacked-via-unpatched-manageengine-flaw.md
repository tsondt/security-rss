Title: <span>Red Cross servers ‘were hacked via unpatched ManageEngine flaw’</span>
Date: 2022-02-18T17:32:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-18-red-cross-servers-were-hacked-via-unpatched-manageengine-flaw

[Source](https://portswigger.net/daily-swig/red-cross-servers-were-hacked-via-unpatched-manageengine-flaw){:target="_blank" rel="noopener"}

> Humanitarian organization failed to apply fix rolled out a couple of months earlier [...]
