Title: Microsoft February 2022 Patch Tuesday fixes 48 flaws, 1 zero-day
Date: 2022-02-08T13:27:31-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-08-microsoft-february-2022-patch-tuesday-fixes-48-flaws-1-zero-day

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-february-2022-patch-tuesday-fixes-48-flaws-1-zero-day/){:target="_blank" rel="noopener"}

> Today is Microsoft's February 2022 Patch Tuesday, and with it comes fixes for one zero-day vulnerability and a total of 48 flaws. [...]
