Title: Friday Squid Blogging: South American Squid Stocks Threatened by Chinese Fishing
Date: 2022-02-18T22:12:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-02-18-friday-squid-blogging-south-american-squid-stocks-threatened-by-chinese-fishing

[Source](https://www.schneier.com/blog/archives/2022/02/friday-squid-blogging-south-american-squid-stocks-threatened-by-chinese-fishing.html){:target="_blank" rel="noopener"}

> There’s a lot of fishing going on : The number of Chinese-flagged vessels in the south Pacific has surged 13-fold from 54 active vessels in 2009 to 707 in 2020, according to the SPRFMO. Meanwhile, the size of China’s squid catch has grown from 70,000 tons in 2009 to 358,000. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
