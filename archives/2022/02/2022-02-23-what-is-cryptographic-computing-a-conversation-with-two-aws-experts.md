Title: What is cryptographic computing? A conversation with two AWS experts
Date: 2022-02-23T17:16:11+00:00
Author: Supriya Anand
Category: AWS Security
Tags: Best Practices;Foundational (100);Security, Identity, & Compliance;cryptography;Security Blog
Slug: 2022-02-23-what-is-cryptographic-computing-a-conversation-with-two-aws-experts

[Source](https://aws.amazon.com/blogs/security/a-conversation-about-cryptographic-computing-at-aws/){:target="_blank" rel="noopener"}

> Joan Feigenbaum Amazon Scholar, AWS Cryptography Bill Horne Principal Product Manager, AWS Cryptography AWS Cryptography tools and services use a wide range of encryption and storage technologies that can help customers protect their data both at rest and in transit. In some instances, customers also require protection of their data even while it is in [...]
