Title: Conti ransomware's internal chats leaked after siding with Russia
Date: 2022-02-27T23:23:59-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-27-conti-ransomwares-internal-chats-leaked-after-siding-with-russia

[Source](https://www.bleepingcomputer.com/news/security/conti-ransomwares-internal-chats-leaked-after-siding-with-russia/){:target="_blank" rel="noopener"}

> An angry member of the Conti ransomware operation has leaked over 60,000 private messages after the gang sided with Russia over the invasion of Ukraine. [...]
