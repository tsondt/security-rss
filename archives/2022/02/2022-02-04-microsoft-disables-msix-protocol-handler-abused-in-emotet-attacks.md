Title: Microsoft disables MSIX protocol handler abused in Emotet attacks
Date: 2022-02-04T19:10:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-04-microsoft-disables-msix-protocol-handler-abused-in-emotet-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-disables-msix-protocol-handler-abused-in-emotet-attacks/){:target="_blank" rel="noopener"}

> Microsoft has disabled the MSIX ms-appinstaller protocol handler exploited in malware attacks to install malicious apps directly from a website via a Windows AppX Installer spoofing vulnerability. [...]
