Title: Chinese cyberspies target govts with their ‘most advanced’ backdoor
Date: 2022-02-28T14:32:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-28-chinese-cyberspies-target-govts-with-their-most-advanced-backdoor

[Source](https://www.bleepingcomputer.com/news/security/chinese-cyberspies-target-govts-with-their-most-advanced-backdoor/){:target="_blank" rel="noopener"}

> ​Security researchers have discovered Daxin, a China-linked stealthy backdoor specifically designed for deployment in hardened corporate networks that feature advanced threat detection capabilities. [...]
