Title: LockBit, BlackCat, Swissport, Oh My! Ransomware Activity Stays Strong
Date: 2022-02-07T22:09:27+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Malware;Vulnerabilities;Web Security
Slug: 2022-02-07-lockbit-blackcat-swissport-oh-my-ransomware-activity-stays-strong

[Source](https://threatpost.com/lockbit-blackcat-swissport-ransomware-activity/178261/){:target="_blank" rel="noopener"}

> However, groups are rebranding and recalibrating their profiles and tactics to respond to law enforcement and the security community's focus on stopping ransomware attacks. [...]
