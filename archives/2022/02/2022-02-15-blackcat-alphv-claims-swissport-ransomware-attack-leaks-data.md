Title: BlackCat (ALPHV) claims Swissport ransomware attack, leaks data
Date: 2022-02-15T05:56:15-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-02-15-blackcat-alphv-claims-swissport-ransomware-attack-leaks-data

[Source](https://www.bleepingcomputer.com/news/security/blackcat-alphv-claims-swissport-ransomware-attack-leaks-data/){:target="_blank" rel="noopener"}

> The BlackCat ransomware group, aka ALPHV, has claimed responsibility for the recent cyber attack on cargo and hospitality services giant Swissport that caused flight delays and service disruptions. [...]
