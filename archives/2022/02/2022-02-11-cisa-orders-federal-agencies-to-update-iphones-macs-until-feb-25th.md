Title: CISA orders federal agencies to update iPhones, Macs until Feb 25th
Date: 2022-02-11T12:45:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2022-02-11-cisa-orders-federal-agencies-to-update-iphones-macs-until-feb-25th

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-federal-agencies-to-update-iphones-macs-until-feb-25th/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) has added a new flaw to its catalog of vulnerabilities exploited in the wild, an Apple WebKit remote code execution bug used to target iPhones, iPads, and Macs. [...]
