Title: Toyota halts production after reported cyberattack on supplier
Date: 2022-02-28T10:18:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-28-toyota-halts-production-after-reported-cyberattack-on-supplier

[Source](https://www.bleepingcomputer.com/news/security/toyota-halts-production-after-reported-cyberattack-on-supplier/){:target="_blank" rel="noopener"}

> Giant Japanese automaker Toyota Motors has announced that it stopped car production operations. The outage was forced by a system failure at one of its suppliers of vital parts, Kojima Industries, which reportedly suffered a cyberattack. [...]
