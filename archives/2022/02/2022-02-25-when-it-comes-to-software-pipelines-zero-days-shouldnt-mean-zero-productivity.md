Title: When it comes to software pipelines, zero days shouldn’t mean zero productivity
Date: 2022-02-25T07:30:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-02-25-when-it-comes-to-software-pipelines-zero-days-shouldnt-mean-zero-productivity

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/25/log4j/){:target="_blank" rel="noopener"}

> Join us and learn how to ride out the next Log4j Webinar DevOps and continuous delivery have revolutionized software development and productivity over the last decade.... [...]
