Title: GitHub code scanning now finds more security vulnerabilities
Date: 2022-02-17T14:47:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-17-github-code-scanning-now-finds-more-security-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/github-code-scanning-now-finds-more-security-vulnerabilities/){:target="_blank" rel="noopener"}

> Code hosting platform GitHub today launched new machine learning-based code scanning analysis features that will automatically discover more common security vulnerabilities before they end up in production. [...]
