Title: Defense contractors hit by stealthy SockDetour Windows backdoor
Date: 2022-02-24T11:43:31-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-24-defense-contractors-hit-by-stealthy-sockdetour-windows-backdoor

[Source](https://www.bleepingcomputer.com/news/security/defense-contractors-hit-by-stealthy-sockdetour-windows-backdoor/){:target="_blank" rel="noopener"}

> A new custom malware dubbed SockDetour found on systems belonging to US defense contractors has been used as a backup backdoor to maintain access to compromised networks. [...]
