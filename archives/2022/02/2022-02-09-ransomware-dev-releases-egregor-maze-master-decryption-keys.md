Title: Ransomware dev releases Egregor, Maze master decryption keys
Date: 2022-02-09T10:26:31-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-09-ransomware-dev-releases-egregor-maze-master-decryption-keys

[Source](https://www.bleepingcomputer.com/news/security/ransomware-dev-releases-egregor-maze-master-decryption-keys/){:target="_blank" rel="noopener"}

> The master decryption keys for the Maze, Egregor, and Sekhmet ransomware operations were released last night on the BleepingComputer forums by the alleged malware developer. [...]
