Title: Ransomware used as decoy in data-wiping attacks on Ukraine
Date: 2022-02-24T10:39:14-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-24-ransomware-used-as-decoy-in-data-wiping-attacks-on-ukraine

[Source](https://www.bleepingcomputer.com/news/security/ransomware-used-as-decoy-in-data-wiping-attacks-on-ukraine/){:target="_blank" rel="noopener"}

> The new data wiper malware deployed on Ukrainian networks in destructive attacks on Wednesday right before Russia invaded Ukraine earlier today was, in some cases, accompanied by a GoLang-based ransomware decoy. [...]
