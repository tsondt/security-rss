Title: <span>SureMDM bug chain enabled wholesale compromise of managed devices</span>
Date: 2022-02-01T15:05:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-01-suremdm-bug-chain-enabled-wholesale-compromise-of-managed-devices

[Source](https://portswigger.net/daily-swig/suremdm-bug-chain-enabled-wholesale-compromise-of-managed-devices){:target="_blank" rel="noopener"}

> Series of flaws in MDM platform addressed in web console and Linux agent [...]
