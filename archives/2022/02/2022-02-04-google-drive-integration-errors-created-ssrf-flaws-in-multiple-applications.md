Title: Google Drive integration errors created SSRF flaws in multiple applications
Date: 2022-02-04T15:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-04-google-drive-integration-errors-created-ssrf-flaws-in-multiple-applications

[Source](https://portswigger.net/daily-swig/google-drive-integration-errors-created-ssrf-flaws-in-multiple-applications){:target="_blank" rel="noopener"}

> Bug hunter earned $17k bounty for HelloSign bug [...]
