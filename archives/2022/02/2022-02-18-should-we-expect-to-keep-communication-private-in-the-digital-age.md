Title: Should we expect to keep communication private in the digital age?
Date: 2022-02-18T15:21:25+00:00
Author: Joe Fay
Category: The Register
Tags: 
Slug: 2022-02-18-should-we-expect-to-keep-communication-private-in-the-digital-age

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/18/reg_debate_privacy_summary_friday/){:target="_blank" rel="noopener"}

> Reg writers and readers wrangle over rights and realities Register Debate Can you have a debate on privacy without mentioning Orwell and 1984 or Bentham's Panopticon?... [...]
