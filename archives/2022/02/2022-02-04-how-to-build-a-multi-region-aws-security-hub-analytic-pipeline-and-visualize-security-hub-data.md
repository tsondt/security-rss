Title: How to build a multi-Region AWS Security Hub analytic pipeline and visualize Security Hub data
Date: 2022-02-04T21:22:10+00:00
Author: David Hessler
Category: AWS Security
Tags: Advanced (300);AWS Security Hub;Security, Identity, & Compliance;Amazon Athena;Amazon QuickSight;Business Intelligence;Cyber Security;infrastructure security
Slug: 2022-02-04-how-to-build-a-multi-region-aws-security-hub-analytic-pipeline-and-visualize-security-hub-data

[Source](https://aws.amazon.com/blogs/security/how-to-build-a-multi-region-aws-security-hub-analytic-pipeline/){:target="_blank" rel="noopener"}

> AWS Security Hub is a service that gives you aggregated visibility into your security and compliance posture across multiple Amazon Web Services (AWS) accounts. By joining Security Hub with Amazon QuickSight—a scalable, serverless, embeddable, machine learning-powered business intelligence (BI) service built for the cloud—your senior leaders and decision-makers can use dashboards to empower data-driven decisions [...]
