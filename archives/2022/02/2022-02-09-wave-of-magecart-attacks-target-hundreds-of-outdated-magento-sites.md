Title: Wave of MageCart attacks target hundreds of outdated Magento sites
Date: 2022-02-09T13:24:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-09-wave-of-magecart-attacks-target-hundreds-of-outdated-magento-sites

[Source](https://www.bleepingcomputer.com/news/security/wave-of-magecart-attacks-target-hundreds-of-outdated-magento-sites/){:target="_blank" rel="noopener"}

> Analysts have found the source of a mass breach of over 500 e-commerce stores running the Magento 1 platform and involves a single domain loading a credit card skimmer on all of them. [...]
