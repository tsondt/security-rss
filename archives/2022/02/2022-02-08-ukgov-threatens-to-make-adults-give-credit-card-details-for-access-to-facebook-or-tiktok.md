Title: UK.gov threatens to make adults give credit card details for access to Facebook or TikTok
Date: 2022-02-08T15:43:46+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-08-ukgov-threatens-to-make-adults-give-credit-card-details-for-access-to-facebook-or-tiktok

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/08/age_verification_for_social_media_ukgov_plans/){:target="_blank" rel="noopener"}

> Age verification for large chunks of WWW to be mandatory Adults will have to hand over credit card or passport details before they can access social media sites, the British government threatened this morning.... [...]
