Title: Puma hit by data breach after Kronos ransomware attack
Date: 2022-02-07T15:49:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-07-puma-hit-by-data-breach-after-kronos-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/puma-hit-by-data-breach-after-kronos-ransomware-attack/){:target="_blank" rel="noopener"}

> Sportswear manufacturer Puma was hit by a data breach following the ransomware attack that hit Kronos, one of its North American workforce management service providers, in December 2021. [...]
