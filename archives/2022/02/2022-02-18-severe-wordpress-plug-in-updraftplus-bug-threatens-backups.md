Title: Severe WordPress Plug-In UpdraftPlus Bug Threatens Backups
Date: 2022-02-18T14:25:09+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-02-18-severe-wordpress-plug-in-updraftplus-bug-threatens-backups

[Source](https://threatpost.com/severe-wordpress-plug-in-updraftplus-bug-threatens-backups/178528/){:target="_blank" rel="noopener"}

> An oversight in a WordPress plug-in exposes PII and authentication data to malicious insiders. [...]
