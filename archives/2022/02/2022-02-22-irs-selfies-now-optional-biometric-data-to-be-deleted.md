Title: IRS: Selfies Now Optional, Biometric Data to Be Deleted
Date: 2022-02-22T17:50:33+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;id.me;Internal Revenue Service;irs;login.gov;Sen. Ron Wyden;U.S. General Services Administration
Slug: 2022-02-22-irs-selfies-now-optional-biometric-data-to-be-deleted

[Source](https://krebsonsecurity.com/2022/02/irs-selfies-now-optional-biometric-data-to-be-deleted/){:target="_blank" rel="noopener"}

> The U.S. Internal Revenue Service (IRS) said Monday that taxpayers are no longer required to provide facial scans to create an account online at irs.gov. In lieu of providing biometric data, taxpayers can now opt for a live video interview with ID.me, the privately-held Virginia company that runs the agency’s identity proofing system. The IRS also said any biometric data already shared with ID.me would be permanently deleted over the next few weeks, and any biometric data provided for new signups will be destroyed after an account is created. “Taxpayers will have the option of verifying their identity during a [...]
