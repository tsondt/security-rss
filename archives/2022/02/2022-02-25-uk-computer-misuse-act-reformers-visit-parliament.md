Title: UK Computer Misuse Act reformers visit Parliament
Date: 2022-02-25T11:15:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-25-uk-computer-misuse-act-reformers-visit-parliament

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/25/cyberup_parliament_rob_dyke/){:target="_blank" rel="noopener"}

> Cyberup campaign hasn't gone away, you know Infosec researcher Rob Dyke, best known to Reg readers for fending off legal threats from not-for-profit open-source foundation Apperta after finding a data breach, has visited Parliament to demand Computer Misuse Act reform.... [...]
