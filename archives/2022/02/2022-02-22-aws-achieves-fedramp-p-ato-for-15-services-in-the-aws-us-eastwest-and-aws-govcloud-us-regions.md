Title: AWS achieves FedRAMP P-ATO for 15 services in the AWS US East/West and AWS GovCloud (US) Regions
Date: 2022-02-22T20:47:03+00:00
Author: Alexis Robinson
Category: AWS Security
Tags: Announcements;AWS GovCloud (US);Federal;Foundational (100);Government;Public Sector;Security, Identity, & Compliance;AWS (US) GovCloud;AWS East/West;FedRAMP;Security Blog
Slug: 2022-02-22-aws-achieves-fedramp-p-ato-for-15-services-in-the-aws-us-eastwest-and-aws-govcloud-us-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-fedramp-p-ato-for-15-services-in-the-aws-us-east-west-and-aws-govcloud-us-regions/){:target="_blank" rel="noopener"}

> AWS is pleased to announce that 15 additional AWS services have achieved Provisional Authority to Operate (P-ATO) from the Federal Risk and Authorization Management Program (FedRAMP) Joint Authorization Board (JAB). AWS is continually expanding the scope of our compliance programs to help customers use authorized services for sensitive and regulated workloads. AWS now offers 111 [...]
