Title: Privacy and computer security are too important to be left to political meddling
Date: 2022-02-17T10:45:07+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2022-02-17-privacy-and-computer-security-are-too-important-to-be-left-to-political-meddling

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/17/encryption_debate_against2_thurs/){:target="_blank" rel="noopener"}

> And it shouldn't be a privilege handed out by governments Register Debate Welcome to the latest Register Debate in which writers discuss technology topics, and you the reader choose the winning argument. The format is simple: we propose a motion, the arguments for the motion will run this Monday and Wednesday, and the arguments against on Tuesday and Thursday. During the week you can cast your vote on which side you support using the poll embedded below, choosing whether you're in favor or against the motion. The final score will be announced on Friday, revealing whether the for or against [...]
