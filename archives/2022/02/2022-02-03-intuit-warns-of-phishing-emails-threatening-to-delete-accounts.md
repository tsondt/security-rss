Title: Intuit warns of phishing emails threatening to delete accounts
Date: 2022-02-03T14:22:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-03-intuit-warns-of-phishing-emails-threatening-to-delete-accounts

[Source](https://www.bleepingcomputer.com/news/security/intuit-warns-of-phishing-emails-threatening-to-delete-accounts/){:target="_blank" rel="noopener"}

> Accounting and tax software provider Intuit has notified customers of an ongoing phishing campaign impersonating the company and trying to lure victims with fake warnings that their accounts have been suspended. [...]
