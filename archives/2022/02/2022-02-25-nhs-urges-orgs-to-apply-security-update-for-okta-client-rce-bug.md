Title: NHS urges orgs to apply security update for Okta Client RCE bug
Date: 2022-02-25T13:58:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-25-nhs-urges-orgs-to-apply-security-update-for-okta-client-rce-bug

[Source](https://www.bleepingcomputer.com/news/security/nhs-urges-orgs-to-apply-security-update-for-okta-client-rce-bug/){:target="_blank" rel="noopener"}

> The UK's NHS Digital agency is warning organizations to apply new security updates for a remote code execution vulnerability in the Windows client for the Okta Advanced Server Access authentication management platform. [...]
