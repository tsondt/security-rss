Title: MFA adoption pushes phishing actors to reverse-proxy solutions
Date: 2022-02-03T09:42:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-03-mfa-adoption-pushes-phishing-actors-to-reverse-proxy-solutions

[Source](https://www.bleepingcomputer.com/news/security/mfa-adoption-pushes-phishing-actors-to-reverse-proxy-solutions/){:target="_blank" rel="noopener"}

> The rising adoption of multi-factor authentication (MFA) for online accounts pushes phishing actors to use more sophisticated solutions to continue their malicious operations, most notably reverse-proxy tools. [...]
