Title: Ransomware extortion doesn't stop after paying the ransom
Date: 2022-02-23T14:43:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-23-ransomware-extortion-doesnt-stop-after-paying-the-ransom

[Source](https://www.bleepingcomputer.com/news/security/ransomware-extortion-doesnt-stop-after-paying-the-ransom/){:target="_blank" rel="noopener"}

> A global survey that looked into the experience of ransomware victims highlights the lack of trustworthiness of ransomware actors, as in most cases of paying the ransom, the extortion simply continues. [...]
