Title: Friday Squid Blogging: Squid Videos
Date: 2022-02-25T22:00:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-02-25-friday-squid-blogging-squid-videos

[Source](https://www.schneier.com/blog/archives/2022/02/friday-squid-blogging-squid-videos.html){:target="_blank" rel="noopener"}

> Here are six beautiful squid videos. I know nothing more about them. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. EDITED TO ADD (2/25): This post accidentally went live on Wednesday, two days early, and people started adding their comments then. I have changed the posting date to the correct one, which means that the comments existing before that time will appear to have been made before the post. I apologize for the confusion. [...]
