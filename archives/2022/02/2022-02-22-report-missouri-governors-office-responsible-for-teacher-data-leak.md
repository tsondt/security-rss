Title: Report: Missouri Governor’s Office Responsible for Teacher Data Leak
Date: 2022-02-22T16:18:57+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Elad Gross;Josh Renaud;Mallory McGowin;Missouri Gov. Mike Parson;Missouri Highway Patrol;Shaji Khan;St. Louis Post-Dispatch
Slug: 2022-02-22-report-missouri-governors-office-responsible-for-teacher-data-leak

[Source](https://krebsonsecurity.com/2022/02/report-missouri-governors-office-responsible-for-teacher-data-leak/){:target="_blank" rel="noopener"}

> Missouri Governor Mike Parson made headlines last year when he vowed to criminally prosecute a journalist for reporting a security flaw in a state website that exposed personal information of more than 100,000 teachers. But Missouri prosecutors now say they will not pursue charges following revelations that the data had been exposed since 2011 — two years after responsibility for securing the state’s IT systems was centralized within Parson’s own Office of Administration. Missouri Gov. Mike Parson (R), vowing to prosecute the St. Louis Post-Dispatch for reporting a security vulnerability that exposed teacher SSNs. In October 2021, St. Louis Post-Dispatch [...]
