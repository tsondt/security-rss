Title: PHP Everywhere Bugs Put 30K+ WordPress Sites at Risk of RCE
Date: 2022-02-10T13:58:07+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-02-10-php-everywhere-bugs-put-30k-wordpress-sites-at-risk-of-rce

[Source](https://threatpost.com/php-everywhere-bugs-wordpress-rce/178338/){:target="_blank" rel="noopener"}

> The plug-in’s default settings spawned flaws that could allow for full site takeover but have since been fixed in an update that users should immediately install, Wordfence researchers said. [...]
