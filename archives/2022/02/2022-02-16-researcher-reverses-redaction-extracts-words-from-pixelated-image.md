Title: Researcher 'reverses' redaction, extracts words from pixelated image
Date: 2022-02-16T06:45:17-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Technology
Slug: 2022-02-16-researcher-reverses-redaction-extracts-words-from-pixelated-image

[Source](https://www.bleepingcomputer.com/news/security/researcher-reverses-redaction-extracts-words-from-pixelated-image/){:target="_blank" rel="noopener"}

> A researcher has demonstrated how he was able to successfully recover text that had been redacted using the pixelation technique. Further, the researcher has released a GitHub tool that can be used by anyone to reconstruct text from obscure, pixelated images. [...]
