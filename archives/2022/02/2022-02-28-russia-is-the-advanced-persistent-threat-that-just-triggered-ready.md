Title: Russia is the advanced persistent threat that just triggered. Ready?
Date: 2022-02-28T09:30:11+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2022-02-28-russia-is-the-advanced-persistent-threat-that-just-triggered-ready

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/28/russia_information_security/){:target="_blank" rel="noopener"}

> Data security looks very different when your life depends on it Opinion Stress-testing security is the only way to be sure it works. Until then, the worst security looks much the same as the best. As events in Ukraine show, leaving the stress-testing of assumptions until a threat is actually attacking is expensively useless.... [...]
