Title: 3 Tips for Facing the Harsh Truths of Cybersecurity in 2022, Part I
Date: 2022-02-09T21:06:38+00:00
Author: Sonya Duffin
Category: Threatpost
Tags: Breach;Cloud Security;InfoSec Insider;Malware
Slug: 2022-02-09-3-tips-for-facing-the-harsh-truths-of-cybersecurity-in-2022-part-i

[Source](https://threatpost.com/harsh-truths-cybersecurity-tips/178311/){:target="_blank" rel="noopener"}

> Sonya Duffin, ransomware and data-protection expert at Veritas Technologies, shares three steps organizations can take today to reduce cyberattack fallout. [...]
