Title: The Week in Ransomware - February 18th 2022 - Mergers & Acquisitions
Date: 2022-02-18T18:17:25-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-18-the-week-in-ransomware-february-18th-2022-mergers-acquisitions

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-february-18th-2022-mergers-and-acquisitions/){:target="_blank" rel="noopener"}

> The big news this week is that the Conti ransomware gang has recruited the core developers and managers of the TrickBot group, the developers of the notorious TrickBot malware. [...]
