Title: FBI says more cyber attacks come from China than everywhere else combined
Date: 2022-02-03T05:58:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-03-fbi-says-more-cyber-attacks-come-from-china-than-everywhere-else-combined

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/03/fbi_china_threat_to_usa/){:target="_blank" rel="noopener"}

> Currently investigating over 2,000 attacks on US targets – new file every 12 hours US Federal Bureau of Investigation director Christopher Wray has named China as the source of more cyber-attacks on the USA than all other nations combined.... [...]
