Title: How $323M in crypto was stolen from a blockchain bridge called Wormhole
Date: 2022-02-04T16:00:30+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;bridges;cryptocurrency;defi;hacking
Slug: 2022-02-04-how-323m-in-crypto-was-stolen-from-a-blockchain-bridge-called-wormhole

[Source](https://arstechnica.com/?p=1831683){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson | Getty Images) This is a story about how a simple software bug allowed the fourth-biggest cryptocurrency theft ever. Hackers stole more than $323 million in cryptocurrency by exploiting a vulnerability in Wormhole, a Web-based service that allows inter-blockchain transactions. Wormhole lets people move digital coins tied to one blockchain over to a different blockchain; such blockchain bridges are particularly useful for decentralized finance (DeFi) services that operate on two or more chains, often with vastly different protocols, rules, and processes. A guardian with no teeth Bridges use wrapped tokens, which lock tokens in one blockchain [...]
