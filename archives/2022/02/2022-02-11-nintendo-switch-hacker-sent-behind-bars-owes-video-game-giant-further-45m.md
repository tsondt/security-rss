Title: Nintendo Switch hacker sent behind bars, owes video game giant further $4.5m
Date: 2022-02-11T14:54:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-11-nintendo-switch-hacker-sent-behind-bars-owes-video-game-giant-further-45m

[Source](https://portswigger.net/daily-swig/nintendo-switch-hacker-sent-behind-bars-owes-video-game-giant-further-4-5m){:target="_blank" rel="noopener"}

> Underground business sold jailbreak devices for consoles including the Nintendo Switch, 3DS, and Microsoft’s Xbox [...]
