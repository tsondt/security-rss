Title: Charming Kitten Sharpens Its Claws with PowerShell Backdoor
Date: 2022-02-02T13:58:34+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-02-02-charming-kitten-sharpens-its-claws-with-powershell-backdoor

[Source](https://threatpost.com/charming-kitten-powershell-backdoor/178158/){:target="_blank" rel="noopener"}

> The notorious Iranian APT is fortifying its arsenal with new malicious tools and evasion tactics and may even be behind the Memento ransomware. [...]
