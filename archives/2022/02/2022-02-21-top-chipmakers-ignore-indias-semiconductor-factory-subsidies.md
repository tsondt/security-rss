Title: Top chipmakers ignore India's semiconductor factory subsidies
Date: 2022-02-21T01:17:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-21-top-chipmakers-ignore-indias-semiconductor-factory-subsidies

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/21/india_semiconductor_subsidies/){:target="_blank" rel="noopener"}

> Maybe they've been spooked by new government scheme to expose VPN users' IP addresses? India has revealed the identities of companies that have applied to build semiconductor manufacturing facilities on its soil under a $10 billion subsidy scheme – and none are substantial chipmakers.... [...]
