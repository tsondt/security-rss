Title: ESET antivirus bug let attackers gain Windows SYSTEM privileges
Date: 2022-02-02T17:00:38-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-02-eset-antivirus-bug-let-attackers-gain-windows-system-privileges

[Source](https://www.bleepingcomputer.com/news/microsoft/eset-antivirus-bug-let-attackers-gain-windows-system-privileges/){:target="_blank" rel="noopener"}

> Slovak internet security firm ESET released security fixes to address a high severity local privilege escalation vulnerability affecting multiple products on systems running Windows 10 and later or Windows Server 2016 and above. [...]
