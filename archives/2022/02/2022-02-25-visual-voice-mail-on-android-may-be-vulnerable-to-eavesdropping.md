Title: Visual Voice Mail on Android may be vulnerable to eavesdropping
Date: 2022-02-25T12:49:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2022-02-25-visual-voice-mail-on-android-may-be-vulnerable-to-eavesdropping

[Source](https://www.bleepingcomputer.com/news/security/visual-voice-mail-on-android-may-be-vulnerable-to-eavesdropping/){:target="_blank" rel="noopener"}

> A security analyst has devised a way to capture Visual Voice Mail (VVM) credentials on Android devices and then remotely listen to voicemail messages without the victim's knowledge. [...]
