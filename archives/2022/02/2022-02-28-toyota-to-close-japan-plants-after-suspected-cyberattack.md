Title: Toyota to Close Japan Plants After Suspected Cyberattack
Date: 2022-02-28T17:23:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2022-02-28-toyota-to-close-japan-plants-after-suspected-cyberattack

[Source](https://threatpost.com/toyota-to-close-japan-plants-after-suspected-cyberattack/178686/){:target="_blank" rel="noopener"}

> The plants will shut down on Tuesday, halting about a third of the company’s global production. Toyota doesn’t know how long the 14 plants will be unplugged. [...]
