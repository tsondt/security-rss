Title: New Zealand government mandates bug reporting process for federal agencies
Date: 2022-02-14T13:42:48+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-14-new-zealand-government-mandates-bug-reporting-process-for-federal-agencies

[Source](https://portswigger.net/daily-swig/new-zealand-government-mandates-bug-reporting-process-for-federal-agencies){:target="_blank" rel="noopener"}

> Researchers can report vulnerabilities on a ‘no blame’ basis [...]
