Title: FBI warns of criminals escalating SIM swap attacks to steal millions
Date: 2022-02-09T07:30:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-09-fbi-warns-of-criminals-escalating-sim-swap-attacks-to-steal-millions

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-criminals-escalating-sim-swap-attacks-to-steal-millions/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) says criminals have escalated SIM swap attacks to steal millions by hijacking victims' phone numbers. [...]
