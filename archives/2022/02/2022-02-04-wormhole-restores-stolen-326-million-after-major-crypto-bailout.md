Title: Wormhole restores stolen $326 million after major crypto bailout
Date: 2022-02-04T05:09:21-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-02-04-wormhole-restores-stolen-326-million-after-major-crypto-bailout

[Source](https://www.bleepingcomputer.com/news/security/wormhole-restores-stolen-326-million-after-major-crypto-bailout/){:target="_blank" rel="noopener"}

> Cryptocurrency platform Wormhole has recovered upwards of $326 million stolen in this week's crypto hack, thanks to a major bailout. [...]
