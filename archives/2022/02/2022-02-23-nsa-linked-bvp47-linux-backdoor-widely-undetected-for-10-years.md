Title: NSA-linked Bvp47 Linux backdoor widely undetected for 10 years
Date: 2022-02-23T19:21:38-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-02-23-nsa-linked-bvp47-linux-backdoor-widely-undetected-for-10-years

[Source](https://www.bleepingcomputer.com/news/security/nsa-linked-bvp47-linux-backdoor-widely-undetected-for-10-years/){:target="_blank" rel="noopener"}

> A report released today dives deep into technical aspects of a Linux backdoor now tracked as Bvp47 that is linked to the Equation Group, the advanced persistent threat actor tied to the U.S. National Security Agency. [...]
