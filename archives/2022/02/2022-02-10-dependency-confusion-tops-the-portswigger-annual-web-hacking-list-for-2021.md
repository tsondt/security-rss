Title: Dependency confusion tops the PortSwigger annual web hacking list for 2021
Date: 2022-02-10T11:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-10-dependency-confusion-tops-the-portswigger-annual-web-hacking-list-for-2021

[Source](https://portswigger.net/daily-swig/dependency-confusion-tops-the-portswigger-annual-web-hacking-list-for-2021){:target="_blank" rel="noopener"}

> Request smuggling attacks a key theme [...]
