Title: The machine fights back: AI that fights cyber-threats on behalf of humans
Date: 2022-02-22T18:00:08+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2022-02-22-the-machine-fights-back-ai-that-fights-cyber-threats-on-behalf-of-humans

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/22/darktrace_aiutonomous_response/){:target="_blank" rel="noopener"}

> Darktrace AI learns organisation's 'pattern of life' Paid feature AI does more than recommend TV shows and validate our bank transfers. Since 2016, it has also been working behind the scenes within the security operations centre (SOC). The reason is simple: when it comes to spotting and neutralising digital threats, humans need help. As we grapple with a torrent of online threats, machines are increasingly fighting our battles for us.... [...]
