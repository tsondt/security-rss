Title: Ransomware surge prompts joint NCSC, CISA warning to safeguard systems
Date: 2022-02-10T16:38:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-10-ransomware-surge-prompts-joint-ncsc-cisa-warning-to-safeguard-systems

[Source](https://portswigger.net/daily-swig/ransomware-surge-prompts-joint-ncsc-cisa-warning-to-safeguard-systems){:target="_blank" rel="noopener"}

> Weekend attacks and assaults on the software supply chain mark evolving TTPs [...]
