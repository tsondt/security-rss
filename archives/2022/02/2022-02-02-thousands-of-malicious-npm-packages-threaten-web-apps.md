Title: Thousands of Malicious npm Packages Threaten Web Apps
Date: 2022-02-02T14:00:23+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Malware;Vulnerabilities;Web Security
Slug: 2022-02-02-thousands-of-malicious-npm-packages-threaten-web-apps

[Source](https://threatpost.com/malicious-npm-packages-web-apps/178137/){:target="_blank" rel="noopener"}

> Attackers increasingly are using malicious JavaScript packages to steal data, engage in cryptojacking and unleash botnets, offering a wide supply-chain attack surface for threat actors. [...]
