Title: GPU giant Nvidia is investigating a potential cyberattack
Date: 2022-02-25T15:51:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-25-gpu-giant-nvidia-is-investigating-a-potential-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/gpu-giant-nvidia-is-investigating-a-potential-cyberattack/){:target="_blank" rel="noopener"}

> US chipmaker giant Nvidia confirmed today it's currently investigating an "incident" that reportedly took down some of its systems for two days. [...]
