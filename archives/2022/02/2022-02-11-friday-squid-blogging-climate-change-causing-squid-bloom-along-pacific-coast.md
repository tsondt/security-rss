Title: Friday Squid Blogging: Climate Change Causing “Squid Bloom” along Pacific Coast
Date: 2022-02-11T22:07:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-02-11-friday-squid-blogging-climate-change-causing-squid-bloom-along-pacific-coast

[Source](https://www.schneier.com/blog/archives/2022/02/friday-squid-blogging-climate-change-causing-squid-bloom-along-pacific-coast.html){:target="_blank" rel="noopener"}

> The oceans are warmer, which means more squid. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
