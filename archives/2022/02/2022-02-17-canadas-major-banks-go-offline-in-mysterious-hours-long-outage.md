Title: Canada's major banks go offline in mysterious hours-long outage
Date: 2022-02-17T01:58:33-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-02-17-canadas-major-banks-go-offline-in-mysterious-hours-long-outage

[Source](https://www.bleepingcomputer.com/news/security/canadas-major-banks-go-offline-in-mysterious-hours-long-outage/){:target="_blank" rel="noopener"}

> Five major Canadian banks went offline for hours blocking access to online and mobile banking as well as e-transfers for customers. The banks hit by the outage include Royal Bank of Canada (RBC), BMO (Bank of Montreal), Scotiabank, TD Bank Canada, and the Canadian Imperial Bank of Commerce (CIBC). [...]
