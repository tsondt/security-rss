Title: Russian ransomware attacks increased during 2021, joint review finds
Date: 2022-02-09T14:07:19+00:00
Author: Dan Sabbagh Defence and security editor
Category: The Guardian
Tags: Cybercrime;Malware;Russia;Technology;Hacking;Data and computer security;Europe;Internet;World news;UK news;US news;Australia news
Slug: 2022-02-09-russian-ransomware-attacks-increased-during-2021-joint-review-finds

[Source](https://www.theguardian.com/technology/2022/feb/09/russian-ransomware-attacks-increased-during-2021-joint-review-finds){:target="_blank" rel="noopener"}

> Britain, the US and Australia point to growth in ‘sophisticated, high-impact ransomware incidents’ There have been further increases in “sophisticated, high-impact ransomware incidents” coming from Russia and other former Soviet states during 2021, Britain, the US and Australia said in a joint review of cyber-extortion trends. Universities and schools were one of the top sectors targeted in the UK last year, the National Cyber Security Centre (NCSC) said, as well as businesses, charities, law firms, councils and the NHS. Hackers are increasingly offering services or exploits “for hire”. Continue reading... [...]
