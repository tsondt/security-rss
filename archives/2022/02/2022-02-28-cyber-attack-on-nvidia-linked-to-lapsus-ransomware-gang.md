Title: Cyber-attack on Nvidia linked to Lapsus$ ransomware gang
Date: 2022-02-28T16:30:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-28-cyber-attack-on-nvidia-linked-to-lapsus-ransomware-gang

[Source](https://portswigger.net/daily-swig/cyber-attack-on-nvidia-linked-to-lapsus-ransomware-gang){:target="_blank" rel="noopener"}

> Claims that threat actors said hardware giant had ‘hacked back’ have surfaced [...]
