Title: Microsoft to block downloaded VBA macros in Office – you may be able to run 'em anyway
Date: 2022-02-08T02:53:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-08-microsoft-to-block-downloaded-vba-macros-in-office-you-may-be-able-to-run-em-anyway

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/08/microsoft_office_default_macro_block/){:target="_blank" rel="noopener"}

> Aims to make life harder for miscreants Microsoft Office will soon block untrusted Visual Basic for Applications (VBA) macros sourced from the internet by default – a security measure users can still circumvent, permissions allowing.... [...]
