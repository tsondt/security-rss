Title: JumpCloud joins the patch management crowd, starting with Windows and Mac updates
Date: 2022-02-03T19:07:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-02-03-jumpcloud-joins-the-patch-management-crowd-starting-with-windows-and-mac-updates

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/03/jumpcloud_patches/){:target="_blank" rel="noopener"}

> Linux and mobile coming soon Cloud directory specialist JumpCloud is moving into the crowded patch management market with an extension to its platform to automate patch updates.... [...]
