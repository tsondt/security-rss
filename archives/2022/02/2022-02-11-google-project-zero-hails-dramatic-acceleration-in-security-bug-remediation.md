Title: Google Project Zero hails dramatic acceleration in security bug remediation
Date: 2022-02-11T19:49:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-11-google-project-zero-hails-dramatic-acceleration-in-security-bug-remediation

[Source](https://portswigger.net/daily-swig/google-project-zero-hails-dramatic-acceleration-in-security-bug-remediation){:target="_blank" rel="noopener"}

> Researchers credit greater transparency and responsible disclosure policies for improvements in the patching process [...]
