Title: Ukraine-Russia Cyber Warzone Splits Cyber Underground
Date: 2022-02-28T21:00:32+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Web Security
Slug: 2022-02-28-ukraine-russia-cyber-warzone-splits-cyber-underground

[Source](https://threatpost.com/ukraine-russia-cyber-warzone-splits-cyber-underground/178693/){:target="_blank" rel="noopener"}

> A pro-Ukraine Conti member spilled 13 months of the ransomware group's chats, while cyber actors are rushing to align with both sides. [...]
