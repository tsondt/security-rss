Title: Expeditors shuts down global operations after likely ransomware attack
Date: 2022-02-21T13:35:45-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-02-21-expeditors-shuts-down-global-operations-after-likely-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/expeditors-shuts-down-global-operations-after-likely-ransomware-attack/){:target="_blank" rel="noopener"}

> Seattle-based logistics and freight forwarding company Expeditors International has been targeted in a cyberattack over the weekend that forced the organization to shut down most of its operations worldwide. [...]
