Title: The Account Takeover Cat-and-Mouse Game
Date: 2022-02-01T20:59:53+00:00
Author: Jason Kent
Category: Threatpost
Tags: InfoSec Insider;Web Security
Slug: 2022-02-01-the-account-takeover-cat-and-mouse-game

[Source](https://threatpost.com/account-takeover-cat-mouse-game/178128/){:target="_blank" rel="noopener"}

> ATO attacks are evolving. Jason Kent, hacker-in-residence at Cequence Security, discusses what new-style cyberattacks look like in the wild. [...]
