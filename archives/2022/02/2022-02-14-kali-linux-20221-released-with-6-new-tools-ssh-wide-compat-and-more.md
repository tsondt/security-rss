Title: Kali Linux 2022.1 released with 6 new tools, SSH wide compat, and more
Date: 2022-02-14T19:41:20-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Linux
Slug: 2022-02-14-kali-linux-20221-released-with-6-new-tools-ssh-wide-compat-and-more

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20221-released-with-6-new-tools-ssh-wide-compat-and-more/){:target="_blank" rel="noopener"}

> Offensive Security has released ​Kali Linux 2022.1, the first version of 2022, with improved accessibility features, a visual refresh, SSH wide compatibility, and of course, new toys to play with! [...]
