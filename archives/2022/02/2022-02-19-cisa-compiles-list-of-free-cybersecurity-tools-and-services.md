Title: CISA compiles list of free cybersecurity tools and services
Date: 2022-02-19T11:15:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-02-19-cisa-compiles-list-of-free-cybersecurity-tools-and-services

[Source](https://www.bleepingcomputer.com/news/security/cisa-compiles-list-of-free-cybersecurity-tools-and-services/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) has published a list of free cybersecurity services and tools to help organizations increase their security capabilities and better defend against cyberattacks. [...]
