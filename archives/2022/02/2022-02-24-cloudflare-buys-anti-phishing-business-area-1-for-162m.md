Title: Cloudflare buys anti-phishing business Area 1 for $162m
Date: 2022-02-24T14:31:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-02-24-cloudflare-buys-anti-phishing-business-area-1-for-162m

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/24/cloudflare_area_1/){:target="_blank" rel="noopener"}

> Bolstering email defences to keep cybercrooks at bay Krazy Glue of the internet Cloudflare has buffed up its email security with the purchase of anti-phishing firm Area 1.... [...]
