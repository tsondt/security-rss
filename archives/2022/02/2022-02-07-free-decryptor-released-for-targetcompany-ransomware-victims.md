Title: Free decryptor released for TargetCompany ransomware victims
Date: 2022-02-07T12:08:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-07-free-decryptor-released-for-targetcompany-ransomware-victims

[Source](https://www.bleepingcomputer.com/news/security/free-decryptor-released-for-targetcompany-ransomware-victims/){:target="_blank" rel="noopener"}

> Czech cybersecurity software firm Avast has released a decryption utility to help TargetCompany ransomware victims recover their files for free. [...]
