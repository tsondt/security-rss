Title: Tool trio released to protect JavaScript applications from malicious NPM packages
Date: 2022-02-11T11:10:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-11-tool-trio-released-to-protect-javascript-applications-from-malicious-npm-packages

[Source](https://portswigger.net/daily-swig/tool-trio-released-to-protect-javascript-applications-from-malicious-npm-packages){:target="_blank" rel="noopener"}

> Security tools inspired by recent case where a package maintainer went rogue [...]
