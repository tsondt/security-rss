Title: Emergency updates: Adobe, Chrome patch security bugs under active attack
Date: 2022-02-16T21:25:43+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-16-emergency-updates-adobe-chrome-patch-security-bugs-under-active-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/16/adobe_chrome_patch/){:target="_blank" rel="noopener"}

> Friends are always telling me... just be good to free() Adobe has released an out-of-band security update for Adobe Commerce and Magento Open Source to address active exploitation of a known vulnerability, and Google has an emergency issue, too.... [...]
