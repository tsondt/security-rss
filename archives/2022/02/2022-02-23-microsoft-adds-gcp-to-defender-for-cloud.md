Title: Microsoft adds GCP to Defender for Cloud
Date: 2022-02-23T17:29:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-02-23-microsoft-adds-gcp-to-defender-for-cloud

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/23/microsoft_defender_cloud_gcp/){:target="_blank" rel="noopener"}

> And let me clue you in: I am the one who CloudKnox Microsoft Defender's tentacles have spread to include the Google Cloud Platform (GCP) – and beefed up visibility with a public preview of CloudKnox Permissions.... [...]
