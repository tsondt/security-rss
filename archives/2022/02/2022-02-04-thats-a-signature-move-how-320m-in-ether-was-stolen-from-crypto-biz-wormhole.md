Title: That's a signature move: How $320m in Ether was stolen from crypto biz Wormhole
Date: 2022-02-04T00:42:23+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-04-thats-a-signature-move-how-320m-in-ether-was-stolen-from-crypto-biz-wormhole

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/04/wormhole_currency_theft/){:target="_blank" rel="noopener"}

> Failure to validate input in DeFi code let attacker mint money Wormhole, a protocol for connecting different blockchains, lost about $320m worth of Ether (ETH), thanks to poorly crafted code.... [...]
