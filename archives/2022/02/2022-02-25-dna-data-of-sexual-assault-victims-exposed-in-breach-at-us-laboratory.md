Title: DNA data of sexual assault victims exposed in breach at US laboratory
Date: 2022-02-25T12:00:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-25-dna-data-of-sexual-assault-victims-exposed-in-breach-at-us-laboratory

[Source](https://portswigger.net/daily-swig/dna-data-of-sexual-assault-victims-exposed-in-breach-at-us-laboratory){:target="_blank" rel="noopener"}

> Medical information included in leak after third-party compromise [...]
