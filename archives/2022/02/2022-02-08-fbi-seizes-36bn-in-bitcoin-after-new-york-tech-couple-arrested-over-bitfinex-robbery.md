Title: FBI seizes $3.6bn in Bitcoin after New York 'tech couple' arrested over Bitfinex robbery
Date: 2022-02-08T21:24:32+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-08-fbi-seizes-36bn-in-bitcoin-after-new-york-tech-couple-arrested-over-bitfinex-robbery

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/08/bitfinex_arrests_cryptocurrency/){:target="_blank" rel="noopener"}

> Ilya Lichtenstein (yes really) and partner cuffed via blockchain records Updated Two New York-based "tech entrepreneurs" were arrested on Tuesday for allegedly conspiring to launder $4.5bn in stolen cryptocurrency, the US Department of Justice said, adding it's so far recovered $3.6bn in purloined digicash - based on current prices.... [...]
