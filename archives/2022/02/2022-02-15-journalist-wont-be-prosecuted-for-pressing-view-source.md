Title: Journalist won't be prosecuted for pressing 'view source'
Date: 2022-02-15T21:57:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-15-journalist-wont-be-prosecuted-for-pressing-view-source

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/15/missouri_html_hacking/){:target="_blank" rel="noopener"}

> Despite all logic, state governor still insists hitting F12 in a web browser is 'hacking' A reporter who faced potential hacking charges for viewing website source code in his browser can rest easier now that Missouri officials have decided not to prosecute him.... [...]
