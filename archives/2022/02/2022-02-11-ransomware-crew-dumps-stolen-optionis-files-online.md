Title: Ransomware crew dumps stolen Optionis files online
Date: 2022-02-11T12:29:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-11-ransomware-crew-dumps-stolen-optionis-files-online

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/11/optionis_stolen_data/){:target="_blank" rel="noopener"}

> Suggests accounting'n'umbrella firm didn't pay ransom What appears to be stolen data belonging to customers of accounting conglomerate Optionis Group has surfaced on the dark web weeks after the firm confirmed intruders had broken into its systems.... [...]
