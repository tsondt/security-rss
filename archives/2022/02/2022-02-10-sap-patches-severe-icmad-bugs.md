Title: SAP Patches Severe ‘ICMAD’ Bugs
Date: 2022-02-10T16:39:04+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-02-10-sap-patches-severe-icmad-bugs

[Source](https://threatpost.com/sap-patches-severe-icmad-bugs/178344/){:target="_blank" rel="noopener"}

> SAP’s Patch Tuesday brought fixes for a trio of flaws in the ubiquitous ICM component in internet-exposed apps. One of them, with a risk score of 10, could allow attackers to hijack identities, steal data and more. [...]
