Title: Spot the irony: India's Reserve Bank says outsourcing and offshoring are risky
Date: 2022-02-14T03:59:49+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-14-spot-the-irony-indias-reserve-bank-says-outsourcing-and-offshoring-are-risky

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/14/reserve_bank_of_india_outsourcing_risk_warning/){:target="_blank" rel="noopener"}

> So risky that new guidelines are needed to ensure engagements don't go pear-shaped The Reserve Bank of India (RBI) has warned the nation's finance sector that outsourcing information technology jobs could "expose them to significant financial, operational and reputational risks."... [...]
