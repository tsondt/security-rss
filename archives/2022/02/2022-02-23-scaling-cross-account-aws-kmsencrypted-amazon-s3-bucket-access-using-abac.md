Title: Scaling cross-account AWS KMS–encrypted Amazon S3 bucket access using ABAC
Date: 2022-02-23T20:19:34+00:00
Author: Jorg Huser
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;ABAC;Amazon EMR;Amazon S3;Attribute-based access control;authorization;AWS Key Management Service (KMS);AWS Lake Formation;Big Data Platform;Big Data Security Management;cross-account privilege design escalation;data lake;Data Protection in Data Lakes;Key management;Key Management for Big Data;PrincipalOrgId;Resource-based policies
Slug: 2022-02-23-scaling-cross-account-aws-kmsencrypted-amazon-s3-bucket-access-using-abac

[Source](https://aws.amazon.com/blogs/security/scaling-cross-account-aws-kms-encrypted-amazon-s3-bucket-access-using-abac/){:target="_blank" rel="noopener"}

> This blog post shows you how to share encrypted Amazon Simple Storage Service (Amazon S3) buckets across accounts on a multi-tenant data lake. Our objective is to show scalability over a larger volume of accounts that can access the data lake, in a scenario where there is one central account to share from. Most use [...]
