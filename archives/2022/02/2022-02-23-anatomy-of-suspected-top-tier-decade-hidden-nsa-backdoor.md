Title: Anatomy of suspected top-tier decade-hidden NSA backdoor
Date: 2022-02-23T20:23:56+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-23-anatomy-of-suspected-top-tier-decade-hidden-nsa-backdoor

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/23/chinese_nsa_linux/){:target="_blank" rel="noopener"}

> Bvp47 of yore said to have used BPF to conceal comms in network traffic Pangu Lab has identified what it claims is a sophisticated backdoor that was used by the NSA to subvert highly targeted Linux systems around the world for more than a decade.... [...]
