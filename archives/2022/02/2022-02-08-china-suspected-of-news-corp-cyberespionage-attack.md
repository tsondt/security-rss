Title: China Suspected of News Corp Cyberespionage Attack
Date: 2022-02-08T14:14:59+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Malware;Web Security
Slug: 2022-02-08-china-suspected-of-news-corp-cyberespionage-attack

[Source](https://threatpost.com/china-suspected-news-corp-cyberespionage/178277/){:target="_blank" rel="noopener"}

> Attackers infiltrated the media giant’s network using BEC, while Microsoft moved to stop such attacks by blocking VBA macros in 5 Windows apps. Included: more ways to help stop BEC. [...]
