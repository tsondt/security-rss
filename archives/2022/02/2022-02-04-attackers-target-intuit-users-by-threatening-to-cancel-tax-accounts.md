Title: Attackers Target Intuit Users by Threatening to Cancel Tax Accounts
Date: 2022-02-04T13:28:01+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security;Privacy;Web Security
Slug: 2022-02-04-attackers-target-intuit-users-by-threatening-to-cancel-tax-accounts

[Source](https://threatpost.com/attackers-intuit-cancel-tax-accounts/178219/){:target="_blank" rel="noopener"}

> The usual tax-season barrage of cybercriminal activity is already underway with a phishing campaign impersonating the popular accounting and tax-filing software. [...]
