Title: Couple charged with laundering proceeds from $4.5bn Bitfinex cryptocurrency hack
Date: 2022-02-09T15:31:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-09-couple-charged-with-laundering-proceeds-from-45bn-bitfinex-cryptocurrency-hack

[Source](https://portswigger.net/daily-swig/couple-charged-with-laundering-proceeds-from-4-5bn-bitfinex-cryptocurrency-hack){:target="_blank" rel="noopener"}

> US investigators recover $3.6bn in digital assets [...]
