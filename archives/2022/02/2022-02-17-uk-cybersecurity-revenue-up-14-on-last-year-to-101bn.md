Title: UK cybersecurity revenue up 14% on last year to £10.1bn
Date: 2022-02-17T08:28:05+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-02-17-uk-cybersecurity-revenue-up-14-on-last-year-to-101bn

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/17/uk_cybersecurity_revenue/){:target="_blank" rel="noopener"}

> Sector also sees record level of investment, according to government-backed research The UK government is claiming a record year for revenue in the cybersecurity sector saying the industry generated £10.1bn.... [...]
