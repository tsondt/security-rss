Title: Zero-day vulnerabilities in Nooie baby monitors could allow video feed hijack
Date: 2022-02-08T15:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-08-zero-day-vulnerabilities-in-nooie-baby-monitors-could-allow-video-feed-hijack

[Source](https://portswigger.net/daily-swig/zero-day-vulnerabilities-in-nooie-baby-monitors-could-allow-video-feed-hijack){:target="_blank" rel="noopener"}

> Unresolved vulnerabilities also create code execution risk, warns Bitdefender [...]
