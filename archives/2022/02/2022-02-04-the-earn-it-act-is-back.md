Title: The EARN IT Act Is Back
Date: 2022-02-04T15:44:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;national security policy;privacy;social media;surveillance
Slug: 2022-02-04-the-earn-it-act-is-back

[Source](https://www.schneier.com/blog/archives/2022/02/the-earn-it-act-is-back.html){:target="_blank" rel="noopener"}

> Senators have reintroduced the EARN IT Act, requiring social media companies (among others) to administer a massive surveillance operation on their users: A group of lawmakers led by Sen. Richard Blumenthal (D-CT) and Sen. Lindsey Graham (R-SC) have re-introduced the EARN IT Act, an incredibly unpopular bill from 2020 that was dropped in the face of overwhelming opposition. Let’s be clear: the new EARN IT Act would pave the way for a massive new surveillance system, run by private companies, that would roll back some of the most important privacy and security features in technology used by people around the [...]
