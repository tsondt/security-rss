Title: Quarter of a million lawyer disciplinary records leak
Date: 2022-02-28T21:53:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-28-quarter-of-a-million-lawyer-disciplinary-records-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/28/ca_legal_leak/){:target="_blank" rel="noopener"}

> When it comes to the privacy of witnesses and attorneys, this Bar is set low Approximately 260,000 nonpublic disciplinary records stored on behalf of The State Bar of California were found to be exposed to the public and to have been republished on Judyrecords.com, a website that aggregates over 630 million public court records.... [...]
