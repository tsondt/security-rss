Title: PHP Everywhere RCE flaws threaten thousands of WordPress sites
Date: 2022-02-09T16:33:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-09-php-everywhere-rce-flaws-threaten-thousands-of-wordpress-sites

[Source](https://www.bleepingcomputer.com/news/security/php-everywhere-rce-flaws-threaten-thousands-of-wordpress-sites/){:target="_blank" rel="noopener"}

> Researchers found three critical remote code execution (RCE) vulnerabilities in the PHP Everywhere plugin for WordPress, used by over 30,000 websites worldwide. [...]
