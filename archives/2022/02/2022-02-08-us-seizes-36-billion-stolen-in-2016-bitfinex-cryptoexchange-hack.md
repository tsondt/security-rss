Title: US seizes $3.6 billion stolen in 2016 Bitfinex cryptoexchange hack
Date: 2022-02-08T12:51:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-08-us-seizes-36-billion-stolen-in-2016-bitfinex-cryptoexchange-hack

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-36-billion-stolen-in-2016-bitfinex-cryptoexchange-hack/){:target="_blank" rel="noopener"}

> The US Department of Justice announced that law enforcement seized billions worth of cryptocurrency linked to the 2016 Bitfinex cryptocurrency exchange hack. [...]
