Title: Network hackers focus on selling high-value targets in the U.S.
Date: 2022-02-23T17:46:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-23-network-hackers-focus-on-selling-high-value-targets-in-the-us

[Source](https://www.bleepingcomputer.com/news/security/network-hackers-focus-on-selling-high-value-targets-in-the-us/){:target="_blank" rel="noopener"}

> A Crowdstrike report looking into access brokers' advertisements since 2019 has identified a preference in academic, government, and technology entities based in the United States. [...]
