Title: Emotet Now Spreading Through Malicious Excel Files
Date: 2022-02-16T13:39:33+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-02-16-emotet-now-spreading-through-malicious-excel-files

[Source](https://threatpost.com/emotet-spreading-malicious-excel-files/178444/){:target="_blank" rel="noopener"}

> An ongoing malicious email campaign that includes macro-laden files and multiple layers of obfuscation has been active since late December. [...]
