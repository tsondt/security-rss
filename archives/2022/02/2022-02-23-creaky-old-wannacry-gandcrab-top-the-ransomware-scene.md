Title: Creaky Old WannaCry, GandCrab Top the Ransomware Scene
Date: 2022-02-23T14:00:22+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-02-23-creaky-old-wannacry-gandcrab-top-the-ransomware-scene

[Source](https://threatpost.com/wannacry-gandcrab-top-ransomware-scene/178589/){:target="_blank" rel="noopener"}

> Nothing like zombie campaigns: WannaCry's old as dirt, and GandCrab threw in the towel years ago. They're on auto-pilot at this point, researchers say. [...]
