Title: Vendors are Fixing Security Flaws Faster
Date: 2022-02-16T13:00:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-02-16-vendors-are-fixing-security-flaws-faster

[Source](https://www.schneier.com/blog/archives/2022/02/vendors-are-fixing-security-flaws-faster.html){:target="_blank" rel="noopener"}

> Google’s Project Zero is reporting that software vendors are patching their code faster. tl;dr In 2021, vendors took an average of 52 days to fix security vulnerabilities reported from Project Zero. This is a significant acceleration from an average of about 80 days 3 years ago. In addition to the average now being well below the 90-day deadline, we have also seen a dropoff in vendors missing the deadline (or the additional 14-day grace period). In 2021, only one bug exceeded its fix deadline, though 14% of bugs required the grace period. Differences in the amount of time it takes [...]
