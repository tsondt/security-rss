Title: Ukrainian DDoS Attacks Should Put US on Notice–Researchers
Date: 2022-02-17T16:04:36+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Critical Infrastructure;Government;Hacks;Malware;Web Security
Slug: 2022-02-17-ukrainian-ddos-attacks-should-put-us-on-noticeresearchers

[Source](https://threatpost.com/ukrainian-ddos-attacks-should-put-us-on-notice-researchers/178498/){:target="_blank" rel="noopener"}

> On Tuesday, institutions central to Ukraine’s military and economy were hit with denial-of-service (DoS) attacks. Impact was limited, but the ramifications are not. [...]
