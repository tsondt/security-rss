Title: Google Drive flags macOS '.DS_Store' files for copyright violation
Date: 2022-02-18T04:10:51-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Technology
Slug: 2022-02-18-google-drive-flags-macos-ds_store-files-for-copyright-violation

[Source](https://www.bleepingcomputer.com/news/security/google-drive-flags-macos-ds-store-files-for-copyright-violation/){:target="_blank" rel="noopener"}

> Google Drive was seen flagging '.DS_Store' files generated by macOS file systems as a violation of its copyright infringement policy. '.DS_Store' is a metadata file commonly seen by Apple users when they transfer their folders and archives from a macOS to a non-Apple operating system, like Windows. [...]
