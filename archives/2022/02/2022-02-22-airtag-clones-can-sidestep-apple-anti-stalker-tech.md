Title: Airtag clones can sidestep Apple anti-stalker tech
Date: 2022-02-22T11:14:54+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-22-airtag-clones-can-sidestep-apple-anti-stalker-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/22/apple_airtags_protections_bypass/){:target="_blank" rel="noopener"}

> Open source + public key generation = no alerts, says infosec startup An infosec startup says it has built an Apple Airtag clone that bypasses anti-stalking protection features while running on Apple's Find My protocol.... [...]
