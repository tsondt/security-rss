Title: Friday Squid Blogging: Squid Videos
Date: 2022-02-23T22:22:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-02-23-friday-squid-blogging-squid-videos

[Source](https://www.schneier.com/blog/archives/2022/02/friday-squid-blogging-squid-videos.html){:target="_blank" rel="noopener"}

> Here are six beautiful squid videos. I know nothing more about them. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
