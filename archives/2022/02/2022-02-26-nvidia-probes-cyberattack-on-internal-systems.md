Title: Nvidia probes cyberattack on internal systems
Date: 2022-02-26T00:39:29+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-02-26-nvidia-probes-cyberattack-on-internal-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/26/in_brief_security/){:target="_blank" rel="noopener"}

> Also don't try to unlock your GPU cards with fake mining tool, and more In brief Nvidia is probing what may be a ransomware infection that caused outages on its internal network.... [...]
