Title: Bypassing Apple’s AirTag Security
Date: 2022-02-23T12:28:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;iPhone;privacy;surveillance;tracking
Slug: 2022-02-23-bypassing-apples-airtag-security

[Source](https://www.schneier.com/blog/archives/2022/02/bypassing-apples-airtag-security.html){:target="_blank" rel="noopener"}

> A Berlin-based company has developed an AirTag clone that bypasses Apple’s anti-stalker security systems. Source code for these AirTag clones is available online. So now we have several problems with the system. Apple’s anti-stalker security only works with iPhones. (Apple wrote an Android app that can detect AirTags, but how many people are going to download it?) And now non-AirTags can piggyback on Apple’s system without triggering the alarms. Apple didn’t think this through nearly as well as it claims to have. I think the general problem is one that I have written about before : designers just don’t have [...]
