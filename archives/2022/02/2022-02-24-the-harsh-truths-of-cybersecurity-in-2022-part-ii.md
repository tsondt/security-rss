Title: The Harsh Truths of Cybersecurity in 2022, Part II
Date: 2022-02-24T21:11:33+00:00
Author: Sonya Duffin
Category: Threatpost
Tags: Breach;InfoSec Insider;Malware
Slug: 2022-02-24-the-harsh-truths-of-cybersecurity-in-2022-part-ii

[Source](https://threatpost.com/harsh-truths-cybersecurity-part-two/178447/){:target="_blank" rel="noopener"}

> Sonya Duffin, ransomware and data-protection expert at Veritas Technologies, shares three steps organizations can take today to reduce cyberattack fallout. [...]
