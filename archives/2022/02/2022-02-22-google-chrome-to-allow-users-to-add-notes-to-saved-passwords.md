Title: Google Chrome to allow users to add notes to saved passwords
Date: 2022-02-22T14:46:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Google;Security;Software
Slug: 2022-02-22-google-chrome-to-allow-users-to-add-notes-to-saved-passwords

[Source](https://www.bleepingcomputer.com/news/google/google-chrome-to-allow-users-to-add-notes-to-saved-passwords/){:target="_blank" rel="noopener"}

> Google is testing a new Chrome feature that allows users to add notes on passwords saved in the web browser. [...]
