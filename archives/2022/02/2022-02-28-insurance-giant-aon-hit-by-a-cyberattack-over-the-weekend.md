Title: Insurance giant AON hit by a cyberattack over the weekend
Date: 2022-02-28T10:39:08-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-28-insurance-giant-aon-hit-by-a-cyberattack-over-the-weekend

[Source](https://www.bleepingcomputer.com/news/security/insurance-giant-aon-hit-by-a-cyberattack-over-the-weekend/){:target="_blank" rel="noopener"}

> Professional services and insurance giant AON has suffered a cyberattack that impacted a "limited" number of systems. [...]
