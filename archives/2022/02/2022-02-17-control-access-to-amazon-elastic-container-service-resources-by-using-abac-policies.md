Title: Control access to Amazon Elastic Container Service resources by using ABAC policies
Date: 2022-02-17T17:58:35+00:00
Author: Kriti Heda
Category: AWS Security
Tags: Intermediate (200);Security;Security, Identity, & Compliance;ABAC;Amazon ECS;Attribute-based access control;authorization;AWS IAM;Security Blog;Tags;TBAC
Slug: 2022-02-17-control-access-to-amazon-elastic-container-service-resources-by-using-abac-policies

[Source](https://aws.amazon.com/blogs/security/control-access-to-amazon-elastic-container-service-resources-by-using-abac-policies/){:target="_blank" rel="noopener"}

> As an AWS customer, if you use multiple Amazon Elastic Container Service (Amazon ECS) services/tasks to achieve better isolation, you often have the challenge of how to manage access to these containers. In such cases, using tags can enable you to categorize these services in different ways, such as by owner or environment. This blog [...]
