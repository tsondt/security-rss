Title: San Francisco 49ers catch ransomware, sample files leaked online
Date: 2022-02-14T22:10:43+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-14-san-francisco-49ers-catch-ransomware-sample-files-leaked-online

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/14/49ers_ransomware_blackbyte/){:target="_blank" rel="noopener"}

> US football team bitten by BlackByte gang the FBI just warned us about American football team the San Francisco 49ers have been hit by ransomware, with the criminals responsible claiming to have stolen corporate data and threatened to publish it.... [...]
