Title: Google Groups unsubscribe feature abused to remove members without consent
Date: 2022-02-23T11:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-23-google-groups-unsubscribe-feature-abused-to-remove-members-without-consent

[Source](https://portswigger.net/daily-swig/google-groups-unsubscribe-feature-abused-to-remove-members-without-consent){:target="_blank" rel="noopener"}

> ‘This could have destroyed the Google Payment system flow,’ security researcher tells The Daily Swig [...]
