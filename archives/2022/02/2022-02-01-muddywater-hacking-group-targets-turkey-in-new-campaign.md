Title: MuddyWater hacking group targets Turkey in new campaign
Date: 2022-02-01T02:30:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-01-muddywater-hacking-group-targets-turkey-in-new-campaign

[Source](https://www.bleepingcomputer.com/news/security/muddywater-hacking-group-targets-turkey-in-new-campaign/){:target="_blank" rel="noopener"}

> The Iranian-backed MuddyWater hacking group is conducting a new malicious campaign targeting private Turkish organizations and governmental institutions. [...]
