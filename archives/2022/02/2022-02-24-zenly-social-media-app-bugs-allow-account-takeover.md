Title: Zenly Social-Media App Bugs Allow Account Takeover
Date: 2022-02-24T20:07:34+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Mobile Security;Vulnerabilities
Slug: 2022-02-24-zenly-social-media-app-bugs-allow-account-takeover

[Source](https://threatpost.com/zenly-bugs-account-takeover/178646/){:target="_blank" rel="noopener"}

> A pair of bugs in the Snap-owned tracking app reveal phone numbers and allow account hijacking. [...]
