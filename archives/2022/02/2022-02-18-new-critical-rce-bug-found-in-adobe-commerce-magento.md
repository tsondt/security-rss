Title: New Critical RCE Bug Found in Adobe Commerce, Magento
Date: 2022-02-18T16:55:59+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-02-18-new-critical-rce-bug-found-in-adobe-commerce-magento

[Source](https://threatpost.com/new-critical-rce-bug-found-in-adobe-commerce-magento/178554/){:target="_blank" rel="noopener"}

> Adobe updated its recent out-of-band security advisory to add another critical bug, while researchers put out a PoC for the one it emergency-fixed last weekend. [...]
