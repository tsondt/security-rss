Title: Hackers to NVIDIA: Remove mining cap or we leak hardware data
Date: 2022-02-28T14:13:55-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-02-28-hackers-to-nvidia-remove-mining-cap-or-we-leak-hardware-data

[Source](https://www.bleepingcomputer.com/news/security/hackers-to-nvidia-remove-mining-cap-or-we-leak-hardware-data/){:target="_blank" rel="noopener"}

> The Lapsus$ data extortion group has released what they claim to be data stolen from the Nvidia GPU designer. The cache is an archive that is almost 20GB large. [...]
