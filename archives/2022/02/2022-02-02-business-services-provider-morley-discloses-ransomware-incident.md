Title: Business services provider Morley discloses ransomware incident
Date: 2022-02-02T11:02:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-02-business-services-provider-morley-discloses-ransomware-incident

[Source](https://www.bleepingcomputer.com/news/security/business-services-provider-morley-discloses-ransomware-incident/){:target="_blank" rel="noopener"}

> Morley Companies Inc. disclosed a data breach after suffering a ransomware attack on August 1st, 2021, allowing threat actors to steal data before encrypting files. [...]
