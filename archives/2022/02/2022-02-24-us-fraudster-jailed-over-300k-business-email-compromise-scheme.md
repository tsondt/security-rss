Title: US fraudster jailed over $300k business email compromise scheme
Date: 2022-02-24T13:20:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-24-us-fraudster-jailed-over-300k-business-email-compromise-scheme

[Source](https://portswigger.net/daily-swig/us-fraudster-jailed-over-300k-business-email-compromise-scheme){:target="_blank" rel="noopener"}

> Scheme unraveled when defendant tried to cash ill-gotten gains [...]
