Title: LockBit, Conti most active ransomware targeting industrial sector
Date: 2022-02-23T05:48:16-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-02-23-lockbit-conti-most-active-ransomware-targeting-industrial-sector

[Source](https://www.bleepingcomputer.com/news/security/lockbit-conti-most-active-ransomware-targeting-industrial-sector/){:target="_blank" rel="noopener"}

> Ransomware attacks extended into the industrial sector last year to such a degree that this type of incident became the number one threat in the industrial sector. [...]
