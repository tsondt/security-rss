Title: Your software supply chain is under attack – how are you going to react?
Date: 2022-02-15T07:30:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-02-15-your-software-supply-chain-is-under-attack-how-are-you-going-to-react

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/15/your_software_supply_chain_is/){:target="_blank" rel="noopener"}

> This webinar will prepare you for when the worst really does happen Paid Post Imagine if someone told you that your software has a backdoor that leaves you vulnerable to cyberattack – along with your entire downstream customer base?... [...]
