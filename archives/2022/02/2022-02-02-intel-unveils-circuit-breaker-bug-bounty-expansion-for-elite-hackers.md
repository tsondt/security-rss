Title: Intel unveils Circuit Breaker bug bounty expansion for elite hackers
Date: 2022-02-02T12:54:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-02-intel-unveils-circuit-breaker-bug-bounty-expansion-for-elite-hackers

[Source](https://www.bleepingcomputer.com/news/security/intel-unveils-circuit-breaker-bug-bounty-expansion-for-elite-hackers/){:target="_blank" rel="noopener"}

> Intel says its engineers are partnering with security researchers to hunt for vulnerabilities in firmware, GPUs, hypervisors, chipsets, and other products in a new expansion to its bug bounty program. [...]
