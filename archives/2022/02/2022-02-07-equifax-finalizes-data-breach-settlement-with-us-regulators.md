Title: Equifax finalizes data breach settlement with US regulators
Date: 2022-02-07T16:32:29+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-07-equifax-finalizes-data-breach-settlement-with-us-regulators

[Source](https://portswigger.net/daily-swig/equifax-finalizes-data-breach-settlement-with-us-regulators){:target="_blank" rel="noopener"}

> Settlement includes up to $425 million to help people affected by 2017 mega breach [...]
