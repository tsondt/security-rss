Title: QuaDream, 2nd Israeli Spyware Firm, Weaponizes iPhone Bug
Date: 2022-02-07T18:49:59+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Mobile Security;Privacy;Vulnerabilities;Web Security
Slug: 2022-02-07-quadream-2nd-israeli-spyware-firm-weaponizes-iphone-bug

[Source](https://threatpost.com/quadream-israeli-spyware-weaponized-iphone-bug/178252/){:target="_blank" rel="noopener"}

> The now-patched flaw that led to the ForcedEntry exploit of iPhones was exploited by both NSO Group and a different, newly detailed surveillance vendor. [...]
