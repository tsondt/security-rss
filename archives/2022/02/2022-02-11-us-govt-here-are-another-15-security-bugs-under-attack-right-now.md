Title: US govt: Here are another 15 security bugs under attack right now
Date: 2022-02-11T19:02:46+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-11-us-govt-here-are-another-15-security-bugs-under-attack-right-now

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/11/cisa_database/){:target="_blank" rel="noopener"}

> Best plug HiveNightmare if you haven't already, unless you like new admins The US government has added 15 vulns under active attack to a little-known but very useful public database: its Known Exploited Vulnerabilities catalogue.... [...]
