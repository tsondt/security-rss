Title: Suspected data breach at Washington State Department of Licensing
Date: 2022-02-07T11:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-07-suspected-data-breach-at-washington-state-department-of-licensing

[Source](https://portswigger.net/daily-swig/suspected-data-breach-at-washington-state-department-of-licensing){:target="_blank" rel="noopener"}

> Agency pulls POLARIS platform offline as investigation continues [...]
