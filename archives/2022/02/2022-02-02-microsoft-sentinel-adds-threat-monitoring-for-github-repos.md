Title: Microsoft Sentinel adds threat monitoring for GitHub repos
Date: 2022-02-02T11:29:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-02-microsoft-sentinel-adds-threat-monitoring-for-github-repos

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-sentinel-adds-threat-monitoring-for-github-repos/){:target="_blank" rel="noopener"}

> Microsoft says its cloud-native SIEM (Security Information and Event Management) platform now allows to detect potential ransomware activity using the Fusion machine learning model. [...]
