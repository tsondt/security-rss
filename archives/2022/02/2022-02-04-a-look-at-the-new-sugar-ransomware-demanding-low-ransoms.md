Title: A look at the new Sugar ransomware demanding low ransoms
Date: 2022-02-04T13:16:06-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-04-a-look-at-the-new-sugar-ransomware-demanding-low-ransoms

[Source](https://www.bleepingcomputer.com/news/security/a-look-at-the-new-sugar-ransomware-demanding-low-ransoms/){:target="_blank" rel="noopener"}

> A new Sugar Ransomware operation actively targets individual computers, rather than corporate networks, with low ransom demands. [...]
