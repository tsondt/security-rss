Title: Cyberattacker hits German service station petrol terminal provider
Date: 2022-02-01T15:50:29+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2022-02-01-cyberattacker-hits-german-service-station-petrol-terminal-provider

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/01/oiltrading/){:target="_blank" rel="noopener"}

> Shell station logistics supplier Oiltanking 'operating with limited capacity' Two companies owned by Hamburg-based company fuel group Marquard & Bahls are battling cyberattackers, with loading and unloading systems at the German arm of petrol tank terminal provider Oiltanking affected.... [...]
