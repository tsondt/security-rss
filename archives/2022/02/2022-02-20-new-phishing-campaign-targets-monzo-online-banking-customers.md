Title: New phishing campaign targets Monzo online-banking customers
Date: 2022-02-20T10:09:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-02-20-new-phishing-campaign-targets-monzo-online-banking-customers

[Source](https://www.bleepingcomputer.com/news/security/new-phishing-campaign-targets-monzo-online-banking-customers/){:target="_blank" rel="noopener"}

> Users of Monzo, one of the UK's most popular digital-only banking platforms, are being targeted by phishing messages supported by a growing network of malicious websites. [...]
