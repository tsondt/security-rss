Title: ExpressVPN offering $100,000 to first person who hacks its servers
Date: 2022-02-08T11:18:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-08-expressvpn-offering-100000-to-first-person-who-hacks-its-servers

[Source](https://www.bleepingcomputer.com/news/security/expressvpn-offering-100-000-to-first-person-who-hacks-its-servers/){:target="_blank" rel="noopener"}

> ExpressVPN has updated its bug bounty program to make it more inviting to ethical hackers, now offering a one-time $100,000 bug bounty to whoever can compromise its systems. [...]
