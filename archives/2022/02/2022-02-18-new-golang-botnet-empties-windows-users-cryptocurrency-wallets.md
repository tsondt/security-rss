Title: New Golang botnet empties Windows users’ cryptocurrency wallets
Date: 2022-02-18T15:27:47-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-02-18-new-golang-botnet-empties-windows-users-cryptocurrency-wallets

[Source](https://www.bleepingcomputer.com/news/security/new-golang-botnet-empties-windows-users-cryptocurrency-wallets/){:target="_blank" rel="noopener"}

> A new Golang-based botnet under active development has been ensnaring hundreds of Windows devices each time its operators deploy a new command and control (C2) server. [...]
