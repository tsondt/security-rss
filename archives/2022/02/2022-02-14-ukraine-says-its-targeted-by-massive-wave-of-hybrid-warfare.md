Title: Ukraine says it’s targeted by ‘massive wave of hybrid warfare’
Date: 2022-02-14T16:21:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-14-ukraine-says-its-targeted-by-massive-wave-of-hybrid-warfare

[Source](https://www.bleepingcomputer.com/news/security/ukraine-says-it-s-targeted-by-massive-wave-of-hybrid-warfare-/){:target="_blank" rel="noopener"}

> The Security Service of Ukraine (SSU) today said the country is the target of an ongoing "wave of hybrid warfare," aiming to instill anxiety and undermine Ukrainian society's confidence in the state's ability to defend its citizens. [...]
