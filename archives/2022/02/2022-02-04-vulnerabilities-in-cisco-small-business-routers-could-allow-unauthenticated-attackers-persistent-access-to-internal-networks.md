Title: Vulnerabilities in Cisco Small Business routers could allow unauthenticated attackers persistent access to internal networks
Date: 2022-02-04T13:35:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-04-vulnerabilities-in-cisco-small-business-routers-could-allow-unauthenticated-attackers-persistent-access-to-internal-networks

[Source](https://portswigger.net/daily-swig/vulnerabilities-in-cisco-small-business-routers-could-allow-unauthenticated-attackers-persistent-access-to-internal-networks){:target="_blank" rel="noopener"}

> Critical security bugs inherited by multiple products [...]
