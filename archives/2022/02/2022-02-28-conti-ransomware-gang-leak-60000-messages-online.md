Title: Conti ransomware gang leak: 60,000 messages online
Date: 2022-02-28T18:14:52+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-28-conti-ransomware-gang-leak-60000-messages-online

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/28/conti_ransomware_gang_chats_leaked/){:target="_blank" rel="noopener"}

> What looks like Jabber chat app files published after pro-Russia pledge Activists have reportedly leaked the contents of internal chats from the Russia-affiliated Conti ransomware gang as the Ukraine war continues.... [...]
