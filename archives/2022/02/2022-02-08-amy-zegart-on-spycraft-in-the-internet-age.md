Title: Amy Zegart on Spycraft in the Internet Age
Date: 2022-02-08T16:52:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;books;cyberespionage;espionage;intelligence;secrecy
Slug: 2022-02-08-amy-zegart-on-spycraft-in-the-internet-age

[Source](https://www.schneier.com/blog/archives/2022/02/amy-zegart-on-spycraft-in-the-internet-age.html){:target="_blank" rel="noopener"}

> Amy Zegart has a new book: Spies, Lies, and Algorithms: The History and Future of American Intelligence. Wired has an excerpt : In short, data volume and accessibility are revolutionizing sensemaking. The intelligence playing field is leveling­ — and not in a good way. Intelligence collectors are everywhere, and government spy agencies are drowning in data. This is a radical new world and intelligence agencies are struggling to adapt to it. While secrets once conferred a huge advantage, today open source information increasingly does. Intelligence used to be a race for insight where great powers were the only ones with [...]
