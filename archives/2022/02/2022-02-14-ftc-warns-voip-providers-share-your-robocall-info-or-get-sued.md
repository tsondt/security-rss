Title: FTC warns VoIP providers: Share your robocall info or get sued
Date: 2022-02-14T14:05:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-14-ftc-warns-voip-providers-share-your-robocall-info-or-get-sued

[Source](https://www.bleepingcomputer.com/news/security/ftc-warns-voip-providers-share-your-robocall-info-or-get-sued/){:target="_blank" rel="noopener"}

> The US Federal Trade Commission (FTC) said today that it will take legal action against Voice-over-Internet Protocol (VoIP) service providers who do not hand over information requested during robocall investigations. [...]
