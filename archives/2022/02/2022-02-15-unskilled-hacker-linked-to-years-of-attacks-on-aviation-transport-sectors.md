Title: Unskilled hacker linked to years of attacks on aviation, transport sectors
Date: 2022-02-15T07:28:39-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-02-15-unskilled-hacker-linked-to-years-of-attacks-on-aviation-transport-sectors

[Source](https://www.bleepingcomputer.com/news/security/unskilled-hacker-linked-to-years-of-attacks-on-aviation-transport-sectors/){:target="_blank" rel="noopener"}

> For years, a low-skilled attacker has been using off-the-shelf malware in malicious campaigns aimed at companies in the aviation sector as well as in other sensitive industries. [...]
