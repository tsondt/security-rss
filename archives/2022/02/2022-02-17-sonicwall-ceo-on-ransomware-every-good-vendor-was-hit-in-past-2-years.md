Title: SonicWall CEO on ransomware: Every good vendor was hit in past 2 years
Date: 2022-02-17T16:34:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-02-17-sonicwall-ceo-on-ransomware-every-good-vendor-was-hit-in-past-2-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/17/sonicwall_cyber_threat_2022/){:target="_blank" rel="noopener"}

> Public and private sector both under attack as malware evolution accelerates SonicWall's annual cyber-threat report shows ransomware-spreading miscreants are making hay and getting quicker at doing so.... [...]
