Title: Microsoft Patch Tuesday, February 2022 Edition
Date: 2022-02-08T22:38:16+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;Allan Liska;Andrew Cunningham;Ars Technica;CVE-2022-21989;CVE-2022-21996;CVE-2022-22005;Greg Wiseman;Immersive Labs;Kevin Breen;Print Spooler;Rapid7;Recorded Future;Win32k
Slug: 2022-02-08-microsoft-patch-tuesday-february-2022-edition

[Source](https://krebsonsecurity.com/2022/02/microsoft-patch-tuesday-february-2022-edition/){:target="_blank" rel="noopener"}

> Microsoft today released software updates to plug security holes in its Windows operating systems and related software. This month’s relatively light patch batch is refreshingly bereft of any zero-day threats, or even scary critical vulnerabilities. But it does fix four dozen flaws, including several that Microsoft says will likely soon be exploited by malware or malcontents. While none of the patches address bugs that earned Microsoft’s most dire “critical” rating, there are multiple “remote code execution” vulnerabilities that Redmond believes are ripe for exploitation. Among those is CVE-2022-22005, a weakness in Microsoft’s Sharepoint Server versions 2013-2019 that could be exploited [...]
