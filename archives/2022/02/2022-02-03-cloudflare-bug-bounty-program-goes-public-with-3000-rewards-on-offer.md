Title: Cloudflare bug bounty program goes public with $3,000 rewards on offer
Date: 2022-02-03T15:56:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-03-cloudflare-bug-bounty-program-goes-public-with-3000-rewards-on-offer

[Source](https://portswigger.net/daily-swig/cloudflare-bug-bounty-program-goes-public-with-3-000-rewards-on-offer){:target="_blank" rel="noopener"}

> Silicon Valley firm has paid out more than $200,000 since private program’s 2018 launch [...]
