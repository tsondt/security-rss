Title: Citibank phishing baits customers with fake suspension alerts
Date: 2022-02-24T09:00:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-24-citibank-phishing-baits-customers-with-fake-suspension-alerts

[Source](https://www.bleepingcomputer.com/news/security/citibank-phishing-baits-customers-with-fake-suspension-alerts/){:target="_blank" rel="noopener"}

> An ongoing large-scale phishing campaign is targeting customers of Citibank, requesting recipients to disclose sensitive personal details to lift alleged account holds. [...]
