Title: Google Cloud hypervisor modified to detect cryptominers without agents
Date: 2022-02-07T12:05:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-02-07-google-cloud-hypervisor-modified-to-detect-cryptominers-without-agents

[Source](https://www.bleepingcomputer.com/news/security/google-cloud-hypervisor-modified-to-detect-cryptominers-without-agents/){:target="_blank" rel="noopener"}

> Google has announced the public preview of a new Virtual Machine Threat Detection (VMTD) system that can detect cryptocurrency miners and other malware without the need for software agents. [...]
