Title: Wormhole platform hacked to steal $326 million in crypto
Date: 2022-02-02T18:58:24-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency;Security
Slug: 2022-02-02-wormhole-platform-hacked-to-steal-326-million-in-crypto

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/wormhole-platform-hacked-to-steal-326-million-in-crypto/){:target="_blank" rel="noopener"}

> Hackers have exploited a vulnerability in the Wormhole cross-chain crypto platform to steal $320 million in cryptocurrency. [...]
