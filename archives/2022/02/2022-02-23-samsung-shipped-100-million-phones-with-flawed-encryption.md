Title: Samsung shipped '100 million' phones with flawed encryption
Date: 2022-02-23T01:36:34+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-23-samsung-shipped-100-million-phones-with-flawed-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/23/samsung_encryption_phones/){:target="_blank" rel="noopener"}

> Academics found TrustZone-level code could not be trusted to keep secrets Academics at Tel Aviv University in Israel have found that recent Android-based Samsung phones shipped with design flaws that allow the extraction of secret cryptographic keys.... [...]
