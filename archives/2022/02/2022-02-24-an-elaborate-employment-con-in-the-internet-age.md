Title: An Elaborate Employment Con in the Internet Age
Date: 2022-02-24T12:13:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cons;employment;fraud;LinkedIn
Slug: 2022-02-24-an-elaborate-employment-con-in-the-internet-age

[Source](https://www.schneier.com/blog/archives/2022/02/an-elaborate-employment-con-in-the-internet-age.html){:target="_blank" rel="noopener"}

> The story is an old one, but the tech gives it a bunch of new twists : Gemma Brett, a 27-year-old designer from west London, had only been working at Madbird for two weeks when she spotted something strange. Curious about what her commute would be like when the pandemic was over, she searched for the company’s office address. The result looked nothing like the videos on Madbird’s website of a sleek workspace buzzing with creative-types. Instead, Google Street View showed an upmarket block of flats in London’s Kensington. [...] Using online reverse image searches they dug deeper. They found [...]
