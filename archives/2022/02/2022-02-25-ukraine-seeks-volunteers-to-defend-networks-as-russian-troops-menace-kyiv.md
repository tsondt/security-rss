Title: Ukraine seeks volunteers to defend networks as Russian troops menace Kyiv
Date: 2022-02-25T19:07:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-25-ukraine-seeks-volunteers-to-defend-networks-as-russian-troops-menace-kyiv

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/25/ukraine_cyber_russia/){:target="_blank" rel="noopener"}

> While Moscow tells its operators: Treat any infrastructure outage as a 'computer attack' As the Russian invasion of Ukraine continues, the latter's government is reportedly seeking cybersecurity volunteers to help defend itself. Meanwhile, Russia's CERT has warned critical infrastructure operators that any strange outages should be treated as "a computer attack."... [...]
