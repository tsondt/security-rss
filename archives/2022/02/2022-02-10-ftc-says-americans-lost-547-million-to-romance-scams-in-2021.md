Title: FTC says Americans lost $547 million to romance scams in 2021
Date: 2022-02-10T11:13:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-10-ftc-says-americans-lost-547-million-to-romance-scams-in-2021

[Source](https://www.bleepingcomputer.com/news/security/ftc-says-americans-lost-547-million-to-romance-scams-in-2021/){:target="_blank" rel="noopener"}

> The US Federal Trade Commission (FTC) said that Americans reported record high losses of $547 million to romance scams in 2021, up almost 80% compared to 2020 and over six times compared to losses reported in 2017. [...]
