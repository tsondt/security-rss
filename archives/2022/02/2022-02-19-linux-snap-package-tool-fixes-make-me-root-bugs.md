Title: Linux Snap package tool fixes make-me-root bugs
Date: 2022-02-19T00:15:57+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-19-linux-snap-package-tool-fixes-make-me-root-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/19/linux_snap_ubuntu/){:target="_blank" rel="noopener"}

> Or you could think of them as a superuser password reset function The snap-confine tool in the Linux world's Snap software packaging system can be potentially exploited by ordinary users to gain root powers, says Qualys.... [...]
