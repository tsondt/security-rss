Title: In an arms race with criminals to protect our privacy, it's too early to admit defeat
Date: 2022-02-15T10:45:06+00:00
Author: Dave Cartwright
Category: The Register
Tags: 
Slug: 2022-02-15-in-an-arms-race-with-criminals-to-protect-our-privacy-its-too-early-to-admit-defeat

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/15/encryption_debate_against1_tues/){:target="_blank" rel="noopener"}

> Government agents that really want your messages, though? Well, that can be another story Register Debate Welcome to the latest Register Debate in which writers discuss technology topics, and you the reader choose the winning argument. The format is simple: we propose a motion, the arguments for the motion will run this Monday and Wednesday, and the arguments against on Tuesday and Thursday. During the week you can cast your vote on which side you support using the poll embedded below, choosing whether you're in favor or against the motion. The final score will be announced on Friday, revealing whether [...]
