Title: Spain dismantles SIM swapping group who emptied bank accounts
Date: 2022-02-10T06:27:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-02-10-spain-dismantles-sim-swapping-group-who-emptied-bank-accounts

[Source](https://www.bleepingcomputer.com/news/security/spain-dismantles-sim-swapping-group-who-emptied-bank-accounts/){:target="_blank" rel="noopener"}

> Spanish National Police has arrested eight suspects allegedly part of a crime ring who drained bank accounts in a series of SIM swapping attacks. [...]
