Title: Swipe left: Snoops use dating apps to hook sources, says Australian Five Eyes boss
Date: 2022-02-10T04:58:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-10-swipe-left-snoops-use-dating-apps-to-hook-sources-says-australian-five-eyes-boss

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/10/spies_using_dating_apps/){:target="_blank" rel="noopener"}

> Warns that foreign interference needs more attention than terrorism Nations running online foreign influence campaigns have turned to dating apps to recruit people privy to sensitive information, according to the director general of the Australian Security and Intelligence Organisation (ASIO), the nation's security agency directed against external threats and a key partner in the Five Eyes security alliance.... [...]
