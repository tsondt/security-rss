Title: Zero-day RCE flaw among multiple bugs found in Extensis Portfolio – research
Date: 2022-02-22T14:27:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-22-zero-day-rce-flaw-among-multiple-bugs-found-in-extensis-portfolio-research

[Source](https://portswigger.net/daily-swig/zero-day-rce-flaw-among-multiple-bugs-found-in-extensis-portfolio-research){:target="_blank" rel="noopener"}

> Extensis was described as ‘not receptive’ to disclosure and has allegedly not provided patches Researchers have disclosed critical vulnerabilities in Extensis Portfolio, including a zero-day flaw that [...]
