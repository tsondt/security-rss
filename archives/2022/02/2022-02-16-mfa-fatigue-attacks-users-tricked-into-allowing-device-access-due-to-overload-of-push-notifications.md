Title: MFA fatigue attacks: Users tricked into allowing device access due to overload of push notifications
Date: 2022-02-16T15:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-16-mfa-fatigue-attacks-users-tricked-into-allowing-device-access-due-to-overload-of-push-notifications

[Source](https://portswigger.net/daily-swig/mfa-fatigue-attacks-users-tricked-into-allowing-device-access-due-to-overload-of-push-notifications){:target="_blank" rel="noopener"}

> Social engineering technique confuses victims to gain entry to their accounts [...]
