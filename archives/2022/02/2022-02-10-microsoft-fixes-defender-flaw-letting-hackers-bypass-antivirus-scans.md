Title: Microsoft fixes Defender flaw letting hackers bypass antivirus scans
Date: 2022-02-10T19:20:20-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-10-microsoft-fixes-defender-flaw-letting-hackers-bypass-antivirus-scans

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-defender-flaw-letting-hackers-bypass-antivirus-scans/){:target="_blank" rel="noopener"}

> Microsoft has recently addressed a weakness in the Microsoft Defender Antivirus on Windows that allowed attackers to plant and execute malicious payloads without triggering Defender's malware detection engine. [...]
