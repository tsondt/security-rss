Title: Critical vulnerabilities in Zabbix Web Frontend allow authentication bypass, code execution on servers
Date: 2022-02-18T14:41:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-18-critical-vulnerabilities-in-zabbix-web-frontend-allow-authentication-bypass-code-execution-on-servers

[Source](https://portswigger.net/daily-swig/critical-vulnerabilities-in-zabbix-web-frontend-allow-authentication-bypass-code-execution-on-servers){:target="_blank" rel="noopener"}

> Patch now to protect, say researchers [...]
