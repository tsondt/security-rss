Title: Meta and Chime sue Nigerians behind Facebook, Instagram phishing
Date: 2022-02-09T10:10:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-09-meta-and-chime-sue-nigerians-behind-facebook-instagram-phishing

[Source](https://www.bleepingcomputer.com/news/security/meta-and-chime-sue-nigerians-behind-facebook-instagram-phishing/){:target="_blank" rel="noopener"}

> Meta (formerly known as Facebook) has filed a joint lawsuit with Chime, a financial technology and digital banking company, against two Nigerian individuals who allegedly used Instagram and Facebook accounts to impersonate Chime and target its users in phishing attacks. [...]
