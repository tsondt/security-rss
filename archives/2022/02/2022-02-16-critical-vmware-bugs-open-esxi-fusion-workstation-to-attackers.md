Title: Critical VMware Bugs Open ESXi, Fusion & Workstation to Attackers
Date: 2022-02-16T15:59:14+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2022-02-16-critical-vmware-bugs-open-esxi-fusion-workstation-to-attackers

[Source](https://threatpost.com/critical-vmware-bugs-esxi-fusion-workstation/178461/){:target="_blank" rel="noopener"}

> A group of five security vulnerabilities could lead to a range of bad outcomes for virtual-machine enthusiasts, including command execution and DoS. [...]
