Title: Open-source Kubernetes tool Argo CD has a high-severity path traversal flaw: Patch now
Date: 2022-02-04T15:22:33+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-04-open-source-kubernetes-tool-argo-cd-has-a-high-severity-path-traversal-flaw-patch-now

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/04/argo_cd_0day_kubernetes/){:target="_blank" rel="noopener"}

> Craft Helm chart, receive secrets A zero-day vulnerability in open-source Kubernetes development tool Argo lets malicious people steal passwords from git-crypt and other sensitive information by simply uploading a crafted Helm chart.... [...]
