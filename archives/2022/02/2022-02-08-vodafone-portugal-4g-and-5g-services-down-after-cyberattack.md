Title: Vodafone Portugal 4G and 5G services down after cyberattack
Date: 2022-02-08T10:49:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-08-vodafone-portugal-4g-and-5g-services-down-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/vodafone-portugal-4g-and-5g-services-down-after-cyberattack/){:target="_blank" rel="noopener"}

> Vodafone Portugal suffered a cyberattack causing country-wide service outages, including the disruption of 4G/5G data networks, SMS texts, and television services. [...]
