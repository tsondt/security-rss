Title: New tool can uncover redacted, pixelated text to reveal sensitive data
Date: 2022-02-15T15:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-15-new-tool-can-uncover-redacted-pixelated-text-to-reveal-sensitive-data

[Source](https://portswigger.net/daily-swig/new-tool-can-uncover-redacted-pixelated-text-to-reveal-sensitive-data){:target="_blank" rel="noopener"}

> Developer warns that redaction method is insecure [...]
