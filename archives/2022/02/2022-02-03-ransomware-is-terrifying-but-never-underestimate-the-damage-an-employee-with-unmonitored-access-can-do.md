Title: Ransomware is terrifying – but never underestimate the damage an employee with unmonitored access can do
Date: 2022-02-03T18:00:14+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-02-03-ransomware-is-terrifying-but-never-underestimate-the-damage-an-employee-with-unmonitored-access-can-do

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/03/ransomware_terrifying/){:target="_blank" rel="noopener"}

> This webinar shows you how to keep a lookout for internal data leaks Webinar Is the biggest threat to your data a mysterious ransomware merchant or an advanced persistent threat cartel?... [...]
