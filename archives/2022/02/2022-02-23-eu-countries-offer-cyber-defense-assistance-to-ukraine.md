Title: EU countries offer cyber-defense assistance to Ukraine
Date: 2022-02-23T16:40:25+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-23-eu-countries-offer-cyber-defense-assistance-to-ukraine

[Source](https://portswigger.net/daily-swig/eu-countries-offer-cyber-defense-assistance-to-ukraine){:target="_blank" rel="noopener"}

> Increase in cyber-attacks expected to accompany further incursions into Ukrainian territory [...]
