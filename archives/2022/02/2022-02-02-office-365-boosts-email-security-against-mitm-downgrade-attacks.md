Title: Office 365 boosts email security against MITM, downgrade attacks
Date: 2022-02-02T14:24:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-02-office-365-boosts-email-security-against-mitm-downgrade-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/office-365-boosts-email-security-against-mitm-downgrade-attacks/){:target="_blank" rel="noopener"}

> Microsoft has added SMTP MTA Strict Transport Security (MTA-STS) support to Exchange Online to ensure Office 365 customers' email communication integrity and security. [...]
