Title: Interpol: Policing model needs to change with cybercrime
Date: 2022-02-17T18:55:32+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-02-17-interpol-policing-model-needs-to-change-with-cybercrime

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/17/interpol_cybercrime/){:target="_blank" rel="noopener"}

> Law enforcement to work in a more networked, borderless fashion – just like the crooks The digitalisation of the global workforce in the face of a pandemic has led criminals to upgrade their working model, and now law enforcement must too.... [...]
