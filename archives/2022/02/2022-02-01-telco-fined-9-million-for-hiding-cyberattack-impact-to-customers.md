Title: Telco fined €9 million for hiding cyberattack impact to customers
Date: 2022-02-01T05:27:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-01-telco-fined-9-million-for-hiding-cyberattack-impact-to-customers

[Source](https://www.bleepingcomputer.com/news/security/telco-fined-9-million-for-hiding-cyberattack-impact-to-customers/){:target="_blank" rel="noopener"}

> The Greek data protection supervisory authority has imposed fines of 5,850,000 EUR ($6.55 million) to COSMOTE and 3,250,000 EUR ($3.65 million) to OTE, for leaking sensitive customer communication data due to insufficient security measures. [...]
