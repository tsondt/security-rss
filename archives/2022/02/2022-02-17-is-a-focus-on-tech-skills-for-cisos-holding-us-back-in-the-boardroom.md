Title: Is a focus on tech skills for CISOs holding us back in the boardroom?
Date: 2022-02-17T07:30:11+00:00
Author: The Masked CISO
Category: The Register
Tags: 
Slug: 2022-02-17-is-a-focus-on-tech-skills-for-cisos-holding-us-back-in-the-boardroom

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/17/vectra_masked_ciso/){:target="_blank" rel="noopener"}

> The Vectra Masked CISO series gives security leaders a place to expose the biggest issues in security and advise peers on how to overcome them. Advertorial Cyber security is a fledgeling compared to industries like risk management - Lloyd’s insurance was founded in 1688! The CISO title is even younger, first appearing around 2005. But the role has still never been clearly defined, and every CISO is working differently.... [...]
