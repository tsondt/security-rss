Title: UK anti-encryption drive meets fierce resistance from privacy, security advocates
Date: 2022-02-08T14:27:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-08-uk-anti-encryption-drive-meets-fierce-resistance-from-privacy-security-advocates

[Source](https://portswigger.net/daily-swig/uk-anti-encryption-drive-meets-fierce-resistance-from-privacy-security-advocates){:target="_blank" rel="noopener"}

> Privacy campaigners sign open letter urging government to reconsider E2EE stance [...]
