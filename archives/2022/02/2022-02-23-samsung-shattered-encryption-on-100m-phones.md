Title: Samsung Shattered Encryption on 100M Phones
Date: 2022-02-23T21:29:30+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Mobile Security;Privacy;Vulnerabilities
Slug: 2022-02-23-samsung-shattered-encryption-on-100m-phones

[Source](https://threatpost.com/samsung-shattered-encryption-on-100m-phones/178606/){:target="_blank" rel="noopener"}

> One cryptography expert said that 'serious flaws' in the way Samsung phones encrypt sensitive material, as revealed by academics, are 'embarrassingly bad.' [...]
