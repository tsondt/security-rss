Title: Nigerian hacker pleads guilty to stealing payroll deposits
Date: 2022-02-23T13:47:55-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-02-23-nigerian-hacker-pleads-guilty-to-stealing-payroll-deposits

[Source](https://www.bleepingcomputer.com/news/security/nigerian-hacker-pleads-guilty-to-stealing-payroll-deposits/){:target="_blank" rel="noopener"}

> A Nigerian national named Charles Onus has pled guilty in the District Court of the Southern District of New York to hacking into a payroll company's user accounts and stealing payroll deposits. [...]
