Title: Decryption key released for DeadBolt ransomware after QNAP NAS devices infected
Date: 2022-02-01T13:59:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-01-decryption-key-released-for-deadbolt-ransomware-after-qnap-nas-devices-infected

[Source](https://portswigger.net/daily-swig/decryption-key-released-for-deadbolt-ransomware-after-qnap-nas-devices-infected){:target="_blank" rel="noopener"}

> Tool enables decryption key to work after forced firmware update rendered it useless [...]
