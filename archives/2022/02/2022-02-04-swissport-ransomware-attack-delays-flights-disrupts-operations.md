Title: Swissport ransomware attack delays flights, disrupts operations
Date: 2022-02-04T09:29:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-04-swissport-ransomware-attack-delays-flights-disrupts-operations

[Source](https://www.bleepingcomputer.com/news/security/swissport-ransomware-attack-delays-flights-disrupts-operations/){:target="_blank" rel="noopener"}

> Aviation services company Swissport International has disclosed a ransomware attack that has impacted its IT infrastructure and services, causing flights to suffer delays. [...]
