Title: Cookware giant Meyer discloses cyberattack that impacted employees
Date: 2022-02-21T11:42:21-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-21-cookware-giant-meyer-discloses-cyberattack-that-impacted-employees

[Source](https://www.bleepingcomputer.com/news/security/cookware-giant-meyer-discloses-cyberattack-that-impacted-employees/){:target="_blank" rel="noopener"}

> Meyer Corporation, the largest cookware distributor in the U.S., and the second-largest globally, has informed U.S. Attorney General offices of a data breach affecting thousands of its employees. [...]
