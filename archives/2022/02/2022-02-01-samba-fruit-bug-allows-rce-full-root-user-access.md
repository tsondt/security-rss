Title: Samba ‘Fruit’ Bug Allows RCE, Full Root User Access
Date: 2022-02-01T20:02:02+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-02-01-samba-fruit-bug-allows-rce-full-root-user-access

[Source](https://threatpost.com/samba-fruit-bug-rce-root-access/178141/){:target="_blank" rel="noopener"}

> The issue in the file-sharing and interop platform also affects Red Hat, SUSE Linux and Ubuntu packages. [...]
