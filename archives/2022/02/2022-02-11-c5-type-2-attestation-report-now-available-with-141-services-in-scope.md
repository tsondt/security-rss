Title: C5 Type 2 attestation report now available with 141 services in scope
Date: 2022-02-11T17:54:49+00:00
Author: Mercy Kanengoni
Category: AWS Security
Tags: Foundational (100);Security;Security, Identity, & Compliance;Auditing;AWS security;C5;Compliance
Slug: 2022-02-11-c5-type-2-attestation-report-now-available-with-141-services-in-scope

[Source](https://aws.amazon.com/blogs/security/c5-type-2-attestation-report-now-available-with-141-services-in-scope/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the issuance of the new Cloud Computing Compliance Controls Catalogue (C5) Type 2 attestation report. We added 18 additional services and service features to the scope of the 2021 report. Germany’s national cybersecurity authority, Bundesamt für Sicherheit in der Informationstechnik (BSI), established C5 to define a reference [...]
