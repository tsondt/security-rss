Title: On the Irish Health Services Executive Hack
Date: 2022-02-11T12:17:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hacking;Ireland;ransomware;reports;security policies
Slug: 2022-02-11-on-the-irish-health-services-executive-hack

[Source](https://www.schneier.com/blog/archives/2022/02/on-the-irish-health-services-executive-hack.html){:target="_blank" rel="noopener"}

> A detailed report of the 2021 ransomware attack against Ireland’s Health Services Executive lists some really bad security practices : The report notes that: The HSE did not have a Chief Information Security Officer (CISO) or a “single responsible owner for cybersecurity at either senior executive or management level to provide leadership and direction. It had no documented cyber incident response runbooks or IT recovery plans (apart from documented AD recovery plans) for recovering from a wide-scale ransomware event. Under-resourced Information Security Managers were not performing their business as usual role (including a NIST-based cybersecurity review of systems) but were [...]
