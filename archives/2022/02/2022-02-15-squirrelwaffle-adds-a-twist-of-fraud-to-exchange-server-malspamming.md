Title: SquirrelWaffle Adds a Twist of Fraud to Exchange Server Malspamming
Date: 2022-02-15T22:31:33+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-02-15-squirrelwaffle-adds-a-twist-of-fraud-to-exchange-server-malspamming

[Source](https://threatpost.com/squirrelwaffle-fraud-exchange-server-malspamming/178434/){:target="_blank" rel="noopener"}

> Researchers have never before seen SquirrelWaffle attackers use typosquatting to keep sending spam once a targeted Exchange server has been patched for ProxyLogon/ProxyShell. [...]
