Title: News Corp discloses hack from "persistent" nation state cyber attacks
Date: 2022-02-04T09:03:26-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-02-04-news-corp-discloses-hack-from-persistent-nation-state-cyber-attacks

[Source](https://www.bleepingcomputer.com/news/security/news-corp-discloses-hack-from-persistent-nation-state-cyber-attacks/){:target="_blank" rel="noopener"}

> American media and publishing giant News Corp has disclosed today that it was the target of a "persistent" cyberattack. The attack discovered sometime this January, reportedly allowed threat actors to access emails and documents of some News Corp employees, including journalists. [...]
