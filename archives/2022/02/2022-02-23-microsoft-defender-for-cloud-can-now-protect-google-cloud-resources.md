Title: Microsoft Defender for Cloud can now protect Google Cloud resources
Date: 2022-02-23T09:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-23-microsoft-defender-for-cloud-can-now-protect-google-cloud-resources

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-for-cloud-can-now-protect-google-cloud-resources/){:target="_blank" rel="noopener"}

> Microsoft announced today that Microsoft Defender for Cloud now also comes with native protection for Google Cloud Platform (GCP) environments, providing security recommendations and threat detection across clouds. [...]
