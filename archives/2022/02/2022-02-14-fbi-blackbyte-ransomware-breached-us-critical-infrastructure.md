Title: FBI: BlackByte ransomware breached US critical infrastructure
Date: 2022-02-14T10:41:14-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-14-fbi-blackbyte-ransomware-breached-us-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/fbi-blackbyte-ransomware-breached-us-critical-infrastructure/){:target="_blank" rel="noopener"}

> The US Federal Bureau of Investigation (FBI) revealed that the BlackByte ransomware group has breached the networks of at least three organizations from US critical infrastructure sectors in the last three months. [...]
