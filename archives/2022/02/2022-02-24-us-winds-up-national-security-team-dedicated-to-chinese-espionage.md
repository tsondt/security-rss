Title: US winds up national security team dedicated to Chinese espionage
Date: 2022-02-24T05:05:50+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-24-us-winds-up-national-security-team-dedicated-to-chinese-espionage

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/24/us_china_initiative_ended/){:target="_blank" rel="noopener"}

> Still plans to make life hard for Beijing, but wants to reduce the chances of domestic division The United States' National Security Division will wind up its "China Initiative" – an effort to combat what then-attorney general Jeff Sessions described in 2018 as "systematic and calculated threats" posed by Beijing-backed economic espionage.... [...]
