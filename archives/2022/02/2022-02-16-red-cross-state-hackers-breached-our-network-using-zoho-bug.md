Title: Red Cross: State hackers breached our network using Zoho bug
Date: 2022-02-16T11:32:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-16-red-cross-state-hackers-breached-our-network-using-zoho-bug

[Source](https://www.bleepingcomputer.com/news/security/red-cross-state-hackers-breached-our-network-using-zoho-bug/){:target="_blank" rel="noopener"}

> The International Committee of the Red Cross (ICRC) said today that the hack disclosed last month against its servers was a targeted attack likely coordinated by a state-backed hacking group. [...]
