Title: Ransomware gangs, hackers pick sides over Russia invading Ukraine
Date: 2022-02-25T15:13:20-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-02-25-ransomware-gangs-hackers-pick-sides-over-russia-invading-ukraine

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-hackers-pick-sides-over-russia-invading-ukraine/){:target="_blank" rel="noopener"}

> Hacker crews are picking sides as the Russian invasion into Ukraine continues, issuing bans and threats for supporters of the opposite side. [...]
