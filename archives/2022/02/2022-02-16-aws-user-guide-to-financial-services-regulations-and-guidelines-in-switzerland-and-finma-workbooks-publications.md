Title: AWS User Guide to Financial Services Regulations and Guidelines in Switzerland and FINMA workbooks publications
Date: 2022-02-16T22:10:01+00:00
Author: Margo Cronin
Category: AWS Security
Tags: Announcements;Compliance;Financial Services;Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS Financial Services Guide;FINMA;Outsourcing Guidelines;Security Blog;Swiss banking regulations;Switzerland
Slug: 2022-02-16-aws-user-guide-to-financial-services-regulations-and-guidelines-in-switzerland-and-finma-workbooks-publications

[Source](https://aws.amazon.com/blogs/security/aws-user-guide-to-financial-services-regulations-and-guidelines-in-switzerland-and-finma/){:target="_blank" rel="noopener"}

> AWS is pleased to announce the publication of the AWS User Guide to Financial Services Regulations and Guidelines in Switzerland whitepaper and workbooks. This guide refers to certain rules applicable to financial institutions in Switzerland, including banks, insurance companies, stock exchanges, securities dealers, portfolio managers, trustees and other financial entities which are overseen (directly or [...]
