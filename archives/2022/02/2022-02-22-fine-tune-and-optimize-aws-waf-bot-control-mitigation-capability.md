Title: Fine-tune and optimize AWS WAF Bot Control mitigation capability
Date: 2022-02-22T18:47:58+00:00
Author: Dmitriy Novikov
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;AWS security;AWS WAF;AWS WAF Bot Control
Slug: 2022-02-22-fine-tune-and-optimize-aws-waf-bot-control-mitigation-capability

[Source](https://aws.amazon.com/blogs/security/fine-tune-and-optimize-aws-waf-bot-control-mitigation-capability/){:target="_blank" rel="noopener"}

> Introduction A few years ago at Sydney Summit, I had an excellent question from one of our attendees. She asked me to help her design a cost-effective, reliable, and not overcomplicated solution for protection against simple bots for her web-facing resources on Amazon Web Services (AWS). I remember the occasion because with the release of [...]
