Title: Vice Society said to be behind digital break-in at UK umbrella and accounting group
Date: 2022-02-08T11:45:12+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-02-08-vice-society-said-to-be-behind-digital-break-in-at-uk-umbrella-and-accounting-group

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/08/optionis_vice_society/){:target="_blank" rel="noopener"}

> As Optionis Group CEO confirms to contractors their data was leaked online Optionis, the group that includes umbrella and accountancy companies providing services to tech contractors, has confirmed that following last month's digital break-in customer data is being leaked online.... [...]
