Title: Ukrainian military agencies, state-owned banks hit by DDoS attacks
Date: 2022-02-15T13:56:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-15-ukrainian-military-agencies-state-owned-banks-hit-by-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-military-agencies-state-owned-banks-hit-by-ddos-attacks/){:target="_blank" rel="noopener"}

> The Ministry of Defense and the Armed Forces of Ukraine and two of the country's state-owned banks, Privatbank (Ukraine's largest bank) and Oschadbank (the State Savings Bank), are being hammered by Distributed Denial-of-Service (DDoS) attacks. [...]
