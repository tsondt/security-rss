Title: The Week in Ransomware - February 4th 2022 - Critical Infrastructure
Date: 2022-02-04T19:15:26-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-04-the-week-in-ransomware-february-4th-2022-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-february-4th-2022-critical-infrastructure/){:target="_blank" rel="noopener"}

> Critical infrastructure suffered ransomware attacks, with threat actors targeting an oil petrol distributor and oil terminals in major ports in different attacks. [...]
