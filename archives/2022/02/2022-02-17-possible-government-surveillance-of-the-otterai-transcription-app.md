Title: Possible Government Surveillance of the Otter.ai Transcription App
Date: 2022-02-17T16:40:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-02-17-possible-government-surveillance-of-the-otterai-transcription-app

[Source](https://www.schneier.com/blog/archives/2022/02/possible-government-surveillance-of-the-otter-ai-transcription-app.html){:target="_blank" rel="noopener"}

> A reporter interviews a Uyghur human-rights advocate, and uses the Otter.ai transcription app. The next day, I received an odd note from Otter.ai, the automated transcription app that I had used to record the interview. It read: “Hey Phelim, to help us improve your Otter’s experience, what was the purpose of this particular recording with titled ‘Mustafa Aksu’ created at ‘2021-11-08 11:02:41’?” Customer service or Chinese surveillance? Turns out it’s hard to tell. [...]
