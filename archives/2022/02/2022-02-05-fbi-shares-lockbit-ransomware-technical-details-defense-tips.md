Title: FBI shares Lockbit ransomware technical details, defense tips
Date: 2022-02-05T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-05-fbi-shares-lockbit-ransomware-technical-details-defense-tips

[Source](https://www.bleepingcomputer.com/news/security/fbi-shares-lockbit-ransomware-technical-details-defense-tips/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) has released technical details and indicators of compromise associated with Lockbit ransomware attacks in a new flash alert published this Friday. [...]
