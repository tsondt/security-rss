Title: Russian Govt. Continues Carding Shop Crackdown
Date: 2022-02-10T01:34:48+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Web Fraud 2.0;Alexander Kovalev;Artem Bystrykh;Denis Pachevsky;Department K;Ferum Shop;Flashpoint;Gemini Advisory;Get-Net LLC;Infraud;Saratovfilm Film Company LLC;Sky-Fraud;Stas Alforov;TASS;Transtekhkom LLC;Trump's-Dumps;Unicc;Vladislav Gilev;Yaroslav Solovyov
Slug: 2022-02-10-russian-govt-continues-carding-shop-crackdown

[Source](https://krebsonsecurity.com/2022/02/russian-govt-continues-carding-shop-crackdown/){:target="_blank" rel="noopener"}

> Russian authorities have arrested six men accused of operating some of the most active online bazaars for selling stolen payment card data. The crackdown — the second closure of major card fraud shops by Russian authorities in as many weeks — comes closely behind Russia’s arrest of 14 alleged affiliates of the REvil ransomware gang, and has many in the cybercrime underground asking who might be next. Dept. K’s message for Trump’s Dumps users. On Feb. 7 and 8, the domains for the carding shops Trump’s Dumps, Ferum Shop, Sky-Fraud and UAS were seized by Department K, a division of [...]
