Title: Red Cross Hack Linked to Iranian Influence Operation?
Date: 2022-02-16T16:44:19+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Ne'er-Do-Well News;Ransomware;fbi;FireEye;ICRC hack;International Committee for the Red Cross;KELA;kelvinmiddelkoop@hotmail.com;RaidForums;Red Cross and Red Crescent Movement;Restoring Family Links;Sheriff;threat_actor;unindicted
Slug: 2022-02-16-red-cross-hack-linked-to-iranian-influence-operation

[Source](https://krebsonsecurity.com/2022/02/red-cross-hack-linked-to-iranian-influence-operation/){:target="_blank" rel="noopener"}

> A network intrusion at the International Committee for the Red Cross (ICRC) in January led to the theft of personal information on more than 500,000 people receiving assistance from the group. KrebsOnSecurity has learned that the email address used by a cybercriminal actor who offered to sell the stolen ICRC data also was used to register multiple domain names the FBI says are tied to a sprawling media influence operation originating from Iran. On Jan. 19, the ICRC disclosed the compromise of servers hosting the personal information of more than 500,000 people receiving services from the Red Cross and Red [...]
