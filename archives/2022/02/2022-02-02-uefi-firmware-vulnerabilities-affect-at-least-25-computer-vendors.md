Title: UEFI firmware vulnerabilities affect at least 25 computer vendors
Date: 2022-02-02T06:17:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-02-uefi-firmware-vulnerabilities-affect-at-least-25-computer-vendors

[Source](https://www.bleepingcomputer.com/news/security/uefi-firmware-vulnerabilities-affect-at-least-25-computer-vendors/){:target="_blank" rel="noopener"}

> Researchers from firmware protection company Binarly have discovered critical vulnerabilities in the UEFI firmware from InsydeH2O used by multiple computer vendors such as Fujitsu, Intel, AMD, Lenovo, Dell, ASUS, HP, Siemens, Microsoft, and Acer. [...]
