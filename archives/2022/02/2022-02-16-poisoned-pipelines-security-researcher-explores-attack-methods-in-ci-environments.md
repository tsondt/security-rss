Title: Poisoned pipelines: Security researcher explores attack methods in CI environments
Date: 2022-02-16T14:32:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-16-poisoned-pipelines-security-researcher-explores-attack-methods-in-ci-environments

[Source](https://portswigger.net/daily-swig/poisoned-pipelines-security-researcher-explores-attack-methods-in-ci-environments){:target="_blank" rel="noopener"}

> Attack vector abuses permissions to force CI pipelines to execute arbitrary commands [...]
