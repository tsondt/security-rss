Title: MoleRats APT Flaunts New Trojan in Latest Cyberespionage Campaign
Date: 2022-02-09T14:03:18+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware;Privacy;Web Security
Slug: 2022-02-09-molerats-apt-flaunts-new-trojan-in-latest-cyberespionage-campaign

[Source](https://threatpost.com/molerats-apt-trojan-cyberespionage-campaign/178305/){:target="_blank" rel="noopener"}

> Researchers from Proofpoint have spotted a new Middle East-targeted phishing campaign that delivers a novel malware dubbed NimbleMamba. [...]
