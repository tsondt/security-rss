Title: CISA publishes list of free security tools for business protection
Date: 2022-02-18T20:08:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-18-cisa-publishes-list-of-free-security-tools-for-business-protection

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/18/cisa_free_security/){:target="_blank" rel="noopener"}

> Agency quiet on the selection criteria but at least the price is right The US Cybersecurity and Infrastructure Agency (CISA) has published a web catalog of free cybersecurity resources in the hope that those overseeing critical infrastructure can use the tools to better secure their systems.... [...]
