Title: Hacking group 'ModifiedElephant' evaded discovery for a decade
Date: 2022-02-10T15:02:17-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-10-hacking-group-modifiedelephant-evaded-discovery-for-a-decade

[Source](https://www.bleepingcomputer.com/news/security/hacking-group-modifiedelephant-evaded-discovery-for-a-decade/){:target="_blank" rel="noopener"}

> Threat analysts have linked a decade of activity to an APT (advanced persistent threat) actor called 'ModifiedElephant', who has managed to remain elusive to all threat intelligence firms since 2012. [...]
