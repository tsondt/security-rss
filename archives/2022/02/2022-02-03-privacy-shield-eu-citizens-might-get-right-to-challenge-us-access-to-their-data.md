Title: Privacy Shield: EU citizens might get right to challenge US access to their data
Date: 2022-02-03T21:34:12+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-02-03-privacy-shield-eu-citizens-might-get-right-to-challenge-us-access-to-their-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/03/privacy_shield_progress/){:target="_blank" rel="noopener"}

> Are we nearly there yet? Officials from the EU and US are nearing a solution in long-running negotiations over transatlantic data sharing.... [...]
