Title: How the growing Russian ransomware threat is costing companies dear
Date: 2022-02-05T10:00:04+00:00
Author: Rob Davies and Dan Milmo
Category: The Guardian
Tags: Cybercrime;Data and computer security;Technology;Internet;Hacking;Russia;Organised crime;Privacy;Data protection;Malware;UK news;World news
Slug: 2022-02-05-how-the-growing-russian-ransomware-threat-is-costing-companies-dear

[Source](https://www.theguardian.com/technology/2022/feb/05/how-the-growing-russian-ransomware-threat-is-costing-companies-dear){:target="_blank" rel="noopener"}

> With KP Snacks the latest cyber-attack victim, firms must learn to defend themselves against a mounting menace The January snow lay thick on the Moscow ground, as masked officers of the FSB – Russia’s fearsome security agency – prepared to smash down the doors at one of 25 addresses they would raid that day. Their target was REvil, a shadowy conclave of hackers that claimed to have stolen more than $100m (£74m) a year through “ransomware” attacks, before suddenly disappearing. Continue reading... [...]
