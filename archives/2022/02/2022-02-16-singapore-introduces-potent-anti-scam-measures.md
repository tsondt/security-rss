Title: Singapore introduces potent anti-scam measures
Date: 2022-02-16T07:03:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-02-16-singapore-introduces-potent-anti-scam-measures

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/16/singapore_anti_scam_measures/){:target="_blank" rel="noopener"}

> Plans to block more scam sites, share liability between banks and customers Singapore will step up up efforts to stamp out phishing and spoofing, ministers told the island nation's parliament on Tuesday.... [...]
