Title: Microsoft bolsters Edge browser security with enhanced features
Date: 2022-02-17T15:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-17-microsoft-bolsters-edge-browser-security-with-enhanced-features

[Source](https://portswigger.net/daily-swig/microsoft-bolsters-edge-browser-security-with-enhanced-features){:target="_blank" rel="noopener"}

> Latest protections unveiled [...]
