Title: Strengthen protection for your GCE VMs with new FIDO security key support
Date: 2022-02-17T20:00:00+00:00
Author: Christiaan Brand
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-02-17-strengthen-protection-for-your-gce-vms-with-new-fido-security-key-support

[Source](https://cloud.google.com/blog/products/identity-security/protect-gce-vms-with-managed-fido-security-keys/){:target="_blank" rel="noopener"}

> With the release of OpenSSH 8.2 almost two years ago, native support for FIDO authentication became an option in SSH. This meant that you could have your SSH private key protected in a purpose-built security key, rather than storing the key locally on a disk where it may be more susceptible to compromise. Building on this capability, today we are excited to announce in public preview that physical security keys can be used to authenticate to Google Compute Engine (GCE) virtual machine (VM) instances that use our OS Login service for SSH management. These advances in OpenSSH made it easier [...]
