Title: Microsoft Exchange servers hacked to deploy Cuba ransomware
Date: 2022-02-24T12:06:54-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-24-microsoft-exchange-servers-hacked-to-deploy-cuba-ransomware

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-servers-hacked-to-deploy-cuba-ransomware/){:target="_blank" rel="noopener"}

> The Cuba ransomware operation is exploiting Microsoft Exchange vulnerabilities to gain initial access to corporate networks and encrypt devices. [...]
