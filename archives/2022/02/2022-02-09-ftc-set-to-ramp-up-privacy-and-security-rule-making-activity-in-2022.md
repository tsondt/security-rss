Title: FTC set to ramp up privacy and security rule-making activity in 2022
Date: 2022-02-09T11:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-09-ftc-set-to-ramp-up-privacy-and-security-rule-making-activity-in-2022

[Source](https://portswigger.net/daily-swig/ftc-set-to-ramp-up-privacy-and-security-rule-making-activity-in-2022){:target="_blank" rel="noopener"}

> Recent moves from the US government agency have laid the groundwork for significant changes to businesses’ compliance obligations, writes US attorney David Oberly [...]
