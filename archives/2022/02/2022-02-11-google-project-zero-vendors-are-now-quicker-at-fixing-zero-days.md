Title: Google Project Zero: Vendors are now quicker at fixing zero-days
Date: 2022-02-11T12:40:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-11-google-project-zero-vendors-are-now-quicker-at-fixing-zero-days

[Source](https://www.bleepingcomputer.com/news/security/google-project-zero-vendors-are-now-quicker-at-fixing-zero-days/){:target="_blank" rel="noopener"}

> Google's Project Zero has published a report showing that organizations took less time to address the zero-day vulnerabilities that the team reported last year. [...]
