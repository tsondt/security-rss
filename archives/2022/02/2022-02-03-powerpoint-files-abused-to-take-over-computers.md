Title: PowerPoint Files Abused to Take Over Computers
Date: 2022-02-03T14:00:25+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-02-03-powerpoint-files-abused-to-take-over-computers

[Source](https://threatpost.com/powerpoint-abused-take-over-computers/178182/){:target="_blank" rel="noopener"}

> Attackers are using socially engineered emails with.ppam file attachments that hide malware that can rewrite Windows registry settings on targeted machines. [...]
