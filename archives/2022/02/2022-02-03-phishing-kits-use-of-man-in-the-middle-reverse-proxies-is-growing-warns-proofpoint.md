Title: Phishing kits' use of man-in-the-middle reverse proxies is growing, warns Proofpoint
Date: 2022-02-03T20:47:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-03-phishing-kits-use-of-man-in-the-middle-reverse-proxies-is-growing-warns-proofpoint

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/03/proofpoint_mitm_reverse_proxies/){:target="_blank" rel="noopener"}

> Spoof site looks real because it is... but you're not talking to who you think In the beginning we had passwords. Their hackability made a lot of people very angry and passwords were widely regarded as a bad move. Then we had two-factor authentication – and now Proofpoint reckons criminals online are able to start bypassing them with transparent reverse proxies.... [...]
