Title: Ex-Gumshoe Nabs Cybercrooks with FBI Tactics
Date: 2022-02-09T14:00:57+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Malware;Podcasts;Web Security
Slug: 2022-02-09-ex-gumshoe-nabs-cybercrooks-with-fbi-tactics

[Source](https://threatpost.com/gumshoe-nabs-cybercrooks-fbi-tactics/178298/){:target="_blank" rel="noopener"}

> Crane Hassold, former FBI analyst turned director of threat intel at Abnormal Security, shares stories from his covert work with cyberattackers. [...]
