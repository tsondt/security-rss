Title: Time for people to patch backup plugin for WordPress
Date: 2022-02-21T09:41:08+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-02-21-time-for-people-to-patch-backup-plugin-for-wordpress

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/21/in_brief_security/){:target="_blank" rel="noopener"}

> Plus advice for Cisco admins from the NSA and blurring's not the best In brief If you're using the UpdraftPlus WordPress plugin to back up your systems, you'll need it patched – or else risk sharing your backups with strangers.... [...]
