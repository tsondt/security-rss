Title: How healthcare can strengthen its own cybersecurity resilience
Date: 2022-02-11T17:00:00+00:00
Author: Seth Rosenblatt
Category: GCP Security
Tags: Healthcare & Life Sciences;Identity & Security
Slug: 2022-02-11-how-healthcare-can-strengthen-its-own-cybersecurity-resilience

[Source](https://cloud.google.com/blog/products/identity-security/how-healthcare-can-strengthen-its-own-cybersecurity-resilience/){:target="_blank" rel="noopener"}

> With all the crises that have buffeted the healthcare and life sciences industries the past two years, one that often gets overlooked is the unceasing wave of cyberattacks aimed at medical, research, educational, and public health organizations. These attacks have shut down critical systems, attempted to steal vaccines and other research, and even halted paychecks from reaching front-line healthcare workers. Coming at the wrong time, these attacks can be as detrimental as a new outbreak or shortage of supplies by causing delays in care, or worse; distracted administrators are forced to tackle network and computer systems failures when so much [...]
