Title: Secret CIA Data Collection Program
Date: 2022-02-15T15:56:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-02-15-secret-cia-data-collection-program

[Source](https://www.schneier.com/blog/archives/2022/02/secret-cia-data-collection-program.html){:target="_blank" rel="noopener"}

> Two US Senators claim that the CIA has been running an unregulated — and almost certainly illegal — mass surveillance program on Americans. The senator’s statement. Some declassified information from the CIA. No real details yet. [...]
