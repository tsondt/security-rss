Title: Friday Squid Blogging: Are Squid from Another Planet?
Date: 2022-02-04T22:15:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2022-02-04-friday-squid-blogging-are-squid-from-another-planet

[Source](https://www.schneier.com/blog/archives/2022/02/friday-squid-blogging-are-squid-from-another-planet.html){:target="_blank" rel="noopener"}

> An actually serious scientific journal has published a paper speculating that octopus and squid could be of extraterrestrial origin. News article. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
