Title: Insurance Coverage for NotPetya Losses
Date: 2022-02-28T12:26:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cybersecurity;economics of security;insurance;Russia;Ukraine
Slug: 2022-02-28-insurance-coverage-for-notpetya-losses

[Source](https://www.schneier.com/blog/archives/2022/02/insurance-coverage-for-notpetya-losses.html){:target="_blank" rel="noopener"}

> Tarah Wheeler and Josephine Wolff analyze a recent court decision that the NotPetya attacks are not considered an act of war under the wording of Merck’s insurance policy, and that the insurers must pay the $1B+ claim. Wheeler and Wolff argue that the judge “did the right thing for the wrong reasons..” [...]
