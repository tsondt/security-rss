Title: Massive cyberattack takes Ukraine military, big bank websites offline
Date: 2022-02-15T19:45:42+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-02-15-massive-cyberattack-takes-ukraine-military-big-bank-websites-offline

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/15/ukraine_cyberattack/){:target="_blank" rel="noopener"}

> What geopolitical standoff could this possibly be linked to? The websites of the Ukrainian military and at least two of the nation's biggest banks were knocked offline in a cyberattack today.... [...]
