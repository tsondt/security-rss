Title: Meta: Ukrainian officials, military targeted by Ghostwriter hackers
Date: 2022-02-28T08:34:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-28-meta-ukrainian-officials-military-targeted-by-ghostwriter-hackers

[Source](https://www.bleepingcomputer.com/news/security/meta-ukrainian-officials-military-targeted-by-ghostwriter-hackers/){:target="_blank" rel="noopener"}

> Facebook (now known as Meta) says it took down accounts used by a Belarusian-linked hacking group (UNC1151 or Ghostwriter) to target Ukrainian officials and military personnel on its platform. [...]
