Title: Taiwan cracks down on China spying on tech firms
Date: 2022-02-18T00:33:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-18-taiwan-cracks-down-on-china-spying-on-tech-firms

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/18/taiwan_chinese_espionage/){:target="_blank" rel="noopener"}

> Revised laws will regulate workers' travel to mainland in bid to protect economy Taiwan's Parliament, the Executive Yuan, yesterday revealed draft amendments to national security laws aimed at deterring and punishing Chinese economic espionage efforts directed at stealing tech industry secrets.... [...]
