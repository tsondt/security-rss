Title: Me on App Store Monopolies and Security
Date: 2022-02-01T20:26:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;cybersecurity;laws;monoculture;privacy
Slug: 2022-02-01-me-on-app-store-monopolies-and-security

[Source](https://www.schneier.com/blog/archives/2022/02/me-on-app-store-monopolies-and-security.html){:target="_blank" rel="noopener"}

> There are two bills working their way through Congress that would force companies like Apple to allow competitive app stores. Apple hates this, since it would break its monopoly, and it’s making a variety of security arguments to bolster its argument. I have written a rebuttal: I would like to address some of the unfounded security concerns raised about these bills. It’s simply not true that this legislation puts user privacy and security at risk. In fact, it’s fairer to say that this legislation puts those companies’ extractive business-models at risk. Their claims about risks to privacy and security are [...]
