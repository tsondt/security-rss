Title: Microsoft 365 gives you the tools to run your business. But where are the tools to protect it?
Date: 2022-02-07T17:00:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-02-07-microsoft-365-gives-you-the-tools-to-run-your-business-but-where-are-the-tools-to-protect-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/07/365_tools/){:target="_blank" rel="noopener"}

> Tune in this week and find out Webinar Microsoft 365 has all the tools you need to run your organization in the cloud, from personal productivity to group collaboration, to advanced analytics and security management.... [...]
