Title: Google fixes remote escalation of privileges bug on Android
Date: 2022-02-08T16:25:48-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-02-08-google-fixes-remote-escalation-of-privileges-bug-on-android

[Source](https://www.bleepingcomputer.com/news/security/google-fixes-remote-escalation-of-privileges-bug-on-android/){:target="_blank" rel="noopener"}

> Google has released the February 2022 Android security updates, addressing two critical vulnerabilities, one being a remote escalation of privilege that requires no user interaction. [...]
