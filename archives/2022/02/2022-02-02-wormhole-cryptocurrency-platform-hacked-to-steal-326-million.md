Title: Wormhole cryptocurrency platform hacked to steal $326 million
Date: 2022-02-02T18:58:24-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency;Security
Slug: 2022-02-02-wormhole-cryptocurrency-platform-hacked-to-steal-326-million

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/wormhole-cryptocurrency-platform-hacked-to-steal-326-million/){:target="_blank" rel="noopener"}

> Hackers have exploited a vulnerability in the Wormhole cross-chain crypto platform to steal approximately $326 million in cryptocurrency. [...]
