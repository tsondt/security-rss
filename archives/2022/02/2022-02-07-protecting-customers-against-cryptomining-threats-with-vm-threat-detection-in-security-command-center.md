Title: Protecting customers against cryptomining threats with VM Threat Detection in Security Command Center
Date: 2022-02-07T17:00:00+00:00
Author: Timothy Peacock
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-02-07-protecting-customers-against-cryptomining-threats-with-vm-threat-detection-in-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/virtual-machine-threat-detection-in-security-command-center/){:target="_blank" rel="noopener"}

> As organizations move to the cloud, VM-based architectures continue to make up a significant portion of compute-centric workloads. To help ensure strong protection for these deployments, we are thrilled to announce a public preview of our newest layer of threat detection in Security Command Center (SCC): Virtual Machine Threat Detection (VMTD). VMTD is a first-to-market detection capability from a major cloud provider that provides agentless memory scanning to help detect threats like cryptomining malware inside your virtual machines running in Google Cloud. The economy of scale enabled by the cloud can help fundamentally change the way security is executed for [...]
