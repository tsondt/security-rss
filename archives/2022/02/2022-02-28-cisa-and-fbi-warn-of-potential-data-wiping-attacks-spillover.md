Title: CISA and FBI warn of potential data wiping attacks spillover
Date: 2022-02-28T15:03:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-28-cisa-and-fbi-warn-of-potential-data-wiping-attacks-spillover

[Source](https://www.bleepingcomputer.com/news/security/cisa-and-fbi-warn-of-potential-data-wiping-attacks-spillover/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) and the Federal Bureau of Investigation (FBI) warned US organizations that data wiping attacks targeting Ukraine could spill over to targets from other countries. [...]
