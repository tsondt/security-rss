Title: HHS: Conti ransomware encrypted 80% of Ireland's HSE IT systems
Date: 2022-02-04T11:01:14-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2022-02-04-hhs-conti-ransomware-encrypted-80-of-irelands-hse-it-systems

[Source](https://www.bleepingcomputer.com/news/security/hhs-conti-ransomware-encrypted-80-percent-of-irelands-hse-it-systems/){:target="_blank" rel="noopener"}

> A threat brief published by the US Department of Health and Human Services (HHS) on Thursday paints a grim picture of how Ireland's health service, the HSE, was overwhelmed and had 80% of its systems encrypted during last year's Conti ransomware attack. [...]
