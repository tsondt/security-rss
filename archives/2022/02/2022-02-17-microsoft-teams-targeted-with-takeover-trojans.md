Title: Microsoft Teams Targeted With Takeover Trojans
Date: 2022-02-17T14:11:48+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-02-17-microsoft-teams-targeted-with-takeover-trojans

[Source](https://threatpost.com/microsoft-teams-targeted-takeover-trojans/178497/){:target="_blank" rel="noopener"}

> Threat actors are infiltrating the increasingly popular collaboration app to attach malicious files to chat threads that drop system-hijacking malware. [...]
