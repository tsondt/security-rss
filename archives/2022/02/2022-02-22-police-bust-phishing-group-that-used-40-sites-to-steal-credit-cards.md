Title: Police bust phishing group that used 40 sites to steal credit cards
Date: 2022-02-22T04:49:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-22-police-bust-phishing-group-that-used-40-sites-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/police-bust-phishing-group-that-used-40-sites-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> The Ukrainian cyberpolice have arrested a group of phishing actors who managed to steal payment card data from at least 70,000 people after luring them to fake mobile service top up sites. [...]
