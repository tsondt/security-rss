Title: Roaming Mantis Expands Android Backdoor to Europe
Date: 2022-02-07T17:32:14+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Privacy;Web Security
Slug: 2022-02-07-roaming-mantis-expands-android-backdoor-to-europe

[Source](https://threatpost.com/roaming-mantis-android-backdoor-europe/178247/){:target="_blank" rel="noopener"}

> The 'smishing' group lives up to its name, expanding globally and adding image exfiltration to the Wroba RAT it uses to infect mobile victims. [...]
