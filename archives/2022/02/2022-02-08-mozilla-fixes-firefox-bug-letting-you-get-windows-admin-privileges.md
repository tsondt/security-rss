Title: Mozilla fixes Firefox bug letting you get Windows admin privileges
Date: 2022-02-08T11:56:33-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-08-mozilla-fixes-firefox-bug-letting-you-get-windows-admin-privileges

[Source](https://www.bleepingcomputer.com/news/security/mozilla-fixes-firefox-bug-letting-you-get-windows-admin-privileges/){:target="_blank" rel="noopener"}

> Mozilla released a security update to address a high severity privilege escalation vulnerability found in the Mozilla Maintenance Service. [...]
