Title: Suspected Chinese spies break into cloud accounts of News Corp journalists
Date: 2022-02-04T21:35:14+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-04-suspected-chinese-spies-break-into-cloud-accounts-of-news-corp-journalists

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/04/news_corp_china_compromised/){:target="_blank" rel="noopener"}

> Read all about it – Beijing probably already has Online work accounts of News Corporation journalists were broken into by snoops seemingly with ties to China, it was claimed today.... [...]
