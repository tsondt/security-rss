Title: Russia's naval exercise near Ireland unlikely to involve cable-tapping shenanigans
Date: 2022-02-01T09:30:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-01-russias-naval-exercise-near-ireland-unlikely-to-involve-cable-tapping-shenanigans

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/01/russia_ireland_naval_exercise_not_cable_tapping/){:target="_blank" rel="noopener"}

> Muscle-flexing rather than publicised 'sabotage right here' plan A Russian naval exercise in the Atlantic, near several submarine cables between Britain, France and the US, is more likely to be sabre-rattling than an attempt to sabotage critical communication links.... [...]
