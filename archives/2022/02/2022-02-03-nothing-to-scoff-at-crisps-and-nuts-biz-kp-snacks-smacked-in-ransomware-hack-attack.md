Title: Nothing to scoff at: Crisps and nuts biz KP Snacks smacked in ransomware hack attack
Date: 2022-02-03T17:17:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-03-nothing-to-scoff-at-crisps-and-nuts-biz-kp-snacks-smacked-in-ransomware-hack-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/03/kp_snacks_ransomware_infection/){:target="_blank" rel="noopener"}

> Firm doesn't know when it'll restart salty goodness deliveries Some of Britain's favourite pub munch could end up in short supply after KP Snacks, makers of nuts and crisps, suffered a ransomware attack.... [...]
