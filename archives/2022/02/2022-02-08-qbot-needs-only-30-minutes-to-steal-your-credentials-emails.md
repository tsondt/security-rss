Title: Qbot needs only 30 minutes to steal your credentials, emails
Date: 2022-02-08T03:12:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-08-qbot-needs-only-30-minutes-to-steal-your-credentials-emails

[Source](https://www.bleepingcomputer.com/news/security/qbot-needs-only-30-minutes-to-steal-your-credentials-emails/){:target="_blank" rel="noopener"}

> The widespread malware known as Qbot (aka Qakbot or QuakBot) has recently returned to light-speed attacks, and according to analysts, it only takes around 30 minutes to steal sensitive data after the initial infection. [...]
