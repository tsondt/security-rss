Title: <span>Flurry Finance heist nets crypto thieves $295k</span>
Date: 2022-02-25T15:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-25-flurry-finance-heist-nets-crypto-thieves-295k

[Source](https://portswigger.net/daily-swig/flurry-finance-heist-nets-crypto-thieves-295k){:target="_blank" rel="noopener"}

> Theft topped out at six figures after DeFi platform blocked ‘token balance multiplier’ exploit [...]
