Title: IRS To Ditch Biometric Requirement for Online Access
Date: 2022-02-07T20:56:52+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;id.me;Internal Revenue Service;Sen. Ron Wyden;The Washington Post;U.S. General Services Administration
Slug: 2022-02-07-irs-to-ditch-biometric-requirement-for-online-access

[Source](https://krebsonsecurity.com/2022/02/irs-to-ditch-biometric-requirement-for-online-access/){:target="_blank" rel="noopener"}

> The Internal Revenue Service (IRS) said today it will be transitioning away from requiring biometric data from taxpayers who wish to access their records at the agency’s website. The reversal comes as privacy experts and lawmakers have been pushing the IRS and other federal agencies to find less intrusive methods for validating one’s identity with the U.S. government online. Late last year, the login page for the IRS was updated with text advising that by the summer of 2022, the only way for taxpayers to access their records at irs.gov will be through ID.me, an online identity verification service that [...]
