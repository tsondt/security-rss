Title: WordPress force installs UpdraftPlus patch on 3 million sites
Date: 2022-02-18T11:19:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-18-wordpress-force-installs-updraftplus-patch-on-3-million-sites

[Source](https://www.bleepingcomputer.com/news/security/wordpress-force-installs-updraftplus-patch-on-3-million-sites/){:target="_blank" rel="noopener"}

> WordPress has taken the rare step of force-updating the UpdraftPlus plugin on all sites to fix a high-severity vulnerability allowing website subscribers to download the latest database backups, which often contain credentials and PII. [...]
