Title: Hackers can crash Cisco Secure Email gateways using malicious emails
Date: 2022-02-17T11:26:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-17-hackers-can-crash-cisco-secure-email-gateways-using-malicious-emails

[Source](https://www.bleepingcomputer.com/news/security/hackers-can-crash-cisco-secure-email-gateways-using-malicious-emails/){:target="_blank" rel="noopener"}

> Cisco has addressed a high severity vulnerability that could allow remote attackers to crash Cisco Secure Email appliances using maliciously crafted email messages. [...]
