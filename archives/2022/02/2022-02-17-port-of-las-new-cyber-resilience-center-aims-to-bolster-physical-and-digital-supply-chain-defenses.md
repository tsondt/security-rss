Title: Port of LA’s new Cyber Resilience Center aims to bolster physical and digital supply chain defenses
Date: 2022-02-17T11:56:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-17-port-of-las-new-cyber-resilience-center-aims-to-bolster-physical-and-digital-supply-chain-defenses

[Source](https://portswigger.net/daily-swig/port-of-las-new-cyber-resilience-center-aims-to-bolster-physical-and-digital-supply-chain-defenses){:target="_blank" rel="noopener"}

> ‘We must take every precaution against potential cyber incidents’, port director tells The Daily Swig [...]
