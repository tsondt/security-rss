Title: How to secure API Gateway HTTP endpoints with JWT authorizer
Date: 2022-02-14T18:32:19+00:00
Author: Siva Rajamani
Category: AWS Security
Tags: Expert (400);Learning Levels;Security;Security, Identity, & Compliance;API GW;authorization;AWS security;IAM;JWT authorizer;Lambda Authorizer;Serverless
Slug: 2022-02-14-how-to-secure-api-gateway-http-endpoints-with-jwt-authorizer

[Source](https://aws.amazon.com/blogs/security/how-to-secure-api-gateway-http-endpoints-with-jwt-authorizer/){:target="_blank" rel="noopener"}

> This blog post demonstrates how you can secure Amazon API Gateway HTTP endpoints with JSON web token (JWT) authorizers. Amazon API Gateway helps developers create, publish, and maintain secure APIs at any scale, helping manage thousands of API calls. There are no minimum fees, and you only pay for the API calls you receive. Based [...]
