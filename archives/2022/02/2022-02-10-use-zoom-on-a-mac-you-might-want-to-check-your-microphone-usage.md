Title: Use Zoom on a Mac? You might want to check your microphone usage
Date: 2022-02-10T14:07:02+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-02-10-use-zoom-on-a-mac-you-might-want-to-check-your-microphone-usage

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/10/zoom_mac_microphone/){:target="_blank" rel="noopener"}

> Uh, why is the mic indicator light on all the time, netizens wonder Apple Mac users running the Zoom meetings app are reporting that it's seemingly keeping their computer's microphone on when they aren't using it.... [...]
