Title: Kronos Still Dragging Itself Back From Ransomware Hell
Date: 2022-02-03T23:08:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Cloud Security;Malware;Web Security
Slug: 2022-02-03-kronos-still-dragging-itself-back-from-ransomware-hell

[Source](https://threatpost.com/kronos-dragging-itself-back-ransomware-hell/178213/){:target="_blank" rel="noopener"}

> And customers including Tesla, PepsiCo and NYC transit workers are filing lawsuits over the “real pain in the rear end” of manual inputting, inaccurate wages & more. [...]
