Title: FBI warns of fake job postings used to steal money, personal info
Date: 2022-02-01T17:14:31-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-01-fbi-warns-of-fake-job-postings-used-to-steal-money-personal-info

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-fake-job-postings-used-to-steal-money-personal-info/){:target="_blank" rel="noopener"}

> Scammers are trying to steal job seekers' money and personal information through phishing campaigns using fake advertisements posted on recruitment platforms. [...]
