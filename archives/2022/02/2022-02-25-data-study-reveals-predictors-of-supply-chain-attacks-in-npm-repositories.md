Title: Data study reveals predictors of supply chain attacks in NPM repositories
Date: 2022-02-25T13:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-25-data-study-reveals-predictors-of-supply-chain-attacks-in-npm-repositories

[Source](https://portswigger.net/daily-swig/data-study-reveals-predictors-of-supply-chain-attacks-in-npm-repositories){:target="_blank" rel="noopener"}

> Researchers outline the six key indicators that an NPM package may be compromised [...]
