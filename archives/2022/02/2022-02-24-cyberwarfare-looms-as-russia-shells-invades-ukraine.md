Title: Cyberwarfare looms as Russia shells, invades Ukraine
Date: 2022-02-24T23:20:13+00:00
Author: Agam Shah
Category: The Register
Tags: 
Slug: 2022-02-24-cyberwarfare-looms-as-russia-shells-invades-ukraine

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/24/cyberwarfare_russia_ukraine/){:target="_blank" rel="noopener"}

> And Player Three 'Iran' has entered the game? Russia's invasion of Ukraine could fuel an escalation in cyberwarfare with the West, experts have warned.... [...]
