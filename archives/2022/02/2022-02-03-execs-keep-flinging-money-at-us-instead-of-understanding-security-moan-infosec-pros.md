Title: Execs keep flinging money at us instead of understanding security, moan infosec pros
Date: 2022-02-03T12:25:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-02-03-execs-keep-flinging-money-at-us-instead-of-understanding-security-moan-infosec-pros

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/03/c_suite_security_survey/){:target="_blank" rel="noopener"}

> Oh what a problem to have Fresh from years of complaining about underfunding and not having enough staff to deal with problems, infosec bods are now complaining that corporate execs merely firehose cash at them without getting their own hands dirty or engaging with the problem.... [...]
