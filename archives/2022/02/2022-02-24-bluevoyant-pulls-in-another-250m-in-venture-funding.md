Title: BlueVoyant pulls in another $250m in venture funding
Date: 2022-02-24T16:33:34+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-02-24-bluevoyant-pulls-in-another-250m-in-venture-funding

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/24/bluevoyant_funding/){:target="_blank" rel="noopener"}

> Company offers protection against internal and supply chain threats Cybersecurity firm BlueVoyant has taken $250m in a fresh funding round announced this week, pushing the company's valuation past the $1bn mark.... [...]
