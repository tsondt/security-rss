Title: Facebook exposes 'god mode' token that could siphon data
Date: 2022-02-12T00:28:46+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-02-12-facebook-exposes-god-mode-token-that-could-siphon-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/12/facebook_god_mode/){:target="_blank" rel="noopener"}

> Ban of Chrome extension by Brave reveals risk of potential API abuse at Meta Brave this week said it is blocking the installation of a popular Chrome extension called L.O.C. because it exposes users' Facebook data to potential theft.... [...]
