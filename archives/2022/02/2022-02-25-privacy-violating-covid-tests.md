Title: Privacy Violating COVID Tests
Date: 2022-02-25T12:15:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;COVID-19;DNA;medicine;privacy
Slug: 2022-02-25-privacy-violating-covid-tests

[Source](https://www.schneier.com/blog/archives/2022/02/privacy-violating-covid-tests.html){:target="_blank" rel="noopener"}

> A good lesson in reading the fine print : Cignpost Diagnostics, which trades as ExpressTest and offers £35 tests for holidaymakers, said it holds the right to analyse samples from seals to “learn more about human health” — and sell information on to third parties. Individuals are required to give informed consent for their sensitive medical data to be used ­ but customers’ consent for their DNA to be sold now as buried in Cignpost’s online documents. Of course, no one ever reads the fine print. [...]
