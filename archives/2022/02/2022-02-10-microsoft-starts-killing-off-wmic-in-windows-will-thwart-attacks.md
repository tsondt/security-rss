Title: Microsoft starts killing off WMIC in Windows, will thwart attacks
Date: 2022-02-10T15:44:01-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-02-10-microsoft-starts-killing-off-wmic-in-windows-will-thwart-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-starts-killing-off-wmic-in-windows-will-thwart-attacks/){:target="_blank" rel="noopener"}

> Microsoft is moving forward with removing the Windows Management Instrumentation Command-line (WMIC) tool, wmic.exe, starting with the latest Windows 11 preview builds in the Dev channel. [...]
