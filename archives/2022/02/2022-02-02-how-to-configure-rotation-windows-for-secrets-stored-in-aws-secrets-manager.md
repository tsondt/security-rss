Title: How to configure rotation windows for secrets stored in AWS Secrets Manager
Date: 2022-02-02T18:34:19+00:00
Author: Fatima Ahmed
Category: AWS Security
Tags: Announcements;Intermediate (200);Learning Levels;Security, Identity, & Compliance;AWS Secrets Manager;Cloud security;rotation;Security Blog
Slug: 2022-02-02-how-to-configure-rotation-windows-for-secrets-stored-in-aws-secrets-manager

[Source](https://aws.amazon.com/blogs/security/how-to-configure-rotation-windows-for-secrets-stored-in-aws-secrets-manager/){:target="_blank" rel="noopener"}

> AWS Secrets Manager now enables you to specify a rotation window for each secret stored. With this launch, you can continue to follow best practice of regularly rotating your secrets, while using the defined time window of your choice. With Secrets Manager, you can manage, retrieve, and rotate database credentials, API keys, and other secrets. [...]
