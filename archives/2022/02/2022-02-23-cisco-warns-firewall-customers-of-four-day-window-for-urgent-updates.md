Title: Cisco warns firewall customers of four-day window for urgent updates
Date: 2022-02-23T08:13:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-23-cisco-warns-firewall-customers-of-four-day-window-for-urgent-updates

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/23/cisco_firepower_rapid_update_required/){:target="_blank" rel="noopener"}

> Firewalls are supposed to update so they block new threats – miss this deadline and they might not Cisco has warned users of its Firepower firewalls – physical and virtual – that they may need to upgrade their kit within a four-day window or miss out on security intelligence updates.... [...]
