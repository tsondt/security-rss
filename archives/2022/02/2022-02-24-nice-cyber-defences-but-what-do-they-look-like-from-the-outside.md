Title: Nice cyber defences. But what do they look like from the outside?
Date: 2022-02-24T07:30:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-02-24-nice-cyber-defences-but-what-do-they-look-like-from-the-outside

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/24/better_cyber_defences/){:target="_blank" rel="noopener"}

> Join this webinar and learn just how exposed you really look Webinar You might think your array of hardened systems, controls and alerts is sufficient to ward off, or at least fatally entangle, any potential cyber-attacker.... [...]
