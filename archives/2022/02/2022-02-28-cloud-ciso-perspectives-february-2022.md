Title: Cloud CISO Perspectives: February 2022
Date: 2022-02-28T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-02-28-cloud-ciso-perspectives-february-2022

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-february-2022/){:target="_blank" rel="noopener"}

> As the war in Ukraine continues to unfold, I want to update you on how we’re supporting our customers and partners during this time. Google is taking a number of actions. Our security teams are actively monitoring developments, and we offer a host of security products and services designed to keep customers and partners safe from attacks. We have published security checklists for small businesses and medium-to-large enterprises, to enable entities to take necessary steps to promote resilience to malicious cyber activity. Below, I’ll recap the latest efforts from the Google Cybersecurity Action Team such as our second Threat Horizons [...]
