Title: Bunnie Huang’s Plausibly Deniable Database
Date: 2022-02-10T12:13:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cryptanalysis;databases;deniability;metadata;threat models
Slug: 2022-02-10-bunnie-huangs-plausibly-deniable-database

[Source](https://www.schneier.com/blog/archives/2022/02/bunnie-huangs-plausibly-deniable-database.html){:target="_blank" rel="noopener"}

> Bunnie Huang has created a Plausibly Deniable Database. Most security schemes facilitate the coercive processes of an attacker because they disclose metadata about the secret data, such as the name and size of encrypted files. This allows specific and enforceable demands to be made: “Give us the passwords for these three encrypted files with names A, B and C, or else...”. In other words, security often focuses on protecting the confidentiality of data, but lacks deniability. A scheme with deniability would make even the existence of secret files difficult to prove. This makes it difficult for an attacker to formulate [...]
