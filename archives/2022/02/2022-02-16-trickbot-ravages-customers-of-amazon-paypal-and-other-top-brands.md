Title: TrickBot Ravages Customers of Amazon, PayPal and Other Top Brands
Date: 2022-02-16T22:34:52+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-02-16-trickbot-ravages-customers-of-amazon-paypal-and-other-top-brands

[Source](https://threatpost.com/trickbot-amazon-paypal-top-brands/178483/){:target="_blank" rel="noopener"}

> The resurgent trojan has targeted 60 top companies to harvest credentials for a wide range of applications, with an eye to virulent follow-on attacks. [...]
