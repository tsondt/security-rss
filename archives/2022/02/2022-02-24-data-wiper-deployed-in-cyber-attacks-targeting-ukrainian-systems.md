Title: Data wiper deployed in cyber-attacks targeting Ukrainian systems
Date: 2022-02-24T15:25:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-24-data-wiper-deployed-in-cyber-attacks-targeting-ukrainian-systems

[Source](https://portswigger.net/daily-swig/data-wiper-deployed-in-cyber-attacks-targeting-ukrainian-systems){:target="_blank" rel="noopener"}

> Newly named ‘HermeticWiper’ malware discovered on hundreds of endpoints [...]
