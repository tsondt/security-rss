Title: Vulnerable Microsoft SQL Servers targeted with Cobalt Strike
Date: 2022-02-22T13:08:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-22-vulnerable-microsoft-sql-servers-targeted-with-cobalt-strike

[Source](https://www.bleepingcomputer.com/news/security/vulnerable-microsoft-sql-servers-targeted-with-cobalt-strike/){:target="_blank" rel="noopener"}

> Threat analysts have observed a new wave of attacks installing Cobalt Strike beacons on vulnerable Microsoft SQL Servers, leading to deeper infiltration and subsequent malware infections. [...]
