Title: Ransomware means your database IS the front line. How are you defending it?
Date: 2022-02-01T18:00:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-02-01-ransomware-means-your-database-is-the-front-line-how-are-you-defending-it

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/01/ransomware_database/){:target="_blank" rel="noopener"}

> We'll show you how to fight back – and get backup Webinar Databases are often thought of as, well, part of the back office of any organization. Yet just consider what would happen to a modern business if its data flow was choked off?... [...]
