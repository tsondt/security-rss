Title: FCC proposes $45 million fine for health insurance robocaller
Date: 2022-02-18T13:47:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-02-18-fcc-proposes-45-million-fine-for-health-insurance-robocaller

[Source](https://www.bleepingcomputer.com/news/security/fcc-proposes-45-million-fine-for-health-insurance-robocaller/){:target="_blank" rel="noopener"}

> The US Federal Communications Commission (FCC) today proposed the largest-ever fine against a robocaller for Telephone Consumer Protection Act violations. [...]
