Title: <span>AirTag clone bypassed Apple’s tracking-protection features, claims researcher</span>
Date: 2022-02-22T15:42:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-22-airtag-clone-bypassed-apples-tracking-protection-features-claims-researcher

[Source](https://portswigger.net/daily-swig/airtag-clone-bypassed-apples-tracking-protection-features-claims-researcher){:target="_blank" rel="noopener"}

> Third-party app allegedly outperforms Find My service by detecting the DIY device [...]
