Title: China's APT10 cyber-spies 'targeted Taiwanese financial firms'
Date: 2022-02-23T05:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-23-chinas-apt10-cyber-spies-targeted-taiwanese-financial-firms

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/23/apt10_operation_cache_panda_taiwan/){:target="_blank" rel="noopener"}

> Operation Cache Panda went after software used by majority of industry players China's state-sponsored snoops conducted a two-month campaign against Taiwanese financial services firms, according to CyCraft, a security consultancy from the island nation.... [...]
