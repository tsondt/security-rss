Title: Google sees 50% security boost for 150M users after 2FA enroll
Date: 2022-02-08T06:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2022-02-08-google-sees-50-security-boost-for-150m-users-after-2fa-enroll

[Source](https://www.bleepingcomputer.com/news/google/google-sees-50-percent-security-boost-for-150m-users-after-2fa-enroll/){:target="_blank" rel="noopener"}

> After accelerating its efforts to auto-enroll as many accounts as possible in two-factor authentication (2FA), Google announced that an additional 150 million users now have 2FA enabled. [...]
