Title: Lagging behind? New study highlights weaknesses in open source patch process
Date: 2022-02-18T12:02:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-02-18-lagging-behind-new-study-highlights-weaknesses-in-open-source-patch-process

[Source](https://portswigger.net/daily-swig/lagging-behind-new-study-highlights-weaknesses-in-open-source-patch-process){:target="_blank" rel="noopener"}

> Patch delays create a ‘window of opportunity’ for observant attackers [...]
