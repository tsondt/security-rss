Title: CIA illegally harvested US citizens' data, senators assert
Date: 2022-02-11T06:41:26+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-02-11-cia-illegally-harvested-us-citizens-data-senators-assert

[Source](https://go.theregister.com/feed/www.theregister.com/2022/02/11/cia_illegal_us_citizen_data/){:target="_blank" rel="noopener"}

> We're shocked, shocked to find unjustified collection and access is going on in here Two US senators have gone public with evidence of what they assert is a previously secret bulk data collection effort by the Central Intelligence Agency (CIA), conducted outside the law and without oversight.... [...]
