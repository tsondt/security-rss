Title: Ukraine recruits "IT Army" to hack Russian entities, lists 31 targets
Date: 2022-02-26T23:28:22-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-02-26-ukraine-recruits-it-army-to-hack-russian-entities-lists-31-targets

[Source](https://www.bleepingcomputer.com/news/security/ukraine-recruits-it-army-to-hack-russian-entities-lists-31-targets/){:target="_blank" rel="noopener"}

> Ukraine is recruiting a volunteer "IT army" of security researchers and hackers to conduct cyberattacks on thirty-one Russian entities, including government agencies, critical infrastructure, and banks. [...]
