Title: OpenSea users lose $2 million worth of NFTs in phishing attack
Date: 2022-02-21T08:12:39-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-02-21-opensea-users-lose-2-million-worth-of-nfts-in-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/opensea-users-lose-2-million-worth-of-nfts-in-phishing-attack/){:target="_blank" rel="noopener"}

> The non-fungible token (NFT) marketplace OpenSea is investigating a phishing attack that left 17 of its users without more than 250 NFTs worth around $2 million. [...]
