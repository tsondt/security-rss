Title: US govt warns Americans of escalating SMS phishing attacks
Date: 2022-07-29T11:21:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-29-us-govt-warns-americans-of-escalating-sms-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-govt-warns-americans-of-escalating-sms-phishing-attacks/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) warned Americans of an increasing wave of SMS (Short Message Service) phishing attacks attempting to steal their personal information and money. [...]
