Title: ‘Endemic’ Log4j bug set to persist in the wild for at least a decade, US government warns
Date: 2022-07-18T14:29:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-18-endemic-log4j-bug-set-to-persist-in-the-wild-for-at-least-a-decade-us-government-warns

[Source](https://portswigger.net/daily-swig/endemic-log4j-bug-set-to-persist-in-the-wild-for-at-least-a-decade-us-government-warns){:target="_blank" rel="noopener"}

> Inaugural report from cyber safety panel outlines strengths and weaknesses exposed by momentous security flaw [...]
