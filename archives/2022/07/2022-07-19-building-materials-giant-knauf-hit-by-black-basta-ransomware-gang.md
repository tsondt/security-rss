Title: Building materials giant Knauf hit by Black Basta ransomware gang
Date: 2022-07-19T16:58:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-19-building-materials-giant-knauf-hit-by-black-basta-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/building-materials-giant-knauf-hit-by-black-basta-ransomware-gang/){:target="_blank" rel="noopener"}

> The Knauf Group has announced it has been the target of a cyberattack that has disrupted its business operations, forcing its global IT team to shut down all IT systems to isolate the incident. [...]
