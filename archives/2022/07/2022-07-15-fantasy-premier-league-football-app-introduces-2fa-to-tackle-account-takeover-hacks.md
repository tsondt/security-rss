Title: Fantasy Premier League football app introduces 2FA to tackle account takeover hacks
Date: 2022-07-15T14:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-15-fantasy-premier-league-football-app-introduces-2fa-to-tackle-account-takeover-hacks

[Source](https://portswigger.net/daily-swig/fantasy-premier-league-football-app-introduces-2fa-to-tackle-account-takeover-hacks){:target="_blank" rel="noopener"}

> Authentication controls added to defend against account hijack threat [...]
