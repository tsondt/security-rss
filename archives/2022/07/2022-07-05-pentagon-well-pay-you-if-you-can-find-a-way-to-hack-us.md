Title: Pentagon: We'll pay you if you can find a way to hack us
Date: 2022-07-05T20:06:42+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-05-pentagon-well-pay-you-if-you-can-find-a-way-to-hack-us

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/05/dod-hackus-bug-bounty/){:target="_blank" rel="noopener"}

> DoD puts money behind bug bounty program after reward-free pilot The US Department of Defense has created a broad but short bug bounty program for reports of vulnerabilities in public-facing systems and applications.... [...]
