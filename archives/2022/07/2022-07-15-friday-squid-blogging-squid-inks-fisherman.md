Title: Friday Squid Blogging: Squid Inks Fisherman
Date: 2022-07-15T21:04:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2022-07-15-friday-squid-blogging-squid-inks-fisherman

[Source](https://www.schneier.com/blog/archives/2022/07/friday-squid-blogging-squid-inks-fisherman.html){:target="_blank" rel="noopener"}

> Short video. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
