Title: Microsoft updates Azure AD with support for temporary passcodes
Date: 2022-07-01T12:05:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-01-microsoft-updates-azure-ad-with-support-for-temporary-passcodes

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-updates-azure-ad-with-support-for-temporary-passcodes/){:target="_blank" rel="noopener"}

> Azure Active Directory (Azure AD) now allows admins to issue time-limited passcodes that can be used to register new passwordless authentication methods, during Windows onboarding, or to recover accounts easier when losing credentials or FIDO2 keys. [...]
