Title: Amazon squashes years-old authentication bugs in AWS Kubernetes service
Date: 2022-07-12T18:45:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-12-amazon-squashes-years-old-authentication-bugs-in-aws-kubernetes-service

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/12/authentication_bug_aws_kubernetes/){:target="_blank" rel="noopener"}

> Three vulnerabilities in one line of code AWS fixed three authentication bugs present in one line of code in its IAM Authenticator for Kubernetes, used by the cloud giant's popular managed Kubernetes service Amazon EKS, that could allow an attacker to escalate privileges within a Kubernetes cluster.... [...]
