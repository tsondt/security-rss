Title: Digital security giant Entrust breached by ransomware gang
Date: 2022-07-22T16:44:45-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-22-digital-security-giant-entrust-breached-by-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/digital-security-giant-entrust-breached-by-ransomware-gang/){:target="_blank" rel="noopener"}

> Digital security giant Entrust has confirmed that it suffered a cyberattack where threat actors breached their network and stole data from internal systems. [...]
