Title: As Microsoft blocks Office macros, hackers find new attack vectors
Date: 2022-07-28T05:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-28-as-microsoft-blocks-office-macros-hackers-find-new-attack-vectors

[Source](https://www.bleepingcomputer.com/news/security/as-microsoft-blocks-office-macros-hackers-find-new-attack-vectors/){:target="_blank" rel="noopener"}

> Hackers who normally distributed malware via phishing attachments with malicious macros gradually changed tactics after Microsoft Office began blocking them by default, switching to new file types such as ISO, RAR, and Windows Shortcut (LNK) attachments. [...]
