Title: ‘Does anybody like CAPTCHAs?’ – Cloudflare CTO John Graham-Cumming envisages a frictionless future for website Turing tests
Date: 2022-07-04T15:20:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-04-does-anybody-like-captchas-cloudflare-cto-john-graham-cumming-envisages-a-frictionless-future-for-website-turing-tests

[Source](https://portswigger.net/daily-swig/does-anybody-like-captchas-cloudflare-cto-john-graham-cumming-envisages-a-frictionless-future-for-website-turing-tests){:target="_blank" rel="noopener"}

> British software engineer also talks HTTP/3, zero trust, and lava lamp-powered cryptography [...]
