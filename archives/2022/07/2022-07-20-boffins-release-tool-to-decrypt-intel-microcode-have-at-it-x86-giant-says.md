Title: Boffins release tool to decrypt Intel microcode. Have at it, x86 giant says
Date: 2022-07-20T19:59:12+00:00
Author: Dylan Martin
Category: The Register
Tags: 
Slug: 2022-07-20-boffins-release-tool-to-decrypt-intel-microcode-have-at-it-x86-giant-says

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/20/intel-cpu-microcode/){:target="_blank" rel="noopener"}

> Peek behind the curtain to see SGX implemented, Spectre mitigated, and more Infosec boffins have released a tool to decrypt and unpack the microcode for a class of low-power Intel CPUs, opening up a way to look at how the chipmaker has implemented various security fixes and features as well as things like virtualization.... [...]
