Title: Free decryptor released for AstraLocker, Yashma ransomware victims
Date: 2022-07-08T05:47:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-08-free-decryptor-released-for-astralocker-yashma-ransomware-victims

[Source](https://www.bleepingcomputer.com/news/security/free-decryptor-released-for-astralocker-yashma-ransomware-victims/){:target="_blank" rel="noopener"}

> New Zealand-based cybersecurity firm Emsisoft has released a free decryption tool to help AstraLocker and Yashma ransomware victims recover their files without paying a ransom. [...]
