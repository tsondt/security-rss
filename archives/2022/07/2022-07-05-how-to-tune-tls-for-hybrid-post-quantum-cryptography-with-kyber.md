Title: How to tune TLS for hybrid post-quantum cryptography with Kyber
Date: 2022-07-05T17:07:22+00:00
Author: Brian Jarvis
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;ACM;AWS Certificate Manager;AWS Key Management Service;AWS KMS;cryptography;Encryption;NIST;post-quantum cryptography;s2n;s2n-tls;Security Blog;TLS;Transport Layer Security
Slug: 2022-07-05-how-to-tune-tls-for-hybrid-post-quantum-cryptography-with-kyber

[Source](https://aws.amazon.com/blogs/security/how-to-tune-tls-for-hybrid-post-quantum-cryptography-with-kyber/){:target="_blank" rel="noopener"}

> We are excited to offer hybrid post-quantum TLS with Kyber for AWS Key Management Service (AWS KMS) and AWS Certificate Manager (ACM). In this blog post, we share the performance characteristics of our hybrid post-quantum Kyber implementation, show you how to configure a Maven project to use it, and discuss how to prepare your connection [...]
