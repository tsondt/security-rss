Title: SMEs slow to adopt MFA – study
Date: 2022-07-07T14:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-07-smes-slow-to-adopt-mfa-study

[Source](https://portswigger.net/daily-swig/smes-slow-to-adopt-mfa-study){:target="_blank" rel="noopener"}

> Authentication shortcomings leave sensitive data at risk [...]
