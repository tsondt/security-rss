Title: The Matrix messaging network now counts more than 60 million users
Date: 2022-07-17T10:12:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2022-07-17-the-matrix-messaging-network-now-counts-more-than-60-million-users

[Source](https://www.bleepingcomputer.com/news/security/the-matrix-messaging-network-now-counts-more-than-60-million-users/){:target="_blank" rel="noopener"}

> The Matrix open network for decentralized communication has announced a record growth of 79% in the past 12 months, now counting more than 60 million users. [...]
