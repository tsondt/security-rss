Title: Atlassian reveals critical flaws in almost everything it makes and touches
Date: 2022-07-21T01:54:25+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-21-atlassian-reveals-critical-flaws-in-almost-everything-it-makes-and-touches

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/21/atlassian_critical_security_advisories/){:target="_blank" rel="noopener"}

> Fixes issued, warns it 'has not exhaustively enumerated all potential consequences' Atlassian has warned users of its Bamboo, Bitbucket, Confluence, Fisheye, Crucible, and Jira products that a pair of critical-rated flaws threaten their security.... [...]
