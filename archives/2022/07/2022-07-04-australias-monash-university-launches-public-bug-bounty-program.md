Title: Australia’s Monash University launches public bug bounty program
Date: 2022-07-04T14:28:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-04-australias-monash-university-launches-public-bug-bounty-program

[Source](https://portswigger.net/daily-swig/australias-monash-university-launches-public-bug-bounty-program){:target="_blank" rel="noopener"}

> Education institution will pay up to $2,500 for valid vulnerabilities [...]
