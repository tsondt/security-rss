Title: Cyber-mercenaries for hire represent shifting criminal business model
Date: 2022-07-25T17:00:55+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-25-cyber-mercenaries-for-hire-represent-shifting-criminal-business-model

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/25/aig-unique-cybercrime-business/){:target="_blank" rel="noopener"}

> Emerging threat group offers a broad range of attack services An emerging and fast-growing threat group is using a unique business model to offer cybercriminals a broad range of services that span from leaked databases and distributed denial-of-service (DDoS) attacks to hacking scripts and, in the future, potentially ransomware.... [...]
