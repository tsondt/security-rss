Title: Ransomware gang now lets you search their stolen data
Date: 2022-07-11T15:24:40-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-07-11-ransomware-gang-now-lets-you-search-their-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-now-lets-you-search-their-stolen-data/){:target="_blank" rel="noopener"}

> Two ransomware gangs and a data extortion group have adopted a new strategy to force victim companies to pay threat actors to not leak stolen data. [...]
