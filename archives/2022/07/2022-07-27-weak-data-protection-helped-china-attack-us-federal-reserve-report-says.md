Title: Weak data protection helped China attack US Federal Reserve, report says
Date: 2022-07-27T10:31:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-27-weak-data-protection-helped-china-attack-us-federal-reserve-report-says

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/27/weak_data_protection_helped_chinese/){:target="_blank" rel="noopener"}

> Details of adversarial tradecraft detailed, includes many email accounts China's cyber espionage activities are extensive and sophisticated but when the Middle Kingdom tried to steal sensitive economic data from the US Fed, poor security meant its operatives didn't have to dip too far into their bags of tricks.... [...]
