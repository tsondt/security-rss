Title: NPM supply-chain attack impacts hundreds of websites and apps
Date: 2022-07-05T13:55:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-05-npm-supply-chain-attack-impacts-hundreds-of-websites-and-apps

[Source](https://www.bleepingcomputer.com/news/security/npm-supply-chain-attack-impacts-hundreds-of-websites-and-apps/){:target="_blank" rel="noopener"}

> An NPM supply-chain attack dating back to December 2021 used dozens of malicious NPM modules containing obfuscated Javascript code to compromise hundreds of downstream desktop apps and websites. [...]
