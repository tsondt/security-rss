Title: Lenovo issues firmware updates after UEFI vulnerabilities disclosed
Date: 2022-07-14T16:15:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-14-lenovo-issues-firmware-updates-after-uefi-vulnerabilities-disclosed

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/14/lenovo_uefi_vuln/){:target="_blank" rel="noopener"}

> Déjà vu all over again for laptop maker as researchers poke holes in its code Security researchers have spotted fresh flaws in Lenovo laptops just months after the vendor patched a bunch of its products.... [...]
