Title: Securing Open-Source Software
Date: 2022-07-27T12:03:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;infrastructure;open source
Slug: 2022-07-27-securing-open-source-software

[Source](https://www.schneier.com/blog/archives/2022/07/securing-open-source-software.html){:target="_blank" rel="noopener"}

> Good essay arguing that open-source software is a critical national-security asset and needs to be treated as such: Open source is at least as important to the economy, public services, and national security as proprietary code, but it lacks the same standards and safeguards. It bears the qualities of a public good and is as indispensable as national highways. Given open source’s value as a public asset, an institutional structure must be built that sustains and secures it. This is not a novel idea. Open-source code has been called the “roads and bridges” of the current digital infrastructure that warrants [...]
