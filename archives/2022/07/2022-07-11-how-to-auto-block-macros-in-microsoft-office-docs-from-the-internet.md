Title: How to auto block macros in Microsoft Office docs from the internet
Date: 2022-07-11T09:34:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-11-how-to-auto-block-macros-in-microsoft-office-docs-from-the-internet

[Source](https://www.bleepingcomputer.com/news/microsoft/how-to-auto-block-macros-in-microsoft-office-docs-from-the-internet/){:target="_blank" rel="noopener"}

> With Microsoft temporarily rolling back a feature that automatically blocks macros in Microsoft Office files downloaded from the Internet, it is essential to learn how to configure this security setting manually. This article will explain why users should block macros and how you can block them in Microsoft Office. [...]
