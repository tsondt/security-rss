Title: Hacker claims to have stolen data on 1 billion Chinese citizens
Date: 2022-07-04T11:29:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-04-hacker-claims-to-have-stolen-data-on-1-billion-chinese-citizens

[Source](https://www.bleepingcomputer.com/news/security/hacker-claims-to-have-stolen-data-on-1-billion-chinese-citizens/){:target="_blank" rel="noopener"}

> An anonymous threat actor is selling several databases they claim to contain more than 22 terabytes of stolen information on roughly 1 billion Chinese citizens for 10 bitcoins (approximately $195,000). [...]
