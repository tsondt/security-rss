Title: Microsoft rolls back decision to block Office macros by default
Date: 2022-07-07T18:33:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-07-microsoft-rolls-back-decision-to-block-office-macros-by-default

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-rolls-back-decision-to-block-office-macros-by-default/){:target="_blank" rel="noopener"}

> While Microsoft announced earlier this year that it would block VBA macros on downloaded documents by default, Redmond said on Thursday that it will roll back this change based on "feedback" until further notice. [...]
