Title: Facebook Is Now Encrypting Links to Prevent URL Stripping
Date: 2022-07-18T14:49:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;browsers;encryption;Facebook;tracking
Slug: 2022-07-18-facebook-is-now-encrypting-links-to-prevent-url-stripping

[Source](https://www.schneier.com/blog/archives/2022/07/facebook-is-now-encrypting-links-to-prevent-url-stripping.html){:target="_blank" rel="noopener"}

> Some sites, including Facebook, add parameters to the web address for tracking purposes. These parameters have no functionality that is relevant to the user, but sites rely on them to track users across pages and properties. Mozilla introduced support for URL stripping in Firefox 102, which it launched in June 2022. Firefox removes tracking parameters from web addresses automatically, but only in private browsing mode or when the browser’s Tracking Protection feature is set to strict. Firefox users may enable URL stripping in all Firefox modes, but this requires manual configuration. Brave Browser strips known tracking parameters from web addresses [...]
