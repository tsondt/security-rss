Title: Atlassian fixes critical Confluence hardcoded credentials flaw
Date: 2022-07-20T14:59:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-20-atlassian-fixes-critical-confluence-hardcoded-credentials-flaw

[Source](https://www.bleepingcomputer.com/news/security/atlassian-fixes-critical-confluence-hardcoded-credentials-flaw/){:target="_blank" rel="noopener"}

> Atlassian has patched a critical hardcoded credentials vulnerability in Confluence Server and Data Center that could let remote, unauthenticated attackers log into vulnerable, unpatched servers. [...]
