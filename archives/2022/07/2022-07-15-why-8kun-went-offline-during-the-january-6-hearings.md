Title: Why 8kun Went Offline During the January 6 Hearings
Date: 2022-07-15T19:43:05+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;8kun;January 6 hearings;Psychz Networks;VanwaTech
Slug: 2022-07-15-why-8kun-went-offline-during-the-january-6-hearings

[Source](https://krebsonsecurity.com/2022/07/why-8kun-went-offline-during-the-january-6-hearings/){:target="_blank" rel="noopener"}

> The latest Jan. 6 committee hearing on Tuesday examined the role of conspiracy theory communities like 8kun[.]top and TheDonald[.]win in helping to organize and galvanize supporters who responded to former President Trump’s invitation to “be wild” in Washington, D.C. on that chaotic day. At the same time the committee was hearing video testimony from 8kun founder Jim Watkins, 8kun and a slew of similar websites were suddenly yanked offline. Watkins suggested the outage was somehow related to the work of the committee, but the truth is KrebsOnSecurity was responsible and the timing was pure coincidence. In a follow-up video address [...]
