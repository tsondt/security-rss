Title: Google Calendar provides new way to block invitation phishing
Date: 2022-07-20T12:24:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-07-20-google-calendar-provides-new-way-to-block-invitation-phishing

[Source](https://www.bleepingcomputer.com/news/security/google-calendar-provides-new-way-to-block-invitation-phishing/){:target="_blank" rel="noopener"}

> The Google Workspace team announced today that it started rolling out a new method to block Google Calendar invitation spam, available to all customers, including legacy G Suite Basic and Business users. [...]
