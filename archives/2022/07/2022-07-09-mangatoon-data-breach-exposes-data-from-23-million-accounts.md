Title: Mangatoon data breach exposes data from 23 million accounts
Date: 2022-07-09T11:12:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-09-mangatoon-data-breach-exposes-data-from-23-million-accounts

[Source](https://www.bleepingcomputer.com/news/security/mangatoon-data-breach-exposes-data-from-23-million-accounts/){:target="_blank" rel="noopener"}

> Manga comic reading app Mangatoon has suffered a data breach that exposed the account information of 23 million users after a hacker stole it from an Elasticsearch database. [...]
