Title: Here today, gone to Maui: That's your data captured by North Korean ransomware
Date: 2022-07-06T22:51:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-07-06-here-today-gone-to-maui-thats-your-data-captured-by-north-korean-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/06/here_today_gone_to_maui/){:target="_blank" rel="noopener"}

> CISA, FBI, US Treasury warn Kim Jong-un's latest malware has hit healthcare orgs For the past year, state-sponsored hackers operating on behalf of North Korea have been using ransomware called Maui to attack healthcare organizations, US cybersecurity authorities said on Wednesday.... [...]
