Title: New Retbleed speculative execution CPU attack bypasses Retpoline fixes
Date: 2022-07-14T03:13:23-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-07-14-new-retbleed-speculative-execution-cpu-attack-bypasses-retpoline-fixes

[Source](https://www.bleepingcomputer.com/news/security/new-retbleed-speculative-execution-cpu-attack-bypasses-retpoline-fixes/){:target="_blank" rel="noopener"}

> Security researchers have discovered a new speculative execution attack called Retbleed that affects processors from both Intel and AMD and could be used to extract sensitive information. [...]
