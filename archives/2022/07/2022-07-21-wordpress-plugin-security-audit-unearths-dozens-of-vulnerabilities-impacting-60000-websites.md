Title: WordPress plugin security audit unearths dozens of vulnerabilities impacting 60,000 websites
Date: 2022-07-21T13:33:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-21-wordpress-plugin-security-audit-unearths-dozens-of-vulnerabilities-impacting-60000-websites

[Source](https://portswigger.net/daily-swig/wordpress-plugin-security-audit-unearths-dozens-of-vulnerabilities-impacting-60-000-websites){:target="_blank" rel="noopener"}

> Unauthenticated SQL injection bugs put thousands of WordPress sites under threat [...]
