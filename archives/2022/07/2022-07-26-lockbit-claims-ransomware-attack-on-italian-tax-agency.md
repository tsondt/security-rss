Title: LockBit claims ransomware attack on Italian tax agency
Date: 2022-07-26T07:17:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-26-lockbit-claims-ransomware-attack-on-italian-tax-agency

[Source](https://www.bleepingcomputer.com/news/security/lockbit-claims-ransomware-attack-on-italian-tax-agency/){:target="_blank" rel="noopener"}

> Italian authorities are investigating claims made by the LockBit ransomware gang that they breached the network of the Italian Internal Revenue Service (L'Agenzia delle Entrate). [...]
