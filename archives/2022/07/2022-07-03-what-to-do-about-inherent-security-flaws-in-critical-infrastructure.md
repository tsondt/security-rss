Title: What to do about inherent security flaws in critical infrastructure?
Date: 2022-07-03T11:17:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-03-what-to-do-about-inherent-security-flaws-in-critical-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/03/inherent_security_flaws_ics/){:target="_blank" rel="noopener"}

> Industrial systems' security got 99 problems and CVEs are one. Or more The latest threat security research into operational technology (OT) and industrial systems identified a bunch of issues — 56 to be exact — that criminals could use to launch cyberattacks against critical infrastructure.... [...]
