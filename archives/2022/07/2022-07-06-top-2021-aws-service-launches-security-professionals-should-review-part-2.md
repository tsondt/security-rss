Title: Top 2021 AWS service launches security professionals should review – Part 2
Date: 2022-07-06T17:04:02+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Security, Identity, & Compliance;2021 recap;ACM Private CA;Amazon Cognito;Amazon DynamoDB;Amazon EBS;Amazon Elastic Compute Cloud (Amazon EC2);Amazon S3;Amazon Virtual Private Cloud (Amazon VPC);AWS Backup;AWS CloudFormation Guard 2.0;AWS CloudTrail;AWS Control Tower;AWS Directory Service;AWS Identity;AWS Identity and Access Management (IAM);AWS Network Firewall;AWS Organizations;AWS security;AWS Shield Advanced;AWS Single Sign-On (SSO);AWS Systems Manager Incident Manager;AWS WAF;Elastic Load Balancing;IAM Access Analyzer;Resource Access Manager (RAM);Security Blog;Service Launches
Slug: 2022-07-06-top-2021-aws-service-launches-security-professionals-should-review-part-2

[Source](https://aws.amazon.com/blogs/security/top-2021-aws-service-launches-security-professionals-should-review-part-2/){:target="_blank" rel="noopener"}

> In Part 1 of this two-part series, we shared an overview of some of the most important 2021 Amazon Web Services (AWS) Security service and feature launches. In this follow-up, we’ll dive deep into additional launches that are important for security professionals to be aware of and understand across all AWS services. There have already [...]
