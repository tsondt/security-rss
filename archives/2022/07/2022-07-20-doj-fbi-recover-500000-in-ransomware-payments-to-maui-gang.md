Title: DoJ, FBI recover $500,000 in ransomware payments to Maui gang
Date: 2022-07-20T15:45:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-20-doj-fbi-recover-500000-in-ransomware-payments-to-maui-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/20/doj-maui-ransomware-payments/){:target="_blank" rel="noopener"}

> Money paid by healthcare facilities to North Korean group traced through blockchain and Chinese launderers Federal law enforcement officials this week said they seized about $500,000 that healthcare facilities in the United States paid to the Maui ransomware group.... [...]
