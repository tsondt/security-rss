Title: AWS achieves TISAX certification (Information with Very High Protection Needs (AL3)
Date: 2022-07-15T20:31:41+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;Auditing;Automobile;AWS security;Compliance;Europe;Security;Security Blog;TISAX
Slug: 2022-07-15-aws-achieves-tisax-certification-information-with-very-high-protection-needs-al3

[Source](https://aws.amazon.com/blogs/security/aws-achieves-tisax-certification-information-with-very-high-protection-needs-al3/){:target="_blank" rel="noopener"}

> We’re excited to announce the completion of the Trusted Information Security Assessment Exchange (TISAX) certification on June 30, 2022 for 19 AWS Regions. These Regions achieved the Information with Very High Protection Needs (AL3) label for the control domains Information Handling and Data Protection. This alignment with TISAX requirements demonstrates our continued commitment to adhere [...]
