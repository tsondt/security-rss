Title: Source code for Rust-based info-stealer released on hacker forums
Date: 2022-07-25T14:30:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-25-source-code-for-rust-based-info-stealer-released-on-hacker-forums

[Source](https://www.bleepingcomputer.com/news/security/source-code-for-rust-based-info-stealer-released-on-hacker-forums/){:target="_blank" rel="noopener"}

> A malware author released the source code of their info-stealer for free on hacking forums earlier this month, and security analysts already report observing several samples being deployed in the wild. [...]
