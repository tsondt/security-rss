Title: Hackers pose as journalists to breach news media org’s networks
Date: 2022-07-16T11:07:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-16-hackers-pose-as-journalists-to-breach-news-media-orgs-networks

[Source](https://www.bleepingcomputer.com/news/security/hackers-pose-as-journalists-to-breach-news-media-org-s-networks/){:target="_blank" rel="noopener"}

> Researchers following the activities of advanced persistent (APT) threat groups originating from China, North Korea, Iran, and Turkey say that journalists and media organizations have remained a constant target for state-aligned actors. [...]
