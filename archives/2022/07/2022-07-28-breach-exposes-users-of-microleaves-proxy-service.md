Title: Breach Exposes Users of Microleaves Proxy Service
Date: 2022-07-28T18:52:28+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;Abhishek Gupta;Acidut;Alexandru Florea;Alexandru Iulian Florea;BlackHatWorld;Constella Intelligence;CPAElites;Hackforums;Intel 471;Microleaves;nevo.julian;online.io;residential proxy;reverseproxies;Shifter.io;SOCKS proxy
Slug: 2022-07-28-breach-exposes-users-of-microleaves-proxy-service

[Source](https://krebsonsecurity.com/2022/07/breach-exposes-users-of-microleaves-proxy-service/){:target="_blank" rel="noopener"}

> Microleaves, a ten-year-old proxy service that lets customers route their web traffic through millions of Microsoft Windows computers, recently fixed a vulnerability in their website that exposed their entire user database. Microleaves claims its proxy software is installed with user consent, but data exposed in the breach shows the service has a lengthy history of being supplied with new proxies by affiliates incentivized to distribute the software any which way they can — such as by secretly bundling it with other titles. The Microleaves proxy service, which is in the process of being rebranded to Shifter[.[io. Launched in 2013, Microleaves [...]
