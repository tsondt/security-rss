Title: Why Physical Security Maintenance Should Never Be an Afterthought
Date: 2022-07-25T11:00:04+00:00
Author: Infosec Contributor
Category: Threatpost
Tags: InfoSec Insider
Slug: 2022-07-25-why-physical-security-maintenance-should-never-be-an-afterthought

[Source](https://threatpost.com/physical-security-maintenance/180269/){:target="_blank" rel="noopener"}

> SecuriThings' CEO Roy Dagan tackles the sometimes overlooked security step of physical security maintenance and breaks down why it is important. [...]
