Title: Suspected radiation alert saboteurs cuffed by cops after sensors disabled
Date: 2022-07-28T19:19:16+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-28-suspected-radiation-alert-saboteurs-cuffed-by-cops-after-sensors-disabled

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/28/spain_radiation_alert/){:target="_blank" rel="noopener"}

> You might say the police were in their element Spain's national police say they have arrested two former government workers suspected of breaking into the computer network of the country's radioactivity alert system (RAR) and disabling more than a third of its sensors.... [...]
