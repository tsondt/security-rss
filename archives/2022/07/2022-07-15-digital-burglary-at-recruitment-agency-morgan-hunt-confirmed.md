Title: Digital burglary at recruitment agency Morgan Hunt confirmed
Date: 2022-07-15T07:30:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-07-15-digital-burglary-at-recruitment-agency-morgan-hunt-confirmed

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/15/digital_burglary_at_recruitment_agency/){:target="_blank" rel="noopener"}

> Third-party software developer blamed for 'improperly storing credentials to our database' The bad news keeps on rolling for British recruitment agency Morgan Hunt amid confirmation it suffered a digital burglary, with intruders making off with the personal data for some of the freelancers on its books.... [...]
