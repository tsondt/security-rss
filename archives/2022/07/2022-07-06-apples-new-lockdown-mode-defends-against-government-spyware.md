Title: Apple’s new Lockdown Mode defends against government spyware
Date: 2022-07-06T14:38:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2022-07-06-apples-new-lockdown-mode-defends-against-government-spyware

[Source](https://www.bleepingcomputer.com/news/apple/apple-s-new-lockdown-mode-defends-against-government-spyware/){:target="_blank" rel="noopener"}

> Apple announced that a new security feature known as Lockdown Mode will roll out with iOS 16, iPadOS 16, and macOS Ventura to protect high-risk individuals like human rights defenders, journalists, and dissidents against targeted spyware attacks. [...]
