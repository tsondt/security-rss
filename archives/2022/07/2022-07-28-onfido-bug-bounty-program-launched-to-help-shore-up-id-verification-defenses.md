Title: Onfido bug bounty program launched to help shore up ID verification defenses
Date: 2022-07-28T13:19:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-28-onfido-bug-bounty-program-launched-to-help-shore-up-id-verification-defenses

[Source](https://portswigger.net/daily-swig/onfido-bug-bounty-program-launched-to-help-shore-up-id-verification-defenses){:target="_blank" rel="noopener"}

> Initiative adds another layer of protection for end-to-end identity verification platform [...]
