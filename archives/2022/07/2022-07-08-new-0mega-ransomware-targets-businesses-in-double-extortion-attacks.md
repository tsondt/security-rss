Title: New 0mega ransomware targets businesses in double-extortion attacks
Date: 2022-07-08T15:22:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-08-new-0mega-ransomware-targets-businesses-in-double-extortion-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-0mega-ransomware-targets-businesses-in-double-extortion-attacks/){:target="_blank" rel="noopener"}

> A new ransomware operation named '0mega' targets organizations worldwide in double-extortion attacks and demands millions of dollars in ransoms. [...]
