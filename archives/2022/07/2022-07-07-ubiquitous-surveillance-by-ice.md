Title: Ubiquitous Surveillance by ICE
Date: 2022-07-07T18:18:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;privacy;reports;surveillance
Slug: 2022-07-07-ubiquitous-surveillance-by-ice

[Source](https://www.schneier.com/blog/archives/2022/07/ubiquitous-surveillance-by-ice.html){:target="_blank" rel="noopener"}

> Report by Georgetown’s Center on Privacy and Technology published a comprehensive report on the surprising amount of mass surveillance conducted by Immigration and Customs Enforcement (ICE). Our two-year investigation, including hundreds of Freedom of Information Act requests and a comprehensive review of ICE’s contracting and procurement records, reveals that ICE now operates as a domestic surveillance agency. Since its founding in 2003, ICE has not only been building its own capacity to use surveillance to carry out deportations but has also played a key role in the federal government’s larger push to amass as much information as possible about all [...]
