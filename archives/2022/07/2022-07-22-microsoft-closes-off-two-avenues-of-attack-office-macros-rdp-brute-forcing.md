Title: Microsoft closes off two avenues of attack: Office macros, RDP brute-forcing
Date: 2022-07-22T21:44:22+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-22-microsoft-closes-off-two-avenues-of-attack-office-macros-rdp-brute-forcing

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/22/microsoft-windows-vba-macros/){:target="_blank" rel="noopener"}

> Blockade against VBA scripts in downloaded files is back on by default Microsoft is trying to shut the door on a couple of routes cybercriminals have used to attack users and networks.... [...]
