Title: Outlook email users alerted to suspicious activity from Microsoft-owned IP address
Date: 2022-07-21T10:27:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-21-outlook-email-users-alerted-to-suspicious-activity-from-microsoft-owned-ip-address

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/21/outlook_sign_ins/){:target="_blank" rel="noopener"}

> People turn amateur sleuths to discover that the source of all those sign-ins seems to be in Redmond Strange things are afoot in the world of Microsoft email with multiple users reporting unusual sign-in notifications for their Outlook accounts.... [...]
