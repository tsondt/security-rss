Title: Google supports CSRB call for open source security improvements in wake of log4j report
Date: 2022-07-22T07:30:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Identity & Security
Slug: 2022-07-22-google-supports-csrb-call-for-open-source-security-improvements-in-wake-of-log4j-report

[Source](https://cloud.google.com/blog/products/identity-security/google-supports-csrb-call-improve-open-source-security/){:target="_blank" rel="noopener"}

> The U.S. Department of Homeland Security (DHS) recently announced the results of the first report from the Cyber Safety Review Board (CSRB) on the log4j software library vulnerabilities discovered in late 2021. Google welcomed the opportunity to participate in the development of the CSRB report and share our own experiences responding to this and other incidents. Building on this momentum, today we are going to share Google’s approach to address the log4j report’s recommendations. We see this as an important part of our effort to support others in the industry as we all work together to increase open source security. [...]
