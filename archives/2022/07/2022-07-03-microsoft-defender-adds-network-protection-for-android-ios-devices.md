Title: Microsoft Defender adds network protection for Android, iOS devices
Date: 2022-07-03T10:09:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-03-microsoft-defender-adds-network-protection-for-android-ios-devices

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-adds-network-protection-for-android-ios-devices/){:target="_blank" rel="noopener"}

> Microsoft has announced the introduction of a new Microsoft Defender for Endpoint (MDE) feature in public preview to help organizations detect weaknesses affecting Android and iOS devices in their enterprise networks. [...]
