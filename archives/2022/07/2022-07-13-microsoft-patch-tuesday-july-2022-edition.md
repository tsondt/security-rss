Title: Microsoft Patch Tuesday, July 2022 Edition
Date: 2022-07-13T01:02:49+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;CVE-2022-22022;CVE-2022-22029;CVE-2022-22038;CVE-2022-22039;CVE-2022-22041;CVE-2022-22047;CVE-2022-30206;CVE-2022-30221;CVE-2022-30226;Dan Goodin;Greg Wiseman;Immersive Labs;Kevin Breen;Microsoft Patch Tuesday July 2022;Rapid7;Sergiu Gatlan;Tenable
Slug: 2022-07-13-microsoft-patch-tuesday-july-2022-edition

[Source](https://krebsonsecurity.com/2022/07/microsoft-patch-tuesday-july-2022-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to fix at least 86 security vulnerabilities in its Windows operating systems and other software, including a weakness in all supported versions of Windows that Microsoft warns is actively being exploited. The software giant also has made a controversial decision to put the brakes on a plan to block macros in Office documents downloaded from the Internet. In February, security experts hailed Microsoft’s decision to block VBA macros in all documents downloaded from the Internet. The company said it would roll out the changes in stages between April and June 2022. Macros have long been a [...]
