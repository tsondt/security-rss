Title: Facebook ads push Android adware with 7 million installs on Google Play
Date: 2022-07-30T11:14:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-30-facebook-ads-push-android-adware-with-7-million-installs-on-google-play

[Source](https://www.bleepingcomputer.com/news/security/facebook-ads-push-android-adware-with-7-million-installs-on-google-play/){:target="_blank" rel="noopener"}

> Several adware apps promoted aggressively on Facebook as system cleaners and optimizers for Android devices are counting millions of installations on Google Play store. [...]
