Title: QNAP warns of new Checkmate ransomware targeting NAS devices
Date: 2022-07-07T11:47:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-07-qnap-warns-of-new-checkmate-ransomware-targeting-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-of-new-checkmate-ransomware-targeting-nas-devices/){:target="_blank" rel="noopener"}

> Taiwan-based network-attached storage (NAS) vendor QNAP warned customers to secure their devices against attacks using Checkmate ransomware to encrypt data. [...]
