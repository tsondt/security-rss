Title: Hackers steal 50,000 credit cards from 300 U.S. restaurants
Date: 2022-07-19T10:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-19-hackers-steal-50000-credit-cards-from-300-us-restaurants

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-50-000-credit-cards-from-300-us-restaurants/){:target="_blank" rel="noopener"}

> Payment card details from customers of more than 300 restaurants have been stolen in two web-skimming campaigns targeting three online ordering platforms. [...]
