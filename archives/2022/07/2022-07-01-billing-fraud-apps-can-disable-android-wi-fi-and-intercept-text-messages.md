Title: Billing fraud apps can disable Android Wi-Fi and intercept text messages
Date: 2022-07-01T20:47:34+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;android;Joker;malware;microsoft
Slug: 2022-07-01-billing-fraud-apps-can-disable-android-wi-fi-and-intercept-text-messages

[Source](https://arstechnica.com/?p=1864065){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson ) Android malware developers are stepping up their billing fraud game with apps that disable Wi-Fi connections, surreptitiously subscribe users to pricey wireless services, and intercept text messages, all in a bid to collect hefty fees from unsuspecting users, Microsoft said on Friday. This threat class has been a fact of life on the Android platform for years, as exemplified by a family of malware known as Joker, which has infected millions of phones since 2016. Despite awareness of the problem, little attention has been paid to the techniques that such "toll fraud" malware uses. Enter [...]
