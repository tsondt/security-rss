Title: Ransom payments fall as fewer victims choose to pay hackers
Date: 2022-07-28T17:35:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-28-ransom-payments-fall-as-fewer-victims-choose-to-pay-hackers

[Source](https://www.bleepingcomputer.com/news/security/ransom-payments-fall-as-fewer-victims-choose-to-pay-hackers/){:target="_blank" rel="noopener"}

> Ransomware statistics from the second quarter of the year show that the ransoms paid to extortionists have dropped in value, a trend that continues since the last quarter of 2021. [...]
