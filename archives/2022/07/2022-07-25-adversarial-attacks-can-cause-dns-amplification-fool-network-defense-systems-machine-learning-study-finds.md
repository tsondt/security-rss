Title: Adversarial attacks can cause DNS amplification, fool network defense systems, machine learning study finds
Date: 2022-07-25T11:33:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-25-adversarial-attacks-can-cause-dns-amplification-fool-network-defense-systems-machine-learning-study-finds

[Source](https://portswigger.net/daily-swig/adversarial-attacks-can-cause-dns-amplification-fool-network-defense-systems-machine-learning-study-finds){:target="_blank" rel="noopener"}

> New research shows how deep learning models trained for network intrusion detection can be bypassed [...]
