Title: Microsoft: Phishing bypassed MFA in attacks against 10,000 orgs
Date: 2022-07-12T13:02:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-07-12-microsoft-phishing-bypassed-mfa-in-attacks-against-10000-orgs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-phishing-bypassed-mfa-in-attacks-against-10-000-orgs/){:target="_blank" rel="noopener"}

> Microsoft says a massive series of phishing attacks has targeted more than 10,000 organizations starting with September 2021, using the gained access to victims' mailboxes in follow-on business email compromise (BEC) attacks. [...]
