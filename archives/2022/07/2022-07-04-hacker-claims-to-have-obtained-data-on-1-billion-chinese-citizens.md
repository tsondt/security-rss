Title: Hacker claims to have obtained data on 1 billion Chinese citizens
Date: 2022-07-04T13:42:27+00:00
Author: Vincent Ni and agency
Category: The Guardian
Tags: Hacking;Data and computer security;Data protection;Technology;China;Asia Pacific;World news;Privacy
Slug: 2022-07-04-hacker-claims-to-have-obtained-data-on-1-billion-chinese-citizens

[Source](https://www.theguardian.com/technology/2022/jul/04/hacker-claims-access-data-billion-chinese-citizens){:target="_blank" rel="noopener"}

> Personal information allegedly taken from Shanghai police database would be one of biggest data breaches in history A hacker has claimed to have stolen the personal information of 1 billion Chinese citizens from a Shanghai police database, in what would amount to one of the biggest data breaches in history if found to be true. The anonymous hacker, identified only as “ChinaDan”, posted on hacker forum Breach Forums last week offering to sell the more than 23 terabytes (TB) of data for 10 bitcoin, equivalent to about $200,000 (£165,000). Continue reading... [...]
