Title: Mantis botnet behind the record-breaking DDoS attack in June
Date: 2022-07-14T11:53:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-14-mantis-botnet-behind-the-record-breaking-ddos-attack-in-june

[Source](https://www.bleepingcomputer.com/news/security/mantis-botnet-behind-the-record-breaking-ddos-attack-in-june/){:target="_blank" rel="noopener"}

> The record-breaking distributed denial-of-service (DDoS) attack that Cloudflare mitigated last month originated from a new botnet called Mantis, which is currently described as "the most powerful botnet to date." [...]
