Title: Cisco fixes bug that lets attackers execute commands as root
Date: 2022-07-20T13:49:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-20-cisco-fixes-bug-that-lets-attackers-execute-commands-as-root

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-bug-that-lets-attackers-execute-commands-as-root/){:target="_blank" rel="noopener"}

> Cisco has addressed severe vulnerabilities in the Cisco Nexus Dashboard data center management solution that can let remote attackers execute commands and perform actions with root or Administrator privileges. [...]
