Title: A pathway to the cloud: Analysis of the Reserve Bank of New Zealand’s Guidance on Cyber Resilience
Date: 2022-07-18T19:29:22+00:00
Author: Julian Busic
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;banking;Financial Services;Guidance on Cyber Resilience;New Zealand;Reserve Bank of New Zealand;Security Blog
Slug: 2022-07-18-a-pathway-to-the-cloud-analysis-of-the-reserve-bank-of-new-zealands-guidance-on-cyber-resilience

[Source](https://aws.amazon.com/blogs/security/a-pathway-to-the-cloud-analysis-of-the-reserve-bank-of-new-zealands-guidance-on-cyber-resilience/){:target="_blank" rel="noopener"}

> The Reserve Bank of New Zealand’s (RBNZ’s) Guidance on Cyber Resilience (referred to as “Guidance” in this post) acknowledges the benefits of RBNZ-regulated financial services companies in New Zealand (NZ) moving to the cloud, as long as this transition is managed prudently—in other words, as long as entities understand the risks involved and manage them [...]
