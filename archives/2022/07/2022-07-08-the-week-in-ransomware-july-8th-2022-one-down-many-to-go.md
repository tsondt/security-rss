Title: The Week in Ransomware - July 8th 2022 - One down, many to go
Date: 2022-07-08T17:21:19-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-08-the-week-in-ransomware-july-8th-2022-one-down-many-to-go

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-july-8th-2022-one-down-many-to-go/){:target="_blank" rel="noopener"}

> While we continue to see new ransomware operations launch, we also received some good news this week, with another ransomware shutting down. [...]
