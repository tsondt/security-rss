Title: Chrome zero-day used to infect journalists with Candiru spyware
Date: 2022-07-21T12:44:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-21-chrome-zero-day-used-to-infect-journalists-with-candiru-spyware

[Source](https://www.bleepingcomputer.com/news/security/chrome-zero-day-used-to-infect-journalists-with-candiru-spyware/){:target="_blank" rel="noopener"}

> The Israeli spyware vendor Candiru was found using a zero-day vulnerability in Google Chrome to spy on journalists and other high-interest individuals in the Middle East with the 'DevilsTongue' spyware. [...]
