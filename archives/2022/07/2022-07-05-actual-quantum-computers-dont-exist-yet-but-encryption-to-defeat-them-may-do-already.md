Title: Actual quantum computers don't exist yet. But encryption to defeat them may do already
Date: 2022-07-05T22:36:33+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-07-05-actual-quantum-computers-dont-exist-yet-but-encryption-to-defeat-them-may-do-already

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/05/us_nist_quantum_algorithms/){:target="_blank" rel="noopener"}

> NIST pushes on with CRYSTALS-KYBER, CRYSTALS-Dilithium, FALCON, and SPHINCS+ The US National Institute of Standards and Technology (NIST) has recommended four cryptographic algorithms for standardization to ensure data can be protected as quantum computers become more capable of decryption.... [...]
