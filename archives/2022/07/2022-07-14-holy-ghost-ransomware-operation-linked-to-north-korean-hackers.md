Title: Holy Ghost ransomware operation linked to North Korean hackers
Date: 2022-07-14T19:10:16-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-07-14-holy-ghost-ransomware-operation-linked-to-north-korean-hackers

[Source](https://www.bleepingcomputer.com/news/security/holy-ghost-ransomware-operation-linked-to-north-korean-hackers/){:target="_blank" rel="noopener"}

> For more than a year, North Korean hackers have been running a ransomware operation called HolyGhost, attacking small businesses in various countries. [...]
