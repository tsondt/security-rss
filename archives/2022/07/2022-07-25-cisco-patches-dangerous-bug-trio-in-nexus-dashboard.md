Title: Cisco patches dangerous bug trio in Nexus Dashboard
Date: 2022-07-25T14:10:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-25-cisco-patches-dangerous-bug-trio-in-nexus-dashboard

[Source](https://portswigger.net/daily-swig/cisco-patches-dangerous-bug-trio-in-nexus-dashboard){:target="_blank" rel="noopener"}

> Inadequate access control and CSRF protections spawn critical and high severity issues [...]
