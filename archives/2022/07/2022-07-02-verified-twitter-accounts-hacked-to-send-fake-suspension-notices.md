Title: Verified Twitter accounts hacked to send fake suspension notices
Date: 2022-07-02T11:12:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-02-verified-twitter-accounts-hacked-to-send-fake-suspension-notices

[Source](https://www.bleepingcomputer.com/news/security/verified-twitter-accounts-hacked-to-send-fake-suspension-notices/){:target="_blank" rel="noopener"}

> [...]
