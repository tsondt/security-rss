Title: Large-Scale Phishing Campaign Bypasses MFA
Date: 2022-07-13T11:45:26+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2022-07-13-large-scale-phishing-campaign-bypasses-mfa

[Source](https://threatpost.com/large-scale-hishing-bypasses-mfa/180212/){:target="_blank" rel="noopener"}

> Attackers used adversary-in-the-middle attacks to steal passwords, hijack sign-in sessions and skip authentication and then use victim mailboxes to launch BEC attacks against other targets. [...]
