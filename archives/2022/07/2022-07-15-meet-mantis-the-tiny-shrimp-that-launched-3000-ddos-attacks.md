Title: Meet Mantis – the tiny shrimp that launched 3,000 DDoS attacks
Date: 2022-07-15T02:28:01+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-15-meet-mantis-the-tiny-shrimp-that-launched-3000-ddos-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/15/mantis_botnet_ddos_attack/){:target="_blank" rel="noopener"}

> Watch out for deadly pinchers after that record-breaking attack The botnet behind the largest-ever HTTPS-based distributed-denial-of-service (DDoS) attack has been named after a tiny shrimp.... [...]
