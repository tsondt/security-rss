Title: Knotweed Euro cyber mercenaries attacking private sector, says Microsoft
Date: 2022-07-27T16:45:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-27-knotweed-euro-cyber-mercenaries-attacking-private-sector-says-microsoft

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/27/knotweed/){:target="_blank" rel="noopener"}

> Reports seeing 'offensive actor' flinging SubZero malware Microsoft has published an analysis of a Europe-based "private-sector offensive actor" with a view to helping its customers spot signs of attacks by money-hungry gangsters.... [...]
