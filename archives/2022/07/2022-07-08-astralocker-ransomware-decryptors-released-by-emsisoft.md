Title: AstraLocker ransomware decryptors released by Emsisoft
Date: 2022-07-08T15:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-08-astralocker-ransomware-decryptors-released-by-emsisoft

[Source](https://portswigger.net/daily-swig/astralocker-ransomware-decryptors-released-by-emsisoft){:target="_blank" rel="noopener"}

> Threat actor released decryption keys after abandoning malware to focus on cryptojacking [...]
