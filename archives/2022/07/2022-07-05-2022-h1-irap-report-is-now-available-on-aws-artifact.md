Title: 2022 H1 IRAP report is now available on AWS Artifact
Date: 2022-07-05T22:09:23+00:00
Author: Matt Brunker
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Australia;IRAP;Security Blog
Slug: 2022-07-05-2022-h1-irap-report-is-now-available-on-aws-artifact

[Source](https://aws.amazon.com/blogs/security/2022-h1-irap-report-is-now-available-on-aws-artifact/){:target="_blank" rel="noopener"}

> We’re excited to announce that a new Information Security Registered Assessors Program (IRAP) report is now available on AWS Artifact. Amazon Web Services (AWS) successfully completed an IRAP assessment in May 2022 by an independent ASD (Australian Signals Directorate) certified IRAP assessor. The new IRAP report includes an additional nine AWS services that are now [...]
