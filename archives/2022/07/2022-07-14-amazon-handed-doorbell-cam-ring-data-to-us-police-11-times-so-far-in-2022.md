Title: Amazon handed doorbell cam Ring data to US police 11 times so far in 2022
Date: 2022-07-14T13:45:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-14-amazon-handed-doorbell-cam-ring-data-to-us-police-11-times-so-far-in-2022

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/14/amazon_gave_police_unauthorized_doorbell/){:target="_blank" rel="noopener"}

> Massachusetts senator Markey on the charge to stop normalizing surveillance for law enforcement purposes Updated Amazon-owned home security company Ring turned over footage to US law enforcement without permission from the devices' owners 11 times so far in 2022, according to Senator Ed Markey.... [...]
