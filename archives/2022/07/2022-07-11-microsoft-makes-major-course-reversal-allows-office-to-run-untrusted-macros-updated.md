Title: Microsoft makes major course reversal, allows Office to run untrusted macros [Updated]
Date: 2022-07-11T16:47:14+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;macros;microsoft;office
Slug: 2022-07-11-microsoft-makes-major-course-reversal-allows-office-to-run-untrusted-macros-updated

[Source](https://arstechnica.com/?p=1865183){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Microsoft has stunned core parts of the security community with a decision to quietly reverse course and allow untrusted macros to be opened by default in Word and other Office applications. ( Update on July 11: The company later clarified that the move is temporary.) In February, the software maker announced a major change it said it enacted to combat the growing scourge of ransomware and other malware attacks. Going forward, macros downloaded from the Internet would be disabled entirely by default. Whereas previously, Office provided alert banners that could be disregarded with the click of [...]
