Title: Convincing ‘YouTube’ Google ads lead to Windows support scams
Date: 2022-07-20T14:43:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-20-convincing-youtube-google-ads-lead-to-windows-support-scams

[Source](https://www.bleepingcomputer.com/news/security/convincing-youtube-google-ads-lead-to-windows-support-scams/){:target="_blank" rel="noopener"}

> A scarily realistic-looking Google Search YouTube advertisement is redirecting visitors to tech support scams pretending to be security alerts from Windows Defender. [...]
