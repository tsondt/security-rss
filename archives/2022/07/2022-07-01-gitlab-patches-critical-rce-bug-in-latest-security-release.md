Title: Gitlab patches critical RCE bug in latest security release
Date: 2022-07-01T13:26:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-01-gitlab-patches-critical-rce-bug-in-latest-security-release

[Source](https://portswigger.net/daily-swig/gitlab-patches-critical-rce-bug-in-latest-security-release){:target="_blank" rel="noopener"}

> Users are urged to update to the latest version [...]
