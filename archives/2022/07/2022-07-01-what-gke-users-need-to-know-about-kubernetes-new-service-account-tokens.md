Title: What GKE users need to know about Kubernetes' new service account tokens
Date: 2022-07-01T16:00:00+00:00
Author: Taahir Ahmed
Category: GCP Security
Tags: Identity & Security;Google Cloud;Containers & Kubernetes
Slug: 2022-07-01-what-gke-users-need-to-know-about-kubernetes-new-service-account-tokens

[Source](https://cloud.google.com/blog/products/containers-kubernetes/kubernetes-bound-service-account-tokens/){:target="_blank" rel="noopener"}

> When you deploy an application on Kubernetes, it runs as a service account — a system user understood by the Kubernetes control plane. The service account is the basic tool for configuring what an application is allowed to do, analogous to the concept of an operating system user on a single machine. Within a Kubernetes cluster, you can use role-based access control to configure what a service account is allowed to do ("list pods in all namespaces", "read secrets in namespace foo"). When running on Google Kubernetes Engine (GKE), you can also use GKE Workload Identity and Cloud IAM to [...]
