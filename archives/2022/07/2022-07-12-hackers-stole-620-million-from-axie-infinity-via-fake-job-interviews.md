Title: Hackers stole $620 million from Axie Infinity via fake job interviews
Date: 2022-07-12T14:03:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-07-12-hackers-stole-620-million-from-axie-infinity-via-fake-job-interviews

[Source](https://www.bleepingcomputer.com/news/security/hackers-stole-620-million-from-axie-infinity-via-fake-job-interviews/){:target="_blank" rel="noopener"}

> The hack that caused Axie Infinity losses of $620 million in crypto started with a fake job offer from North Korean hackers to one of the game's developers. [...]
