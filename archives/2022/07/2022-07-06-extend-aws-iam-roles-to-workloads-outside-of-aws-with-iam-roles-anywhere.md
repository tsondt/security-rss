Title: Extend AWS IAM roles to workloads outside of AWS with IAM Roles Anywhere
Date: 2022-07-06T20:04:19+00:00
Author: Faraz Angabini
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;authentication;authorization;IAM;IAM Roles Anywhere;Identity;Security;Security Blog;Security token service;X.509 certificate
Slug: 2022-07-06-extend-aws-iam-roles-to-workloads-outside-of-aws-with-iam-roles-anywhere

[Source](https://aws.amazon.com/blogs/security/extend-aws-iam-roles-to-workloads-outside-of-aws-with-iam-roles-anywhere/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) has now made it easier for you to use IAM roles for your workloads that are running outside of AWS, with the release of IAM Roles Anywhere. This feature extends the capabilities of IAM roles to workloads outside of AWS. You can use IAM Roles Anywhere to provide a [...]
