Title: ‘Password extraction risk’ in identity provider Okta disputed
Date: 2022-07-19T15:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-19-password-extraction-risk-in-identity-provider-okta-disputed

[Source](https://portswigger.net/daily-swig/password-extraction-risk-in-identity-provider-okta-disputed){:target="_blank" rel="noopener"}

> Researchers go public after vendor disputes impersonation threat [...]
