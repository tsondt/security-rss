Title: How to think about threat detection in the cloud
Date: 2022-07-12T16:00:00+00:00
Author: Timothy Peacock
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-07-12-how-to-think-about-threat-detection-in-the-cloud

[Source](https://cloud.google.com/blog/products/identity-security/how-to-think-about-threat-detection-in-the-cloud/){:target="_blank" rel="noopener"}

> As your organization transitions from on-premises to hybrid cloud or pure cloud, how you think about threat detection must evolve as well—especially when confronting threats across many cloud environments. A new foundational framework for thinking about threat detection in public cloud computing is needed to better secure digital transformations. Because these terms have had different meanings over time, here’s what we mean by threat detection and detection and response. A balanced security strategy covers all three elements of a security triad: prevention, detection, and response. Prevention can improve, but never becomes perfect. Despite preventative controls, we still need to be [...]
