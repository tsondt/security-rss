Title: Hackers breach Ukrainian radio network to spread fake news about Zelenskiy
Date: 2022-07-22T06:56:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-22-hackers-breach-ukrainian-radio-network-to-spread-fake-news-about-zelenskiy

[Source](https://www.bleepingcomputer.com/news/security/hackers-breach-ukrainian-radio-network-to-spread-fake-news-about-zelenskiy/){:target="_blank" rel="noopener"}

> On Thursday, Ukrainian media group TAVR Media confirmed that it was hacked to spread fake news about President Zelenskiy being in critical condition and under intensive care. [...]
