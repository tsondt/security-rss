Title: US eye clinic suffers data breach impacting 92,000 patients
Date: 2022-07-04T12:59:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-04-us-eye-clinic-suffers-data-breach-impacting-92000-patients

[Source](https://portswigger.net/daily-swig/us-eye-clinic-suffers-data-breach-impacting-92-000-patients){:target="_blank" rel="noopener"}

> Mattax Neu Prater Eye Center said customer data was involved in third-party cyber-attack [...]
