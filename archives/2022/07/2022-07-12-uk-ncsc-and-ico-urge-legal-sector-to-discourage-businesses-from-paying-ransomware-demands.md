Title: UK NCSC and ICO urge legal sector to discourage businesses from paying ransomware demands
Date: 2022-07-12T10:07:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-12-uk-ncsc-and-ico-urge-legal-sector-to-discourage-businesses-from-paying-ransomware-demands

[Source](https://portswigger.net/daily-swig/uk-ncsc-and-ico-urge-legal-sector-to-discourage-businesses-from-paying-ransomware-demands){:target="_blank" rel="noopener"}

> Advice comes as cost of cybercrime ‘increases’ [...]
