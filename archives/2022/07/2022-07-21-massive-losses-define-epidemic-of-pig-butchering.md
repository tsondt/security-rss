Title: Massive Losses Define Epidemic of ‘Pig Butchering’
Date: 2022-07-21T16:35:28+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Employment Fraud;Latest Warnings;The Coming Storm;Web Fraud 2.0;Chainalysis;Courtney Nolan;Erin West;fbi;ic3.gov;Justin Maile;pig butchering;REACT Task Force;Vice News;WhatsApp;xtb-market
Slug: 2022-07-21-massive-losses-define-epidemic-of-pig-butchering

[Source](https://krebsonsecurity.com/2022/07/massive-losses-define-epidemic-of-pig-butchering/){:target="_blank" rel="noopener"}

> U.S. state and federal investigators are being inundated with reports from people who’ve lost hundreds of thousands or millions of dollars in connection with a complex investment scam known as “ pig butchering,” wherein people are lured by flirtatious strangers online into investing in cryptocurrency trading platforms that eventually seize any funds when victims try to cash out. The term “pig butchering” refers to a time-tested, heavily scripted, and human-intensive process of using fake profiles on dating apps and social media to lure people into investing in elaborate scams. In a more visceral sense, pig butchering means fattening up a [...]
