Title: Conti’s Reign of Chaos: Costa Rica in the Crosshairs
Date: 2022-07-20T12:35:02+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: InfoSec Insider;Malware
Slug: 2022-07-20-contis-reign-of-chaos-costa-rica-in-the-crosshairs

[Source](https://threatpost.com/contis-costa-rica/180258/){:target="_blank" rel="noopener"}

> Aamir Lakhani, with FortiGuard Labs, answers the question; Why is the Conti ransomware gang targeting people and businesses in Costa Rica? [...]
