Title: Malicious Npm Packages Tapped Again to Target Discord Users
Date: 2022-07-29T15:07:58+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-07-29-malicious-npm-packages-tapped-again-to-target-discord-users

[Source](https://threatpost.com/malicious-npm-discord/180327/){:target="_blank" rel="noopener"}

> Recent LofyLife campaign steals tokens and infects client files to monitor various user actions, such as log-ins, password changes and payment methods. [...]
