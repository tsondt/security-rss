Title: Windows Network File System flaw results in arbitrary code execution as SYSTEM
Date: 2022-07-15T14:15:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-15-windows-network-file-system-flaw-results-in-arbitrary-code-execution-as-system

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/15/windows_nfs_patch/){:target="_blank" rel="noopener"}

> Follina was all very exciting, but did you patch CVE-2022-30136? Trend Micro Research has published an anatomy of a Windows remote code execution vulnerability lurking in the Network File System.... [...]
