Title: Tim Hortons offers free coffee and donut to settle data privacy invasion claims
Date: 2022-07-30T13:25:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-07-30-tim-hortons-offers-free-coffee-and-donut-to-settle-data-privacy-invasion-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/30/in_brief_security/){:target="_blank" rel="noopener"}

> Also, malicious VBA macros are out and container files are in, Robin Banks helps criminals rob banks, and more In brief Canadian fast food chain Tim Hortons is settling multiple data privacy class-action lawsuits against it by offering something it knows it's good for: a donut and coffee.... [...]
