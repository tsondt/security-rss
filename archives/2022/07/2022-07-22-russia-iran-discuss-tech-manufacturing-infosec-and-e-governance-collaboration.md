Title: Russia, Iran discuss tech manufacturing, infosec and e-governance collaboration
Date: 2022-07-22T03:01:52+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-22-russia-iran-discuss-tech-manufacturing-infosec-and-e-governance-collaboration

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/22/iran_russia_tech_collaboration/){:target="_blank" rel="noopener"}

> Proposed working group would see Moscow's miltech conglomerate Rostec operate in Tehran Iran's Communications Ministry joined in a pledge with Russian state-owned defence and technology conglomerate Rostec to explore future collaboration in e-government, information security, and other areas.... [...]
