Title: Microsoft says decision to unblock Office macros is temporary
Date: 2022-07-11T12:53:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-11-microsoft-says-decision-to-unblock-office-macros-is-temporary

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-says-decision-to-unblock-office-macros-is-temporary/){:target="_blank" rel="noopener"}

> Microsoft says last week's decision to roll back VBA macro auto-blocking in downloaded Office documents is only a temporary change. [...]
