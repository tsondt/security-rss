Title: How to introduce more empathy into security operations
Date: 2022-07-26T16:00:00+00:00
Author: Dan Kaplan
Category: GCP Security
Tags: Identity & Security
Slug: 2022-07-26-how-to-introduce-more-empathy-into-security-operations

[Source](https://cloud.google.com/blog/products/identity-security/how-to-introduce-more-empathy-into-security-operations/){:target="_blank" rel="noopener"}

> Editor's note : This blog was originally published by Siemplify on April 29, 2021. With cybersecurity moving from backroom to boardroom, and the societal imperative that workplace cultures renounce toxicity for more sensitivity and tolerance, there is a rapidly-growing call for empathy to pervade the infosec function, from the CISO all the way down. Within security operations, challenges often boil down to alert fatigue, skills shortages and lack of visibility. But another hurdle is just as important : ensuring the extension of humility and compassion—to users, customers, third parties (a huge source of risk ), colleagues on your own team, [...]
