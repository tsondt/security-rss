Title: FBI Warns Fake Crypto Apps are Bilking Investors of Millions
Date: 2022-07-19T15:20:16+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks
Slug: 2022-07-19-fbi-warns-fake-crypto-apps-are-bilking-investors-of-millions

[Source](https://threatpost.com/fbi-warns-fake-crypto-apps/180245/){:target="_blank" rel="noopener"}

> Threat actors offer victims what appear to be investment services from legitimate companies to lure them into downloading malicious apps aimed at defrauding them. [...]
