Title: LinkedIn phishing target employees managing Facebook Ad Accounts
Date: 2022-07-26T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-26-linkedin-phishing-target-employees-managing-facebook-ad-accounts

[Source](https://www.bleepingcomputer.com/news/security/linkedin-phishing-target-employees-managing-facebook-ad-accounts/){:target="_blank" rel="noopener"}

> A new phishing campaign codenamed 'Ducktail' is underway, targeting professionals on LinkedIn to take over Facebook business accounts that manage advertising for the company. [...]
