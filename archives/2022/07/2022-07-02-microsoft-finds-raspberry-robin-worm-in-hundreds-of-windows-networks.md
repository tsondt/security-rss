Title: Microsoft finds Raspberry Robin worm in hundreds of Windows networks
Date: 2022-07-02T10:07:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-07-02-microsoft-finds-raspberry-robin-worm-in-hundreds-of-windows-networks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-finds-raspberry-robin-worm-in-hundreds-of-windows-networks/){:target="_blank" rel="noopener"}

> Microsoft says that a recently spotted Windows worm has been found on the networks of hundreds of organizations from various industry sectors. [...]
