Title: Lockdown Mode: Apple offers $2m bug bounty for vulnerabilities in new anti-spyware tech
Date: 2022-07-07T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-07-lockdown-mode-apple-offers-2m-bug-bounty-for-vulnerabilities-in-new-anti-spyware-tech

[Source](https://portswigger.net/daily-swig/lockdown-mode-apple-offers-2m-bug-bounty-for-vulnerabilities-in-new-anti-spyware-tech){:target="_blank" rel="noopener"}

> Latest feature will protect against targeted attacks [...]
