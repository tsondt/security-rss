Title: Homeland Security warns: Expect Log4j risks for 'a decade or longer'
Date: 2022-07-14T22:59:47+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-14-homeland-security-warns-expect-log4j-risks-for-a-decade-or-longer

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/14/dhs_warns_expect_log4j_risks/){:target="_blank" rel="noopener"}

> Great, another thing that's gone endemic Organizations can expect risks associated with Log4j vulnerabilities for "a decade or longer," according to the US Department of Homeland Security.... [...]
