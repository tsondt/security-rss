Title: Apple to launch ‘lockdown mode’ to protect against Pegasus-style hacks
Date: 2022-07-06T17:17:43+00:00
Author: Dan Milmo and Stephanie Kirchgaessner
Category: The Guardian
Tags: Data and computer security;Apple;iPhone;Hacking;Technology;Smartphones;Mobile phones;Technology sector
Slug: 2022-07-06-apple-to-launch-lockdown-mode-to-protect-against-pegasus-style-hacks

[Source](https://www.theguardian.com/technology/2022/jul/06/apple-to-launch-lockdown-mode-to-protect-against-pegasus-style-hacks){:target="_blank" rel="noopener"}

> Firm says function is intended for users who face ‘grave, targeted threats to their digital security’ Apple is launching a “lockdown mode” for its devices to protect people – including journalists and human rights activists – targeted by hacking attacks like those launched by government clients of NSO Group using its Pegasus spyware. Apple will roll out the setting in the autumn and believes it would have prevented previously known spyware attacks by closing down technical avenues for digital espionage. It said the lockdown mode was intended for users who face “grave, targeted threats to their digital security”. Continue reading... [...]
