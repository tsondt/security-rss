Title: Vivaldi browser founder Jon von Tetzchner puts privacy at the center of development
Date: 2022-07-13T12:31:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-13-vivaldi-browser-founder-jon-von-tetzchner-puts-privacy-at-the-center-of-development

[Source](https://portswigger.net/daily-swig/vivaldi-browser-founder-jon-von-tetzchner-puts-privacy-at-the-center-of-development){:target="_blank" rel="noopener"}

> A man for all four seasons [...]
