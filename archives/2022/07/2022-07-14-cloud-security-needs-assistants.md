Title: Cloud security needs assistants
Date: 2022-07-14T15:45:38+00:00
Author: Belona Greenwood
Category: The Register
Tags: 
Slug: 2022-07-14-cloud-security-needs-assistants

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/14/cloud_security_needs_assistants/){:target="_blank" rel="noopener"}

> Join the Register and Palo Alto Networks to hear the merits of the agents vs agentless approach Sponsored Cloud security is a challenge likely to keep a lot of IT professionals awake at night. So there might be some relief in knowing what types of tool offer the best protection – agent-based or agentless – and if organizations really have to rely on just one or the other.... [...]
