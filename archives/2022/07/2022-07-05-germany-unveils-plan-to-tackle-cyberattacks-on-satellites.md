Title: Germany unveils plan to tackle cyberattacks on satellites
Date: 2022-07-05T12:15:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-05-germany-unveils-plan-to-tackle-cyberattacks-on-satellites

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/05/bsi_satellite_baseline/){:target="_blank" rel="noopener"}

> Vendors get checklist on what to do when crooks inevitably turn up in space The German Federal Office for Information Security (BSI) has put out an IT baseline protection profile for space infrastructure amid concerns that attackers could turn their gaze skywards.... [...]
