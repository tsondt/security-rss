Title: New Lilith ransomware emerges with extortion site, lists first victim
Date: 2022-07-13T17:52:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-13-new-lilith-ransomware-emerges-with-extortion-site-lists-first-victim

[Source](https://www.bleepingcomputer.com/news/security/new-lilith-ransomware-emerges-with-extortion-site-lists-first-victim/){:target="_blank" rel="noopener"}

> A new ransomware operation has been launched under the name 'Lilith,' and it has already posted its first victim on a data leak site created to support double-extortion attacks. [...]
