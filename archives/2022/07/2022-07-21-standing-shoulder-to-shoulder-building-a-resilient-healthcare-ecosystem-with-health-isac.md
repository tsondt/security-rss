Title: Standing shoulder to shoulder - building a resilient healthcare ecosystem with Health-ISAC
Date: 2022-07-21T16:30:00+00:00
Author: Taylor Lehmann
Category: GCP Security
Tags: Healthcare & Life Sciences;Public Sector;Google Cloud;Identity & Security
Slug: 2022-07-21-standing-shoulder-to-shoulder-building-a-resilient-healthcare-ecosystem-with-health-isac

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-joins-with-h-isac-to-help-better-secure-healthcare-systems/){:target="_blank" rel="noopener"}

> Building a resilient healthcare ecosystem is not something done within a vacuum. It takes motivated organizations and people working together in a community of trust to build and defend effectively. We believe the more diverse these communities are, the more effective they can be. Last August, Google announced its commitment to invest at least $10 billion over the next 5 years to advance cybersecurity. We’re making good on our commitment to support the security and digital transformation of government, critical infrastructure, enterprise, small business, and in time, consumers and society overall through community partnerships and other means. As part of [...]
