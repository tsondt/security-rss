Title: Latest Cyberattack Against Iran Part of Ongoing Campaign
Date: 2022-07-05T12:35:09+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Critical Infrastructure;Government;Malware
Slug: 2022-07-05-latest-cyberattack-against-iran-part-of-ongoing-campaign

[Source](https://threatpost.com/cyberattack-iran-campaign/180122/){:target="_blank" rel="noopener"}

> Iran's steel manufacturing industry is victim to ongoing cyberattacks that previously impacted the country's rail system. [...]
