Title: Friday Squid Blogging: Fishing for Squid
Date: 2022-07-06T21:09:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-07-06-friday-squid-blogging-fishing-for-squid

[Source](https://www.schneier.com/blog/archives/2022/07/friday-squid-blogging-fishing-for-squid.html){:target="_blank" rel="noopener"}

> Foreign Policy has a three-part (so far) podcast series on squid and global fishing. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
