Title: Twitter launches probe after miscreants claims to have swiped 5.4m users' details
Date: 2022-07-25T20:21:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-25-twitter-launches-probe-after-miscreants-claims-to-have-swiped-54m-users-details

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/25/twitter_investigates_data_breach/){:target="_blank" rel="noopener"}

> And yes, Musk is back in the headlines, denying another affair Twitter is investigating claims that a near-seven-month-old vulnerability in its software has been exploited to obtain the phone numbers and email addresses of a reported 5.4 million users.... [...]
