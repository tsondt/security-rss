Title: UK response to China's tech ambitions labelled 'incoherent and muted'
Date: 2022-07-11T04:59:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-11-uk-response-to-chinas-tech-ambitions-labelled-incoherent-and-muted

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/11/tech_and_uk_foreign_policy/){:target="_blank" rel="noopener"}

> Working outside power blocs, without policy, leaves Blighty a likely rule-taker says Foreign Affairs Committee The UK's response to China's well-publicized efforts to use technology standards to shape the world in its image has been "incoherent and muted" according to report by the House of Commons Foreign Affairs Committee.... [...]
