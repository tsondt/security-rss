Title: SANS Institute spells out security in multiple languages
Date: 2022-07-07T00:05:04+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-07-07-sans-institute-spells-out-security-in-multiple-languages

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/07/sans_institute_spells_out_security/){:target="_blank" rel="noopener"}

> July and August webcasts in Bahasa Indonesian, Japanese, Korean, Thai, and Vietnamese Sponsored Post If any industry has sought to squeeze the most out of globalisation, it's cybercrime. If technology is a universal language, it stands to reason that hackers' techniques will apply to victims anywhere, regardless of geography, language, or culture.... [...]
