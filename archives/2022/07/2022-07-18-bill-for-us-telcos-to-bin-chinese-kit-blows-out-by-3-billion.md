Title: Bill for US telcos to bin Chinese kit blows out by $3 billion
Date: 2022-07-18T04:59:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-18-bill-for-us-telcos-to-bin-chinese-kit-blows-out-by-3-billion

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/18/scrp_shortfall/){:target="_blank" rel="noopener"}

> Carriers likely to get cents on the dollar for ditched Huawei and ZTE kit unless more funds are found The US Federal Communications Commission (FCC) notified Congress on Friday that the cost to rip and replace equipment kit from Huawei and ZTE installed at US telcos is more than $3 billion higher than funding allocated for the program.... [...]
