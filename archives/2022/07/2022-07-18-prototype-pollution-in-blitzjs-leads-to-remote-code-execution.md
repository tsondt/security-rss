Title: Prototype pollution in Blitz.js leads to remote code execution
Date: 2022-07-18T12:43:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-18-prototype-pollution-in-blitzjs-leads-to-remote-code-execution

[Source](https://portswigger.net/daily-swig/prototype-pollution-in-blitz-js-leads-to-remote-code-execution){:target="_blank" rel="noopener"}

> Chain of exploits could be triggered without any authentication [...]
