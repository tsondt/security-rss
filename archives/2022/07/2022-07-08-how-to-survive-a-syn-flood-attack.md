Title: How to survive a SYN flood attack
Date: 2022-07-08T14:02:08+00:00
Author: Martin Courtney
Category: The Register
Tags: 
Slug: 2022-07-08-how-to-survive-a-syn-flood-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/08/how_to_survive_a_syn/){:target="_blank" rel="noopener"}

> G-Core Labs' XDP-based DDoS protection platform filters bad traffic across a network of high capacity CDNs Sponsored Post If you do any sort of business via the web, the damage caused by a distributed denial of service (DDoS) attack could be catastrophic for your bottom line.... [...]
