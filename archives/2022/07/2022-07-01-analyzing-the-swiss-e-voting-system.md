Title: Analyzing the Swiss E-Voting System
Date: 2022-07-01T14:33:05+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;voting
Slug: 2022-07-01-analyzing-the-swiss-e-voting-system

[Source](https://www.schneier.com/blog/archives/2022/07/analyzing-the-swiss-e-voting-system.html){:target="_blank" rel="noopener"}

> Andrew Appel has a long analysis of the Swiss online voting system. It’s a really good analysis of both the system and the official analyses. [...]
