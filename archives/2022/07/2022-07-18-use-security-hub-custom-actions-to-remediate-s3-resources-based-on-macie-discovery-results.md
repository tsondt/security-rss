Title: Use Security Hub custom actions to remediate S3 resources based on Macie discovery results
Date: 2022-07-18T17:08:20+00:00
Author: Jonathan Nguyen
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Custom Actions;EventBridge;lambda;Macie;Security Blog;Security Hub;Sensitive Data Discovery
Slug: 2022-07-18-use-security-hub-custom-actions-to-remediate-s3-resources-based-on-macie-discovery-results

[Source](https://aws.amazon.com/blogs/security/use-security-hub-custom-actions-to-remediate-s3-resources-based-on-macie-discovery-results/){:target="_blank" rel="noopener"}

> The amount of data available to be collected, stored and processed within an organization’s AWS environment can grow rapidly and exponentially. This increases the operational complexity and the need to identify and protect sensitive data. If your security teams need to review and remediate security risks manually, it would either take a large team or [...]
