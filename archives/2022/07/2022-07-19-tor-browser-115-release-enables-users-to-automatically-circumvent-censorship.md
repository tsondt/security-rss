Title: Tor Browser 11.5 release enables users to automatically circumvent censorship
Date: 2022-07-19T13:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-19-tor-browser-115-release-enables-users-to-automatically-circumvent-censorship

[Source](https://portswigger.net/daily-swig/tor-browser-11-5-release-enables-users-to-automatically-circumvent-censorship){:target="_blank" rel="noopener"}

> New update addresses challenges faced by users in repressive countries [...]
