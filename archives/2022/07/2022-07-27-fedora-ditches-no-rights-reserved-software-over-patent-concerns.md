Title: Fedora ditches 'No Rights Reserved' software over patent concerns
Date: 2022-07-27T07:23:59-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Software
Slug: 2022-07-27-fedora-ditches-no-rights-reserved-software-over-patent-concerns

[Source](https://www.bleepingcomputer.com/news/security/fedora-ditches-no-rights-reserved-software-over-patent-concerns/){:target="_blank" rel="noopener"}

> The Fedora Project has announced that it will no longer permit Creative Commons 'No Rights Reserved' aka CC0-licensed code in its Linux distro or the Fedora Registry. [...]
