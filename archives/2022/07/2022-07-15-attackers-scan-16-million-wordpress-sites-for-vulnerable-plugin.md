Title: Attackers scan 1.6 million WordPress sites for vulnerable plugin
Date: 2022-07-15T03:28:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-15-attackers-scan-16-million-wordpress-sites-for-vulnerable-plugin

[Source](https://www.bleepingcomputer.com/news/security/attackers-scan-16-million-wordpress-sites-for-vulnerable-plugin/){:target="_blank" rel="noopener"}

> Security researchers have detected a massive campaign that scanned close to 1.6 million WordPress sites for the presence of a vulnerable plugin that allows uploading files without authentication. [...]
