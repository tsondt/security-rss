Title: $8 million stolen in large-scale Uniswap airdrop phishing attack
Date: 2022-07-13T10:36:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-13-8-million-stolen-in-large-scale-uniswap-airdrop-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/8-million-stolen-in-large-scale-uniswap-airdrop-phishing-attack/){:target="_blank" rel="noopener"}

> Uniswap, a popular decentralized cryptocurrency exchange, lost close to $8 million worth of Ethereum in a sophisticated phishing attack yesterday. [...]
