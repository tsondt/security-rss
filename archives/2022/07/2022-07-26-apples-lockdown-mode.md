Title: Apple’s Lockdown Mode
Date: 2022-07-26T12:57:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;cybersecurity;iOS;usability;vulnerabilities
Slug: 2022-07-26-apples-lockdown-mode

[Source](https://www.schneier.com/blog/archives/2022/07/apples-lockdown-mode-2.html){:target="_blank" rel="noopener"}

> I haven’t written about Apple’s Lockdown Mode yet, mostly because I haven’t delved into the details. This is how Apple describes it: Lockdown Mode offers an extreme, optional level of security for the very few users who, because of who they are or what they do, may be personally targeted by some of the most sophisticated digital threats, such as those from NSO Group and other private companies developing state-sponsored mercenary spyware. Turning on Lockdown Mode in iOS 16, iPadOS 16, and macOS Ventura further hardens device defenses and strictly limits certain functionalities, sharply reducing the attack surface that potentially [...]
