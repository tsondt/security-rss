Title: BreachForums booms on the back of billion-record Chinese data leak
Date: 2022-07-29T07:05:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-29-breachforums-booms-on-the-back-of-billion-record-chinese-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/29/breachedforums_popularity_surge/){:target="_blank" rel="noopener"}

> Plenty of recent users appear to be from China, and hoping for more leaks of local data The popularity of stolen data bazaar BreachForums surged after it was used to sell a giant database of stolen information describing Chinese citizens, threat intelligence firm Cybersixgill said on Thursday.... [...]
