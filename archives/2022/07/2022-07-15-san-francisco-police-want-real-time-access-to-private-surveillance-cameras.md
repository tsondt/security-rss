Title: San Francisco Police Want Real-Time Access to Private Surveillance Cameras
Date: 2022-07-15T11:17:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cameras;police;privacy;surveillance
Slug: 2022-07-15-san-francisco-police-want-real-time-access-to-private-surveillance-cameras

[Source](https://www.schneier.com/blog/archives/2022/07/san-francisco-police-want-real-time-access-to-private-surveillance-cameras.html){:target="_blank" rel="noopener"}

> Surely no one could have predicted this : The new proposal—championed by Mayor London Breed after November’s wild weekend of orchestrated burglaries and theft in the San Francisco Bay Area—would authorize the police department to use non-city-owned security cameras and camera networks to live monitor “significant events with public safety concerns” and ongoing felony or misdemeanor violations. Currently, the police can only request historical footage from private cameras related to specific times and locations, rather than blanket monitoring. Mayor Breed also complained the police can only use real-time feeds in emergencies involving “imminent danger of death or serious physical injury.” [...]
