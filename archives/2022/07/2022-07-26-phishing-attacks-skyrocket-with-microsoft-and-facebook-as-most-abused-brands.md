Title: Phishing Attacks Skyrocket with Microsoft and Facebook as Most Abused Brands
Date: 2022-07-26T13:05:16+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Hacks
Slug: 2022-07-26-phishing-attacks-skyrocket-with-microsoft-and-facebook-as-most-abused-brands

[Source](https://threatpost.com/popular-bait-in-phishing-attacks/180281/){:target="_blank" rel="noopener"}

> Instances of phishing attacks leveraging the Microsoft brand increased 266 percent in Q1 compared to the year prior. [...]
