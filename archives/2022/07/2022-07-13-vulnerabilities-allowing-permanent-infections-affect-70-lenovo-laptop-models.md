Title: Vulnerabilities allowing permanent infections affect 70 Lenovo laptop models
Date: 2022-07-13T19:44:24+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;firmware;Lenovo;malware;uefi;vulnerabilities
Slug: 2022-07-13-vulnerabilities-allowing-permanent-infections-affect-70-lenovo-laptop-models

[Source](https://arstechnica.com/?p=1866641){:target="_blank" rel="noopener"}

> Enlarge (credit: Lenovo) For owners of more than 70 Lenovo laptop models, it’s time once again to patch the UEFI firmware against critical vulnerabilities that attackers can exploit to install malware that’s nearly impossible to detect or remove. The laptop maker on Tuesday released updates for three vulnerabilities that researchers found in the UEFI firmware used to boot up a host of its laptop models, including the Yoga, ThinkBook, and IdeaPad lines. The company assigned a medium severity rating to the vulnerabilities, which are tracked CVE-2022-1890, CVE-2022-1891, and CVE-2022-1892 and affect the ReadyBootDxe, SystemLoadDefaultDxe, and SystemBootManagerDxe drivers, respectively. [credit: ESET [...]
