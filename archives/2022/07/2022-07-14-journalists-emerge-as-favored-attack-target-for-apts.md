Title: Journalists Emerge as Favored Attack Target for APTs
Date: 2022-07-14T15:08:16+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: 2022-07-14-journalists-emerge-as-favored-attack-target-for-apts

[Source](https://threatpost.com/journalists-target-apts/180224/){:target="_blank" rel="noopener"}

> Since 2021, various state-aligned threat groups have turned up their targeting of journalists to siphon data and credentials and also track them. [...]
