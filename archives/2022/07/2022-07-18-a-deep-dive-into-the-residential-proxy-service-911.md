Title: A Deep Dive Into the Residential Proxy Service ‘911’
Date: 2022-07-18T16:11:12+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;911;International Media Ltd.;Nicolae Aurelian Mazgarean;PPC Leads SRL;Proxygate;residential proxy;Riley Kilmer;spur.us;University of Sherbrooke;ustraffic@qq.com;VIP72;Wugaa Enterprises LLC;Yunhe Wang
Slug: 2022-07-18-a-deep-dive-into-the-residential-proxy-service-911

[Source](https://krebsonsecurity.com/2022/07/a-deep-dive-into-the-residential-proxy-service-911/){:target="_blank" rel="noopener"}

> The 911 service as it exists today. For the past seven years, an online service known as 911 has sold access to hundreds of thousands of Microsoft Windows computers daily, allowing customers to route their Internet traffic through PCs in virtually any country or city around the globe — but predominantly in the United States. 911 says its network is made up entirely of users who voluntarily install its “free VPN” software. But new research shows the proxy service has a long history of purchasing installations via shady “pay-per-install” affiliate marketing schemes, some of which 911 operated on its own. [...]
