Title: Why less can be more in backup and recovery management
Date: 2022-07-14T08:40:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-07-14-why-less-can-be-more-in-backup-and-recovery-management

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/14/why_less_can_be_more/){:target="_blank" rel="noopener"}

> The simplified approach to data protection in hybrid clouds Webinar Most IT infrastructures evolve over time as the needs of the business and its users change to meet fresh demands and comply with updated organizational policies and regulatory requirements.... [...]
