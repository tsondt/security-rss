Title: US military contractor moves to buy Israeli spy-tech company NSO Group
Date: 2022-07-11T13:00:12+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-07-11-us-military-contractor-moves-to-buy-israeli-spy-tech-company-nso-group

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/11/l3harris_nso_group/){:target="_blank" rel="noopener"}

> Biden blacklist a stumbling block for any possible deal US security technology provider L3Harris has courted controversial Israeli spyware firm NSO with an aim to buy it, according to reports.... [...]
