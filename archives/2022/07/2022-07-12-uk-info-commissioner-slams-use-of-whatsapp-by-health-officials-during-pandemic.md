Title: UK Info Commissioner slams use of WhatsApp by health officials during pandemic
Date: 2022-07-12T06:55:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-12-uk-info-commissioner-slams-use-of-whatsapp-by-health-officials-during-pandemic

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/12/uk_department_of_health_and/){:target="_blank" rel="noopener"}

> Sure, stuff got done fast – but personal information was put at risk The UK Information Commissioner's Office (ICO) on Monday issued a reprimand and called for a review of how and whether messaging services should be used for government business practices, after finding widespread and potentially dangerous use of private email, WhatsApp and other messaging tools by officials at the Department of Health and Social Care (DHSC).... [...]
