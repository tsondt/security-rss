Title: Microsoft links Holy Ghost ransomware operation to North Korean hackers
Date: 2022-07-14T19:10:16-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-07-14-microsoft-links-holy-ghost-ransomware-operation-to-north-korean-hackers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-links-holy-ghost-ransomware-operation-to-north-korean-hackers/){:target="_blank" rel="noopener"}

> For more than a year, North Korean hackers have been running a ransomware operation called HolyGhost, attacking small businesses in various countries. [...]
