Title: Security advisory accidentally exposes vulnerable systems
Date: 2022-07-06T15:20:05-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-07-06-security-advisory-accidentally-exposes-vulnerable-systems

[Source](https://www.bleepingcomputer.com/news/security/security-advisory-accidentally-exposes-vulnerable-systems/){:target="_blank" rel="noopener"}

> A security advisory for a vulnerability (CVE) published by MITRE has accidentally been exposing links to remote admin consoles of over a dozen vulnerable IP devices since at least April 2022. [...]
