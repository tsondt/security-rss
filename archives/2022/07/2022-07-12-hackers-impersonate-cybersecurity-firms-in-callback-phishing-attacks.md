Title: Hackers impersonate cybersecurity firms in callback phishing attacks
Date: 2022-07-12T15:54:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-12-hackers-impersonate-cybersecurity-firms-in-callback-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-impersonate-cybersecurity-firms-in-callback-phishing-attacks/){:target="_blank" rel="noopener"}

> Hackers are impersonating well-known cybersecurity companies, such as CrowdStrike, in callback phishing emails to gain initial access to corporate networks. [...]
