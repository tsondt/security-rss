Title: Nigerian Prison Break
Date: 2022-07-11T11:35:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Nigeria;prison escapes;prisons
Slug: 2022-07-11-nigerian-prison-break

[Source](https://www.schneier.com/blog/archives/2022/07/nigerian-prison-break.html){:target="_blank" rel="noopener"}

> There was a massive prison break in Abuja, Nigeria: Armed with bombs, Rocket Propelled Grenade (RPGs) and General Purpose Machine Guns (GPMG), the attackers, who arrived at about 10:05 p.m. local time, gained access through the back of the prison, using dynamites to destroy the heavily fortified facility, freeing 600 out of the prison’s 994 inmates, according to the country’s defense minister, Bashir Magashi.... What’s interesting to me is how the defenders got the threat model wrong. That attack isn’t normally associated with a prison break; it sounds more like a military action in a civil war. [...]
