Title: Friday Squid Blogging: Multiplexing SQUIDs for X-ray Telescopes
Date: 2022-07-01T21:06:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-07-01-friday-squid-blogging-multiplexing-squids-for-x-ray-telescopes

[Source](https://www.schneier.com/blog/archives/2022/07/friday-squid-blogging-multiplexing-squids-for-x-ray-telescopes.html){:target="_blank" rel="noopener"}

> NASA is researching new techniques for multiplexing SQUIDs—that’s superconducting quantum interference devices—for X-ray observatories. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
