Title: How Conti ransomware hacked and encrypted the Costa Rican government
Date: 2022-07-21T10:20:25-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-07-21-how-conti-ransomware-hacked-and-encrypted-the-costa-rican-government

[Source](https://www.bleepingcomputer.com/news/security/how-conti-ransomware-hacked-and-encrypted-the-costa-rican-government/){:target="_blank" rel="noopener"}

> Details have emerged on how the Conti ransomware gang breached the Costa Rican government, showing the attack's precision and the speed of moving from initial access to the final stage of encrypting devices. [...]
