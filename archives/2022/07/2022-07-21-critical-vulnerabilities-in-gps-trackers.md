Title: Critical Vulnerabilities in GPS Trackers
Date: 2022-07-21T13:36:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;GPS;tracking;vulnerabilities
Slug: 2022-07-21-critical-vulnerabilities-in-gps-trackers

[Source](https://www.schneier.com/blog/archives/2022/07/critical-vulnerabilities-in-gps-trackers.html){:target="_blank" rel="noopener"}

> This is a dangerous vulnerability: An assessment from security firm BitSight found six vulnerabilities in the Micodus MV720, a GPS tracker that sells for about $20 and is widely available. The researchers who performed the assessment believe the same critical vulnerabilities are present in other Micodus tracker models. The China-based manufacturer says 1.5 million of its tracking devices are deployed across 420,000 customers. BitSight found the device in use in 169 countries, with customers including governments, militaries, law enforcement agencies, and aerospace, shipping, and manufacturing companies. BitSight discovered what it said were six “severe” vulnerabilities in the device that allow [...]
