Title: US govt warns of Maui ransomware attacks against healthcare orgs
Date: 2022-07-06T10:47:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-06-us-govt-warns-of-maui-ransomware-attacks-against-healthcare-orgs

[Source](https://www.bleepingcomputer.com/news/security/us-govt-warns-of-maui-ransomware-attacks-against-healthcare-orgs/){:target="_blank" rel="noopener"}

> The FBI, CISA, and the U.S. Treasury Department issued today a joint advisory warning of North-Korean-backed threat actors using Maui ransomware in attacks against Healthcare and Public Health (HPH) organizations. [...]
