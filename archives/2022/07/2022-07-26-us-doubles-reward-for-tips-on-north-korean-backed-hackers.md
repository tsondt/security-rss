Title: U.S. doubles reward for tips on North Korean-backed hackers
Date: 2022-07-26T11:06:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-26-us-doubles-reward-for-tips-on-north-korean-backed-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-doubles-reward-for-tips-on-north-korean-backed-hackers/){:target="_blank" rel="noopener"}

> The U.S. State Department has increased rewards paid to anyone providing information on any North Korean-sponsored threat groups' members to $10 million. [...]
