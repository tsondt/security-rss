Title: LockBit ransomware abuses Windows Defender to load Cobalt Strike
Date: 2022-07-29T10:29:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-29-lockbit-ransomware-abuses-windows-defender-to-load-cobalt-strike

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-abuses-windows-defender-to-load-cobalt-strike/){:target="_blank" rel="noopener"}

> Security analysts have observed an affiliate of the LockBit 3.0 ransomware operation abusing a Windows Defender command line tool to decrypt and load Cobalt Strike beacons on the target systems. [...]
