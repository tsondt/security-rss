Title: Multi-cloud doesn’t have to mean multi problems for data protection
Date: 2022-07-06T05:39:10+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2022-07-06-multi-cloud-doesnt-have-to-mean-multi-problems-for-data-protection

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/06/multicloud_doesnt_have_to_mean/){:target="_blank" rel="noopener"}

> Dell aims to take the pain out of backup and recovery for the cloud Sponsored Feature The enterprise multi-cloud migration is in full swing. As businesses continue to simultaneously host more of their applications and workloads with different providers spanning various on- and off-prem environments, it can create problems.... [...]
