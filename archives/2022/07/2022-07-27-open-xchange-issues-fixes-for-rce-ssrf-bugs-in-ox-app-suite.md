Title: Open-Xchange issues fixes for RCE, SSRF bugs in OX App Suite
Date: 2022-07-27T12:49:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-27-open-xchange-issues-fixes-for-rce-ssrf-bugs-in-ox-app-suite

[Source](https://portswigger.net/daily-swig/open-xchange-issues-fixes-for-rce-ssrf-bugs-in-ox-app-suite){:target="_blank" rel="noopener"}

> Security release also includes precautionary patches for potential Log4j-like flaw in Logback library [...]
