Title: Post-quantum cryptography hits standardization milestone
Date: 2022-07-11T15:02:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-11-post-quantum-cryptography-hits-standardization-milestone

[Source](https://portswigger.net/daily-swig/post-quantum-cryptography-hits-standardization-milestone){:target="_blank" rel="noopener"}

> Green light for four ‘future-proofed’ encryption technologies [...]
