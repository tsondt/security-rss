Title: ‘Dirty dancing’ in OAuth: Researcher discloses how cyber-attacks can lead to account hijacking
Date: 2022-07-11T12:33:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-11-dirty-dancing-in-oauth-researcher-discloses-how-cyber-attacks-can-lead-to-account-hijacking

[Source](https://portswigger.net/daily-swig/dirty-dancing-in-oauth-researcher-discloses-how-cyber-attacks-can-lead-to-account-hijacking){:target="_blank" rel="noopener"}

> Single-click account takeovers are made possible by taking advantage of quirks in OAuth [...]
