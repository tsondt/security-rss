Title: Apple network traffic takes mysterious detour through Russia
Date: 2022-07-27T18:56:38+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-07-27-apple-network-traffic-takes-mysterious-detour-through-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/27/apple_networking_traffic_russia_bgp/){:target="_blank" rel="noopener"}

> Land of Putin capable of attacking routes in cyberspace as well as real world Apple's internet traffic took an unwelcome detour through Russian networking equipment for about twelve hours between July 26 and July 27.... [...]
