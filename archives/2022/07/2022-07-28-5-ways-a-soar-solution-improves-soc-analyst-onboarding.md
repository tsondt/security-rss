Title: 5 ways a SOAR solution improves SOC analyst onboarding
Date: 2022-07-28T16:00:00+00:00
Author: Dan Kaplan
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-07-28-5-ways-a-soar-solution-improves-soc-analyst-onboarding

[Source](https://cloud.google.com/blog/products/identity-security/5-ways-a-soar-solution-improves-soc-analyst-onboarding/){:target="_blank" rel="noopener"}

> Editor's note : This blog was originally published by Siemplify on Feb. 19, 2021. The number of unfilled cybersecurity jobs stretches into the millions, and a critical part of the problem is the length of time it takes to backfill a position. Industry group ISACA has found that the average cybersecurity position lies vacant for up to six months. Some positions, like security analyst, are difficult to find suitable candidates for thanks to workplace challenges such as lack of management support and burnout, As the old phrase goes, time is money. So when organizations are fortunate enough to fill a [...]
