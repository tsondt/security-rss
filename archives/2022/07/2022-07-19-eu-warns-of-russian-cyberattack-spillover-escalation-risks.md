Title: EU warns of Russian cyberattack spillover, escalation risks
Date: 2022-07-19T15:57:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-19-eu-warns-of-russian-cyberattack-spillover-escalation-risks

[Source](https://www.bleepingcomputer.com/news/security/eu-warns-of-russian-cyberattack-spillover-escalation-risks/){:target="_blank" rel="noopener"}

> The Council of the European Union (EU) said today that Russian hackers and hacker groups increasingly attacking "essential" organizations worldwide could lead to spillover risks and potential escalation. [...]
