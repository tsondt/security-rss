Title: Don't dive head first into that crypto pool, FBI warns
Date: 2022-07-22T21:00:33+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-22-dont-dive-head-first-into-that-crypto-pool-fbi-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/22/fbi_liquidity_mining_scam/){:target="_blank" rel="noopener"}

> Liquidity scams cost victims more than $70m, agents say The FBI has warned cryptocurrency owners and would-be owners about a scam involving phony liquidity mining that the bureau says has cost victims more than $70 million in combined losses since 2019.... [...]
