Title: Businesses confess: We pass cyberattack costs onto customers
Date: 2022-07-29T06:30:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-29-businesses-confess-we-pass-cyberattack-costs-onto-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/29/ibm_data_inflation/){:target="_blank" rel="noopener"}

> Cover an average of $4.4 million per raid ourselves? No chance, mate The costs incurred by organizations suffering data losses continue to go up, and 60 percent of companies surveyed by IBM said they were passing them onto customers.... [...]
