Title: New UEFI firmware flaws impact over 70 Lenovo laptop models
Date: 2022-07-13T12:15:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-13-new-uefi-firmware-flaws-impact-over-70-lenovo-laptop-models

[Source](https://www.bleepingcomputer.com/news/security/new-uefi-firmware-flaws-impact-over-70-lenovo-laptop-models/){:target="_blank" rel="noopener"}

> The UEFI firmware used in several laptops made by Lenovo is vulnerable to three buffer overflow vulnerabilities that could enable attackers to hijack the startup routine of Windows installations. [...]
