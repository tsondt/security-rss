Title: CISA orders agencies to patch new Windows zero-day used in attacks
Date: 2022-07-12T17:10:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-07-12-cisa-orders-agencies-to-patch-new-windows-zero-day-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-agencies-to-patch-new-windows-zero-day-used-in-attacks/){:target="_blank" rel="noopener"}

> CISA has added an actively exploited local privilege escalation vulnerability in the Windows Client/Server Runtime Subsystem (CSRSS) to its list of bugs abused in the wild. [...]
