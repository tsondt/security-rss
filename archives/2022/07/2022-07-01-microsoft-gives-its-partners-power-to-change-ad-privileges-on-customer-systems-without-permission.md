Title: Microsoft gives its partners power to change AD privileges on customer systems – without permission
Date: 2022-07-01T06:02:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-01-microsoft-gives-its-partners-power-to-change-ad-privileges-on-customer-systems-without-permission

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/01/gdap_permissionless_change_window/){:target="_blank" rel="noopener"}

> Somewhat counterintuitively, this is being done to improve security Microsoft has created a window of time in which its partners can – without permission – create new roles for themselves in customers' Active Directory implementations.... [...]
