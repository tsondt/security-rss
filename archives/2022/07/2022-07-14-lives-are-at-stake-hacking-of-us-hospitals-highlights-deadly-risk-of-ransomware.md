Title: ‘Lives are at stake’: hacking of US hospitals highlights deadly risk of ransomware
Date: 2022-07-14T10:00:54+00:00
Author: Kari Paul in San Francisco
Category: The Guardian
Tags: Hacking;Healthcare industry;Data and computer security;Cybercrime;US news;US politics;US healthcare;Technology
Slug: 2022-07-14-lives-are-at-stake-hacking-of-us-hospitals-highlights-deadly-risk-of-ransomware

[Source](https://www.theguardian.com/technology/2022/jul/14/ransomware-attacks-cybersecurity-targeting-us-hospitals){:target="_blank" rel="noopener"}

> The number of ransomware attacks on US healthcare organizations increased 94% from 2021 to 2022, according to one report Last week, the US government warned that hospitals across the US have been targeted by an aggressive ransomware campaign originating from North Korea since 2021. Ransomware hacks, in which attackers encrypt computer networks and demand payment to make them functional again, have been a growing concern for both the private and public sector since the 90s. But they can be particularly devastating in the healthcare industry, where even minutes of down time can have deadly consequences, and have become ominously frequent. [...]
