Title: 1.9m patient records exposed in healthcare debt collector ransomware attack
Date: 2022-07-13T21:06:50+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-13-19m-patient-records-exposed-in-healthcare-debt-collector-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/13/19m_patients_medical_data_exposed/){:target="_blank" rel="noopener"}

> The P in PFC now stands for Pwned Professional Finance Company, a Colorado-based debt collector whose customers include hundreds of US hospitals, medical clinics, and dental groups, recently disclosed that more than 1.9 million people's private data – including names, addresses, social security numbers and health records – was exposed during a ransomware infection.... [...]
