Title: Threat Actors Pivot Around Microsoft’s Macro-Blocking in Office
Date: 2022-07-28T17:24:17+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2022-07-28-threat-actors-pivot-around-microsofts-macro-blocking-in-office

[Source](https://threatpost.com/threat-pivot-microsofts-macro/180319/){:target="_blank" rel="noopener"}

> Cybercriminals turn to container files and other tactics to get around the company’s attempt to thwart a popular way to deliver malicious phishing payloads. [...]
