Title: Feds put $10m bounty on Putin pal accused of bankrolling US election troll farm
Date: 2022-07-29T19:39:27+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-29-feds-put-10m-bounty-on-putin-pal-accused-of-bankrolling-us-election-troll-farm

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/29/feds_10m_bounty_russia/){:target="_blank" rel="noopener"}

> Just in time for the midterms The Feds have put up a $10 million reward for information about foreign interference in US elections in general, and more specifically a Russian oligarch and close friend of President Vladimir Putin accused of funding an organization that meddled in the 2016 presidential elections.... [...]
