Title: SCOTUS judges 'doxxed' after overturning Roe v Wade
Date: 2022-07-13T18:28:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-13-scotus-judges-doxxed-after-overturning-roe-v-wade

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/13/supremes_doxed_post_roe/){:target="_blank" rel="noopener"}

> Physical and IP addresses as well as credit card info revealed in privacy breach The US Supreme Court justices who overturned Roe v. Wade last month may have been doxxed – had their personal information including physical and IP addresses, and credit card info revealed – according to threat intel firm Cybersixgill.... [...]
