Title: Culture shock: Ransomware gang sacks arts orgs' email lists
Date: 2022-07-26T21:04:44+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-26-culture-shock-ransomware-gang-sacks-arts-orgs-email-lists

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/26/wordfly_ransomware_attack/){:target="_blank" rel="noopener"}

> Don't worry, the crooks totally deleted the data and promised not to use it for evil A ransomware gang has not only taken down WordFly, a mailing list provider for top arts organizations among others, but also siphoned data belonging to the US-based Smithsonian, Canada's Toronto Symphony Orchestra, and the Courtauld Institute of Art in London.... [...]
