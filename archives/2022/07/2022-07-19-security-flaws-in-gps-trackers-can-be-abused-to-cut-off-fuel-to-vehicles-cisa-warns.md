Title: Security flaws in GPS trackers can be abused to cut off fuel to vehicles, CISA warns
Date: 2022-07-19T23:15:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-19-security-flaws-in-gps-trackers-can-be-abused-to-cut-off-fuel-to-vehicles-cisa-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/19/micodus_gps_tracker_vulns/){:target="_blank" rel="noopener"}

> About '1.5 million' folks and organizations use these gadgets A handful of vulnerabilities, some critical, in MiCODUS GPS tracker devices could allow criminals to disrupt fleet operations and spy on routes, or even remotely control or cut off fuel to vehicles, according to CISA. And there's no fixes for these security flaws.... [...]
