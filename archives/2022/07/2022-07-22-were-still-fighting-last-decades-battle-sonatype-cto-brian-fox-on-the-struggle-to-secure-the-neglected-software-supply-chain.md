Title: ‘We’re still fighting last decade’s battle’ – Sonatype CTO Brian Fox on the struggle to secure the neglected software supply chain
Date: 2022-07-22T15:40:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-22-were-still-fighting-last-decades-battle-sonatype-cto-brian-fox-on-the-struggle-to-secure-the-neglected-software-supply-chain

[Source](https://portswigger.net/daily-swig/were-still-fighting-last-decades-battle-sonatype-cto-brian-fox-on-the-struggle-to-secure-the-neglected-software-supply-chain){:target="_blank" rel="noopener"}

> Open source security expert warns there is still a ‘long road’ ahead to prepare for the next attack wave [...]
