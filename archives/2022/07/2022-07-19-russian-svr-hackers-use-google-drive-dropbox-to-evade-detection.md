Title: Russian SVR hackers use Google Drive, Dropbox to evade detection
Date: 2022-07-19T08:35:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-19-russian-svr-hackers-use-google-drive-dropbox-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/russian-svr-hackers-use-google-drive-dropbox-to-evade-detection/){:target="_blank" rel="noopener"}

> State-backed hackers part of Russia's Federation Foreign Intelligence Service (SVR) have switched, for the first time, to using legitimate cloud storage services such as Google Drive to evade detection. [...]
