Title: Windows 11 now blocks RDP brute-force attacks by default
Date: 2022-07-21T07:35:40-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-21-windows-11-now-blocks-rdp-brute-force-attacks-by-default

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-11-now-blocks-rdp-brute-force-attacks-by-default/){:target="_blank" rel="noopener"}

> Recent Windows 11 builds now come with the Account Lockout Policy policy enabled by default which will automatically lock user accounts (including Administrator accounts) after 10 failed sign-in attempts for 10 minutes. [...]
