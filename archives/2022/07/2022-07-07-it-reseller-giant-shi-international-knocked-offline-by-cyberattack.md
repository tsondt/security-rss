Title: IT reseller giant SHI International knocked offline by cyberattack
Date: 2022-07-07T12:15:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-07-it-reseller-giant-shi-international-knocked-offline-by-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/07/shi_outage/){:target="_blank" rel="noopener"}

> Major supplier to US government and enterprise only just getting back on its feet New Jersey-based IT reseller and service provider SHI International was knocked off the web after a July 4 cyberattack.... [...]
