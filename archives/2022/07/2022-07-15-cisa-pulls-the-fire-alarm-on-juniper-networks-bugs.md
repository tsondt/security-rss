Title: CISA pulls the fire alarm on Juniper Networks bugs
Date: 2022-07-15T20:57:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-15-cisa-pulls-the-fire-alarm-on-juniper-networks-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/15/cisa_critical_juniper_bugs/){:target="_blank" rel="noopener"}

> Hate to ruin your Friday Juniper Networks has patched critical-rated bugs across its Junos Space, Contrail Networking and NorthStar Controller products that are serious enough to prompt CISA to weigh in and advise admins to update the software as soon as possible.... [...]
