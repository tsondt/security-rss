Title: Jailed crooks told to cough up $600k for COVID fraud
Date: 2022-07-19T01:59:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-19-jailed-crooks-told-to-cough-up-600k-for-covid-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/19/covid_fraud_sentence/){:target="_blank" rel="noopener"}

> Poetic justice? The virus does love it in some federal prisons Two Florida residents will spend years behind bars and pay more than half a million dollars for wire fraud and identity theft, among other illicit deeds, for running COVID-19 scams.... [...]
