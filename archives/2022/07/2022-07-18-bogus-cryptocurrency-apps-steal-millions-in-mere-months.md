Title: Bogus cryptocurrency apps steal millions in mere months
Date: 2022-07-18T21:46:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-18-bogus-cryptocurrency-apps-steal-millions-in-mere-months

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/18/fbi-cryptocurrency-scams/){:target="_blank" rel="noopener"}

> As if the crypto world needs any help in making money vanish Cybercriminals posing as legitimate investment firms and cryptocurrency exchanges have stolen tens of millions of dollars from more than 200 people by convincing them to download mobile apps and deposit cryptocurrency into wallets owned by the perpetrators.... [...]
