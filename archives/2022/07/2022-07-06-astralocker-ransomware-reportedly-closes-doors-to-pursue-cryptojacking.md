Title: AstraLocker ransomware reportedly closes doors to pursue cryptojacking
Date: 2022-07-06T01:28:43+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-06-astralocker-ransomware-reportedly-closes-doors-to-pursue-cryptojacking

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/06/astralocker-ransomware-shutters-operations/){:target="_blank" rel="noopener"}

> Why go through the hassle of demands and decryption when quietly mining is so much easier? The developer of the AstraLocker ransomware code is reportedly ceasing operations and turning attention to the far simpler art and crime of cryptojacking.... [...]
