Title: Popular vehicle GPS tracker gives hackers admin privileges over SMS
Date: 2022-07-19T11:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2022-07-19-popular-vehicle-gps-tracker-gives-hackers-admin-privileges-over-sms

[Source](https://www.bleepingcomputer.com/news/security/popular-vehicle-gps-tracker-gives-hackers-admin-privileges-over-sms/){:target="_blank" rel="noopener"}

> Vulnerability researchers have found security issues in a GPS tracker that is advertised as being present in about 1.5 million vehicles in 169 countries. [...]
