Title: HavanaCrypt ransomware sails in as a fake Google update
Date: 2022-07-11T16:00:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-11-havanacrypt-ransomware-sails-in-as-a-fake-google-update

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/11/havanacrypt-ransomware-google-update/){:target="_blank" rel="noopener"}

> Difficult to detect, hiding its window by using the ShowWindow function in Windows A new ransomware family is being delivered as a bogus Google Software Update, using Microsoft functionality as part of its attack.... [...]
