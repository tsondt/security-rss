Title: Chrome use subject to restrictions in Dutch schools over data security concerns
Date: 2022-07-23T11:12:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-23-chrome-use-subject-to-restrictions-in-dutch-schools-over-data-security-concerns

[Source](https://www.bleepingcomputer.com/news/security/chrome-use-subject-to-restrictions-in-dutch-schools-over-data-security-concerns/){:target="_blank" rel="noopener"}

> The Ministry of Education in the Netherlands has decided to place a conditional ban on the use of the Chrome OS and Chrome web browser until August 2023 over concerns about data privacy. [...]
