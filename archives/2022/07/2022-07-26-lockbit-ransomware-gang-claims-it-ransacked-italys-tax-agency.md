Title: LockBit ransomware gang claims it ransacked Italy’s tax agency
Date: 2022-07-26T07:30:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-26-lockbit-ransomware-gang-claims-it-ransacked-italys-tax-agency

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/26/lockbit-italy-ransomware-attack/){:target="_blank" rel="noopener"}

> Miscreants boast of 78GB haul, officials say everything's fine The LockBit ransomware crew is claiming to have stolen 78GB of data from Italy's tax agency and is threatening to leak it if a ransom isn't paid by July 31.... [...]
