Title: Time to rethink data protection for cloud workloads
Date: 2022-07-08T00:05:07+00:00
Author: Martin Courtney
Category: The Register
Tags: 
Slug: 2022-07-08-time-to-rethink-data-protection-for-cloud-workloads

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/08/time_to_rethink_data_protection/){:target="_blank" rel="noopener"}

> Cohesity beefs up protection services for Microsoft 365 users in South East Asia Sponsored Post Enterprises often forget that SLAs with cloud providers cover access to the service, but not necessarily protection for the data.... [...]
