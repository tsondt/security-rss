Title: The Week in Ransomware - July 1st 2022 - Bug Bounties
Date: 2022-07-01T15:35:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-01-the-week-in-ransomware-july-1st-2022-bug-bounties

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-july-1st-2022-bug-bounties/){:target="_blank" rel="noopener"}

> It has been relatively busy this week with new ransomware attacks unveiled, a bug bounty program introduced, and new tactics used by the threat actors to distribute their encryptors. [...]
