Title: Actual quantum computers don't exist yet. The encryption to defeat them may already be here
Date: 2022-07-05T22:36:33+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-07-05-actual-quantum-computers-dont-exist-yet-the-encryption-to-defeat-them-may-already-be-here

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/05/us_nist_quantum_algorithms/){:target="_blank" rel="noopener"}

> NIST pushes ahead with CRYSTALS-KYBER, CRYSTALS-Dilithium, FALCON, SPHINCS+ algorithms The US National Institute of Standards and Technology (NIST) has recommended four cryptographic algorithms for standardization to ensure data can be protected as quantum computers become more capable of decryption.... [...]
