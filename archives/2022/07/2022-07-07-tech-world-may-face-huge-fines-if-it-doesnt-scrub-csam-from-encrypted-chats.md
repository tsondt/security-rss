Title: Tech world may face huge fines if it doesn't scrub CSAM from encrypted chats
Date: 2022-07-07T06:27:07+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-07-07-tech-world-may-face-huge-fines-if-it-doesnt-scrub-csam-from-encrypted-chats

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/07/uk_online_safety_bill_chat_scanning/){:target="_blank" rel="noopener"}

> 'It is possible to implement end-to-end crypto in a way that preserves privacy,' claims UK Home Sec Tech companies could be fined $25 million (£18 million) – or ten percent of their global annual revenue – if they don't build suitable mechanisms to scan for child sex abuse material (CSAM) in end-to-end encrypted messages and an amended UK law is passed.... [...]
