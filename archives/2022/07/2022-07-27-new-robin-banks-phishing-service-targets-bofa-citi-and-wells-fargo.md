Title: New ‘Robin Banks’ phishing service targets BofA, Citi, and Wells Fargo
Date: 2022-07-27T14:02:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-27-new-robin-banks-phishing-service-targets-bofa-citi-and-wells-fargo

[Source](https://www.bleepingcomputer.com/news/security/new-robin-banks-phishing-service-targets-bofa-citi-and-wells-fargo/){:target="_blank" rel="noopener"}

> A new phishing as a service (PhaaS) platform named 'Robin Banks' has been launched, offering ready-made phishing kits targeting the customers of well-known banks and online services. [...]
