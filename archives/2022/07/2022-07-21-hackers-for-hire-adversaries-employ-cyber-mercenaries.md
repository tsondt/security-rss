Title: Hackers for Hire: Adversaries Employ ‘Cyber Mercenaries’
Date: 2022-07-21T12:59:30+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks
Slug: 2022-07-21-hackers-for-hire-adversaries-employ-cyber-mercenaries

[Source](https://threatpost.com/hackers-cyber-mercenaries/180263/){:target="_blank" rel="noopener"}

> Also known as the Atlantis Cyber-Army, the emerging organization has an enigmatic leader and a core set of admins that offer a range of services, including exclusive data leaks, DDoS and RDP. [...]
