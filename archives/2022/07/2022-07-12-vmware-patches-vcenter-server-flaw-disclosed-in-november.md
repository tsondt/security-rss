Title: VMware patches vCenter Server flaw disclosed in November
Date: 2022-07-12T19:31:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-12-vmware-patches-vcenter-server-flaw-disclosed-in-november

[Source](https://www.bleepingcomputer.com/news/security/vmware-patches-vcenter-server-flaw-disclosed-in-november/){:target="_blank" rel="noopener"}

> Eight months after disclosing a high-severity privilege escalation flaw in vCenter Server's IWA (Integrated Windows Authentication) mechanism, VMware has finally released a patch for one of the affected versions. [...]
