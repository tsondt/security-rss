Title: Welcoming the AWS Customer Incident Response Team
Date: 2022-07-26T17:13:07+00:00
Author: Kyle Dickinson
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;Incident response;Security Blog;Threat Detection & Incident Response
Slug: 2022-07-26-welcoming-the-aws-customer-incident-response-team

[Source](https://aws.amazon.com/blogs/security/welcoming-the-aws-customer-incident-response-team/){:target="_blank" rel="noopener"}

> Well, hello there! Thanks for reading our inaugural blog post. Who are we, you ask? We are the AWS Customer Incident Response Team (CIRT). The AWS CIRT is a specialized 24/7 global Amazon Web Services (AWS) team that provides support to customers during active security events on the customer side of the AWS Shared Responsibility [...]
