Title: Meta, US hospitals sued for using healthcare data to target ads
Date: 2022-07-30T10:12:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-07-30-meta-us-hospitals-sued-for-using-healthcare-data-to-target-ads

[Source](https://www.bleepingcomputer.com/news/security/meta-us-hospitals-sued-for-using-healthcare-data-to-target-ads/){:target="_blank" rel="noopener"}

> A class action lawsuit has been filed in the Northern District of California against Meta (Facebook), the UCSF Medical Center, and the Dignity Health Medical Foundation, alleging that the organizations are unlawfully collecting sensitive healthcare data about patients for targeted advertising. [...]
