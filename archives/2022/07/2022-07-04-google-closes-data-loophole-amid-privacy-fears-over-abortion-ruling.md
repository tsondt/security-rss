Title: Google closes data loophole amid privacy fears over abortion ruling
Date: 2022-07-04T14:15:54+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;Policy;android;google;Period trackers;privacy;roe v. wade
Slug: 2022-07-04-google-closes-data-loophole-amid-privacy-fears-over-abortion-ruling

[Source](https://arstechnica.com/?p=1864151){:target="_blank" rel="noopener"}

> Enlarge (credit: Lari Bat | Getty Images ) Google is closing a loophole that has allowed thousands of companies to monitor and sell sensitive personal data from Android smartphones, an effort welcomed by privacy campaigners in the wake of the US Supreme Court’s decision to end women’s constitutional right to abortion. It also took a further step on Friday to limit the risk that smartphone data could be used to police new abortion restrictions, announcing it would automatically delete the location history on phones that have been close to a sensitive medical location such an abortion clinic. The Silicon Valley [...]
