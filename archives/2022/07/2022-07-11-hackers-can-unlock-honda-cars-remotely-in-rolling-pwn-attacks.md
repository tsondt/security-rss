Title: Hackers can unlock Honda cars remotely in Rolling-PWN attacks
Date: 2022-07-11T18:10:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-11-hackers-can-unlock-honda-cars-remotely-in-rolling-pwn-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-can-unlock-honda-cars-remotely-in-rolling-pwn-attacks/){:target="_blank" rel="noopener"}

> A team of security researchers found that several modern Honda car models have a vulnerable rolling code mechanism that allows unlocking the cars or even starting the engine remotely. [...]
