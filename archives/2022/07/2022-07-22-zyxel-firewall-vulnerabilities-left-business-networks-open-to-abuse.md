Title: Zyxel firewall vulnerabilities left business networks open to abuse
Date: 2022-07-22T13:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-22-zyxel-firewall-vulnerabilities-left-business-networks-open-to-abuse

[Source](https://portswigger.net/daily-swig/zyxel-firewall-vulnerabilities-left-business-networks-open-to-abuse){:target="_blank" rel="noopener"}

> Severity of code execution bug mitigated by ‘high uptake’ of previous patch [...]
