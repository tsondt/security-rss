Title: Node.js prototype pollution is bad for your app environment
Date: 2022-07-25T21:46:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-07-25-nodejs-prototype-pollution-is-bad-for-your-app-environment

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/25/nodejs_prototype_pollution/){:target="_blank" rel="noopener"}

> Boffins find common code constructs that may be exploitable to achieve remote code execution Back in March, security researchers reported a critical command injection vulnerability in Parse Server, an open-source backend for Node.js environments.... [...]
