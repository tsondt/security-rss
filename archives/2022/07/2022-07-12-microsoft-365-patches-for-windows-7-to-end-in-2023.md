Title: Microsoft 365 patches for Windows 7 to end in 2023
Date: 2022-07-12T12:15:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-12-microsoft-365-patches-for-windows-7-to-end-in-2023

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/12/microsoft_365_windows_7_eol/){:target="_blank" rel="noopener"}

> By then you won't be able to install the suite on Windows 8.1 Microsoft has warned users clinging to Windows 7 and Windows 8.1 that the end really is nigh.... [...]
