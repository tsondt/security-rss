Title: The agent of successful cyber security defense
Date: 2022-07-07T16:15:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-07-07-the-agent-of-successful-cyber-security-defense

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/07/the_agent_of_successful_cyber/){:target="_blank" rel="noopener"}

> A two-pronged approach that combines agent and agentless tools may offer the best protection Webinar Agents sit on devices to perform security scanning and reporting, system restarts/reboots, software patching, configuration and general system monitoring. Agentless security tools do much the same, just without the agents, making them a better bet for security vulnerability scanning on remote machines where its harder to install an agent – like the cloud.... [...]
