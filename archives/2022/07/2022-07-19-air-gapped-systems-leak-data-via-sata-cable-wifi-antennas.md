Title: Air-gapped systems leak data via SATA cable WiFi antennas
Date: 2022-07-19T09:52:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-19-air-gapped-systems-leak-data-via-sata-cable-wifi-antennas

[Source](https://www.bleepingcomputer.com/news/security/air-gapped-systems-leak-data-via-sata-cable-wifi-antennas/){:target="_blank" rel="noopener"}

> An Israeli security researcher has demonstrated a novel attack against air-gapped systems by leveraging the SATA cables inside computers as a wireless antenna to emanate data via radio signals. [...]
