Title: Friday Squid Blogging: Bathyteuthis berryi Holding Eggs
Date: 2022-07-22T21:12:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2022-07-22-friday-squid-blogging-bathyteuthis-berryi-holding-eggs

[Source](https://www.schneier.com/blog/archives/2022/07/friday-squid-blogging-bathyteuthis-berryi-holding-eggs.html){:target="_blank" rel="noopener"}

> Image and video of a Bathyteuthis berryi carrying a few hundred eggs, taken at a depth of 4,650 feet. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
