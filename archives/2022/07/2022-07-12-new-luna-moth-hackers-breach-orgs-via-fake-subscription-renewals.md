Title: New ‘Luna Moth’ hackers breach orgs via fake subscription renewals
Date: 2022-07-12T10:32:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-12-new-luna-moth-hackers-breach-orgs-via-fake-subscription-renewals

[Source](https://www.bleepingcomputer.com/news/security/new-luna-moth-hackers-breach-orgs-via-fake-subscription-renewals/){:target="_blank" rel="noopener"}

> A new data extortion group has been breaching companies to steal confidential information, threatening victims to make the files publicly available unless they pay a ransom. [...]
