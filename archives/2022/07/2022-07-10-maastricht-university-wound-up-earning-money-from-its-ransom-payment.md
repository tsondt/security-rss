Title: Maastricht University wound up earning money from its ransom payment
Date: 2022-07-10T10:03:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-10-maastricht-university-wound-up-earning-money-from-its-ransom-payment

[Source](https://www.bleepingcomputer.com/news/security/maastricht-university-wound-up-earning-money-from-its-ransom-payment/){:target="_blank" rel="noopener"}

> Maastricht University (UM), a Dutch university with more than 22,000 students, said last week that it has recovered the ransom paid after a ransomware attack that hit its network in December 2019. [...]
