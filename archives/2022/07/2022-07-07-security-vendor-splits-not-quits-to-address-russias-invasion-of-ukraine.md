Title: Security vendor splits – not quits – to address Russia's invasion of Ukraine
Date: 2022-07-07T10:44:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-07-security-vendor-splits-not-quits-to-address-russias-invasion-of-ukraine

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/07/group_ib_splits_russian_operation/){:target="_blank" rel="noopener"}

> Singapore's Group-IB was once a Moscow startup and will now conduct 'regional diversification' Singapore-based security vendor and services provider Group-IB has commenced a "regional diversification" program that will see it not just continue to operate in Russia (unlike a great many other companies), but do so with a dedicated entity.... [...]
