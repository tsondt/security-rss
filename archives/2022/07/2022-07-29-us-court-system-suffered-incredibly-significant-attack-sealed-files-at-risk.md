Title: US court system suffered 'incredibly significant attack' – sealed files at risk
Date: 2022-07-29T04:29:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-29-us-court-system-suffered-incredibly-significant-attack-sealed-files-at-risk

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/29/us_judiciary_attack/){:target="_blank" rel="noopener"}

> Effects still being felt today across US government The United States' federal court system "faced an incredibly significant and sophisticated cyber security breach, one which has since had lingering impacts on the department and other agencies."... [...]
