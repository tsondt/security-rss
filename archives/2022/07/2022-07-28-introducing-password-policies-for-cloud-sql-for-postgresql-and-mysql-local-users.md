Title: Introducing password policies for Cloud SQL for PostgreSQL and MySQL local users
Date: 2022-07-28T16:00:00+00:00
Author: Maayan Rossmann
Category: GCP Security
Tags: Databases;Google Cloud;Identity & Security
Slug: 2022-07-28-introducing-password-policies-for-cloud-sql-for-postgresql-and-mysql-local-users

[Source](https://cloud.google.com/blog/products/identity-security/introducing-password-policies-for-cloud-sql-for-postgresql-and-mysql-local-users/){:target="_blank" rel="noopener"}

> Preventing data breaches is an important priority when creating and managing database environments. Ensuring user and application passwords meet high security standards is crucial for reducing risk and helping to achieve compliance with best practices and regulatory standards. To address these concerns, we are thrilled to announce the general availability of Local Users Password Validation feature for Cloud SQL for PostgreSQL and MySQL. It allows you to set up password rules for your local database users and can help better secure your databases. This feature is complementary to the existing Identity and Access Management (IAM) integration. Password validation for PostgreSQL [...]
