Title: Emerging H0lyGh0st Ransomware Tied to North Korea
Date: 2022-07-15T16:26:53+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware
Slug: 2022-07-15-emerging-h0lygh0st-ransomware-tied-to-north-korea

[Source](https://threatpost.com/h0lygh0st-ransomware-north-korea/180232/){:target="_blank" rel="noopener"}

> Microsoft has linked a threat that emerged in June 2021 and targets small-to-mid-sized businesses to state-sponsored actors tracked as DEV-0530. [...]
