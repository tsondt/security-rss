Title: Defense contractor pays $9m to settle whistleblower's cybersecurity allegations
Date: 2022-07-11T18:18:53+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-11-defense-contractor-pays-9m-to-settle-whistleblowers-cybersecurity-allegations

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/11/aerojet_cybersecurity_whistleblower/){:target="_blank" rel="noopener"}

> Former Aerojet Rocketdyne employee cites failure to meet minimums for NASA, Pentagon Aerojet Rocketdyne, which makes propulsion and power systems for launch vehicles, missiles and satellites for NASA and the US military, has agreed to pay $9 million to settle charges it misrepresented its products' compliance with cybersecurity requirements in federal government contracts.... [...]
