Title: Jenkins discloses dozens of zero-day bugs in multiple plugins
Date: 2022-07-01T06:12:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-01-jenkins-discloses-dozens-of-zero-day-bugs-in-multiple-plugins

[Source](https://www.bleepingcomputer.com/news/security/jenkins-discloses-dozens-of-zero-day-bugs-in-multiple-plugins/){:target="_blank" rel="noopener"}

> On Thursday, the Jenkins security team announced 34 security vulnerabilities affecting 29 plugins for the Jenkins open source automation server, 29 of the bugs being zero-days still waiting to be patched. [...]
