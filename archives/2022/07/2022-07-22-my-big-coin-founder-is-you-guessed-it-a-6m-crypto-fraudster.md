Title: My Big Coin founder is – you guessed it – a $6m crypto-fraudster
Date: 2022-07-22T23:08:31+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-22-my-big-coin-founder-is-you-guessed-it-a-6m-crypto-fraudster

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/22/my_big_coin_founder_convicted/){:target="_blank" rel="noopener"}

> Con man blew victims' cash on antiques, artwork, other riches A crook who created a business called My Big Coin to cheat victims out of more than $6 million has been found guilty by a jury.... [...]
