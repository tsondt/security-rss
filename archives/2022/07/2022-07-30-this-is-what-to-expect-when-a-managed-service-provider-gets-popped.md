Title: This is what to expect when a managed service provider gets popped
Date: 2022-07-30T00:30:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-30-this-is-what-to-expect-when-a-managed-service-provider-gets-popped

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/30/msp_access_russia/){:target="_blank" rel="noopener"}

> MSP should just stand for My Server's Pwned! A Russian-language miscreant claims to have hacked their way into a managed service provider, and has asked for help monetizing what's said to be access to the networks and computers of that MSP's 50-plus US customers.... [...]
