Title: Magecart Serves Up Card Skimmers on Restaurant-Ordering Systems
Date: 2022-07-20T12:14:47+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2022-07-20-magecart-serves-up-card-skimmers-on-restaurant-ordering-systems

[Source](https://threatpost.com/magecart-restaurant-ordering-systems/180254/){:target="_blank" rel="noopener"}

> 300 restaurants and at least 50,000 payment cards compromised by two separate campaigns against MenuDrive, Harbortouch and InTouchPOS services. [...]
