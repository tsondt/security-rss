Title: Dutch University retrieves Bitcoin ransomware payment and makes a profit
Date: 2022-07-05T07:46:32+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-05-dutch-university-retrieves-bitcoin-ransomware-payment-and-makes-a-profit

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/05/maastricht_university_ransom_return/){:target="_blank" rel="noopener"}

> Promises funds will be used to support struggling students The Netherlands' Maastricht University has managed to recoup the Bitcoin ransom it paid to ransomware scum in 2019 – and has made a tidy profit on the deal.... [...]
