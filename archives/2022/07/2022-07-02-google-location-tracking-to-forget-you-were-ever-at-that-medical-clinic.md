Title: Google location tracking to forget you were ever at that medical clinic
Date: 2022-07-02T07:41:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-07-02-google-location-tracking-to-forget-you-were-ever-at-that-medical-clinic

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/02/google_medical_privacy/){:target="_blank" rel="noopener"}

> Plus: Cyber-mercenaries said to target legal world, backdoor found on web servers, and more In brief Google on Friday pledged to update its location history system so that visits to medical clinics and similarly sensitive places are automatically deleted.... [...]
