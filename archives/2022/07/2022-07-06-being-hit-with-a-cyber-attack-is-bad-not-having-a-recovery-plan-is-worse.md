Title: Being hit with a cyber-attack is bad. Not having a recovery plan is worse
Date: 2022-07-06T15:42:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-07-06-being-hit-with-a-cyber-attack-is-bad-not-having-a-recovery-plan-is-worse

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/06/being_hit_with_a_cyberattack/){:target="_blank" rel="noopener"}

> This webinar will show you how to bounce back Webinar What’s the biggest threat to your business? Ransomware? A natural disaster? A critical infrastructure failure?... [...]
