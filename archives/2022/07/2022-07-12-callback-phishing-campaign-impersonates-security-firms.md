Title: ‘Callback’ Phishing Campaign Impersonates Security Firms
Date: 2022-07-12T11:43:11+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2022-07-12-callback-phishing-campaign-impersonates-security-firms

[Source](https://threatpost.com/callback-phishing-security-firms/180182/){:target="_blank" rel="noopener"}

> Victims instructed to make a phone call that will direct them to a link for downloading malware. [...]
