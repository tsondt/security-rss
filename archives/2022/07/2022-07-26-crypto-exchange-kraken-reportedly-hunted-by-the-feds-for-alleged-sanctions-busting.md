Title: Crypto exchange Kraken reportedly hunted by the Feds for alleged sanctions busting
Date: 2022-07-26T22:36:04+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-26-crypto-exchange-kraken-reportedly-hunted-by-the-feds-for-alleged-sanctions-busting

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/26/us_government_kraken/){:target="_blank" rel="noopener"}

> Plus: Coinbase said to face SEC wrath, blockchain scam CEO admits using victims' millions to fund Hawaiian condo The US government is reportedly investigating Kraken, a massive cryptocurrency exchange suspected of violating sanctions against Iran, and is expected to slap the crypto behemoth with a fine in the near future.... [...]
