Title: PyPI repo to distribute 4,000 security keys to maintainers of ‘critical projects’ in 2FA drive
Date: 2022-07-11T15:56:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-11-pypi-repo-to-distribute-4000-security-keys-to-maintainers-of-critical-projects-in-2fa-drive

[Source](https://portswigger.net/daily-swig/pypi-repo-to-distribute-4-000-security-keys-to-maintainers-of-critical-projects-in-2fa-drive){:target="_blank" rel="noopener"}

> Google is providing Titan Security Keys to maintainers of projects in top 1% of downloads [...]
