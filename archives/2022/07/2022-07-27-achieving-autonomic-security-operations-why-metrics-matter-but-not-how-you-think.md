Title: Achieving Autonomic Security Operations: Why metrics matter (but not how you think)
Date: 2022-07-27T16:00:00+00:00
Author: Anton Chuvakin
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-07-27-achieving-autonomic-security-operations-why-metrics-matter-but-not-how-you-think

[Source](https://cloud.google.com/blog/products/identity-security/mind-your-metrics-to-achieve-better-autonomic-security-operations/){:target="_blank" rel="noopener"}

> What’s the most difficult question a security operations team can face? For some, is it, “Who is trying to attacks us?” Or perhaps, “Which cyberattacks can we detect?” How do teams know when they have enough information to make the “right” decision? Metrics can help inform our responses to those questions and more, but how can we tell which metrics are the best ones to rely on during mission-critical or business-critical crises? As we discussed in our blogs, “ Achieving Autonomic Security Operations: Reducing toil ” and “ Achieving Autonomic Security Operations: Automation as a Force Multiplier,” your Security Operations [...]
