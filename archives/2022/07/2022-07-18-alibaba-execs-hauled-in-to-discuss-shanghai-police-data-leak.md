Title: Alibaba execs hauled in to discuss Shanghai Police data leak
Date: 2022-07-18T01:15:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-18-alibaba-execs-hauled-in-to-discuss-shanghai-police-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/18/apac_tech_news_roundup/){:target="_blank" rel="noopener"}

> Plus: Weibo cracks down on political puns; Singaporean crypto biz Vauld restructures; Philippines fights Facebook rumors Asia In Brief Senior execs from Alibaba Cloud were summoned to discuss the data leak that saw information pertaining to a billion Chinese citizens sold on the dark web, according to Nikkei and The Wall Street Journal.... [...]
