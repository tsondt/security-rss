Title: Use AWS Chatbot in Slack to remediate security findings from AWS Security Hub
Date: 2022-07-25T17:58:21+00:00
Author: Vikas Purohit
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;AWS Chatbot;ChatSecOps;Security Blog;Security Hub
Slug: 2022-07-25-use-aws-chatbot-in-slack-to-remediate-security-findings-from-aws-security-hub

[Source](https://aws.amazon.com/blogs/security/use-aws-chatbot-in-slack-to-remediate-security-findings-from-aws-security-hub/){:target="_blank" rel="noopener"}

> You can use AWS Chatbot and its integration with Slack and Amazon Chime to receive and remediate security findings from AWS Security Hub. To learn about how to configure AWS Chatbot to send findings from Security Hub to Slack, see the blog post Enabling AWS Security Hub integration with AWS Chatbot. In this blog post, [...]
