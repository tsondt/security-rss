Title: Cloud CISO Perspectives: July 2022
Date: 2022-07-28T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-07-28-cloud-ciso-perspectives-july-2022

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-july-2022/){:target="_blank" rel="noopener"}

> Welcome to this month’s Cloud CISO Perspectives. Starting with this issue, we’re going to add even more top-of-mind content based on the hot topics we see emerge across the industry and with our customers, globally. Today, we're focusing on the evolving relationship between an organization's boardroom and its cybersecurity practice, especially in the context of digital transformation to the cloud. This has been a regular dialogue of late, driven in part by corporate risk processes, potential regulations, and ongoing drum beats to improve cybersecurity risk mitigation, all while managing the enterprise's strategic, competitive, and defensive risks. As with all Cloud [...]
