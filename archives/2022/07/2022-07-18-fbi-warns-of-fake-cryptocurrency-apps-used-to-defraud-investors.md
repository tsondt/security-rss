Title: FBI warns of fake cryptocurrency apps used to defraud investors
Date: 2022-07-18T13:36:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-18-fbi-warns-of-fake-cryptocurrency-apps-used-to-defraud-investors

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-fake-cryptocurrency-apps-used-to-defraud-investors/){:target="_blank" rel="noopener"}

> The FBI warned that cybercriminals are creating and using fraudulent cryptocurrency investment applications to steal funds from US cryptocurrency investors. [...]
