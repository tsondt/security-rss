Title: Malicious npm packages steal Discord users’ payment card info
Date: 2022-07-28T10:13:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-28-malicious-npm-packages-steal-discord-users-payment-card-info

[Source](https://www.bleepingcomputer.com/news/security/malicious-npm-packages-steal-discord-users-payment-card-info/){:target="_blank" rel="noopener"}

> Multiple npm packages are being used in an ongoing malicious campaign to infect Discord users with malware that steals their payment card information. [...]
