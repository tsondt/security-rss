Title: Hackers steal $6 million from blockchain music platform Audius
Date: 2022-07-26T12:09:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-07-26-hackers-steal-6-million-from-blockchain-music-platform-audius

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-6-million-from-blockchain-music-platform-audius/){:target="_blank" rel="noopener"}

> The decentralized music platform Audius was hacked over the weekend, with threat actors stealing over 18 million AUDIO tokens worth approximately $6 million. [...]
