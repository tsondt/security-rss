Title: UK heat wave causes Google and Oracle cloud outages
Date: 2022-07-19T16:07:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-19-uk-heat-wave-causes-google-and-oracle-cloud-outages

[Source](https://www.bleepingcomputer.com/news/security/uk-heat-wave-causes-google-and-oracle-cloud-outages/){:target="_blank" rel="noopener"}

> An ongoing heatwave in the United Kingdom has led to Google Cloud and Oracle Cloud outages after cooling systems failed at the companies' data centers. [...]
