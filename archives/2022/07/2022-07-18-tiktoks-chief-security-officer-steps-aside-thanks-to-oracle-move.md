Title: TikTok's chief security officer steps aside, thanks to Oracle move
Date: 2022-07-18T03:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-18-tiktoks-chief-security-officer-steps-aside-thanks-to-oracle-move

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/18/tiktok_cso_steps_down/){:target="_blank" rel="noopener"}

> Takes up advisory role that might leave time to play with parent company's homebrew cloudy SmartNICs TikTok's Global Chief Security Officer Roland Cloutier has "transitioned" from his job into "a strategic advisory role focusing on the business impact of security and trust programs."... [...]
