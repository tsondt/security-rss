Title: Hive ransomware gang rapidly evolves with complex encryption, Rust code
Date: 2022-07-06T17:50:04+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-06-hive-ransomware-gang-rapidly-evolves-with-complex-encryption-rust-code

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/06/hive-ransomware-rust-microsoft/){:target="_blank" rel="noopener"}

> RaaS malware devs have been busy bees The Hive group, which has become one of the most prolific ransomware-as-a-service (RaaS) operators, has significantly overhauled its malware, including migrating the code to the Rust programming language and using a more complex file encryption process.... [...]
