Title: UK Army’s Twitter, YouTube accounts hacked to push crypto scam
Date: 2022-07-04T09:43:28-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-07-04-uk-armys-twitter-youtube-accounts-hacked-to-push-crypto-scam

[Source](https://www.bleepingcomputer.com/news/security/uk-army-s-twitter-youtube-accounts-hacked-to-push-crypto-scam/){:target="_blank" rel="noopener"}

> British Army's Twitter and YouTube accounts were hacked sometime yesterday and altered to promote online crypto scams. In a statement, UK's Ministry of Defence confirms it is investigating the attack. [...]
