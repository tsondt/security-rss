Title: HackerOne employee stole data from bug bounty reports for financial gain
Date: 2022-07-05T13:23:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-05-hackerone-employee-stole-data-from-bug-bounty-reports-for-financial-gain

[Source](https://portswigger.net/daily-swig/hackerone-employee-stole-data-from-bug-bounty-reports-for-financial-gain){:target="_blank" rel="noopener"}

> Vulnerability disclosure platform shares details of incident [...]
