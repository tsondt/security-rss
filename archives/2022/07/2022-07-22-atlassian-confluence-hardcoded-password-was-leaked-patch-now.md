Title: Atlassian: Confluence hardcoded password was leaked, patch now!
Date: 2022-07-22T11:05:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-22-atlassian-confluence-hardcoded-password-was-leaked-patch-now

[Source](https://www.bleepingcomputer.com/news/security/atlassian-confluence-hardcoded-password-was-leaked-patch-now/){:target="_blank" rel="noopener"}

> Australian software firm Atlassian warned customers to immediately patch a critical vulnerability that provides remote attackers with hardcoded credentials to log into unpatched Confluence Server and Data Center servers. [...]
