Title: How data on a billion people may have leaked from a Chinese police dashboard
Date: 2022-07-10T16:48:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-10-how-data-on-a-billion-people-may-have-leaked-from-a-chinese-police-dashboard

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/10/stolen_shanghai_police_data/){:target="_blank" rel="noopener"}

> Record-breaking dump thanks to password-less Kibana endpoint? Details have emerged on how more than a billion personal records were stolen in China and put up for sale on the dark web, and it all boils down to a unprotected online dashboard that left the data open to anyone who could find it.... [...]
