Title: Google brings Street View back to India following 2016 ban
Date: 2022-07-28T10:28:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-28-google-brings-street-view-back-to-india-following-2016-ban

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/28/google_street_view_returns_india/){:target="_blank" rel="noopener"}

> This time local companies provide the images and there's no mention of national security worries Google has brought its Street View service – which offers photographs of most locations on Google Maps – back to India, six years after the nation rejected it as an invasion of privacy and a threat to national security.... [...]
