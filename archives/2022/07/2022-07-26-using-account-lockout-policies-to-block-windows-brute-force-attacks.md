Title: Using Account Lockout policies to block Windows Brute Force Attacks
Date: 2022-07-26T10:04:02-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-07-26-using-account-lockout-policies-to-block-windows-brute-force-attacks

[Source](https://www.bleepingcomputer.com/news/security/using-account-lockout-policies-to-block-windows-brute-force-attacks/){:target="_blank" rel="noopener"}

> A strong account lockout policy is one of the most effective tools for stopping brute force authentication attempts on Windows domains. Learn how to add one to your organization's Windows Active Directory. [...]
