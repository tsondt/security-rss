Title: NSO Group’s Pegasus Spyware Used against Thailand Pro-Democracy Activists and Leaders
Date: 2022-07-19T14:40:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Citizen Lab;hacking;human rights;spyware;Thailand
Slug: 2022-07-19-nso-groups-pegasus-spyware-used-against-thailand-pro-democracy-activists-and-leaders

[Source](https://www.schneier.com/blog/archives/2022/07/nso-groups-pegasus-spyware-used-against-thailand-pro-democracy-activists-and-leaders.html){:target="_blank" rel="noopener"}

> Yet another basic human rights violation, courtesy of NSO Group: Citizen Lab has the details : Key Findings We discovered an extensive espionage campaign targeting Thai pro-democracy protesters, and activists calling for reforms to the monarchy. We forensically confirmed that at least 30 individuals were infected with NSO Group’s Pegasus spyware. The observed infections took place between October 2020 and November 2021. The ongoing investigation was triggered by notifications sent by Apple to Thai civil society members in November 2021. Following the notification, multiple recipients made contact with civil society groups, including the Citizen Lab. The report describes the results [...]
