Title: British Army Twitter and YouTube feeds hijacked by crypto-promos
Date: 2022-07-04T01:07:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-04-british-army-twitter-and-youtube-feeds-hijacked-by-crypto-promos

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/04/british_army_social_media_hijcaked/){:target="_blank" rel="noopener"}

> If you can't defend against crypto bros... The British Army has apologized after its Twitter and YouTube accounts were compromised by entities that used them to promote NFTs.... [...]
