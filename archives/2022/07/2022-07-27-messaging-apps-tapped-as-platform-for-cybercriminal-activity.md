Title: Messaging Apps Tapped as Platform for Cybercriminal Activity
Date: 2022-07-27T16:57:23+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks;Malware
Slug: 2022-07-27-messaging-apps-tapped-as-platform-for-cybercriminal-activity

[Source](https://threatpost.com/messaging-apps-cybercriminals/180303/){:target="_blank" rel="noopener"}

> Built-in Telegram and Discord services are fertile ground for storing stolen data, hosting malware and using bots for nefarious purposes. [...]
