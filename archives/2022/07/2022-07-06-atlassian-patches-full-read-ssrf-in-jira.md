Title: Atlassian patches full-read SSRF in Jira
Date: 2022-07-06T15:21:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-06-atlassian-patches-full-read-ssrf-in-jira

[Source](https://portswigger.net/daily-swig/atlassian-patches-full-read-ssrf-in-jira){:target="_blank" rel="noopener"}

> Severity of authenticated flaw heightened by abuse of Jira Service Desk signup facility [...]
