Title: Australia charges dev of Imminent Monitor RAT used by domestic abusers
Date: 2022-07-31T19:48:55-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-31-australia-charges-dev-of-imminent-monitor-rat-used-by-domestic-abusers

[Source](https://www.bleepingcomputer.com/news/security/australia-charges-dev-of-imminent-monitor-rat-used-by-domestic-abusers/){:target="_blank" rel="noopener"}

> ​An Australian man was charged for developing and selling the Imminent Monitor remote access trojan, used to spy on victims' devices remotely. [...]
