Title: Enforcing Password History in Your Windows AD to Curb Password Reuse
Date: 2022-07-18T10:04:02-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-07-18-enforcing-password-history-in-your-windows-ad-to-curb-password-reuse

[Source](https://www.bleepingcomputer.com/news/security/enforcing-password-history-in-your-windows-ad-to-curb-password-reuse/){:target="_blank" rel="noopener"}

> 65% of end-users openly admit to reusing the same password for one or more (or all!) of their accounts. Password history requirements discourage this behavior by making it more difficult for a user to reuse their old password. [...]
