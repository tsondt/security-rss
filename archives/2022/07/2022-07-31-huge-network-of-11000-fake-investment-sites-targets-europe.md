Title: Huge network of 11,000 fake investment sites targets Europe
Date: 2022-07-31T10:22:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-31-huge-network-of-11000-fake-investment-sites-targets-europe

[Source](https://www.bleepingcomputer.com/news/security/huge-network-of-11-000-fake-investment-sites-targets-europe/){:target="_blank" rel="noopener"}

> Researchers have uncovered a gigantic network of more than 11,000 domains used to promote numerous fake investment schemes to users in Europe. [...]
