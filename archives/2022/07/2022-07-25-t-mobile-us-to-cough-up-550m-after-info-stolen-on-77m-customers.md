Title: T-Mobile US to cough up $550m after info stolen on 77m customers
Date: 2022-07-25T20:58:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-25-t-mobile-us-to-cough-up-550m-after-info-stolen-on-77m-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/25/tmobile_to_pay_550m_data_breach/){:target="_blank" rel="noopener"}

> Oops, did the Un-carrier under-count by 29m punters? T-Mobile US has agreed to pay about $550 million to end legal action against it and improve its security after crooks infiltrated the self-described Un-carrier last summer and harvested personal data belonging to almost 77 million customers.... [...]
