Title: Ransomware, hacking groups move from Cobalt Strike to Brute Ratel
Date: 2022-07-06T13:32:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-06-ransomware-hacking-groups-move-from-cobalt-strike-to-brute-ratel

[Source](https://www.bleepingcomputer.com/news/security/ransomware-hacking-groups-move-from-cobalt-strike-to-brute-ratel/){:target="_blank" rel="noopener"}

> Hacking groups and ransomware operations are moving away from Cobalt Strike to the newer Brute Ratel post-exploitation toolkit to evade detection by EDR and antivirus solutions. [...]
