Title: 911 Proxy Service Implodes After Disclosing Breach
Date: 2022-07-29T19:34:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Web Fraud 2.0;911;LuxSocks;residential proxies;Riley Kilmer;spur.us;VIP72
Slug: 2022-07-29-911-proxy-service-implodes-after-disclosing-breach

[Source](https://krebsonsecurity.com/2022/07/911-proxy-service-implodes-after-disclosing-breach/){:target="_blank" rel="noopener"}

> The 911 service as it existed until July 28, 2022. 911[.]re, a proxy service that since 2015 has sold access to hundreds of thousands of Microsoft Windows computers daily, announced this week that it is shutting down in the wake of a data breach that destroyed key components of its business operations. The abrupt closure comes ten days after KrebsOnSecurity published an in-depth look at 911 and its connections to shady pay-per-install affiliate programs that secretly bundled 911’s proxy software with other titles, including “free” utilities and pirated software. 911[.]re is was one of the original “residential proxy” networks, which [...]
