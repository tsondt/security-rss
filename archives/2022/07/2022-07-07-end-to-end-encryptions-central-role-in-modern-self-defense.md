Title: End-to-end encryption’s central role in modern self-defense
Date: 2022-07-07T11:07:38+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;encryption;privacy
Slug: 2022-07-07-end-to-end-encryptions-central-role-in-modern-self-defense

[Source](https://arstechnica.com/?p=1864680){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) A number of course-altering US Supreme Court decisions last month—including the reversal of a constitutional right to abortion and the overturning of a century-old limit on certain firearms permits—have activists and average Americans around the country anticipating the fallout for rights and privacy as abortion “trigger laws,” expanded access to concealed carry permits, and other regulations are expected to take effect in some states. And as people seeking abortions scramble to protect their digital privacy and researchers plumb the relationship between abortion speech and tech regulations, encryption proponents have a clear message: Access to end-to-end [...]
