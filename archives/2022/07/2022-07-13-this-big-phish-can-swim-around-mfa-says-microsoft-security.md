Title: This big phish can swim around MFA, says Microsoft Security
Date: 2022-07-13T19:04:56+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-13-this-big-phish-can-swim-around-mfa-says-microsoft-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/13/aitm-phishing-microsoft/){:target="_blank" rel="noopener"}

> Slippery AiTM attacks targeted more than 10,000 orgs over the past nine months A widespread phishing campaign that has hit more than 10,000 organizations since September 2021 uses adversary-in-the-middle (AiTM) proxy sites to get around multifactor authentication (MFA) features and steal credentials that are then used to compromise business email accounts.... [...]
