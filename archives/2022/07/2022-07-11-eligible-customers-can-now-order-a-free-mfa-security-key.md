Title: Eligible customers can now order a free MFA security key
Date: 2022-07-11T15:02:32+00:00
Author: CJ Moses
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;cybersecurity;MFA;MFA security key;Security Blog
Slug: 2022-07-11-eligible-customers-can-now-order-a-free-mfa-security-key

[Source](https://aws.amazon.com/blogs/security/eligible-customers-can-now-order-a-free-mfa-security-key/){:target="_blank" rel="noopener"}

> One of the best ways for individuals and businesses to protect themselves online is through multi-factor authentication (MFA). MFA offers an additional layer of protection to help prevent unauthorized individuals from gaining access to systems or data. In fall 2021, Amazon Web Services (AWS) Security began offering a free MFA security key to AWS account [...]
