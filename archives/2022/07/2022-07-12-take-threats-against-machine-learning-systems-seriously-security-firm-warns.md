Title: Take threats against machine learning systems seriously, security firm warns
Date: 2022-07-12T13:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-12-take-threats-against-machine-learning-systems-seriously-security-firm-warns

[Source](https://portswigger.net/daily-swig/take-threats-against-machine-learning-systems-seriously-security-firm-warns){:target="_blank" rel="noopener"}

> A new white paper from NCC Group details the myriad security threats associated with machine learning models [...]
