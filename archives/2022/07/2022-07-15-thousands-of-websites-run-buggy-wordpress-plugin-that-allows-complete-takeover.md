Title: Thousands of websites run buggy WordPress plugin that allows complete takeover
Date: 2022-07-15T19:15:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-15-thousands-of-websites-run-buggy-wordpress-plugin-that-allows-complete-takeover

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/15/buggy_wordpress_plugin/){:target="_blank" rel="noopener"}

> All versions are susceptible, there's no patch, so now's a good time to remove this add-on Miscreants have reportedly scanned almost 1.6 million websites in attempts to exploit an arbitrary file upload vulnerability in a previously disclosed buggy WordPress plugin.... [...]
