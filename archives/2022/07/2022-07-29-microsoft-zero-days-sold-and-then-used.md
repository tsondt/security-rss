Title: Microsoft Zero-Days Sold and then Used
Date: 2022-07-29T15:08:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2022-07-29-microsoft-zero-days-sold-and-then-used

[Source](https://www.schneier.com/blog/archives/2022/07/microsoft-zero-days-sold-and-then-used.html){:target="_blank" rel="noopener"}

> Yet another article about cyber-weapons arms manufacturers and their particular supply chain. This one is about Windows and Adobe Reader zero-day exploits sold by an Austrian company named DSIRF. There’s an entire industry devoted to undermining all of our security. It needs to be stopped. [...]
