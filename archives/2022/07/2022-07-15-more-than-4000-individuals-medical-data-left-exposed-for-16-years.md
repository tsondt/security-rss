Title: More than 4,000 individuals’ medical data left exposed for 16 years
Date: 2022-07-15T15:28:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-15-more-than-4000-individuals-medical-data-left-exposed-for-16-years

[Source](https://portswigger.net/daily-swig/more-than-4-000-individuals-medical-data-left-exposed-for-16-years){:target="_blank" rel="noopener"}

> Private healthcare information was accessible since 2006 [...]
