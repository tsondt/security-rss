Title: Calls for bans on Chinese CCTV makers Hikvision, Dahua expand
Date: 2022-07-05T16:30:06+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-07-05-calls-for-bans-on-chinese-cctv-makers-hikvision-dahua-expand

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/05/uk_ban_hikvision_dahua/){:target="_blank" rel="noopener"}

> UK wants to follow US move to stop sales from equipment manufacturers A group of politicians and lawmakers in the UK have backed a campaign to ban the sale of CCTV systems made by companies alleged to introduce potential security issues as well as being linked to human rights abuses in China.... [...]
