Title: Akamai blocked largest DDoS in Europe against one of its customers
Date: 2022-07-28T10:19:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-28-akamai-blocked-largest-ddos-in-europe-against-one-of-its-customers

[Source](https://www.bleepingcomputer.com/news/security/akamai-blocked-largest-ddos-in-europe-against-one-of-its-customers/){:target="_blank" rel="noopener"}

> The largest distributed denial-of-service (DDoS) attack that Europe has ever seen occurred earlier this month and hit an organization in Eastern Europe. [...]
