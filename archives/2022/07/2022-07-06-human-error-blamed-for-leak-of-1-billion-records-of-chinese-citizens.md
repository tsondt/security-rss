Title: Human Error Blamed for Leak of 1 Billion Records of Chinese Citizens
Date: 2022-07-06T10:33:35+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy
Slug: 2022-07-06-human-error-blamed-for-leak-of-1-billion-records-of-chinese-citizens

[Source](https://threatpost.com/hbillion-records/180125/){:target="_blank" rel="noopener"}

> A developer appears to have divulged credentials to a police database on a popular developer forum, leading to a breach and subsequent bid to sell 23 terabytes of personal data on the dark web. [...]
