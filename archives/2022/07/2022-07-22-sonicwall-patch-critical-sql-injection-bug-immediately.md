Title: SonicWall: Patch critical SQL injection bug immediately
Date: 2022-07-22T13:01:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-22-sonicwall-patch-critical-sql-injection-bug-immediately

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-patch-critical-sql-injection-bug-immediately/){:target="_blank" rel="noopener"}

> SonicWall has published a security advisory today to warn of a critical SQL injection flaw impacting the GMS (Global Management System) and Analytics On-Prem products. [...]
