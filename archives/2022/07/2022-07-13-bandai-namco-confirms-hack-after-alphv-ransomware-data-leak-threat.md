Title: Bandai Namco confirms hack after ALPHV ransomware data leak threat
Date: 2022-07-13T16:50:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2022-07-13-bandai-namco-confirms-hack-after-alphv-ransomware-data-leak-threat

[Source](https://www.bleepingcomputer.com/news/security/bandai-namco-confirms-hack-after-alphv-ransomware-data-leak-threat/){:target="_blank" rel="noopener"}

> Game publishing giant Bandai Namco has confirmed that they suffered a cyberattack that may have resulted in the theft of customers' personal data. [...]
