Title: A sneak peek at the governance, risk, and compliance sessions for AWS re:Inforce 2022
Date: 2022-07-11T20:14:28+00:00
Author: Greg Eppel
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Events;Security, Identity, & Compliance;Live Events;Security Blog
Slug: 2022-07-11-a-sneak-peek-at-the-governance-risk-and-compliance-sessions-for-aws-reinforce-2022

[Source](https://aws.amazon.com/blogs/security/a-sneak-peek-at-the-governance-risk-and-compliance-sessions-for-aws-reinforce-2022/){:target="_blank" rel="noopener"}

> Register now with discount code SALUZwmdkJJ to get $150 off your full conference pass to AWS re:Inforce. For a limited time only and while supplies last. Today we want to tell you about some of the exciting governance, risk, and compliance sessions planned for AWS re:Inforce 2022. AWS re:Inforce is a conference where you can [...]
