Title: Hackers scan for vulnerabilities within 15 minutes of disclosure
Date: 2022-07-26T15:44:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-26-hackers-scan-for-vulnerabilities-within-15-minutes-of-disclosure

[Source](https://www.bleepingcomputer.com/news/security/hackers-scan-for-vulnerabilities-within-15-minutes-of-disclosure/){:target="_blank" rel="noopener"}

> System administrators have even less time to patch disclosed security vulnerabilities than previously thought, as a new report shows threat actors scanning for vulnerable endpoints within 15 minutes of a new CVE being publicly disclosed. [...]
