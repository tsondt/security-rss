Title: Marriott hit by new data breach and a failed extortion attempt
Date: 2022-07-06T12:52:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-06-marriott-hit-by-new-data-breach-and-a-failed-extortion-attempt

[Source](https://www.bleepingcomputer.com/news/security/marriott-hit-by-new-data-breach-and-a-failed-extortion-attempt/){:target="_blank" rel="noopener"}

> Hotel giant Marriott International confirmed this week that it was hit by another data breach after an unknown threat actor managed to breach one of its properties and steal 20 GB worth of files. [...]
