Title: Older AMD, Intel chips vulnerable to data-leaking 'Retbleed' Spectre variant
Date: 2022-07-12T16:00:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-07-12-older-amd-intel-chips-vulnerable-to-data-leaking-retbleed-spectre-variant

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/12/amd_intel_retbleed/){:target="_blank" rel="noopener"}

> Speculative execution side-channels continue to haunt silicon world Older AMD and Intel chips are vulnerable to yet another Spectre-based speculative-execution attack that exposes secrets within kernel memory despite defenses already in place. Mitigating this side channel is expected to take a toll on performance.... [...]
