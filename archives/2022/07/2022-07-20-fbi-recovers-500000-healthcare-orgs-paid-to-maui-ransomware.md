Title: FBI recovers $500,000 healthcare orgs paid to Maui ransomware
Date: 2022-07-20T10:24:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2022-07-20-fbi-recovers-500000-healthcare-orgs-paid-to-maui-ransomware

[Source](https://www.bleepingcomputer.com/news/security/fbi-recovers-500-000-healthcare-orgs-paid-to-maui-ransomware/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice has announced the seizure of approximately $500,000 in Bitcoin, paid by American health care providers to the operators of the Maui ransomware strain. [...]
