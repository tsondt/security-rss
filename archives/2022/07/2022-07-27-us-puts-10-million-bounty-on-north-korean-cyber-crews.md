Title: US puts $10 million bounty on North Korean cyber-crews
Date: 2022-07-27T19:30:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-27-us-puts-10-million-bounty-on-north-korean-cyber-crews

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/27/north_korea_us_reward/){:target="_blank" rel="noopener"}

> Kim will be shaking in his shoes The US is offering up to $10 million for information on members of state-sponsored North Korean threat groups, double the amount that the State Department announced in April.... [...]
