Title: Cyber Europe 2022: EU completes large-scale cyber war game exercise
Date: 2022-07-01T13:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-01-cyber-europe-2022-eu-completes-large-scale-cyber-war-game-exercise

[Source](https://portswigger.net/daily-swig/cyber-europe-2022-eu-completes-large-scale-cyber-war-game-exercise){:target="_blank" rel="noopener"}

> Incident response and inter-agency capabilities road-tested [...]
