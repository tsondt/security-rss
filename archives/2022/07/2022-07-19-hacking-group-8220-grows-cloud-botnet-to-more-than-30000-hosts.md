Title: Hacking group '8220' grows cloud botnet to more than 30,000 hosts
Date: 2022-07-19T18:52:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-19-hacking-group-8220-grows-cloud-botnet-to-more-than-30000-hosts

[Source](https://www.bleepingcomputer.com/news/security/hacking-group-8220-grows-cloud-botnet-to-more-than-30-000-hosts/){:target="_blank" rel="noopener"}

> A cryptomining gang known as 8220 Gang has been exploiting Linux and cloud app vulnerabilities to grow their botnet to more than 30,000 infected hosts. [...]
