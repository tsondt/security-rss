Title: DDoS Attack Trends in 2022: Ultrashort, Powerful, Multivector Attacks
Date: 2022-07-27T10:08:48-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-27-ddos-attack-trends-in-2022-ultrashort-powerful-multivector-attacks

[Source](https://www.bleepingcomputer.com/news/security/ddos-attack-trends-in-2022-ultrashort-powerful-multivector-attacks/){:target="_blank" rel="noopener"}

> The political situation in Europe and the rest of the world has degraded dramatically in 2022. This has affected the nature, intensity, and geography of DDoS attacks, which have become actively used for political purposes. Find out more in this summary of G-Core Lab's latest DDoS Trends report. [...]
