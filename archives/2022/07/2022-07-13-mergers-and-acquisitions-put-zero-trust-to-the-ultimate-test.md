Title: Mergers and acquisitions put zero trust to the ultimate test
Date: 2022-07-13T17:00:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-13-mergers-and-acquisitions-put-zero-trust-to-the-ultimate-test

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/13/mergers-zero-trust-zscaler/){:target="_blank" rel="noopener"}

> Bypasses an arduous integration process with right security footing from the start When Jay Chaudhry launched Zscaler in 2007, he envisioned a number of use cases for the zero-trust platform, from security for a growing distributed, virtualized IT environment a nascent cloud computing environment to improved network visibility and identity governance.... [...]
