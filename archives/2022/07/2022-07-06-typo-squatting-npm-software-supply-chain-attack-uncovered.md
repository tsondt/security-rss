Title: Typo-squatting NPM software supply chain attack uncovered
Date: 2022-07-06T14:30:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-06-typo-squatting-npm-software-supply-chain-attack-uncovered

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/06/npm_supply_chain_attack/){:target="_blank" rel="noopener"}

> Beawre teh mizpelled pakcage naem Researchers at ReversingLabs have uncovered evidence of a widespread software supply chain attack through malicious JavaScript packages picked up via NPM.... [...]
