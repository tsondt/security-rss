Title: Authentication Risks Discovered in Okta Platform
Date: 2022-07-19T15:33:01+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Privacy;Web Security
Slug: 2022-07-19-authentication-risks-discovered-in-okta-platform

[Source](https://threatpost.com/risks-okta-sso/180249/){:target="_blank" rel="noopener"}

> Four newly discovered attack paths could lead to PII exposure, account takeover, even organizational data destruction. [...]
