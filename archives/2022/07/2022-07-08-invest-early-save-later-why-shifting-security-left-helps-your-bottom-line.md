Title: Invest early, save later: Why shifting security left helps your bottom line
Date: 2022-07-08T17:00:00+00:00
Author: Jeff Levine
Category: GCP Security
Tags: Infrastructure Modernization;Google Cloud;Identity & Security
Slug: 2022-07-08-invest-early-save-later-why-shifting-security-left-helps-your-bottom-line

[Source](https://cloud.google.com/blog/products/identity-security/shift-left-on-google-cloud-security-invest-now-save-later/){:target="_blank" rel="noopener"}

> Shifting left on security with Google Cloud infrastructure The concept of "shifting left" has been widely promoted in the software development lifecycle. The concept is that introducing security earlier, or leftwards, in the development process will lead to fewer software-related security defects later, or rightwards, in production. Shifting cloud security left can help identify potential misconfigurations earlier in the development cycle, which if unresolved can lead to security defects. Catching those misconfigurations early can improve the security posture of production deployments. Why shifting security left matters Google’s DevOps Research and Assessment (DORA) highlights the importance of integrating security into DevOps [...]
