Title: Free smartphone stalkerware detection tool gets dedicated hub
Date: 2022-07-03T11:12:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-03-free-smartphone-stalkerware-detection-tool-gets-dedicated-hub

[Source](https://www.bleepingcomputer.com/news/security/free-smartphone-stalkerware-detection-tool-gets-dedicated-hub/){:target="_blank" rel="noopener"}

> Kaspersky has launched a new information hub to help with their open-source stalkerware detection tool named TinyCheck, created in 2019 to help people detect if their devices are being monitored. [...]
