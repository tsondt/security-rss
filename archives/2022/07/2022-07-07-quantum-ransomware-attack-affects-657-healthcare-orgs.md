Title: Quantum ransomware attack affects 657 healthcare orgs
Date: 2022-07-07T13:19:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-07-quantum-ransomware-attack-affects-657-healthcare-orgs

[Source](https://www.bleepingcomputer.com/news/security/quantum-ransomware-attack-affects-657-healthcare-orgs/){:target="_blank" rel="noopener"}

> Professional Finance Company Inc. (PFC), a full-service accounts receivables management company, says that a ransomware attack in late February led to a data breach affecting over 600 healthcare organizations. [...]
