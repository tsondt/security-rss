Title: Providing our EU Education customers’ with data transfer protections to support privacy assessments
Date: 2022-07-22T20:00:00+00:00
Author: John Solomon
Category: GCP Security
Tags: Public Sector;Identity & Security
Slug: 2022-07-22-providing-our-eu-education-customers-with-data-transfer-protections-to-support-privacy-assessments

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-eu-education-data-transfer-protections-privacy-assessment/){:target="_blank" rel="noopener"}

> Recently, Datatilsynet (the Danish Data Protection Authority) issued a ruling emphasizing the importance of conducting proper due diligence before implementing cloud services. We agree due diligence is an important step for customers since privacy assessments and outcomes can vary significantly based on the way customers have configured their system. Although this ruling is limited to Helsingør Municipality, it may be of interest to other Danish controllers. To be clear, the ruling does not apply to Google directly or to other customers, nor does it prohibit use of Google Workspace for Education or Chromebooks in Denmark. It serves as a reminder [...]
