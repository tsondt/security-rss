Title: Billion-record stolen Chinese database for sale on breach forum
Date: 2022-07-05T06:04:18+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-05-billion-record-stolen-chinese-database-for-sale-on-breach-forum

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/05/shanghai_police_database_for_sell/){:target="_blank" rel="noopener"}

> Appears to have leaked from a cloud thanks to sloppy coding A threat actor has taken to a forum for news and discussion of data breaches with an offer to sell what they assert is a database containing records of over a billion Chinese civilians – allegedly stolen from the Shanghai Police.... [...]
