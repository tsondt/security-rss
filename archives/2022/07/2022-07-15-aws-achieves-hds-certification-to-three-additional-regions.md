Title: AWS achieves HDS certification to three additional Regions
Date: 2022-07-15T20:22:47+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;Auditing;AWS security;Compliance;HDS;Healthcare;Hébergeur de Données de Santé;Security;Security Blog
Slug: 2022-07-15-aws-achieves-hds-certification-to-three-additional-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-hds-certification-to-three-additional-regions/){:target="_blank" rel="noopener"}

> We’re excited to announce that three additional AWS Regions—Asia Pacific (Korea), Europe (London), and Europe (Stockholm)—have been granted the Health Data Hosting (Hébergeur de Données de Santé, HDS) certification. This alignment with the HDS requirements demonstrates our continued commitment to adhere to the heightened expectations for cloud service providers. AWS customers who handle personal health [...]
