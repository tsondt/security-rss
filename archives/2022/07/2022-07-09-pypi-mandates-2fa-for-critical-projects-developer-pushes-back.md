Title: PyPI mandates 2FA for critical projects, developer pushes back
Date: 2022-07-09T12:31:57-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-07-09-pypi-mandates-2fa-for-critical-projects-developer-pushes-back

[Source](https://www.bleepingcomputer.com/news/security/pypi-mandates-2fa-for-critical-projects-developer-pushes-back/){:target="_blank" rel="noopener"}

> On Friday, the Python Package Index (PyPI), repository of open source Python projects announced plans to rollout two factor authentication for maintainers of "critical" projects. Although many praised the move, the developer of a popular Python project decided to delete his code from PyPI in retaliation. [...]
