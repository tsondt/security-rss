Title: Cyberattack shuts down unemployment, labor websites across the US
Date: 2022-07-01T20:41:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-01-cyberattack-shuts-down-unemployment-labor-websites-across-the-us

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/01/gsi-cyberattack-state-unemployment/){:target="_blank" rel="noopener"}

> Software maker GSI took systems offline, affecting thousands of people in as many as 40 states A cyberattack on a software company almost a week ago continues to ripple through labor and workforce agencies in a number of US states, cutting off people from such services as unemployment benefits and job-seeking programs.... [...]
