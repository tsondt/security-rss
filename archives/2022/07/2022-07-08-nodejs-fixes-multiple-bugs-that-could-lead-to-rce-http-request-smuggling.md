Title: Node.js fixes multiple bugs that could lead to RCE, HTTP request smuggling
Date: 2022-07-08T13:21:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-08-nodejs-fixes-multiple-bugs-that-could-lead-to-rce-http-request-smuggling

[Source](https://portswigger.net/daily-swig/node-js-fixes-multiple-bugs-that-could-lead-to-rce-http-request-smuggling){:target="_blank" rel="noopener"}

> All security issues have been patched – update now [...]
