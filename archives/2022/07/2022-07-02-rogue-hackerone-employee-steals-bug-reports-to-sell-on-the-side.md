Title: Rogue HackerOne employee steals bug reports to sell on the side
Date: 2022-07-02T11:36:48-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-07-02-rogue-hackerone-employee-steals-bug-reports-to-sell-on-the-side

[Source](https://www.bleepingcomputer.com/news/security/rogue-hackerone-employee-steals-bug-reports-to-sell-on-the-side/){:target="_blank" rel="noopener"}

> A HackerOne employee stole vulnerability reports submitted through the bug bounty platform and disclosed them to affected customers to claim financial rewards. [...]
