Title: Online programming IDEs can be used to launch remote cyberattacks
Date: 2022-07-07T10:26:41-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-07-07-online-programming-ides-can-be-used-to-launch-remote-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/online-programming-ides-can-be-used-to-launch-remote-cyberattacks/){:target="_blank" rel="noopener"}

> Security researchers are warning that hackers can abuse online programming learning platforms to remotely launch cyberattacks, steal data, and scan for vulnerable devices, simply by using a web browser. [...]
