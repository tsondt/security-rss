Title: Microsoft's latest security patch troubles Windows 11 users
Date: 2022-07-18T14:00:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-18-microsofts-latest-security-patch-troubles-windows-11-users

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/18/windows_11_patch_problems/){:target="_blank" rel="noopener"}

> The curse of Patch Tuesday strikes again as error codes wreak minor havoc Complaints over Microsoft's latest patch Tuesday have intensified after some Windows 11 users found their systems worse for wear following installation.... [...]
