Title: Apple’s Lockdown Mode
Date: 2022-07-08T14:18:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;cybersecurity;iOS;iPhone;spyware
Slug: 2022-07-08-apples-lockdown-mode

[Source](https://www.schneier.com/blog/archives/2022/07/apples-lockdown-mode.html){:target="_blank" rel="noopener"}

> Apple has introduced lockdown mode for high-risk users who are concerned about nation-state attacks. It trades reduced functionality for increased security in a very interesting way. [...]
