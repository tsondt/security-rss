Title: Malicious Android apps with 300K installs found on Google Play
Date: 2022-07-19T14:19:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-07-19-malicious-android-apps-with-300k-installs-found-on-google-play

[Source](https://www.bleepingcomputer.com/news/security/malicious-android-apps-with-300k-installs-found-on-google-play/){:target="_blank" rel="noopener"}

> Cybersecurity researchers have discovered three Android malware families infiltrating the Google Play Store, hiding their malicious payloads inside many seemingly innocuous applications. [...]
