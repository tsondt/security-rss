Title: Hack Allows Drone Takeover Via ‘ExpressLRS’ Protocol
Date: 2022-07-07T11:31:31+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Hacks;Vulnerabilities
Slug: 2022-07-07-hack-allows-drone-takeover-via-expresslrs-protocol

[Source](https://threatpost.com/drone-hack-expresslrs-hijacked/180133/){:target="_blank" rel="noopener"}

> A radio control system for drones is vulnerable to remote takeover, thanks to a weakness in the mechanism that binds transmitter and receiver. [...]
