Title: How to spot your biggest security threat? Just look out for the humans
Date: 2022-07-05T15:18:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-07-05-how-to-spot-your-biggest-security-threat-just-look-out-for-the-humans

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/05/how_to_spot_your_biggest/){:target="_blank" rel="noopener"}

> If you’re wondering why, here’s a primer Sponsored Post How would you describe the biggest security threat to your organization? Perhaps you envision a faceless cybercrime syndicate or hostile state. Or a humming botnet, remorselessly probing your systems.... [...]
