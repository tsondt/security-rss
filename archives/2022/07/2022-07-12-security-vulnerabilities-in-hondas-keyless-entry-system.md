Title: Security Vulnerabilities in Honda’s Keyless Entry System
Date: 2022-07-12T12:23:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;keys;locks;reports
Slug: 2022-07-12-security-vulnerabilities-in-hondas-keyless-entry-system

[Source](https://www.schneier.com/blog/archives/2022/07/security-vulnerabilities-in-hondas-keyless-entry-system.html){:target="_blank" rel="noopener"}

> Honda vehicles from 2021 to 2022 are vulnerable to this attack : On Thursday, a security researcher who goes by Kevin2600 published a technical report and videos on a vulnerability that he claims allows anyone armed with a simple hardware device to steal the code to unlock Honda vehicles. Kevin2600, who works for cybersecurity firm Star-V Lab, dubbed the attack RollingPWN. [...] In a phone call, Kevin2600 explained that the attack relies on a weakness that allows someone using a software defined radio— such as HackRF —to capture the code that the car owner uses to open the car, and [...]
