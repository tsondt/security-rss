Title: Spring Data MongoDB hit by another critical SpEL injection flaw
Date: 2022-07-05T15:50:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-05-spring-data-mongodb-hit-by-another-critical-spel-injection-flaw

[Source](https://portswigger.net/daily-swig/spring-data-mongodb-hit-by-another-critical-spel-injection-flaw){:target="_blank" rel="noopener"}

> Bug mirrors recent SpEL injection vulnerability that emerged alongside ‘SpringShell’ issue [...]
