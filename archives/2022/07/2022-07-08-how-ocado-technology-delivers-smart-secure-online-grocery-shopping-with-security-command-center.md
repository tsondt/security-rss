Title: How Ocado Technology delivers smart, secure online grocery shopping with Security Command Center
Date: 2022-07-08T16:30:00+00:00
Author: Piotr Jakielarz
Category: GCP Security
Tags: Customers;Google Cloud in Europe;Google Cloud;Identity & Security
Slug: 2022-07-08-how-ocado-technology-delivers-smart-secure-online-grocery-shopping-with-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/how-ocado-technology-delivers-online-grocery-shopping-securely-with-google-cloud/){:target="_blank" rel="noopener"}

> Grocery shopping has changed for good and Ocado Group has played a major role in this transformation. We started as an online supermarket, applying technology and automation to revolutionise the online grocery space. Today, after two decades of innovation, we are a global technology company providing state-of-the-art software, robotics, and AI solutions for online grocery. We created the Ocado Smart Platform, which powers the online operations of some of the world's most forward-thinking grocery retailers, from Kroger in the U.S. to Coles in Australia. Grocery shopping has changed for good and Ocado Group has played a major role in this [...]
