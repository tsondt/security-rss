Title: GitHub introduces 2FA and quality of life improvements for npm
Date: 2022-07-27T10:29:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-27-github-introduces-2fa-and-quality-of-life-improvements-for-npm

[Source](https://www.bleepingcomputer.com/news/security/github-introduces-2fa-and-quality-of-life-improvements-for-npm/){:target="_blank" rel="noopener"}

> GitHub has announced the general availability of three significant improvements to npm (Node Package Manager), aiming to make using the software more secure and manageable. [...]
