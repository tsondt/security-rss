Title: W3C launches Decentralized Identifiers as a web standard
Date: 2022-07-20T12:53:01+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-20-w3c-launches-decentralized-identifiers-as-a-web-standard

[Source](https://portswigger.net/daily-swig/w3c-launches-decentralized-identifiers-as-a-web-standard){:target="_blank" rel="noopener"}

> DID has been designed to give users and organizations greater security and privacy [...]
