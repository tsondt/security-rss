Title: A Retrospective on the 2015 Ashley Madison Breach
Date: 2022-07-27T01:04:51+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ashley Madison breach;ashley madison breach;Avid Life Media;Flashpoint;Impact Team;Intel 471;Noel Biderman;Per Thorsheim;Robert Graham;wired.com
Slug: 2022-07-27-a-retrospective-on-the-2015-ashley-madison-breach

[Source](https://krebsonsecurity.com/2022/07/a-retrospective-on-the-2015-ashley-madison-breach/){:target="_blank" rel="noopener"}

> It’s been seven years since the online cheating site AshleyMadison.com was hacked and highly sensitive data about its users posted online. The leak led to the public shaming and extortion of many Ashley Madison users, and to at least two suicides. To date, little is publicly known about the perpetrators or the true motivation for the attack. But a recent review of Ashley Madison mentions across Russian cybercrime forums and far-right websites in the months leading up to the hack revealed some previously unreported details that may deserve further scrutiny. As first reported by KrebsOnSecurity on July 19, 2015, a [...]
