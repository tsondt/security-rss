Title: How Google is preparing for a post-quantum world
Date: 2022-07-06T18:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-07-06-how-google-is-preparing-for-a-post-quantum-world

[Source](https://cloud.google.com/blog/products/identity-security/how-google-is-preparing-for-a-post-quantum-world/){:target="_blank" rel="noopener"}

> The National Institute of Standards and Technology (NIST) on Tuesday announced the completion of the third round of the Post-Quantum Cryptography (PQC) standardization process, and we are pleased to share that a submission (SPHINCS+) with Google’s involvement was selected for standardization. Two submissions (Classic McEliece, BIKE) are being considered for the next round. We want to congratulate the Googlers involved in the submissions (Stefan Kölbl, Rafael Misoczki, and Christiane Peters) and thank Sophie Schmieg for moving PQC efforts forward at Google. We would also like to congratulate all the participants and thank NIST for their dedication to advancing these important [...]
