Title: AstraLocker ransomware shuts down and releases decryptors
Date: 2022-07-04T14:15:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-04-astralocker-ransomware-shuts-down-and-releases-decryptors

[Source](https://www.bleepingcomputer.com/news/security/astralocker-ransomware-shuts-down-and-releases-decryptors/){:target="_blank" rel="noopener"}

> The threat actor behind the lesser-known AstraLocker ransomware told BleepingComputer they're shutting down the operation and plan to switch to cryptojacking. [...]
