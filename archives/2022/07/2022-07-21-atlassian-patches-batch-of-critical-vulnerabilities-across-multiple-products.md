Title: Atlassian patches batch of critical vulnerabilities across multiple products
Date: 2022-07-21T15:17:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-21-atlassian-patches-batch-of-critical-vulnerabilities-across-multiple-products

[Source](https://portswigger.net/daily-swig/atlassian-patches-batch-of-critical-vulnerabilities-across-multiple-products){:target="_blank" rel="noopener"}

> Jira, Bamboo, Bitbucket, Confluence, Fisheye/Crucible, and Questions for Confluence affected [...]
