Title: Take the day off: Windows Autopatch is live and can even fix cloudy PCs
Date: 2022-07-12T06:03:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-12-take-the-day-off-windows-autopatch-is-live-and-can-even-fix-cloudy-pcs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/12/windows_auopatch_live/){:target="_blank" rel="noopener"}

> But first, there's a whole lot of AD and Intune prep to be done Microsoft's promised service to enable automatic patching of Windows has gone live.... [...]
