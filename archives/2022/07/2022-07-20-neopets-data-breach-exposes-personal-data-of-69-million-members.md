Title: Neopets data breach exposes personal data of 69 million members
Date: 2022-07-20T18:45:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-20-neopets-data-breach-exposes-personal-data-of-69-million-members

[Source](https://www.bleepingcomputer.com/news/security/neopets-data-breach-exposes-personal-data-of-69-million-members/){:target="_blank" rel="noopener"}

> Virtual pet website Neopets has suffered a data breach leading to the theft of source code and a database containing the personal information of over 69 million members. [...]
