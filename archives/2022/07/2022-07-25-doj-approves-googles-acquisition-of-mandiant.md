Title: DoJ approves Google's acquisition of Mandiant
Date: 2022-07-25T15:00:04+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-07-25-doj-approves-googles-acquisition-of-mandiant

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/25/security_in_brief/){:target="_blank" rel="noopener"}

> Plus: Ukrainian fake news and Uber admits covering up data breach In Brief Google's legally fraught journey to buy cybersecurity business Mandiant is in its final stretch, with the US Department of Justice closing its investigation and giving the go-ahead for the sale to proceed.... [...]
