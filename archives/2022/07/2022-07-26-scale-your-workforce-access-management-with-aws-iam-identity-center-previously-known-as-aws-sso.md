Title: Scale your workforce access management with AWS IAM Identity Center (previously known as AWS SSO)
Date: 2022-07-26T14:15:04+00:00
Author: Ron Cully
Category: AWS Security
Tags: Best Practices;Foundational (100);Security, Identity, & Compliance;AWS Identity and Access Management;AWS SSO;IAM;Security Blog;Single sign-on
Slug: 2022-07-26-scale-your-workforce-access-management-with-aws-iam-identity-center-previously-known-as-aws-sso

[Source](https://aws.amazon.com/blogs/security/scale-your-workforce-access-management-with-aws-iam-identity-center-previously-known-as-aws-sso/){:target="_blank" rel="noopener"}

> AWS Single Sign-On (AWS SSO) is now AWS IAM Identity Center. Amazon Web Services (AWS) is changing the name to highlight the service’s foundation in AWS Identity and Access Management (IAM), to better reflect its full set of capabilities, and to reinforce its recommended role as the central place to manage access across AWS accounts [...]
