Title: JPMorgan, UBS among trio accused of shoddy ID theft protection
Date: 2022-07-28T21:59:41+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-28-jpmorgan-ubs-among-trio-accused-of-shoddy-id-theft-protection

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/28/jpmorgan_sec_fine/){:target="_blank" rel="noopener"}

> SEC extracts pocket change from bankers, wags finger, sends them on their way JPMorgan Securities, UBS Financial Services, and TradeStation Securities aren't doing enough to thwart crooks who want to steal customers' identity, says America's financial watchdog.... [...]
