Title: Popular NFT Marketplace Phished for $540M
Date: 2022-07-11T20:06:10+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Cryptography;Hacks
Slug: 2022-07-11-popular-nft-marketplace-phished-for-540m

[Source](https://threatpost.com/popular-nft-marketplace-phished-for-540m/180174/){:target="_blank" rel="noopener"}

> In March, a North Korean APT siphoned blockchain gaming platform Axie Infinity of $540M. [...]
