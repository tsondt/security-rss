Title: Alibaba affiliate Ant Group open sources its privacy software and a 'Secure Processing Unit'
Date: 2022-07-05T10:30:58+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-05-alibaba-affiliate-ant-group-open-sources-its-privacy-software-and-a-secure-processing-unit

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/05/ant_group_open_source/){:target="_blank" rel="noopener"}

> Well, that's one way to ensure it is more widely used... Alibaba's financial services affiliate, Ant Group, has open sourced its "privacy-preserving Computation Framework."... [...]
