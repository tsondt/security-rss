Title: Infosec not your job but your responsibility? How to be smarter than the average bear
Date: 2022-07-25T11:27:07+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2022-07-25-infosec-not-your-job-but-your-responsibility-how-to-be-smarter-than-the-average-bear

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/25/infosec_not_your_job/){:target="_blank" rel="noopener"}

> Many of last week's security stories tell the same tale Opinion The calls are coming from inside the house! Lately, Outlook users have been getting their own version of this classic urban horror myth. The email system is alerting them to suspicious activity on their accounts, and helpfully providing the IP addresses responsible.... [...]
