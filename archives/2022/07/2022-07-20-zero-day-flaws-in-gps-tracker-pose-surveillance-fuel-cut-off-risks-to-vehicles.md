Title: Zero-day flaws in GPS tracker pose surveillance, fuel cut-off risks to vehicles
Date: 2022-07-20T16:18:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-20-zero-day-flaws-in-gps-tracker-pose-surveillance-fuel-cut-off-risks-to-vehicles

[Source](https://portswigger.net/daily-swig/zero-day-flaws-in-gps-tracker-pose-surveillance-fuel-cut-off-risks-to-vehicles){:target="_blank" rel="noopener"}

> Broader architectural failings of Chinese vendor potentially puts 1.5m devices at risk [...]
