Title: Microsoft SQL servers hacked to steal bandwidth for proxy services
Date: 2022-07-28T13:26:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2022-07-28-microsoft-sql-servers-hacked-to-steal-bandwidth-for-proxy-services

[Source](https://www.bleepingcomputer.com/news/security/microsoft-sql-servers-hacked-to-steal-bandwidth-for-proxy-services/){:target="_blank" rel="noopener"}

> Threat actors are generating revenue by using adware bundles, malware, or even hacking into Microsoft SQL servers, to convert devices into proxies that are rented through online proxy services. [...]
