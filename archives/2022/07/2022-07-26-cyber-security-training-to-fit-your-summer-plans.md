Title: Cyber security training to fit your summer plans
Date: 2022-07-26T14:21:14+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-07-26-cyber-security-training-to-fit-your-summer-plans

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/26/cyber_security_training_to_fit/){:target="_blank" rel="noopener"}

> A flexible approach to cyber security training and certification from SANS & GIAC Sponsored Post Keeping the world safe from cyber threats requires both passion and skills. And you can grow both with training that makes you battle-ready as soon as you leave the classroom.... [...]
