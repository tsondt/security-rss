Title: New Luna ransomware encrypts Windows, Linux, and ESXi systems
Date: 2022-07-20T05:32:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-20-new-luna-ransomware-encrypts-windows-linux-and-esxi-systems

[Source](https://www.bleepingcomputer.com/news/security/new-luna-ransomware-encrypts-windows-linux-and-esxi-systems/){:target="_blank" rel="noopener"}

> A new ransomware family dubbed Luna can be used to encrypt devices running several operating systems, including Windows, Linux, and ESXi systems. [...]
