Title: Someone may be prepping an NPM crypto-mining spree
Date: 2022-07-07T17:55:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-07-someone-may-be-prepping-an-npm-crypto-mining-spree

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/07/npm-cryptomining-attack/){:target="_blank" rel="noopener"}

> 1,300 packages from 1,000 automated user accounts set the stage for something big A burst of almost 1,300 JavaScript packages automatically created on NPM via more than 1,000 user accounts could be the initial step in a major crypto-mining campaign, according to researchers at Checkmarx.... [...]
