Title: Microsoft fixes dozens of Azure Site Recovery privilege escalation bugs
Date: 2022-07-12T18:19:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-12-microsoft-fixes-dozens-of-azure-site-recovery-privilege-escalation-bugs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-dozens-of-azure-site-recovery-privilege-escalation-bugs/){:target="_blank" rel="noopener"}

> Microsoft has fixed 32 vulnerabilities in the Azure Site Recovery suite that could have allowed attackers to gain elevated privileges or perform remote code execution. [...]
