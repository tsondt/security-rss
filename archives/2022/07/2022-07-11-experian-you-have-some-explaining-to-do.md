Title: Experian, You Have Some Explaining to Do
Date: 2022-07-11T04:07:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;Arthur Rishi;Emory Roan;Equifax;Experian;John Turner;Nicholas Weaver;Privacyrights.org;TransUnion
Slug: 2022-07-11-experian-you-have-some-explaining-to-do

[Source](https://krebsonsecurity.com/2022/07/experian-you-have-some-explaining-to-do/){:target="_blank" rel="noopener"}

> Twice in the past month KrebsOnSecurity has heard from readers who’ve had their accounts at big-three credit bureau Experian hacked and updated with a new email address that wasn’t theirs. In both cases the readers used password managers to select strong, unique passwords for their Experian accounts. Research suggests identity thieves were able to hijack the accounts simply by signing up for new accounts at Experian using the victim’s personal information and a different email address. John Turner is a software engineer based in Salt Lake City. Turner said he created the account at Experian in 2020 to place a [...]
