Title: Chromium's WebRTC zero-day fix arrives in Microsoft Edge
Date: 2022-07-07T16:00:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-07-chromiums-webrtc-zero-day-fix-arrives-in-microsoft-edge

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/07/edge_cves/){:target="_blank" rel="noopener"}

> Update addresses heap buffer overflow and type confusion bugs in Google's browser engine Microsoft has followed Google's lead and issued an update for its Edge browser following the arrival of a WebRTC zero-day.... [...]
