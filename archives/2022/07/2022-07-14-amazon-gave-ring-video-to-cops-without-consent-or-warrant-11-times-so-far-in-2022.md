Title: Amazon gave Ring video to cops without consent or warrant 11 times so far in 2022
Date: 2022-07-14T13:45:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-14-amazon-gave-ring-video-to-cops-without-consent-or-warrant-11-times-so-far-in-2022

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/14/amazon_gave_police_unauthorized_doorbell/){:target="_blank" rel="noopener"}

> Got no time for that red tape in an emergency, says exec Updated Amazon's home security wing Ring turned over footage to US law enforcement without permission from the devices' owners and seemingly without a warrant 11 times so far in 2022.... [...]
