Title: Belgium says Chinese cyber gangs attacked its government and military
Date: 2022-07-20T03:15:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-20-belgium-says-chinese-cyber-gangs-attacked-its-government-and-military

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/20/belgium_alleges_china_apt_attacks/){:target="_blank" rel="noopener"}

> China, as usual, says it just wants a peaceful and prosperous internet The government of Belgium has claimed it detected three Chinese Advanced Persistent Threat actors attacking its public service and defence forces.... [...]
