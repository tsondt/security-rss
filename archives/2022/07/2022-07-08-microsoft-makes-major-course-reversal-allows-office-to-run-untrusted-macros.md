Title: Microsoft makes major course reversal, allows Office to run untrusted macros
Date: 2022-07-08T18:20:14+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;macros;microsoft;office
Slug: 2022-07-08-microsoft-makes-major-course-reversal-allows-office-to-run-untrusted-macros

[Source](https://arstechnica.com/?p=1865183){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Microsoft has stunned core parts of the security community with a decision to quietly reverse course and allow untrusted macros to be opened by default in Word and other Office applications. In February, the software maker announced a major change it said it enacted to combat the growing scourge of ransomware and other malware attacks. Going forward, macros downloaded from the Internet would be disabled entirely by default. Whereas previously, Office provided alert banners that could be disregarded with the click of a button, the new warnings would provide no such way to enable the macros. [...]
