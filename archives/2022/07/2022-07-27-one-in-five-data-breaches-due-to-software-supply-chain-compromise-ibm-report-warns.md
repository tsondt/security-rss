Title: One in five data breaches due to software supply chain compromise, IBM report warns
Date: 2022-07-27T15:04:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-27-one-in-five-data-breaches-due-to-software-supply-chain-compromise-ibm-report-warns

[Source](https://portswigger.net/daily-swig/one-in-five-data-breaches-due-to-software-supply-chain-compromise-ibm-report-warns){:target="_blank" rel="noopener"}

> Attack vector cost businesses 2.5% more in one year [...]
