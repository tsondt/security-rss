Title: PayPal phishing kit added to hacked WordPress sites for full ID theft
Date: 2022-07-14T14:09:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-14-paypal-phishing-kit-added-to-hacked-wordpress-sites-for-full-id-theft

[Source](https://www.bleepingcomputer.com/news/security/paypal-phishing-kit-added-to-hacked-wordpress-sites-for-full-id-theft/){:target="_blank" rel="noopener"}

> A newly discovered phishing kit targeting PayPal users is trying to steal a large set of personal information from victims that includes government identification documents and photos. [...]
