Title: Amazon sues 10,000 Facebook Group admins for offering fake reviews
Date: 2022-07-20T06:33:12+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-07-20-amazon-sues-10000-facebook-group-admins-for-offering-fake-reviews

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/20/amazon_facebook_reviews/){:target="_blank" rel="noopener"}

> Good luck deciding which toxic monopolist deserves your sympathy in this fight Amazon is suing over 10,000 administrators of Facebook groups that offer to post fake reviews on the online souk's website in exchange for products and money.... [...]
