Title: Microsoft quietly fixes ShadowCoerce Windows NTLM Relay bug
Date: 2022-07-05T12:17:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-05-microsoft-quietly-fixes-shadowcoerce-windows-ntlm-relay-bug

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-quietly-fixes-shadowcoerce-windows-ntlm-relay-bug/){:target="_blank" rel="noopener"}

> Microsoft has confirmed it fixed a previously disclosed 'ShadowCoerce' vulnerability as part of the June 2022 updates that enabled attackers to target Windows servers in NTLM relay attacks. [...]
