Title: AWS re:Inforce 2022: Network & Infrastructure Security track preview
Date: 2022-07-22T17:39:34+00:00
Author: Satinder Khasriya
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS re:Inforce;Network and infrastructure security;Network security;Security Blog
Slug: 2022-07-22-aws-reinforce-2022-network-infrastructure-security-track-preview

[Source](https://aws.amazon.com/blogs/security/aws-reinforce-2022-network-infrastructure-security-track-preview/){:target="_blank" rel="noopener"}

> Register now with discount code SALvWQHU2Km to get $150 off your full conference pass to AWS re:Inforce. For a limited time only and while supplies last. Today we’re going to highlight just some of the network and infrastructure security focused sessions planned for AWS re:Inforce. AWS re:Inforce 2022 will take place in-person in Boston, MA [...]
