Title: Charter told to pay $7.3b in damages after cable installer murders grandmother
Date: 2022-07-27T00:54:07+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: 2022-07-27-charter-told-to-pay-73b-in-damages-after-cable-installer-murders-grandmother

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/27/charter_spectrum_murder_damages/){:target="_blank" rel="noopener"}

> Broadband giant says it will appeal jury verdict in negligence case Charter Communications must pay out $7 billion in damages after one of its Spectrum cable technicians robbed and killed an elderly woman, a jury decided Tuesday.... [...]
