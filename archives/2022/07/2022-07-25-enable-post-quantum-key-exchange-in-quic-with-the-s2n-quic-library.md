Title: Enable post-quantum key exchange in QUIC with the s2n-quic library
Date: 2022-07-25T19:18:33+00:00
Author: Panos Kampanakis
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Cryptography library;Encryption;post-quantum QUIC;QUIC;QUIC protocol;s2n-quic;Security Blog
Slug: 2022-07-25-enable-post-quantum-key-exchange-in-quic-with-the-s2n-quic-library

[Source](https://aws.amazon.com/blogs/security/enable-post-quantum-key-exchange-in-quic-with-the-s2n-quic-library/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS) we prioritize security, performance, and strong encryption in our cloud services. In order to be prepared for quantum computer advancements, we’ve been investigating the use of quantum-safe algorithms for key exchange in the TLS protocol. In this blog post, we’ll first bring you up to speed on what we’ve been [...]
