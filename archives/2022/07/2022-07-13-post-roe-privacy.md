Title: Post-Roe Privacy
Date: 2022-07-13T11:00:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;medicine;privacy;surveillance
Slug: 2022-07-13-post-roe-privacy

[Source](https://www.schneier.com/blog/archives/2022/07/post-roe-privacy.html){:target="_blank" rel="noopener"}

> This is an excellent essay outlining the post-Roe privacy threat model. (Summary: period tracking apps are largely a red herring.) Taken together, this means the primary digital threat for people who take abortion pills is the actual evidence of intention stored on your phone, in the form of texts, emails, and search/web history. Cynthia Conti-Cook’s incredible article “ Surveilling the Digital Abortion Diary details what we know now about how digital evidence has been used to prosecute women who have been pregnant. That evidence includes search engine history, as in the case of the prosecution of Latice Fisher in Mississippi. [...]
