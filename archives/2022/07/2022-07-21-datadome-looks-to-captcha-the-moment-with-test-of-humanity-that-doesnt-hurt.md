Title: DataDome looks to CAPTCHA the moment with test of humanity that doesn't hurt
Date: 2022-07-21T12:15:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-21-datadome-looks-to-captcha-the-moment-with-test-of-humanity-that-doesnt-hurt

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/21/datadome-captcha-verification/){:target="_blank" rel="noopener"}

> As the verification technology weathers ongoing criticism from users, one anti-bot security vendor rolls out its own tool Apple last month gave hope to a large segment of the mobile device-using population when it announced that the upcoming iOS 16 operating system will eliminate the requirement to use CAPTCHAs to verify their humanity before accessing a website.... [...]
