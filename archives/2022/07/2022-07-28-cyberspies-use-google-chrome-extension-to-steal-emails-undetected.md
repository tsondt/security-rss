Title: Cyberspies use Google Chrome extension to steal emails undetected
Date: 2022-07-28T11:10:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-28-cyberspies-use-google-chrome-extension-to-steal-emails-undetected

[Source](https://www.bleepingcomputer.com/news/security/cyberspies-use-google-chrome-extension-to-steal-emails-undetected/){:target="_blank" rel="noopener"}

> A North Korean-backed threat group tracked as Kimsuky is using a malicious browser extension to steal emails from Google Chrome or Microsoft Edge users reading their webmail. [...]
