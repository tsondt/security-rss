Title: Belgium says Chinese hackers attacked its Ministry of Defense
Date: 2022-07-19T10:44:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-19-belgium-says-chinese-hackers-attacked-its-ministry-of-defense

[Source](https://www.bleepingcomputer.com/news/security/belgium-says-chinese-hackers-attacked-its-ministry-of-defense/){:target="_blank" rel="noopener"}

> The Minister for Foreign Affairs of Belgium says multiple Chinese state-backed threat groups targeted the country's defense and interior ministries. [...]
