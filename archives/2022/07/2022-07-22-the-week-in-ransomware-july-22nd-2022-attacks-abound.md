Title: The Week in Ransomware - July 22nd 2022 - Attacks abound
Date: 2022-07-22T23:52:14-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-22-the-week-in-ransomware-july-22nd-2022-attacks-abound

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-july-22nd-2022-attacks-abound/){:target="_blank" rel="noopener"}

> New ransomware operations continue to be launched this week, with the new Luna ransomware found to be targeting both Windows and VMware ESXi servers. [...]
