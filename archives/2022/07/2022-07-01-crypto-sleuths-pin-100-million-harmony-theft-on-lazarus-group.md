Title: Crypto sleuths pin $100 million Harmony theft on Lazarus Group
Date: 2022-07-01T18:11:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-01-crypto-sleuths-pin-100-million-harmony-theft-on-lazarus-group

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/01/lazarus-crypto-hack-harmony/){:target="_blank" rel="noopener"}

> Elliptic points to several indicators that suggest the North Korea-linked gang was behind the hack Investigators at a blockchain analysis outfit have linked the theft of $100 million in crypto assets last week to the notorious North Korean-based cybercrime group Lazarus. The company said it had tracked the movement of some of the stolen cryptocurrency to a so-called mixer used to launder such ill-gotten funds.... [...]
