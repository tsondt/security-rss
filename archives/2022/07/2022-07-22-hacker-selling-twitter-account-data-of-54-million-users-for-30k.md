Title: Hacker selling Twitter account data of 5.4 million users for $30k
Date: 2022-07-22T18:00:35-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-22-hacker-selling-twitter-account-data-of-54-million-users-for-30k

[Source](https://www.bleepingcomputer.com/news/security/hacker-selling-twitter-account-data-of-54-million-users-for-30k/){:target="_blank" rel="noopener"}

> Twitter has suffered a data breach after threat actors used a vulnerability to build a database of phone numbers and email addresses belonging to 5.4 million accounts, with the data now up for sale on a hacker forum for $30,000. [...]
