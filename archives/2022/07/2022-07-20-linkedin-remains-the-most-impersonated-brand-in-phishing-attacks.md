Title: LinkedIn remains the most impersonated brand in phishing attacks
Date: 2022-07-20T11:36:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-20-linkedin-remains-the-most-impersonated-brand-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/linkedin-remains-the-most-impersonated-brand-in-phishing-attacks/){:target="_blank" rel="noopener"}

> LinkedIn is holding the top spot for the most impersonated brand in phishing campaigns observed during the second quarter of 2022. [...]
