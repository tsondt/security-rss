Title: Russian hackers use fake DDoS app to infect pro-Ukrainian activists
Date: 2022-07-19T13:06:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-19-russian-hackers-use-fake-ddos-app-to-infect-pro-ukrainian-activists

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-use-fake-ddos-app-to-infect-pro-ukrainian-activists/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group (TAG), whose primary goal is to defend Google users from state-sponsored attacks, said today that Russian-backed threat groups are still focusing their attacks on Ukrainian organizations. [...]
