Title: Spain arrests suspected hackers who sabotaged radiation alert system
Date: 2022-07-27T13:05:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-27-spain-arrests-suspected-hackers-who-sabotaged-radiation-alert-system

[Source](https://www.bleepingcomputer.com/news/security/spain-arrests-suspected-hackers-who-sabotaged-radiation-alert-system/){:target="_blank" rel="noopener"}

> The Spanish police have announced the arrest of two hackers believed to be responsible for cyberattacks on the country's radioactivity alert network (RAR), which took place between March and June 2021. [...]
