Title: Elastix VoIP systems hacked in massive campaign to install PHP web shells
Date: 2022-07-16T10:11:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-16-elastix-voip-systems-hacked-in-massive-campaign-to-install-php-web-shells

[Source](https://www.bleepingcomputer.com/news/security/elastix-voip-systems-hacked-in-massive-campaign-to-install-php-web-shells/){:target="_blank" rel="noopener"}

> Threat analysts have uncovered a large-scale campaign targeting Elastix VoIP telephony servers with more than 500,000 malware samples over a period of three months. [...]
