Title: Fortinet patch batch remedies multiple path traversal vulnerabilities
Date: 2022-07-07T12:47:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-07-fortinet-patch-batch-remedies-multiple-path-traversal-vulnerabilities

[Source](https://portswigger.net/daily-swig/fortinet-patch-batch-remedies-multiple-path-traversal-vulnerabilities){:target="_blank" rel="noopener"}

> Four high, six medium, and one low severity issue fixed [...]
