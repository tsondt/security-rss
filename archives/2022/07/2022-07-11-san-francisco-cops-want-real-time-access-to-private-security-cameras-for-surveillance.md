Title: San Francisco cops want real-time access to private security cameras for surveillance
Date: 2022-07-11T23:24:36+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-11-san-francisco-cops-want-real-time-access-to-private-security-cameras-for-surveillance

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/11/san_francisco_police_private_security_cameras/){:target="_blank" rel="noopener"}

> ACLU hits back at 'unprecedented power grab' San Francisco lawmakers are mulling a proposed law that would allow police to use private security cameras – think: those in residential doorbells, medical clinics, and retail shops – in real time for surveillance purposes.... [...]
