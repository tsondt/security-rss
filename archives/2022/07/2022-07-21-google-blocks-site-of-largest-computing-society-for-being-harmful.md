Title: Google blocks site of largest computing society for being ‘harmful’
Date: 2022-07-21T10:44:53-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-07-21-google-blocks-site-of-largest-computing-society-for-being-harmful

[Source](https://www.bleepingcomputer.com/news/security/google-blocks-site-of-largest-computing-society-for-being-harmful-/){:target="_blank" rel="noopener"}

> Google Search and Drive are erroneously flagging links to Association for Computing Machinery (ACM) research papers and websites as malware. BleepingComputer has successfully reproduced the issue, first reported by researcher Maximilian Golla. [...]
