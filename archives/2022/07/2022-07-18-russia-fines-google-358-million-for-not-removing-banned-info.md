Title: Russia fines Google $358 million for not removing banned info
Date: 2022-07-18T13:51:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-07-18-russia-fines-google-358-million-for-not-removing-banned-info

[Source](https://www.bleepingcomputer.com/news/security/russia-fines-google-358-million-for-not-removing-banned-info/){:target="_blank" rel="noopener"}

> A court in Moscow has imposed a fine of $358 million (21 billion rubles) on Google LLC for failing to restrict access to information considered prohibited in the country. [...]
