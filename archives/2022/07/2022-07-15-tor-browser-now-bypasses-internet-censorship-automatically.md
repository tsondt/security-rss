Title: Tor Browser now bypasses internet censorship automatically
Date: 2022-07-15T10:27:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2022-07-15-tor-browser-now-bypasses-internet-censorship-automatically

[Source](https://www.bleepingcomputer.com/news/security/tor-browser-now-bypasses-internet-censorship-automatically/){:target="_blank" rel="noopener"}

> The Tor Project team has announced the release of Tor Browser 11.5, a major release that brings new features to help users fight censorship easier. [...]
