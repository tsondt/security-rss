Title: FBI and MI5 bosses: China cheats and steals at massive scale
Date: 2022-07-07T06:12:03+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-07-fbi-and-mi5-bosses-china-cheats-and-steals-at-massive-scale

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/07/fbi_and_mi5_china_warning/){:target="_blank" rel="noopener"}

> Other US spooks chime in with similar warnings The directors of the UK Military Intelligence, Section 5 (MI5) and the US Federal Bureau of Investigation on Wednesday shared a public platform for the first time and warned of China's increased espionage activity on UK and US intellectual property.... [...]
