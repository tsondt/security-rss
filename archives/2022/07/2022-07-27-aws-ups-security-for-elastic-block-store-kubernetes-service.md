Title: AWS ups security for Elastic Block Store, Kubernetes service
Date: 2022-07-27T17:00:15+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-07-27-aws-ups-security-for-elastic-block-store-kubernetes-service

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/27/aws_security_elastic/){:target="_blank" rel="noopener"}

> Stretching its security software a bit further Amazon’s cloud platform is extending security capabilities for a couple of its widely used services; Amazon Elastic Block Store (EBS) and Amazon Elastic Kubernetes Service (EKS).... [...]
