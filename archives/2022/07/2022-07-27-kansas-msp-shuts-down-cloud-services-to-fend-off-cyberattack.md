Title: Kansas MSP shuts down cloud services to fend off cyberattack
Date: 2022-07-27T20:15:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-27-kansas-msp-shuts-down-cloud-services-to-fend-off-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/kansas-msp-shuts-down-cloud-services-to-fend-off-cyberattack/){:target="_blank" rel="noopener"}

> A US managed service provider NetStandard suffered a cyberattack causing the company to shut down its MyAppsAnywhere cloud services, consisting of hosted Dynamics GP, Exchange, Sharepoint, and CRM services. [...]
