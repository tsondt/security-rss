Title: NIST Announces First Four Quantum-Resistant Cryptographic Algorithms
Date: 2022-07-06T16:49:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;encryption;NIST;quantum computing
Slug: 2022-07-06-nist-announces-first-four-quantum-resistant-cryptographic-algorithms

[Source](https://www.schneier.com/blog/archives/2022/07/nist-announces-first-four-quantum-resistant-cryptographic-algorithms.html){:target="_blank" rel="noopener"}

> NIST’s post-quantum computing cryptography standard process is entering its final phases. It announced the first four algorithms: For general encryption, used when we access secure websites, NIST has selected the CRYSTALS-Kyber algorithm. Among its advantages are comparatively small encryption keys that two parties can exchange easily, as well as its speed of operation. For digital signatures, often used when we need to verify identities during a digital transaction or to sign a document remotely, NIST has selected the three algorithms CRYSTALS-Dilithium, FALCON and SPHINCS+ (read as “Sphincs plus”). Reviewers noted the high efficiency of the first two, and NIST recommends [...]
