Title: No More Ransom helps millions of ransomware victims in 6 years
Date: 2022-07-26T09:38:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-07-26-no-more-ransom-helps-millions-of-ransomware-victims-in-6-years

[Source](https://www.bleepingcomputer.com/news/security/no-more-ransom-helps-millions-of-ransomware-victims-in-6-years/){:target="_blank" rel="noopener"}

> The No More Ransom project celebrates its sixth anniversary today after helping millions of ransomware victims recover their files for free. [...]
