Title: Microsoft: IIS extensions increasingly used as Exchange backdoors
Date: 2022-07-26T14:01:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-26-microsoft-iis-extensions-increasingly-used-as-exchange-backdoors

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-iis-extensions-increasingly-used-as-exchange-backdoors/){:target="_blank" rel="noopener"}

> Microsoft says attackers increasingly use malicious Internet Information Services (IIS) web server extensions to backdoor unpatched Exchange servers as they have lower detection rates compared to web shells. [...]
