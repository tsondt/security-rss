Title: Five accused of trying to silence China critics in US
Date: 2022-07-07T18:56:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-07-07-five-accused-of-trying-to-silence-china-critics-in-us

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/07/five_indicted_china_usa/){:target="_blank" rel="noopener"}

> Alleged campaign involved stalking via GPS and hidden cameras, fake interviews, confidential government data Five suspects were indicted in a federal court in Brooklyn, New York on Wednesday for alleged crimes related to a campaign to silence dissidents in the US who opposed the government of the People's Republic of China (PRC).... [...]
