Title: Ex-Coinbase manager charged in first-ever crypto insider trading case
Date: 2022-07-21T22:20:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-21-ex-coinbase-manager-charged-in-first-ever-crypto-insider-trading-case

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/21/coinbase_crypto_insider_trading/){:target="_blank" rel="noopener"}

> Exec, his brother, and a pal raked in $1.5m in illicit gains, Feds claim A now-former Coinbase manager, his brother, and a friend were today charged with wire fraud conspiracy and wire fraud in connection with the first-ever cryptocurrency insider trading scheme in the US.... [...]
