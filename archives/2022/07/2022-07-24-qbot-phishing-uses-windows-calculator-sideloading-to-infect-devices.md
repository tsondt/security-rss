Title: QBot phishing uses Windows Calculator sideloading to infect devices
Date: 2022-07-24T11:18:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-24-qbot-phishing-uses-windows-calculator-sideloading-to-infect-devices

[Source](https://www.bleepingcomputer.com/news/security/qbot-phishing-uses-windows-calculator-sideloading-to-infect-devices/){:target="_blank" rel="noopener"}

> The operators of the QBot malware have been using the Windows Calculator to side-load the malicious payload on infected computers. [...]
