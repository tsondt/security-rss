Title: XSS vulnerabilities in Google Cloud, Google Play could lead to account hijacks
Date: 2022-07-29T14:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-29-xss-vulnerabilities-in-google-cloud-google-play-could-lead-to-account-hijacks

[Source](https://portswigger.net/daily-swig/xss-vulnerabilities-in-google-cloud-google-play-could-lead-to-account-hijacks){:target="_blank" rel="noopener"}

> Reflected XSS and DOM-based XSS bugs net researchers $3,000 and $5,000 bug bounties [...]
