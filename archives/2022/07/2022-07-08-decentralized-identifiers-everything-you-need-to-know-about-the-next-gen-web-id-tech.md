Title: Decentralized Identifiers: Everything you need to know about the next-gen web ID tech
Date: 2022-07-08T14:14:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-08-decentralized-identifiers-everything-you-need-to-know-about-the-next-gen-web-id-tech

[Source](https://portswigger.net/daily-swig/decentralized-identifiers-everything-you-need-to-know-about-the-next-gen-web-id-tech){:target="_blank" rel="noopener"}

> DID promises to give web users more control over their digital identities [...]
