Title: New RedAlert Ransomware targets Windows, Linux VMware ESXi servers
Date: 2022-07-05T18:20:47-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-07-05-new-redalert-ransomware-targets-windows-linux-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/new-redalert-ransomware-targets-windows-linux-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> A new ransomware operation called RedAlert, or N13V, encrypts both Windows and Linux VMWare ESXi servers in attacks on corporate networks. [...]
