Title: IBM puts NIST’s quantum-resistant crypto to work in Z16 mainframe
Date: 2022-07-27T06:30:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-27-ibm-puts-nists-quantum-resistant-crypto-to-work-in-z16-mainframe

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/27/z16_ibm_post_quantum_crypto/){:target="_blank" rel="noopener"}

> Big Blue says it helped developed the algos, so knows what it's doing IBM has started offering quantum-resistant crypto – using the quantum-resistant crypto recommended by the US National Institute of Standards and Technology (NIST).... [...]
