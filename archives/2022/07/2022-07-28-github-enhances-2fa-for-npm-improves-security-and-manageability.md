Title: GitHub enhances 2FA for npm, improves security and manageability
Date: 2022-07-28T15:32:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-28-github-enhances-2fa-for-npm-improves-security-and-manageability

[Source](https://portswigger.net/daily-swig/github-enhances-2fa-for-npm-improves-security-and-manageability){:target="_blank" rel="noopener"}

> New features also include ability to connect social media accounts [...]
