Title: Google boosts Android privacy with support for DNS-over-HTTP/3
Date: 2022-07-20T17:13:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2022-07-20-google-boosts-android-privacy-with-support-for-dns-over-http3

[Source](https://www.bleepingcomputer.com/news/security/google-boosts-android-privacy-with-support-for-dns-over-http-3/){:target="_blank" rel="noopener"}

> Google has added support for the DNS-over-HTTP/3 (DoH3) protocol on Android 11 and later to increase the privacy of DNS queries while providing better performance. [...]
