Title: New Browser De-anonymization Technique
Date: 2022-07-14T14:31:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;browsers;de-anonymization;privacy;side-channel attacks
Slug: 2022-07-14-new-browser-de-anonymization-technique

[Source](https://www.schneier.com/blog/archives/2022/07/new-browser-de-anonymization-technique.html){:target="_blank" rel="noopener"}

> Researchers have a new way to de-anonymize browser users, by correlating their behavior on one account with their behavior on another: The findings, which NJIT researchers will present at the Usenix Security Symposium in Boston next month, show how an attacker who tricks someone into loading a malicious website can determine whether that visitor controls a particular public identifier, like an email address or social media account, thus linking the visitor to a piece of potentially personal data. When you visit a website, the page can capture your IP address, but this doesn’t necessarily give the site owner enough information [...]
