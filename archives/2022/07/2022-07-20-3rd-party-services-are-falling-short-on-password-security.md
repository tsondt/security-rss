Title: 3rd Party Services Are Falling Short on Password Security
Date: 2022-07-20T10:03:06-04:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2022-07-20-3rd-party-services-are-falling-short-on-password-security

[Source](https://www.bleepingcomputer.com/news/security/3rd-party-services-are-falling-short-on-password-security/){:target="_blank" rel="noopener"}

> Preventing the use of weak and leaked passwords within an enterprise environment is a manageable task for your IT department, but what about other services where end-users share business-critical data in order to do their work? They could be putting your organization at risk, and the team at Specops Software decided to see for sure. [...]
