Title: New UFEI Rootkit
Date: 2022-07-28T11:16:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;implants;Kaspersky;malware;reports;rootkits
Slug: 2022-07-28-new-ufei-rootkit

[Source](https://www.schneier.com/blog/archives/2022/07/new-ufei-rootkit.html){:target="_blank" rel="noopener"}

> Kaspersky is reporting on a new UFEI rootkit that survives reinstalling the operating system and replacing the hard drive. From an article : The firmware compromises the UEFI, the low-level and highly opaque chain of firmware required to boot up nearly every modern computer. As the software that bridges a PC’s device firmware with its operating system, the UEFI—short for Unified Extensible Firmware Interface—is an OS in its own right. It’s located in an SPI-connected flash storage chip soldered onto the computer motherboard, making it difficult to inspect or patch the code. Because it’s the first thing to run when [...]
