Title: FileWave fixes bugs that left 1,000+ orgs open to ransomware, data theft
Date: 2022-07-27T22:33:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-27-filewave-fixes-bugs-that-left-1000-orgs-open-to-ransomware-data-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/27/filewave_mdm_fixes/){:target="_blank" rel="noopener"}

> Internet-connected MDM instances, each with an 'unrestricted number' of managed devices, were vulnerable FileWave has fixed a couple vulnerabilities in its endpoint management software that could allow a remote attacker to bypass authentication and take full control of the deployment and associated devices.... [...]
