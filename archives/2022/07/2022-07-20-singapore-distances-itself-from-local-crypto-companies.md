Title: Singapore distances itself from local crypto companies
Date: 2022-07-20T10:45:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-07-20-singapore-distances-itself-from-local-crypto-companies

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/20/singapore_cryptocurrency_policy/){:target="_blank" rel="noopener"}

> Consumer protection regulation coming soon as anti-crypto rhetoric ratchets The Monetary Authority of Singapore (MAS) said on Tuesday that its cryptocurrency regulations will add measures to protect consumers, in addition to ongoing work to contain money laundering and terrorist funding.... [...]
