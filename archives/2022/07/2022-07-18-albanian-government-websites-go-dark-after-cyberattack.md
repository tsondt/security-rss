Title: Albanian government websites go dark after cyberattack
Date: 2022-07-18T15:00:05+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-07-18-albanian-government-websites-go-dark-after-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/18/albania_down/){:target="_blank" rel="noopener"}

> Citizen services only moved online in May. What could possibly go wrong? Albania's online public services and websites have gone dark following what appears to be a cyberattack.... [...]
