Title: FileWave MDM authentication bypass bugs expose managed devices to hijack risk
Date: 2022-07-26T15:17:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-26-filewave-mdm-authentication-bypass-bugs-expose-managed-devices-to-hijack-risk

[Source](https://portswigger.net/daily-swig/filewave-mdm-authentication-bypass-bugs-expose-managed-devices-to-hijack-risk){:target="_blank" rel="noopener"}

> ‘Vast majority’ of users have updated systems thanks to vendor warnings [...]
