Title: Marriott Hotels admits to third data breach in 4 years
Date: 2022-07-06T14:00:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2022-07-06-marriott-hotels-admits-to-third-data-breach-in-4-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/06/marriott_hotels_suffer_yet_another/){:target="_blank" rel="noopener"}

> Digital thieves made off with 20GB of internal documents and customer data Crooks have reportedly made off with 20GB of data from Marriott Hotels, which apparently included credit card info and internal company documents.... [...]
