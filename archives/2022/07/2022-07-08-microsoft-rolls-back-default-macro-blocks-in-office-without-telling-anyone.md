Title: Microsoft rolls back default macro blocks in Office without telling anyone
Date: 2022-07-08T03:02:25+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-08-microsoft-rolls-back-default-macro-blocks-in-office-without-telling-anyone

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/08/office_macro_block_rollback/){:target="_blank" rel="noopener"}

> Based on 'feedback'. Which one of you asked for this, and why? Microsoft appears set to roll back its decision to adopt a default stance of preventing macros sourced from the internet from running in Office unless given explicit permission.... [...]
