Title: High severity OpenSSL bug could lead to remote code execution
Date: 2022-07-06T13:14:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-06-high-severity-openssl-bug-could-lead-to-remote-code-execution

[Source](https://portswigger.net/daily-swig/high-severity-openssl-bug-could-lead-to-remote-code-execution){:target="_blank" rel="noopener"}

> Fixes are available, update now [...]
