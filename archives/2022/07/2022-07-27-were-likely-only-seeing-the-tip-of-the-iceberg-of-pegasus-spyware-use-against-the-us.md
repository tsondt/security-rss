Title: We're likely only seeing 'the tip of the iceberg' of Pegasus spyware use against the US
Date: 2022-07-27T21:58:53+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2022-07-27-were-likely-only-seeing-the-tip-of-the-iceberg-of-pegasus-spyware-use-against-the-us

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/27/us_congress_spyware_debate/){:target="_blank" rel="noopener"}

> House intel chair raises snoop tool concerns as Google and others call for greater crack down Google and internet rights groups have called on Congress to weigh in on spyware, asking for sanctions and increased enforcement against so-called legit surveillanceware makers.... [...]
