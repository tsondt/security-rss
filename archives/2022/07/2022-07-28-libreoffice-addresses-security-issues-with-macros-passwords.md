Title: LibreOffice addresses security issues with macros, passwords
Date: 2022-07-28T12:33:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-28-libreoffice-addresses-security-issues-with-macros-passwords

[Source](https://www.bleepingcomputer.com/news/security/libreoffice-addresses-security-issues-with-macros-passwords/){:target="_blank" rel="noopener"}

> The LibreOffice suite has been updated to address several security vulnerabilities related to the execution of macros and the protection of passwords for web connections. [...]
