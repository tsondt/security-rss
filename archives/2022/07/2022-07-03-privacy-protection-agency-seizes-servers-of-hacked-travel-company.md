Title: Privacy protection agency seizes servers of hacked travel company
Date: 2022-07-03T15:34:29-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-07-03-privacy-protection-agency-seizes-servers-of-hacked-travel-company

[Source](https://www.bleepingcomputer.com/news/security/privacy-protection-agency-seizes-servers-of-hacked-travel-company/){:target="_blank" rel="noopener"}

> The Privacy Protection Authority in Israel seized servers hosting multiple travel booking websites because their operator failed to address security issues that enabled data breaches affecting more than 300,000 individuals. [...]
