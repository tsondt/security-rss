Title: Microsoft Exchange servers increasingly hacked with IIS backdoors
Date: 2022-07-26T14:01:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-26-microsoft-exchange-servers-increasingly-hacked-with-iis-backdoors

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-exchange-servers-increasingly-hacked-with-iis-backdoors/){:target="_blank" rel="noopener"}

> Microsoft says attackers increasingly use malicious Internet Information Services (IIS) web server extensions to backdoor unpatched Exchange servers as they have lower detection rates compared to web shells. [...]
