Title: Decentralized IPFS networks forming the 'hotbed of phishing'
Date: 2022-07-29T18:00:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2022-07-29-decentralized-ipfs-networks-forming-the-hotbed-of-phishing

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/29/ipfs_phishing_trustwave/){:target="_blank" rel="noopener"}

> P2P file system makes it more difficult to detect and take down malicious content Threat groups are increasingly turning to InterPlanetary File System (IPFS) peer-to-peer data sites to host their phishing attacks because the decentralized nature of the sharing system means malicious content is more effective and easier to hide.... [...]
