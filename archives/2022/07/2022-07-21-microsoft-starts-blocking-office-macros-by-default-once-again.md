Title: Microsoft starts blocking Office macros by default, once again
Date: 2022-07-21T04:40:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-07-21-microsoft-starts-blocking-office-macros-by-default-once-again

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-starts-blocking-office-macros-by-default-once-again/){:target="_blank" rel="noopener"}

> Microsoft announced today that it resumed the rollout of VBA macro auto-blocking in downloaded Office documents after temporarily rolling it back earlier this month following user feedback. [...]
