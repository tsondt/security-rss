Title: OSPAR 2022 report now available with 142 services in scope
Date: 2022-07-07T16:49:36+00:00
Author: Joseph Goh
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;Banking Regulations;Financial Services;OSPAR;Security Blog;Singapore
Slug: 2022-07-07-ospar-2022-report-now-available-with-142-services-in-scope

[Source](https://aws.amazon.com/blogs/security/ospar-2022-report-now-available-with-142-services-in-scope/){:target="_blank" rel="noopener"}

> We’re excited to announce the completion of our annual Outsourced Service Provider’s Audit Report (OSPAR) audit cycle on July 1, 2022. The 2022 OSPAR certification cycle includes the addition of 15 new services in scope, bringing the total number of services in scope to 142 in the AWS Asia Pacific (Singapore) Region. Newly added services [...]
