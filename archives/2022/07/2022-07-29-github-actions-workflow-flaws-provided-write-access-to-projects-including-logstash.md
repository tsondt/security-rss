Title: GitHub Actions workflow flaws provided write access to projects including Logstash
Date: 2022-07-29T15:24:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-29-github-actions-workflow-flaws-provided-write-access-to-projects-including-logstash

[Source](https://portswigger.net/daily-swig/github-actions-workflow-flaws-provided-write-access-to-projects-including-logstash){:target="_blank" rel="noopener"}

> Malicious builds and wider infrastructural compromise were worst-case scenarios [...]
