Title: New Redeemer ransomware version promoted on hacker forums
Date: 2022-07-21T02:38:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-07-21-new-redeemer-ransomware-version-promoted-on-hacker-forums

[Source](https://www.bleepingcomputer.com/news/security/new-redeemer-ransomware-version-promoted-on-hacker-forums/){:target="_blank" rel="noopener"}

> A threat actor is promoting a new version of their free-to-use 'Redeemer' ransomware builder on hacker forums, offering unskilled threat actors an easy entry to the world of encryption-backed extortion attacks. [...]
