Title: U.S. Healthcare Orgs Targeted with Maui Ransomware
Date: 2022-07-08T10:46:55+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Malware
Slug: 2022-07-08-us-healthcare-orgs-targeted-with-maui-ransomware

[Source](https://threatpost.com/healthcare-maui-ransomware/180154/){:target="_blank" rel="noopener"}

> State-sponsored actors are deploying the unique malware--which targets specific files and leaves no ransomware note--in ongoing attacks. [...]
