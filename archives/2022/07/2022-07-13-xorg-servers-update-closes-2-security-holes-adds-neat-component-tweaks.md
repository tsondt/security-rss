Title: X.org servers update closes 2 security holes, adds neat component tweaks
Date: 2022-07-13T16:00:06+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-07-13-xorg-servers-update-closes-2-security-holes-adds-neat-component-tweaks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/13/xorg_servers_updated/){:target="_blank" rel="noopener"}

> Arbitrary code execution flaws in the X Keyboard Extension were bad news X.org has released a bunch of updates, which includes closing two security holes and, yes, this affects Wayland users too.... [...]
