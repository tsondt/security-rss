Title: Automatically block suspicious DNS activity with Amazon GuardDuty and Route 53 Resolver DNS Firewall
Date: 2022-07-20T19:39:56+00:00
Author: Akshay Karanth
Category: AWS Security
Tags: Advanced (300);Amazon GuardDuty;Amazon Route 53;Security, Identity, & Compliance;Amazon EventBridge;AWS Security Hub;AWS Step Functions;Resolver DNS Firewall;Security Blog
Slug: 2022-07-20-automatically-block-suspicious-dns-activity-with-amazon-guardduty-and-route-53-resolver-dns-firewall

[Source](https://aws.amazon.com/blogs/security/automatically-block-suspicious-dns-activity-with-amazon-guardduty-and-route-53-resolver-dns-firewall/){:target="_blank" rel="noopener"}

> In this blog post, we’ll show you how to use Amazon Route 53 Resolver DNS Firewall to automatically respond to suspicious DNS queries that are detected by Amazon GuardDuty within your Amazon Web Services (AWS) environment. The Security Pillar of the AWS Well-Architected Framework includes incident response, stating that your organization should implement mechanisms to [...]
