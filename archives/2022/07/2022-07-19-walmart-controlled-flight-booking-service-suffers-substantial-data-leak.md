Title: Walmart-controlled flight booking service suffers substantial data leak
Date: 2022-07-19T11:15:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-07-19-walmart-controlled-flight-booking-service-suffers-substantial-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/19/cleartrip_data_leak/){:target="_blank" rel="noopener"}

> India's Cleartrip is being very opaque about what happened An Indian flight booking website majority-owned by US retail colossus Walmart has experienced a data breach, but is saying very little about what happened or the risks to customers.... [...]
