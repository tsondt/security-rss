Title: Cloud fax company claims healthcare pros are ditching email for ‘more secure’ fax
Date: 2022-07-26T11:52:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-26-cloud-fax-company-claims-healthcare-pros-are-ditching-email-for-more-secure-fax

[Source](https://portswigger.net/daily-swig/cloud-fax-company-claims-healthcare-pros-are-ditching-email-for-more-secure-fax){:target="_blank" rel="noopener"}

> The fax is dead. Long live the online fax? A new study suggests many healthcare professionals believe that flaws in today’s web security landscape are prompting a return to what’s been deemed an “extr [...]
