Title: Crunch time for EU web authentication plan as Mozilla launches campaign to protect status quo
Date: 2022-07-15T13:22:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-07-15-crunch-time-for-eu-web-authentication-plan-as-mozilla-launches-campaign-to-protect-status-quo

[Source](https://portswigger.net/daily-swig/crunch-time-for-eu-web-authentication-plan-as-mozilla-launches-campaign-to-protect-status-quo){:target="_blank" rel="noopener"}

> Mozilla’s message to MEPs appears to be gaining traction, says senior public policy manager at the non-profit [...]
