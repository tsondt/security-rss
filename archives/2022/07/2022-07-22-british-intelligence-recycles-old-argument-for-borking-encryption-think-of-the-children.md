Title: British intelligence recycles old argument for borking encryption: think of the children!
Date: 2022-07-22T07:30:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-07-22-british-intelligence-recycles-old-argument-for-borking-encryption-think-of-the-children

[Source](https://go.theregister.com/feed/www.theregister.com/2022/07/22/british_encryption_scanning/){:target="_blank" rel="noopener"}

> Levy and Robinson are at it again Comment Two notorious characters from the British security services have published a paper that once again suggests breaking end-to-end encryption would be a good thing for society.... [...]
