Title: Ukrainian cops nab husband and wife suspected to be part of $1m ransomware operation
Date: 2022-01-13T15:31:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-13-ukrainian-cops-nab-husband-and-wife-suspected-to-be-part-of-1m-ransomware-operation

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/13/ukraine_arrests_five_ransomware_suspects/){:target="_blank" rel="noopener"}

> Plus three other suspects nicked in raids today Ukrainian police have arrested five people on suspicion of operating a ransomware gang, including a husband-and-wife team, following tipoffs from UK law enforcement.... [...]
