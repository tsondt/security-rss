Title: Ukraine blames Belarus for PC-wiping 'ransomware' that has no recovery method and nukes target boxen
Date: 2022-01-17T16:24:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-17-ukraine-blames-belarus-for-pc-wiping-ransomware-that-has-no-recovery-method-and-nukes-target-boxen

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/17/ukraine_pc_wiping_malware_belarus_accusations/){:target="_blank" rel="noopener"}

> And for last week's digital graffiti operations, too After last week's website defacements, Ukraine is now being targeted by boot record-wiping malware that looks like ransomware but with one crucial difference: there's no recovery method. Officials have pointed the finger at Belarus.... [...]
