Title: State hackers use new PowerShell backdoor in Log4j attacks
Date: 2022-01-11T18:17:45-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-11-state-hackers-use-new-powershell-backdoor-in-log4j-attacks

[Source](https://www.bleepingcomputer.com/news/security/state-hackers-use-new-powershell-backdoor-in-log4j-attacks/){:target="_blank" rel="noopener"}

> Hackers believed to be part of the Iranian APT35 state-backed group (aka 'Charming Kitten' or 'Phosphorus') has been observed leveraging Log4Shell attacks to drop a new PowerShell backdoor. [...]
