Title: Microsoft Faces Wormable, Critical RCE Bug & 6 Zero-Days
Date: 2022-01-11T21:54:57+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;IoT;Vulnerabilities;Web Security
Slug: 2022-01-11-microsoft-faces-wormable-critical-rce-bug-6-zero-days

[Source](https://threatpost.com/microsoft-wormable-critical-rce-bug-zero-day/177564/){:target="_blank" rel="noopener"}

> The large January 2022 Patch Tuesday update covers nine critical CVEs, including a self-propagator with a 9.8 CVSS score. [...]
