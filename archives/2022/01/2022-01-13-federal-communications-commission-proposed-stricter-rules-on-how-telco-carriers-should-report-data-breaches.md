Title: Federal Communications Commission proposed stricter rules on how telco carriers should report data breaches
Date: 2022-01-13T22:42:42+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2022-01-13-federal-communications-commission-proposed-stricter-rules-on-how-telco-carriers-should-report-data-breaches

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/13/fcc_data_rules/){:target="_blank" rel="noopener"}

> Customers shouldn't need to wait seven days before being told The US Federal Communications Commission is considering imposing stricter rules requiring telecommunications carriers to report data breaches to customers and law enforcement more quickly.... [...]
