Title: Microsoft: Edge will mitigate 'unforeseen active' zero day bugs
Date: 2022-01-17T14:51:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-01-17-microsoft-edge-will-mitigate-unforeseen-active-zero-day-bugs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-edge-will-mitigate-unforeseen-active-zero-day-bugs/){:target="_blank" rel="noopener"}

> Microsoft Edge has added a new feature to the Beta channel that will be able to mitigate future in-the-wild exploitation of unknown zero-day vulnerabilities. [...]
