Title: Safari and iOS users: Your browsing activity is being leaked in real time
Date: 2022-01-18T18:14:21+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;apple;iOS;iPadOS;privacy;Safari
Slug: 2022-01-18-safari-and-ios-users-your-browsing-activity-is-being-leaked-in-real-time

[Source](https://arstechnica.com/?p=1826566){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) For the past four months, Apple’s iOS and iPadOS devices and Safari browser have violated one of the Internet’s most sacrosanct security policies. The violation results from a bug that leaks user identities and browsing activity in real time. The same-origin policy is a foundational security mechanism that forbids documents, scripts, or other content loaded from one origin—meaning the protocol, domain name, and port of a given webpage or app—from interacting with resources from other origins. Without this policy, malicious sites—say, badguy.example.com—could access login credentials for Google or another trusted site when it’s open in a [...]
