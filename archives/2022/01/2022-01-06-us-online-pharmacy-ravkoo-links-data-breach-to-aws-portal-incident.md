Title: US online pharmacy Ravkoo links data breach to AWS portal incident
Date: 2022-01-06T11:48:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-06-us-online-pharmacy-ravkoo-links-data-breach-to-aws-portal-incident

[Source](https://www.bleepingcomputer.com/news/security/us-online-pharmacy-ravkoo-links-data-breach-to-aws-portal-incident/){:target="_blank" rel="noopener"}

> Ravkoo, a US Internet-based pharmacy service, has disclosed a data breach after the company's AWS hosted cloud prescription portal was involved in a security incident that may have led to personal and health information being accessed. [...]
