Title: Surge in Malicious QR Codes Sparks FBI Alert
Date: 2022-01-24T21:13:22+00:00
Author: Becky Bracken
Category: Threatpost
Tags: IoT;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2022-01-24-surge-in-malicious-qr-codes-sparks-fbi-alert

[Source](https://threatpost.com/fbi-malicious-qr-codes/177902/){:target="_blank" rel="noopener"}

> QR codes have become a go-to staple for contactless transactions of all sorts during the pandemic, and the FBI is warning cybercriminals are capitalizing on their lax security to steal data and money, and drop malware. [...]
