Title: Analyze AWS WAF logs using Amazon OpenSearch Service anomaly detection built on Random Cut Forests
Date: 2022-01-24T20:19:14+00:00
Author: Umesh Ramesh
Category: AWS Security
Tags: Expert (400);Security, Identity, & Compliance;Anomaly detection;AWS WAF;AWS WAF Logs;OpenSearch
Slug: 2022-01-24-analyze-aws-waf-logs-using-amazon-opensearch-service-anomaly-detection-built-on-random-cut-forests

[Source](https://aws.amazon.com/blogs/security/analyze-aws-waf-logs-using-amazon-opensearch-service-anomaly-detection-built-on-random-cut-forests/){:target="_blank" rel="noopener"}

> This blog post shows you how to use the machine learning capabilities of Amazon OpenSearch Service (successor to Amazon Elasticsearch Service) to detect and visualize anomalies in AWS WAF logs. AWS WAF logs are streamed to Amazon OpenSearch Service using Amazon Kinesis Data Firehose. Kinesis Data Firehose invokes an AWS Lambda function to transform incoming [...]
