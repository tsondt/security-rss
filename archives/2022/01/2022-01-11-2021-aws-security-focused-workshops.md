Title: 2021 AWS security-focused workshops
Date: 2022-01-11T20:38:55+00:00
Author: Temi Adebambo
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;Thought Leadership;AWS Security Labs;AWS security training;AWS Workshops;Compliance;Privacy;Security Blog;Security education
Slug: 2022-01-11-2021-aws-security-focused-workshops

[Source](https://aws.amazon.com/blogs/security/2021-aws-security-focused-workshops/){:target="_blank" rel="noopener"}

> Every year, Amazon Web Services (AWS) looks to help our customers gain more experience and knowledge of our services through hands-on workshops. In 2021, we unfortunately couldn’t connect with you in person as much as we would have liked, so we wanted to create and share new ways to learn and build on AWS. We [...]
