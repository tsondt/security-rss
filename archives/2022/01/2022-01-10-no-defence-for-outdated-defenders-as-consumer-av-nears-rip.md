Title: No defence for outdated defenders as consumer AV nears RIP
Date: 2022-01-10T10:00:06+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2022-01-10-no-defence-for-outdated-defenders-as-consumer-av-nears-rip

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/10/opinion_column_consumer_av/){:target="_blank" rel="noopener"}

> How sad would you be to see AV go? Us neither Opinion Game knows game. Thus it came as little surprise that Norton's consumer security software not only sprouted a cryptominer that slurps your computer's life essence and skims a cut, but that it's hard to turn it off.... [...]
