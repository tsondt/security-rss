Title: The future of work requires a more human approach to security
Date: 2022-01-18T17:00:00+00:00
Author: Michael Karner
Category: GCP Security
Tags: Identity & Security;Google Workspace
Slug: 2022-01-18-the-future-of-work-requires-a-more-human-approach-to-security

[Source](https://cloud.google.com/blog/products/workspace/the-future-of-work-requires-more-human-approach-to-security/){:target="_blank" rel="noopener"}

> Most businesses that have adopted off-site or hybrid working models over the last two years made the change under immense pressure. The need was incredibly urgent and timing was a major factor. Now that they’ve had a chance to adapt and settle in, leaders are revisiting how their teams work in a more proactive way. They’re updating strategies and policies with a focus on what will be best for both the company and the employees long term. This is especially true when it comes to data integrity and security. Hybrid/flexible work will be a “standard practice” within three years, say [...]
