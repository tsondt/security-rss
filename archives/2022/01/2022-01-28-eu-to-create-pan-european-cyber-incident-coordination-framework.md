Title: EU to create pan-European cyber incident coordination framework
Date: 2022-01-28T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-28-eu-to-create-pan-european-cyber-incident-coordination-framework

[Source](https://www.bleepingcomputer.com/news/security/eu-to-create-pan-european-cyber-incident-coordination-framework/){:target="_blank" rel="noopener"}

> The European Systemic Risk Board (ESRB) proposed a new systemic cyber incident coordination framework that would allow EU relevant authorities to better coordinate when having to respond to major cross-border cyber incidents impacting the Union's financial sector. [...]
