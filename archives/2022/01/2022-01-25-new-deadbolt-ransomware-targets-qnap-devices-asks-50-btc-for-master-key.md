Title: New DeadBolt ransomware targets QNAP devices, asks 50 BTC for master key
Date: 2022-01-25T19:28:37-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-25-new-deadbolt-ransomware-targets-qnap-devices-asks-50-btc-for-master-key

[Source](https://www.bleepingcomputer.com/news/security/new-deadbolt-ransomware-targets-qnap-devices-asks-50-btc-for-master-key/){:target="_blank" rel="noopener"}

> A new DeadBolt ransomware group is encrypting QNAP NAS devices worldwide using what they claim is a zero-day vulnerability in the device's software. [...]
