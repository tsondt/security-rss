Title: Purple Fox Rootkit Dropped by Malicious Telegram Installers
Date: 2022-01-04T17:12:12+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware
Slug: 2022-01-04-purple-fox-rootkit-dropped-by-malicious-telegram-installers

[Source](https://threatpost.com/purple-fox-rootkit-telegram-installers/177330/){:target="_blank" rel="noopener"}

> Multiple malicious installers were delivering the same Purple Fox rootkit version using the same attack chain, possibly distributed via email or phishing sites. [...]
