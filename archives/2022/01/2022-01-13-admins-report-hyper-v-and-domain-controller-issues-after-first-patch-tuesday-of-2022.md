Title: Admins report Hyper-V and domain controller issues after first Patch Tuesday of 2022
Date: 2022-01-13T13:17:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-01-13-admins-report-hyper-v-and-domain-controller-issues-after-first-patch-tuesday-of-2022

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/13/microsoft_patch_tuesday_titsup/){:target="_blank" rel="noopener"}

> Start as you mean to go on, Microsoft Updated Microsoft's first Patch Tuesday of 2022 has, for some folk, broken Hyper-V and sent domain controllers into boot loops.... [...]
