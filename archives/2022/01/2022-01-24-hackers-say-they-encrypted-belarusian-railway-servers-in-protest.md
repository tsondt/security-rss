Title: Hackers say they encrypted Belarusian Railway servers in protest
Date: 2022-01-24T12:34:14-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-24-hackers-say-they-encrypted-belarusian-railway-servers-in-protest

[Source](https://www.bleepingcomputer.com/news/security/hackers-say-they-encrypted-belarusian-railway-servers-in-protest/){:target="_blank" rel="noopener"}

> A group of hackers (known as Belarusian Cyber-Partisans) claim they breached and encrypted servers belonging to the Belarusian Railway, Belarus's national state-owned railway company. [...]
