Title: China puts Walmart in the naughty corner, citing 19 alleged cybersecurity 'violations'
Date: 2022-01-10T13:35:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-01-10-china-puts-walmart-in-the-naughty-corner-citing-19-alleged-cybersecurity-violations

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/10/walmart_china_security/){:target="_blank" rel="noopener"}

> Warning comes weeks after govt body accused subsidiary Sam’s Club of 'ulterior motive' in goods stocking spat American budget retailer Walmart was cited for 19 alleged cybersecurity breaches in China, state-sponsored media reported last week.... [...]
