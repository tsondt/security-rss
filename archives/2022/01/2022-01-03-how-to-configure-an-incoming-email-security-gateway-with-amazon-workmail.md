Title: How to configure an incoming email security gateway with Amazon WorkMail
Date: 2022-01-03T21:37:21+00:00
Author: Jesse Thompson
Category: AWS Security
Tags: Best Practices;Foundational (100);Security, Identity, & Compliance;anti-spam;dmarc;Email;mta;proxy;relay;smtp
Slug: 2022-01-03-how-to-configure-an-incoming-email-security-gateway-with-amazon-workmail

[Source](https://aws.amazon.com/blogs/security/how-to-configure-an-incoming-email-security-gateway-with-amazon-workmail/){:target="_blank" rel="noopener"}

> This blog post will walk you through the steps needed to integrate Amazon WorkMail with an email security gateway. Configuring WorkMail this way can provide a versatile defense strategy for inbound email threats. Amazon WorkMail is a secure, managed business email and calendar service. WorkMail leverages the email receiving capabilities of Amazon Simple Email Service [...]
