Title: Austrian watchdog rules German company's use of Google Analytics breached GDPR by sending data to US
Date: 2022-01-13T14:48:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-01-13-austrian-watchdog-rules-german-companys-use-of-google-analytics-breached-gdpr-by-sending-data-to-us

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/13/google_analytics_gdpr/){:target="_blank" rel="noopener"}

> Schrems II ruling continues to trouble transatlantic data sharing The Austrian data protection authority has ruled that use of Google Analytics by a German company is in breach of European law in light of the Schrems II EU-US data sharing ruling.... [...]
