Title: North Korean hackers stole nearly $400 million in crypto last year
Date: 2022-01-16T22:34:17+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Policy;cryptocurrency;hacking;North Korea
Slug: 2022-01-16-north-korean-hackers-stole-nearly-400-million-in-crypto-last-year

[Source](https://arstechnica.com/?p=1825925){:target="_blank" rel="noopener"}

> Enlarge The past year saw a breathtaking rise in the value of cryptocurrencies like Bitcoin and Ethereum, with Bitcoin gaining 60 percent in value in 2021 and Ethereum spiking 80 percent. So perhaps it's no surprise that the relentless North Korean hackers who feed off that booming crypto economy had a very good year as well. North Korean hackers stole a total of $395 million worth of crypto coins last year across seven intrusions into cryptocurrency exchanges and investment firms, according to blockchain analysis firm Chainalysis. The nine-figure sum represents a nearly $100 million increase over the previous year's thefts [...]
