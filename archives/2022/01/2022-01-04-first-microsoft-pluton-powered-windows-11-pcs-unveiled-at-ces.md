Title: First Microsoft Pluton-powered Windows 11 PCs unveiled at CES
Date: 2022-01-04T14:28:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-01-04-first-microsoft-pluton-powered-windows-11-pcs-unveiled-at-ces

[Source](https://www.bleepingcomputer.com/news/microsoft/first-microsoft-pluton-powered-windows-11-pcs-unveiled-at-ces/){:target="_blank" rel="noopener"}

> Lenovo unveiled today at CES 2022 the first Microsoft Pluton-powered Windows 11 PCs, the ThinkPad Z13 and Z16, with AMD Ryzen 6000 Series processors. [...]
