Title: How ransomware gangs went pro
Date: 2022-01-05T08:30:08+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2022-01-05-how-ransomware-gangs-went-pro

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/05/how_ransomware_went_pro/){:target="_blank" rel="noopener"}

> They're developing new techniques 'in every area' says Darktrace Paid Feature Ransomware has come a long way since the early days. When it first started out, it spread indiscriminately and often used poor code. Over the years, it has become more sophisticated and is now an efficient business. How did it become so professional?... [...]
