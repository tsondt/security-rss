Title: 105 million Android users targeted by subscription fraud campaign
Date: 2022-01-27T07:07:48-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-01-27-105-million-android-users-targeted-by-subscription-fraud-campaign

[Source](https://www.bleepingcomputer.com/news/security/105-million-android-users-targeted-by-subscription-fraud-campaign/){:target="_blank" rel="noopener"}

> A premium services subscription scam for Android has been operating for close to two years. Called 'Dark Herring', the operation used 470 Google Play Store apps and affected over 100 million users worldwide, potentially causing hundreds of millions of USD in total losses. [...]
