Title: Researchers discover Log4j-like flaw in H2 database console
Date: 2022-01-07T16:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-07-researchers-discover-log4j-like-flaw-in-h2-database-console

[Source](https://portswigger.net/daily-swig/researchers-discover-log4j-like-flaw-in-h2-database-console){:target="_blank" rel="noopener"}

> Impact of JNDI bug mitigated by vulnerable behavior being disabled by default [...]
