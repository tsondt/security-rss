Title: Firefox fixes fullscreen notification bypass bug that could have led to convincing phishing campaigns
Date: 2022-01-12T14:11:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-12-firefox-fixes-fullscreen-notification-bypass-bug-that-could-have-led-to-convincing-phishing-campaigns

[Source](https://portswigger.net/daily-swig/firefox-fixes-fullscreen-notification-bypass-bug-that-could-have-led-to-convincing-phishing-campaigns){:target="_blank" rel="noopener"}

> Flurry of issues patched in web browser’s latest advisory [...]
