Title: MacOS Bug Could Let Creeps Snoop On You
Date: 2022-01-11T20:35:47+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Privacy;Vulnerabilities
Slug: 2022-01-11-macos-bug-could-let-creeps-snoop-on-you

[Source](https://threatpost.com/macos-bug-snooping-microsoft/177551/){:target="_blank" rel="noopener"}

> The flaw could allow attackers to bypass Privacy preferences, giving apps with no right to access files, microphones or cameras the ability to record you or grab screenshots. [...]
