Title: 'Now' would be the right time to patch Ubuntu container hosts and ditch 21.04 thanks to heap buffer overflow bug
Date: 2022-01-20T14:38:26+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-01-20-now-would-be-the-right-time-to-patch-ubuntu-container-hosts-and-ditch-2104-thanks-to-heap-buffer-overflow-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/20/ubuntu_2104_eol/){:target="_blank" rel="noopener"}

> Red Hat agrees The CVE-2022-0185 vulnerability in Ubuntu is severe enough that Red Hat is also advising immediate patching.... [...]
