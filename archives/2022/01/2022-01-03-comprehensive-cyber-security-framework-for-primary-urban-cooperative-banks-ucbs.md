Title: Comprehensive Cyber Security Framework for Primary (Urban) Cooperative Banks (UCBs)
Date: 2022-01-03T17:09:43+00:00
Author: Vikas Purohit
Category: AWS Security
Tags: Announcements;Compliance;Financial Services;Security, Identity, & Compliance
Slug: 2022-01-03-comprehensive-cyber-security-framework-for-primary-urban-cooperative-banks-ucbs

[Source](https://aws.amazon.com/blogs/security/comprehensive-cyber-security-framework-for-primary-urban-cooperative-banks/){:target="_blank" rel="noopener"}

> We are pleased to announce a new Amazon Web Services (AWS) workbook designed to help India Primary (UCBs) customers align with the Reserve Bank of India (RBI) guidance in Comprehensive Cyber Security Framework for Primary (Urban) Cooperative Banks (UCBs) – A Graded Approach. In addition to RBI’s basic cyber security framework for Primary (Urban) Cooperative [...]
