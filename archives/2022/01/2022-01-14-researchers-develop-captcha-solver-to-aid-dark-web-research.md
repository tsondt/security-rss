Title: Researchers develop CAPTCHA solver to aid dark web research
Date: 2022-01-14T13:35:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2022-01-14-researchers-develop-captcha-solver-to-aid-dark-web-research

[Source](https://www.bleepingcomputer.com/news/security/researchers-develop-captcha-solver-to-aid-dark-web-research/){:target="_blank" rel="noopener"}

> A team of researchers at the Universities of Arizona, Georgia, and South Florida, have developed a machine-learning-based CAPTCHA solver that they claim can overcome 94.4% of real challenges on dark websites. [...]
