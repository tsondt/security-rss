Title: Windows giant seeks Pluton-ic relationship with chipmaker: AMD first out of the gates with Microsoft's security processor
Date: 2022-01-05T12:11:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-01-05-windows-giant-seeks-pluton-ic-relationship-with-chipmaker-amd-first-out-of-the-gates-with-microsofts-security-processor

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/05/microsoft_pluton/){:target="_blank" rel="noopener"}

> Yes, you're going to have to get a new CPU (again) It's been a while coming, but it looks like PCs with Microsoft's Pluton security processor are just around the corner. So long as your silicon of choice comes from AMD, for the time being at least.... [...]
