Title: <span>White House tackles ‘unique security challenges’ faced by open source ecosystem during dedicated virtual summit</span>
Date: 2022-01-17T15:02:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-17-white-house-tackles-unique-security-challenges-faced-by-open-source-ecosystem-during-dedicated-virtual-summit

[Source](https://portswigger.net/daily-swig/white-house-tackles-unique-security-challenges-faced-by-open-source-ecosystem-during-dedicated-virtual-summit){:target="_blank" rel="noopener"}

> Silicon Valley giants joined government officials to thrash out remedies to software supply chain woes [...]
