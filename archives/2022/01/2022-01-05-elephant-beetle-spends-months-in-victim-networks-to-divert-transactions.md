Title: ‘Elephant Beetle’ spends months in victim networks to divert transactions
Date: 2022-01-05T08:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-05-elephant-beetle-spends-months-in-victim-networks-to-divert-transactions

[Source](https://www.bleepingcomputer.com/news/security/elephant-beetle-spends-months-in-victim-networks-to-divert-transactions/){:target="_blank" rel="noopener"}

> A financially-motivated actor dubbed 'Elephant Beetle' is stealing millions of dollars from organizations worldwide using an arsenal of over 80 unique tools and scripts. [...]
