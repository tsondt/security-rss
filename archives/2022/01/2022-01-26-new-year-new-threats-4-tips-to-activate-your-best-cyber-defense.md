Title: New Year, New Threats: 4 Tips to Activate Your Best Cyber-Defense
Date: 2022-01-26T20:23:24+00:00
Author: Kerry Matre
Category: Threatpost
Tags: Breach;Cloud Security;Critical Infrastructure;InfoSec Insider;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2022-01-26-new-year-new-threats-4-tips-to-activate-your-best-cyber-defense

[Source](https://threatpost.com/tips-activate-cyber-defense/177955/){:target="_blank" rel="noopener"}

> Need a blueprint for architecting a formidable cyber-defense? Kerry Matre, senior director at Mandiant, shares hers in this detailed breakdown. [...]
