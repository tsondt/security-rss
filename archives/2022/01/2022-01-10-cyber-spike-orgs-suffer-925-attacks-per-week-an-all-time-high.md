Title: Cyber-Spike: Orgs Suffer 925 Attacks per Week, an All-Time High
Date: 2022-01-10T16:29:55+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;IoT;Malware;Mobile Security;Most Recent ThreatLists;Vulnerabilities;Web Security
Slug: 2022-01-10-cyber-spike-orgs-suffer-925-attacks-per-week-an-all-time-high

[Source](https://threatpost.com/cyber-spike-attacks-high-log4j/177481/){:target="_blank" rel="noopener"}

> Cyberattacks increased 50 percent YoY in 2021 and peaked in December due to a frenzy of Log4j exploits, researchers found. [...]
