Title: Finland warns of Facebook accounts hijacked via Messenger phishing
Date: 2022-01-28T07:52:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-28-finland-warns-of-facebook-accounts-hijacked-via-messenger-phishing

[Source](https://www.bleepingcomputer.com/news/security/finland-warns-of-facebook-accounts-hijacked-via-messenger-phishing/){:target="_blank" rel="noopener"}

> Finland's National Cyber Security Centre (NCSC-FI) warns of an ongoing phishing campaign attempting to hijack Facebook accounts by impersonating victims' friends in Facebook Messenger chats. [...]
