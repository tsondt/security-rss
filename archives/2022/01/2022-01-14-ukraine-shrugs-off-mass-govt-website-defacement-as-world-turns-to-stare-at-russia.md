Title: Ukraine shrugs off mass govt website defacement as world turns to stare at Russia
Date: 2022-01-14T15:49:45+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-14-ukraine-shrugs-off-mass-govt-website-defacement-as-world-turns-to-stare-at-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/14/ukraine_cyberattack_gov_websites_defaced/){:target="_blank" rel="noopener"}

> Despite threatening messages nothing's been leaked, say victims A "massive" cyber attack on Ukraine caught the world's eye this morning as the country's foreign ministry said its website, among others, had been taken down by unidentified hackers.... [...]
