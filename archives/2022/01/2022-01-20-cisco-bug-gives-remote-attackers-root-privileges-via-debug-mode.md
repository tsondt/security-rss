Title: Cisco bug gives remote attackers root privileges via debug mode
Date: 2022-01-20T08:15:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-20-cisco-bug-gives-remote-attackers-root-privileges-via-debug-mode

[Source](https://www.bleepingcomputer.com/news/security/cisco-bug-gives-remote-attackers-root-privileges-via-debug-mode/){:target="_blank" rel="noopener"}

> Cisco has fixed a critical security flaw discovered in the Cisco Redundancy Configuration Manager (RCM) for Cisco StarOS Software during internal security testing. [...]
