Title: The Week in Ransomware - January 28th 2022 - Get NAS devices off the Internet
Date: 2022-01-28T16:57:32-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-28-the-week-in-ransomware-january-28th-2022-get-nas-devices-off-the-internet

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-28th-2022-get-nas-devices-off-the-internet/){:target="_blank" rel="noopener"}

> It's been a busy week with ransomware attacks tied to political protests, new attacks on NAS devices, amazing research released about tactics, REvil's history, and more. [...]
