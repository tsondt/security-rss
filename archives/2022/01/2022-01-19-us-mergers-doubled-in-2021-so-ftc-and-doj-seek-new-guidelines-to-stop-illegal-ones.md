Title: US mergers doubled in 2021 so FTC and DoJ seek new guidelines to stop illegal ones
Date: 2022-01-19T12:31:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-01-19-us-mergers-doubled-in-2021-so-ftc-and-doj-seek-new-guidelines-to-stop-illegal-ones

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/19/ftc_doj_merger_guidelines/){:target="_blank" rel="noopener"}

> Last set of rules written in 2010 – a whole different era in tech terms The US Federal Trade Commission (FTC) and Department of Justice (DoJ) Antitrust Division are launching a joint public inquiry as a first step to modernising merger guidelines and preventing anticompetitive deals.... [...]
