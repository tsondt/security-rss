Title: Russia's Putin out the idea of a broad cryptocurrency ban
Date: 2022-01-21T04:57:39+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-01-21-russias-putin-out-the-idea-of-a-broad-cryptocurrency-ban

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/21/russia_cryptocurrency_ban_consultation_paper/){:target="_blank" rel="noopener"}

> Central bank worries that block-bucks reduce government control and are used by crims Russia has floated the prospect of Putin a ban on cryptocurrencies.... [...]
