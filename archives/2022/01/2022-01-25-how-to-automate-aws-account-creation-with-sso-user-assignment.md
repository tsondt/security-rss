Title: How to automate AWS account creation with SSO user assignment
Date: 2022-01-25T20:52:15+00:00
Author: Rafael Koike
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;AWS Control Tower;AWS Organizations;Permission sets;SAML;SSO
Slug: 2022-01-25-how-to-automate-aws-account-creation-with-sso-user-assignment

[Source](https://aws.amazon.com/blogs/security/how-to-automate-aws-account-creation-with-sso-user-assignment/){:target="_blank" rel="noopener"}

> Background AWS Control Tower offers a straightforward way to set up and govern an Amazon Web Services (AWS) multi-account environment, following prescriptive best practices. AWS Control Tower orchestrates the capabilities of several other AWS services, including AWS Organizations, AWS Service Catalog, and AWS Single Sign-On (AWS SSO), to build a landing zone very quickly. AWS [...]
