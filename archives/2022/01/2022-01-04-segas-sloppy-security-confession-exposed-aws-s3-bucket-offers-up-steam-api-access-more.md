Title: SEGA’s Sloppy Security Confession: Exposed AWS S3 Bucket Offers Up Steam API Access & More
Date: 2022-01-04T20:49:39+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security
Slug: 2022-01-04-segas-sloppy-security-confession-exposed-aws-s3-bucket-offers-up-steam-api-access-more

[Source](https://threatpost.com/sega-security-aws-s3-exposed-steam/177352/){:target="_blank" rel="noopener"}

> SEGA's disclosure underscores a common, potentially catastrophic, flub — misconfigured Amazon Web Services (AWS) S3 buckets. [...]
