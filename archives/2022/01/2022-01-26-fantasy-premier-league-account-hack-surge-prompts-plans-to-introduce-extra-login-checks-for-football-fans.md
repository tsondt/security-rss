Title: Fantasy Premier League account hack surge prompts plans to introduce extra login checks for football fans
Date: 2022-01-26T16:52:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-26-fantasy-premier-league-account-hack-surge-prompts-plans-to-introduce-extra-login-checks-for-football-fans

[Source](https://portswigger.net/daily-swig/fantasy-premier-league-account-hack-surge-prompts-plans-to-introduce-extra-login-checks-for-football-fans){:target="_blank" rel="noopener"}

> FA (belatedly) says OK to 2FA [...]
