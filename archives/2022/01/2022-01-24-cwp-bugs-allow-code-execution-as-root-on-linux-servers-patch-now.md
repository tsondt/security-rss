Title: CWP bugs allow code execution as root on Linux servers, patch now
Date: 2022-01-24T14:34:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux
Slug: 2022-01-24-cwp-bugs-allow-code-execution-as-root-on-linux-servers-patch-now

[Source](https://www.bleepingcomputer.com/news/security/cwp-bugs-allow-code-execution-as-root-on-linux-servers-patch-now/){:target="_blank" rel="noopener"}

> Two security vulnerabilities that impact the Control Web Panel (CWP) software can be chained by unauthenticated attackers to gain remote code execution (RCE) as root on vulnerable Linux servers. [...]
