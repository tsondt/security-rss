Title: Have I Been Pwned warns of DatPiff data breach impacting millions
Date: 2022-01-04T11:22:55-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-04-have-i-been-pwned-warns-of-datpiff-data-breach-impacting-millions

[Source](https://www.bleepingcomputer.com/news/security/have-i-been-pwned-warns-of-datpiff-data-breach-impacting-millions/){:target="_blank" rel="noopener"}

> The cracked passwords for almost 7.5 million DatPiff members are being sold online, and users can check if they are part of the data breach through the Have I Been Pwned notification service. [...]
