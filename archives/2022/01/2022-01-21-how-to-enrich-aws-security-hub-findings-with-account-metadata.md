Title: How to enrich AWS Security Hub findings with account metadata
Date: 2022-01-21T18:46:33+00:00
Author: Siva Rajamani
Category: AWS Security
Tags: Advanced (300);AWS Security Hub;Security, Identity, & Compliance;Amazon EventBridge;AWS Lambda;AWS Organizations;Security
Slug: 2022-01-21-how-to-enrich-aws-security-hub-findings-with-account-metadata

[Source](https://aws.amazon.com/blogs/security/how-to-enrich-aws-security-hub-findings-with-account-metadata/){:target="_blank" rel="noopener"}

> In this blog post, we’ll walk you through how to deploy a solution to enrich AWS Security Hub findings with additional account-related metadata, such as the account name, the Organization Unit (OU) associated with the account, security contact information, and account tags. Account metadata can help you search findings, create insights, and better respond to [...]
