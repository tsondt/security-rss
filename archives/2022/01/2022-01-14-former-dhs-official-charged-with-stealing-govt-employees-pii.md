Title: Former DHS official charged with stealing govt employees' PII
Date: 2022-01-14T15:22:52-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-14-former-dhs-official-charged-with-stealing-govt-employees-pii

[Source](https://www.bleepingcomputer.com/news/security/former-dhs-official-charged-with-stealing-govt-employees-pii/){:target="_blank" rel="noopener"}

> A former Department of Homeland Security acting inspector general pleaded guilty today to stealing confidential and proprietary software and sensitive databases from the US government containing employees' personal identifying information (PII). [...]
