Title: Info-saturated techie builds bug alert service that phones you to warn of new vulns
Date: 2022-01-12T11:02:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-12-info-saturated-techie-builds-bug-alert-service-that-phones-you-to-warn-of-new-vulns

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/12/bugalert_matt_sullivan_interview/){:target="_blank" rel="noopener"}

> Or SMSes, if the idea of midnight robot calls worries you An infosec pro fed up of having to follow tedious Twitter accounts to stay on top of cybersecurity developments has set up a website that phones you if there's a new vuln you really need to know about.... [...]
