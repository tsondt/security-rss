Title: Apple pays out $100k bounty for Safari webcam hack that imperiled victims’ online accounts
Date: 2022-01-27T15:48:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-27-apple-pays-out-100k-bounty-for-safari-webcam-hack-that-imperiled-victims-online-accounts

[Source](https://portswigger.net/daily-swig/apple-pays-out-100k-bounty-for-safari-webcam-hack-that-imperiled-victims-online-accounts){:target="_blank" rel="noopener"}

> Gatekeeper defenses prove no match for uXSS attack [...]
