Title: New Intel chips won't play Blu-ray disks due to SGX deprecation
Date: 2022-01-14T11:46:42-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2022-01-14-new-intel-chips-wont-play-blu-ray-disks-due-to-sgx-deprecation

[Source](https://www.bleepingcomputer.com/news/security/new-intel-chips-wont-play-blu-ray-disks-due-to-sgx-deprecation/){:target="_blank" rel="noopener"}

> Intel has removed support for SGX (software guard extension) in 12th Generation Intel Core 11000 and 12000 processors, rendering modern PCs unable to playback Blu-ray disks in 4K resolution. [...]
