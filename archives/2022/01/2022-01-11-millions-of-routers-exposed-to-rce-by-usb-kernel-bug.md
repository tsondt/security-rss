Title: Millions of Routers Exposed to RCE by USB Kernel Bug
Date: 2022-01-11T12:00:04+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: IoT;Vulnerabilities;Web Security
Slug: 2022-01-11-millions-of-routers-exposed-to-rce-by-usb-kernel-bug

[Source](https://threatpost.com/millions-routers-exposed-bug-usb-module-kcodes-netusb/177506/){:target="_blank" rel="noopener"}

> The high-severity RCE flaw is in the KCodes NetUSB kernel module, used by popular routers from Netgear, TP-Link, DLink, Western Digital, et al. [...]
