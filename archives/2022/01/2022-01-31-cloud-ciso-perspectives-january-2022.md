Title: Cloud CISO Perspectives: January 2022
Date: 2022-01-31T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-01-31-cloud-ciso-perspectives-january-2022

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-january-2022/){:target="_blank" rel="noopener"}

> I’m excited to share our first Cloud CISO Perspectives post of 2022. It's already shaping up to be an eventful year for our industry and we’re only in month one. There’s a lot to recap in this post, including the U.S. government’s recent efforts to address critical security issues, like open source software security and zero trust architectures. We’ve also released new resources from our Google Cybersecurity Action Team like the Cloud Security Megatrends and the Boards of Directors whitepaper on cloud risk governance. Cloud Security Megatrends We’re often asked if the cloud is more secure than on-prem (and why) [...]
