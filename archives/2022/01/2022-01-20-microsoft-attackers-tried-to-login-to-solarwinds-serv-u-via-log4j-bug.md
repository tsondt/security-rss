Title: Microsoft: Attackers Tried to Login to SolarWinds Serv-U Via Log4j Bug
Date: 2022-01-20T18:39:21+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-01-20-microsoft-attackers-tried-to-login-to-solarwinds-serv-u-via-log4j-bug

[Source](https://threatpost.com/microsoft-log4j-attackssolarwinds-serv-u-bug/177824/){:target="_blank" rel="noopener"}

> UPDATE: SolarWinds has fixed a Serv-U bug discovered when attackers used the Log4j flaw to try to log in to the file-sharing software. [...]
