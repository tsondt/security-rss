Title: Nintendo warns of spoofed sites pushing fake Switch discounts
Date: 2022-01-17T12:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-17-nintendo-warns-of-spoofed-sites-pushing-fake-switch-discounts

[Source](https://www.bleepingcomputer.com/news/security/nintendo-warns-of-spoofed-sites-pushing-fake-switch-discounts/){:target="_blank" rel="noopener"}

> Nintendo has warned customers of multiple sites impersonating the Japanese video game company's official website and pretending to sell Nintendo Switch consoles at significant discounts. [...]
