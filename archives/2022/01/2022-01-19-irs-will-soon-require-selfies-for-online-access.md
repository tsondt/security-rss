Title: IRS Will Soon Require Selfies for Online Access
Date: 2022-01-19T17:15:06+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Tax Refund Fraud;Blake Hall;Equifax;id.me;U.S. Internal Revenue Service
Slug: 2022-01-19-irs-will-soon-require-selfies-for-online-access

[Source](https://krebsonsecurity.com/2022/01/irs-will-soon-require-selfies-for-online-access/){:target="_blank" rel="noopener"}

> If you created an online account to manage your tax records with the U.S. Internal Revenue Service (IRS), those login credentials will cease to work later this year. The agency says that by the summer of 2022, the only way to log in to irs.gov will be through ID.me, an online identity verification service that requires applicants to submit copies of bills and identity documents, as well as a live video feed of their faces via a mobile device. The IRS says it will require ID.me for all logins later this summer. McLean, Va.-based ID.me was originally launched in 2010 [...]
