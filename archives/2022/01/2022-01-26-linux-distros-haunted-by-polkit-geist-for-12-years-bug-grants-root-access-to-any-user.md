Title: Linux distros haunted by Polkit-geist for 12+ years: Bug grants root access to any user
Date: 2022-01-26T01:02:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-26-linux-distros-haunted-by-polkit-geist-for-12-years-bug-grants-root-access-to-any-user

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/26/pwnkit_vulnerability_linuix/){:target="_blank" rel="noopener"}

> What happens when argc is zero and a SUID program doesn't care? Let's find out! Linux vendors on Tuesday issued patches for a memory corruption vulnerability in a component called polkit that allows an unprivileged logged-in user to gain full root access on a system in its default configuration.... [...]
