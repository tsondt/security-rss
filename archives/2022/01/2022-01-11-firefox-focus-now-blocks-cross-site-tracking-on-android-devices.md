Title: Firefox Focus now blocks cross-site tracking on Android devices
Date: 2022-01-11T15:42:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-11-firefox-focus-now-blocks-cross-site-tracking-on-android-devices

[Source](https://www.bleepingcomputer.com/news/security/firefox-focus-now-blocks-cross-site-tracking-on-android-devices/){:target="_blank" rel="noopener"}

> Mozilla's Firefox Focus web browser can now protect Android users against cross-site tracking while browsing the Internet by preventing cookies from being used for advertising and monitoring your activity. [...]
