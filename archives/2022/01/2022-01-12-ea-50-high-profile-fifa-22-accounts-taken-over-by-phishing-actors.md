Title: EA: 50 high-profile FIFA 22 accounts taken over by phishing actors
Date: 2022-01-12T04:43:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-12-ea-50-high-profile-fifa-22-accounts-taken-over-by-phishing-actors

[Source](https://www.bleepingcomputer.com/news/security/ea-50-high-profile-fifa-22-accounts-taken-over-by-phishing-actors/){:target="_blank" rel="noopener"}

> Electronic Arts (EA) has published an official response to numerous reports about hacked player accounts, confirming the problem and attributing it to phishing actors. [...]
