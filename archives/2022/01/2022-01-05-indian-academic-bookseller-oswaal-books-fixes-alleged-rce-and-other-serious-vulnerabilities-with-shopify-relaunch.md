Title: Indian academic bookseller Oswaal Books fixes alleged RCE and other serious vulnerabilities with Shopify relaunch
Date: 2022-01-05T13:52:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-05-indian-academic-bookseller-oswaal-books-fixes-alleged-rce-and-other-serious-vulnerabilities-with-shopify-relaunch

[Source](https://portswigger.net/daily-swig/indian-academic-bookseller-oswaal-books-fixes-alleged-rce-and-other-serious-vulnerabilities-with-shopify-relaunch){:target="_blank" rel="noopener"}

> Researcher claims he found RCE, authentication bypass, CSRF flaws [...]
