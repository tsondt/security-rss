Title: Infosec big dogs break out the bubbly over UK government's latest cyber strategy emission
Date: 2022-01-26T11:55:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-26-infosec-big-dogs-break-out-the-bubbly-over-uk-governments-latest-cyber-strategy-emission

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/26/ukgov_latest_cyber_strategy_2030/){:target="_blank" rel="noopener"}

> See that? That's a promise of fat contracts, that is Big industry players have praised the latest cybersecurity strategy emitted by the British government, rubbing their hands with glee at its promises of lucrative public contracts for the rest of the 2020s.... [...]
