Title: Dev corrupts NPM libs 'colors' and 'faker' breaking thousands of apps
Date: 2022-01-09T09:17:39-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-01-09-dev-corrupts-npm-libs-colors-and-faker-breaking-thousands-of-apps

[Source](https://www.bleepingcomputer.com/news/security/dev-corrupts-npm-libs-colors-and-faker-breaking-thousands-of-apps/){:target="_blank" rel="noopener"}

> Users of popular open-source libraries 'colors' and 'faker' were left stunned after they saw their applications, using these libraries, printing gibberish data and breaking. Some surmised if the NPM libraries had been compromised, but it turns out there's more to the story. [...]
