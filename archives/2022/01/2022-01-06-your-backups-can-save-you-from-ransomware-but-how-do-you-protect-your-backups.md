Title: Your backups can save you from ransomware. But how do you protect your backups?
Date: 2022-01-06T18:15:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-01-06-your-backups-can-save-you-from-ransomware-but-how-do-you-protect-your-backups

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/06/your_backups_can_save_you/){:target="_blank" rel="noopener"}

> Immutability, analytics, and a complete lack of trust... Webinar When it comes to cybersecurity, your backup data is no longer your last line of defence.... [...]
