Title: Researchers discover ‘extremely easy’ 2FA bypass in Box cloud management software
Date: 2022-01-18T14:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-18-researchers-discover-extremely-easy-2fa-bypass-in-box-cloud-management-software

[Source](https://portswigger.net/daily-swig/researchers-discover-extremely-easy-2fa-bypass-in-box-cloud-management-software){:target="_blank" rel="noopener"}

> Breaking the Box [...]
