Title: Crypto platform ARBIX flagged as a rugpull, transfers $10 million
Date: 2022-01-05T11:55:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-01-05-crypto-platform-arbix-flagged-as-a-rugpull-transfers-10-million

[Source](https://www.bleepingcomputer.com/news/security/crypto-platform-arbix-flagged-as-a-rugpull-transfers-10-million/){:target="_blank" rel="noopener"}

> Arbix Finance, an audited and supposedly trustworthy yield farming platform, has been flagged as a 'rugpull,' deleting its site, Twitter, and Telegram channel and transferring $10 million worth of deposited cryptocurrency. [...]
