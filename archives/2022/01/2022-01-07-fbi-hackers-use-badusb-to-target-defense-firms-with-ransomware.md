Title: FBI: Hackers use BadUSB to target defense firms with ransomware
Date: 2022-01-07T13:14:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-07-fbi-hackers-use-badusb-to-target-defense-firms-with-ransomware

[Source](https://www.bleepingcomputer.com/news/security/fbi-hackers-use-badusb-to-target-defense-firms-with-ransomware/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned US companies in a recently updated flash alert that the financially motivated FIN7 cybercriminal group targeted the US defense industry with packages containing malicious USB devices to deploy ransomware. [...]
