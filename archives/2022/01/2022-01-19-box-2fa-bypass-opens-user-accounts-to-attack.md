Title: Box 2FA Bypass Opens User Accounts to Attack
Date: 2022-01-19T18:30:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Mobile Security;Vulnerabilities;Web Security
Slug: 2022-01-19-box-2fa-bypass-opens-user-accounts-to-attack

[Source](https://threatpost.com/box-2fa-bypass-accounts-attack/177760/){:target="_blank" rel="noopener"}

> A security bug in the file-sharing cloud app could have allowed attackers using stolen credentials to skate by one-time SMS code verification requirements. [...]
