Title: White House reminds tech giants open source is a national security issue
Date: 2022-01-14T14:04:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-14-white-house-reminds-tech-giants-open-source-is-a-national-security-issue

[Source](https://www.bleepingcomputer.com/news/security/white-house-reminds-tech-giants-open-source-is-a-national-security-issue/){:target="_blank" rel="noopener"}

> The White House wants government and private sector organizations to rally their efforts and resources to secure open-source software and its supply chain after the Log4J vulnerabilities exposed critical infrastructure to threat actors' attacks. [...]
