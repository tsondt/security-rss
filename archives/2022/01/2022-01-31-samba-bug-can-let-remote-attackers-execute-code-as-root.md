Title: Samba bug can let remote attackers execute code as root
Date: 2022-01-31T16:15:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-31-samba-bug-can-let-remote-attackers-execute-code-as-root

[Source](https://www.bleepingcomputer.com/news/security/samba-bug-can-let-remote-attackers-execute-code-as-root/){:target="_blank" rel="noopener"}

> Samba has addressed a critical severity vulnerability that can let attackers gain remote code execution with root privileges on servers running vulnerable software. [...]
