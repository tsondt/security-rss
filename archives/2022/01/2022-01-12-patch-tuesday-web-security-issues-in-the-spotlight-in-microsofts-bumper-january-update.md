Title: Patch Tuesday: Web security issues in the spotlight in Microsoft’s bumper January update
Date: 2022-01-12T15:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-12-patch-tuesday-web-security-issues-in-the-spotlight-in-microsofts-bumper-january-update

[Source](https://portswigger.net/daily-swig/patch-tuesday-web-security-issues-in-the-spotlight-in-microsofts-bumper-january-update){:target="_blank" rel="noopener"}

> ‘Wormable’ flaw in HTTP Protocol Stack causes concern [...]
