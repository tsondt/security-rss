Title: SEC Filing Reveals Fortune 500 Firm Targeted in Ransomware Attack
Date: 2022-01-20T14:27:48+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Hacks;Malware
Slug: 2022-01-20-sec-filing-reveals-fortune-500-firm-targeted-in-ransomware-attack

[Source](https://threatpost.com/fortune-500-firm-ransomware/177787/){:target="_blank" rel="noopener"}

> R.R. Donnelly, the integrated services company, confirmed a ‘systems intrusion’ that occurred in late December and is still under investigation. [...]
