Title: Dark Souls 3 Servers Shut Down Due to Critical RCE Bug
Date: 2022-01-24T20:26:32+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-01-24-dark-souls-3-servers-shut-down-due-to-critical-rce-bug

[Source](https://threatpost.com/dark-souls-servers-down-rce-bug/177896/){:target="_blank" rel="noopener"}

> The bug can allow attackers to remotely execute code on gamers’ computers. The devs temporarily deactivated PvP servers across multiple affected versions. [...]
