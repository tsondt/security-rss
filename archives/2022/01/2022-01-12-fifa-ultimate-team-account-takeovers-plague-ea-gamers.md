Title: FIFA Ultimate Team Account Takeovers Plague EA Gamers
Date: 2022-01-12T13:21:33+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Hacks;Web Security
Slug: 2022-01-12-fifa-ultimate-team-account-takeovers-plague-ea-gamers

[Source](https://threatpost.com/phishers-ea-gamers/177575/){:target="_blank" rel="noopener"}

> Electronic Arts blamed “human error” after attackers compromised customer support and took over and drained some of the top FIFA Ultimate Team player accounts. [...]
