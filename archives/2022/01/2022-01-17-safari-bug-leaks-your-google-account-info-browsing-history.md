Title: Safari bug leaks your Google account info, browsing history
Date: 2022-01-17T08:47:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2022-01-17-safari-bug-leaks-your-google-account-info-browsing-history

[Source](https://www.bleepingcomputer.com/news/security/safari-bug-leaks-your-google-account-info-browsing-history/){:target="_blank" rel="noopener"}

> There's a problem with the implementation of the IndexedDB API in Safari's WebKit engine, which could result in leaking browsing histories and even user identities to anyone exploiting the flaw. [...]
