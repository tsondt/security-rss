Title: Continuous compliance monitoring using custom audit controls and frameworks with AWS Audit Manager
Date: 2022-01-18T16:10:31+00:00
Author: Deenadayaalan Thirugnanasambandam
Category: AWS Security
Tags: AWS Audit Manager;Intermediate (200);Learning Levels;Security, Identity, & Compliance;Technical How-to;audit management;Compliance;continuously audit your AWS usage;controls;enterprise controls;risk
Slug: 2022-01-18-continuous-compliance-monitoring-using-custom-audit-controls-and-frameworks-with-aws-audit-manager

[Source](https://aws.amazon.com/blogs/security/continuous-compliance-monitoring-using-custom-audit-controls-and-frameworks-with-aws-audit-manager/){:target="_blank" rel="noopener"}

> For most customers today, security compliance auditing can be a very cumbersome and costly process. This activity within a security program often comes with a dependency on third party audit firms and robust security teams, to periodically assess risk and raise compliance gaps aligned with applicable industry requirements. Due to the nature of how audits [...]
