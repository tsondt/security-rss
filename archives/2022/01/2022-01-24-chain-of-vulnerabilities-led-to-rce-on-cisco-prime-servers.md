Title: Chain of vulnerabilities led to RCE on Cisco Prime servers
Date: 2022-01-24T13:05:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-24-chain-of-vulnerabilities-led-to-rce-on-cisco-prime-servers

[Source](https://portswigger.net/daily-swig/chain-of-vulnerabilities-led-to-rce-on-cisco-prime-servers){:target="_blank" rel="noopener"}

> Full chain exploit ready for Prime time [...]
