Title: Scary Fraud Ensues When ID Theft & Usury Collide
Date: 2022-01-25T19:48:40+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Web Fraud 2.0;Buckley LLP;installment loan;Leslie Bailey;Mountain Summit Financial;Native American Financial Services Association;payday loan;Public Justice;tribal lenders
Slug: 2022-01-25-scary-fraud-ensues-when-id-theft-usury-collide

[Source](https://krebsonsecurity.com/2022/01/scary-fraud-ensues-when-id-theft-usury-collide/){:target="_blank" rel="noopener"}

> What’s worse than finding out that identity thieves took out a 546 percent interest payday loan in your name? How about a 900 percent interest loan? Or how about not learning of the fraudulent loan until it gets handed off to collection agents? One reader’s nightmare experience spotlights what can happen when ID thieves and hackers start targeting online payday lenders. The reader who shared this story (and copious documentation to go with it) asked to have his real name omitted to avoid encouraging further attacks against his identity. So we’ll just call him “Jim.” Last May, someone applied for [...]
