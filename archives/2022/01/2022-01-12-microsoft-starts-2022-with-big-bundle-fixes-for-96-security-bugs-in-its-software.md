Title: Microsoft starts 2022 with big bundle fixes for 96 security bugs in its software
Date: 2022-01-12T01:14:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-12-microsoft-starts-2022-with-big-bundle-fixes-for-96-security-bugs-in-its-software

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/12/january_patch_tuesday/){:target="_blank" rel="noopener"}

> Nothing is certain except death, taxes, and programming errors Patch Tuesday The new year brings the same old chore of shoring up Microsoft software. For its first Patch Tuesday of 2022, Redmond has bestowed 96 new CVEs affecting its Windows products.... [...]
