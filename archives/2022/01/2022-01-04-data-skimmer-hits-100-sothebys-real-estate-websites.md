Title: Data Skimmer Hits 100+ Sotheby’s Real-Estate Websites
Date: 2022-01-04T20:33:40+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-01-04-data-skimmer-hits-100-sothebys-real-estate-websites

[Source](https://threatpost.com/data-skimmer-sothebys-real-estate-websites/177347/){:target="_blank" rel="noopener"}

> The campaign was an opportunistic supply-chain attack abusing a weaponized cloud video player. [...]
