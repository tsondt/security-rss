Title: FIN7 Mails Malicious USB Sticks to Drop Ransomware
Date: 2022-01-11T17:06:11+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware
Slug: 2022-01-11-fin7-mails-malicious-usb-sticks-to-drop-ransomware

[Source](https://threatpost.com/fin7-mailing-malicious-usb-sticks-ransomware/177541/){:target="_blank" rel="noopener"}

> The FBI warned that attackers are impersonating Health & Human Services and/or Amazon to mail BadUSB-poisoned USB devices to targets in transportation, insurance & defense. [...]
