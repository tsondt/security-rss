Title: Merck Wins Insurance Lawsuit re NotPetya Attack
Date: 2022-01-25T15:35:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;cyberattack;data destruction;insurance;Russia
Slug: 2022-01-25-merck-wins-insurance-lawsuit-re-notpetya-attack

[Source](https://www.schneier.com/blog/archives/2022/01/merck-wins-insurance-lawsuit-re-notpetya-attack.html){:target="_blank" rel="noopener"}

> The insurance company Ace American has to pay for the losses: On 6th December 2021, the New Jersey Superior Court granted partial summary judgment (attached) in favour of Merck and International Indemnity, declaring that the War or Hostile Acts exclusion was inapplicable to the dispute. Merck suffered US$1.4 billion in business interruption losses from the Notpetya cyber attack of 2017 which were claimed against “all risks” property re/insurance policies providing coverage for losses resulting from destruction or corruption of computer data and software. The parties disputed whether the Notpetya malware which affected Merck’s computers in 2017 was an instrument of [...]
