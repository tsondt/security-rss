Title: Attackers now actively targeting critical SonicWall RCE bug
Date: 2022-01-24T16:48:56-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-24-attackers-now-actively-targeting-critical-sonicwall-rce-bug

[Source](https://www.bleepingcomputer.com/news/security/attackers-now-actively-targeting-critical-sonicwall-rce-bug/){:target="_blank" rel="noopener"}

> A critical severity vulnerability impacting SonicWall's Secure Mobile Access (SMA) gateways addressed last month is now targeted in ongoing exploitation attempts. [...]
