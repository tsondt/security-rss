Title: Apple’s Private Relay Is Being Blocked
Date: 2022-01-11T15:09:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;anonymity;Apple;encryption;privacy;T-Mobile;VPN;web privacy
Slug: 2022-01-11-apples-private-relay-is-being-blocked

[Source](https://www.schneier.com/blog/archives/2022/01/apples-private-relay-is-being-blocked.html){:target="_blank" rel="noopener"}

> Some European cell phone carriers, and now T-Mobile, are blocking Apple’s Private Relay anonymous browsing feature. This could be an interesting battle to watch. Slashdot thread. [...]
