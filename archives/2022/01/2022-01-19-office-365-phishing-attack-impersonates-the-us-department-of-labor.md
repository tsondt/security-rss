Title: Office 365 phishing attack impersonates the US Department of Labor
Date: 2022-01-19T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-19-office-365-phishing-attack-impersonates-the-us-department-of-labor

[Source](https://www.bleepingcomputer.com/news/security/office-365-phishing-attack-impersonates-the-us-department-of-labor/){:target="_blank" rel="noopener"}

> A new phishing campaign impersonating the United States Department of Labor asks recipients to submit bids to steal Office 365 credentials. [...]
