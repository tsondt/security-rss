Title: Cyber espionage campaign targets renewable energy companies
Date: 2022-01-17T11:38:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-17-cyber-espionage-campaign-targets-renewable-energy-companies

[Source](https://www.bleepingcomputer.com/news/security/cyber-espionage-campaign-targets-renewable-energy-companies/){:target="_blank" rel="noopener"}

> A large-scale cyber-espionage campaign targeting primarily renewable energy and industrial technology organizations have been discovered to be active since at least 2019, targeting over fifteen entities worldwide. [...]
