Title: Privacy slalom: Human rights, media orgs offer OPSEC warning to Winter Olympics attendees
Date: 2022-01-28T11:40:54+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-28-privacy-slalom-human-rights-media-orgs-offer-opsec-warning-to-winter-olympics-attendees

[Source](https://portswigger.net/daily-swig/privacy-slalom-human-rights-media-orgs-offer-opsec-warning-to-winter-olympics-attendees){:target="_blank" rel="noopener"}

> Behind the spectacle of Beijing 2022, visitors’ digital freedoms may be left out in the cold [...]
