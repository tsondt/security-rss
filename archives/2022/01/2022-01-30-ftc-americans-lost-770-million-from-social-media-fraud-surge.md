Title: FTC: Americans lost $770 million from social media fraud surge
Date: 2022-01-30T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-30-ftc-americans-lost-770-million-from-social-media-fraud-surge

[Source](https://www.bleepingcomputer.com/news/security/ftc-americans-lost-770-million-from-social-media-fraud-surge/){:target="_blank" rel="noopener"}

> Americans are increasingly targeted by scammers on social media, according to tens of thousands of reports received by the US Federal Trade Commission (FTC) in 2021. [...]
