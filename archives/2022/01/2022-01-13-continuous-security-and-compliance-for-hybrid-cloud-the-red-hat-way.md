Title: Continuous security and compliance for hybrid cloud, the Red Hat way
Date: 2022-01-13T18:00:09+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2022-01-13-continuous-security-and-compliance-for-hybrid-cloud-the-red-hat-way

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/13/red_hat_devsecops_hybrid_cloud/){:target="_blank" rel="noopener"}

> Tune in, turn on, run in the background, using Red Hat DevSecOps framework Paid feature Assessing what can go wrong in a hybrid cloud environment can be daunting. Applications can be poorly coded, security vulnerabilities may be present but hard to detect or manage, and applications and the IT infrastructure may not be designed for DevSecOps.... [...]
