Title: Intel fails to get Spectre, Meltdown chip flaw class-action super-suit tossed out
Date: 2022-01-28T01:18:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-28-intel-fails-to-get-spectre-meltdown-chip-flaw-class-action-super-suit-tossed-out

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/28/intel_spectre_lawsuit/){:target="_blank" rel="noopener"}

> Cheesed-off customers have 'alleged enough facts at this stage' to allow legal battle to continue, says judge Intel will have to defend itself against claims that the semiconductor goliath knew its microprocessors were defective and failed to tell customers.... [...]
