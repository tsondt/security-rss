Title: Privacy is for paedophiles, UK government seems to be saying while spending £500k demonising online chat encryption
Date: 2022-01-20T15:06:24+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-20-privacy-is-for-paedophiles-uk-government-seems-to-be-saying-while-spending-500k-demonising-online-chat-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/20/no_place_hide_campaign_anti_e2ee_ukgov/){:target="_blank" rel="noopener"}

> So far we've got a pisspoor video and... er, that's it Opinion The British government's PR campaign to destroy popular support for end-to-end encryption on messaging platforms has kicked off, under the handle "No Place To Hide", and it's as broad as any previous attack on the safety-guaranteeing technology.... [...]
