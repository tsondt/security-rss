Title: Over 20,000 data center management systems exposed to hackers
Date: 2022-01-29T11:08:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-29-over-20000-data-center-management-systems-exposed-to-hackers

[Source](https://www.bleepingcomputer.com/news/security/over-20-000-data-center-management-systems-exposed-to-hackers/){:target="_blank" rel="noopener"}

> Researchers have found over 20,000 instances of publicly exposed data center infrastructure management (DCIM) software that monitor devices, HVAC control systems, and power distribution units, which could be used for a range of catastrophic attacks. [...]
