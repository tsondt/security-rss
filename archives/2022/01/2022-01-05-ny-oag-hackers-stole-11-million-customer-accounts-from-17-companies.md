Title: NY OAG: Hackers stole 1.1 million customer accounts from 17 companies
Date: 2022-01-05T12:42:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-05-ny-oag-hackers-stole-11-million-customer-accounts-from-17-companies

[Source](https://www.bleepingcomputer.com/news/security/ny-oag-hackers-stole-11-million-customer-accounts-from-17-companies/){:target="_blank" rel="noopener"}

> The New York State Office of the Attorney General (NY OAG) has warned 17 well-known companies that roughly 1.1 million of their customers have had their user accounts compromised in credential stuffing attacks. [...]
