Title: FCC wants new data breach reporting rules for telecom carriers
Date: 2022-01-13T16:39:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-13-fcc-wants-new-data-breach-reporting-rules-for-telecom-carriers

[Source](https://www.bleepingcomputer.com/news/security/fcc-wants-new-data-breach-reporting-rules-for-telecom-carriers/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) has proposed more rigorous data breach reporting requirements for telecom carriers in response to breaches that recently hit the telecommunications industry. [...]
