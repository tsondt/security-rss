Title: FTC to Go After Companies that Ignore Log4j
Date: 2022-01-05T19:00:03+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-01-05-ftc-to-go-after-companies-that-ignore-log4j

[Source](https://threatpost.com/ftc-pursue-companies-log4j/177368/){:target="_blank" rel="noopener"}

> Companies that fail to protect consumer data from Log4J attacks are at risk of facing Equifax-esque legal action and fines, the FTC warned. [...]
