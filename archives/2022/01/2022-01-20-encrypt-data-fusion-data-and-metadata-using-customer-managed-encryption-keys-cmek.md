Title: Encrypt Data Fusion data and metadata using Customer Managed Encryption Keys (CMEK)
Date: 2022-01-20T17:00:00+00:00
Author: Bhooshan Mogal
Category: GCP Security
Tags: Developers & Practitioners;Identity & Security;Data Analytics
Slug: 2022-01-20-encrypt-data-fusion-data-and-metadata-using-customer-managed-encryption-keys-cmek

[Source](https://cloud.google.com/blog/products/data-analytics/general-availability-for-cmek-in-data-fusion/){:target="_blank" rel="noopener"}

> We are pleased to announce the general availability of Customer Managed Encryption Keys (CMEK) integration for Cloud Data Fusion. CMEK enables encryption of both user data and metadata at rest with a key that you can control through Cloud Key Management Service (KMS). This capability will help meet the security, privacy and compliance requirements of CDF customers (particularly in regulated industries) for mission-critical workloads. Data Fusion already supported encrypting all user data generated on popular Google Cloud services such as Cloud Storage, BigQuery, Cloud Spanner with CMEK. This release takes it a step further by allowing customers to use their [...]
