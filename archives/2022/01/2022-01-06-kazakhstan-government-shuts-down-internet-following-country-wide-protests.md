Title: Kazakhstan government shuts down internet following country-wide protests
Date: 2022-01-06T13:07:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-06-kazakhstan-government-shuts-down-internet-following-country-wide-protests

[Source](https://portswigger.net/daily-swig/kazakhstan-government-shuts-down-internet-following-country-wide-protests){:target="_blank" rel="noopener"}

> This isn’t the first time the landlocked nation has restricted web access for citizens [...]
