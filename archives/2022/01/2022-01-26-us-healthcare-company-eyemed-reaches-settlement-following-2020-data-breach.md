Title: US healthcare company EyeMed reaches settlement following 2020 data breach
Date: 2022-01-26T14:59:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-26-us-healthcare-company-eyemed-reaches-settlement-following-2020-data-breach

[Source](https://portswigger.net/daily-swig/us-healthcare-company-eyemed-reaches-settlement-following-2020-data-breach){:target="_blank" rel="noopener"}

> Vision benefits provider agrees to $600,000 [...]
