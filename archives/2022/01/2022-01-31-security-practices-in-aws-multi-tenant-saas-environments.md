Title: Security practices in AWS multi-tenant SaaS environments
Date: 2022-01-31T20:20:51+00:00
Author: Keith P
Category: AWS Security
Tags: Expert (400);Security;Security, Identity, & Compliance;Cognito;IAM;JWT;SaaS;SaaS lens;Tenant isolation;VPC
Slug: 2022-01-31-security-practices-in-aws-multi-tenant-saas-environments

[Source](https://aws.amazon.com/blogs/security/security-practices-in-aws-multi-tenant-saas-environments/){:target="_blank" rel="noopener"}

> Securing software-as-a-service (SaaS) applications is a top priority for all application architects and developers. Doing so in an environment shared by multiple tenants can be even more challenging. Identity frameworks and concepts can take time to understand, and forming tenant isolation in these environments requires deep understanding of different tools and services. While security is [...]
