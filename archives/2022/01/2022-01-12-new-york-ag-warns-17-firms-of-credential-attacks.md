Title: New York AG Warns 17 Firms of Credential Attacks
Date: 2022-01-12T18:11:53+00:00
Author: Threatpost
Category: Threatpost
Tags: Sponsored
Slug: 2022-01-12-new-york-ag-warns-17-firms-of-credential-attacks

[Source](https://threatpost.com/ny-ag-credential-attacks/177584/){:target="_blank" rel="noopener"}

> Sponsored: Password security is highlighted in attorney general warning to New York state businesses. [...]
