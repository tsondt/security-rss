Title: Critical Cisco StarOS Bug Grants Root Access via Debug Mode
Date: 2022-01-20T19:35:29+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-01-20-critical-cisco-staros-bug-grants-root-access-via-debug-mode

[Source](https://threatpost.com/critical-cisco-staros-bug-root-access-debug-mode/177832/){:target="_blank" rel="noopener"}

> Cisco issued a critical fix for a flaw in its Cisco RCM for Cisco StarOS Software that could give attackers RCE on the application with root-level privileges. [...]
