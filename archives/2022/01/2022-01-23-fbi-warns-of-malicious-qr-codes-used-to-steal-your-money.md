Title: FBI warns of malicious QR codes used to steal your money
Date: 2022-01-23T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-23-fbi-warns-of-malicious-qr-codes-used-to-steal-your-money

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-malicious-qr-codes-used-to-steal-your-money/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned Americans this week that cybercriminals are using maliciously crafted Quick Response (QR) codes to steal their credentials and financial info. [...]
