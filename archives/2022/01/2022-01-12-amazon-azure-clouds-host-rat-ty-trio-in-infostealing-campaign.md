Title: Amazon, Azure Clouds Host RAT-ty Trio in Infostealing Campaign
Date: 2022-01-12T21:04:58+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Malware
Slug: 2022-01-12-amazon-azure-clouds-host-rat-ty-trio-in-infostealing-campaign

[Source](https://threatpost.com/amazon-azure-clouds-rat-infostealing/177606/){:target="_blank" rel="noopener"}

> A cloudy campaign delivers commodity remote-access trojans to steal information and execute code. [...]
