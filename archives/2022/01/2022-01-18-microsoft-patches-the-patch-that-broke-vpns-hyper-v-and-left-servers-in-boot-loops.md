Title: Microsoft patches the patch that broke VPNs, Hyper-V, and left servers in boot loops
Date: 2022-01-18T11:34:15+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-01-18-microsoft-patches-the-patch-that-broke-vpns-hyper-v-and-left-servers-in-boot-loops

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/18/patching_patch_tuesday/){:target="_blank" rel="noopener"}

> Testing? Isn't that what users are for? Microsoft has patched the patch that broke chunks of Windows and emitted fixes for a Patch Tuesday cock-up that left servers rebooting and VPNs disconnected.... [...]
