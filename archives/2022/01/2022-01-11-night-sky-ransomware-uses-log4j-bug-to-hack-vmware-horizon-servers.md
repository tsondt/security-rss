Title: Night Sky ransomware uses Log4j bug to hack VMware Horizon servers
Date: 2022-01-11T06:24:43-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-01-11-night-sky-ransomware-uses-log4j-bug-to-hack-vmware-horizon-servers

[Source](https://www.bleepingcomputer.com/news/security/night-sky-ransomware-uses-log4j-bug-to-hack-vmware-horizon-servers/){:target="_blank" rel="noopener"}

> The Night Sky ransomware gang has started to exploit the critical CVE-2021-4422 vulnerability in the Log4j logging library, also known as Log4Shell, to gain access to VMware Horizon systems. [...]
