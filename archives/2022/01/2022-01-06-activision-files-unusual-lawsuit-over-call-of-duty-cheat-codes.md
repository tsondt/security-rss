Title: Activision Files Unusual Lawsuit over Call of Duty Cheat Codes
Date: 2022-01-06T19:48:04+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2022-01-06-activision-files-unusual-lawsuit-over-call-of-duty-cheat-codes

[Source](https://threatpost.com/activision-lawsuit-call-of-duty-cheat-codes/177443/){:target="_blank" rel="noopener"}

> Activision is suing to shut down the EngineOwning cheat-code site and hold individual developers and coders liable for damages. [...]
