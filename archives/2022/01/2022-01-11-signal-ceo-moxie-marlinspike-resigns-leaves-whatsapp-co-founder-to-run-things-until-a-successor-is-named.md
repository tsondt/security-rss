Title: Signal CEO Moxie Marlinspike resigns, leaves WhatsApp co-founder to run things until a successor is named
Date: 2022-01-11T01:02:44+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-11-signal-ceo-moxie-marlinspike-resigns-leaves-whatsapp-co-founder-to-run-things-until-a-successor-is-named

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/11/signal_ceo_moxie_marlinspike_resigns/){:target="_blank" rel="noopener"}

> Departure comes as app courts controversy by integrating private cryptocurrency scheme Moxie Marlinspike, the creator of the Signal secure messaging app, on Monday announced his resignation as CEO of the company.... [...]
