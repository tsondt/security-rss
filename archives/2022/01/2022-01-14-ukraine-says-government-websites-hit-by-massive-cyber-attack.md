Title: Ukraine says government websites hit by “massive cyber attack”
Date: 2022-01-14T15:55:47+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;Policy;cyberattack;Cyberwarfare;hacking;Russa;Ukraine
Slug: 2022-01-14-ukraine-says-government-websites-hit-by-massive-cyber-attack

[Source](https://arstechnica.com/?p=1825915){:target="_blank" rel="noopener"}

> Enlarge / A Ukrainian Military Forces serviceman watches through a spyglass in a trench on the frontline with Russia-backed separatists near Avdiivka, southeastern Ukraine, on January 9, 2022. (credit: Anatolii Stepanov | Getty Images) Ukraine said it was the target of a “massive cyber attack” after about 70 government websites ceased functioning. On Friday morning targets included websites of the ministerial cabinet, the foreign, education, agriculture, emergency, energy, veterans affairs, and environment ministries. Also out of service were the websites of the state treasury and the Diia electronic public services platform, where vaccination certificates and electronic passports are stored. “Ukrainians! [...]
