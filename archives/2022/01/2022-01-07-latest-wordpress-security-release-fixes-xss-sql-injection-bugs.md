Title: Latest WordPress security release fixes XSS, SQL injection bugs
Date: 2022-01-07T15:14:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-07-latest-wordpress-security-release-fixes-xss-sql-injection-bugs

[Source](https://portswigger.net/daily-swig/latest-wordpress-security-release-fixes-xss-sql-injection-bugs){:target="_blank" rel="noopener"}

> Quartet of software flaws addressed ahead of next major release of popular CMS [...]
