Title: McAfee and FireEye rename themselves ‘Trellix’
Date: 2022-01-20T07:01:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-01-20-mcafee-and-fireeye-rename-themselves-trellix

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/20/trellix_mcafee_fireye_logowatch/){:target="_blank" rel="noopener"}

> To evoke support for growing things, not the 1990s vendor of web-pages-made-easy-ware LogoWatch Newly combined security outfits McAfee and FireEye have revealed a new name: "Trellix".... [...]
