Title: Umbrella company Parasol Group confirms cyber attack as 'root cause' of prolonged network outage
Date: 2022-01-17T13:28:11+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-01-17-umbrella-company-parasol-group-confirms-cyber-attack-as-root-cause-of-prolonged-network-outage

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/17/umbrella_company_parasol_group_confirms/){:target="_blank" rel="noopener"}

> 'Malicious activity on our network' spotted, says CEO, as some contractors say they've still not been paid Umbrella company Parasol Group has confirmed why it shut down part of its IT last week: it found unauthorised activity from an intruder.... [...]
