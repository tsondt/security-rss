Title: DHL dethrones Microsoft as most imitated brand in phishing attacks
Date: 2022-01-17T12:45:25-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-17-dhl-dethrones-microsoft-as-most-imitated-brand-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/dhl-dethrones-microsoft-as-most-imitated-brand-in-phishing-attacks/){:target="_blank" rel="noopener"}

> DHL was the most imitated brand in phishing campaigns throughout Q4 2021, pushing Microsoft to second place, and Google to fourth. [...]
