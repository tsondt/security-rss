Title: Apple fixes doorLock bug that can disable iPhones and iPads
Date: 2022-01-12T16:45:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-12-apple-fixes-doorlock-bug-that-can-disable-iphones-and-ipads

[Source](https://www.bleepingcomputer.com/news/security/apple-fixes-doorlock-bug-that-can-disable-iphones-and-ipads/){:target="_blank" rel="noopener"}

> Apple has released security updates to address a persistent denial of service (DoS) dubbed doorLock that would altogether disable iPhones and iPads running HomeKit on iOS 14.7 and later. [...]
