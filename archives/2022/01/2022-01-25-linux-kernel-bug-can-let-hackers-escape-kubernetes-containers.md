Title: Linux kernel bug can let hackers escape Kubernetes containers
Date: 2022-01-25T11:56:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-25-linux-kernel-bug-can-let-hackers-escape-kubernetes-containers

[Source](https://www.bleepingcomputer.com/news/security/linux-kernel-bug-can-let-hackers-escape-kubernetes-containers/){:target="_blank" rel="noopener"}

> A vulnerability affecting Linux kernel and tracked as CVE-2022-0185 can be used to escape Kubernetes containers, giving access to resources on the host system. [...]
