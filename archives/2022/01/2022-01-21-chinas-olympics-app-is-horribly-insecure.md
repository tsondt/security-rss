Title: China’s Olympics App Is Horribly Insecure
Date: 2022-01-21T12:06:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;China;data collection;encryption;privacy;sports;surveillance
Slug: 2022-01-21-chinas-olympics-app-is-horribly-insecure

[Source](https://www.schneier.com/blog/archives/2022/01/chinas-olympics-app-is-horribly-insecure.html){:target="_blank" rel="noopener"}

> China is mandating that athletes download and use a health and travel app when they attend the Winter Olympics next month. Citizen Lab examined the app and found it riddled with security holes. Key Findings: MY2022, an app mandated for use by all attendees of the 2022 Olympic Games in Beijing, has a simple but devastating flaw where encryption protecting users’ voice audio and file transfers can be trivially sidestepped. Health customs forms which transmit passport details, demographic information, and medical and travel history are also vulnerable. Server responses can also be spoofed, allowing an attacker to display fake instructions [...]
