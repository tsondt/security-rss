Title: Orca Security tells AWS fail tale with a happy ending
Date: 2022-01-13T21:02:44+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-13-orca-security-tells-aws-fail-tale-with-a-happy-ending

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/13/orca_security_tells_aws_fail/){:target="_blank" rel="noopener"}

> Those critical AWS flaws that exposed data and broke tenant separation? All fixed! Two serious security vulnerabilities were recently found in AWS services, but because they were responsibly reported and the cloud biz responded quickly, no harm appears to have been done.... [...]
