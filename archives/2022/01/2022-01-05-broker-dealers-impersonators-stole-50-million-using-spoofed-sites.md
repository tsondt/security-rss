Title: Broker-dealers impersonators stole $50 million using spoofed sites
Date: 2022-01-05T17:42:25-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-05-broker-dealers-impersonators-stole-50-million-using-spoofed-sites

[Source](https://www.bleepingcomputer.com/news/security/broker-dealers-impersonators-stole-50-million-using-spoofed-sites/){:target="_blank" rel="noopener"}

> A California man confirmed his role in a large-scale and long-running Internet-based fraud scheme that allowed him and other fraudsters to siphon roughly $50 million from dozens of investors over eight years, between 2012 to October 2020. [...]
