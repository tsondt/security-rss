Title: BotenaGo Botnet Code Leaked to GitHub, Impacting Millions of Devices
Date: 2022-01-27T17:19:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: IoT;Malware;Web Security
Slug: 2022-01-27-botenago-botnet-code-leaked-to-github-impacting-millions-of-devices

[Source](https://threatpost.com/botenago-botnet-code-leaked-to-github/178059/){:target="_blank" rel="noopener"}

> The malware had already put millions of routers and IoT devices at risk, and now any noob can have at it. [...]
