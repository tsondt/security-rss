Title: Telegram is a hotspot for the sale of stolen financial accounts
Date: 2022-01-18T16:39:50-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-18-telegram-is-a-hotspot-for-the-sale-of-stolen-financial-accounts

[Source](https://www.bleepingcomputer.com/news/security/telegram-is-a-hotspot-for-the-sale-of-stolen-financial-accounts/){:target="_blank" rel="noopener"}

> Telegram is increasingly abused by cybercriminals to set up underground channels to sell stolen financial details to pseudonymous users. [...]
