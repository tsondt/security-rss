Title: Google Drive flags nearly empty files for 'copyright infringement'
Date: 2022-01-25T03:08:02-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-01-25-google-drive-flags-nearly-empty-files-for-copyright-infringement

[Source](https://www.bleepingcomputer.com/news/security/google-drive-flags-nearly-empty-files-for-copyright-infringement/){:target="_blank" rel="noopener"}

> Users were left startled as Google Drive's automated detection systems flagged a nearly empty file for copyright infringement. The file, according to one Drive user, contained nothing other than just the digit "1" within. [...]
