Title: Moodle e-learning platform patches session hijack bug that led to pre-auth RCE
Date: 2022-01-12T12:32:33+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-12-moodle-e-learning-platform-patches-session-hijack-bug-that-led-to-pre-auth-rce

[Source](https://portswigger.net/daily-swig/moodle-e-learning-platform-patches-session-hijack-bug-that-led-to-pre-auth-rce){:target="_blank" rel="noopener"}

> Researchers disclose second critical flaw in authentication plugin [...]
