Title: VMware: Patch Horizon servers against ongoing Log4j attacks!
Date: 2022-01-25T16:19:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-25-vmware-patch-horizon-servers-against-ongoing-log4j-attacks

[Source](https://www.bleepingcomputer.com/news/security/vmware-patch-horizon-servers-against-ongoing-log4j-attacks/){:target="_blank" rel="noopener"}

> VMware is urging customers to patch critical Log4j security vulnerabilities impacting Internet-exposed VMware Horizon servers targeted in ongoing attacks. [...]
