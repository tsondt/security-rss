Title: Spyware Blitzes Compromise, Cannibalize ICS Networks
Date: 2022-01-21T14:10:07+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-01-21-spyware-blitzes-compromise-cannibalize-ics-networks

[Source](https://threatpost.com/spyware-blitzes-compromise-cannibalize-ics-networks/177851/){:target="_blank" rel="noopener"}

> The brief spearphishing campaigns spread malware and use compromised networks to steal credentials that can be sold or used to commit financial fraud. [...]
