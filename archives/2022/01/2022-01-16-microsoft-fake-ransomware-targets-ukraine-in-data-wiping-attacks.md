Title: Microsoft: Fake ransomware targets Ukraine in data-wiping attacks
Date: 2022-01-16T13:32:35-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-01-16-microsoft-fake-ransomware-targets-ukraine-in-data-wiping-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fake-ransomware-targets-ukraine-in-data-wiping-attacks/){:target="_blank" rel="noopener"}

> Microsoft is warning of destructive data-wiping malware disguised as ransomware being used in attacks against multiple organizations in Ukraine. [...]
