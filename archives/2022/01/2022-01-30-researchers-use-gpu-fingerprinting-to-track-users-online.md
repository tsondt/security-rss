Title: Researchers use GPU fingerprinting to track users online
Date: 2022-01-30T10:12:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-30-researchers-use-gpu-fingerprinting-to-track-users-online

[Source](https://www.bleepingcomputer.com/news/security/researchers-use-gpu-fingerprinting-to-track-users-online/){:target="_blank" rel="noopener"}

> A team of researchers from French, Israeli, and Australian universities has explored the possibility of using people's GPUs to create unique fingerprints and use them for persistent web tracking. [...]
