Title: Google Voice Authentication Scam Leaves Victims on the Hook
Date: 2022-01-06T17:28:43+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Mobile Security;Web Security
Slug: 2022-01-06-google-voice-authentication-scam-leaves-victims-on-the-hook

[Source](https://threatpost.com/google-voice-authentication-scam/177421/){:target="_blank" rel="noopener"}

> The FBI is seeing so much activity around malicious Google Voice activity, where victims are associated with fraudulent virtual phone numbers, that it sent out an alert this week. [...]
