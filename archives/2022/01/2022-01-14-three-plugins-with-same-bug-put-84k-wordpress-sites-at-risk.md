Title: Three Plugins with Same Bug Put 84K WordPress Sites at Risk
Date: 2022-01-14T14:07:36+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-01-14-three-plugins-with-same-bug-put-84k-wordpress-sites-at-risk

[Source](https://threatpost.com/plugins-vulnerability-84k-wordpress-sites/177654/){:target="_blank" rel="noopener"}

> Researchers discovered vulnerabilities that can allow for full site takeover in login and e-commerce add-ons for the popular website-building platform. [...]
