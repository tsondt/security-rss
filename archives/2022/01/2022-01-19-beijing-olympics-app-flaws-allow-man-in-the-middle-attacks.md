Title: Beijing Olympics App Flaws Allow Man-in-the-Middle Attacks
Date: 2022-01-19T13:36:34+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Privacy;Vulnerabilities;Web Security
Slug: 2022-01-19-beijing-olympics-app-flaws-allow-man-in-the-middle-attacks

[Source](https://threatpost.com/beijing-olympics-app-flaws-allow-man-in-the-middle-attacks/177748/){:target="_blank" rel="noopener"}

> Attackers can access audio and files uploaded to the MY2022 mobile app required for use by all winter games attendees – including personal health details. [...]
