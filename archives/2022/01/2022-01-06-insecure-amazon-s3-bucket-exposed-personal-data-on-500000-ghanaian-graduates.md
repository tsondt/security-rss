Title: Insecure Amazon S3 bucket exposed personal data on 500,000 Ghanaian graduates
Date: 2022-01-06T10:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-06-insecure-amazon-s3-bucket-exposed-personal-data-on-500000-ghanaian-graduates

[Source](https://portswigger.net/daily-swig/insecure-amazon-s3-bucket-exposed-personal-data-on-500-000-ghanaian-graduates){:target="_blank" rel="noopener"}

> Cloud storage misconfiguration left sensitive data openly accessible [...]
