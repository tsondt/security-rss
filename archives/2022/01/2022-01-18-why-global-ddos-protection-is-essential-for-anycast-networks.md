Title: Why global DDoS protection is essential for Anycast networks
Date: 2022-01-18T11:55:39+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2022-01-18-why-global-ddos-protection-is-essential-for-anycast-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/18/if_you_dont_have_anycast/){:target="_blank" rel="noopener"}

> ‘If you don’t have Anycast it’s not a good DNS service’ Paid Feature In October 2021, in an incident lasting more than six hours, Facebook disappeared from the Internet. This wasn’t a temporary.com outage on the company’s primary domain but a complete shutdown of its public existence that also dragged into the darkness WhatsApp, Instagram, and Messenger.... [...]
