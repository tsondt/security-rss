Title: Indonesia's central bank confirms ransomware attack, Conti leaks data
Date: 2022-01-20T10:41:20-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-20-indonesias-central-bank-confirms-ransomware-attack-conti-leaks-data

[Source](https://www.bleepingcomputer.com/news/security/indonesias-central-bank-confirms-ransomware-attack-conti-leaks-data/){:target="_blank" rel="noopener"}

> Bank Indonesia (BI), the central bank of the Republic of Indonesia, has confirmed today that a ransomware attack hit its networks last month. [...]
