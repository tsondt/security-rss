Title: Supply chain attack used legitimate WordPress add-ons to backdoor sites
Date: 2022-01-21T21:01:16+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;backdoors;malware;supply chain attack
Slug: 2022-01-21-supply-chain-attack-used-legitimate-wordpress-add-ons-to-backdoor-sites

[Source](https://arstechnica.com/?p=1827592){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Dozens of legitimate WordPress add-ons downloaded from their original sources have been found backdoored through a supply chain attack, researchers said. The backdoor has been found on “quite a few” sites running the open source content management system. The backdoor gave the attackers full administrative control of websites that used at least 93 WordPress plugins and themes downloaded from AccessPress Themes. The backdoor was discovered by security researchers from JetPack, the maker of security software owned by Automatic, provider of the WordPress.com hosting service and a major contributor to the development of WordPress. In all, Jetpack [...]
