Title: NSO Group Pegasus Spyware Aims at Finnish Diplomats
Date: 2022-01-31T17:56:09+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2022-01-31-nso-group-pegasus-spyware-aims-at-finnish-diplomats

[Source](https://threatpost.com/nso-group-pegasus-spyware-finnish-diplomats/178113/){:target="_blank" rel="noopener"}

> Finland is weathering a bout of Pegasus infections, along with a Facebook Messenger phishing scam. [...]
