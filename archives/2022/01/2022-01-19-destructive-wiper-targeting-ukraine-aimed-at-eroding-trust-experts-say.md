Title: Destructive Wiper Targeting Ukraine Aimed at Eroding Trust, Experts Say
Date: 2022-01-19T20:55:28+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware
Slug: 2022-01-19-destructive-wiper-targeting-ukraine-aimed-at-eroding-trust-experts-say

[Source](https://threatpost.com/destructive-wiper-ukraine/177768/){:target="_blank" rel="noopener"}

> Disruptive malware attacks on Ukrainian organizations (posing as ransomware attacks) are very likely part of Russia’s wider effort to undermine Ukraine’s sovereignty, according to analysts. [...]
