Title: WordPress plugin flaw puts users of 20,000 sites at phishing risk
Date: 2022-01-20T10:50:25-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-20-wordpress-plugin-flaw-puts-users-of-20000-sites-at-phishing-risk

[Source](https://www.bleepingcomputer.com/news/security/wordpress-plugin-flaw-puts-users-of-20-000-sites-at-phishing-risk/){:target="_blank" rel="noopener"}

> The WordPress WP HTML Mail plugin, installed in over 20,000 sites, is vulnerable to a high-severity flaw that can lead to code injection and the distribution of convincing phishing emails. [...]
