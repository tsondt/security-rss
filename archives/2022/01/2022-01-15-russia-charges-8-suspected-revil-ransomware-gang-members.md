Title: Russia charges 8 suspected REvil ransomware gang members
Date: 2022-01-15T12:06:08-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-01-15-russia-charges-8-suspected-revil-ransomware-gang-members

[Source](https://www.bleepingcomputer.com/news/security/russia-charges-8-suspected-revil-ransomware-gang-members/){:target="_blank" rel="noopener"}

> Eight members of the REvil ransomware operation that have been detained by Russian officers are currently facing criminal charges for their illegal activity. [...]
