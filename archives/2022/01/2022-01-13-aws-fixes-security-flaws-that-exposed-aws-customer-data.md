Title: AWS fixes security flaws that exposed AWS customer data
Date: 2022-01-13T15:04:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-13-aws-fixes-security-flaws-that-exposed-aws-customer-data

[Source](https://www.bleepingcomputer.com/news/security/aws-fixes-security-flaws-that-exposed-aws-customer-data/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has addressed an AWS Glue security issue that allowed attackers to access and alter data linked to other AWS customer accounts. [...]
