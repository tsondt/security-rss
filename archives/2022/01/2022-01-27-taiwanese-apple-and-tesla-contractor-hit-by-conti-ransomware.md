Title: Taiwanese Apple and Tesla contractor hit by Conti ransomware
Date: 2022-01-27T14:28:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-27-taiwanese-apple-and-tesla-contractor-hit-by-conti-ransomware

[Source](https://www.bleepingcomputer.com/news/security/taiwanese-apple-and-tesla-contractor-hit-by-conti-ransomware/){:target="_blank" rel="noopener"}

> Delta Electronics, a Taiwanese electronics company and a provider for Apple, Tesla, HP, and Dell, disclosed that it was the victim of a cyberattack discovered on Friday morning. [...]
