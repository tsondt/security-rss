Title: Hive View security camera customers left in the dark as some gear gives up the ghost
Date: 2022-01-24T14:21:05+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-01-24-hive-view-security-camera-customers-left-in-the-dark-as-some-gear-gives-up-the-ghost

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/24/hive_view_issue/){:target="_blank" rel="noopener"}

> Devices of the affected seem intent on going into the light Customers of Centrica-owned Hive are reporting problems with their cameras, with many complaining the devices have packed up, some after a few years of operation and others after mere days.... [...]
