Title: Partially Unpatched VMware Bug Opens Door to Hypervisor Takeover
Date: 2022-01-06T16:47:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities
Slug: 2022-01-06-partially-unpatched-vmware-bug-opens-door-to-hypervisor-takeover

[Source](https://threatpost.com/unpatched-vmware-bug-hypervisor-takeover/177428/){:target="_blank" rel="noopener"}

> ESXi version 7 users are still waiting for a full fix for a high-severity heap-overflow security vulnerability, but Cloud Foundation, Fusion and Workstation users can go ahead and patch. [...]
