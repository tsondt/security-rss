Title: PrinterLogic vendor addresses triple RCE threat against all connected endpoints
Date: 2022-01-25T15:09:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-25-printerlogic-vendor-addresses-triple-rce-threat-against-all-connected-endpoints

[Source](https://portswigger.net/daily-swig/printerlogic-vendor-addresses-triple-rce-threat-against-all-connected-endpoints){:target="_blank" rel="noopener"}

> High severity flaws show centralized management means centralized risk, say researchers [...]
