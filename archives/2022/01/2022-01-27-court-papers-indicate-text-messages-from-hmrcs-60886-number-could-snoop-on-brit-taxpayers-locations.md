Title: Court papers indicate text messages from HMRC's 60886 number could snoop on Brit taxpayers' locations
Date: 2022-01-27T11:59:28+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-27-court-papers-indicate-text-messages-from-hmrcs-60886-number-could-snoop-on-brit-taxpayers-locations

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/27/hmrc_ss7_hlr_lookups/){:target="_blank" rel="noopener"}

> Bitter contract dispute revealed HLR lookup capability baked into agreement Exclusive Britain's tax collection agency asked a contractor to use the SS7 mobile phone signalling protocol that would make available location data of alleged tax defaulters, a High Court lawsuit has revealed.... [...]
