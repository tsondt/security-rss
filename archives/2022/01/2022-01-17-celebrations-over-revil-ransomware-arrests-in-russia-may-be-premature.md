Title: Celebrations over REvil ransomware arrests in Russia may be premature
Date: 2022-01-17T17:21:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-17-celebrations-over-revil-ransomware-arrests-in-russia-may-be-premature

[Source](https://portswigger.net/daily-swig/celebrations-over-revil-ransomware-arrests-in-russia-may-be-premature){:target="_blank" rel="noopener"}

> ‘It’s not clear whether the developers or lower-level criminals were arrested’, threat intel experts tell The Daily Swig [...]
