Title: Ukranian police arrests ransomware gang that hit over 50 firms
Date: 2022-01-13T07:47:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal;Technology
Slug: 2022-01-13-ukranian-police-arrests-ransomware-gang-that-hit-over-50-firms

[Source](https://www.bleepingcomputer.com/news/security/ukranian-police-arrests-ransomware-gang-that-hit-over-50-firms/){:target="_blank" rel="noopener"}

> Ukrainian police officers have arrested a ransomware affiliate group responsible for attacking at least 50 companies in the U.S. and Europe. [...]
