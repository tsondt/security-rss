Title: Russia starts playing by the rules: FSB busts 14 REvil ransomware suspects
Date: 2022-01-14T21:01:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-14-russia-starts-playing-by-the-rules-fsb-busts-14-revil-ransomware-suspects

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/14/russia_revil_ransomware_gang_charged/){:target="_blank" rel="noopener"}

> Cybercrook gang has 'ceased to exist' says Putin's military service Russia's internal security agency said today it had dismantled the REvil ransomware gang's networks and raided its operators' homes following arrests yesterday in Ukraine.... [...]
