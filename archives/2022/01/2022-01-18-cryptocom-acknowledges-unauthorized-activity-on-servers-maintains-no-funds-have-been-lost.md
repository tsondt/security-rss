Title: Crypto.com acknowledges 'unauthorized activity' on servers, maintains no funds have been lost
Date: 2022-01-18T21:12:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-18-cryptocom-acknowledges-unauthorized-activity-on-servers-maintains-no-funds-have-been-lost

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/18/crypto_com_security_incident/){:target="_blank" rel="noopener"}

> Security biz PeckShield claims $15m in Ethereum taken Crypto.com, a Singapore-based cryptocurrency exchange, has denied reports that the firm lost nearly $15m in Ethereum in a possible network intrusion over the weekend.... [...]
