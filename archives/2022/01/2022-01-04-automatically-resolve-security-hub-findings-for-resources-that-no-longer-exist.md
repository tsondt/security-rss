Title: Automatically resolve Security Hub findings for resources that no longer exist
Date: 2022-01-04T19:02:20+00:00
Author: Kris Normand
Category: AWS Security
Tags: Amazon EventBridge;AWS Security Hub;Security, Identity, & Compliance;Security Blog
Slug: 2022-01-04-automatically-resolve-security-hub-findings-for-resources-that-no-longer-exist

[Source](https://aws.amazon.com/blogs/security/automatically-resolve-security-hub-findings-for-resources-that-no-longer-exist/){:target="_blank" rel="noopener"}

> In this post, you’ll learn how to automatically resolve AWS Security Hub findings for previously deleted Amazon Web Services (AWS) resources. By using an event-driven solution, you can automatically resolve findings for AWS and third-party service integrations. Security Hub provides a comprehensive view of your security alerts and security posture across your AWS accounts. Security [...]
