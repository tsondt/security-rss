Title: Coming to a laptop near you: A new type of security chip from Microsoft
Date: 2022-01-04T22:15:27+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;AMD;Lenovo;pluton;Ryzen;ThinkPad
Slug: 2022-01-04-coming-to-a-laptop-near-you-a-new-type-of-security-chip-from-microsoft

[Source](https://arstechnica.com/?p=1823688){:target="_blank" rel="noopener"}

> Enlarge (credit: Lenovo ) In November 2020, Microsoft unveiled Pluton, a security processor that the company designed to thwart some of the most sophisticated types of hack attacks. On Tuesday, AMD said it would integrate the chip into its upcoming Ryzen CPUs for use in Lenovo's ThinkPad Z Series of laptops. Microsoft already used Pluton to secure Xbox Ones and Azure Sphere microcontrollers against attacks that involve people with physical access opening device cases and performing hardware hacks that bypass security protections. Such hacks are usually carried out by device owners who want to run unauthorized games or programs for [...]
