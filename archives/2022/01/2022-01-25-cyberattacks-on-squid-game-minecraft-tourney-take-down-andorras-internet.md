Title: Cyberattacks on Squid Game Minecraft Tourney Take Down Andorra’s Internet
Date: 2022-01-25T21:00:08+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2022-01-25-cyberattacks-on-squid-game-minecraft-tourney-take-down-andorras-internet

[Source](https://threatpost.com/cyberattacks-squid-game-minecraft-andorra-internet/177981/){:target="_blank" rel="noopener"}

> Some of the bursts of traffic reached up to 10Gbps, reports noted, overwhelming the country's only ISP, and crippling Andorran Squidcraft gamers along with the rest of the population. [...]
