Title: Web skimming attacks on hundreds of real estate websites deployed via cloud video hosting service
Date: 2022-01-05T14:56:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-05-web-skimming-attacks-on-hundreds-of-real-estate-websites-deployed-via-cloud-video-hosting-service

[Source](https://portswigger.net/daily-swig/web-skimming-attacks-on-hundreds-of-real-estate-websites-deployed-via-cloud-video-hosting-service){:target="_blank" rel="noopener"}

> Attackers leverage software supply chain to compromise high-traffic sites [...]
