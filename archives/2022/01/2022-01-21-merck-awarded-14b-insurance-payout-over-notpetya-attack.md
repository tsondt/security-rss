Title: Merck Awarded $1.4B Insurance Payout over NotPetya Attack
Date: 2022-01-21T20:27:15+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware
Slug: 2022-01-21-merck-awarded-14b-insurance-payout-over-notpetya-attack

[Source](https://threatpost.com/merck-insurance-payout-notpetya-attack/177872/){:target="_blank" rel="noopener"}

> Court rules ‘War or Hostile Acts’ exclusion doesn’t apply to the pharma giant's 2017 cyberattack. [...]
