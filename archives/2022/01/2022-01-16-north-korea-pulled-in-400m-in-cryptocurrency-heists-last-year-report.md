Title: North Korea pulled in $400m in cryptocurrency heists last year – report
Date: 2022-01-16T11:01:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-01-16-north-korea-pulled-in-400m-in-cryptocurrency-heists-last-year-report

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/16/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: FIFA 22 players lose their identity and Texas gets phony QR codes In brief Thieves operating for the North Korean government made off with almost $400m in digicash last year in a concerted attack to steal and launder as much currency as they could.... [...]
