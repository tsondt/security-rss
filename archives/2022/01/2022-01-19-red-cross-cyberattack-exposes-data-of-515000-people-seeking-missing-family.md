Title: Red Cross cyberattack exposes data of 515,000 people seeking missing family
Date: 2022-01-19T18:26:50-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-19-red-cross-cyberattack-exposes-data-of-515000-people-seeking-missing-family

[Source](https://www.bleepingcomputer.com/news/security/red-cross-cyberattack-exposes-data-of-515-000-people-seeking-missing-family/){:target="_blank" rel="noopener"}

> A cyberattack on a Red Cross contactor has led to the theft of personal data for more than 515,000 people in 'Restoring Family Links,' a program that helps reunite families separated by war, disaster, and migration. [...]
