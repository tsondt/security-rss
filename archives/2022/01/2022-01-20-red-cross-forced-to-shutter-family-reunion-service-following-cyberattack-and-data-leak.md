Title: Red Cross forced to shutter family reunion service following cyberattack and data leak
Date: 2022-01-20T07:58:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-01-20-red-cross-forced-to-shutter-family-reunion-service-following-cyberattack-and-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/20/red_cross_hit_by_cyberattack/){:target="_blank" rel="noopener"}

> Director-general pleads with cyber-scum: leave this data alone, because the people involved have suffered enough Humanitarian organization the International Red Cross disclosed this week that it has fallen foul of a cyberattack that saw the data of over 515,000 "highly vulnerable people" exposed to an unknown entity.... [...]
