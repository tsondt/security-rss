Title: Researcher discloses alleged zero-day vulnerabilities in NUUO NVRmini2 recording device
Date: 2022-01-14T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-14-researcher-discloses-alleged-zero-day-vulnerabilities-in-nuuo-nvrmini2-recording-device

[Source](https://portswigger.net/daily-swig/researcher-discloses-alleged-zero-day-vulnerabilities-in-nuuo-nvrmini2-recording-device){:target="_blank" rel="noopener"}

> Exploit code has also been released for flaws that supposedly date back to 2016 [...]
