Title: Sophos: Log4Shell would have been a catastrophe without the Y2K-esque mobilisation of engineers
Date: 2022-01-25T15:32:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-01-25-sophos-log4shell-would-have-been-a-catastrophe-without-the-y2k-esque-mobilisation-of-engineers

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/25/sophos_log4shell/){:target="_blank" rel="noopener"}

> Anti-malware biz weighs in on one of the worst security flaws of recent times Anti-malware outfit Sophos has weighed in on Log4Shell, saying that the galvanization of the IT world to avert disaster would be familiar to those who lived through the Y2K era.... [...]
