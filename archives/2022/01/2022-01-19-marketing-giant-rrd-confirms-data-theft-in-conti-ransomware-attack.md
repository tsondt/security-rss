Title: Marketing giant RRD confirms data theft in Conti ransomware attack
Date: 2022-01-19T16:25:11-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-19-marketing-giant-rrd-confirms-data-theft-in-conti-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/marketing-giant-rrd-confirms-data-theft-in-conti-ransomware-attack/){:target="_blank" rel="noopener"}

> RR Donnelly has confirmed that threat actors stole data in a December cyberattack, confirmed by BleepingComputer to be a Conti ransomware attack. [...]
