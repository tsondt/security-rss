Title: School District reports a 334% hike in cybersecurity insurance costs
Date: 2022-01-22T11:16:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-22-school-district-reports-a-334-hike-in-cybersecurity-insurance-costs

[Source](https://www.bleepingcomputer.com/news/security/school-district-reports-a-334-percent-hike-in-cybersecurity-insurance-costs/){:target="_blank" rel="noopener"}

> Bloomington School District 87 in Illinois has published its cyber-insurance renewal details, and the cost has jumped from $6,661 in 2021 to $22,229 this year. [...]
