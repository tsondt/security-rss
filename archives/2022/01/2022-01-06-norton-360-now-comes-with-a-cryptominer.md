Title: Norton 360 Now Comes With a Cryptominer
Date: 2022-01-06T17:26:10+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Adi Robertson;Chris Vickery;Cory Doctorow;ETH;Ethereum;NCrypt.exe;Norton Crypto;NortonLifeLock;RadioShack;Symantec Corp.
Slug: 2022-01-06-norton-360-now-comes-with-a-cryptominer

[Source](https://krebsonsecurity.com/2022/01/norton-360-now-comes-with-a-cryptominer/){:target="_blank" rel="noopener"}

> Norton 360, one of the most popular antivirus products on the market today, has installed a cryptocurrency mining program on its customers’ computers. Norton’s parent firm says the cloud-based service that activates the program and allows customers to profit from the scheme — in which the company keeps 15 percent of any currencies mined — is “opt-in,” meaning users have to agree to enable it. But many Norton users complain the mining program is difficult to remove, and reactions from longtime customers have ranged from unease and disbelief to, “Dude, where’s my crypto?” Norton 360 is owned by Tempe, Ariz.-based [...]
