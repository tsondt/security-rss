Title: UK mulls making MSPs subject to mandatory security standards where they provide critical infrastructure
Date: 2022-01-20T17:15:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-20-uk-mulls-making-msps-subject-to-mandatory-security-standards-where-they-provide-critical-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/20/uk_nis_regulations_msp_plans/){:target="_blank" rel="noopener"}

> And to pay for the privilege. Consultation's open, though Small and medium-sized managed service providers (MSPs) could find themselves subject to the Network and Information Systems Regulations under government plans to tighten cybersecurity laws – and have got three months to object to the tax hikes that will follow.... [...]
