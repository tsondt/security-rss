Title: OceanLotus hackers turn to web archive files to deploy backdoors
Date: 2022-01-12T10:20:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-12-oceanlotus-hackers-turn-to-web-archive-files-to-deploy-backdoors

[Source](https://www.bleepingcomputer.com/news/security/oceanlotus-hackers-turn-to-web-archive-files-to-deploy-backdoors/){:target="_blank" rel="noopener"}

> The OceanLotus group of state-sponsored hackers are now using the web archive file format (.MHT and.MHTML) to deploy backdoors to compromised systems. [...]
