Title: Red Cross suffers cyber-attack – data of 515,000 ‘highly vulnerable’ people exposed
Date: 2022-01-20T14:05:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-20-red-cross-suffers-cyber-attack-data-of-515000-highly-vulnerable-people-exposed

[Source](https://portswigger.net/daily-swig/red-cross-suffers-cyber-attack-data-of-515-000-highly-vulnerable-people-exposed){:target="_blank" rel="noopener"}

> The ‘sophisticated’ attack was detected last week [...]
