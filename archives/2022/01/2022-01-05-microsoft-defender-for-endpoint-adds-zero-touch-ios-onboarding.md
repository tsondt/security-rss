Title: Microsoft Defender for Endpoint adds zero-touch iOS onboarding
Date: 2022-01-05T15:51:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-01-05-microsoft-defender-for-endpoint-adds-zero-touch-ios-onboarding

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-for-endpoint-adds-zero-touch-ios-onboarding/){:target="_blank" rel="noopener"}

> Microsoft says zero-touch onboarding for Microsoft Defender for Endpoint (MDE) on iOS is now available in public preview, allowing enterprise admins to silently install Defender for Endpoint automatically on enrolled devices. [...]
