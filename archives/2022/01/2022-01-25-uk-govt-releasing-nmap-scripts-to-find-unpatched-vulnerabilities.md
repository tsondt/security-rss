Title: UK govt releasing Nmap scripts to find unpatched vulnerabilities
Date: 2022-01-25T13:45:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-25-uk-govt-releasing-nmap-scripts-to-find-unpatched-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/uk-govt-releasing-nmap-scripts-to-find-unpatched-vulnerabilities/){:target="_blank" rel="noopener"}

> The United Kingdom's National Cyber Security Centre (NCSC), the government agency that leads UK's cyber security mission, is releasing NMAP Scripting Engine scripts to help defenders scan for and remediate vulnerable systems on their networks. [...]
