Title: People Are Increasingly Choosing Private Web Search
Date: 2022-01-06T12:29:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Google;privacy;search engines;web privacy
Slug: 2022-01-06-people-are-increasingly-choosing-private-web-search

[Source](https://www.schneier.com/blog/archives/2022/01/people-are-increasingly-choosing-private-web-search.html){:target="_blank" rel="noopener"}

> DuckDuckGo has had a banner year : And yet, DuckDuckGo. The privacy-oriented search engine netted more than 35 billion search queries in 2021, a 46.4% jump over 2020 (23.6 billion). That’s big. Even so, the company, which bills itself as the “Internet privacy company,” offering a search engine and other products designed to “empower you to seamlessly take control of your personal information online without any tradeoffs,” remains a rounding error compared to Google in search. I use it. It’s not as a good a search engine as Google. Or, at least, Google often gets me what I want faster [...]
