Title: ThePhish: ‘the most complete’ non-commercial phishing email analysis tool
Date: 2022-01-19T12:46:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-19-thephish-the-most-complete-non-commercial-phishing-email-analysis-tool

[Source](https://portswigger.net/daily-swig/thephish-the-most-complete-non-commercial-phishing-email-analysis-tool){:target="_blank" rel="noopener"}

> Developer says tool is more precise and queries a wider range of utilities than other free and open source rivals [...]
