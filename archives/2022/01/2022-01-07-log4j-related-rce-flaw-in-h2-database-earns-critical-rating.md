Title: Log4J-Related RCE Flaw in H2 Database Earns Critical Rating
Date: 2022-01-07T15:12:26+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities
Slug: 2022-01-07-log4j-related-rce-flaw-in-h2-database-earns-critical-rating

[Source](https://threatpost.com/log4j-related-flaw-h2-database/177448/){:target="_blank" rel="noopener"}

> Critical flaw in the H2 open-source Java SQL database are similar to the Log4J vulnerability, but do not pose a widespread threat. [...]
