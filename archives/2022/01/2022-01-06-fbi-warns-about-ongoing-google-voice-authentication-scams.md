Title: FBI warns about ongoing Google Voice authentication scams
Date: 2022-01-06T08:29:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-06-fbi-warns-about-ongoing-google-voice-authentication-scams

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-about-ongoing-google-voice-authentication-scams/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) says Americans who share their phone number online are being targeted by Google Voice authentication scams. [...]
