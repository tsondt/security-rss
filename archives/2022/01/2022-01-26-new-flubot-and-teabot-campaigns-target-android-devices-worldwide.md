Title: New FluBot and TeaBot campaigns target Android devices worldwide
Date: 2022-01-26T09:19:25-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2022-01-26-new-flubot-and-teabot-campaigns-target-android-devices-worldwide

[Source](https://www.bleepingcomputer.com/news/security/new-flubot-and-teabot-campaigns-target-android-devices-worldwide/){:target="_blank" rel="noopener"}

> New FluBot and TeaBot malware distribution campaigns have been spotted, using typical smishing lures or laced apps against Android users in Australia, Germany, Poland, Spain, and Romania. [...]
