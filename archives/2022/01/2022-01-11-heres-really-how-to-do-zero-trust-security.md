Title: Here’s REALLY How to Do Zero-Trust Security
Date: 2022-01-11T22:13:19+00:00
Author: Joseph Carson
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Vulnerabilities;Web Security
Slug: 2022-01-11-heres-really-how-to-do-zero-trust-security

[Source](https://threatpost.com/zero-trust-future-security-risks/177502/){:target="_blank" rel="noopener"}

> It's not about buying security products! Joseph Carson, chief security scientist from ThycoticCentrify, offers practical steps to start the zero-trust journey. [...]
