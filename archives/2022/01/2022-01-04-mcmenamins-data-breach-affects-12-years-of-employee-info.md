Title: McMenamins Data Breach Affects 12 Years of Employee Info
Date: 2022-01-04T16:43:57+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Malware;Privacy;Web Security
Slug: 2022-01-04-mcmenamins-data-breach-affects-12-years-of-employee-info

[Source](https://threatpost.com/mcmenamins-data-breach-employee-info/177336/){:target="_blank" rel="noopener"}

> The Pacific Northwest hospitality stalwart is also still operationally crippled by a Dec. 12 ransomware attack. [...]
