Title: DeepDotWeb administrator gets eight-year stretch in US prison for money laundering
Date: 2022-01-27T13:52:07+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-27-deepdotweb-administrator-gets-eight-year-stretch-in-us-prison-for-money-laundering

[Source](https://portswigger.net/daily-swig/deepdotweb-administrator-gets-eight-year-stretch-in-us-prison-for-money-laundering){:target="_blank" rel="noopener"}

> Tal Prihar pleaded guilty to his role in darknet kickback scheme last year [...]
