Title: Russian Security Takes Down REvil Ransomware Gang
Date: 2022-01-14T14:45:35+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware
Slug: 2022-01-14-russian-security-takes-down-revil-ransomware-gang

[Source](https://threatpost.com/russian-security-revil-ransomware/177660/){:target="_blank" rel="noopener"}

> The country's FSB said that it raided gang hideouts; seized currency, cars and personnel; and neutralized REvil's infrastructure. [...]
