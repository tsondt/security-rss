Title: Backdoor RAT for Windows, macOS, and Linux went undetected until now
Date: 2022-01-15T14:00:43+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;backdoors;cross-platform;malware
Slug: 2022-01-15-backdoor-rat-for-windows-macos-and-linux-went-undetected-until-now

[Source](https://arstechnica.com/?p=1826079){:target="_blank" rel="noopener"}

> Enlarge (credit: Jeremy Brooks / Flickr ) Researchers have uncovered a never-before-seen backdoor malware written from scratch for systems running Windows, macOS, or Linux that remained undetected by virtually all malware scanning engines. Researchers from security firm Intezer said they discovered SysJoker—the name they gave the backdoor malware—on the Linux-based Webserver of a “leading educational institution.” As the researchers dug in, they found SysJoker versions for both Windows and macOS as well. They suspect the cross-platform RAT—short for remote access trojan—was unleashed in the second half of last year. The discovery is significant for several reasons. First, fully cross-platform malware [...]
