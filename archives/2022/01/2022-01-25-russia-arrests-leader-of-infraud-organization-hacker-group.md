Title: Russia arrests leader of “Infraud Organization” hacker group
Date: 2022-01-25T09:00:55-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-01-25-russia-arrests-leader-of-infraud-organization-hacker-group

[Source](https://www.bleepingcomputer.com/news/security/russia-arrests-leader-of-infraud-organization-hacker-group/){:target="_blank" rel="noopener"}

> The Russian Federal Security Service (FSB) and law enforcement have arrested Andrey Sergeevich Novak, the alleged leader of the Infraud Organization, a hacker group that caused losses of more than $560 million in seven years of activity. [...]
