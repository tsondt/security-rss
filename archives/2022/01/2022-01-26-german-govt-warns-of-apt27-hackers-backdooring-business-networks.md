Title: German govt warns of APT27 hackers backdooring business networks
Date: 2022-01-26T08:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-26-german-govt-warns-of-apt27-hackers-backdooring-business-networks

[Source](https://www.bleepingcomputer.com/news/security/german-govt-warns-of-apt27-hackers-backdooring-business-networks/){:target="_blank" rel="noopener"}

> The BfV German domestic intelligence services (short for Bun­des­amt für Ver­fas­sungs­schutz) warn of ongoing attacks coordinated by the APT27 Chinese-backed hacking group. [...]
