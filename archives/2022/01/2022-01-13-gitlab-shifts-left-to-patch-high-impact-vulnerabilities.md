Title: GitLab shifts left to patch high-impact vulnerabilities
Date: 2022-01-13T16:41:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-13-gitlab-shifts-left-to-patch-high-impact-vulnerabilities

[Source](https://portswigger.net/daily-swig/gitlab-shifts-left-to-patch-high-impact-vulnerabilities){:target="_blank" rel="noopener"}

> HackerOne bug bounty reports triaged [...]
