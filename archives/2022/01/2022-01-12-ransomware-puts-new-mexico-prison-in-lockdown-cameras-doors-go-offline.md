Title: Ransomware puts New Mexico prison in lockdown: Cameras, doors go offline
Date: 2022-01-12T22:03:35+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-12-ransomware-puts-new-mexico-prison-in-lockdown-cameras-doors-go-offline

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/12/ransomware_new_mexico_prison/){:target="_blank" rel="noopener"}

> Bernalillo County's Metropolitan Detention Center still recovering from infection Bernalillo County, New Mexico, has been unable to comply with the settlement terms of a 27-year-old lawsuit over prison conditions because of a ransomware attack last week that saw prisoners back under manual control.... [...]
