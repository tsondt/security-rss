Title: Ozzy Osbourne NFTs Used to Bite Off Chunk of Crypto Coin
Date: 2022-01-25T20:45:00+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2022-01-25-ozzy-osbourne-nfts-used-to-bite-off-chunk-of-crypto-coin

[Source](https://threatpost.com/ozzy-osbourne-nfts-cryptocurrency/177969/){:target="_blank" rel="noopener"}

> A discarded Discord vanity URL for CryptoBatz was hijacked by cybercriminals to drain cryptocurrency wallets. [...]
