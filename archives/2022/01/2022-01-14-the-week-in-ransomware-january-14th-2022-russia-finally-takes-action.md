Title: The Week in Ransomware - January 14th 2022 - Russia finally takes action
Date: 2022-01-14T18:53:15-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-14-the-week-in-ransomware-january-14th-2022-russia-finally-takes-action

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-14th-2022-russia-finally-takes-action/){:target="_blank" rel="noopener"}

> Today, the Russian government announced that they arrested fourteen members of the REvil ransomware gang on behalf of US authorities. [...]
