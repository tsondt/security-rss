Title: FlexBooker discloses data breach, over 3.7 million accounts impacted
Date: 2022-01-06T15:53:02-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-01-06-flexbooker-discloses-data-breach-over-37-million-accounts-impacted

[Source](https://www.bleepingcomputer.com/news/security/flexbooker-discloses-data-breach-over-37-million-accounts-impacted/){:target="_blank" rel="noopener"}

> Accounts of more than three million users of the U.S.-based FlexBooker appointment scheduling service have been stolen in an attack before the holidays and are now being traded on hacker forums. [...]
