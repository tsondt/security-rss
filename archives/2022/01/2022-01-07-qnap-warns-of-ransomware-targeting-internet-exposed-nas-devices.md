Title: QNAP warns of ransomware targeting Internet-exposed NAS devices
Date: 2022-01-07T08:20:29-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-07-qnap-warns-of-ransomware-targeting-internet-exposed-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-of-ransomware-targeting-internet-exposed-nas-devices/){:target="_blank" rel="noopener"}

> QNAP has warned customers today to secure Internet-exposed network-attached storage (NAS) devices immediately from ongoing ransomware and brute-force attacks. [...]
