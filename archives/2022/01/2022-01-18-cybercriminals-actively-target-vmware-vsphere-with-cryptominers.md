Title: Cybercriminals Actively Target VMware vSphere with Cryptominers
Date: 2022-01-18T19:33:13+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Malware
Slug: 2022-01-18-cybercriminals-actively-target-vmware-vsphere-with-cryptominers

[Source](https://threatpost.com/cybercriminals-vmware-vsphere-cryptominers/177722/){:target="_blank" rel="noopener"}

> VMware's container-based application development environment has become attractive to cyberattackers. [...]
