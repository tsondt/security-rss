Title: OpenSubtitles data breach: Users asked to re-secure accounts after plaintext password snafu
Date: 2022-01-24T14:29:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-24-opensubtitles-data-breach-users-asked-to-re-secure-accounts-after-plaintext-password-snafu

[Source](https://portswigger.net/daily-swig/opensubtitles-data-breach-users-asked-to-re-secure-accounts-after-plaintext-password-snafu){:target="_blank" rel="noopener"}

> Movie translation site asked victims to reset passwords... then sent them in clear text [...]
