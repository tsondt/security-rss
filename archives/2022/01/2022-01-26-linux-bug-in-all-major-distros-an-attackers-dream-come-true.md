Title: Linux Bug in All Major Distros: ‘An Attacker’s Dream Come True’
Date: 2022-01-26T17:52:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: News;Vulnerabilities;Web Security
Slug: 2022-01-26-linux-bug-in-all-major-distros-an-attackers-dream-come-true

[Source](https://threatpost.com/linux-bug-in-all-major-distros-an-attackers-dream-come-true/177996/){:target="_blank" rel="noopener"}

> The 12-year-old flaw in the sudo-like Polkit’s pkexec tool, found in all major Linux distributions, is likely to be exploited in the wild within days. [...]
