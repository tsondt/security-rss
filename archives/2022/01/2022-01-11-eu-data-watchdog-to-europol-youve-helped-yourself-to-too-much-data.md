Title: EU data watchdog to Europol: You've helped yourself to too much data
Date: 2022-01-11T11:47:50+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-01-11-eu-data-watchdog-to-europol-youve-helped-yourself-to-too-much-data

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/11/eu_data_watchdog_to_europol/){:target="_blank" rel="noopener"}

> Law enforcement agency now has one year to delete any data older than 6 months not related to criminal activity The European Data Protection Supervisor (EDPS) has ordered European Union law enforcement agency Europol to delete any data it has on individuals that's over six months old, provided there's no link to criminal activity.... [...]
