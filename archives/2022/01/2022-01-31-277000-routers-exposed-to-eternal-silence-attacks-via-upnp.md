Title: 277,000 routers exposed to Eternal Silence attacks via UPnP
Date: 2022-01-31T10:40:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-31-277000-routers-exposed-to-eternal-silence-attacks-via-upnp

[Source](https://www.bleepingcomputer.com/news/security/277-000-routers-exposed-to-eternal-silence-attacks-via-upnp/){:target="_blank" rel="noopener"}

> A malicious campaign known as 'Eternal Silence' is abusing Universal Plug and Play (UPnP) turns your router into a proxy server used to launch malicious attacks while hiding the location of the threat actors. [...]
