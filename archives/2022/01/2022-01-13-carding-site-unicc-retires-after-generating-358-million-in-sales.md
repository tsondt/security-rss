Title: Carding site UniCC retires after generating $358 million in sales
Date: 2022-01-13T11:21:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-13-carding-site-unicc-retires-after-generating-358-million-in-sales

[Source](https://www.bleepingcomputer.com/news/security/carding-site-unicc-retires-after-generating-358-million-in-sales/){:target="_blank" rel="noopener"}

> ​UniCC, the largest carding site operating on the dark web at the moment, has announced its retirement, claiming reasons of tiredness. [...]
