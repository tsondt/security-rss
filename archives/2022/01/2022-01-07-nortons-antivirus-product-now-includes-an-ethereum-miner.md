Title: Norton’s Antivirus Product Now Includes an Ethereum Miner
Date: 2022-01-07T12:13:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;antivirus;cryptocurrency
Slug: 2022-01-07-nortons-antivirus-product-now-includes-an-ethereum-miner

[Source](https://www.schneier.com/blog/archives/2022/01/nortons-antivirus-product-now-includes-an-ethereum-miner.html){:target="_blank" rel="noopener"}

> Norton 360 can now mine Ethereum. It’s opt-in, and the company keeps 15%. It’s hard to uninstall this option. [...]
