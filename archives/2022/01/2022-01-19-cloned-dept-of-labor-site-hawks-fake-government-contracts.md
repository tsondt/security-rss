Title: Cloned Dept. of Labor Site Hawks Fake Government Contracts
Date: 2022-01-19T11:00:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Web Security
Slug: 2022-01-19-cloned-dept-of-labor-site-hawks-fake-government-contracts

[Source](https://threatpost.com/cloned-dept-of-labor-fake-government-contracts/177734/){:target="_blank" rel="noopener"}

> A well-crafted but fake government procurement portal offers the opportunity to submit a bid for lucrative government projects -- but harvests credentials instead. [...]
