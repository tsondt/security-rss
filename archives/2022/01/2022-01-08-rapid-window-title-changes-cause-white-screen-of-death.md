Title: Rapid window title changes cause ‘white screen of death’
Date: 2022-01-08T10:16:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-08-rapid-window-title-changes-cause-white-screen-of-death

[Source](https://www.bleepingcomputer.com/news/security/rapid-window-title-changes-cause-white-screen-of-death-/){:target="_blank" rel="noopener"}

> Experimentation with ANSI escape characters on terminal emulators has led to the discovery of multiple high-severity DoS (denial of service) vulnerabilities on Windows terminals and Chrome-based web browsers. [...]
