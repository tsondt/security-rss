Title: Who Wrote the ALPHV/BlackCat Ransomware Strain?
Date: 2022-01-28T13:18:36+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;@CookieDays;ALPHV ransomware;Binrs;BlackCat ransomware;BlackMatter;Catalin Cimpanu;DarkSide;DuckerMan;duckermanit;Flashpoint;Jason Hill;Paul Roberts;RAMP;Recorded Future;ReversingLabs;rEvil;Sergey Duck;Sergey Kryakov;Sergey Penchikov;smiseo;The Record;ToX;Varonis;YBCat
Slug: 2022-01-28-who-wrote-the-alphvblackcat-ransomware-strain

[Source](https://krebsonsecurity.com/2022/01/who-wrote-the-alphv-blackcat-ransomware-strain/){:target="_blank" rel="noopener"}

> In December 2021, researchers discovered a new ransomware-as-a-service named ALPHV (a.k.a. “ BlackCat “), considered to be the first professional cybercrime group to create and use a ransomware strain written in the Rust programming language. In this post, we’ll explore some of the clues left behind by a developer who was reputedly hired to code the ransomware variant. Image: Varonis. According to an analysis released this week by Varonis, ALPHV is actively recruiting operators from several ransomware organizations — including REvil, BlackMatter and DarkSide — and is offering affiliates up to 90 percent of any ransom paid by a victim [...]
