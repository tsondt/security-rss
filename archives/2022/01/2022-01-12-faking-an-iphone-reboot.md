Title: Faking an iPhone Reboot
Date: 2022-01-12T12:15:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;iOS;iPhone;malware
Slug: 2022-01-12-faking-an-iphone-reboot

[Source](https://www.schneier.com/blog/archives/2022/01/faking-an-iphone-reboot.html){:target="_blank" rel="noopener"}

> Researchers have figured how how to intercept and fake an iPhone reboot: We’ll dissect the iOS system and show how it’s possible to alter a shutdown event, tricking a user that got infected into thinking that the phone has been powered off, but in fact, it’s still running. The “NoReboot” approach simulates a real shutdown. The user cannot feel a difference between a real shutdown and a “fake shutdown.” There is no user-interface or any button feedback until the user turns the phone back “on.” It’s a complicated hack, but it works. Uses are obvious : Historically, when malware infects [...]
