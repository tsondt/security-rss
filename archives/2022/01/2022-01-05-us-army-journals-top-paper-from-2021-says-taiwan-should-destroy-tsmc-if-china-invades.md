Title: US Army journal's top paper from 2021 says Taiwan should destroy TSMC if China invades
Date: 2022-01-05T19:01:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-01-05-us-army-journals-top-paper-from-2021-says-taiwan-should-destroy-tsmc-if-china-invades

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/05/taiwan_should_destory_tsmc_paper/){:target="_blank" rel="noopener"}

> No more chip factories would surely change Beijing's mind about unification A top US Army War College paper suggests Taiwan should credibly threaten to eradicate its semiconductor industry if threatened by China so that Beijing would no longer be interested in unification.... [...]
