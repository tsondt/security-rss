Title: 10 questions to help boards safely maximize cloud opportunities
Date: 2022-01-13T17:30:00+00:00
Author: Nick Godfrey
Category: GCP Security
Tags: Cloud Migration;Google Cloud;Identity & Security
Slug: 2022-01-13-10-questions-to-help-boards-safely-maximize-cloud-opportunities

[Source](https://cloud.google.com/blog/products/identity-security/10-questions-to-help-boards-safely-maximize-cloud-opportunities/){:target="_blank" rel="noopener"}

> The accelerating pursuit of cloud-enabled digital transformations brings new growth opportunities to organizations, but also raises new challenges. To ensure that they can lock in newfound agility, quality improvements, and marketplace relevance, boards of directors must prioritize safe, secure, and compliant adoption processes that support this new technological environment. The adoption of cloud at scale by a large enterprise requires the orchestration of a number of significant activities, including: Rethinking how strategic outcomes leverage technology, and how to enable those outcomes by changing how software is designed, delivered, managed across the organization. Refactoring security, controls, and risk governance processes to [...]
