Title: QNAP force-installs update after DeadBolt ransomware hits 3,600 devices
Date: 2022-01-28T01:30:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2022-01-28-qnap-force-installs-update-after-deadbolt-ransomware-hits-3600-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-force-installs-update-after-deadbolt-ransomware-hits-3-600-devices/){:target="_blank" rel="noopener"}

> QNAP force-updated customer's Network Attached Storage (NAS) devices with firmware containing the latest security updates to protect against the DeadBolt ransomware, which has already encrypted over 3,600 devices. [...]
