Title: US government’s ‘zero trust’ roadmap calls time on perimeter-based paradigm
Date: 2022-01-28T15:38:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-28-us-governments-zero-trust-roadmap-calls-time-on-perimeter-based-paradigm

[Source](https://portswigger.net/daily-swig/us-governments-zero-trust-roadmap-calls-time-on-perimeter-based-paradigm){:target="_blank" rel="noopener"}

> Federal agencies have a little over two years to fundamentally remodel cyber defenses [...]
