Title: Crypto.com now says someone tried to drain $34m from hundreds of accounts
Date: 2022-01-20T22:29:51+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-20-cryptocom-now-says-someone-tried-to-drain-34m-from-hundreds-of-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/20/cryptocom_cryptocurrency_theft/){:target="_blank" rel="noopener"}

> Won't reveal net loss, says it stopped some withdrawals and has reimbursed those who had funds taken Crypto.com on Thursday said in a roundabout way that an unidentified person stole or attempted to steal as much as $34m in cryptocurrency from customer accounts.... [...]
