Title: New DeadBolt Ransomware Targets NAT Devices
Date: 2022-01-26T16:04:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;encryption;ransomware;zero-day
Slug: 2022-01-26-new-deadbolt-ransomware-targets-nat-devices

[Source](https://www.schneier.com/blog/archives/2022/01/new-deadbolt-ransomware-targets-nat-devices.html){:target="_blank" rel="noopener"}

> There’s a new ransomware that targets NAT devices made by QNAP: The attacks started today, January 25th, with QNAP devices suddenly finding their files encrypted and file names appended with a.deadbolt file extension. Instead of creating ransom notes in each folder on the device, the QNAP device’s login page is hijacked to display a screen stating, “WARNING: Your files have been locked by DeadBolt”.... [...] BleepingComputer is aware of at least fifteen victims of the new DeadBolt ransomware attack, with no specific region being targeted. As with all ransomware attacks against QNAP devices, the DeadBolt attacks only affect devices accessible [...]
