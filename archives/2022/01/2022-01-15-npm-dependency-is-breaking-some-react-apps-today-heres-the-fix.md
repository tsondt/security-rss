Title: npm dependency is breaking some React apps today — here's the fix
Date: 2022-01-15T12:35:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-01-15-npm-dependency-is-breaking-some-react-apps-today-heres-the-fix

[Source](https://www.bleepingcomputer.com/news/security/npm-dependency-is-breaking-some-react-apps-today-heres-the-fix/){:target="_blank" rel="noopener"}

> Tons of users are reporting their Facebook Create React App builds are failing since yesterday. The cause has been traced down to a dependency used by create-react-app, the latest version of which is breaking developers' apps. [...]
