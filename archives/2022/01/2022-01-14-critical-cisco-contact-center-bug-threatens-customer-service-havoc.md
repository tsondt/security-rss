Title: Critical Cisco Contact Center Bug Threatens Customer-Service Havoc
Date: 2022-01-14T16:37:13+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-01-14-critical-cisco-contact-center-bug-threatens-customer-service-havoc

[Source](https://threatpost.com/critical-cisco-contact-center-bug/177681/){:target="_blank" rel="noopener"}

> Attackers could access and modify agent resources, telephone queues and other customer-service systems – and access personal information on companies’ customers. [...]
