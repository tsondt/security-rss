Title: Security vulnerabilities in Umbraco CMS could lead to account takeover
Date: 2022-01-19T14:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-19-security-vulnerabilities-in-umbraco-cms-could-lead-to-account-takeover

[Source](https://portswigger.net/daily-swig/security-vulnerabilities-in-umbraco-cms-could-lead-to-account-takeover){:target="_blank" rel="noopener"}

> Partial fix applied for two separate bugs in the open source software [...]
