Title: US links MuddyWater hacking group to Iranian intelligence agency
Date: 2022-01-12T15:27:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-12-us-links-muddywater-hacking-group-to-iranian-intelligence-agency

[Source](https://www.bleepingcomputer.com/news/security/us-links-muddywater-hacking-group-to-iranian-intelligence-agency/){:target="_blank" rel="noopener"}

> US Cyber Command (USCYBERCOM) has officially linked the Iranian-backed MuddyWatter hacking group to Iran's Ministry of Intelligence and Security (MOIS). [...]
