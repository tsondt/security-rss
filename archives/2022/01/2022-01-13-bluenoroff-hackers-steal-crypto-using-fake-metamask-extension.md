Title: BlueNoroff hackers steal crypto using fake MetaMask extension
Date: 2022-01-13T15:14:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-13-bluenoroff-hackers-steal-crypto-using-fake-metamask-extension

[Source](https://www.bleepingcomputer.com/news/security/bluenoroff-hackers-steal-crypto-using-fake-metamask-extension/){:target="_blank" rel="noopener"}

> The North Korean threat actor group known as 'BlueNoroff' has been spotted targeting cryptocurrency startups with malicious documents and fake MetaMask browser extensions. [...]
