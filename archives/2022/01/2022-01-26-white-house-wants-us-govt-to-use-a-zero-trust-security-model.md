Title: White House wants US govt to use a Zero Trust security model
Date: 2022-01-26T11:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-26-white-house-wants-us-govt-to-use-a-zero-trust-security-model

[Source](https://www.bleepingcomputer.com/news/security/white-house-wants-us-govt-to-use-a-zero-trust-security-model/){:target="_blank" rel="noopener"}

> A newly released Federal strategy wants the US government to adopt a "zero trust" security model within the next two years to defend against current threats and boost cybersecurity defenses across federal agencies. [...]
