Title: Crime Shop Sells Hacked Logins to Other Crime Shops
Date: 2022-01-21T17:11:36+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Accountz Club;Genesis Market
Slug: 2022-01-21-crime-shop-sells-hacked-logins-to-other-crime-shops

[Source](https://krebsonsecurity.com/2022/01/crime-shop-sells-hacked-logins-to-other-crime-shops/){:target="_blank" rel="noopener"}

> Up for the “Most Meta Cybercrime Offering” award this year is Accountz Club, a new cybercrime store that sells access to purloined accounts at services built for cybercriminals, including shops peddling stolen payment cards and identities, spamming tools, email and phone bombing services, and those selling authentication cookies for a slew of popular websites. Criminals ripping off other crooks is a constant theme in the cybercrime underworld; Accountz Club’s slogan — “the best autoshop for your favorite shops’ accounts” — just normalizes this activity by making logins stolen from users of various cybercrime shops for sale at a fraction of [...]
