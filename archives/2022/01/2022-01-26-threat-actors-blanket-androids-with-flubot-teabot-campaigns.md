Title: Threat Actors Blanket Androids with Flubot, Teabot Campaigns
Date: 2022-01-26T14:02:07+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: 2022-01-26-threat-actors-blanket-androids-with-flubot-teabot-campaigns

[Source](https://threatpost.com/threat-actors-androids-flubot-teabot-campaigns/177991/){:target="_blank" rel="noopener"}

> Attackers are getting creative, using smishing & a malicious Google Play QR reader to plant banking trojans on the phones of victims across the globe. [...]
