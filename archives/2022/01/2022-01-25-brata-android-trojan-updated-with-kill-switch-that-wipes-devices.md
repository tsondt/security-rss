Title: BRATA Android Trojan Updated with ‘Kill Switch’ that Wipes Devices
Date: 2022-01-25T13:56:19+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security
Slug: 2022-01-25-brata-android-trojan-updated-with-kill-switch-that-wipes-devices

[Source](https://threatpost.com/brata-android-trojan-kill-switch-wipes/177921/){:target="_blank" rel="noopener"}

> Researchers identify three new versions of the banking trojan that include various new features, including GPS tracking and novel obfuscation techniques. [...]
