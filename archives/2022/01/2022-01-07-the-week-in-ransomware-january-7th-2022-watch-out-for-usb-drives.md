Title: The Week in Ransomware - January 7th 2022 - Watch out for USB drives
Date: 2022-01-07T17:50:34-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-07-the-week-in-ransomware-january-7th-2022-watch-out-for-usb-drives

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-7th-2022-watch-out-for-usb-drives/){:target="_blank" rel="noopener"}

> With the holidays these past two weeks, there have been only a few known ransomware attacks and little research released. Here is what we know. [...]
