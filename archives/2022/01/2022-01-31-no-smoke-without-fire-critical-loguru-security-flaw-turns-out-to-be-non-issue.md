Title: No smoke without fire? ‘Critical’ Loguru security flaw turns out to be non-issue
Date: 2022-01-31T14:45:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-31-no-smoke-without-fire-critical-loguru-security-flaw-turns-out-to-be-non-issue

[Source](https://portswigger.net/daily-swig/no-smoke-without-fire-critical-loguru-security-flaw-turns-out-to-be-non-issue){:target="_blank" rel="noopener"}

> Invalid CVE saga highlights potential problems in the automated vulnerability alert process [...]
