Title: Using Foreign Nationals to Bypass US Surveillance Restrictions
Date: 2022-01-13T15:35:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberespionage;espionage;FBI;law enforcement;privacy;surveillance
Slug: 2022-01-13-using-foreign-nationals-to-bypass-us-surveillance-restrictions

[Source](https://www.schneier.com/blog/archives/2022/01/using-foreign-nationals-to-bypass-us-surveillance-restrictions.html){:target="_blank" rel="noopener"}

> Remember when the US and Australian police surreptitiously owned and operated the encrypted cell phone app ANOM? They arrested 800 people in 2021 based on that operation. New documents received by Motherboard show that over 100 of those phones were shipped to users in the US, far more than previously believed. What’s most interesting to me about this new information is how the US used the Australians to get around domestic spying laws: For legal reasons, the FBI did not monitor outgoing messages from Anom devices determined to be inside the U.S. Instead, the Australian Federal Police (AFP) monitored them [...]
