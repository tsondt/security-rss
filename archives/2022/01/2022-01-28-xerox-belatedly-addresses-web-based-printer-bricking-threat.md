Title: Xerox belatedly addresses web-based printer bricking threat
Date: 2022-01-28T14:15:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-28-xerox-belatedly-addresses-web-based-printer-bricking-threat

[Source](https://portswigger.net/daily-swig/xerox-belatedly-addresses-web-based-printer-bricking-threat){:target="_blank" rel="noopener"}

> Firmware flaw resolved after extended 28-month disclosure process [...]
