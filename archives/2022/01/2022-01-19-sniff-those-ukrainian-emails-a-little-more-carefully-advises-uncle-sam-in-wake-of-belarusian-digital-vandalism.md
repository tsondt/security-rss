Title: Sniff those Ukrainian emails a little more carefully, advises Uncle Sam in wake of Belarusian digital vandalism
Date: 2022-01-19T20:01:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-19-sniff-those-ukrainian-emails-a-little-more-carefully-advises-uncle-sam-in-wake-of-belarusian-digital-vandalism

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/19/us_cisa_ukraine_cross_infection_warning/){:target="_blank" rel="noopener"}

> NotPetya started over there, don't forget US companies should be on the lookout for security nasties from Ukrainian partners following the digital graffiti and malware attack launched against Ukraine by Belarus, the CISA has warned.... [...]
