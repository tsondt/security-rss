Title: The Week in Ransomware - January 21st 2022 - Arrests, Wipers, and More
Date: 2022-01-21T16:40:51-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-21-the-week-in-ransomware-january-21st-2022-arrests-wipers-and-more

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-21st-2022-arrests-wipers-and-more/){:target="_blank" rel="noopener"}

> It has been quite a busy week with ransomware, with law enforcement making arrests, data-wiping attacks, and the return of the Qlocker ransomware. [...]
