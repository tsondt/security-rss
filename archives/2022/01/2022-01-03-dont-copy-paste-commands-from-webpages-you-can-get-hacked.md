Title: Don't copy-paste commands from webpages — you can get hacked
Date: 2022-01-03T08:00:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-01-03-dont-copy-paste-commands-from-webpages-you-can-get-hacked

[Source](https://www.bleepingcomputer.com/news/security/dont-copy-paste-commands-from-webpages-you-can-get-hacked/){:target="_blank" rel="noopener"}

> Programmers, sysadmins, security researchers, and tech hobbyists copying-pasting commands from web pages into a console or terminal risk having their system compromised. Wizer's Gabriel Friedlander demonstrates an obvious, simple yet stunning trick that'll make you think twice before copying-pasting text from web pages. [...]
