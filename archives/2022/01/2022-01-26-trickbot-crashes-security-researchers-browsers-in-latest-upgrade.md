Title: TrickBot Crashes Security Researchers’ Browsers in Latest Upgrade
Date: 2022-01-26T22:39:34+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-01-26-trickbot-crashes-security-researchers-browsers-in-latest-upgrade

[Source](https://threatpost.com/trickbot-crash-security-researchers-browsers/178046/){:target="_blank" rel="noopener"}

> The malware has added an anti-debugging tool that crashes browser tabs when researchers use code beautifying for analysis. [...]
