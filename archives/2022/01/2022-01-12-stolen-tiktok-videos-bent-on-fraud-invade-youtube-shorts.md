Title: Stolen TikTok Videos, Bent on Fraud, Invade YouTube Shorts
Date: 2022-01-12T19:49:14+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security
Slug: 2022-01-12-stolen-tiktok-videos-bent-on-fraud-invade-youtube-shorts

[Source](https://threatpost.com/stolen-tiktok-videos-fraud-youtube-shorts/177600/){:target="_blank" rel="noopener"}

> Scammers easily game YouTube Shorts with viral TikTok content, bilking both creators and users. [...]
