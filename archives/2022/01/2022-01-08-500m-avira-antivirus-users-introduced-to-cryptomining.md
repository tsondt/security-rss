Title: 500M Avira Antivirus Users Introduced to Cryptomining
Date: 2022-01-08T18:05:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;AVAST!;Avira Crypto;cryptomining;Norton 360;Norton Crypto;NortonLifeLock
Slug: 2022-01-08-500m-avira-antivirus-users-introduced-to-cryptomining

[Source](https://krebsonsecurity.com/2022/01/500m-avira-antivirus-users-introduced-to-cryptomining/){:target="_blank" rel="noopener"}

> Many readers were surprised to learn recently that the popular Norton 360 antivirus suite now ships with a program which lets customers make money mining virtual currency. But Norton 360 isn’t alone in this dubious endeavor: Avira antivirus — which has built a base of 500 million users worldwide largely by making the product free — was recently bought by the same company that owns Norton 360 and is introducing its customers to a service called Avira Crypto. Avira Crypto Founded in 2006, Avira Operations GmbH & Co. KG is a German multinational software company best known for their Avira [...]
