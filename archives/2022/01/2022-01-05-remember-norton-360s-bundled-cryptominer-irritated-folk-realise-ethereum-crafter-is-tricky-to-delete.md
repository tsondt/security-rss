Title: Remember Norton 360's bundled cryptominer? Irritated folk realise Ethereum crafter is tricky to delete
Date: 2022-01-05T15:56:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-05-remember-norton-360s-bundled-cryptominer-irritated-folk-realise-ethereum-crafter-is-tricky-to-delete

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/05/norton_360_cryptominer_deletion/){:target="_blank" rel="noopener"}

> Disable anti-tamper features first and you'll be all right Norton antivirus's inbuilt cryptominer has re-entered the public consciousness after a random Twitter bod expressed annoyance at how difficult it is to uninstall.... [...]
