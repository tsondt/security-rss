Title: US arrests suspect who stole unpublished books in phishing attacks
Date: 2022-01-06T12:55:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-06-us-arrests-suspect-who-stole-unpublished-books-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-arrests-suspect-who-stole-unpublished-books-in-phishing-attacks/){:target="_blank" rel="noopener"}

> An Italian man allegedly involved in a multi-year scheme to fraudulently obtain hundreds of prepublication manuscripts was arrested on Wednesday at the John F. Kennedy International Airport, in New York. [...]
