Title: URL Parsing Bugs Allow DoS, RCE, Spoofing & More
Date: 2022-01-10T17:55:00+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-01-10-url-parsing-bugs-allow-dos-rce-spoofing-more

[Source](https://threatpost.com/url-parsing-bugs-dos-rce-spoofing/177493/){:target="_blank" rel="noopener"}

> Dangerous security bugs stemming from widespread inconsistencies among 16 popular third-party URL-parsing libraries could affect a wide swath of web applications. [...]
