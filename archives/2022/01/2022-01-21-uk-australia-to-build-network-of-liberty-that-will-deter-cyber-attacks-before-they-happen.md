Title: UK, Australia, to build 'network of liberty that will deter cyber attacks before they happen'
Date: 2022-01-21T08:02:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-01-21-uk-australia-to-build-network-of-liberty-that-will-deter-cyber-attacks-before-they-happen

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/21/uk_australia_cyber_and_critical_technology_partnership/){:target="_blank" rel="noopener"}

> Enhanced 'Cyber and Critical Technology Partnership' will transport crime to harsh penal regime on the other side of the world The United Kingdom and Australia have signed a Cyber and Critical Technology Partnership that will, among other things, transport criminals to a harsh penal regime on the other side of the world.... [...]
