Title: Keep tabs on your tables: Cloud SQL for MySQL launches database auditing
Date: 2022-01-19T17:00:00+00:00
Author: Akhil Jariwala
Category: GCP Security
Tags: Identity & Security;Databases
Slug: 2022-01-19-keep-tabs-on-your-tables-cloud-sql-for-mysql-launches-database-auditing

[Source](https://cloud.google.com/blog/products/databases/cloud-sql-for-mysql-launches-database-auditing/){:target="_blank" rel="noopener"}

> If you manage sensitive data in your MySQL database, you might be obligated to record and monitor user database activity, especially if you work in a regulated industry. Although you could set up MySQL’s slow query log or general log to create an audit trail of user activity, these logs significantly impact database performance and aren’t formatted optimally for auditing. Purpose-built, open source audit plugins are better, but they lack some of the advanced security features that enterprise users need, such as rule-based auditing and results masking. Cloud SQL for MySQL has developed a new audit plugin called the Cloud [...]
