Title: New York Attorney General flags 1.1 million online accounts compromised by credential stuffing attacks
Date: 2022-01-06T14:34:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-06-new-york-attorney-general-flags-11-million-online-accounts-compromised-by-credential-stuffing-attacks

[Source](https://portswigger.net/daily-swig/new-york-attorney-general-flags-1-1-million-online-accounts-compromised-by-credential-stuffing-attacks){:target="_blank" rel="noopener"}

> Bureau of Internet and Technology helped affected organizations secure accounts and bolster defenses [...]
