Title: Vulnerabilities and censorship tools among hot new features in Beijing's Olympics app
Date: 2022-01-19T18:11:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-01-19-vulnerabilities-and-censorship-tools-among-hot-new-features-in-beijings-olympics-app

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/19/olympics_app_vulnerabilities_censorship/){:target="_blank" rel="noopener"}

> Visitors have to install it 14 days prior to arrival in China until their departure Toronto-based Citizen Lab has warned that an app required by Beijing law to attend the 2022 Olympics contains vulnerabilities that can leak calls and data to malicious users, as well as the potential to subject the user to scanning for censored keywords.... [...]
