Title: ‘Anomalous’ spyware stealing credentials in industrial firms
Date: 2022-01-20T16:29:45-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-20-anomalous-spyware-stealing-credentials-in-industrial-firms

[Source](https://www.bleepingcomputer.com/news/security/anomalous-spyware-stealing-credentials-in-industrial-firms/){:target="_blank" rel="noopener"}

> Researchers have uncovered several spyware campaigns that target industrial enterprises, aiming to steal email account credentials and conduct financial fraud or resell them to other actors. [...]
