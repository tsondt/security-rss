Title: Report: DDoS attacks increasing year on year as cybercriminals demand extortionate payouts
Date: 2022-01-10T16:06:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-10-report-ddos-attacks-increasing-year-on-year-as-cybercriminals-demand-extortionate-payouts

[Source](https://portswigger.net/daily-swig/report-ddos-attacks-increasing-year-on-year-as-cybercriminals-demand-extortionate-payouts){:target="_blank" rel="noopener"}

> Crooks attempt to cash in by upping the ante [...]
