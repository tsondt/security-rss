Title: Microsoft warns of multi-stage phishing campaign leveraging Azure AD
Date: 2022-01-27T13:11:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-27-microsoft-warns-of-multi-stage-phishing-campaign-leveraging-azure-ad

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-multi-stage-phishing-campaign-leveraging-azure-ad/){:target="_blank" rel="noopener"}

> Microsoft's threat analysts have uncovered a large-scale, multi-phase phishing campaign that uses stolen credentials to register devices onto the target's network and use them to distribute phishing emails. [...]
