Title: Canada's foreign affairs ministry hacked, some services down
Date: 2022-01-25T01:38:13-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-01-25-canadas-foreign-affairs-ministry-hacked-some-services-down

[Source](https://www.bleepingcomputer.com/news/security/canadas-foreign-affairs-ministry-hacked-some-services-down/){:target="_blank" rel="noopener"}

> The Canadian government department for foreign and consular relations, Global Affairs Canada was hit by a cyberattack last week. While critical services remain accessible, access to some online services is currently not available, as government systems continue to recover from the attack. [...]
