Title: Best practices for cross-Region aggregation of security findings
Date: 2022-01-18T23:22:30+00:00
Author: Marshall Jones
Category: AWS Security
Tags: AWS Security Hub;Intermediate (200);Security;Security, Identity, & Compliance;Cross-Region;cross-region aggregation;multi-region;securityhub
Slug: 2022-01-18-best-practices-for-cross-region-aggregation-of-security-findings

[Source](https://aws.amazon.com/blogs/security/best-practices-for-cross-region-aggregation-of-security-findings/){:target="_blank" rel="noopener"}

> AWS Security Hub enables customers to have a centralized view into the security posture across their AWS environment by aggregating your security alerts from various AWS services and partner products in a standardized format so that you can more easily take action on them. To facilitate that central view, Security Hub allows you to designate [...]
