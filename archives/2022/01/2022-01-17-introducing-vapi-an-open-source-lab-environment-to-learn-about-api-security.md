Title: Introducing vAPI – an open source lab environment to learn about API security
Date: 2022-01-17T11:55:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-17-introducing-vapi-an-open-source-lab-environment-to-learn-about-api-security

[Source](https://portswigger.net/daily-swig/introducing-vapi-an-open-source-lab-environment-to-learn-about-api-security){:target="_blank" rel="noopener"}

> Platform aims to educate security professionals on the challenges of securing modern web APIs [...]
