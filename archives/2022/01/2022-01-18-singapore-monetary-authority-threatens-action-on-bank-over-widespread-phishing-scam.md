Title: Singapore monetary authority threatens action on bank over widespread phishing scam
Date: 2022-01-18T13:04:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-01-18-singapore-monetary-authority-threatens-action-on-bank-over-widespread-phishing-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/18/singapore_monetary_authority_threatens_action/){:target="_blank" rel="noopener"}

> Scam has claimed 469 victims in December alone, of which OCBC has issued goodwill payments to 30 The Monetary Authority of Singapore says it is considering supervisory action against Southeast Asia's second largest bank, Oversea-Chinese Banking Corporation (OCBC), which was criticised for its incident response to a widespread phishing scheme across the island nation.... [...]
