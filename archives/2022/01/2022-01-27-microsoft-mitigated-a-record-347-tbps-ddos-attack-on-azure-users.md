Title: Microsoft mitigated a record 3.47 Tbps DDoS attack on Azure users
Date: 2022-01-27T08:12:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-27-microsoft-mitigated-a-record-347-tbps-ddos-attack-on-azure-users

[Source](https://www.bleepingcomputer.com/news/security/microsoft-mitigated-a-record-347-tbps-ddos-attack-on-azure-users/){:target="_blank" rel="noopener"}

> Microsoft says its Azure DDoS protection platform mitigated a massive 3.47 terabits per second (Tbps) distributed denial of service (DDoS) attack targeting an Azure customer from Asia in November. [...]
