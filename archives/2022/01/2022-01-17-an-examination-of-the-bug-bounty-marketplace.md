Title: An Examination of the Bug Bounty Marketplace
Date: 2022-01-17T12:16:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;economics of security;hacking;reports
Slug: 2022-01-17-an-examination-of-the-bug-bounty-marketplace

[Source](https://www.schneier.com/blog/archives/2022/01/an-examination-of-the-bug-bounty-marketplace.html){:target="_blank" rel="noopener"}

> Here’s a fascinating report: “ Bounty Everything: Hackers and the Making of the Global Bug Marketplace.” From a summary :...researchers Ryan Ellis and Yuan Stevens provide a window into the working lives of hackers who participate in “bug bounty” programs­ — programs that hire hackers to discover and report bugs or other vulnerabilities in their systems. This report illuminates the risks and insecurities for hackers as gig workers, and how bounty programs rely on vulnerable workers to fix their vulnerable systems. Ellis and Stevens’s research offers a historical overview of bounty programs and an analysis of contemporary bug bounty platforms [...]
