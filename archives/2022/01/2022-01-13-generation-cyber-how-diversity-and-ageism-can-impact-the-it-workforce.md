Title: Generation cyber: How diversity and ageism can impact the IT workforce
Date: 2022-01-13T14:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-13-generation-cyber-how-diversity-and-ageism-can-impact-the-it-workforce

[Source](https://portswigger.net/daily-swig/generation-cyber-how-diversity-and-ageism-can-impact-the-it-workforce){:target="_blank" rel="noopener"}

> Report claims generational attitudes can help or hinder the industry [...]
