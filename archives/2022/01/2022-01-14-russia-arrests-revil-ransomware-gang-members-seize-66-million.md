Title: Russia arrests REvil ransomware gang members, seize $6.6 million
Date: 2022-01-14T08:51:17-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-01-14-russia-arrests-revil-ransomware-gang-members-seize-66-million

[Source](https://www.bleepingcomputer.com/news/security/russia-arrests-revil-ransomware-gang-members-seize-66-million/){:target="_blank" rel="noopener"}

> The Federal Security Service (FSB) of the Russian Federation has announced today that they shut down the REvil ransomware gang after U.S. authorities reported on the leader. [...]
