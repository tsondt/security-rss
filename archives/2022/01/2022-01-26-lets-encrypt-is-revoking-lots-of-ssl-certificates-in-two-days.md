Title: Let's Encrypt is revoking lots of SSL certificates in two days
Date: 2022-01-26T05:38:12-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-01-26-lets-encrypt-is-revoking-lots-of-ssl-certificates-in-two-days

[Source](https://www.bleepingcomputer.com/news/security/lets-encrypt-is-revoking-lots-of-ssl-certificates-in-two-days/){:target="_blank" rel="noopener"}

> Let's Encrypt will begin revoking certain SSL/TLS certificates issued within the last 90 days starting January 28, 2022. The move could impact millions of active Let's Encrypt certificates. [...]
