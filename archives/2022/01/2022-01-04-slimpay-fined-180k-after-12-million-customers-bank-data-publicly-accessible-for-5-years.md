Title: SlimPay fined €180k after 12 million customers' bank data publicly accessible for 5 years
Date: 2022-01-04T17:33:08+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-01-04-slimpay-fined-180k-after-12-million-customers-bank-data-publicly-accessible-for-5-years

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/04/slimpay_breach_fine/){:target="_blank" rel="noopener"}

> French regulator's investigation finds multiple breaches of GDPR SlimPay, a Paris-based subscription payment services company, has been fined €180,000 by the French CNIL regulatory body after it was found to have held sensitive customer data on a publicly accessible server for five years.... [...]
