Title: US Military Ties Prolific MuddyWater Cyberespionage APT to Iran
Date: 2022-01-13T17:35:34+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Malware;Vulnerabilities;Web Security
Slug: 2022-01-13-us-military-ties-prolific-muddywater-cyberespionage-apt-to-iran

[Source](https://threatpost.com/us-military-ties-muddywater-cyberespionage-apt-iran/177633/){:target="_blank" rel="noopener"}

> US Cyber Command linked the group to Iranian intelligence and detailed its multi-pronged, increasingly sophisticated suite of malware tools. [...]
