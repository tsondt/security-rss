Title: Zoho plugs another critical security hole in Desktop Central
Date: 2022-01-17T13:04:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-17-zoho-plugs-another-critical-security-hole-in-desktop-central

[Source](https://www.bleepingcomputer.com/news/security/zoho-plugs-another-critical-security-hole-in-desktop-central/){:target="_blank" rel="noopener"}

> Zoho has addressed a new critical severity vulnerability found to affect the company's Desktop Central and Desktop Central MSP unified endpoint management (UEM) solutions. [...]
