Title: Dutch cybersecurity agency warns of lingering Log4j risks
Date: 2022-01-22T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-22-dutch-cybersecurity-agency-warns-of-lingering-log4j-risks

[Source](https://www.bleepingcomputer.com/news/security/dutch-cybersecurity-agency-warns-of-lingering-log4j-risks/){:target="_blank" rel="noopener"}

> In a warning issued on Thursday, the Dutch National Cybersecurity Centre (NCSC) says organizations should still be aware of risks connected to Log4j attacks and remain vigilant for ongoing threats. [...]
