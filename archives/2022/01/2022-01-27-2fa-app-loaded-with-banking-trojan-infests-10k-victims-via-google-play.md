Title: 2FA App Loaded with Banking Trojan Infests 10K Victims via Google Play
Date: 2022-01-27T20:59:53+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security;Web Security
Slug: 2022-01-27-2fa-app-loaded-with-banking-trojan-infests-10k-victims-via-google-play

[Source](https://threatpost.com/2fa-app-banking-trojan-google-play/178077/){:target="_blank" rel="noopener"}

> The Vultur trojan steals bank credentials but asks for permissions to do far more damage down the line. [...]
