Title: RCE bug chain patched in CentOS Web Panel
Date: 2022-01-24T15:30:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-24-rce-bug-chain-patched-in-centos-web-panel

[Source](https://portswigger.net/daily-swig/rce-bug-chain-patched-in-centos-web-panel){:target="_blank" rel="noopener"}

> Shell injected on servers via bypass of local file inclusion defenses [...]
