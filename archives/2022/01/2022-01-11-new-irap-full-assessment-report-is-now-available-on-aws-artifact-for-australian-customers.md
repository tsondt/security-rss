Title: New IRAP full assessment report is now available on AWS Artifact for Australian customers
Date: 2022-01-11T16:34:22+00:00
Author: Clara Lim
Category: AWS Security
Tags: Announcements;AWS Artifact;Foundational (100);Security, Identity, & Compliance;Australia;IRAP;Security Blog
Slug: 2022-01-11-new-irap-full-assessment-report-is-now-available-on-aws-artifact-for-australian-customers

[Source](https://aws.amazon.com/blogs/security/new-irap-full-assessment-report-is-now-available-on-aws-artifact-for-australian-customers/){:target="_blank" rel="noopener"}

> We are excited to announce that a new Information Security Registered Assessors Program (IRAP) report is now available on AWS Artifact, after a successful full assessment completed in December 2021 by an independent ASD (Australian Signals Directorate) certified IRAP assessor. The new IRAP report includes reassessment of the existing 111 services which are already in [...]
