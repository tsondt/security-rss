Title: California public office admits Covid-19 healthcare data breach
Date: 2022-01-25T13:39:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-25-california-public-office-admits-covid-19-healthcare-data-breach

[Source](https://portswigger.net/daily-swig/california-public-office-admits-covid-19-healthcare-data-breach){:target="_blank" rel="noopener"}

> Some citizens’ personal information was available to view online [...]
