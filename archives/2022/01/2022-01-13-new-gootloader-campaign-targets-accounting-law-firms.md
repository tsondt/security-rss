Title: New GootLoader Campaign Targets Accounting, Law Firms
Date: 2022-01-13T15:04:01+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-01-13-new-gootloader-campaign-targets-accounting-law-firms

[Source](https://threatpost.com/gootloader-accounting-law-firms/177629/){:target="_blank" rel="noopener"}

> GootLoader hijacks WordPress sites to lure professionals to download malicious sample contract templates. [...]
