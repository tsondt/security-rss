Title: Fashion giant Moncler confirms data breach after ransomware attack
Date: 2022-01-18T14:51:50-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-18-fashion-giant-moncler-confirms-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/fashion-giant-moncler-confirms-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Italian luxury fashion giant Moncler confirmed that they suffered a data breach after files were stolen by the AlphV/BlackCat ransomware operation in December and published today on the dark web. [...]
