Title: Salesforce mandates MFA by default
Date: 2022-01-07T07:30:13+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2022-01-07-salesforce-mandates-mfa-by-default

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/07/salesforce_mandates_mfa_by_default/){:target="_blank" rel="noopener"}

> Thales: ‘Significant change in security culture' Paid Feature Of all the cybersecurity developments in 2021, a relatively low-key announcement made by software company Salesforce.com (SFDC) in March might eventually turn out to be one of the most significant.... [...]
