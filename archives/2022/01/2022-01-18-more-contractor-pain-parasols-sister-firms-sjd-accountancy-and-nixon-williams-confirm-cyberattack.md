Title: More contractor pain: Parasol's sister firms, SJD Accountancy and Nixon Williams, confirm cyberattack
Date: 2022-01-18T14:45:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-18-more-contractor-pain-parasols-sister-firms-sjd-accountancy-and-nixon-williams-confirm-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/18/sjd_accountancy_ransomware_attack/){:target="_blank" rel="noopener"}

> Ransomware suspected but not confirmed SJD Accountancy and Nixon Williams – both contractor-focused beancounting firms owned by the same corporate parent as cyber-attack-struck UK umbrella company Parasol – have been hit by online attackers.... [...]
