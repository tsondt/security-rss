Title: Apache Software Foundation warns its patching efforts are being undercut by use of end-of-life software
Date: 2022-01-14T15:01:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-14-apache-software-foundation-warns-its-patching-efforts-are-being-undercut-by-use-of-end-of-life-software

[Source](https://portswigger.net/daily-swig/apache-software-foundation-warns-its-patching-efforts-are-being-undercut-by-use-of-end-of-life-software){:target="_blank" rel="noopener"}

> Non-profit shares metrics in its latest annual security review of 350-plus projects [...]
