Title: You better have patched those Log4j holes or we'll see what a judge has to say – FTC
Date: 2022-01-05T22:30:03+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-05-you-better-have-patched-those-log4j-holes-or-well-see-what-a-judge-has-to-say-ftc

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/05/ftc_log4j_fix/){:target="_blank" rel="noopener"}

> Apply fixes responsibly in a timely manner or face the wrath of Lina Khan The US Federal Trade Commission on Tuesday warned companies that vulnerable Log4j software needs to be patched... or else.... [...]
