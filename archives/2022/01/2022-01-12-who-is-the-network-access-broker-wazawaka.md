Title: Who is the Network Access Broker ‘Wazawaka?’
Date: 2022-01-12T05:17:31+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;Ransomware;902228;Abakan;Abaza;Constella Intelligence;cs-arena.org;DarkSide;ddosis.ru;devdelphi@yandex.ru;domaintools;Flashpoint;initial access broker;Kopyovo-a;LockBit;Mikhail Matveev;Mikhail Mix Matveev;mix@devilart.net;mixfb@yandex.ru;ransomware;Uhodiransomware;Wazawaka
Slug: 2022-01-12-who-is-the-network-access-broker-wazawaka

[Source](https://krebsonsecurity.com/2022/01/who-is-the-network-access-broker-wazawaka/){:target="_blank" rel="noopener"}

> In a great many ransomware attacks, the criminals who pillage the victim’s network are not the same crooks who gained the initial access to the victim organization. More commonly, the infected PC or stolen VPN credentials the gang used to break in were purchased from a cybercriminal middleman known as an initial access broker. This post examines some of the clues left behind by “ Wazawaka,” the hacker handle chosen by a major access broker in the Russian-speaking cybercrime scene. Wazawaka has been a highly active member of multiple cybercrime forums over the past decade, but his favorite is the [...]
