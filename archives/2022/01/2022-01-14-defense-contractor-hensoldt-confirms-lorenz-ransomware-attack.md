Title: Defense contractor Hensoldt confirms Lorenz ransomware attack
Date: 2022-01-14T12:33:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-14-defense-contractor-hensoldt-confirms-lorenz-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/defense-contractor-hensoldt-confirms-lorenz-ransomware-attack/){:target="_blank" rel="noopener"}

> Hensoldt, a multinational defense contractor headquartered in Germany, has confirmed that some of its UK subsidiary's systems were compromised in a ransomware attack. [...]
