Title: 3.7M FlexBooker Records Dumped on Hacker Forum
Date: 2022-01-07T19:12:20+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;News
Slug: 2022-01-07-37m-flexbooker-records-dumped-on-hacker-forum

[Source](https://threatpost.com/flexbooker-records-dumped-hacker-forum/177460/){:target="_blank" rel="noopener"}

> Attackers are trading millions of records from a trio of pre-holiday breaches on an online forum. [...]
