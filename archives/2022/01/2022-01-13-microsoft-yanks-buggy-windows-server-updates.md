Title: Microsoft Yanks Buggy Windows Server Updates
Date: 2022-01-13T23:08:53+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-01-13-microsoft-yanks-buggy-windows-server-updates

[Source](https://threatpost.com/microsoft-yanks-buggy-windows-server-updates/177648/){:target="_blank" rel="noopener"}

> Since their release on Patch Tuesday, the updates have been breaking Windows, causing spontaneous boot loops on Windows domain controller servers, breaking Hyper-V and making ReFS volume systems unavailable. [...]
