Title: Why should I pay for that security option? Hijacking only happens to planes
Date: 2022-01-21T08:30:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2022-01-21-why-should-i-pay-for-that-security-option-hijacking-only-happens-to-planes

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/21/on_call/){:target="_blank" rel="noopener"}

> But if I give him my bank details, I'll be rich! On Call Friday is here. We'd suggest an adult beverage or two to celebrate, but only if you BYOB. While you fill your suitcase, may we present an episode of On Call in which a reader saves his boss from a dunking.... [...]
