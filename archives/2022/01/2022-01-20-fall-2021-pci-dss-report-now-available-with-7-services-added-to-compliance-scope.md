Title: Fall 2021 PCI DSS report now available with 7 services added to compliance scope
Date: 2022-01-20T23:12:29+00:00
Author: Michael Oyeniya
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;Compliance reports;PCI;PCI DSS
Slug: 2022-01-20-fall-2021-pci-dss-report-now-available-with-7-services-added-to-compliance-scope

[Source](https://aws.amazon.com/blogs/security/fall-2021-pci-dss-report-now-available-with-7-services-added-to-compliance-scope/){:target="_blank" rel="noopener"}

> We’re continuing to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that seven new services have been added to the scope of our Payment Card Industry Data Security Standard (PCI DSS) certification. These new services provide our customers with more options to process and store their [...]
