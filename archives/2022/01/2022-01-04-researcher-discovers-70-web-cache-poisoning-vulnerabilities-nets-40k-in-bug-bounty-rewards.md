Title: Researcher discovers 70 web cache poisoning vulnerabilities, nets $40k in bug bounty rewards
Date: 2022-01-04T12:01:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-04-researcher-discovers-70-web-cache-poisoning-vulnerabilities-nets-40k-in-bug-bounty-rewards

[Source](https://portswigger.net/daily-swig/researcher-discovers-70-web-cache-poisoning-vulnerabilities-nets-40k-in-bug-bounty-rewards){:target="_blank" rel="noopener"}

> Targets included GitHub, GitLab, HackerOne, and Cloudflare [...]
