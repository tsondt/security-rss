Title: Night Sky is the latest ransomware targeting corporate networks
Date: 2022-01-06T17:09:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-06-night-sky-is-the-latest-ransomware-targeting-corporate-networks

[Source](https://www.bleepingcomputer.com/news/security/night-sky-is-the-latest-ransomware-targeting-corporate-networks/){:target="_blank" rel="noopener"}

> It's a new year, and with it comes a new ransomware to keep an eye on called 'Night Sky' that targets corporate networks and steals data in double-extortion attacks. [...]
