Title: GCHQ was rebuked for ignoring spy law safeguards as pandemic hit Britain
Date: 2022-01-10T12:47:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-10-gchq-was-rebuked-for-ignoring-spy-law-safeguards-as-pandemic-hit-britain

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/10/ipco_report_2020/){:target="_blank" rel="noopener"}

> Auditor IPCO flagged it up – but then approved 99.94% of state snooping Former foreign secretary Dominic Raab rebuked GCHQ for secretly halting internal compliance audits that ensured the spy agency was obeying the law, a government report has revealed – while just 0.06 per cent of spying requests made by Britain's public sector were refused by its supposed overseer.... [...]
