Title: At Request of U.S., Russia Rounds Up 14 REvil Ransomware Affiliates
Date: 2022-01-14T22:41:34+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;The Coming Storm;Andrey Sergeevich Bessonov;Colonial Pipeline;DarkSide;Dmitri Alperovitch;FSB;GandCrab;Immersive Labs;Kevin Breen;NotPetya;President Biden;rEvil;Roman Gennadyevich Muromsky;Vladimir Putin
Slug: 2022-01-14-at-request-of-us-russia-rounds-up-14-revil-ransomware-affiliates

[Source](https://krebsonsecurity.com/2022/01/at-request-of-u-s-russia-rounds-up-14-revil-ransomware-affiliates/){:target="_blank" rel="noopener"}

> The Russian government said today it arrested 14 people accused of working for “ REvil,” a particularly aggressive ransomware group that has extorted hundreds of millions of dollars from victim organizations. The Russian Federal Security Service (FSB) said the actions were taken in response to a request from U.S. officials, but many experts believe the crackdown is part of an effort to reduce tensions over Russian President Vladimir Putin’s decision to station 100,000 troops along the nation’s border with Ukraine. The FSB headquarters at Lubyanka Square, Moscow. Image: Wikipedia. The FSB said it arrested 14 REvil ransomware members, and searched [...]
