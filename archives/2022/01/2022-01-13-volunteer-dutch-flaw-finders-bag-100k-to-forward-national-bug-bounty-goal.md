Title: Volunteer Dutch flaw finders bag $100k to forward national bug bounty goal
Date: 2022-01-13T08:33:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-13-volunteer-dutch-flaw-finders-bag-100k-to-forward-national-bug-bounty-goal

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/13/divd_bug_bounty/){:target="_blank" rel="noopener"}

> Huntress Labs tips some loose change into vuln-spotters' cup The Dutch Initiative for Vulnerability Disclosure has scored $100k towards its founder's hope of a nationwide bug bounty available for anything at all.... [...]
