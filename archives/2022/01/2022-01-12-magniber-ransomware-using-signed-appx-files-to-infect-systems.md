Title: Magniber ransomware using signed APPX files to infect systems
Date: 2022-01-12T12:53:27-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-12-magniber-ransomware-using-signed-appx-files-to-infect-systems

[Source](https://www.bleepingcomputer.com/news/security/magniber-ransomware-using-signed-appx-files-to-infect-systems/){:target="_blank" rel="noopener"}

> The Magniber ransomware has been spotted using Windows application package files (.APPX) signed with valid certificates to drop malware pretending to be Chrome and Edge web browser updates. [...]
