Title: China orders web operators to spring clean its entire internet
Date: 2022-01-27T03:01:25+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-01-27-china-orders-web-operators-to-spring-clean-its-entire-internet

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/27/china_internet_spring_clean/){:target="_blank" rel="noopener"}

> Purveyors of rumours, flirty teens, dodgy infomercials, or bloody violence all told to get in the bin – yet again The Cyberspace Administration of China (CAC) has shared its spring-cleaning plans with the world – and suggested it's time to make the Middle Kingdom's web sites sparkle with wholesome content.... [...]
