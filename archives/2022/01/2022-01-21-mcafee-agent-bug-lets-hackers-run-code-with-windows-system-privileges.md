Title: McAfee Agent bug lets hackers run code with Windows SYSTEM privileges
Date: 2022-01-21T08:22:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-21-mcafee-agent-bug-lets-hackers-run-code-with-windows-system-privileges

[Source](https://www.bleepingcomputer.com/news/security/mcafee-agent-bug-lets-hackers-run-code-with-windows-system-privileges/){:target="_blank" rel="noopener"}

> McAfee Enterprise (now rebranded as Trellix) has patched a security vulnerability discovered in the company's McAfee Agent software for Windows enabling attackers to escalate privileges and execute arbitrary code with SYSTEM privileges. [...]
