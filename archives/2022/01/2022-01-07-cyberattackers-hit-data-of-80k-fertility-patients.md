Title: Cyberattackers Hit Data of 80K Fertility Patients
Date: 2022-01-07T21:14:35+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;IoT;Privacy;Vulnerabilities;Web Security
Slug: 2022-01-07-cyberattackers-hit-data-of-80k-fertility-patients

[Source](https://threatpost.com/cyberattackers-data-80k-patients-fertility-centers-illinois/177467/){:target="_blank" rel="noopener"}

> Fertility Centers of Illinois' security measures protected electronic medical records, but the attackers still got at extremely intimate data in admin files. [...]
