Title: IP spoofing bug leaves Django REST applications open to DDoS, password-cracking attacks
Date: 2022-01-11T12:21:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-11-ip-spoofing-bug-leaves-django-rest-applications-open-to-ddos-password-cracking-attacks

[Source](https://portswigger.net/daily-swig/ip-spoofing-bug-leaves-django-rest-applications-open-to-ddos-password-cracking-attacks){:target="_blank" rel="noopener"}

> Security researcher discovers how to send unlimited HTTP requests with the same client [...]
