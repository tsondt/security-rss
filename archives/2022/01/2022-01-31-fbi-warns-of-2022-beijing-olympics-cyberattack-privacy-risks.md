Title: FBI warns of 2022 Beijing Olympics cyberattack, privacy risks
Date: 2022-01-31T18:27:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-31-fbi-warns-of-2022-beijing-olympics-cyberattack-privacy-risks

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-2022-beijing-olympics-cyberattack-privacy-risks/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warned today that threat actors could potentially target the February 2022 Beijing Winter Olympics and March 2022 Paralympics. However, evidence of such attacks being planned is yet to be uncovered. [...]
