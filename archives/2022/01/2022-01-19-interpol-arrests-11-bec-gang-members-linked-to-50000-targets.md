Title: Interpol arrests 11 BEC gang members linked to 50,000 targets
Date: 2022-01-19T07:16:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-19-interpol-arrests-11-bec-gang-members-linked-to-50000-targets

[Source](https://www.bleepingcomputer.com/news/security/interpol-arrests-11-bec-gang-members-linked-to-50-000-targets/){:target="_blank" rel="noopener"}

> Interpol, in coordination with the Nigerian Police Force, have arrested eleven individuals who are suspects of participating in an international BEC (business email compromise) ring. [...]
