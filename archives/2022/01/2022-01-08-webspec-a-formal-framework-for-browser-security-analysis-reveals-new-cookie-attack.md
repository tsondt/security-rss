Title: WebSpec, a formal framework for browser security analysis, reveals new cookie attack
Date: 2022-01-08T08:45:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-08-webspec-a-formal-framework-for-browser-security-analysis-reveals-new-cookie-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/08/webspec_browser_security/){:target="_blank" rel="noopener"}

> Boffins in Vienna devise way to make software prove how it behaves Folks at Technische Universität Wien in Austria have devised a formal security framework called WebSpec to analyze browser security.... [...]
