Title: 20K WordPress Sites Exposed by Insecure Plugin REST-API
Date: 2022-01-21T18:19:37+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-01-21-20k-wordpress-sites-exposed-by-insecure-plugin-rest-api

[Source](https://threatpost.com/wordpress-insecure-plugin-rest-api/177866/){:target="_blank" rel="noopener"}

> The WordPress WP HTML Mail plugin for personalized emails is vulnerable to code injection and phishing due to XSS. [...]
