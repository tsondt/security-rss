Title: Broward Breach Highlights Healthcare Supply-Chain Problems
Date: 2022-01-05T21:09:35+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;IoT;Vulnerabilities;Web Security
Slug: 2022-01-05-broward-breach-highlights-healthcare-supply-chain-problems

[Source](https://threatpost.com/broward-breach-healthcare-supply-chain/177401/){:target="_blank" rel="noopener"}

> More than 1.3 million patient records were stolen in the just-disclosed breach, which occurred back in October. [...]
