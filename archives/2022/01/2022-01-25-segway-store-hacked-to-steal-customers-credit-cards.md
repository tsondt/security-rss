Title: Segway store hacked to steal customers' credit cards
Date: 2022-01-25T09:59:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-25-segway-store-hacked-to-steal-customers-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/segway-store-hacked-to-steal-customers-credit-cards/){:target="_blank" rel="noopener"}

> Segway's online store was compromised to include a malicious Magecart script that potentially allowed threat actors to steal credit cards and customer information during checkout. [...]
