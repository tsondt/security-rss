Title: SonicWall: Y2K22 bug hits Email Security, firewall products
Date: 2022-01-07T16:56:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-07-sonicwall-y2k22-bug-hits-email-security-firewall-products

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-y2k22-bug-hits-email-security-firewall-products/){:target="_blank" rel="noopener"}

> SonicWall has confirmed today that some of its Email Security and firewall products have been hit by the Y2K22 bug, causing message log updates and junk box failures starting with January 1, 2022. [...]
