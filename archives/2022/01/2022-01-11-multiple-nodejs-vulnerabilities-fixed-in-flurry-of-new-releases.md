Title: Multiple Node.js vulnerabilities fixed in flurry of new releases
Date: 2022-01-11T14:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-11-multiple-nodejs-vulnerabilities-fixed-in-flurry-of-new-releases

[Source](https://portswigger.net/daily-swig/multiple-node-js-vulnerabilities-fixed-in-flurry-of-new-releases){:target="_blank" rel="noopener"}

> Three medium-impact and one low severity bug have been patched [...]
