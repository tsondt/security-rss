Title: Open debug mode in Cisco mobile networking software created critical security hole
Date: 2022-01-21T13:45:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-21-open-debug-mode-in-cisco-mobile-networking-software-created-critical-security-hole

[Source](https://portswigger.net/daily-swig/open-debug-mode-in-cisco-mobile-networking-software-created-critical-security-hole){:target="_blank" rel="noopener"}

> Patch issued after testing engineers uncover RCE threat [...]
