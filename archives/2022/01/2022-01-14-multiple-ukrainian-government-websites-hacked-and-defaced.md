Title: Multiple Ukrainian government websites hacked and defaced
Date: 2022-01-14T11:11:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-01-14-multiple-ukrainian-government-websites-hacked-and-defaced

[Source](https://www.bleepingcomputer.com/news/security/multiple-ukrainian-government-websites-hacked-and-defaced/){:target="_blank" rel="noopener"}

> At least 15 websites belonging to various Ukrainian public institutions were compromised, defaced, and subsequently taken offline. [...]
