Title: Uber Bug, Ignored for Years, Casts Doubt on Official Uber Emails
Date: 2022-01-05T20:49:37+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-01-05-uber-bug-ignored-for-years-casts-doubt-on-official-uber-emails

[Source](https://threatpost.com/uber-bug-ignored-emails/177395/){:target="_blank" rel="noopener"}

> A simple-to-exploit bug that allows bad actors to send emails from Uber's official system – skating past email security – went unaddressed despite flagging by multiple researchers. [...]
