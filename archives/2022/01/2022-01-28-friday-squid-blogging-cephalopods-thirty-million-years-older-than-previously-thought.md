Title: Friday Squid Blogging: Cephalopods Thirty Million Years Older Than Previously Thought
Date: 2022-01-28T22:04:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-01-28-friday-squid-blogging-cephalopods-thirty-million-years-older-than-previously-thought

[Source](https://www.schneier.com/blog/archives/2022/01/friday-squid-blogging-cephalopods-thirty-million-years-older-than-previously-thought.html){:target="_blank" rel="noopener"}

> New fossils from Newfoundland push the origins of cephalopods to 522 million years ago. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
