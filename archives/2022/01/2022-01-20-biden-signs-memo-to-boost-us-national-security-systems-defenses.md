Title: Biden signs memo to boost US national security systems’ defenses
Date: 2022-01-20T08:57:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-01-20-biden-signs-memo-to-boost-us-national-security-systems-defenses

[Source](https://www.bleepingcomputer.com/news/security/biden-signs-memo-to-boost-us-national-security-systems-defenses/){:target="_blank" rel="noopener"}

> President Joe Biden signed a national security memorandum (NSM) on Wednesday to increase the security of national security systems part of critical US government networks used in military and intelligence activities when storing or transferring classified info. [...]
