Title: Nobel Foundation site hit by DDoS attack on award day
Date: 2022-01-26T05:04:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-26-nobel-foundation-site-hit-by-ddos-attack-on-award-day

[Source](https://www.bleepingcomputer.com/news/security/nobel-foundation-site-hit-by-ddos-attack-on-award-day/){:target="_blank" rel="noopener"}

> The Nobel Foundation and the Norwegian Nobel Institute have disclosed a cyber-attack that unfolded during the award ceremony on December 10, 2021. [...]
