Title: European Commission launches new open source software bug bounty program
Date: 2022-01-21T16:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-21-european-commission-launches-new-open-source-software-bug-bounty-program

[Source](https://portswigger.net/daily-swig/european-commission-launches-new-open-source-software-bug-bounty-program){:target="_blank" rel="noopener"}

> Hackers are invited to test services used by EU agencies [...]
