Title: Hackers are taking over CEO accounts with rogue OAuth apps
Date: 2022-01-28T09:29:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-28-hackers-are-taking-over-ceo-accounts-with-rogue-oauth-apps

[Source](https://www.bleepingcomputer.com/news/security/hackers-are-taking-over-ceo-accounts-with-rogue-oauth-apps/){:target="_blank" rel="noopener"}

> Threat analysts have observed a new campaign named 'OiVaVoii', targeting company executives and general managers with malicious OAuth apps and custom phishing lures sent from hijacked Office 365 accounts. [...]
