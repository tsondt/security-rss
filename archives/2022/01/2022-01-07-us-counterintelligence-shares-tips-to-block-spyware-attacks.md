Title: US counterintelligence shares tips to block spyware attacks
Date: 2022-01-07T11:22:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-07-us-counterintelligence-shares-tips-to-block-spyware-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-counterintelligence-shares-tips-to-block-spyware-attacks/){:target="_blank" rel="noopener"}

> The US National Counterintelligence and Security Center (NCSC) and the Department of State have jointly published guidance on defending against attacks using commercial surveillance tools. [...]
