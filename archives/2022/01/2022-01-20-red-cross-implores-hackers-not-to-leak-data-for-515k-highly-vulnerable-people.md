Title: Red Cross implores hackers not to leak data for 515k “highly vulnerable people”
Date: 2022-01-20T01:17:58+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;cyberattack;data breach;hacking;Red Cross
Slug: 2022-01-20-red-cross-implores-hackers-not-to-leak-data-for-515k-highly-vulnerable-people

[Source](https://arstechnica.com/?p=1827094){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) The Red Cross on Wednesday pleaded with the threat actors behind a cyberattack that stole the personal data of about 515,000 people who used a program that works to reunite family members separated by conflict, disaster or migration. "While we don't know who is responsible for this attack, or why they carried it out, we do have this appeal to make to them," Robert Mardini, the director-general of the International Committee for the Red Cross, said in a release. “Your actions could potentially cause yet more harm and pain to those who have already endured [...]
