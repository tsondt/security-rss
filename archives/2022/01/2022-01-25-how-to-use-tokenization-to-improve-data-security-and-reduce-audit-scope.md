Title: How to use tokenization to improve data security and reduce audit scope
Date: 2022-01-25T16:54:58+00:00
Author: Tim Winston
Category: AWS Security
Tags: AWS Security Token Service;Best Practices;Security, Identity, & Compliance;Technical How-to;AWS Data Lake;AWS KMS;Cardholder Data;Compliance;Encryption;PCI DSS;PHI;PII;Security Assurance;Tokenization
Slug: 2022-01-25-how-to-use-tokenization-to-improve-data-security-and-reduce-audit-scope

[Source](https://aws.amazon.com/blogs/security/how-to-use-tokenization-to-improve-data-security-and-reduce-audit-scope/){:target="_blank" rel="noopener"}

> Tokenization of sensitive data elements is a hot topic, but you may not know what to tokenize, or even how to determine if tokenization is right for your organization’s business needs. Industries subject to financial, data security, regulatory, or privacy compliance standards are increasingly looking for tokenization solutions to minimize distribution of sensitive data, reduce [...]
