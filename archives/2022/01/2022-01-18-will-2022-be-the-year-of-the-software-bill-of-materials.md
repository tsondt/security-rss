Title: Will 2022 Be the Year of the Software Bill of Materials?
Date: 2022-01-18T22:33:43+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Malware;Vulnerabilities;Web Security
Slug: 2022-01-18-will-2022-be-the-year-of-the-software-bill-of-materials

[Source](https://threatpost.com/2022-software-bill-of-materials/177736/){:target="_blank" rel="noopener"}

> Praise be & pass the recipe for the software soup: There's too much scrambling to untangle vulnerabilities and dependencies, say a security experts roundtable. [...]
