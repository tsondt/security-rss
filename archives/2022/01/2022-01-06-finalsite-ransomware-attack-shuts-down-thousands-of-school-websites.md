Title: FinalSite ransomware attack shuts down thousands of school websites
Date: 2022-01-06T19:34:06-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-06-finalsite-ransomware-attack-shuts-down-thousands-of-school-websites

[Source](https://www.bleepingcomputer.com/news/security/finalsite-ransomware-attack-shuts-down-thousands-of-school-websites/){:target="_blank" rel="noopener"}

> FinalSite, a leading school website services provider, has suffered a ransomware attack disrupting access to websites for thousands of schools worldwide. [...]
