Title: Disabling Security Hub controls in a multi-account environment
Date: 2022-01-06T20:49:46+00:00
Author: Priyank Ghedia
Category: AWS Security
Tags: AWS Security Hub;Best Practices
Slug: 2022-01-06-disabling-security-hub-controls-in-a-multi-account-environment

[Source](https://aws.amazon.com/blogs/security/disabling-security-hub-controls-in-a-multi-account-environment/){:target="_blank" rel="noopener"}

> In this blog post, you’ll learn about an automated process for disabling or enabling selected AWS Security Hub controls across multiple accounts and multiple regions. You may already know how to disable Security Hub controls through the Security Hub console, or using the Security Hub update-standards-control API. However, these methods work on a per account [...]
