Title: New SysJoker backdoor targets Windows, macOS, and Linux
Date: 2022-01-11T10:04:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-11-new-sysjoker-backdoor-targets-windows-macos-and-linux

[Source](https://www.bleepingcomputer.com/news/security/new-sysjoker-backdoor-targets-windows-macos-and-linux/){:target="_blank" rel="noopener"}

> A new multi-platform backdoor malware named 'SysJoker' has emerged in the wild, targeting Windows, Linux, and macOS with the ability to evade detection on all three operating systems. [...]
