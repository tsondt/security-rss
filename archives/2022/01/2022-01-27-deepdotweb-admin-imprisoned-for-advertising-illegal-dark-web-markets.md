Title: DeepDotWeb admin imprisoned for advertising illegal dark web markets
Date: 2022-01-27T16:13:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-27-deepdotweb-admin-imprisoned-for-advertising-illegal-dark-web-markets

[Source](https://www.bleepingcomputer.com/news/security/deepdotweb-admin-imprisoned-for-advertising-illegal-dark-web-markets/){:target="_blank" rel="noopener"}

> An Israeli citizen who operated DeepDotWeb (DDW), a news site and review site for dark web sites, has received a sentence of 97 months in prison for money laundering and was ordered to forfeit $8,414,173. [...]
