Title: Beijing 2022 Winter Olympics app bursting with privacy risks
Date: 2022-01-18T09:50:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-18-beijing-2022-winter-olympics-app-bursting-with-privacy-risks

[Source](https://www.bleepingcomputer.com/news/security/beijing-2022-winter-olympics-app-bursting-with-privacy-risks/){:target="_blank" rel="noopener"}

> The official app for Beijing 2022 Winter Olympics, 'My 2022,' was found to be insecure when it comes to protecting the sensitive data of its users. [...]
