Title: EoL Systems Stonewalling Log4j Fixes for Fed Agencies
Date: 2022-01-07T22:16:03+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Cloud Security;Government;Malware;Vulnerabilities;Web Security
Slug: 2022-01-07-eol-systems-stonewalling-log4j-fixes-for-fed-agencies

[Source](https://threatpost.com/eol-systems-stonewalling-log4j-fixes-for-fed-agencies/177475/){:target="_blank" rel="noopener"}

> End of life, end of support, pandemic-induced shipping delays and remote work, scanning failures: It’s a recipe for a patching nightmare, federal cyberserurity CTO Matt Keller says. [...]
