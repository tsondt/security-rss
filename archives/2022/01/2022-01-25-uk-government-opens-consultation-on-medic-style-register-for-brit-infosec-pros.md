Title: UK government opens consultation on medic-style register for Brit infosec pros
Date: 2022-01-25T10:14:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-25-uk-government-opens-consultation-on-medic-style-register-for-brit-infosec-pros

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/25/ukgov_cybersecurity_profession_regulation_ukcsc/){:target="_blank" rel="noopener"}

> Are you competent? Ethical? Welcome to UKCSC's new list Frustrated at lack of activity from the "standard setting" UK Cyber Security Council, the government wants to pass new laws making it into the statutory regulator of the UK infosec trade.... [...]
