Title: Linux Servers at Risk of RCE Due to Critical CWP Bugs
Date: 2022-01-24T23:08:56+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: News;Vulnerabilities;Web Security
Slug: 2022-01-24-linux-servers-at-risk-of-rce-due-to-critical-cwp-bugs

[Source](https://threatpost.com/linux-servers-rce-critical-cwp-bugs/177906/){:target="_blank" rel="noopener"}

> The two flaws in Control Web Panel – a popular web hosting management software used by 200K+ servers – allow code execution as root on Linux servers. [...]
