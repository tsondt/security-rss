Title: Twitter's top security staff out after incoming CEO shakes things up
Date: 2022-01-25T00:05:58+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2022-01-25-twitters-top-security-staff-out-after-incoming-ceo-shakes-things-up

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/25/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Nigerian BEC gang bust, NSO woes, and more In brief Twitter's head of security and CISO both ejected from the social media biz this month.... [...]
