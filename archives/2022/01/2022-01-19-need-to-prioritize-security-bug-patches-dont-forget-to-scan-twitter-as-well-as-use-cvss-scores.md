Title: Need to prioritize security bug patches? Don't forget to scan Twitter as well as use CVSS scores
Date: 2022-01-19T21:22:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-19-need-to-prioritize-security-bug-patches-dont-forget-to-scan-twitter-as-well-as-use-cvss-scores

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/19/twitter_cvss_vulnerabilites/){:target="_blank" rel="noopener"}

> Exploit, vulnerability discussion online can offer useful signals Organizations looking to minimize exposure to exploitable software should scan Twitter for mentions of security bugs as well as use the Common Vulnerability Scoring System or CVSS, Kenna Security argues.... [...]
