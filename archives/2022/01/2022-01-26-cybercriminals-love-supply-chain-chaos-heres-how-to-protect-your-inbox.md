Title: Cybercriminals Love Supply-Chain Chaos: Here’s How to Protect Your Inbox
Date: 2022-01-26T19:37:12+00:00
Author: Troy Gill
Category: Threatpost
Tags: InfoSec Insider;Web Security
Slug: 2022-01-26-cybercriminals-love-supply-chain-chaos-heres-how-to-protect-your-inbox

[Source](https://threatpost.com/cybercriminals-supply-chain-protect-inbox/178002/){:target="_blank" rel="noopener"}

> Threat actors use bogus 'shipping delays' to deceive customers and businesses. Troy Gill, senior manager of threat intelligence at Zix, discusses how spoofing is evolving and what to do. [...]
