Title: 1.1M Compromised Accounts Found at 17 Major Companies
Date: 2022-01-05T23:13:43+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Web Security
Slug: 2022-01-05-11m-compromised-accounts-found-at-17-major-companies

[Source](https://threatpost.com/compromised-accounts-17-major-companies/177417/){:target="_blank" rel="noopener"}

> The accounts fell victim to credential-stuffing attacks, according to the New York State AG. [...]
