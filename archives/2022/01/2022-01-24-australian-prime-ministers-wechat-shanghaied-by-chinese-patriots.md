Title: Australian Prime Minister's WeChat Shanghaied by Chinese patriots
Date: 2022-01-24T04:58:31+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-01-24-australian-prime-ministers-wechat-shanghaied-by-chinese-patriots

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/24/australian_prime_ministers_wechat_taken/){:target="_blank" rel="noopener"}

> Politicians rush to blame Beijing, Tencent says the account was transferred from its original registrar and it's probing that shift Update Australian Prime Minister Scott Morrison's WeChat account has been taken over by entities that have rebranded it "Australian Chinese new life" and used the account to offer advice on living in Australia for the nation's Chinese community.... [...]
