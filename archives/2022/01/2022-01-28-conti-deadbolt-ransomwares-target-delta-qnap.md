Title: Conti, DeadBolt Ransomwares Target Delta, QNAP
Date: 2022-01-28T14:15:47+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-01-28-conti-deadbolt-ransomwares-target-delta-qnap

[Source](https://threatpost.com/conti-deadbolt-delta-qnap-ransomware/178083/){:target="_blank" rel="noopener"}

> QNAP had to push out an unexpected (and not entirely welcome) NAS device update, and Delta Electronics' network has been crippled. [...]
