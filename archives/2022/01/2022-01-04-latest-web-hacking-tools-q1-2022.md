Title: Latest web hacking tools – Q1 2022
Date: 2022-01-04T14:53:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-04-latest-web-hacking-tools-q1-2022

[Source](https://portswigger.net/daily-swig/latest-web-hacking-tools-q1-2022){:target="_blank" rel="noopener"}

> We take a look at the latest additions to security researchers’ armory [...]
