Title: Zerodium Spikes Payout for Zero-Click Outlook Zero-Days
Date: 2022-01-28T16:54:06+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Bug Bounty;Hacks;Malware;Vulnerabilities;Web Security
Slug: 2022-01-28-zerodium-spikes-payout-for-zero-click-outlook-zero-days

[Source](https://threatpost.com/zerodium-payout-outlook-zero-days/178089/){:target="_blank" rel="noopener"}

> The sweetened deal came on the same day that Trustwave SpiderLabs published a new way to bypass Outlook security to deliver malicious links to victims. [...]
