Title: City of Grass Valley, California, suffers data breach – employee and citizen information exposed
Date: 2022-01-10T14:39:29+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-10-city-of-grass-valley-california-suffers-data-breach-employee-and-citizen-information-exposed

[Source](https://portswigger.net/daily-swig/city-of-grass-valley-california-suffers-data-breach-employee-and-citizen-information-exposed){:target="_blank" rel="noopener"}

> Social Security numbers and medical information has been accessed, city confirms [...]
