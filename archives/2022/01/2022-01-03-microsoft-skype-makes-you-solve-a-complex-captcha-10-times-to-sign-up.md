Title: Microsoft Skype makes you solve a complex captcha 10 times to sign up
Date: 2022-01-03T14:57:48-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-01-03-microsoft-skype-makes-you-solve-a-complex-captcha-10-times-to-sign-up

[Source](https://www.bleepingcomputer.com/news/security/microsoft-skype-makes-you-solve-a-complex-captcha-10-times-to-sign-up/){:target="_blank" rel="noopener"}

> New Skype users report frustration after being presented with a captcha that requires them to solve a complex puzzle ten times before signing up for the service. [...]
