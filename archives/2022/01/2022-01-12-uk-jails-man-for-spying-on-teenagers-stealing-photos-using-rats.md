Title: UK jails man for spying on teenagers, stealing photos using RATs
Date: 2022-01-12T13:38:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-12-uk-jails-man-for-spying-on-teenagers-stealing-photos-using-rats

[Source](https://www.bleepingcomputer.com/news/security/uk-jails-man-for-spying-on-teenagers-stealing-photos-using-rats/){:target="_blank" rel="noopener"}

> A Nottingham man was imprisoned this week for more than two years after hacking the computers and phones of dozens of victims, some of them underage, and spying on them using remote access trojans (RATs). [...]
