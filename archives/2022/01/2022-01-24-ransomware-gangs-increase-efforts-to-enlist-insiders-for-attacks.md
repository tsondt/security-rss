Title: Ransomware gangs increase efforts to enlist insiders for attacks
Date: 2022-01-24T11:40:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-24-ransomware-gangs-increase-efforts-to-enlist-insiders-for-attacks

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-increase-efforts-to-enlist-insiders-for-attacks/){:target="_blank" rel="noopener"}

> A recent survey of 100 large (over 5,000 employees) North American IT firms shows that ransomware actors are making greater effort to recruit insiders in targeted firms to aid in attacks. [...]
