Title: BitLocker encryption: Clear text key storage prompts security debate online
Date: 2022-01-21T11:56:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-21-bitlocker-encryption-clear-text-key-storage-prompts-security-debate-online

[Source](https://portswigger.net/daily-swig/bitlocker-encryption-clear-text-key-storage-prompts-security-debate-online){:target="_blank" rel="noopener"}

> Many are questioning why keys are saved in the clear ahead of sign-in [...]
