Title: Targeted ransomware takes aim at QNAP NAS drives, warns vendor: Get your updates done pronto
Date: 2022-01-27T16:19:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-27-targeted-ransomware-takes-aim-at-qnap-nas-drives-warns-vendor-get-your-updates-done-pronto

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/27/qnas_ransomware_deadbolt_nas_targeting/){:target="_blank" rel="noopener"}

> Nasty demands hefty Bitcoin ransom QNAP has urged NAS users to act "immediately" to install its latest updates and enable security protections after warning that product-specific ransomware called Deadbolt is targeting users' boxen.... [...]
