Title: Android users can now disable 2G to block Stingray attacks
Date: 2022-01-13T16:56:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2022-01-13-android-users-can-now-disable-2g-to-block-stingray-attacks

[Source](https://www.bleepingcomputer.com/news/security/android-users-can-now-disable-2g-to-block-stingray-attacks/){:target="_blank" rel="noopener"}

> Google has finally rolled out an option on Android allowing users to disable 2G connections, which come with a host of privacy and security problems exploited by cell-site simulators. [...]
