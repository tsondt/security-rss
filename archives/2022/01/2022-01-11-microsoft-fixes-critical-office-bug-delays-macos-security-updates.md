Title: Microsoft fixes critical Office bug, delays macOS security updates
Date: 2022-01-11T14:33:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-01-11-microsoft-fixes-critical-office-bug-delays-macos-security-updates

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-critical-office-bug-delays-macos-security-updates/){:target="_blank" rel="noopener"}

> During this year's first Patch Tuesday, Microsoft has addressed a critical severity Office vulnerability that can let attackers execute malicious code remotely on vulnerable systems. [...]
