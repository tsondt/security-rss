Title: Linux version of LockBit ransomware targets VMware ESXi servers
Date: 2022-01-26T18:40:10-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-26-linux-version-of-lockbit-ransomware-targets-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-lockbit-ransomware-targets-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> LockBit is the latest ransomware gang whose Linux encryptor has been discovered to be focusing on the encryption of VMware ESXi virtual machines. [...]
