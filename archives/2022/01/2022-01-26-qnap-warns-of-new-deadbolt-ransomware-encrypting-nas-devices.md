Title: QNAP warns of new DeadBolt ransomware encrypting NAS devices
Date: 2022-01-26T04:34:33-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-26-qnap-warns-of-new-deadbolt-ransomware-encrypting-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-of-new-deadbolt-ransomware-encrypting-nas-devices/){:target="_blank" rel="noopener"}

> QNAP is warning customers again to secure their Internet-exposed Network Attached Storage (NAS) devices to defend against ongoing and widespread attacks targeting their data with the new DeadBolt ransomware strain. [...]
