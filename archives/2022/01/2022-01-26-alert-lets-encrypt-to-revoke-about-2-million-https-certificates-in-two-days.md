Title: Alert: Let's Encrypt to revoke about 2 million HTTPS certificates in two days
Date: 2022-01-26T21:26:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-26-alert-lets-encrypt-to-revoke-about-2-million-https-certificates-in-two-days

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/26/lets_encrypt_certificates/){:target="_blank" rel="noopener"}

> Relatively small number of certs issued using a verification method that doesn't comply with policy Let's Encrypt, a non-profit organization that helps people obtain free SSL/TLS certificates for websites, plans to revoke a non-trivial number of its certs on Friday because they were improperly issued.... [...]
