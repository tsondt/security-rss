Title: North Korean APTs Stole ~$400M in Crypto in 2021
Date: 2022-01-13T21:03:09+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Web Security
Slug: 2022-01-13-north-korean-apts-stole-400m-in-crypto-in-2021

[Source](https://threatpost.com/north-korea-apts-stole-400m-cryptocurrency/177638/){:target="_blank" rel="noopener"}

> Meanwhile, EtherumMax got sued over an alleged pump-and-dump scam after using celebs like Floyd Mayweather Jr. & Kim Kardashian to promote EMAX Tokens. [...]
