Title: Swiss army bans all chat apps but locally-developed Threema
Date: 2022-01-06T11:02:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2022-01-06-swiss-army-bans-all-chat-apps-but-locally-developed-threema

[Source](https://www.bleepingcomputer.com/news/security/swiss-army-bans-all-chat-apps-but-locally-developed-threema/){:target="_blank" rel="noopener"}

> The Swiss army has banned foreign instant-messaging apps such as Signal, Telegram, and WhatsApp and requires army members to use the locally-developed Threema messaging app instead. [...]
