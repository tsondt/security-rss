Title: eNom data center migration mistakenly knocks sites offline
Date: 2022-01-16T14:42:46-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-01-16-enom-data-center-migration-mistakenly-knocks-sites-offline

[Source](https://www.bleepingcomputer.com/news/security/enom-data-center-migration-mistakenly-knocks-sites-offline/){:target="_blank" rel="noopener"}

> A data center migration from eNom web hosting provider caused unexpected domain resolution problems that are expected to last for a few hours. [...]
