Title: Cybersecurity conferences 2022: A rundown of online, in person, and ‘hybrid’ events
Date: 2022-01-12T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-12-cybersecurity-conferences-2022-a-rundown-of-online-in-person-and-hybrid-events

[Source](https://portswigger.net/daily-swig/cybersecurity-conferences-2022-a-rundown-of-online-in-person-and-hybrid-events){:target="_blank" rel="noopener"}

> With many events choosing to retain virtual elements forced on them by the pandemic, there’s now an abundance of online content to choose from [...]
