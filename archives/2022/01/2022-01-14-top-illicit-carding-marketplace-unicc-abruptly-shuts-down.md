Title: Top Illicit Carding Marketplace UniCC Abruptly Shuts Down
Date: 2022-01-14T17:31:04+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Privacy;Web Security
Slug: 2022-01-14-top-illicit-carding-marketplace-unicc-abruptly-shuts-down

[Source](https://threatpost.com/carding-marketplace-unicc-shuts-down/177688/){:target="_blank" rel="noopener"}

> UniCC controlled 30 percent of the stolen payment-card data market; leaving analysts eyeing what’s next. [...]
