Title: UK Government to Launch PR Campaign Undermining End-to-End Encryption
Date: 2022-01-18T12:05:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;child pornography;children;crypto wars;cybersecurity;encryption;marketing;privacy;propaganda
Slug: 2022-01-18-uk-government-to-launch-pr-campaign-undermining-end-to-end-encryption

[Source](https://www.schneier.com/blog/archives/2022/01/uk-government-to-launch-pr-campaign-undermining-end-to-end-encryption.html){:target="_blank" rel="noopener"}

> Rolling Stone is reporting that the UK government has hired the M&C Saatchi advertising agency to launch an anti-encryption advertising campaign. Presumably they’ll lean heavily on the “think of the children!” rhetoric we’re seeing in this current wave of the crypto wars. The technical eavesdropping mechanisms have shifted to client-side scanning, which won’t actually help — but since that’s not really the point, it’s not argued on its merits. [...]
