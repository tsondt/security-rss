Title: Finnish diplomats’ phones infected with NSO Group Pegasus spyware
Date: 2022-01-28T08:26:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-28-finnish-diplomats-phones-infected-with-nso-group-pegasus-spyware

[Source](https://www.bleepingcomputer.com/news/security/finnish-diplomats-phones-infected-with-nso-group-pegasus-spyware/){:target="_blank" rel="noopener"}

> Finland's Ministry for Foreign Affairs says devices of Finnish diplomats have been hacked and infected with NSO Group's Pegasus spyware in a cyber-espionage campaign. [...]
