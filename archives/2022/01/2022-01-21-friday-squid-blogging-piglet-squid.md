Title: Friday Squid Blogging: Piglet Squid
Date: 2022-01-21T22:11:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-01-21-friday-squid-blogging-piglet-squid

[Source](https://www.schneier.com/blog/archives/2022/01/friday-squid-blogging-piglet-squid.html){:target="_blank" rel="noopener"}

> Nice article on the piglet squid. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
