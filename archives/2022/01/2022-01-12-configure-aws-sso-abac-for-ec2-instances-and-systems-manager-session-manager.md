Title: Configure AWS SSO ABAC for EC2 instances and Systems Manager Session Manager
Date: 2022-01-12T20:24:51+00:00
Author: Rodrigo Ferroni
Category: AWS Security
Tags: Advanced (300);AWS Single Sign-On (SSO);Best Practices;Technical How-to;ABAC;Attributes;AWS Single Sign-On;AWS SSO;SAML;Tags
Slug: 2022-01-12-configure-aws-sso-abac-for-ec2-instances-and-systems-manager-session-manager

[Source](https://aws.amazon.com/blogs/security/configure-aws-sso-abac-for-ec2-instances-and-systems-manager-session-manager/){:target="_blank" rel="noopener"}

> In this blog post, I show you how to configure AWS Single Sign-On to define attribute-based access control (ABAC) permissions to manage Amazon Elastic Compute Cloud (Amazon EC2) instances and AWS Systems Manager Session Manager for federated users. This combination allows you to control access to specific Amazon EC2 instances based on users’ attributes. I show [...]
