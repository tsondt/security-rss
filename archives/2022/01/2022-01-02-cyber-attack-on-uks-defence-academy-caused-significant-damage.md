Title: Cyber-attack on UK’s Defence Academy caused ‘significant’ damage
Date: 2022-01-02T23:42:09+00:00
Author: PA Media
Category: The Guardian
Tags: Ministry of Defence;Hacking;Data and computer security;Cybercrime;Military;UK news
Slug: 2022-01-02-cyber-attack-on-uks-defence-academy-caused-significant-damage

[Source](https://www.theguardian.com/uk-news/2022/jan/02/cyber-attack-on-uks-defence-academy-caused-significant-damage){:target="_blank" rel="noopener"}

> Former senior officer says unsolved hack of MoD training school systems did not succeed but still had costs A cyber-attack on the UK’s Defence Academy caused “significant” damage, a retired high-ranking officer has revealed. Air Marshal Edward Stringer, who left the armed forces in August, told Sky News the attack, which was discovered in March 2021, meant the Defence Academy was forced to rebuild its network. Continue reading... [...]
