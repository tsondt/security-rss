Title: MoleRats APT Launches Spy Campaign on Bankers, Politicians, Journalists
Date: 2022-01-24T21:54:58+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware;Web Security
Slug: 2022-01-24-molerats-apt-launches-spy-campaign-on-bankers-politicians-journalists

[Source](https://threatpost.com/molerats-apt-spy-bankers-politicians-journalists/177907/){:target="_blank" rel="noopener"}

> State-sponsored cyberattackers are using Google Drive, Dropbox and other legitimate services to drop spyware on Middle-Eastern targets and exfiltrate data. [...]
