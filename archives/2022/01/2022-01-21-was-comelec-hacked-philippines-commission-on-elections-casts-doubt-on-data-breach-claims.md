Title: Was COMELEC hacked? Philippines Commission on Elections casts doubt on data breach claims
Date: 2022-01-21T14:51:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-21-was-comelec-hacked-philippines-commission-on-elections-casts-doubt-on-data-breach-claims

[Source](https://portswigger.net/daily-swig/was-comelec-hacked-philippines-commission-on-elections-casts-doubt-on-data-breach-claims){:target="_blank" rel="noopener"}

> Local newspaper alleges that usernames and PINs of vote-counting machines were stolen [...]
