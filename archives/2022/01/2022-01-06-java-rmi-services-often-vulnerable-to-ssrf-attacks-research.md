Title: Java RMI services often vulnerable to SSRF attacks – research
Date: 2022-01-06T15:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-06-java-rmi-services-often-vulnerable-to-ssrf-attacks-research

[Source](https://portswigger.net/daily-swig/java-rmi-services-often-vulnerable-to-ssrf-attacks-research){:target="_blank" rel="noopener"}

> Trust boundaries breached by security shortcomings [...]
