Title: Prosecutors file additional charges against former Uber security chief over 2016 data breach ‘cover up’
Date: 2022-01-05T17:10:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-05-prosecutors-file-additional-charges-against-former-uber-security-chief-over-2016-data-breach-cover-up

[Source](https://portswigger.net/daily-swig/prosecutors-file-additional-charges-against-former-uber-security-chief-over-2016-data-breach-cover-up){:target="_blank" rel="noopener"}

> Alleged misuse of bug bounty and failure to disclose breach leads to criminal charges [...]
