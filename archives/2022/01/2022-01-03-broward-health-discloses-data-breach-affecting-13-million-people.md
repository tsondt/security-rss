Title: Broward Health discloses data breach affecting 1.3 million people
Date: 2022-01-03T11:50:42-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2022-01-03-broward-health-discloses-data-breach-affecting-13-million-people

[Source](https://www.bleepingcomputer.com/news/security/broward-health-discloses-data-breach-affecting-13-million-people/){:target="_blank" rel="noopener"}

> Florida's Broward Health healthcare system has disclosed a large-scale data breach incident impacting 1,357,879 individuals. [...]
