Title: Arm rages against the insecure chip machine with new Morello architecture
Date: 2022-01-21T18:21:04+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2022-01-21-arm-rages-against-the-insecure-chip-machine-with-new-morello-architecture

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/21/arm_morello_testing/){:target="_blank" rel="noopener"}

> Prototypes now available for testing Arm has made available for testing prototypes of its Morello architecture, aimed at bringing features into the design of CPUs that provide greater robustness and make them resistant to certain attack vectors. If it performs as expected, it will likely become a fundamental part of future processor designs.... [...]
