Title: Pervasive Apple Safari Bug Exposes Web-Browsing Data, Google IDs
Date: 2022-01-20T16:50:30+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Privacy;Vulnerabilities;Web Security
Slug: 2022-01-20-pervasive-apple-safari-bug-exposes-web-browsing-data-google-ids

[Source](https://threatpost.com/apple-safari-bug-browsing-data-google-ids/177809/){:target="_blank" rel="noopener"}

> The information-disclosure issue, affecting Macs, iPhones and iPads, allows a snooping website to find out information about other tabs a user might have open. [...]
