Title: Adobe Cloud Abused to Steal Office 365, Gmail Credentials
Date: 2022-01-13T14:00:54+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware;Web Security
Slug: 2022-01-13-adobe-cloud-abused-to-steal-office-365-gmail-credentials

[Source](https://threatpost.com/adobe-cloud-steal-office-365-gmail-credentials/177625/){:target="_blank" rel="noopener"}

> Threat actors are creating accounts within the Adobe Cloud suite and sending images and PDFs that appear legitimate to target Office 365 and Gmail users, researchers from Avanan discovered. [...]
