Title: GitHub Actions flaw that allowed code to be approved without review is addressed with new feature rollout
Date: 2022-01-19T16:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-19-github-actions-flaw-that-allowed-code-to-be-approved-without-review-is-addressed-with-new-feature-rollout

[Source](https://portswigger.net/daily-swig/github-actions-flaw-that-allowed-code-to-be-approved-without-review-is-addressed-with-new-feature-rollout){:target="_blank" rel="noopener"}

> Uncheck risky setting option offered [...]
