Title: Raising the bar in Security Operations: Google Acquires Siemplify
Date: 2022-01-04T14:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-01-04-raising-the-bar-in-security-operations-google-acquires-siemplify

[Source](https://cloud.google.com/blog/products/identity-security/raising-the-bar-in-security-operations/){:target="_blank" rel="noopener"}

> At Google Cloud, we are committed to advancing invisible security and democratizing security operations for every organization. Today, we’re proud to share the next step in this journey with the acquisition of Siemplify, a leading security orchestration, automation and response (SOAR) provider. Siemplify shares our vision in this space, and will join Google Cloud’s security team to help companies better manage their threat response. In a time when cyberattacks are rapidly growing in both frequency and sophistication, there’s never been a better time to bring these two companies together. We both share the belief that security analysts need to be [...]
