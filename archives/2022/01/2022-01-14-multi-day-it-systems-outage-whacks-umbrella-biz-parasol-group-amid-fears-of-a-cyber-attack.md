Title: Multi-day IT systems outage whacks umbrella biz Parasol Group amid fears of a cyber attack
Date: 2022-01-14T16:30:05+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-01-14-multi-day-it-systems-outage-whacks-umbrella-biz-parasol-group-amid-fears-of-a-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/14/multiday_it_systems_outage_whacks/){:target="_blank" rel="noopener"}

> Contractors say they haven't been paid, and are in the dark too Contractors employed via umbrella company Parasol Group are increasingly nervous about a multi-day outage of some IT systems used to process payroll, with several suspecting a security attack as the root cause.... [...]
