Title: Are Fake COVID Testing Sites Harvesting Data?
Date: 2022-01-19T12:10:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;COVID-19;data collection;fraud;healthcare
Slug: 2022-01-19-are-fake-covid-testing-sites-harvesting-data

[Source](https://www.schneier.com/blog/archives/2022/01/are-fake-covid-testing-sites-harvesting-data.html){:target="_blank" rel="noopener"}

> Over the past few weeks, I’ve seen a bunch of writing about what seems to be fake COVID-19 testing sites. They take your name and info, and do a nose swab, but you never get test results. Speculation centered around data harvesting, but that didn’t make sense because it was far too labor intensive for that and — sorry to break it to you — your data isn’t worth all that much. It seems to be multilevel marketing fraud instead: The Center for COVID Control is a management company to Doctors Clinical Laboratory. It provides tests and testing supplies, software, [...]
