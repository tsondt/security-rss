Title: Red Cross Begs Attackers Not to Leak Stolen Data for 515K People
Date: 2022-01-20T15:49:56+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Hacks;Web Security
Slug: 2022-01-20-red-cross-begs-attackers-not-to-leak-stolen-data-for-515k-people

[Source](https://threatpost.com/red-cross-begs-attackers-not-to-leak-515k-peoples-stolen-data/177799/){:target="_blank" rel="noopener"}

> A cyberattack forced the Red Cross to shut down IT systems running the Restoring Family Links system, which reunites families fractured by war, disaster or migration. [...]
