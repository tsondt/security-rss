Title: FTC warns companies to secure consumer data from Log4J attacks
Date: 2022-01-04T15:20:25-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-04-ftc-warns-companies-to-secure-consumer-data-from-log4j-attacks

[Source](https://www.bleepingcomputer.com/news/security/ftc-warns-companies-to-secure-consumer-data-from-log4j-attacks/){:target="_blank" rel="noopener"}

> The US Federal Trade Commission (FTC) has warned today that it will go after any US company that fails to protect its customers' data against ongoing Log4J attacks. [...]
