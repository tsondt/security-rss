Title: Chaes banking trojan hijacks Chrome with malicious extensions
Date: 2022-01-26T11:39:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-26-chaes-banking-trojan-hijacks-chrome-with-malicious-extensions

[Source](https://www.bleepingcomputer.com/news/security/chaes-banking-trojan-hijacks-chrome-with-malicious-extensions/){:target="_blank" rel="noopener"}

> A large-scale campaign involving over 800 compromised WordPress websites is spreading banking trojans that target the credentials of Brazilian e-banking users. [...]
