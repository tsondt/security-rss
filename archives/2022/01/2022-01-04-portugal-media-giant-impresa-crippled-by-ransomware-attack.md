Title: Portugal Media Giant Impresa Crippled by Ransomware Attack
Date: 2022-01-04T13:16:55+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware
Slug: 2022-01-04-portugal-media-giant-impresa-crippled-by-ransomware-attack

[Source](https://threatpost.com/portuguese-media-giant-impresa-ransomware/177323/){:target="_blank" rel="noopener"}

> The websites of the company and the Expresso newspaper, as well as all of its SIC TV channels remained offline Tuesday after the New Year’s weekend attack. [...]
