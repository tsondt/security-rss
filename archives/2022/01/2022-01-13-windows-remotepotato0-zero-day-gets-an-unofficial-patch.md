Title: Windows 'RemotePotato0' zero-day gets an unofficial patch
Date: 2022-01-13T12:31:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2022-01-13-windows-remotepotato0-zero-day-gets-an-unofficial-patch

[Source](https://www.bleepingcomputer.com/news/security/windows-remotepotato0-zero-day-gets-an-unofficial-patch/){:target="_blank" rel="noopener"}

> A privilege escalation vulnerability impacting all Windows versions that can let threat actors gain domain admin privileges through an NTLM relay attack has received unofficial patches after Microsoft tagged it as "won't fix." [...]
