Title: Singapore gives banks two-week deadline to fix SMS security
Date: 2022-01-20T06:01:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-01-20-singapore-gives-banks-two-week-deadline-to-fix-sms-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/20/singapores_monetary_authority_requires_banks/){:target="_blank" rel="noopener"}

> Edict follows widespread bank phishing scam claiming well over $6.3 million A widespread phishing operation targeting Southeast Asia's second-largest bank – Oversea-Chinese Banking Corporation (OCBC) – has prompted the Monetary Authority of Singapore (MAS) to introduce regulations for internet banking that include use of an SMS Sender ID registry.... [...]
