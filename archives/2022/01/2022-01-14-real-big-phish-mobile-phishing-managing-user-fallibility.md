Title: Real Big Phish: Mobile Phishing & Managing User Fallibility
Date: 2022-01-14T16:43:43+00:00
Author: Daniel Spicer
Category: Threatpost
Tags: InfoSec Insider;News;Vulnerabilities;Web Security
Slug: 2022-01-14-real-big-phish-mobile-phishing-managing-user-fallibility

[Source](https://threatpost.com/mobile-phishing-zero-trust-security/177594/){:target="_blank" rel="noopener"}

> Phishing is more successful than ever. Daniel Spicer, CSO of Ivanti, discusses emerging trends in phishing, and using zero-trust security to patch the human vulnerabilities underpinning the spike. [...]
