Title: Infosec chap: I found a way to hijack your web accounts, turn on your webcam from Safari – and Apple gave me $100k
Date: 2022-01-26T08:32:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-26-infosec-chap-i-found-a-way-to-hijack-your-web-accounts-turn-on-your-webcam-from-safari-and-apple-gave-me-100k

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/26/apple_filesharing_exploit/){:target="_blank" rel="noopener"}

> Now you see a harmless PNG. Now it's a malicious payload. Look into my eyes A security bod scored a $100,500 bug bounty from Apple after discovering a vulnerability in Safari on macOS that could have been exploited by a malicious website to potentially access victims' logged-in online accounts – and even their webcams.... [...]
