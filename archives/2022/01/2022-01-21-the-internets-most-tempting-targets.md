Title: The Internet’s Most Tempting Targets
Date: 2022-01-21T21:03:23+00:00
Author: David “moose” Wolpoff
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;InfoSec Insider;IoT;Mobile Security;Vulnerabilities;Web Security
Slug: 2022-01-21-the-internets-most-tempting-targets

[Source](https://threatpost.com/internet-most-tempting-targets/177869/){:target="_blank" rel="noopener"}

> What attracts the attackers? David "moose" Wolpoff, CTO at Randori, discusses how to evaluate your infrastructure for juicy targets. [...]
