Title: More Russian Cyber Operations against Ukraine
Date: 2022-01-05T12:12:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberwar;Russia;Ukraine
Slug: 2022-01-05-more-russian-cyber-operations-against-ukraine

[Source](https://www.schneier.com/blog/archives/2022/01/more-russian-cyber-operations-against-ukraine.html){:target="_blank" rel="noopener"}

> Both Russia and Ukraine are preparing for military operations in cyberspace. [...]
