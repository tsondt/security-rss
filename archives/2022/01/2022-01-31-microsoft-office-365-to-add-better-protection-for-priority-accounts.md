Title: Microsoft Office 365 to add better protection for priority accounts
Date: 2022-01-31T12:17:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-01-31-microsoft-office-365-to-add-better-protection-for-priority-accounts

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-office-365-to-add-better-protection-for-priority-accounts/){:target="_blank" rel="noopener"}

> Microsoft is working on updating Microsoft Defender for Office 365 with differentiated protection for enterprise accounts tagged as critical for an organization (i.e., accounts of high-profile employees including executive-level managers, the ones most often targeted by attackers). [...]
