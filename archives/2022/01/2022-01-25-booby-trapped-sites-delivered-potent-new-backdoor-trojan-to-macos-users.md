Title: Booby-trapped sites delivered potent new backdoor trojan to macOS users
Date: 2022-01-25T21:31:00+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;backdoor;exploits;MacOS;malware
Slug: 2022-01-25-booby-trapped-sites-delivered-potent-new-backdoor-trojan-to-macos-users

[Source](https://arstechnica.com/?p=1828331){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Researchers have uncovered advanced, never-before-seen macOS malware that was installed using exploits that were almost impossible for most users to detect or stop once the users landed on a malicious website. The malware was a full-featured backdoor that was written from scratch, an indication that the developers behind it have significant resources and expertise. DazzleSpy, as researchers from security firm Eset have named it, provides an array of advanced capabilities that give the attackers the ability to fully monitor and control infected Macs. Features include: victim device fingerprinting screen capture file download/upload execute terminal commands [...]
