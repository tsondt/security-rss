Title: Four million outdated Log4j downloads were served from Apache Maven Central alone despite vuln publicity blitz
Date: 2022-01-11T08:27:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-11-four-million-outdated-log4j-downloads-were-served-from-apache-maven-central-alone-despite-vuln-publicity-blitz

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/11/outdated_log4j_downloads/){:target="_blank" rel="noopener"}

> It's not as though folks haven't been warned about this There have been millions of downloads of outdated, vulnerable Log4j versions despite the emergence of a serious security hole in December 2021, according to figures compiled by the firm that runs Apache Maven's Central Repository.... [...]
