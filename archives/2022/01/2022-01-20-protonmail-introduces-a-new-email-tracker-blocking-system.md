Title: ProtonMail introduces a new email tracker blocking system
Date: 2022-01-20T11:25:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-20-protonmail-introduces-a-new-email-tracker-blocking-system

[Source](https://www.bleepingcomputer.com/news/security/protonmail-introduces-a-new-email-tracker-blocking-system/){:target="_blank" rel="noopener"}

> ProtonMail has introduced an enhanced email tracking protection system for its web-based email solution that prevents senders from being tracked by recipients who open their messages. [...]
