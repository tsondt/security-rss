Title: ‘Be Afraid:’ Massive Cyberattack Downs Ukrainian Gov’t Sites
Date: 2022-01-14T16:06:49+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Government;Hacks;Web Security
Slug: 2022-01-14-be-afraid-massive-cyberattack-downs-ukrainian-govt-sites

[Source](https://threatpost.com/be-afraid-massive-cyberattack-downs-ukrainian-govt-sites/177659/){:target="_blank" rel="noopener"}

> As Moscow moves troops and threatens military action, about 70 Ukrainian government sites were hit. “Be afraid” was scrawled on the Foreign Ministry site. [...]
