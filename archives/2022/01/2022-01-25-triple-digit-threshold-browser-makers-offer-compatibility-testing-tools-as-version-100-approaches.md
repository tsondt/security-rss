Title: Triple-digit threshold: Browser makers offer compatibility testing tools as version 100 approaches
Date: 2022-01-25T12:12:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-25-triple-digit-threshold-browser-makers-offer-compatibility-testing-tools-as-version-100-approaches

[Source](https://portswigger.net/daily-swig/triple-digit-threshold-browser-makers-offer-compatibility-testing-tools-as-version-100-approaches){:target="_blank" rel="noopener"}

> Google and Mozilla are acting to help ensure minimal disruption for site owners [...]
