Title: ‘White Rabbit’ Ransomware May Be FIN8’s Latest Tool
Date: 2022-01-18T17:23:12+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-01-18-white-rabbit-ransomware-may-be-fin8s-latest-tool

[Source](https://threatpost.com/white-rabbit-ransomware-fin8/177703/){:target="_blank" rel="noopener"}

> It's a double-extortion play that uses the command-line password ‘KissMe’ to hide its nasty acts and adorns its ransom note with cutesy ASCII bunny art. [...]
