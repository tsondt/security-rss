Title: ‘Wormable’ Flaw Leads January 2022 Patch Tuesday
Date: 2022-01-11T22:18:55+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Time to Patch;CVE-2021-22947;CVE-2021-36976;CVE-2022-21846;CVE-2022-21907;Dustin Childs;Exchange Server;HTTP Protocol Stack;Microsoft Patch Tuesday January 2022;national security agency;Rapid7;Satnam Narang;Tenable;trend micro;Zero Day Initiative
Slug: 2022-01-11-wormable-flaw-leads-january-2022-patch-tuesday

[Source](https://krebsonsecurity.com/2022/01/wormable-flaw-leads-january-2022-patch-tuesday/){:target="_blank" rel="noopener"}

> Microsoft today released updates to plug nearly 120 security holes in Windows and supported software. Six of the vulnerabilities were publicly detailed already, potentially giving attackers a head start in figuring out how to exploit them in unpatched systems. More concerning, Microsoft warns that one of the flaws fixed this month is “wormable,” meaning no human interaction would be required for an attack to spread from one vulnerable Windows box to another. Nine of the vulnerabilities fixed in this month’s Patch Tuesday received Microsoft’s “critical” rating, meaning malware or miscreants can exploit them to gain remote access to vulnerable Windows [...]
