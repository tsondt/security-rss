Title: Secure boot for UK electric car chargers isn't mandatory until 2023 – but why the delay?
Date: 2022-01-11T10:17:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-11-secure-boot-for-uk-electric-car-chargers-isnt-mandatory-until-2023-but-why-the-delay

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/11/electric_car_charging_security_uk/){:target="_blank" rel="noopener"}

> Good: New requirements in new law. Bad: Grace period Electric car chargers will have to include secure boot and automatic network disconnection if unsigned software runs on the smart devices – but only from 2023, the British government has said.... [...]
