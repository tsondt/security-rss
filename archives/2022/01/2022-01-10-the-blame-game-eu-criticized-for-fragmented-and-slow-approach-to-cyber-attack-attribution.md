Title: <span>The blame game: EU criticized for ‘fragmented and slow’ approach to cyber-attack attribution</span>
Date: 2022-01-10T12:44:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-10-the-blame-game-eu-criticized-for-fragmented-and-slow-approach-to-cyber-attack-attribution

[Source](https://portswigger.net/daily-swig/the-blame-game-eu-criticized-for-fragmented-and-slow-approach-to-cyber-attack-attribution){:target="_blank" rel="noopener"}

> Cyber sanctions can send a powerful message – why aren’t they being used more widely? [...]
