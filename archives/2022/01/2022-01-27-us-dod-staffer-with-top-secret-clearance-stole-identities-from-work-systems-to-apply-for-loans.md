Title: US DoD staffer with top-secret clearance stole identities from work systems to apply for loans
Date: 2022-01-27T23:41:07+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: 2022-01-27-us-dod-staffer-with-top-secret-clearance-stole-identities-from-work-systems-to-apply-for-loans

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/27/dod_sharepoint_apple_white_house/){:target="_blank" rel="noopener"}

> Plus: Apple patches exploited-in-the-wild bug, White House zero-trust order, and more In brief A US Department of Defense staffer with top-secret clearance stole the identities of dozens of people from a work SharePoint system to apply for loans totaling nearly a quarter of a million dollars.... [...]
