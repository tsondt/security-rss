Title: Tracking Secret German Organizations with Apple AirTags
Date: 2022-01-28T12:13:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;geolocation;intelligence
Slug: 2022-01-28-tracking-secret-german-organizations-with-apple-airtags

[Source](https://www.schneier.com/blog/archives/2022/01/tracking-secret-german-organizations-with-apple-airtags.html){:target="_blank" rel="noopener"}

> A German activist is trying to track down a secret government intelligence agency. One of her research techniques is to mail Apple AirTags to see where they actually end up: Wittmann says that everyone she spoke to denied being part of this intelligence agency. But what she describes as a “good indicator,” would be if she could prove that the postal address for this “federal authority” actually leads to the intelligence service’s apparent offices. “To understand where mail ends up,” she writes (in translation), “[you can do] a lot of manual research. Or you can simply send a small device [...]
