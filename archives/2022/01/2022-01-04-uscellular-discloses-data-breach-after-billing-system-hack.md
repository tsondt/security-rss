Title: UScellular discloses data breach after billing system hack
Date: 2022-01-04T12:07:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-04-uscellular-discloses-data-breach-after-billing-system-hack

[Source](https://www.bleepingcomputer.com/news/security/uscellular-discloses-data-breach-after-billing-system-hack/){:target="_blank" rel="noopener"}

> UScellular, self-described as the fourth-largest wireless carrier in the US, has disclosed a data breach after the company's billing system was hacked in December 2021. [...]
