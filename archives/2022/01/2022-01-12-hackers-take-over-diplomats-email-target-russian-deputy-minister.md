Title: Hackers take over diplomat's email, target Russian deputy minister
Date: 2022-01-12T03:35:06-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2022-01-12-hackers-take-over-diplomats-email-target-russian-deputy-minister

[Source](https://www.bleepingcomputer.com/news/security/hackers-take-over-diplomats-email-target-russian-deputy-minister/){:target="_blank" rel="noopener"}

> Hackers believed to work for the North Korean government have compromised the email account of a staff member of Russia's Ministry of Foreign Affairs (MID) and deployed spear-phishing attacks against the country's diplomats in other regions. [...]
