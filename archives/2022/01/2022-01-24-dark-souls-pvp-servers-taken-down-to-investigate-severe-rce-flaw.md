Title: Dark Souls PvP servers taken down to investigate severe RCE flaw
Date: 2022-01-24T06:58:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2022-01-24-dark-souls-pvp-servers-taken-down-to-investigate-severe-rce-flaw

[Source](https://www.bleepingcomputer.com/news/security/dark-souls-pvp-servers-taken-down-to-investigate-severe-rce-flaw/){:target="_blank" rel="noopener"}

> Bandai Namco has deactivated the online PvP mode for the Dark Souls role-playing game, taking its servers offline to investigate reports about a severe security issue that may pose a risk to players. [...]
