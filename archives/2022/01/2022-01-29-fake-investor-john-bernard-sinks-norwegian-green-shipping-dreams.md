Title: Fake Investor John Bernard Sinks Norwegian Green Shipping Dreams
Date: 2022-01-29T18:05:52+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Freidig Shipping;Guy Devos;Harald Berglihn;Harald Vanvik;Inside Knowledge;John Bernard;John Clifton Davies;Nils-Odd Tønnevold;The Weeknd;Uber
Slug: 2022-01-29-fake-investor-john-bernard-sinks-norwegian-green-shipping-dreams

[Source](https://krebsonsecurity.com/2022/01/fake-investor-john-bernard-sinks-norwegian-green-shipping-dreams/){:target="_blank" rel="noopener"}

> Several articles here have delved into the history of John Bernard, the pseudonym used by a fake billionaire technology investor who tricked dozens of startups into giving him tens of millions of dollars. Bernard’s latest victim — a Norwegian company hoping to build a fleet of environmentally friendly shipping vessels — is now embroiled in a lawsuit over a deal gone bad, in which Bernard falsely claimed to have secured $100 million from six other wealthy investors, including the founder of Uber and the artist Abel Makkonen Tesfaye, better known as The Weeknd. John Bernard is a pseudonym used by [...]
