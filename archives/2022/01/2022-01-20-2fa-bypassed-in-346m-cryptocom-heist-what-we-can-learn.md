Title: 2FA Bypassed in $34.6M Crypto.com Heist: What We Can Learn
Date: 2022-01-20T23:14:23+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Breach;Web Security
Slug: 2022-01-20-2fa-bypassed-in-346m-cryptocom-heist-what-we-can-learn

[Source](https://threatpost.com/2fa-bypassed-crypto-com-heist/177846/){:target="_blank" rel="noopener"}

> In a display of 2FA's fallibility, unauthorized transactions approved without users' authentication bled 483 accounts of funds. [...]
