Title: Internet Society condemns UK's Online Safety Bill for demonising encryption using 'think of the children' tactic
Date: 2022-01-28T12:56:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: 2022-01-28-internet-society-condemns-uks-online-safety-bill-for-demonising-encryption-using-think-of-the-children-tactic

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/28/internet_society_calls_out_uk_encryption_war/){:target="_blank" rel="noopener"}

> Plus: Cops' surveillance is used against drug gangs and not child abusers, says Tutanota Britain's controversial Online Safety Bill will leave Britons more exposed to internet harms than ever before, the Internet Society has said, while data from other countries suggests surveillance mostly isn't used to target child abusers online, despite this being a key cited rationale of linked measures.... [...]
