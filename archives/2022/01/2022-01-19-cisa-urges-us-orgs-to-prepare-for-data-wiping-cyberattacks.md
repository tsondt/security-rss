Title: CISA urges US orgs to prepare for data-wiping cyberattacks
Date: 2022-01-19T13:33:32-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-19-cisa-urges-us-orgs-to-prepare-for-data-wiping-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-urges-us-orgs-to-prepare-for-data-wiping-cyberattacks/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) urges U.S. organizations to strengthen their cybersecurity defenses against data-wiping attacks recently seen targeting Ukrainian government agencies and businesses. [...]
