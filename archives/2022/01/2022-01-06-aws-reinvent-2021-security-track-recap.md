Title: AWS re:Invent 2021 security track recap
Date: 2022-01-06T16:49:14+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Security, Identity, & Compliance;AWS re:Invent 2021;Security education
Slug: 2022-01-06-aws-reinvent-2021-security-track-recap

[Source](https://aws.amazon.com/blogs/security/aws-reinvent-2021-security-track-recap/){:target="_blank" rel="noopener"}

> Another AWS re:Invent is in the books! We were so pleased to be able to host live in Las Vegas again this year. And we were also thrilled to be able to host a large virtual audience. If you weren’t able to participate live, you can now view some of the security sessions by visiting [...]
