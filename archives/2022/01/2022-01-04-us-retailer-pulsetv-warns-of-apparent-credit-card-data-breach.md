Title: US retailer PulseTV warns of apparent credit card data breach
Date: 2022-01-04T16:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-04-us-retailer-pulsetv-warns-of-apparent-credit-card-data-breach

[Source](https://portswigger.net/daily-swig/us-retailer-pulsetv-warns-of-apparent-credit-card-data-breach){:target="_blank" rel="noopener"}

> Payment system updated amidst fears 200,000 records may have been exposed [...]
