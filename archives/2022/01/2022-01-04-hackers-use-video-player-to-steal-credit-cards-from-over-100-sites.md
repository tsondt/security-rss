Title: Hackers use video player to steal credit cards from over 100 sites
Date: 2022-01-04T12:52:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2022-01-04-hackers-use-video-player-to-steal-credit-cards-from-over-100-sites

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-video-player-to-steal-credit-cards-from-over-100-sites/){:target="_blank" rel="noopener"}

> Hackers used a cloud video hosting service to perform a supply chain attack on over one hundred real estate sites that injected malicious scripts to steal information inputted in website forms. [...]
