Title: KCodes NetUSB bug exposes millions of routers to RCE attacks
Date: 2022-01-11T07:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-11-kcodes-netusb-bug-exposes-millions-of-routers-to-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/kcodes-netusb-bug-exposes-millions-of-routers-to-rce-attacks/){:target="_blank" rel="noopener"}

> A high-severity remote code execution flaw tracked as CVE-2021-45388 has been discovered in the KCodes NetUSB kernel module, used by millions of router devices from various vendors. [...]
