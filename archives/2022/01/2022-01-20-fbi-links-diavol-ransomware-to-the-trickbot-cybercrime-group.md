Title: FBI links Diavol ransomware to the TrickBot cybercrime group
Date: 2022-01-20T13:37:25-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-20-fbi-links-diavol-ransomware-to-the-trickbot-cybercrime-group

[Source](https://www.bleepingcomputer.com/news/security/fbi-links-diavol-ransomware-to-the-trickbot-cybercrime-group/){:target="_blank" rel="noopener"}

> The FBI has formally linked the Diavol ransomware operation to the TrickBot Group, the malware developers behind the notorious TrickBot banking trojan. [...]
