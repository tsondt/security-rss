Title: <span>Chrome to bolster CSRF protections with CORS preflight checks on private network requests</span>
Date: 2022-01-18T15:35:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-18-chrome-to-bolster-csrf-protections-with-cors-preflight-checks-on-private-network-requests

[Source](https://portswigger.net/daily-swig/chrome-to-bolster-csrf-protections-with-cors-preflight-checks-on-private-network-requests){:target="_blank" rel="noopener"}

> Phased rollout begins from Chrome 98 with DevTools warnings of failed preflight requests [...]
