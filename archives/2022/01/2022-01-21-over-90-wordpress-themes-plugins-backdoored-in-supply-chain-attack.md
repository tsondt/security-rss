Title: Over 90 WordPress themes, plugins backdoored in supply chain attack
Date: 2022-01-21T10:34:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-21-over-90-wordpress-themes-plugins-backdoored-in-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/over-90-wordpress-themes-plugins-backdoored-in-supply-chain-attack/){:target="_blank" rel="noopener"}

> A massive supply chain attack compromised 93 WordPress themes and plugins to contain a backdoor, giving threat-actors full access to websites. [...]
