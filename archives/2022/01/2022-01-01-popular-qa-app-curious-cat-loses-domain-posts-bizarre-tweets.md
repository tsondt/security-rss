Title: Popular Q&A app Curious Cat loses domain, posts bizarre tweets
Date: 2022-01-01T07:46:26-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-01-01-popular-qa-app-curious-cat-loses-domain-posts-bizarre-tweets

[Source](https://www.bleepingcomputer.com/news/security/popular-qanda-app-curious-cat-loses-domain-posts-bizarre-tweets/){:target="_blank" rel="noopener"}

> Popular social networking and anonymous Q&A app, Curious Cat has lost control of its domain. Soon after the platform announced losing control of their domain, a series of bizarre events and support responses have confused the app users who are now unable to trust Curious Cat. [...]
