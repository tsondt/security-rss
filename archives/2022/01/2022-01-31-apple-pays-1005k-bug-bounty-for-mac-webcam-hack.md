Title: Apple Pays $100.5K Bug Bounty for Mac Webcam Hack
Date: 2022-01-31T18:18:41+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Bug Bounty;Cloud Security;Vulnerabilities;Web Security
Slug: 2022-01-31-apple-pays-1005k-bug-bounty-for-mac-webcam-hack

[Source](https://threatpost.com/apple-bug-bounty-mac-webcam-hack/178114/){:target="_blank" rel="noopener"}

> The researcher found that he could gain unauthorized camera access via a shared iCloud document that could also "hack every website you've ever visited." [...]
