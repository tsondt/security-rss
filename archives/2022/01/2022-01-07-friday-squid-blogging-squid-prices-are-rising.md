Title: Friday Squid Blogging: Squid Prices Are Rising
Date: 2022-01-07T22:11:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-01-07-friday-squid-blogging-squid-prices-are-rising

[Source](https://www.schneier.com/blog/archives/2022/01/friday-squid-blogging-squid-prices-are-rising.html){:target="_blank" rel="noopener"}

> The price of squid in Korea is rising due to limited supply. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
