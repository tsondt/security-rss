Title: Solarwinds fixes code execution bug in enterprise helpdesk software
Date: 2022-01-27T14:28:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-27-solarwinds-fixes-code-execution-bug-in-enterprise-helpdesk-software

[Source](https://portswigger.net/daily-swig/solarwinds-fixes-code-execution-bug-in-enterprise-helpdesk-software){:target="_blank" rel="noopener"}

> Exploit gave attackers access to corporate databases, although local access was required [...]
