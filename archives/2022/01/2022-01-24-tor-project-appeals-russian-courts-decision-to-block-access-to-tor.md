Title: Tor Project appeals Russian court's decision to block access to Tor
Date: 2022-01-24T15:39:03-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2022-01-24-tor-project-appeals-russian-courts-decision-to-block-access-to-tor

[Source](https://www.bleepingcomputer.com/news/security/tor-project-appeals-russian-courts-decision-to-block-access-to-tor/){:target="_blank" rel="noopener"}

> US-based Tor Project and Russian digital-rights protection org RosKomSvoboda are appealing a Russian court's decision to block access to public Tor nodes and the project's website. [...]
