Title: Microsoft January 2022 Patch Tuesday fixes 6 zero-days, 97 flaws
Date: 2022-01-11T13:31:10-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2022-01-11-microsoft-january-2022-patch-tuesday-fixes-6-zero-days-97-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-january-2022-patch-tuesday-fixes-6-zero-days-97-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's January 2022 Patch Tuesday, and with it comes fixes for six zero-day vulnerabilities and a total of 97 flaws. [...]
