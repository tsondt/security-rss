Title: Fake QR Codes on Parking Meters
Date: 2022-01-10T12:21:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;fraud;phishing
Slug: 2022-01-10-fake-qr-codes-on-parking-meters

[Source](https://www.schneier.com/blog/archives/2022/01/fake-qr-codes-on-parking-meters.html){:target="_blank" rel="noopener"}

> The City of Austin is warning about QR codes stuck to parking meters that take people to fraudulent payment sites. [...]
