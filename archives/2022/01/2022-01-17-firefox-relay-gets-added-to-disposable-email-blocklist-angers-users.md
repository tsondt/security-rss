Title: Firefox Relay gets added to disposable email blocklist, angers users
Date: 2022-01-17T14:33:56-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2022-01-17-firefox-relay-gets-added-to-disposable-email-blocklist-angers-users

[Source](https://www.bleepingcomputer.com/news/security/firefox-relay-gets-added-to-disposable-email-blocklist-angers-users/){:target="_blank" rel="noopener"}

> The maintainers of a "disposable email service" blocklist have decided to add Firefox Relay to the list, leaving many users of the service upset. Firefox Relay is a privacy-centric email service that enables users to protect their real email addresses and hence limit spam. [...]
