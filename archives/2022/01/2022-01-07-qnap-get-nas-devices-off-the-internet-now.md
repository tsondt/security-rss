Title: QNAP: Get NAS Devices Off the Internet Now
Date: 2022-01-07T16:14:21+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-01-07-qnap-get-nas-devices-off-the-internet-now

[Source](https://threatpost.com/qnap-nas-devices-ransomware-attacks/177452/){:target="_blank" rel="noopener"}

> There are active ransomware and brute-force attacks being launched against internet-exposed, network-attached storage devices, the device maker warned. [...]
