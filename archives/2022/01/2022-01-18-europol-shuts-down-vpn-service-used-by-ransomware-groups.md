Title: Europol shuts down VPN service used by ransomware groups
Date: 2022-01-18T06:55:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2022-01-18-europol-shuts-down-vpn-service-used-by-ransomware-groups

[Source](https://www.bleepingcomputer.com/news/security/europol-shuts-down-vpn-service-used-by-ransomware-groups/){:target="_blank" rel="noopener"}

> Law enforcement authorities from 10 countries took down VPNLab.net, a VPN service provider used by ransomware operators and malware actors. [...]
