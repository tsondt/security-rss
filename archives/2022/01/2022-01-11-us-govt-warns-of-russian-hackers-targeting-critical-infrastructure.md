Title: US govt warns of Russian hackers targeting critical infrastructure
Date: 2022-01-11T11:03:38-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-11-us-govt-warns-of-russian-hackers-targeting-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/us-govt-warns-of-russian-hackers-targeting-critical-infrastructure/){:target="_blank" rel="noopener"}

> The FBI, CISA, and the NSA have warned critical infrastructure network defenders to be ready to detect and block incoming attacks targeting organizations from US critical infrastructure sectors orchestrated by Russian-backed hacking groups. [...]
