Title: UK’s Cyber Security Center publishes new guidance to fight smishing
Date: 2022-01-19T11:44:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-19-uks-cyber-security-center-publishes-new-guidance-to-fight-smishing

[Source](https://www.bleepingcomputer.com/news/security/uk-s-cyber-security-center-publishes-new-guidance-to-fight-smishing/){:target="_blank" rel="noopener"}

> UK's National Cyber Security Center (NCSC) has published new guidance for organizations to follow when communicating with customers via SMS or phone calls. [...]
