Title: New White Rabbit ransomware linked to FIN8 hacking group
Date: 2022-01-18T11:56:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-18-new-white-rabbit-ransomware-linked-to-fin8-hacking-group

[Source](https://www.bleepingcomputer.com/news/security/new-white-rabbit-ransomware-linked-to-fin8-hacking-group/){:target="_blank" rel="noopener"}

> A new ransomware family called 'White Rabbit' appeared in the wild recently, and according to recent research findings, could be a side-operation of the FIN8 hacking group. [...]
