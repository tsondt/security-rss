Title: Qlocker ransomware returns to target QNAP NAS devices worldwide
Date: 2022-01-15T11:20:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-15-qlocker-ransomware-returns-to-target-qnap-nas-devices-worldwide

[Source](https://www.bleepingcomputer.com/news/security/qlocker-ransomware-returns-to-target-qnap-nas-devices-worldwide/){:target="_blank" rel="noopener"}

> Threat actors behind the Qlocker ransomware are once again targeting Internet-exposed QNAP Network Attached Storage (NAS) devices worldwide. [...]
