Title: For those worried about Microsoft's Pluton TPM chip: Lenovo won't even switch it on by default in latest ThinkPads
Date: 2022-01-20T20:44:23+00:00
Author: Agam Shah
Category: The Register
Tags: 
Slug: 2022-01-20-for-those-worried-about-microsofts-pluton-tpm-chip-lenovo-wont-even-switch-it-on-by-default-in-latest-thinkpads

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/20/microsoft_amd_pluton_lenovo/){:target="_blank" rel="noopener"}

> Folks can enable or disable it, install Linux as normal. Just sayin' PCs coming out this year with Microsoft's integrated Pluton security chip won't be locked down to Windows 11, and users will have the option to turn off the feature completely as well as install, say, Linux as normal, we understand.... [...]
