Title: Bug Alert launched to provide early warning system for super-critical vulnerabilities
Date: 2022-01-13T12:03:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-13-bug-alert-launched-to-provide-early-warning-system-for-super-critical-vulnerabilities

[Source](https://portswigger.net/daily-swig/bug-alert-launched-to-provide-early-warning-system-for-super-critical-vulnerabilities){:target="_blank" rel="noopener"}

> Open source project will draw on community input to rapidly close down zero-days [...]
