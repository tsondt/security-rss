Title: TellYouThePass ransomware returns as a cross-platform Golang threat
Date: 2022-01-12T11:36:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-12-tellyouthepass-ransomware-returns-as-a-cross-platform-golang-threat

[Source](https://www.bleepingcomputer.com/news/security/tellyouthepass-ransomware-returns-as-a-cross-platform-golang-threat/){:target="_blank" rel="noopener"}

> TellYouThePass ransomware has re-emerged as a Golang-compiled malware, making it easier to target major platforms beyond Windows, like macOS and Linux. [...]
