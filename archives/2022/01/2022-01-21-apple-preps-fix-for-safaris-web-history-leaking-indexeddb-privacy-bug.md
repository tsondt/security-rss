Title: Apple preps fix for Safari's web-history-leaking IndexedDB privacy bug
Date: 2022-01-21T22:56:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2022-01-21-apple-preps-fix-for-safaris-web-history-leaking-indexeddb-privacy-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/21/apple_safari_webkit_indexeddb/){:target="_blank" rel="noopener"}

> Disclosure of WebKit flaw appears to have prodded iBiz to undertake repairs Apple is preparing to repair a bug in its WebKit browser engineer that has been leaking data from its Safari 15 browser at least since the problem was reported last November.... [...]
