Title: Tor Project heads to Russian court to appeal against censorship
Date: 2022-01-26T11:54:37+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-26-tor-project-heads-to-russian-court-to-appeal-against-censorship

[Source](https://portswigger.net/daily-swig/tor-project-heads-to-russian-court-to-appeal-against-censorship){:target="_blank" rel="noopener"}

> Volunteers urged to build bridges while Tor contests blockade [...]
