Title: Crypto outfit Qubit appeals to the honour of thieves who lifted $80M of its digi-dollars
Date: 2022-01-31T05:58:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-01-31-crypto-outfit-qubit-appeals-to-the-honour-of-thieves-who-lifted-80m-of-its-digi-dollars

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/31/qubit_bug_bounty/){:target="_blank" rel="noopener"}

> Offers $2 million bug bounty and hopes perps see that record payout, and a clean conscience, as reasons to sacrifice $78m Another week, another crypto upstart admitting its lax security has been exploited and parties unknown have made off with millions. But this time there's a twist: the crypto upstart has appealed for the return of its assets by appealing to the thieves' consciences.... [...]
