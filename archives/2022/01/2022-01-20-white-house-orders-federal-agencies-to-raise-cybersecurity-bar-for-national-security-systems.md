Title: White House orders federal agencies to raise cybersecurity bar for national security systems
Date: 2022-01-20T15:24:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-20-white-house-orders-federal-agencies-to-raise-cybersecurity-bar-for-national-security-systems

[Source](https://portswigger.net/daily-swig/white-house-orders-federal-agencies-to-raise-cybersecurity-bar-for-national-security-systems){:target="_blank" rel="noopener"}

> New guidance will bring standards into line with federal civilian networks [...]
