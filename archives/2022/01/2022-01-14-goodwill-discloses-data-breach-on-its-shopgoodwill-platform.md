Title: Goodwill discloses data breach on its ShopGoodwill platform
Date: 2022-01-14T16:13:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-14-goodwill-discloses-data-breach-on-its-shopgoodwill-platform

[Source](https://www.bleepingcomputer.com/news/security/goodwill-discloses-data-breach-on-its-shopgoodwill-platform/){:target="_blank" rel="noopener"}

> American nonprofit Goodwill has disclosed a data breach that affected the accounts of customers using its ShopGoodwill.com e-commerce auction platform. [...]
