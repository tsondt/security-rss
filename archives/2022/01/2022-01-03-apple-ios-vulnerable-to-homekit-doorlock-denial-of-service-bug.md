Title: Apple iOS vulnerable to HomeKit 'doorLock' denial of service bug
Date: 2022-01-03T10:39:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2022-01-03-apple-ios-vulnerable-to-homekit-doorlock-denial-of-service-bug

[Source](https://www.bleepingcomputer.com/news/security/apple-ios-vulnerable-to-homekit-doorlock-denial-of-service-bug/){:target="_blank" rel="noopener"}

> A novel persistent denial of service vulnerability named 'doorLock' was discovered in Apple HomeKit, affecting iOS 14.7 through 15.2. [...]
