Title: ‘Elephant Beetle’ Lurks for Months in Networks
Date: 2022-01-05T22:18:28+00:00
Author: Lisa Vaas
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-01-05-elephant-beetle-lurks-for-months-in-networks

[Source](https://threatpost.com/elephant-beetle-months-networks-financial/177393/){:target="_blank" rel="noopener"}

> The group blends into an environment before loading up trivial, thickly stacked, fraudulent financial transactions too tiny to be noticed but adding up to millions of dollars. [...]
