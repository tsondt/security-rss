Title: Silk could tie up all-but-unbreakable encryption, say South Korean boffins
Date: 2022-01-28T05:31:44+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-01-28-silk-could-tie-up-all-but-unbreakable-encryption-say-south-korean-boffins

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/28/silken_security/){:target="_blank" rel="noopener"}

> At last, a worm that improves security Silk could become a means of authentication and unbreakable encryption, according to South Korean boffins.... [...]
