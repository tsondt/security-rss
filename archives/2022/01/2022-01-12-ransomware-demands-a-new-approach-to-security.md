Title: Ransomware demands… a new approach to security
Date: 2022-01-12T18:00:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2022-01-12-ransomware-demands-a-new-approach-to-security

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/12/a_new_approach_to_security/){:target="_blank" rel="noopener"}

> Spending more time thinking about trust could mean spending less cash on ransom Webinar A ransomware attack is not a smash and grab operation. Once attackers are in your systems they’ll quietly take their time working out what to encrypt to have maximum impact and make you more likely to pay up.... [...]
