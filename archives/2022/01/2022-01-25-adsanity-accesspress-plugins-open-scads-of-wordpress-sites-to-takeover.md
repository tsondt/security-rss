Title: AdSanity, AccessPress Plugins Open Scads of WordPress Sites to Takeover
Date: 2022-01-25T16:22:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;Web Security
Slug: 2022-01-25-adsanity-accesspress-plugins-open-scads-of-wordpress-sites-to-takeover

[Source](https://threatpost.com/adsanity-accesspress-plugins-wordpress-sites-takeover/177932/){:target="_blank" rel="noopener"}

> A critical security bug and a months-long, ongoing supply-chain attack spell trouble for WordPress users. [...]
