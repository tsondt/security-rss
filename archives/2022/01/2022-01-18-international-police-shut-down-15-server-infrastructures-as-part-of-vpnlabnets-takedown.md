Title: International police shut down 15 server infrastructures as part of VPNLab.net's takedown
Date: 2022-01-18T17:01:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-01-18-international-police-shut-down-15-server-infrastructures-as-part-of-vpnlabnets-takedown

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/18/police_nab_15_server_infrastructures/){:target="_blank" rel="noopener"}

> VPN service used by crims to support ransomware attacks and other illicit activity Some 15 server infrastructures used by crims to prepare ransomware attacks were seized by cops yesterday as part of an international sting to take down VPNLab.net.... [...]
