Title: 483 Crypto.com accounts compromised in $34 million hack
Date: 2022-01-20T04:10:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2022-01-20-483-cryptocom-accounts-compromised-in-34-million-hack

[Source](https://www.bleepingcomputer.com/news/security/483-cryptocom-accounts-compromised-in-34-million-hack/){:target="_blank" rel="noopener"}

> Crypto.com has confirmed that a multi-million dollar cyberattack led to the compromise of 483 of its customer accounts. Although, the company's CEO stresses that customer funds are not at risk. Crypto.com is reportedly the world's third-largest cryptocurrency trading platform. [...]
