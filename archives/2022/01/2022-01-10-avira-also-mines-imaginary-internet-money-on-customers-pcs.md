Title: Avira also mines imaginary internet money on customers' PCs
Date: 2022-01-10T18:36:05+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2022-01-10-avira-also-mines-imaginary-internet-money-on-customers-pcs

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/10/avira_ethereum/){:target="_blank" rel="noopener"}

> Who should your PC work for: you, or your antivirus vendor? Germany-based security biz Avira's antivirus has enabled a new feature: " Avira Crypto ". It's opt-in, but if you click "yes", the AV will use your computer to mine Ethereum.... [...]
