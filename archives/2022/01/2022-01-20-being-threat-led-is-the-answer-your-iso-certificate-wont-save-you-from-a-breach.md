Title: Being “Threat-Led” is the answer. Your ISO certificate won’t save you from a breach!
Date: 2022-01-20T07:30:13+00:00
Author: The Masked CISO
Category: The Register
Tags: 
Slug: 2022-01-20-being-threat-led-is-the-answer-your-iso-certificate-wont-save-you-from-a-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/20/security_compliance_issues/){:target="_blank" rel="noopener"}

> The Vectra Masked CISO series gives security leaders a place to expose the biggest issues in security and advise peers on how to overcome them. Advertorial I’ve seen it countless times. Another CISO walks into a board meeting and muddles through stats showing their compliance status. Great, you’re 75% compliant with ISO 27001, but what does this tell anyone about their level of risk?... [...]
