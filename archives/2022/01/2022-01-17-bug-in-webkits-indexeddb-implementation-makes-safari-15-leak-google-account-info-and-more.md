Title: Bug in WebKit's IndexedDB implementation makes Safari 15 leak Google account info... and more
Date: 2022-01-17T18:31:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-01-17-bug-in-webkits-indexeddb-implementation-makes-safari-15-leak-google-account-info-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/17/safari_15_indexeddb_bug/){:target="_blank" rel="noopener"}

> Glitch is spilling private data and there's not much Apple users can do about it An improperly implemented API that stores data on browsers has caused a vulnerability in Safari 15 that leaks user internet activity and personal identifiers.... [...]
