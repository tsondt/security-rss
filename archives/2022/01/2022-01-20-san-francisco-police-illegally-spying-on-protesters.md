Title: San Francisco Police Illegally Spying on Protesters
Date: 2022-01-20T12:13:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;EFF;EPIC;police;privacy;surveillance
Slug: 2022-01-20-san-francisco-police-illegally-spying-on-protesters

[Source](https://www.schneier.com/blog/archives/2022/01/san-francisco-police-illegally-spying-on-protesters.html){:target="_blank" rel="noopener"}

> Last summer, the San Francisco police illegally used surveillance cameras at the George Floyd protests. The EFF is suing the police: This surveillance invaded the privacy of protesters, targeted people of color, and chills and deters participation and organizing for future protests. The SFPD also violated San Francisco’s new Surveillance Technology Ordinance. It prohibits city agencies like the SFPD from acquiring, borrowing, or using surveillance technology, without prior approval from the city’s Board of Supervisors, following an open process that includes public participation. Here, the SFPD went through no such process before spying on protesters with this network of surveillance [...]
