Title: VPNLab takedown: Authorities dismantle secure communication tool favored by cybercriminals
Date: 2022-01-18T10:48:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2022-01-18-vpnlab-takedown-authorities-dismantle-secure-communication-tool-favored-by-cybercriminals

[Source](https://portswigger.net/daily-swig/vpnlab-takedown-authorities-dismantle-secure-communication-tool-favored-by-cybercriminals){:target="_blank" rel="noopener"}

> VPN service was being used to support ‘serious criminal acts’, Europol says [...]
