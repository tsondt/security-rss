Title: Indonesia bars financial institutions from offering crypto services
Date: 2022-01-27T07:13:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2022-01-27-indonesia-bars-financial-institutions-from-offering-crypto-services

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/27/indonesia_cryptocurrency_trading_ban/){:target="_blank" rel="noopener"}

> Advises citizens to avoid 'Ponzi schemes' Another week, another big economy restricting cryptocurrency. This time Indonesia has barred financial services firms from offering bit-buck-related services to their customers.... [...]
