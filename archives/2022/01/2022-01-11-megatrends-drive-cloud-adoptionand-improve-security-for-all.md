Title: Megatrends drive cloud adoption—and improve security for all
Date: 2022-01-11T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2022-01-11-megatrends-drive-cloud-adoptionand-improve-security-for-all

[Source](https://cloud.google.com/blog/products/identity-security/8-megatrends-drive-cloud-adoption-and-improve-security-for-all/){:target="_blank" rel="noopener"}

> We are often asked if the cloud is more secure than on-premise infrastructure. The quick answer is that, in general, it is. The complete answer is more nuanced and is grounded in a series of cloud security “megatrends” that drive technological innovation and improve the overall security posture of cloud providers and customers. An on-prem environment can, with a lot of effort, have the same default level of security as a reputable cloud provider’s infrastructure. Conversely, a weak cloud configuration can give rise to many security issues. But in general, the base security of the cloud coupled with a suitably [...]
