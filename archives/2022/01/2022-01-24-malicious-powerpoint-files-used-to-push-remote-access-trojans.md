Title: Malicious PowerPoint files used to push remote access trojans
Date: 2022-01-24T09:37:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2022-01-24-malicious-powerpoint-files-used-to-push-remote-access-trojans

[Source](https://www.bleepingcomputer.com/news/security/malicious-powerpoint-files-used-to-push-remote-access-trojans/){:target="_blank" rel="noopener"}

> Since December 2021, a growing trend in phishing campaigns has emerged that uses malicious PowerPoint documents to distribute various types of malware, including remote access and information-stealing trojans. [...]
