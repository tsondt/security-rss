Title: Segway Hit by Magecart Attack Hiding in a Favicon
Date: 2022-01-25T20:35:56+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security
Slug: 2022-01-25-segway-hit-by-magecart-attack-hiding-in-a-favicon

[Source](https://threatpost.com/segway-magecart-attack-favicon/177971/){:target="_blank" rel="noopener"}

> Visitors who shopped on the company's eCommerce website in January will likely find their payment-card data heisted, researchers warned. [...]
