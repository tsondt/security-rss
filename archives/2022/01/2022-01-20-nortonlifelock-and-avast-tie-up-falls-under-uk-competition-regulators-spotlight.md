Title: NortonLifeLock and Avast tie-up falls under UK competition regulator's spotlight
Date: 2022-01-20T11:03:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2022-01-20-nortonlifelock-and-avast-tie-up-falls-under-uk-competition-regulators-spotlight

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/20/nortonlocklife_avast_cma/){:target="_blank" rel="noopener"}

> CMA invites comments from 'interested parties' on what merger means to them The UK's Competition and Markets Authority has invited comments from industry and interested parties about NortonLifeLock's proposed $8bn purchase of fellow infosec outfit Avast.... [...]
