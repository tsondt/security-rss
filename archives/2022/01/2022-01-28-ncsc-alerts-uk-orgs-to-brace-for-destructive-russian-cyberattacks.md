Title: NCSC alerts UK orgs to brace for destructive Russian cyberattacks
Date: 2022-01-28T11:20:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2022-01-28-ncsc-alerts-uk-orgs-to-brace-for-destructive-russian-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/ncsc-alerts-uk-orgs-to-brace-for-destructive-russian-cyberattacks/){:target="_blank" rel="noopener"}

> The UK's National Cyber Security Centre (NCSC) is urging organizations to bolster security and prepare for a potential wave of destructive cyberattacks after recent breaches of Ukrainian entities. [...]
