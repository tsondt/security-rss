Title: John Edwards takes the reins at the UK's data protection watchdog
Date: 2022-01-04T13:58:13+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2022-01-04-john-edwards-takes-the-reins-at-the-uks-data-protection-watchdog

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/04/edwards_ico/){:target="_blank" rel="noopener"}

> Information Commissioner faces a year of upheaval in data law The Information Commissioner's Office has confirmed that former New Zealand privacy commissioner John Edwards has started his new role as the UK's Information Commissioner.... [...]
