Title: US bans major Chinese telecom over national security risks
Date: 2022-01-28T11:30:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-28-us-bans-major-chinese-telecom-over-national-security-risks

[Source](https://www.bleepingcomputer.com/news/security/us-bans-major-chinese-telecom-over-national-security-risks/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) has revoked China Unicom Americas' license, one of the world's largest mobile service providers, over "serious national security concerns." [...]
