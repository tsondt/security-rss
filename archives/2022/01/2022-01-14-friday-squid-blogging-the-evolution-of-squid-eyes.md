Title: Friday Squid Blogging: The Evolution of Squid Eyes
Date: 2022-01-14T22:12:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2022-01-14-friday-squid-blogging-the-evolution-of-squid-eyes

[Source](https://www.schneier.com/blog/archives/2022/01/friday-squid-blogging-the-evolution-of-squid-eyes.html){:target="_blank" rel="noopener"}

> New research : The researchers from the FAS Center for Systems Biology discovered a network of genes important in squid eye development that are known to also play a crucial role in limb development across animals, including vertebrates and insects. The scientists say these genes have been repurposed in squid to make camera-lens-type eyes. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
