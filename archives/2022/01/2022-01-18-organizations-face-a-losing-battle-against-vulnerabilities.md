Title: Organizations Face a ‘Losing Battle’ Against Vulnerabilities
Date: 2022-01-18T14:03:08+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Bug Bounty;Malware;Mobile Security;Vulnerabilities;Web Security
Slug: 2022-01-18-organizations-face-a-losing-battle-against-vulnerabilities

[Source](https://threatpost.com/organizations-losing-battle-vulnerabilities/177696/){:target="_blank" rel="noopener"}

> Companies must take more ‘innovative and proactive’ approaches to security in 2022 to combat threats that emerged last year, researchers said. [...]
