Title: Introducing Certificate Manager to simplify SaaS scale TLS and certificate management
Date: 2022-01-31T17:00:00+00:00
Author: Babi Seal
Category: GCP Security
Tags: Developers & Practitioners;Networking;DevOps & SRE;Identity & Security
Slug: 2022-01-31-introducing-certificate-manager-to-simplify-saas-scale-tls-and-certificate-management

[Source](https://cloud.google.com/blog/products/identity-security/simplify-saas-scale-tls-certificate-management/){:target="_blank" rel="noopener"}

> We’re excited to announce the public preview of Certificate Manager and its integration with External HTTPS Load Balancing. Certificate Manager enables you to use External HTTPS Load Balancing with as many certificates or domains as you need. You can bring your own TLS certificates and keys if you have an existing certificate lifecycle management solution you'd like to use with Google Cloud, or enjoy the convenience of our fully Managed TLS offerings. Extend the security and performance of the Google network to your customers Certificate Manager brings support for multiple certificates per customer. When coupled with our global anycast load [...]
