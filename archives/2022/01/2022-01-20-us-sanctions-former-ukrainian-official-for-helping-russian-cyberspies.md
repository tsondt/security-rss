Title: US sanctions former Ukrainian official for helping Russian cyberspies
Date: 2022-01-20T11:37:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2022-01-20-us-sanctions-former-ukrainian-official-for-helping-russian-cyberspies

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-former-ukrainian-official-for-helping-russian-cyberspies/){:target="_blank" rel="noopener"}

> The U.S. Treasury Department announced today sanctions against Volodymyr Oliynyk, a former Ukrainian official, for collecting and sharing info on critical Ukrainian infrastructure with Russia's Federal Security Service (FSB). [...]
