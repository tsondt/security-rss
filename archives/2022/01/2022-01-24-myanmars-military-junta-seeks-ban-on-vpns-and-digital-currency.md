Title: Myanmar's military junta seeks ban on VPNs and digital currency
Date: 2022-01-24T07:02:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2022-01-24-myanmars-military-junta-seeks-ban-on-vpns-and-digital-currency

[Source](https://go.theregister.com/feed/www.theregister.com/2022/01/24/myanmar_military_junta_bans_vpns_crypto/){:target="_blank" rel="noopener"}

> People would no longer be able to rely on VPNs for their preferred communication tool, Facebook Myanmar's military junta has floated a cyber security law that would ban the use of virtual private networks, under penalty of imprisonment and/or fines, leaving digital rights organisations concerned about the effects of further closing the country off digitally to the outside world.... [...]
