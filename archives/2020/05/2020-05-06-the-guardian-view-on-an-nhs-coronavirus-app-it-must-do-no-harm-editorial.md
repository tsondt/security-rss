Title: The Guardian view on an NHS coronavirus app: it must do no harm | Editorial
Date: 2020-05-06T18:13:18+00:00
Author: Editorial
Category: The Guardian
Tags: NHS;Health;Society;Coronavirus outbreak;Infectious diseases;Medical research;Apple;Google;Alphabet;Technology;Privacy;Data and computer security;Computing;World news;Microbiology;Science
Slug: the-guardian-view-on-an-nhs-coronavirus-app-it-must-do-no-harm-editorial

[Source](https://www.theguardian.com/commentisfree/2020/may/06/the-guardian-view-on-an-nhs-coronavirus-app-it-must-do-no-harm){:target="_blank" rel="noopener"}

> Smartphones can be used to digitally trace Covid-19. But not if the public don’t download an app over privacy fears – or find it won’t work on their device The idea of the NHS tracing app is to enable smartphones to track users and tell them whether they interacted with someone who had Covid-19. Yet this will work only if large proportions of the population download the app. No matter how smart a solution may appear, mass consent is required. That will not be easy. Ministers and officials have failed to address the trade-offs between health and privacy by being [...]
