Title: EasyJet hacking attack: are you affected and what should you do?
Date: 2020-05-19T13:14:44+00:00
Author: Rupert Jones
Category: The Guardian
Tags: easyJet;Airline industry;Business;UK news;Data and computer security;Technology
Slug: easyjet-hacking-attack-are-you-affected-and-what-should-you-do

[Source](https://www.theguardian.com/business/2020/may/19/easyjet-hacking-attack-what-to-do-customers){:target="_blank" rel="noopener"}

> The airline has said the personal information of 9 million customers has been compromised EasyJet reveals cyber-attack exposed 9m customers’ details EasyJet revealed on Tuesday it had suffered a “highly sophisticated” cyber-attack. It comes at a time of heightened concern about a surge in online and phone scams linked to the coronavirus pandemic. Related: EasyJet reveals cyber-attack exposed 9m customers' details Continue reading... [...]
