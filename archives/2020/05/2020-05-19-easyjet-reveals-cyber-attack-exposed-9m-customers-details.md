Title: EasyJet reveals cyber-attack exposed 9m customers' details
Date: 2020-05-19T11:13:57+00:00
Author: Jasper Jolly
Category: The Guardian
Tags: easyJet;Airline industry;Business;UK news;Cybercrime;Data and computer security;Technology;Internet;Consumer affairs;Money;Hacking;Privacy
Slug: easyjet-reveals-cyber-attack-exposed-9m-customers-details

[Source](https://www.theguardian.com/business/2020/may/19/easyjet-cyber-attack-customers-details-credit-card){:target="_blank" rel="noopener"}

> Airline apologises after credit card details of about 2,200 passengers were stolen • Q&A: are you affected and what should you do? EasyJet has revealed that the personal information of 9 million customers was accessed in a “highly sophisticated” cyber-attack on the airline. The company said on Tuesday that email addresses and travel details were accessed and it would contact the customers affected. Continue reading... [...]
