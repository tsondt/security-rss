Title: More on COVID Surveillance: Mobile Phone Location
Date: 2020-05-06T18:01:16+00:00
Author: Eric Rescorla
Category: Mozilla Security
Tags: Privacy
Slug: more-on-covid-surveillance-mobile-phone-location

[Source](https://blog.mozilla.org/blog/2020/05/06/more-on-covid-surveillance-mobile-phone-location/){:target="_blank" rel="noopener"}

> Previously I wrote about the use of mobile apps for COVID contact tracing. This idea has gotten a lot of attention in the tech press — probably because there are... Read more The post More on COVID Surveillance: Mobile Phone Location appeared first on The Mozilla Blog. [...]
