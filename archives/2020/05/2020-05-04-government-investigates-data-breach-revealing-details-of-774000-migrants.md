Title: Government investigates data breach revealing details of 774,000 migrants
Date: 2020-05-04T17:30:14+00:00
Author: Paul Karp
Category: The Guardian
Tags: Australian immigration and asylum;Data and computer security;Australia news
Slug: government-investigates-data-breach-revealing-details-of-774000-migrants

[Source](https://www.theguardian.com/australia-news/2020/may/05/government-investigates-data-breach-revealing-details-of-774000-migrants){:target="_blank" rel="noopener"}

> Guardian Australia on Sunday revealed SkillSelect app allowed users to see partial names of applicants for skilled visas The home affairs and employment departments are investigating a data breach revealing the personal details of 774,000 migrants and people aspiring to migrate to Australia, despite playing down the seriousness of the breach. On Sunday, Guardian Australia revealed the government’s SkillSelect app allowed users to see unique identifiers of applicants for skilled visas, including partial names, which could then be used through searches with multiple filters to reveal other information about applicants. Related: Immigrants don't take Australian jobs. They create jobs for [...]
