Title: Protecting Search and Browsing Data from Warrantless Access
Date: 2020-05-22T15:20:25+00:00
Author: Alan Davidson
Category: Mozilla Security
Tags: General;Mozilla News;Press Releases;Privacy;Statement
Slug: protecting-search-and-browsing-data-from-warrantless-access

[Source](https://blog.mozilla.org/blog/2020/05/22/protecting-search-and-browsing-data-from-warrantless-access/){:target="_blank" rel="noopener"}

> As the maker of Firefox, we know that browsing and search data can provide a detailed portrait of our private lives and needs to be protected. That’s why we work... Read more The post Protecting Search and Browsing Data from Warrantless Access appeared first on The Mozilla Blog. [...]
