Title: Early access to superannuation paused as police freeze $120,000 in allegedly stolen funds
Date: 2020-05-08T01:57:44+00:00
Author: Paul Karp
Category: The Guardian
Tags: Superannuation;Coronavirus outbreak;Australia news;Crime - Australia;Identity fraud;Data and computer security
Slug: early-access-to-superannuation-paused-as-police-freeze-120000-in-allegedly-stolen-funds

[Source](https://www.theguardian.com/australia-news/2020/may/08/early-access-to-superannuation-paused-as-police-freeze-120000-in-allegedly-stolen-funds){:target="_blank" rel="noopener"}

> ‘Sophisticated’ identity theft attack leads to Australian Tax Office stopping early super withdrawals until Monday Sign up for Guardian Australia’s daily coronavirus email Download the free Guardian app to get the most important news notifications Allegations of identity theft involving 150 Australians have forced the government to pause the early release of superannuation, after police froze $120,000 believed to have been ripped off from retirement savings. On Friday the assistant treasurer, Michael Sukkar, announced the Australian Tax Office would pause requests for early access of superannuation until Monday “out of an abundance of caution” to consider further anti-fraud protection. Related: [...]
