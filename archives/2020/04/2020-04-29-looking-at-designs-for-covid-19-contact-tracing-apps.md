Title: Looking at designs for COVID-19 Contact Tracing Apps
Date: 2020-04-29T15:58:58+00:00
Author: Eric Rescorla
Category: Mozilla Security
Tags: Privacy
Slug: looking-at-designs-for-covid-19-contact-tracing-apps

[Source](https://blog.mozilla.org/blog/2020/04/29/designs-contact-tracing-apps/){:target="_blank" rel="noopener"}

> A number of the proposals for how to manage the COVID-19 pandemic rely on being able to determine who has come into contact with infected people and therefore are at... Read more The post Looking at designs for COVID-19 Contact Tracing Apps appeared first on The Mozilla Blog. [...]
