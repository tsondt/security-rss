Title: Using Big Tech to tackle coronavirus risks swapping one lockdown for another | Adam Smith
Date: 2020-04-22T10:00:42+00:00
Author: Adam Smith
Category: The Guardian
Tags: Coronavirus outbreak;Surveillance;UK news;Technology;Politics;Data and computer security;World news;Privacy
Slug: using-big-tech-to-tackle-coronavirus-risks-swapping-one-lockdown-for-another-adam-smith

[Source](https://www.theguardian.com/commentisfree/2020/apr/22/using-big-tech-to-tackle-coronavirus-risks-swapping-one-lockdown-for-another){:target="_blank" rel="noopener"}

> An app that logs movements and contacts might seem like a fair trade now but we risk giving away our privacy for good Coronavirus – latest updates See all our coronavirus coverage Even when the lockdown is lifted, there is no guarantee that life will ever return to normal. To prevent a future outbreak of coronavirus, the UK will need to roll out mass testing, maintain some social distancing measures and closely monitor communities to curb future flare-ups. In pursuing that last aim, governments across the world are developing technology to track our movements. When lockdown ends, technology could be [...]
