Title: Email bungle at company seeking jobkeeper payments exposes staff's personal details
Date: 2020-04-22T23:04:56+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Australian media;Australia news;Privacy;Data and computer security;Welfare
Slug: email-bungle-at-company-seeking-jobkeeper-payments-exposes-staffs-personal-details

[Source](https://www.theguardian.com/media/2020/apr/23/email-bungle-at-company-seeking-jobkeeper-payments-exposes-staffs-personal-details){:target="_blank" rel="noopener"}

> Names, addresses and birthdates of more than 100 people shared in privacy breach The company responsible for delivering traffic reports on radio and TV stations across Australia accidentally sent out the dates of birth, names and home addresses of more than 100 current and former staff to potentially thousands of people as the company seeks to apply for the jobkeeper payments. Australian Traffic Network provides short traffic report updates during news bulletins to 80 radio and television stations, including the ABC, Seven, Nine, 10, 2GB and Triple M. Related: As Australia takes on Google and Facebook over news content, the [...]
