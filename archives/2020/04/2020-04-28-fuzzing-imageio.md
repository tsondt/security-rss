Title: Fuzzing ImageIO
Date: 2020-04-28T10:09:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: fuzzing-imageio

[Source](https://googleprojectzero.blogspot.com/2020/04/fuzzing-imageio.html){:target="_blank" rel="noopener"}

> Posted by Samuel Groß, Project Zero This blog post discusses an old type of issue, vulnerabilities in image format parsers, in a new(er) context: on interactionless code paths in popular messenger apps. This research was focused on the Apple ecosystem and the image parsing API provided by it: the ImageIO framework. Multiple vulnerabilities in image parsing code were found, reported to Apple or the respective open source image library maintainers, and subsequently fixed. During this research, a lightweight and low-overhead guided fuzzing approach for closed source binaries was implemented and is released alongside this blogpost. To reiterate an important point, [...]
