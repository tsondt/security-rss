Title: Why isn't the government publishing more data about coronavirus deaths? | Jeni Tennison
Date: 2020-04-02T05:00:41+00:00
Author: Jeni Tennison
Category: The Guardian
Tags: Coronavirus outbreak;World news;Data and computer security;Data protection;Technology;UK news;Politics;Infectious diseases
Slug: why-isnt-the-government-publishing-more-data-about-coronavirus-deaths-jeni-tennison

[Source](https://www.theguardian.com/commentisfree/2020/apr/02/government-publish-data-coronavirus-deaths){:target="_blank" rel="noopener"}

> Studying the past is futile in an unprecedented crisis. Science is the answer – and open data is paramount • Coronavirus – latest updates • See all our coronavirus coverage Wherever we look, there is a demand for data about Covid-19. We devour dashboards, graphs and visualisations. We want to know about the numbers of tests, cases and deaths; how many beds and ventilators are available, how many NHS workers are off sick. When information is missing, we speculate about what the government might be hiding, or fill in the gaps with anecdotes. Data is a necessary ingredient in day-to-day [...]
