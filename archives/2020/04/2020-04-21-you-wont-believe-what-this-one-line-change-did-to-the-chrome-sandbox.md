Title: You Won't Believe what this One Line Change Did to the Chrome Sandbox
Date: 2020-04-21T11:25:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: you-wont-believe-what-this-one-line-change-did-to-the-chrome-sandbox

[Source](https://googleprojectzero.blogspot.com/2020/04/you-wont-believe-what-this-one-line.html){:target="_blank" rel="noopener"}

> Posted by James Forshaw, Project Zero The Chromium sandbox on Windows has stood the test of time. It’s considered one of the better sandboxing mechanisms deployed at scale without requiring elevated privileges to function. For all the good, it does have its weaknesses. The main one being the sandbox’s implementation is reliant on the security of the Windows OS. Changing the behavior of Windows is out of the control of the Chromium development team. If a bug is found in the security enforcement mechanisms of Windows then the sandbox can break. This blog is about a vulnerability introduced in Windows [...]
