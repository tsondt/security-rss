Title: Contact Tracing, Governments, and Data
Date: 2020-04-29T16:02:41+00:00
Author: Alan Davidson
Category: Mozilla Security
Tags: General;Mozilla Community;Mozilla News;Press Releases;Privacy;Contact Tracing
Slug: contact-tracing-governments-and-data

[Source](https://blog.mozilla.org/blog/2020/04/29/contact-tracing-governments-and-data/){:target="_blank" rel="noopener"}

> Digital contact tracing apps have emerged in recent weeks as one potential tool in a suite of solutions that would allow countries around the world to respond to the COVID-19... Read more The post Contact Tracing, Governments, and Data appeared first on The Mozilla Blog. [...]
