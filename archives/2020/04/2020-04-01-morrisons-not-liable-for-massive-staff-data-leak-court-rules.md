Title: Morrisons not liable for massive staff data leak, court rules
Date: 2020-04-01T12:53:32+00:00
Author: PA Media
Category: The Guardian
Tags: Morrisons;Data and computer security;Business;UK supreme court;Retail industry;Supermarkets;Law;UK news;Privacy
Slug: morrisons-not-liable-for-massive-staff-data-leak-court-rules

[Source](https://www.theguardian.com/business/2020/apr/01/morrisons-is-not-liable-for-massive-staff-data-leak-court-rules){:target="_blank" rel="noopener"}

> UK supreme court says retailer not to blame for actions of employee with grudge The UK’s highest court has ruled that Morrisons should not be held liable for the criminal act of an employee with a grudge who leaked the payroll data of about 100,000 members of staff. The supermarket group brought a supreme court challenge in an attempt to overturn previous judgments which gave the go-ahead for compensation claims by thousands of employees whose personal details were posted on the internet. Continue reading... [...]
