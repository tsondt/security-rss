Title: Who is Tech Investor John Bernard?
Date: 2020-09-25T13:21:00+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Colette Davies;Igor Gubskyi;Ihor Hubskyi;Inside Knowledge;Iryna Davies;John Bernard;John Clifton Davies;john888@myswissmail.ch;National Crime Agency;The Private Office of John Bernard
Slug: who-is-tech-investor-john-bernard

[Source](https://krebsonsecurity.com/2020/09/who-is-tech-investor-john-bernard/){:target="_blank" rel="noopener"}

> John Bernard, the subject of a story here last week about a self-proclaimed millionaire investor who has bilked countless tech startups, appears to be a pseudonym for John Clifton Davies, a U.K. man who absconded from justice before being convicted on multiple counts of fraud in 2015. Prior to his conviction, Davies served 16 months in jail before being cleared of murdering his wife on their honeymoon in India. The Private Office of John Bernard, which advertises itself as a capital investment firm based in Switzerland, has for years been listed on multiple investment sites as the home of a [...]
