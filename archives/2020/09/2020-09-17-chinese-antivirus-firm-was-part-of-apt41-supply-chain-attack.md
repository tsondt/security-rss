Title: Chinese Antivirus Firm Was Part of APT41 ‘Supply Chain’ Attack
Date: 2020-09-17T22:03:21+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;anvisoft;APT41;Barium;Chengdu 404;Cisco;Citrix;D-Link;Pulse;SonarX;Tan Dailin;Techcrunch;Wicked Panda;Wicked Rose;Wicked Spider;Winnti;Withered Rose;Zack Whittaker
Slug: chinese-antivirus-firm-was-part-of-apt41-supply-chain-attack

[Source](https://krebsonsecurity.com/2020/09/chinese-antivirus-firm-was-part-of-apt41-supply-chain-attack/){:target="_blank" rel="noopener"}

> The U.S. Justice Department this week indicted seven Chinese nationals for a decade-long hacking spree that targeted more than 100 high-tech and online gaming companies. The government alleges the men used malware-laced phishing emails and “supply chain” attacks to steal data from companies and their customers. One of the alleged hackers was first profiled here in 2012 as the owner of a Chinese antivirus firm. Image: FBI Charging documents say the seven men are part of a hacking group known variously as “ APT41,” “ Barium,” “ Winnti,” “ Wicked Panda,” and “ Wicked Spider.” Once inside of a target [...]
