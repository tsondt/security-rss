Title: Govt. Services Firm Tyler Technologies Hit in Apparent Ransomware Attack
Date: 2020-09-23T23:06:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;ransomware;Tyler Technologies;tylertech.com
Slug: govt-services-firm-tyler-technologies-hit-in-apparent-ransomware-attack

[Source](https://krebsonsecurity.com/2020/09/govt-services-firm-tyler-technologies-hit-in-apparent-ransomware-attack/){:target="_blank" rel="noopener"}

> Tyler Technologies, a Texas-based company that bills itself as the largest provider of software and technology services to the United States public sector, is battling a network intrusion that has disrupted its operations. The company declined to discuss the exact cause of the disruption, but their response so far is straight out of the playbook for responding to ransomware incidents. Plano, Texas-based Tyler Technologies [ NYSE:TYL ] has some 5,300 employees and brought in revenues of more than $1 billion in 2019. It sells a broad range of services to state and local governments, including appraisal and tax software, integrated [...]
