Title: Plane-tracking site Flight Radar 24 DDoSed... just as drones spotted buzzing over Azerbaijan and Armenia
Date: 2020-09-29T18:44:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: plane-tracking-site-flight-radar-24-ddosed-just-as-drones-spotted-buzzing-over-azerbaijan-and-armenia

[Source](https://go.theregister.com/feed/www.theregister.com/2020/09/29/flight_radar_24_ddos/){:target="_blank" rel="noopener"}

> That's one way of poking the world's eyes out for a few hours Popular plane-tracking website Flight Radar 24 has been the victim of multiple DDoS attacks over the past few days – and though the site's operators haven't attributed blame, some have wondered if a regional conflict may have been the cause.... [...]
