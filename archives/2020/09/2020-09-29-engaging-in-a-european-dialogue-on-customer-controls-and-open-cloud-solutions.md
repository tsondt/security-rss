Title: Engaging in a European dialogue on customer controls and open cloud solutions
Date: 2020-09-29T05:00:00+00:00
Author: Thomas Kurian
Category: GCP Security
Tags: Google Cloud Platform;Inside Google Cloud;Identity & Security
Slug: engaging-in-a-european-dialogue-on-customer-controls-and-open-cloud-solutions

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-is-addressing-data-sovereignty-in-europe-2020/){:target="_blank" rel="noopener"}

> At last year’s Europe-focused Google Cloud Next event, we outlined our commitment to European customers, sharing ways Google Cloud is helping European organizations transform their businesses in our cloud and address their strict data security and privacy requirements. This included expanding our existing cloud regions on the continent, growing our ecosystem of local partners, and adding compliance certifications, to name a few. Since then, we have made significant progress on all these fronts and are deeply committed to delivering additional capabilities. In recent months, European customers and policymakers have placed an even greater emphasis on working with cloud service providers [...]
