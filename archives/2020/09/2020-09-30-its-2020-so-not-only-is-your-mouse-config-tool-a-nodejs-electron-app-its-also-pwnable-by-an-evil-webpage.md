Title: It's 2020 so not only is your mouse config tool a Node.JS Electron app, it's also pwnable by an evil webpage
Date: 2020-09-30T07:50:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: its-2020-so-not-only-is-your-mouse-config-tool-a-nodejs-electron-app-its-also-pwnable-by-an-evil-webpage

[Source](https://go.theregister.com/feed/www.theregister.com/2020/09/30/kensingtonworks_mouse_flaw/){:target="_blank" rel="noopener"}

> Malicious JavaScript can inject commands to execute Earlier this year, peripheral maker Kensington patched its desktop software to close a vulnerability that could have been exploited by malicious websites to quietly hijack victims' computers.... [...]
