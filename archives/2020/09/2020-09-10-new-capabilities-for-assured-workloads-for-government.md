Title: New capabilities for Assured Workloads for Government
Date: 2020-09-10T16:00:00+00:00
Author: Bryce Buffaloe
Category: GCP Security
Tags: Google Cloud Platform;Compliance;Public Sector;Identity & Security
Slug: new-capabilities-for-assured-workloads-for-government

[Source](https://cloud.google.com/blog/products/identity-security/assured-workloads-for-government-is-now-ga/){:target="_blank" rel="noopener"}

> Earlier this year, we announced Assured Workloads for Government, a first-of-its kind service that allows Google Cloud Platform (GCP) customers to quickly and easily create controlled environments where U.S. data location and personnel access controls are enforced in any of our U.S. cloud regions (private beta). Starting today, customers who require FedRAMP Moderate support will also be able to leverage Assured Workloads, which is now generally available (GA). All new projects that require FedRAMP Moderate controls can be created at no additional charge in Assured Workloads —and if you've already accepted FedRAMP Moderate terms, we'll automatically transition those projects. Assured [...]
