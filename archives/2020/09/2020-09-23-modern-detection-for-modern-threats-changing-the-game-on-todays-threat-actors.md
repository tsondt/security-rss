Title: Modern detection for modern threats: Changing the game on today’s threat actors
Date: 2020-09-23T16:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: modern-detection-for-modern-threats-changing-the-game-on-todays-threat-actors

[Source](https://cloud.google.com/blog/products/identity-security/introducing-chronicle-detect-from-google-cloud/){:target="_blank" rel="noopener"}

> 2020 has introduced complex challenges for enterprise IT environments. Data volumes have grown, attacker techniques have become complex yet more subtle, and existing detection and analytics tools struggle to keep up. In legacy security systems, it’s difficult to run many rules in parallel and at scale—so even if detection is possible, it may be too late. Most analytics tools use a data query language, making it difficult to write detection rules described in scenarios such as the Mitre ATT&CK framework. Finally, detections often require threat intelligence on attacker activity that many vendors simply don’t have. As a result, security tools [...]
