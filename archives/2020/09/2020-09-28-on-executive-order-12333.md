Title: On Executive Order 12333
Date: 2020-09-28T11:21:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;data collection;espionage;geolocation;national security policy;privacy;surveillance
Slug: on-executive-order-12333

[Source](https://www.schneier.com/blog/archives/2020/09/on-executive-order-12333.html){:target="_blank" rel="noopener"}

> Mark Jaycox has written a long article on the US Executive Order 12333: “ No Oversight, No Limits, No Worries: A Primer on Presidential Spying and Executive Order 12,333 “: Abstract : Executive Order 12,333 (“EO 12333”) is a 1980s Executive Order signed by President Ronald Reagan that, among other things, establishes an overarching policy framework for the Executive Branch’s spying powers. Although electronic surveillance programs authorized by EO 12333 generally target foreign intelligence from foreign targets, its permissive targeting standards allow for the substantial collection of Americans’ communications containing little to no foreign intelligence value. This fact alone necessitates [...]
