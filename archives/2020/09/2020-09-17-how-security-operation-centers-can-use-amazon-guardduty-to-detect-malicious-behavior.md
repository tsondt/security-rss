Title: How Security Operation Centers can use Amazon GuardDuty to detect malicious behavior
Date: 2020-09-17T19:26:38+00:00
Author: Darren House
Category: AWS Security
Tags: Amazon GuardDuty;Intermediate (200);Security, Identity, & Compliance;Cloud security;Security Blog;threat detection;Threat Intelligence
Slug: how-security-operation-centers-can-use-amazon-guardduty-to-detect-malicious-behavior

[Source](https://aws.amazon.com/blogs/security/how-security-operation-centers-can-use-amazon-guardduty-to-detect-malicious-behavior/){:target="_blank" rel="noopener"}

> The Security Operations Center (SOC) has a tough job. As customers modernize and shift to cloud architectures, the ability to monitor, detect, and respond to risks poses different challenges. In this post we address how Amazon GuardDuty can address some common concerns of the SOC regarding the number of security tools and the overhead to [...]
