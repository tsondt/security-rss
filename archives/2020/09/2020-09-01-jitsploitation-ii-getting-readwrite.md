Title: JITSploitation II: Getting Read/Write
Date: 2020-09-01T06:36:00.001000-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: jitsploitation-ii-getting-readwrite

[Source](https://googleprojectzero.blogspot.com/2020/09/jitsploitation-two.html){:target="_blank" rel="noopener"}

> Posted by Samuel Groß, Project Zero This three-part series highlights the technical challenges involved in finding and exploiting JavaScript engine vulnerabilities in modern web browsers and evaluates current exploit mitigation technologies. The exploited vulnerability, CVE-2020-9802, was fixed in iOS 13.5, while two of the mitigation bypasses, CVE-2020-9870 and CVE-2020-9910, were fixed in iOS 13.6. ========== This is the second part in a series about a Safari renderer exploit from a JIT bug. In Part 1, a vulnerability in the DFG JIT’s implementation of Common-Subexpression Elimination was discussed. The second part starts from the well-known addrof and fakeobj primitives and shows [...]
