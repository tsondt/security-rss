Title: How to add DNS filtering to your NAT instance with Squid
Date: 2020-09-23T17:46:19+00:00
Author: Nicolas Malaval
Category: AWS Security
Tags: Advanced (300);Amazon VPC;Security, Identity, & Compliance;DNS filtering;NAT;NAT instance;proxy;Security Blog;Squid
Slug: how-to-add-dns-filtering-to-your-nat-instance-with-squid

[Source](https://aws.amazon.com/blogs/security/how-to-add-dns-filtering-to-your-nat-instance-with-squid/){:target="_blank" rel="noopener"}

> September 23, 2020: The squid configuration file in this blog post and associated YAML template have been updated. September 4, 2019: We’ve updated this blog post, initially published on January 26, 2016. Major changes include: support of Amazon Linux 2, no longer having to compile Squid 3.5, and a high availability version of the solution [...]
