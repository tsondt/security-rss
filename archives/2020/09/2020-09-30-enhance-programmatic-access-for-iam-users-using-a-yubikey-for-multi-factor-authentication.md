Title: Enhance programmatic access for IAM users using a YubiKey for multi-factor authentication
Date: 2020-09-30T17:21:39+00:00
Author: Edouard Kachelmann
Category: AWS Security
Tags: AWS Command Line Interface;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;AWS CLI;AWS IAM;Security Blog;Temporary Credentials
Slug: enhance-programmatic-access-for-iam-users-using-a-yubikey-for-multi-factor-authentication

[Source](https://aws.amazon.com/blogs/security/enhance-programmatic-access-for-iam-users-using-yubikey-for-multi-factor-authentication/){:target="_blank" rel="noopener"}

> Organizations are increasingly providing access to corporate resources from employee laptops and are required to apply the correct permissions to these computing devices to make sure that secrets and sensitive data are adequately protected. The combination of Amazon Web Services (AWS) long-term credentials and a YubiKey security token for multi-factor authentication (MFA) is an option [...]
