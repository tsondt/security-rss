Title: Who’s Behind Monday’s 14-State 911 Outage?
Date: 2020-09-29T22:26:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;911 outage;Azure;CenturyLink;FCC;Federal Communications Commission;Intrado;microsoft;The Washington Post;West Corporation
Slug: whos-behind-mondays-14-state-911-outage

[Source](https://krebsonsecurity.com/2020/09/whos-behind-mondays-14-state-911-outage/){:target="_blank" rel="noopener"}

> Emergency 911 systems were down for more than an hour on Monday in towns and cities across 14 U.S. states. The outages led many news outlets to speculate the problem was related to Microsoft ‘s Azure web services platform, which also was struggling with a widespread outage at the time. However, multiple sources tell KrebsOnSecurity the 911 issues stemmed from some kind of technical snafu involving Intrado and Lumen, two companies that together handle 911 calls for a broad swath of the United States. Image: West.com On the afternoon of Monday, Sept. 28, several states including Arizona, California, Colorado, Delaware, [...]
