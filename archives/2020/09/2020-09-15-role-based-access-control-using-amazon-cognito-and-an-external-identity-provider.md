Title: Role-based access control using Amazon Cognito and an external identity provider
Date: 2020-09-15T19:30:02+00:00
Author: Eran Medan
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;AWS Identity and Access Management (IAM);Security, Identity, & Compliance;ADFS federation;LDAP;RBAC;Security Blog;Serverless
Slug: role-based-access-control-using-amazon-cognito-and-an-external-identity-provider

[Source](https://aws.amazon.com/blogs/security/role-based-access-control-using-amazon-cognito-and-an-external-identity-provider/){:target="_blank" rel="noopener"}

> Amazon Cognito simplifies the development process by helping you manage identities for your customer-facing applications. As your application grows, some of your enterprise customers may ask you to integrate with their own Identity Provider (IdP) so that their users can sign-on to your app using their company’s identity, and have role-based access-control (RBAC) based on [...]
