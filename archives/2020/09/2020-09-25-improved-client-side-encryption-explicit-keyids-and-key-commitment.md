Title: Improved client-side encryption: Explicit KeyIds and key commitment
Date: 2020-09-25T02:45:26+00:00
Author: Alex Tribble
Category: AWS Security
Tags: AWS Key Management Service;Expert (400);Security, Identity, & Compliance;AWS Encryption SDK;Encryption;keyrings;multi-region;Security Blog
Slug: improved-client-side-encryption-explicit-keyids-and-key-commitment

[Source](https://aws.amazon.com/blogs/security/improved-client-side-encryption-explicit-keyids-and-key-commitment/){:target="_blank" rel="noopener"}

> I’m excited to announce the launch of two new features in the AWS Encryption SDK (ESDK): local KeyId filtering and key commitment. These features each enhance security for our customers, acting as additional layers of protection for your most critical data. In this post I’ll tell you how they work. Let’s dig in. The ESDK [...]
