Title: JITSploitation I: A JIT Bug
Date: 2020-09-01T07:44:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: jitsploitation-i-a-jit-bug

[Source](https://googleprojectzero.blogspot.com/2020/09/jitsploitation-one.html){:target="_blank" rel="noopener"}

> By Samuel Gro ß, Project Zero This three-part series highlights the technical challenges involved in finding and exploiting JavaScript engine vulnerabilities in modern web browsers and evaluates current exploit mitigation technologies. The exploited vulnerability, CVE-2020-9802, was fixed in iOS 13.5, while two of the mitigation bypasses, CVE-2020-9870 and CVE-2020-9910, were fixed in iOS 13.6. ========== How might a browser renderer exploit look like in 2020? I set out to answer that question in January this year. Since it’s one of my favorite areas in computer science, I wanted to find a JIT compiler vulnerability, and I was especially interested in [...]
