Title: gVisor: Protecting GKE and serverless users in the real world
Date: 2020-09-18T17:30:00+00:00
Author: Eric Brewer
Category: GCP Security
Tags: Identity & Security;Open Source;Google Cloud Platform;Containers & Kubernetes
Slug: gvisor-protecting-gke-and-serverless-users-in-the-real-world

[Source](https://cloud.google.com/blog/products/containers-kubernetes/how-gvisor-protects-google-cloud-services-from-cve-2020-14386/){:target="_blank" rel="noopener"}

> Security is a top priority for Google Cloud, and we protect our customers through how we design our infrastructure, our services, and how we work. Googlers created some of the fundamental components of containers, like cgroups, and we were an early adopter of containers for our internal systems. We realized we needed a way to increase the security of this technology. This led to the development of gVisor, a container security sandbox that we have since open sourced and integrated into multiple Google Cloud products. When a recent Linux kernel vulnerability was disclosed, users of these products were not affected [...]
