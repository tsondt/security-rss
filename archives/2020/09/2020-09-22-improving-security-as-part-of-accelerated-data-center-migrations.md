Title: Improving security as part of accelerated data center migrations
Date: 2020-09-22T20:27:28+00:00
Author: Stephen Bowie
Category: AWS Security
Tags: AWS Managed Services;Customer Enablement;Foundational (100);Security, Identity, & Compliance;AWS Customer Enablement;Management and Governance;Migration and Transfer Services;Security Blog
Slug: improving-security-as-part-of-accelerated-data-center-migrations

[Source](https://aws.amazon.com/blogs/security/improving-security-as-part-of-accelerated-data-center-migrations/){:target="_blank" rel="noopener"}

> Approached correctly, cloud migrations are a great opportunity to improve the security and stability of your applications. Many organizations are looking for guidance on how to meet their security requirements while moving at the speed that the cloud enables. They often try to configure everything perfectly in the data center before they migrate their first [...]
