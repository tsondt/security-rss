Title: Negotiating with Ransomware Gangs
Date: 2020-09-30T11:19:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: ransomware;risk assessment
Slug: negotiating-with-ransomware-gangs

[Source](https://www.schneier.com/blog/archives/2020/09/negotiating-with-ransomware-gangs.html){:target="_blank" rel="noopener"}

> Really interesting conversation with someone who negotiates with ransomware gangs: For now, it seems that paying ransomware, while obviously risky and empowering/encouraging ransomware attackers, can perhaps be comported so as not to break any laws (like anti-terrorist laws, FCPA, conspiracy and others) ­ and even if payment is arguably unlawful, seems unlikely to be prosecuted. Thus, the decision whether to pay or ignore a ransomware demand, seems less of a legal, and more of a practical, determination ­ almost like a cost-benefit analysis. The arguments for rendering a ransomware payment include: Payment is the least costly option; Payment is in [...]
