Title: Updated IRAP reference architectures and consumer guidance for Australian public sector organizations building workloads at PROTECTED level
Date: 2020-09-29T15:33:03+00:00
Author: Michael Stringer
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;IRAP;IRAP PROTECTED;Security Blog
Slug: updated-irap-reference-architectures-and-consumer-guidance-for-australian-public-sector-organizations-building-workloads-at-protected-level

[Source](https://aws.amazon.com/blogs/security/updated-irap-reference-architectures-consumer-guidance-australian-public-sector-organizations-building-workloads-protected-level/){:target="_blank" rel="noopener"}

> In July 2020, we announced that 92 Amazon Web Services (AWS) services had successfully assessed compliant with the Australian government’s Information Security Registered Assessors Program (IRAP) for operating workloads at the PROTECTED level. This enables organizations to use AWS to build a wide range of applications and services for the benefit of all residents of [...]
