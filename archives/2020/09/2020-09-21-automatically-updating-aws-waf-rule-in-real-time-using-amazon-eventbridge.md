Title: Automatically updating AWS WAF Rule in real time using Amazon EventBridge
Date: 2020-09-21T19:28:02+00:00
Author: Adam Cerini
Category: AWS Security
Tags: Amazon EventBridge;AWS WAF;Intermediate (200);Kinesis Data Analytics;Security, Identity, & Compliance;Amazon Kinesis Data Firehose;EventBridge;kinesis analytics;Security Blog;web application firewall
Slug: automatically-updating-aws-waf-rule-in-real-time-using-amazon-eventbridge

[Source](https://aws.amazon.com/blogs/security/automatically-updating-aws-waf-rule-in-real-time-using-amazon-eventbridge/){:target="_blank" rel="noopener"}

> In this post, I demonstrate a method for collecting and sharing threat intelligence between Amazon Web Services (AWS) accounts by using AWS WAF, Amazon Kinesis Data Analytics, and Amazon EventBridge. AWS WAF helps protect against common web exploits and gives you control over which traffic can reach your application. Attempted exploitation blocked by AWS WAF [...]
