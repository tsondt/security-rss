Title: Expanding Google Cloud’s Confidential Computing portfolio
Date: 2020-09-08T12:00:00+00:00
Author: Eyal Manor
Category: GCP Security
Tags: Google Cloud Platform;Next;Containers & Kubernetes;GKE;Identity & Security
Slug: expanding-google-clouds-confidential-computing-portfolio

[Source](https://cloud.google.com/blog/products/identity-security/expanding-google-clouds-confidential-computing-portfolio/){:target="_blank" rel="noopener"}

> However you use Google Cloud services, your data is your data. Our layered approach to security proactively protects your data and gives you control on your terms. In fact, at Google we believe the future of computing will increasingly shift to private, encrypted services where users can be confident that their data is not being exposed to cloud providers or their own insiders. Confidential Computing makes this future possible by keeping data encrypted in memory, and elsewhere outside the CPU, while it is being processed. In July, on the opening day of Google Cloud Next ‘20: OnAir, we announced the [...]
