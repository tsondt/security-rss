Title: FYI: If you're running HP Device Manager, anyone on your network can get admin on your server via backdoor
Date: 2020-09-30T08:32:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: fyi-if-youre-running-hp-device-manager-anyone-on-your-network-can-get-admin-on-your-server-via-backdoor

[Source](https://go.theregister.com/feed/www.theregister.com/2020/09/30/hp_device_manager_backdoor_database_account/){:target="_blank" rel="noopener"}

> Hidden database account discovered, patches finally available as well as mitigations HP Device Manager, software that allows IT administrators to manage HP Thin Client devices, comes with a backdoor database user account that undermines network security, a UK-based consultant has warned.... [...]
