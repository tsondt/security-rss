Title: Lost in translation: encryption, key management, and real security
Date: 2020-09-11T16:00:00+00:00
Author: Honna Segel
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: lost-in-translation-encryption-key-management-and-real-security

[Source](https://cloud.google.com/blog/products/identity-security/how-encryption-and-key-management-enable-real-security/){:target="_blank" rel="noopener"}

> Being compliant does not mean you’re secure. The definition of “being secure” varies too much across industries, organizations, and threat profiles—secure against what?—to ever match an external checklist. Because of this, compliance and security have developed a peculiar relationship. Regulation is meant to provide guidance, through accepted best practices, from the body of knowledge for a given domain. But, because laws and regulations tend to evolve more slowly than technology, compliance regimes don’t foresee many new technologies. This applies to both technologies to secure and tools to secure them with. Many security professionals quote the “compliance is not security” cliche, [...]
