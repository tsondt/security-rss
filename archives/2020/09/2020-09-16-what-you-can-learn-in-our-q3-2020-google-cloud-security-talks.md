Title: What you can learn in our Q3 2020 Google Cloud Security Talks
Date: 2020-09-16T16:00:00+00:00
Author: Rob Sadowski
Category: GCP Security
Tags: Google Cloud Platform;Google Workspace;Events;Identity & Security
Slug: what-you-can-learn-in-our-q3-2020-google-cloud-security-talks

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-security-talks-the-latest-in-cloud-security/){:target="_blank" rel="noopener"}

> Cloud deployments and technologies have become an even more central part of organizations’ security program in today’s new normal. As you continue to evolve your strategies and operations, it’s vital to understand the resources at your disposal to protect your users, applications, and data. To help you navigate the latest thinking in cloud security, we hope you’ll join us for the latest installment of our Google Cloud Security Talks, a live online event on September 23rd. We’ll share expert insights into our security ecosystem and cover the following topics Sunil Potti and Rob Sadowski will open the digital event with [...]
