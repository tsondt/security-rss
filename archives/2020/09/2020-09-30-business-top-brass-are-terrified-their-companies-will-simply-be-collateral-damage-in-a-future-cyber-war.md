Title: Business top brass are terrified their companies will simply be collateral damage in a future cyber-war
Date: 2020-09-30T21:11:21+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: business-top-brass-are-terrified-their-companies-will-simply-be-collateral-damage-in-a-future-cyber-war

[Source](https://go.theregister.com/feed/www.theregister.com/2020/09/30/cyber_war_fears/){:target="_blank" rel="noopener"}

> Organizations need not fear a direct hit – someone knackering the internet or the grid would be enough Businesses are worrying about being caught in the crossfire of cyber warfare, according to research from Bitdefender – while industry figures warn that the gap between common-or-garden cyber threats and “oh, look what nation states are doing” is becoming ever smaller.... [...]
