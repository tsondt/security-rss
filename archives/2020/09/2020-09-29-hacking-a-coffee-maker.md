Title: Hacking a Coffee Maker
Date: 2020-09-29T11:16:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: hacking;Internet of Things;reverse engineering;vulnerabilities
Slug: hacking-a-coffee-maker

[Source](https://www.schneier.com/blog/archives/2020/09/hacking-a-coffee-maker.html){:target="_blank" rel="noopener"}

> As expected, IoT devices are filled with vulnerabilities : As a thought experiment, Martin Hron, a researcher at security company Avast, reverse engineered one of the older coffee makers to see what kinds of hacks he could do with it. After just a week of effort, the unqualified answer was: quite a lot. Specifically, he could trigger the coffee maker to turn on the burner, dispense water, spin the bean grinder, and display a ransom message, all while beeping repeatedly. Oh, and by the way, the only way to stop the chaos was to unplug the power cord. [...] In [...]
