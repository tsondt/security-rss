Title: JITSploitation III: Subverting Control Flow
Date: 2020-09-01T06:33:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: jitsploitation-iii-subverting-control-flow

[Source](https://googleprojectzero.blogspot.com/2020/09/jitsploitation-three.html){:target="_blank" rel="noopener"}

> Posted by Samuel Groß, Project Zero This three-part series highlights the technical challenges involved in finding and exploiting JavaScript engine vulnerabilities in modern web browsers and evaluates current exploit mitigation technologies. The exploited vulnerability, CVE-2020-9802, was fixed in iOS 13.5, while two of the mitigation bypasses, CVE-2020-9870 and CVE-2020-9910, were fixed in iOS 13.6. ========== This post is third in a series about a Safari renderer exploit. Part 1 discussed a JIT compiler vulnerability in JSC and Part 2 showed how it could be turned into a reliable read/write primitive despite various mitigations. The purpose of this post is to [...]
