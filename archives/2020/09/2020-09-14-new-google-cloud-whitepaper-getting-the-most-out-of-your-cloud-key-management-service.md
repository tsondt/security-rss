Title: New Google Cloud whitepaper: Getting the most out of your Cloud Key Management Service
Date: 2020-09-14T16:00:00+00:00
Author: Honna Segel
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: new-google-cloud-whitepaper-getting-the-most-out-of-your-cloud-key-management-service

[Source](https://cloud.google.com/blog/products/identity-security/new-google-cloud-whitepaper-cloud-key-management-service/){:target="_blank" rel="noopener"}

> The cliché that “encryption is easy, but key management is hard,” remains true: encryption key management is still a challenge for many large organizations. Add in cloud migration of many sensitive workloads, which necessitates encryption, and the challenges have become more acute. Cloud, however, also holds the potential for making encryption key management more performant, secure, and compliant—and even easier to manage. Done right, cloud-based key management can improve trust in cloud computing. However, it will only achieve these goals if it’s done transparently. Google Cloud Security recently published a whitepaper titled “Cloud Key Management Service Deep Dive”, to help [...]
