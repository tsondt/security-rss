Title: Who watches the watchers? Samsung does so it can fling ads at owners of its smart TVs
Date: 2020-09-30T16:15:12+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: who-watches-the-watchers-samsung-does-so-it-can-fling-ads-at-owners-of-its-smart-tvs

[Source](https://go.theregister.com/feed/www.theregister.com/2020/09/30/samsung_smart_tv_ads/){:target="_blank" rel="noopener"}

> Customers forgot they are the product when splashing out on a nice telly Samsung brags to advertisers that "first screen ads", seen by all users of its Smart TVs when they turn on, are 100 per cent viewable, audience targeted, and seen 400 times per TV per month. Some users are not happy.... [...]
