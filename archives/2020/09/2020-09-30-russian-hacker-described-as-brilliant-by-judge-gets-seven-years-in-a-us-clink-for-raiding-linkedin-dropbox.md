Title: Russian hacker, described as 'brilliant' by judge, gets seven years in a US clink for raiding LinkedIn, Dropbox
Date: 2020-09-30T20:15:27+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: russian-hacker-described-as-brilliant-by-judge-gets-seven-years-in-a-us-clink-for-raiding-linkedin-dropbox

[Source](https://go.theregister.com/feed/www.theregister.com/2020/09/30/linkedin_hacker_prison/){:target="_blank" rel="noopener"}

> Yevgeniy Nikulin, grabbed in Prague, unlikely to see his mother alive again after swiping 200m+ user records A Russian scumbag found guilty of hacking into LinkedIn, Dropbox, and Formspring – and stealing data on over 200 million users – has been sent down for more than seven years.... [...]
