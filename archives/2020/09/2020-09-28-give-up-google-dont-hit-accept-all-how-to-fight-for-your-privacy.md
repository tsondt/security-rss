Title: Give up Google, don't hit 'accept all': how to fight for your privacy
Date: 2020-09-28T15:44:43+00:00
Author: Elle Hunt
Category: The Guardian
Tags: Books;Data and computer security;Culture;Technology;Surveillance;World news;Artificial intelligence (AI);Computing
Slug: give-up-google-dont-hit-accept-all-how-to-fight-for-your-privacy

[Source](https://www.theguardian.com/books/2020/sep/28/carissa-veliz-intrusion-privacy-is-power-data){:target="_blank" rel="noopener"}

> In Privacy Is Power, professor Carissa Véliz has made a shocking survey of how much intimate data we are surrendering. But she has a plan to fight back “If you’re reading this book, you probably already know your personal data is being collected, stored and analysed,” Carissa Véliz begins, in Privacy Is Power. Her challenge, as a writer and a privacy advocate, is to shake us out of our complacency; to persuade us to see this not as a necessary sacrifice in the digital age, but an intolerable invasion. From the mounting dread I felt while reading Privacy Is Power, [...]
