Title: Two Russians Charged in $17M Cryptocurrency Phishing Spree
Date: 2020-09-16T20:53:17+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Binance;Danil Potekhin;Dmitirii Karasavidi;Ethereum;Gemini;Poloniex;U.S. Justice Department;U.S. Treasury Department
Slug: two-russians-charged-in-17m-cryptocurrency-phishing-spree

[Source](https://krebsonsecurity.com/2020/09/two-russians-charged-in-17m-cryptocurrency-phishing-spree/){:target="_blank" rel="noopener"}

> U.S. authorities today announced criminal charges and financial sanctions against two Russian men accused of stealing nearly $17 million worth of virtual currencies in a series of phishing attacks throughout 2017 and 2018 that spoofed websites for some of the most popular cryptocurrency exchanges. The Justice Department unsealed indictments against Russian nationals Danil Potekhin and Dmitirii Karasavidi, alleging the duo was responsible for a sophisticated phishing and money laundering campaign that resulted in the theft of $16.8 million in cryptocurrencies and fiat money from victims. Separately, the U.S. Treasury Department announced economic sanctions against Potekhin and Karasavidi, effectively freezing all [...]
