Title: Integrating AWS CloudFormation security tests with AWS Security Hub and AWS CodeBuild reports
Date: 2020-09-14T19:59:38+00:00
Author: Vesselin Tzvetkov
Category: AWS Security
Tags: Advanced (300);AWS CloudFormation;AWS CodeBuild;AWS Security Hub;Security, Identity, & Compliance;CFN_NAG;Code Build Test Report;Security Blog;security code scan;vulnerability code scan
Slug: integrating-aws-cloudformation-security-tests-with-aws-security-hub-and-aws-codebuild-reports

[Source](https://aws.amazon.com/blogs/security/integrating-aws-cloudformation-security-tests-with-aws-security-hub-and-aws-codebuild-reports/){:target="_blank" rel="noopener"}

> The concept of infrastructure as code, by using pipelines for continuous integration and delivery, is fundamental for the development of cloud infrastructure. Including code quality and vulnerability scans in the pipeline is essential for the security of this infrastructure as code. In one of our previous posts, How to build a CI/CD pipeline for container [...]
