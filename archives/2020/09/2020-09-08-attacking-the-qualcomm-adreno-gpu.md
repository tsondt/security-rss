Title: Attacking the Qualcomm Adreno GPU
Date: 2020-09-08T09:06:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: attacking-the-qualcomm-adreno-gpu

[Source](https://googleprojectzero.blogspot.com/2020/09/attacking-qualcomm-adreno-gpu.html){:target="_blank" rel="noopener"}

> Posted by Ben Hawkes, Project Zero When writing an Android exploit, breaking out of the application sandbox is often a key step. There are a wide range of remote attacks that give you code execution with the privileges of an application (like the browser or a messaging application), but a sandbox escape is still required to gain full system access. This blog post focuses on an interesting attack surface that is accessible from the Android application sandbox: the graphics processing unit (GPU) hardware. We describe an unusual vulnerability in Qualcomm's Adreno GPU, and how it could be used to achieve [...]
