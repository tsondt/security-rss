Title: On-Demand SCIM provisioning of Azure AD to AWS SSO with PowerShell
Date: 2020-09-22T15:28:44+00:00
Author: Aidan Keane
Category: AWS Security
Tags: Advanced (300);AWS Single Sign-On (SSO);Security, Identity, & Compliance;AWS SSO;Azure Active Directory;Management and Governance;Security Blog
Slug: on-demand-scim-provisioning-of-azure-ad-to-aws-sso-with-powershell

[Source](https://aws.amazon.com/blogs/security/on-demand-scim-provisioning-of-azure-ad-to-aws-sso-with-powershell/){:target="_blank" rel="noopener"}

> In this post, I will demonstrate how you can use a PowerShell script to initiate an on-demand synchronization between Azure Active Directory and AWS Single Sign-On (AWS SSO) and avoid the default 40-minute synchronization schedule between both identity providers. This solution helps enterprises quickly synchronize changes made to users, groups, or permissions within Azure AD [...]
