Title: Security-at-scale: 10 new security and management controls
Date: 2020-09-29T16:00:00+00:00
Author: Swati Sharma
Category: GCP Security
Tags: Google Workspace;Google Cloud Platform;Identity & Security
Slug: security-at-scale-10-new-security-and-management-controls

[Source](https://cloud.google.com/blog/products/identity-security/10-new-security-and-management-controls/){:target="_blank" rel="noopener"}

> With so many people working remotely, it's imperative that the tools we use to stay productive are secure. Already this year we have worked to strengthen security for our customers and help make threat defense more effective. Today, we’re sharing new advancements that provide robust controls, enable automation, and simplify managing security-at-scale with both G Suite and Google Cloud Platform. Enabling extensibility through APIs First, let’s take a look at enabling extensibility with APIs with the general availability of both Cloud Identity groups API and service account API access for Cloud Identity customers. We recognize that some Google Cloud customers [...]
