Title: Get ready for upcoming changes in the AWS Single Sign-On user sign-in process
Date: 2020-09-18T17:38:13+00:00
Author: Yuri Duchovny
Category: AWS Security
Tags: AWS Single Sign-On (SSO);Foundational (100);Security, Identity, & Compliance;AWS SSO;Security Blog;sign-in
Slug: get-ready-for-upcoming-changes-in-the-aws-single-sign-on-user-sign-in-process

[Source](https://aws.amazon.com/blogs/security/get-ready-upcoming-changes-aws-single-sign-on-user-sign-in-process/){:target="_blank" rel="noopener"}

> To improve security, enhance user experience, and address compatibility with future AWS Identity changes, AWS Single Sign-On (SSO) is making changes to the sign-in process that will affect some AWS SSO customers. The changes will go into effect globally in early October 2020. The AWS SSO sign-in pages are moving to a new top-level DNS [...]
