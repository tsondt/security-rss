Title: How to configure an LDAPS endpoint for Simple AD
Date: 2020-09-09T17:35:50+00:00
Author: Marco Sommella
Category: AWS Security
Tags: Amazon EC2;Amazon Route 53;AWS Directory Service;Intermediate (200);Security, Identity, & Compliance;Encryption;LDAPS;NLB;Security Blog;Simple AD
Slug: how-to-configure-an-ldaps-endpoint-for-simple-ad

[Source](https://aws.amazon.com/blogs/security/how-to-configure-ldaps-endpoint-for-simple-ad/){:target="_blank" rel="noopener"}

> In this blog post, we show you how to configure an LDAPS (LDAP over SSL or TLS) encrypted endpoint for Simple AD so that you can extend Simple AD over untrusted networks. Our solution uses Network Load Balancer (NLB) as SSL/TLS termination. The data is then decrypted and sent to Simple AD. Network Load Balancer [...]
