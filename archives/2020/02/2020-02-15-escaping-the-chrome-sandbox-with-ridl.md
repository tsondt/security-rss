Title: Escaping the Chrome Sandbox with RIDL
Date: 2020-02-15T09:02:00-08:00
Author: Ben (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: escaping-the-chrome-sandbox-with-ridl

[Source](https://googleprojectzero.blogspot.com/2020/02/escaping-chrome-sandbox-with-ridl.html){:target="_blank" rel="noopener"}

> Guest blog post by Stephen Röttger tl;dr: Vulnerabilities that leak cross process memory can be exploited to escape the Chrome sandbox. An attacker is still required to compromise the renderer prior to mounting this attack. To protect against attacks on affected CPUs make sure your microcode is up to date and disable hyper-threading (HT). In my last guest blog post “Trashing the Flow of Data” I described how to exploit a bug in Chrome’s JavaScript engine V8 to gain code execution in the renderer. For such an exploit to be useful, you will usually need to chain it with a [...]
