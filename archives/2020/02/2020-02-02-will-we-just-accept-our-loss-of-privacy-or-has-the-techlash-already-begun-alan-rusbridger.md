Title: Will we just accept our loss of privacy, or has the techlash already begun? | Alan Rusbridger
Date: 2020-02-02T08:15:37+00:00
Author: Alan Rusbridger
Category: The Guardian
Tags: Privacy;Data and computer security;Technology;Data protection;Google;Facial recognition;Hacking;Social networking;Surveillance;World news;Media
Slug: will-we-just-accept-our-loss-of-privacy-or-has-the-techlash-already-begun-alan-rusbridger

[Source](https://www.theguardian.com/commentisfree/2020/feb/02/will-we-just-accept-our-loss-of-privacy-or-has-the-techlash-already-begun){:target="_blank" rel="noopener"}

> Not so long ago we searched Google. Now we seem quite happy to let Google search us Probably too late to ask, but was the past year the moment we lost our technological innocence? The Alexa in the corner of the kitchen monitoring your every word? The location-betraying device in your pocket? The dozen trackers on that web page you just opened? The thought that a 5G network could, in some hazily understood way, be hardwired back to Beijing ? The spooky use of live facial recognition on CCTV cameras across London. With privacy there have been so many landmarks [...]
