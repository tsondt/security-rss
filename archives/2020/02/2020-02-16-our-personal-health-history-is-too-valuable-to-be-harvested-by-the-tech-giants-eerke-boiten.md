Title: Our personal health history is too valuable to be harvested by the tech giants | Eerke Boiten
Date: 2020-02-16T07:15:22+00:00
Author: Eerke Boiten
Category: The Guardian
Tags: Data and computer security;Technology;Society;NHS;Cybercrime;Google;DeepMind;Internet;Health
Slug: our-personal-health-history-is-too-valuable-to-be-harvested-by-the-tech-giants-eerke-boiten

[Source](https://www.theguardian.com/commentisfree/2020/feb/16/our-personal-health-history-is-too-valuable-to-be-harvested-by-tech-giants){:target="_blank" rel="noopener"}

> Action to prevent deeper access to our private lives and data is more essential than ever Health data paints a rich picture of our lives. Even if you remove your name, date of birth and NHS number to “anonymise” yourself, a full health history will reveal your age, gender, the places where you have lived, your family relationships and aspects of your lifestyle. Used in combination with other available information, this may be enough to verify that this medical history relates to you personally and to target you online. Consequently, whenever the NHS shares health data, even if it is [...]
