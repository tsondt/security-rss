Title: Contacts of 1m Virgin Media customers left on unsecured database
Date: 2020-03-05T20:29:31+00:00
Author: Alex Hern
Category: The Guardian
Tags: Virgin Media;Media;Media business;UK news;Information commissioner;Data and computer security;Technology
Slug: contacts-of-1m-virgin-media-customers-left-on-unsecured-database

[Source](https://www.theguardian.com/media/2020/mar/05/contacts-of-1m-virgin-media-customers-left-on-unsecured-database){:target="_blank" rel="noopener"}

> At least one person from outside Virgin Media accessed non-financial details Almost a million Virgin Media customers had their personal details stored on a marketing database that had been left unsecured since last April, the company has admitted. Records show that the database has been accessed by at least one person from outside the company, Virgin Media said, although it does not yet have any evidence that the information has been used illegally. Continue reading... [...]
