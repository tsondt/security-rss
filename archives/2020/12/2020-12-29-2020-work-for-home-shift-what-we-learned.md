Title: 2020 Work-for-Home Shift: What We Learned
Date: 2020-12-29T13:00:39+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Hacks;Mobile Security;Vulnerabilities;Web Security;cloud;collaboration;Cybersecurity;lessons learned;mobile;Phishing;remote working;takeaways;work from home;zero-trust framework
Slug: 2020-work-for-home-shift-what-we-learned

[Source](https://threatpost.com/2020-work-for-home-shift-learned/162595/){:target="_blank" rel="noopener"}

> Threatpost explores 5 big takeaways from 2020 -- and what they mean for 2021. [...]
