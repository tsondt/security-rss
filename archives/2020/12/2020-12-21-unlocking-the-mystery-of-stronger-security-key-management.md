Title: Unlocking the mystery of stronger security key management
Date: 2020-12-21T20:00:00+00:00
Author: Honna Segel
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: unlocking-the-mystery-of-stronger-security-key-management

[Source](https://cloud.google.com/blog/products/identity-security/better-encrypt-your-security-keys-in-google-cloud/){:target="_blank" rel="noopener"}

> One of the “classic” data security mistakes involving encryption is encrypting the data and failing to secure the encryption key. To make matters worse, a sadly common issue is leaving the key “close” to data, such as in the same database or on the same system as the encrypted files. Such practices reportedly were a contributing factor for some prominent data breaches. Sometimes, an investigation revealed that encryption was implemented for compliance and without clear threat model thinking —key management was an afterthought or not even considered. One could argue that the key must be better protected than the data [...]
