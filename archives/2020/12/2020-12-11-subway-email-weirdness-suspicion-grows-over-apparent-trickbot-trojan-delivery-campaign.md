Title: Subway email weirdness: Suspicion grows over apparent Trickbot trojan delivery campaign
Date: 2020-12-11T14:15:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: subway-email-weirdness-suspicion-grows-over-apparent-trickbot-trojan-delivery-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/11/subway_email_oddity_trickbot_links/){:target="_blank" rel="noopener"}

> If you got an unexpected message from the not-footlong guys, don't click links Updated Subway patrons in the UK received suspicious emails this morning and infosec researchers fear this is linked to the theft of customer details – and a Trickbot malware campaign.... [...]
