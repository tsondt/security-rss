Title: Microsoft December 2020 Patch Tuesday fixes 58 vulnerabilities
Date: 2020-12-08T13:37:31
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-december-2020-patch-tuesday-fixes-58-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/microsoft-december-2020-patch-tuesday-fixes-58-vulnerabilities/){:target="_blank" rel="noopener"}

> Today is Microsoft's December 2020 Patch Tuesday, and Windows administrators will be scrambling to put out fires, so be kind to them. As part of this Patch Tuesday, Microsoft fixed 58 security vulnerabilities and release a DNS cache poisoning vulnerability advisory. [...]
