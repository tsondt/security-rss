Title: IRS to Make ID Protection PIN Open to All
Date: 2020-12-04T14:50:05+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Tax Refund Fraud;Identity Protection Personal Identification Number;Internal Revenue Service;IP PIN;irs;tax refund fraud
Slug: irs-to-make-id-protection-pin-open-to-all

[Source](https://krebsonsecurity.com/2020/12/irs-to-make-id-protection-pin-open-to-all/){:target="_blank" rel="noopener"}

> The U.S. Internal Revenue Service (IRS) said this week that beginning in 2021 it will allow all taxpayers to apply for an identity protection personal identification number (IP PIN), a single-use code designed to block identity thieves from falsely claiming a tax refund in your name. Currently, IP PINs are issued only to those who fill out an ID theft affidavit, or to taxpayers who’ve experienced tax refund fraud in previous years. Tax refund fraud is a perennial problem involving the use of identity information and often stolen or misdirected W-2 forms to electronically file an unauthorized tax return for [...]
