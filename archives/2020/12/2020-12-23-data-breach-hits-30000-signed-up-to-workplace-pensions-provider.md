Title: Data breach hits 30,000 signed up to workplace pensions provider
Date: 2020-12-23T18:40:41+00:00
Author: Miles Brignall
Category: The Guardian
Tags: Data and computer security;Pensions industry;Crime;Scams;Internet safety;Information commissioner;Information;Technology;Business;UK news
Slug: data-breach-hits-30000-signed-up-to-workplace-pensions-provider

[Source](https://www.theguardian.com/technology/2020/dec/23/data-breach-hits-30000-signed-up-to-workplace-pensions-provider){:target="_blank" rel="noopener"}

> Fraud worries as UK company Now:Pensions says ‘third-party contractor’ posted personal details of clients to online public forum About 30,000 customers of Now:Pensions face an anxious Christmas after a serious data breach at the pensions provider led to their sensitive personal details being posted on the internet. In an email sent to affected customers, the workplace pensions firm warned that names, postal and email addresses, birth dates and National Insurance numbers all appeared in a public forum online. Continue reading... [...]
