Title: US govt, FireEye breached after SolarWinds supply-chain attack
Date: 2020-12-14T10:04:46
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-govt-fireeye-breached-after-solarwinds-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/us-govt-fireeye-breached-after-solarwinds-supply-chain-attack/){:target="_blank" rel="noopener"}

> Trojanized versions of SolarWinds' Orion IT monitoring and management software have been used in a supply chain attack leading to the breach of government and high-profile companies after attackers deployed a backdoor dubbed SUNBURST or Solorigate. [...]
