Title: CISA releases Azure, Microsoft 365 malicious activity detection tool
Date: 2020-12-28T12:48:46
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisa-releases-azure-microsoft-365-malicious-activity-detection-tool

[Source](https://www.bleepingcomputer.com/news/security/cisa-releases-azure-microsoft-365-malicious-activity-detection-tool/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has released a PowerShell-based tool that helps detect potentially compromised applications and accounts in Azure/Microsoft 365 environments. [...]
