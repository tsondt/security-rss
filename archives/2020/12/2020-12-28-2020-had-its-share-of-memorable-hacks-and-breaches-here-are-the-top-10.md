Title: 2020 had its share of memorable hacks and breaches. Here are the top 10
Date: 2020-12-28T12:46:14+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;breaches;espionage;exploits;hacking;ransomware;vulnerabilities
Slug: 2020-had-its-share-of-memorable-hacks-and-breaches-here-are-the-top-10

[Source](https://arstechnica.com/?p=1731509){:target="_blank" rel="noopener"}

> Enlarge (credit: Traitov | Getty Images ) 2020 was a tough year for a lot of reasons, not least of which were breaches and hacks that visited pain on end users, customers, and the organizations that were targeted. The ransomware menace dominated headlines, with an endless stream of compromises hitting schools, governments, and private companies as criminals demanded ransoms in the millions of dollars. There was a steady stream of data breaches as well. Several mass account takeovers made appearances, too. What follows are some of the highlights. For good measure, we’re also throwing in a couple notable hacks that, [...]
