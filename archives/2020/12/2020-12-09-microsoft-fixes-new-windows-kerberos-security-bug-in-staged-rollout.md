Title: Microsoft fixes new Windows Kerberos security bug in staged rollout
Date: 2020-12-09T08:25:38
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-fixes-new-windows-kerberos-security-bug-in-staged-rollout

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-new-windows-kerberos-security-bug-in-staged-rollout/){:target="_blank" rel="noopener"}

> Microsoft has issued security updates to address a Kerberos security feature bypass vulnerability impacting multiple Windows Server versions in a two-phase staged rollout. [...]
