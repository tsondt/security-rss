Title: European Medicines Agency cyber-attack: Coronavirus vaccine documents ‘unlawfully accessed’, claim Pfizer and BioNTech
Date: 2020-12-10T12:26:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: european-medicines-agency-cyber-attack-coronavirus-vaccine-documents-unlawfully-accessed-claim-pfizer-and-biontech

[Source](https://portswigger.net/daily-swig/european-medicines-agency-cyber-attack-coronavirus-vaccine-documents-unlawfully-accessed-claim-pfizer-and-biontech){:target="_blank" rel="noopener"}

> EU healthcare body assures pharma giants that attack ‘will have no impact’ on vaccine authorization timeline [...]
