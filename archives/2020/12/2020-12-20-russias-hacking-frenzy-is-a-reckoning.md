Title: Russia’s hacking frenzy is a reckoning
Date: 2020-12-20T11:50:01+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Fancy Bear;hacking;russia;solarwinds
Slug: russias-hacking-frenzy-is-a-reckoning

[Source](https://arstechnica.com/?p=1730859){:target="_blank" rel="noopener"}

> Enlarge / The attack hit multiple US agencies—and a full assessment of the damage may still be months away. (credit: Andrew Harrer | Bloomberg | Getty Images ) Last week, several major United States government agencies—including the Departments of Homeland Security, Commerce, Treasury, and State—discovered that their digital systems had been breached by Russian hackers in a months-long espionage operation. The breadth and depth of the attacks will take months, if not longer, to fully understand. But it's already clear that they represent a moment of reckoning, both for the federal government and the IT industry that supplies it. As [...]
