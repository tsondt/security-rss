Title: Holiday Puppy Swindle Has Consumers Howling
Date: 2020-12-22T20:32:01+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security;Bitcoin;consumer fraud;fake online seller;Fraud;holiday shooping scam;online fraud;puppy for bitcoin;puppy fraud;puppy scam;scam online sellers
Slug: holiday-puppy-swindle-has-consumers-howling

[Source](https://threatpost.com/holiday-puppy-swindle-consumers-howling/162565/){:target="_blank" rel="noopener"}

> Those buying German Shepherd puppies for Bitcoin online are in for a ruff ride. [...]
