Title: FireEye Cyberattack Compromises Red-Team Security Tools
Date: 2020-12-08T22:08:41+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;cyberattack;Cybersecurity;FireEye;hack;red team tool;state sponsored attack;zero day
Slug: fireeye-cyberattack-compromises-red-team-security-tools

[Source](https://threatpost.com/fireeye-cyberattack-red-team-security-tools/162056/){:target="_blank" rel="noopener"}

> An attacker stole FireEye's Red Team assessment tools that the company uses to test its customers’ security. [...]
