Title: Microsoft Revamps ‘Invasive’ M365 Feature After Privacy Backlash
Date: 2020-12-02T15:44:59+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Privacy;Web Security;Data Privacy;Microsoft;Microsoft 365;microsoft teams;Office 365;Outlook;productivity score;skype;workplace surveillance
Slug: microsoft-revamps-invasive-m365-feature-after-privacy-backlash

[Source](https://threatpost.com/microsoft-m365-privacy-backlash/161760/){:target="_blank" rel="noopener"}

> The Microsoft 365 tool that tracked employee usage of applications like Outlook, Skype and Teams was widely condemned by privacy experts. [...]
