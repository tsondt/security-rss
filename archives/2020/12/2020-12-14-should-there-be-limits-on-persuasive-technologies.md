Title: Should There Be Limits on Persuasive Technologies?
Date: 2020-12-14T20:03:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: economics of security;essays;marketing;national security policy;propaganda
Slug: should-there-be-limits-on-persuasive-technologies

[Source](https://www.schneier.com/blog/archives/2020/12/should-there-be-limits-on-persuasive-technologies.html){:target="_blank" rel="noopener"}

> Persuasion is as old as our species. Both democracy and the market economy depend on it. Politicians persuade citizens to vote for them, or to support different policy positions. Businesses persuade consumers to buy their products or services. We all persuade our friends to accept our choice of restaurant, movie, and so on. It’s essential to society; we couldn’t get large groups of people to work together without it. But as with many things, technology is fundamentally changing the nature of persuasion. And society needs to adapt its rules of persuasion or suffer the consequences. Democratic societies, in particular, are [...]
