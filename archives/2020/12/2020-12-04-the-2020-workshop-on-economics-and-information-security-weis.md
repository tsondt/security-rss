Title: The 2020 Workshop on Economics and Information Security (WEIS)
Date: 2020-12-04T20:21:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: conferences;economics of security
Slug: the-2020-workshop-on-economics-and-information-security-weis

[Source](https://www.schneier.com/blog/archives/2020/12/the-2020-workshop-on-economics-and-information-security-weis.html){:target="_blank" rel="noopener"}

> The workshop on Economics and Information Security is always an interesting conference. This year, it will be online. Here’s the program. Registration is free. [...]
