Title: North Korean state hackers breach COVID-19 research entities
Date: 2020-12-24T12:00:11
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: north-korean-state-hackers-breach-covid-19-research-entities

[Source](https://www.bleepingcomputer.com/news/security/north-korean-state-hackers-breach-covid-19-research-entities/){:target="_blank" rel="noopener"}

> North Korean nation-state hackers tracked as the Lazarus Group have recently compromised organizations involved in COVID-19 research and vaccine development. [...]
