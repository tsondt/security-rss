Title: Malicious Chrome, Edge extensions with 3M installs still in stores
Date: 2020-12-16T17:04:01
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Microsoft
Slug: malicious-chrome-edge-extensions-with-3m-installs-still-in-stores

[Source](https://www.bleepingcomputer.com/news/security/malicious-chrome-edge-extensions-with-3m-installs-still-in-stores/){:target="_blank" rel="noopener"}

> Malicious Chrome and Edge browser extensions with over 3 million installs, most of them still available on the Chrome Web Store and the Microsoft Edge Add-ons portal, are capable of stealing users' info and redirecting them to phishing sites. [...]
