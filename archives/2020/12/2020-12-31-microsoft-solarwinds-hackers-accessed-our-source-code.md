Title: Microsoft: SolarWinds hackers accessed our source code
Date: 2020-12-31T14:52:04
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-solarwinds-hackers-accessed-our-source-code

[Source](https://www.bleepingcomputer.com/news/security/microsoft-solarwinds-hackers-accessed-our-source-code/){:target="_blank" rel="noopener"}

> The threat actors behind the SolarWinds attack could breach internal Microsoft accounts to view the source code for Microsoft products. [...]
