Title: 3M Users Targeted by Malicious Facebook, Insta Browser Add-Ons
Date: 2020-12-17T17:03:48+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Facebook;Web Security;Avast Threat Intelligence;browsers;chrome;Edge;extensions;google;Instagram;malicious scripts;malware;Microsoft;Vimeo
Slug: 3m-users-targeted-by-malicious-facebook-insta-browser-add-ons

[Source](https://threatpost.com/3m-users-malicious-facebook-insta-browser-add-ons/162350/){:target="_blank" rel="noopener"}

> Researchers identify malware existing in popular add-ons for Facebook, Vimeo, Instagram and others that are commonly used in browsers from Google and Microsoft. [...]
