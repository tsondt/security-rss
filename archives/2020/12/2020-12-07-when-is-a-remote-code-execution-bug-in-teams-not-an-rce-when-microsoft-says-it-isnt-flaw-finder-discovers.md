Title: When is a remote-code-execution bug in Teams not an RCE? When Microsoft says it isn't, flaw finder discovers
Date: 2020-12-07T21:58:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: when-is-a-remote-code-execution-bug-in-teams-not-an-rce-when-microsoft-says-it-isnt-flaw-finder-discovers

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/07/microsoft_teams_rce_flaw/){:target="_blank" rel="noopener"}

> 'Zero-click, wormable, cross-platform' vuln deemed 'important, spoofing' rather than, say, 'aaargh!' Updated At some point since August, Microsoft quietly fixed a cross-site scripting (XSS) bug in its Teams web app that opened the door to a serious remote-code-execution (RCE) vulnerability in the Linux, macOS, and Windows desktop versions of its Teams collaboration app.... [...]
