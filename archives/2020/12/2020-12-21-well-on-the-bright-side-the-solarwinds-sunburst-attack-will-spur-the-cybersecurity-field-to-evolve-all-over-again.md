Title: Well, on the bright side, the SolarWinds Sunburst attack will spur the cybersecurity field to evolve all over again
Date: 2020-12-21T09:30:36+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: well-on-the-bright-side-the-solarwinds-sunburst-attack-will-spur-the-cybersecurity-field-to-evolve-all-over-again

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/21/solarwinds_sunburst_evolve/){:target="_blank" rel="noopener"}

> We have to be smarter than the baddies and expect the unexpected Column One of the great threats to our civilization is space weather. Specifically, the Sun's proven ability to target the planet with a tremendous cosmic belch of radiation, knocking out satellites, power grids, and networks worldwide.... [...]
