Title: Subway Sandwich Loyalty-Card Users Suffer Ham-Handed Phishing Scam
Date: 2020-12-15T21:43:58+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Web Security;loyalty card programs;malware;phishing scam;subway;subway loyalty card;subway sandwiches
Slug: subway-sandwich-loyalty-card-users-suffer-ham-handed-phishing-scam

[Source](https://threatpost.com/subway-loyalty-card-phishing-scam/162308/){:target="_blank" rel="noopener"}

> Subway loyalty program members in U.K. and Ireland have been sent scam emails to trick them into downloading malware. [...]
