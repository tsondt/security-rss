Title: Forget Snow Day: Baltimore's 115,000+ public school kids get Ransomware Day, must check Win PCs for infection
Date: 2020-12-01T02:21:24+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: forget-snow-day-baltimores-115000-public-school-kids-get-ransomware-day-must-check-win-pcs-for-infection

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/01/baltimore_ransomware_attack/){:target="_blank" rel="noopener"}

> We don't need no education... IT department may need some Students in Baltimore, Maryland, were on Sunday warned against connecting their Windows PCs to the county’s public school IT system after it was hit by ransomware.... [...]
