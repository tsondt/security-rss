Title: US and Australia to develop shared cyberattack training platform
Date: 2020-12-04T12:28:37
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Technology
Slug: us-and-australia-to-develop-shared-cyberattack-training-platform

[Source](https://www.bleepingcomputer.com/news/security/us-and-australia-to-develop-shared-cyberattack-training-platform/){:target="_blank" rel="noopener"}

> The United States and Australia have signed a first-ever bilateral agreement that allows the U.S. Cyber Command (USCYBERCOM) and the Information Warfare Division (IWD) of the Australian Defense Force to jointly develop and share a virtual cyber training platform. [...]
