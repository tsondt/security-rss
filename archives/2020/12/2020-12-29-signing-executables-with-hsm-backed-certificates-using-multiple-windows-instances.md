Title: Signing executables with HSM-backed certificates using multiple Windows instances
Date: 2020-12-29T17:57:40+00:00
Author: Karim Hamdy Abdelmonsif Ibrahim
Category: AWS Security
Tags: AWS CloudHSM;Intermediate (200);Security, Identity, & Compliance;CloudHSM;Security Blog;SignTool
Slug: signing-executables-with-hsm-backed-certificates-using-multiple-windows-instances

[Source](https://aws.amazon.com/blogs/security/signing-executables-with-hsm-backed-certificates-using-multiple-windows-instances/){:target="_blank" rel="noopener"}

> Customers use code signing certificates to digitally sign software, documents, and other certificates. Signing is a cryptographic tool that lets users verify that the code hasn’t been altered and that the software, documents or other certificates can be trusted. This blog post shows you how to configure your applications so you can use a key [...]
