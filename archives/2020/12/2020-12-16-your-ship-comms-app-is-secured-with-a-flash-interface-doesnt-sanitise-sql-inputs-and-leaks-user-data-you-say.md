Title: Your ship comms app is 'secured' with a Flash interface, doesn't sanitise SQL inputs and leaks user data, you say?
Date: 2020-12-16T09:30:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: your-ship-comms-app-is-secured-with-a-flash-interface-doesnt-sanitise-sql-inputs-and-leaks-user-data-you-say

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/16/dualog_communications_suite_cves/){:target="_blank" rel="noopener"}

> One? Two? Nope. Six CVEs patched after being found in Dualog Communications Suite A software suite intended to let merchant ships’ crews digitally communicate with the world ashore was riddled with security vulnerabilities including undocumented admin accounts with hardcoded passwords and widespread use of Adobe Flash.... [...]
