Title: Emulated mobile devices used to steal millions from US, EU banks
Date: 2020-12-16T12:26:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Mobile
Slug: emulated-mobile-devices-used-to-steal-millions-from-us-eu-banks

[Source](https://www.bleepingcomputer.com/news/security/emulated-mobile-devices-used-to-steal-millions-from-us-eu-banks/){:target="_blank" rel="noopener"}

> Threat actors behind an ongoing worldwide mobile banking fraud campaign were able to steal millions from multiple US and EU banks, needing just a few days for each attack. [...]
