Title: More on the SolarWinds Breach
Date: 2020-12-17T20:18:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: breaches;cyberespionage;cybersecurity;national security policy;Russia
Slug: more-on-the-solarwinds-breach

[Source](https://www.schneier.com/blog/archives/2020/12/more-on-the-solarwinds-breach.html){:target="_blank" rel="noopener"}

> The New York Times has more details. About 18,000 private and government users downloaded a Russian tainted software update –­ a Trojan horse of sorts ­– that gave its hackers a foothold into victims’ systems, according to SolarWinds, the company whose software was compromised. Among those who use SolarWinds software are the Centers for Disease Control and Prevention, the State Department, the Justice Department, parts of the Pentagon and a number of utility companies. While the presence of the software is not by itself evidence that each network was compromised and information was stolen, investigators spent Monday trying to understand [...]
