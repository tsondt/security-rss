Title: Ticketmaster fined $10 million for breaking into rival’s systems
Date: 2020-12-31T02:59:09
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ticketmaster-fined-10-million-for-breaking-into-rivals-systems

[Source](https://www.bleepingcomputer.com/news/security/ticketmaster-fined-10-million-for-breaking-into-rival-s-systems/){:target="_blank" rel="noopener"}

> Ticketmaster, a Live Nation subsidiary and a leading ticket distribution and sales company, was fined $10 million for illegally accessing the systems of competitor CrowdSurge using the credentials of one of its former employees. [...]
