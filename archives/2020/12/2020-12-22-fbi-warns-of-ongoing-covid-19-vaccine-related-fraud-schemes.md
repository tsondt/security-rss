Title: FBI warns of ongoing COVID-19 vaccine related fraud schemes
Date: 2020-12-22T10:52:12
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-ongoing-covid-19-vaccine-related-fraud-schemes

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-ongoing-covid-19-vaccine-related-fraud-schemes/){:target="_blank" rel="noopener"}

> US federal agencies have warned about scammers exploiting the public's interest in the COVID-19 vaccine to harvest personal information and steal money through multiple ongoing and emerging fraud schemes. [...]
