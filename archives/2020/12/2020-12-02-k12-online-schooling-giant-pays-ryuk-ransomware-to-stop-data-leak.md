Title: K12 online schooling giant pays Ryuk ransomware to stop data leak
Date: 2020-12-02T16:15:20
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: k12-online-schooling-giant-pays-ryuk-ransomware-to-stop-data-leak

[Source](https://www.bleepingcomputer.com/news/security/k12-online-schooling-giant-pays-ryuk-ransomware-to-stop-data-leak/){:target="_blank" rel="noopener"}

> Online education giant K12 Inc. has paid a ransom after their systems were hit by Ryuk ransomware in the middle of November. [...]
