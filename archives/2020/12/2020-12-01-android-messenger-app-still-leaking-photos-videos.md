Title: Android Messenger App Still Leaking Photos, Videos
Date: 2020-12-01T21:28:45+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Privacy;Vulnerabilities;android messenger app;Exploit;go sms pro;messenger app;new version;security bug;sensitive information;TrustWave;Underground forums;vulnerability
Slug: android-messenger-app-still-leaking-photos-videos

[Source](https://threatpost.com/android-messenger-app-leaking-photos-videos/161741/){:target="_blank" rel="noopener"}

> The GO SMS Pro app has been downloaded 100 million times; now, underground forums are actively sharing images stolen from GO SMS servers. [...]
