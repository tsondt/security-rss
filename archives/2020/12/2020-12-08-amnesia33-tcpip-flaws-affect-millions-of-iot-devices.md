Title: ‘Amnesia:33’ TCP/IP Flaws Affect Millions of IoT Devices
Date: 2020-12-08T11:00:53+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Podcasts;Vulnerabilities;Amnesia:33;embedded systems;flaws;Internet of things;IoT;Open Source;operational technology;OT;patch;Routers;Security Vulnerabilities;tcp/ip;TCP/IP Stack
Slug: amnesia33-tcpip-flaws-affect-millions-of-iot-devices

[Source](https://threatpost.com/amnesia33-tcp-ip-flaws-iot-devices/161928/){:target="_blank" rel="noopener"}

> A new set of vulnerabilities has been discovered affecting millions of routers and IoT and OT devices from more than 150 vendors, new research warns. [...]
