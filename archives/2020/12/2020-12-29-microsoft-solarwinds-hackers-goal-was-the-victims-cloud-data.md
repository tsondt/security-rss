Title: Microsoft: SolarWinds hackers' goal was the victims' cloud data
Date: 2020-12-29T13:30:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-solarwinds-hackers-goal-was-the-victims-cloud-data

[Source](https://www.bleepingcomputer.com/news/security/microsoft-solarwinds-hackers-goal-was-the-victims-cloud-data/){:target="_blank" rel="noopener"}

> Microsoft says that the end goal of the SolarWinds supply chain compromise was to pivot to the victims' cloud assets after deploying the Sunburst/Solorigate backdoor on their local networks. [...]
