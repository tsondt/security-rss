Title: Supreme Court mulls whether a cop looking up a license plate for cash is equivalent to watching Instagram at work
Date: 2020-12-01T12:16:09+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: supreme-court-mulls-whether-a-cop-looking-up-a-license-plate-for-cash-is-equivalent-to-watching-instagram-at-work

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/01/cffa_supreme_court/){:target="_blank" rel="noopener"}

> America's outdated Computer Fraud and Abuse Act gets a roasting Analysis There’s a growing problem with computer laws written in the late 1980s and early 1990s. They were produced just as PCs began entering widespread personal usage but they failed to account for what electronic devices would soon be used for most of the time: accessing information over the internet.... [...]
