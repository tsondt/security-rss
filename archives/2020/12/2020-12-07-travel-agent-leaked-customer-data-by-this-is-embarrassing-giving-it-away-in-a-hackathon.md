Title: Travel agent leaked customer data by – this is embarrassing - giving it away in a hackathon
Date: 2020-12-07T07:33:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: travel-agent-leaked-customer-data-by-this-is-embarrassing-giving-it-away-in-a-hackathon

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/07/data_breach_in_hackathon_data/){:target="_blank" rel="noopener"}

> Bad design bites as Flight Centre's policy of no credit card or passport numbers in app's free text field was not enforced, therefore ignored Be careful what you wish for when running a hackathon, because one in Australia turned up a data breach in the trove of sample data offered to hackers. And it was probably developers’ fault.... [...]
