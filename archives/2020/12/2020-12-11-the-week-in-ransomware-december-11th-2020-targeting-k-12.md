Title: The Week in Ransomware - December 11th 2020 -  Targeting K-12
Date: 2020-12-11T16:31:32
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-december-11th-2020-targeting-k-12

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-11th-2020-targeting-k-12/){:target="_blank" rel="noopener"}

> This week we continued to see ransomware target businesses, education, and healthcare with cyberattacks that disrupt operations and lead to school closings. [...]
