Title: Kawasaki Heavy Industries reports data breach as attackers found with year-long network access
Date: 2020-12-29T12:21:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: kawasaki-heavy-industries-reports-data-breach-as-attackers-found-with-year-long-network-access

[Source](https://portswigger.net/daily-swig/kawasaki-heavy-industries-reports-data-breach-as-attackers-found-with-year-long-network-access){:target="_blank" rel="noopener"}

> Japanese conglomerate confirms possible leak of sensitive information [...]
