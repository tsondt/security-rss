Title: 2020 ISO certificates are here, with a new Region and increased in-scope services
Date: 2020-12-17T21:21:02+00:00
Author: Anastasia Strebkova
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS ISO;AWS ISO Certificates;AWS ISO27001;AWS ISO27017;AWS ISO27018;AWS ISO9001;Security Blog
Slug: 2020-iso-certificates-are-here-with-a-new-region-and-increased-in-scope-services

[Source](https://aws.amazon.com/blogs/security/2020-iso-certificates-are-here-with-a-new-region-and-increased-in-scope-services/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) successfully completed the surveillance audits with no findings for ISO 9001, 27001, 27017, or 27018. Ernst and Young Certify Point auditors reissued the certificates on November 6, 2020. The certificates validate ISO compliance of our Information Security Management System from the perspective of third-party auditors. We included 9 additional AWS services [...]
