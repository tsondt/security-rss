Title: Upcoming Speaking Engagements
Date: 2020-12-14T19:39:45+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Schneier news
Slug: upcoming-speaking-engagements-4

[Source](https://www.schneier.com/blog/archives/2020/12/upcoming-speaking-engagements-4.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking (online) at Western Washington University on January 20, 2021. Details to come. I’ll be speaking at an Informa event on February 28, 2021. Details to come. The list is maintained on this page. [...]
