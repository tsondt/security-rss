Title: The patch that wasn't: Cisco emits fresh fixes for NTLM hash-spilling vuln and XSS-RCE combo in Jabber app
Date: 2020-12-10T17:30:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: the-patch-that-wasnt-cisco-emits-fresh-fixes-for-ntlm-hash-spilling-vuln-and-xss-rce-combo-in-jabber-app

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/10/cisco_jabber_xss_rce_ntlm_hash_patches/){:target="_blank" rel="noopener"}

> Wormable nasty still doesn't need any user input to pwn target devices A previous patch for Cisco's Jabber chat product did not in fact fix four vulnerabilities – including one remote code execution (RCE) flaw that would allow malicious people to hijack targeted devices by sending a carefully crafted message.... [...]
