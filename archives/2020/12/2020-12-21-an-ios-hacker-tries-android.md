Title: An iOS hacker tries Android
Date: 2020-12-21T12:10:00.001000-08:00
Author: Ryan (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: an-ios-hacker-tries-android

[Source](https://googleprojectzero.blogspot.com/2020/12/an-ios-hacker-tries-android.html){:target="_blank" rel="noopener"}

> Written by Brandon Azad, when working at Project Zero One of the amazing aspects of working at Project Zero is having the flexibility to direct my own research agenda. My prior work has almost exclusively focused on iOS exploitation, but back in August, I thought it could be interesting to try writing a kernel exploit for Android to see how it compares. I have two aims for this blog post: First, I will walk you through my full journey from bug description to kernel read/write/execute on the Samsung Galaxy S10, starting from the perspective of a pure-iOS security researcher. Second, [...]
