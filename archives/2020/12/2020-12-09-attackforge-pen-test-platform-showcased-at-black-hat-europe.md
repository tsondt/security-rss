Title: AttackForge pen test platform showcased at Black Hat Europe
Date: 2020-12-09T12:48:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: attackforge-pen-test-platform-showcased-at-black-hat-europe

[Source](https://portswigger.net/daily-swig/attackforge-pen-test-platform-showcased-at-black-hat-europe){:target="_blank" rel="noopener"}

> Collaboration tool aimed at standardizing and streamlining the security testing process [...]
