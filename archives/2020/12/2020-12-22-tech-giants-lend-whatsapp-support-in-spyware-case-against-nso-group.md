Title: Tech Giants Lend WhatsApp Support in Spyware Case Against NSO Group
Date: 2020-12-22T16:24:34+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security;Privacy;appeals court;Cisco systems;cyber-surveillance;Electronic Frontier Foundation;Facebook;google;Lawsuit;legal;Microsoft;mobile devices;NSO Group;Pegasus;Spyware;United States Court of Appeals for the Ninth Circuit;vmware;WhatsApp
Slug: tech-giants-lend-whatsapp-support-in-spyware-case-against-nso-group

[Source](https://threatpost.com/tech-giants-lend-whatsapp-support-in-spyware-case-against-nso-group/162552/){:target="_blank" rel="noopener"}

> Google, Microsoft, Cisco Systems and others want appeals court to deny immunity to Israeli company for its alleged distribution of spyware and illegal cyber-surveillance activities. [...]
