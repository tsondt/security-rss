Title: How the SolarWinds Hackers Bypassed Duo’s Multi-Factor Authentication
Date: 2020-12-15T20:13:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: authentication;breaches;network security;two-factor authentication
Slug: how-the-solarwinds-hackers-bypassed-duos-multi-factor-authentication

[Source](https://www.schneier.com/blog/archives/2020/12/how-the-solarwinds-hackers-bypassed-duo-multi-factor-authentication.html){:target="_blank" rel="noopener"}

> This is interesting : Toward the end of the second incident that Volexity worked involving Dark Halo, the actor was observed accessing the e-mail account of a user via OWA. This was unexpected for a few reasons, not least of which was the targeted mailbox was protected by MFA. Logs from the Exchange server showed that the attacker provided username and password authentication like normal but were not challenged for a second factor through Duo. The logs from the Duo authentication server further showed that no attempts had been made to log into the account in question. Volexity was able [...]
