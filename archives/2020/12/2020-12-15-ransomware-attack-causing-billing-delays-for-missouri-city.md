Title: Ransomware attack causing billing delays for Missouri city
Date: 2020-12-15T11:09:59
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-attack-causing-billing-delays-for-missouri-city

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-causing-billing-delays-for-missouri-city/){:target="_blank" rel="noopener"}

> The City of Independence, Missouri, suffered a ransomware attack last week that continues to disrupt the city's services. [...]
