Title: Rogue ex-Cisco employee who crippled WebEx conferences and cost Cisco millions gets two years in US prison
Date: 2020-12-12T11:04:25+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: rogue-ex-cisco-employee-who-crippled-webex-conferences-and-cost-cisco-millions-gets-two-years-in-us-prison

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/12/in_brief_security/){:target="_blank" rel="noopener"}

> And the week's other security news A former Cisco employee who went medieval on his former employer and cost the company millions, has been sentenced to two years in prison and a $15,000 fine.... [...]
