Title: Xerox DocuShare Bugs Allows Data Leaks
Date: 2020-12-02T20:17:34+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;document management;IBM;opentext;Oracle;OWASP Foundation;SSRF attacks;Xerox;XXE attack
Slug: xerox-docushare-bugs-allows-data-leaks

[Source](https://threatpost.com/xerox-docushare-bugs/161791/){:target="_blank" rel="noopener"}

> CISA warns the leading enterprise document management platform is open to attack and urges companies to apply fixes. [...]
