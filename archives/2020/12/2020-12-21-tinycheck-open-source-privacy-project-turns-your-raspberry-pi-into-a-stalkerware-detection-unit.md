Title: TinyCheck: Open source privacy project turns your Raspberry Pi into a stalkerware detection unit
Date: 2020-12-21T12:54:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: tinycheck-open-source-privacy-project-turns-your-raspberry-pi-into-a-stalkerware-detection-unit

[Source](https://portswigger.net/daily-swig/tinycheck-open-source-privacy-project-turns-your-raspberry-pi-into-a-stalkerware-detection-unit){:target="_blank" rel="noopener"}

> Tool captures potentially malicious communications being sent from smartphone to WiFi points [...]
