Title: SnitchDNS framework offers automation and network monitoring features to pen testers
Date: 2020-12-10T14:31:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: snitchdns-framework-offers-automation-and-network-monitoring-features-to-pen-testers

[Source](https://portswigger.net/daily-swig/snitchdns-framework-offers-automation-and-network-monitoring-features-to-pen-testers){:target="_blank" rel="noopener"}

> ‘It’s always DNS’ [...]
