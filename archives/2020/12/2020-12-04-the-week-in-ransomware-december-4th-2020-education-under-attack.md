Title: The Week in Ransomware - December 4th 2020 - Education under attack
Date: 2020-12-04T17:55:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-december-4th-2020-education-under-attack

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-4th-2020-education-under-attack/){:target="_blank" rel="noopener"}

> It has been another rough week for the enterprise and education as ransomware continues to impact business operations and shut down schools. [...]
