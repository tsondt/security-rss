Title: WordPress 5.6 lands with new auto-update UI, Site Health enhancements
Date: 2020-12-08T16:22:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: wordpress-56-lands-with-new-auto-update-ui-site-health-enhancements

[Source](https://portswigger.net/daily-swig/wordpress-5-6-lands-with-new-auto-update-ui-site-health-enhancements){:target="_blank" rel="noopener"}

> Webmasters can also issue authenticated requests to WordPress APIs via Application Passwords [...]
