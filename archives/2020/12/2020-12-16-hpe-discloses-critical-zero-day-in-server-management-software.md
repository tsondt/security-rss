Title: HPE discloses critical zero-day in server management software
Date: 2020-12-16T09:55:35
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hpe-discloses-critical-zero-day-in-server-management-software

[Source](https://www.bleepingcomputer.com/news/security/hpe-discloses-critical-zero-day-in-server-management-software/){:target="_blank" rel="noopener"}

> Hewlett Packard Enterprise (HPE) has disclosed a zero-day bug in the latest versions of its proprietary HPE Systems Insight Manager (SIM) software for Windows and Linux. [...]
