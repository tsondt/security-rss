Title: Spotify Wrapped 2020 Rollout Marred by Pop Star Hacks
Date: 2020-12-02T21:38:55+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Web Security;breach;cyberattack;daniel;Defacement;dua lipa;future;hack;hacktivism;lana del rey;Passwords;pop smoke;pop star;pop star pages;Spotify;Spotify for Artists;spotify wrapped 2020;taylor swift;wrapped 2020
Slug: spotify-wrapped-2020-rollout-marred-by-pop-star-hacks

[Source](https://threatpost.com/spotify-wrapped-2020-rollout-pop-star-hacks/161811/){:target="_blank" rel="noopener"}

> Spotify pages for Dua Lipa, Lana Del Rey, Future and others were defaced by an attacker pledging his love for Taylor Swift and Trump. [...]
