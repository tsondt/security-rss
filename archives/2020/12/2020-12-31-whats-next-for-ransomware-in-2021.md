Title: What’s Next for Ransomware in 2021?
Date: 2020-12-31T14:00:22+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Featured;Government;Hacks;Malware;Vulnerabilities;Web Security;Webinars;2020;business plan;Cyber Insurance;Cybereason;data stolent;DDoS;Digital Shadows;double extortion;IBM;Incident response;payouts;ransomware;trends;what's next
Slug: whats-next-for-ransomware-in-2021

[Source](https://threatpost.com/ransomware-getting-ahead-inevitable-attack/162655/){:target="_blank" rel="noopener"}

> Ransomware response demands a whole-of-business plan before the next attack, according to our roundtable of experts. [...]
