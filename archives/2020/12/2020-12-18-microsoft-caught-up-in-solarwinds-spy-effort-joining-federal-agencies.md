Title: Microsoft Caught Up in SolarWinds Spy Effort, Joining Federal Agencies
Date: 2020-12-18T16:42:53+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Government;Hacks;Malware;Vulnerabilities;Brad Smith;cyberattack;espionage;federal agencies;Microsoft;orion;solarwinds;solorigate;spy effort;sunburst;US attack;victim
Slug: microsoft-caught-up-in-solarwinds-spy-effort-joining-federal-agencies

[Source](https://threatpost.com/microsoft-solarwinds-spy-attack-federal-agencies/162414/){:target="_blank" rel="noopener"}

> The ongoing, growing campaign is “effectively an attack on the United States and its government and other critical institutions,” Microsoft warned. [...]
