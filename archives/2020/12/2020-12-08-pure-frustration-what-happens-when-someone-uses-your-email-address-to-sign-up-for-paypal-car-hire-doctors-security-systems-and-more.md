Title: Pure frustration: What happens when someone uses your email address to sign up for PayPal, car hire, doctors, security systems and more
Date: 2020-12-08T10:16:07+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: pure-frustration-what-happens-when-someone-uses-your-email-address-to-sign-up-for-paypal-car-hire-doctors-security-systems-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/08/pure_frustration_what_happens_when/){:target="_blank" rel="noopener"}

> Messsage Center: Call this number. Number: Use the Message Center. Many companies have no mechanism to deal with a common problem: when users open accounts using someone else's email address, either by accident or design. "I have had a barrage of account creation requests that will fail... also a large number of invoices, warranty emails and so on for purchases, from furniture to electronics," a reader informed us.... [...]
