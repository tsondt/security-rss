Title: How to leak data via Wi-Fi when there's no Wi-Fi chip: Boffin turns memory bus into covert data transmitter
Date: 2020-12-16T07:30:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: how-to-leak-data-via-wi-fi-when-theres-no-wi-fi-chip-boffin-turns-memory-bus-into-covert-data-transmitter

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/16/wifi_memory_hacking/){:target="_blank" rel="noopener"}

> Another nail in the coffin of assuming that airgapped means secure Mordechai Guri, an Israeli cyber security researcher who focuses on covert side channel attacks, has devised yet another way to undermine air gapping – the practice of keeping computers disconnected from any external network for the sake of security.... [...]
