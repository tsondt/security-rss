Title: Alabama school district shut down by ransomware attack
Date: 2020-12-01T19:09:57
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: alabama-school-district-shut-down-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/alabama-school-district-shut-down-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Ransomware operators have attacked the Huntsville City Schools district in Alabama, forcing them to shut down schools for the rest of the week and possibly next week. [...]
