Title: Multi-platform card skimmer found on Shopify, BigCommerce stores
Date: 2020-12-28T09:21:12
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: multi-platform-card-skimmer-found-on-shopify-bigcommerce-stores

[Source](https://www.bleepingcomputer.com/news/security/multi-platform-card-skimmer-found-on-shopify-bigcommerce-stores/){:target="_blank" rel="noopener"}

> A recently discovered multi-platform credit card skimmer can harvest payment info on compromised stores powered by Shopify, BigCommerce, Zencart, and Woocommerce. [...]
