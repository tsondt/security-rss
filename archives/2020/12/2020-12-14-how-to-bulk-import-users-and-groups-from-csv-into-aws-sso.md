Title: How to bulk import users and groups from CSV into AWS SSO
Date: 2020-12-14T18:36:30+00:00
Author: Darryn Hendricks
Category: AWS Security
Tags: AWS Single Sign-On (SSO);Intermediate (200);Security, Identity, & Compliance;AWS Single Sign-On;AWS SSO;Bulk import;CSV import;Security Blog
Slug: how-to-bulk-import-users-and-groups-from-csv-into-aws-sso

[Source](https://aws.amazon.com/blogs/security/how-to-bulk-import-users-and-groups-from-csv-into-aws-sso/){:target="_blank" rel="noopener"}

> When you connect an external identity provider (IdP) to AWS Single Sign-On (SSO) using Security Assertion Markup Language (SAML) 2.0 standard, you must create all users and groups into AWS SSO before you can make any assignments to AWS accounts or applications. If your IdP supports user and group provisioning by way of the System [...]
