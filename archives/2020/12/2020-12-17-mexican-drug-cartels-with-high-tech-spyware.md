Title: Mexican Drug Cartels with High-Tech Spyware
Date: 2020-12-17T13:19:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberweapons;drug trade;Mexico;privacy;spyware;surveillance
Slug: mexican-drug-cartels-with-high-tech-spyware

[Source](https://www.schneier.com/blog/archives/2020/12/mexican-drug-cartels-with-high-tech-spyware.html){:target="_blank" rel="noopener"}

> Sophisticated spyware, sold by surveillance tech companies to Mexican government agencies, are ending up in the hands of drug cartels : As many as 25 private companies — including the Israeli company NSO Group and the Italian firm Hacking Team — have sold surveillance software to Mexican federal and state police forces, but there is little or no regulation of the sector — and no way to control where the spyware ends up, said the officials. Lots of details in the article. The cyberweapons arms business is immoral in many ways. This is just one of them. [...]
