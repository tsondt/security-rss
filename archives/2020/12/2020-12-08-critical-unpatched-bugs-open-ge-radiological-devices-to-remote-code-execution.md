Title: Critical, Unpatched Bugs Open GE Radiological Devices to Remote Code Execution
Date: 2020-12-08T17:00:44+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;IoT;Privacy;Vulnerabilities;big;cisa alert;cybermdx;default credentials;Device security;GE Healthcare;Healthcare;Hospitals;medical devices;medical imaging;mri machines;radiological devices;remote code execution;security vulnerability;unpatched
Slug: critical-unpatched-bugs-open-ge-radiological-devices-to-remote-code-execution

[Source](https://threatpost.com/critical-unpatched-bug-ge-radiological-devices/162012/){:target="_blank" rel="noopener"}

> A CISA alert is flagging a critical default credentials issue that affects 100+ types of devices found in hospitals, from MRI machines to surgical imaging. [...]
