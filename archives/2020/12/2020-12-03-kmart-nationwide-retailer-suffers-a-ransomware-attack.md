Title: Kmart nationwide retailer suffers a ransomware attack
Date: 2020-12-03T13:08:47
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: kmart-nationwide-retailer-suffers-a-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/kmart-nationwide-retailer-suffers-a-ransomware-attack/){:target="_blank" rel="noopener"}

> US department store retailer Kmart has suffered a ransomware attack that impacts back-end services at the company, BleepingComputer has learned. [...]
