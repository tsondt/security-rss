Title: XSS for PDFs – New injection technique offers rich pickings for security researchers
Date: 2020-12-10T15:44:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: xss-for-pdfs-new-injection-technique-offers-rich-pickings-for-security-researchers

[Source](https://portswigger.net/daily-swig/xss-for-pdfs-new-injection-technique-offers-rich-pickings-for-security-researchers){:target="_blank" rel="noopener"}

> A lack of input sanitization leaves PDF documents ripe for exfiltration [...]
