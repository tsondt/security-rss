Title: Spearphishing Attack Spoofs Microsoft.com to Target 200M Office 365 Users
Date: 2020-12-08T13:54:41+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Web Security;exact domain spoofing;financial services;Healthcare;Ironscales;manufacturing;Microsoft;Office 365;spearphishing;Spoofing;telecom;utilities;vertical markets
Slug: spearphishing-attack-spoofs-microsoftcom-to-target-200m-office-365-users

[Source](https://threatpost.com/spearphishing-attack-spoofs-microsoft-office-365/162001/){:target="_blank" rel="noopener"}

> It remains unknown as to why Microsoft is allowing a spoof of their very own domain against their own email infrastructure. [...]
