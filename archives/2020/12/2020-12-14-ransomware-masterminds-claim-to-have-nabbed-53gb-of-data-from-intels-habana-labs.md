Title: Ransomware masterminds claim to have nabbed 53GB of data from Intel's Habana Labs
Date: 2020-12-14T20:24:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: ransomware-masterminds-claim-to-have-nabbed-53gb-of-data-from-intels-habana-labs

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/14/habana_labs_ransomware/){:target="_blank" rel="noopener"}

> Miscreants threaten to make files, source code public within 72 hours The Pay2Key ransomware group on Sunday posted what appear to be details of internal files obtained from Habana Labs, an Israel-based chip startup acquired a year ago by Intel.... [...]
