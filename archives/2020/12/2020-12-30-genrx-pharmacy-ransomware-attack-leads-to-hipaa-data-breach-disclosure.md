Title: GenRx Pharmacy ransomware attack leads to HIPAA data breach disclosure
Date: 2020-12-30T12:33:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: genrx-pharmacy-ransomware-attack-leads-to-hipaa-data-breach-disclosure

[Source](https://portswigger.net/daily-swig/genrx-pharmacy-ransomware-attack-leads-to-hipaa-data-breach-disclosure){:target="_blank" rel="noopener"}

> More than 130,000 patients alerted to potential data breach following healthcare cyber-attack [...]
