Title: Latest web hacking tools – Q4 2020
Date: 2020-12-29T16:39:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: latest-web-hacking-tools-q4-2020

[Source](https://portswigger.net/daily-swig/latest-web-hacking-tools-q4-2020){:target="_blank" rel="noopener"}

> We take a look back at some of the best offensive security tools that were launched over the past three months [...]
