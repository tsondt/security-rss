Title: Cyberpunk 2077 Headaches Grow: New Spyware Found in Fake Android Download
Date: 2020-12-18T16:32:55+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Vulnerabilities;Android;Black Kingdom;CoderWare;cyberpunk 2077;gamers;Google Play Store;Kaspersky Labs;keanu reeves;Sony;Sony PlayStation;Tatyana Shishkova;twitter;videogame
Slug: cyberpunk-2077-headaches-grow-new-spyware-found-in-fake-android-download

[Source](https://threatpost.com/cyberpunk-2077-headaches-grow-android-spyware/162406/){:target="_blank" rel="noopener"}

> Threat actors impersonate Google Play store in scam as Sony pulls the game off the PlayStation store due to myriad performance issues. [...]
