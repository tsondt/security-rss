Title: Report: DevSecOps adoption gathers pace, but record on applying security updates is patchy
Date: 2020-12-08T15:25:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: report-devsecops-adoption-gathers-pace-but-record-on-applying-security-updates-is-patchy

[Source](https://portswigger.net/daily-swig/report-devsecops-adoption-gathers-pace-but-record-on-applying-security-updates-is-patchy){:target="_blank" rel="noopener"}

> Many IT professionals recognize importance of addressing vulnerabilities – but many are slow to apply patches [...]
