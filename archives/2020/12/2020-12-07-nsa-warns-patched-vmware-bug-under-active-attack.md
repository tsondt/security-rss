Title: NSA Warns: Patched VMware Bug Under Active Attack
Date: 2020-12-07T22:06:34+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities;Web Security;command injection vulnerability;CVE-2020-4006;NSA;SAML;vmware;Workspace One
Slug: nsa-warns-patched-vmware-bug-under-active-attack

[Source](https://threatpost.com/nsa-vmware-bug-under-attack/161985/){:target="_blank" rel="noopener"}

> Feds are warning that adversaries are exploiting a weeks-old bug in VMware’s Workspace One Access and VMware Identity Manager products. [...]
