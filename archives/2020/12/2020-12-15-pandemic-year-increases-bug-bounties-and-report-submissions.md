Title: Pandemic year increases bug bounties and report submissions
Date: 2020-12-15T11:18:48
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: pandemic-year-increases-bug-bounties-and-report-submissions

[Source](https://www.bleepingcomputer.com/news/security/pandemic-year-increases-bug-bounties-and-report-submissions/){:target="_blank" rel="noopener"}

> Vulnerability submissions have increased over the past 12 months on at least one crowdsourced security platform, with critical issue reports recording a 65% jump. [...]
