Title: Investigating the Navalny Poisoning
Date: 2020-12-23T12:44:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: forensics;operational security;Russia;traffic analysis
Slug: investigating-the-navalny-poisoning

[Source](https://www.schneier.com/blog/archives/2020/12/investigating-the-navalny-poisoning.html){:target="_blank" rel="noopener"}

> Bellingcat has investigated the near-fatal poisoning of Alexey Navalny by the Russian FSB back in August. The details display some impressive traffic analysis. Navalny got a confession out of one of the poisoners, displaying some masterful social engineering. Lots of interesting opsec details in all of this. [...]
