Title: Android app still exposing messages of 100M users despite bug fix
Date: 2020-12-01T09:00:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: android-app-still-exposing-messages-of-100m-users-despite-bug-fix

[Source](https://www.bleepingcomputer.com/news/security/android-app-still-exposing-messages-of-100m-users-despite-bug-fix/){:target="_blank" rel="noopener"}

> GO SMS Pro, an Android instant messaging app with more than 100 million installs, is still exposing the privately shared messages of millions of users even though the developer has been working on a fix for the flaw behind the data leak for almost two weeks. [...]
