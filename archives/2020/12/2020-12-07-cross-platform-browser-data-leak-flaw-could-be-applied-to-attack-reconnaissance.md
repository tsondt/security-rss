Title: Cross-platform browser data leak flaw could be applied to attack reconnaissance
Date: 2020-12-07T15:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cross-platform-browser-data-leak-flaw-could-be-applied-to-attack-reconnaissance

[Source](https://portswigger.net/daily-swig/cross-platform-browser-data-leak-flaw-could-be-applied-to-attack-reconnaissance){:target="_blank" rel="noopener"}

> Medium risk vulnerability patched by Mozilla – but not Google or Microsoft [...]
