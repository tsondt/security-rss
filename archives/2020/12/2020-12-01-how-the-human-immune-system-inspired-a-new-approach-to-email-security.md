Title: How the human immune system inspired a new approach to email security
Date: 2020-12-01T20:30:11+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: how-the-human-immune-system-inspired-a-new-approach-to-email-security

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/01/how_the_human_immune_system/){:target="_blank" rel="noopener"}

> AI excels at interpreting high volume, high velocity, complex data – which is just the ticket here Sponsored Computer scientists have long been interested in the human body's immune system. It fights off an incredible range of attacks spanning from the common cold to flu, measles, and worse. As companies face a rising tide of cyber attacks, a new approach to email defence developed by cybersecurity company Darktrace uses our own ability to fight off external threats and replicates this ‘immune system’ approach in the digital world.... [...]
