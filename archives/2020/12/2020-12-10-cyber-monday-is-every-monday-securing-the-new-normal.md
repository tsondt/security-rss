Title: Cyber Monday is Every Monday: Securing the ‘New Normal’
Date: 2020-12-10T15:00:29+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Podcasts;Vulnerabilities;Web Security;connnected speakers;consumer grade routers;cyber monday;eCommerce;IoT devices;Privacy;retail threats;Router
Slug: cyber-monday-is-every-monday-securing-the-new-normal

[Source](https://threatpost.com/consumer-grade-router-attacks-the-new-normal/162080/){:target="_blank" rel="noopener"}

> From eCommerce threats, to attacks at the smart edge, Fortinet researchers discuss the top evolving threats of 2020, heading into the new year. [...]
