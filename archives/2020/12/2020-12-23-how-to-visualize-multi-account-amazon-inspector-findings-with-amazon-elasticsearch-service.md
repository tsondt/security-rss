Title: How to visualize multi-account Amazon Inspector findings with Amazon Elasticsearch Service
Date: 2020-12-23T17:56:54+00:00
Author: Moumita Saha
Category: AWS Security
Tags: Advanced (300);Amazon Elasticsearch Service;Amazon Inspector;Security, Identity, & Compliance;Amazon S3;Inspector findings;Kibana;SecOps;security automation;Security Blog;security monitoring;Vulnerability Assessment
Slug: how-to-visualize-multi-account-amazon-inspector-findings-with-amazon-elasticsearch-service

[Source](https://aws.amazon.com/blogs/security/how-to-visualize-multi-account-amazon-inspector-findings-with-amazon-elasticsearch-service/){:target="_blank" rel="noopener"}

> Amazon Inspector helps to improve the security and compliance of your applications that are deployed on Amazon Web Services (AWS). It automatically assesses Amazon Elastic Compute Cloud (Amazon EC2) instances and applications on those instances. From that assessment, it generates findings related to exposure, potential vulnerabilities, and deviations from best practices. You can use the [...]
