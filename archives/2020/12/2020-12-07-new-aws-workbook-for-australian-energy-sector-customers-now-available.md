Title: New AWS Workbook for Australian energy sector customers now available
Date: 2020-12-07T20:56:34+00:00
Author: Julian Busic
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AEMO;AESCSF;AESCSF 2019;Australia;Australian Energy Market Operator;Australian Energy Sector Cyber Security Framework 2019;Energy sector;Security Blog
Slug: new-aws-workbook-for-australian-energy-sector-customers-now-available

[Source](https://aws.amazon.com/blogs/security/new-aws-workbook-for-australian-energy-sector-customers-now-available/){:target="_blank" rel="noopener"}

> I’m pleased to announce the Amazon Web Services (AWS) AESCSF 2019 Workbook, a resource designed to help energy sector customers align with the Australian Energy Market Operator (AEMO)’s Australian Energy Sector Cyber Security Framework (AESCSF) 2019. The workbook helps energy sector customers to: Conduct due diligence on the AWS control environment, by mapping the AESCSF [...]
