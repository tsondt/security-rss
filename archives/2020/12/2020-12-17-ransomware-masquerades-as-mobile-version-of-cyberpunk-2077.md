Title: Ransomware masquerades as mobile version of Cyberpunk 2077
Date: 2020-12-17T14:01:12
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-masquerades-as-mobile-version-of-cyberpunk-2077

[Source](https://www.bleepingcomputer.com/news/security/ransomware-masquerades-as-mobile-version-of-cyberpunk-2077/){:target="_blank" rel="noopener"}

> A threat actor is distributing fake Windows and Android installers for the Cyberpunk 2077 game that is installing a ransomware calling itself CoderWare. [...]
