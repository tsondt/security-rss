Title: Adobe Warns Windows, macOS Users of Critical-Severity Flaws
Date: 2020-12-08T16:36:45+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;adobe;Adobe Experience Manager;adobe Lightroom;adobe prelude;arbitrary code execution;blind server-side request forgery;critical flaw;CVE-2020-24440;CVE-2020-24444;CVE-2020-24445;CVE-2020-24447;macOS;vulnerability;Windows
Slug: adobe-warns-windows-macos-users-of-critical-severity-flaws

[Source](https://threatpost.com/adobe-windows-macos-critical-severity-flaws/162007/){:target="_blank" rel="noopener"}

> Adobe fixed three critical-severity flaws in Adobe Prelude, Adobe Experience Manager and Adobe Lightroom. [...]
