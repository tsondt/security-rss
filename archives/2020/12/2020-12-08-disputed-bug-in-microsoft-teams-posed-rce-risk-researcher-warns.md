Title: Disputed bug in Microsoft Teams posed RCE risk, researcher warns
Date: 2020-12-08T17:40:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: disputed-bug-in-microsoft-teams-posed-rce-risk-researcher-warns

[Source](https://portswigger.net/daily-swig/disputed-bug-in-microsoft-teams-posed-rce-risk-researcher-warns){:target="_blank" rel="noopener"}

> Flaw finder disputes Microsoft’s ‘spoofing’ designation [...]
