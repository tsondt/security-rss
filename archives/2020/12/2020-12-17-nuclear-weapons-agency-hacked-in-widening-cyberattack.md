Title: Nuclear Weapons Agency Hacked in Widening Cyberattack
Date: 2020-12-17T23:07:56+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Hacks;Malware;Vulnerabilities;attack vector;CISA;cyberattack;damage;Department of Energy;doe;FERC;nuclear weapons agency;orion;solarwinds
Slug: nuclear-weapons-agency-hacked-in-widening-cyberattack

[Source](https://threatpost.com/nuclear-weapons-agency-hacked-cyberattack/162387/){:target="_blank" rel="noopener"}

> The DoE suffered "damage" in the attack, which also likely extends beyond the initially known SolarWinds Orion attack vector. [...]
