Title: It’s dark out there, and if you want to keep the lights on, you need to update your cyber-security skills with SANS
Date: 2020-12-04T07:00:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: its-dark-out-there-and-if-you-want-to-keep-the-lights-on-you-need-to-update-your-cyber-security-skills-with-sans

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/04/update_your_cybersec_skills/){:target="_blank" rel="noopener"}

> Learn over one week, two weeks, or even two days – the choice is yours Promo As the nights get darker, and the holiday season begins, thoughts naturally turn to... how am I am going to sharpen up my cyber-security skills in 2021? And how am I going to find the time to do it? After all, miscreants are showing no sign of deskilling or taking a day off any time soon.... [...]
