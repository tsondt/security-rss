Title: Facebook unmasks Vietnam’s APT32 hacking group
Date: 2020-12-11T10:06:12
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: facebook-unmasks-vietnams-apt32-hacking-group

[Source](https://www.bleepingcomputer.com/news/security/facebook-unmasks-vietnam-s-apt32-hacking-group/){:target="_blank" rel="noopener"}

> The Facebook security team has revealed today the real identity of APT32, a Vietnam-backed hacking group active in cyberespionage campaigns targeting foreign government, multi-national corporations, and journalists since at least 2014. [...]
