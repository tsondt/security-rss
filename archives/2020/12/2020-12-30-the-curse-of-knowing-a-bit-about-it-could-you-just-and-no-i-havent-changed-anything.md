Title: The curse of knowing a bit about IT: 'Could you just...?' and 'No I haven't changed <i>anything</i>'
Date: 2020-12-30T09:30:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: the-curse-of-knowing-a-bit-about-it-could-you-just-and-no-i-havent-changed-anything

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/30/12boc/){:target="_blank" rel="noopener"}

> Two firewalls are better than one, right? 12BoC On the sixth day of Christmas, the bork gods sent to me: Fix the printer, nerds, Scottish parking whi-i-i-i-nge, one dead DB, petty angry user, flightless Windows signage, and a server they said had ceased to be.... [...]
