Title: Oblivious DoH, OPAQUE passwords, Encrypted Client Hello: Cloudflare's protocol proposals to protect privacy
Date: 2020-12-08T18:45:07+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: oblivious-doh-opaque-passwords-encrypted-client-hello-cloudflares-protocol-proposals-to-protect-privacy

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/08/cloudflare_privacy_protocols/){:target="_blank" rel="noopener"}

> 'Adopting these may have legal and policy implications' Web infrastructure company Cloudflare is pushing for the adoption of new internet protocols it says will enable a "privacy-respecting internet".... [...]
