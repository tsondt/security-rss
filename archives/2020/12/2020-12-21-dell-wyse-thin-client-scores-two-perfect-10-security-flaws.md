Title: Dell Wyse Thin Client scores two perfect 10 security flaws
Date: 2020-12-21T17:00:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: dell-wyse-thin-client-scores-two-perfect-10-security-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/21/dell_wyse_thin_client_scores/){:target="_blank" rel="noopener"}

> Come on in and enjoy our unprotected FTP server and unsigned configuration files Dell, which pitches its Wyse ThinOS as "the most secure thin client operating system," plans to publish an advisory on Monday for two security vulnerabilities that are as bad as they could possibly be.... [...]
