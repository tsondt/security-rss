Title: Metro Vancouver's transit system hit by Egregor ransomware
Date: 2020-12-04T00:25:35
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: metro-vancouvers-transit-system-hit-by-egregor-ransomware

[Source](https://www.bleepingcomputer.com/news/security/metro-vancouvers-transit-system-hit-by-egregor-ransomware/){:target="_blank" rel="noopener"}

> The Egregor ransomware operation has breached Metro Vancouver's transportation agency TransLink with the cyberattack causing disruptions in services and payment systems. [...]
