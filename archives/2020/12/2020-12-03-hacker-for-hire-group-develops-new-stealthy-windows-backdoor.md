Title: Hacker-for-hire group develops new stealthy Windows backdoor
Date: 2020-12-03T11:57:08
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hacker-for-hire-group-develops-new-stealthy-windows-backdoor

[Source](https://www.bleepingcomputer.com/news/security/hacker-for-hire-group-develops-new-stealthy-windows-backdoor/){:target="_blank" rel="noopener"}

> Kaspersky researchers discovered a previously undocumented Windows PowerShell malware dubbed PowerPepper and developed by the hacker-for-hire group DeathStalker. [...]
