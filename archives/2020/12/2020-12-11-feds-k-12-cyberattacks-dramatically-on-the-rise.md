Title: Feds: K-12 Cyberattacks Dramatically on the Rise
Date: 2020-12-11T18:14:34+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware;Vulnerabilities;Web Security;alert;CISA;Cyberattacks;Cybersecurity and Infrastructure Security Agency;DDoS;distance learning;edtech;education;FBI;k-12;Phishing;ransomware;remote learning;schools;security advisory;zoom bomb
Slug: feds-k-12-cyberattacks-dramatically-on-the-rise

[Source](https://threatpost.com/feds-k12-cyberattacks-rise/162202/){:target="_blank" rel="noopener"}

> Attackers are targeting students and faculty alike with malware, phishing, DDoS, Zoom bombs and more, the FBI and CISA said. [...]
