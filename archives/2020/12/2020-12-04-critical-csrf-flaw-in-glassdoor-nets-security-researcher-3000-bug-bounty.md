Title: Critical CSRF flaw in Glassdoor nets security researcher $3,000 bug bounty
Date: 2020-12-04T14:53:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: critical-csrf-flaw-in-glassdoor-nets-security-researcher-3000-bug-bounty

[Source](https://portswigger.net/daily-swig/critical-csrf-flaw-in-glassdoor-nets-security-researcher-3-000-bug-bounty){:target="_blank" rel="noopener"}

> Researcher bypassed security defenses to edit jobseeker profiles, amend employer accounts, and more [...]
