Title: Koei Tecmo discloses data breach after hacker leaks stolen data
Date: 2020-12-26T13:51:17
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: koei-tecmo-discloses-data-breach-after-hacker-leaks-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/koei-tecmo-discloses-data-breach-after-hacker-leaks-stolen-data/){:target="_blank" rel="noopener"}

> Japanese game developer Koei Tecmo has disclosed a data breach and taken their European and American websites offline after stolen data was posted to a hacker forum. [...]
