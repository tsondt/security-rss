Title: On the Evolution of Ransomware
Date: 2020-12-30T12:33:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: on-the-evolution-of-ransomware

[Source](https://www.schneier.com/blog/archives/2020/12/on-the-evolution-of-ransomware.html){:target="_blank" rel="noopener"}

> Good article on the evolution of ransomware : Though some researchers say that the scale and severity of ransomware attacks crossed a bright line in 2020, others describe this year as simply the next step in a gradual and, unfortunately, predictable devolution. After years spent honing their techniques, attackers are growing bolder. They’ve begun to incorporate other types of extortion like blackmail into their arsenals, by exfiltrating an organization’s data and then threatening to release it if the victim doesn’t pay an additional fee. Most significantly, ransomware attackers have transitioned from a model in which they hit lots of individuals [...]
