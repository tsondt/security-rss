Title: Air-Gap Attack Turns Memory Modules into Wi-Fi Radios
Date: 2020-12-17T19:18:31+00:00
Author: Tom Spring
Category: Threatpost
Tags: Government;Hacks;IoT;Malware;802.11b/g/n;AIR-FI;Air-Gap Attack;Air-Gappped;DDR;DDR SDRAM buses;Mordechai Guri;Wi-Fi
Slug: air-gap-attack-turns-memory-modules-into-wi-fi-radios

[Source](https://threatpost.com/air-gap-attack-turns-memory-wifi/162358/){:target="_blank" rel="noopener"}

> Attack turns SDRAM buses into a Wi-Fi radio to leak data from air-gapped computers. [...]
