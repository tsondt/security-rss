Title: Censys: How a university project became a major commercial security platform
Date: 2020-12-02T13:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: censys-how-a-university-project-became-a-major-commercial-security-platform

[Source](https://portswigger.net/daily-swig/censys-how-a-university-project-became-a-major-commercial-security-platform){:target="_blank" rel="noopener"}

> Mapping organizations’ attack surface, one scan at a time [...]
