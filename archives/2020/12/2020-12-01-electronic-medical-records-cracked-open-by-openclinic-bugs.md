Title: Electronic Medical Records Cracked Open by OpenClinic Bugs
Date: 2020-12-01T16:57:07+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Bishop Fox;CVE-2020-28937;CVE-2020-28938;CVE-2020-28939;Electronic Medical Records;information disclosure;medical records management;Open Source;openclinic;patient data theft;personal health information;PHI;Security Vulnerabilities;unpatched
Slug: electronic-medical-records-cracked-open-by-openclinic-bugs

[Source](https://threatpost.com/electronic-medical-records-openclinic-bugs/161722/){:target="_blank" rel="noopener"}

> Four security vulnerabilities in an open-source medical records management platform allow remote code execution, patient data theft and more. [...]
