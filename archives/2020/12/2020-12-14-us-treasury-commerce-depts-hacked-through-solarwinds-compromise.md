Title: U.S. Treasury, Commerce Depts. Hacked Through SolarWinds Compromise
Date: 2020-12-14T16:26:43+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;The Coming Storm;APT29;Cybersecurity and Infrastructure Security Agency;Department of Commerce;FireEye hack;microsoft;Orion;Reuters;SolarWinds breach;U.S. Treasury Department
Slug: us-treasury-commerce-depts-hacked-through-solarwinds-compromise

[Source](https://krebsonsecurity.com/2020/12/u-s-treasury-commerce-depts-hacked-through-solarwinds-compromise/){:target="_blank" rel="noopener"}

> Communications at the U.S. Treasury and Commerce Departments were reportedly compromised by a supply chain attack on SolarWinds, a security vendor that helps the federal government and a range of Fortune 500 companies monitor the health of their IT networks. Given the breadth of the company’s customer base, experts say the incident may be just the first of many such disclosures. Some of SolarWinds’ customers. Source: solarwinds.com According to a Reuters story, hackers believed to be working for Russia have been monitoring internal email traffic at the U.S. Treasury and Commerce departments. Reuters reports the attackers were able to surreptitiously [...]
