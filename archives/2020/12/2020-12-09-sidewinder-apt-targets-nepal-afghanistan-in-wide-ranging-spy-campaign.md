Title: SideWinder APT Targets Nepal, Afghanistan in Wide-Ranging Spy Campaign
Date: 2020-12-09T19:53:13+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Privacy;Vulnerabilities;Web Security;advanced persistent threat;Afghanistan;apt;backdoor;CVE-2017-11882;CVE-2019-2215;Cyberattacks;email;Email credentials;espionage;everest;malware;mediatek-su;Nepal;Phishing;sidewinder;Trend Micro
Slug: sidewinder-apt-targets-nepal-afghanistan-in-wide-ranging-spy-campaign

[Source](https://threatpost.com/sidewinder-apt-nepal-afghanistan-spy-campaign/162086/){:target="_blank" rel="noopener"}

> Convincing email-credentials phishing, emailed backdoors and mobile apps are all part of the groups latest effort against military and government targets. [...]
