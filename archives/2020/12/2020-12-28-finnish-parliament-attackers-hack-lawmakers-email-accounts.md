Title: Finnish Parliament attackers hack lawmakers’ email accounts
Date: 2020-12-28T13:46:45
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: finnish-parliament-attackers-hack-lawmakers-email-accounts

[Source](https://www.bleepingcomputer.com/news/security/finnish-parliament-attackers-hack-lawmakers-email-accounts/){:target="_blank" rel="noopener"}

> The email accounts of multiple members of parliament (MPs) were compromised following a cyberattack as revealed today by the Parliament of Finland. [...]
