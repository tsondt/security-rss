Title: Use a single AWS Managed Microsoft AD for Amazon RDS for SQL Server instances in multiple Regions
Date: 2020-12-14T20:42:23+00:00
Author: Jeremy Girven
Category: AWS Security
Tags: Amazon RDS;AWS Directory Service;Intermediate (200);RDS for SQL Server;Security, Identity, & Compliance;AWS Managed Microsoft AD;RDS for SQL;Security Blog
Slug: use-a-single-aws-managed-microsoft-ad-for-amazon-rds-for-sql-server-instances-in-multiple-regions

[Source](https://aws.amazon.com/blogs/security/use-a-single-aws-managed-microsoft-ad-for-amazon-rds-for-sql-server-instances-in-multiple-regions/){:target="_blank" rel="noopener"}

> Many Amazon Web Services (AWS) customers use Active Directory to centralize user authentication and authorization for a variety of applications and services. For these customers, Active Directory is a critical piece of their IT infrastructure. AWS offers AWS Directory Service for Microsoft Active Directory, also known as AWS Managed Microsoft AD, to provide a highly [...]
