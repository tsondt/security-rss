Title: Friday Squid Blogging: Bigfin Squid Found in Australian Waters
Date: 2020-12-04T22:11:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-bigfin-squid-found-in-australian-waters

[Source](https://www.schneier.com/blog/archives/2020/12/friday-squid-blogging-bigfin-squid-found-in-australian-waters.html){:target="_blank" rel="noopener"}

> A bigfin squid has been found — and filmed — in Australian waters for the first time. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
