Title: New SUPERNOVA backdoor found in SolarWinds cyberattack analysis
Date: 2020-12-21T09:17:57
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: new-supernova-backdoor-found-in-solarwinds-cyberattack-analysis

[Source](https://www.bleepingcomputer.com/news/security/new-supernova-backdoor-found-in-solarwinds-cyberattack-analysis/){:target="_blank" rel="noopener"}

> While analyzing artifacts from the SolarWinds Orion supply-chain attack, security researchers discovered another backdoor that is likely from a second threat actor. [...]
