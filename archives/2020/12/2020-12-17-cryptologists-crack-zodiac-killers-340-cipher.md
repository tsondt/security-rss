Title: Cryptologists Crack Zodiac Killer’s 340 Cipher
Date: 2020-12-17T17:30:15+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cryptography;340 cipher;Cipher;cryptography;Cybersecurity;solved;Zodiac;Zodiac cipher code breaker;zodiac serial killer
Slug: cryptologists-crack-zodiac-killers-340-cipher

[Source](https://threatpost.com/cryptologists-zodiac-killer-340-cipher/162353/){:target="_blank" rel="noopener"}

> The Zodiac’s serial killer’s 340 cipher, which couldn’t be solved for 50 years, has been cracked by a remote team of mathematicians. [...]
