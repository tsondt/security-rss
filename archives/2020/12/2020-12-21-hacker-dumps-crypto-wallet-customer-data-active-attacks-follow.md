Title: Hacker Dumps Crypto Wallet Customer Data; Active Attacks Follow
Date: 2020-12-21T16:39:50+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Privacy
Slug: hacker-dumps-crypto-wallet-customer-data-active-attacks-follow

[Source](https://threatpost.com/ledger-dump-active-attacks-follow/162477/){:target="_blank" rel="noopener"}

> Customer data from a June attack against cryptocurrency wallet firm Ledger is now public and actively being used in attacks. [...]
