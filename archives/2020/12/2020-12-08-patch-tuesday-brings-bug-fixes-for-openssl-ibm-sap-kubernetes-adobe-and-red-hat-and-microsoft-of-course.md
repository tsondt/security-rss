Title: Patch Tuesday brings bug fixes for OpenSSL, IBM, SAP, Kubernetes, Adobe, and Red Hat. And Microsoft, of course
Date: 2020-12-08T22:17:56+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: patch-tuesday-brings-bug-fixes-for-openssl-ibm-sap-kubernetes-adobe-and-red-hat-and-microsoft-of-course

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/08/patch_tuesday_fixes/){:target="_blank" rel="noopener"}

> Light load from Redmond as everyone else seeks to bury bad news, sorry, align in update cadence Patch Tuesday For December's Patch Tuesday bug bonanza, Microsoft handed out fixes for a mere 58 vulnerabilities while various other orgs addressed shortcomings in their own software in separate, parallel announcements.... [...]
