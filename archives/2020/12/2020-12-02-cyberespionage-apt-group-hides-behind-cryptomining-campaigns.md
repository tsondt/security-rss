Title: Cyberespionage APT group hides behind cryptomining campaigns
Date: 2020-12-02T03:25:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: cyberespionage-apt-group-hides-behind-cryptomining-campaigns

[Source](https://www.bleepingcomputer.com/news/security/cyberespionage-apt-group-hides-behind-cryptomining-campaigns/){:target="_blank" rel="noopener"}

> An advanced threat group called Bismuth recently used cryptocurrency mining as a way to hide the purpose of their activity and to avoid triggering high-priority alerts. [...]
