Title: Open Source Does Not Equal Secure
Date: 2020-12-03T17:21:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;open source;security engineering
Slug: open-source-does-not-equal-secure

[Source](https://www.schneier.com/blog/archives/2020/12/open-source-does-not-equal-secure.html){:target="_blank" rel="noopener"}

> Way back in 1999, I wrote about open-source software: First, simply publishing the code does not automatically mean that people will examine it for security flaws. Security researchers are fickle and busy people. They do not have the time to examine every piece of source code that is published. So while opening up source code is a good thing, it is not a guarantee of security. I could name a dozen open source security libraries that no one has ever heard of, and no one has ever evaluated. On the other hand, the security code in Linux has been looked [...]
