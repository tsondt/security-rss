Title: Adobe releases final Flash Player update, warns of 2021 kill switch
Date: 2020-12-12T10:02:02
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: adobe-releases-final-flash-player-update-warns-of-2021-kill-switch

[Source](https://www.bleepingcomputer.com/news/software/adobe-releases-final-flash-player-update-warns-of-2021-kill-switch/){:target="_blank" rel="noopener"}

> After 24 years of fun games and abuse by threat actors, Adobe has released their final Flash Player update and thanked everyone for the fantastic content that they have released over the years. [...]
