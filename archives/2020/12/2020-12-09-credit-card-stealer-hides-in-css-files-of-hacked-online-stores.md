Title: Credit card stealer hides in CSS files of hacked online stores
Date: 2020-12-09T11:38:46
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: credit-card-stealer-hides-in-css-files-of-hacked-online-stores

[Source](https://www.bleepingcomputer.com/news/security/credit-card-stealer-hides-in-css-files-of-hacked-online-stores/){:target="_blank" rel="noopener"}

> Credit card stealer scripts are evolving and become increasingly harder to detect due to novel hiding tactics. The latest example is a web skimmer that uses CSS code to blend within the pages of a compromised store and to steal customers' personal and payment information. [...]
