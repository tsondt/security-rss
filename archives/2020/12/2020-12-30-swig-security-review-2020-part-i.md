Title: Swig Security Review 2020 – Part I
Date: 2020-12-30T15:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: swig-security-review-2020-part-i

[Source](https://portswigger.net/daily-swig/swig-security-review-2020-part-i){:target="_blank" rel="noopener"}

> Key thinkers on the biggest security stories and trends in 2020 [...]
