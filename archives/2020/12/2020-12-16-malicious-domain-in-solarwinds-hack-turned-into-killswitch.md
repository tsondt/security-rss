Title: Malicious Domain in SolarWinds Hack Turned into ‘Killswitch’
Date: 2020-12-16T18:37:47+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;FireEye;GoDaddy;microsoft;Orion;RedDrip Team;SolarWinds breach;SUNBURST
Slug: malicious-domain-in-solarwinds-hack-turned-into-killswitch

[Source](https://krebsonsecurity.com/2020/12/malicious-domain-in-solarwinds-hack-turned-into-killswitch/){:target="_blank" rel="noopener"}

> A key malicious domain name used to control potentially thousands of computer systems compromised via the months-long breach at network monitoring software vendor SolarWinds was commandeered by security experts and used as a “killswitch” designed to turn the sprawling cybercrime operation against itself, KrebsOnSecurity has learned. Austin, Texas-based SolarWinds disclosed this week that a compromise of its software update servers earlier this year may have resulted in malicious code being pushed to nearly 18,000 customers of its Orion platform. Many U.S. federal agencies and Fortune 500 firms use(d) Orion to monitor the health of their IT networks. On Dec. 13, [...]
