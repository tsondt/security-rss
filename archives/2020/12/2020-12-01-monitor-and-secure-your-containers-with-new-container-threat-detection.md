Title: Monitor and secure your containers with new Container Threat Detection
Date: 2020-12-01T17:00:00+00:00
Author: Tim Peacock
Category: GCP Security
Tags: Containers & Kubernetes;Google Cloud Platform;Identity & Security
Slug: monitor-and-secure-your-containers-with-new-container-threat-detection

[Source](https://cloud.google.com/blog/products/identity-security/container-threat-detection-is-ga/){:target="_blank" rel="noopener"}

> As more containerized workloads find their way into your organization, you want to be able to detect and respond to threats to containers running in this environment. Today, we’re excited to announce the general availability of Container Threat Detection to help you monitor and secure your container deployments in Google Cloud. Container Threat Detection is a built-in service in Security Command Center Premium tier. Container Threat Detection detects the most common container runtime attacks and alerts you to any suspicious activity. This release includes multiple new detection capabilities and provides an API. Here are the key findings that are identified [...]
