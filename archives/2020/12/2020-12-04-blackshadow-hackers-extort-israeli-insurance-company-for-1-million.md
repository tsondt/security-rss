Title: BlackShadow hackers extort Israeli insurance company for $1 million
Date: 2020-12-04T02:02:02
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: blackshadow-hackers-extort-israeli-insurance-company-for-1-million

[Source](https://www.bleepingcomputer.com/news/security/blackshadow-hackers-extort-israeli-insurance-company-for-1-million/){:target="_blank" rel="noopener"}

> Threat actors are extorting an Israeli insurance company by demanding almost $1 million in bitcoin to stop leaking the company's stolen data. [...]
