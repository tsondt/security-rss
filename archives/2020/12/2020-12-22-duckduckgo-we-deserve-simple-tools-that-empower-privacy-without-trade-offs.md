Title: DuckDuckGo: ‘We deserve simple tools that empower privacy, without trade-offs’
Date: 2020-12-22T15:12:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: duckduckgo-we-deserve-simple-tools-that-empower-privacy-without-trade-offs

[Source](https://portswigger.net/daily-swig/duckduckgo-we-deserve-simple-tools-that-empower-privacy-without-trade-offs){:target="_blank" rel="noopener"}

> DuckDuckGo’s Daniel Davis discusses the privacy-focused search engine’s future in the market [...]
