Title: Safe-Inet: VPN service for cybercriminals taken down in law enforcement bust
Date: 2020-12-22T18:27:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: safe-inet-vpn-service-for-cybercriminals-taken-down-in-law-enforcement-bust

[Source](https://portswigger.net/daily-swig/safe-inet-vpn-service-for-cybercriminals-taken-down-in-law-enforcement-bust){:target="_blank" rel="noopener"}

> Users including suspected ransomware slingers to be targeted in follow-up investigations [...]
