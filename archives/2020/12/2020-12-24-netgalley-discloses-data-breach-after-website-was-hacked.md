Title: NetGalley discloses data breach after website was hacked
Date: 2020-12-24T10:20:49
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: netgalley-discloses-data-breach-after-website-was-hacked

[Source](https://www.bleepingcomputer.com/news/security/netgalley-discloses-data-breach-after-website-was-hacked/){:target="_blank" rel="noopener"}

> The NetGalley book promotion site has suffered a data breach that allowed threat actors to access a database with members' personal information. [...]
