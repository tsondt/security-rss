Title: Healthcare in Crisis: Diagnosing Cybersecurity Shortcomings in Unprecedented Times
Date: 2020-12-07T17:03:20+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Cloud Security;Malware;Privacy;Vulnerabilities;Web Security;COVID-19;Healthcare;medical cybersecurity;MiniMed insulin pump;Pandemic;patient data privacy;ransomware
Slug: healthcare-in-crisis-diagnosing-cybersecurity-shortcomings-in-unprecedented-times

[Source](https://threatpost.com/healthcare-in-crisis-diagnosing-cybersecurity-shortcomings-in-unprecedented-times/161917/){:target="_blank" rel="noopener"}

> In the early fog of the COVID-19 pandemic, cybersecurity took a back seat to keeping patients alive. Lost in the chaos was IT security. [...]
