Title: Voyager cryptocurrency broker halted trading due to cyberattack
Date: 2020-12-29T12:54:37
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency
Slug: voyager-cryptocurrency-broker-halted-trading-due-to-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/voyager-cryptocurrency-broker-halted-trading-due-to-cyberattack/){:target="_blank" rel="noopener"}

> The Voyager cryptocurrency brokerage platform halted trading yesterday after suffering a cyberattack targeting their DNS configuration. [...]
