Title: Detecting sensitive data in DynamoDB with Macie
Date: 2020-12-11T16:39:05+00:00
Author: Sheldon Sides
Category: AWS Security
Tags: Advanced (300);Amazon DynamoDB;Amazon Macie;Security, Identity, & Compliance;Security Blog
Slug: detecting-sensitive-data-in-dynamodb-with-macie

[Source](https://aws.amazon.com/blogs/security/detecting-sensitive-data-in-dynamodb-with-macie/){:target="_blank" rel="noopener"}

> Amazon Macie is a fully managed data security and data privacy service that uses machine learning and pattern matching to discover and protect your sensitive data in Amazon Web Services (AWS). It gives you the ability to automatically scan for sensitive data and get an inventory of your Amazon Simple Storage Service (Amazon S3) buckets. [...]
