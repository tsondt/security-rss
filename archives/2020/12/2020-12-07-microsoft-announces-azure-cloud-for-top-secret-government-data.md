Title: Microsoft announces Azure cloud for top secret government data
Date: 2020-12-07T11:30:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-announces-azure-cloud-for-top-secret-government-data

[Source](https://www.bleepingcomputer.com/news/security/microsoft-announces-azure-cloud-for-top-secret-government-data/){:target="_blank" rel="noopener"}

> Microsoft today announced the launch of a new offering for its mission-critical Azure Government cloud targeted at government customers and partners that regularly work with top-secret classified data. [...]
