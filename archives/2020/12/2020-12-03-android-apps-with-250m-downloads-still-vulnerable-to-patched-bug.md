Title: Android apps with 250M downloads still vulnerable to patched bug
Date: 2020-12-03T06:00:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google
Slug: android-apps-with-250m-downloads-still-vulnerable-to-patched-bug

[Source](https://www.bleepingcomputer.com/news/security/android-apps-with-250m-downloads-still-vulnerable-to-patched-bug/){:target="_blank" rel="noopener"}

> Android apps with over 250 million downloads are still susceptible to a severe vulnerability in a Google library that was patched in August 2020. [...]
