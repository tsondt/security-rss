Title: Google Play Apps Remain Vulnerable to High-Severity Flaw
Date: 2020-12-03T11:00:10+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;Android;bumble;cisco teams;CVE-2020-8913;Edge;google;google play;google play core library;Grindr;vulnerability
Slug: google-play-apps-remain-vulnerable-to-high-severity-flaw

[Source](https://threatpost.com/google-play-apps-remain-vulnerable-to-high-severity-flaw/161785/){:target="_blank" rel="noopener"}

> Patches for a flaw (CVE-2020-8913) in the Google Play Core Library have not been implemented by several popular Google Play apps, including Cisco Teams and Edge. [...]
