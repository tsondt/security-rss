Title: Google invites security researchers to collaborate on new XS-Leaks wiki platform
Date: 2020-12-07T13:38:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-invites-security-researchers-to-collaborate-on-new-xs-leaks-wiki-platform

[Source](https://portswigger.net/daily-swig/google-invites-security-researchers-to-collaborate-on-new-xs-leaks-wiki-platform){:target="_blank" rel="noopener"}

> Knowledge-sharing website aims to help developers protect against side-channel browser attacks [...]
