Title: New worm turns Windows, Linux servers into Monero miners
Date: 2020-12-30T09:40:36
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency;Linux
Slug: new-worm-turns-windows-linux-servers-into-monero-miners

[Source](https://www.bleepingcomputer.com/news/security/new-worm-turns-windows-linux-servers-into-monero-miners/){:target="_blank" rel="noopener"}

> A newly discovered and self-spreading Golang-based malware has been actively dropping XMRig cryptocurrency miners on Windows and Linux servers since early December. [...]
