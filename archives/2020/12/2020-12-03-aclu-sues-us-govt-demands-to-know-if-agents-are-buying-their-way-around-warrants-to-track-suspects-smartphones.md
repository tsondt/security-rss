Title: ACLU sues US govt, demands to know if agents are buying their way around warrants to track suspects' smartphones
Date: 2020-12-03T13:30:12+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: aclu-sues-us-govt-demands-to-know-if-agents-are-buying-their-way-around-warrants-to-track-suspects-smartphones

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/03/aclu_phone_location/){:target="_blank" rel="noopener"}

> 'Cos that would be against the Fourth Amendment, legal team says The American Civil Liberties Union (ACLU) has sued the US government, claiming Homeland Security agents trampled over people's constitutional rights – by buying phone location data from commercial brokers rather than getting necessary search warrants.... [...]
