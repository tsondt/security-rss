Title: US Treasury, Dept of Commerce hacks linked to SolarWinds IT monitoring software supply-chain attack
Date: 2020-12-14T02:10:47+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: us-treasury-dept-of-commerce-hacks-linked-to-solarwinds-it-monitoring-software-supply-chain-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/14/solarwinds_fireeye_cozybear_us_government/){:target="_blank" rel="noopener"}

> Russia's Cozy Bear fingered, FireEye details injected backdoor and says it's worldwide Updated SolarWinds' Orion IT monitoring platform has been compromised, and speculation is swirling it was used as a base camp by state-backed hackers to infiltrate major US government organizations.... [...]
