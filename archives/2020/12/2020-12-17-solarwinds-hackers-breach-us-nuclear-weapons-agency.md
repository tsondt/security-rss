Title: SolarWinds hackers breach US nuclear weapons agency
Date: 2020-12-17T16:29:23
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Government
Slug: solarwinds-hackers-breach-us-nuclear-weapons-agency

[Source](https://www.bleepingcomputer.com/news/security/solarwinds-hackers-breach-us-nuclear-weapons-agency/){:target="_blank" rel="noopener"}

> Nation-state hackers have breached the networks of the National Nuclear Security Administration (NNSA) and the US Department of Energy (DOE). [...]
