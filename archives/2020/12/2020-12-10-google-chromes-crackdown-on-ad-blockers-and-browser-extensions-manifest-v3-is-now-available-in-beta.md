Title: Google Chrome's crackdown on ad blockers and browser extensions, Manifest v3, is now available in beta
Date: 2020-12-10T08:27:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: google-chromes-crackdown-on-ad-blockers-and-browser-extensions-manifest-v3-is-now-available-in-beta

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/10/googles_browser_extension_platform_rewrite/){:target="_blank" rel="noopener"}

> Web advertising giant says it has been working with filter makers, others to 'evolve the platform' Google, which makes most of its money from online ads, insists it wants ad blockers to continue working under the latest, more locked-down iteration of its Chrome browser extension platform, known as Manifest v3.... [...]
