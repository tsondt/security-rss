Title: 45 Million Medical Images Left Exposed Online
Date: 2020-12-15T17:36:17+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;CT scans;data leak;DICOM;medical data;mri;network attached storage;personal healthcare information;PHI;PII;sensitive data;X-ray
Slug: 45-million-medical-images-left-exposed-online

[Source](https://threatpost.com/million-medical-images-online/162284/){:target="_blank" rel="noopener"}

> A six-month investigation by CybelAngel discovered unsecured sensitive patient data available for third parties to access for blackmail, fraud or other nefarious purposes. [...]
