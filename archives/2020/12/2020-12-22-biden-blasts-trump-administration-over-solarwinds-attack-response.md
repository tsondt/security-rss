Title: Biden blasts Trump administration over SolarWinds attack response
Date: 2020-12-22T17:20:11
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Government
Slug: biden-blasts-trump-administration-over-solarwinds-attack-response

[Source](https://www.bleepingcomputer.com/news/security/biden-blasts-trump-administration-over-solarwinds-attack-response/){:target="_blank" rel="noopener"}

> U.S. President-Elect Joe Biden has criticized the Trump administration over the lack of response regarding the SolarWinds response and for failing to officially attribute the attacks. [...]
