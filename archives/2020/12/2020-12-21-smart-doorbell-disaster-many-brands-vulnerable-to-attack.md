Title: Smart Doorbell Disaster: Many Brands Vulnerable to Attack
Date: 2020-12-21T21:35:58+00:00
Author: Tom Spring
Category: Threatpost
Tags: Hacks;IoT;Vulnerabilities;360 D819 Smart Video Doorbell;Accfly;digital doorbells;Docooler;Extaum;hard coded credential;HTTP;Qihoo;Smart WiFi Doorbell;Tickas;VD300;Victure;XF-IP007H
Slug: smart-doorbell-disaster-many-brands-vulnerable-to-attack

[Source](https://threatpost.com/smart-doorbell-vulnerable-to-attack/162527/){:target="_blank" rel="noopener"}

> Investigation reveals device sector is problem plagued when it comes to security bugs. [...]
