Title: Zero-day vulnerabilities in healthcare records application OpenClinic could expose patients’ test results
Date: 2020-12-02T15:54:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: zero-day-vulnerabilities-in-healthcare-records-application-openclinic-could-expose-patients-test-results

[Source](https://portswigger.net/daily-swig/zero-day-vulnerabilities-in-healthcare-records-application-openclinic-could-expose-patients-test-results){:target="_blank" rel="noopener"}

> Users urged to abandon open source application after maintainers fail to respond to disclosure [...]
