Title: UK proposes new powers for comms regulator to legally unleash avenging hordes on security-breached telcos
Date: 2020-12-16T12:32:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-proposes-new-powers-for-comms-regulator-to-legally-unleash-avenging-hordes-on-security-breached-telcos

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/16/telco_security_bill_sueball_power/){:target="_blank" rel="noopener"}

> Suffered 'loss or damage' as a customer? Get Ofcom's permission and sue away Britain's Telecommunications Security Bill will allow anyone to sue their telco if they suffer "loss or damage" as a result of a system breach – but only if they get Ofcom's permission.... [...]
