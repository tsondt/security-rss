Title: Hacker given three years for stealing secret Nintendo Switch blueprints, collecting child sex abuse vids
Date: 2020-12-02T20:54:23+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: hacker-given-three-years-for-stealing-secret-nintendo-switch-blueprints-collecting-child-sex-abuse-vids

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/02/nintendo_hacker_prison/){:target="_blank" rel="noopener"}

> Ryan Hernandez continued to raid gaming giant's systems even after FBI gave him a warning A young man caught hacking into Nintendo’s servers to steal secret Switch blueprints has been sentenced to three years in prison after ignoring an FBI warning to stop.... [...]
