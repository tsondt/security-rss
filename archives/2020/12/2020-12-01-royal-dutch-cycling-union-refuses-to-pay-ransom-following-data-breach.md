Title: Royal Dutch Cycling Union refuses to pay ransom following data breach
Date: 2020-12-01T14:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: royal-dutch-cycling-union-refuses-to-pay-ransom-following-data-breach

[Source](https://portswigger.net/daily-swig/royal-dutch-cycling-union-refuses-to-pay-ransom-following-data-breach){:target="_blank" rel="noopener"}

> The Netherlands’ national governing body for cycling has urged members to update their login credentials [...]
