Title: Outing of FSB hit squad highlights Russia's data security problem
Date: 2020-12-16T17:53:17+00:00
Author: Andrew Roth in Moscow
Category: The Guardian
Tags: Russia;Cybercrime;Europe;World news;Internet;Technology;Data and computer security
Slug: outing-of-fsb-hit-squad-highlights-russias-data-security-problem

[Source](https://www.theguardian.com/world/2020/dec/16/outing-of-fsb-hit-squad-highlights-russias-data-security-problem){:target="_blank" rel="noopener"}

> Analysis: trade in stolen data is a boon for investigators and a headache for Kremlin In early 2019, the journalist Andrei Zakharov managed to buy his own phone and banking records in a groundbreaking investigation into Russia’s thriving markets in stolen personal data, in which law enforcement and telecoms employees can be contracted anonymously to dip into their systems and pull out sensitive details on anyone. A year and a half later, investigators from Bellingcat and the Insider used some of the same tools and clever analysis to out a secret FSB team that had been tasked with killing Alexei [...]
