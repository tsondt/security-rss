Title: Enigma Machine Recovered from the Baltic Sea
Date: 2020-12-04T15:18:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Enigma;history of cryptography
Slug: enigma-machine-recovered-from-the-baltic-sea

[Source](https://www.schneier.com/blog/archives/2020/12/enigma-machine-recovered-from-the-baltic-sea.html){:target="_blank" rel="noopener"}

> Neat story : German divers searching the Baltic Sea for discarded fishing nets have stumbled upon a rare Enigma cipher machine used by the Nazi military during World War Two which they believe was thrown overboard from a scuttled submarine. Thinking they had discovered a typewriter entangled in a net on the seabed of Gelting Bay, underwater archaeologist Florian Huber quickly realised the historical significance of the find. EDITED TO ADD: Slashdot thread. [...]
