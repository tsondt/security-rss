Title: FreePBX developer Sangoma hit with Conti ransomware attack
Date: 2020-12-24T13:12:37
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: freepbx-developer-sangoma-hit-with-conti-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/freepbx-developer-sangoma-hit-with-conti-ransomware-attack/){:target="_blank" rel="noopener"}

> Sangoma disclosed a data breach after files were stolen during a recent Conti ransomware attack and published online. [...]
