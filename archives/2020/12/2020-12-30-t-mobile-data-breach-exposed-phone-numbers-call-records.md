Title: T-Mobile data breach exposed phone numbers, call records
Date: 2020-12-30T12:04:12
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Mobile
Slug: t-mobile-data-breach-exposed-phone-numbers-call-records

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-data-breach-exposed-phone-numbers-call-records/){:target="_blank" rel="noopener"}

> T-Mobile has announced a data breach exposing customers' proprietary network information (CPNI), including phone numbers and call records. [...]
