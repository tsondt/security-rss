Title: Oblivious DNS-over-HTTPS
Date: 2020-12-08T21:02:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;anonymity;DNS;https;protocols
Slug: oblivious-dns-over-https

[Source](https://www.schneier.com/blog/archives/2020/12/oblivious-dns-over-https.html){:target="_blank" rel="noopener"}

> This new protocol, called Oblivious DNS-over-HTTPS (ODoH), hides the websites you visit from your ISP. Here’s how it works: ODoH wraps a layer of encryption around the DNS query and passes it through a proxy server, which acts as a go-between the internet user and the website they want to visit. Because the DNS query is encrypted, the proxy can’t see what’s inside, but acts as a shield to prevent the DNS resolver from seeing who sent the query to begin with. IETF memo. The paper : Abstract: The Domain Name System (DNS) is the foundation of a human-usable Internet, [...]
