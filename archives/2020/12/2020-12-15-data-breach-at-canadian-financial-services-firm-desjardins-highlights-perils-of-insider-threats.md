Title: Data breach at Canadian financial services firm Desjardins highlights perils of insider threats
Date: 2020-12-15T15:43:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-at-canadian-financial-services-firm-desjardins-highlights-perils-of-insider-threats

[Source](https://portswigger.net/daily-swig/data-breach-at-canadian-financial-services-firm-desjardins-highlights-perils-of-insider-threats){:target="_blank" rel="noopener"}

> ‘Malicious’ employee stole 10 million Canadians’ sensitive information over two-year period [...]
