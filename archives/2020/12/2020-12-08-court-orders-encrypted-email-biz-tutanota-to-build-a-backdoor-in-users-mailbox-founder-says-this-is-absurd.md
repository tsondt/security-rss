Title: Court orders encrypted email biz Tutanota to build a backdoor in user's mailbox, founder says 'this is absurd'
Date: 2020-12-08T21:07:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: court-orders-encrypted-email-biz-tutanota-to-build-a-backdoor-in-users-mailbox-founder-says-this-is-absurd

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/08/tutanota_backdoor_court_order/){:target="_blank" rel="noopener"}

> Plus: Yet another UK.gov bod demands end-to-end encryption is broken Tutanota has been served with a court order to backdoor its encrypted email service – a situation founder Matthias Pfau described to The Register as "absurd."... [...]
