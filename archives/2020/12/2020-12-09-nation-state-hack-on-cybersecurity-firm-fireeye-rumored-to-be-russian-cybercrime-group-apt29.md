Title: ‘Nation-state hack’ on cybersecurity firm FireEye rumored to be Russian cybercrime group APT29
Date: 2020-12-09T16:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nation-state-hack-on-cybersecurity-firm-fireeye-rumored-to-be-russian-cybercrime-group-apt29

[Source](https://portswigger.net/daily-swig/nation-state-hack-on-cybersecurity-firm-fireeye-rumored-to-be-russian-cybercrime-group-apt29){:target="_blank" rel="noopener"}

> Cozy Bear threat actors suspected in ‘top-tier offensive’ [...]
