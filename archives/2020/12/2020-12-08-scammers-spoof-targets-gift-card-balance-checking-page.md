Title: Scammers spoof Target's gift card balance checking page
Date: 2020-12-08T11:03:52
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: scammers-spoof-targets-gift-card-balance-checking-page

[Source](https://www.bleepingcomputer.com/news/security/scammers-spoof-targets-gift-card-balance-checking-page/){:target="_blank" rel="noopener"}

> It's the giving season, and cybercriminals are more actively looking to steal gift cards. One of the most popular brands in their sight is giant retailer Target. [...]
