Title: US nuke agency hacked by suspected Russian SolarWinds spies, Microsoft also installed backdoor
Date: 2020-12-18T01:59:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: us-nuke-agency-hacked-by-suspected-russian-solarwinds-spies-microsoft-also-installed-backdoor

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/18/solarwinds_nnsa_microsoft_cisa/){:target="_blank" rel="noopener"}

> Windows giant, nuclear administration play down danger – and kill switch found and activated America's nuclear weapons agency was hacked by the suspected Russian spies who backdoored SolarWinds' IT monitoring software and compromised several US government bodies, and Microsoft was caught up in the same cyber-storm, too, it was reported Thursday.... [...]
