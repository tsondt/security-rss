Title: Weak authentication created backdoor risk for D-Link routers
Date: 2020-12-17T17:00:03+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: weak-authentication-created-backdoor-risk-for-d-link-routers

[Source](https://portswigger.net/daily-swig/weak-authentication-created-backdoor-risk-for-d-link-routers){:target="_blank" rel="noopener"}

> Details of recently patched vulnerabilities laid bare [...]
