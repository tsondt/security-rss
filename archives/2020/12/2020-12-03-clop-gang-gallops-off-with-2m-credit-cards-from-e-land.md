Title: Clop Gang Gallops Off with 2M Credit Cards from E-Land
Date: 2020-12-03T14:27:32+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Hacks;Malware;Web Security;clop ransomware;Credit Card Theft;data leak;data theft;E-Land;E-Land Global;NC Department Store;New Core;point of sale malware;pos;ransomware;ransomware gang;South Korea
Slug: clop-gang-gallops-off-with-2m-credit-cards-from-e-land

[Source](https://threatpost.com/clop-gang-2m-credit-cards-eland/161833/){:target="_blank" rel="noopener"}

> The ransomware group pilfered payment-card data and credentials for over a year, before ending with an attack last month that shut down many of the South Korean retailer’s stores. [...]
