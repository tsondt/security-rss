Title: AWS extends its TISAX scope to cover the London and Paris Regions
Date: 2020-12-17T17:22:15+00:00
Author: Clara Lim
Category: AWS Security
Tags: Announcements;Automotive;Foundational (100);Security, Identity, & Compliance;automotive;automotive industry;Security Blog;TISAX
Slug: aws-extends-its-tisax-scope-to-cover-the-london-and-paris-regions

[Source](https://aws.amazon.com/blogs/security/aws-extends-tisax-scope-to-cover-london-paris-regions/){:target="_blank" rel="noopener"}

> We’re excited to announce the completion of Trusted Information Security Assessment Exchange (TISAX) certification on December 08, 2020 for the London and Paris regions. These regions were assessed at the HIGH protection level (AL 2) for the control domains Information Handling and Data Protection, according to article 28 (“Processor”) of the European General Data Protection [...]
