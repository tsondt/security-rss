Title: Payment Processing Giant TSYS: Ransomware Incident “Immaterial” to Company
Date: 2020-12-10T17:45:36+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Ransomware;Conti;fs-isac;global payments;Ryuk;Total Systems Services Inc.;TSYS
Slug: payment-processing-giant-tsys-ransomware-incident-immaterial-to-company

[Source](https://krebsonsecurity.com/2020/12/payment-processing-giant-tsys-ransomware-incident-immaterial-to-company/){:target="_blank" rel="noopener"}

> Payment card processing giant TSYS suffered a ransomware attack earlier this month. Since then reams of data stolen from the company have been posted online, with the attackers promising to publish more in the coming days. But the company says the malware did not jeopardize card data, and that the incident was limited to administrative areas of its business. Headquartered in Columbus, Ga., Total System Services Inc. (TSYS) is the third-largest third-party payment processor for financial institutions in North America, and a major processor in Europe. TSYS provides payment processing services, merchant services and other payment solutions, including prepaid debit [...]
