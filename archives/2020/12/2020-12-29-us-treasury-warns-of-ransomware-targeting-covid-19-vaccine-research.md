Title: US Treasury warns of ransomware targeting COVID-19 vaccine research
Date: 2020-12-29T09:18:56
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-treasury-warns-of-ransomware-targeting-covid-19-vaccine-research

[Source](https://www.bleepingcomputer.com/news/security/us-treasury-warns-of-ransomware-targeting-covid-19-vaccine-research/){:target="_blank" rel="noopener"}

> The US Treasury Department's Financial Crimes Enforcement Network (FinCEN) warned financial institutions of ransomware actively targeting vaccine research organizations. [...]
