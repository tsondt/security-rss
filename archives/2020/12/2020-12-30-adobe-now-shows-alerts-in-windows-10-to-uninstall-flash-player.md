Title: Adobe now shows alerts in Windows 10 to uninstall Flash Player
Date: 2020-12-30T17:35:33
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software;Microsoft
Slug: adobe-now-shows-alerts-in-windows-10-to-uninstall-flash-player

[Source](https://www.bleepingcomputer.com/news/software/adobe-now-shows-alerts-in-windows-10-to-uninstall-flash-player/){:target="_blank" rel="noopener"}

> With the Flash Player officially reaching the end of life tomorrow, Adobe has started to display alerts on Windows computers recommending that users uninstall Flash Player. [...]
