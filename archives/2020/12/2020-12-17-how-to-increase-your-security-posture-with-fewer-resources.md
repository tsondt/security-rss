Title: How to Increase Your Security Posture with Fewer Resources
Date: 2020-12-17T23:11:27+00:00
Author: Justin Jett
Category: Threatpost
Tags: InfoSec Insider;Automation;do more with less;education;infosec insider;justin jett;machine learning;plixer;ransomware;schools;security resources;user awareness
Slug: how-to-increase-your-security-posture-with-fewer-resources

[Source](https://threatpost.com/increase-security-posture-fewer-resources/162382/){:target="_blank" rel="noopener"}

> Plixer's Justin Jett, Compliance & Audit director, discusses how to do more with less when your security resources are thin. [...]
