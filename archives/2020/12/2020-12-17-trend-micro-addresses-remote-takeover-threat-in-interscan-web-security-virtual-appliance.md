Title: Trend Micro addresses remote takeover threat in InterScan Web Security Virtual Appliance
Date: 2020-12-17T14:05:02+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: trend-micro-addresses-remote-takeover-threat-in-interscan-web-security-virtual-appliance

[Source](https://portswigger.net/daily-swig/trend-micro-addresses-remote-takeover-threat-in-interscan-web-security-virtual-appliance){:target="_blank" rel="noopener"}

> Security researcher chained flaws to seize control of aged appliance [...]
