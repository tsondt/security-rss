Title: Critical Steam Flaws Could Let Gamers to Crash Opponents’ Computers
Date: 2020-12-10T11:00:46+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Counter-Strike: Global Offensive;critical flaw;CVE-2020-6016;CVE-2020-6017;CVE-2020-6018;CVE-2020-6019;Dota2;Half Life;Steam;steam sockets;Valve
Slug: critical-steam-flaws-could-let-gamers-to-crash-opponents-computers

[Source](https://threatpost.com/critical-steam-flaws-crash-opponents-computers/162100/){:target="_blank" rel="noopener"}

> Valve fixed critical bugs in its Steam gaming client, which is a platform for popular video games like Counter Strike: Global Offensive, Dota2 and Half Life. [...]
