Title: Intel's Habana Labs hacked by Pay2Key ransomware, data stolen
Date: 2020-12-13T13:19:18
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: intels-habana-labs-hacked-by-pay2key-ransomware-data-stolen

[Source](https://www.bleepingcomputer.com/news/security/intels-habana-labs-hacked-by-pay2key-ransomware-data-stolen/){:target="_blank" rel="noopener"}

> ​Intel-owned AI processor developer Habana Labs has suffered a cyberattack where data was stolen and leaked by threat actors. [...]
