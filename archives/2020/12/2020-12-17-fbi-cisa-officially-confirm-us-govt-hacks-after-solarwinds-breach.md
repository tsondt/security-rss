Title: FBI, CISA officially confirm US govt hacks after SolarWinds breach
Date: 2020-12-17T09:39:18
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-cisa-officially-confirm-us-govt-hacks-after-solarwinds-breach

[Source](https://www.bleepingcomputer.com/news/security/fbi-cisa-officially-confirm-us-govt-hacks-after-solarwinds-breach/){:target="_blank" rel="noopener"}

> The compromise of multiple US federal networks following the SolarWinds breach was officially confirmed for the first time in a joint statement released earlier today by the FBI, DHS-CISA, and the Office of the Director of National Intelligence (ODNI). [...]
