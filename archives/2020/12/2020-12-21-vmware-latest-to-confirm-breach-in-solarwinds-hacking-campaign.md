Title: VMware latest to confirm breach in SolarWinds hacking campaign
Date: 2020-12-21T10:38:48
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: vmware-latest-to-confirm-breach-in-solarwinds-hacking-campaign

[Source](https://www.bleepingcomputer.com/news/security/vmware-latest-to-confirm-breach-in-solarwinds-hacking-campaign/){:target="_blank" rel="noopener"}

> VMware is the latest company to confirm that it had its systems breached in the recent SolarWinds attacks and said that the hackers did not make any attempts of further exploitation after gaining access through the deployed backdoor. [...]
