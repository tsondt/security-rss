Title: Agent Tesla Keylogger Gets Data Theft and Targeting Update
Date: 2020-12-15T16:47:26+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware;agent tesla;data exfiltration;Email credentials;India;keylogger;Linux;malware;Microsoft;Microsoft Windows;Pale Moon;RAT;the bat email;web browser;Windows
Slug: agent-tesla-keylogger-gets-data-theft-and-targeting-update

[Source](https://threatpost.com/agent-tesla-targeting-data-tactics/162268/){:target="_blank" rel="noopener"}

> The infamous keylogger has shifted its targeting tactics and now collects stored credentials for less-popular web browsers and email clients. [...]
