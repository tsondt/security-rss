Title: Microsoft Office 365 Credentials Under Attack By Fax ‘Alert’ Emails
Date: 2020-12-14T18:36:06+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;boom;compromised credentials;eFax;email security;enterprise;Microsoft;o365;Office 365;Phishing;quip;Spear Phishing;weebly
Slug: microsoft-office-365-credentials-under-attack-by-fax-alert-emails

[Source](https://threatpost.com/microsoft-office-365-credentials-attack-fax/162232/){:target="_blank" rel="noopener"}

> Emails from legitimate, compromised accounts are being sent to numerous enterprise employees with the aim of stealing their O365 credentials. [...]
