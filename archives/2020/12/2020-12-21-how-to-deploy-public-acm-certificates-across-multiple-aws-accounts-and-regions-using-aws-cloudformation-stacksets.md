Title: How to deploy public ACM certificates across multiple AWS accounts and Regions using AWS CloudFormation StackSets
Date: 2020-12-21T17:26:01+00:00
Author: Prakhar Malik
Category: AWS Security
Tags: AWS Certificate Manager;AWS CloudFormation;AWS Lambda;Intermediate (200);Security, Identity, & Compliance;ACM;AWS IAM;Certificate;Cross account;Security Blog
Slug: how-to-deploy-public-acm-certificates-across-multiple-aws-accounts-and-regions-using-aws-cloudformation-stacksets

[Source](https://aws.amazon.com/blogs/security/how-to-deploy-public-acm-certificates-across-multiple-aws-accounts-and-regions-using-aws-cloudformation-stacksets/){:target="_blank" rel="noopener"}

> In this post, I take you through the steps to deploy a public AWS Certificate Manager (ACM) certificate across multiple accounts and AWS Regions by using the functionality of AWS CloudFormation StackSets and AWS Lambda. ACM is a service offered by Amazon Web Services (AWS) that you can use to obtain x509 v3 SSL/TLS certificates. [...]
