Title: Ransomware gang says they stole 2 million credit cards from E-Land
Date: 2020-12-03T02:02:02
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-says-they-stole-2-million-credit-cards-from-e-land

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-says-they-stole-2-million-credit-cards-from-e-land/){:target="_blank" rel="noopener"}

> Clop ransomware is claiming to have stolen 2 million credit cards from E-Land Retail over a one-year period ending with last months ransomware attack. [...]
