Title: Phishing targets US brokerage firms using FINRA lookalike domain
Date: 2020-12-02T11:09:03
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: phishing-targets-us-brokerage-firms-using-finra-lookalike-domain

[Source](https://www.bleepingcomputer.com/news/security/phishing-targets-us-brokerage-firms-using-finra-lookalike-domain/){:target="_blank" rel="noopener"}

> US securities industry regulator FINRA warned brokerage firms earlier this week of ongoing phishing attacks using a recently registered web domain spoofing a legitimate FINRA website. [...]
