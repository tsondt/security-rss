Title: Google makes it easier to qualify for higher payouts for Chrome browser engine bugs
Date: 2020-12-11T12:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-makes-it-easier-to-qualify-for-higher-payouts-for-chrome-browser-engine-bugs

[Source](https://portswigger.net/daily-swig/google-makes-it-easier-to-qualify-for-higher-payouts-for-chrome-browser-engine-bugs){:target="_blank" rel="noopener"}

> V8 exploits – so hot right now [...]
