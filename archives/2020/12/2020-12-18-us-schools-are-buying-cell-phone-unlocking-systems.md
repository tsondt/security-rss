Title: US Schools Are Buying Cell Phone Unlocking Systems
Date: 2020-12-18T12:53:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cell phones;cracking;forensics;privacy;schools;searches;surveillance
Slug: us-schools-are-buying-cell-phone-unlocking-systems

[Source](https://www.schneier.com/blog/archives/2020/12/us-schools-are-buying-cell-phone-unlocking-systems.html){:target="_blank" rel="noopener"}

> Gizmodo is reporting that schools in the US are buying equipment to unlock cell phones from companies like Cellebrite: Gizmodo has reviewed similar accounting documents from eight school districts, seven of which are in Texas, showing that administrators paid as much $11,582 for the controversial surveillance technology. Known as mobile device forensic tools (MDFTs), this type of tech is able to siphon text messages, photos, and application data from student’s devices. Together, the districts encompass hundreds of schools, potentially exposing hundreds of thousands of students to invasive cell phone searches. The eighth district was in Los Angeles. [...]
