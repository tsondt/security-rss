Title: Fines against banks for data breaches and noncompliance more than doubled in 2020
Date: 2020-12-31T13:55:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: fines-against-banks-for-data-breaches-and-noncompliance-more-than-doubled-in-2020

[Source](https://portswigger.net/daily-swig/fines-against-banks-for-data-breaches-and-noncompliance-more-than-doubled-in-2020){:target="_blank" rel="noopener"}

> Crackdown against financial misdeeds during lockdown leads to worldwide enforcement actions [...]
