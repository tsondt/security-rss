Title: Samsung fixes critical Android bugs in December 2020 updates
Date: 2020-12-11T13:08:57
Author: Ax Sharma
Category: BleepingComputer
Tags: Google
Slug: samsung-fixes-critical-android-bugs-in-december-2020-updates

[Source](https://www.bleepingcomputer.com/news/security/samsung-fixes-critical-android-bugs-in-december-2020-updates/){:target="_blank" rel="noopener"}

> This week Samsung has started rolling out Android's December security updates to mobile devices to patch critical security vulnerabilities in the operating system. This comes after Android had published their December 2020 security updates bulletin, which includes patches for critical bugs. [...]
