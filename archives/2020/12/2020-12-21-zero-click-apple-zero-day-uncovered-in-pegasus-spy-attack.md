Title: Zero-Click Apple Zero-Day Uncovered in Pegasus Spy Attack
Date: 2020-12-21T19:38:47+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;advanced persistent threat;al-jazeera;apple;apt;Citizen Lab;journalists;malware;monarchy;NSO Group;Pegasus;saudi arabia;sneaky kestrel;spywarem surveillance;united arab emirates;zero day;zero-click
Slug: zero-click-apple-zero-day-uncovered-in-pegasus-spy-attack

[Source](https://threatpost.com/zero-click-apple-zero-day-pegasus-spy-attack/162515/){:target="_blank" rel="noopener"}

> The phones of 36 journalists were infected by four APTs, possibly linked to Saudi Arabia or the UAE. [...]
