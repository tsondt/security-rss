Title: Universities urged to review remote learning software in order to minimize security risks
Date: 2020-12-18T15:50:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: universities-urged-to-review-remote-learning-software-in-order-to-minimize-security-risks

[Source](https://portswigger.net/daily-swig/universities-urged-to-review-remote-learning-software-in-order-to-minimize-security-risks){:target="_blank" rel="noopener"}

> Academic study warns about real harms from virtual classrooms [...]
