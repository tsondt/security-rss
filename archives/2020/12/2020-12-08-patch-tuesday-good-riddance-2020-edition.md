Title: Patch Tuesday, Good Riddance 2020 Edition
Date: 2020-12-08T23:47:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;adobe;Allan Liska;Microsoft Office;Microsoft Patch Tuesday December 2020;Microsoft Teams;Oskars Vegeris;Recorded Future
Slug: patch-tuesday-good-riddance-2020-edition

[Source](https://krebsonsecurity.com/2020/12/patch-tuesday-good-riddance-2020-edition/){:target="_blank" rel="noopener"}

> Microsoft today issued its final batch of security updates for Windows PCs in 2020, ending the year with a relatively light patch load. Nine of the 58 security vulnerabilities addressed this month earned Microsoft’s most-dire “critical” label, meaning they can be abused by malware or miscreants to seize remote control over PCs without any help from users. Mercifully, it does not appear that any of the flaws fixed this month are being actively exploited, nor have any them been detailed publicly prior to today. The critical bits reside in updates for Microsoft Exchange Server, Sharepoint Server, and Windows 10 and [...]
