Title: US Department of Homeland Security warns American business not to use Chinese tech or let data behind the Great Firewall
Date: 2020-12-23T06:01:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: us-department-of-homeland-security-warns-american-business-not-to-use-chinese-tech-or-let-data-behind-the-great-firewall

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/23/dhs_warns_us_businesses_dont_use_china_tech/){:target="_blank" rel="noopener"}

> Even fitness trackers ruled a big risk due to potential for record-matching identifying your family The United States Department of Homeland Security (DHS) has published a guide to the terrifying risks that businesses will expose themselves to if they use tech created in the Peoples’ Republic of China (PRC) or engage in any business activity with the Middle Kingdom.... [...]
