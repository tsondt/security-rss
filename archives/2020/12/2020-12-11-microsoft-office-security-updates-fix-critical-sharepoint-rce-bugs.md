Title: Microsoft Office security updates fix critical SharePoint RCE bugs
Date: 2020-12-11T12:39:16
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-office-security-updates-fix-critical-sharepoint-rce-bugs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-office-security-updates-fix-critical-sharepoint-rce-bugs/){:target="_blank" rel="noopener"}

> Microsoft has addressed critical remote code execution vulnerabilities in multiple SharePoint versions with this month's Office security updates. [...]
