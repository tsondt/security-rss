Title: QNAP High-Severity Flaws Plague NAS Systems
Date: 2020-12-07T16:15:48+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;cross-site scripting;flaw;NAS;NAS Systems;patch;QNAP;remote code injection;Security Update;vulnerability;XSS
Slug: qnap-high-severity-flaws-plague-nas-systems

[Source](https://threatpost.com/qnap-flaws-plague-nas-systems/161924/){:target="_blank" rel="noopener"}

> The high-severity cross-site scripting flaws could allow remote-code injection on QNAP NAS systems. [...]
