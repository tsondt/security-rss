Title: High-Severity Chrome Bugs Allow Browser Hacks
Date: 2020-12-04T20:40:45+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities;Web Security;87.0.4280.88;Browser Bugs;chrome browser;CVE-2020-16037;CVE-2020-16038;CVE-2020-16039;CVE-2020-16040;December Browser Security Update;Google Chrome browser;Google Patches;Linux;use-after-free
Slug: high-severity-chrome-bugs-allow-browser-hacks

[Source](https://threatpost.com/google_chrome_bugs_patched/161907/){:target="_blank" rel="noopener"}

> Desktop versions of the browser received a total of eight fixes, half rated high-severity. [...]
