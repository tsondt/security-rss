Title: Security Issues in PoS Terminals Open Consumers to Fraud
Date: 2020-12-11T20:51:14+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;arbitrary code execution;default password;include Verifone VX520;ingenico;Ingenico Telium 2 series;Password;point of sale;PoS terminal;verifone;Verifone MX series
Slug: security-issues-in-pos-terminals-open-consumers-to-fraud

[Source](https://threatpost.com/security-issues-pos-terminals-fraud/162210/){:target="_blank" rel="noopener"}

> Point-of-sale terminal vendors Verifone and Ingenico have issued mitigations after researchers found the devices use default passwords. [...]
