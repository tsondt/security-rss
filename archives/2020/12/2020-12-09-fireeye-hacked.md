Title: FireEye Hacked
Date: 2020-12-09T12:36:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: breaches;cyberespionage;hacking;network security;Russia
Slug: fireeye-hacked

[Source](https://www.schneier.com/blog/archives/2020/12/fireeye-hacked.html){:target="_blank" rel="noopener"}

> FireEye was hacked by — they believe — “a nation with top-tier offensive capabilities”: During our investigation to date, we have found that the attacker targeted and accessed certain Red Team assessment tools that we use to test our customers’ security. These tools mimic the behavior of many cyber threat actors and enable FireEye to provide essential diagnostic security services to our customers. None of the tools contain zero-day exploits. Consistent with our goal to protect the community, we are proactively releasing methods and means to detect the use of our stolen Red Team tools. We are not sure if [...]
