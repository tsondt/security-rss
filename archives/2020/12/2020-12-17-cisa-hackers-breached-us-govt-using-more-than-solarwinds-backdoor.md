Title: CISA: Hackers breached US govt using more than SolarWinds backdoor
Date: 2020-12-17T12:48:50
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Government
Slug: cisa-hackers-breached-us-govt-using-more-than-solarwinds-backdoor

[Source](https://www.bleepingcomputer.com/news/security/cisa-hackers-breached-us-govt-using-more-than-solarwinds-backdoor/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) said that the APT group behind the recent compromise campaign targeting US government agencies used more than one initial access vector. [...]
