Title: Ethical power supplier People's Energy hacked, 250,000 customers' personal info accessed
Date: 2020-12-17T21:06:49+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: ethical-power-supplier-peoples-energy-hacked-250000-customers-personal-info-accessed

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/17/peoples_energy_hacked/){:target="_blank" rel="noopener"}

> Financial info swiped for 15 small-biz clients, too Renewable electricity and gas supplier People’s Energy has told its 250,000-plus customers that a “gap” in the security of its IT system was exploited by digital burglars.... [...]
