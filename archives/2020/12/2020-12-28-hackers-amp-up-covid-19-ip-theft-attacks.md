Title: Hackers Amp Up COVID-19 IP Theft Attacks
Date: 2020-12-28T17:21:55+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Government;Hacks;Malware;Vulnerabilities;APT29;COVID-19;COVID-19 supply chain;Cozy Bear;Darkhotel;ebook;malware;moderna;Pfizer;U.S. Department of Homeland Security
Slug: hackers-amp-up-covid-19-ip-theft-attacks

[Source](https://threatpost.com/hackers-amp-up-covid-19-ip-theft-attacks/162634/){:target="_blank" rel="noopener"}

> In-depth report looks at how COVID-19 research has become as a juicy new target for organized cybercrime. [...]
