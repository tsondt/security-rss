Title: UK Ministry of Defence: We won't prosecute bug bounty hunters – oh btw, we now have one of those
Date: 2020-12-10T10:28:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-ministry-of-defence-we-wont-prosecute-bug-bounty-hunters-oh-btw-we-now-have-one-of-those

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/10/uk_mod_bug_bounty/){:target="_blank" rel="noopener"}

> 'Better late than never' opines industry bod The UK's Ministry of Defence has launched a bug bounty scheme, promising privateer pentesters they won't be prosecuted if they stick to the published script.... [...]
