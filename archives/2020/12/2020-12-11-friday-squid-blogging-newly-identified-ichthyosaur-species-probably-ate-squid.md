Title: Friday Squid Blogging: Newly Identified Ichthyosaur Species Probably Ate Squid
Date: 2020-12-11T22:10:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;squid
Slug: friday-squid-blogging-newly-identified-ichthyosaur-species-probably-ate-squid

[Source](https://www.schneier.com/blog/archives/2020/12/friday-squid-blogging-newly-identified-ichthyosaur-species-probably-ate-squid.html){:target="_blank" rel="noopener"}

> This is a deep-diving species that “fed on small prey items such as squid.” Academic paper. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
