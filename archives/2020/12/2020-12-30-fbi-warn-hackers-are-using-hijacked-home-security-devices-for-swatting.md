Title: FBI Warn Hackers are Using Hijacked Home Security Devices for ‘Swatting’
Date: 2020-12-30T21:42:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Government;IoT;Privacy;Vulnerabilities;2FA;home security hack;home surveillance hack;mfa;multi-factor authentication;Ring;ring hack;security camera;smart home device;smart home device security;swat;swatting;Two Factor Authentication
Slug: fbi-warn-hackers-are-using-hijacked-home-security-devices-for-swatting

[Source](https://threatpost.com/fbi-warn-home-security-devices-swatting/162678/){:target="_blank" rel="noopener"}

> Stolen email credentials are being used to hijack home surveillance devices, such as Ring, to call police with a fake emergency, then watch the chaos unfold. [...]
