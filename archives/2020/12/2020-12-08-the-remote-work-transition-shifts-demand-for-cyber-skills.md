Title: The Remote-Work Transition Shifts Demand for Cyber Skills
Date: 2020-12-08T19:00:16+00:00
Author: Threatpost
Category: Threatpost
Tags: Web Security;cyber skills;cybersecurity roles;remote work
Slug: the-remote-work-transition-shifts-demand-for-cyber-skills

[Source](https://threatpost.com/wiley-the-remote-work-transition-shifts-demand-for-cyber-skills/162019/){:target="_blank" rel="noopener"}

> According to Cyberseek, an interactive mapping tool that tracks the current state of the security job market, there are more than half a million open cybersecurity positions available in the U.S. alone (522,000). [...]
