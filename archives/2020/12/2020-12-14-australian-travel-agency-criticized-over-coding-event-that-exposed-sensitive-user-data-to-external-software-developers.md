Title: Australian travel agency criticized over coding event that exposed sensitive user data to external software developers
Date: 2020-12-14T16:02:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: australian-travel-agency-criticized-over-coding-event-that-exposed-sensitive-user-data-to-external-software-developers

[Source](https://portswigger.net/daily-swig/australian-travel-agency-criticized-over-coding-event-that-exposed-sensitive-user-data-to-external-software-developers){:target="_blank" rel="noopener"}

> When a ‘design jam’ ends up costing thousands of dollars in new passports [...]
