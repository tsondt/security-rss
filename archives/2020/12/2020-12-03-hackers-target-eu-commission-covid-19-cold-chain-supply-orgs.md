Title: Hackers target EU Commission, COVID-19 cold chain supply orgs
Date: 2020-12-03T09:54:26
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hackers-target-eu-commission-covid-19-cold-chain-supply-orgs

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-eu-commission-covid-19-cold-chain-supply-orgs/){:target="_blank" rel="noopener"}

> IBM X-Force warned of threat actors actively targeting organizations associated with the COVID-19 vaccine cold chain in a large scale spear-phishing campaign that has started three months ago, in September 2020. [...]
