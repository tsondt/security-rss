Title: Cross-layer attacks: New hacking technique raises DNS cache poisoning, user tracking risk
Date: 2020-12-24T11:57:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cross-layer-attacks-new-hacking-technique-raises-dns-cache-poisoning-user-tracking-risk

[Source](https://portswigger.net/daily-swig/cross-layer-attacks-new-hacking-technique-raises-dns-cache-poisoning-user-tracking-risk){:target="_blank" rel="noopener"}

> PRNG flaw in Linux kernel created multiple security vulnerabilities [...]
