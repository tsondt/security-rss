Title: SolarWinds: Hey, only as many as 18,000 customers installed backdoored software linked to US govt hacks
Date: 2020-12-15T03:14:08+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: solarwinds-hey-only-as-many-as-18000-customers-installed-backdoored-software-linked-to-us-govt-hacks

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/15/solar_winds_update/){:target="_blank" rel="noopener"}

> Orion networking monitoring users need to take action as we summarize what the hell is going on Analysis As the debris from the explosive SolarWinds hack continues to fly, it has been a busy 48 hours as everyone scrambles to find out if, like various US government bodies, they're been caught in the blast. So, where are we at?... [...]
