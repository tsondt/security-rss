Title: New study: DNS spoofing doubles in six years ... albeit from the point of naff all
Date: 2020-12-01T07:06:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: new-study-dns-spoofing-doubles-in-six-years-albeit-from-the-point-of-naff-all

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/01/dns_spoofing_rare_but_growing/){:target="_blank" rel="noopener"}

> Boffins see more interference with domain-name look-up system, wonder why DNSSEC is taking so long Boffins from the University of Southern California's Information Sciences Institute have crunched six years and four months of data, and found that DNS spoofing, while uncommon, has doubled during that time.... [...]
