Title: Google Patches Critical Wi-Fi and Audio Bugs in Android Handsets
Date: 2020-12-08T22:52:24+00:00
Author: Tom Spring
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;Android;Android buffer overflow;December Android Security Bulletin;remote code execution
Slug: google-patches-critical-wi-fi-and-audio-bugs-in-android-handsets

[Source](https://threatpost.com/google-patches-critical-wi-fi-and-audio-bugs-in-android-handsets/162060/){:target="_blank" rel="noopener"}

> Google updates its mobile OS, fixing ten critical bugs, including one remote code execution flaw. [...]
