Title: Ransomware forces hosting provider Netgain to take down data centers
Date: 2020-12-08T18:13:21
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-forces-hosting-provider-netgain-to-take-down-data-centers

[Source](https://www.bleepingcomputer.com/news/security/ransomware-forces-hosting-provider-netgain-to-take-down-data-centers/){:target="_blank" rel="noopener"}

> Cloud hosting and IT services provider Netgain was forced to take some of their data centers offline after suffering a ransomware attack in late November. [...]
