Title: Facebook Shutters Accounts Used in APT32 Cyberattacks
Date: 2020-12-11T17:05:37+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Facebook;Hacks;Malware;APT32;Bangladesh;cyberattack;google apps;google play;malware;malware attack;Phishing;phishing attack;watering hole
Slug: facebook-shutters-accounts-used-in-apt32-cyberattacks

[Source](https://threatpost.com/facebook-accounts-apt32-cyberattacks/162186/){:target="_blank" rel="noopener"}

> Facebook shut down accounts and Pages used by two separate threat groups to spread malware and conduct phishing attacks. [...]
