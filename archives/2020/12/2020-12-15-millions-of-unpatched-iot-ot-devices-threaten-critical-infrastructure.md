Title: Millions of Unpatched IoT, OT Devices Threaten Critical Infrastructure
Date: 2020-12-15T16:43:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;IoT;Vulnerabilities;Armis;CDPwn;critical infrastructure;factories;Internet of things;medical devices;operational technology;OT;Security Bugs;takeover;unpatched;urgent/11;vulnerabilities
Slug: millions-of-unpatched-iot-ot-devices-threaten-critical-infrastructure

[Source](https://threatpost.com/unpatched-iot-ot-devices-threaten-critical-infrastructure/162275/){:target="_blank" rel="noopener"}

> Industrial, factory and medical gear remain largely unpatched when it comes to the URGENT/11 and CDPwn groups of vulnerabilities. [...]
