Title: Iranian nation-state hackers linked to Pay2Key ransomware
Date: 2020-12-17T12:01:29
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: iranian-nation-state-hackers-linked-to-pay2key-ransomware

[Source](https://www.bleepingcomputer.com/news/security/iranian-nation-state-hackers-linked-to-pay2key-ransomware/){:target="_blank" rel="noopener"}

> Iranian-backed hacking group Fox Kitten has been linked to the Pay2Key ransomware operation that has recently started targeting organizations from Israel and Brazil. [...]
