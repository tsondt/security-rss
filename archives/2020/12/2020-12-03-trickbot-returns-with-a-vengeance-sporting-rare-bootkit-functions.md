Title: TrickBot Returns with a Vengeance, Sporting Rare Bootkit Functions
Date: 2020-12-03T18:58:57+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;advintel;BIOS;Bootkit;botnet;eclypsium;firmware;firmware inspection;malware;Malware analysis;Microsoft;read-write everything;RWEverything;takedown;trickboot;TrickBot;UEFI;vulnerability scanning
Slug: trickbot-returns-with-a-vengeance-sporting-rare-bootkit-functions

[Source](https://threatpost.com/trickbot-returns-bootkit-functions/161873/){:target="_blank" rel="noopener"}

> A new "TrickBoot" module scans for vulnerable firmware and has the ability to read, write and erase it on devices. [...]
