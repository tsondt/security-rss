Title: Microsoft falls prey to SolarWinds supply chain cyber-attacks
Date: 2020-12-18T17:27:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: microsoft-falls-prey-to-solarwinds-supply-chain-cyber-attacks

[Source](https://portswigger.net/daily-swig/microsoft-falls-prey-to-solarwinds-supply-chain-cyber-attacks){:target="_blank" rel="noopener"}

> State-sponsored campaign is shaping to be one of the most devastating ever [...]
