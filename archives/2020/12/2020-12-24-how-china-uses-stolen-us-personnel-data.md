Title: How China Uses Stolen US Personnel Data
Date: 2020-12-24T12:44:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: China;espionage;hacking;intelligence;operational security
Slug: how-china-uses-stolen-us-personnel-data

[Source](https://www.schneier.com/blog/archives/2020/12/how-china-uses-stolen-us-personnel-data.html){:target="_blank" rel="noopener"}

> Interesting analysis of China’s efforts to identify US spies: By about 2010, two former CIA officials recalled, the Chinese security services had instituted a sophisticated travel intelligence program, developing databases that tracked flights and passenger lists for espionage purposes. “We looked at it very carefully,” said the former senior CIA official. China’s spies “were actively using that for counterintelligence and offensive intelligence. The capability was there and was being utilized.” China had also stepped up its hacking efforts targeting biometric and passenger data from transit hubs... To be sure, China had stolen plenty of data before discovering how deeply infiltrated [...]
