Title: SolarWinds hackers breached US Treasury officials’ email accounts
Date: 2020-12-22T12:45:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: solarwinds-hackers-breached-us-treasury-officials-email-accounts

[Source](https://www.bleepingcomputer.com/news/security/solarwinds-hackers-breached-us-treasury-officials-email-accounts/){:target="_blank" rel="noopener"}

> US Senator Ron Wyden said that dozens of US Treasury email accounts were compromised by the threat actors behind the SolarWinds hack. [...]
