Title: Sunburst’s C2 Secrets Reveal Second-Stage SolarWinds Victims
Date: 2020-12-18T19:01:07+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;Government;Hacks;Malware;Vulnerabilities;Web Security;backdoor;cyberespionage;Dark Halo;dns communications;further exploitation;government agency;Kaspersky;second-stage;solarwinds;solorigate;sunburst;technical analysis;telco;UNC2452
Slug: sunbursts-c2-secrets-reveal-second-stage-solarwinds-victims

[Source](https://threatpost.com/sunburst-c2-secrets-rsolarwinds-victims/162426/){:target="_blank" rel="noopener"}

> Examining the backdoor's DNS communications led researchers to find a government agency and a big U.S. telco that were flagged for further exploitation in the spy campaign. [...]
