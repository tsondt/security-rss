Title: Passwords begone: GitHub will ban them next year for authenticating Git operations
Date: 2020-12-17T08:29:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: passwords-begone-github-will-ban-them-next-year-for-authenticating-git-operations

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/17/github_bans_passwords/){:target="_blank" rel="noopener"}

> Prepare for two brownouts in July when things get tested properly Microsoft's GitHub plans to stop accepting account passwords as a way to authenticate Git operations, starting August 13, 2021, following a test period without passwords two-weeks earlier.... [...]
