Title: Healthcare 2021: Cyberattacks to Center on COVID-19 Spying, Patient Data
Date: 2020-12-02T17:09:09+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Critical Infrastructure;Hacks;Malware;Privacy;Vulnerabilities;Web Security;2021 predictions;APT attacks;cloud storage;covid-19 research;COVID-19 vaccine;Cybersecurity;Healthcare;intellectual property theft;Kaspersky;Medical;patient data;Phishing;ransomware;SMBs;wellmess
Slug: healthcare-2021-cyberattacks-to-center-on-covid-19-spying-patient-data

[Source](https://threatpost.com/healthcare-2021-cyberattacks-covid-19-patient-data/161776/){:target="_blank" rel="noopener"}

> The post-COVID-19 surge in the criticality level of medical infrastructure, coupled with across-the-board digitalization, will be big drivers for medical-sector cyberattacks next year. [...]
