Title: Fake websites and false cures: Interpol warns of Covid-19 vaccine scams
Date: 2020-12-03T16:13:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: fake-websites-and-false-cures-interpol-warns-of-covid-19-vaccine-scams

[Source](https://portswigger.net/daily-swig/fake-websites-and-false-cures-interpol-warns-of-covid-19-vaccine-scams){:target="_blank" rel="noopener"}

> Coronavirus-related cybercrime expected to increase following Pfizer vaccination success [...]
