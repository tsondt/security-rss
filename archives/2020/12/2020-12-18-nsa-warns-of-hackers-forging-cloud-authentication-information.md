Title: NSA warns of hackers forging cloud authentication information
Date: 2020-12-18T11:56:14
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: nsa-warns-of-hackers-forging-cloud-authentication-information

[Source](https://www.bleepingcomputer.com/news/security/nsa-warns-of-hackers-forging-cloud-authentication-information/){:target="_blank" rel="noopener"}

> An advisory from the U.S. National Security Agency is providing Microsoft Azure administrators guidance to detect and protect against threat actors looking to access resources in the cloud by forging authentication information. [...]
