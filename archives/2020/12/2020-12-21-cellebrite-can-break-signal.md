Title: Cellebrite Can Break Signal
Date: 2020-12-21T12:06:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cryptanalysis;encryption;Signal
Slug: cellebrite-can-break-signal

[Source](https://www.schneier.com/blog/archives/2020/12/cellebrite-can-break-signal.html){:target="_blank" rel="noopener"}

> Cellebrite announced that it can break Signal. (Note that the company has heavily edited its blog post, but the original — with lots of technical details — was saved by the Wayback Machine.) News article. Slashdot post. The whole story is puzzling. Cellebrite’s details will make it easier for the Signal developers to patch the vulnerability. So either Cellebrite believes it is so good that it can break whatever Signal does, or the original blog post was a mistake. [...]
