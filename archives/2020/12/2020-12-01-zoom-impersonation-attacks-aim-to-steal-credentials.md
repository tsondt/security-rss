Title: Zoom Impersonation Attacks Aim to Steal Credentials
Date: 2020-12-01T17:06:11+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security;collaboration tools;coronavirus pandemic;COVID-19;Pandemic;Phishing;remote work;scam;skype;Slack;zoom;zoom credential;Zoom-bombing
Slug: zoom-impersonation-attacks-aim-to-steal-credentials

[Source](https://threatpost.com/zoom-impersonation-attacks-credentials/161718/){:target="_blank" rel="noopener"}

> The Better Business Bureau warns of phishing messages with the Zoom logo that tell recipients they have a missed meeting or suspended account. [...]
