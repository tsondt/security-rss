Title: DHS warns of data theft risk when using Chinese products
Date: 2020-12-23T11:49:58
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: dhs-warns-of-data-theft-risk-when-using-chinese-products

[Source](https://www.bleepingcomputer.com/news/security/dhs-warns-of-data-theft-risk-when-using-chinese-products/){:target="_blank" rel="noopener"}

> The US Department of Homeland Security (DHS) warned American businesses of the data theft risks behind using equipment and data services provided by companies linked with the People's Republic of China (PRC). [...]
