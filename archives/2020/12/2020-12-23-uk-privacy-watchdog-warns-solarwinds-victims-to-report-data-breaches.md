Title: UK privacy watchdog warns SolarWinds victims to report data breaches
Date: 2020-12-23T13:12:50
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: uk-privacy-watchdog-warns-solarwinds-victims-to-report-data-breaches

[Source](https://www.bleepingcomputer.com/news/security/uk-privacy-watchdog-warns-solarwinds-victims-to-report-data-breaches/){:target="_blank" rel="noopener"}

> United Kingdom's Information Commissioner's Office (ICO) has warned organizations that fell victim to the SolarWinds hack that they are required to report data breaches within three days after their discovery. [...]
