Title: Adobe Flash Player is officially dead tomorrow
Date: 2020-12-31T07:30:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Technology
Slug: adobe-flash-player-is-officially-dead-tomorrow

[Source](https://www.bleepingcomputer.com/news/security/adobe-flash-player-is-officially-dead-tomorrow/){:target="_blank" rel="noopener"}

> Flash Player will reach its end of life (EOL) on January 1, 2021, after always being a security risk to those who have used it over the years. [...]
