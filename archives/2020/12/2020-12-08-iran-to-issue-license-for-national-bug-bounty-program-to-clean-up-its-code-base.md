Title: Iran to issue license for national bug bounty program to clean up its code base
Date: 2020-12-08T05:02:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: iran-to-issue-license-for-national-bug-bounty-program-to-clean-up-its-code-base

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/08/iran_bug_bounty_program/){:target="_blank" rel="noopener"}

> Maybe don't enter unless you fancy reading colossal piles of sanctions rules Iran has asked for bids to provide the nation with a bug bounty program.... [...]
