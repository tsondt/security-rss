Title: Cyberattacks Target COVID-19 Vaccine ‘Cold-Chain’ Orgs
Date: 2020-12-03T15:47:40+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;CCEOP;Cold Chain Equipment Optimization Platform;COVID-19;COVID-19 vaccine;cyberattack;cyberespionage;Gavi;Haier Biomedical;moderna;Pfizer;Phishing;sputnik;the Vaccine Alliance;vaccine
Slug: cyberattacks-target-covid-19-vaccine-cold-chain-orgs

[Source](https://threatpost.com/attacks-covid-cold-chain-orgs/161838/){:target="_blank" rel="noopener"}

> Cybercriminals try to steal the credentials of top companies associated with the COVID-19 vaccine supply chain in an espionage effort. [...]
