Title: Automate domain join for Amazon EC2 instances from multiple AWS accounts and Regions
Date: 2020-12-10T19:22:35+00:00
Author: Sanjay Patel
Category: AWS Security
Tags: Amazon EC2;AWS Directory Service;Intermediate (200);Security, Identity, & Compliance;Access Control;How-to guides;Microsoft Active Directory;Microsoft AD;Security Blog
Slug: automate-domain-join-for-amazon-ec2-instances-from-multiple-aws-accounts-and-regions

[Source](https://aws.amazon.com/blogs/security/automate-domain-join-for-amazon-ec2-instances-multiple-aws-accounts-regions/){:target="_blank" rel="noopener"}

> As organizations scale up their Amazon Web Services (AWS) presence, they are faced with the challenge of administering user identities and controlling access across multiple accounts and Regions. As this presence grows, managing user access to cloud resources such as Amazon Elastic Compute Cloud (Amazon EC2) becomes increasingly complex. AWS Directory Service for Microsoft Active [...]
