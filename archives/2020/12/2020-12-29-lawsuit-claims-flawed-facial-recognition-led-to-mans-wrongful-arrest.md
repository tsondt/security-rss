Title: Lawsuit Claims Flawed Facial Recognition Led to Man’s Wrongful Arrest
Date: 2020-12-29T21:27:23+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Clearview AI;facial recognition racial bias lawsuit;facial recognition software;law enforcement;police sued for facial recognition software;racial bias facial recognition software;racist facial recognition
Slug: lawsuit-claims-flawed-facial-recognition-led-to-mans-wrongful-arrest

[Source](https://threatpost.com/lawsuit-claims-flawed-facial-recognition-led-to-mans-wrongful-arrest/162663/){:target="_blank" rel="noopener"}

> Black man sues police, saying he was falsely ID’d by facial recognition, joining other Black Americans falling victim to the technology’s racial bias. [...]
