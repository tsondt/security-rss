Title: Citrix confirms ongoing DDoS attack impacting NetScaler ADCs
Date: 2020-12-24T06:26:49
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: citrix-confirms-ongoing-ddos-attack-impacting-netscaler-adcs

[Source](https://www.bleepingcomputer.com/news/security/citrix-confirms-ongoing-ddos-attack-impacting-netscaler-adcs/){:target="_blank" rel="noopener"}

> Citrix has confirmed today that an ongoing 'DDoS attack pattern' using DTLS as an amplification vector is affecting Citrix Application Delivery Controller (ADC) networking appliances with EDT enabled. [...]
