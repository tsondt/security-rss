Title: MetaMask phishing steals cryptocurrency wallets via Google ads
Date: 2020-12-05T10:02:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: metamask-phishing-steals-cryptocurrency-wallets-via-google-ads

[Source](https://www.bleepingcomputer.com/news/security/metamask-phishing-steals-cryptocurrency-wallets-via-google-ads/){:target="_blank" rel="noopener"}

> Over the past week, users of the MetaMask cryptocurrency wallet have been losing funds to a phishing scam that lured potential victims through Google search ads. [...]
