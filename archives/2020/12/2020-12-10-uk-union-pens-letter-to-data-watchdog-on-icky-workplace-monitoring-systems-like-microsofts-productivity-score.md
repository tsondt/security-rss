Title: UK union pens letter to data watchdog on icky workplace monitoring systems like Microsoft's Productivity Score
Date: 2020-12-10T13:30:33+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: uk-union-pens-letter-to-data-watchdog-on-icky-workplace-monitoring-systems-like-microsofts-productivity-score

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/10/ico_workplace_monitoring/){:target="_blank" rel="noopener"}

> The Pandora's Box that won't stay closed UK trade union Prospect has chimed in with the chorus of disapproval at technologies such as Microsoft's Productivity Score being used on the nation's workers.... [...]
