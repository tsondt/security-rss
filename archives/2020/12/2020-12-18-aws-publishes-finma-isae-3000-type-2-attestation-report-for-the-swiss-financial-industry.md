Title: AWS publishes FINMA ISAE 3000 Type 2 attestation report for the Swiss financial industry
Date: 2020-12-18T17:47:54+00:00
Author: Niyaz Noor
Category: AWS Security
Tags: Financial Services;Foundational (100);Security, Identity, & Compliance;AWS Compliance;FINMA;Outsourcing Guidelines;Security Blog;Swiss banking regulations;Switzerland
Slug: aws-publishes-finma-isae-3000-type-2-attestation-report-for-the-swiss-financial-industry

[Source](https://aws.amazon.com/blogs/security/aws-publishes-finma-isae-3000-type-2-attestation-report-for-the-swiss-financial-industry/){:target="_blank" rel="noopener"}

> Gaining and maintaining customer trust is an ongoing commitment at Amazon Web Services (AWS). Our customers’ industry security requirements drive the scope and portfolio of compliance reports, attestations, and certifications we pursue. Following up on our announcement in November 2020 of the new EU (Zurich) Region, AWS is pleased to announce the issuance of the [...]
