Title: Insider Threats: What Are They, Really?
Date: 2020-12-18T16:00:13+00:00
Author: Threatpost
Category: Threatpost
Tags: Web Security;insider threat
Slug: insider-threats-what-are-they-really

[Source](https://threatpost.com/dnsfilter-insider-threats-what-are-they-really/162261/){:target="_blank" rel="noopener"}

> "Insider threat" or "human error" shows up a lot as the major cause of data breaches across all types of reports out there. But often it's not defined, or it's not clearly defined, so people conjure up their own definition. [...]
