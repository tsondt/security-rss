Title: PGMiner, Innovative Monero-Mining Botnet, Surprises Researchers
Date: 2020-12-11T19:41:40+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Malware;Vulnerabilities;botnet;cryptomining;CVE-2019-9193;database servers;Linux;Malware analysis;Monero;Palo Alto;PGMiner;PostgreSQL;RCE;remote code execution;security vulnerability;Unit 42
Slug: pgminer-innovative-monero-mining-botnet-surprises-researchers

[Source](https://threatpost.com/pgminer-monero-mining-botnet/162209/){:target="_blank" rel="noopener"}

> The malware takes aim at PostgreSQL database servers with never-before-seen techniques. [...]
