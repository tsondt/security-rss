Title: Europol Warns COVID-19 Vaccine Rollout Vulnerable to Fraud, Theft
Date: 2020-12-07T20:38:09+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Critical Infrastructure;Web Security;counterfeit vaccines;COVID-19 vaccine;COVID-19 vaccine fraud;cyberattack COVID-18 vaccine;fake;Fraud;Hoax;Operation Warp Speed;Phishing;The Vaccine Alliance’s Cold Chain Equipment Optimizations Platform (CCEOP) program
Slug: europol-warns-covid-19-vaccine-rollout-vulnerable-to-fraud-theft

[Source](https://threatpost.com/europol-covid-19-vaccine-rollout-fraud-theft/161968/){:target="_blank" rel="noopener"}

> With the promise of a widely available COVID-19 vaccine on the horizon, Europol, the European Union’s law-enforcement agency, has issued a warning about the rise of vaccine-related Dark Web activity. The agency joins a chorus of security professionals that have concerns about widespread attacks on the COVID-19 vaccine rollout. The warning comes after Europol discovered [...]
