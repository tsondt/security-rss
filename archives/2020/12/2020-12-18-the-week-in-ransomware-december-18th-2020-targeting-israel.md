Title: The Week in Ransomware - December 18th 2020 - Targeting Israel
Date: 2020-12-18T17:59:30
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-december-18th-2020-targeting-israel

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-18th-2020-targeting-israel/){:target="_blank" rel="noopener"}

> The SolarWinds supply chain attack has dominated this week's cybersecurity news, but there was still plenty of ransomware news this week. [...]
