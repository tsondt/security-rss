Title: Unsecured Azure blob exposed 500,000+ highly confidential docs from UK firm's CRM customers
Date: 2020-12-18T15:32:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: unsecured-azure-blob-exposed-500000-highly-confidential-docs-from-uk-firms-crm-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/18/probase_unsecured_azure_blob/){:target="_blank" rel="noopener"}

> Medical records, insurance claim docs, promotion process feedback... you name it, Probase bared it Exclusive A business app developer's unsecured Microsoft Azure blob left more than half a million confidential and sensitive documents belonging to its customers freely exposed to the public internet, The Register can reveal.... [...]
