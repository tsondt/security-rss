Title: Apple Manufacturer Foxconn Confirms Cyberattack
Date: 2020-12-08T20:03:49+00:00
Author: Tom Spring
Category: Threatpost
Tags: Breach;Hacks;Malware;apple;DopplePaymer;Foxconn;Foxconn Technology Group;ransomware;U.S. Cyber Emergency Response Team
Slug: apple-manufacturer-foxconn-confirms-cyberattack

[Source](https://threatpost.com/foxconn-confirms-cyber-attack/162035/){:target="_blank" rel="noopener"}

> Manufacturing powerhouse confirmed North American operations impacted by November cyberattack. [...]
