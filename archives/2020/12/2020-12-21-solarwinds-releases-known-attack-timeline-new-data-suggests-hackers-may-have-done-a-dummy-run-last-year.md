Title: SolarWinds releases known attack timeline, new data suggests hackers may have done a dummy run last year
Date: 2020-12-21T13:30:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: solarwinds-releases-known-attack-timeline-new-data-suggests-hackers-may-have-done-a-dummy-run-last-year

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/21/in_brief_security/){:target="_blank" rel="noopener"}

> And SS7 telco hack rears its ugly head yet again In brief In an 8-K filing to the US Securities and Exchange Commission, SolarWinds has given more details on exactly how it learned its servers were spewing out malware.... [...]
