Title: Brexit Deal Mandates Old Insecure Crypto Algorithms
Date: 2020-12-31T12:19:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: AES;algorithms;cryptography;e-mail;encryption;hashes;RSA;UK
Slug: brexit-deal-mandates-old-insecure-crypto-algorithms

[Source](https://www.schneier.com/blog/archives/2020/12/brexit-deal-mandates-old-insecure-crypto-algorithms.html){:target="_blank" rel="noopener"}

> In what is surely an unthinking cut-and-paste issue, page 921 of the Brexit deal mandates the use of SHA-1 and 1024-bit RSA: The open standard s/MIME as extension to de facto e-mail standard SMTP will be deployed to encrypt messages containing DNA profile information. The protocol s/MIME (V3) allows signed receipts, security labels, and secure mailing lists... The underlying certificate used by s/MIME mechanism has to be in compliance with X.509 standard.... The processing rules for s/MIME encryption operations... are as follows: the sequence of the operations is: first encryption and then signing, the encryption algorithm AES (Advanced Encryption Standard) [...]
