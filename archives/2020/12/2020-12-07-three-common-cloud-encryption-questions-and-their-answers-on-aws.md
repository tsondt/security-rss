Title: Three common cloud encryption questions and their answers on AWS
Date: 2020-12-07T17:59:02+00:00
Author: Peter M. O'Donnell
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Compliance;Crypto Agility;cryptography;data access;Defense in depth;Encryption;Security Blog
Slug: three-common-cloud-encryption-questions-and-their-answers-on-aws

[Source](https://aws.amazon.com/blogs/security/three-common-cloud-encryption-questions-and-their-answers-on-aws/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we encourage our customers to take advantage of encryption to help secure their data. Encryption is a core component of a good data protection strategy, but people sometimes have questions about how to manage encryption in the cloud to meet the growth pace and complexity of today’s enterprises. Encryption can [...]
