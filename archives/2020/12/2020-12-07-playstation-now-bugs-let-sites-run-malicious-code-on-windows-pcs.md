Title: PlayStation Now bugs let sites run malicious code on Windows PCs
Date: 2020-12-07T16:18:57
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: playstation-now-bugs-let-sites-run-malicious-code-on-windows-pcs

[Source](https://www.bleepingcomputer.com/news/security/playstation-now-bugs-let-sites-run-malicious-code-on-windows-pcs/){:target="_blank" rel="noopener"}

> Security bugs found in the PlayStation Now (PS Now) cloud gaming Windows application allowed attackers to execute arbitrary code on Windows devices running vulnerable app versions. [...]
