Title: Configuring AWS VPN for UK public sector use
Date: 2020-12-01T22:50:10+00:00
Author: Charlie Llewellyn
Category: AWS Security
Tags: AWS VPN;Intermediate (200);Security, Identity, & Compliance;ipsec VPN;NCSC;Security Blog;UK;VPN
Slug: configuring-aws-vpn-for-uk-public-sector-use

[Source](https://aws.amazon.com/blogs/security/configuring-aws-vpn-for-uk-public-sector-use/){:target="_blank" rel="noopener"}

> In this post, we explain the United Kingdom (UK) National Cyber Security Centre (NCSC)’s guidance on VPN profiles configuration, and how the configuration parameters for the AWS Virtual Private Network (AWS VPN) align with the NCSC guidance. At the end of the post, there are links to code to deploy the AWS VPN in line [...]
