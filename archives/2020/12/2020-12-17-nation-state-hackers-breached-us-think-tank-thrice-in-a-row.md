Title: Nation-state hackers breached US think tank thrice in a row
Date: 2020-12-17T15:17:31
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: nation-state-hackers-breached-us-think-tank-thrice-in-a-row

[Source](https://www.bleepingcomputer.com/news/security/nation-state-hackers-breached-us-think-tank-thrice-in-a-row/){:target="_blank" rel="noopener"}

> An advanced hacking group believed to be working for the Russian government has compromised the internal network of a think tank in the U.S. three times. [...]
