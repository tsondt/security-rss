Title: QNAP patches QTS vulnerabilities allowing NAS device takeover
Date: 2020-12-07T09:10:25
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qnap-patches-qts-vulnerabilities-allowing-nas-device-takeover

[Source](https://www.bleepingcomputer.com/news/security/qnap-patches-qts-vulnerabilities-allowing-nas-device-takeover/){:target="_blank" rel="noopener"}

> Network-attached storage (NAS) maker QNAP today released security updates to address vulnerabilities that could enable attackers to take control of unpatched NAS devices following successful exploitation. [...]
