Title: GitHub offers tighter integration of security to development workflows
Date: 2020-12-09T14:32:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: github-offers-tighter-integration-of-security-to-development-workflows

[Source](https://portswigger.net/daily-swig/github-offers-tighter-integration-of-security-to-development-workflows){:target="_blank" rel="noopener"}

> New dependency review feature offers automated DevSecOps benefits [...]
