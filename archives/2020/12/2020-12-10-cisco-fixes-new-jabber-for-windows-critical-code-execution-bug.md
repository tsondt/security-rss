Title: Cisco fixes new Jabber for Windows critical code execution bug
Date: 2020-12-10T11:00:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisco-fixes-new-jabber-for-windows-critical-code-execution-bug

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-new-jabber-for-windows-critical-code-execution-bug/){:target="_blank" rel="noopener"}

> Cisco has addressed a new critical severity remote code execution (RCE) vulnerability affecting several versions of Cisco Jabber for Windows, macOS, and mobile platforms after patching a related security bug in September. [...]
