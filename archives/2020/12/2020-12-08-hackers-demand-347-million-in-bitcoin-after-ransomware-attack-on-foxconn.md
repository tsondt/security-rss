Title: Hackers demand $34.7 million in Bitcoin after ransomware attack on Foxconn
Date: 2020-12-08T14:01:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: hackers-demand-347-million-in-bitcoin-after-ransomware-attack-on-foxconn

[Source](https://portswigger.net/daily-swig/hackers-demand-34-7-million-in-bitcoin-after-ransomware-attack-on-foxconn){:target="_blank" rel="noopener"}

> Electronics manufacturer hit by cyber-attack [...]
