Title: Zero-day XML mutation flaws in Go programming language can lead to authentication bypass
Date: 2020-12-16T11:49:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: zero-day-xml-mutation-flaws-in-go-programming-language-can-lead-to-authentication-bypass

[Source](https://portswigger.net/daily-swig/zero-day-xml-mutation-flaws-in-go-programming-language-can-lead-to-authentication-bypass){:target="_blank" rel="noopener"}

> Input-output parsing mismatches have repercussions across the Golang ecosystem [...]
