Title: Chinese Breakthrough in Quantum Computing a Warning for Security Teams
Date: 2020-12-07T17:16:59+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cryptography;China;Cybersecurity;Gaussian boson sampling;Jiuzhang;NIST;quantum computing;quantum supremacy;RSA;RSA cryptography
Slug: chinese-breakthrough-in-quantum-computing-a-warning-for-security-teams

[Source](https://threatpost.com/chinese-quantum-computing-warning-security/161935/){:target="_blank" rel="noopener"}

> China joins Google in claiming quantum supremacy with new technology, ratcheting up RSA decryption concerns. [...]
