Title: Friday Squid Blogging: Christmas Squid Memories
Date: 2020-12-18T23:08:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: friday-squid-blogging-christmas-squid-memories

[Source](https://www.schneier.com/blog/archives/2020/12/friday-squid-blogging-christmas-squid-memories.html){:target="_blank" rel="noopener"}

> Stuffed squid for Christmas Eve. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
