Title: FBI’s dark web investigations hampered by inefficiencies, overlapping objectives of different units
Date: 2020-12-21T16:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: fbis-dark-web-investigations-hampered-by-inefficiencies-overlapping-objectives-of-different-units

[Source](https://portswigger.net/daily-swig/fbis-dark-web-investigations-hampered-by-inefficiencies-overlapping-objectives-of-different-units){:target="_blank" rel="noopener"}

> Bureau-wide cybercrime strategy would be more efficient, audit concludes [...]
