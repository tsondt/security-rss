Title: DHS Among Those Hit in Sophisticated Cyberattack by Foreign Adversaries – Report
Date: 2020-12-14T19:08:27+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Hacks;Malware;Vulnerabilities;apt;commerce;cyberattack;Department of Homeland Security;espionage;FireEye;foreign adversaries;government agencies;orion;russia;solarwinds;supply chain;Treasury
Slug: dhs-among-those-hit-in-sophisticated-cyberattack-by-foreign-adversaries-report

[Source](https://threatpost.com/dhs-sophisticated-cyberattack-foreign-adversaries/162242/){:target="_blank" rel="noopener"}

> The attack was mounted via SolarWinds Orion, in a manual and targeted supply-chain effort. [...]
