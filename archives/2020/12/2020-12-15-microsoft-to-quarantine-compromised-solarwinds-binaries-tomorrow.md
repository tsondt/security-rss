Title: Microsoft to quarantine compromised SolarWinds binaries tomorrow
Date: 2020-12-15T16:46:38
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-to-quarantine-compromised-solarwinds-binaries-tomorrow

[Source](https://www.bleepingcomputer.com/news/security/microsoft-to-quarantine-compromised-solarwinds-binaries-tomorrow/){:target="_blank" rel="noopener"}

> Microsoft has announced today that Microsoft Defender will begin quarantining compromised SolarWind Orion binaries starting tomorrow morning. [...]
