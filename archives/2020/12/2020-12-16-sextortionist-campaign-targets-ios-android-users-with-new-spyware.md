Title: Sextortionist Campaign Targets iOS, Android Users with New Spyware
Date: 2020-12-16T16:16:36+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security;Privacy;Android;apple;Blackmail;email;google;Goontact;ios;iphone;KakaoTalk;mobile threats;sextortionist;Spyware;telegram;threat actors
Slug: sextortionist-campaign-targets-ios-android-users-with-new-spyware

[Source](https://threatpost.com/sextortionist-campaign-targets-ios-android-users-with-new-spyware/162321/){:target="_blank" rel="noopener"}

> Goontact lures users of illicit sites through Telegram and other secure messaging apps and steals their information for future fraudulent use. [...]
