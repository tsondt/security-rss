Title: Cayman Islands Bank Records Exposed in Open Azure Blob
Date: 2020-12-01T19:35:50+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Privacy;Vulnerabilities;Amazon AWS S3 bucket;Azure blob;cayman islands;Cloud misconfiguration;data leak;investment firm;Microsoft Azure Blob;offshore banking;personal information;security vulnerability
Slug: cayman-islands-bank-records-exposed-in-open-azure-blob

[Source](https://threatpost.com/cayman-islands-bank-records-exposed-azure-blob/161729/){:target="_blank" rel="noopener"}

> An offshore Cayman Islands bank’s backups, covering a $500 million investment portfolio, were left unsecured and leaking personal banking information, passport data and even online banking PINs. [...]
