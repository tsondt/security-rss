Title: Protecting your Kubernetes deployments with Policy Controller
Date: 2020-12-17T19:00:00+00:00
Author: John Murray
Category: GCP Security
Tags: Containers & Kubernetes;Identity & Security;Hybrid & Multi-Cloud;Google Cloud Platform;Application Development
Slug: protecting-your-kubernetes-deployments-with-policy-controller

[Source](https://cloud.google.com/blog/products/application-development/protecting-your-kubernetes-deployments-policy-controller/){:target="_blank" rel="noopener"}

> In November, the Kubernetes project disclosed a vulnerability which every Kuberenetes administrator or adopter should be aware of. The vulnerability, known as CVE-2020-8554, stems from default permissions allowing users to create objects that could act as a “Man in the Middle” and therefore potentially intercept sensitive data. If you are using a Google Cloud managed solution like Anthos or Kubernetes Engine (GKE), you can easily and effectively mitigate this vulnerability. In this blog, we’ll show you how. First let’s talk about the vulnerability. Who is vulnerable: CVE-2020-8554 affects all multi-tenant Kubernetes clusters. Multi-tenancy is defined in a Kubernetes cluster as [...]
