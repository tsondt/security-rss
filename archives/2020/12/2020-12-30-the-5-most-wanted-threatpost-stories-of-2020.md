Title: The 5 Most-Wanted Threatpost Stories of 2020
Date: 2020-12-30T13:00:26+00:00
Author: Threatpost
Category: Threatpost
Tags: Breach;Cloud Security;Critical Infrastructure;Cryptography;Editor's Picks;Hacks;IoT;Malware;Mobile Security;Most Recent ThreatLists;Privacy;Vulnerabilities;Web Security;amongus;Android;chrome;cobalt strike;cyberpunk 2077;Cybersecurity;emotet;Gaming;joker malware;Lucifer malware;malicious extension;malware;Microsoft;Microsoft 365;microsoft credentials;microsoft teams;mobile malware;most read;nvidia security bugs;Office 365;Phishing;ps5;ransomware;retrospective;scalperbots;Spear Phishing;top headlines;top stories 2020;Xbox Series X
Slug: the-5-most-wanted-threatpost-stories-of-2020

[Source](https://threatpost.com/top-threatpost-stories-2020/162501/){:target="_blank" rel="noopener"}

> A look back at what was hot with readers -- offering a snapshot of the security stories that were most top-of-mind for security professionals and consumers throughout the year. [...]
