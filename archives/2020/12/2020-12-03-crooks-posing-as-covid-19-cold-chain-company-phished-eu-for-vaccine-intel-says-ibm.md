Title: Crooks posing as COVID-19 'cold chain' company phished EU for vaccine intel, says IBM
Date: 2020-12-03T18:05:53+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: crooks-posing-as-covid-19-cold-chain-company-phished-eu-for-vaccine-intel-says-ibm

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/03/ibm_phishing_covid/){:target="_blank" rel="noopener"}

> Medical fridge firm finagling carries hallmarks of state-sponsored hack dogs An unidentified group of malicious sorts impersonated a so-called "cold chain" company involved in COVID-19 vaccine distribution networks then targeted an EU governmental agency, according to IBM.... [...]
