Title: US court order brings down the shutters on tech support scheme impacting the elderly
Date: 2020-12-31T16:54:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-court-order-brings-down-the-shutters-on-tech-support-scheme-impacting-the-elderly

[Source](https://portswigger.net/daily-swig/us-court-order-brings-down-the-shutters-on-tech-support-scheme-impacting-the-elderly){:target="_blank" rel="noopener"}

> Action bars operation of US-registered firms linked to Indian call centre [...]
