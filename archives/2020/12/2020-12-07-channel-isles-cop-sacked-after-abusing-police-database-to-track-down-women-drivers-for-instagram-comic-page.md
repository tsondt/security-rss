Title: Channel Isles cop sacked after abusing police database to track down women drivers for Instagram 'comic' page
Date: 2020-12-07T16:14:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: channel-isles-cop-sacked-after-abusing-police-database-to-track-down-women-drivers-for-instagram-comic-page

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/07/guernsey_police_database_sacking/){:target="_blank" rel="noopener"}

> Community service for creepy constable A police constable has been sacked after reportedly tracking down young women motorists through their car numberplates and propositioning them on social media.... [...]
