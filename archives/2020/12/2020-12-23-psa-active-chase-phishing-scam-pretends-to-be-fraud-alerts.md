Title: PSA: Active Chase phishing scam pretends to be fraud alerts
Date: 2020-12-23T16:23:17
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: psa-active-chase-phishing-scam-pretends-to-be-fraud-alerts

[Source](https://www.bleepingcomputer.com/news/security/psa-active-chase-phishing-scam-pretends-to-be-fraud-alerts/){:target="_blank" rel="noopener"}

> A large scale phishing scam is underway that pretends to be a security notice from Chase stating that fraudulent activity has been detected and caused the recipient's account to be blocked. [...]
