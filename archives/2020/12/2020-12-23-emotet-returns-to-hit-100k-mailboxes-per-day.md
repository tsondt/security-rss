Title: Emotet Returns to Hit 100K Mailboxes Per Day
Date: 2020-12-23T15:36:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security;Cyberattacks;email attack;emotet;emotet returns;malspam;malware;TrickBot;Trojan
Slug: emotet-returns-to-hit-100k-mailboxes-per-day

[Source](https://threatpost.com/emotet-returns-100k-mailboxes/162584/){:target="_blank" rel="noopener"}

> Just in time for the Christmas holiday, Emotet is sending the gift of Trickbot. [...]
