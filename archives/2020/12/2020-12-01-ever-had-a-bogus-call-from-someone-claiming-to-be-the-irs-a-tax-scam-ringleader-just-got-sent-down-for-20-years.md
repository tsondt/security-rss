Title: Ever had a bogus call from someone claiming to be the IRS? A tax scam ringleader just got sent down for 20 years
Date: 2020-12-01T21:28:06+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: ever-had-a-bogus-call-from-someone-claiming-to-be-the-irs-a-tax-scam-ringleader-just-got-sent-down-for-20-years

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/01/scam_call_prison/){:target="_blank" rel="noopener"}

> Hitesh Patel also faces $9m payback for defrauding thousands of US citizens The man who headed an international criminal call center racket that conned Americans into handing over tens of millions of dollars in the belief they were being chased for money by the US government has been jailed for 20 years.... [...]
