Title: NSA on Authentication Hacks (Related to SolarWinds Breach)
Date: 2020-12-18T16:35:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: authentication;credentials;hacking;NSA;trust
Slug: nsa-on-authentication-hacks-related-to-solarwinds-breach

[Source](https://www.schneier.com/blog/archives/2020/12/nsa-on-authentication-hacks-related-to-solarwinds-breach.html){:target="_blank" rel="noopener"}

> The NSA has published an advisory outlining how “malicious cyber actors” are “are manipulating trust in federated authentication environments to access protected data in the cloud.” This is related to the SolarWinds hack I have previously written about, and represents one of the techniques the SVR is using once it has gained access to target networks. From the summary : Malicious cyberactors are abusing trust in federated authentication environments to access protected data. The exploitation occurs after the actors have gained initial access to a victim’s on-premises network. The actors leverage privileged access in the on-premises environment to subvert the [...]
