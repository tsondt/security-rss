Title: More than half of GDPR fines issued by UK data privacy watchdog remain unpaid
Date: 2020-12-04T16:18:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: more-than-half-of-gdpr-fines-issued-by-uk-data-privacy-watchdog-remain-unpaid

[Source](https://portswigger.net/daily-swig/more-than-half-of-gdpr-fines-issued-uk-data-privacy-watchdog-remain-unpaid){:target="_blank" rel="noopener"}

> Information Commissioner’s Office struggling to recoup data breach debts, report suggests [...]
