Title: Swatters hijack smart home devices to watch emergency responders
Date: 2020-12-29T14:29:36
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: swatters-hijack-smart-home-devices-to-watch-emergency-responders

[Source](https://www.bleepingcomputer.com/news/security/swatters-hijack-smart-home-devices-to-watch-emergency-responders/){:target="_blank" rel="noopener"}

> Weak credentials and login protections come with the risk of swatting for owners of connected devices with video and voice capabilities, warns the U.S. Federal Bureau of Investigation (FBI). [...]
