Title: Russia’s SolarWinds Attack
Date: 2020-12-28T12:21:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;cyberattack;cybercrime;cyberespionage;espionage;essays;hacking;Russia;vulnerabilities
Slug: russias-solarwinds-attack

[Source](https://www.schneier.com/blog/archives/2020/12/russias-solarwinds-attack.html){:target="_blank" rel="noopener"}

> Recent news articles have all been talking about the massive Russian cyberattack against the United States, but that’s wrong on two accounts. It wasn’t a cyberattack in international relations terms, it was espionage. And the victim wasn’t just the US, it was the entire world. But it was massive, and it is dangerous. Espionage is internationally allowed in peacetime. The problem is that both espionage and cyberattacks require the same computer and network intrusions, and the difference is only a few keystrokes. And since this Russian operation isn’t at all targeted, the entire world is at risk — and not [...]
