Title: The SolarWinds Perfect Storm: Default Password, Access Sales and More
Date: 2020-12-16T17:05:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Hacks;Malware;Vulnerabilities;antivirus disabled;cyberattack;default password;DHS;FireEye;fxmsp;kill switch;Microsoft;network access;solarwinds;solarwinds123;solorigate;supply chain hack;u.s. government agencies
Slug: the-solarwinds-perfect-storm-default-password-access-sales-and-more

[Source](https://threatpost.com/solarwinds-default-password-access-sales/162327/){:target="_blank" rel="noopener"}

> Meanwhile, FireEye has found a kill switch, and Microsoft and other vendors are quickly moving to block the Sunburst backdoor used in the attack. [...]
