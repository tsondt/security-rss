Title: Hacker earns $2 million in bug bounties on HackerOne
Date: 2020-12-24T08:20:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hacker-earns-2-million-in-bug-bounties-on-hackerone

[Source](https://www.bleepingcomputer.com/news/security/hacker-earns-2-million-in-bug-bounties-on-hackerone/){:target="_blank" rel="noopener"}

> Cosmin Iordache is the first bug bounty hunter to earn more than $2,000,000 in bounty awards through the vulnerability coordination and bug bounty program HackerOne. [...]
