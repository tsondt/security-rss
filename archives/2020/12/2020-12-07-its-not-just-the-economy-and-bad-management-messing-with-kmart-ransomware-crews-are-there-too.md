Title: It's not just the economy and bad management messing with Kmart - ransomware crews are there too
Date: 2020-12-07T15:24:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: its-not-just-the-economy-and-bad-management-messing-with-kmart-ransomware-crews-are-there-too

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/07/in_brief_security/){:target="_blank" rel="noopener"}

> The week's other security news In Brief It looks like the Egregor crew is at it again, and this time the ransomware-flingers have caught venerable but struggling US retail biz Kmart.... [...]
