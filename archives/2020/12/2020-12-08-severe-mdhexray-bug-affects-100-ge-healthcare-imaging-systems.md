Title: Severe MDHexRay bug affects 100+ GE Healthcare imaging systems
Date: 2020-12-08T12:00:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Business
Slug: severe-mdhexray-bug-affects-100-ge-healthcare-imaging-systems

[Source](https://www.bleepingcomputer.com/news/security/severe-mdhexray-bug-affects-100-plus-ge-healthcare-imaging-systems/){:target="_blank" rel="noopener"}

> A vulnerability in GE Healthcare's proprietary management software used for medical imaging devices could put patients' health privacy at risk, potentially their lives. [...]
