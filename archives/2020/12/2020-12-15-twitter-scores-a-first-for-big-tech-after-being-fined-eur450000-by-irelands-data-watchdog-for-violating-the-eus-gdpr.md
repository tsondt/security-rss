Title: Twitter scores a first for big tech after being fined €450,000 by Ireland's data watchdog for violating the EU's GDPR
Date: 2020-12-15T18:59:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: twitter-scores-a-first-for-big-tech-after-being-fined-eur450000-by-irelands-data-watchdog-for-violating-the-eus-gdpr

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/15/twitter_gdpr_fine/){:target="_blank" rel="noopener"}

> Fellow industry giants shuffle feet nervously Ireland's Data Protection Commission (DPC) has fined Twitter €450,000 after ruling a bug in the firm's Android app that allowed users private messages to be publicly viewed infringed the EU's General Data Protection Regulation (GDPR).... [...]
