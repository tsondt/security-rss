Title: 250,000 stolen MySQL databases for sale on dark web auction site
Date: 2020-12-10T13:39:46
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: 250000-stolen-mysql-databases-for-sale-on-dark-web-auction-site

[Source](https://www.bleepingcomputer.com/news/security/250-000-stolen-mysql-databases-for-sale-on-dark-web-auction-site/){:target="_blank" rel="noopener"}

> Hackers have set up an auction site on the dark web to sell 250,000 databases stolen from tens of thousands of breached MySQL servers. [...]
