Title: As Modern Mobile Enables Remote Work, It Also Demands Security
Date: 2020-12-03T15:18:52+00:00
Author: Hank Schless
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Mobile Security;Vulnerabilities;Web Security;chromebooks;COVID-19;Cyberattacks;hank schless;infosec insider;Lookout;mobile endpoint security;Mobile security;Pandemic;remote learning;remote work;security risks;Vishing
Slug: as-modern-mobile-enables-remote-work-it-also-demands-security

[Source](https://threatpost.com/mobile-remote-work-security/161842/){:target="_blank" rel="noopener"}

> Lookout's Hank Schless discusses accelerated threats to mobile endpoints in the age of COVID-19-sparked remote working. [...]
