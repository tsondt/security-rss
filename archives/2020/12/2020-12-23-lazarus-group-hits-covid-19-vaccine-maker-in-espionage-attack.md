Title: Lazarus Group Hits COVID-19 Vaccine-Maker in Espionage Attack
Date: 2020-12-23T19:02:30+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Government;Hacks;Malware;advanced persistent threat;apt;COVID-19;cyberattack;espionage;health ministry;IP theft;Lazarus Group;North Korea;pharmaceutical company;supply chain attack;vaccine;vaccine development
Slug: lazarus-group-hits-covid-19-vaccine-maker-in-espionage-attack

[Source](https://threatpost.com/lazarus-covid-19-vaccine-maker-espionage/162591/){:target="_blank" rel="noopener"}

> The nation-state actor is looking to speed up vaccine development efforts in North Korea. [...]
