Title: P2P mobile file transfer apps open to attacks, researchers find
Date: 2020-12-17T12:22:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: p2p-mobile-file-transfer-apps-open-to-attacks-researchers-find

[Source](https://portswigger.net/daily-swig/p2p-mobile-file-transfer-apps-open-to-attacks-researchers-find){:target="_blank" rel="noopener"}

> Shared design flaws discovered in Huawei, LG, and Xiaomi smartphones allowed attackers to hijack file transfer sessions [...]
