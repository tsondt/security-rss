Title: 'Long-standing vulns' in 5G protocols open the door for attacks on smartphone users
Date: 2020-12-18T10:30:14+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: long-standing-vulns-in-5g-protocols-open-the-door-for-attacks-on-smartphone-users

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/18/5g_security_enisa_positive_technologies/){:target="_blank" rel="noopener"}

> Plus: EU agrees that security could be better and calls for bigger role for itself Some 5G networks are at risk of attack thanks to "long-standing vulnerabilities" in core protocols, according to infosec researchers at Positive Technologies.... [...]
