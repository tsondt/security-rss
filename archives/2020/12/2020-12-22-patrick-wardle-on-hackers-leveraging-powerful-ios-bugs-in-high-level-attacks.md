Title: Patrick Wardle on Hackers Leveraging ‘Powerful’ iOS Bugs in High-Level Attacks
Date: 2020-12-22T14:00:37+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Videos;Adware;apple;Big Sur;Content filters;ios;iphone;Lazarus;Mac;macOS;Patrick Wardle;security apple;VPN
Slug: patrick-wardle-on-hackers-leveraging-powerful-ios-bugs-in-high-level-attacks

[Source](https://threatpost.com/patrick-wardle-on-hackers-leveraging-powerful-ios-bugs-in-high-level-attacks/162521/){:target="_blank" rel="noopener"}

> Noted Apple security expert Patrick Wardle discusses how cybercriminals are stepping up their game in targeting Apple users with new techniques and cyberattacks. [...]
