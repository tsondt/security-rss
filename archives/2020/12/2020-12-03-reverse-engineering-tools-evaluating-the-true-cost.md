Title: Reverse Engineering Tools: Evaluating the True Cost
Date: 2020-12-03T17:00:26+00:00
Author: Threatpost
Category: Threatpost
Tags: Web Security
Slug: reverse-engineering-tools-evaluating-the-true-cost

[Source](https://threatpost.com/hex-rays-reverse-engineering-tools-evaluating-the-true-cost/161767/){:target="_blank" rel="noopener"}

> Breaking down the true cost of software tools in the context of reverse engineering and debugging may not be as clear-cut as it appears. [...]
