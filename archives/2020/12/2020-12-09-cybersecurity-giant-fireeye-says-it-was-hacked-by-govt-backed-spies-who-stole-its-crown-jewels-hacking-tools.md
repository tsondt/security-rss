Title: Cybersecurity giant FireEye says it was hacked by govt-backed spies who stole its crown-jewels hacking tools
Date: 2020-12-09T01:14:06+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: cybersecurity-giant-fireeye-says-it-was-hacked-by-govt-backed-spies-who-stole-its-crown-jewels-hacking-tools

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/09/fireeye_tools_hacked/){:target="_blank" rel="noopener"}

> Not a great look Cybersecurity corp FireEye has confessed its most secure servers have been compromised, almost certainly by state-backed hackers who then made away with its proprietary hacking tools.... [...]
