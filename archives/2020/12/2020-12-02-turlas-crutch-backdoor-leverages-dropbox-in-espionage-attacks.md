Title: Turla’s ‘Crutch’ Backdoor Leverages Dropbox in Espionage Attacks
Date: 2020-12-02T18:06:30+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;Malware;apt;backdoor;Crutch;cyberattack;cyberespionage;dropbox;espionage;malware;Ministry of Foreign Affairs;PowerShell Empire;Skipper implant;Turla
Slug: turlas-crutch-backdoor-leverages-dropbox-in-espionage-attacks

[Source](https://threatpost.com/turla-backdoor-dropbox-espionage-attacks/161777/){:target="_blank" rel="noopener"}

> In a recent cyberattack against an E.U. country's Ministry of Foreign Affairs, the Crutch backdoor leveraged Dropbox to exfiltrate sensitive documents. [...]
