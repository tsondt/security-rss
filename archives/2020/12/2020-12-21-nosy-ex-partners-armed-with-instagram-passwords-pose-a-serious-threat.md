Title: Nosy Ex-Partners Armed with Instagram Passwords Pose a Serious Threat
Date: 2020-12-21T21:48:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Facebook;Privacy;Web Security;ex-partners;exes;Instagram;multifactor authentication;Netflix;revenge;serious threat;shared passwords;social media accounts;stalker;survey
Slug: nosy-ex-partners-armed-with-instagram-passwords-pose-a-serious-threat

[Source](https://threatpost.com/nosy-exes-passwords-serious-threat/162533/){:target="_blank" rel="noopener"}

> A survey of single people found almost a third are still logging into their ex’s social-media accounts, some for revenge. [...]
