Title: Code42 Incydr Series: Bringing Shadow IT into the light with Code42 Incydr
Date: 2020-12-17T17:00:29+00:00
Author: Threatpost
Category: Threatpost
Tags: Web Security;Mirror IT;shadow IT
Slug: code42-incydr-series-bringing-shadow-it-into-the-light-with-code42-incydr

[Source](https://threatpost.com/code42-incydr-series-bringing-shadow-it-into-the-light-with-code42-incydr/162311/){:target="_blank" rel="noopener"}

> The massive shift to remote work has turbocharged the shadow IT problem. [...]
