Title: Meet urlhunter, the URLTeam companion and shortened URL search engine
Date: 2020-12-03T13:21:19+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: meet-urlhunter-the-urlteam-companion-and-shortened-url-search-engine

[Source](https://portswigger.net/daily-swig/meet-urlhunter-the-urlteam-companion-and-shortened-url-search-engine){:target="_blank" rel="noopener"}

> Open source tool useful for threat intel experts and bug bounty hunters, says its architect [...]
