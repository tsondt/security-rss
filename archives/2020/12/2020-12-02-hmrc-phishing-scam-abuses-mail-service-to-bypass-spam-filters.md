Title: HMRC phishing scam abuses mail service to bypass spam filters
Date: 2020-12-02T17:10:46
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: hmrc-phishing-scam-abuses-mail-service-to-bypass-spam-filters

[Source](https://www.bleepingcomputer.com/news/security/hmrc-phishing-scam-abuses-mail-service-to-bypass-spam-filters/){:target="_blank" rel="noopener"}

> Threat actors are exploiting legitimate SendGrid mailing service to send HMRC phishing emails that bypass spam filters. [...]
