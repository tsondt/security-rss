Title: CrowdStrike releases free Azure security tool after failed hack
Date: 2020-12-25T14:08:50
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: crowdstrike-releases-free-azure-security-tool-after-failed-hack

[Source](https://www.bleepingcomputer.com/news/security/crowdstrike-releases-free-azure-security-tool-after-failed-hack/){:target="_blank" rel="noopener"}

> Leading cybersecurity firm CrowdStrike was notified by Microsoft that threat actors had attempted to read the company's emails through compromised by Microsoft Azure credentials. [...]
