Title: Defending Against State and State-Sponsored Threat Actors
Date: 2020-12-21T21:01:14+00:00
Author: Saryu Nayyar
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Malware;Mobile Security;Vulnerabilities;best practices;Cyberattacks;Cybersecurity;Defense;nation state;social engineering;spearphishing;state actors;State sponsored;technical defenses;tips
Slug: defending-against-state-and-state-sponsored-threat-actors

[Source](https://threatpost.com/defending-against-state-threat-actors/162518/){:target="_blank" rel="noopener"}

> Saryu Nayyar of Gurucul discusses state and state-sponsored threat actors, the apex predators of the cybersecurity world. [...]
