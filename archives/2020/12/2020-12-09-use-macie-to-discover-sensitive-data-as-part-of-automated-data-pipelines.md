Title: Use Macie to discover sensitive data as part of automated data pipelines
Date: 2020-12-09T21:26:31+00:00
Author: Brandon Wu
Category: AWS Security
Tags: Advanced (300);Amazon Macie;Security, Identity, & Compliance;Amazon S3;Amazon SES;API Gateway;AWS Lambda;Cloud security;cybersecurity;data discovery;data lake;data pipeline;Data protection;data security;DevSecOps;PII;Security Blog;Serverless;step functions
Slug: use-macie-to-discover-sensitive-data-as-part-of-automated-data-pipelines

[Source](https://aws.amazon.com/blogs/security/use-macie-to-discover-sensitive-data-as-part-of-automated-data-pipelines/){:target="_blank" rel="noopener"}

> Data is a crucial part of every business and is used for strategic decision making at all levels of an organization. To extract value from their data more quickly, Amazon Web Services (AWS) customers are building automated data pipelines—from data ingestion to transformation and analytics. As part of this process, my customers often ask how [...]
