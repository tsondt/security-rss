Title: Happy 11th Birthday, KrebsOnSecurity!
Date: 2020-12-30T01:24:33+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other
Slug: happy-11th-birthday-krebsonsecurity

[Source](https://krebsonsecurity.com/2020/12/happy-11th-birthday-krebsonsecurity/){:target="_blank" rel="noopener"}

> Today marks the 11th anniversary of KrebsOnSecurity! Thank you, Dear Readers, for your continued encouragement and support! With the ongoing disruption to life and livelihood wrought by the Covid-19 pandemic, 2020 has been a fairly horrid year by most accounts. And it’s perhaps fitting that this was also a leap year, piling on an extra day to a solar rotation that most of us probably can’t wait to see in the rearview mirror. But it was hardly a dull one for computer security news junkies. In almost every category — from epic breaches and ransomware to cybercrime justice and increasingly [...]
