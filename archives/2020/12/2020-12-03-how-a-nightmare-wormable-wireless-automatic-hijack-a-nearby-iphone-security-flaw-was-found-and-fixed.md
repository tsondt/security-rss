Title: How a nightmare wormable, wireless, automatic hijack-a-nearby-iPhone security flaw was found and fixed
Date: 2020-12-03T08:26:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: how-a-nightmare-wormable-wireless-automatic-hijack-a-nearby-iphone-security-flaw-was-found-and-fixed

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/03/apple_wireless_bug/){:target="_blank" rel="noopener"}

> You're probably all patched by now, which is just as well A Google security guru has published details of a critical hole in Apple's iOS that can be exploited by miscreants to hijack strangers' iPhones over the air without any user interaction.... [...]
