Title: Announcing Cloud Audit Academy AWS-specific for audit and compliance teams
Date: 2020-12-01T18:36:10+00:00
Author: Chad Woolf
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;Auditing;Auditing security;AWS best practices;AWS Compliance;cloud agnostic;Cloud Audit Academy;Compliance frameworks;eLearning;instructor-led training;Security Blog
Slug: announcing-cloud-audit-academy-aws-specific-for-audit-and-compliance-teams

[Source](https://aws.amazon.com/blogs/security/announcing-cloud-audit-academy-aws-specific-for-audit-and-compliance-teams/){:target="_blank" rel="noopener"}

> Today, I’m pleased to announce the launch of Cloud Audit Academy AWS-specific (CAA AWS-specific). This is a new, accelerated training program for auditing AWS Cloud implementations, and is designed for auditors, regulators, or anyone working within a control framework. Over the past few years, auditing security in the cloud has become one of the fastest [...]
