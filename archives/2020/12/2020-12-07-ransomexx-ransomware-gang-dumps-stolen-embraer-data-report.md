Title: RansomExx Ransomware Gang Dumps Stolen Embraer Data: Report
Date: 2020-12-07T14:18:43+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Hacks;barnes & noble;Brazil;Clop;conti;Dark Web;data leak;data theft;Defray 777;egregor;Embraer;Kmart;leak site;malware;RansomExx;ransomware;Translink;ZDNET
Slug: ransomexx-ransomware-gang-dumps-stolen-embraer-data-report

[Source](https://threatpost.com/ransomexx-ransomware-gang-dumps-stolen-embraer-data-report/161918/){:target="_blank" rel="noopener"}

> The group published files stolen from the Brazilian aircraft manufacturer in a ransomware attack last month. [...]
