Title: Data breach at US legal aid firm Brooklyn Defender Services exposed clients’ personal data
Date: 2020-12-11T16:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-at-us-legal-aid-firm-brooklyn-defender-services-exposed-clients-personal-data

[Source](https://portswigger.net/daily-swig/data-breach-at-us-legal-aid-firm-brooklyn-defender-services-exposed-clients-personal-data){:target="_blank" rel="noopener"}

> Public defender organization represents low-income clients in 35,000 cases annually [...]
