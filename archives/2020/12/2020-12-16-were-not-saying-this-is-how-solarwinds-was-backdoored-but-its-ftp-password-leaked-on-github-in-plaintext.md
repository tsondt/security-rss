Title: We're not saying this is how SolarWinds was backdoored, but its FTP password 'leaked on GitHub in plaintext'
Date: 2020-12-16T00:00:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: were-not-saying-this-is-how-solarwinds-was-backdoored-but-its-ftp-password-leaked-on-github-in-plaintext

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/16/solarwinds_github_password/){:target="_blank" rel="noopener"}

> 'solarwinds123' won't inspire confidence, if true SolarWinds, the maker of the Orion network management software that was subverted to distribute backdoored updates that led to the compromise of multiple US government bodies, was apparently told last year that credentials for its software update server had been exposed in a public GitHub repo.... [...]
