Title: UK NCA visits WeLeakInfo users to warn of using stolen data
Date: 2020-12-28T10:38:12
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: uk-nca-visits-weleakinfo-users-to-warn-of-using-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/uk-nca-visits-weleakinfo-users-to-warn-of-using-stolen-data/){:target="_blank" rel="noopener"}

> 21 WeLeakInfo customers have been arrested across the UK for using stolen credentials downloaded from WeLeakInfo following an operation coordinated by the UK National Crime Agency (NCA). [...]
