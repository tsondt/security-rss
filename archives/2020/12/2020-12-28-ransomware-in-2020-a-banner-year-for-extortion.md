Title: Ransomware in 2020: A Banner Year for Extortion
Date: 2020-12-28T14:00:54+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Malware;Clop;conti;COVID-19;cyberattack;DoppelPaymer;election ransomware;evil corp;Garmin ransomware attack;mySQL servers;Pandemic;Please_read_me ransomware attack;ransomware;UVM health network ransomware
Slug: ransomware-in-2020-a-banner-year-for-extortion

[Source](https://threatpost.com/ransomware-2020-extortion/162319/){:target="_blank" rel="noopener"}

> From attacks on the UVM Health Network that delayed chemotherapy appointments, to ones on public schools that delayed students going back to the classroom, ransomware gangs disrupted organizations to inordinate levels in 2020. [...]
