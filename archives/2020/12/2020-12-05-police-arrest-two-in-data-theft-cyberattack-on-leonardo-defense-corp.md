Title: Police arrest two in data theft cyberattack on Leonardo defense corp
Date: 2020-12-05T15:33:14
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Government
Slug: police-arrest-two-in-data-theft-cyberattack-on-leonardo-defense-corp

[Source](https://www.bleepingcomputer.com/news/security/police-arrest-two-in-data-theft-cyberattack-on-leonardo-defense-corp/){:target="_blank" rel="noopener"}

> Italian police have arrested two people allegedly for using malware to steal 10 GB of confidental data and military secrets from defense company Leonardo S.p.A. [...]
