Title: Japanese Aerospace Firm Kawasaki Warns of Data Breach
Date: 2020-12-29T15:11:13+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Breach;Hacks;breach;compromised data;Customer Data;data breach;Kawasaki;Kawasaki Heavy Industries;stolen data
Slug: japanese-aerospace-firm-kawasaki-warns-of-data-breach

[Source](https://threatpost.com/japanese-aerospace-firm-kawasaki-warns-of-data-breach/162642/){:target="_blank" rel="noopener"}

> The Japanese aerospace manufacturer said that starting in June, overseas unauthorized access to its servers may have compromised customer data. [...]
