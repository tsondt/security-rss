Title: Easy WP SMTP Security Bug Can Reveal Admin Credentials
Date: 2020-12-15T21:30:11+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security;admin credentials;debug file;easy wp smtp;email management;plugin;security bug;site takeover;vulnerability;wordpress
Slug: easy-wp-smtp-security-bug-can-reveal-admin-credentials

[Source](https://threatpost.com/easy-wp-smtp-security-bug/162301/){:target="_blank" rel="noopener"}

> A poorly configured file opens users up to site takeover. [...]
