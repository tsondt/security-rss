Title: 5M WordPress Sites Running ‘Contact Form 7’ Plugin Open to Attack
Date: 2020-12-17T22:27:38+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities;Web Security;5.3.2;but;Contact Form 7;CVE-2020-35489;patch;plugin;security vulnerability;site takeover;Update;wordpress
Slug: 5m-wordpress-sites-running-contact-form-7-plugin-open-to-attack

[Source](https://threatpost.com/contact-form-7-plugin-bug/162383/){:target="_blank" rel="noopener"}

> A critical unrestricted file upload bug in Contact Form 7 allows an unauthenticated visitor to take over a site running the plugin. [...]
