Title: Swig Security Review 2020 – Part II
Date: 2020-12-31T15:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: swig-security-review-2020-part-ii

[Source](https://portswigger.net/daily-swig/swig-security-review-2020-part-ii){:target="_blank" rel="noopener"}

> Key thinkers on the biggest security stories and trends in 2020 [...]
