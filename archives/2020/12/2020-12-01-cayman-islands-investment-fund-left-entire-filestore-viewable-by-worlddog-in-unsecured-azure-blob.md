Title: Cayman Islands investment fund left entire filestore viewable by world+dog in unsecured Azure blob
Date: 2020-12-01T11:30:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: cayman-islands-investment-fund-left-entire-filestore-viewable-by-worlddog-in-unsecured-azure-blob

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/01/auster_capital_data_breach/){:target="_blank" rel="noopener"}

> Blank share certificates, passport scans, you name it Exclusive A Cayman Islands-based investment fund has exposed its entire backups to the internet after failing to properly configure a secure Microsoft Azure blob.... [...]
