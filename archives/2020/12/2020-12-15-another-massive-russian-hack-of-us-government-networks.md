Title: Another Massive Russian Hack of US Government Networks
Date: 2020-12-15T12:44:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberespionage;cybersecurity;hacking;Russia
Slug: another-massive-russian-hack-of-us-government-networks

[Source](https://www.schneier.com/blog/archives/2020/12/another-massive-russian-hack-of-us-government-networks.html){:target="_blank" rel="noopener"}

> The press is reporting a massive hack of US government networks by sophisticated Russian hackers. Officials said a hunt was on to determine if other parts of the government had been affected by what looked to be one of the most sophisticated, and perhaps among the largest, attacks on federal systems in the past five years. Several said national security-related agencies were also targeted, though it was not clear whether the systems contained highly classified material. [...] The motive for the attack on the agency and the Treasury Department remains elusive, two people familiar with the matter said. One government [...]
