Title: Code42 Incydr Series: Honing in on High-Risk Users with Code42 Incydr
Date: 2020-12-03T14:00:41+00:00
Author: Threatpost
Category: Threatpost
Tags: Web Security;high-risk users
Slug: code42-incydr-series-honing-in-on-high-risk-users-with-code42-incydr

[Source](https://threatpost.com/code42-incydr-series-honing-in-on-high-risk-users-with-code42-incydr/161626/){:target="_blank" rel="noopener"}

> Incydr lets you monitor your high-risk users without impeding their ongoing work. [...]
