Title: Taking a Neighborhood Watch Approach to Retail Cybersecurity
Date: 2020-12-30T15:00:59+00:00
Author: caseyellis
Category: Threatpost
Tags: Cloud Security;Hacks;InfoSec Insider;Mobile Security;Vulnerabilities;Web Security;Amazon Prime Day;bug bounty program;Bugcrowd;Casey Ellis;COVID-19;Cybersecurity;ethical hackers;holiday shopping;neighborhood watch;online retailers;retail cybersecurity;Vulnerability Disclosure
Slug: taking-a-neighborhood-watch-approach-to-retail-cybersecurity

[Source](https://threatpost.com/neighborhood-watch-retail-cybersecurity/162653/){:target="_blank" rel="noopener"}

> Bugcrowd CTO Casey Ellis covers new cybersecurity challenges for online retailers. [...]
