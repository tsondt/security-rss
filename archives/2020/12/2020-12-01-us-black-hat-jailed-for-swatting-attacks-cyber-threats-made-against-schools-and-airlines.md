Title: US black hat jailed for swatting attacks, cyber-threats made against schools and airlines
Date: 2020-12-01T11:59:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-black-hat-jailed-for-swatting-attacks-cyber-threats-made-against-schools-and-airlines

[Source](https://portswigger.net/daily-swig/us-black-hat-jailed-for-swatting-attacks-cyber-threats-made-against-schools-and-airlines){:target="_blank" rel="noopener"}

> Member of hacking group jailed for his role in cybercrime spree [...]
