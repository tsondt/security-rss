Title: Finnish Data Theft and Extortion
Date: 2020-12-10T19:48:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: blackmail;cybercrime;cybersecurity;data breaches;extortion;healthcare;theft
Slug: finnish-data-theft-and-extortion

[Source](https://www.schneier.com/blog/archives/2020/12/finnish-data-theft-and-extortion.html){:target="_blank" rel="noopener"}

> The Finnish psychotherapy clinic Vastaamo was the victim of a data breach and theft. The criminals tried extorting money from the clinic. When that failed, they started extorting money from the patients : Neither the company nor Finnish investigators have released many details about the nature of the breach, but reports say the attackers initially sought a payment of about 450,000 euros to protect about 40,000 patient records. The company reportedly did not pay up. Given the scale of the attack and the sensitive nature of the stolen data, the case has become a national story in Finland. Globally, attacks [...]
