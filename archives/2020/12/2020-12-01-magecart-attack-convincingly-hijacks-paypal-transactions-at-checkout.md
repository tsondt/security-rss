Title: Magecart Attack Convincingly Hijacks PayPal Transactions at Checkout
Date: 2020-12-01T13:18:34+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Web Security;Affable Kraut;Credential Theft;credit card skimmer;e-commerce;magecart;online shopping;PayPal;postMessage;threat actors;twitter
Slug: magecart-attack-convincingly-hijacks-paypal-transactions-at-checkout

[Source](https://threatpost.com/magecart-hijacks-paypal-transactions/161697/){:target="_blank" rel="noopener"}

> New credit-card skimmer uses postMessage to make malicious process look authentic to victims to steal payment data. [...]
