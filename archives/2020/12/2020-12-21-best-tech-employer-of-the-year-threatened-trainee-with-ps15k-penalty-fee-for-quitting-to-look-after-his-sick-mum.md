Title: 'Best tech employer of the year' threatened trainee with £15k penalty fee for quitting to look after his sick mum
Date: 2020-12-21T11:45:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: best-tech-employer-of-the-year-threatened-trainee-with-ps15k-penalty-fee-for-quitting-to-look-after-his-sick-mum

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/21/sparta_global_employment_tribunal/){:target="_blank" rel="noopener"}

> And Sparta Global then didn't bother turning up to the Employment Tribunal A company hailed as the UK's top tech employer tried to diddle a former trainee out of £2,000 in unlawfully withheld back pay – and a judge was startled when he discovered how Sparta Global treats its new hires.... [...]
