Title: Third-Party APIs: How to Prevent Enumeration Attacks
Date: 2020-12-23T17:11:48+00:00
Author: Jason Kent
Category: Threatpost
Tags: InfoSec Insider;Vulnerabilities;Web Security;APIs;cequence;credit card processing;Credit Card Theft;enumeration attack;Fraud;Hacking;infosec insiders;jason kent;online retail;risk;third party
Slug: third-party-apis-how-to-prevent-enumeration-attacks

[Source](https://threatpost.com/third-party-apis-enumeration-attacks/162589/){:target="_blank" rel="noopener"}

> Jason Kent, hacker-in-residence at Cequence, walks through online-retail card fraud and what to do about it. [...]
