Title: 138 AWS services achieve CSA STAR Level 2 certification
Date: 2020-12-17T20:29:05+00:00
Author: Anastasia Strebkova
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS CSA STAR;AWS CSA STAR Certificates;AWS CSA STAR Level 2 Certification;AWS SCA STAR Level 2;Security Blog
Slug: 138-aws-services-achieve-csa-star-level-2-certification

[Source](https://aws.amazon.com/blogs/security/138-aws-services-achieve-csa-star-level-2-certification/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has achieved Cloud Security Alliance (CSA) Security Trust Assurance and Risk (STAR) Level 2 certification with no findings. CSA STAR Level 2 certification is a rigorous third-party independent assessment of the security of a cloud service provider. The certification demonstrates that a cloud service provider conforms [...]
