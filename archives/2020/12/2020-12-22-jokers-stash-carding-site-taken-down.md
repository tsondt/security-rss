Title: Joker’s Stash Carding Site Taken Down
Date: 2020-12-22T16:05:50+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Web Security;.bazar;blockchain dns;carding site;criminal undergorund;Department of Justice;interpol;Joker’s Stash;law enforcement;takedown
Slug: jokers-stash-carding-site-taken-down

[Source](https://threatpost.com/jokers-stash-carding-site-taken-down/162548/){:target="_blank" rel="noopener"}

> The underground payment-card data broker saw its blockchain DNS sites taken offline after an apparent law-enforcement effort - and now Tor sites are down. [...]
