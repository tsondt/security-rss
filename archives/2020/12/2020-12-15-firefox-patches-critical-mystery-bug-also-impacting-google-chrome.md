Title: Firefox Patches Critical Mystery Bug, Also Impacting Google Chrome
Date: 2020-12-15T21:04:30+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities;Web Security;Chromium Project;CVE-2020-16042;Java;memory safety bugs;Mozilla Foundation;Mozilla Foundation Security Advisory;V8 JavaScript Engine;WASM;WebAssembly
Slug: firefox-patches-critical-mystery-bug-also-impacting-google-chrome

[Source](https://threatpost.com/firefox-patches-critical-mystery-bug-also-impacting-google-chrome/162294/){:target="_blank" rel="noopener"}

> Mozilla Foundation releases Firefox 84 browser, fixing several flaws and delivering performance gains and Apple processor support. [...]
