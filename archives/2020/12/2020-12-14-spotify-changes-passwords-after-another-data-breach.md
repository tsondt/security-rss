Title: Spotify Changes Passwords After Another Data Breach
Date: 2020-12-14T20:45:31+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Cloud Security;Web Security;breach;Credential stuffing;data exposure;Passwords;security bug;Spotify;Spotify breach;User data
Slug: spotify-changes-passwords-after-another-data-breach

[Source](https://threatpost.com/spotify-changes-passwords-data-breach/162256/){:target="_blank" rel="noopener"}

> This is the third breach in the past few weeks for the world’s most popular streaming service. [...]
