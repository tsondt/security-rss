Title: As if Productivity Score wasn't creepy enough, Microsoft has patented tech for 'meeting quality monitoring devices'
Date: 2020-12-01T14:30:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: as-if-productivity-score-wasnt-creepy-enough-microsoft-has-patented-tech-for-meeting-quality-monitoring-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/01/microsoft_meeting_insight/){:target="_blank" rel="noopener"}

> Speech patterns consistent with boredom?! Minus 10 points! The slightly creepy "Productivity Score" may not be all that's in store for Microsoft 365 users, judging by a trawl of Redmond's patents.... [...]
