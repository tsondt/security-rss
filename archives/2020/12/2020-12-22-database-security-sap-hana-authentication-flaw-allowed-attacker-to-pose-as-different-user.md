Title: Database security: SAP HANA authentication flaw allowed attacker to pose as different user
Date: 2020-12-22T13:06:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: database-security-sap-hana-authentication-flaw-allowed-attacker-to-pose-as-different-user

[Source](https://portswigger.net/daily-swig/database-security-sap-hana-authentication-flaw-allowed-attacker-to-pose-as-different-user){:target="_blank" rel="noopener"}

> Developers address SAML implementation bug [...]
