Title: Largest global staffing agency Randstad hit by Egregor ransomware
Date: 2020-12-04T10:25:16
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: largest-global-staffing-agency-randstad-hit-by-egregor-ransomware

[Source](https://www.bleepingcomputer.com/news/security/largest-global-staffing-agency-randstad-hit-by-egregor-ransomware/){:target="_blank" rel="noopener"}

> Staffing agency Randstad NV announced today that their network was breached by the Egregor ransomware, who stole unencrypted files during the attack. [...]
