Title: Microsoft identifies 40+ victims of SolarWinds hack, 80% from US
Date: 2020-12-18T08:56:33
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-identifies-40-victims-of-solarwinds-hack-80-from-us

[Source](https://www.bleepingcomputer.com/news/security/microsoft-identifies-40-plus-victims-of-solarwinds-hack-80-percent-from-us/){:target="_blank" rel="noopener"}

> Microsoft said that over 40 of its customers had their networks infiltrated by hackers following the SolarWinds supply chain attack after they installed backdoored versions of the Orion IT monitoring platform. [...]
