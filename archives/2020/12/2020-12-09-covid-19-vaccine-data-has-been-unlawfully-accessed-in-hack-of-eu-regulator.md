Title: COVID-19 vaccine data has been unlawfully accessed in hack of EU regulator
Date: 2020-12-09T21:19:32+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;COVID-19;hackers;hacking;vaccine
Slug: covid-19-vaccine-data-has-been-unlawfully-accessed-in-hack-of-eu-regulator

[Source](https://arstechnica.com/?p=1728782){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Information relating to the one of the most promising coronavirus vaccines has been “unlawfully accessed” following a hack on the European regulatory body that’s in the final stages of approving it, the firms jointly developing the vaccine said on Wednesday. The European Medicines Agency based in Amsterdam first disclosed the breach. The statement said only that the EMA had been subject to a cyberattack and that it had begun a joint investigation along with law enforcement. The agency didn’t say when the hack happened or whether the attackers sought vaccine information, tried to infect the [...]
