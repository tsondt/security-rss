Title: Whistleblowers have come to us alleging spy agency wrongdoing, says UK auditor IPCO
Date: 2020-12-17T12:35:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: whistleblowers-have-come-to-us-alleging-spy-agency-wrongdoing-says-uk-auditor-ipco

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/17/ipco_annual_report/){:target="_blank" rel="noopener"}

> And police are backsliding on vital legal paperwork, warns body Three UK law enforcement agents blew the whistle about unlawful state surveillance to the Investigatory Powers Commissioner’s office – and one of those incidents was bad enough for the investigation to still be ongoing today.... [...]
