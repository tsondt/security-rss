Title: Ransomware and IP Theft: Top COVID-19 Healthcare Security Scares
Date: 2020-12-15T14:00:44+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Critical Infrastructure;IoT;Malware;Videos;coronavirus;COVID-19;COVID-19 supply chain;Healthcare;healthcare security;hospital ransomware;Hospitals;IoT Medical Devices;IP theft;Pandemic;Ransomware Attack;UVM health network
Slug: ransomware-and-ip-theft-top-covid-19-healthcare-security-scares

[Source](https://threatpost.com/ransomware-ip-theft-top-covid-19-healthcare-security-scares/162247/){:target="_blank" rel="noopener"}

> From ransomware attacks that crippled hospitals, to espionage attacks targeting COVID-19 vaccine supply chain, Beau Woods discusses the top healthcare security risks. [...]
