Title: Ryuk, Egregor Ransomware Attacks Leverage SystemBC Backdoor
Date: 2020-12-16T18:37:18+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Malware;backdoor;cobalt strike;egregor;qbot;ransomware;Ransomware Attack;ryuk;socks5 proxy;systembc;Tor
Slug: ryuk-egregor-ransomware-attacks-leverage-systembc-backdoor

[Source](https://threatpost.com/ryuk-egregor-ransomware-systembc-backdoor/162333/){:target="_blank" rel="noopener"}

> In the past few months researchers have detected hundreds of attempted SystemBC deployments globally, as part of recent Ryuk and Egregor ransomware attacks. [...]
