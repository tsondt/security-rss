Title: Ex-Cisco Employee Convicted for Deleting 16K Webex Accounts
Date: 2020-12-14T19:50:14+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security;Cisco;conviction;Court case;deleted accounts;sentencing;Sudhish Kasaba Ramesh;Webex
Slug: ex-cisco-employee-convicted-for-deleting-16k-webex-accounts

[Source](https://threatpost.com/cisco-employee-convicted-deleting-webex-accounts/162246/){:target="_blank" rel="noopener"}

> The insider threat will go to jail for two years after compromising Cisco's cloud infrastructure. [...]
