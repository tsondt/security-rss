Title: Ex-Cisco engineer who nuked 16k WebEx accounts goes to prison
Date: 2020-12-11T10:59:29
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ex-cisco-engineer-who-nuked-16k-webex-accounts-goes-to-prison

[Source](https://www.bleepingcomputer.com/news/security/ex-cisco-engineer-who-nuked-16k-webex-accounts-goes-to-prison/){:target="_blank" rel="noopener"}

> Sudhish Kasaba Ramesh, a former Cisco engineer, was sentenced on Wednesday to two years in prison and ordered to pay a $15,000 fine for shutting down more than 16,000 WebEx Teams accounts and over 450 virtual machines in 2018, [...]
