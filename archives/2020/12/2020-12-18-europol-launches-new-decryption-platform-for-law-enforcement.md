Title: Europol launches new decryption platform for law enforcement
Date: 2020-12-18T13:01:03
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: europol-launches-new-decryption-platform-for-law-enforcement

[Source](https://www.bleepingcomputer.com/news/security/europol-launches-new-decryption-platform-for-law-enforcement/){:target="_blank" rel="noopener"}

> Europol and the European Commission have launched a new decryption platform that will help boost Europol's ability to gain access to information stored in encrypted media collected during criminal investigations. [...]
