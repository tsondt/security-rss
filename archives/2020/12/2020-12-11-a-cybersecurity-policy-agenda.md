Title: A Cybersecurity Policy Agenda
Date: 2020-12-11T12:57:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;national security policy;reports;risks
Slug: a-cybersecurity-policy-agenda

[Source](https://www.schneier.com/blog/archives/2020/12/a-cybersecurity-policy-agenda.html){:target="_blank" rel="noopener"}

> The Aspen Institute’s Aspen Cybersecurity Group — I’m a member — has released its cybersecurity policy agenda for the next four years. The next administration and Congress cannot simultaneously address the wide array of cybersecurity risks confronting modern society. Policymakers in the White House, federal agencies, and Congress should zero in on the most important and solvable problems. To that end, this report covers five priority areas where we believe cybersecurity policymakers should focus their attention and resources as they contend with a presidential transition, a new Congress, and massive staff turnover across our nation’s capital. Education and Workforce Development [...]
