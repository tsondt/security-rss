Title: Roanoke College delays spring semester after cyberattack
Date: 2020-12-22T16:27:07
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: roanoke-college-delays-spring-semester-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/roanoke-college-delays-spring-semester-after-cyberattack/){:target="_blank" rel="noopener"}

> Roanoke College has delayed their spring semester by almost a month after a cyberattack has impacted files and data access. [...]
