Title: MoleRats APT Returns with Espionage Play Using Facebook, Dropbox
Date: 2020-12-10T17:50:29+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Malware;Web Security;analysis;apt;backdoor;Cloud Services;cyber espionage;Cybereason;dropbook;dropbox;Facebook;malware;Middle East;molerats;Phishing;quasar rat;sharpstage
Slug: molerats-apt-returns-with-espionage-play-using-facebook-dropbox

[Source](https://threatpost.com/molerats-apt-espionage-facebook-dropbox/162162/){:target="_blank" rel="noopener"}

> The threat group is increasing its espionage activity in light of the current political climate and recent events in the Middle East, with two new backdoors. [...]
