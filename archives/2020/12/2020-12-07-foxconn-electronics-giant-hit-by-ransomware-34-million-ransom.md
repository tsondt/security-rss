Title: Foxconn electronics giant hit by ransomware, $34 million ransom
Date: 2020-12-07T13:01:04
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: foxconn-electronics-giant-hit-by-ransomware-34-million-ransom

[Source](https://www.bleepingcomputer.com/news/security/foxconn-electronics-giant-hit-by-ransomware-34-million-ransom/){:target="_blank" rel="noopener"}

> Foxconn electronics giant suffered a ransomware attack at a Mexican facility over the Thanksgiving weekend, where attackers stole unencrypted files before encrypting devices. [...]
