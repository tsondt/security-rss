Title: Pfizer COVID-19 vaccine documents accessed in EMA cyberattack
Date: 2020-12-09T13:51:08
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: pfizer-covid-19-vaccine-documents-accessed-in-ema-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/pfizer-covid-19-vaccine-documents-accessed-in-ema-cyberattack/){:target="_blank" rel="noopener"}

> The European Medicines Agency (EMA) responsible for COVID-19 vaccine approval has suffered a cyberattack of an undisclosed nature, according to a statement posted on their website. [...]
