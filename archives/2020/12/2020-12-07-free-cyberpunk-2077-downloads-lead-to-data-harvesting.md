Title: ‘Free’ Cyberpunk 2077 Downloads Lead to Data Harvesting
Date: 2020-12-07T20:01:47+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Privacy;Web Security;contact information;cyberpunk 2077;data harvesting;fake installer;free download;Hoax;Kaspersky;keanu reeves;release date;review;scam;survey
Slug: free-cyberpunk-2077-downloads-lead-to-data-harvesting

[Source](https://threatpost.com/free-cyberpunk-2077-downloads/161963/){:target="_blank" rel="noopener"}

> The hotly anticipated game -- featuring a digital Keanu Reeves as a major character -- is being used as a lure for cyberattacks. [...]
