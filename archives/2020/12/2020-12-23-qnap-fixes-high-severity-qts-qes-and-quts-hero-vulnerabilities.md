Title: QNAP fixes high severity QTS, QES, and QuTS hero vulnerabilities
Date: 2020-12-23T09:59:47
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qnap-fixes-high-severity-qts-qes-and-quts-hero-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/qnap-fixes-high-severity-qts-qes-and-quts-hero-vulnerabilities/){:target="_blank" rel="noopener"}

> QNAP has released security updates to fix multiple high severity security vulnerabilities impacting network-attached storage (NAS) devices running the QES, QTS, and QuTS hero operating systems. [...]
