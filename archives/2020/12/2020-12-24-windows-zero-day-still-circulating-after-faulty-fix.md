Title: Windows Zero-Day Still Circulating After Faulty Fix
Date: 2020-12-24T16:31:38+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;bad patch;CVE-2020-0986;CVE-2020-17008;Google Project Zero;Local Privilege Escalation;Proof of Concept;unpatched;Windows zero day
Slug: windows-zero-day-still-circulating-after-faulty-fix

[Source](https://threatpost.com/windows-zero-day-circulating-faulty-fix/162610/){:target="_blank" rel="noopener"}

> The LPE bug could allow an attacker to install programs; view, change, or delete data; or create new accounts with full user rights. [...]
