Title: The SolarWinds cyberattack: The hack, the victims, and what we know
Date: 2020-12-19T10:10:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-solarwinds-cyberattack-the-hack-the-victims-and-what-we-know

[Source](https://www.bleepingcomputer.com/news/security/the-solarwinds-cyberattack-the-hack-the-victims-and-what-we-know/){:target="_blank" rel="noopener"}

> Since the SolarWinds supply chain attack was disclosed last Sunday, there has been a whirlwind of news, technical details, and analysis released about the hack. Because the amount of information that was released in such a short time is definitely overwhelming, we have published this as a roundup of this week's SolarWinds news. [...]
