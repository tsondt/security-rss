Title: Verizon has been leaking customers’ personal information for days (at least)
Date: 2020-12-03T18:05:30+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;personal information;privacy;verizon;website flaws
Slug: verizon-has-been-leaking-customers-personal-information-for-days-at-least

[Source](https://arstechnica.com/?p=1727360){:target="_blank" rel="noopener"}

> Enlarge / A Verizon FiOS truck in Manhattan on September 15, 2017. (credit: Getty Images | Smith Collection | Gado ) Verizon is struggling to fix a glitch that has been leaking customers’ addresses, phone numbers, account numbers, and other personal information through a chat system that helps prospective subscribers figure out if Fios services are available in their location. The personal details appear when people click on a link to chat with a Verizon representative. When the chat window opens, it contains transcripts of conversations that other customers, either prospective or current, have had. The transcripts include full names, [...]
