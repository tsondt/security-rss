Title: Adobe fixes critical security vulnerabilities in Lightroom, Prelude
Date: 2020-12-09T09:26:48
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: adobe-fixes-critical-security-vulnerabilities-in-lightroom-prelude

[Source](https://www.bleepingcomputer.com/news/security/adobe-fixes-critical-security-vulnerabilities-in-lightroom-prelude/){:target="_blank" rel="noopener"}

> Adobe has released security updates to address critical severity security bugs affecting Windows and macOS versions of Adobe Lightroom and Adobe Prelude. [...]
