Title: FBI warns of BEC scammers using email auto-forwarding in attacks
Date: 2020-12-01T09:30:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-bec-scammers-using-email-auto-forwarding-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-bec-scammers-using-email-auto-forwarding-in-attacks/){:target="_blank" rel="noopener"}

> The FBI is warning U.S. companies about scammers actively abusing auto-forwarding rules on web-based email clients to increase the likelihood of successful Business Email Compromise (BEC) attacks. [...]
