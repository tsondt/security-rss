Title: How cyber-attackers are coming after you in 2021
Date: 2020-12-17T11:15:04+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: how-cyber-attackers-are-coming-after-you-in-2021

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/17/attackers_are_coming_in_2021/){:target="_blank" rel="noopener"}

> And how you can defend yourself Sponsored Cybersecurity company Darktrace understands digital attack techniques better than most. Its unique AI-driven approach evolved out of work between former spies from government intelligence agencies and mathematicians at the University of Cambridge.... [...]
