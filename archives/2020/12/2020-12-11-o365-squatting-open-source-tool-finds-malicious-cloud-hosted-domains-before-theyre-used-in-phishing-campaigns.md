Title: O365 Squatting: Open source tool finds malicious cloud-hosted domains before they’re used in phishing campaigns
Date: 2020-12-11T13:46:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: o365-squatting-open-source-tool-finds-malicious-cloud-hosted-domains-before-theyre-used-in-phishing-campaigns

[Source](https://portswigger.net/daily-swig/o365-squatting-open-source-tool-finds-malicious-cloud-hosted-domains-before-theyre-used-in-phishing-campaigns){:target="_blank" rel="noopener"}

> First-of-its-kind recon module trawls cloud infrastructure rather than using DNS queries [...]
