Title: Black Hat Europe: Diverse security teams are key to successful government cyber defense
Date: 2020-12-09T15:25:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: black-hat-europe-diverse-security-teams-are-key-to-successful-government-cyber-defense

[Source](https://portswigger.net/daily-swig/black-hat-europe-diverse-security-teams-are-key-to-successful-government-cyber-defense){:target="_blank" rel="noopener"}

> Technology comes second to creative thinking, says UK cyber expert Pete Cooper during today’s virtual keynote [...]
