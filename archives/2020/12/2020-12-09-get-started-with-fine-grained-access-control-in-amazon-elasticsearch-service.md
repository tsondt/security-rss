Title: Get started with fine-grained access control in Amazon Elasticsearch Service
Date: 2020-12-09T18:09:39+00:00
Author: Jon Handler
Category: AWS Security
Tags: Amazon Elasticsearch Service;Intermediate (200);Security, Identity, & Compliance;Amazon ES;fine grained access control;Kibana;Security Blog
Slug: get-started-with-fine-grained-access-control-in-amazon-elasticsearch-service

[Source](https://aws.amazon.com/blogs/security/get-started-with-fine-grained-access-control-in-amazon-elasticsearch-service/){:target="_blank" rel="noopener"}

> Amazon Elasticsearch Service (Amazon ES) provides fine-grained access control, powered by the Open Distro for Elasticsearch security plugin. The security plugin adds Kibana authentication and access control at the cluster, index, document, and field levels that can help you secure your data. You now have many different ways to configure your Amazon ES domain to [...]
