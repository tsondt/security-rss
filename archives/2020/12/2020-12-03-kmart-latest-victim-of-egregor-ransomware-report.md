Title: Kmart, Latest Victim of Egregor Ransomware – Report
Date: 2020-12-03T22:04:33+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Web Security;back-end servers;cyberattack;egregor;holiday shopping;Kmart;ransomware
Slug: kmart-latest-victim-of-egregor-ransomware-report

[Source](https://threatpost.com/kmart-egregor-ransomware/161881/){:target="_blank" rel="noopener"}

> The struggling retailer's back-end services have been impacted, according to a report, just in time for the holidays. [...]
