Title: FBI: Iran behind pro-Trump ‘enemies of the people’ doxing site
Date: 2020-12-23T18:34:06
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: fbi-iran-behind-pro-trump-enemies-of-the-people-doxing-site

[Source](https://www.bleepingcomputer.com/news/security/fbi-iran-behind-pro-trump-enemies-of-the-people-doxing-site/){:target="_blank" rel="noopener"}

> Iranian cyber actors are likely behind a campaign that encouraged deadly violence against U.S. state officials certifying the 2020 election results. [...]
