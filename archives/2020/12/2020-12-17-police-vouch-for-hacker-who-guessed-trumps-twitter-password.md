Title: Police Vouch for Hacker Who Guessed Trump’s Twitter Password
Date: 2020-12-17T19:42:45+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security;2FA;charges;dutch police;Ethical Hacker;Ethical Hacking;MAGA2020!;prosecution;Trump;Trump hack;Trump twitter account;Victor Gevers
Slug: police-vouch-for-hacker-who-guessed-trumps-twitter-password

[Source](https://threatpost.com/police-vouch-hacker-trump-twitter-password/162371/){:target="_blank" rel="noopener"}

> No charges for Dutch ethical hacker Victor Gevers who prosecutors say did actually access Trump’s Twitter account by guessing his password, “MAGA2020!” last October. [...]
