Title: Fake data breach alerts used to steal Ledger cryptocurrency wallets
Date: 2020-12-10T17:54:40
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fake-data-breach-alerts-used-to-steal-ledger-cryptocurrency-wallets

[Source](https://www.bleepingcomputer.com/news/security/fake-data-breach-alerts-used-to-steal-ledger-cryptocurrency-wallets/){:target="_blank" rel="noopener"}

> A phishing scam is underway that targets Ledger wallet users with fake data breach notifications used to steal cryptocurrency from recipients. [...]
