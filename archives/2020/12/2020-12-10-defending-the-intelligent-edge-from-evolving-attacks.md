Title: Defending the Intelligent Edge from Evolving Attacks
Date: 2020-12-10T21:24:13+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: Cloud Security;Hacks;InfoSec Insider;Malware;Vulnerabilities;Web Security;Aamir Lakhani;best practices;cloud networks;cyber-defense;Cybersecurity;EAT;edge access trojan;Fortinet;intelligent edge;network perimeter;remote workers;securing company data;work from home
Slug: defending-the-intelligent-edge-from-evolving-attacks

[Source](https://threatpost.com/defending-intelligent-edge-evolving-attacks/162172/){:target="_blank" rel="noopener"}

> Fortinet's Aamir Lakhani discusses best practices for securing company data against next-gen threats, like edge access trojans (EATs). [...]
