Title: Bcrypt implementation flaw in Bouncy Castle crypto library laid bare
Date: 2020-12-21T15:58:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: bcrypt-implementation-flaw-in-bouncy-castle-crypto-library-laid-bare

[Source](https://portswigger.net/daily-swig/bcrypt-implementation-flaw-in-bouncy-castle-crypto-library-laid-bare){:target="_blank" rel="noopener"}

> API cryptography software made a hash of authentication checks [...]
