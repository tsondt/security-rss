Title: Account Hijacking Site OGUsers Hacked, Again
Date: 2020-12-02T16:29:13+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Ne'er-Do-Well News;Disco Payments;Discoli;ogusers;OGusers breach
Slug: account-hijacking-site-ogusers-hacked-again

[Source](https://krebsonsecurity.com/2020/12/account-hijacking-site-ogusers-hacked-again/){:target="_blank" rel="noopener"}

> For at least the third time in its existence, OGUsers — a forum overrun with people looking to buy, sell and trade access to compromised social media accounts — has been hacked. An offer by the apparent hackers of OGUsers, offering to remove account information from the eventual database leak in exchange for payment. Roughly a week ago, the OGUsers homepage was defaced with a message stating the forum’s user database had been compromised. The hack was acknowledged by the forum’s current administrator, who assured members that their passwords were protected with a password obfuscation technology that was extremely difficult [...]
