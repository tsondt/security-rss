Title: DHS orders federal agencies to update SolarWinds Orion platform
Date: 2020-12-30T12:18:22
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: dhs-orders-federal-agencies-to-update-solarwinds-orion-platform

[Source](https://www.bleepingcomputer.com/news/security/dhs-orders-federal-agencies-to-update-solarwinds-orion-platform/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has ordered all US federal agencies to update the SolarWinds Orion platform to the latest version by the end of business hours on December 31, 2020. [...]
