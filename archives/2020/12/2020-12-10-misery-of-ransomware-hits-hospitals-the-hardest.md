Title: Misery of Ransomware Hits Hospitals the Hardest
Date: 2020-12-10T12:44:36+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Healthcare;Hospitals;ransomware;Universal Health Services
Slug: misery-of-ransomware-hits-hospitals-the-hardest

[Source](https://threatpost.com/ransomware-hits-hospitals-hardest/162096/){:target="_blank" rel="noopener"}

> Ransomware attacks targeting hospitals have exacted a human cost as well as financial. [...]
