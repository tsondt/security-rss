Title: Making Sense of the Security Sensor Landscape
Date: 2020-12-04T21:33:42+00:00
Author: Chris Calvert
Category: Threatpost
Tags: InfoSec Insider;best practices;challenges;chris calvert;Compliance;Defensive;efficacy;FireEye;infosec insider;managed security services provider;MSSP;Network security;network security sensors;respond software;security operations center;Sensitivity;Visibility
Slug: making-sense-of-the-security-sensor-landscape

[Source](https://threatpost.com/making-sense-security-sensor-landscape/161911/){:target="_blank" rel="noopener"}

> Chris Calvert of Respond Software (now part of FireEye) outlines the challenges that reduce the efficacy of network security sensors. [...]
