Title: Cops raid home of ousted data scientist who created her own Florida COVID-19 dashboard
Date: 2020-12-08T02:52:17+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: cops-raid-home-of-ousted-data-scientist-who-created-her-own-florida-covid-19-dashboard

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/08/rebekah_jones_police_raid/){:target="_blank" rel="noopener"}

> Rebekah Jones claimed Sunshine State fudged infection numbers, now she's suspected of hacking Florida's state police on Monday morning raided the home of coronavirus tracker Rebekah Jones, seizing her electronics at gunpoint as part of a computer hacking investigation.... [...]
