Title: Trucking giant Forward Air hit by new Hades ransomware gang
Date: 2020-12-21T16:23:13
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: trucking-giant-forward-air-hit-by-new-hades-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/trucking-giant-forward-air-hit-by-new-hades-ransomware-gang/){:target="_blank" rel="noopener"}

> Trucking and freight logistics company Forward Air has suffered a ransomware attack by a new ransomware gang that has impacted the company's business operations. [...]
