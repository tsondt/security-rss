Title: South Korea kills ActiveX-based government digital certificate service
Date: 2020-12-10T04:31:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: south-korea-kills-activex-based-government-digital-certificate-service

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/10/south_korea_activex_certs_dead/){:target="_blank" rel="noopener"}

> Service was so unpopular, getting rid of it was an election policy South Korea on Thursday shuttered a government-run digital certificate service that required the use of Microsoft’s ancient ActiveX technology.... [...]
