Title: Record Levels of Software Bugs Plague Short-Staffed IT Teams in 2020
Date: 2020-12-09T21:26:54+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Most Recent ThreatLists;Vulnerabilities;2020 total bug reports;Bug;Bugs;comparitech;cyberpion;cybersecurity skills;fujiwara events;IT security teams;IT teams;Patch management;patch tuesday;personnel;retailers;risk based security;software patching;software vulnerabilities;third-party online software;top 50 vendors;vulnerability;vulnerability Fujiwara events;workforce shortage
Slug: record-levels-of-software-bugs-plague-short-staffed-it-teams-in-2020

[Source](https://threatpost.com/record-levels-software-bugs-it-teams-2020/162095/){:target="_blank" rel="noopener"}

> As just one symptom, 83 percent of the Top 30 U.S. retailers have vulnerabilities which pose an “imminent” cyber-threat, including Amazon, Costco, Kroger and Walmart. [...]
