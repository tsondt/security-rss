Title: FireEye, Microsoft create kill switch for SolarWinds backdoor
Date: 2020-12-16T16:21:50
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: fireeye-microsoft-create-kill-switch-for-solarwinds-backdoor

[Source](https://www.bleepingcomputer.com/news/security/fireeye-microsoft-create-kill-switch-for-solarwinds-backdoor/){:target="_blank" rel="noopener"}

> Microsoft, FireEye, and GoDaddy have collaborated to create a kill switch for the SolarWinds Sunburst backdoor that forces the malware to terminate itself. [...]
