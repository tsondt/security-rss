Title: Insider Report: Healthcare Security Woes Balloon in COVID-Era
Date: 2020-12-07T17:19:24+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Critical Infrastructure;eBook;IoT;Malware;Vulnerabilities;COVID-19;cyberattack;data theft;Healthcare;HIPPA;Hospital;patient data privacy;Ransomware Attack;UVM health network
Slug: insider-report-healthcare-security-woes-balloon-in-covid-era

[Source](https://threatpost.com/insider-report-healthcare-security-covid-era/161801/){:target="_blank" rel="noopener"}

> As hackers put a bullseye on healthcare, Threatpost spotlights how hospitals, researchers and patients have been affected and how the sector is bolstering their cyber defenses. [...]
