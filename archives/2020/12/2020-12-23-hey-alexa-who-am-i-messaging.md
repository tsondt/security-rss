Title: Hey Alexa, Who Am I Messaging?
Date: 2020-12-23T15:55:22+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: IoT;Amazon Alexa;amazon echo;Connected devices;Cybersecurity;data theft;digital home assistant;digital smart home;eavesdrop;Researchers;secret recording;security research;sensitive information;smartphone;university of cambridge
Slug: hey-alexa-who-am-i-messaging

[Source](https://threatpost.com/hey-alexa-who-messaging/162587/){:target="_blank" rel="noopener"}

> Research shows that microphones on digital assistants are sensitive enough to record what someone is typing on a smartphone to steal PINs and other sensitive info. [...]
