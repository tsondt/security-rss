Title: VMware Flaw a Vector in SolarWinds Breach?
Date: 2020-12-18T18:33:13+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;The Coming Storm;APT 29;Ars Technica;CISA;Cozy Bear;Cyber Security and Infrastructure Security Agency;Duo;FSB;Microsoft Outlook Web App;New York Times;nsa;SAML token compromise;Security Assertion Markup Language;U.S. National Security Agency;VMware;Volexity;washington post
Slug: vmware-flaw-a-vector-in-solarwinds-breach

[Source](https://krebsonsecurity.com/2020/12/vmware-flaw-a-vector-in-solarwinds-breach/){:target="_blank" rel="noopener"}

> U.S. government cybersecurity agencies warned this week that the attackers behind the widespread hacking spree stemming from the compromise at network software firm SolarWinds used weaknesses in other, non-SolarWinds products to attack high-value targets. According to sources, among those was a flaw in software virtualization platform VMware, which the U.S. National Security Agency (NSA) warned on Dec. 7 was being used by Russian hackers to impersonate authorized users on victim networks. On Dec. 7, 2020, the NSA said “Russian state-sponsored malicious cyber actors are exploiting a vulnerability in VMware Access and VMware Identity Manager products, allowing the actors access to [...]
