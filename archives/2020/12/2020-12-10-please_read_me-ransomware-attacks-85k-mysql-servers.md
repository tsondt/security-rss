Title: PLEASE_READ_ME Ransomware Attacks 85K MySQL Servers
Date: 2020-12-10T16:26:14+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;compromised data;cyberattack;MySQL;PLEASE_READ_ME;ransom;ransomware;ransomware extortion;Server;stolen data
Slug: please_read_me-ransomware-attacks-85k-mysql-servers

[Source](https://threatpost.com/please_read_me-ransomware-mysql-servers/162136/){:target="_blank" rel="noopener"}

> Ransomware actors behind the attack have breached at least 85,000 MySQL servers, and are currently selling at least compromised 250,000 databases. [...]
