Title: Eavesdropping on Phone Taps from Voice Assistants
Date: 2020-12-22T16:21:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;eavesdropping;privacy;side-channel attacks;surveillance
Slug: eavesdropping-on-phone-taps-from-voice-assistants

[Source](https://www.schneier.com/blog/archives/2020/12/eavesdropping-on-phone-taps-from-voice-assistants.html){:target="_blank" rel="noopener"}

> The microphones on voice assistants are very sensitive, and can snoop on all sorts of data : In Hey Alexa what did I just type? we show that when sitting up to half a meter away, a voice assistant can still hear the taps you make on your phone, even in presence of noise. Modern voice assistants have two to seven microphones, so they can do directional localisation, just as human ears do, but with greater sensitivity. We assess the risk and show that a lot more work is needed to understand the privacy implications of the always-on microphones that [...]
