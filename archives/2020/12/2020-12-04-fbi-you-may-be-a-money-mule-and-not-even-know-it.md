Title: FBI: You may be a money mule and not even know it
Date: 2020-12-04T15:50:39
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-you-may-be-a-money-mule-and-not-even-know-it

[Source](https://www.bleepingcomputer.com/news/security/fbi-you-may-be-a-money-mule-and-not-even-know-it/){:target="_blank" rel="noopener"}

> The FBI has warned of an increasing number of scammers preying on unemployed Americans by trying to recruit them into their money mule schemes and use them to launder funds obtained via fraud, online scams, and other types of criminal activities. [...]
