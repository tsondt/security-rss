Title: Microsoft Wraps Up a Lighter Patch Tuesday for the Holidays
Date: 2020-12-08T20:23:30+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security;critical;december 2020;Exchange Server;Microsoft;patch tuesday;patching priority;Security Bugs;security patches;Sharepoint
Slug: microsoft-wraps-up-a-lighter-patch-tuesday-for-the-holidays

[Source](https://threatpost.com/microsoft-patch-tuesday-holidays/162041/){:target="_blank" rel="noopener"}

> Nine critical bugs and 58 overall fixes mark the last scheduled security advisory of 2020. [...]
