Title: Think-Tanks Under Attack by Foreign APTs, CISA Warns
Date: 2020-12-02T21:21:55+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Government;Hacks;Malware;Vulnerabilities;Web Security;advanced persistent threat;apt;cisa alert;covid-19. remote access;Cyberattacks;data exfiltration;espionage;exploits;nation states;Phishing;remote working;spearphishing;Think tanks;VPN;vulnerabilities
Slug: think-tanks-under-attack-by-foreign-apts-cisa-warns

[Source](https://threatpost.com/think-tanks-attack-apts-cisa/161807/){:target="_blank" rel="noopener"}

> The feds have seen ongoing cyberattacks on think-tanks (bent on espionage, malware delivery and more), using phishing and VPN exploits as primary attack vectors. [...]
