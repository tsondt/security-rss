Title: SolarWinds’ shares drop 22 per cent. But what’s this? $286m in stock sales just before hack announced?
Date: 2020-12-16T23:58:12+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: solarwinds-shares-drop-22-per-cent-but-whats-this-286m-in-stock-sales-just-before-hack-announced

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/16/solarwinds_stock_sale/){:target="_blank" rel="noopener"}

> VC firms say they weren't aware Orion code had been backdoored Two Silicon Valley VC firms, Silver Lake and Thoma Bravo, sold hundreds of millions of dollars in SolarWinds shares just days before the software biz emerged at the center of a massive hacking campaign.... [...]
