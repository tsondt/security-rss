Title: Flavors designer Symrise halts production after Clop ransomware attack
Date: 2020-12-20T14:20:28
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: flavors-designer-symrise-halts-production-after-clop-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/flavors-designer-symrise-halts-production-after-clop-ransomware-attack/){:target="_blank" rel="noopener"}

> Flavor and fragrance developer Symrise has suffered a Clop ransomware attack where the attackers allegedly stole 500 GB of unencrypted files and encrypted close to 1,000 devices. [...]
