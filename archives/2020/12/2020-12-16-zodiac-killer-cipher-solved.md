Title: Zodiac Killer Cipher Solved
Date: 2020-12-16T13:01:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: crime;cryptanalysis;encryption
Slug: zodiac-killer-cipher-solved

[Source](https://www.schneier.com/blog/archives/2020/12/zodiac-killer-cipher-solved.html){:target="_blank" rel="noopener"}

> The SF Chronicle is reporting (more details here ), and the FBI is confirming, that a Melbourne mathematician and team has decrypted the 1969 message sent by the Zodiac Killer to the newspaper. There’s no paper yet, but there are a bunch of details in the news articles. Here’s an interview with one of the researchers: Cryptologist David Oranchak, who has been trying to crack the notorious “340 cipher” (it contains 340 characters) for more than a decade, made a crucial breakthrough earlier this year when applied mathematician Sam Blake came up with about 650,000 different possible ways in which [...]
