Title: Healthcare security woes: More than 45 million medical images openly accessible online
Date: 2020-12-15T11:52:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: healthcare-security-woes-more-than-45-million-medical-images-openly-accessible-online

[Source](https://portswigger.net/daily-swig/healthcare-security-woes-more-than-45-million-medical-images-openly-accessible-online){:target="_blank" rel="noopener"}

> Huge breach of personal data due to unprotected storage devices [...]
