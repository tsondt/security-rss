Title: Google, Qualcomm team up to make long-term Android updates easier on Snapdragon
Date: 2020-12-17T19:31:05+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: google-qualcomm-team-up-to-make-long-term-android-updates-easier-on-snapdragon

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/17/google_qualcomm_patching/){:target="_blank" rel="noopener"}

> Long-term patching problems still haunt web giant's mobile OS platform Google and Qualcomm have linked arms to extend the lifecycle of new Android devices, meaning future phones could receive as many as three major operating system updates provided they're running the latest Snapdragon silicon.... [...]
