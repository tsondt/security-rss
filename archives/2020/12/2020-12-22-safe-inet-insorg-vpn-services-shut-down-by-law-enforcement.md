Title: Safe-Inet, Insorg VPN services shut down by law enforcement
Date: 2020-12-22T13:57:08
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: safe-inet-insorg-vpn-services-shut-down-by-law-enforcement

[Source](https://www.bleepingcomputer.com/news/security/safe-inet-insorg-vpn-services-shut-down-by-law-enforcement/){:target="_blank" rel="noopener"}

> Law enforcement agencies around the world in a coordinated effort took down and seized the infrastructure supporting Safe-Inet and Insorg VPN and proxy services known for catering cybercriminal activity. [...]
