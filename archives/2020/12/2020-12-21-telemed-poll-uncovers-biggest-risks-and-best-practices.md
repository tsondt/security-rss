Title: Telemed Poll Uncovers Biggest Risks and Best Practices
Date: 2020-12-21T12:00:41+00:00
Author: Threatpost
Category: Threatpost
Tags: IoT;Malware;Privacy;Vulnerabilities;Bugcrowd;COVID-19;cyberattack;Healthcare;HIPAA;hospital IT departments;telemed;threatpost poll;zoom
Slug: telemed-poll-uncovers-biggest-risks-and-best-practices

[Source](https://threatpost.com/telemed-poll-uncovers-risks-best-practices/162444/){:target="_blank" rel="noopener"}

> What are the riskiest links in the virtual healthcare chain? Threatpost readers weigh in as part of an exclusive telemed poll. [...]
