Title: Emergency directive: Global governments issue alert after FireEye hack is linked to SolarWinds supply chain attack
Date: 2020-12-14T14:42:47+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: emergency-directive-global-governments-issue-alert-after-fireeye-hack-is-linked-to-solarwinds-supply-chain-attack

[Source](https://portswigger.net/daily-swig/emergency-directive-global-governments-issue-alert-after-fireeye-hack-is-linked-to-solarwinds-supply-chain-attack){:target="_blank" rel="noopener"}

> Authorities worldwide caution against use of IT management tool Orion [...]
