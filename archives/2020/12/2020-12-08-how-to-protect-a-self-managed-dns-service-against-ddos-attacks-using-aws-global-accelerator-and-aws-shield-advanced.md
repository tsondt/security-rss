Title: How to protect a self-managed DNS service against DDoS attacks using AWS Global Accelerator and AWS Shield Advanced
Date: 2020-12-08T19:03:36+00:00
Author: Chido Chemambo
Category: AWS Security
Tags: Advanced (300);AWS Global Accelerator;AWS Shield;Security, Identity, & Compliance;AWS Threat Research Team;DNS;Global Accelerator;Security Blog
Slug: how-to-protect-a-self-managed-dns-service-against-ddos-attacks-using-aws-global-accelerator-and-aws-shield-advanced

[Source](https://aws.amazon.com/blogs/security/how-to-protect-a-self-managed-dns-service-against-ddos-attacks-using-aws-global-accelerator-and-aws-shield-advanced/){:target="_blank" rel="noopener"}

> In this blog post, I show you how to improve the distributed denial of service (DDoS) resilience of your self-managed Domain Name System (DNS) service by using AWS Global Accelerator and AWS Shield Advanced. You can use those services to incorporate some of the techniques used by Amazon Route 53 to protect against DDoS attacks. [...]
