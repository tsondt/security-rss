Title: Yes, we’re all going to be at home for the foreseeable future. Does that leave you feeling insecure?
Date: 2020-12-01T10:50:09+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: yes-were-all-going-to-be-at-home-for-the-foreseeable-future-does-that-leave-you-feeling-insecure

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/01/is_it_time_to_get_sase/){:target="_blank" rel="noopener"}

> Or is it time to get SASE? Webcast After more than six months, the shine is wearing off working from home – not just for the workers, but for the security teams who need to rethink pretty much everything about how to protect them and their companies.... [...]
