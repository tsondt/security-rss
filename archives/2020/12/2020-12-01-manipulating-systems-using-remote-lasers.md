Title: Manipulating Systems Using Remote Lasers
Date: 2020-12-01T12:13:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Amazon;Apple;Facebook;Google;hacking;side-channel attacks
Slug: manipulating-systems-using-remote-lasers

[Source](https://www.schneier.com/blog/archives/2020/12/manipulating-systems-using-remote-lasers.html){:target="_blank" rel="noopener"}

> Many systems are vulnerable : Researchers at the time said that they were able to launch inaudible commands by shining lasers — from as far as 360 feet — at the microphones on various popular voice assistants, including Amazon Alexa, Apple Siri, Facebook Portal, and Google Assistant. [...] They broadened their research to show how light can be used to manipulate a wider range of digital assistants — including Amazon Echo 3 — but also sensing systems found in medical devices, autonomous vehicles, industrial systems and even space systems. The researchers also delved into how the ecosystem of devices connected [...]
