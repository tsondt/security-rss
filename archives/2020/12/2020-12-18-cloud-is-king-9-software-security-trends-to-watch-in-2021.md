Title: Cloud is King: 9 Software Security Trends to Watch in 2021
Date: 2020-12-18T21:26:47+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;IoT;Vulnerabilities;2021 security predictions;API;application security;Checkmarx;cloud-native security;Code as Infrastructure;Developers;IoT security;Patching;Software security
Slug: cloud-is-king-9-software-security-trends-to-watch-in-2021

[Source](https://threatpost.com/cloud-king-software-security-trends-2021/162442/){:target="_blank" rel="noopener"}

> Researchers predict software security will continue to struggle to keep up with cloud and IoT in the new year. [...]
