Title: Physical addresses of 270K Ledger owners leaked on hacker forum
Date: 2020-12-21T02:15:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: physical-addresses-of-270k-ledger-owners-leaked-on-hacker-forum

[Source](https://www.bleepingcomputer.com/news/security/physical-addresses-of-270k-ledger-owners-leaked-on-hacker-forum/){:target="_blank" rel="noopener"}

> A threat actor has leaked the stolen email and mailing addresses for Ledger cryptocurrency wallet users on a hacker forum for free. [...]
