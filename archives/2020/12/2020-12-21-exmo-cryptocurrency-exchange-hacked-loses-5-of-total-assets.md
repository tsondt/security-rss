Title: EXMO cryptocurrency exchange hacked, loses 5% of total assets
Date: 2020-12-21T14:01:11
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: exmo-cryptocurrency-exchange-hacked-loses-5-of-total-assets

[Source](https://www.bleepingcomputer.com/news/security/exmo-cryptocurrency-exchange-hacked-loses-5-percent-of-total-assets/){:target="_blank" rel="noopener"}

> British cryptocurrency exchange EXMO has disclosed that unknown attackers withdrew almost 5% of its total assets after compromising its hot wallets. [...]
