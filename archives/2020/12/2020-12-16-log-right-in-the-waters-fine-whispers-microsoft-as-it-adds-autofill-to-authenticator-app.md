Title: Log right in, the water's fine, whispers Microsoft as it adds autofill to Authenticator app
Date: 2020-12-16T18:45:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: log-right-in-the-waters-fine-whispers-microsoft-as-it-adds-autofill-to-authenticator-app

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/16/authenticator_autofill/){:target="_blank" rel="noopener"}

> Great, another password manager Microsoft has opened up the public preview of password autofill via its Authenticator app for iOS and Android.... [...]
