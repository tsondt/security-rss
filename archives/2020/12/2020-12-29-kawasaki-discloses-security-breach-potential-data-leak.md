Title: Kawasaki discloses security breach, potential data leak
Date: 2020-12-29T08:15:02
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: kawasaki-discloses-security-breach-potential-data-leak

[Source](https://www.bleepingcomputer.com/news/security/kawasaki-discloses-security-breach-potential-data-leak/){:target="_blank" rel="noopener"}

> Japan's Kawasaki Heavy Industries announced a security breach and potential data leak after unauthorized access to a Japanese company server from multiple overseas offices. [...]
