Title: Data breach broker selling user records stolen from 26 companies
Date: 2020-12-31T10:04:01
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: data-breach-broker-selling-user-records-stolen-from-26-companies

[Source](https://www.bleepingcomputer.com/news/security/data-breach-broker-selling-user-records-stolen-from-26-companies/){:target="_blank" rel="noopener"}

> A data breach broker is selling the allegedly stolen user records for twenty-six companies on a hacker forum, BleepingComputer has learned. [...]
