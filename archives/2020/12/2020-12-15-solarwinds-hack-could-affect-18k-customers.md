Title: SolarWinds Hack Could Affect 18K Customers
Date: 2020-12-15T17:41:37+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Alan Paller;Andrew Morris;Center for Strategic and International Studies;CISA;Cybersecurity and Infrastructure Security Agency;FireEye;GreyNoise Intelligence;James Lewis;microsoft;Orion software;SANS Institute;SolarWinds breach;U.S. Securities and Exchange Commission;Vinoth Kumar
Slug: solarwinds-hack-could-affect-18k-customers

[Source](https://krebsonsecurity.com/2020/12/solarwinds-hack-could-affect-18k-customers/){:target="_blank" rel="noopener"}

> The still-unfolding breach at network management software firm SolarWinds may have resulted in malicious code being pushed to nearly 18,000 customers, the company said in a legal filing on Monday. Meanwhile, Microsoft should soon have some idea which and how many SolarWinds customers were affected, as it recently took possession of a key domain name used by the intruders to control infected systems. On Dec. 13, SolarWinds acknowledged that hackers had inserted malware into a service that provided software updates for its Orion platform, a suite of products broadly used across the U.S. federal government and Fortune 500 firms to [...]
