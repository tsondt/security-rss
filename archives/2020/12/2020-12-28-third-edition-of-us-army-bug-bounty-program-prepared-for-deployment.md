Title: Third edition of US Army bug bounty program prepared for deployment
Date: 2020-12-28T14:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: third-edition-of-us-army-bug-bounty-program-prepared-for-deployment

[Source](https://portswigger.net/daily-swig/third-edition-of-us-army-bug-bounty-program-prepared-for-deployment){:target="_blank" rel="noopener"}

> Hack the Army 3.0 promises ‘more targets, bounties, and hackers’ [...]
