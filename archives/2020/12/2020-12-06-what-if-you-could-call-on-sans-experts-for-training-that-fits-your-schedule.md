Title: What if you could call on SANS experts for training that fits your schedule?
Date: 2020-12-06T23:00:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: what-if-you-could-call-on-sans-experts-for-training-that-fits-your-schedule

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/06/training_that_fits_your_schedule/){:target="_blank" rel="noopener"}

> Well now you can. Here’s how... Promo If you’re on the front line of cyber security, this has been a year like no other, with new threats emerging even as the great shift to home working has exposed long ignored weaknesses in existing infrastructure.... [...]
