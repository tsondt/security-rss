Title: Norway: Russian APT28 state hackers likely behind Parliament attack
Date: 2020-12-08T12:48:37
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: norway-russian-apt28-state-hackers-likely-behind-parliament-attack

[Source](https://www.bleepingcomputer.com/news/security/norway-russian-apt28-state-hackers-likely-behind-parliament-attack/){:target="_blank" rel="noopener"}

> Russian-backed hacking group APT28 has likely brute-forced multiple Norwegian Parliament (Stortinget) email accounts on August 24, 2020, according to the Norwegian Police Security Service (PST, short for Politiets Sikkerhetstjeneste). [...]
