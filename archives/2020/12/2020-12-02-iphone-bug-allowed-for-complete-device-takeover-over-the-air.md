Title: iPhone Bug Allowed for Complete Device Takeover Over the Air
Date: 2020-12-02T13:52:19+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;apple;google;Google Project Zero;ian beer;ios;iPadOS;iphone;ipod;memory corruption bug;radio;threat actors;Update;vulnerabilities;Wireless
Slug: iphone-bug-allowed-for-complete-device-takeover-over-the-air

[Source](https://threatpost.com/iphone-bug-takeover-over-the-air/161748/){:target="_blank" rel="noopener"}

> Researcher Ian Beer from Google Project Zero took six months to figure out the radio-proximity exploit of a memory corruption bug that was patched in May. [...]
