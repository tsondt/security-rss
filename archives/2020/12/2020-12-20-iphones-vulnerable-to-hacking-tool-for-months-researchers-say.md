Title: iPhones vulnerable to hacking tool for months, researchers say
Date: 2020-12-20T20:05:26+00:00
Author: Alex Hern
Category: The Guardian
Tags: Malware;Apple;iPhone;iOS;Hacking;Data and computer security;Smartphones;Technology;Privacy;World news
Slug: iphones-vulnerable-to-hacking-tool-for-months-researchers-say

[Source](https://www.theguardian.com/technology/2020/dec/20/iphones-vulnerable-to-hacking-tool-for-months-researchers-say){:target="_blank" rel="noopener"}

> Analysis: NSO Group’s Pegasus spyware could allegedly track locations and access passwords Dozens of Al Jazeera journalists allegedly hacked using Israeli firm’s spyware For almost a year, spyware sold by Israel’s NSO Group was allegedly armed with a computer security super-weapon: a zero-footprint, zero-click, zero-day exploit that used a vulnerability in iMessage to seize control of an iPhone at the push of a button. That means it would have left no visible trace of being placed on target’s phones, could be installed by simply sending a message that the victim didn’t even need to click on, and worked even on [...]
