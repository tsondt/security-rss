Title: US seizes domains used for COVID-19 vaccine phishing attacks
Date: 2020-12-21T12:28:02
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: us-seizes-domains-used-for-covid-19-vaccine-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-domains-used-for-covid-19-vaccine-phishing-attacks/){:target="_blank" rel="noopener"}

> The US Department of Justice has seized two domain names used to impersonate the official websites of biotechnology companies Moderna and Regeneron involved in the development of COVID-19 vaccines. [...]
