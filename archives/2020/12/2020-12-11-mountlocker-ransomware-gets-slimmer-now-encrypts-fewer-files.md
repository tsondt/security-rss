Title: MountLocker ransomware gets slimmer, now encrypts fewer files
Date: 2020-12-11T15:30:22
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: mountlocker-ransomware-gets-slimmer-now-encrypts-fewer-files

[Source](https://www.bleepingcomputer.com/news/security/mountlocker-ransomware-gets-slimmer-now-encrypts-fewer-files/){:target="_blank" rel="noopener"}

> MountLocker ransomware received an update recently that cut its size by half but preserves a weakness that could potentially allow learning the random key used to encrypt files. [...]
