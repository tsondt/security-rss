Title: U.S. warns of increased cyberattacks against K-12 distance learning
Date: 2020-12-10T18:22:01
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: us-warns-of-increased-cyberattacks-against-k-12-distance-learning

[Source](https://www.bleepingcomputer.com/news/security/us-warns-of-increased-cyberattacks-against-k-12-distance-learning/){:target="_blank" rel="noopener"}

> K-12 educational institutions in the U.S. are being targeted by malicious actors for extortion, data theft, and general disruption of normal activity. The trend will continue through the 2020/2021 academic year. [...]
