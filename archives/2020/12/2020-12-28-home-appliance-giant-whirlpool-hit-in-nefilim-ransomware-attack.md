Title: Home appliance giant Whirlpool hit in Nefilim ransomware attack
Date: 2020-12-28T12:12:53
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: home-appliance-giant-whirlpool-hit-in-nefilim-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/home-appliance-giant-whirlpool-hit-in-nefilim-ransomware-attack/){:target="_blank" rel="noopener"}

> Home appliances giant Whirlpool suffered a ransomware attack by the Nefilim ransomware gang who stole data before encrypting devices. [...]
