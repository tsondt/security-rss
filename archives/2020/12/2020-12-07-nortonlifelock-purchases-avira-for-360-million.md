Title: NortonLifeLock purchases Avira for $360 million
Date: 2020-12-07T11:19:43
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software;Technology
Slug: nortonlifelock-purchases-avira-for-360-million

[Source](https://www.bleepingcomputer.com/news/security/nortonlifelock-purchases-avira-for-360-million/){:target="_blank" rel="noopener"}

> NortonLifeLock announced today that they have agreed to acquire Avira in an all-cash transaction for approximately $360 million. [...]
