Title: Critical Bugs in Dell Wyse Thin Clients Allow Code Execution, Client Takeovers
Date: 2020-12-21T17:00:51+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Bugs;configuration files;critical security vulnerabilities;dell wyse;ftp server;information disclosure;no authentication;patch;read write access;thin clients;version 9.x
Slug: critical-bugs-in-dell-wyse-thin-clients-allow-code-execution-client-takeovers

[Source](https://threatpost.com/critical-bugs-dell-wyse-thin-clients/162452/){:target="_blank" rel="noopener"}

> The bugs rate 10 out of 10 on the vulnerability-severity scale, thanks to the ease of exploitation. [...]
