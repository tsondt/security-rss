Title: Swedish university fined $66,000 for GDPR violations
Date: 2020-12-16T14:01:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: swedish-university-fined-66000-for-gdpr-violations

[Source](https://portswigger.net/daily-swig/swedish-university-fined-66-000-for-gdpr-violations){:target="_blank" rel="noopener"}

> Umeå University research group held sensitive information on insecure cloud storage [...]
