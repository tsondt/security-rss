Title: Trump administration says Russia behind SolarWinds hack.Trump himself begs to differ
Date: 2020-12-20T23:52:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: trump-administration-says-russia-behind-solarwinds-hacktrump-himself-begs-to-differ

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/20/solarwinds_update_trump_contradicts_pompeo_russia_attribution/){:target="_blank" rel="noopener"}

> Microsoft’s analysis of hack details next-level 'DLL hell' after likely developer pipeline compromise United States secretary of state Mike Pompeo has laid the blame for the SolarWinds hack on Russia, but his boss begs to differ.... [...]
