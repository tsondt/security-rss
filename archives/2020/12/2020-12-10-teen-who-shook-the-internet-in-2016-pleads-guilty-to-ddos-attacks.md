Title: Teen who shook the Internet in 2016 pleads guilty to DDoS attacks
Date: 2020-12-10T09:24:28
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: teen-who-shook-the-internet-in-2016-pleads-guilty-to-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/teen-who-shook-the-internet-in-2016-pleads-guilty-to-ddos-attacks/){:target="_blank" rel="noopener"}

> One of the operators behind a Mirai botnet pleaded guilty to their involvement in a huge DDoS attack that caused a massive Internet disruption during October 2016. [...]
