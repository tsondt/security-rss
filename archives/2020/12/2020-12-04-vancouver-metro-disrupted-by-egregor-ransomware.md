Title: Vancouver Metro Disrupted by Egregor Ransomware
Date: 2020-12-04T14:25:55+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Hacks;IoT;Malware;Compass;cyberattack;egregor;Encryption;kiosks;Kmart;malware;ransom note;ransomware;ransomware gang;Translink;Vancouver metro
Slug: vancouver-metro-disrupted-by-egregor-ransomware

[Source](https://threatpost.com/vancouver-metro-egregor-ransomware/161892/){:target="_blank" rel="noopener"}

> The attack, which prevented Translink users from using their metro cards or buying tickets at kiosks, is the second from the prolific threat group just this week. [...]
