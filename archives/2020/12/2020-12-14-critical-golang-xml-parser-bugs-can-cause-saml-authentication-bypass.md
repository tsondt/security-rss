Title: Critical Golang XML parser bugs can cause SAML authentication bypass
Date: 2020-12-14T20:23:09
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: critical-golang-xml-parser-bugs-can-cause-saml-authentication-bypass

[Source](https://www.bleepingcomputer.com/news/security/critical-golang-xml-parser-bugs-can-cause-saml-authentication-bypass/){:target="_blank" rel="noopener"}

> This week, Mattermost, in coordination with Golang has disclosed 3 critical vulnerabilities within Go language's XML parser. If exploited, these vulnerabilities, also impacting multiple Go-based SAML implementations, can lead to a complete bypass of SAML authentication which powers prominent web applications today. [...]
