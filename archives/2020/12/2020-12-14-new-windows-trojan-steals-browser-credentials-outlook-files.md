Title: New Windows Trojan Steals Browser Credentials, Outlook Files
Date: 2020-12-14T16:34:11+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;AridViper;browser credentials;informations stealer;malware;Micropsia;Microsoft Windows;Middle East;Outlook;PyInstaller;PyMicropsia;Python;threat group;Trojan;Windows
Slug: new-windows-trojan-steals-browser-credentials-outlook-files

[Source](https://threatpost.com/windows-trojan-steals-browser-credentials-outlook-files/162223/){:target="_blank" rel="noopener"}

> The newly discovered Python-based malware family targets the Outlook processes, and browser credentials, of Microsoft Windows victims. [...]
