Title: FireEye reveals that it was hacked by a nation state APT group
Date: 2020-12-08T16:58:18
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fireeye-reveals-that-it-was-hacked-by-a-nation-state-apt-group

[Source](https://www.bleepingcomputer.com/news/security/fireeye-reveals-that-it-was-hacked-by-a-nation-state-apt-group/){:target="_blank" rel="noopener"}

> Leading cybersecurity company FireEye disclosed today that it was hacked by a threat actor showing all the signs of a state-sponsored hacking group. [...]
