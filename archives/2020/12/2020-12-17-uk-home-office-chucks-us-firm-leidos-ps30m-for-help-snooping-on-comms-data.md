Title: UK Home Office chucks US firm Leidos £30m for help snooping on comms data
Date: 2020-12-17T09:30:08+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: uk-home-office-chucks-us-firm-leidos-ps30m-for-help-snooping-on-comms-data

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/17/home_office_leidos_30m/){:target="_blank" rel="noopener"}

> Er, we mean fighting terrorism and organised crime! The UK's Home Office has handed a £30m contract to engineering and IT outfit Leidos to help government agencies access and analyse communications data for combatting terrorism and organised crime.... [...]
