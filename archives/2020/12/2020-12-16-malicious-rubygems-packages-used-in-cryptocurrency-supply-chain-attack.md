Title: Malicious RubyGems packages used in cryptocurrency supply chain attack
Date: 2020-12-16T11:00:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: malicious-rubygems-packages-used-in-cryptocurrency-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/malicious-rubygems-packages-used-in-cryptocurrency-supply-chain-attack/){:target="_blank" rel="noopener"}

> New malicious RubyGems packages have been discovered that are being used in a supply chain attack to steal cryptocurrency from unsuspecting users. [...]
