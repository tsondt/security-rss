Title: Critical bugs in Dell Wyse ThinOS allow thin client take over
Date: 2020-12-21T12:59:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: critical-bugs-in-dell-wyse-thinos-allow-thin-client-take-over

[Source](https://www.bleepingcomputer.com/news/security/critical-bugs-in-dell-wyse-thinos-allow-thin-client-take-over/){:target="_blank" rel="noopener"}

> Almost a dozen Dell Wyse thin client models are vulnerable to critical issues that could be exploited by a remote attacker to run malicious code and gain access to arbitrary files. [...]
