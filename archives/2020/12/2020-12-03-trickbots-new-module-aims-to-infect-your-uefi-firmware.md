Title: TrickBot's new module aims to infect your UEFI firmware
Date: 2020-12-03T06:17:33
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: trickbots-new-module-aims-to-infect-your-uefi-firmware

[Source](https://www.bleepingcomputer.com/news/security/trickbots-new-module-aims-to-infect-your-uefi-firmware/){:target="_blank" rel="noopener"}

> The developers of TrickBot have created a new module that probes for UEFI vulnerabilities, demonstrating the actor's effort to take attacks at a level that would give them ultimate control over infected machines. [...]
