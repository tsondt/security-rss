Title: NetGalley data breach: Publishing industry website forces password reset following ‘security incident’
Date: 2020-12-24T16:53:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: netgalley-data-breach-publishing-industry-website-forces-password-reset-following-security-incident

[Source](https://portswigger.net/daily-swig/netgalley-data-breach-publishing-industry-website-forces-password-reset-following-security-incident){:target="_blank" rel="noopener"}

> Attackers defaced homepage and accessed a database backup file containing passwords [...]
