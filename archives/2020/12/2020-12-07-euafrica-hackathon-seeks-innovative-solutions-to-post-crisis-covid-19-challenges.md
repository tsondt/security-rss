Title: EU:Africa hackathon seeks innovative solutions to ‘post-crisis’ Covid-19 challenges
Date: 2020-12-07T16:41:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: euafrica-hackathon-seeks-innovative-solutions-to-post-crisis-covid-19-challenges

[Source](https://portswigger.net/daily-swig/eu-africa-hackathon-seeks-innovative-solutions-to-post-crisis-covid-19-challenges){:target="_blank" rel="noopener"}

> A silver lining, organizer says, is that coronavirus restrictions have expanded the scalability of hackathons [...]
