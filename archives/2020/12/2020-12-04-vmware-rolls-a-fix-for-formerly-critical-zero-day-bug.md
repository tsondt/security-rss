Title: VMware Rolls a Fix for Formerly Critical Zero-Day Bug
Date: 2020-12-04T15:31:15+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;cisa alert;Command injection;critical;CVE-2020-4006;Cybersecurity;NSA;patch;privilege escalation;security advisory;security vulnerability;severity rating;vmware;workaround;zero day
Slug: vmware-rolls-a-fix-for-formerly-critical-zero-day-bug

[Source](https://threatpost.com/vmware-fix-critical-zero-day-bug/161896/){:target="_blank" rel="noopener"}

> VMware has issued a full patch and revised the severity level of the NSA-reported vulnerability to "important." [...]
