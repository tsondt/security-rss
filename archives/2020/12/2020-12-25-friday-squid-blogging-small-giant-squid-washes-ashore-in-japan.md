Title: Friday Squid Blogging: Small Giant Squid Washes Ashore in Japan
Date: 2020-12-25T22:19:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-small-giant-squid-washes-ashore-in-japan

[Source](https://www.schneier.com/blog/archives/2020/12/friday-squid-blogging-small-giant-squid-washes-ashore-in-japan.html){:target="_blank" rel="noopener"}

> A ten-foot giant squid has washed ashore on the Western coast of Japan. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
