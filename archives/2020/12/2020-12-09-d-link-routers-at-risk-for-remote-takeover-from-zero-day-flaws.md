Title: D-Link Routers at Risk for Remote Takeover from Zero-Day Flaws
Date: 2020-12-09T14:56:14+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;COVID-19;D-Link;Digital Defense;firmware;Home routers;internet;networking;Pandemic;remote command execution;remote workers;Routers;security flaws;vulnerabilities;Wireless;zero-day flaws
Slug: d-link-routers-at-risk-for-remote-takeover-from-zero-day-flaws

[Source](https://threatpost.com/d-link-routers-zero-day-flaws/162064/){:target="_blank" rel="noopener"}

> Critical vulnerabilities discovered by Digital Defense can allow attackers to gain root access and take over devices running same firmware. [...]
