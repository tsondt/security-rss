Title: Australia proposes Privacy Act 1988 reforms inspired by EU’s GDPR
Date: 2020-12-18T12:41:15+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: australia-proposes-privacy-act-1988-reforms-inspired-by-eus-gdpr

[Source](https://portswigger.net/daily-swig/australia-proposes-privacy-act-1988-reforms-inspired-by-eus-gdpr){:target="_blank" rel="noopener"}

> Proposal focuses on ways for ‘public trust’ to be rebuilt, but legal experts remain ambivalent [...]
