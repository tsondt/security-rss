Title: Authentication Failure
Date: 2020-12-14T12:31:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: authentication;impersonation
Slug: authentication-failure

[Source](https://www.schneier.com/blog/archives/2020/12/authentication-failure.html){:target="_blank" rel="noopener"}

> This is a weird story of a building owner commissioning an artist to paint a mural on the side of his building — except that he wasn’t actually the building’s owner. The fake landlord met Hawkins in person the day after Thanksgiving, supplying the paint and half the promised fee. They met again a couple of days later for lunch, when the job was mostly done. Hawkins showed him photographs. The patron seemed happy. He sent Hawkins the rest of the (sorry) dough. But when Hawkins invited him down to see the final result, his client didn’t answer the phone. [...]
