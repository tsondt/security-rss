Title: Dark Web Pricing Skyrockets for Microsoft RDP Servers, Payment-Card Data
Date: 2020-12-21T17:07:00+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Most Recent ThreatLists;compromised payment card;COVID-19;cybercrime;cybercriminals;Dark Web;DDoS;DDoS-for-hire;Microsoft;Microsoft RDP Server;Pandemic;pricing;RDP;underground marketplace
Slug: dark-web-pricing-skyrockets-for-microsoft-rdp-servers-payment-card-data

[Source](https://threatpost.com/rdp-server-access-payment-card-data-in-high-cybercrime-demand/162476/){:target="_blank" rel="noopener"}

> Underground marketplace pricing on RDP server access, compromised payment card data and DDoS-For-Hire services are surging. [...]
