Title: Bomb Threat, DDoS Purveyor Gets Eight Years
Date: 2020-12-01T14:01:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: DDoS-for-Hire;Ne'er-Do-Well News;Apophis Squad;HDGZero;Nicola Hanna;Otis D. Wright II;Protonmail;Timothy Dalton Vaughn;WantedByFeds;Xavier Farbel
Slug: bomb-threat-ddos-purveyor-gets-eight-years

[Source](https://krebsonsecurity.com/2020/12/bomb-threat-ddos-purveyor-gets-eight-years/){:target="_blank" rel="noopener"}

> A 22-year-old North Carolina man has been sentenced to nearly eight years in prison for conducting bomb threats against thousands of schools in the U.S. and United Kingdom, running a service that launched distributed denial-of-service (DDoS) attacks, and for possessing sexually explicit images of minors. Timothy Dalton Vaughn from Winston-Salem, N.C. was a key member of the Apophis Squad, a gang of young ne’er-do-wells who made bomb threats to more than 2,400 schools and launched DDoS attacks against countless Web sites — including KrebsOnSecurity on multiple occasions. The Justice Department says Vaughn and his gang ran a DDoS-for-hire service that [...]
