Title: F5 warns over ‘critical’ XSS flaw in BIG-IP
Date: 2020-12-16T16:14:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: f5-warns-over-critical-xss-flaw-in-big-ip

[Source](https://portswigger.net/daily-swig/f5-warns-over-critical-xss-flaw-in-big-ip){:target="_blank" rel="noopener"}

> Prompt triage is recommended [...]
