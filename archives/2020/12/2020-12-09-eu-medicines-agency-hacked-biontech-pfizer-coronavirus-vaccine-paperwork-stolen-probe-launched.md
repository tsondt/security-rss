Title: EU Medicines Agency hacked, BioNTech-Pfizer coronavirus vaccine paperwork stolen, probe launched
Date: 2020-12-09T20:09:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: eu-medicines-agency-hacked-biontech-pfizer-coronavirus-vaccine-paperwork-stolen-probe-launched

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/09/european_medicines_agency_cyberattack/){:target="_blank" rel="noopener"}

> Regulatory submissions for COVID-19 jab candidate 'unlawfully accessed' The EU Medicines Agency today revealed it was hacked, just a week after infosec eggheads said foreign state hackers have been targeting European institutions.... [...]
