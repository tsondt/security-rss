Title: UK firm NOW: Pensions tells some customers a 'service partner' leaked their data all over 'public software forum'
Date: 2020-12-22T13:33:11+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: uk-firm-now-pensions-tells-some-customers-a-service-partner-leaked-their-data-all-over-public-software-forum

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/22/data_breach_now_pensions/){:target="_blank" rel="noopener"}

> Compromised info include names, email addresses, DoBs, and National Insurance numbers Updated Workplace pension provider NOW: Pensions has emailed a number of UK customers to warn about a data leakage caused by contractor error.... [...]
