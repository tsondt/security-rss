Title: Google Cloud named a leader in latest Forrester Research IaaS Platform Native Security Wave
Date: 2020-12-08T17:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Google Cloud Platform;Inside Google Cloud;Identity & Security
Slug: google-cloud-named-a-leader-in-latest-forrester-research-iaas-platform-native-security-wave

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-a-leader-in-iaas-platform-native-security-wave/){:target="_blank" rel="noopener"}

> The adoption of cloud services has created a generational opportunity to meaningfully improve information security and reduce risk. As an organization moves applications and data to the cloud, they can take advantage of native security capabilities in their cloud platform. Done well, use of these engineered-in platform capabilities can simplify security to the extent that it becomes almost invisible to users, reducing operational complexity, favorably altering the balance of shared responsibility for customers, and decreasing the need for highly specialized security talent. At Google Cloud, we call the result Invisible Security, and it requires a foundation of innovative, powerful, best-in-class [...]
