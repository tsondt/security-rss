Title: Aida Cruises cancels trips due to mysterious "IT restrictions"
Date: 2020-12-28T16:18:52
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Technology
Slug: aida-cruises-cancels-trips-due-to-mysterious-it-restrictions

[Source](https://www.bleepingcomputer.com/news/security/aida-cruises-cancels-trips-due-to-mysterious-it-restrictions/){:target="_blank" rel="noopener"}

> German cruise line AIDA Cruises is dealing with mysterious "IT restrictions" that have led to the cancellation of New Year's Eve cruises embarking this past weekend. [...]
