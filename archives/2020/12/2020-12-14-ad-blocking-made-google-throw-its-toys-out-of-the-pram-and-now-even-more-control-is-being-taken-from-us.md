Title: Ad blocking made Google throw its toys out of the pram – and now even more control is being taken from us
Date: 2020-12-14T09:32:31+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: ad-blocking-made-google-throw-its-toys-out-of-the-pram-and-now-even-more-control-is-being-taken-from-us

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/14/google_manifest_plugin_opinion/){:target="_blank" rel="noopener"}

> We need regulations on web advertising Column Google makes its money from being the world's middle man for online advertising. It's kind of a tech company too, but in a good-enough sort of way rather than the "hey, we invented the transistor" sort of way. It doesn't do anything nobody else can do, except leverage its search dominance into advertising dominance.... [...]
