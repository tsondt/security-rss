Title: Mind the gap: CERT report reveals security holes across Polish education sector
Date: 2020-12-02T12:23:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: mind-the-gap-cert-report-reveals-security-holes-across-polish-education-sector

[Source](https://portswigger.net/daily-swig/mind-the-gap-cert-report-reveals-security-holes-across-polish-education-sector){:target="_blank" rel="noopener"}

> Concerns raised about the safety of student and staff data [...]
