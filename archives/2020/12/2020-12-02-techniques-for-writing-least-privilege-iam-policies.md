Title: Techniques for writing least privilege IAM policies
Date: 2020-12-02T18:01:31+00:00
Author: Ben Potter
Category: AWS Security
Tags: Advanced (300);AWS Identity and Access Management (IAM);AWS Well-Architected;Security, Identity, & Compliance;AWS IAM;Best Practices;IAM policies;least privilege;Security Blog;Well-Architected
Slug: techniques-for-writing-least-privilege-iam-policies

[Source](https://aws.amazon.com/blogs/security/techniques-for-writing-least-privilege-iam-policies/){:target="_blank" rel="noopener"}

> In this post, I’m going to share two techniques I’ve used to write least privilege AWS Identity and Access Management (IAM) policies. If you’re not familiar with IAM policy structure, I highly recommend you read understanding how IAM works and policies and permissions. Least privilege is a principle of granting only the permissions required to [...]
