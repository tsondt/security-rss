Title: Hey Alexa, what’s my PIN? Researchers show voice assistants can hear the taps made on a smartphone keyboard
Date: 2020-12-03T17:16:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: hey-alexa-whats-my-pin-researchers-show-voice-assistants-can-hear-the-taps-made-on-a-smartphone-keyboard

[Source](https://portswigger.net/daily-swig/hey-alexa-whats-my-pin-researchers-show-voice-assistants-can-hear-the-taps-made-on-a-smartphone-keyboard){:target="_blank" rel="noopener"}

> Phone tapping of a different kind [...]
