Title: Backdoored SolarWinds software, linked to US govt hacks, in wide use throughout the British public sector
Date: 2020-12-14T18:40:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: backdoored-solarwinds-software-linked-to-us-govt-hacks-in-wide-use-throughout-the-british-public-sector

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/14/solarwinds_public_sector/){:target="_blank" rel="noopener"}

> And what's the impact of months-long compromise? UK.gov won't say – as CISA orders shutdown of machines Concern is gathering over the effects of the backdoor inserted into SolarWinds' network monitoring software on Britain's public sector – as tight-lipped government departments refuse to say whether UK institutions were accessed by Russian spies.... [...]
