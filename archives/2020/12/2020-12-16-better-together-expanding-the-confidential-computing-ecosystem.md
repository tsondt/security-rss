Title: Better together: Expanding the Confidential Computing ecosystem
Date: 2020-12-16T17:00:00+00:00
Author: Sam Lugani
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: better-together-expanding-the-confidential-computing-ecosystem

[Source](https://cloud.google.com/blog/products/identity-security/partners-share-thoughts-on-confidential-computing/){:target="_blank" rel="noopener"}

> Core to our goal of delivering security innovation is the ability to offer powerful features as part of our cloud infrastructure that are easy for customers to implement and use. Confidential computing can provide a flexible, isolated, hardware-based trusted execution environment, allowing adopters to protect their data and sensitive code against malicious access and memory snooping while data is in use. Today, we are happy to announce that we have completed the rollout of Confidential VMs to general availability in nine regions. Our partners have played a huge part in this journey. They have been critical in establishing an ecosystem [...]
