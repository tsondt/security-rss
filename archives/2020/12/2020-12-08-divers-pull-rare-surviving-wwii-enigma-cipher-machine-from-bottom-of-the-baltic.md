Title: Divers Pull Rare Surviving WWII Enigma Cipher Machine from Bottom of the Baltic
Date: 2020-12-08T20:35:11+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cryptography;Alan Turing;auction;baltic sea;bletchly park;cryptography;Enigma;Enigma Cipher machine;Enigma crypto;Germany;ghost net;world war II;world wildlife fund
Slug: divers-pull-rare-surviving-wwii-enigma-cipher-machine-from-bottom-of-the-baltic

[Source](https://threatpost.com/divers-wwii-enigma-cipher-baltic/162045/){:target="_blank" rel="noopener"}

> This sealogged Nazi machine will undergo restoration. [...]
