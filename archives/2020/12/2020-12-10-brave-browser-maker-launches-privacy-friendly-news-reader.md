Title: Brave browser-maker launches privacy-friendly news reader
Date: 2020-12-10T17:00:11+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;brave;browsers;privacy
Slug: brave-browser-maker-launches-privacy-friendly-news-reader

[Source](https://arstechnica.com/?p=1728878){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Brave Software, maker of the Brave Web browser, is introducing a news reader that’s designed to protect user privacy by preventing parties—both internal and third party—from tracking the sites, articles, and story topics people view. Brave Today, as the service is called, is using technology that the company says sets it apart from news services offered by Google and Facebook. It’s designed to deliver personalized news feeds in a way that leaves no trail for Brave, ISPs, and third parties to track. The new service is part of Brave’s strategy of differentiating its browser as more [...]
