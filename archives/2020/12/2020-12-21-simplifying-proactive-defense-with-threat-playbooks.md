Title: Simplifying Proactive Defense With Threat Playbooks
Date: 2020-12-21T17:10:35+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Podcasts;attribution;blue team;IOCs;red team;security defense;security playbook;ttps
Slug: simplifying-proactive-defense-with-threat-playbooks

[Source](https://threatpost.com/simplifying-proactive-defense-with-threat-playbooks/162468/){:target="_blank" rel="noopener"}

> FortiGuard Labs’ Derek Manky talks about how threat playbooks can equip defense teams with the tools they need to fight back against evolving attacker TTPs. [...]
