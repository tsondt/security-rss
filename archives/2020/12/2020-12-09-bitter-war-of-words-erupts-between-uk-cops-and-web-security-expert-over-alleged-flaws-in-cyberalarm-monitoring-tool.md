Title: Bitter war of words erupts between UK cops and web security expert over alleged flaws in Cyberalarm monitoring tool
Date: 2020-12-09T09:30:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: bitter-war-of-words-erupts-between-uk-cops-and-web-security-expert-over-alleged-flaws-in-cyberalarm-monitoring-tool

[Source](https://go.theregister.com/feed/www.theregister.com/2020/12/09/cyberalarm_pervade_software_npcc_kerfuffle/){:target="_blank" rel="noopener"}

> Pervade Software's product isn't perfect but neither is the police response to security concerns A war of words has erupted between the National Police Chiefs' Council (NPCC) and a British web security pro after a senior cop declared it would be "a waste of public money" to keep discussing security flaws in the body's Cyberalarm product.... [...]
