Title: ‘Air-Fi’ attack renders air-gapped computers open to data exfiltration through WiFi signals
Date: 2020-12-23T12:28:29+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: air-fi-attack-renders-air-gapped-computers-open-to-data-exfiltration-through-wifi-signals

[Source](https://portswigger.net/daily-swig/air-fi-attack-renders-air-gapped-computers-open-to-data-exfiltration-through-wifi-signals){:target="_blank" rel="noopener"}

> Where’s a Faraday cage when you need one? [...]
