Title: FBI and Homeland Security warn of APT attacks on US think tanks
Date: 2020-12-02T08:30:21
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-and-homeland-security-warn-of-apt-attacks-on-us-think-tanks

[Source](https://www.bleepingcomputer.com/news/security/fbi-and-homeland-security-warn-of-apt-attacks-on-us-think-tanks/){:target="_blank" rel="noopener"}

> The FBI and DHS-CISA warned of state-sponsored hacking groups targeting U.S. think tank organizations in a joint alert published on Tuesday evening. [...]
