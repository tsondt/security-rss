Title: Pfizer COVID-19 Vaccine Targeted in EU Cyberattack
Date: 2020-12-10T20:41:57+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Government;Hacks;BioNTech;COVID-19 vaccine;cyberattack;EMA;European Medicines Agency;Pfizer vaccine;Pfizer vaccine breach;vaccine documentation
Slug: pfizer-covid-19-vaccine-targeted-in-eu-cyberattack

[Source](https://threatpost.com/pfizer-covid-19-vaccine-cyberattack/162170/){:target="_blank" rel="noopener"}

> Threat actors accessed Pfizer vaccine documentation submitted to EU regulators in the latest cyberattack trying to profit off pandemic suffering. [...]
