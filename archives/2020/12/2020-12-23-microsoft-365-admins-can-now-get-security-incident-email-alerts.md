Title: Microsoft 365 admins can now get security incident email alerts
Date: 2020-12-23T14:34:40
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-365-admins-can-now-get-security-incident-email-alerts

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-admins-can-now-get-security-incident-email-alerts/){:target="_blank" rel="noopener"}

> Microsoft has added support for security incident email notifications to the Microsoft 365 Defender enterprise threat protection solution. [...]
