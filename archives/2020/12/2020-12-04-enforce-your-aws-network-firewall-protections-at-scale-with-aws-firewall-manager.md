Title: Enforce your AWS Network Firewall protections at scale with AWS Firewall Manager
Date: 2020-12-04T17:08:39+00:00
Author: Michael Wasielewski
Category: AWS Security
Tags: AWS Firewall Manager;Security, Identity, & Compliance;Network security;Security Blog
Slug: enforce-your-aws-network-firewall-protections-at-scale-with-aws-firewall-manager

[Source](https://aws.amazon.com/blogs/security/enforce-your-aws-network-firewall-protections-at-scale-with-aws-firewall-manager/){:target="_blank" rel="noopener"}

> As you look to manage network security on Amazon Web Services (AWS), there are multiple tools you can use to protect your resources and keep your data safe. Amazon Virtual Private Cloud (Amazon VPC), security groups (SGs), network access control lists (network ACLs), AWS WAF, and the recently launched AWS Network Firewall all offer points [...]
