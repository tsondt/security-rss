Title: Hackers rummaged about in Finnish psychotherapy clinic – now patients extorted with public data dump threats
Date: 2020-10-26T16:50:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: hackers-rummaged-about-in-finnish-psychotherapy-clinic-now-patients-extorted-with-public-data-dump-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/26/finland_psychotherapy_clinic_ransom_attack/){:target="_blank" rel="noopener"}

> Details on 300 reportedly already published to Tor website A Finnish psychotherapy centre was hit by hackers who stole therapy session notes – before threatening patients of the clinic with ransom demands amid selective dark web leaks of stolen material.... [...]
