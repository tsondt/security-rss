Title: Barnes & Noble Hack: A Reading List for Phishers and Crooks
Date: 2020-10-15T13:55:46+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Web Security;barnes & noble;cyberattack;data breach;email notice;Fraud;hack;nook outage;personal;personal information;Phishing;PII;purchase history;ransomware;reading lists;transaction history
Slug: barnes-noble-hack-a-reading-list-for-phishers-and-crooks

[Source](https://threatpost.com/barnes-noble-hack-phishers-crooks/160148/){:target="_blank" rel="noopener"}

> Customers' lists of book purchases along with email addresses and more could have been exposed during a (ransomware?) attack -- and that's a problem. [...]
