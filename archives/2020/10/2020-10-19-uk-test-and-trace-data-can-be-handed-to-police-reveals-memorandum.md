Title: UK test and trace data can be handed to police, reveals memorandum
Date: 2020-10-19T15:29:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: uk-test-and-trace-data-can-be-handed-to-police-reveals-memorandum

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/19/uk_test_and_trace_data/){:target="_blank" rel="noopener"}

> Oh great. 'Police involvement' could deter testing, says doctors' union As if things were not going badly enough for the UK's COVID-19 test and trace service, it now seems police will be able to access some test data, prompting fear that the disclosure could deter people who should have tests from coming forward.... [...]
