Title: Hey, pull your nose out of BlackBerry's poor financials and pay attention to this all-singing security doodah
Date: 2020-10-08T18:57:14+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: hey-pull-your-nose-out-of-blackberrys-poor-financials-and-pay-attention-to-this-all-singing-security-doodah

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/08/blackberry_security_summit/){:target="_blank" rel="noopener"}

> Zero trust, zero touch, crypto-jacking – bung all the usual buzzwords in "BlackBerry has always been known for our strong strategy," chief exec John Chen told the BlackBerry Security Summit earlier this week – just as a well-read investment blog concluded that "without a meaningful shift, this company (and stock) will probably keep on struggling".... [...]
