Title: Carnival Corp. Ransomware Attack Affects Three Cruise Lines
Date: 2020-10-15T12:08:46+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Malware
Slug: carnival-corp-ransomware-attack-affects-three-cruise-lines

[Source](https://threatpost.com/carnival-corp-ransomware-attack-cruise/160134/){:target="_blank" rel="noopener"}

> Hackers accessed personal information of guests, employees and crew for Carnival Cruise, Holland America and Seabourn as well as casino operations. [...]
