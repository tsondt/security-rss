Title: Breach at Dickey’s BBQ Smokes 3M Cards
Date: 2020-10-15T20:44:44+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Latest Warnings;Dickey's Barbeque breach;Eli Dominitz;Gemini Advisory;Q6Cyber
Slug: breach-at-dickeys-bbq-smokes-3m-cards

[Source](https://krebsonsecurity.com/2020/10/breach-at-dickeys-bbq-smokes-3m-cards/){:target="_blank" rel="noopener"}

> One of the digital underground’s most popular stores for peddling stolen credit card information began selling a batch of more than three million new card records this week. KrebsOnSecurity has learned the data was stolen in a lengthy data breach at more than 100 Dickey’s Barbeque Restaurant locations around the country. An ad on the popular carding site Joker’s Stash for “BlazingSun,” which fraud experts have traced back to a card breach at Dickey’s BBQ. On Monday, the carding bazaar Joker’s Stash debuted “ BlazingSun,” a new batch of more than three million stolen card records, advertising “valid rates” of [...]
