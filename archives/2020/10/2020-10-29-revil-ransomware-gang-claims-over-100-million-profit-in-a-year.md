Title: REvil ransomware gang claims over $100 million profit in a year
Date: 2020-10-29T02:02:02
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: revil-ransomware-gang-claims-over-100-million-profit-in-a-year

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-gang-claims-over-100-million-profit-in-a-year/){:target="_blank" rel="noopener"}

> REvil ransomware developers say that they made more than $100 million in one year of extorting large businesses across the world from various sectors. [...]
