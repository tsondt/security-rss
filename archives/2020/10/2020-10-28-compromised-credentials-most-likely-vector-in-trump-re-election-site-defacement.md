Title: ‘Compromised credentials’ most likely vector in Trump re-election site defacement
Date: 2020-10-28T14:50:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: compromised-credentials-most-likely-vector-in-trump-re-election-site-defacement

[Source](https://portswigger.net/daily-swig/compromised-credentials-most-likely-vector-in-trump-re-election-site-defacement){:target="_blank" rel="noopener"}

> Make Websites Safe Again [...]
