Title: RAINBOWMIX Apps in Google Play Serve Up Millions of Ad Fraud Victims
Date: 2020-10-08T19:46:04+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security;Web Security;8-16 bit color palate;ad fraud;Android;emulator;google play;Malicious ads;malware;Nintendo;OOC ads;out of context ads;RAINBOWMIX;retro games;White Ops
Slug: rainbowmix-apps-in-google-play-serve-up-millions-of-ad-fraud-victims

[Source](https://threatpost.com/rainbowmix-apps-google-play-ad-fraud/159982/){:target="_blank" rel="noopener"}

> Collectively, 240 fraudulent Android apps -- masquerading as retro game emulators -- account for 14 million installs. [...]
