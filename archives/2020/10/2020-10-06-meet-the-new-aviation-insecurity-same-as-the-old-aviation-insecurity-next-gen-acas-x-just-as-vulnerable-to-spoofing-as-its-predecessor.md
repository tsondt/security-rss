Title: Meet the new aviation insecurity, same as the old aviation insecurity: Next-gen ACAS X just as vulnerable to spoofing as its predecessor
Date: 2020-10-06T10:46:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: meet-the-new-aviation-insecurity-same-as-the-old-aviation-insecurity-next-gen-acas-x-just-as-vulnerable-to-spoofing-as-its-predecessor

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/06/acasx_spoofing_vulnerability/){:target="_blank" rel="noopener"}

> Faking an emergency collision alarm - just what you don't need over Heathrow Aviation boffins have found that next-gen collision aircraft avoidance systems appear to be just as vulnerable to signal spoofing attacks as older kit.... [...]
