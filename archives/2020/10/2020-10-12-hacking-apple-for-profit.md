Title: Hacking Apple for Profit
Date: 2020-10-12T10:58:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Apple;hacking;incentives;vulnerabilities
Slug: hacking-apple-for-profit

[Source](https://www.schneier.com/blog/archives/2020/10/hacking-apple-for-profit.html){:target="_blank" rel="noopener"}

> Five researchers hacked Apple Computer’s networks — not their products — and found fifty-five vulnerabilities. So far, they have received $289K. One of the worst of all the bugs they found would have allowed criminals to create a worm that would automatically steal all the photos, videos, and documents from someone’s iCloud account and then do the same to the victim’s contacts. Lots of details in this blog post by one of the hackers. [...]
