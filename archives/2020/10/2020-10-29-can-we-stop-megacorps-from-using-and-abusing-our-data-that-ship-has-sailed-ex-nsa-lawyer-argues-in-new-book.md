Title: Can we stop megacorps from using and abusing our data? That ship has sailed, ex-NSA lawyer argues in new book
Date: 2020-10-29T09:30:10+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: can-we-stop-megacorps-from-using-and-abusing-our-data-that-ship-has-sailed-ex-nsa-lawyer-argues-in-new-book

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/29/cyber_privacy_april_falcon_doss_interview/){:target="_blank" rel="noopener"}

> Companies are a bigger threat than governments – because they're less regulated Interview Cyber Privacy: Who Has Your Data and Why You Should Care is the title of a new book from April Falcon Doss, formerly associate general counsel for intelligence law at the US National Security Agency. Doss spoke to The Register about her concerns with pervasive data collection and its potential for harm.... [...]
