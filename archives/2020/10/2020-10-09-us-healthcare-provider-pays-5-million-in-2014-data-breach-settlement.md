Title: US healthcare provider pays $5 million in 2014 data breach settlement
Date: 2020-10-09T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-healthcare-provider-pays-5-million-in-2014-data-breach-settlement

[Source](https://portswigger.net/daily-swig/us-healthcare-provider-pays-5-million-in-2014-data-breach-settlement){:target="_blank" rel="noopener"}

> More than six million patient records were exposed [...]
