Title: Holiday Shopping Craze, COVID-19 Spur Retail Security Storm
Date: 2020-10-27T13:00:52+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Podcasts;Vulnerabilities;Web Security;Amazon Prime Day;black friday;COVID-19;cyber monday;holiday shopping;magecart;online shopping;Pandemic;podcast;retail;Retail Security;retail software;Target;Veracode
Slug: holiday-shopping-craze-covid-19-spur-retail-security-storm

[Source](https://threatpost.com/holiday-shopping-covid-19-retail-security/160550/){:target="_blank" rel="noopener"}

> Veracode's Chris Eng discusses the cyber threats facing shoppers who are going online due to the pandemic and the imminent holiday season. [...]
