Title: FIN11 hackers jump into the ransomware money-making scheme
Date: 2020-10-14T11:57:06-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: fin11-hackers-jump-into-the-ransomware-money-making-scheme

[Source](https://www.bleepingcomputer.com/news/security/fin11-hackers-jump-into-the-ransomware-money-making-scheme/){:target="_blank" rel="noopener"}

> FIN11, a financially-motivated hacker group with a history starting since at least 2016, has adapted malicious email campaigns to transition to ransomware as the main monetization method. [...]
