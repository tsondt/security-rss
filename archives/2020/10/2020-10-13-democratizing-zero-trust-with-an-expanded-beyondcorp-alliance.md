Title: Democratizing Zero Trust with an expanded BeyondCorp Alliance
Date: 2020-10-13T16:00:00+00:00
Author: Sampath Srinivas
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: democratizing-zero-trust-with-an-expanded-beyondcorp-alliance

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-announces-new-partners-in-its-beyondcorp-alliance/){:target="_blank" rel="noopener"}

> The need to quickly provide secure access for a newly remote workforce during the early days of COVID-19 drove many organizations to explore new technologies and start down a path towards a Zero Trust model. As time has passed, it’s become clear that remote work will be a defining characteristic of the new normal, and modernizing security by fully embracing zero trust models is an imperative, not an option. We need to work to further democratize this technology, accelerate and ease its adoption to help organizations stay secure, agile, and productive. We’ve been working on Zero Trust for more than [...]
