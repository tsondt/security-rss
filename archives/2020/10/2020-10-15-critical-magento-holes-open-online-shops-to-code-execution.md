Title: Critical Magento Holes Open Online Shops to Code Execution
Date: 2020-10-15T20:59:30+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;adobe;code execution;critical flaw;CVE-2020-24400;CVE-2020-24407;eCommerce;File Upload Allow List Bypass;fix;magecart;Magento;patch;pre-authorization;sql injection
Slug: critical-magento-holes-open-online-shops-to-code-execution

[Source](https://threatpost.com/critical-magento-holes-online-shops-code-execution/160181/){:target="_blank" rel="noopener"}

> Adobe says the two critical flaws (CVE-2020-24407 and CVE-2020-24400) could allow arbitrary code execution as well as read or write access to the database. [...]
