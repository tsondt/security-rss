Title: Cybercriminals Steal Nearly 1TB of Data from Miami-Based International Tech Firm
Date: 2020-10-14T13:26:27+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Breach;Hacks;Malware;1 terabyte;Credential Theft;credit card;cyber attack;Cyber security;CyberNews;data breach;data leak;database;hacker forum;Hackers;Intcomex;miami;PII;ransomware;russia;Russian hackers;stolen data
Slug: cybercriminals-steal-nearly-1tb-of-data-from-miami-based-international-tech-firm

[Source](https://threatpost.com/cybercriminals-steal-data-miami-tech-firm/160073/){:target="_blank" rel="noopener"}

> Databases of sensitive, financial and personally identifiable info and documents from Intcomex were leaked on Russian-language hacker forum after a ransomware attack. [...]
