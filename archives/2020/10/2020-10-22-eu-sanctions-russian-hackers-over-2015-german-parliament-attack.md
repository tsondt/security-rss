Title: EU sanctions Russian hackers over 2015 German parliament attack
Date: 2020-10-22T13:26:31
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: eu-sanctions-russian-hackers-over-2015-german-parliament-attack

[Source](https://www.bleepingcomputer.com/news/security/eu-sanctions-russian-hackers-over-2015-german-parliament-attack/){:target="_blank" rel="noopener"}

> The Council of the European Union today announced sanctions imposed on Russian military intelligence officers part of the 85th Main Centre for Special Services (GTsSS) for their involvement in a 2015 hack of the German Federal Parliament (Deutscher Bundestag). [...]
