Title: Global Privacy Control emerges as latest attempt to let netizens choose whether they want to be tracked online
Date: 2020-10-10T07:16:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: global-privacy-control-emerges-as-latest-attempt-to-let-netizens-choose-whether-they-want-to-be-tracked-online

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/10/global_privacy_control/){:target="_blank" rel="noopener"}

> It's Do Not Track II: The Wrath of Ashkan and Sebastian... Caaaaaaaan't you stop stalking us around the internet A coalition of technology companies, publishers, academics and advocacy groups this week proposed a web specification to allow internet users to declare whether they agree to have their personal data shared or sold.... [...]
