Title: Brit startup would like to beam 5G connectivity down at you from hydrogen-fuelled drones
Date: 2020-10-27T10:10:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: brit-startup-would-like-to-beam-5g-connectivity-down-at-you-from-hydrogen-fuelled-drones

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/27/stratospheric_platforms_5g_hydrogen_drone/){:target="_blank" rel="noopener"}

> And Deutsche Telekom's happy with this airborne base station plan A British startup is hoping to strap 5G antennas to liquid-hydrogen-powered high-altitude pseudo-satellites in the hope of replacing mobile base stations on the ground.... [...]
