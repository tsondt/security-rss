Title: Western governments double down efforts to curtail end-to-end encryption
Date: 2020-10-12T16:00:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: western-governments-double-down-efforts-to-curtail-end-to-end-encryption

[Source](https://portswigger.net/daily-swig/western-governments-double-down-efforts-to-curtail-end-to-end-encryption){:target="_blank" rel="noopener"}

> Security community resists anti-encryption push as counter-productive [...]
