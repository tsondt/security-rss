Title: US govt wins right to snaffle Edward Snowden's $5m+ book royalties, speech fees – and all future related earnings
Date: 2020-10-02T03:56:08+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: us-govt-wins-right-to-snaffle-edward-snowdens-5m-book-royalties-speech-fees-and-all-future-related-earnings

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/02/us_govt_snowden_court_royalties_win/){:target="_blank" rel="noopener"}

> Big blow to big whistleblower The US government's Department of Justice has won its multi-million-dollar claim to Edward Snowden's Permanent Record book royalties as well as any future related earnings.... [...]
