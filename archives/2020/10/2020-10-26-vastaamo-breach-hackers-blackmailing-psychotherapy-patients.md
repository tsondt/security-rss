Title: Vastaamo Breach: Hackers Blackmailing Psychotherapy Patients
Date: 2020-10-26T15:21:18+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Privacy;Vulnerabilities;breach;data breach;Data Privacy;psychotherapy;ransom_man;therapy privacy;Tor;Vastaamo
Slug: vastaamo-breach-hackers-blackmailing-psychotherapy-patients

[Source](https://threatpost.com/vastaamo-hackers-blackmailing-therapy-patients/160536/){:target="_blank" rel="noopener"}

> Cybercriminals have already reportedly posted the details of 300 Vastaamo patients - and are threatening to release the data of others unless a ransom is paid. [...]
