Title: Google Mending Another Crack in Widevine
Date: 2020-10-26T23:54:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;David Buchanan;digital rights management;DRM;Google Widevine;L3;Tomer Hadad
Slug: google-mending-another-crack-in-widevine

[Source](https://krebsonsecurity.com/2020/10/google-mending-another-crack-in-widevine/){:target="_blank" rel="noopener"}

> For the second time in as many years, Google is working to fix a weakness in its Widevine digital rights management (DRM) technology used by online streaming sites like Disney, Hulu and Netflix to prevent their content from being pirated. The latest cracks in Widevine concern the encryption technology’s protection for L3 streams, which is used for low-quality video and audio streams only. Google says the weakness does not affect L1 and L2 streams, which encompass more high-definition video and audio content. “As code protection is always evolving to address new threats, we are currently working to update our Widevine [...]
