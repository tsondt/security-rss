Title: VMware patches, among other things, ESXi flaw that can be abused by miscreants on the network to hijack hosts
Date: 2020-10-20T20:14:35+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: vmware-patches-among-other-things-esxi-flaw-that-can-be-abused-by-miscreants-on-the-network-to-hijack-hosts

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/20/vmware_adobe_critical_cve_patches/){:target="_blank" rel="noopener"}

> Adobe issues out-of-band patches, too, for Photoshop, Illustrator, InDesign, After Effects, etc Sysadmins responsible for VMware deployments should test and apply the latest security updates for the software.... [...]
