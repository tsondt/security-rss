Title: Researchers: LinkedIn, Instagram Vulnerable to Preview-Link RCE Security Woes
Date: 2020-10-27T16:01:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Privacy;Vulnerabilities;Web Security;a line;bakry;Chat;End to end encryption;Facebook;Instagram;ip address leak;link previews;LinkedIn;location data;Mysk;personalized ads;preview link function;remote code execution;Security Bugs;Slack;third party information sharing;twitter
Slug: researchers-linkedin-instagram-vulnerable-to-preview-link-rce-security-woes

[Source](https://threatpost.com/linkedin-instagram-preview-link-rce-security/160600/){:target="_blank" rel="noopener"}

> Popular chat apps, including LINE, Slack, Twitter DMs and others, can also leak location data and share private info with third-party servers. [...]
