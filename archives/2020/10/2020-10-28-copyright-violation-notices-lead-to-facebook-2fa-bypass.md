Title: ‘Copyright Violation’ Notices Lead to Facebook 2FA Bypass
Date: 2020-10-28T20:13:05+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Facebook;Web Security;2FA;2FA breach;2fa bypass;2FA code;copyright violation;copyright violation email;Credentials;Phishing;phishing email;scam;stolen data;Two Factor Authentication
Slug: copyright-violation-notices-lead-to-facebook-2fa-bypass

[Source](https://threatpost.com/copyright-violation-facebook-2fa-bypass/160690/){:target="_blank" rel="noopener"}

> Fraudulent Facebook messages allege copyright infringement and threaten to take down pages, unless users enter logins, passwords and 2FA codes. [...]
