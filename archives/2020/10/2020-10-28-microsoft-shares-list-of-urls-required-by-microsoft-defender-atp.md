Title: Microsoft shares list of URLs required by Microsoft Defender ATP
Date: 2020-10-28T13:59:03
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-shares-list-of-urls-required-by-microsoft-defender-atp

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-list-of-urls-required-by-microsoft-defender-atp/){:target="_blank" rel="noopener"}

> Microsoft has released a spreadsheet containing the full list of URLs that Microsoft Defender ATP must reach to function correctly. [...]
