Title: Google, Intel Warn on ‘Zero-Click’ Kernel Bug in Linux-Based IoT Devices
Date: 2020-10-14T13:37:13+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: IoT;Vulnerabilities;bleedingtooth;Bluetooth;BlueZ;Bug;CVE-2020-12351;CVE-2020-12352;CVE-2020-24490;github;google;Intel;Kernel;security vulnerability;zero-click
Slug: google-intel-warn-on-zero-click-kernel-bug-in-linux-based-iot-devices

[Source](https://threatpost.com/google-intel-kernel-bug-linux-iot/160067/){:target="_blank" rel="noopener"}

> Intel and Google are urging users to update the Linux kernel to version 5.9 or later. [...]
