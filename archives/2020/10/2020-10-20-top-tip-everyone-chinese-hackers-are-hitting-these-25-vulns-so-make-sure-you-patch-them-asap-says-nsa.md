Title: Top tip, everyone: Chinese hackers are hitting these 25 vulns, so make sure you patch them ASAP, says NSA
Date: 2020-10-20T23:40:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: top-tip-everyone-chinese-hackers-are-hitting-these-25-vulns-so-make-sure-you-patch-them-asap-says-nsa

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/20/nsa_china_hacking/){:target="_blank" rel="noopener"}

> Plus this Chrome one being exploited in the wild, we note The NSA has blown the lid off 25 computer security vulnerabilities Chinese government hackers are using to break into networks, steal data, and so on. The US super-spies said they went public with their list to help IT staff prioritize bug fixing. That is to say: if you're unsure of which patches to apply, do these first.... [...]
