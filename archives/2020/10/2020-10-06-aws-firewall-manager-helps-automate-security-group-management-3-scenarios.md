Title: AWS Firewall Manager helps automate security group management: 3 scenarios
Date: 2020-10-06T20:10:40+00:00
Author: Sonakshi Pandey
Category: AWS Security
Tags: Advanced (300);AWS Firewall Manager;Security, Identity, & Compliance;Centralized management;Compliance;Security Blog;Security groups;VPC security groups
Slug: aws-firewall-manager-helps-automate-security-group-management-3-scenarios

[Source](https://aws.amazon.com/blogs/security/aws-firewall-manager-helps-automate-security-group-management-3-scenarios/){:target="_blank" rel="noopener"}

> In this post, we walk you through scenarios that use AWS Firewall Manager to centrally manage security groups across your AWS Organizations implementation. Firewall Manager is a security management tool that helps you centralize, configure, and maintain AWS WAF rules, AWS Shield Advanced protections, and Amazon Virtual Private Cloud (Amazon VPC) security groups across AWS [...]
