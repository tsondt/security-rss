Title: Is it Iran or Russia's hackers we need to worry about? The Russians, definitely the Russians, says US intelligence
Date: 2020-10-22T23:39:54+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: is-it-iran-or-russias-hackers-we-need-to-worry-about-the-russians-definitely-the-russians-says-us-intelligence

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/22/us_russian_hacking/){:target="_blank" rel="noopener"}

> Energetic Bear team caught breaking into govt systems, no harm done to Nov 3 elections The FBI and the US government's Cybersecurity and Infrastructure Security Agency on Thursday issued a joint warning that a Kremlin hacking crew is probing or breaking into systems belonging to the US government and aviation industry.... [...]
