Title: Ransomware Group Makes Splashy $20K Donation to Charities
Date: 2020-10-20T20:36:36+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Web Security;breach;Charity;Children international;cybercriminals;Dark Web;Darkside;Data;data breach;data leak;donation;Extortion;ransomware;robin hood;The Water Project
Slug: ransomware-group-makes-splashy-20k-donation-to-charities

[Source](https://threatpost.com/ransomware-20k-donation-charities/160386/){:target="_blank" rel="noopener"}

> Cybercriminal gang Darkside sent $20K in donations to charities in a ‘Robin Hood’ effort that’s likely intended to draw attention to future data dumps, according to experts. [...]
