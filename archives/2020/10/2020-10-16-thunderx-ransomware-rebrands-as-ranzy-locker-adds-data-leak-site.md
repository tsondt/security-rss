Title: ThunderX Ransomware rebrands as Ranzy Locker, adds data leak site
Date: 2020-10-16T16:07:29
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: thunderx-ransomware-rebrands-as-ranzy-locker-adds-data-leak-site

[Source](https://www.bleepingcomputer.com/news/security/thunderx-ransomware-rebrands-as-ranzy-locker-adds-data-leak-site/){:target="_blank" rel="noopener"}

> ThunderX has changed its name to Ranzy Locker and launched a data leak site where they shame victims who do not pay the ransom. [...]
