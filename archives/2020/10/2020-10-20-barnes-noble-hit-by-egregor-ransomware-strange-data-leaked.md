Title: Barnes & Noble hit by Egregor ransomware, strange data leaked
Date: 2020-10-20T14:53:42
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: barnes-noble-hit-by-egregor-ransomware-strange-data-leaked

[Source](https://www.bleepingcomputer.com/news/security/barnes-and-noble-hit-by-egregor-ransomware-strange-data-leaked/){:target="_blank" rel="noopener"}

> ​U.S. Bookstore giant Barnes & Noble was hit by the Egregor ransomware gang on October 10th, 2020, that led to a disruption of services and the theft of unencrypted files. [...]
