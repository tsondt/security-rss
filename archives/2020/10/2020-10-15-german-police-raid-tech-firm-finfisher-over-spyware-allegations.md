Title: German police raid tech firm FinFisher over spyware allegations
Date: 2020-10-15T15:42:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: german-police-raid-tech-firm-finfisher-over-spyware-allegations

[Source](https://portswigger.net/daily-swig/german-police-raid-tech-firm-finfisher-over-spyware-allegations){:target="_blank" rel="noopener"}

> Company accused of selling surveillance software outside the EU without an export license [...]
