Title: Rapper Scams $1.2M in COVID-19 Relief, Gloats with ‘EDD’ Video
Date: 2020-10-19T19:22:56+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Privacy;Web Security;arrested;BEC;big wizza;Business Email Compromise;CARES Act;EDD;Fontrell Antonio Baines;Fraud;Identity theft;Nuke Bizzle;Pandemic Unemployment insurance;Phishing;phishing scam;PUA;scattered canary;tax data;Tax Fraud
Slug: rapper-scams-12m-in-covid-19-relief-gloats-with-edd-video

[Source](https://threatpost.com/rapper-scams-covid-19-relief-video/160315/){:target="_blank" rel="noopener"}

> "Nuke Bizzle" faces 22 years in prison after brazenly bragging about an identity-theft campaign in his music video, "EDD." [...]
