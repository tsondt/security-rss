Title: Travelex, Other Orgs Face DDoS Threats as Extortion Campaign Rages On
Date: 2020-10-14T20:40:17+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security;Armada Collective;Bitcoin;DDoS;Distributed Denial of Service;Extortion;Fancy Bear;Lazarus Group;ransom;ransomware;travelex
Slug: travelex-other-orgs-face-ddos-threats-as-extortion-campaign-rages-on

[Source](https://threatpost.com/travelex-ddos-extortion-campaign/160110/){:target="_blank" rel="noopener"}

> Organizations worldwide – including Travelex – have been sent letters threatening to launch DDoS attacks on their network unless a $230K ransom is paid. [...]
