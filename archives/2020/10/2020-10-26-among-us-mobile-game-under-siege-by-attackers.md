Title: ‘Among Us’ Mobile Game Under Siege by Attackers
Date: 2020-10-26T19:15:04+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Web Security;Alexandria Ocasio-Cortez;Among Us;among us 2;Among us subreddit;AOC;bots;breach;Cybersecurity;Eric Loris;eris loris;game servers;Gaming;hack;Hackers;InnerSloth;kicked off;mobile game;security breach;server update
Slug: among-us-mobile-game-under-siege-by-attackers

[Source](https://threatpost.com/among-us-mobile-game-attackers/160555/){:target="_blank" rel="noopener"}

> Ongoing attacks on the wildly popular game Among Us are testing developers’ ability to keep up. [...]
