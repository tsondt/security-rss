Title: FIFA 21 Blockbuster Release Gives Fraudsters an Open Field for Theft
Date: 2020-10-15T20:08:40+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security;attack;coins;COVID positive;Cristiano Ronaldo;cyberattack;Cybersecurity;FIFA;FIFA 21;FIFA 21 game;FIFA cards;FIFA fans;FIFA game;FIFA game scams;FIFA points;FIFA world cup;Fraud;FUT;in-game purchases;Lionel Messi;Phishing;security breach;soccer;video game launch;World Cup;world’s most popular sport
Slug: fifa-21-blockbuster-release-gives-fraudsters-an-open-field-for-theft

[Source](https://threatpost.com/fifa-21-release-fraudsters-theft/160185/){:target="_blank" rel="noopener"}

> In-game features of the just-released FIFA 21 title give scammers easy access its vast audience. [...]
