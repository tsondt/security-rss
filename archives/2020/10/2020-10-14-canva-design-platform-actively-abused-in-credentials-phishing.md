Title: Canva design platform actively abused in credentials phishing
Date: 2020-10-14T09:00:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: canva-design-platform-actively-abused-in-credentials-phishing

[Source](https://www.bleepingcomputer.com/news/security/canva-design-platform-actively-abused-in-credentials-phishing/){:target="_blank" rel="noopener"}

> Free graphics design website Canva is being abused by threat actors to create and host intricate phishing landing pages. [...]
