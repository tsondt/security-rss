Title: Friday Squid Blogging: Saving the Humboldt Squid
Date: 2020-10-09T21:02:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-saving-the-humboldt-squid

[Source](https://www.schneier.com/blog/archives/2020/10/friday-squid-blogging-saving-the-humboldt-squid.html){:target="_blank" rel="noopener"}

> Genetic research finds the Humboldt squid is vulnerable to overfishing. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
