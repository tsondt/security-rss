Title: Let's talk about data security in the age of the 'new normal' with folks from FireEye, Microsoft, Splunk – and more
Date: 2020-10-02T16:00:04+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: lets-talk-about-data-security-in-the-age-of-the-new-normal-with-folks-from-fireeye-microsoft-splunk-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/02/data_security_summit/){:target="_blank" rel="noopener"}

> Catch up with cybersecurity heavyweights in the comfort of your living room via Rubrik's virtual Data Security Summit Promo After six months of the so-called new normal, are you ready to take a breath? Or are you acutely aware that the real threats to your organization are only now becoming clear?... [...]
