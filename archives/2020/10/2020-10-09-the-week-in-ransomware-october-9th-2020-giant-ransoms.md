Title: The Week in Ransomware - October 9th 2020 - Giant ransoms
Date: 2020-10-09T18:22:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-october-9th-2020-giant-ransoms

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-9th-2020-giant-ransoms/){:target="_blank" rel="noopener"}

> Ransomware continues to run rampant this week, with well-known organizations getting hit with massive ransomware attacks. [...]
