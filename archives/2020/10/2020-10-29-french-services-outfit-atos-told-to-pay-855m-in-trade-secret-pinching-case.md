Title: French services outfit Atos told to pay $855m in trade secret pinching case
Date: 2020-10-29T07:02:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: french-services-outfit-atos-told-to-pay-855m-in-trade-secret-pinching-case

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/29/atos_syntel_trizetto_verdict/){:target="_blank" rel="noopener"}

> Challenges jury verdict immediately and offers to pay one percent of damages French services outfit Atos has been ordered to pay $855m for pinching a rival’s trade secrets.... [...]
