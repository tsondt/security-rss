Title: Exponential growth in DDoS attack volumes
Date: 2020-10-16T17:30:00+00:00
Author: Damian Menscher
Category: GCP Security
Tags: Networking;Google Cloud Platform;Identity & Security
Slug: exponential-growth-in-ddos-attack-volumes

[Source](https://cloud.google.com/blog/products/identity-security/identifying-and-protecting-against-the-largest-ddos-attacks/){:target="_blank" rel="noopener"}

> Security threats such as distributed denial-of-service (DDoS) attacks disrupt businesses of all sizes, leading to outages, and worse, loss of user trust. These threats are a big reason why at Google we put a premium on service reliability that’s built on the foundation of a rugged network. To help ensure reliability, we’ve devised some innovative ways to defend against advanced attacks. In this post, we’ll take a deep dive into DDoS threats, showing the trends we’re seeing and describing how we prepare for multi-terabit attacks, so your sites stay up and running. Taxonomy of attacker capabilities With a DDoS attack, [...]
