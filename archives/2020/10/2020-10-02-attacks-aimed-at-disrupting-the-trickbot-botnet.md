Title: Attacks Aimed at Disrupting the Trickbot Botnet
Date: 2020-10-02T18:20:26+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;The Coming Storm;alex holden;Hold Security;Intel 471;Mark Arena;Ryuk;trickbot;UHS;Universal Health Services
Slug: attacks-aimed-at-disrupting-the-trickbot-botnet

[Source](https://krebsonsecurity.com/2020/10/attacks-aimed-at-disrupting-the-trickbot-botnet/){:target="_blank" rel="noopener"}

> Over the past 10 days, someone has been launching a series of coordinated attacks designed to disrupt Trickbot, an enormous collection of more than two million malware-infected Windows PCs that are constantly being harvested for financial data and are often used as the entry point for deploying ransomware within compromised organizations. A text snippet from one of the bogus Trickbot configuration updates. Source: Intel 471 On Sept. 22, someone pushed out a new configuration file to Windows computers currently infected with Trickbot. The crooks running the Trickbot botnet typically use these config files to pass new instructions to their fleet [...]
