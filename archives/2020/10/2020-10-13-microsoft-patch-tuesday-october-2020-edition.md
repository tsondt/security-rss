Title: Microsoft Patch Tuesday, October 2020 Edition
Date: 2020-10-13T20:10:36+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;CVE-2020-16898;CVE-2020-16947;Dustin Childs;Flash Player patch;Ivanti;mcafee;Microsoft Patch Tuesday October 2020;Steve Povolny;Todd Schell;trend micro;Zero Day Initiative
Slug: microsoft-patch-tuesday-october-2020-edition

[Source](https://krebsonsecurity.com/2020/10/microsoft-patch-tuesday-october-2020-edition/){:target="_blank" rel="noopener"}

> It’s Cybersecurity Awareness Month! In keeping with that theme, if you (ab)use Microsoft Windows computers you should be aware the company shipped a bevy of software updates today to fix at least 87 security problems in Windows and programs that run on top of the operating system. That means it’s once again time to backup and patch up. Eleven of the vulnerabilities earned Microsoft’s most-dire “critical” rating, which means bad guys or malware could use them to gain complete control over an unpatched system with little or no help from users. Worst in terms of outright scariness is probably CVE-2020-16898, [...]
