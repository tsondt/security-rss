Title: Cisco Warns of Severe DoS Flaws in Network Security Software
Date: 2020-10-21T18:57:51+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;Adaptive Security Appliance;Bugs;Cisco;Cross-site request forgery;CSRF;CVE-2020-3456;CVE-2020-3499;CVE-2020-3562;CVE-2020-3563;CVE-2020-3571;Denial of Service;DoS;firepower threat defense;Patches;Security Vulnerabilities
Slug: cisco-warns-of-severe-dos-flaws-in-network-security-software

[Source](https://threatpost.com/cisco-dos-flaws-network-security-software/160414/){:target="_blank" rel="noopener"}

> The majority of the bugs in Cisco’s Firepower Threat Defense (FTD) and Adaptive Security Appliance (ASA) software can enable denial of service (DoS) on affected devices. [...]
