Title: Sophisticated Android Ransomware Executes with the Home Button
Date: 2020-10-09T17:40:41+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Android ransomware;home button;machine learning;mallocker;Microsoft;mobile malware;permissions;sophisticated
Slug: sophisticated-android-ransomware-executes-with-the-home-button

[Source](https://threatpost.com/android-ransomware-home-button/160001/){:target="_blank" rel="noopener"}

> The malware also has a unique machine-learning module. [...]
