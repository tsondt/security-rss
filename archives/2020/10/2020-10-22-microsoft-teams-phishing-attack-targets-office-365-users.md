Title: Microsoft Teams Phishing Attack Targets Office 365 Users
Date: 2020-10-22T17:48:08+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security;Credentials;malicious email;malicious link;Microsoft;microsoft teams;Office 365;phishing campaign;Phishing emails;phishing link
Slug: microsoft-teams-phishing-attack-targets-office-365-users

[Source](https://threatpost.com/microsoft-teams-phishing-office-365/160458/){:target="_blank" rel="noopener"}

> Up to 50,000 Office 365 users are being targeted by a phishing campaign that purports to notify them of a "missed chat" from Microsoft Teams. [...]
