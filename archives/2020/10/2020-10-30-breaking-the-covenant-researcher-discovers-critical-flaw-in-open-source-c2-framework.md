Title: Breaking the Covenant: Researcher discovers critical flaw in open source C2 framework
Date: 2020-10-30T16:32:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: breaking-the-covenant-researcher-discovers-critical-flaw-in-open-source-c2-framework

[Source](https://portswigger.net/daily-swig/breaking-the-covenant-researcher-discovers-critical-flaw-in-open-source-c2-framework){:target="_blank" rel="noopener"}

> Tables turned as red teaming tool gets pwned [...]
