Title: Apple pays $288,000 to white-hat hackers who had run of company’s network
Date: 2020-10-08T23:47:10+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;apple;bug bounties;hacking;icloud
Slug: apple-pays-288000-to-white-hat-hackers-who-had-run-of-companys-network

[Source](https://arstechnica.com/?p=1712964){:target="_blank" rel="noopener"}

> Enlarge (credit: Nick Wright. Used by permission.) For months, Apple’s corporate network was at risk of hacks that could have stolen sensitive data from potentially millions of its customers and executed malicious code on their phones and computers, a security researcher said on Thursday. Sam Curry, a 20-year-old researcher who specializes in website security, said that, in total, he and his team found 55 vulnerabilities. He rated 11 of them critical because they allowed him to take control of core Apple infrastructure and from there steal private emails, iCloud data, and other private information. The 11 critical bugs were: Read [...]
