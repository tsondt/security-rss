Title: Mac users unable to print after Apple revoked HP certificate
Date: 2020-10-27T16:24:06
Author: Ax Sharma
Category: BleepingComputer
Tags: Apple
Slug: mac-users-unable-to-print-after-apple-revoked-hp-certificate

[Source](https://www.bleepingcomputer.com/news/security/mac-users-unable-to-print-after-apple-revoked-hp-certificate/){:target="_blank" rel="noopener"}

> Apple macOS X users with HP printers are left unable to print from their computers after Apple revoked a certificate that signed HP's print drivers. The result is print drivers being mistaken for malware. [...]
