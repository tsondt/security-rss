Title: Tech giants among those affected by breach at PDF signature software maker Nitro
Date: 2020-10-27T15:54:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: tech-giants-among-those-affected-by-breach-at-pdf-signature-software-maker-nitro

[Source](https://portswigger.net/daily-swig/tech-giants-among-those-affected-by-breach-at-pdf-signature-software-maker-nitro){:target="_blank" rel="noopener"}

> Darknet auction spawns fears that attack might expose sensitive data from business customers [...]
