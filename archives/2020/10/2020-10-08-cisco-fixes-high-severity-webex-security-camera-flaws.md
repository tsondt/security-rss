Title: Cisco Fixes High-Severity Webex, Security Camera Flaws
Date: 2020-10-08T17:30:18+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;Cisco;cisco discovery protocol;Cisco WebEx;Cisco’s Video Surveillance 8000 Series IP Cameras;CVE-2020-3467;CVE-2020-3535;CVE-2020-3544;high severity flaw;Identity Services Engine;Patches;security camera;Security Vulnerabilities
Slug: cisco-fixes-high-severity-webex-security-camera-flaws

[Source](https://threatpost.com/cisco-webex-security-camera-flaws/159969/){:target="_blank" rel="noopener"}

> Three high-severity flaws exist in Cisco's Webex video conferencing system, Cisco’s Video Surveillance 8000 Series IP Cameras and Identity Services Engine. [...]
