Title: Office 365 OAuth Attack Targets Coinbase Users
Date: 2020-10-20T14:33:34+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security;account takeover;Coinbase;consent app;email attack;inbox access;malicious OAuth app;Microsoft;oauth;Office 365
Slug: office-365-oauth-attack-targets-coinbase-users

[Source](https://threatpost.com/office-365-oauth-attack-coinbase/160337/){:target="_blank" rel="noopener"}

> Attackers are targeting Microsoft Office 365 users with a Coinbase-themed attack, aiming to take control of their inboxes via OAuth. [...]
