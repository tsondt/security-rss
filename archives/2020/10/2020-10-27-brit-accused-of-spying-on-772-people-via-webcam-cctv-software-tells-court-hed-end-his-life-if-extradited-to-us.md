Title: Brit accused of spying on 772 people via webcam CCTV software tells court he'd end his life if extradited to US
Date: 2020-10-27T15:41:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: brit-accused-of-spying-on-772-people-via-webcam-cctv-software-tells-court-hed-end-his-life-if-extradited-to-us

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/27/christopher_taylor_extradition_hearing/){:target="_blank" rel="noopener"}

> 'I've seen programmes on American prisons' says wife A Briton is reportedly fighting extradition to the United States after deploying webcam malware onto hundreds of women's laptops so he could spy on them undressing and having sex.... [...]
