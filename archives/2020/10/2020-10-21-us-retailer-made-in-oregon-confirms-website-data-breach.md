Title: US retailer Made in Oregon confirms website data breach
Date: 2020-10-21T13:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-retailer-made-in-oregon-confirms-website-data-breach

[Source](https://portswigger.net/daily-swig/us-retailer-made-in-oregon-confirms-website-data-breach){:target="_blank" rel="noopener"}

> Customer credit card details included in list of potentially stolen information [...]
