Title: Will there be no end to govt attempts to break encryption? Hand over your data or the kiddies get it, threaten Five Eyes spies
Date: 2020-10-19T10:30:08+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: will-there-be-no-end-to-govt-attempts-to-break-encryption-hand-over-your-data-or-the-kiddies-get-it-threaten-five-eyes-spies

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/19/e2e_break_five_eyes/){:target="_blank" rel="noopener"}

> The Great Unicorn Prayer of security services: Stay secure, but - ya know - give us backdoors In a move as predictable as it is wearisome, a bunch of government security agencies have got together and demanded we let them have our data. This latest spooky manifestation is a collection of the Five Eyes - the US, the UK, Canada, Australia and New Zealand - and for some reason Japan and India. Let’s call this coalition of the chilling, JIANUSCUK.... [...]
