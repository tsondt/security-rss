Title: Facebook, News and XSS Underpin Complex Browser Locker Attack
Date: 2020-10-22T17:52:01+00:00
Author: Tara Seals
Category: Threatpost
Tags: Facebook;Vulnerabilities;Web Security;browser locker;Cross Site Scripting;grupo ppe;Malwarebytes;news site;Open Redirect;peru;redirections;security bug;tech support scam;XSS
Slug: facebook-news-and-xss-underpin-complex-browser-locker-attack

[Source](https://threatpost.com/facebook-xss-browser-locker/160465/){:target="_blank" rel="noopener"}

> An elaborate set of redirections and hundreds of URLs make up a wide-ranging tech-support scam. [...]
