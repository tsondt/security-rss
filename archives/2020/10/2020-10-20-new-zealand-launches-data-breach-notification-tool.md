Title: New Zealand launches data breach notification tool
Date: 2020-10-20T12:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: new-zealand-launches-data-breach-notification-tool

[Source](https://portswigger.net/daily-swig/new-zealand-launches-data-breach-notification-tool){:target="_blank" rel="noopener"}

> NotifyUs service aids businesses and organizations [...]
