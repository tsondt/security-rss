Title: Microsoft releases Azure Defender for IoT in public preview
Date: 2020-10-16T13:01:26
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-releases-azure-defender-for-iot-in-public-preview

[Source](https://www.bleepingcomputer.com/news/security/microsoft-releases-azure-defender-for-iot-in-public-preview/){:target="_blank" rel="noopener"}

> Microsoft announced today that Azure Defender for IoT, its agentless security solution for networked IoT and Operational Technology (OT) devices, has entered public preview. [...]
