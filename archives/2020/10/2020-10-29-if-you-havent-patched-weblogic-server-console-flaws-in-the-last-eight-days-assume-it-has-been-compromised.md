Title: If you haven't patched WebLogic server console flaws in the last eight days 'assume it has been compromised'
Date: 2020-10-29T22:35:20+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: if-you-havent-patched-weblogic-server-console-flaws-in-the-last-eight-days-assume-it-has-been-compromised

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/29/weblogic_exploit_attack/){:target="_blank" rel="noopener"}

> Stark warning from SANS' Johannes Ullrich - RCE's gonna GET 'ya Last week Oracle released one of its mammoth quarterly patch dumps - with 402 fixes. Well, it turns out that if you missed one and you're running WebLogic 10.3.6.0.0, 12.1.3.0.0, 12.2.1.3.0, 12.2.1.4.0 and 14.1.1.0.0, you've probably already been tagged by hackers.... [...]
