Title: Adobe fixes 18 critical bugs affecting its Windows, macOS apps
Date: 2020-10-20T13:55:50
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: adobe-fixes-18-critical-bugs-affecting-its-windows-macos-apps

[Source](https://www.bleepingcomputer.com/news/security/adobe-fixes-18-critical-bugs-affecting-its-windows-macos-apps/){:target="_blank" rel="noopener"}

> Adobe has released security updates to address critical vulnerabilities affecting ten of its Windows and macOS products that could allow attackers to execute arbitrary code on devices running vulnerable software versions. [...]
