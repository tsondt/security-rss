Title: Montreal's STM public transport system hit by ransomware attack
Date: 2020-10-21T01:26:01
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: montreals-stm-public-transport-system-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/montreals-stm-public-transport-system-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Montreal's Société de transport de Montréal (STM) public transport system was hit with a RansomExx ransomware attack that has impacted services and online systems. [...]
