Title: Santander downplays 'hack' of PagoFX cash transfer biz, says nothing to worry about
Date: 2020-10-27T06:02:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: santander-downplays-hack-of-pagofx-cash-transfer-biz-says-nothing-to-worry-about

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/27/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: US govt sanctions Russia for refinery-bothering malware In brief Spanish financial giant Santander has downplayed claims its international money transfer startup PagoFX was compromised.... [...]
