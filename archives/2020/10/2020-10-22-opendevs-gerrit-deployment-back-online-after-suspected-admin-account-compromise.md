Title: OpenDev’s Gerrit deployment back online after suspected admin account compromise
Date: 2020-10-22T14:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: opendevs-gerrit-deployment-back-online-after-suspected-admin-account-compromise

[Source](https://portswigger.net/daily-swig/opendevs-gerrit-deployment-back-online-after-suspected-admin-account-compromise){:target="_blank" rel="noopener"}

> ‘Suspicious activity’ caused devs to pull the plug pending further investigation [...]
