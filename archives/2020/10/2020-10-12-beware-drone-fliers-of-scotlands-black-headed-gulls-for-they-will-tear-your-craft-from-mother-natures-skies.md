Title: Beware, drone fliers, of Scotland's black-headed gulls. For they will tear your craft from Mother Nature's skies
Date: 2020-10-12T11:00:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: beware-drone-fliers-of-scotlands-black-headed-gulls-for-they-will-tear-your-craft-from-mother-natures-skies

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/12/drone_gull_attack_scotland/){:target="_blank" rel="noopener"}

> Innocent survey UAS brutally smashed into roof by Stranraer seabird An innocent drone has crashed after being attacked by an aggressive Scottish black-headed gull.... [...]
