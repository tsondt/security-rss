Title: News Wrap: Barnes & Noble Hack, DDoS Extortion Threats and More
Date: 2020-10-16T13:00:35+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Malware;Podcasts;adobe patch;barnes and noble;cyberattack;DDoS;email threat;Encryption;End to end encryption;Extortion;hack;Microsoft;microsoft ping of death;patch tuesday;Threatpost podcast;zoom
Slug: news-wrap-barnes-noble-hack-ddos-extortion-threats-and-more

[Source](https://threatpost.com/barnes-noble-hack-ddos-extortion-threats/160193/){:target="_blank" rel="noopener"}

> From a cyberattack on Barnes & Noble to Zoom rolling out end-to-end encryption, Threatpost editors break down the top security stories of the week. [...]
