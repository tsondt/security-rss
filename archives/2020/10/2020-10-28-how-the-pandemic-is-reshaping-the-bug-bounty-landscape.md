Title: How the Pandemic is Reshaping the Bug-Bounty Landscape
Date: 2020-10-28T17:23:27+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Bug Bounty;Newsmaker Interviews;Videos;bug bounty;bug bounty program;Bugcrowd;Casey Ellis;COVID-19;Ethical Hacking;Pandemic;pen testing;uptick;VDP;video interview;vulnerability disclosure program
Slug: how-the-pandemic-is-reshaping-the-bug-bounty-landscape

[Source](https://threatpost.com/pandemic-reshaping-bug-bounty-landscape/160644/){:target="_blank" rel="noopener"}

> Bugcrowd Founder Casey Ellis talks about COVID-19's impact on bug bounty hunters, bug bounty program adoption and more. [...]
