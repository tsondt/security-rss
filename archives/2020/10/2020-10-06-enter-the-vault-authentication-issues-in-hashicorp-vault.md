Title: Enter the Vault: Authentication Issues in HashiCorp Vault
Date: 2020-10-06T09:38:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: enter-the-vault-authentication-issues-in-hashicorp-vault

[Source](https://googleprojectzero.blogspot.com/2020/10/enter-the-vault-auth-issues-hashicorp-vault.html){:target="_blank" rel="noopener"}

> Posted by Felix Wilhelm, Project Zero Introduction In this blog post I'll discuss two vulnerabilities in HashiCorp Vault and its integration with Amazon Web Services (AWS) and Google Cloud Platform (GCP). These issues can lead to an authentication bypass in configurations that use the aws and gcp auth methods, and demonstrate the type of issues you can find in modern “cloud-native” software. Both vulnerabilities ( CVE-2020-16250/16251) were addressed by HashiCorp and are fixed in Vault versions 1.2.5, 1.3.8, 1.4.4 and 1.5.1 released in August. Vault is a widely used tool for securely storing, generating and accessing secrets such as API [...]
