Title: Apple's T2 custom secure boot chip is not only insecure, it cannot be fixed without replacing the silicon
Date: 2020-10-08T11:04:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: apples-t2-custom-secure-boot-chip-is-not-only-insecure-it-cannot-be-fixed-without-replacing-the-silicon

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/08/apple_t2_security_chip/){:target="_blank" rel="noopener"}

> Which means your new Mac is vulnerable to 'evil maid' attacks, if that's something you worry about Apple's T2 security chip is insecure and cannot be fixed, a group of security researchers report.... [...]
