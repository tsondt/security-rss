Title: On Risk-Based Authentication
Date: 2020-10-05T16:47:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;authentication;passwords;risk assessment;risks;usability
Slug: on-risk-based-authentication

[Source](https://www.schneier.com/blog/archives/2020/10/on-risk-based-authentication.html){:target="_blank" rel="noopener"}

> Interesting usability study: “ More Than Just Good Passwords? A Study on Usability and Security Perceptions of Risk-based Authentication “: Abstract : Risk-based Authentication (RBA) is an adaptive security measure to strengthen password-based authentication. RBA monitors additional features during login, and when observed feature values differ significantly from previously seen ones, users have to provide additional authentication factors such as a verification code. RBA has the potential to offer more usable authentication, but the usability and the security perceptions of RBA are not studied well. We present the results of a between-group lab study (n=65) to evaluate usability and security [...]
