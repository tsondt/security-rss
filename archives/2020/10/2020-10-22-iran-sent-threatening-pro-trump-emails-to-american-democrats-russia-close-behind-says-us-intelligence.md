Title: Iran sent threatening pro-Trump emails to American Democrats, Russia close behind, says US intelligence
Date: 2020-10-22T02:03:18+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: iran-sent-threatening-pro-trump-emails-to-american-democrats-russia-close-behind-says-us-intelligence

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/22/iran_russia_emails/){:target="_blank" rel="noopener"}

> No, say it ain't so, maga_christ9000@post.ir Menacing emails to Democratic voters, telling them to vote for Donald Trump in the upcoming US elections or else, were sent by Iran, US intelligence claimed on Wednesday night.... [...]
