Title: British Airways fined £20m for Magecart hack that exposed 400k folks' credit card details to crooks
Date: 2020-10-16T12:15:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: british-airways-fined-ps20m-for-magecart-hack-that-exposed-400k-folks-credit-card-details-to-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/16/british_airways_ico_fine_20m/){:target="_blank" rel="noopener"}

> Airline was saving domain admin creds and card details alike in plaintext British Airways is to pay a £20m data protection fine after its 2018 Magecart hack – even though the Information Commissioner’s Office discovered the airline had been saving credit card details in plain text since 2015.... [...]
