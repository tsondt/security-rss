Title: FBI shares technical details on Iran's fake Proud Boys emails
Date: 2020-10-30T15:43:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-shares-technical-details-on-irans-fake-proud-boys-emails

[Source](https://www.bleepingcomputer.com/news/security/fbi-shares-technical-details-on-irans-fake-proud-boys-emails/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) shared indicators of compromise (IOCs) associated with the Iranian state-sponsored threat group behind last week's Proud Boys voter intimidation emails that targeted Democratic voters. [...]
