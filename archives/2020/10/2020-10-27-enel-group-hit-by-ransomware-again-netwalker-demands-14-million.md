Title: Enel Group hit by ransomware again, Netwalker demands $14 million
Date: 2020-10-27T13:12:31
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: enel-group-hit-by-ransomware-again-netwalker-demands-14-million

[Source](https://www.bleepingcomputer.com/news/security/enel-group-hit-by-ransomware-again-netwalker-demands-14-million/){:target="_blank" rel="noopener"}

> Networks of giant energy company Enel have been hit by a ransomware attack for the second time this year. This time, it's Netwalker asking $14 million ransom for the decryption key. [...]
