Title: Ransomware Attackers Buy Network Access in Cyberattack Shortcut
Date: 2020-10-12T13:00:49+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Malware;Avaddon;compromised network;cyberattack;Exorcist;Lockbit;maze;netwalker;network access;network access seller;ransomware;RDP;Sodinokibi;VPN
Slug: ransomware-attackers-buy-network-access-in-cyberattack-shortcut

[Source](https://threatpost.com/ransomware-network-access-cyberattack/159998/){:target="_blank" rel="noopener"}

> Network access to various industries is being offered in underground forums at as little as $300 a pop - and researchers warn that ransomware groups like Maze and NetWalker could be buying in. [...]
