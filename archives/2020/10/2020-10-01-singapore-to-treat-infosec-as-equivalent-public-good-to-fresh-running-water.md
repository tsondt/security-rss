Title: Singapore to treat infosec as equivalent public good to fresh running water
Date: 2020-10-01T05:13:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: singapore-to-treat-infosec-as-equivalent-public-good-to-fresh-running-water

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/01/singapore_infosec_strategy/){:target="_blank" rel="noopener"}

> Consumer tech security rating scheme to launch next week, will be offered to rest of world The deputy chief executive of Singapore’s Cyber Security Agency, Brigadier General Gaurav Keerthi, says the island nation now considers providing a secure environment to citizens and businesses the equivalent of providing fresh water and sewerage services, and will next week improve digital hygiene with a voluntary "Cybersecurity Labelling Scheme" that will rate consumer broadband gateways.... [...]
