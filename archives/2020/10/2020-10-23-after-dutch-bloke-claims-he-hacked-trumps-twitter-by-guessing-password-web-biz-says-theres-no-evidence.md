Title: After Dutch bloke claims he hacked Trump's Twitter by guessing password, web biz says there's 'no evidence'
Date: 2020-10-23T05:36:16+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: after-dutch-bloke-claims-he-hacked-trumps-twitter-by-guessing-password-web-biz-says-theres-no-evidence

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/23/trump_twitter_account_no_mfa/){:target="_blank" rel="noopener"}

> It's saying something when it's easy to believe the US President's passphrase was maga2020! Donald Trump's Twitter password was easily guessed, and he still isn't using multi-factor authentication, claims a Dutch hacker who on Thursday bragged he broke into the President's account last week. Twitter says it has "no evidence" this claim is true.... [...]
