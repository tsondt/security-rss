Title: How to implement password-less authentication with Amazon Cognito and WebAuthn
Date: 2020-10-30T17:45:53+00:00
Author: Mahmoud Matouk
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;Security, Identity, & Compliance;FIDO2;Identity;Security Blog;WebAuthn
Slug: how-to-implement-password-less-authentication-with-amazon-cognito-and-webauthn

[Source](https://aws.amazon.com/blogs/security/how-to-implement-password-less-authentication-with-amazon-cognito-and-webauthn/){:target="_blank" rel="noopener"}

> In this blog post, I show you how to offer a password-less authentication experience to your customers. To do this, you’ll allow physical security keys or platform authenticators (like finger-print scanners) to be used as the authentication factor to your web or mobile applications that use Amazon Cognito user pools for authentication. An Amazon Cognito [...]
