Title: Microsoft Uses Trademark Law to Disrupt Trickbot Botnet
Date: 2020-10-12T12:52:02+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;The Coming Storm
Slug: microsoft-uses-trademark-law-to-disrupt-trickbot-botnet

[Source](https://krebsonsecurity.com/2020/10/microsoft-uses-copyright-law-to-disrupt-trickbot-botnet/){:target="_blank" rel="noopener"}

> Microsoft Corp. has executed a coordinated legal sneak attack in a bid to disrupt the malware-as-a-service botnet Trickbot, a global menace that has infected millions of computers and is used to spread ransomware. A court in Virginia granted Microsoft control over many Internet servers Trickbot uses to plunder infected systems, based on novel claims that the crime machine abused the software giant’s trademarks. However, it appears the operation has not completely disabled the botnet. A spam email containing a Trickbot-infected attachment that was sent earlier this year. Image: Microsoft. “We disrupted Trickbot through a court order we obtained as well [...]
