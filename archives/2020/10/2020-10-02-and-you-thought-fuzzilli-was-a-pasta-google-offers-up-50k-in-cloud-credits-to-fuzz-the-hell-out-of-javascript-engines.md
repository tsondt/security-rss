Title: And you thought Fuzzilli was a pasta... Google offers up $50k in cloud credits to fuzz the hell out of JavaScript engines
Date: 2020-10-02T22:50:38+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: and-you-thought-fuzzilli-was-a-pasta-google-offers-up-50k-in-cloud-credits-to-fuzz-the-hell-out-of-javascript-engines

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/02/google_javascript_fuzzing_funds/){:target="_blank" rel="noopener"}

> And don't forget the paperwork after, says Chocolate Factory Google is offering bug hunters thousands of dollars worth of compute time on its cloud to hammer away at JavaScript engines and uncover new security flaws in the software.... [...]
