Title: Critical SonicWall VPN Portal Bug Allows DoS, Worming RCE
Date: 2020-10-14T18:43:23+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security;Craig Young;critical bug;CVE-2020-5135;Denial of Service;DoS;Exploit;Network Security Appliance;pre-authentication;RCE;remote code execution;security vulnerability;SonicWall;stack-based buffer overflow;Tripwire;trivial;vpn portal;Worm
Slug: critical-sonicwall-vpn-portal-bug-allows-dos-worming-rce

[Source](https://threatpost.com/critical-sonicwall-vpn-bug/160108/){:target="_blank" rel="noopener"}

> The CVE-2020-5135 stack-based buffer overflow security vulnerability is trivial to exploit, without logging in. [...]
