Title: Microsoft October 2020 Patch Tuesday fixes 87 security bugs
Date: 2020-10-13T13:47:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: microsoft-october-2020-patch-tuesday-fixes-87-security-bugs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-october-2020-patch-tuesday-fixes-87-security-bugs/){:target="_blank" rel="noopener"}

> Today is Microsoft's October 2020 Patch Tuesday, and your Windows administrators will be pulling their hair out as they install new updates and try to fix bugs that pop up. [...]
