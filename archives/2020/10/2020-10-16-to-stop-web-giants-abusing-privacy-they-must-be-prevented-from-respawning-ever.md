Title: To stop web giants abusing privacy, they must be prevented from respawning. Ever
Date: 2020-10-16T15:00:13+00:00
Author: Mark Pesce
Category: The Register
Tags: 
Slug: to-stop-web-giants-abusing-privacy-they-must-be-prevented-from-respawning-ever

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/16/web_giants_breakup/){:target="_blank" rel="noopener"}

> History tells us tech companies just get bigger, even after being broken up or battered Column Thriving amidst the pervasive chaos of 2020, the world’s largest technology companies - the FAANGs*, as we’ve come to know them - have managed to grow larger, richer and more powerful.... [...]
