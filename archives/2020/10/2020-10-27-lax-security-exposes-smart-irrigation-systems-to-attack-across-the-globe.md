Title: Lax Security Exposes Smart-Irrigation Systems to Attack Across the Globe
Date: 2020-10-27T21:43:09+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Critical Infrastructure;IoT;Web Security;Connected devices;cory gardner bill;critical infrastructure;cyberattack;Cybersecurity;default password;galilee;government;infrastructure security;Internet of things;irrigation systems;Israel;mottech water management;open to internet;Security Joes;smart irrigation;water system attacks
Slug: lax-security-exposes-smart-irrigation-systems-to-attack-across-the-globe

[Source](https://threatpost.com/lax-security-smart-irrigation-attack/160625/){:target="_blank" rel="noopener"}

> Systems designed by Mottech Water Management were misconfigured and put in place and connected to the internet without password protections. [...]
