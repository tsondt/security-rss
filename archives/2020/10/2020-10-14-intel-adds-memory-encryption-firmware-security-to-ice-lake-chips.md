Title: Intel Adds Memory Encryption, Firmware Security to Ice Lake Chips
Date: 2020-10-14T15:45:12+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;IoT;Vulnerabilities;AMD;chip level protection;CPU;hardware;ice lake;Intel;intel platform firmware resilience;Intel Security;Meltdown;memory encryption;plundervolt;sgx;Side-channel attacks;silicon chip;specter;Spectre;speculative execution flaws;total memory encryption;xeon scalable platform
Slug: intel-adds-memory-encryption-firmware-security-to-ice-lake-chips

[Source](https://threatpost.com/intel-encryption-security-ice-lake-chips/160083/){:target="_blank" rel="noopener"}

> Intel's addition of memory encryption to its upcoming 3rd generation Xeon Scalable processors matches AMD's Secure Memory Encryption (SME) feature. [...]
