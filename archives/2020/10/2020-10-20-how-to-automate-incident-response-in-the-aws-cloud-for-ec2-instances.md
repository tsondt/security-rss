Title: How to automate incident response in the AWS Cloud for EC2 instances
Date: 2020-10-20T17:00:35+00:00
Author: Ben Eichorst
Category: AWS Security
Tags: Amazon EC2;Expert (400);Security, Identity, & Compliance;DevSecOps;Incident response;security automation;Security Blog
Slug: how-to-automate-incident-response-in-the-aws-cloud-for-ec2-instances

[Source](https://aws.amazon.com/blogs/security/how-to-automate-incident-response-in-aws-cloud-for-ec2-instances/){:target="_blank" rel="noopener"}

> One of the security epics core to the AWS Cloud Adoption Framework (AWS CAF) is a focus on incident response and preparedness to address unauthorized activity. Multiple methods exist in Amazon Web Services (AWS) for automating classic incident response techniques, and the AWS Security Incident Response Guide outlines many of these methods. This post demonstrates [...]
