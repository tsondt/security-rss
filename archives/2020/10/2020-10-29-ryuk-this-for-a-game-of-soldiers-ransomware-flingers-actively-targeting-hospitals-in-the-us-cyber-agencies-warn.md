Title: Ryuk this for a game of soldiers: Ransomware-flingers actively targeting hospitals in the US, cyber agencies warn
Date: 2020-10-29T18:15:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ryuk-this-for-a-game-of-soldiers-ransomware-flingers-actively-targeting-hospitals-in-the-us-cyber-agencies-warn

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/29/ryuk_ransomware_us_hospitals_warning/){:target="_blank" rel="noopener"}

> And infosec firms say it's only got worse over this year Ryuk ransomware is being aggressively deployed to target US healthcare institutions, government cyber organisations in the US have warned.... [...]
