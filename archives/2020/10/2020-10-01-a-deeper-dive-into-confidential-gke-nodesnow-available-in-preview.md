Title: A deeper dive into Confidential GKE Nodes—now available in preview
Date: 2020-10-01T16:00:00+00:00
Author: Anoosh Saboori
Category: GCP Security
Tags: Containers & Kubernetes;GKE;Google Cloud Platform;Identity & Security
Slug: a-deeper-dive-into-confidential-gke-nodesnow-available-in-preview

[Source](https://cloud.google.com/blog/products/identity-security/confidential-gke-nodes-now-available/){:target="_blank" rel="noopener"}

> The benefits of containers and Kubernetes over traditional on-premises architectures are well-documented and understood. But when considering moving to the cloud, organizations want controls to limit risk and potential exposure of their data. In July, we announced the availability of the Confidential Computing product family, whose breakthrough technology encrypts data in-use—while it is being processed—without any code changes to the application. We also introduced Confidential VMs as the first member of that product family, which perform at levels comparable to VMs A few weeks back we announced the upcoming launch of Confidential Google Kubernetes Engine ( GKE ) Nodes in [...]
