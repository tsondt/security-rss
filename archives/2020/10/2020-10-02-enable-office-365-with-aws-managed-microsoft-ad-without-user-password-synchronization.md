Title: Enable Office 365 with AWS Managed Microsoft AD without user password synchronization
Date: 2020-10-02T17:53:21+00:00
Author: Darryn Hendricks
Category: AWS Security
Tags: AWS Directory Service;Intermediate (200);Security, Identity, & Compliance;Azure Active Directory;Azure AD Connect;Azure AD Connect Pass-through Authentication;Microsoft Active Directory;Microsoft AD;Office 365;Pass-through Authentication;Security Blog
Slug: enable-office-365-with-aws-managed-microsoft-ad-without-user-password-synchronization

[Source](https://aws.amazon.com/blogs/security/enable-office-365-with-aws-managed-microsoft-ad-without-user-password-synchronization/){:target="_blank" rel="noopener"}

> In this post, we explain how you can use AWS Directory Service for Microsoft Active Directory (AWS Managed Microsoft AD) to enable your users to access Microsoft Office 365 without synchronizing passwords using Azure Active Directory (Azure AD) Pass-through Authentication (PTA). This makes it easier to configure Microsoft Office 365 with AWS Managed Microsoft AD. [...]
