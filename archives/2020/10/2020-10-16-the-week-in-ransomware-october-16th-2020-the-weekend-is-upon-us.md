Title: The Week in Ransomware - October 16th 2020 - The weekend is upon us
Date: 2020-10-16T19:13:50
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-october-16th-2020-the-weekend-is-upon-us

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-16th-2020-the-weekend-is-upon-us/){:target="_blank" rel="noopener"}

> Ransomware continues to target government entities and the enterprise, while victims quietly pay ransoms that power this cycle of attacks. [...]
