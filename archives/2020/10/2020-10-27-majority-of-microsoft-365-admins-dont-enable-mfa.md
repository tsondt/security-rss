Title: Majority of Microsoft 365 Admins Don’t Enable MFA
Date: 2020-10-27T14:49:40+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Cloud Security;Most Recent ThreatLists;Privacy;Web Security;admin;mfa;Microsoft;microsoft 365. office 365;Multi Factor Authentication;security policy
Slug: majority-of-microsoft-365-admins-dont-enable-mfa

[Source](https://threatpost.com/microsoft-365-admins-mfa/160592/){:target="_blank" rel="noopener"}

> Beyond admins, researchers say that 97 percent of all total Microsoft 365 users do not use multi-factor authentication. [...]
