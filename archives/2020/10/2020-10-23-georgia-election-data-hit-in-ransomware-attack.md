Title: Georgia Election Data Hit in Ransomware Attack
Date: 2020-10-23T18:21:44+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Government;Malware;attacker;cybercriminal;Election;election hack;Hall County;malware;patch;Patching;public sector;ransom;ransomware;Ransomware Attack;social engineering;voter registration;voter registration systems
Slug: georgia-election-data-hit-in-ransomware-attack

[Source](https://threatpost.com/georgia-election-data-ransomware/160499/){:target="_blank" rel="noopener"}

> With Election Day approaching, local governments need to be prepared for malware attacks on election infrastructure. [...]
