Title: What you can learn in our Q4 2020 Google Cloud Security Talks
Date: 2020-10-27T18:30:00+00:00
Author: Rob Sadowski
Category: GCP Security
Tags: Google Cloud Platform;Google Workspace;Events;Identity & Security
Slug: what-you-can-learn-in-our-q4-2020-google-cloud-security-talks

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-security-talks-q4-2020-the-latest-in-cloud-security/){:target="_blank" rel="noopener"}

> 2020 has brought with it some tremendous innovations in the area of cloud security. As cloud deployments and technologies have become an even more central part of organizations’ security program, we hope you’ll join us for the latest installment of our Google Cloud Security Talks, a live online event on November 18th, where we’ll help you navigate the latest thinking in cloud security. We’ll share expert insights into our security ecosystem and cover the following topics Sunil Potti and Rob Sadowski will open the digital event with our latest Google Cloud security announcements. This will be followed by a panel [...]
