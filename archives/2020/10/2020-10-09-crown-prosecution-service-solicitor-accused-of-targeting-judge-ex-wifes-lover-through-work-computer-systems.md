Title: Crown Prosecution Service solicitor accused of targeting judge ex-wife's lover through work computer systems
Date: 2020-10-09T15:44:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: crown-prosecution-service-solicitor-accused-of-targeting-judge-ex-wifes-lover-through-work-computer-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/09/cps_solicitor_computer_misuse_act_charges/){:target="_blank" rel="noopener"}

> Computer Misuse Act charges stack up against vengeful former hubby A Crown Prosecution Service lawyer is on trial accused of unlawfully accessing information about his judge wife's new lover after their marriage broke down.... [...]
