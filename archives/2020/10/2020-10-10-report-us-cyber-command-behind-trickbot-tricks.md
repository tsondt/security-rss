Title: Report: U.S. Cyber Command Behind Trickbot Tricks
Date: 2020-10-10T04:47:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;alex holden;Hold Security;national security agency;The Washington Post;trickbot;U.S. Cyber Command
Slug: report-us-cyber-command-behind-trickbot-tricks

[Source](https://krebsonsecurity.com/2020/10/report-u-s-cyber-command-behind-trickbot-tricks/){:target="_blank" rel="noopener"}

> A week ago, KrebsOnSecurity broke the news that someone was attempting to disrupt the Trickbot botnet, a malware crime machine that has infected millions of computers and is often used to spread ransomware. A new report Friday says the coordinated attack was part of an operation carried out by the U.S. military’s Cyber Command. Image: Shutterstock. On October 2, KrebsOnSecurity reported that twice in the preceding ten days, an unknown entity that had inside access to the Trickbot botnet sent all infected systems a command telling them to disconnect themselves from the Internet servers the Trickbot overlords used to control [...]
