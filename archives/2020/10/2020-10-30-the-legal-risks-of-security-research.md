Title: The Legal Risks of Security Research
Date: 2020-10-30T14:14:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;business of security;courts;risk assessment;risks
Slug: the-legal-risks-of-security-research

[Source](https://www.schneier.com/blog/archives/2020/10/the-legal-risks-of-security-research.html){:target="_blank" rel="noopener"}

> Sunoo Park and Kendra Albert have published “ A Researcher’s Guide to Some Legal Risks of Security Research.” From a summary : Such risk extends beyond anti-hacking laws, implicating copyright law and anti-circumvention provisions (DMCA §1201), electronic privacy law (ECPA), and cryptography export controls, as well as broader legal areas such as contract and trade secret law. Our Guide gives the most comprehensive presentation to date of this landscape of legal risks, with an eye to both legal and technical nuance. Aimed at researchers, the public, and technology lawyers alike, its aims both to provide pragmatic guidance to those navigating [...]
