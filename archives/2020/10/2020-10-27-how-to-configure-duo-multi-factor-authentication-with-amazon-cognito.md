Title: How to configure Duo multi-factor authentication with Amazon Cognito
Date: 2020-10-27T20:32:44+00:00
Author: Mahmoud Matouk
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;Security, Identity, & Compliance;Identity;MFA;Security Blog
Slug: how-to-configure-duo-multi-factor-authentication-with-amazon-cognito

[Source](https://aws.amazon.com/blogs/security/how-to-configure-duo-multi-factor-authentication-with-amazon-cognito/){:target="_blank" rel="noopener"}

> Adding multi-factor authentication (MFA) reduces the risk of user account take-over, phishing attacks, and password theft. Adding MFA while providing a frictionless sign-in experience requires you to offer a variety of MFA options that support a wide range of users and devices. Let’s see how you can achieve that with Amazon Cognito and Duo MFA. [...]
