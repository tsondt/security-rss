Title: Home Depot Confirms Data Breach in Order Confirmation SNAFU
Date: 2020-10-29T15:28:51+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Privacy;Web Security;Canada;data breach;emails;home improvement;in-store pickups;online order confirmations;online orders;order details;payment card information;personal information;Phishing;physical addresses;the home depot
Slug: home-depot-confirms-data-breach-in-order-confirmation-snafu

[Source](https://threatpost.com/home-depot-data-breach-order-confirmation/160728/){:target="_blank" rel="noopener"}

> Hundreds of emailed order confirmations for random strangers were sent to Canadian customers, each containing personal information. [...]
