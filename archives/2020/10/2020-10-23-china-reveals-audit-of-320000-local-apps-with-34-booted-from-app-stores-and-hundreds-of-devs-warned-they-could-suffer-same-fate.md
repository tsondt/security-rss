Title: China reveals audit of 320,000 local apps, with 34 booted from app stores and hundreds of devs warned they could suffer same fate
Date: 2020-10-23T04:27:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: china-reveals-audit-of-320000-local-apps-with-34-booted-from-app-stores-and-hundreds-of-devs-warned-they-could-suffer-same-fate

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/23/china_app_privacy_review/){:target="_blank" rel="noopener"}

> Privacy crackdown in the land of ubiquitous surveillance, where 5G now blankets all cities Through most of 2020 bans on Chinese apps have meant geopolitical strife, but China yesterday revealed it has started banning some of its own apps.... [...]
