Title: Darkside ransomware donates $20K of extortion money to charities
Date: 2020-10-20T11:48:41
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: darkside-ransomware-donates-20k-of-extortion-money-to-charities

[Source](https://www.bleepingcomputer.com/news/security/darkside-ransomware-donates-20k-of-extortion-money-to-charities/){:target="_blank" rel="noopener"}

> The operators of Darkside ransomware have donated some of the money they made extorting victims to non-profits Children International and The Water Project. [...]
