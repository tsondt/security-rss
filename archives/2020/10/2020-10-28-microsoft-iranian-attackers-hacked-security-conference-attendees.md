Title: Microsoft: Iranian attackers hacked security conference attendees
Date: 2020-10-28T12:34:34
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-iranian-attackers-hacked-security-conference-attendees

[Source](https://www.bleepingcomputer.com/news/security/microsoft-iranian-attackers-hacked-security-conference-attendees/){:target="_blank" rel="noopener"}

> Microsoft disclosed today that Iranian state-sponsored hackers successfully hacked into the email accounts of multiple high-profile individuals and potential attendees at this year's Munich Security Conference and the Think 20 (T20) summit. [...]
