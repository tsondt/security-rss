Title: 'You've got the old cheeky Corona': Ireland's pandemic advice SMS service can be spoofed, warns researcher
Date: 2020-10-12T16:21:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: youve-got-the-old-cheeky-corona-irelands-pandemic-advice-sms-service-can-be-spoofed-warns-researcher

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/12/ireland_covid_advice_sms_spoofable/){:target="_blank" rel="noopener"}

> Get cell broadcast tech in, urges onetime Lulzsec white hat Ireland's efforts to keep residents informed about coronavirus has fallen foul of the same basic SMS vulnerability that one of their British neighbours experienced back in March.... [...]
