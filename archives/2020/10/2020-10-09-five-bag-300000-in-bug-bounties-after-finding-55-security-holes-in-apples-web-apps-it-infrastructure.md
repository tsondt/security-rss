Title: Five bag $300,000 in bug bounties after finding 55 security holes in Apple's web apps, IT infrastructure
Date: 2020-10-09T23:19:56+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: five-bag-300000-in-bug-bounties-after-finding-55-security-holes-in-apples-web-apps-it-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/09/apple_bug_bounty_vulnerabilities/){:target="_blank" rel="noopener"}

> Unpatched Cisco VPN servers, access to the iOS source code, AWS secret keys – this is weapons grade 'oof' A team of vulnerability spotters have netted themselves a six-figure payout from Apple after discovering dozens security holes in the Cupertino giant's computer systems, some of which could have been exploited to steal iOS source code, and more.... [...]
