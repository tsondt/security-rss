Title: TrickBot botnet targeted in takedown operations, little impact seen
Date: 2020-10-12T07:00:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: trickbot-botnet-targeted-in-takedown-operations-little-impact-seen

[Source](https://www.bleepingcomputer.com/news/security/trickbot-botnet-targeted-in-takedown-operations-little-impact-seen/){:target="_blank" rel="noopener"}

> TrickBot, one of the most active botnets on the planet, recently has suffered some strong blows from actors in the cybersecurity industry aiming at disrupting its operations. [...]
