Title: After first floating $20bn penalty, DoJ suggests $60m fine for UMC's theft of Micron’s DRAM secrets
Date: 2020-10-23T02:58:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: after-first-floating-20bn-penalty-doj-suggests-60m-fine-for-umcs-theft-of-microns-dram-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/23/doj_decides_60m_is_enough/){:target="_blank" rel="noopener"}

> Taiwanese chipmaker promises ‘substantial assistance’ in ongoing China IP theft action Taiwanese chip-maker United Microelectronics Corporation (UMC) will plead guilty to theft of trade secrets from Micron Technologies and pay a $60m fine to the USA.... [...]
