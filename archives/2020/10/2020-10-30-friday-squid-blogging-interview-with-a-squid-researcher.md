Title: Friday Squid Blogging: Interview with a Squid Researcher
Date: 2020-10-30T21:07:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: friday-squid-blogging-interview-with-a-squid-researcher

[Source](https://www.schneier.com/blog/archives/2020/10/friday-squid-blogging-interview-with-a-squid-researcher.html){:target="_blank" rel="noopener"}

> Interview with Mike Vecchione, Curator of Cephalopoda — now that’s a job title — at the Smithsonian Museum of National History. One reason they’re so interesting is they are intelligent invertebrates. Almost everything that we think of as being intelligent — parrots, dolphins, etc. — are vertebrates, so their brains are built on the same basic structure. Whereas cephalopod brains have evolved from a ring of nerves around the esophagus. It’s a form of intelligence that’s completely independent from ours. As usual, you can also use this squid post to talk about the security stories in the news that I [...]
