Title: Zoom rolls out end-to-end encryption (E2EE) next week
Date: 2020-10-14T14:10:17
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: zoom-rolls-out-end-to-end-encryption-e2ee-next-week

[Source](https://www.bleepingcomputer.com/news/security/zoom-rolls-out-end-to-end-encryption-e2ee-next-week/){:target="_blank" rel="noopener"}

> Zoom estimates that the next E2EE rollout phase will start in 2021, adding Single sign-on (SSO) integration and better identity management [...]
