Title: Friday Squid Blogging: Chinese Squid Fishing Near the Galapagos
Date: 2020-10-16T21:03:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: China;squid
Slug: friday-squid-blogging-chinese-squid-fishing-near-the-galapagos

[Source](https://www.schneier.com/blog/archives/2020/10/friday-squid-blogging-chinese-squid-fishing-near-the-galapagos.html){:target="_blank" rel="noopener"}

> The Chinese have been illegally squid fishing near the Galapagos Islands. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
