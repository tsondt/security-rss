Title: October Patch Tuesday: Microsoft Patches Critical, Wormable RCE Bug
Date: 2020-10-13T20:44:01+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security;critical;CVE-2020-16898;Microsoft;october 2020;patch tuesday;Patches;publicly disclosed;remote code execution;router advertisements;security bug;Security Vulnerabilities;tcp/ip;unpatched bugs;wormable
Slug: october-patch-tuesday-microsoft-patches-critical-wormable-rce-bug

[Source](https://threatpost.com/october-patch-tuesday-wormable-bug/160044/){:target="_blank" rel="noopener"}

> There were 11 critical bugs and six that were unpatched but publicly known in this month's regularly scheduled Microsoft updates. [...]
