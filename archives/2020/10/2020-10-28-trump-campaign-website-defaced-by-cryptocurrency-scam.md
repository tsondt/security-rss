Title: Trump Campaign Website Defaced by Cryptocurrency Scam
Date: 2020-10-28T11:32:21+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cryptography;Government;Hacks;Malware;2020 election;coronavirus;Cryptocurrency;hack;Hackers;Hacking;Monero;President Trump;threat actors;Trump campaign;twitter;voter fraud;voting
Slug: trump-campaign-website-defaced-by-cryptocurrency-scam

[Source](https://threatpost.com/trump-website-defaced/160634/){:target="_blank" rel="noopener"}

> Hackers claim to have access to classified information linking the president to the origin of the coronavirus and criminal collusion with foreign actors. [...]
