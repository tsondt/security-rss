Title: WastedLocker ransomware hits Boyne Resorts ski resort operator
Date: 2020-10-23T16:14:53
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: wastedlocker-ransomware-hits-boyne-resorts-ski-resort-operator

[Source](https://www.bleepingcomputer.com/news/security/wastedlocker-ransomware-hits-boyne-resorts-ski-resort-operator/){:target="_blank" rel="noopener"}

> US-based ski and golf resort operator Boyne Resorts has suffered a cyberattack by the WastedLocker operation that has impacted company-wide reservation systems. [...]
