Title: Softly-as-a-service: IBM whispers plan for security SaaS based on a Cloud Pak
Date: 2020-10-14T08:03:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: softly-as-a-service-ibm-whispers-plan-for-security-saas-based-on-a-cloud-pak

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/14/ibm_security_saas/){:target="_blank" rel="noopener"}

> Appears to cook a new way to shift containerised wares and get you onto OpenShift IBM has quietly announced a plan to turn one of its security software bundles into a software-as-a-service.... [...]
