Title: COVID-19 and Acedia
Date: 2020-10-02T19:15:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: COVID-19;essays
Slug: covid-19-and-acedia

[Source](https://www.schneier.com/blog/archives/2020/10/covid-19-and-acedia.html){:target="_blank" rel="noopener"}

> Note: This isn’t my usual essay topic. Still, I want to put it on my blog. Six months into the pandemic with no end in sight, many of us have been feeling a sense of unease that goes beyond anxiety or distress. It’s a nameless feeling that somehow makes it hard to go on with even the nice things we regularly do. What’s blocking our everyday routines is not the anxiety of lockdown adjustments, or the worries about ourselves and our loved ones — real though those worries are. It isn’t even the sense that, if we’re really honest with [...]
