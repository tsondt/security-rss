Title: Phishing in the Amazon: Internet shoppers urged to look out for Prime Day scams
Date: 2020-10-13T14:24:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: phishing-in-the-amazon-internet-shoppers-urged-to-look-out-for-prime-day-scams

[Source](https://portswigger.net/daily-swig/phishing-in-the-amazon-internet-shoppers-urged-to-look-out-for-prime-day-scams){:target="_blank" rel="noopener"}

> Research details how cybercriminals are targeting retail giants’ customers [...]
