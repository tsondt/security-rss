Title: Pharma Giant Pfizer Leaks Customer Prescription Info, Call Transcripts
Date: 2020-10-20T16:20:16+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Privacy;advil;call transcripts;cancer drugs;chantix;Customer Data;Customer Records;data breach;data leak;drug company;exposure;google cloud;lyrica;misconfiguration;open database;patient information;Pfizer;pharmaceuticals;premarin;prescriptions;Viagra;vpnMentor
Slug: pharma-giant-pfizer-leaks-customer-prescription-info-call-transcripts

[Source](https://threatpost.com/pharma-pfizer-leaks-prescription-call-transcripts/160354/){:target="_blank" rel="noopener"}

> Hundreds of medical patients taking cancer drugs, Premarin, Lyrica and more are now vulnerable to phishing, malware and identity fraud. [...]
