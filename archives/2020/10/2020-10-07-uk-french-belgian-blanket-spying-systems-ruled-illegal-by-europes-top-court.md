Title: UK, French, Belgian blanket spying systems ruled illegal by Europe’s top court
Date: 2020-10-07T06:54:06+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: uk-french-belgian-blanket-spying-systems-ruled-illegal-by-europes-top-court

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/07/eu_privacy_ruling/){:target="_blank" rel="noopener"}

> Five-year legal battle pays off. Now countries have to figure out what to do Analysis Mass surveillance programs run by the UK, French and Belgian governments are illegal, Europe’s top court has decided in a huge win for privacy advocates.... [...]
