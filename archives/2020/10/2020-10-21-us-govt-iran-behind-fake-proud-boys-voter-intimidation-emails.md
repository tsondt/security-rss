Title: US govt: Iran behind fake Proud Boys voter intimidation emails
Date: 2020-10-21T20:41:08
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Government
Slug: us-govt-iran-behind-fake-proud-boys-voter-intimidation-emails

[Source](https://www.bleepingcomputer.com/news/government/us-govt-iran-behind-fake-proud-boys-voter-intimidation-emails/){:target="_blank" rel="noopener"}

> The US govt has stated that Iran is behind threatening emails sent to Democratic voters warning that they must vote for Trump or face consequences. [...]
