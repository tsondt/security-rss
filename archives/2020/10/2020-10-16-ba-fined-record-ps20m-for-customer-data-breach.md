Title: BA fined record £20m for customer data breach
Date: 2020-10-16T11:02:05+00:00
Author: Gwyn Topham Transport correspondent
Category: The Guardian
Tags: British Airways;Hacking;Business;Data and computer security;Airline industry;Technology;Travel & leisure;UK news
Slug: ba-fined-record-ps20m-for-customer-data-breach

[Source](https://www.theguardian.com/business/2020/oct/16/ba-fined-record-20m-for-customer-data-breach){:target="_blank" rel="noopener"}

> Personal details of more than 400,000 customers accessed by hackers in 2018 British Airways has been fined a record £20m for a data breach in which more than 400,000 customers’ personal details were compromised by hackers in 2018. The fine is the biggest ever issued by the Information Commissioner’s Office (ICO), but a fraction of the £183m fine initially announced last year. This was reduced after investigators accepted BA’s representations about the circumstances of the attack; and was reduced further to take into account the dire financial position of BA since the onset of Covid-19. Continue reading... [...]
