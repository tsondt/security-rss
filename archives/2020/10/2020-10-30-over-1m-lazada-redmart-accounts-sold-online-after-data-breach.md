Title: Over 1M Lazada RedMart accounts sold online after data breach
Date: 2020-10-30T12:54:59
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: over-1m-lazada-redmart-accounts-sold-online-after-data-breach

[Source](https://www.bleepingcomputer.com/news/security/over-1m-lazada-redmart-accounts-sold-online-after-data-breach/){:target="_blank" rel="noopener"}

> Singapore's largest online grocery store Lazada Redmart has suffered a data breach after 1.1 million user accounts were put up for sale on a hacker forum. [...]
