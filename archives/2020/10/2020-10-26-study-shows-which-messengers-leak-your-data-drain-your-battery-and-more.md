Title: Study shows which messengers leak your data, drain your battery, and more
Date: 2020-10-26T21:31:52+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Facebook;Instagram;instant message;MESSENGER;privacy
Slug: study-shows-which-messengers-leak-your-data-drain-your-battery-and-more

[Source](https://arstechnica.com/?p=1717263){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Link previews are a ubiquitous feature found in just about every chat and messaging app, and with good reason. They make online conversations easier by providing images and text associated with the file that’s being linked. Unfortunately, they can also leak our sensitive data, consume our limited bandwidth, drain our batteries, and, in one case, expose links in chats that are supposed to be end-to-end encrypted. Among the worst offenders, according to research published on Monday, were messengers from Facebook, Instagram, LinkedIn, and Line. More about that shortly. First a brief discussion of previews. When [...]
