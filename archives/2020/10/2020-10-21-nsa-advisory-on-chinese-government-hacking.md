Title: NSA Advisory on Chinese Government Hacking
Date: 2020-10-21T14:21:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: China;hacking;NSA;vulnerabilities
Slug: nsa-advisory-on-chinese-government-hacking

[Source](https://www.schneier.com/blog/archives/2020/10/nsa-advisory-on-chinese-government-hacking.html){:target="_blank" rel="noopener"}

> The NSA released an advisory listing the top twenty-five known vulnerabilities currently being exploited by Chinese nation-state attackers. This advisory provides Common Vulnerabilities and Exposures (CVEs) known to be recently leveraged, or scanned-for, by Chinese state-sponsored cyber actors to enable successful hacking operations against a multitude of victim networks. Most of the vulnerabilities listed below can be exploited to gain initial access to victim networks using products that are directly accessible from the Internet and act as gateways to internal networks. The majority of the products are either for remote access (T1133) or for external web services (T1190), and should [...]
