Title: Microsoft now lets you disable insecure JScript in Internet Explorer
Date: 2020-10-15T16:40:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-now-lets-you-disable-insecure-jscript-in-internet-explorer

[Source](https://www.bleepingcomputer.com/news/security/microsoft-now-lets-you-disable-insecure-jscript-in-internet-explorer/){:target="_blank" rel="noopener"}

> Microsoft says that customers can now disable JScript (JScript.dll) execution in Internet Explorer 11 after installing the Windows October 2020 monthly security updates. [...]
