Title: Tokyo Stock Exchange breaks new record. Sadly, not a good one... its longest ever outage
Date: 2020-10-02T02:14:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: tokyo-stock-exchange-breaks-new-record-sadly-not-a-good-one-its-longest-ever-outage

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/02/tokyo_stock_exchange_outage/){:target="_blank" rel="noopener"}

> Fujtisu kit on the floor tho bourse takes the blame for day-long dead zone Tokyo’s Stock Exchange (TSE) went offline for most of Thursday, its longest-ever outage and a very unwelcome one as it is the world’s third-largest bourse, when measured by market capitalisation.... [...]
