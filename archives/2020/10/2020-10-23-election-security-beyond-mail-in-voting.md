Title: Election Security: Beyond Mail-In Voting
Date: 2020-10-23T19:10:01+00:00
Author: Joseph Carson
Category: Threatpost
Tags: Government;Hacks;InfoSec Insider;Vulnerabilities;Web Security;election results;election security;Hackers;infosec insider;infrastructure;joseph carson;mail in voting;security risks;Thycotic
Slug: election-security-beyond-mail-in-voting

[Source](https://threatpost.com/election-security-mail-in-voting/160502/){:target="_blank" rel="noopener"}

> There are many areas of the election process that criminal hackers can target to influence election results. [...]
