Title: Improving security, compliance, and governance with cloud-based DLP data discovery
Date: 2020-10-20T16:00:00+00:00
Author: Scott Ellis
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: improving-security-compliance-and-governance-with-cloud-based-dlp-data-discovery

[Source](https://cloud.google.com/blog/products/identity-security/modern-dlp-for-cloud-data-discovery/){:target="_blank" rel="noopener"}

> One of the more critical, but sometimes forgotten, questions related to data security is how do you find the data you need to secure. To security newcomers, this may sound contradictory; surely you have that valuable data firmly in your hands? In reality, many types of sensitive, personal and even regulated data get “misplaced” at some organizations. For example, cases where payment data—credit card numbers, in particular—is found outside of the formally defined Cardholder Data Environment (CDE) have been strikingly common over many years. Sadly, they often come to light during a post-breach investigation or, somewhat better, during a PCI [...]
