Title: COVID-19 Vaccine-Maker Hit with Cyberattack, Data Breach
Date: 2020-10-23T17:04:12+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Government;Hacks;Mobile Security;clinical trials;coronavirus;COVID-19;cyberattack;data breach;data center;dr. reddy's;drugmaker;espionage;Pandemic;phase 2;plants closed;Research;russia;sputnik V;vaccine;vaccine development
Slug: covid-19-vaccine-maker-hit-with-cyberattack-data-breach

[Source](https://threatpost.com/covid-19-vaccine-cyberattack-data-breach/160495/){:target="_blank" rel="noopener"}

> Dr. Reddy's, the contractor for Russia’s “Sputinik V” COVID-19 vaccine and a major generics producer, has had to close plants and isolate its data centers. [...]
