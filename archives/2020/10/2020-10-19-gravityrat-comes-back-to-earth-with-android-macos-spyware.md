Title: GravityRAT Comes Back to Earth with Android, macOS Spyware
Date: 2020-10-19T17:34:37+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Web Security;Android;Facebook messages;gravity rat;India;Kaspersky;macOS;malicious apps;Malware analysis;multiplatform;remote access;social media;Spyware;travel application;Trojan
Slug: gravityrat-comes-back-to-earth-with-android-macos-spyware

[Source](https://threatpost.com/gravityrat-back-android-macos-spyware/160299/){:target="_blank" rel="noopener"}

> The espionage tool masquerades as legitimate applications and robs victims blind of their data. [...]
