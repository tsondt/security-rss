Title: Upcoming Speaking Engagements
Date: 2020-10-14T17:15:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Schneier news
Slug: upcoming-speaking-engagements-2

[Source](https://www.schneier.com/blog/archives/2020/10/upcoming-speaking-engagements-2.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’ll be speaking at Cyber Week Online, October 19-21, 2020. I’ll be speaking at the IEEE Symposium on Technology and Society virtual conference, November 12-15, 2020. I’ll be keynoting the 2020 Conference on Cyber Norms on November 12, 2020. I’m speaking at the (ISC)2 Security Congress 2020, November 16, 2020. I’ll be on a panel at the OECD Global Blockchain Policy Forum 2020 on November 17, 2020. I’ll be keynoting the HITB CyberWeek Virtual Edition on November 18, 2020. I’ll be speaking at an Informa event [...]
