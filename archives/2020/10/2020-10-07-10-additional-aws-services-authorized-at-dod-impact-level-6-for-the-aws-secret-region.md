Title: 10 additional AWS services authorized at DoD Impact Level 6 for the AWS Secret Region
Date: 2020-10-07T17:42:06+00:00
Author: Tyler Harding
Category: AWS Security
Tags: Announcements;Defense;Foundational (100);Security, Identity, & Compliance;Department of Defense (DoD);DoD;Impact Level 6;Security Blog
Slug: 10-additional-aws-services-authorized-at-dod-impact-level-6-for-the-aws-secret-region

[Source](https://aws.amazon.com/blogs/security/10-additional-aws-services-authorized-dod-impact-level-6-for-aws-secret-region/){:target="_blank" rel="noopener"}

> The Defense Information Systems Agency (DISA) has authorized 10 additional AWS services in the AWS Secret Region for production workloads at the Department of Defense (DoD) Impact Level (IL) 6 under the DoD’s Cloud Computing Security Requirements Guide (DoD CC SRG). With this authorization at DoD IL 6, DoD Mission Owners can process classified and [...]
