Title: Barnes & Noble hit by cyberattack that exposed customer data
Date: 2020-10-14T23:25:13
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: barnes-noble-hit-by-cyberattack-that-exposed-customer-data

[Source](https://www.bleepingcomputer.com/news/security/barnes-and-noble-hit-by-cyberattack-that-exposed-customer-data/){:target="_blank" rel="noopener"}

> ​U.S. Bookstore giant Barnes & Noble has disclosed that they were victims of a cyberattack that may have exposed customers' data. [...]
