Title: DOMPurify mutation XSS bypass achieved through MathML namespace confusion
Date: 2020-10-08T12:03:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: dompurify-mutation-xss-bypass-achieved-through-mathml-namespace-confusion

[Source](https://portswigger.net/daily-swig/dompurify-mutation-xss-bypass-achieved-through-mathml-namespace-confusion){:target="_blank" rel="noopener"}

> Security flaw introduced through the serialization of foreign content [...]
