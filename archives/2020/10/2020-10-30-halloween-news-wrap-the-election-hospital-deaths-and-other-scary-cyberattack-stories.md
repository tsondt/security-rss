Title: Halloween News Wrap: The Election, Hospital Deaths and Other Scary Cyberattack Stories
Date: 2020-10-30T18:41:26+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;Malware;Podcasts;Vulnerabilities;Web Security;CISA;COVID-19;cyberattack Trump;election security;elections security;FBI;hospital ransomware;hospital security;Microsoft;patch;patient death;patient safety;ransomware;ryuk;smbghost;U.S. elections;Windows;windows flaw;zerologon
Slug: halloween-news-wrap-the-election-hospital-deaths-and-other-scary-cyberattack-stories

[Source](https://threatpost.com/halloween-election-hospital-death-cyberattacks/160781/){:target="_blank" rel="noopener"}

> Threatpost breaks down the scariest stories of the week ended Oct. 30 haunting the security industry -- including bugs that just won't die. [...]
