Title: Patch Tuesday: Microsoft remedies critical TCP/IP remote code execution bug
Date: 2020-10-14T11:50:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: patch-tuesday-microsoft-remedies-critical-tcpip-remote-code-execution-bug

[Source](https://portswigger.net/daily-swig/patch-tuesday-microsoft-remedies-critical-tcp-ip-remote-code-execution-bug){:target="_blank" rel="noopener"}

> Packet pwnage peril [...]
