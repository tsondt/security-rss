Title: US Cyber Command and Microsoft Are Both Disrupting TrickBot
Date: 2020-10-15T11:01:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: botnets;Microsoft;national security policy
Slug: us-cyber-command-and-microsoft-are-both-disrupting-trickbot

[Source](https://www.schneier.com/blog/archives/2020/10/us-cyber-command-and-microsoft-are-both-disrupting-trickbot.html){:target="_blank" rel="noopener"}

> Earlier this month, we learned that someone is disrupting the TrickBot botnet network. Over the past 10 days, someone has been launching a series of coordinated attacks designed to disrupt Trickbot, an enormous collection of more than two million malware-infected Windows PCs that are constantly being harvested for financial data and are often used as the entry point for deploying ransomware within compromised organizations. On Sept. 22, someone pushed out a new configuration file to Windows computers currently infected with Trickbot. The crooks running the Trickbot botnet typically use these config files to pass new instructions to their fleet of [...]
