Title: You’ve open sourced your relational database manager with PostgreSQL – but how can you keep it secure?
Date: 2020-10-20T06:00:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: youve-open-sourced-your-relational-database-manager-with-postgresql-but-how-can-you-keep-it-secure

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/20/postgresql/){:target="_blank" rel="noopener"}

> We'll help you manage risk while chasing your RDBMS dreams Webcast There was a time when open source was still – no matter how many decades it had driven software projects – regarded as the playground of hippies and utopians. Bold and brave, yet thrown together, inconsistent and unsecured when compared to more established products.... [...]
