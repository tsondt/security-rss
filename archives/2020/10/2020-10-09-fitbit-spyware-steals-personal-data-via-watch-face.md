Title: Fitbit Spyware Steals Personal Data via Watch Face
Date: 2020-10-09T18:58:00+00:00
Author: Becky Bracken
Category: Threatpost
Tags: IoT;Malware;Mobile Security;Privacy;Vulnerabilities;API;app;Application;breen;Connected Device;data theft;Fitbit;Fitbit Gallery;immersive;malicious watch face;malware;privacy controls;Spyware
Slug: fitbit-spyware-steals-personal-data-via-watch-face

[Source](https://threatpost.com/fitbit-personal-data-watch-face/160003/){:target="_blank" rel="noopener"}

> Immersive Labs Researcher takes advantage of lax Fitbit privacy controls to build a malicious spyware watch face. [...]
