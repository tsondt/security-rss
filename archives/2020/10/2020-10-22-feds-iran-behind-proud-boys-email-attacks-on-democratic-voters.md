Title: Feds: Iran Behind ‘Proud Boys’ Email Attacks on Democratic Voters
Date: 2020-10-22T13:43:19+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Web Security;democratic party;Director of National Intelligence;E-mail;Election;FBI;Hackers;Irán;John Radcliffe;President Trump;Proofpoint;Proud Boys;russia;voters
Slug: feds-iran-behind-proud-boys-email-attacks-on-democratic-voters

[Source](https://threatpost.com/feds-iran-proud-boy-email-attacks-democratic-voters/160429/){:target="_blank" rel="noopener"}

> Messages that threaten people to ‘vote for Trump or else’ are part of foreign adversaries’ attempts to interfere with the Nov. 3 election, according to feds. [...]
