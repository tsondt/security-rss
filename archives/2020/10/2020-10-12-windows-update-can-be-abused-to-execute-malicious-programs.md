Title: Windows Update can be abused to execute malicious programs
Date: 2020-10-12T18:02:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: windows-update-can-be-abused-to-execute-malicious-programs

[Source](https://www.bleepingcomputer.com/news/security/windows-update-can-be-abused-to-execute-malicious-programs/){:target="_blank" rel="noopener"}

> The Windows Update client has just been added to the list of living-off-the-land binaries (LoLBins) attackers can use to execute malicious code on Windows systems. [...]
