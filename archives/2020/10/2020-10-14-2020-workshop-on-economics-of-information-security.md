Title: 2020 Workshop on Economics of Information Security
Date: 2020-10-14T11:09:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: conferences;economics of security
Slug: 2020-workshop-on-economics-of-information-security

[Source](https://www.schneier.com/blog/archives/2020/10/2020-workshop-on-economics-of-information-security.html){:target="_blank" rel="noopener"}

> The Workshop on Economics of Information Security will be online this year. Register here. [...]
