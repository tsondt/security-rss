Title: Software AG Data Released After Clop Ransomware Strike – Report
Date: 2020-10-13T18:57:16+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;IoT;Malware;Web Security;$23 million;Clop;cybercriminal;data breach;Data security;double extortion;Germany;malware;Malware analysis;ransom;ransomware;software ag
Slug: software-ag-data-released-after-clop-ransomware-strike-report

[Source](https://threatpost.com/software-ag-data-clop-ransomware/160042/){:target="_blank" rel="noopener"}

> The Clop group attacked Software AG, a German conglomerate with operations in more than 70 countries, threatening to dump stolen data if the whopping $23 million ransom isn’t paid. [...]
