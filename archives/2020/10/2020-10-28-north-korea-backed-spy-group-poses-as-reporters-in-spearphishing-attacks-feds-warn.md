Title: North Korea-Backed Spy Group Poses as Reporters in Spearphishing Attacks, Feds Warn
Date: 2020-10-28T12:32:26+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware;Web Security;active campaigns;alert;analysis;babyshark;CISA;espionage;Hidden Cobra;Kimsuky;malware;North Korea;reporters;South Korea;Spyware
Slug: north-korea-backed-spy-group-poses-as-reporters-in-spearphishing-attacks-feds-warn

[Source](https://threatpost.com/north-korea-spy-reporters-feds-warn/160622/){:target="_blank" rel="noopener"}

> The Kimsuky/Hidden Cobra APT is going after the commercial sector, according to CISA. [...]
