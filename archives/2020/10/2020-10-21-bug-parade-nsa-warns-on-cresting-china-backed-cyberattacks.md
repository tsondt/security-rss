Title: Bug Parade: NSA Warns on Cresting China-Backed Cyberattacks
Date: 2020-10-21T20:31:17+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Vulnerabilities;advanced persistent threats;apt;bluekeep;bug parade;Bugs;Cactus Pete;China;china backed;chinese;CVE-2019-11510;Cyberattacks;Exchange;f5;Microsoft Windows;most exploited;nation state;NSA;Patches;pulse vpn;Security Vulnerabilities;TA413;top 25;top exploits;vicious panda;winniti;zerologon
Slug: bug-parade-nsa-warns-on-cresting-china-backed-cyberattacks

[Source](https://threatpost.com/bug-nsa-china-backed-cyberattacks/160421/){:target="_blank" rel="noopener"}

> The Feds have published a Top 25 exploits list, rife with big names like BlueKeep, Zerologon and other notorious security vulnerabilities. [...]
