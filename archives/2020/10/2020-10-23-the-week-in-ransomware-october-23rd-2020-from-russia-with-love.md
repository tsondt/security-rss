Title: The Week in Ransomware - October 23rd 2020 - From Russia with Love
Date: 2020-10-23T17:38:45-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-october-23rd-2020-from-russia-with-love

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-23rd-2020-from-russia-with-love/){:target="_blank" rel="noopener"}

> This week has been busy with ransomware related news, including new charges against Russian state-sponsored hackers and numerous attacks against well-known organizations. [...]
