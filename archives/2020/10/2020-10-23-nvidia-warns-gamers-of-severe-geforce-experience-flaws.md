Title: Nvidia Warns Gamers of Severe GeForce Experience Flaws
Date: 2020-10-23T14:09:28+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security;Bug;code execution;CVE-2020-5977;CVE-2020-5990;Denial of Service;DoS;enthusiast gaming;gamer;geforce experience;GeForce GTX graphics card;GPU;graphics driver;high severity flaw;Nvidia;Nvidia GeForce 3.20.5.70;security vulnerability;Windows
Slug: nvidia-warns-gamers-of-severe-geforce-experience-flaws

[Source](https://threatpost.com/nvidia-gamers-geforce-experience-flaws/160487/){:target="_blank" rel="noopener"}

> Versions of Nvidia GeForce Experience for Windows prior to 3.20.5.70 are affected by a high-severity bug that could enable code execution, denial of service and more. [...]
