Title: Amid an Embarrassment of Riches, Ransom Gangs Increasingly Outsource Their Work
Date: 2020-10-08T19:42:04+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;domaintools;Dr. Samuil;Intel 471;Mark Arena;Sergey Rakityansky
Slug: amid-an-embarrassment-of-riches-ransom-gangs-increasingly-outsource-their-work

[Source](https://krebsonsecurity.com/2020/10/amid-an-embarrassment-of-riches-ransom-gangs-increasingly-outsource-their-work/){:target="_blank" rel="noopener"}

> There’s an old adage in information security: “Every company gets penetration tested, whether or not they pay someone for the pleasure.” Many organizations that do hire professionals to test their network security posture unfortunately tend to focus on fixing vulnerabilities hackers could use to break in. But judging from the proliferation of help-wanted ads for offensive pentesters in the cybercrime underground, today’s attackers have exactly zero trouble gaining that initial intrusion: The real challenge seems to be hiring enough people to help everyone profit from the access already gained. One of the most common ways such access is monetized these [...]
