Title: Lightning Network discloses "concerning" crypto vulnerabilities
Date: 2020-10-21T08:39:40
Author: Ax Sharma
Category: BleepingComputer
Tags: CryptoCurrency
Slug: lightning-network-discloses-concerning-crypto-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/lightning-network-discloses-concerning-crypto-vulnerabilities/){:target="_blank" rel="noopener"}

> The team behind Lightning Network have disclosed full details on multiple vulnerabilities that had been partially disclosed on October 9th, 2020. Attackers could have exploited these vulnerabilities to cause DoS and to disrupt crypto transactions by intercepting "smart contracts" made between two parties. [...]
