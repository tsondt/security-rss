Title: Steelcase furniture giant hit by Ryuk ransomware attack
Date: 2020-10-27T12:30:42
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: steelcase-furniture-giant-hit-by-ryuk-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/steelcase-furniture-giant-hit-by-ryuk-ransomware-attack/){:target="_blank" rel="noopener"}

> Office furniture giant Steelcase has suffered a ransomware attack that forced them to shut down their network to contain the attack's spread. [...]
