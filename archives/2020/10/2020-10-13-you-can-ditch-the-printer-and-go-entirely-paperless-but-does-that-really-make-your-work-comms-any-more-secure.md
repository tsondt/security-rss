Title: You can ditch the printer and go entirely paperless, but does that really make your work comms any more secure?
Date: 2020-10-13T16:00:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: you-can-ditch-the-printer-and-go-entirely-paperless-but-does-that-really-make-your-work-comms-any-more-secure

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/13/paperless_secure_comms_regcast/){:target="_blank" rel="noopener"}

> Tune in and learn how to reinvigorate your digital transformation Webcast It’s easy to say you want to kick off a digital transformation. It’s often harder to say what this should look like in practice, or what the benefits for your organisation will be. Just junking the printer and saying “we’re paperless now” isn’t a strategy, and certainly isn’t a plan. But moving to secure paperless communications is certainly a start.... [...]
