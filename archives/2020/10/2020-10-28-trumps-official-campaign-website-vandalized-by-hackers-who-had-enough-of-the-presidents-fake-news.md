Title: Trump's official campaign website vandalized by hackers who 'had enough of the President's fake news'
Date: 2020-10-28T02:29:06+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: trumps-official-campaign-website-vandalized-by-hackers-who-had-enough-of-the-presidents-fake-news

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/28/trump_website_hacked/){:target="_blank" rel="noopener"}

> Well, that narrows down the list of suspects to just a few billion people Donald Trump's presidential campaign website was briefly hacked and defaced tonight.... [...]
