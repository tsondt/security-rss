Title: US Cyber Command: Patch Windows 'Bad Neighbor' TCP/IP bug now
Date: 2020-10-14T10:42:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: us-cyber-command-patch-windows-bad-neighbor-tcpip-bug-now

[Source](https://www.bleepingcomputer.com/news/security/us-cyber-command-patch-windows-bad-neighbor-tcp-ip-bug-now/){:target="_blank" rel="noopener"}

> US Cyber Command warns Microsoft customers to patch their systems immediately against the critical and remotely exploitable CVE-2020-16898 vulnerability addressed during this month's Patch Tuesday. [...]
