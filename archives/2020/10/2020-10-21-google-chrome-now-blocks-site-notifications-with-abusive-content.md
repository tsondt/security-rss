Title: Google Chrome now blocks site notifications with abusive content
Date: 2020-10-21T15:07:16
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-chrome-now-blocks-site-notifications-with-abusive-content

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-now-blocks-site-notifications-with-abusive-content/){:target="_blank" rel="noopener"}

> Starting with Chrome 86, Google is automatically hiding website notification spam on sites showing a pattern of sending abusive notification content to visitors. [...]
