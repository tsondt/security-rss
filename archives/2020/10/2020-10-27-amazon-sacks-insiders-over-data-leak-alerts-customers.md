Title: Amazon sacks insiders over data leak, alerts customers
Date: 2020-10-27T04:33:58
Author: Ax Sharma
Category: BleepingComputer
Tags: Legal
Slug: amazon-sacks-insiders-over-data-leak-alerts-customers

[Source](https://www.bleepingcomputer.com/news/security/amazon-sacks-insiders-over-data-leak-alerts-customers/){:target="_blank" rel="noopener"}

> Amazon has recently dismissed multiple employees for leaking customer data including their email addresses to an unaffiliated third-party. The company has sent out an email announcement to the affected customers following the incident. [...]
