Title: Privacy Badger turns ‘local learning’ off by default following Google security warnings
Date: 2020-10-09T16:26:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: privacy-badger-turns-local-learning-off-by-default-following-google-security-warnings

[Source](https://portswigger.net/daily-swig/privacy-badger-turns-local-learning-off-by-default-following-google-security-warnings){:target="_blank" rel="noopener"}

> Users will now receive automatic updates to the pre-trained list of trackers [...]
