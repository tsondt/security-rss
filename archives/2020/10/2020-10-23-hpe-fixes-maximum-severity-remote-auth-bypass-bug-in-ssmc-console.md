Title: HPE fixes maximum severity remote auth bypass bug in SSMC console
Date: 2020-10-23T18:22:56
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hpe-fixes-maximum-severity-remote-auth-bypass-bug-in-ssmc-console

[Source](https://www.bleepingcomputer.com/news/security/hpe-fixes-maximum-severity-remote-auth-bypass-bug-in-ssmc-console/){:target="_blank" rel="noopener"}

> Hewlett Packard Enterprise (HPE) has fixed a maximum severity remote authentication bypass vulnerability affecting the company's HPE StoreServ Management Console (SSMC) data center storage management solution. [...]
