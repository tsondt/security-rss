Title: FTC receives almost 2 million robocall complaints in nine months
Date: 2020-10-27T19:27:57
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ftc-receives-almost-2-million-robocall-complaints-in-nine-months

[Source](https://www.bleepingcomputer.com/news/security/ftc-receives-almost-2-million-robocall-complaints-in-nine-months/){:target="_blank" rel="noopener"}

> The Federal Trade Commission (FTC) received almost 2 million complaints from Americans regarding illegal robocalls during the first nine months of 2020. [...]
