Title: Big IQ play from IT outsourcer: Can't create batch files if you can't save files. Of any kind
Date: 2020-10-05T07:15:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: big-iq-play-from-it-outsourcer-cant-create-batch-files-if-you-cant-save-files-of-any-kind

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/05/who_me/){:target="_blank" rel="noopener"}

> Be careful what you wish for Who, Me? The end of a damp weekend (for the UK at least) heralds a new instalment in our ongoing series of Register reader confessions. Welcome back to Who, Me?... [...]
