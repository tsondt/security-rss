Title: QBot uses Windows Defender Antivirus lure to infect computers
Date: Mon, 12 Oct 2020 15:50:10 EDT
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: qbot-uses-windows-defender-antivirus-lure-to-infect-computers

[Source](https://www.bleepingcomputer.com/news/security/qbot-uses-windows-defender-antivirus-lure-to-infect-computers/){:target="_blank" rel="noopener"}

> The Qbot botnet uses a new template for the distribution of their malware that uses a fake Windows Defender Antivirus theme to trick you into enabling Excel macros. [...]
