Title: Notpetya, Olympics hacking, Novichok probe meddling... America throws the book at six alleged Kremlin hackers
Date: 2020-10-19T20:47:55+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: notpetya-olympics-hacking-novichok-probe-meddling-america-throws-the-book-at-six-alleged-kremlin-hackers

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/19/russians_charged_olympics/){:target="_blank" rel="noopener"}

> While the UK says Russia probed 2020 Games systems, too Six men have been named as Russian military hackers and accused of spreading malware, disrupting the Olympics in retaliation for Russia's doping ban, and meddling with elections as well as probes into Novichok poisonings.... [...]
