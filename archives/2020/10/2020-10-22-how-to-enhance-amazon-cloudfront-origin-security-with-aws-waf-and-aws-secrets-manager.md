Title: How to enhance Amazon CloudFront origin security with AWS WAF and AWS Secrets Manager
Date: 2020-10-22T16:35:22+00:00
Author: Cameron Worrell
Category: AWS Security
Tags: Amazon CloudFront;AWS Secrets Manager;AWS WAF;Intermediate (200);Security, Identity, & Compliance;Security Blog
Slug: how-to-enhance-amazon-cloudfront-origin-security-with-aws-waf-and-aws-secrets-manager

[Source](https://aws.amazon.com/blogs/security/how-to-enhance-amazon-cloudfront-origin-security-with-aws-waf-and-aws-secrets-manager/){:target="_blank" rel="noopener"}

> Whether your web applications provide static or dynamic content, you can improve their performance, availability, and security by using Amazon CloudFront as your content delivery network (CDN). CloudFront is a web service that speeds up distribution of your web content through a worldwide network of data centers called edge locations. CloudFront ensures that end-user requests [...]
