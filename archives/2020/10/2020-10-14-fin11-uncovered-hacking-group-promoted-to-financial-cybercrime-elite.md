Title: FIN11 uncovered: Hacking group promoted to financial cybercrime elite
Date: 2020-10-14T14:27:35+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: fin11-uncovered-hacking-group-promoted-to-financial-cybercrime-elite

[Source](https://portswigger.net/daily-swig/fin11-uncovered-hacking-group-promoted-to-financial-cybercrime-elite){:target="_blank" rel="noopener"}

> Versatile threat actors are the first cybercrime gang to win the ‘FIN’ designation in three years [...]
