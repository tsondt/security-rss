Title: Android ransomware has picked up some ominous new tricks
Date: 2020-10-10T10:16:36+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Tech;android;malware;ransomware
Slug: android-ransomware-has-picked-up-some-ominous-new-tricks

[Source](https://arstechnica.com/?p=1713163){:target="_blank" rel="noopener"}

> Enlarge (credit: Milana Romazanova | Getty Images ) Though ransomware has been around for years, it poses an ever-increasing threat to hospitals, municipal governments, and basically any institution that can't tolerate downtime. But along with the various types of PC malware that are typically used in these attacks, there's another burgeoning platform for ransomware as well: Android phones. And new research from Microsoft shows that criminal hackers are investing time and resources in refining their mobile ransomware tools—a sign that their attacks are generating payouts. Released on Thursday, the findings, which were detected using Microsoft Defender on mobile, look at [...]
