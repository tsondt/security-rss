Title: Software AG IT giant hit with $23 million ransom by Clop ransomware
Date: 2020-10-09T15:15:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: software-ag-it-giant-hit-with-23-million-ransom-by-clop-ransomware

[Source](https://www.bleepingcomputer.com/news/security/software-ag-it-giant-hit-with-23-million-ransom-by-clop-ransomware/){:target="_blank" rel="noopener"}

> The Clop ransomware gang hit the network of German enterprise software giant Software AG last Saturday, asking for a ransom of $23 million after stealing employee information and company documents. [...]
