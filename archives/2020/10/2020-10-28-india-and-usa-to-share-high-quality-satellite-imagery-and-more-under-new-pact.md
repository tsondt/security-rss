Title: India and USA to share high-quality satellite imagery and more under new pact
Date: 2020-10-28T01:58:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: india-and-usa-to-share-high-quality-satellite-imagery-and-more-under-new-pact

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/28/india_and_usa_to_share/){:target="_blank" rel="noopener"}

> To get a better view of regional hotspots in case things get kinetic The USA and India have struck a new defence pact that will see the two nations share high-quality spatial data and satellite images.... [...]
