Title: Report: UK colleges face testing times with ageing kit, iffy connectivity, and some IT staff supporting 1k+ users
Date: 2020-10-26T14:05:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: report-uk-colleges-face-testing-times-with-ageing-kit-iffy-connectivity-and-some-it-staff-supporting-1k-users

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/26/jisc_research/){:target="_blank" rel="noopener"}

> Cloud-first but no 'resilient internet connection'? Oh dear New research into the IT infrastructure of the UK's higher education sectors does not make happy reading for those wishing to cure all ills with the sticking plaster of "digital".... [...]
