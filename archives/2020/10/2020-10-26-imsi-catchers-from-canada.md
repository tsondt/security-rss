Title: IMSI-Catchers from Canada
Date: 2020-10-26T11:53:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cell phones;law enforcement;privacy;surveillance;tracking
Slug: imsi-catchers-from-canada

[Source](https://www.schneier.com/blog/archives/2020/10/imsi-catchers-from-canada.html){:target="_blank" rel="noopener"}

> Gizmodo is reporting that Harris Corp. is no longer selling Stingray IMSI-catchers (and, presumably, its follow-on models Hailstorm and Crossbow) to local governments: L3Harris Technologies, formerly known as the Harris Corporation, notified police agencies last year that it planned to discontinue sales of its surveillance boxes at the local level, according to government records. Additionally, the company would no longer offer access to software upgrades or replacement parts, effectively slapping an expiration date on boxes currently in use. Any advancements in cellular technology, such as the rollout of 5G networks in most major U.S. cities, would render them obsolete. The [...]
