Title: Spoiling the ballot: Cyber issues cast cloud over US presidential election
Date: 2020-10-29T16:25:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: spoiling-the-ballot-cyber-issues-cast-cloud-over-us-presidential-election

[Source](https://portswigger.net/daily-swig/spoiling-the-ballot-cyber-issues-cast-cloud-over-us-presidential-election){:target="_blank" rel="noopener"}

> Complex voting system is being subjected to the ultimate stress test [...]
