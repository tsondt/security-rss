Title: Authentication Bug Opens Android Smart-TV Box to Data Theft
Date: 2020-10-13T16:36:15+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Vulnerabilities;Web Security;Android Debug Bridge;arbitrary code execution;command line;critical;hindotech;HK1 TV Box;Local Privilege Escalation;root;security vulnerability;serial port;set-top box;sick.codes;Smart TV;UART
Slug: authentication-bug-opens-android-smart-tv-box-to-data-theft

[Source](https://threatpost.com/authentication-bug-android-smart-tv-data-theft/160025/){:target="_blank" rel="noopener"}

> The streaming box allows arbitrary code execution as root, paving the way to pilfering social-media tokens, passwords, messaging history and more. [...]
