Title: Microsoft unveils plans for Project Zero-style Chromium research program
Date: 2020-10-16T13:28:18+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: microsoft-unveils-plans-for-project-zero-style-chromium-research-program

[Source](https://portswigger.net/daily-swig/microsoft-unveils-plans-for-project-zero-style-chromium-research-program){:target="_blank" rel="noopener"}

> Quid pro Chromium [...]
