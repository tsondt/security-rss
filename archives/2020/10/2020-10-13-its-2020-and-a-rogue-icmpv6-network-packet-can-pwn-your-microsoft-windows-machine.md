Title: It's 2020 and a rogue ICMPv6 network packet can pwn your Microsoft Windows machine
Date: 2020-10-13T20:09:52+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: its-2020-and-a-rogue-icmpv6-network-packet-can-pwn-your-microsoft-windows-machine

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/13/microsoft_patch_tuesday/){:target="_blank" rel="noopener"}

> Redmond urges folks to apply update ASAP – plus more fixes for Outlook and software from Adobe, Intel, SAP, Red Hat Patch Tuesday Microsoft's Update Tuesday patch dump for October 2020 has delivered security patches that attempt to address 87 CVEs for a dozen Redmond products.... [...]
