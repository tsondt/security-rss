Title: Microsoft Defender ATP adds vulnerable Windows device tracking
Date: 2020-10-28T16:11:42
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-atp-adds-vulnerable-windows-device-tracking

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-atp-adds-vulnerable-windows-device-tracking/){:target="_blank" rel="noopener"}

> The Microsoft Defender Advanced Threat Protection (ATP) endpoint security platform now provides users with a new report designed to help them keep track of vulnerable Windows and macOS devices within their organization's environment. [...]
