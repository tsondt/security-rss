Title: Cruise operator Carnival suffers data breach – customer information potentially exposed
Date: 2020-10-15T11:05:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cruise-operator-carnival-suffers-data-breach-customer-information-potentially-exposed

[Source](https://portswigger.net/daily-swig/cruise-operator-carnival-suffers-data-breach-customer-information-potentially-exposed){:target="_blank" rel="noopener"}

> Global cruise line and casino brands implicated in cyber-attack [...]
