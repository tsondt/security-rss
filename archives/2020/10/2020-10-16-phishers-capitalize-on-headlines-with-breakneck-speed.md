Title: Phishers Capitalize on Headlines with Breakneck Speed
Date: 2020-10-16T21:20:59+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Most Recent ThreatLists;Web Security;Amazon Prime Day;branding;covid;Fraud;infrastructure analysis;PayPal;Phishing;phishing lures;phishkit;Proofpoint;rebranding;telegram;voter registration;WhatsApp
Slug: phishers-capitalize-on-headlines-with-breakneck-speed

[Source](https://threatpost.com/phishers-capitalize-headlines-speed/160249/){:target="_blank" rel="noopener"}

> Marking a pivot from COVID-19 scams, researchers track a single threat actor through the evolution from the pandemic to PayPal, and on to more timely voter scams -- all with the same infrastructure. [...]
