Title: Data breach at Mississippi ambulance service exposes sensitive information of patients
Date: 2020-10-08T14:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-at-mississippi-ambulance-service-exposes-sensitive-information-of-patients

[Source](https://portswigger.net/daily-swig/data-breach-at-mississippi-ambulance-service-exposes-sensitive-information-of-patients){:target="_blank" rel="noopener"}

> Incident comes following ransomware attack in July [...]
