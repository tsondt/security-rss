Title: Google Safari Workaround case inspires campaign to sue Facebook in UK's High Court over Cambridge Analytica app
Date: 2020-10-29T20:00:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: google-safari-workaround-case-inspires-campaign-to-sue-facebook-in-uks-high-court-over-cambridge-analytica-app

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/29/facebook_you_owe_us_high_court_campaign/){:target="_blank" rel="noopener"}

> 'Facebook You Owe Us' wants to run a not-quite-class-action-style lawsuit A campaign to sue Facebook over lax privacy policies that allowed Cambridge Analytica to slurp almost a million people's personal data from the social networking website hopes to become a representative action in the High Court, its instigators said today.... [...]
