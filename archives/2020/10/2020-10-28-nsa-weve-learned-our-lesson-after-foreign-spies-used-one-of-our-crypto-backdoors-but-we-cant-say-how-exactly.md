Title: NSA: We've learned our lesson after foreign spies used one of our crypto backdoors – but we can't say how exactly
Date: 2020-10-28T23:44:52+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: nsa-weve-learned-our-lesson-after-foreign-spies-used-one-of-our-crypto-backdoors-but-we-cant-say-how-exactly

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/28/nsa_backdoor_wyden/){:target="_blank" rel="noopener"}

> Senator Wyden puts surveillance nerve-center on blast It's said the NSA drew up a report on what it learned after a foreign government exploited a weak encryption scheme, championed by the US spying agency, in Juniper firewall software.... [...]
