Title: IoT Device Takeovers Surge 100 Percent in 2020
Date: 2020-10-23T20:49:03+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;IoT;Vulnerabilities;Web Security;Android;COVID-19;device infection;device takeover;Internet of things;IoT security;malware;mobile networks;nat;network address translation;Nokia;Pandemic;threat intelligence report;Windows
Slug: iot-device-takeovers-surge-100-percent-in-2020

[Source](https://threatpost.com/iot-device-takeovers-surge/160504/){:target="_blank" rel="noopener"}

> The COVID-19 pandemic, coupled with an explosion in the number of connected devices, have led to a swelling in IoT infections observed on wireless networks. [...]
