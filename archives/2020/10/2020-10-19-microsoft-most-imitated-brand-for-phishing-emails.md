Title: Microsoft: Most-Imitated Brand for Phishing Emails
Date: 2020-10-19T10:00:14+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Most Recent ThreatLists;Web Security;amazon;brand impersonation;brand phishing;Check Point;COVID-19;email;Microsoft;Pandemic;Phishing;remote working;the report;third quarter
Slug: microsoft-most-imitated-brand-for-phishing-emails

[Source](https://threatpost.com/microsoft-most-imitated-phishing/160255/){:target="_blank" rel="noopener"}

> The shift to remote working spurred Microsoft and Amazon to the top of the heap for cybercriminals to use as lures in the third quarter. [...]
