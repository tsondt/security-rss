Title: Sam's Club customer accounts hacked in credential stuffing attacks
Date: 2020-10-08T16:49:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: sams-club-customer-accounts-hacked-in-credential-stuffing-attacks

[Source](https://www.bleepingcomputer.com/news/security/sams-club-customer-accounts-hacked-in-credential-stuffing-attacks/){:target="_blank" rel="noopener"}

> Walmart-owned Sam's Club has been emailing customers that may have been victims of credential stuffing and phishing attacks. [...]
