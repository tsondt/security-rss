Title: The NSA is Refusing to Disclose its Policy on Backdooring Commercial Products
Date: 2020-10-28T14:40:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: backdoors;national security policy;NSA;privacy;surveillance;terrorism
Slug: the-nsa-is-refusing-to-disclose-its-policy-on-backdooring-commercial-products

[Source](https://www.schneier.com/blog/archives/2020/10/the-nsa-is-refusing-to-disclose-its-policy-on-backdooring-commercial-products.html){:target="_blank" rel="noopener"}

> Senator Ron Wyden asked, and the NSA didn’t answer : The NSA has long sought agreements with technology companies under which they would build special access for the spy agency into their products, according to disclosures by former NSA contractor Edward Snowden and reporting by Reuters and others. These so-called back doors enable the NSA and other agencies to scan large amounts of traffic without a warrant. Agency advocates say the practice has eased collection of vital intelligence in other countries, including interception of terrorist communications. The agency developed new rules for such practices after the Snowden leaks in order [...]
