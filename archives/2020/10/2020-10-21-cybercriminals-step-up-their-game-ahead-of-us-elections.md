Title: Cybercriminals Step Up Their Game Ahead of U.S. Elections
Date: 2020-10-21T13:48:03+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Hacks;InfoSec Insider;Videos;Cisco Talos;cybercriminals;disinformation campaign;Donald Trump;election infrastructure;election security;Facebook;Hacking;Joe biden;mail in voting;matt olney;November elections;Pandemic;twitter;U.S. Government;US election;video interview
Slug: cybercriminals-step-up-their-game-ahead-of-us-elections

[Source](https://threatpost.com/cybercriminals-step-up-game-us-elections/160373/){:target="_blank" rel="noopener"}

> Ahead of the November U.S. elections, cybercriminals are stepping up their offensive in both attacks against security infrastructure and disinformation campaigns - but this time, social media giants, the government and citizens are more prepared. [...]
