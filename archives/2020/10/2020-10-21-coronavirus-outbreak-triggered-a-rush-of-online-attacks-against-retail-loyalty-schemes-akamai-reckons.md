Title: Coronavirus outbreak triggered a rush of online attacks against retail loyalty schemes, Akamai reckons
Date: 2020-10-21T20:25:51+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: coronavirus-outbreak-triggered-a-rush-of-online-attacks-against-retail-loyalty-schemes-akamai-reckons

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/21/akamai_retail_security_report/){:target="_blank" rel="noopener"}

> Digital souks are sitting ducks for identity fraudsters Hackers are breaking into online loyalty card accounts using stolen credentials or easily obtainable information, and then not only ransacking the profiles' balances but also harvesting victims' personal data for subsequent identity theft, Akamai has warned.... [...]
