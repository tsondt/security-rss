Title: AWS achieves FedRAMP P-ATO for 5 services in AWS US East/West and GovCloud (US) Regions
Date: 2020-10-27T17:10:13+00:00
Author: Amendaze Thomas
Category: AWS Security
Tags: Announcements;Foundational (100);Government;Security, Identity, & Compliance;FedRAMP;Security Blog
Slug: aws-achieves-fedramp-p-ato-for-5-services-in-aws-us-eastwest-and-govcloud-us-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-fedramp-p-ato-for-5-services-in-aws-us-east-west-and-govcloud-us-regions/){:target="_blank" rel="noopener"}

> We’re pleased to announce that five additional AWS services have achieved provisional authorization (P-ATO) by the Federal Risk and Authorization Management Program (FedRAMP) Joint Authorization Board (JAB). These services provide the following capabilities for the federal government and customers with regulated workloads: Enable your organization’s developers, scientists, and engineers to easily and efficiently run hundreds [...]
