Title: Massive Nitro data breach impacts Microsoft, Google, Apple, more
Date: 2020-10-26T13:04:57
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: massive-nitro-data-breach-impacts-microsoft-google-apple-more

[Source](https://www.bleepingcomputer.com/news/security/massive-nitro-data-breach-impacts-microsoft-google-apple-more/){:target="_blank" rel="noopener"}

> A massive data breach suffered by the Nitro PDF service impacts many well-known organizations, including Google, Apple, Microsoft, Chase, and Citibank. [...]
