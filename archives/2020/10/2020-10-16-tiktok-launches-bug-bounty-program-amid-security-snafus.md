Title: TikTok Launches Bug Bounty Program Amid Security Snafus
Date: 2020-10-16T13:26:49+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security;Android;apple;bug bounty;Critical flaws;Developers;ethical hackers;HackerOne;Hackers;Oracle;tiktok;vulnerabilities;Wal-Mart
Slug: tiktok-launches-bug-bounty-program-amid-security-snafus

[Source](https://threatpost.com/tiktok-bug-bounty-security/160203/){:target="_blank" rel="noopener"}

> The move is a distinct change in direction for the app, which has been criticized and even banned for its security practices. [...]
