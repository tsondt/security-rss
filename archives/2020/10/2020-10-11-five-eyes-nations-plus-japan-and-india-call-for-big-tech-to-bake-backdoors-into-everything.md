Title: Five Eyes nations plus Japan and India call for Big Tech to bake backdoors into everything
Date: 2020-10-11T23:51:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: five-eyes-nations-plus-japan-and-india-call-for-big-tech-to-bake-backdoors-into-everything

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/11/international_statementon_end_to_end_encryption_and_public_safety/){:target="_blank" rel="noopener"}

> Or as they put it: ‘Embed the safety of the public in system designs... facilitating the investigation and prosecution of offences’ The nations of the Five Eyes security alliance – Australia, Canada, New Zealand, the USA and the UK – plus Japan and India, have called on technology companies to design their products so they offer access to encrypted messages and content.... [...]
