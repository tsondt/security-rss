Title: Huawei's UK code reviewers say Chinese mega-corp is still totally crap at basic software security. Bad crypto, buffer overflows, logic errors...
Date: 2020-10-01T13:00:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: huaweis-uk-code-reviewers-say-chinese-mega-corp-is-still-totally-crap-at-basic-software-security-bad-crypto-buffer-overflows-logic-errors

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/01/huawei_uk_security_code_review_panel/){:target="_blank" rel="noopener"}

> Last year telcos scrambled to plug 'critical user-facing vulns' in network kit UK.gov security researchers examining Huawei source code have so far verified just eight firmware binaries out of more than 60 used across Britain's mobile phone networks, according to the GCHQ-backed agency's annual report.... [...]
