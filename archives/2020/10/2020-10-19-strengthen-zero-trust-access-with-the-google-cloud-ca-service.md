Title: Strengthen zero trust access with the Google Cloud CA service
Date: 2020-10-19T16:00:00+00:00
Author: Anton Chuvakin
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: strengthen-zero-trust-access-with-the-google-cloud-ca-service

[Source](https://cloud.google.com/blog/products/identity-security/now-available-in-beta-google-cloud-certificate-authority-service/){:target="_blank" rel="noopener"}

> As more organizations undergo digital transformation, evolve their IT infrastructure and migrate to public cloud, the role of digital certificates will grow—and grow a lot. Certificates and certificate authorities (CAs) play a key role in both modern IT models like DevOps and in the evolution of traditional enterprise IT. In August, we announced our Certificate Authority Service (CAS)—a highly scalable and available service that simplifies and automates the management and deployment of private CAs while meeting the needs of modern developers building and running modern systems and applications. Take a look at how easy it is to set up a [...]
