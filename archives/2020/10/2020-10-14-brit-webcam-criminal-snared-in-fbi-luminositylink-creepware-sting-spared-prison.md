Title: Brit webcam criminal snared in FBI LuminosityLink creepware sting spared prison
Date: 2020-10-14T13:57:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: brit-webcam-criminal-snared-in-fbi-luminositylink-creepware-sting-spared-prison

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/14/swindon_webcam_perv_john_wood/){:target="_blank" rel="noopener"}

> Swindon man walks away with two-year suspended sentence A man who spied on unsuspecting victims through their webcams has escaped a prison sentence after buying off-the-shelf LuminosityLink malware and using CCTV software to spy on them.... [...]
