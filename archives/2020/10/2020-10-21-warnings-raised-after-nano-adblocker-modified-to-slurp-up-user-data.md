Title: Warnings raised after Nano Adblocker modified to slurp up user data
Date: 2020-10-21T14:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: warnings-raised-after-nano-adblocker-modified-to-slurp-up-user-data

[Source](https://portswigger.net/daily-swig/warnings-raised-after-nano-adblocker-modified-to-slurp-up-user-data){:target="_blank" rel="noopener"}

> Popular software changed to collect user info by new owners [...]
