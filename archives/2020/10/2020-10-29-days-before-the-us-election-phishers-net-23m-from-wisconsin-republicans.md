Title: Days before the US election, phishers net $2.3m from Wisconsin Republicans
Date: 2020-10-29T20:58:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: days-before-the-us-election-phishers-net-23m-from-wisconsin-republicans

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/29/wisconsin_political_phishing/){:target="_blank" rel="noopener"}

> Big money in American politics proves chum in the water for online sharks As America counts down to the November 3 elections, things are tense for political campaigns. There's a lot of money flying around and the online criminals have sensed blood in the water.... [...]
