Title: BazarLoader used to deploy Ryuk ransomware on high-value targets
Date: 2020-10-12T12:53:03-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: bazarloader-used-to-deploy-ryuk-ransomware-on-high-value-targets

[Source](https://www.bleepingcomputer.com/news/security/bazarloader-used-to-deploy-ryuk-ransomware-on-high-value-targets/){:target="_blank" rel="noopener"}

> The TrickBot gang operators are increasingly targeting high-value targets with the new stealthy BazarLoader trojan before deploying the Ryuk ransomware. [...]
