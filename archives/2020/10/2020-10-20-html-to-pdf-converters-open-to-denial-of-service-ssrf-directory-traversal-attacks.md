Title: HTML-to-PDF converters open to denial-of-service, SSRF, directory traversal attacks
Date: 2020-10-20T14:34:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: html-to-pdf-converters-open-to-denial-of-service-ssrf-directory-traversal-attacks

[Source](https://portswigger.net/daily-swig/html-to-pdf-converters-open-to-denial-of-service-ssrf-directory-traversal-attacks){:target="_blank" rel="noopener"}

> Infosec intern assailed eight open source libraries in 11 different ways [...]
