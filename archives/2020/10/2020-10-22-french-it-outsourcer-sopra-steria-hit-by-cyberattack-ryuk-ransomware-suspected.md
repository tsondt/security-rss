Title: French IT outsourcer Sopra Steria hit by 'cyberattack', Ryuk ransomware suspected
Date: 2020-10-22T14:37:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: french-it-outsourcer-sopra-steria-hit-by-cyberattack-ryuk-ransomware-suspected

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/22/sopra_steria_ryuk_ransomware_reports/){:target="_blank" rel="noopener"}

> You know, the firm that runs half of NHS Business Services French-headquartered IT outsourcer Sopra Steria has been struck by a “cyberattack,” reportedly linked to the Ryuk ransomware gang.... [...]
