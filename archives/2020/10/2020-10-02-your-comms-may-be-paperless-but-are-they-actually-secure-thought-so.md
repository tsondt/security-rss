Title: Your comms may be paperless, but are they actually secure? Thought so...
Date: 2020-10-02T06:00:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: your-comms-may-be-paperless-but-are-they-actually-secure-thought-so

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/02/paperless_comms_regcast/){:target="_blank" rel="noopener"}

> Learn how to get it right with El Reg and Echoworx (...Echoworx) (...Echoworx) Webcast The idea of the paperless office has been with us since, probably, the invention of paper. But like the 15-hour working week and the flying-car commute, it always seems to be just over the rainbow of practicality.... [...]
