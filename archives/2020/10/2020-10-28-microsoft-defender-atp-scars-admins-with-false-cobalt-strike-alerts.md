Title: Microsoft Defender ATP scars admins with false Cobalt Strike alerts
Date: 2020-10-28T11:14:08
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-atp-scars-admins-with-false-cobalt-strike-alerts

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-atp-scars-admins-with-false-cobalt-strike-alerts/){:target="_blank" rel="noopener"}

> Administrators woke up to a scary surprise today after false positives in Microsoft Defender ATP showed network devices infected with Cobalt Strike. [...]
