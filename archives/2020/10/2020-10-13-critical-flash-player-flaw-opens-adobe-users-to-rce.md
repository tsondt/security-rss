Title: Critical Flash Player Flaw Opens Adobe Users to RCE
Date: 2020-10-13T17:46:11+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;adobe;adobe flash desktop runtime;CVE-2020-9746;Linux;macOS;NULL pointer dereference;patch;patch tuesday;vulnerability;Windows
Slug: critical-flash-player-flaw-opens-adobe-users-to-rce

[Source](https://threatpost.com/flash-player-flaw-adobe-rce/160034/){:target="_blank" rel="noopener"}

> The flaw stems from a NULL Pointer Dereference error and plagues the Windows, macOS, Linux and ChromeOS versions of Adobe Flash Player. [...]
