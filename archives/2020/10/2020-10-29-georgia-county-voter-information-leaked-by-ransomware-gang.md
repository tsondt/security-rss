Title: Georgia county voter information leaked by ransomware gang
Date: 2020-10-29T13:53:54
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: georgia-county-voter-information-leaked-by-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/georgia-county-voter-information-leaked-by-ransomware-gang/){:target="_blank" rel="noopener"}

> The DoppelPaymer ransomware gang has released unencrypted data stolen from Hall County, Georgia, during a cyberattack earlier this month. [...]
