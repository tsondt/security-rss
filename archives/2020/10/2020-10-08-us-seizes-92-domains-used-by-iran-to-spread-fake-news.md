Title: US seizes 92 domains used by Iran to spread ‘fake news’
Date: 2020-10-08T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-seizes-92-domains-used-by-iran-to-spread-fake-news

[Source](https://portswigger.net/daily-swig/us-seizes-92-domains-used-by-iran-to-spread-fake-news){:target="_blank" rel="noopener"}

> FBI investigation spurred by intelligence from Google [...]
