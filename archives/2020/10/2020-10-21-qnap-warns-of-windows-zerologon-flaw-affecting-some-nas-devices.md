Title: QNAP warns of Windows Zerologon flaw affecting some NAS devices
Date: 2020-10-21T13:06:41
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qnap-warns-of-windows-zerologon-flaw-affecting-some-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-of-windows-zerologon-flaw-affecting-some-nas-devices/){:target="_blank" rel="noopener"}

> Network-attached storage device maker QNAP warns customers that some NAS storage devices running vulnerable versions of the QTS operating system are exposed to attacks attempting to exploit the critical Windows ZeroLogon vulnerability. [...]
