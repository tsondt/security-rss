Title: 'Facebook simply would not exist today if not for Bletchley Park,' says social network – but don't hold that against it
Date: 2020-10-14T10:29:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: facebook-simply-would-not-exist-today-if-not-for-bletchley-park-says-social-network-but-dont-hold-that-against-it

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/14/facebook_bletchley_park/){:target="_blank" rel="noopener"}

> Zuckerberg and UK government throw code-breaking site funding lifeline From the Department of Definitely Not Evil comes news that Facebook is donating £1m to Britain's Bletchley Park computing landmark.... [...]
