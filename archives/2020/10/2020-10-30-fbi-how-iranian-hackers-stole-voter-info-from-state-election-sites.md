Title: FBI: How Iranian hackers stole voter info from state election sites
Date: 2020-10-30T18:44:54
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-how-iranian-hackers-stole-voter-info-from-state-election-sites

[Source](https://www.bleepingcomputer.com/news/security/fbi-how-iranian-hackers-stole-voter-info-from-state-election-sites/){:target="_blank" rel="noopener"}

> DHS CISA and the FBI today shared more info on how an Iranian state-sponsored hacking group was able to harvest voter registration info from U.S. state websites, including election sites. [...]
