Title: HEH P2P Botnet Sports Dangerous Wiper Function
Date: 2020-10-08T17:27:57+00:00
Author: Tara Seals
Category: Threatpost
Tags: IoT;Malware;Mobile Security;Web Security;360netlab;botnet;brute forcing;heh;Malware analysis;P2P;peer to peer;self destruct;Telnet;Wiper
Slug: heh-p2p-botnet-sports-dangerous-wiper-function

[Source](https://threatpost.com/heh-p2p-botnet-wiper-function/159974/){:target="_blank" rel="noopener"}

> The P2P malware is infecting any and all types of endpoints via brute-forcing, with 10 versions targeting desktops, laptops, mobile and IoT devices. [...]
