Title: Maze ransomware is shutting down its cybercrime operation
Date: 2020-10-29T00:31:18
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: maze-ransomware-is-shutting-down-its-cybercrime-operation

[Source](https://www.bleepingcomputer.com/news/security/maze-ransomware-is-shutting-down-its-cybercrime-operation/){:target="_blank" rel="noopener"}

> ​The Maze cybercrime gang is shutting down its operations after rising to become one of the most prominent players performing ransomware attacks. [...]
