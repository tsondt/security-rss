Title: Why, yes, you can register an XSS attack as a UK company name. How do we know that? Someone actually did it
Date: 2020-10-30T13:00:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: why-yes-you-can-register-an-xss-attack-as-a-uk-company-name-how-do-we-know-that-someone-actually-did-it

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/30/companies_house_xss_silliness/){:target="_blank" rel="noopener"}

> And the 'acceptable company name' charset is hardcoded... in legislation Companies House has blocked someone who registered a new biz with a name that contained the right characters arranged in the right order to trigger a cross-site scripting (XSS) attack against users of the service's API.... [...]
