Title: OpenStack haven OpenDev yanks Gerrit code review tool after admin account compromised for two weeks
Date: 2020-10-21T02:08:46+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: openstack-haven-opendev-yanks-gerrit-code-review-tool-after-admin-account-compromised-for-two-weeks

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/21/opendev_gerrit_attack/){:target="_blank" rel="noopener"}

> Source warehouse asks users to verify recent project commits to ensure they’re not malicious OpenDev.org, which hosts the official OpenStack source code, on Tuesday tore down its Gerrit deployment after realizing it had been secretly hacked two weeks ago.... [...]
