Title: Wroba Mobile Banking Trojan Spreads to the U.S. via Texts
Date: 2020-10-30T18:35:15+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;Web Security;apac;apple id credentials;banking trojan;fake browser update;fake shipment notifications;ios phishing;Kaspersky;mobile malware;package notifications;roaming mantis;self-propagate;SMS;targeting;texts;U.S.;wroba
Slug: wroba-mobile-banking-trojan-spreads-to-the-us-via-texts

[Source](https://threatpost.com/wroba-mobile-banking-trojan-spreads-us/160785/){:target="_blank" rel="noopener"}

> The Roaming Mantis group is targeting the States with a malware that can steal information, harvest financial data and send texts to self-propagate. [...]
