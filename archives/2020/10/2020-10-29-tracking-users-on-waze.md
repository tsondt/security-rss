Title: Tracking Users on Waze
Date: 2020-10-29T14:52:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: anonymity;de-anonymization;vulnerabilities
Slug: tracking-users-on-waze

[Source](https://www.schneier.com/blog/archives/2020/10/tracking-users-on-waze.html){:target="_blank" rel="noopener"}

> A security researcher discovered a wulnerability in Waze that breaks the anonymity of users: I found out that I can visit Waze from any web browser at waze.com/livemap so I decided to check how are those driver icons implemented. What I found is that I can ask Waze API for data on a location by sending my latitude and longitude coordinates. Except the essential traffic information, Waze also sends me coordinates of other drivers who are nearby. What caught my eyes was that identification numbers (ID) associated with the icons were not changing over time. I decided to track one [...]
