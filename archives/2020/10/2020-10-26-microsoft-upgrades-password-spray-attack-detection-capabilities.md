Title: Microsoft upgrades password spray attack detection capabilities
Date: 2020-10-26T14:45:40
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-upgrades-password-spray-attack-detection-capabilities

[Source](https://www.bleepingcomputer.com/news/security/microsoft-upgrades-password-spray-attack-detection-capabilities/){:target="_blank" rel="noopener"}

> Microsoft has improved password spray detection in Azure Active Directory (Azure AD) by doubling the number of compromised accounts it detects using a new machine learning (ML) system. [...]
