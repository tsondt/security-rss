Title: How to get read-only visibility into the AWS Control Tower console
Date: 2020-10-01T19:30:38+00:00
Author: Bruno Mendez
Category: AWS Security
Tags: AWS Control Tower;Foundational (100);Security, Identity, & Compliance;IAM policy;least privilege;Readonly;Security Blog
Slug: how-to-get-read-only-visibility-into-the-aws-control-tower-console

[Source](https://aws.amazon.com/blogs/security/how-to-get-read-only-visibility-into-aws-control-tower-console/){:target="_blank" rel="noopener"}

> When you audit an environment governed by AWS Control Tower, having visibility into the AWS Control Tower console allows you to collect important configuration information, but currently there isn’t a read-only role installed by AWS Control Tower. In this post, I will show you how to create a custom permission set by using both a [...]
