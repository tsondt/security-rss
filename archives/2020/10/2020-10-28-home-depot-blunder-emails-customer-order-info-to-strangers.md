Title: Home Depot blunder emails customer order info to strangers
Date: 2020-10-28T17:53:35
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: home-depot-blunder-emails-customer-order-info-to-strangers

[Source](https://www.bleepingcomputer.com/news/security/home-depot-blunder-emails-customer-order-info-to-strangers/){:target="_blank" rel="noopener"}

> Multiple reports emerged today from Home Depot customers in Canada stating that the company had accidentally sent them hundreds of emails containing order information of strangers. Multiple users received hundreds of "order ready for pickup" reminder emails, each pertaining to a different order and not associated with their account. [...]
