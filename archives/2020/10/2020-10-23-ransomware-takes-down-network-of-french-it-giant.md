Title: Ransomware Takes Down Network of French IT Giant
Date: 2020-10-23T12:38:22+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;BazarLoader Universal Health Services;cyber attack;Cybersecurity;digital transformation;IT services;Point3 Security;ransomware;ryuk;Sopra Steria;TrickBot
Slug: ransomware-takes-down-network-of-french-it-giant

[Source](https://threatpost.com/ransomware-french-it-giant/160484/){:target="_blank" rel="noopener"}

> Sopra Steria hit with cyber attack that reportedly encrypted parts of their network on Oct. 20 but has remained mostly mum on details. [...]
