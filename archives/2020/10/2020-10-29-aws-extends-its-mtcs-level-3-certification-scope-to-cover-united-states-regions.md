Title: AWS extends its MTCS Level 3 certification scope to cover United States Regions
Date: 2020-10-29T17:52:04+00:00
Author: Clara Lim
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Banking Regulations;FSI;Korea;MTCS;Security Blog;Seoul;Singapore
Slug: aws-extends-its-mtcs-level-3-certification-scope-to-cover-united-states-regions

[Source](https://aws.amazon.com/blogs/security/aws-extends-its-mtcs-level-3-certification-scope-to-cover-united-states-regions/){:target="_blank" rel="noopener"}

> We’re excited to announce the completion of the Multi-Tier Cloud Security (MTCS) Level 3 triennial certification in September 2020. The scope was expanded to cover the United States Amazon Web Services (AWS) Regions, excluding AWS GovCloud (US) Regions, in addition to Singapore and Seoul. AWS was the first cloud service provider (CSP) to attain the [...]
