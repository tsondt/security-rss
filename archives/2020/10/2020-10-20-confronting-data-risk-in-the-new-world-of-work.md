Title: Confronting Data Risk in the New World of Work
Date: 2020-10-20T13:00:14+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Web Security;data risk;insider risks;remote work
Slug: confronting-data-risk-in-the-new-world-of-work

[Source](https://threatpost.com/code42-confronting-data-risk-in-the-new-world-of-work/160140/){:target="_blank" rel="noopener"}

> With Stanford research showing that nearly half of the U.S. labor force is now working from home full-time, insider threats are a much more difficult problem. [...]
