Title: QBot uses Windows Defender Antivirus phishing bait to infect PCs
Date: 2020-10-12T15:50:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: qbot-uses-windows-defender-antivirus-phishing-bait-to-infect-pcs

[Source](https://www.bleepingcomputer.com/news/security/qbot-uses-windows-defender-antivirus-phishing-bait-to-infect-pcs/){:target="_blank" rel="noopener"}

> The Qbot botnet uses a new template for the distribution of their malware that uses a fake Windows Defender Antivirus theme to trick you into enabling Excel macros. [...]
