Title: Friday Squid Blogging: After Squidnight
Date: 2020-10-02T21:05:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-after-squidnight

[Source](https://www.schneier.com/blog/archives/2020/10/friday-squid-blogging-after-squidnight.html){:target="_blank" rel="noopener"}

> Review of a squid-related children’s book. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
