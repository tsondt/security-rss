Title: Microsoft launches machine learning cyber-attack threat matrix
Date: 2020-10-23T12:46:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: microsoft-launches-machine-learning-cyber-attack-threat-matrix

[Source](https://portswigger.net/daily-swig/microsoft-launches-machine-learning-cyber-attack-threat-matrix){:target="_blank" rel="noopener"}

> Framework aimed at helping security pros detect and remediate threats against ML systems [...]
