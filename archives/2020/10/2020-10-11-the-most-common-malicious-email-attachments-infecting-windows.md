Title: The most common malicious email attachments infecting Windows
Date: 2020-10-11T13:18:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-most-common-malicious-email-attachments-infecting-windows

[Source](https://www.bleepingcomputer.com/news/security/the-most-common-malicious-email-attachments-infecting-windows/){:target="_blank" rel="noopener"}

> To stay safe online, everyone needs to recognize malicious attachments that are commonly used in phishing emails to distribute malware. [...]
