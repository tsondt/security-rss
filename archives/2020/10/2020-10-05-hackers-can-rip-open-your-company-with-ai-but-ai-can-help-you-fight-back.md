Title: Hackers can rip open your company with AI… But AI can help you fight back
Date: 2020-10-05T17:05:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: hackers-can-rip-open-your-company-with-ai-but-ai-can-help-you-fight-back

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/05/how_phishing_has_become_phatal/){:target="_blank" rel="noopener"}

> How? Join our October 7 online broadcast to find the answer Webcast Consider a world where cyber-attackers are using AI to refine, control and scale up their attacks. There is no need to stretch your imagination: sophisticated hackers are using AI techniques today to manage botnets, mount attacks, and cover up their traces, as well as to help them understand the context they’re operating in, and, naturally, increase profitability.... [...]
