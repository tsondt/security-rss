Title: Behind the botnet: Akamai’s Tony Lauro on tackling real-world credential stuffing attacks
Date: 2020-10-30T12:12:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: behind-the-botnet-akamais-tony-lauro-on-tackling-real-world-credential-stuffing-attacks

[Source](https://portswigger.net/daily-swig/behind-the-botnet-akamais-tony-lauro-on-tackling-real-world-credential-stuffing-attacks){:target="_blank" rel="noopener"}

> Countermeasures must harness the broadest possible datasets and personalized consumer identity info [...]
