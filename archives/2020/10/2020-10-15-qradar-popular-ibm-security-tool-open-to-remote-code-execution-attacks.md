Title: QRadar: Popular IBM security tool open to remote code execution attacks
Date: 2020-10-15T12:54:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: qradar-popular-ibm-security-tool-open-to-remote-code-execution-attacks

[Source](https://portswigger.net/daily-swig/qradar-popular-ibm-security-tool-open-to-remote-code-execution-attacks){:target="_blank" rel="noopener"}

> Deserialization vulnerability in SIEM product could lead to complete system compromise [...]
