Title: FBI warns of newly registered domains spoofing US Census Bureau
Date: 2020-10-19T14:57:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-newly-registered-domains-spoofing-us-census-bureau

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-newly-registered-domains-spoofing-us-census-bureau/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) issued a flash alert to warn of the potential use of spoofed US Census Bureau domains in future malicious campaigns including phishing and credential theft attacks. [...]
