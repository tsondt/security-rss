Title: Ed Snowden doesn’t need to worry about being turfed out of Russia any more
Date: 2020-10-23T06:34:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: ed-snowden-doesnt-need-to-worry-about-being-turfed-out-of-russia-any-more

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/23/edward_snowden_russian_permanent_residency/){:target="_blank" rel="noopener"}

> Immigration reforms allowing more skilled workers to stay seem to have resulted in permanent residency Russia has apparently given super-leaker and former NSA sysadmin Edward Snowden de facto permanent residence.... [...]
