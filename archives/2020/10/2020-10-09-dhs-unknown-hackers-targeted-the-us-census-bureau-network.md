Title: DHS: Unknown hackers targeted the US Census Bureau network
Date: 2020-10-09T12:31:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: dhs-unknown-hackers-targeted-the-us-census-bureau-network

[Source](https://www.bleepingcomputer.com/news/security/dhs-unknown-hackers-targeted-the-us-census-bureau-network/){:target="_blank" rel="noopener"}

> The US Department of Homeland Security said that unknown threat actors have targeted the U.S. Census network during the last year in its first-ever Homeland Threat Assessment (HTA) report released earlier this week. [...]
