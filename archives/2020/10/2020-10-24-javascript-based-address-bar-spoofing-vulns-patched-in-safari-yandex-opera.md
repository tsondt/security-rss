Title: JavaScript-based address bar spoofing vulns patched in Safari, Yandex, Opera
Date: 2020-10-24T07:14:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: javascript-based-address-bar-spoofing-vulns-patched-in-safari-yandex-opera

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/24/browser_address_spoofing/){:target="_blank" rel="noopener"}

> Are you where you think you are, or are you where I want you to think you are? Rapid7 found Apple’s Safari browser, as well as the Opera Mini and Yandex browsers, were vulnerable to JavaScript-based address bar spoofing.... [...]
