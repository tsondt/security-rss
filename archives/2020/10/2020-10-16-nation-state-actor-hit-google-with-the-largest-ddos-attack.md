Title: Nation-state actor hit Google with the largest DDoS attack
Date: 2020-10-16T17:30:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: nation-state-actor-hit-google-with-the-largest-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/nation-state-actor-hit-google-with-the-largest-ddos-attack/){:target="_blank" rel="noopener"}

> In an overview of distributed denial-of-service (DDoS) trends targeting its network links, Google revealed that in 2017 a nation-state actor used massive firepower that amounted to more than 2.7 terabits per second. [...]
