Title: TrickBot Takedown Disrupts Major Crimeware Apparatus
Date: 2020-10-13T14:45:51+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;IoT;Malware;botnet;Copyright Infringement;disruption;ESET;global action;malware;Microsoft;takedown;TrickBot
Slug: trickbot-takedown-disrupts-major-crimeware-apparatus

[Source](https://threatpost.com/trickbot-takedown-crimeware-apparatus/160018/){:target="_blank" rel="noopener"}

> Microsoft and partners went after the botnet using a copyright infringement tactic and hunting down C2 servers. [...]
