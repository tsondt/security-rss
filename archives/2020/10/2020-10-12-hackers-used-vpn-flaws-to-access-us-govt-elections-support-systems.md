Title: Hackers used VPN flaws to access US govt elections support systems
Date: 2020-10-12T13:47:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hackers-used-vpn-flaws-to-access-us-govt-elections-support-systems

[Source](https://www.bleepingcomputer.com/news/security/hackers-used-vpn-flaws-to-access-us-govt-elections-support-systems/){:target="_blank" rel="noopener"}

> Government-backed hackers have compromised and gained access to US elections support systems by chaining together VPN vulnerabilities and the recent Windows CVE-2020-1472 security flaw. [...]
