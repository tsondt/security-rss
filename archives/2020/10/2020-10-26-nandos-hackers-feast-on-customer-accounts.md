Title: Nando’s Hackers Feast on Customer Accounts
Date: 2020-10-26T14:40:42+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Web Security;Account Credentials;account takeover;compromise;Credential stuffing;high volume orders;nando's;peri peri chicken
Slug: nandos-hackers-feast-on-customer-accounts

[Source](https://threatpost.com/nandos-hackers-customer-accounts/160527/){:target="_blank" rel="noopener"}

> Multiple chicken diners said their usernames and passwords were stolen and the accounts used to place high-volume orders. [...]
