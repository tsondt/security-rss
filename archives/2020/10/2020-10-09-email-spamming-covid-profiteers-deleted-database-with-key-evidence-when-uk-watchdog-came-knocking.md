Title: Email-spamming COVID profiteers deleted database with 'key evidence' when UK watchdog came knocking
Date: 2020-10-09T08:30:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: email-spamming-covid-profiteers-deleted-database-with-key-evidence-when-uk-watchdog-came-knocking

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/09/covid_spam_profiteer_fined_40k_ico/){:target="_blank" rel="noopener"}

> Fined £40k after probe. Plus: ICO wants your views on its future powers A company that fired out more than 9,000 spam emails promoting face masks has been fined £40,000 by the UK Information Commissioner's Office and ordered to stop doing it.... [...]
