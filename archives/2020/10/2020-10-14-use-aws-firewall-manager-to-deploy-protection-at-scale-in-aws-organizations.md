Title: Use AWS Firewall Manager to deploy protection at scale in AWS Organizations
Date: 2020-10-14T22:30:38+00:00
Author: Chamandeep Singh
Category: AWS Security
Tags: Advanced (300);AWS Firewall Manager;Security, Identity, & Compliance;AWS FMS;AWS Shield Advanced;Centralized management;Compliance;DDoS notifications;Security Blog;Security groups;VPC security groups
Slug: use-aws-firewall-manager-to-deploy-protection-at-scale-in-aws-organizations

[Source](https://aws.amazon.com/blogs/security/use-aws-firewall-manager-to-deploy-protection-at-scale-in-aws-organizations/){:target="_blank" rel="noopener"}

> Security teams that are responsible for securing workloads in hundreds of Amazon Web Services (AWS) accounts in different organizational units aim for a consistent approach across AWS Organizations. Key goals include enforcing preventative measures to mitigate known security issues, having a central approach for notifying the SecOps team about potential distributed denial of service (DDoS) [...]
