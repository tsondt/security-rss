Title: Symfony-based websites open to RCE attack, research finds
Date: 2020-10-23T15:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: symfony-based-websites-open-to-rce-attack-research-finds

[Source](https://portswigger.net/daily-swig/symfony-based-websites-open-to-rce-attack-research-finds){:target="_blank" rel="noopener"}

> Researcher’s methods rely on misconfigured servers – but these are easy to find [...]
