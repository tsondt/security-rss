Title: Diplomats are supposed to be subtle and clever. Australia’s just leaked 1,000 citizens’ email addresses
Date: 2020-10-01T04:23:55+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: diplomats-are-supposed-to-be-subtle-and-clever-australias-just-leaked-1000-citizens-email-addresses

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/01/dfat_email_leak/){:target="_blank" rel="noopener"}

> And not just any citizens, but folks stranded overseas and in dire need of assistance Australia’s Department of Foreign Affairs and Trade (DFAT) has just exposed personal details of over 1,000 citizens in an email.... [...]
