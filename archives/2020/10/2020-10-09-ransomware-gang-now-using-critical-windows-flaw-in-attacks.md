Title: Ransomware gang now using critical Windows flaw in attacks
Date: 2020-10-09T03:33:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-now-using-critical-windows-flaw-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-now-using-critical-windows-flaw-in-attacks/){:target="_blank" rel="noopener"}

> Microsoft is warning that cybercriminals have started to incorporate exploit code for the ZeroLogon vulnerability in their attacks. The alert comes after the company noticed ongoing attacks from cyber-espionage group MuddyWater (SeedWorm) in the second half of September. [...]
