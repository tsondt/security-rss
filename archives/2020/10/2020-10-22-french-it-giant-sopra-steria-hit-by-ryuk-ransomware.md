Title: French IT giant Sopra Steria hit by Ryuk ransomware
Date: 2020-10-22T17:36:34
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: french-it-giant-sopra-steria-hit-by-ryuk-ransomware

[Source](https://www.bleepingcomputer.com/news/security/french-it-giant-sopra-steria-hit-by-ryuk-ransomware/){:target="_blank" rel="noopener"}

> French IT services giant Sopra Steria suffered a cyberattack on October 20th, 2020, that reportedly encrypted portions of their network with the Ryuk ransomware. [...]
