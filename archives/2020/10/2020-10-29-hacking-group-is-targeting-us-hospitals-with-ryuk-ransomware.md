Title: Hacking group is targeting US hospitals with Ryuk ransomware
Date: 2020-10-29T07:31:31
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hacking-group-is-targeting-us-hospitals-with-ryuk-ransomware

[Source](https://www.bleepingcomputer.com/news/security/hacking-group-is-targeting-us-hospitals-with-ryuk-ransomware/){:target="_blank" rel="noopener"}

> In a joint statement, the U.S. government is warning the healthcare industry that a hacking group is actively targeting hospitals and healthcare providers in Ryuk ransomware attacks. [...]
