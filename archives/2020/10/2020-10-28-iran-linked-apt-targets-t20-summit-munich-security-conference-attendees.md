Title: Iran-linked APT Targets T20 Summit, Munich Security Conference Attendees
Date: 2020-10-28T15:40:50+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;APT 35;Charming Kitten;Microsoft;Munich security conference;phosphorous APT;T20;the Think 20 summit
Slug: iran-linked-apt-targets-t20-summit-munich-security-conference-attendees

[Source](https://threatpost.com/microsoft-iranian-apt-t20-summit-munich-security-conference/160654/){:target="_blank" rel="noopener"}

> The Phosphorous APT has launched successful attacks against world leaders who are attending the Munich Security Conference and the Think 20 (T20) Summit in Saudi Arabia, Microsoft warns. [...]
