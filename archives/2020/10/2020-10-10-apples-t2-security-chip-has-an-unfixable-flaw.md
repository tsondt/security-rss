Title: Apple’s T2 security chip has an unfixable flaw
Date: 2020-10-10T11:04:05+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Tech;apple;checkm8;jailbreak;T2
Slug: apples-t2-security-chip-has-an-unfixable-flaw

[Source](https://arstechnica.com/?p=1713174){:target="_blank" rel="noopener"}

> Enlarge / The 2014 Mac mini is pictured here alongside the 2012 Mac mini. They looked the same, but the insides were different in some key—and disappointing—ways. (credit: Andrew Cunningham ) A recently released tool is letting anyone exploit an unusual Mac vulnerability to bypass Apple's trusted T2 security chip and gain deep system access. The flaw is one researchers have also been using for more than a year to jailbreak older models of iPhones. But the fact that the T2 chip is vulnerable in the same way creates a new host of potential threats. Worst of all, while Apple [...]
