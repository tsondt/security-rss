Title: Former antivirus baron John McAfee collared, faces extradition to America on tax evasion, securities allegations
Date: 2020-10-06T02:42:31+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: former-antivirus-baron-john-mcafee-collared-faces-extradition-to-america-on-tax-evasion-securities-allegations

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/06/john_mcafee_tax_evasion/){:target="_blank" rel="noopener"}

> He paid more or less than $750, though? Two US government tentacles this week snared John McAfee, accusing the one-time antivirus mogul of tax evasion and breaking securities law.... [...]
