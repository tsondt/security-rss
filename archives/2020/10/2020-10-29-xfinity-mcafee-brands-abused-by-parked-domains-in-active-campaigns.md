Title: Xfinity, McAfee Brands Abused by Parked Domains in Active Campaigns
Date: 2020-10-29T10:00:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: Web Security;abuse;active campaign;Comcast;cyberattack;emotet;exploit kit;impersonation;Malvertising;McAfee;parked domains;redirect;Typosquatting;website;Xfinity
Slug: xfinity-mcafee-brands-abused-by-parked-domains-in-active-campaigns

[Source](https://threatpost.com/xfinity-mcafee-brands-abused-parked-domains/160698/){:target="_blank" rel="noopener"}

> Malicious redirection websites are using typosquatting and impersonation to attack unwary visitors. [...]
