Title: Finnish mental health patients blackmailed after suspected data breach
Date: 2020-10-26T16:16:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: finnish-mental-health-patients-blackmailed-after-suspected-data-breach

[Source](https://portswigger.net/daily-swig/finnish-mental-health-patients-blackmailed-after-suspected-data-breach){:target="_blank" rel="noopener"}

> Two security incidents have been recorded at a Helsinki clinic [...]
