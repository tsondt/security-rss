Title: Three steps to data-centric security: Discovery, protection, and control
Date: 2020-10-28T14:00:11+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: three-steps-to-data-centric-security-discovery-protection-and-control

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/28/three_steps_data_centric_security/){:target="_blank" rel="noopener"}

> It's 2020 and the enemy isn't at the gate anymore. It's in your network, probing your switches and servers Sponsored It's 2020 and the enemy isn't at the gate anymore. It's in your network, probing your switches and servers. That makes the gate irrelevant. So what do you do now?... [...]
