Title: Microsoft issues out-of-band Windows security updates for RCE bugs
Date: 2020-10-16T15:22:41
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-issues-out-of-band-windows-security-updates-for-rce-bugs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-issues-out-of-band-windows-security-updates-for-rce-bugs/){:target="_blank" rel="noopener"}

> Microsoft has released two out-of-band security updates designed to address remote code execution (RCE) bugs found to affect Visual Studio Code and the Microsoft Windows Codecs Library. [...]
