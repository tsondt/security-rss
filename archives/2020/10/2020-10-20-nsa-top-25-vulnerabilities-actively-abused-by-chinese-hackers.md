Title: NSA: Top 25 vulnerabilities actively abused by Chinese hackers
Date: 2020-10-20T11:20:49
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Government
Slug: nsa-top-25-vulnerabilities-actively-abused-by-chinese-hackers

[Source](https://www.bleepingcomputer.com/news/security/nsa-top-25-vulnerabilities-actively-abused-by-chinese-hackers/){:target="_blank" rel="noopener"}

> The U.S. National Security Agency (NSA) warns that Chinese state-sponsored hackers exploit 25 different vulnerabilities in attacks against U.S. organizations and interests. [...]
