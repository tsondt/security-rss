Title: McAfee rattles tin for $600m+ in fresh IPO filing valuing firm at $3.6bn
Date: 2020-10-14T15:52:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: mcafee-rattles-tin-for-600m-in-fresh-ipo-filing-valuing-firm-at-36bn

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/14/mcafee_ipo_reports/){:target="_blank" rel="noopener"}

> That's if shares sell at the high end McAfee – the antivirus vendor, not the totally sane and level-headed man who founded it – is reportedly looking to raise more than $600m in its upcoming IPO.... [...]
