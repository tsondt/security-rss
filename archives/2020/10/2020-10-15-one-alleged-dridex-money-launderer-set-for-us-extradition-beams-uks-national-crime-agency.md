Title: One alleged Dridex money-launderer set for US extradition, beams UK's National Crime Agency
Date: 2020-10-15T19:28:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: one-alleged-dridex-money-launderer-set-for-us-extradition-beams-uks-national-crime-agency

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/15/dridex_money_laundering_allegations_nca/){:target="_blank" rel="noopener"}

> They nicked six alleged perps last year but only one was charged Britain’s National Crime Agency arrested six men in London on suspicion of laundering “tens of millions” for the Trickbot and Dridex banking malware gangs, the not-quite-police agency declared today.... [...]
