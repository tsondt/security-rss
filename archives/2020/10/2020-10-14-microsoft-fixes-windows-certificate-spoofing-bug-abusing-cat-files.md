Title: Microsoft fixes Windows certificate spoofing bug abusing CAT files
Date: 2020-10-14T12:35:03-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-fixes-windows-certificate-spoofing-bug-abusing-cat-files

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-windows-certificate-spoofing-bug-abusing-cat-files/){:target="_blank" rel="noopener"}

> Microsoft's October 2020 Patch Tuesday fixed 87 security bugs, one of which is an "Important" Windows Spoofing Vulnerability that abuses CAT files. [...]
