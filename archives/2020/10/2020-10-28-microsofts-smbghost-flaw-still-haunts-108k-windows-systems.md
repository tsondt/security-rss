Title: Microsoft’s SMBGhost Flaw Still Haunts 108K Windows Systems
Date: 2020-10-28T20:36:09+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;CVE-2020-0796;Microsoft;microsoft flaw;Microsoft Windows;patch;Shodan;smbghost;Windows
Slug: microsofts-smbghost-flaw-still-haunts-108k-windows-systems

[Source](https://threatpost.com/microsofts-smbghost-flaw-108k-windows-systems/160682/){:target="_blank" rel="noopener"}

> While Microsoft patched the bug known as CVE-2020-0796 back in March, more than one 100,000 Windows systems are still vulnerable. [...]
