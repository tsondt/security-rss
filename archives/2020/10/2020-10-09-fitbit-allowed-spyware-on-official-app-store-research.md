Title: Fitbit allowed spyware on official app store – research
Date: 2020-10-09T12:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: fitbit-allowed-spyware-on-official-app-store-research

[Source](https://portswigger.net/daily-swig/fitbit-allowed-spyware-on-official-app-store-research){:target="_blank" rel="noopener"}

> Exercise tracker firm tightens security controls to thwart social engineering-based attack [...]
