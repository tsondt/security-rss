Title: The Windows 10 Calculator has been ported to Linux
Date: 2020-10-17T10:01:01
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-windows-10-calculator-has-been-ported-to-linux

[Source](https://www.bleepingcomputer.com/news/security/the-windows-10-calculator-has-been-ported-to-linux/){:target="_blank" rel="noopener"}

> The Windows 10 Calculator has been ported to Linux and can be installed from the Canonical Snap Store. [...]
