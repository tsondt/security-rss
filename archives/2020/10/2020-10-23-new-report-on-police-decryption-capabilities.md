Title: New Report on Police Decryption Capabilities
Date: 2020-10-23T13:47:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cell phones;encryption;law enforcement;police;smartphones
Slug: new-report-on-police-decryption-capabilities

[Source](https://www.schneier.com/blog/archives/2020/10/new-report-on-police-decryption-capabilities.html){:target="_blank" rel="noopener"}

> There is a new report on police decryption capabilities: specifically, mobile device forensic tools (MDFTs). Short summary: it’s not just the FBI that can do it. This report documents the widespread adoption of MDFTs by law enforcement in the United States. Based on 110 public records requests to state and local law enforcement agencies across the country, our research documents more than 2,000 agencies that have purchased these tools, in all 50 states and the District of Columbia. We found that state and local law enforcement agencies have performed hundreds of thousands of cellphone extractions since 2015, often without a [...]
