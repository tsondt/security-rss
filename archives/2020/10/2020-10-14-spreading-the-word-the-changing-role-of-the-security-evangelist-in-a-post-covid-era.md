Title: Spreading the word: The changing role of the security evangelist in a post-Covid era
Date: 2020-10-14T15:21:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: spreading-the-word-the-changing-role-of-the-security-evangelist-in-a-post-covid-era

[Source](https://portswigger.net/daily-swig/spreading-the-word-the-changing-role-of-the-security-evangelist-in-a-post-covid-era){:target="_blank" rel="noopener"}

> Prominent security evangelists say their credibility rests on sharing technical expertise, not marketing hype [...]
