Title: US Treasury hits bitcoin mixer with $60 million penalty
Date: 2020-10-19T16:00:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency
Slug: us-treasury-hits-bitcoin-mixer-with-60-million-penalty

[Source](https://www.bleepingcomputer.com/news/security/us-treasury-hits-bitcoin-mixer-with-60-million-penalty/){:target="_blank" rel="noopener"}

> The US Department of Treasury's Financial Crimes Enforcement Network (FinCEN) today announced the first-ever penalty against a Helix and Coin Ninja cryptocurrency mixing services. [...]
