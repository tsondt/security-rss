Title: BEC Attacks: Nigeria No Longer the Epicenter as Losses Top $26B
Date: 2020-10-14T20:27:54+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Most Recent ThreatLists;Web Security;2020 losses;ACID;Agari;average wire transfer;BEC;BEC attacks;Business Email Compromise;cybercriminals;global attacks;money mules;Nigera;scam;social engineering
Slug: bec-attacks-nigeria-no-longer-the-epicenter-as-losses-top-26b

[Source](https://threatpost.com/bec-attacks-nigeria-losses-snowball/160118/){:target="_blank" rel="noopener"}

> BEC fraudsters now have bases of operation across at least 39 counties and are responsible for $26 billion in losses annually -- and growing. [...]
