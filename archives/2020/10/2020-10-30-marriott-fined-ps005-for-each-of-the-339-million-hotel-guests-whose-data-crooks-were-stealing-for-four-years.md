Title: Marriott fined £0.05 for each of the 339 million hotel guests whose data crooks were stealing for four years
Date: 2020-10-30T14:08:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: marriott-fined-ps005-for-each-of-the-339-million-hotel-guests-whose-data-crooks-were-stealing-for-four-years

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/30/marriott_starwood_hack_fine_just_18_4bn/){:target="_blank" rel="noopener"}

> UK watchdog's mooted £99m penalty comes in at just £18.4m Your name, address, phone number, email address, passport number, date of birth, and sex are worth just £0.05 in the eyes of the UK Information Commissioner's Office, which has fined Marriott £18.4m after 339 million people's data was stolen from the hotel chain.... [...]
