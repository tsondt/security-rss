Title: The rise of fearware and how to fight back
Date: 2020-10-14T18:30:14+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: the-rise-of-fearware-and-how-to-fight-back

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/14/fearware_how_to_fight_back/){:target="_blank" rel="noopener"}

> A new kind of email filtering protects against fraud Sponsored We've had malware, ransomware, and spyware. Now, prepare yourself for the latest in a litany of online nasties: fearware. Thanks to the pandemic, cybercriminals are finding new and more sophisticated ways to fleece us – and it's going to take a new approach to pinpoint, stop, catch and fight back against those attacks.... [...]
