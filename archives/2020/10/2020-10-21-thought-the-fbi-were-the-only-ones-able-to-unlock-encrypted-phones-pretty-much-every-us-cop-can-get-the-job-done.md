Title: Thought the FBI were the only ones able to unlock encrypted phones? Pretty much every US cop can get the job done
Date: 2020-10-21T23:34:43+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: thought-the-fbi-were-the-only-ones-able-to-unlock-encrypted-phones-pretty-much-every-us-cop-can-get-the-job-done

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/21/us_phone_cracking/){:target="_blank" rel="noopener"}

> Massive public records request reveals scale of warrantless surveillance Never mind the Feds. American police forces routinely "circumvent most security features" in smartphones to extract mountains of personal information, according to a report that details the massive, ubiquitous cracking of devices by cops.... [...]
