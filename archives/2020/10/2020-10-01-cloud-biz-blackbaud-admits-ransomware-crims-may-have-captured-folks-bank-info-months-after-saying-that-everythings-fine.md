Title: Cloud biz Blackbaud admits ransomware crims may have captured folks' bank info, months after saying that everything's fine
Date: 2020-10-01T20:59:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: cloud-biz-blackbaud-admits-ransomware-crims-may-have-captured-folks-bank-info-months-after-saying-that-everythings-fine

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/01/blackbaud_ransomeware_data/){:target="_blank" rel="noopener"}

> The same lot who bought off crooks in May but kept quiet till July +Comment Blackbaud, the cloud CRM provider whose execs bought off ransomware crooks in exchange for a pinky promise that stolen data would not be misused, has now confessed that customers' bank account information may have been taken from its servers by the criminals.... [...]
