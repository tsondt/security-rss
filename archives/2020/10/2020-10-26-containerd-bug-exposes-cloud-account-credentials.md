Title: Containerd Bug Exposes Cloud Account Credentials
Date: 2020-10-26T17:12:13+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Bug;cloud accounts;container image-pulling;containerd;credential leak;CVE-2020-15157;google compute platform;host registry;security vulnerability
Slug: containerd-bug-exposes-cloud-account-credentials

[Source](https://threatpost.com/containerd-bug-cloud-account-credentials/160546/){:target="_blank" rel="noopener"}

> The flaw (CVE-2020-15157) is located in the container image-pulling process. [...]
