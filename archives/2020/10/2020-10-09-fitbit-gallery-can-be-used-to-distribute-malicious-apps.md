Title: Fitbit gallery can be used to distribute malicious apps
Date: 2020-10-09T09:08:12-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: fitbit-gallery-can-be-used-to-distribute-malicious-apps

[Source](https://www.bleepingcomputer.com/news/security/fitbit-gallery-can-be-used-to-distribute-malicious-apps/){:target="_blank" rel="noopener"}

> A security researcher discovered that malicious apps for FitBit devices can be uploaded to the legitimate FitBit domain and users can install them from private links. [...]
