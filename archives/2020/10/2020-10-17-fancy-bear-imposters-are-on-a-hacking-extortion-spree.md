Title: Fancy Bear imposters are on a hacking extortion spree
Date: 2020-10-17T12:00:56+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;extortion;Fancy Bear;hacking
Slug: fancy-bear-imposters-are-on-a-hacking-extortion-spree

[Source](https://arstechnica.com/?p=1715454){:target="_blank" rel="noopener"}

> Enlarge Ransomware attacks that tear through corporate networks can bring massive organizations to their knees. But even as these hacks reach new popularity highs —and new ethical lows—among attackers, it's not the only technique criminals are using to shake down corporate victims. A new wave of attacks relies instead on digital extortion—with a side of impersonation. On Wednesday, the Web security firm Radware published extortion notes that had been sent to a variety of companies around the world. In each of them, the senders purport to be from the North Korean government hackers Lazarus Group, or APT38, and Russian state-backed [...]
