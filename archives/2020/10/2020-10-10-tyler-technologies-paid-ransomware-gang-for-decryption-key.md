Title: Tyler Technologies paid ransomware gang for decryption key
Date: 2020-10-10T10:05:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: tyler-technologies-paid-ransomware-gang-for-decryption-key

[Source](https://www.bleepingcomputer.com/news/security/tyler-technologies-paid-ransomware-gang-for-decryption-key/){:target="_blank" rel="noopener"}

> Tyler Technologies has paid a ransom for a decryption key to recover files encrypted in a recent ransomware attack. [...]
