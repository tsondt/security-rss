Title: Britannia should rule the (cyber) waves, minister tells Singapore event in bid to drum up Commonwealth support
Date: 2020-10-12T08:30:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: britannia-should-rule-the-cyber-waves-minister-tells-singapore-event-in-bid-to-drum-up-commonwealth-support

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/12/uk_commonwealth_cyber_exports_speech/){:target="_blank" rel="noopener"}

> Rhetoric for the post-Brexit era but will actions follow words? Comment A UK government minister has called for the country to "shape the standards of new technology" in a speech aimed at drumming up Commonwealth support for a cyber "leadership" role for post-Brexit Britain.... [...]
