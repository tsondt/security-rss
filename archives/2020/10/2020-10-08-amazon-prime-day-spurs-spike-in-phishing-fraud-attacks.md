Title: Amazon Prime Day Spurs Spike in Phishing, Fraud Attacks
Date: 2020-10-08T13:00:43+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security;amazon;Amazon Prime;Bolster Research;consumers;COVID-19;Credential Theft;malicious websites;online retailers;Online Security;online shopping;Phishing;Prime Day
Slug: amazon-prime-day-spurs-spike-in-phishing-fraud-attacks

[Source](https://threatpost.com/amazon-prime-day-spurs-spike-in-phishing-fraud-attacks/159960/){:target="_blank" rel="noopener"}

> A spike in phishing and malicious websites aimed at defrauding Amazon.com customers aim to make Prime Day a field day for hackers. [...]
