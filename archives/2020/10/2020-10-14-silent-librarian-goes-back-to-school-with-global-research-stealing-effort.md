Title: Silent Librarian Goes Back to School with Global Research-Stealing Effort
Date: 2020-10-14T16:52:09+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Web Security;colleges;Credential Theft;Credentials;global;Irán;Iranian hackers;library portals;Malwarebytes;Phishing;Research;Sanctions;silent librarian;stealing;universities
Slug: silent-librarian-goes-back-to-school-with-global-research-stealing-effort

[Source](https://threatpost.com/silent-librarian-school-research-stealing/160099/){:target="_blank" rel="noopener"}

> The Iranian hacker group is targeting universities in 12 countries. [...]
