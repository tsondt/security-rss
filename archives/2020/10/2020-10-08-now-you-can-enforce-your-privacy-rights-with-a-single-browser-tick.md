Title: Now you can enforce your privacy rights with a single browser tick
Date: 2020-10-08T11:15:35+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;browsers;do not track;global privacy control;privacy
Slug: now-you-can-enforce-your-privacy-rights-with-a-single-browser-tick

[Source](https://arstechnica.com/?p=1712604){:target="_blank" rel="noopener"}

> Enlarge (credit: Global Privacy Control ) Anyone who remembers Do Not Track—the initiative that was supposed to allow browser users to reclaim their privacy on the Web—knows it was a failure. Not only did websites ignore it, using it arguably made people less private because it made them stick out. Now, privacy advocates are back with a new specification, and this time they’ve brought the lawyers. Under the hood, the specification, known as Global Privacy Control, works pretty much the same way Do Not Track did. A small HTTP header informs sites that a visitor doesn’t want their data sold. [...]
