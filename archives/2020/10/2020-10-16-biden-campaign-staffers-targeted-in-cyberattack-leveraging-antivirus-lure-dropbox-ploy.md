Title: Biden Campaign Staffers Targeted in Cyberattack Leveraging Antivirus Lure, Dropbox Ploy
Date: 2020-10-16T20:00:43+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Government;Vulnerabilities;Web Security;credential;cyberattack;Donald Trump;dropbox;election security;google;Google Threat Analysis Group;Joe biden;McAfee;Phishing;Spear Phishing;US presidential election
Slug: biden-campaign-staffers-targeted-in-cyberattack-leveraging-antivirus-lure-dropbox-ploy

[Source](https://threatpost.com/biden-campaign-staffers-targeted-in-cyberattack-leveraging-anti-virus-lure-dropbox-ploy/160234/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group sheds more light on targeted credential phishing and malware attacks on the staff of Joe Biden's presidential campaign. [...]
