Title: Boom! Hacked page on mobile phone website is stealing customers’ card data
Date: 2020-10-06T00:20:59+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;eCommerce;payment cards;skimmers;websites
Slug: boom-hacked-page-on-mobile-phone-website-is-stealing-customers-card-data

[Source](https://arstechnica.com/?p=1712072){:target="_blank" rel="noopener"}

> Enlarge / Computer hacker character stealing money online. Vector flat cartoon illustration (credit: GettyImages ) If you’re in the market for a new mobile phone plan, it’s best to avoid turning to Boom! Mobile. That is, unless you don’t mind your sensitive payment card data being sent to criminals in an attack that remained ongoing in the last few hours. According to researchers from security firm Malwarebytes, Boom! Mobile’s boom.us website is infected with a malicious script that skims payment card data and sends it to a server under the control of a criminal group researchers have dubbed Fullz House. [...]
