Title: Hackers hack Hackney: Local government cries 'cyberattack' while UK infosec officials rush to figure out what happened
Date: 2020-10-13T12:32:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: hackers-hack-hackney-local-government-cries-cyberattack-while-uk-infosec-officials-rush-to-figure-out-what-happened

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/13/hackney_council_hacked_hackers_cyberhack/){:target="_blank" rel="noopener"}

> Check bank accounts, don't open council emails, you know how this goes Hackney Council in East London has declared that it was hit by a "cyberattack" – but both the authority and officials from the National Cyber Security Centre (NCSC) remain tight-lipped about what actually happened.... [...]
