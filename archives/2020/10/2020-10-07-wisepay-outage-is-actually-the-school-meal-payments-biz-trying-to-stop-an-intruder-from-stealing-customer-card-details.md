Title: Wisepay 'outage' is actually the school meal payments biz trying to stop an intruder from stealing customer card details
Date: 2020-10-07T15:01:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: wisepay-outage-is-actually-the-school-meal-payments-biz-trying-to-stop-an-intruder-from-stealing-customer-card-details

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/07/wisepay_outage_was_cyber_attack/){:target="_blank" rel="noopener"}

> We pulled entire website to halt attack, says spokesman UK cashless school payments firm Wisepay has pulled its website offline after spotting a miscreant trying to spoof its card payment page.... [...]
