Title: Detecting Deep Fakes with a Heartbeat
Date: 2020-10-01T11:19:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;biometrics;deep fake
Slug: detecting-deep-fakes-with-a-heartbeat

[Source](https://www.schneier.com/blog/archives/2020/10/detecting-deep-fakes-with-a-heartbeat.html){:target="_blank" rel="noopener"}

> Researchers can detect deep fakes because they don’t convincingly mimic human blood circulation in the face: In particular, video of a person’s face contains subtle shifts in color that result from pulses in blood circulation. You might imagine that these changes would be too minute to detect merely from a video, but viewing videos that have been enhanced to exaggerate these color shifts will quickly disabuse you of that notion. This phenomenon forms the basis of a technique called photoplethysmography, or PPG for short, which can be used, for example, to monitor newborns without having to attach anything to a [...]
