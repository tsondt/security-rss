Title: Software engineer leaked UK missile system secrets and refused to hand cops his passwords, Old Bailey told
Date: 2020-10-28T17:31:59+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: software-engineer-leaked-uk-missile-system-secrets-and-refused-to-hand-cops-his-passwords-old-bailey-told

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/28/simon_finch_official_secrets_passwords_trial/){:target="_blank" rel="noopener"}

> Revelations triggered by previous police abuse, court hears A former BAE Systems software engineer who allegedly leaked top-secret details about a frontline missile system also ignored orders from police to hand over passwords to his electronic devices, a court has heard.... [...]
