Title: Google Boots 21 Bogus Gaming Apps from Play Marketplace
Date: 2020-10-27T12:10:58+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;Mobile Security;Adware;Android;bad apps;Developers;gaming apps;google;google play;hiddenads;malware;mobile apps;Mobile security
Slug: google-boots-21-bogus-gaming-apps-from-play-marketplace

[Source](https://threatpost.com/google-boots-apps-from-play/160585/){:target="_blank" rel="noopener"}

> Android apps packed with malware from HiddenAds family downloaded 8 million times from the online marketplace. [...]
