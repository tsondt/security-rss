Title: Imagine running a dating app and being told accounts could be easily hijacked. How did that feel, Grindr?
Date: 2020-10-03T09:08:08+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: imagine-running-a-dating-app-and-being-told-accounts-could-be-easily-hijacked-how-did-that-feel-grindr

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/03/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: A little reminder to not pay off ransomware crooks In brief LGBTQ dating site Grindr has squashed a security bug in its website that could have been trivially exploited to hijack anyone's profile using just the victim's email address.... [...]
