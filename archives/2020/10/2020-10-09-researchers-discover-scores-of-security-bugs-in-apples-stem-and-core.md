Title: Researchers discover scores of security bugs in Apple’s stem and core
Date: 2020-10-09T14:28:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: researchers-discover-scores-of-security-bugs-in-apples-stem-and-core

[Source](https://portswigger.net/daily-swig/researchers-discover-scores-of-security-bugs-in-apples-stem-and-core){:target="_blank" rel="noopener"}

> Bug bounty hunters receive impressive security reward payout from tech giant [...]
