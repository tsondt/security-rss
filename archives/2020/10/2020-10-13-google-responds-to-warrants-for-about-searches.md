Title: Google Responds to Warrants for “About” Searches
Date: 2020-10-13T11:20:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Edward Snowden;Google;national security policy;NSA;privacy;searches;surveillance
Slug: google-responds-to-warrants-for-about-searches

[Source](https://www.schneier.com/blog/archives/2020/10/google-responds-to-warrants-for-about-searches.html){:target="_blank" rel="noopener"}

> One of the things we learned from the Snowden documents is that the NSA conducts “about” searches. That is, searches based on activities and not identifiers. A normal search would be on a name, or IP address, or phone number. An about search would something like “show me anyone that has used this particular name in a communications,” or “show me anyone who was at this particular location within this time frame.” These searches are legal when conducted for the purpose of foreign surveillance, but the worry about using them domestically is that they are unconstitutionally broad. After all, the [...]
