Title: Google’s Waze Can Allow Hackers to Identify and Track Users
Date: 2020-10-20T10:48:45+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Privacy;Vulnerabilities;API;bug bounty;crowd-sourcing;flaw;google;malgregator;mobile app security;mobile apps;navigation;Peter Gasper;security research;vulnerability;Waze
Slug: googles-waze-can-allow-hackers-to-identify-and-track-users

[Source](https://threatpost.com/googles-waze-track-users/160332/){:target="_blank" rel="noopener"}

> The company already patched an API flaw that allowed a security researcher to use the app to find the real identity of drivers using it. [...]
