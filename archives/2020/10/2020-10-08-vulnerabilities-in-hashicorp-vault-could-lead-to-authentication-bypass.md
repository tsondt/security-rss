Title: Vulnerabilities in HashiCorp Vault could lead to authentication bypass
Date: 2020-10-08T10:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: vulnerabilities-in-hashicorp-vault-could-lead-to-authentication-bypass

[Source](https://portswigger.net/daily-swig/vulnerabilities-in-hashicorp-vault-could-lead-to-authentication-bypass){:target="_blank" rel="noopener"}

> Software could expose users’ secrets when configured with AWS and Google Cloud [...]
