Title: Microsoft warns of ongoing attacks using Windows Zerologon flaw
Date: 2020-10-29T16:46:40
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-warns-of-ongoing-attacks-using-windows-zerologon-flaw

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-ongoing-attacks-using-windows-zerologon-flaw/){:target="_blank" rel="noopener"}

> Microsoft today warned that threat actors are continuing to actively exploit systems unpatched against the ZeroLogon privilege escalation vulnerability in the Netlogon Remote Protocol (MS-NRPC). [...]
