Title: Software AG hit with ransomware: Crooks leak staffers' passports, want millions for stolen files
Date: 2020-10-09T17:40:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: software-ag-hit-with-ransomware-crooks-leak-staffers-passports-want-millions-for-stolen-files

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/09/software_ag_ransomware/){:target="_blank" rel="noopener"}

> There's only one way to stop this, says counter-ransomware bod Software AG has seemingly been hit by ransomware, with the German IT giant itself telling the Euro nation's stock market it had been “affected by a malware attack.”... [...]
