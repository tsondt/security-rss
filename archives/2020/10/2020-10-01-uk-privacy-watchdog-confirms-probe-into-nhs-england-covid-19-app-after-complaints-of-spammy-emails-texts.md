Title: UK privacy watchdog confirms probe into NHS England COVID-19 app after complaints of spammy emails, texts
Date: 2020-10-01T09:05:11+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: uk-privacy-watchdog-confirms-probe-into-nhs-england-covid-19-app-after-complaints-of-spammy-emails-texts

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/01/ico_complaints_covid_app_spam/){:target="_blank" rel="noopener"}

> Officials say it was a 'matter of public health importance to encourage people to download the app' Exclusive Britain's Information Commissioner's Office (ICO) has confirmed it is investigating grumbles about heavy-handed marketing emails and texts promoting the NHS COVID-19 contact-tracing app in England.... [...]
