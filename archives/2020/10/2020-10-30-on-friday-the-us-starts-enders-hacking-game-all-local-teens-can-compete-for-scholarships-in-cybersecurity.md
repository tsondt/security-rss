Title: On Friday the US starts Ender's hacking game: All local teens can compete for scholarships in cybersecurity
Date: 2020-10-30T10:00:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: on-friday-the-us-starts-enders-hacking-game-all-local-teens-can-compete-for-scholarships-in-cybersecurity

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/30/cyberstart_hacking_challenge/){:target="_blank" rel="noopener"}

> CyberStart America challenge aims to find talented network defenders Starting on Friday, US high school students can register to participate in CyberStart America, an online puzzle-solving game designed to identify cybersecurity talent and qualify participants for an opportunity to compete in the National Cyber Scholarship Competition next year.... [...]
