Title: Linguists team up with computer scientists to spot trends on cybercrime forums
Date: 2020-10-19T13:33:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: linguists-team-up-with-computer-scientists-to-spot-trends-on-cybercrime-forums

[Source](https://portswigger.net/daily-swig/linguists-team-up-with-computer-scientists-to-spot-trends-on-cybercrime-forums){:target="_blank" rel="noopener"}

> Cambridge University boffins apply natural language processing to sort out the slang on HackForums [...]
