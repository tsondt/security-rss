Title: Google employees personal info exposed in law firm data breach
Date: 2020-10-26T19:34:52
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google
Slug: google-employees-personal-info-exposed-in-law-firm-data-breach

[Source](https://www.bleepingcomputer.com/news/security/google-employees-personal-info-exposed-in-law-firm-data-breach/){:target="_blank" rel="noopener"}

> Immigration law firm Fragomen, Del Rey, Bernsen & Loewy, LLP has disclosed a data breach that exposed current and former Google employees' personal information. [...]
