Title: Online proctor service ProctorTrack disables service after hack
Date: 2020-10-14T21:27:46
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: online-proctor-service-proctortrack-disables-service-after-hack

[Source](https://www.bleepingcomputer.com/news/security/online-proctor-service-proctortrack-disables-service-after-hack/){:target="_blank" rel="noopener"}

> The online proctoring service ProctorTrack has disabled access to their service after its parent company was hacked. [...]
