Title: Microsoft would love to hear about 'critical bugs' in .NET 5.0 ahead of the 'unified' platform's November launch
Date: 2020-10-15T08:04:12+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: microsoft-would-love-to-hear-about-critical-bugs-in-net-50-ahead-of-the-unified-platforms-november-launch

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/15/microsoft_emits_net_50_go/){:target="_blank" rel="noopener"}

> Dare ye use RC2 in production? The version of.NET formerly called.NET Core is crawling closer to its November launch with.NET 5.0 Release Candidate 2 packing updates for key frameworks ASP.NET Core and Entity Framework Core, and a go-live licence.... [...]
