Title: JWT Heartbreaker offers remedy for weak JSON web tokens
Date: 2020-10-09T11:05:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: jwt-heartbreaker-offers-remedy-for-weak-json-web-tokens

[Source](https://portswigger.net/daily-swig/jwt-heartbreaker-offers-remedy-for-weak-json-web-tokens){:target="_blank" rel="noopener"}

> Security audits found that 95% of JWT tokens were signed but not encrypted [...]
