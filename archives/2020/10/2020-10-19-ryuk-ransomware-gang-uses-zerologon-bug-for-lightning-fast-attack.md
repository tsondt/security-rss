Title: Ryuk Ransomware Gang Uses Zerologon Bug for Lightning-Fast Attack
Date: 2020-10-19T16:36:00+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Vulnerabilities;Web Security;Active Directory;attack analysis;bazar loader;cobalt strike;CVE-2020-1472;dfir report;five hours;initial phishing email;malware;privilege escalation;ryuk;zerologon
Slug: ryuk-ransomware-gang-uses-zerologon-bug-for-lightning-fast-attack

[Source](https://threatpost.com/ryuk-ransomware-gang-zerologon-lightning-attack/160286/){:target="_blank" rel="noopener"}

> Researchers said the group was able to move from initial phish to full domain-wide encryption in just five hours. [...]
