Title: Collaborative bug hunting ‘could be very lucrative’ – security pro Alex Chapman on the future of ethical hacking
Date: 2020-10-26T15:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: collaborative-bug-hunting-could-be-very-lucrative-security-pro-alex-chapman-on-the-future-of-ethical-hacking

[Source](https://portswigger.net/daily-swig/collaborative-bug-hunting-could-be-very-lucrative-security-pro-alex-chapman-on-the-future-of-ethical-hacking){:target="_blank" rel="noopener"}

> ‘Persistence is key, and so is not expecting a huge payout on day one’ [...]
