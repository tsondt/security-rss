Title: Brooklyn & Vermont hospitals are latest Ryuk ransomware victims
Date: 2020-10-29T19:26:56
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: brooklyn-vermont-hospitals-are-latest-ryuk-ransomware-victims

[Source](https://www.bleepingcomputer.com/news/security/brooklyn-and-vermont-hospitals-are-latest-ryuk-ransomware-victims/){:target="_blank" rel="noopener"}

> Wyckoff Heights Medical Center in Brooklyn and the University of Vermont Health Network are the latest victims of the Ryuk ransomware attack spree covering the healthcare industry across the U.S. [...]
