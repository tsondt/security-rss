Title: Facebook: A Top Launching Pad For Phishing Attacks
Date: 2020-10-20T16:54:57+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Facebook;Vulnerabilities;Web Security;amazon;apple;blacklisted acts;block applications;cybercriminals;Facebook. phishing;Kaspersky;Netflix;phishing attack;small and medium business;top phishing apps;WhatsApp
Slug: facebook-a-top-launching-pad-for-phishing-attacks

[Source](https://threatpost.com/facebook-launching-pad-phishing-attacks/160351/){:target="_blank" rel="noopener"}

> Amazon, Apple, Netflix, Facebook and WhatsApp are top brands leveraged by cybercriminals in phishing and fraud attacks - including a recent strike on a half-million Facebook users. [...]
