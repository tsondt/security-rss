Title: Coinbase phishing hijacks Microsoft 365 accounts via OAuth app
Date: 2020-10-19T15:37:26
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: coinbase-phishing-hijacks-microsoft-365-accounts-via-oauth-app

[Source](https://www.bleepingcomputer.com/news/microsoft/coinbase-phishing-hijacks-microsoft-365-accounts-via-oauth-app/){:target="_blank" rel="noopener"}

> A new phishing campaign uses a Coinbase-themed email to install an Office 365 consent app that gives attackers access to a victim's email. [...]
