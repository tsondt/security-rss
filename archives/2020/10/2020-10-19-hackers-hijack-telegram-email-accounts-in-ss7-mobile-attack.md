Title: Hackers hijack Telegram, email accounts in SS7 mobile attack
Date: 2020-10-19T12:22:10
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: hackers-hijack-telegram-email-accounts-in-ss7-mobile-attack

[Source](https://www.bleepingcomputer.com/news/security/hackers-hijack-telegram-email-accounts-in-ss7-mobile-attack/){:target="_blank" rel="noopener"}

> Hackers with access to the Signaling System 7 (SS7) used for connecting mobile networks across the world were able to gain access to Telegram messenger and email data of high-profile individuals in the cryptocurrency business. [...]
