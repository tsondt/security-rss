Title: Wisc. GOP’s $2.3M MAGA Hat Debacle Showcases Fraud Concerns
Date: 2020-10-30T20:08:49+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Web Security;BEC;Business Email Compromise;cyberattack;Cyberattacks;cybercriminal;Election;election security;Fraud;hats;maga;MAGA hat invoices;MAGA hat vendors;Phishing;politics;presidential election;Trump campaign;Wisconsin GOP;Wisconsin Republicans
Slug: wisc-gops-23m-maga-hat-debacle-showcases-fraud-concerns

[Source](https://threatpost.com/wisc-gop-maga-hat-fraud/160806/){:target="_blank" rel="noopener"}

> Scammers bilked Wisconsin Republicans out of $2.3 million in a basic BEC scam — and anyone working on the upcoming election needs to pay attention. [...]
