Title: Hacker is selling 34 million user records stolen from 17 companies
Date: 2020-10-31T12:14:27
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hacker-is-selling-34-million-user-records-stolen-from-17-companies

[Source](https://www.bleepingcomputer.com/news/security/hacker-is-selling-34-million-user-records-stolen-from-17-companies/){:target="_blank" rel="noopener"}

> A threat actor is selling account databases containing an aggregate total of 34 million user records that they claim were stolen from seventeen companies during data breaches. [...]
