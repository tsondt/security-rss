Title: Apple provides technical steer on Face ID, Touch ID authentication for websites
Date: 2020-10-21T15:51:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: apple-provides-technical-steer-on-face-id-touch-id-authentication-for-websites

[Source](https://portswigger.net/daily-swig/apple-provides-technical-steer-on-face-id-touch-id-authentication-for-websites){:target="_blank" rel="noopener"}

> Introduction of technology dubbed ‘a huge leap forward for authentication on the web’ [...]
