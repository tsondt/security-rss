Title: DOJ Charges 6 Sandworm APT Members in NotPetya Cyberattacks
Date: 2020-10-19T19:10:47+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;apt;cyberattack;GRU;Justice Department;NotPetya;olympics cyberattack;sandworm;Ukraine power grid cyberattack;US DoJ
Slug: doj-charges-6-sandworm-apt-members-in-notpetya-cyberattacks

[Source](https://threatpost.com/doj-charges-6-sandworm-apt-members-in-notpetya-cyberattacks/160304/){:target="_blank" rel="noopener"}

> DOJ charges six Russian nationals for their alleged part in the NotPetya, Ukraine power grid and Olympics cyberattacks. [...]
