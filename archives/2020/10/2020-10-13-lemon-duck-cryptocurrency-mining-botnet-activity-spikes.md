Title: Lemon Duck Cryptocurrency-Mining Botnet Activity Spikes
Date: 2020-10-13T20:41:52+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;AMD;attack spike;bluekeep;botnet;brute force;Cisco Talos;COVID-19;Cryptocurrency;cryptomining;DNS;GTX;lemon duck;Linux;Monero;Nvidia;RDP;Windows
Slug: lemon-duck-cryptocurrency-mining-botnet-activity-spikes

[Source](https://threatpost.com/lemon-duck-cryptocurrency-botnet/160046/){:target="_blank" rel="noopener"}

> Researchers warn of a spike in the cryptocurrency-mining botnet since August 2020. [...]
