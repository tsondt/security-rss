Title: Experian vows to drag UK's Information Commissioner's Office to court after being told off for data-slurping practices
Date: 2020-10-28T13:29:54+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: experian-vows-to-drag-uks-information-commissioners-office-to-court-after-being-told-off-for-data-slurping-practices

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/28/experian_ico_marketing_data_investigation/){:target="_blank" rel="noopener"}

> Credit reference agency recycled personal details for marketing purposes, says regulator Experian has been rapped over the knuckles by the UK's Information Commissioner's Office (ICO) after it discovered the credit reference agency was trading "millions" of people's data for marketing purposes.... [...]
