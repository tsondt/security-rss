Title: Microsoft IE Browser Death March Hastens
Date: 2020-10-26T22:26:57+00:00
Author: Tom Spring
Category: Threatpost
Tags: Web Security;browser support;Chromium;end of support;end-of-life;IE;IE-to-Edge;Internet Explorer 11;Microsoft Edge;Microsoft Edge 87;Security Bugs;Security issues;transition;vulnerabilities;Windows XP
Slug: microsoft-ie-browser-death-march-hastens

[Source](https://threatpost.com/ie-browser-death-march/160571/){:target="_blank" rel="noopener"}

> Internet Explorer redirects more traffic to Edge Chromium browser as Microsoft warns of the upcoming demise of the once dominant browser. [...]
