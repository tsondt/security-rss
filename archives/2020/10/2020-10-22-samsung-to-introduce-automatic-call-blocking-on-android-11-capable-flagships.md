Title: Samsung to introduce automatic call blocking on Android 11-capable flagships
Date: 2020-10-22T11:44:20+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: samsung-to-introduce-automatic-call-blocking-on-android-11-capable-flagships

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/22/samsung_to_introduce_automatic_call/){:target="_blank" rel="noopener"}

> Yeah I've heard you were in a car accident that wasn't your fault. Is that right? *Click* Samsung phones will soon come with automatic spam call blocking. The feature, which is part of Samsung Smart Call, will debut on the Galaxy Note20 and will roll out to all new devices released after 2020.... [...]
