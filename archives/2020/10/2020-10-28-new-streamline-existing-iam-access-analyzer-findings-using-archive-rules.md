Title: New! Streamline existing IAM Access Analyzer findings using archive rules
Date: 2020-10-28T18:13:32+00:00
Author: Andrea Nedic
Category: AWS Security
Tags: AWS IAM Access Analyzer;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Access management;Automated reasoning;AWS IAM;IAM Access Analyzer;least privilege;Security Blog
Slug: new-streamline-existing-iam-access-analyzer-findings-using-archive-rules

[Source](https://aws.amazon.com/blogs/security/new-streamline-existing-iam-access-analyzer-findings-using-archive-rules/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) Access Analyzer generates comprehensive findings to help you identify resources that grant public and cross-account access. Now, you can also apply archive rules to existing findings, so you can better manage findings and focus on the findings that need your attention most. You can think of archive rules as [...]
