Title: Tiki Wiki authentication bypass flaw gives attackers full control of websites, intranets
Date: 2020-10-26T14:09:57+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: tiki-wiki-authentication-bypass-flaw-gives-attackers-full-control-of-websites-intranets

[Source](https://portswigger.net/daily-swig/tiki-wiki-authentication-bypass-flaw-gives-attackers-full-control-of-websites-intranets){:target="_blank" rel="noopener"}

> Brute-force exploit hands over the keys to CMS admin accounts [...]
