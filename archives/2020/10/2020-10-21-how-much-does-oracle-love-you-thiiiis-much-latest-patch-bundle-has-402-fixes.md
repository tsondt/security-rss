Title: How much does Oracle love you? Thiiiis much: Latest patch bundle has 402 fixes
Date: 2020-10-21T18:32:31+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: how-much-does-oracle-love-you-thiiiis-much-latest-patch-bundle-has-402-fixes

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/21/oracle_october_patches/){:target="_blank" rel="noopener"}

> How many times do you want to read the CVSS rating 9.8 today? Oracle has released its final quarterly batch of patches for the year for security flaws in its products. The total this time? 402 fixes, the bulk of which are rated critical in terms of severity.... [...]
