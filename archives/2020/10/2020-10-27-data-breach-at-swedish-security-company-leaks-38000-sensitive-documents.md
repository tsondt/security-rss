Title: Data breach at Swedish security company leaks 38,000 sensitive documents
Date: 2020-10-27T17:40:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-at-swedish-security-company-leaks-38000-sensitive-documents

[Source](https://portswigger.net/daily-swig/data-breach-at-swedish-security-company-leaks-38-000-sensitive-documents){:target="_blank" rel="noopener"}

> Blueprints of government buildings and bank vaults among exposed files [...]
