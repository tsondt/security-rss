Title: Here's US Homeland Security collaring a suspected arsonist after asking Google for the IP addresses of folks who made a specific search
Date: 2020-10-09T12:04:07+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: heres-us-homeland-security-collaring-a-suspected-arsonist-after-asking-google-for-the-ip-addresses-of-folks-who-made-a-specific-search

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/09/google_search_arrest/){:target="_blank" rel="noopener"}

> Don't worry, says the internet giant, this doesn't happen too often An unsealed warrant in a case involving alleged pedophile R&B star R. Kelly has shown how the Feds can get Google to hand over the details of people who make specific web search queries.... [...]
