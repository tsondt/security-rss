Title: Norway says Russian hackers were behind August Parliament attack
Date: 2020-10-13T11:48:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: norway-says-russian-hackers-were-behind-august-parliament-attack

[Source](https://www.bleepingcomputer.com/news/security/norway-says-russian-hackers-were-behind-august-parliament-attack/){:target="_blank" rel="noopener"}

> Norway's Minister of Foreign Affairs Ine Eriksen Søreide today said that Russia is behind the August 2020 cyber-attack on the Norwegian Parliament (Stortinget). [...]
