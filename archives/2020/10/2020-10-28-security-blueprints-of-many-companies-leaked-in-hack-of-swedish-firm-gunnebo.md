Title: Security Blueprints of Many Companies Leaked in Hack of Swedish Firm Gunnebo
Date: 2020-10-28T16:58:55+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Ransomware;Dagens Nyheter;Gunnebo Group breach;Hold Security;Intel 471;Linus Larsson;Mark Arena;ransomware;Rasmus Jansson;RDP;rEvil;Sodinokibi;Stefan Syrén
Slug: security-blueprints-of-many-companies-leaked-in-hack-of-swedish-firm-gunnebo

[Source](https://krebsonsecurity.com/2020/10/security-blueprints-of-many-companies-leaked-in-hack-of-swedish-firm-gunnebo/){:target="_blank" rel="noopener"}

> In March 2020, KrebsOnSecurity alerted Swedish security giant Gunnebo Group that hackers had broken into its network and sold the access to a criminal group which specializes in deploying ransomware. In August, Gunnebo said it had successfully thwarted a ransomware attack, but this week it emerged that the intruders stole and published online tens of thousands of sensitive documents — including schematics of client bank vaults and surveillance systems. The Gunnebo Group is a Swedish multinational company that provides physical security to a variety of customers globally, including banks, government agencies, airports, casinos, jewelry stores, tax agencies and even nuclear [...]
