Title: New IAMCTL tool compares multiple IAM roles and policies
Date: 2020-10-06T16:08:53+00:00
Author: Sudhir Reddy Maddulapally
Category: AWS Security
Tags: AWS Identity and Access Management (IAM);Expert (400);Security, Identity, & Compliance;AWS CLI;AWS IAM;Drift;governance;Policies;roles;Security Blog;Trust policy
Slug: new-iamctl-tool-compares-multiple-iam-roles-and-policies

[Source](https://aws.amazon.com/blogs/security/new-iamctl-tool-compares-multiple-iam-roles-and-policies/){:target="_blank" rel="noopener"}

> If you have multiple Amazon Web Services (AWS) accounts, and you have AWS Identity and Access Management (IAM) roles among those multiple accounts that are supposed to be similar, those roles can deviate over time from your intended baseline due to manual actions performed directly out-of-band called drift. As part of regular compliance checks, you [...]
