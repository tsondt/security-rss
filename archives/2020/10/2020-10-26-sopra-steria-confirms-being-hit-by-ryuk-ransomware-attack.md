Title: Sopra Steria confirms being hit by Ryuk ransomware attack
Date: 2020-10-26T09:54:50
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: sopra-steria-confirms-being-hit-by-ryuk-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/sopra-steria-confirms-being-hit-by-ryuk-ransomware-attack/){:target="_blank" rel="noopener"}

> French enterprise IT services company Sopra Steria confirmed today that they were hit with a Ryuk ransomware attack on October 20th, 2020. [...]
