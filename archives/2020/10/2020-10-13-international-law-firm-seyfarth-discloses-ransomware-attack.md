Title: International law firm Seyfarth discloses ransomware attack
Date: 2020-10-13T12:55:46-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Deals
Slug: international-law-firm-seyfarth-discloses-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/international-law-firm-seyfarth-discloses-ransomware-attack/){:target="_blank" rel="noopener"}

> International law firm Seyfarth Shaw announced on Monday that it was the victim of a ransomware attack over the weekend. [...]
