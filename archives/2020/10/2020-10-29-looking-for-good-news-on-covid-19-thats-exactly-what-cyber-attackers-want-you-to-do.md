Title: Looking for good news on COVID-19? That’s exactly what cyber attackers want you to do
Date: 2020-10-29T17:00:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: looking-for-good-news-on-covid-19-thats-exactly-what-cyber-attackers-want-you-to-do

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/29/how_i_would_attack_you_webcast/){:target="_blank" rel="noopener"}

> Let us show you how to outsmart them Webcast If you think cybercriminals and hackers are without a shred of empathy or human understanding, you’d be wrong.... [...]
