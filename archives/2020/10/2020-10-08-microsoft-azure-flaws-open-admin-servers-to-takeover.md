Title: Microsoft Azure Flaws Open Admin Servers to Takeover
Date: 2020-10-08T15:28:37+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Cloud Security;Hacks;Vulnerabilities;attack;Azure;azure app services;cloud;code execution;Microsoft;server side forgery request;server takeover;vulnerability
Slug: microsoft-azure-flaws-open-admin-servers-to-takeover

[Source](https://threatpost.com/microsoft-azure-flaws-servers-takeover/159965/){:target="_blank" rel="noopener"}

> Two flaws in Microsoft's cloud-based Azure App Services could have allowed server-side forgery request (SSFR) and remote code-execution attacks. [...]
