Title: National Cybersecurity Awareness Month: Security is for life, not just October
Date: 2020-10-30T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: national-cybersecurity-awareness-month-security-is-for-life-not-just-october

[Source](https://portswigger.net/daily-swig/national-cybersecurity-awareness-month-security-is-for-life-not-just-october){:target="_blank" rel="noopener"}

> ‘Applying a regular security regime is no longer a nice thing to do – it’s a compulsory thing to do,’ one expert warns [...]
