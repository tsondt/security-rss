Title: New security and privacy announcements for Google Workspace
Date: 2020-10-06T08:00:00+00:00
Author: Karthik Lakshminarayanan
Category: GCP Security
Tags: Identity & Security;Google Workspace
Slug: new-security-and-privacy-announcements-for-google-workspace

[Source](https://cloud.google.com/blog/products/workspace/announcing-new-security-and-privacy-updates-in-google-workspace/){:target="_blank" rel="noopener"}

> Today, we announced Google Workspace, which brings together everything you need to get anything done, now in one place. It’s never been more critical to protect the connections that Google Workspace enables everyday, and we’re constantly innovating to deliver the best in security. Our security features help you create flexible workspaces that scale, no matter what device or browser you are using. At the same time, we want to make sure security doesn’t get in the way of you accomplishing your goals, but rather helps you easily protect the data that's so critical to your organization. For this reason, today’s [...]
