Title: FBI: Hackers stole government source code via SonarQube instances
Date: 2020-10-27T11:35:41
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-hackers-stole-government-source-code-via-sonarqube-instances

[Source](https://www.bleepingcomputer.com/news/security/fbi-hackers-stole-government-source-code-via-sonarqube-instances/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) issued a flash alert warning of hackers stealing data from U.S. government agencies and enterprise organizations via insecure and internet-exposed SonarQube instances. [...]
