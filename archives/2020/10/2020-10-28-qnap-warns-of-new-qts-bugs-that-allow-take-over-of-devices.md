Title: QNAP warns of new QTS bugs that allow take over of devices
Date: 2020-10-28T11:13:45
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: qnap-warns-of-new-qts-bugs-that-allow-take-over-of-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-of-new-qts-bugs-that-allow-take-over-of-devices/){:target="_blank" rel="noopener"}

> QNAP today announced two vulnerabilities affecting QTS, the operating system powering its network-attached storage devices, that could allow running arbitrary commands. [...]
