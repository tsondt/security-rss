Title: Semgrep: Static code analysis tool helps ‘eliminate entire classes of vulnerabilities’
Date: 2020-10-29T12:06:11+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: semgrep-static-code-analysis-tool-helps-eliminate-entire-classes-of-vulnerabilities

[Source](https://portswigger.net/daily-swig/semgrep-static-code-analysis-tool-helps-eliminate-entire-classes-of-vulnerabilities){:target="_blank" rel="noopener"}

> After gathering traction on GitHub, open source security tool enjoys formal launch today [...]
