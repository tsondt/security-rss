Title: UHS restores hospital systems after Ryuk ransomware attack
Date: 2020-10-30T12:22:31
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: uhs-restores-hospital-systems-after-ryuk-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/uhs-restores-hospital-systems-after-ryuk-ransomware-attack/){:target="_blank" rel="noopener"}

> Universal Health Services (UHS), a Fortune 500 hospital and healthcare services provider, says that it has managed to restore systems after a September Ryuk ransomware attack. [...]
