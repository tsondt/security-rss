Title: Election Security: How Mobile Devices Are Shaping the Way We Work, Play and Vote
Date: 2020-10-28T13:32:55+00:00
Author: Hank Schless
Category: Threatpost
Tags: Government;InfoSec Insider;Mobile Security;Vulnerabilities;campaign texts;election security;fake texts;hank schless;infosec insider;Lookout;mobile attacks;mobile devices;mobile phishing;presidential election 2020;SMS messages;texts;tiktok;vote joe app
Slug: election-security-how-mobile-devices-are-shaping-the-way-we-work-play-and-vote

[Source](https://threatpost.com/mobile-devices-vote-election-security/160648/){:target="_blank" rel="noopener"}

> With the election just a week away, cybercriminals are ramping up mobile attacks on citizens under the guise of campaign communications. [...]
