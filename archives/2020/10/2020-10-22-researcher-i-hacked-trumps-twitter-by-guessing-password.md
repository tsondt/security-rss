Title: Researcher: I Hacked Trump’s Twitter by Guessing Password
Date: 2020-10-22T18:45:54+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Web Security;2FA;Dutch researcher;hack;Password;Trump;Trump hack;twitter;Two Factor Authentication;Victor Gevers;weak password
Slug: researcher-i-hacked-trumps-twitter-by-guessing-password

[Source](https://threatpost.com/researcher-hacked-trumps-twitter-password/160473/){:target="_blank" rel="noopener"}

> Trump’s weak Twitter password and lack of basic two-factor authentication protections made it shockingly simple to hack his account, Dutch security researcher Victor Gevers reported. [...]
