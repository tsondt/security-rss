Title: Russian Turla hackers breach European government organization
Date: 2020-10-28T14:46:08
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: russian-turla-hackers-breach-european-government-organization

[Source](https://www.bleepingcomputer.com/news/security/russian-turla-hackers-breach-european-government-organization/){:target="_blank" rel="noopener"}

> Russian-speaking hacking group Turla has hacked into the systems of an undisclosed European government organization according to a new Accenture Cyber Threat Intelligence (ACTI) report. [...]
