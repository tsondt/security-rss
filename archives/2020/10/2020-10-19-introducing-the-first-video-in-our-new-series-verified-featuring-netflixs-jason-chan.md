Title: Introducing the first video in our new series, Verified, featuring Netflix’s Jason Chan
Date: 2020-10-19T19:26:09+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Cloud security;cybersecurity;enterprise IT security;Jason Chan;Netflix;Privacy;remote workforce;Security Blog;security hiring;security talent;team-building;Zero Trust;Zero Trust Access
Slug: introducing-the-first-video-in-our-new-series-verified-featuring-netflixs-jason-chan

[Source](https://aws.amazon.com/blogs/security/introducing-first-video-new-series-verified-featuring-netflix-jason-chan/){:target="_blank" rel="noopener"}

> The year has been a profoundly different one for us all, and like many of you, I’ve been adjusting, both professionally and personally, to this “new normal.” Here at AWS we’ve seen an increase in customers looking for secure solutions to maintain productivity in an increased work-from-home world. We’ve also seen an uptick in requests [...]
