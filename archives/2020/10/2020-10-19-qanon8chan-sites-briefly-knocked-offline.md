Title: QAnon/8Chan Sites Briefly Knocked Offline
Date: 2020-10-19T04:03:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;The Coming Storm;8chan;8kun;CNServers;ddos-guard;Nick Lim;OrcaTech;QAnon;Ron Guilmette;Ryan McCully;Spartan Host Ltd.;VanwaTech
Slug: qanon8chan-sites-briefly-knocked-offline

[Source](https://krebsonsecurity.com/2020/10/qanon-8chan-sites-briefly-knocked-offline/){:target="_blank" rel="noopener"}

> A phone call to an Internet provider in Oregon on Sunday evening was all it took to briefly sideline multiple websites related to 8chan/8kun — a controversial online image board linked to several mass shootings — and QAnon, the far-right conspiracy theory which holds that a cabal of Satanic pedophiles is running a global child sex-trafficking ring and plotting against President Donald Trump. Following a brief disruption, the sites have come back online with the help of an Internet company based in St. Petersburg, Russia. The IP address range in the upper-right portion of this map of QAnon and 8kun-related [...]
