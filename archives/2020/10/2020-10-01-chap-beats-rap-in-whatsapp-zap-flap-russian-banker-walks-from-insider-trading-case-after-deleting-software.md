Title: Chap beats rap in WhatsApp zap flap: Russian banker walks from insider trading case after deleting software
Date: 2020-10-01T06:53:13+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: chap-beats-rap-in-whatsapp-zap-flap-russian-banker-walks-from-insider-trading-case-after-deleting-software

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/01/whatsapp_fca_evidence/){:target="_blank" rel="noopener"}

> Brit financial watchdogs foxed by not guilty verdict A Russian ex-banker has been found not guilty of destroying potential evidence after he deleted a copy of WhatsApp from his phone before handing it over to police.... [...]
