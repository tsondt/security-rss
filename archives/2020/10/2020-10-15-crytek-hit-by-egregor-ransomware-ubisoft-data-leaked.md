Title: Crytek hit by Egregor ransomware, Ubisoft data leaked
Date: 2020-10-15T14:24:24
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: crytek-hit-by-egregor-ransomware-ubisoft-data-leaked

[Source](https://www.bleepingcomputer.com/news/security/crytek-hit-by-egregor-ransomware-ubisoft-data-leaked/){:target="_blank" rel="noopener"}

> The Egregor ransomware gang has hit game developer Crytek in a confirmed ransomware attack and leaked what they claim are files stolen from Ubisoft's network. [...]
