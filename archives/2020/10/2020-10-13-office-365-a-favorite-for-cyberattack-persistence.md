Title: Office 365: A Favorite for Cyberattack Persistence
Date: 2020-10-13T13:20:16+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security;Cloud Security;COVID-19;cyber attacks;Microsoft;Office 365;Phishing;productivity suite;ransomware;SaaS;Software as a Service;threat actors;Vectra AI
Slug: office-365-a-favorite-for-cyberattack-persistence

[Source](https://threatpost.com/office-365-persistent-cyberattacks/160010/){:target="_blank" rel="noopener"}

> Bad actors are leveraging legitimate services and tools within Microsoft's productivity suite to launch cyberattacks on COVID-19 stay-at-home workers, new research finds. [...]
