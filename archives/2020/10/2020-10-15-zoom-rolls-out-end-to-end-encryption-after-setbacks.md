Title: Zoom Rolls Out End-to-End Encryption After Setbacks
Date: 2020-10-15T15:12:14+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security;coronavirus;COVID-19;E2EE;Encryption;End to end encryption;Pandemic;remote work;transport layer security encryption;video conferencing security;zoom;zoom meeting;Zoom-bombing
Slug: zoom-rolls-out-end-to-end-encryption-after-setbacks

[Source](https://threatpost.com/zoom-end-to-end-encryption/160150/){:target="_blank" rel="noopener"}

> After backlash over false marketing around its encryption policies, Zoom will finally roll out end-to-end encryption next week. [...]
