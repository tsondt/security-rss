Title: Aetna agrees million-dollar settlement after healthcare data breaches violate HIPAA rules
Date: 2020-10-29T16:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: aetna-agrees-million-dollar-settlement-after-healthcare-data-breaches-violate-hipaa-rules

[Source](https://portswigger.net/daily-swig/aetna-agrees-million-dollar-settlement-after-healthcare-data-breaches-violate-hipaa-rules){:target="_blank" rel="noopener"}

> Life insurance firm settles multiple incidents dating back to 2017 [...]
