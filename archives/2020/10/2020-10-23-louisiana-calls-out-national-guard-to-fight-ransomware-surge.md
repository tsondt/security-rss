Title: Louisiana Calls Out National Guard to Fight Ransomware Surge
Date: 2020-10-23T20:28:03+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware;Cyberattacks;emotet;government offices;kimjongrat;louisiana;malware;municipal targets;national guard;ransomware
Slug: louisiana-calls-out-national-guard-to-fight-ransomware-surge

[Source](https://threatpost.com/louisiana-national-guard-ransomware/160508/){:target="_blank" rel="noopener"}

> An investigation showed a custom backdoor RAT and the Emotet trojan in the networks of municipal victims of the attacks. [...]
