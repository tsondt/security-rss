Title: Chrome 86 Aims to Bar Abusive Notification Content
Date: 2020-10-22T16:36:59+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Web Security;abusive content;abusive notifications;blocking;Browser;browser notifications;chrome 80;chrome 84;chrome 86;google;Google Chrome;malicious notification;Safe Browsing;Web security
Slug: chrome-86-aims-to-bar-abusive-notification-content

[Source](https://threatpost.com/chrome-86-abusive-notification-content/160445/){:target="_blank" rel="noopener"}

> Google said Chrome 86 will automatically block malicious notifications that may be used for phishing or malware. [...]
