Title: Palo Alto Networks threatens to sue security startup for comparison review, says it breaks software EULA
Date: 2020-10-23T17:58:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: palo-alto-networks-threatens-to-sue-security-startup-for-comparison-review-says-it-breaks-software-eula

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/23/palo_alto_orca_lawsuit/){:target="_blank" rel="noopener"}

> 'I'm not going to be bullied by someone with deeper pockets' vows Orca boss Palo Alto Networks has threatened a startup with legal action after the smaller biz published a comparison review of one of its products.... [...]
