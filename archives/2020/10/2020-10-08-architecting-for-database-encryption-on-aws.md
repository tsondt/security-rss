Title: Architecting for database encryption on AWS
Date: 2020-10-08T17:57:24+00:00
Author: Jonathan Jenkyn
Category: AWS Security
Tags: Advanced (300);AWS CloudHSM;AWS Key Management Service;Best Practices;Security, Identity, & Compliance;AWS KMS;Database;EKM;Encryption at Rest;Oracle TDE;RDS;Security Blog;TDE;Transparent data encryption
Slug: architecting-for-database-encryption-on-aws

[Source](https://aws.amazon.com/blogs/security/architecting-for-database-encryption-on-aws/){:target="_blank" rel="noopener"}

> In this post, I review the options you have to protect your customer data when migrating or building new databases in Amazon Web Services (AWS). I focus on how you can support sensitive workloads in ways that help you maintain compliance and regulatory obligations, and meet security objectives. Understanding transparent data encryption I commonly see [...]
