Title: For Foxit's sake: Windows and Mac users alike urged to patch PhantomPDF over use-after-free vulns
Date: 2020-10-13T17:30:21+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: for-foxits-sake-windows-and-mac-users-alike-urged-to-patch-phantompdf-over-use-after-free-vulns

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/13/foxit_phantompdf_vulns_update/){:target="_blank" rel="noopener"}

> US CIST points spotlight at PDF reader 'n' creator suite Windows and Mac users running Foxit's popular PhantomPDF reader should update their installations to the latest version after the US CISA cybersecurity agency warned of a handful of high-severity product vulnerabilities.... [...]
