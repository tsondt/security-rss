Title: Zoom finally adds end-to-end encryption for all, for free – though there are caveats
Date: 2020-10-27T20:09:00+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: zoom-finally-adds-end-to-end-encryption-for-all-for-free-though-there-are-caveats

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/27/zoom_endtoend_encryption/){:target="_blank" rel="noopener"}

> Perhaps we can talk about extra security and compromises honestly Zoom has finally added end-to-end encryption to its video conferencing service at no additional cost for all users, whether they are paying subscribers or not.... [...]
