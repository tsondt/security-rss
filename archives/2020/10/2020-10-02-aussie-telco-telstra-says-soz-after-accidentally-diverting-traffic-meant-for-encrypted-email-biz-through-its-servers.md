Title: Aussie telco Telstra says soz after accidentally diverting traffic meant for encrypted email biz through its servers
Date: 2020-10-02T18:01:09+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: aussie-telco-telstra-says-soz-after-accidentally-diverting-traffic-meant-for-encrypted-email-biz-through-its-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/02/protonmail_telstra_bgp_hijack/){:target="_blank" rel="noopener"}

> Resource Public Key Infrastructure now, bellows ProtonMail Aussie telco Telstra has apologised after a Border Gateway Protocol (BGP) routing oddity caused traffic destined for encrypted email service ProtonMail to wrongly pass through Telstra's servers.... [...]
