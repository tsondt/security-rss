Title: New Privacy Features in iOS 14
Date: 2020-10-07T11:05:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Apple;iOS;privacy;security engineering
Slug: new-privacy-features-in-ios-14

[Source](https://www.schneier.com/blog/archives/2020/10/new-privacy-features-in-ios-14.html){:target="_blank" rel="noopener"}

> A good rundown. [...]
