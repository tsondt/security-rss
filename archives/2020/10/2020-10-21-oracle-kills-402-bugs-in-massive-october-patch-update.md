Title: Oracle Kills 402 Bugs in Massive October Patch Update
Date: 2020-10-21T17:21:13+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;CPU;Critical Patch Update;CVE-2020-14871;CVE-2020-1953;Oracle;Oracle Communications;Oracle E-Business Suite;Oracle Financial Services Applications;Oracle Fusion Middleware;Oracle MySQL;Oracle Retail Applications;quarterly patch update;remote code execution;Security Update
Slug: oracle-kills-402-bugs-in-massive-october-patch-update

[Source](https://threatpost.com/oracle-october-patch-update/160407/){:target="_blank" rel="noopener"}

> Over half of Oracle's flaws in its quarterly patch update can be remotely exploitable without authentication; 65 are critical, and two have CVSS scores of 10 out of 10. [...]
