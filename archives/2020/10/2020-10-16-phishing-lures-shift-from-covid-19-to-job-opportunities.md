Title: Phishing Lures Shift from COVID-19 to Job Opportunities
Date: 2020-10-16T18:35:32+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Cloud Security;Podcasts;Vulnerabilities;Web Security;COVID-19;fortiguard;Fortinet;job opportunity lure;malware;Phishing;phishing lure;podcast;security threat;threat
Slug: phishing-lures-shift-from-covid-19-to-job-opportunities

[Source](https://threatpost.com/phishing-lures-shifting-from-covid-19-to-job-opportunities/160143/){:target="_blank" rel="noopener"}

> Fortinet researchers are seeing a pivot in the spear-phishing and phishing lures used by cybercriminals, to entice potential job candidates as businesses open up. [...]
