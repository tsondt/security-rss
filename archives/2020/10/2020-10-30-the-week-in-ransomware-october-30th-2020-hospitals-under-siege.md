Title: The Week in Ransomware - October 30th 2020 - Hospitals under siege
Date: 2020-10-30T15:29:57
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-october-30th-2020-hospitals-under-siege

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-30th-2020-hospitals-under-siege/){:target="_blank" rel="noopener"}

> This week we have seen a concerted attack against the healthcare industry by hacking groups utilizing the Ryuk ransomware. Also, we saw some large well-known companies suffer ransomware attacks that impacted their business operations. [...]
