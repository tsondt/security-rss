Title: How to add authentication to a single-page web application with Amazon Cognito OAuth2 implementation
Date: 2020-10-09T17:26:10+00:00
Author: George Conti
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;Security, Identity, & Compliance;authentication;authorization;Javascript;OAuth;OIDC;OpenID Connect;Security Blog;Single Page App;SPA
Slug: how-to-add-authentication-to-a-single-page-web-application-with-amazon-cognito-oauth2-implementation

[Source](https://aws.amazon.com/blogs/security/how-to-add-authentication-single-page-web-application-with-amazon-cognito-oauth2-implementation/){:target="_blank" rel="noopener"}

> In this post, I’ll be showing you how to configure Amazon Cognito as an OpenID provider (OP) with a single-page web application. This use case describes using Amazon Cognito to integrate with an existing authorization system following the OpenID Connect (OIDC) specification. OIDC is an identity layer on top of the OAuth 2.0 protocol to [...]
