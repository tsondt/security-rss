Title: US SIM swap fraudsters charged with multiple crimes
Date: 2020-10-29T14:02:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-sim-swap-fraudsters-charged-with-multiple-crimes

[Source](https://portswigger.net/daily-swig/us-sim-swap-fraudsters-charged-with-multiple-crimes){:target="_blank" rel="noopener"}

> One defendant allegedly tried to engineer a SWAT raid on his co-conspirator’s home [...]
