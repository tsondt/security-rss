Title: Google warned users of 33,000 state-sponsored attacks in 2020
Date: 2020-10-16T17:30:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google
Slug: google-warned-users-of-33000-state-sponsored-attacks-in-2020

[Source](https://www.bleepingcomputer.com/news/security/google-warned-users-of-33-000-state-sponsored-attacks-in-2020/){:target="_blank" rel="noopener"}

> Google delivered over 33,000 alerts to its users during the first three quarters of 2020 to warn them of state-sponsored phishing attacks targeting their accounts. [...]
