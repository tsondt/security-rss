Title: LockBit ransomware moves quietly on the network, strikes fast
Date: 2020-10-21T10:28:22
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: lockbit-ransomware-moves-quietly-on-the-network-strikes-fast

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-moves-quietly-on-the-network-strikes-fast/){:target="_blank" rel="noopener"}

> LockBit ransomware takes as little as five minutes to deploy the encryption routine on target systems once it lands on the victim network. [...]
