Title: Undocumented backdoor that covertly takes snapshots found in kids’ smartwatch
Date: 2020-10-12T13:00:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;backdoors;firmware;smartwatch
Slug: undocumented-backdoor-that-covertly-takes-snapshots-found-in-kids-smartwatch

[Source](https://arstechnica.com/?p=1713224){:target="_blank" rel="noopener"}

> Enlarge (credit: Xplora ) A popular smartwatch designed exclusively for children contains an undocumented backdoor that makes it possible for someone to remotely capture camera snapshots, wiretap voice calls, and track locations in real time, a researcher said. The X4 smartwatch is marketed by Xplora, a Norway-based seller of children’s watches. The device, which sells for about $200, runs on Android and offers a range of capabilities, including the ability to make and receive voice calls to parent-approved numbers and to send an SOS broadcast that alerts emergency contacts to the location of the watch. A separate app that runs [...]
