Title: ReNgine: Open source recon tool automates intel-gathering process for pen testers
Date: 2020-10-20T10:58:51+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: rengine-open-source-recon-tool-automates-intel-gathering-process-for-pen-testers

[Source](https://portswigger.net/daily-swig/rengine-open-source-recon-tool-automates-intel-gathering-process-for-pen-testers){:target="_blank" rel="noopener"}

> Recon framework presents the results of website and endpoint scans in a single window [...]
