Title: Critical Oracle WebLogic flaw actively targeted in attacks
Date: 2020-10-29T08:07:13
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: critical-oracle-weblogic-flaw-actively-targeted-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/critical-oracle-weblogic-flaw-actively-targeted-in-attacks/){:target="_blank" rel="noopener"}

> Threat actors have started to hunt for servers running Oracle WebLogic instances vulnerable to a critical flaw that allows taking control of the system with little effort and no authentication. [...]
