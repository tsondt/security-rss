Title: Intel celebrates security of Ice Lake Xeon processors, so far impervious to any threat due to their unavailability
Date: 2020-10-14T19:26:25+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: intel-celebrates-security-of-ice-lake-xeon-processors-so-far-impervious-to-any-threat-due-to-their-unavailability

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/14/intel_ice_lake_xeon_security/){:target="_blank" rel="noopener"}

> But when they ship, Chipzilla promises its server silicon will 'double down' on defense mechanisms Intel on Wednesday talked up a set of security features planned for its promised third-generation Xeon Scalable Processors, code-named Ice Lake, which are supposed to show up before the end of the year.... [...]
