Title: Remember when Zoom was rumbled for lousy crypto? Six months later it says end-to-end is ready
Date: 2020-10-15T07:33:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: remember-when-zoom-was-rumbled-for-lousy-crypto-six-months-later-it-says-end-to-end-is-ready

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/15/zoom_end_to_end_security/){:target="_blank" rel="noopener"}

> But it’s a tech preview and requires opt-in for every meeting The world’s plague-time video meeting tool of choice, Zoom, says it’s figured out how to do end-to-end encryption sufficiently well to offer users a tech preview.... [...]
