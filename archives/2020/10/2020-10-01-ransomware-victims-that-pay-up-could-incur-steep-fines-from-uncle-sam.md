Title: Ransomware Victims That Pay Up Could Incur Steep Fines from Uncle Sam
Date: 2020-10-01T16:36:19+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;Cryptolocker;Eversheds Sutherland;Evgeniy Mikhailovich Bogachev;Evil Corp.;Ginger Faulk;Lazarus Group;Office of Foreign Assets Control;SamSam ransomware;Treasury Department
Slug: ransomware-victims-that-pay-up-could-incur-steep-fines-from-uncle-sam

[Source](https://krebsonsecurity.com/2020/10/ransomware-victims-that-pay-up-could-incur-steep-fines-from-uncle-sam/){:target="_blank" rel="noopener"}

> Companies victimized by ransomware and firms that facilitate negotiations with ransomware extortionists could face steep fines from the U.S. federal government if the crooks who profit from the attack are already under economic sanctions, the Treasury Department warned today. Image: Shutterstock In its advisory (PDF), the Treasury’s Office of Foreign Assets Control (OFAC) said “companies that facilitate ransomware payments to cyber actors on behalf of victims, including financial institutions, cyber insurance firms, and companies involved in digital forensics and incident response, not only encourage future ransomware payment demands but also may risk violating OFAC regulations.” As financial losses from cybercrime [...]
