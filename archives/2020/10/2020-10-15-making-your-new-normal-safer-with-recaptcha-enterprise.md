Title: Making your new normal safer with reCAPTCHA Enterprise
Date: 2020-10-15T19:00:00+00:00
Author: Jason Fedor
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: making-your-new-normal-safer-with-recaptcha-enterprise

[Source](https://cloud.google.com/blog/products/identity-security/how-recaptcha-enterprise-protects-organizations/){:target="_blank" rel="noopener"}

> Traffic from both humans and bots are at record highs. Since March 2020, reCAPTCHA has seen a 40% increase in usage - businesses and services that previously saw most of their users in person have shifted to online-first or online-only. This increased demand for online services and transactions can expose businesses to various forms of online fraud and abuse, and without dedicated teams familiar with these attacks and how to stop them, we’ve seen hundreds of thousands of new websites come to reCAPTCHA for visibility and protection. During COVID-19, reCAPTCHA is playing a critical role helping global public sector agencies [...]
