Title: Automate AWS Firewall Manager onboarding using AWS Centralized WAF and VPC Security Group Management solution
Date: 2020-10-15T17:14:31+00:00
Author: Satheesh Kumar
Category: AWS Security
Tags: Advanced (300);AWS Firewall Manager;AWS Shield;AWS WAF;Security, Identity, & Compliance;Security Blog;VPC security groups
Slug: automate-aws-firewall-manager-onboarding-using-aws-centralized-waf-and-vpc-security-group-management-solution

[Source](https://aws.amazon.com/blogs/security/automate-aws-firewall-manager-onboarding-using-aws-centralized-waf-and-vpc-security-group-management-solution/){:target="_blank" rel="noopener"}

> Many customers—especially large enterprises—run workloads across multiple AWS accounts and in multiple AWS regions. AWS Firewall Manager service, launched in April 2018, enables customers to centrally configure and manage AWS WAF rules, audit Amazon VPC security group rules across accounts and applications in AWS Organizations, and protect resources against distributed DDoS attacks. In this blog [...]
