Title: Google Chrome partitions browser HTTP cache to defend against XS-Leak attacks
Date: 2020-10-13T15:57:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-chrome-partitions-browser-http-cache-to-defend-against-xs-leak-attacks

[Source](https://portswigger.net/daily-swig/google-chrome-partitions-browser-http-cache-to-defend-against-xs-leak-attacks){:target="_blank" rel="noopener"}

> Fresh walls erected against privacy-busting web threats [...]
