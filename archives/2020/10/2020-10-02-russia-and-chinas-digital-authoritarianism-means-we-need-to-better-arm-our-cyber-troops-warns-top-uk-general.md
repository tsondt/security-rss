Title: Russia and China's 'digital authoritarianism' means we need to better arm our cyber troops, warns top UK general
Date: 2020-10-02T09:15:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: russia-and-chinas-digital-authoritarianism-means-we-need-to-better-arm-our-cyber-troops-warns-top-uk-general

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/02/digital_authoritarianism_general_nick_carter/){:target="_blank" rel="noopener"}

> New era, new weapons needed, says Chief of the Defence Staff Britain's enemies are investing more and more in cyber warfare capabilities, the UK's top general has warned – singling out Russia and its "digital authoritarianism".... [...]
