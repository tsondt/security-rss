Title: Largest cruise line operator Carnival confirms ransomware data theft
Date: 2020-10-09T17:31:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: largest-cruise-line-operator-carnival-confirms-ransomware-data-theft

[Source](https://www.bleepingcomputer.com/news/security/largest-cruise-line-operator-carnival-confirms-ransomware-data-theft/){:target="_blank" rel="noopener"}

> Carnival Corporation, the world's largest cruise line operator, has confirmed that the personal information of customers, employees, and ship crews was stolen during an August ransomware attack. [...]
