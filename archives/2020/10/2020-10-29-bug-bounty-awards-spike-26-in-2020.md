Title: Bug-Bounty Awards Spike 26% in 2020
Date: 2020-10-29T13:14:04+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Bug Bounty;Most Recent ThreatLists;Vulnerabilities;apple;bounty payouts;bug bounty;COVID-19;cross-site scripting;ethical hackers;flaws;HackerOne;information disclosure;most valuable;most-rewarded;stay at home orders;tiktok;top 10;vulnerabilities;XSS
Slug: bug-bounty-awards-spike-26-in-2020

[Source](https://threatpost.com/bug-bounty-awards-spike-2020/160719/){:target="_blank" rel="noopener"}

> The most-rewarded flaw is XSS, which is among those that are relatively cheap for organizations to identify. [...]
