Title: US indicts Russian GRU 'Sandworm' hackers for NotPetya, worldwide attacks
Date: 2020-10-19T14:42:16
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: us-indicts-russian-gru-sandworm-hackers-for-notpetya-worldwide-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-indicts-russian-gru-sandworm-hackers-for-notpetya-worldwide-attacks/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice has charged six Russian intelligence operatives for hacking operations related to the Pyeongchang Winter Olympics, the 2017 French elections, and the notorious NotPetya ransomware attack. [...]
