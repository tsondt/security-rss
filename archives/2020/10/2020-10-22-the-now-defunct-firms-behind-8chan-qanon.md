Title: The Now-Defunct Firms Behind 8chan, QAnon
Date: 2020-10-22T21:48:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;The Coming Storm;8chan;8kun;American Registry for Internet Numbers;ARIN;Jim Watkins;N.T. Technology Inc.;QAnon;Ron Guilmette
Slug: the-now-defunct-firms-behind-8chan-qanon

[Source](https://krebsonsecurity.com/2020/10/the-now-defunct-firms-behind-8chan-qanon/){:target="_blank" rel="noopener"}

> Some of the world’s largest Internet firms have taken steps to crack down on disinformation spread by QAnon conspiracy theorists and the hate-filled anonymous message board 8chan. But according to a California-based security researcher, those seeking to de-platform these communities may have overlooked a simple legal solution to that end: Both the Nevada-based web hosting company owned by 8chan’s current figurehead and the California firm that provides its sole connection to the Internet are defunct businesses in the eyes of their respective state regulators. In practical terms, what this means is that the legal contracts which granted these companies temporary [...]
