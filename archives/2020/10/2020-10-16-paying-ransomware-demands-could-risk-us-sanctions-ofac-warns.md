Title: Paying ransomware demands could risk US sanctions, OFAC warns
Date: 2020-10-16T14:36:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: paying-ransomware-demands-could-risk-us-sanctions-ofac-warns

[Source](https://portswigger.net/daily-swig/paying-ransomware-demands-could-risk-us-sanctions-ofac-warns){:target="_blank" rel="noopener"}

> Government could impose penalties for improper conduct [...]
