Title: REvil Gang Promises a Big Video-Game Hit; Claims Massive Revenue
Date: 2020-10-29T19:48:25+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Malware;Vulnerabilities;Web Security;$100 million;annual revenue;cartel;data leak;egregor;interview;maze;operations;ransomware;revil;russian osint;shut down;trump files;video game company;Youtube
Slug: revil-gang-promises-a-big-video-game-hit-claims-massive-revenue

[Source](https://threatpost.com/revil-video-game-hit-revenue/160743/){:target="_blank" rel="noopener"}

> In a wide-ranging interview, a REvil leader said the gang is earning $100 million per year, and provided insights into the life of a cybercriminal. [...]
