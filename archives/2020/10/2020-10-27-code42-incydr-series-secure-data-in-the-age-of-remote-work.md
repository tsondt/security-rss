Title: Code42 Incydr Series: Secure Data in the Age of Remote Work
Date: 2020-10-27T13:00:53+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Web Security;data security risk;remote work;wfh
Slug: code42-incydr-series-secure-data-in-the-age-of-remote-work

[Source](https://threatpost.com/code42-incydr-series-secure-data-in-the-age-of-remote-work/160427/){:target="_blank" rel="noopener"}

> With Code42 Incydr, you can keep tabs on when and where your data is going — without restricting where or how your employees want to collaborate and work. [...]
