Title: Reverse-Engineering the Redactions in the Ghislaine Maxwell Deposition
Date: 2020-10-27T11:34:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: courts;redaction;reverse engineering
Slug: reverse-engineering-the-redactions-in-the-ghislaine-maxwell-deposition

[Source](https://www.schneier.com/blog/archives/2020/10/reverse-engineering-the-redactions-in-the-ghislaine-maxwell-deposition.html){:target="_blank" rel="noopener"}

> Slate magazine was able to cleverly read the Ghislaine Maxwell deposition and reverse-engineer many of the redacted names. We’ve long known that redacting is hard in the modern age, but most of the failures to date have been a result of not realizing that covering digital text with a black bar doesn’t always remove the text from the underlying digital file. As far as I know, this reverse-engineering technique is new. [...]
