Title: 2 More Hospitals Hit by Growing Wave of Ransomware Attacks, As Feds Issue Warning
Date: 2020-10-28T21:43:08+00:00
Author: Tom Spring
Category: Threatpost
Tags: IoT;Malware;Canton-Potsdam;COVID-19;device sprawl;Dusseldorf University Hospital;emergency room;Gouverneur;hospital ransomware;Lazarus;Massena;MIoT;New York;Oregon;patient safety;ransomware;rerouting;ryuk;Sky Lakes Medical Center;St. Lawrence Health System
Slug: 2-more-hospitals-hit-by-growing-wave-of-ransomware-attacks-as-feds-issue-warning

[Source](https://threatpost.com/hospitals-hit-by-ransomware/160695/){:target="_blank" rel="noopener"}

> Hospitals in New York and Oregon were targeted on Tuesday by threat actors who crippled systems and forced ambulances with sick patients to be rerouted, in some cases. [...]
