Title: Game Titles Watch Dogs: Legion, Albion Both Targeted by Hackers
Date: 2020-10-19T14:24:58+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Vulnerabilities;Web Security;activision;albion;Credentials;crytek;cyberattack;data breach;egregor;forum users;Gaming;Hacked;MMORPG;ransomware;release date;sandbox interactive;source code;ubisoft;watch dogs: legion
Slug: game-titles-watch-dogs-legion-albion-both-targeted-by-hackers

[Source](https://threatpost.com/hackers-source-code-theft-watch-dogs-legion-albion/160268/){:target="_blank" rel="noopener"}

> In both cases, cybercriminals claim to have reams of information for the popular gaming titles. [...]
