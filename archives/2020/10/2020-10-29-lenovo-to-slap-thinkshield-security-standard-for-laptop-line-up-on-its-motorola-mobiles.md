Title: Lenovo to slap ThinkShield security standard for laptop line-up on its Motorola mobiles
Date: 2020-10-29T13:41:49+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: lenovo-to-slap-thinkshield-security-standard-for-laptop-line-up-on-its-motorola-mobiles

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/29/motorola_lenovo_thinkshield_for_mobile/){:target="_blank" rel="noopener"}

> Scheme to roll out across firm's device portfolio in coming months Motorola will push ThinkShield onto the business end of its smartphone portfolio, as an extension of the security and management programme on Lenovo's laptop and desktop line.... [...]
