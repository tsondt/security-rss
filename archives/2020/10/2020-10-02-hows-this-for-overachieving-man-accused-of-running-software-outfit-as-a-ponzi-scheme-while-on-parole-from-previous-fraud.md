Title: How's this for overachieving? Man accused of running software outfit as a Ponzi scheme while on parole from previous fraud
Date: 2020-10-02T03:10:10+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: hows-this-for-overachieving-man-accused-of-running-software-outfit-as-a-ponzi-scheme-while-on-parole-from-previous-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/02/scammers_gonna_allegedly_scam/){:target="_blank" rel="noopener"}

> He masterminded $7m scam from halfway house, Feds claim A convicted fraudster was out on parole when he allegedly conned victims into giving him millions of dollars to place surefire sports bets on their behalf using special software that didn't actually exist.... [...]
