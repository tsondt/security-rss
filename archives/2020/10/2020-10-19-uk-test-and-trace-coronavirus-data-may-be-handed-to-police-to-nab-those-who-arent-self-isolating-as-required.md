Title: UK test-and-trace coronavirus data may be handed to police to nab those who aren't self-isolating as required
Date: 2020-10-19T15:29:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: uk-test-and-trace-coronavirus-data-may-be-handed-to-police-to-nab-those-who-arent-self-isolating-as-required

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/19/uk_test_and_trace_data/){:target="_blank" rel="noopener"}

> Plod 'involvement' could deter testing, says doctors' union As if things were not going badly enough for the UK's COVID-19 test-and-trace service, it now seems police will be able to access some test data, prompting fears the disclosure could deter people who should have tests from coming forward.... [...]
