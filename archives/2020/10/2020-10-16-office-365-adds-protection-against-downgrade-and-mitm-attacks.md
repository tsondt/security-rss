Title: Office 365 adds protection against downgrade and MITM attacks
Date: 2020-10-16T03:30:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: office-365-adds-protection-against-downgrade-and-mitm-attacks

[Source](https://www.bleepingcomputer.com/news/security/office-365-adds-protection-against-downgrade-and-mitm-attacks/){:target="_blank" rel="noopener"}

> Microsoft is working on adding SMTP MTA Strict Transport Security (MTA-STS) support to Exchange Online to ensure Office 365 customers' email communication security and integrity. [...]
