Title: Cybersecurity Awareness Month—New security announcements for Google Cloud
Date: 2020-10-29T16:00:00+00:00
Author: Rob Sadowski
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: cybersecurity-awareness-month-new-security-announcements-for-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/october-2020-google-cloud-security-announcements/){:target="_blank" rel="noopener"}

> Security is at the heart of any cloud journey. On the one hand, as you adopt cloud services and move workloads to the cloud, you need to make sure you’re conforming to your established security policies. On the other hand, you can take advantage of new capabilities, use new tools, and help improve your security posture. Today’s announcements include new security features, whitepapers that explore our encryption capabilities and use-case demos to help deploy products optimally. These updates will help facilitate safer cloud journeys and give admins increased visibility and control for their organizations. New Google Cloud Security Showcase videos [...]
