Title: How to plan a password security project
Date: 2020-10-30T07:00:05+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: how-to-plan-a-password-security-project

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/30/plan_a_password_security_project/){:target="_blank" rel="noopener"}

> First, you need to prove to the budget holder that you’ve got a problem Sponsored Weak password security is a torment that afflicts networks in so many ways. On the user side is the certainty of hopeless and reused passwords, while on the attacker’s side are a gamut of techniques for targeting them such as phishing, credential stuffing, brute forcing, and spotting backdoors to hidden applications such as RDP, SSH, and shadow IT.... [...]
