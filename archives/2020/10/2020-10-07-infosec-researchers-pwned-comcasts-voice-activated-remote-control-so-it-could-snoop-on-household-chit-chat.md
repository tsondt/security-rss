Title: Infosec researchers pwned Comcast's voice-activated remote control so it could snoop on household chit-chat
Date: 2020-10-07T13:02:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: infosec-researchers-pwned-comcasts-voice-activated-remote-control-so-it-could-snoop-on-household-chit-chat

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/07/comcast_xr11_voice_remote_pwnable/){:target="_blank" rel="noopener"}

> Own one of those Xfinity XR11 TV zappers? You better get patching A voice-activated TV remote can be turned into a covert home surveillance device, according to researchers from infosec firm Guardicore who probed the device to show that a man-in-the-middle attack could compromise it.... [...]
