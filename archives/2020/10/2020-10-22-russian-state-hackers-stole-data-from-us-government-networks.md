Title: Russian state hackers stole data from US government networks
Date: 2020-10-22T15:55:51
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: russian-state-hackers-stole-data-from-us-government-networks

[Source](https://www.bleepingcomputer.com/news/security/russian-state-hackers-stole-data-from-us-government-networks/){:target="_blank" rel="noopener"}

> DHS Cybersecurity and Infrastructure Security Agency (CISA) and the FBI today warned that a Russian state-sponsored APT threat group known as Energetic Bear has hacked and stolen data from US government networks during the last two months. [...]
