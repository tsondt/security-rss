Title: Oracle WebLogic Server RCE Flaw Under Active Attack
Date: 2020-10-29T14:49:58+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security;active exploit;console;CVE-2020-14882;device takeover;Oracle;Oracle WebLogic Server;remote code execution
Slug: oracle-weblogic-server-rce-flaw-under-active-attack

[Source](https://threatpost.com/oracle-weblogic-server-rce-flaw-attack/160723/){:target="_blank" rel="noopener"}

> The flaw in the console component of the WebLogic Server, CVE-2020-14882, is under active attack, researchers warn. [...]
