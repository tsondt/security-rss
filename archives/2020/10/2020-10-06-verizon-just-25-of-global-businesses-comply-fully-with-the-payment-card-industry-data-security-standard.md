Title: Verizon: Just 25% of global businesses comply fully with the Payment Card Industry Data Security Standard
Date: 2020-10-06T17:41:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: verizon-just-25-of-global-businesses-comply-fully-with-the-payment-card-industry-data-security-standard

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/06/verizon_pci_dss_survey/){:target="_blank" rel="noopener"}

> Gives you confidence in an era where nobody accepts cash any more A little more than a quarter of companies worldwide are fully compliant with the exacting PCI DSS online payment security standard, according to US telco Verizon.... [...]
