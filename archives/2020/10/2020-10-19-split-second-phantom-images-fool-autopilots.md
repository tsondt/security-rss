Title: Split-Second Phantom Images Fool Autopilots
Date: 2020-10-19T11:28:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;cars;machine learning;spoofing
Slug: split-second-phantom-images-fool-autopilots

[Source](https://www.schneier.com/blog/archives/2020/10/split-second-phantom-images-fool-autopilots.html){:target="_blank" rel="noopener"}

> Researchers are tricking autopilots by inserting split-second images into roadside billboards. Researchers at Israel’s Ben Gurion University of the Negev... previously revealed that they could use split-second light projections on roads to successfully trick Tesla’s driver-assistance systems into automatically stopping without warning when its camera sees spoofed images of road signs or pedestrians. In new research, they’ve found they can pull off the same trick with just a few frames of a road sign injected on a billboard’s video. And they warn that if hackers hijacked an internet-connected billboard to carry out the trick, it could be used to cause [...]
