Title: First, Patch Tuesday. Now, Oh Hell, Monday: Microsoft emits bonus fixes for Visual Studio, Windows 10 security bugs
Date: 2020-10-19T14:43:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: first-patch-tuesday-now-oh-hell-monday-microsoft-emits-bonus-fixes-for-visual-studio-windows-10-security-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/19/security_in_brief/){:target="_blank" rel="noopener"}

> Plus: A warning to SharePoint operators In brief Just days after issuing fixes for scores of bugs in its products for this month's Patch Tuesday, Microsoft has issued two more patches for security holes that can be exploited by maliciously crafted files to run malware on victims' computers.... [...]
