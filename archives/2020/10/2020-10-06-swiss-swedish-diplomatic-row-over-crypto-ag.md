Title: Swiss-Swedish Diplomatic Row Over Crypto AG
Date: 2020-10-06T11:11:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: CIA;cybersecurity;espionage;hardware;Sweden;Switzerland
Slug: swiss-swedish-diplomatic-row-over-crypto-ag

[Source](https://www.schneier.com/blog/archives/2020/10/swiss-swedish-diplomatic-row-over-crypto-ag.html){:target="_blank" rel="noopener"}

> Previously I have written about the Swedish-owned Swiss-based cryptographic hardware company: Crypto AG. It was a CIA-owned Cold War operation for decades. Today it is called Crypto International, still based in Switzerland but owned by a Swedish company. It’s back in the news : Late last week, Swedish Foreign Minister Ann Linde said she had canceled a meeting with her Swiss counterpart Ignazio Cassis slated for this month after Switzerland placed an export ban on Crypto International, a Swiss-based and Swedish-owned cybersecurity company. The ban was imposed while Swiss authorities examine long-running and explosive claims that a previous incarnation of [...]
