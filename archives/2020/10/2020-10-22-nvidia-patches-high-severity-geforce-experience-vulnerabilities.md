Title: NVIDIA patches high severity GeForce Experience vulnerabilities
Date: 2020-10-22T19:01:02
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: nvidia-patches-high-severity-geforce-experience-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/nvidia-patches-high-severity-geforce-experience-vulnerabilities/){:target="_blank" rel="noopener"}

> NVIDIA released a security update for the Windows NVIDIA GeForce Experience (GFE) app to address vulnerabilities that could enable attackers to execute arbitrary code, escalate privileges, gain access to sensitive info, or trigger a denial of service (DoS) state on systems running unpatched software. [...]
