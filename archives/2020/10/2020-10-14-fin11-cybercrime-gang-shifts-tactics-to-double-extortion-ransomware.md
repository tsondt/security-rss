Title: FIN11 Cybercrime Gang Shifts Tactics to Double-Extortion Ransomware
Date: 2020-10-14T15:46:59+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Malware;Clop;double extortion;fin11;financially motivated;leaked data;MANDIANT;ransomware;shifting tactics;ta505
Slug: fin11-cybercrime-gang-shifts-tactics-to-double-extortion-ransomware

[Source](https://threatpost.com/fin11-gang-double-extortion-ransomware/160089/){:target="_blank" rel="noopener"}

> The Clop ransomware has become a tool of choice for the financially motivated group. [...]
