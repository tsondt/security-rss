Title: How cybercriminals play the domain game
Date: 2020-10-21T06:00:14+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: how-cybercriminals-play-the-domain-game

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/21/how_cybercriminals_play_the_domain/){:target="_blank" rel="noopener"}

> And why AI tools will make you less vulnerable Sponsored Conventional email security tools are losing the battle against phishing attacks. The cause? Instead of registering a handful of domains from which to conduct their phishing campaigns, many cybercriminals now buy them by the thousand. This approach makes it harder for traditional email protection tools to spot phishing emails among the ‘noise’. Thanks to bulk domain registration services, malicious spammers can tip the balance in their favour through sheer volume.... [...]
