Title: Amazon Fires Employee Who Leaked Customer Names, Emails
Date: 2020-10-27T20:36:37+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Breach;Privacy;amazon;amazon data;Amazon privacy;customer notification;data breach;data collection;Data sharing;email address;employee;fired;insider threat;law enforcement
Slug: amazon-fires-employee-who-leaked-customer-names-emails

[Source](https://threatpost.com/amazon-fires-employee-customer-data/160610/){:target="_blank" rel="noopener"}

> Amazon notified customers and law enforcement of the insider-threat incident this week. [...]
