Title: Want to set up a successful bug bounty? Make sure you write it for the flaw finders and not the lawyers
Date: 2020-10-08T22:40:30+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: want-to-set-up-a-successful-bug-bounty-make-sure-you-write-it-for-the-flaw-finders-and-not-the-lawyers

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/08/cisa_bug_bounty_panel/){:target="_blank" rel="noopener"}

> Plus: Experts talk voting machine security, 'warming' of relations with infosec community If you're designing a security bug bounty for your organization's products, by all means get the lawyers to take a look, but keep their hands off the keyboard. If it's one thing flaw-finders find too tedious to deal with, which will put them off finding holes in your defenses, it's legalese – and these are people who otherwise spend all day combing reverse-engineered code for typos.... [...]
