Title: NVIDIA Patches Critical Bug in High-Performance Servers
Date: 2020-10-29T23:15:17+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities;ASRockRack IPMI;ASUS ASMB9-iKVM;Baseboard Management Controller;BMC;CCTV;critical bug;DEPO Computers;DGX A100;DGX AMI baseboard management controller;DGX servers;DGX-1;DGX-2;Gigabyte IPMI Motherboards;Gooxi BMC;GPU;Hewlett-Packard Enterprise Megarac;high-performance computing;HPC;IBM;Lenovo;Mikrobits;netapp;Nvidia;NVIDIA DGX GPU;NVIDIA DGX GPU servers;RSA;ThinkServer;TYAN Motherboard
Slug: nvidia-patches-critical-bug-in-high-performance-servers

[Source](https://threatpost.com/nvidia-critical-bug-hpc/160762/){:target="_blank" rel="noopener"}

> NVIDIA said a high-severity information-disclosure bug impacting its DGX A100 server line wouldn't be patched until early 2021. [...]
