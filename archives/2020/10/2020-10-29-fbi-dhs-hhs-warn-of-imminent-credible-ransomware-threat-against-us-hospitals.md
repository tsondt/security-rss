Title: FBI, DHS, HHS Warn of Imminent, Credible Ransomware Threat Against U.S. Hospitals
Date: 2020-10-29T00:43:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Ransomware;The Coming Storm;alex holden;Charles Carmakal;Department of Homeland Security;fbi;Health and Human Services;Hold Security;Mandiant;ransomware;Reuters;Ryuk
Slug: fbi-dhs-hhs-warn-of-imminent-credible-ransomware-threat-against-us-hospitals

[Source](https://krebsonsecurity.com/2020/10/fbi-dhs-hhs-warn-of-imminent-credible-ransomware-threat-against-u-s-hospitals/){:target="_blank" rel="noopener"}

> On Monday, Oct. 26, KrebsOnSecurity began following up on a tip from a reliable source that an aggressive Russian cybercriminal gang known for deploying ransomware was preparing to disrupt information technology systems at hundreds of hospitals, clinics and medical care facilities across the United States. Today, officials from the FBI and the U.S. Department of Homeland Security hastily assembled a conference call with healthcare industry executives warning about an “imminent cybercrime threat to U.S. hospitals and healthcare providers.” The agencies on the conference call, which included the U.S. Department of Health and Human Services (HHS), warned participants about “credible information [...]
