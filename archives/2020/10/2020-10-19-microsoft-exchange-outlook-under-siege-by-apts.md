Title: Microsoft Exchange, Outlook Under Siege By APTs
Date: 2020-10-19T15:09:06+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Cloud Security;Government;Hacks;Vulnerabilities;Web Security;Accenture 2020 Cyber Threatscape report;advanced threat;aka APT39;apt;BELUGASTURGEON APT;BLACKSTURGEON APT;chafer;Microsoft;Microsoft Exchange;Microsoft Outlook;Outlook on the Web;OWA;russia;Sourface;tactics;Turla APT;Whitebear APT;Windows Internet Information Services
Slug: microsoft-exchange-outlook-under-siege-by-apts

[Source](https://threatpost.com/microsoft-exchange-outlook-apts/160273/){:target="_blank" rel="noopener"}

> A new threat report shows that APTs are switching up their tactics when exploiting Microsoft services like Exchange and OWA, in order to avoid detection. [...]
