Title: Cybersecurity Visuals
Date: 2020-10-20T11:29:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: contests;cybersecurity
Slug: cybersecurity-visuals

[Source](https://www.schneier.com/blog/archives/2020/10/cybersecurity-visuals.html){:target="_blank" rel="noopener"}

> The Hewlett Foundation just announced its top five ideas in its Cybersecurity Visuals Challenge. The problem Hewlett is trying to solve is the dearth of good visuals for cybersecurity. A Google Images Search demonstrates the problem: locks, fingerprints, hands on laptops, scary looking hackers in black hoodies. Hewlett wanted to go beyond those tropes. I really liked the idea, but find the results underwhelming. It’s a hard problem. Hewlett press release. [...]
