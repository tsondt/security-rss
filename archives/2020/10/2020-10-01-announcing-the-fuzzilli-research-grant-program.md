Title: Announcing the Fuzzilli Research Grant Program
Date: 2020-10-01T09:26:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: announcing-the-fuzzilli-research-grant-program

[Source](https://googleprojectzero.blogspot.com/2020/10/announcing-fuzzilli-research-grant.html){:target="_blank" rel="noopener"}

> Posted by Samuel Groß, Project Zero Project Zero’s mission is to make 0-day hard in order to improve end-user security. We attack this problem in different ways, including supporting other security researchers. While Google currently offers research grants, they are limited to academics and those affiliated with universities. Today we are announcing a new USD $50,000 pilot program to foster research into JavaScript engine fuzzing through Google Compute Engine (GCE) credit grants. Here is how it works: Interested researchers submit a proposal for a project about fuzzing JavaScript engines. The proposal will be reviewed by an internal review board and, [...]
