Title: Microsoft adds protection for critical accounts in Office 365
Date: 2020-10-23T15:22:17
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-adds-protection-for-critical-accounts-in-office-365

[Source](https://www.bleepingcomputer.com/news/security/microsoft-adds-protection-for-critical-accounts-in-office-365/){:target="_blank" rel="noopener"}

> Microsoft is working on improving Microsoft Defender for Office 365 with priority protection features for accounts of high-profile employees like executive-level managers that threat actors target most often. [...]
