Title: Adventures in SQL Server 2019: Microsoft updates the update that broke the update
Date: 2020-10-02T19:06:59+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: adventures-in-sql-server-2019-microsoft-updates-the-update-that-broke-the-update

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/02/sql_server_cu8/){:target="_blank" rel="noopener"}

> I don't know why she swallowed the fly... There was good news for administrators of Microsoft's SQL Server 2019 last night as Cumulative Update 8 emerged, fixing the borkage of its predecessor.... [...]
