Title: Microsoft Fixes RCE Flaws in Out-of-Band Windows Update
Date: 2020-10-16T20:47:02+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security;CVE-2020-17022;CVE-2020-17023;Microsoft;microsoft store;patch;patch tuesday;RCE;remote code execution;visual studio code;Windows;windows codecs library
Slug: microsoft-fixes-rce-flaws-in-out-of-band-windows-update

[Source](https://threatpost.com/microsoft-rce-flaws-windows-update/160244/){:target="_blank" rel="noopener"}

> The two important-severity flaws in Microsoft Windows Codecs Library and Visual Studio Code could enable remote code execution. [...]
