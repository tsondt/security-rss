Title: Researchers warn over mobile browser address bar spoofing vulnerabilities
Date: 2020-10-22T15:46:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: researchers-warn-over-mobile-browser-address-bar-spoofing-vulnerabilities

[Source](https://portswigger.net/daily-swig/researchers-warn-over-mobile-browser-address-bar-spoofing-vulnerabilities){:target="_blank" rel="noopener"}

> Phishing risk from rogue pop-ups only partially addressed [...]
