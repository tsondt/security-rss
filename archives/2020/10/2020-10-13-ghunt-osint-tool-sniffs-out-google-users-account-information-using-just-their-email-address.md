Title: GHunt OSINT tool sniffs out Google users’ account information using just their email address
Date: 2020-10-13T11:22:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ghunt-osint-tool-sniffs-out-google-users-account-information-using-just-their-email-address

[Source](https://portswigger.net/daily-swig/ghunt-osint-tool-sniffs-out-google-users-account-information-using-just-their-email-address){:target="_blank" rel="noopener"}

> Open source tool hunts for active Google IDs, YouTube channels, and other Google-owned services [...]
