Title: Finnish psychotherapy clinic discloses data breach, victims extorted
Date: 2020-10-27T03:30:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: finnish-psychotherapy-clinic-discloses-data-breach-victims-extorted

[Source](https://www.bleepingcomputer.com/news/security/finnish-psychotherapy-clinic-discloses-data-breach-victims-extorted/){:target="_blank" rel="noopener"}

> A large psychotherapy clinic in Finland is under heavy stress after a threat actor asked a ransom for a client database with confidential information stolen in a data breach that likely happened almost two years ago. [...]
