Title: Crippling Cyberattacks, Disinformation Top Concerns for Election Day
Date: 2020-10-30T22:34:11+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Government;Malware;Mobile Security;Vulnerabilities;Web Security;cyber concerns;Cyberattacks;disinformation campaigns;election day 2020;election infrastructure;information bomb;mail in ballots;nov. 3;presidential election;ransomware;russia;top concerns;u.s. election
Slug: crippling-cyberattacks-disinformation-top-concerns-for-election-day

[Source](https://threatpost.com/cyberattacks-disinformation-top-concerns-election-day/160814/){:target="_blank" rel="noopener"}

> Cyber-researchers weigh in on what concerns them the most as the U.S. heads into the final weekend before the presidential election -- and they also highlight the positives. [...]
