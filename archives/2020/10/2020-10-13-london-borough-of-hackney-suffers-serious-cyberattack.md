Title: London Borough of Hackney suffers ‘serious’ cyberattack
Date: 2020-10-13T09:47:57-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: london-borough-of-hackney-suffers-serious-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/london-borough-of-hackney-suffers-serious-cyberattack/){:target="_blank" rel="noopener"}

> The city council systems for the London Borough of Hackney have been hit with a 'serious' cyberattack that impacts many of their services and IT systems. [...]
