Title: Interview: the Anti-Phishing Working Group’s Peter Cassidy on finding the antidote to cybercrime
Date: 2020-10-28T13:27:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: interview-the-anti-phishing-working-groups-peter-cassidy-on-finding-the-antidote-to-cybercrime

[Source](https://portswigger.net/daily-swig/interview-the-anti-phishing-working-groups-peter-cassidy-on-finding-the-antidote-to-cybercrime){:target="_blank" rel="noopener"}

> Can threat data exchanges offer a vaccine for cyber-attacks? [...]
