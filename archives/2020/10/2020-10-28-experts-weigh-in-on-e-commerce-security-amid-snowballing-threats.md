Title: Experts Weigh in on E-Commerce Security Amid Snowballing Threats
Date: 2020-10-28T12:00:12+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Malware;Videos;Vulnerabilities;Web Security;Webinars;Allan Liska;card skimmer;COVID-19;DDoS attacks;ddos extortion;e-commerce;Fraud;holiday shopping;loyalty points;loyalty programs;magecart;Matt Wilson;NeuStar;NuData;online retail;online sales;online shoppers;online shopping;Pandemic;Phishing;phishing lures;Recorded Future;Robert Capps;scam;secure checkout;WAF;web application firewall
Slug: experts-weigh-in-on-e-commerce-security-amid-snowballing-threats

[Source](https://threatpost.com/experts-weigh-in-ecommerce-security/160630/){:target="_blank" rel="noopener"}

> How a retail sector reeling from COVID-19 can lock down their online systems to prevent fraud during the upcoming holiday shopping spike. [...]
