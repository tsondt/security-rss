Title: K8s on a plane! US Air Force slaps Googly container tech on yet another war machine to 'run advanced ML algorithms'
Date: 2020-10-08T14:05:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: k8s-on-a-plane-us-air-force-slaps-googly-container-tech-on-yet-another-war-machine-to-run-advanced-ml-algorithms

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/08/usaf_u2_spyplane_kubernetes_britten_norman/){:target="_blank" rel="noopener"}

> And if that's not Skynet enough for ya, Britten-Norman is working to make the BN-2 Islander fly autonomously The US Air Force (USAF) is deploying Kubernetes containerisation tech aboard some of its spyplanes – as UK-based Britten-Norman teams up to make one of its flagship aircraft semi-autonomous.... [...]
