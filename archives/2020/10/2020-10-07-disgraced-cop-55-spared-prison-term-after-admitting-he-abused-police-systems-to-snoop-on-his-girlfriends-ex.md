Title: Disgraced cop, 55, spared prison term after admitting he abused police systems to snoop on his girlfriend's ex
Date: 2020-10-07T11:15:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: disgraced-cop-55-spared-prison-term-after-admitting-he-abused-police-systems-to-snoop-on-his-girlfriends-ex

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/07/michael_westbury_police_computer_misuse/){:target="_blank" rel="noopener"}

> 120 hours' community service and suspended sentence for computer misuse and data protection crimes A police officer who quit while under investigation for computer misuse crimes has walked free from court after pleading guilty to a total of nine offences.... [...]
