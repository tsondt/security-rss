Title: Adobe Fixes 16 Critical Code-Execution Bugs Across Portfolio
Date: 2020-10-20T18:31:55+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;adobe;after effects;animate;arbitrary code execution;creative cloud installer;critical;dreamweaver;illustrator;InDesign;marketo;Media Encoder;october 2020;out of band;Patches;photoshop;premiere pro;Security Updates;XSS
Slug: adobe-fixes-16-critical-code-execution-bugs-across-portfolio

[Source](https://threatpost.com/adobe-critical-code-execution-bugs/160369/){:target="_blank" rel="noopener"}

> The out-of-band patches follow a lighter-than-usual Patch Tuesday update earlier this month. [...]
