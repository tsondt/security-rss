Title: How to automatically archive expected IAM Access Analyzer findings
Date: 2020-10-12T17:04:22+00:00
Author: Josh Joy
Category: AWS Security
Tags: AWS IAM Access Analyzer;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Automated reasoning;AWS Identity and Access Management;IAM Access Analyzer;Security Blog;Zelkova
Slug: how-to-automatically-archive-expected-iam-access-analyzer-findings

[Source](https://aws.amazon.com/blogs/security/how-to-automatically-archive-expected-iam-access-analyzer-findings/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) Access Analyzer continuously monitors your Amazon Web Services (AWS) resource-based policies for changes in order to identify resources that grant public or cross-account access from outside your AWS account or organization. Access Analyzer findings include detailed information that you can use to make an informed decision about whether access [...]
