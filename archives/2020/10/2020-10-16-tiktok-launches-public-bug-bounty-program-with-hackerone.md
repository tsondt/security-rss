Title: TikTok launches public bug bounty program with HackerOne
Date: 2020-10-16T11:13:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: tiktok-launches-public-bug-bounty-program-with-hackerone

[Source](https://portswigger.net/daily-swig/tiktok-launches-public-bug-bounty-program-with-hackerone){:target="_blank" rel="noopener"}

> Video-sharing app partners with crowdsourced security platform [...]
