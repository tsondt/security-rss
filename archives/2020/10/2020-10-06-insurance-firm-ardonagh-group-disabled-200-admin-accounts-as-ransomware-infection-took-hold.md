Title: Insurance firm Ardonagh Group disabled 200 admin accounts as ransomware infection took hold
Date: 2020-10-06T09:15:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: insurance-firm-ardonagh-group-disabled-200-admin-accounts-as-ransomware-infection-took-hold

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/06/ardonagh_group_ransomware/){:target="_blank" rel="noopener"}

> Firm says 'cyber incident' is being fought with third-party help Jersey-headquartered insurance company Ardonagh Group has suffered a potential ransomware infection.... [...]
