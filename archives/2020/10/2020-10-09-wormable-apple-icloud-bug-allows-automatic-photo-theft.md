Title: Wormable Apple iCloud Bug Allows Automatic Photo Theft
Date: 2020-10-09T13:02:12+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Bug Bounty;Cloud Security;Hacks;IoT;Mobile Security;Privacy;Vulnerabilities;Web Security;$300;000;apple;Apple Bug Bounty Program;Applications;authentication bypass;bug bounty;critical bugs;Critical flaws;Developers;ethical hackers;Hackers;hardware;iCloud;sam curry;software;source code;takeover;three-month hack;vulnerabilities;wormable;XSS
Slug: wormable-apple-icloud-bug-allows-automatic-photo-theft

[Source](https://threatpost.com/3-month-apple-hack-vulnerabilities-critical/159988/){:target="_blank" rel="noopener"}

> Ethical hackers so far have earned nearly $300K in payouts from the Apple bug-bounty program for discovering 55 bugs, 11 of them critical, during a three-month hack. [...]
