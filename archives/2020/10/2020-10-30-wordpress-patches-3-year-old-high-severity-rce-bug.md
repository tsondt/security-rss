Title: WordPress Patches 3-Year-Old High-Severity RCE Bug
Date: 2020-10-30T20:56:19+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities;Web Security;Cross Site Forgery;cross site scripting vulnerability;DeteAct;DoS;MySQL;RCE;remote code execution;wordpress;WordPress 3.7;WordPress 5.5.1;WordPress 5.5.2;WordPress Security and Maintenance Release
Slug: wordpress-patches-3-year-old-high-severity-rce-bug

[Source](https://threatpost.com/wordpress-patches-rce-bug/160812/){:target="_blank" rel="noopener"}

> In all, WordPress patched 10 security bugs as part of the release of version 5.5.2 of its web publishing software. [...]
