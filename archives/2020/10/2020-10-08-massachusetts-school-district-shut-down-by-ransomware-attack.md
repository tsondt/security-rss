Title: Massachusetts school district shut down by ransomware attack
Date: 2020-10-08T15:31:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: massachusetts-school-district-shut-down-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/massachusetts-school-district-shut-down-by-ransomware-attack/){:target="_blank" rel="noopener"}

> The Springfield Public Schools district in Massachusetts has become the victim of a ransomware attack that has caused the closure of schools while they investigate the cyberattack. [...]
