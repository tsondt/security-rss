Title: Complexity has broken computer security, says academic who helped spot Meltdown and Spectre flaws
Date: 2020-10-02T15:15:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: complexity-has-broken-computer-security-says-academic-who-helped-spot-meltdown-and-spectre-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/02/daniel_gruss_complexity_broke_security/){:target="_blank" rel="noopener"}

> Graz University of Tech's Daniel Gruss thinks natural sciences can save us Complexity has broken cybersecurity, but a reappraisal of computer science can keep us safe.... [...]
