Title: Facebook Debuts Bug-Bounty ‘Loyalty Program’
Date: 2020-10-09T14:50:25+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Facebook;Vulnerabilities;Web Security;bonuses;bounty hunter;bug bounty;facebook login sdk;hacker;hacker plus;leagues;loyalty program;tiers;XSS Flaw
Slug: facebook-debuts-bug-bounty-loyalty-program

[Source](https://threatpost.com/facebook-bug-bounty-loyalty-program/159993/){:target="_blank" rel="noopener"}

> Facebook bounty hunters will be placed into tiers by analyzing their score, signal and number of submitted bug reports -- which will dictate new bonus percentages. [...]
