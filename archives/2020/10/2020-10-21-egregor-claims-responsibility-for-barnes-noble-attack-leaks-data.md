Title: Egregor Claims Responsibility for Barnes & Noble Attack, Leaks Data
Date: 2020-10-21T15:30:55+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Malware;audit data;barnes & noble;cyberattack;Dark Web;data leak;egregor;encrypted files;financial data;network access seller;ransomware;stolen data;underground
Slug: egregor-claims-responsibility-for-barnes-noble-attack-leaks-data

[Source](https://threatpost.com/egregor-responsibility-barnes-noble/160401/){:target="_blank" rel="noopener"}

> The ransomware gang claims to have bought network access to the bookseller's systems before encrypting the networks and stealing "financial and audit data." [...]
