Title: One year after server hackers left NordVPN red-faced, firm's first colocated setup is online
Date: 2020-10-12T10:18:15+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: one-year-after-server-hackers-left-nordvpn-red-faced-firms-first-colocated-setup-is-online

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/12/roundup_week_oct9/){:target="_blank" rel="noopener"}

> Plus: Bunch of Cisco fixes for Patch Tuesday week, Fitbit kit hit, RAT malware written in Golang, and more In brief NordVPN has hit the go-live button for the first of its colocated servers.... [...]
