Title: The seven deadly sins letting hackers hijack America's govt networks: These unpatched bugs leave systems open
Date: 2020-10-12T23:26:06+00:00
Author: Shaun Nichols
Category: The Register
Tags: 
Slug: the-seven-deadly-sins-letting-hackers-hijack-americas-govt-networks-these-unpatched-bugs-leave-systems-open

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/12/cisa_fbi_warning/){:target="_blank" rel="noopener"}

> 'Unauthorized access to elections support systems' detected tho 'no evidence to date that integrity of elections data has been compromised' If you're wondering which bugs in particular miscreants are exploiting to break into, or attempt to break into, US government networks, wonder no more. And then make sure you've patched them.... [...]
