Title: Promising Infusions of Cash, Fake Investor John Bernard Walked Away With $30M
Date: 2020-10-07T14:58:51+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Docklands Enterprise Ltd.;Ecaterina Dudorenko;Inside Knowledge Solutions Ltd.;Iryna Davies;John Bernard;John Clifton Davies;Katherine Miller;Organized Crime and Corruption Reporting Project;Pravda;SafeSwiss Secure Communication AG;Secure Swiss Data;Sergey Valentinov Pankov;The Inside Knowledge;The Private Office of John Bernard;the-private-office.ch
Slug: promising-infusions-of-cash-fake-investor-john-bernard-walked-away-with-30m

[Source](https://krebsonsecurity.com/2020/10/promising-infusions-of-cash-fake-investor-john-bernard-walked-away-with-30m/){:target="_blank" rel="noopener"}

> September featured two stories on a phony tech investor named John Bernard, a pseudonym used by a convicted thief named John Clifton Davies who’s fleeced dozens of technology companies out of an estimated $30 million with the promise of lucrative investments. Those stories prompted a flood of tips from Davies’ victims that paints a much clearer picture of this serial con man and his cohorts, including allegations of hacking, smuggling, bank fraud and murder. KrebsOnSecurity interviewed more than a dozen of Davies’ victims over the past five years, none of whom wished to be quoted here out of fear of [...]
