Title: COVID-19 security tips: Ensure you sack your staff without leaving their IT access enabled, says Secureworks
Date: 2020-10-15T17:30:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: covid-19-security-tips-ensure-you-sack-your-staff-without-leaving-their-it-access-enabled-says-secureworks

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/15/secureworks_report/){:target="_blank" rel="noopener"}

> Infosec biz issues mildly off-the-wall guidance for incident responders The global switch to remote working in early 2020 gave hackers a whole new set of juicy ransomware targets.... [...]
