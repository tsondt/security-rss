Title: Friday Squid Blogging: Squid-like Nebula
Date: 2020-10-23T21:05:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-squid-like-nebula

[Source](https://www.schneier.com/blog/archives/2020/10/friday-squid-blogging-squid-like-nebula.html){:target="_blank" rel="noopener"}

> Pretty astronomical photo. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
