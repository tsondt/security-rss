Title: Security much? Twitter should have had a CISO to prevent Bitcoin hack, says US state financial body
Date: 2020-10-15T14:15:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: security-much-twitter-should-have-had-a-ciso-to-prevent-bitcoin-hack-says-us-state-financial-body

[Source](https://go.theregister.com/feed/www.theregister.com/2020/10/15/twitter_hack_report_new_york_post_censorship/){:target="_blank" rel="noopener"}

> Plus: Platform censors US newspaper and triggers ordure tsunami American financial regulators in New York have demanded Twitter be subject to harsher rules following the July hacks of prominent users' accounts – as CEO Jack Dorsey furiously backpedals after his website censored a news article from a US newspaper.... [...]
