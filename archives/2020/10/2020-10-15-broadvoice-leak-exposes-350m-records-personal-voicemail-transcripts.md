Title: Broadvoice Leak Exposes 350M Records, Personal Voicemail Transcripts
Date: 2020-10-15T14:46:56+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Privacy;350 million;b-hive;broadvoice;cloud communications;Cloud misconfiguration;cloud pbx;Customer Records;data breach;data collections;data leak;data retention policies;Elasticsearch;personal data;voicemail transcripts;VoIP
Slug: broadvoice-leak-exposes-350m-records-personal-voicemail-transcripts

[Source](https://threatpost.com/broadvoice-leaks-350m-records-voicemail-transcripts/160158/){:target="_blank" rel="noopener"}

> Companies that use Broadvoice's cloud-based VoIP platform may find their patients, customers, suppliers and partners to be impacted by a massive data exposure. [...]
