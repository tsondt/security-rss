Title: Israeli spyware firm fails to get hacking case dismissed
Date: 2020-01-16T10:27:24+00:00
Author: Oliver Holmes and Stephanie Kirchgaessner
Category: The Guardian
Tags: Espionage;Israel;Data and computer security;Saudi Arabia;WhatsApp;Hacking;Middle East and North Africa;Technology;World news;Jamal Khashoggi
Slug: israeli-spyware-firm-fails-to-get-hacking-case-dismissed

[Source](https://www.theguardian.com/world/2020/jan/16/israeli-spyware-firm-nso-hacking-case){:target="_blank" rel="noopener"}

> Judge orders NSO Group to fight case brought by Saudi activist and pay his legal costs An Israeli judge has rejected an attempt by the spyware firm NSO Group to dismiss a case brought against it by a prominent Saudi activist who alleged that the company’s cyberweapons were used to hack his phone. The decision could add pressure on the company, which faces multiple accusations that it sold surveillance technology, named Pegasus, to authoritarian regimes and other governments that have allegedly used it to target political activists and journalists. Continue reading... [...]
