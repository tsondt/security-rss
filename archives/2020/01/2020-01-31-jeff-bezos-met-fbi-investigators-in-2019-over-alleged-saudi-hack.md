Title: Jeff Bezos met FBI investigators in 2019 over alleged Saudi hack
Date: 2020-01-31T12:03:02+00:00
Author: Stephanie Kirchgaessner in Washington
Category: The Guardian
Tags: Jeff Bezos;FBI;US news;Technology;World news;Saudi Arabia;Middle East and North Africa;Hacking;Malware;Data and computer security;Israel
Slug: jeff-bezos-met-fbi-investigators-in-2019-over-alleged-saudi-hack

[Source](https://www.theguardian.com/technology/2020/jan/31/jeff-bezos-met-fbi-investigators-in-2019-over-alleged-saudi-hack){:target="_blank" rel="noopener"}

> Amazon founder interviewed as FBI conducts inquiry into Israeli firm linked to malware Jeff Bezos met federal investigators in April 2019 after they received information about the alleged hack of the billionaire’s mobile phone by Saudi Arabia, the Guardian has been told. Bezos was interviewed by investigators at a time when the FBI was conducting an investigation into the Israeli technology company NSO Group, according to a person who was present at the meeting. Continue reading... [...]
