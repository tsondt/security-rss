Title: Boris Johnson gets final warning with Huawei 5G verdict imminent
Date: 2020-01-28T09:49:52+00:00
Author: Kate Proctor Political correspondent
Category: The Guardian
Tags: Huawei;5G;UK security and counter-terrorism;Data and computer security;Surveillance;China;Foreign policy;Politics;UK news;Technology;Asia Pacific;World news
Slug: boris-johnson-gets-final-warning-with-huawei-5g-verdict-imminent

[Source](https://www.theguardian.com/technology/2020/jan/28/boris-johnson-gets-final-warning-with-huawei-5g-verdict-imminent){:target="_blank" rel="noopener"}

> Former senior government figures voice security fears as PM chairs meeting of NSC Former ministers have sounded their final warnings to Boris Johnson about the Chinese telecoms firm Huawei ahead of his expected decision on whether it will play a part in the UK’s 5G network. The prime minister will chair a meeting of the national security council (NSC) later on Tuesday before making a judgment on the firm’s future in the country after months of concern around security, including from the US president, Donald Trump. 5G is the next generation mobile phone network and it promises much higher connection [...]
