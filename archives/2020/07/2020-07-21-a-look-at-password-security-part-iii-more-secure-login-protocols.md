Title: A look at password security, Part III: More secure login protocols
Date: 2020-07-21T00:06:49+00:00
Author: Eric Rescorla
Category: Mozilla Security
Tags: General
Slug: a-look-at-password-security-part-iii-more-secure-login-protocols

[Source](https://blog.mozilla.org/blog/2020/07/20/a-look-at-password-security-part-iii-more-secure-login-protocols/){:target="_blank" rel="noopener"}

> In part II, we looked at the problem of Web authentication and covered the twin problems of phishing and password database compromise. In this post, I’ll be covering some of... Read more The post A look at password security, Part III: More secure login protocols appeared first on The Mozilla Blog. [...]
