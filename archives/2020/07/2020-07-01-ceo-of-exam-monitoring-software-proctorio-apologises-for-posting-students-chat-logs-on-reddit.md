Title: CEO of exam monitoring software Proctorio apologises for posting student's chat logs on Reddit
Date: 2020-07-01T06:27:38+00:00
Author: Naaman Zhou
Category: The Guardian
Tags: Australian universities;Australian education;Australia news;Coronavirus outbreak;Privacy;Data and computer security
Slug: ceo-of-exam-monitoring-software-proctorio-apologises-for-posting-students-chat-logs-on-reddit

[Source](https://www.theguardian.com/australia-news/2020/jul/01/ceo-of-exam-monitoring-software-proctorio-apologises-for-posting-students-chat-logs-on-reddit){:target="_blank" rel="noopener"}

> Australian students who have raised privacy concerns describe the incident involving a Canadian student as ‘freakishly disrespectful’ The chief executive of an exam monitoring software firm that has raised privacy concerns in Australia has apologised for publicly posting a student’s chat logs during an argument on the website Reddit. Mike Olsen, who is the CEO of the US-based Proctorio, has since deleted the posts and apologised, saying that he and Proctorio “take privacy very seriously”. Related: Coalition's university fee overhaul accused of being an 'attack on women' Related: Dan Tehan’s threat to police university enrolments can’t plug the holes in [...]
