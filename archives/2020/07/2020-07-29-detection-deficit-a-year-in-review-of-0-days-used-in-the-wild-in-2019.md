Title: Detection Deficit: A Year in Review of 0-days Used In-The-Wild in 2019
Date: 2020-07-29T10:27:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: detection-deficit-a-year-in-review-of-0-days-used-in-the-wild-in-2019

[Source](https://googleprojectzero.blogspot.com/2020/07/detection-deficit-year-in-review-of-0.html){:target="_blank" rel="noopener"}

> Posted by Maddie Stone, Project Zero In May 2019, Project Zero released our tracking spreadsheet for 0-days used “in the wild” and we started a more focused effort on analyzing and learning from these exploits. This is another way Project Zero is trying to make zero-day hard. This blog post synthesizes many of our efforts and what we’ve seen over the last year. We provide a review of what we can learn from 0-day exploits detected as used in the wild in 2019. In conjunction with this blog post, we are also publishing another blog post today about our root [...]
