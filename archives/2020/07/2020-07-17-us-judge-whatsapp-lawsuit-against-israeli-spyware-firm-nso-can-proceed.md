Title: US judge: WhatsApp lawsuit against Israeli spyware firm NSO can proceed
Date: 2020-07-17T16:27:51+00:00
Author: Stephanie Kirchgaessner in Washington
Category: The Guardian
Tags: WhatsApp;Israel;Technology;World news;Apps;Malware;Data and computer security
Slug: us-judge-whatsapp-lawsuit-against-israeli-spyware-firm-nso-can-proceed

[Source](https://www.theguardian.com/technology/2020/jul/17/us-judge-whatsapp-lawsuit-against-israeli-spyware-firm-nso-can-proceed){:target="_blank" rel="noopener"}

> NSO Group was sued last year by messaging app owned by Facebook Who has been using spyware on Catalan independence campaigners? An Israeli company whose spyware has been used to target journalists in India, politicians in Spain, and human rights activists in Morocco may soon be forced to divulge information about its government clients and practices after a judge in California ruled that a lawsuit against the company could proceed. NSO Group was sued by WhatsApp, which is owned by Facebook, last year, after the popular messaging app accused the company of sending malware to 1,400 of its users over [...]
