Title: Smartwatch maker Garmin hit by outages after ransomware attack
Date: 2020-07-24T07:49:35+00:00
Author: Mark Sweney
Category: The Guardian
Tags: Cybercrime;Business;Data and computer security;Smartwatches;Technology;US news;World news;Gadgets;Wearable technology
Slug: smartwatch-maker-garmin-hit-by-outages-after-ransomware-attack

[Source](https://www.theguardian.com/business/2020/jul/24/smartwatch-maker-garmin-hit-by-outages-after-ransomware-attack){:target="_blank" rel="noopener"}

> US company forced to shut down call centres, website and some other online services Garmin down: how to still get your activities on to Strava Garmin has been forced to shut down its call centres, website and some other online services after a ransomware attack encrypted the smartwatch maker’s internal network and some production systems. The US company shut down services including the official Garmin website and all customer services, including phone lines, online chat and email. Related: The five: ransomware attacks Ransomware is the most common form of criminal malware currently in use. Targets are commonly infected through malicious [...]
