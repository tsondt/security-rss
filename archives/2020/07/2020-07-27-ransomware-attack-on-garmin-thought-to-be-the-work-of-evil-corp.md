Title: Ransomware attack on Garmin thought to be the work of 'Evil Corp'
Date: 2020-07-27T17:57:17+00:00
Author: Alex Hern UK technology editor
Category: The Guardian
Tags: Cybercrime;Technology;Internet;Russia;World news;Computing;Data and computer security;Smartwatches;Wearable technology
Slug: ransomware-attack-on-garmin-thought-to-be-the-work-of-evil-corp

[Source](https://www.theguardian.com/technology/2020/jul/27/ransomware-attack-on-garmin-thought-to-be-the-work-of-evil-corp){:target="_blank" rel="noopener"}

> Russian cybercrime gang is believed to be responsible for taking Garmin services offline A ransomware attack that took the GPS and smartwatch business Garmin entirely offline for more than three days is believed to have been carried out by a Russian cybercriminal gang which calls itself “Evil Corp”. Garmin began to restore services to customers on Monday morning, after being held hostage for a reported ransom of $10m, although some services were still operating with limited functionality. Ransomware is the most common form of criminal malware currently in use. Targets are commonly infected through malicious emails, which may trick them [...]
