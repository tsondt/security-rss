Title: How to unc0ver a 0-day in 4 hours or less
Date: 2020-07-09T09:04:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: how-to-unc0ver-a-0-day-in-4-hours-or-less

[Source](https://googleprojectzero.blogspot.com/2020/07/how-to-unc0ver-0-day-in-4-hours-or-less.html){:target="_blank" rel="noopener"}

> By Brandon Azad, Project Zero At 3 PM PDT on May 23, 2020, the unc0ver jailbreak was released for iOS 13.5 (the latest signed version at the time of release) using a zero-day vulnerability and heavy obfuscation. By 7 PM, I had identified the vulnerability and informed Apple. By 1 AM, I had sent Apple a POC and my analysis. This post takes you along that journey. Initial identification I wanted to find the vulnerability used in unc0ver and report it to Apple quickly in order to demonstrate that obfuscating an exploit does little to prevent the bug from winding [...]
