Title: A look at password security, Part II: Web Sites
Date: 2020-07-13T16:36:40+00:00
Author: Eric Rescorla
Category: Mozilla Security
Tags: 
Slug: a-look-at-password-security-part-ii-web-sites

[Source](https://blog.mozilla.org/blog/2020/07/13/password-security-part-ii/){:target="_blank" rel="noopener"}

> In part I, we took a look at the design of password authentication systems for old-school multiuser systems. While timesharing is mostly gone, most of us continue to use multiuser... Read more The post A look at password security, Part II: Web Sites appeared first on The Mozilla Blog. [...]
