Title: Spanish deputy PM urges investigation into Catalan spyware claims
Date: 2020-07-16T16:08:39+00:00
Author: Sam Jones in Madrid
Category: The Guardian
Tags: Spain;World news;Europe;Technology;Data and computer security
Slug: spanish-deputy-pm-urges-investigation-into-catalan-spyware-claims

[Source](https://www.theguardian.com/world/2020/jul/16/spains-deputy-pm-urges-investigation-into-catalan-spyware-claims){:target="_blank" rel="noopener"}

> Exclusive: Pablo Iglesias calls alleged targeting of independence movement figures unacceptable The Spanish deputy prime minister, Pablo Iglesias, has become the most senior political figure to call for a parliamentary investigation into the use of spyware to target prominent members of the Catalan independence movement, saying such practices are “unacceptable in a democracy”. A joint investigation this week by the Guardian and El País has revealed that Roger Torrent, the speaker of the Catalan parliament, and former regional foreign minister Ernest Maragall are among at least four pro-independence activists who have been targeted using Israeli spyware that its makers said [...]
