Title: The core of Apple is PPL: Breaking the XNU kernel's kernel
Date: 2020-07-31T09:19:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: the-core-of-apple-is-ppl-breaking-the-xnu-kernels-kernel

[Source](https://googleprojectzero.blogspot.com/2020/07/the-core-of-apple-is-ppl-breaking-xnu.html){:target="_blank" rel="noopener"}

> Posted by Brandon Azad, Project Zero While doing research for the one-byte exploit technique, I considered several ways it might be possible to bypass Apple's Page Protection Layer (PPL) using just a physical address mapping primitive, that is, before obtaining kernel read/write or defeating PAC. Given that PPL is even more privileged than the rest of the XNU kernel, the idea of compromising PPL "before" XNU was appealing. In the end, though, I wasn't able to think of a way to break PPL using the physical mapping primitive alone. PPL's goal is to prevent an attacker from modifying a process's [...]
