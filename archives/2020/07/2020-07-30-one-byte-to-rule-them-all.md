Title: One Byte to rule them all
Date: 2020-07-30T09:17:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: one-byte-to-rule-them-all

[Source](https://googleprojectzero.blogspot.com/2020/07/one-byte-to-rule-them-all.html){:target="_blank" rel="noopener"}

> Posted by Brandon Azad, Project Zero One Byte to rule them all, One Byte to type them, One Byte to map them all, and in userspace bind them -- Comment above vm_map_copy_t For the last several years, nearly all iOS kernel exploits have followed the same high-level flow: memory corruption and fake Mach ports are used to gain access to the kernel task port, which provides an ideal kernel read/write primitive to userspace. Recent iOS kernel exploit mitigations like PAC and zone_require seem geared towards breaking the canonical techniques seen over and over again to achieve this exploit flow. But [...]
