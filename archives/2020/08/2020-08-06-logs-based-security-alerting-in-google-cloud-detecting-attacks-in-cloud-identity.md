Title: Logs-based Security Alerting in Google Cloud: Detecting attacks in Cloud Identity
Date: 2020-08-06T16:00:00+00:00
Author: Jeanno Cheung
Category: GCP Security
Tags: Google Cloud Platform;Cloud Identity;Identity & Security
Slug: logs-based-security-alerting-in-google-cloud-detecting-attacks-in-cloud-identity

[Source](https://cloud.google.com/blog/products/identity-security/logs-based-security-alerting-in-google-cloud/){:target="_blank" rel="noopener"}

> Shifting from an on-premise model to a cloud-based one opens up new opportunities when it comes to logging and securing your workloads. In this series of blog posts, we’ll cover some cloud-native technologies you can use to detect security threats and alert on logs in Google Cloud. The end result will be an end-to-end logs-based security alerting pipeline in Google Cloud Platform (GCP). We’ll start with a look into alerting on Cloud Identity logs in the Admin Console. Cloud Identity Customers use Cloud Identity to provision, manage, and authenticate users across their Google Cloud deployment. Cloud Identity is how the [...]
