Title: A look at password security, Part IV: WebAuthn
Date: 2020-08-20T18:35:19+00:00
Author: Eric Rescorla
Category: Mozilla Security
Tags: Firefox
Slug: a-look-at-password-security-part-iv-webauthn

[Source](https://blog.mozilla.org/blog/2020/08/20/password-security-part-iv-webauthn/){:target="_blank" rel="noopener"}

> As discussed in part III, public key authentication is great in principle but in practice has been hard to integrate into the Web environment. However, we’re now seeing deployment of... Read more The post A look at password security, Part IV: WebAuthn appeared first on The Mozilla Blog. [...]
