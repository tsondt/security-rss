Title: Security in the new normal: What happened week 4 at Google Cloud Next ’20: OnAir
Date: 2020-08-07T17:00:00+00:00
Author: Rob Sadowski
Category: GCP Security
Tags: Identity & Security;Next
Slug: security-in-the-new-normal-what-happened-week-4-at-google-cloud-next-20-onair

[Source](https://cloud.google.com/blog/topics/google-cloud-next/what-happened-week4-of-google-cloud-next20-onair/){:target="_blank" rel="noopener"}

> It’s hard to believe, but Google Cloud Next ‘20: OnAir Week 4 is already in our rearview. Week 4 focused on security, and we had a lot to share, from solution announcements and roadmaps, to best practices and tips, to demos and customer stories. It would be impossible for any one person to have experienced everything, so let’s take a look back at some highlights from the week and some resources going forward. Key announcements from the week Security is constantly evolving. When you add in all the ways work has changed due to the coronavirus pandemic, the rate of [...]
