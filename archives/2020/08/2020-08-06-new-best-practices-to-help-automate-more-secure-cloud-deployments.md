Title: New best practices to help automate more secure Cloud deployments
Date: 2020-08-06T16:00:00+00:00
Author: Andy Chang
Category: GCP Security
Tags: Next;Google Cloud Platform;Identity & Security
Slug: new-best-practices-to-help-automate-more-secure-cloud-deployments

[Source](https://cloud.google.com/blog/products/identity-security/best-practices-to-help-automate-more-secure-cloud-deployments/){:target="_blank" rel="noopener"}

> Organizations move to the cloud for many reasons, from improved efficiency, to ease of management, to better security. That’s right, one of the most important benefits of moving to the cloud is the opportunity to establish a robust baseline security and compliance posture. But it doesn’t just magically happen. While you can depend on Google Cloud’s secure-by-design core infrastructure, built-in product security features, and advanced security tools, you also need to configure cloud deployments to meet your own unique security and compliance requirements. We believe that a big part of our shared responsibility for security is to help make meeting [...]
