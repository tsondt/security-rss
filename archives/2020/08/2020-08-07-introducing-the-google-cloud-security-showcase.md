Title: Introducing the Google Cloud Security Showcase
Date: 2020-08-07T17:00:00+00:00
Author: Rob Sadowski
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: introducing-the-google-cloud-security-showcase

[Source](https://cloud.google.com/blog/products/identity-security/introducing-the-google-cloud-security-showcase/){:target="_blank" rel="noopener"}

> Security is at the heart of any cloud journey. On the one hand, as you adopt cloud services and move workloads to the cloud, you need to make sure you’re conforming to your established security policies. On the other hand, you can take advantage of new capabilities, use new tools, and help improve your security posture. We’ve had many conversations with our users to understand the most pressing security use cases they want to address in their cloud environments, and shared our expertise on how we can help. With the Google Cloud Security Showcase we want to share these insights [...]
