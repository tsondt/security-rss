Title: From recovery to reinvention: A Q&A with Equifax CTO Bryson Koehler
Date: 2020-08-11T16:00:00+00:00
Author: Glen Tillman
Category: GCP Security
Tags: Google Cloud Platform;Customers;Identity & Security
Slug: from-recovery-to-reinvention-a-qa-with-equifax-cto-bryson-koehler

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-helps-equifax-achieve-digital-transformation/){:target="_blank" rel="noopener"}

> With hundreds of millions of people and organizations relying on financial services organizations each day to keep their information safe and secure, trust is at the heart of everything these organizations do. Consumer credit reporting agency Equifax knows this all too well. In 2017, it experienced a historic data breach. As CTO, Bryson Koehler, recognized that rebuilding trust would mean fundamentally rethinking how Equifax managed technology and data security—and their business—as a whole. Today, Equifax is in a very different place. The company is transforming the majority of their IT operations on the strong foundation of Google Cloud, and in [...]
