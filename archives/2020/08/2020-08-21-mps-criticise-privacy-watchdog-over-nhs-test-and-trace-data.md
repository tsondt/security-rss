Title: MPs criticise privacy watchdog over NHS test-and-trace data
Date: 2020-08-21T05:00:41+00:00
Author: Alex Hern Technology editor
Category: The Guardian
Tags: Information commissioner;Data protection;Data and computer security;Technology;Coronavirus outbreak;House of Commons;Politics;UK news;NHS;Health;Health policy;Society
Slug: mps-criticise-privacy-watchdog-over-nhs-test-and-trace-data

[Source](https://www.theguardian.com/uk-news/2020/aug/21/mps-criticise-privacy-watchdog-information-commissioner-nhs-test-and-trace-data){:target="_blank" rel="noopener"}

> UK information commissioner ‘must ensure government uses public’s data safely and legally’ Coronavirus – latest updates See all our coronavirus coverage A cross-party group of more than 20 MPs has accused the UK’s privacy watchdog of failing to hold the government to account for its failures in the NHS coronavirus test-and-trace programme. The MPs have urged Elizabeth Denham, the information commissioner, to demand that the government change the programme after it admitted failing to conduct a legally required impact assessment of its privacy implications. Continue reading... [...]
