Title: Your data is not destined for China, assures TikTok’s UK boss
Date: 2020-08-23T05:56:48+00:00
Author: Chris Stokel-Walker
Category: The Guardian
Tags: TikTok;Music;Social media;Culture;Technology;Digital media;China;Cyberwar;UK news;Media;Judi Dench;Donald Trump;World news;US news;Asia Pacific;Apps;Data and computer security;Europe
Slug: your-data-is-not-destined-for-china-assures-tiktoks-uk-boss

[Source](https://www.theguardian.com/technology/2020/aug/23/date-china-tiktok-uk-boss-app-battle-digital-security-richard-waterworth){:target="_blank" rel="noopener"}

> The controversial app’s users are ignoring geopolitical battle over its digital security, says Richard Waterworth TikTok’s UK chief has strenuously denied the video-sharing app, which Donald Trump has threatened to ban, shares data with China. Richard Waterworth told the Observer that the UK and European arm of TikTok was growing quickly, despite the “turbulent” geopolitical battle in which the Chinese-born app has found itself. Continue reading... [...]
