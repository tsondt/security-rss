Title: The best of Google Cloud Next ’20: OnAir's Security Week for technical practitioners
Date: 2020-08-05T16:00:00+00:00
Author: Ian Lewis
Category: GCP Security
Tags: Google Cloud Platform;Next;Identity & Security
Slug: the-best-of-google-cloud-next-20-onairs-security-week-for-technical-practitioners

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-next20-onair-security-week-for-technical-practitioners/){:target="_blank" rel="noopener"}

> Hello security aficionados! This is your week for Google Cloud Next ’20: OnAir. There is a ton of content related to security coming out this week across a wide range of topics and audiences. With that in mind, here are some sessions that I think are particularly useful for security professionals and technical practitioners: Take Control of Security with Cloud Security Command Center : Guillaume Blaquiere from Veolia, along with Kyle Olive and Andy Chang from Google Cloud demonstrate how to prevent, detect, and respond to threats in virtual machines, containers, and more using Cloud Security Command Center. Authentication for [...]
