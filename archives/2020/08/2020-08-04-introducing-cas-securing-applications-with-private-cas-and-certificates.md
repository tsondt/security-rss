Title: Introducing CAS: Securing applications with private CAs and certificates
Date: 2020-08-04T12:00:00+00:00
Author: Anton Chuvakin
Category: GCP Security
Tags: Google Cloud Platform;Next;Identity & Security
Slug: introducing-cas-securing-applications-with-private-cas-and-certificates

[Source](https://cloud.google.com/blog/products/identity-security/introducing-cas-a-cloud-based-managed-ca-for-the-devops-and-iot-world/){:target="_blank" rel="noopener"}

> Digital certificates underpin identity and authentication for many networked devices and services. Recently, we’ve seen increased interest in using public key infrastructure (PKI) in DevOps and device management, particularly for IoT devices. But one of the most fundamental problems with PKI remains—it’s hard to set up Certificate Authorities (CA), and even harder to do it reliably at scale. To help, we’re announcing Certificate Authority Service (CAS), now in beta, from Google Cloud—a highly scalable and available service that simplifies and automates the management and deployment of private CAs while meeting the needs of modern developers and applications. To see how [...]
