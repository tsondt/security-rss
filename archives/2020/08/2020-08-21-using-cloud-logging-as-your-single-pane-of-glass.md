Title: Using Cloud Logging as your single pane of glass
Date: 2020-08-21T16:00:00+00:00
Author: Jeanno Cheung
Category: GCP Security
Tags: Cloud Identity;Stackdriver;Google Cloud Platform;Identity & Security
Slug: using-cloud-logging-as-your-single-pane-of-glass

[Source](https://cloud.google.com/blog/products/identity-security/centralize-cloud-identity-logs-behind-a-single-pane-of-glass/){:target="_blank" rel="noopener"}

> Logs are an essential tool for helping to secure your cloud deployments. In the first post in this series, we explored Cloud Identity logs and how you can configure alerts for potentially malicious activity in the Cloud Identity Admin Console to make your cloud deployment more secure. Today, we’ll take it a step further and look at how you can centralize collection of these logs to view activity across your deployment in a single pane of glass. Our best practices for enterprises using Google Cloud Platform (GCP) encourage customers to centralize log management, operations, searching, and analysis in GCP’s Cloud [...]
