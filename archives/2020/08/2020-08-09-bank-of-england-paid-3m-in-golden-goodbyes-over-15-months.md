Title: Bank of England paid £3m in 'golden goodbyes' over 15 months
Date: 2020-08-09T12:57:22+00:00
Author: Jasper Jolly
Category: The Guardian
Tags: Bank of England;Financial sector;Data and computer security;Hedge funds;Business;UK news;Technology
Slug: bank-of-england-paid-3m-in-golden-goodbyes-over-15-months

[Source](https://www.theguardian.com/business/2020/aug/09/bank-of-england-paid-3m-in-golden-goodbyes-over-15-months){:target="_blank" rel="noopener"}

> Rise in settlements in 2019 included those paid to departing tech security staff shortly before major breach The Bank of England paid departing staff almost £3m in “golden goodbyes” over 15 months, at the same time as an exodus of workers from its information security team. Settlement payments to former staff surged to £2.3m in 2019, according to data provided to the Guardian under freedom of information laws. The Bank confirmed that former information security staff received some of the payments. Continue reading... [...]
