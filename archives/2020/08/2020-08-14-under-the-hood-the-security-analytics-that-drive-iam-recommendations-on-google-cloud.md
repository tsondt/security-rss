Title: Under the hood: The security analytics that drive IAM recommendations on Google Cloud
Date: 2020-08-14T16:00:00+00:00
Author: Liang Zhang
Category: GCP Security
Tags: Google Cloud Platform;Next;Identity & Security
Slug: under-the-hood-the-security-analytics-that-drive-iam-recommendations-on-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/the-security-analytics-that-deliver-iam-recommendations/){:target="_blank" rel="noopener"}

> IAM Recommender helps security professionals enforce the principle of least privilege by identifying and removing unwanted access to Google Cloud Platform (GCP) resources. In our previous blog, we described some best practices for achieving least privilege with less effort using IAM Recommender—which uses machine learning to help determine what users actually need by analyzing their permission use over a 90-day period. In this post we’ll peek under the hood to see how IAM Recommender works, with the help of a step-by-step example. A DIY approach For a little more background, IAM Recommender generates daily policy recommendations and serves them to [...]
