Title: Achieve least privilege with less effort using IAM Recommender
Date: 2020-08-04T12:00:00+00:00
Author: Liang Zhang
Category: GCP Security
Tags: Next;Identity & Security
Slug: achieve-least-privilege-with-less-effort-using-iam-recommender

[Source](https://cloud.google.com/blog/products/identity-security/achieve-least-privilege-with-less-effort-using-iam-recommender/){:target="_blank" rel="noopener"}

> As cloud adoption grows, we’re seeing exponential growth in cloud resources. With this we’re also seeing growth in permissions, granted to humans and workloads, to access and change those resources. This introduces potential risks, including the misuse of privileges, that can compromise your organization’s security. To mitigate these risks, ideally every human or workload should only be granted the permissions they need, at the time they need them. This is the security best practice known as “least privilege access.” Unfortunately, we don’t implement this practice enough: In fact, internal Google research shows that most permissions granted by admins aren’t used [...]
