Title: VMware patches security flaws leading to RCE in SD-WAN Orchestrator
Date: 2020-11-27T16:11:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: vmware-patches-security-flaws-leading-to-rce-in-sd-wan-orchestrator

[Source](https://portswigger.net/daily-swig/vmware-patches-security-flaws-leading-to-rce-in-sd-wan-orchestrator){:target="_blank" rel="noopener"}

> Research concludes four-part series unearthing RCE chains in ‘single point of failure’ SD-WAN products [...]
