Title: Cold storage giant Americold services impacted by cyberattack
Date: 2020-11-16T19:02:10
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: cold-storage-giant-americold-services-impacted-by-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/cold-storage-giant-americold-services-impacted-by-cyberattack/){:target="_blank" rel="noopener"}

> Cold storage giant Americold is currently dealing with a cyberattack impacting their operations, including phone systems, email, inventory management, and order fulfillment. [...]
