Title: Are you protected from the latest threats to Industrial Control Systems?
Date: 2020-11-02T23:00:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: are-you-protected-from-the-latest-threats-to-industrial-control-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/02/ics_asia_pacific_summit/){:target="_blank" rel="noopener"}

> SANS Institute opens up ICS Asia Pacific Summit to all Promo 2020 has been a year of incredible uncertainty and upheaval, which for security professionals inevitably means threats have multiplied right across the enterprise.... [...]
