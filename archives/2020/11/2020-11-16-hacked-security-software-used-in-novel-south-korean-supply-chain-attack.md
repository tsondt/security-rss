Title: Hacked Security Software Used in Novel South Korean Supply-Chain Attack
Date: 2020-11-16T18:23:36+00:00
Author: Tom Spring
Category: Threatpost
Tags: Government;Hacks;Web Security;apt;Lazarus;Lazarus Group;Sony Pictures Entertainment;South Korea;Wizvera;Wizvera VeraPort
Slug: hacked-security-software-used-in-novel-south-korean-supply-chain-attack

[Source](https://threatpost.com/hacked-software-south-korea-supply-chain-attack/161257/){:target="_blank" rel="noopener"}

> Lazarus Group is believed to be behind a spate of attacks that leverage stolen digital certificates tied to browser software that secures communication with government and financial websites in South Korea. [...]
