Title: Laptop maker Compal hit by ransomware, $17 million demanded
Date: 2020-11-09T13:33:28
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: laptop-maker-compal-hit-by-ransomware-17-million-demanded

[Source](https://www.bleepingcomputer.com/news/security/laptop-maker-compal-hit-by-ransomware-17-million-demanded/){:target="_blank" rel="noopener"}

> Taiwanese laptop maker Compal Electronics suffered a DoppelPaymer ransomware attack over the weekend, with the attackers demanding an almost $17 million ransom. [...]
