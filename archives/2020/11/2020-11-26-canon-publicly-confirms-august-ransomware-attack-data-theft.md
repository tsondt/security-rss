Title: Canon publicly confirms August ransomware attack, data theft
Date: 2020-11-26T14:38:48
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: canon-publicly-confirms-august-ransomware-attack-data-theft

[Source](https://www.bleepingcomputer.com/news/security/canon-publicly-confirms-august-ransomware-attack-data-theft/){:target="_blank" rel="noopener"}

> Canon has finally confirmed publicly that the cyberattack suffered in early August was caused by ransomware and that the hackers stole data from company servers. [...]
