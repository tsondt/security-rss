Title: Laser-Based Hacking from Afar Goes Beyond Amazon Alexa
Date: 2020-11-25T14:40:09+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;IoT;Vulnerabilities;Web Security;Amazon Alexa;amazon echo;apple Siri;Connected devices;cyberattack;digital home assistants;Eavesdropping;facebook portal;google home;Internet of things;laser pointer;MEMS;Smart Home;University of Florida;University of Michigan
Slug: laser-based-hacking-from-afar-goes-beyond-amazon-alexa

[Source](https://threatpost.com/light-based-attacks-digital-home/161583/){:target="_blank" rel="noopener"}

> The team that hacked Amazon Echo and other smart speakers using a laser pointer continue to investigate why MEMS microphones respond to sound. [...]
