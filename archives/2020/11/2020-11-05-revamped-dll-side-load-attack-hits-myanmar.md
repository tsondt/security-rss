Title: Revamped DLL side-load attack hits Myanmar
Date: 2020-11-05T02:02:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: revamped-dll-side-load-attack-hits-myanmar

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/05/killsomeone_dll_attack/){:target="_blank" rel="noopener"}

> Sophos reckons Chinese gangs are behind attack it's charmingly chosen to name ‘KillSomeOne’ Security vendor Sophos has suggested Chinese purveyors of advanced persistent threats (APTs) are behind a recent wave of attacks on non-governmental organisations and other commercial entities in Myanmar.... [...]
