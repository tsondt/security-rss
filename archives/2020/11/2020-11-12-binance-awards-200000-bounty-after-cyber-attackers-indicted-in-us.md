Title: Binance awards $200,000 bounty after cyber-attackers indicted in US
Date: 2020-11-12T12:39:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: binance-awards-200000-bounty-after-cyber-attackers-indicted-in-us

[Source](https://portswigger.net/daily-swig/binance-awards-200-000-bounty-after-cyber-attackers-indicted-in-us){:target="_blank" rel="noopener"}

> Crypto-exchange pays investigators after hackers identified [...]
