Title: FBI warns of increasing Ragnar Locker ransomware activity
Date: 2020-11-20T15:34:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-increasing-ragnar-locker-ransomware-activity

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-increasing-ragnar-locker-ransomware-activity/){:target="_blank" rel="noopener"}

> The U.S. Federal Bureau of Investigation (FBI) Cyber Division has warned private industry partners of increased Ragnar Locker ransomware activity following a confirmed attack from April 2020. [...]
