Title: Microsoft rolls out protection for critical accounts in Office 365
Date: 2020-11-19T11:39:38
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: microsoft-rolls-out-protection-for-critical-accounts-in-office-365

[Source](https://www.bleepingcomputer.com/news/security/microsoft-rolls-out-protection-for-critical-accounts-in-office-365/){:target="_blank" rel="noopener"}

> Microsoft has launched Office 365 priority protection for accounts of high-profile employees such as executive-level managers who are most often targeted by threat actors. [...]
