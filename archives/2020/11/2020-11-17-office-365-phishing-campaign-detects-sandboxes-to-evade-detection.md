Title: Office 365 phishing campaign detects sandboxes to evade detection
Date: 2020-11-17T11:15:38
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: office-365-phishing-campaign-detects-sandboxes-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/office-365-phishing-campaign-detects-sandboxes-to-evade-detection/){:target="_blank" rel="noopener"}

> Microsoft is tracking an ongoing Office 365 phishing campaign that makes use of several methods to evade automated analysis in attacks against enterprise targets. [...]
