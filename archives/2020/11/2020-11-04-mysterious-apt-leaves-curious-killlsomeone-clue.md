Title: Mysterious APT Leaves Curious ‘KilllSomeOne’ Clue
Date: 2020-11-04T22:42:54+00:00
Author: Tom Spring
Category: Threatpost
Tags: Government;Hacks;Malware;Web Security;DLL Side Load;DLL Sideloading;DLL spoofing;KillSomeOne;Windows
Slug: mysterious-apt-leaves-curious-killlsomeone-clue

[Source](https://threatpost.com/apt-leaves-killlsomeone-clue/160975/){:target="_blank" rel="noopener"}

> APT cloaks identity using script-kiddie messages and advanced deployment and targeting techniques. [...]
