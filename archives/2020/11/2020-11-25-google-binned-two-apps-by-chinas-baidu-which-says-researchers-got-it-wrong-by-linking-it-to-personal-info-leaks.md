Title: Google binned two apps by China’s Baidu, which says researchers got it wrong by linking it to personal info leaks
Date: 2020-11-25T01:58:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: google-binned-two-apps-by-chinas-baidu-which-says-researchers-got-it-wrong-by-linking-it-to-personal-info-leaks

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/25/palo_alto_detects_leaking_baidu_apps/){:target="_blank" rel="noopener"}

> Palo Alto Networks spotted subscriber IDs and MAC addresses on the move UPDATED Infosec researchers at Palo Alto Networks’ Unit 42 threat intelligence unit spotted a pair of prominent Chinese apps leaking personal data, and after it informed Google the ad giant dumped the apps from its Play store.... [...]
