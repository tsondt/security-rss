Title: Healthcare provider AspenPointe data breach affects 295K patients
Date: 2020-11-30T13:12:44
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: healthcare-provider-aspenpointe-data-breach-affects-295k-patients

[Source](https://www.bleepingcomputer.com/news/security/healthcare-provider-aspenpointe-data-breach-affects-295k-patients/){:target="_blank" rel="noopener"}

> U.S. healthcare provider AspenPointe notified patients of a data breach stemming from a September 2020 cyberattack that enabled attackers to steal protected health information (PHI) and personally identifiable information (PII). [...]
