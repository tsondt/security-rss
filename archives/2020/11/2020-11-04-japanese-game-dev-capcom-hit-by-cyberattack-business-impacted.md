Title: Japanese game dev Capcom hit by cyberattack, business impacted
Date: 2020-11-04T16:01:28
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: japanese-game-dev-capcom-hit-by-cyberattack-business-impacted

[Source](https://www.bleepingcomputer.com/news/security/japanese-game-dev-capcom-hit-by-cyberattack-business-impacted/){:target="_blank" rel="noopener"}

> Japanese game developer Capcom has disclosed that they suffered a cyberattack over the weekend that is impacting business operations, including email systems. [...]
