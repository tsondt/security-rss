Title: CERT/CC: 'Sensational' bug names spark fear, hype – so we'll give flaws our own labels... like Suggestive Bunny
Date: 2020-11-03T06:02:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: certcc-sensational-bug-names-spark-fear-hype-so-well-give-flaws-our-own-labels-like-suggestive-bunny

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/03/cert_bug_names/){:target="_blank" rel="noopener"}

> Officials go with randomly selected words with unintentionally hilarious results. Filthy Python, anyone? Many memorable events get named, whether they're hurricanes, political events, or security incidents like the Morris Worm, which surfaced 32 years ago yesterday.... [...]
