Title: Intel fixes 95 vulnerabilities in November 2020 Platform Update
Date: 2020-11-11T18:07:46
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: intel-fixes-95-vulnerabilities-in-november-2020-platform-update

[Source](https://www.bleepingcomputer.com/news/security/intel-fixes-95-vulnerabilities-in-november-2020-platform-update/){:target="_blank" rel="noopener"}

> Intel addressed 95 vulnerabilities as part of the November 2020 Patch Tuesday, including critical ones affecting Intel Wireless Bluetooth products and Intel Active Management Technology (AMT). [...]
