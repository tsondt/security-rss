Title: Be Very Sparing in Allowing Site Notifications
Date: 2020-11-17T14:13:29+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;Frank Angiolelli;Indelible LLC;Malwarebytes;mcafee;Norton;Pieter Arntz;PushWelcome
Slug: be-very-sparing-in-allowing-site-notifications

[Source](https://krebsonsecurity.com/2020/11/be-very-sparing-in-allowing-site-notifications/){:target="_blank" rel="noopener"}

> An increasing number of websites are asking visitors to approve “notifications,” browser modifications that periodically display messages on the user’s mobile or desktop device. In many cases these notifications are benign, but several dodgy firms are paying site owners to install their notification scripts and then selling that communications pathway to scammers and online hucksters. Notification prompts in Firefox (left) and Google Chrome. When a website you visit asks permission to send notifications and you approve the request, the resulting messages that pop up appear outside of the browser. For example, on Microsoft Windows systems they typically show up in [...]
