Title: Data breach at Mashable leaks users’&nbsp;personal information online
Date: 2020-11-09T12:41:27+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-at-mashable-leaks-users-personal-information-online

[Source](https://portswigger.net/daily-swig/data-breach-at-mashable-leaks-users-nbsp-personal-information-online){:target="_blank" rel="noopener"}

> US news site confirms database leak [...]
