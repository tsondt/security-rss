Title: Detecting Phishing Emails
Date: 2020-11-06T12:28:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;behavioral detection;e-mail;phishing;scams
Slug: detecting-phishing-emails

[Source](https://www.schneier.com/blog/archives/2020/11/detecting-phishing-emails.html){:target="_blank" rel="noopener"}

> Research paper: Rick Wash, “ How Experts Detect Phishing Scam Emails “: Abstract: Phishing scam emails are emails that pretend to be something they are not in order to get the recipient of the email to undertake some action they normally would not. While technical protections against phishing reduce the number of phishing emails received, they are not perfect and phishing remains one of the largest sources of security risk in technology and communication systems. To better understand the cognitive process that end users can use to identify phishing messages, I interviewed 21 IT experts about instances where they successfully [...]
