Title: Vermont hospitals still recovering from October ransomware attack
Date: 2020-11-30T11:37:15
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: vermont-hospitals-still-recovering-from-october-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/vermont-hospitals-still-recovering-from-october-ransomware-attack/){:target="_blank" rel="noopener"}

> The University of Vermont Health Network is still recovering from a Ryuk Ransomware attack in October 2020, with services slowly coming back online. [...]
