Title: UK infoseccer launches petition asking government not to backdoor encryption
Date: 2020-11-27T14:01:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: uk-infoseccer-launches-petition-asking-government-not-to-backdoor-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/27/encryption_backdoor_petition/){:target="_blank" rel="noopener"}

> Sign up and make your voice heard A UK infosec bod has launched a petition asking the government if it would please drop its plans to install backdoors in end-to-end encryption.... [...]
