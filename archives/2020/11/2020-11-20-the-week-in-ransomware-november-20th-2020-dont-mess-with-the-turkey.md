Title: The Week in Ransomware - November 20th 2020 - Don't mess with the turkey
Date: 2020-11-20T18:43:26
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-november-20th-2020-dont-mess-with-the-turkey

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-20th-2020-dont-mess-with-the-turkey/){:target="_blank" rel="noopener"}

> This week we saw two massive attacks that had a signifcant impact on the food supply industry, as well as a demonstration of Egregor's annoying ransom note print bombs. [...]
