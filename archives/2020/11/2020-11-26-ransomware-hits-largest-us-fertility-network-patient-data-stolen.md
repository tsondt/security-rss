Title: Ransomware hits largest US fertility network, patient data stolen
Date: 2020-11-26T11:26:34
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: ransomware-hits-largest-us-fertility-network-patient-data-stolen

[Source](https://www.bleepingcomputer.com/news/security/ransomware-hits-largest-us-fertility-network-patient-data-stolen/){:target="_blank" rel="noopener"}

> US Fertility, the largest network of fertility centers in the U.S., says that some of its systems were encrypted in a ransomware attack that affected the company two months ago, in September 2020. [...]
