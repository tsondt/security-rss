Title: Dating Site Bumble Leaves Swipes Unsecured for 100M Users
Date: 2020-11-16T22:09:54+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Mobile Security;Privacy;Vulnerabilities;API Bug;bumble;bumble boost;dating app;dating site;HackerOne;information exposure;personal information;security vulnerability;swipe right;the beeline
Slug: dating-site-bumble-leaves-swipes-unsecured-for-100m-users

[Source](https://threatpost.com/dating-site-bumble-swipes-unsecured-100m-users/161276/){:target="_blank" rel="noopener"}

> Bumble fumble: An API bug exposed personal information of users like political leanings, astrological signs, education, and even height and weight, and their distance away in miles. [...]
