Title: Crooks impersonate US govt agencies offering financial aid
Date: 2020-11-24T16:30:04
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: crooks-impersonate-us-govt-agencies-offering-financial-aid

[Source](https://www.bleepingcomputer.com/news/security/crooks-impersonate-us-govt-agencies-offering-financial-aid/){:target="_blank" rel="noopener"}

> Cybercriminals looking to steal personal information are baiting U.S. citizens with emails purporting to be from government agencies offering federal assistance. [...]
