Title: IBM Power9 processors beset by Cardiac Osprey data-leaking flaw as Spectre still haunts speculative chips
Date: 2020-11-20T21:21:58+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: ibm-power9-processors-beset-by-cardiac-osprey-data-leaking-flaw-as-spectre-still-haunts-speculative-chips

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/20/ibm_power9_flaw/){:target="_blank" rel="noopener"}

> Fix available, but Big Blue's chips get slower as a result IBM Power9 processors, intended for data centers and mainframes, are potentially vulnerable to abuse of their speculative execution capability. The security shortcoming could allow a local user to access privileged information.... [...]
