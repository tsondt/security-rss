Title: Android chat app with 100 million installs exposes private messages
Date: 2020-11-19T10:12:27
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: android-chat-app-with-100-million-installs-exposes-private-messages

[Source](https://www.bleepingcomputer.com/news/security/android-chat-app-with-100-million-installs-exposes-private-messages/){:target="_blank" rel="noopener"}

> GO SMS Pro, an Android instant messaging application with over 100 million installs, is publicly exposing private multimedia files shared between its users. [...]
