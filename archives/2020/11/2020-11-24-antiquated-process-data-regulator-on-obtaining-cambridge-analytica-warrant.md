Title: 'Antiquated process': data regulator on obtaining Cambridge Analytica warrant
Date: 2020-11-24T00:01:05+00:00
Author: Alex Hern
Category: The Guardian
Tags: Data protection;Information commissioner;Privacy;Cambridge Analytica;Facebook;Regulators;UK news;Data and computer security;Technology
Slug: antiquated-process-data-regulator-on-obtaining-cambridge-analytica-warrant

[Source](https://www.theguardian.com/technology/2020/nov/24/antiquated-process-data-regulator-obtaining-cambridge-analytica-warrant){:target="_blank" rel="noopener"}

> UK information commissioner calls for international approach to emerging threat The information commissioner has criticised the “antiquated process” that led to Facebook getting hold of Cambridge Analytica’s servers before the UK regulator itself, and renewed calls for an international approach to data privacy to tackle the emerging threat of data havens. Elizabeth Denham, the information commissioner, spoke to Damian Collins MP, the former chair of the digital, culture, media and sport committee, who led the parliamentary enquiry into disinformation, on his podcast Infotagion. She described discovering that Facebook was inside the offices of defunct electioneering consultancy Cambridge Analytica while in [...]
