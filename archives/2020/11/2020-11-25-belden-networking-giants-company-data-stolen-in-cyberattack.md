Title: Belden networking giant's company data stolen in cyberattack
Date: 2020-11-25T10:18:25
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: belden-networking-giants-company-data-stolen-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/belden-networking-giants-company-data-stolen-in-cyberattack/){:target="_blank" rel="noopener"}

> Network device manufacturer Belden was hit with a cyberattack that allowed threat actors to steal files containing information about employees and business partners. [...]
