Title: India securities regulator suggests webcam surveillance of finance folk who work from home
Date: 2020-11-02T01:07:49+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: india-securities-regulator-suggests-webcam-surveillance-of-finance-folk-who-work-from-home

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/02/sedi_india_security_advice/){:target="_blank" rel="noopener"}

> Random snaps suggested to authenticate users, along with hasty adoption of VPNs and MFA India’s Securities and Exchange Board (SEBI) appears to have sent a circular to stock exchanges that calls for market participants to upgrade information security as bad actors seek to take advantage of the financial services industry’s move to working from home.... [...]
