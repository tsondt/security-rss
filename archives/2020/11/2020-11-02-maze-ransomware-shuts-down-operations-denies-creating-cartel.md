Title: Maze ransomware shuts down operations, denies creating cartel
Date: 2020-11-02T08:25:58
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: maze-ransomware-shuts-down-operations-denies-creating-cartel

[Source](https://www.bleepingcomputer.com/news/security/maze-ransomware-shuts-down-operations-denies-creating-cartel/){:target="_blank" rel="noopener"}

> The Maze ransomware gang announced today that they have officially closed down their ransomware operation and will no longer be leaking new companies' data on their site. [...]
