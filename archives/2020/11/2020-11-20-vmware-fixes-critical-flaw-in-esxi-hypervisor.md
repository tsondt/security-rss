Title: VMware Fixes Critical Flaw in ESXi Hypervisor
Date: 2020-11-20T20:18:13+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;CVE-2020-4004;CVE-2020-4005;Hypervisor;Tiunfu Cup;vmware;VMware ESXi;vulnerability
Slug: vmware-fixes-critical-flaw-in-esxi-hypervisor

[Source](https://threatpost.com/vmware-critical-flaw-esxi-hypervisor/161457/){:target="_blank" rel="noopener"}

> The critical and important-severity flaws were found by a team at the China-based Tiunfu Cup hacking challenge. [...]
