Title: Ransomware crims read our bank balance and demanded the lot, reveals Scotland's Dundee and Angus College
Date: 2020-11-06T17:17:04+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ransomware-crims-read-our-bank-balance-and-demanded-the-lot-reveals-scotlands-dundee-and-angus-college

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/06/dundee_college_ransomware_bank_account/){:target="_blank" rel="noopener"}

> But we rebuilt the entire institution in 5 days, says principal as he looks back on February attack The criminals who took out Scotland's Dundee and Angus College made a ransom demand that precisely added up to the contents of its bank account – and that was no accident, its principal has said.... [...]
