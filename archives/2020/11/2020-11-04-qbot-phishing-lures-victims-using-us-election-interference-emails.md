Title: QBot phishing lures victims using US election interference emails
Date: 2020-11-04T18:18:51
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: qbot-phishing-lures-victims-using-us-election-interference-emails

[Source](https://www.bleepingcomputer.com/news/security/qbot-phishing-lures-victims-using-us-election-interference-emails/){:target="_blank" rel="noopener"}

> The Qbot botnet is now spewing U.S. election-themed phishing emails used to infect victims with malicious payloads designed to harvest user data and emails for use in future campaigns. [...]
