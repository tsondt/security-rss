Title: GitHub Actions platform vulnerable to code injection attacks – research
Date: 2020-11-06T15:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: github-actions-platform-vulnerable-to-code-injection-attacks-research

[Source](https://portswigger.net/daily-swig/github-actions-platform-vulnerable-to-code-injection-attacks-research){:target="_blank" rel="noopener"}

> Security flaw could allow attackers to write access to repositories [...]
