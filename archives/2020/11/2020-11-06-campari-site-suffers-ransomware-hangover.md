Title: Campari Site Suffers Ransomware Hangover
Date: 2020-11-06T19:42:36+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;campari;Matthew McConaughey;ragnar locker;ransom;ransomware;site restoration;wild turkey
Slug: campari-site-suffers-ransomware-hangover

[Source](https://threatpost.com/campari-site-ransomware-hangover/161029/){:target="_blank" rel="noopener"}

> The Ragnar Locker operators released a stolen contract between Wild Turkey and actor Matthew McConaughey, as proof of compromise. [...]
