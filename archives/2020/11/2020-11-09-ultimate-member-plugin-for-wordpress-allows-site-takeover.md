Title: Ultimate Member Plugin for WordPress Allows Site Takeover
Date: 2020-11-09T19:13:02+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security;Bugs;Cyberattacks;patch;plugin;privilege escalation;Security Vulnerabilities;site membership;site takeover;ultimate member;WordFence;wordpress
Slug: ultimate-member-plugin-for-wordpress-allows-site-takeover

[Source](https://threatpost.com/ultimate-member-plugin-wordpress-site-takeover/161053/){:target="_blank" rel="noopener"}

> Three critical security bugs allow for easy privilege escalation to an administrator role. [...]
