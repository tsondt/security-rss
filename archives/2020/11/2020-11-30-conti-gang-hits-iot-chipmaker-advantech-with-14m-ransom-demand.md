Title: Conti Gang Hits IoT Chipmaker Advantech with $14M Ransom Demand
Date: 2020-11-30T21:19:50+00:00
Author: Becky Bracken
Category: Threatpost
Tags: IoT;Malware;Web Security;$14 million ransom;Advantech;conti;cyberattack;Industrial IoT;Internet of things;ransomware
Slug: conti-gang-hits-iot-chipmaker-advantech-with-14m-ransom-demand

[Source](https://threatpost.com/conti-iot-chip-advantech-ransom-demand/161691/){:target="_blank" rel="noopener"}

> The ransomware group has leaked stolen data to add pressure on the company to pay up. [...]
