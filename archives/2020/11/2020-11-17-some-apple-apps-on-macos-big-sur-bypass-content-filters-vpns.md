Title: Some Apple Apps on macOS Big Sur Bypass Content Filters, VPNs
Date: 2020-11-17T13:23:21+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Vulnerabilities;Web Security;apple;apps;Big Sur;desktop;Developers;firewall;flaw;macOS;Privacy;software;VPN;vulnerability
Slug: some-apple-apps-on-macos-big-sur-bypass-content-filters-vpns

[Source](https://threatpost.com/some-apple-apps-on-macos-big-sur-bypass-content-filters-vpns/161295/){:target="_blank" rel="noopener"}

> Attackers can exploit the feature and send people’s data directly to remote servers, posing a privacy and security risk, researchers said. [...]
