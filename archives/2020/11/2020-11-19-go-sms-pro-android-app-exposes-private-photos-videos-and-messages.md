Title: GO SMS Pro Android App Exposes Private Photos, Videos and Messages
Date: 2020-11-19T19:52:25+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Privacy;Vulnerabilities;Web Security;Android;go sms pro;google play;information disclosure;media content exposure;mobile messaging app;patch;predictable URLs;private photos;security vulnerability;Trustwave SpiderLabs
Slug: go-sms-pro-android-app-exposes-private-photos-videos-and-messages

[Source](https://threatpost.com/go-sms-pro-android-app-exposes-private-photos/161407/){:target="_blank" rel="noopener"}

> The vulnerable version of the app, which has 100 million users, uses easily predictable URLs to link to private content. [...]
