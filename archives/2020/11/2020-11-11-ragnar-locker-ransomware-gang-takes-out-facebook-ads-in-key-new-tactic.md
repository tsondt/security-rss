Title: Ragnar Locker Ransomware Gang Takes Out Facebook Ads in Key New Tactic
Date: 2020-11-11T18:42:49+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Facebook;Hacks;Malware;campari;double extortion;facebook ads;ragnar locker;ransomware;Ransomware Attack;stolen data
Slug: ragnar-locker-ransomware-gang-takes-out-facebook-ads-in-key-new-tactic

[Source](https://threatpost.com/ragnar-locker-ransomware-facebook-ads/161133/){:target="_blank" rel="noopener"}

> Following a Nov. 3 ransomware attack against Campari, Ragnar Locker group took out public Facebook ads threatening to release stolen data. [...]
