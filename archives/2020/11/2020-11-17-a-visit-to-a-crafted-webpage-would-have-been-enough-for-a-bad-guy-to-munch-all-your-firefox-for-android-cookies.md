Title: A visit to a crafted webpage would have been enough for a bad guy to munch all your Firefox for Android cookies
Date: 2020-11-17T18:33:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: a-visit-to-a-crafted-webpage-would-have-been-enough-for-a-bad-guy-to-munch-all-your-firefox-for-android-cookies

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/17/firefox_cookie_theft/){:target="_blank" rel="noopener"}

> So make sure you've updated since July, fandroids A crafty person could have slurped every single cookie from a Firefox-using Android device by tricking a user to look at a specially crafted HTML file.... [...]
