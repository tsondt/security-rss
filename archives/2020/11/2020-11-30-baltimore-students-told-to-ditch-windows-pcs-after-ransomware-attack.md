Title: Baltimore students told to ditch Windows PCs after ransomware attack
Date: 2020-11-30T12:00:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: baltimore-students-told-to-ditch-windows-pcs-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/baltimore-students-told-to-ditch-windows-pcs-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Baltimore County Public Schools (BCPS) urged students and staff to stop using their school-issued Windows computers and only use Chromebooks and Google accounts following a ransomware attack that hit the district's network last Wednesday. [...]
