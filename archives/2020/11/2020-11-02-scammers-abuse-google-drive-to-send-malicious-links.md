Title: Scammers Abuse Google Drive to Send Malicious Links
Date: 2020-11-02T16:19:09+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Cloud Security;Hacks;Web Security;abuse;coronavirus;COVID-19;email security;Fraud;google;google drive;google drive notification;malicious link;Malicious URL;remote work;scam;scammer;Tripwire
Slug: scammers-abuse-google-drive-to-send-malicious-links

[Source](https://threatpost.com/scammers-google-drive-malicious-links/160832/){:target="_blank" rel="noopener"}

> Cybercriminals are sending malicious links to hundreds of thousands of users via Google Drive notifications. [...]
