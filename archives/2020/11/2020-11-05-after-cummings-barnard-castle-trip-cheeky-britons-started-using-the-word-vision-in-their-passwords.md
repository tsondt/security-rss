Title: After Cummings' Barnard Castle trip, cheeky Britons started using the word 'vision' in their passwords
Date: 2020-11-05T15:45:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: after-cummings-barnard-castle-trip-cheeky-britons-started-using-the-word-vision-in-their-passwords

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/05/passwords_ptp_study_vision_dominic_cummings/){:target="_blank" rel="noopener"}

> That was still dwarfed by clods using 'password' itself, though Britons began using the word "vision" in their passwords after prime ministerial advisor Dominic Cummings was caught travelling across the country from his parents' farm in Durham to Barnard Castle "to test" his eyesight, according to research from Pen Test Partners (PTP).... [...]
