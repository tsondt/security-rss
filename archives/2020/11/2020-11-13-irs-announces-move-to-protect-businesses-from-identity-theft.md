Title: IRS announces move to protect businesses from identity theft
Date: 2020-11-13T19:03:55
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: irs-announces-move-to-protect-businesses-from-identity-theft

[Source](https://www.bleepingcomputer.com/news/security/irs-announces-move-to-protect-businesses-from-identity-theft/){:target="_blank" rel="noopener"}

> The U.S. Internal Revenue Service (IRS) has announced today that sensitive information will be masked on all business tax transcripts starting next month to protect companies from identity theft. [...]
