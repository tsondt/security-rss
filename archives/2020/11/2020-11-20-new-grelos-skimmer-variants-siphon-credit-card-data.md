Title: New Grelos Skimmer Variants Siphon Credit Card Data
Date: 2020-11-20T17:23:33+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security;compromised payment card;Credit Card Theft;data breach;Grelos;magecart;web skimmer;webSocket
Slug: new-grelos-skimmer-variants-siphon-credit-card-data

[Source](https://threatpost.com/grelos-skimmer-variants-credit-card/161439/){:target="_blank" rel="noopener"}

> Domains related to the new variant of the Grelos web skimmer have compromised dozens of websites so far. [...]
