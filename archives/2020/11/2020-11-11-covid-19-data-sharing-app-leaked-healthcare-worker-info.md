Title: COVID-19 Data-Sharing App Leaked Healthcare Worker Info
Date: 2020-11-11T13:34:14+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Privacy;Vulnerabilities;Android;COVID-19;data breach;data leak;Dure Technologies;Healthcare data;ios;Pandemic;Philippines;Philippines Department of Health;The Citizen Lab;University of Toronto;vulnerabilities;Web;world health organization
Slug: covid-19-data-sharing-app-leaked-healthcare-worker-info

[Source](https://threatpost.com/covid-19-data-leaked-healthcare-worker-info/161108/){:target="_blank" rel="noopener"}

> Philippines COVID-KAYA app allowed for unauthorized access typically protected by ‘superuser’ credentials and also may have exposed patient data. [...]
