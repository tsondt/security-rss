Title: Microsoft warns against SMS, voice calls for multi-factor authentication: Try something that can't be SIM swapped
Date: 2020-11-11T21:19:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: microsoft-warns-against-sms-voice-calls-for-multi-factor-authentication-try-something-that-cant-be-sim-swapped

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/11/microsoft_mfa_warning/){:target="_blank" rel="noopener"}

> Sending codes over the insecure public telephone network isn't the way to go Microsoft on Tuesday advised internet users to embrace multi-factor authentication (MFA)... except where publicly switched telephone networks are involved.... [...]
