Title: Let's Encrypt warns about a third of Android devices will from next year stumble over sites that use its certs
Date: 2020-11-06T23:58:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: lets-encrypt-warns-about-a-third-of-android-devices-will-from-next-year-stumble-over-sites-that-use-its-certs

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/06/android_encryption_certs/){:target="_blank" rel="noopener"}

> Expiration of cross-signed root certificates spells trouble for pre-7.1.1 kit... unless they're using Firefox Let's Encrypt, a Certificate Authority (CA) that puts the "S" in "HTTPS" for about 220m domains, has issued a warning to users of older Android devices that their web surfing may get choppy next year.... [...]
