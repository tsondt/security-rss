Title: Origin Dollar cryptocurrency hacked to the tune of $7m less than two months after launch
Date: 2020-11-17T14:40:22+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: origin-dollar-cryptocurrency-hacked-to-the-tune-of-7m-less-than-two-months-after-launch

[Source](https://portswigger.net/daily-swig/origin-dollar-cryptocurrency-hacked-to-the-tune-of-7m-less-than-two-months-after-launch){:target="_blank" rel="noopener"}

> Project leads say legal action will not be taken against the culprit if they return the stolen funds [...]
