Title: Capcom hit by Ragnar Locker ransomware, 1TB allegedly stolen
Date: 2020-11-05T11:05:32
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: capcom-hit-by-ragnar-locker-ransomware-1tb-allegedly-stolen

[Source](https://www.bleepingcomputer.com/news/security/capcom-hit-by-ragnar-locker-ransomware-1tb-allegedly-stolen/){:target="_blank" rel="noopener"}

> Japanese game developer Capcom has suffered a ransomware attack where threat actors claim to have stolen 1TB of sensitive data from their corporate networks in the US, Japan, and Canada. [...]
