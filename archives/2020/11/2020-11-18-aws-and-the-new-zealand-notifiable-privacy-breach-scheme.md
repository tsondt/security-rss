Title: AWS and the New Zealand notifiable privacy breach scheme
Date: 2020-11-18T23:05:34+00:00
Author: Adam Star
Category: AWS Security
Tags: Announcements;AWS Artifact;Foundational (100);Security, Identity, & Compliance;AWS NZNDB Addendum;AWS Organizations NZNDB Addendum;New Zealand Privacy Act 2020;Notifiable Data Breaches;Notifiable Privacy Breaches;NZNDB Addendum;Security Blog
Slug: aws-and-the-new-zealand-notifiable-privacy-breach-scheme

[Source](https://aws.amazon.com/blogs/security/aws-and-the-new-zealand-notifiable-privacy-breach-scheme/){:target="_blank" rel="noopener"}

> The updated New Zealand Privacy Act 2020 (Privacy Act) will come into force on December 1, 2020. Importantly, it establishes a new notifiable privacy breach scheme (NZ scheme). The NZ scheme gives affected individuals the opportunity to take steps to protect their personal information following a privacy breach that has caused, or is likely to [...]
