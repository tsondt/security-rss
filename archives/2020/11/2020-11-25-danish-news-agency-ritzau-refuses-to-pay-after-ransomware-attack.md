Title: Danish news agency Ritzau refuses to pay after ransomware attack
Date: 2020-11-25T14:11:30
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: danish-news-agency-ritzau-refuses-to-pay-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/danish-news-agency-ritzau-refuses-to-pay-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Ritzau, the largest independent news agency in Denmark founded in 1866 by Erik Ritzau, said in a statement that it will not pay the ransom demanded by a ransomware gang that hit its network on Tuesday morning. [...]
