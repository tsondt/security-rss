Title: Microsoft fixes Windows zero-day disclosed by Google last month
Date: 2020-11-10T13:50:02
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-fixes-windows-zero-day-disclosed-by-google-last-month

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-windows-zero-day-disclosed-by-google-last-month/){:target="_blank" rel="noopener"}

> Microsoft has fixed today a Windows kernel zero-day vulnerability exploited in the wild as part of targeted attacks and publicly disclosed by Project Zero, Google's 0day bug-hunting team, last month. [...]
