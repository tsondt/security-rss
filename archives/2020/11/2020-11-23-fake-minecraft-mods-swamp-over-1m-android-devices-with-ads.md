Title: Fake Minecraft mods swamp over 1M Android devices with ads
Date: 2020-11-23T12:24:56
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: fake-minecraft-mods-swamp-over-1m-android-devices-with-ads

[Source](https://www.bleepingcomputer.com/news/security/fake-minecraft-mods-swamp-over-1m-android-devices-with-ads/){:target="_blank" rel="noopener"}

> Fraudsters bypassed Google's protections for the official Play Android store and published more than 20 fake modpacks for the popular game Minecraft. [...]
