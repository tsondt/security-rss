Title: 34M Records from 17 Companies Up for Sale in Cybercrime Forum
Date: 2020-11-03T19:10:55+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Privacy;Web Security;17 companies;34 million records;Alibaba;cyberattackers;Dark Web;data breach;data dump;eatigo;for sale;geekie;redmart;underground forum;wongnai
Slug: 34m-records-from-17-companies-up-for-sale-in-cybercrime-forum

[Source](https://threatpost.com/34m-records-17-companies-cybercrime-forum/160923/){:target="_blank" rel="noopener"}

> A diverse set of companies, including an adaptive-learning platform in Brazil, an online grocery service in Singapore and a cold-brew coffee-maker company, are caught up in the large data trove. [...]
