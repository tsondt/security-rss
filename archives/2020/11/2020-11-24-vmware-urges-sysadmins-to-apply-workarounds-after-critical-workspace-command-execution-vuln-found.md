Title: VMware urges sysadmins to apply workarounds after critical Workspace command execution vuln found
Date: 2020-11-24T19:04:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: vmware-urges-sysadmins-to-apply-workarounds-after-critical-workspace-command-execution-vuln-found

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/24/vmware_urges_sysadmins_to_implement/){:target="_blank" rel="noopener"}

> If you've been pwned in the past, pay special attention to this one VMware has published a series of workarounds for critical command injection vulnerabilities in its Workspace One Access, Access Connector, Identity Manager and Identity Manager Connector products.... [...]
