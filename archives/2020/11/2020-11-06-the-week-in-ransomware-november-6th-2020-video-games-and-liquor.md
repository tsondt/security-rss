Title: The Week in Ransomware - November 6th 2020 - Video Games and Liquor!
Date: 2020-11-06T18:22:34
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-november-6th-2020-video-games-and-liquor

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-6th-2020-video-games-and-liquor/){:target="_blank" rel="noopener"}

> This week, it has been busy with attacks worldwide and one of the largest ransomware operations officially shutting down. [...]
