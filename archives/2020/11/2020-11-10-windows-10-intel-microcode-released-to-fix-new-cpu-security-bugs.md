Title: Windows 10 Intel microcode released to fix new CPU security bugs
Date: 2020-11-10T15:20:15
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: windows-10-intel-microcode-released-to-fix-new-cpu-security-bugs

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-10-intel-microcode-released-to-fix-new-cpu-security-bugs/){:target="_blank" rel="noopener"}

> Microsoft has released a new batch of Intel microcode updates for Windows 10 20H2, 2004, 1909, and older versions to fix new hardware vulnerabilities discovered in Intel CPUs. [...]
