Title: Firefox introduces HTTPS-Only Mode to shield users from insecure connections
Date: 2020-11-17T17:33:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: firefox-introduces-https-only-mode-to-shield-users-from-insecure-connections

[Source](https://portswigger.net/daily-swig/firefox-introduces-https-only-mode-to-shield-users-from-insecure-connections){:target="_blank" rel="noopener"}

> ‘A great interim solution to pave the way for a future HTTPS-only web’ [...]
