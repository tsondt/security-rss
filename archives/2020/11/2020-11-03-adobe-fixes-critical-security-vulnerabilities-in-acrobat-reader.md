Title: Adobe fixes critical security vulnerabilities in Acrobat, Reader
Date: 2020-11-03T12:40:11
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: adobe-fixes-critical-security-vulnerabilities-in-acrobat-reader

[Source](https://www.bleepingcomputer.com/news/security/adobe-fixes-critical-security-vulnerabilities-in-acrobat-reader/){:target="_blank" rel="noopener"}

> Adobe has released security updates to address critical severity vulnerabilities affecting Adobe Acrobat and Reader for Windows and macOS that could enable attackers to execute arbitrary code on vulnerable devices. [...]
