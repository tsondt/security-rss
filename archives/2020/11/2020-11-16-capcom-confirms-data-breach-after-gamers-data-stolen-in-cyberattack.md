Title: Capcom confirms data breach after gamers' data stolen in cyberattack
Date: 2020-11-16T10:24:27
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Gaming
Slug: capcom-confirms-data-breach-after-gamers-data-stolen-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/capcom-confirms-data-breach-after-gamers-data-stolen-in-cyberattack/){:target="_blank" rel="noopener"}

> Japanese game giant Capcom has announced a data breach after confirming that attackers stole sensitive customer and employee information during a recent ransomware attack. [...]
