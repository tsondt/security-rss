Title: Over 300K Spotify accounts hacked in credential stuffing attack
Date: 2020-11-23T16:07:49
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: over-300k-spotify-accounts-hacked-in-credential-stuffing-attack

[Source](https://www.bleepingcomputer.com/news/security/over-300k-spotify-accounts-hacked-in-credential-stuffing-attack/){:target="_blank" rel="noopener"}

> Hackers have been attempting to gain access to Spotify accounts using a database of 380 million records with login credentials and personal information collected from various sources. [...]
