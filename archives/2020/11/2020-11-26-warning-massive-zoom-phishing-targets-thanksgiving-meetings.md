Title: Warning: Massive Zoom phishing targets Thanksgiving meetings
Date: 2020-11-26T13:05:53
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: warning-massive-zoom-phishing-targets-thanksgiving-meetings

[Source](https://www.bleepingcomputer.com/news/security/warning-massive-zoom-phishing-targets-thanksgiving-meetings/){:target="_blank" rel="noopener"}

> Everyone should be on the lookout for a massive ongoing phishing attack today, pretending to be an invite for a Zoom meeting. Hosted on numerous landing pages, BleepingComputer has learned that thousands of users' credentials have already been stolen by the attack. [...]
