Title: Cisco Webex ‘Ghost’ Flaw Opens Meetings to Snooping
Date: 2020-11-18T18:58:08+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;Cisco;Cisco DNA Spaces Connector;cisco flaw;Cisco Integrated Management Controller;Cisco Security Update;Cisco WebEx;coronavirus;CVE-2020-3419;CVE-2020-3441;CVE-2020-3471
Slug: cisco-webex-ghost-flaw-opens-meetings-to-snooping

[Source](https://threatpost.com/cisco-webex-flaw-snooping/161355/){:target="_blank" rel="noopener"}

> Cisco patched the Webex flaw, as well as three critical-severity vulnerabilities, in a slew of security updates on Wednesday. [...]
