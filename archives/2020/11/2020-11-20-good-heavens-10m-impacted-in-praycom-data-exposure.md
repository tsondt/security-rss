Title: Good Heavens! 10M Impacted in Pray.com Data Exposure
Date: 2020-11-20T20:17:13+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Mobile Security;Web Security;$10 million;Amazon Web Services;AWS S3;bible verses;church donations;cloud bucket;Cloud misconfiguration;data breach;data exposure;james earl jones;joel osteen;kristin bell;mobile app;personally identifiable information;PII;pray.com;vpnMentor
Slug: good-heavens-10m-impacted-in-praycom-data-exposure

[Source](https://threatpost.com/10m-impacted-pray-com-data-exposure/161459/){:target="_blank" rel="noopener"}

> The information exposed in a public cloud bucket included PII, church-donation information, photos and users' contact lists. [...]
