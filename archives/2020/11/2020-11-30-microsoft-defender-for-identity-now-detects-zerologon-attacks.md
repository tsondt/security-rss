Title: Microsoft Defender for Identity now detects Zerologon attacks
Date: 2020-11-30T15:00:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-defender-for-identity-now-detects-zerologon-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-for-identity-now-detects-zerologon-attacks/){:target="_blank" rel="noopener"}

> Microsoft has added support for Zerologon exploitation detection to Microsoft Defender for Identity to allow Security Operations teams to detect on-premises attacks attempting to abuse this critical vulnerability. [...]
