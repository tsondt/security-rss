Title: Survey: Cybersecurity Skills Shortage is ‘Bad,’ But There’s Hope
Date: 2020-11-02T20:09:29+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Malware;Mobile Security;Most Recent ThreatLists;Vulnerabilities;Web Security;Automation;Cybersecurity Professionals;cybersecurity recruiting;Cybersecurity Skills Gap;How to Minimize the Impact of the Cybersecurity Skills Shortage;machine learning;managed security services;recruitment;skills gap;survey;training;TrustWave;workforce shortage
Slug: survey-cybersecurity-skills-shortage-is-bad-but-theres-hope

[Source](https://threatpost.com/cybersecurity-skills-shortage-survey/160866/){:target="_blank" rel="noopener"}

> Automation, strategic process design and an investment in training are the keys to managing the cybersecurity skills gap, according to a recent survey from Trustwave. [...]
