Title: Cybersecurity skills gap narrows for the first time
Date: 2020-11-11T16:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cybersecurity-skills-gap-narrows-for-the-first-time

[Source](https://portswigger.net/daily-swig/cybersecurity-skills-gap-narrows-for-the-first-time){:target="_blank" rel="noopener"}

> Most organizations also coped well with the switch to remote working [...]
