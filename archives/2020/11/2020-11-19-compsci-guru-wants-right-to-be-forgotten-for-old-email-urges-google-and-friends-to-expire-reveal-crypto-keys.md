Title: Compsci guru wants 'right to be forgotten' for old email, urges Google and friends to expire, reveal crypto-keys
Date: 2020-11-19T07:24:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: compsci-guru-wants-right-to-be-forgotten-for-old-email-urges-google-and-friends-to-expire-reveal-crypto-keys

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/19/dkim_encryption_expiration/){:target="_blank" rel="noopener"}

> DKIM 'makes us all more vulnerable to extortion, blackmail,' argues Green Matthew Green, associate professor of computer science at Johns Hopkins University in the US, wants Google and other email providers to make it possible for people to deny they've written old email messages.... [...]
