Title: Why Paying to Delete Stolen Data is Bonkers
Date: 2020-11-04T19:32:40+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ransomware;Coveware;Emsisoft;Fabian Wosar;ransomware
Slug: why-paying-to-delete-stolen-data-is-bonkers

[Source](https://krebsonsecurity.com/2020/11/why-paying-to-delete-stolen-data-is-bonkers/){:target="_blank" rel="noopener"}

> Companies hit by ransomware often face a dual threat: Even if they avoid paying the ransom and can restore things from scratch, about half the time the attackers also threaten to release sensitive stolen data unless the victim pays for a promise to have the data deleted. Leaving aside the notion that victims might have any real expectation the attackers will actually destroy the stolen data, new research suggests a fair number of victims who do pay up may see some or all of the stolen data published anyway. The findings come in a report today from Coveware, a company [...]
