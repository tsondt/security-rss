Title: OSINT: What is open source intelligence and how is it used?
Date: 2020-11-19T14:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: osint-what-is-open-source-intelligence-and-how-is-it-used

[Source](https://portswigger.net/daily-swig/osint-what-is-open-source-intelligence-and-how-is-it-used){:target="_blank" rel="noopener"}

> Long favored by spooks and spies, OSINT is also a powerful weapon in the security pro’s armory [...]
