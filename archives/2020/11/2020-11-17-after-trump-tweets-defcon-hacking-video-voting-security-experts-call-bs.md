Title: After Trump tweets Defcon hacking video, voting security experts call BS
Date: 2020-11-17T14:07:06+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;elections;electronic voting;hacking
Slug: after-trump-tweets-defcon-hacking-video-voting-security-experts-call-bs

[Source](https://arstechnica.com/?p=1723660){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) As President Trump continues to make unfounded claims of widespread election fraud, 59 of the world’s foremost experts on electronic voting are hitting back, saying that recent allegations of actual voting machine hacking “have been unsubstantiated or are technically incoherent.” Monday’s letter came after almost two weeks of baseless and unfounded claims from Trump and some of his supporters that this month’s presidential election had been “rigged” in favor of President-elect Joe Biden. On Thursday, Trump started a new round of disinformation when he took to Twitter to say that polling machines made by Dominion Voting [...]
