Title: Reverse shell botnet Gitpaste-12 spreads via GitHub and Pastebin
Date: 2020-11-06T04:22:13
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: reverse-shell-botnet-gitpaste-12-spreads-via-github-and-pastebin

[Source](https://www.bleepingcomputer.com/news/security/reverse-shell-botnet-gitpaste-12-spreads-via-github-and-pastebin/){:target="_blank" rel="noopener"}

> A newly discovered worm and botnet named Gitpaste-12 lives on GitHub and also uses Pastebin to host malicious code. The advanced malware comes equipped with reverse shell and crypto mining capabilities. [...]
