Title: Malspam Campaign Milks Election Uncertainty
Date: 2020-11-05T13:07:17+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Malware;2020 elections;E-mail;emotet;infostealer;malspam;malware;Malwarebytes Labs;qbot;Research;threat actors;Trojan;voters;voting
Slug: malspam-campaign-milks-election-uncertainty

[Source](https://threatpost.com/malspam-campaign-milks-election-uncertainty/160983/){:target="_blank" rel="noopener"}

> Emails try to lure victims with malicious documents claiming to have information about voting interference. [...]
