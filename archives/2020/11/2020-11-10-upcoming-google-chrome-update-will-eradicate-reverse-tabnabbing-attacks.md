Title: Upcoming Google Chrome update will eradicate reverse tabnabbing attacks
Date: 2020-11-10T12:17:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: upcoming-google-chrome-update-will-eradicate-reverse-tabnabbing-attacks

[Source](https://portswigger.net/daily-swig/upcoming-google-chrome-update-will-eradicate-reverse-tabnabbing-attacks){:target="_blank" rel="noopener"}

> In mitigating the risk of the phishing attack variant, Google is following in the footsteps of Safari and Firefox [...]
