Title: Widespread Scans Underway for RCE Bugs in WordPress Websites
Date: 2020-11-18T21:53:55+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security;epsilon framework;Hackers;internet scans;probes;RCE;remote code execution;Security Vulnerabilities;site takeover;themes;WordFence;wordpress
Slug: widespread-scans-underway-for-rce-bugs-in-wordpress-websites

[Source](https://threatpost.com/widespread-scans-rce-bugs-wordpress-websites/161374/){:target="_blank" rel="noopener"}

> WordPress websites using buggy Epsilon Framework themes are being hunted by hackers. [...]
