Title: GoDaddy Employees Tricked into Compromising Cryptocurrency Sites
Date: 2020-11-23T21:08:46+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;Web Security;CISA. vishing warning;Cryptocurrency;domain registry;GoDaddy;liquid;mobile endpoint;nice hash;Phising;security training;social engineering;Vishing;VPN
Slug: godaddy-employees-tricked-into-compromising-cryptocurrency-sites

[Source](https://threatpost.com/godaddy-employees-tricked-compromise-cryptocurrency/161520/){:target="_blank" rel="noopener"}

> ‘Vishing’ attack on GoDaddy employees gave fraudsters access to cryptocurrency service domains NiceHash, Liquid. [...]
