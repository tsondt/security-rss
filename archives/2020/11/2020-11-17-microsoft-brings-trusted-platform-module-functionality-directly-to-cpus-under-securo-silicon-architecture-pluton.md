Title: Microsoft brings Trusted Platform Module functionality directly to CPUs under securo-silicon architecture Pluton
Date: 2020-11-17T19:15:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: microsoft-brings-trusted-platform-module-functionality-directly-to-cpus-under-securo-silicon-architecture-pluton

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/17/microsoft_pluton_cpu_hardware_security/){:target="_blank" rel="noopener"}

> Intel, AMD, Qualcomm are all on board Microsoft has joined hands with Intel, AMD, and Qualcomm to release a new security chip called Pluton, which Redmond reckons will delete "entire vectors of attack" from the infosec landscape.... [...]
