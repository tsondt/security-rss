Title: Cyberup campaign: 80% of infosec pros fear they might fall foul of UK's outdated Computer Misuse Act
Date: 2020-11-19T14:49:03+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: cyberup-campaign-80-of-infosec-pros-fear-they-might-fall-foul-of-uks-outdated-computer-misuse-act

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/19/computer_misuse_act_reform_cyberup/){:target="_blank" rel="noopener"}

> Creaky old law holds back global competitiveness, says group A majority of British infosec professionals worry about accidentally breaking the UK's antiquated Computer Misuse Act, according to an industry campaign group that hopes to reform the law.... [...]
