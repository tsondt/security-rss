Title: Security luminary Bruce Schneier opens (ISC)2 conference with renewed call for public-interest technologists
Date: 2020-11-17T12:54:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-luminary-bruce-schneier-opens-isc2-conference-with-renewed-call-for-public-interest-technologists

[Source](https://portswigger.net/daily-swig/security-luminary-bruce-schneier-opens-isc-2-conference-with-renewed-call-for-public-interest-technologists){:target="_blank" rel="noopener"}

> ‘Technology is remaking the world, and we will never get the policy right if policymakers get the tech wrong’ [...]
