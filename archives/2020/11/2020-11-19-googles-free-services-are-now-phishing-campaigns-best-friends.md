Title: Google’s free services are now phishing campaign’s best friends
Date: 2020-11-19T09:00:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: googles-free-services-are-now-phishing-campaigns-best-friends

[Source](https://www.bleepingcomputer.com/news/security/google-s-free-services-are-now-phishing-campaign-s-best-friends/){:target="_blank" rel="noopener"}

> Threat actors are abusing Google's free productivity tools and services to create convincing phishing campaigns that steal your credentials or trick you into installing malware. [...]
