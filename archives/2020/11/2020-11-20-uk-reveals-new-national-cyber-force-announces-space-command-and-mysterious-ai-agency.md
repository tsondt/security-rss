Title: UK reveals new 'National Cyber Force', announces Space Command and mysterious AI agency
Date: 2020-11-20T07:39:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: uk-reveals-new-national-cyber-force-announces-space-command-and-mysterious-ai-agency

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/20/uk_ai_space_cyber_agency/){:target="_blank" rel="noopener"}

> Combined Ministry of Defence and GCHQ team has worked since April to 'transform cyber capabilities' The United Kingdom has announced £16.5 billion ($22bn) of new defence spending, some of which has gone towards a newly revealed National Cyber Force and some earmarked to create a Space Command and agency dedicated to AI.... [...]
