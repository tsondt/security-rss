Title: Manchester United: IT Systems Disrupted in Cyberattack
Date: 2020-11-23T17:15:49+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Hacks;Malware;Vulnerabilities;Web Security;cyberattack;fan data;football;it disruption;manchester united;match schedule;old trafford;personal data;soccer club
Slug: manchester-united-it-systems-disrupted-in-cyberattack

[Source](https://threatpost.com/manchester-united-disrupted-cyberattack/161488/){:target="_blank" rel="noopener"}

> The popular U.K. soccer club confirmed an attack but said personal fan data remains secure. [...]
