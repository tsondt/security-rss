Title: Michael Ellis as NSA General Counsel
Date: 2020-11-18T12:21:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: national security policy;NSA
Slug: michael-ellis-as-nsa-general-counsel

[Source](https://www.schneier.com/blog/archives/2020/11/michael-ellis-as-nsa-general-counsel.html){:target="_blank" rel="noopener"}

> Over at Lawfare, Susan Hennessey has an excellent primer on how Trump loyalist Michael Ellis got to be the NSA General Counsel, over the objections of NSA Director Paul Nakasone, and what Biden can and should do about it. While important details remain unclear, media accounts include numerous indications of irregularity in the process by which Ellis was selected for the job, including interference by the White House. At a minimum, the evidence of possible violations of civil service rules demand immediate investigation by Congress and the inspectors general of the Department of Defense and the NSA. The moment also [...]
