Title: Hackers are actively probing millions of WordPress sites
Date: 2020-11-17T17:02:17
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: hackers-are-actively-probing-millions-of-wordpress-sites

[Source](https://www.bleepingcomputer.com/news/security/hackers-are-actively-probing-millions-of-wordpress-sites/){:target="_blank" rel="noopener"}

> Unknown threat actors are scanning for WordPress websites with Epsilon Framework themes installed on over 150,000 sites and vulnerable to Function Injection attacks that could lead to full site takeovers. [...]
