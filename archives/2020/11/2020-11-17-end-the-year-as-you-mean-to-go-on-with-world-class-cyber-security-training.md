Title: End the year as you mean to go on... with world-class cyber-security training
Date: 2020-11-17T07:00:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: end-the-year-as-you-mean-to-go-on-with-world-class-cyber-security-training

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/17/sans_cybersecurity_training_course/){:target="_blank" rel="noopener"}

> Top speakers, new courses, all live online Promo If you work in cybersecurity you’ll know that come December, it’s time to kick back, take stock... and prepare for whatever devilish tricks the hacker community is planning to pull over Christmas and into 2021. And this year and next can be expected to be particularly challenging, with cyber criminals looking to take advantage of a chaotic 2020, whether it’s by targeting the security gaps opened up as your workforce has gone remote or ripping the headlines for enticing spear phishing material.... [...]
