Title: More on the Security of the 2020 US Election
Date: 2020-11-23T12:44:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;infrastructure;national security policy;voting
Slug: more-on-the-security-of-the-2020-us-election

[Source](https://www.schneier.com/blog/archives/2020/11/more-on-the-security-of-the-2020-us-election.html){:target="_blank" rel="noopener"}

> Last week I signed on to two joint letters about the security of the 2020 election. The first was as one of 59 election security experts, basically saying that while the election seems to have been both secure and accurate (voter suppression notwithstanding), we still need to work to secure our election systems: We are aware of alarming assertions being made that the 2020 election was “rigged” by exploiting technical vulnerabilities. However, in every case of which we are aware, these claims either have been unsubstantiated or are technically incoherent. To our collective knowledge, no credible evidence has been put [...]
