Title: How to deploy the AWS Solution for Security Hub Automated Response and Remediation
Date: 2020-11-19T23:05:03+00:00
Author: Ramesh Venkataraman
Category: AWS Security
Tags: AWS Security Hub;Intermediate (200);Security, Identity, & Compliance;Automation;AWS Lambda;DevSecOps;Open source;SecOps;Security Blog;Serverless
Slug: how-to-deploy-the-aws-solution-for-security-hub-automated-response-and-remediation

[Source](https://aws.amazon.com/blogs/security/how-to-deploy-the-aws-solution-for-security-hub-automated-response-and-remediation/){:target="_blank" rel="noopener"}

> In this blog post I show you how to deploy the Amazon Web Services (AWS) Solution for Security Hub Automated Response and Remediation. The first installment of this series was about how to create playbooks using Amazon CloudWatch Events, AWS Lambda functions, and AWS Security Hub custom actions that you can run manually based on [...]
