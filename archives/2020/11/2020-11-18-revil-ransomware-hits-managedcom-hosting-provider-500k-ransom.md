Title: REvil ransomware hits Managed.com hosting provider, 500K ransom
Date: 2020-11-18T10:53:27
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: revil-ransomware-hits-managedcom-hosting-provider-500k-ransom

[Source](https://www.bleepingcomputer.com/news/security/revil-ransomware-hits-managedcom-hosting-provider-500k-ransom/){:target="_blank" rel="noopener"}

> Managed web hosting provider Managed.com has taken their servers and web hosting systems offline as they struggle to recover from a weekend REvil ransomware attack. [...]
