Title: Samsung fixes critical Android flaws with November 2020 updates
Date: 2020-11-11T05:56:24
Author: Ax Sharma
Category: BleepingComputer
Tags: Mobile;Technology
Slug: samsung-fixes-critical-android-flaws-with-november-2020-updates

[Source](https://www.bleepingcomputer.com/news/security/samsung-fixes-critical-android-flaws-with-november-2020-updates/){:target="_blank" rel="noopener"}

> Samsung has rolled out November 2020 Android updates today on their Galaxy devices. These patch serious vulnerabilities along with enhancing the overall device functionality. [...]
