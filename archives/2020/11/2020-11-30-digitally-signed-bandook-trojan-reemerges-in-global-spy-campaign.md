Title: Digitally Signed Bandook Trojan Reemerges in Global Spy Campaign
Date: 2020-11-30T19:39:24+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Malware;Privacy;bandook;Check Point;Cyberattacks;Dark Caracal;global spy campaign;malicious macros;Malware analysis;targeted campaigns;Trojan;valid certificate;wave of attacks
Slug: digitally-signed-bandook-trojan-reemerges-in-global-spy-campaign

[Source](https://threatpost.com/digitally-signed-bandook-trojan-spy-campaign/161676/){:target="_blank" rel="noopener"}

> A strain of the 13-year old backdoor Bandook trojan has been spotted in an espionage campaign. [...]
