Title: Zoom rolls out security enhancements to stop zoombombing trolls
Date: 2020-11-16T17:55:34
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: zoom-rolls-out-security-enhancements-to-stop-zoombombing-trolls

[Source](https://www.bleepingcomputer.com/news/security/zoom-rolls-out-security-enhancements-to-stop-zoombombing-trolls/){:target="_blank" rel="noopener"}

> Zoom has announced today the rollout of new security enhancements designed to help meeting hosts to block zoombombing attempts and participants to report misbehaving users. [...]
