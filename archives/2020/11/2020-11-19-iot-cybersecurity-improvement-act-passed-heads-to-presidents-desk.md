Title: IoT Cybersecurity Improvement Act Passed, Heads to President’s Desk
Date: 2020-11-19T16:50:18+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: IoT;Vulnerabilities;Web Security;federal agencies;Federal Security;federal security requirements;insecure IoT devices;Internet of things;IoT cybersecurity improvement act;passed;president's desk;Privacy;security act;Senate;vulnerability disclosure policy
Slug: iot-cybersecurity-improvement-act-passed-heads-to-presidents-desk

[Source](https://threatpost.com/iot-cybersecurity-improvement-act-passed/161396/){:target="_blank" rel="noopener"}

> Security experts praised the newly approved IoT law as a step in the right direction for insecure connected federal devices. [...]
