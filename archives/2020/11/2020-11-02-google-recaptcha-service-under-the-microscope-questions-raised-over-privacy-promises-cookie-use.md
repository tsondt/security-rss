Title: Google reCAPTCHA service under the microscope: Questions raised over privacy promises, cookie use
Date: 2020-11-02T08:15:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: google-recaptcha-service-under-the-microscope-questions-raised-over-privacy-promises-cookie-use

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/02/google_ad_recaptcha/){:target="_blank" rel="noopener"}

> Web giant insists anti-bot service isn't used for personalized ads – but cookie claims don't quite add up Analysis Six years ago, Google revised its reCAPTCHA service, designed to filter out bots, scrapers, and other automated web browsing, and allow humans through to websites.... [...]
