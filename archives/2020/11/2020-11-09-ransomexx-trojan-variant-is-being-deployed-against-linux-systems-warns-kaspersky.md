Title: RansomEXX trojan variant is being deployed against Linux systems, warns Kaspersky
Date: 2020-11-09T17:12:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ransomexx-trojan-variant-is-being-deployed-against-linux-systems-warns-kaspersky

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/09/linux_ransomware_kaspersky/){:target="_blank" rel="noopener"}

> Inoculation is simple: MFA, regular timely patching A trojan targeting Linux and deployed by a known ransomware gang has been discovered by Russian antivirus firm Kaspersky.... [...]
