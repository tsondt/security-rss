Title: The Week in Ransomware - November 27th 2020 - Attacks continue
Date: 2020-11-27T15:31:31
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-november-27th-2020-attacks-continue

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-27th-2020-attacks-continue/){:target="_blank" rel="noopener"}

> With the USA holidays, this has been a relatively slow week in new research being released. We did, though, see some organizations get attacked or report historical attacks. [...]
