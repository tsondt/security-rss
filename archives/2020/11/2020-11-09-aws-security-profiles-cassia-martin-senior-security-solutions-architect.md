Title: AWS Security Profiles: Cassia Martin, Senior Security Solutions Architect
Date: 2020-11-09T19:53:34+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;AWS Security Profile;re:Invent 2020;Security Blog
Slug: aws-security-profiles-cassia-martin-senior-security-solutions-architect

[Source](https://aws.amazon.com/blogs/security/aws-security-profiles-cassia-martin-senior-security-solutions-architect/){:target="_blank" rel="noopener"}

> In the weeks leading up to re:Invent, we’ll share conversations we’ve had with people at AWS who will be presenting, and get a sneak peek at their work. How long have you been at AWS and what do you do in your current role? I’ve been at Amazon for nearly 4 years, and at AWS [...]
