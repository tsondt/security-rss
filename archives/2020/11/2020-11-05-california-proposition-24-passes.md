Title: California Proposition 24 Passes
Date: 2020-11-05T15:28:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: laws;privacy;voting
Slug: california-proposition-24-passes

[Source](https://www.schneier.com/blog/archives/2020/11/california-proposition-24-passes.html){:target="_blank" rel="noopener"}

> California’s Proposition 24, aimed at improving the California Consumer Privacy Act, passed this week. Analyses are very mixed. I was very mixed on the proposition, but on the whole I supported it. The proposition has some serious flaws, and was watered down by industry, but voting for privacy feels like it’s generally a good thing. [...]
