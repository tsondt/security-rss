Title: Zoom Takes on Zoom-Bombers Following FTC Settlement
Date: 2020-11-17T17:18:07+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Cryptography;Privacy;Web Security;at risk meeting notifier;Cyberattacks;disruptive participants;E2EE;Encryption;End to end encryption;FTC settlement;new security features;security controls;zoom;Zoom-bombing
Slug: zoom-takes-on-zoom-bombers-following-ftc-settlement

[Source](https://threatpost.com/zoom-bombers-ftc-settlement/161312/){:target="_blank" rel="noopener"}

> The videoconferencing giant has upped the ante on cybersecurity with three fresh disruption controls. [...]
