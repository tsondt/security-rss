Title: Baltimore County Public Schools hit by ransomware attack
Date: 2020-11-25T11:34:43
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: baltimore-county-public-schools-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/baltimore-county-public-schools-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Baltimore County Public Schools has been hit today by a ransomware attack that led to a systemic shutdown of its network due to the number of systems impacted in the attack. [...]
