Title: The Cloud trust paradox: To trust cloud computing more, you need the ability to trust it less
Date: 2020-11-06T17:00:00+00:00
Author: Il-Sung Lee
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: the-cloud-trust-paradox-to-trust-cloud-computing-more-you-need-the-ability-to-trust-it-less

[Source](https://cloud.google.com/blog/products/identity-security/trust-a-cloud-provider-that-enables-you-to-trust-them-less/){:target="_blank" rel="noopener"}

> At their core, many cloud security and, in fact, cloud computing discussions ultimately distill to trust. This concept of trust is much bigger than cyber security, and even bigger than a triad of security, privacy, and compliance. For example, trust may involve geopolitical matters focused on data residency and data sovereignty. At the same time, trust may even be about the emotional matters, something far removed from the digital domain of bits and bytes, going all the way to the entire society. In a decade since the rise of cloud computing, a lot of research has been generated on the [...]
