Title: Apple’s Safari browser blocks CNAME cloaking in Big Sur privacy boost
Date: 2020-11-13T16:52:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: apples-safari-browser-blocks-cname-cloaking-in-big-sur-privacy-boost

[Source](https://portswigger.net/daily-swig/apples-safari-browser-blocks-cname-cloaking-in-big-sur-privacy-boost){:target="_blank" rel="noopener"}

> Web browser also hampers bounce tracking, among other tweaks, across all latest Apple OS updates [...]
