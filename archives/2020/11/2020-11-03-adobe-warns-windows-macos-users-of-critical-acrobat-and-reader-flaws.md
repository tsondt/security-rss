Title: Adobe Warns Windows, MacOS Users of Critical Acrobat and Reader Flaws
Date: 2020-11-03T15:55:09+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;acrobat and reader;adobe;adobe acrobat and reader;arbitrary code execution;critical bugs;CVE-2020-24430;CVE-2020-24435;CVE-2020-24436;CVE-2020-24437;information disclosure;november 2020 patch update;patch tuesday;Security Vulnerabilities
Slug: adobe-warns-windows-macos-users-of-critical-acrobat-and-reader-flaws

[Source](https://threatpost.com/adobe-windows-macos-critical-acrobat-reader-flaws/160903/){:target="_blank" rel="noopener"}

> The critical-severity Adobe Acrobat and Reader vulnerabilities could enable arbitrary code execution and are part of a 14-CVE patch update. [...]
