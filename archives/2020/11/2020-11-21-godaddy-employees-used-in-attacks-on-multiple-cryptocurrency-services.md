Title: GoDaddy Employees Used in Attacks on Multiple Cryptocurrency Services
Date: 2020-11-21T18:15:49+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Web Fraud 2.0;Bibox;Celcius.network;Dan Race;Farsight Security;GitHub;GoDaddy;Namecheap;phishing;privateemail.com;Slack;vishing;Wirex.app
Slug: godaddy-employees-used-in-attacks-on-multiple-cryptocurrency-services

[Source](https://krebsonsecurity.com/2020/11/godaddy-employees-used-in-attacks-on-multiple-cryptocurrency-services/){:target="_blank" rel="noopener"}

> Fraudsters redirected email and web traffic destined for several cryptocurrency trading platforms over the past week. The attacks were facilitated by scams targeting employees at GoDaddy, the world’s largest domain name registrar, KrebsOnSecurity has learned. The incident is the latest incursion at GoDaddy that relied on tricking employees into transferring ownership and/or control over targeted domains to fraudsters. In March, a voice phishing scam targeting GoDaddy support employees allowed attackers to assume control over at least a half-dozen domain names, including transaction brokering site escrow.com. And in May of this year, GoDaddy disclosed that 28,000 of its customers’ web hosting [...]
