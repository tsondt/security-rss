Title: Websites that use mix of HTTP, HTTPS schemes may break under new Chrome SameSite rules
Date: 2020-11-23T17:48:38+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: websites-that-use-mix-of-http-https-schemes-may-break-under-new-chrome-samesite-rules

[Source](https://portswigger.net/daily-swig/websites-that-use-mix-of-http-https-schemes-may-break-under-new-chrome-samesite-rules){:target="_blank" rel="noopener"}

> Webmasters should make the switch soon, developers warn [...]
