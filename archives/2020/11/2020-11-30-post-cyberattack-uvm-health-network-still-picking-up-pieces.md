Title: Post-Cyberattack, UVM Health Network Still Picking Up Pieces
Date: 2020-11-30T21:25:27+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;cyberattack;Healthcare;healthcare security;Hospital;myChart;UVM health network
Slug: post-cyberattack-uvm-health-network-still-picking-up-pieces

[Source](https://threatpost.com/cyberattack-uvm-health-picking-up-pieces/161681/){:target="_blank" rel="noopener"}

> More than a month after the cyberattack first hit, the UVM health network is still grappling with delayed payment processing and other issues. [...]
