Title: Steelcase furniture giant down for 2 weeks after ransomware attack
Date: 2020-11-12T11:52:50
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: steelcase-furniture-giant-down-for-2-weeks-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/steelcase-furniture-giant-down-for-2-weeks-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Office furniture giant Steelcase says that no information was stolen during a Ryuk ransomware attack that forced them to shut down global operations for roughly two weeks. [...]
