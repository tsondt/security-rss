Title: Vulnerabilities in Ubuntu Desktop enabled root access in two simple steps
Date: 2020-11-11T15:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: vulnerabilities-in-ubuntu-desktop-enabled-root-access-in-two-simple-steps

[Source](https://portswigger.net/daily-swig/vulnerabilities-in-ubuntu-desktop-enabled-root-access-in-two-simple-steps){:target="_blank" rel="noopener"}

> How an accidental discovery saw one security researcher gain complete control of Linux devices [...]
