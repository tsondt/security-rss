Title: New Zealand Privacy Act: Updated data breach legislation comes into effect on December 1
Date: 2020-11-30T13:59:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: new-zealand-privacy-act-updated-data-breach-legislation-comes-into-effect-on-december-1

[Source](https://portswigger.net/daily-swig/new-zealand-privacy-act-updated-data-breach-legislation-comes-into-effect-on-december-1){:target="_blank" rel="noopener"}

> New data privacy law will mandate reporting of ‘serious’ security incidents [...]
