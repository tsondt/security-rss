Title: Ransomware forces E-Land South Korean retail giant to close stores
Date: 2020-11-23T13:37:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-forces-e-land-south-korean-retail-giant-to-close-stores

[Source](https://www.bleepingcomputer.com/news/security/ransomware-forces-e-land-south-korean-retail-giant-to-close-stores/){:target="_blank" rel="noopener"}

> South Korean conglomerate and retail giant E-Land has suffered a ransomware attack causing 23 of its retail stores to suspend operations while they deal with the attack. [...]
