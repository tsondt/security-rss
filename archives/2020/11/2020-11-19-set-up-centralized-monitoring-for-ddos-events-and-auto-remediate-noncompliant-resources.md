Title: Set up centralized monitoring for DDoS events and auto-remediate noncompliant resources
Date: 2020-11-19T19:21:33+00:00
Author: Fola Bolodeoku
Category: AWS Security
Tags: AWS Firewall Manager;AWS Shield;Intermediate (200);Security, Identity, & Compliance;AWS Threat Research Team;Compliance;DDoS;Detection;governance;Security Blog
Slug: set-up-centralized-monitoring-for-ddos-events-and-auto-remediate-noncompliant-resources

[Source](https://aws.amazon.com/blogs/security/set-up-centralized-monitoring-for-ddos-events-and-auto-remediate-noncompliant-resources/){:target="_blank" rel="noopener"}

> When you build applications on Amazon Web Services (AWS), it’s a common security practice to isolate production resources from non-production resources by logically grouping them into functional units or organizational units. There are many benefits to this approach, such as making it easier to implement the principal of least privilege, or reducing the scope of [...]
