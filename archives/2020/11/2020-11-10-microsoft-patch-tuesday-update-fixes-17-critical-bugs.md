Title: Microsoft Patch Tuesday Update Fixes 17 Critical Bugs
Date: 2020-11-10T21:12:21+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities;Web Security;critical vulnerabilities;CVE-2020-17078;CVE-2020-17087;google;Internet Explorer;Network File System;November Patch Tuesday;patch tuesday;Windows 10;Windows server
Slug: microsoft-patch-tuesday-update-fixes-17-critical-bugs

[Source](https://threatpost.com/microsoft-patch-tuesday-critical-bugs/161098/){:target="_blank" rel="noopener"}

> Remote code execution vulnerabilities dominate this month’s security bulletin of warnings and patches. [...]
