Title: Citrix SD-WAN Bugs Allow Remote Code Execution
Date: 2020-11-16T20:20:58+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;Web Security;citrix;CVE-2020–8271;CVE-2020–8272;CVE-2020–8273;realmode;remote code execution;sd-wan;Security Bugs;vulnerabilities
Slug: citrix-sd-wan-bugs-allow-remote-code-execution

[Source](https://threatpost.com/citrix-sd-wan-bugs-remote-code-execution/161274/){:target="_blank" rel="noopener"}

> The bugs tracked as CVE-2020–8271, CVE-2020–8272 and CVE-2020–8273 exist in the Citrix SD-WAN Center. [...]
