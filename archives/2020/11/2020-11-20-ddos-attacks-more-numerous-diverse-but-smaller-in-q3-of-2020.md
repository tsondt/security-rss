Title: DDoS attacks more numerous, diverse, but smaller in Q3 of 2020
Date: 2020-11-20T16:47:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ddos-attacks-more-numerous-diverse-but-smaller-in-q3-of-2020

[Source](https://portswigger.net/daily-swig/ddos-attacks-more-numerous-diverse-but-smaller-in-q3-of-2020){:target="_blank" rel="noopener"}

> UDP-protocol attacks, often used in ransom-driven assaults, surged 2,680% [...]
