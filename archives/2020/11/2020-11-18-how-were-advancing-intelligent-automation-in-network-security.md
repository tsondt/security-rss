Title: How we're advancing intelligent automation in network security
Date: 2020-11-18T17:00:00+00:00
Author: Sam Lugani
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: how-were-advancing-intelligent-automation-in-network-security

[Source](https://cloud.google.com/blog/products/identity-security/security-enhancements-on-google-cloud-platform-november-2020/){:target="_blank" rel="noopener"}

> We’re always looking to make advanced security easier for enterprises so they can stay focused on their core business. Already this year, we’ve worked to strengthen DDoS protection, talked about some of the largest attacks we have stopped and made firewall defences more effective. We continue to push our pace of security innovation, and today we’re announcing enhancements to existing protections, as well as new capabilities to help customers protect their users, data, and applications in the cloud. 1. Using machine learning to detect and block DDoS Attacks with Adaptive Protection We recently talked about how our infrastructure absorbed a [...]
