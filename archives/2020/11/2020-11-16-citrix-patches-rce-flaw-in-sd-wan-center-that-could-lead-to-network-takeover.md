Title: Citrix patches RCE flaw in SD-WAN Center that could lead to network takeover
Date: 2020-11-16T17:15:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: citrix-patches-rce-flaw-in-sd-wan-center-that-could-lead-to-network-takeover

[Source](https://portswigger.net/daily-swig/citrix-patches-rce-flaw-in-sd-wan-center-that-could-lead-to-network-takeover){:target="_blank" rel="noopener"}

> Don’t let attackers eat CakePHP2 [...]
