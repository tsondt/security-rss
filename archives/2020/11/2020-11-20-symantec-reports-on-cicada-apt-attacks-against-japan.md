Title: Symantec Reports on Cicada APT Attacks against Japan
Date: 2020-11-20T12:05:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: advanced persistent threats;China;Japan;malware
Slug: symantec-reports-on-cicada-apt-attacks-against-japan

[Source](https://www.schneier.com/blog/archives/2020/11/symantec-reports-on-cicada-apt-attacks-against-japan.html){:target="_blank" rel="noopener"}

> Symantec is reporting on an APT group linked to China, named Cicada. They have been attacking organizations in Japan and elsewhere. Cicada has historically been known to target Japan-linked organizations, and has also targeted MSPs in the past. The group is using living-off-the-land tools as well as custom malware in this attack campaign, including a custom malware — Backdoor.Hartip — that Symantec has not seen being used by the group before. Among the machines compromised during this attack campaign were domain controllers and file servers, and there was evidence of files being exfiltrated from some of the compromised machines. The [...]
