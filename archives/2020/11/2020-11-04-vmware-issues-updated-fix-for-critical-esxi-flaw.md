Title: VMware Issues Updated Fix For Critical ESXi Flaw
Date: 2020-11-04T16:17:20+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;Bug;critical flaw;CVE-2020-3992;ESXi;OpenSLP;remote code execution;use-after-free;version ESXi70U1a-17119627;vmware
Slug: vmware-issues-updated-fix-for-critical-esxi-flaw

[Source](https://threatpost.com/vmware-updated-fix-critical-esxi-flaw/160944/){:target="_blank" rel="noopener"}

> A previous fix for the critical remote code execution bug was "incomplete," according to VMware. [...]
