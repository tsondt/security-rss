Title: Apple search bot leaked internal IPs via proxy configuration
Date: 2020-11-04T13:50:35
Author: Ax Sharma
Category: BleepingComputer
Tags: Apple
Slug: apple-search-bot-leaked-internal-ips-via-proxy-configuration

[Source](https://www.bleepingcomputer.com/news/security/apple-search-bot-leaked-internal-ips-via-proxy-configuration/){:target="_blank" rel="noopener"}

> A podcast creator and security researcher discovered that Apple's search bot, namely Applebot, crawling his podcast series had been leaking internal IPs due to a misconfigured proxy server. And, it took Apple over 9 months to fix this, for no obvious reason. [...]
