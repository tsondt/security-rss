Title: Zero Trust architectures: An AWS perspective
Date: 2020-11-23T17:14:44+00:00
Author: Mark Ryland
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;BeyondCorp;CARTA;Security Blog;Zero Trust;Zero Trust Network Access;ZTNA
Slug: zero-trust-architectures-an-aws-perspective

[Source](https://aws.amazon.com/blogs/security/zero-trust-architectures-an-aws-perspective/){:target="_blank" rel="noopener"}

> Our mission at Amazon Web Services (AWS) is to innovate on behalf of our customers so they have less and less work to do when building, deploying, and rapidly iterating on secure systems. From a security perspective, our customers seek answers to the ongoing question What are the optimal patterns to ensure the right level [...]
