Title: Brave browser acts quickly to resolve Tor session confidentiality bug
Date: 2020-11-05T17:13:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: brave-browser-acts-quickly-to-resolve-tor-session-confidentiality-bug

[Source](https://portswigger.net/daily-swig/brave-browser-acts-quickly-to-resolve-tor-session-confidentiality-bug){:target="_blank" rel="noopener"}

> Feature did not sufficiently anonymize private browsing sessions [...]
