Title: Source code review uncovers filtering bypass bugs in Naxsi WAF
Date: 2020-11-12T14:02:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: source-code-review-uncovers-filtering-bypass-bugs-in-naxsi-waf

[Source](https://portswigger.net/daily-swig/source-code-review-uncovers-filtering-bypass-bugs-in-naxsi-waf){:target="_blank" rel="noopener"}

> Web application firewall circumvention vulnerabilities discovered and addressed [...]
