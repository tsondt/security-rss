Title: $100M Botnet Scheme Lands Cybercriminal 8 Years in Jail
Date: 2020-11-02T20:23:58+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;Aleksandr Brovko;botnet;Dark Web;Department of Justice;financial data;hack;hacker;hacker jail time;personal identifiable information;PII;Russian cybercriminal;stolen data
Slug: 100m-botnet-scheme-lands-cybercriminal-8-years-in-jail

[Source](https://threatpost.com/100m-botnet-russian-cybercriminal-8-years-jail/160852/){:target="_blank" rel="noopener"}

> Aleksandr Brovko faces jail time after stealing $100 million worth of personal identifiable information (PII) and financial data over the course of more than 10 years. [...]
