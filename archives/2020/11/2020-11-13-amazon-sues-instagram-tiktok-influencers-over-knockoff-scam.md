Title: Amazon Sues Instagram, TikTok Influencers Over Knockoff Scam
Date: 2020-11-13T19:05:25+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Web Security;amazon;counterfeit goods;dupes;influencers;Instagram;Kelly Fitzpatrick;Lawsuit;luxury items;order this get this;Sabrina Kelly-Krejci;tiktok
Slug: amazon-sues-instagram-tiktok-influencers-over-knockoff-scam

[Source](https://threatpost.com/amazon-sues-instagram-tiktok-knockoff-scam/161233/){:target="_blank" rel="noopener"}

> 'Order This, Get This': Social-media influencers are in Amazon’s legal crosshairs for promoting generic Amazon listings with the promise to get prohibited counterfeit luxury items instead. [...]
