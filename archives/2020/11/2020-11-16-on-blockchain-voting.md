Title: On Blockchain Voting
Date: 2020-11-16T15:55:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;blockchain;cybersecurity;national security policy;voting
Slug: on-blockchain-voting

[Source](https://www.schneier.com/blog/archives/2020/11/on-blockchain-voting.html){:target="_blank" rel="noopener"}

> Blockchain voting is a spectacularly dumb idea for a whole bunch of reasons. I have generally quoted Matt Blaze : Why is blockchain voting a dumb idea? Glad you asked. For starters: It doesn’t solve any problems civil elections actually have. It’s basically incompatible with “software independence”, considered an essential property. It can make ballot secrecy difficult or impossible. I’ve also quoted this XKCD cartoon. But now I have this excellent paper from MIT researchers: “Going from Bad to Worse: From Internet Voting to Blockchain Voting” Sunoo Park, Michael Specter, Neha Narula, and Ronald L. Rivest Abstract: Voters are understandably [...]
