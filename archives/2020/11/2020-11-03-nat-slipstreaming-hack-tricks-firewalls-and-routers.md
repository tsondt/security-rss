Title: NAT Slipstreaming hack tricks firewalls and routers
Date: 2020-11-03T13:23:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: nat-slipstreaming-hack-tricks-firewalls-and-routers

[Source](https://portswigger.net/daily-swig/nat-slipstreaming-hack-tricks-firewalls-and-routers){:target="_blank" rel="noopener"}

> Port-based protection gets pwned [...]
