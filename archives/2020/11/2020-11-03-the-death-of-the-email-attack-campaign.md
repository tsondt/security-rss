Title: The death of the email attack 'campaign'
Date: 2020-11-03T19:00:13+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: the-death-of-the-email-attack-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/03/death_of_email_attack_campaign/){:target="_blank" rel="noopener"}

> So long and (no) thanks for all the phish Sponsored By the time we hit 50, most of us start slowing down. Not so for email. As the technology hits its half-century next year, it's speeding up. Malicious emails are more ubiquitous than ever, and experts are seeing a worrying trend: Phishing campaigns are becoming faster, and smarter.... [...]
