Title: Upcoming Speaking Engagements
Date: 2020-11-14T18:35:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Schneier news
Slug: upcoming-speaking-engagements-3

[Source](https://www.schneier.com/blog/archives/2020/11/upcoming-speaking-engagements-3.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at the (ISC)2 Security Congress 2020, November 16, 2020. I’ll be on a panel at the OECD Global Blockchain Policy Forum 2020 on November 17, 2020. The panel is called “Deep Dive: Digital Security and Distributed Ledger Technology: Myths and Reality.” I’m speaking on “ Securing a World of Physically Capable Computers ” as part of Cary Library’s Science & Economics Series on November 17, 2020. I’ll be keynoting the HITB CyberWeek Virtual Edition on November 18, 2020. I’m appearing on a panel called [...]
