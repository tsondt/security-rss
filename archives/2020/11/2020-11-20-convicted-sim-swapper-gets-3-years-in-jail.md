Title: Convicted SIM Swapper Gets 3 Years in Jail
Date: 2020-11-20T15:05:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;SIM Swapping
Slug: convicted-sim-swapper-gets-3-years-in-jail

[Source](https://krebsonsecurity.com/2020/11/convicted-sim-swapper-gets-3-years-in-jail/){:target="_blank" rel="noopener"}

> A 21-year-old Irishman who pleaded guilty to charges of helping to steal millions of dollars in cryptocurrencies from victims has been sentenced to just under three years in prison. The defendant is part of an alleged conspiracy involving at least eight others in the United States who stand accused of theft via SIM swapping, a crime that involves convincing mobile phone company employees to transfer ownership of the target’s phone number to a device the attackers control. Conor Freeman of Dublin took part in the theft of more than two million dollars worth of cryptocurrency from different victims throughout 2018. [...]
