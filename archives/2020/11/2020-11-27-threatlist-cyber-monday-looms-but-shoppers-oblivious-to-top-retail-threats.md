Title: ThreatList: Cyber Monday Looms – But Shoppers Oblivious to Top Retail Threats
Date: 2020-11-27T14:00:28+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware;Most Recent ThreatLists;Web Security;black friday;cisa alert;coronavirus;COVID-19;credit cart skimmer;cyber monday;Cybersecurity;digital payment skimmer;holiday shopping;magecart;malicious app;online shopping;top cyber threats
Slug: threatlist-cyber-monday-looms-but-shoppers-oblivious-to-top-retail-threats

[Source](https://threatpost.com/threatlist-cyber-monday-looms-retail-threats/161563/){:target="_blank" rel="noopener"}

> Online shoppers are blissfully unaware of credit card skimming threats and malicious shopping apps as they head into this year's Black Friday and Cyber Monday holiday shopping events. [...]
