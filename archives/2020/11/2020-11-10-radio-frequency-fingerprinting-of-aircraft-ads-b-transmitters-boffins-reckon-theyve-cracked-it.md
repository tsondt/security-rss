Title: Radio Frequency fingerprinting of aircraft ADS-B transmitters? Boffins reckon they've cracked it
Date: 2020-11-10T19:58:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: radio-frequency-fingerprinting-of-aircraft-ads-b-transmitters-boffins-reckon-theyve-cracked-it

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/10/adsb_fingerprinting_research/){:target="_blank" rel="noopener"}

> More data points needed, says academic, but technique could give governments a spoofin' bad headache A group of academics reckon they've found a way to uniquely fingerprint aeroplanes’ Automatic Dependent Surveillance-Broadcast (ADS-B) tracking transmitters – though an aviation infosec boffin says more research is needed to verify the new technique.... [...]
