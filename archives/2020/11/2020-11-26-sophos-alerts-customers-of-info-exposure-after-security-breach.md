Title: Sophos alerts customers of info exposure after security breach
Date: 2020-11-26T08:12:56
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: sophos-alerts-customers-of-info-exposure-after-security-breach

[Source](https://www.bleepingcomputer.com/news/security/sophos-alerts-customers-of-info-exposure-after-security-breach/){:target="_blank" rel="noopener"}

> British cybersecurity and hardware company Sophos has emailed a small group of customers to alert them that their personal information was exposed following a security breach discovered on Tuesday. [...]
