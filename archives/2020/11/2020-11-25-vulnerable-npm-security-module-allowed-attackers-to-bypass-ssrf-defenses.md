Title: Vulnerable NPM security module allowed attackers to bypass SSRF defenses
Date: 2020-11-25T13:10:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: vulnerable-npm-security-module-allowed-attackers-to-bypass-ssrf-defenses

[Source](https://portswigger.net/daily-swig/vulnerable-npm-security-module-allowed-attackers-to-bypass-ssrf-defenses){:target="_blank" rel="noopener"}

> Private-IP users should update to prevent their apps from spilling internal data [...]
