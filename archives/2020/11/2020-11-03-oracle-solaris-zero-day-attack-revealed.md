Title: Oracle Solaris Zero-Day Attack Revealed
Date: 2020-11-03T21:39:58+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Malware;Vulnerabilities;bluekeep;CVE-2020-14871;Exploit;Oracle;oracle solaris;SLAPSTICK;unc1945;zero day
Slug: oracle-solaris-zero-day-attack-revealed

[Source](https://threatpost.com/oracle-solaris-zero-day-attack/160929/){:target="_blank" rel="noopener"}

> A threat actor is compromising telecommunications companies and targeted financial and professional consulting industries using an Oracle flaw. [...]
