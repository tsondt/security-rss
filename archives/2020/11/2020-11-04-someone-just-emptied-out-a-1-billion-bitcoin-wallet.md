Title: Someone just emptied out a $1 billion BitCoin wallet
Date: 2020-11-04T04:18:10
Author: Ax Sharma
Category: BleepingComputer
Tags: CryptoCurrency
Slug: someone-just-emptied-out-a-1-billion-bitcoin-wallet

[Source](https://www.bleepingcomputer.com/news/security/someone-just-emptied-out-a-1-billion-bitcoin-wallet/){:target="_blank" rel="noopener"}

> A password-protected cryptocurrency wallet with almost a billion dollar-worth of BitCoins has just been emptied out. Mystery surrounds the party who finally managed to cash out the big fat amount: was it the owner themselves, or did someone crack this wallet? [...]
