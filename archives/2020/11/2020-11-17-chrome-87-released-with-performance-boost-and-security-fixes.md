Title: Chrome 87 released with performance boost and security fixes
Date: 2020-11-17T15:06:08
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Software
Slug: chrome-87-released-with-performance-boost-and-security-fixes

[Source](https://www.bleepingcomputer.com/news/google/chrome-87-released-with-performance-boost-and-security-fixes/){:target="_blank" rel="noopener"}

> Google has released Chrome 87 today, November 17th, 2020, to the Stable desktop channel, and it includes numerous performance improvements, security fixes, and new features. [...]
