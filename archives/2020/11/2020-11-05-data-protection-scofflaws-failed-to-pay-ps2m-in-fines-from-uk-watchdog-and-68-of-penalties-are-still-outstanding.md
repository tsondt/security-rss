Title: Data protection scofflaws failed to pay £2m in fines from UK watchdog – and 68% of penalties are still outstanding
Date: 2020-11-05T12:15:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: data-protection-scofflaws-failed-to-pay-ps2m-in-fines-from-uk-watchdog-and-68-of-penalties-are-still-outstanding

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/05/ico_fines_unpaid_research/){:target="_blank" rel="noopener"}

> We're trying, insists beleaguered Information Commissioner's Office Scofflaws have failed to pay nearly £2m in fines handed out by the UK Information Commissioner's Office over the past 18 months, according to new research.... [...]
