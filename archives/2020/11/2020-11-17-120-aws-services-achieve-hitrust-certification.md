Title: 120 AWS services achieve HITRUST certification
Date: 2020-11-17T23:40:33+00:00
Author: Hadis Ali
Category: AWS Security
Tags: Announcements;AWS Artifact;Foundational (100);Security, Identity, & Compliance;HITRUST CSF certification;Security Blog
Slug: 120-aws-services-achieve-hitrust-certification

[Source](https://aws.amazon.com/blogs/security/120-aws-services-achieve-hitrust-certification/){:target="_blank" rel="noopener"}

> We’re excited to announce that 120 Amazon Web Services (AWS) services are certified for the HITRUST Common Security Framework (CSF) for the 2020 cycle. The full list of AWS services that were audited by a third-party assessor and certified under HITRUST CSF is available on our Services in Scope by Compliance Program page. You can [...]
