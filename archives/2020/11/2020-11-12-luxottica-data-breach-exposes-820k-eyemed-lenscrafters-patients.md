Title: Luxottica data breach exposes 820K EyeMed, LensCrafters patients
Date: 2020-11-12T16:09:35
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: luxottica-data-breach-exposes-820k-eyemed-lenscrafters-patients

[Source](https://www.bleepingcomputer.com/news/security/luxottica-data-breach-exposes-820k-eyemed-lenscrafters-patients/){:target="_blank" rel="noopener"}

> A Luxottica data breach has exposed the personal and protected health information of 829,454 patients at LensCrafters, Target Optical, EyeMed, and other eye care practices. [...]
