Title: Sodinokibi/REvil ransomware gang pwns British housing biz via suspected phishing attack
Date: 2020-11-06T16:08:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: sodinokibirevil-ransomware-gang-pwns-british-housing-biz-via-suspected-phishing-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/06/revil_sodinokibi_ransomware_gang_flagship_group_housing/){:target="_blank" rel="noopener"}

> Same people who killed Travelex and revenge-published personal data when ignored. Nice folk A social housing provider in Norwich, England, has said it was hit with the Sodinokibi ransomware following what it assumes was a successful phishing attack.... [...]
