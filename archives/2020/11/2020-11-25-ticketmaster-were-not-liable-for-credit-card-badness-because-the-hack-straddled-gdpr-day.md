Title: Ticketmaster: We're not liable for credit card badness because the hack straddled GDPR day
Date: 2020-11-25T11:59:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ticketmaster-were-not-liable-for-credit-card-badness-because-the-hack-straddled-gdpr-day

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/25/ticketmaster_gdpr_fine_chicanery/){:target="_blank" rel="noopener"}

> Lawyers for fined firm make eyebrow-raising claim to irate ex-customer Ticketmaster is claiming that the ICO's £1.25m data breach fine clears it of any responsibility for its network being infected by card-skimming malware, according to correspondence seen by The Register.... [...]
