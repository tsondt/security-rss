Title: Millions of Hotel Guests Worldwide Caught Up in Mass Data Leak
Date: 2020-11-09T15:43:07+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Vulnerabilities;10 million records;AWS S3 Bucket;booking.com;cloud hospitality;Cloud misconfiguration;credit card fraud;data leak;Expedia;exposure;hotel records;hotel reservations;prestige;website planet
Slug: millions-of-hotel-guests-worldwide-caught-up-in-mass-data-leak

[Source](https://threatpost.com/millions-hotel-guests-worldwide-data-leak/161044/){:target="_blank" rel="noopener"}

> A cloud misconfiguration affecting users of a popular reservation platform threatens travelers with identity theft, scams, credit-card fraud and vacation-stealing. [...]
