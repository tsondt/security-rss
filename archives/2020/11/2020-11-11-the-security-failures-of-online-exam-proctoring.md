Title: The Security Failures of Online Exam Proctoring
Date: 2020-11-11T16:25:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: the-security-failures-of-online-exam-proctoring

[Source](https://www.schneier.com/blog/archives/2020/11/the-security-failures-of-online-exam-proctoring.html){:target="_blank" rel="noopener"}

> Proctoring an online exam is hard. It’s hard to be sure that the student isn’t cheating, maybe by having reference materials at hand, or maybe by substituting someone else to take the exam for them. There are a variety of companies that provide online proctoring services, but they’re uniformly mediocre : The remote proctoring industry offers a range of services, from basic video links that allow another human to observe students as they take exams to algorithmic tools that use artificial intelligence (AI) to detect cheating. But asking students to install software to monitor them during a test raises a [...]
