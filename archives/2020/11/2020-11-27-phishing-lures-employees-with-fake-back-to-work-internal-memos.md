Title: Phishing lures employees with fake 'back to work' internal memos
Date: 2020-11-27T10:15:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: phishing-lures-employees-with-fake-back-to-work-internal-memos

[Source](https://www.bleepingcomputer.com/news/security/phishing-lures-employees-with-fake-back-to-work-internal-memos/){:target="_blank" rel="noopener"}

> Scammers are trying to steal email credentials from employees by impersonating their organization's human resources (HR) department in phishing emails camouflaged as internal 'back to work' company memos. [...]
