Title: Chinese-linked Muhstik botnet targets Oracle WebLogic, Drupal
Date: 2020-11-11T10:02:37
Author: Ax Sharma
Category: BleepingComputer
Tags: Cloud
Slug: chinese-linked-muhstik-botnet-targets-oracle-weblogic-drupal

[Source](https://www.bleepingcomputer.com/news/security/chinese-linked-muhstik-botnet-targets-oracle-weblogic-drupal/){:target="_blank" rel="noopener"}

> Muhstik botnet has been targeting cloud infrastructure for years. New details have emerged related to this malware that shed light on its nefarious activities and origins. [...]
