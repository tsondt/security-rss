Title: Critical bug actively used to deploy Cobalt Strike on Oracle servers
Date: 2020-11-05T12:55:04
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: critical-bug-actively-used-to-deploy-cobalt-strike-on-oracle-servers

[Source](https://www.bleepingcomputer.com/news/security/critical-bug-actively-used-to-deploy-cobalt-strike-on-oracle-servers/){:target="_blank" rel="noopener"}

> Threat actors are actively exploiting vulnerable Oracle WebLogic servers unpatched against CVE-2020-14882 to deploy Cobalt Strike beacons to gain persistent remote access to compromised devices. [...]
