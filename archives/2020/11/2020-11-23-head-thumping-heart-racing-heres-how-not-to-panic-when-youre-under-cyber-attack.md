Title: Head thumping, heart racing? Here’s how not to panic when you’re under cyber attack
Date: 2020-11-23T06:00:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: head-thumping-heart-racing-heres-how-not-to-panic-when-youre-under-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/23/sophos_incident_response_plan/){:target="_blank" rel="noopener"}

> No incident response plan? We can help you there... Promo You know the symptoms – a sinking feeling in the pit of the stomach, tingling in the fingers, blood thumping in the temples as time slows to a crawl. Realizing you’re facing a full-on cyber attack can feel horribly visceral, even if the attackers are virtual.... [...]
