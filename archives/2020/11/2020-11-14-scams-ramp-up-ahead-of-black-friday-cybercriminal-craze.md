Title: Scams Ramp Up Ahead of Black Friday Cybercriminal Craze
Date: 2020-11-14T14:00:40+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;Amazon Prime;black friday;China;COVID-19;cyber monday;holiday scams;holiday shopping;online shopping;phishing attack;Scams;Singles Day
Slug: scams-ramp-up-ahead-of-black-friday-cybercriminal-craze

[Source](https://threatpost.com/scams-black-friday-cybercriminal-craze/161239/){:target="_blank" rel="noopener"}

> With more online shoppers this year due to COVID-19, cybercriminals are pulling the trigger on new scams ahead of Black Friday and Cyber Monday. [...]
