Title: Data breach at UK flooring firm Headlam Group after company suffers cyber-attack
Date: 2020-11-24T13:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: data-breach-at-uk-flooring-firm-headlam-group-after-company-suffers-cyber-attack

[Source](https://portswigger.net/daily-swig/data-breach-at-uk-flooring-firm-headlam-group-after-company-suffers-cyber-attack){:target="_blank" rel="noopener"}

> ‘Well-known’ unnamed organization blamed for incident [...]
