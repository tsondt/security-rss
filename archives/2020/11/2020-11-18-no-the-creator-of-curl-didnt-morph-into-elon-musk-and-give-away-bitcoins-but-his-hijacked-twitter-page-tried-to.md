Title: No, the creator of cURL didn't morph into Elon Musk and give away Bitcoins. But his hijacked Twitter page tried to
Date: 2020-11-18T06:26:08+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: no-the-creator-of-curl-didnt-morph-into-elon-musk-and-give-away-bitcoins-but-his-hijacked-twitter-page-tried-to

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/18/curlr_bitcoin_hack/){:target="_blank" rel="noopener"}

> Daniel Stenberg tells The Reg he's baffled by profile hack The creator of cURL reassured The Reg on Tuesday that he's not a billionaire rocket man giving away Bitcoins, no matter what his Twitter account claimed.... [...]
