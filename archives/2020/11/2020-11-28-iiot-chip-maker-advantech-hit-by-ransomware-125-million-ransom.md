Title: IIoT chip maker Advantech hit by ransomware, $12.5 million ransom
Date: 2020-11-28T10:00:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: iiot-chip-maker-advantech-hit-by-ransomware-125-million-ransom

[Source](https://www.bleepingcomputer.com/news/security/iiot-chip-maker-advantech-hit-by-ransomware-125-million-ransom/){:target="_blank" rel="noopener"}

> The Conti ransomware gang hit the systems of industrial automation and Industrial IoT (IIoT) chip maker Advantech and is now demanding a $14 million ransom to decrypt affected systems and to stop leaking stolen company data. [...]
