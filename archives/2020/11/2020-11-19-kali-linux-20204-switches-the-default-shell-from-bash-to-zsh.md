Title: Kali Linux 2020.4 switches the default shell from Bash to ZSH
Date: 2020-11-19T17:44:04
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Linux
Slug: kali-linux-20204-switches-the-default-shell-from-bash-to-zsh

[Source](https://www.bleepingcomputer.com/news/linux/kali-linux-20204-switches-the-default-shell-from-bash-to-zsh/){:target="_blank" rel="noopener"}

> ​Kali Linux 2020.4 was released yesterday by Offensive Security, and it takes the big step of changing the default shell from Bash to ZSH. [...]
