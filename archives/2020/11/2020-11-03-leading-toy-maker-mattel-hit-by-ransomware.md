Title: Leading toy maker Mattel hit by ransomware
Date: 2020-11-03T18:38:35
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: leading-toy-maker-mattel-hit-by-ransomware

[Source](https://www.bleepingcomputer.com/news/security/leading-toy-maker-mattel-hit-by-ransomware/){:target="_blank" rel="noopener"}

> ​Toy industry giant Mattel disclosed that they suffered a ransomware attack in July that impacted some of its business functions but did not lead to data theft. [...]
