Title: Black Friday 2020 deal: 20% off Zero2Automated reverse engineering courses
Date: 2020-11-25T08:00:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: black-friday-2020-deal-20-off-zero2automated-reverse-engineering-courses

[Source](https://www.bleepingcomputer.com/news/security/black-friday-2020-deal-20-percent-off-zero2automated-reverse-engineering-courses/){:target="_blank" rel="noopener"}

> The popular Zero2Automated malware reverse-engineering course is having Black Friday promotion where you can get 20% off all courses on their site. [...]
