Title: Facebook pays out $25k bug bounty for chained DOM-based XSS
Date: 2020-11-09T17:55:12+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: facebook-pays-out-25k-bug-bounty-for-chained-dom-based-xss

[Source](https://portswigger.net/daily-swig/facebook-pays-out-25k-bug-bounty-for-chained-dom-based-xss){:target="_blank" rel="noopener"}

> Researcher awarded five-figure sum for ‘easy to exploit’ bug [...]
