Title: Apple lets some Big Sur network traffic bypass firewalls
Date: 2020-11-17T20:48:32+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Tech;apple;firewalls;MacOS;privacy;vpns
Slug: apple-lets-some-big-sur-network-traffic-bypass-firewalls

[Source](https://arstechnica.com/?p=1723835){:target="_blank" rel="noopener"}

> Enlarge (credit: Patrick Wardle) Firewalls aren’t just for corporate networks. Large numbers of security- or privacy-conscious people also use them to filter or redirect traffic flowing in and out of their computers. Apple recently made a major change to macOS that frustrates these efforts. Beginning with macOS Catalina released last year, Apple added a list of 50 Apple-specific apps and processes that were to be exempted from firewalls like Little Snitch and Lulu. The undocumented exemption, which didn't take effect until firewalls were rewritten to implement changes in Big Sur, first came to light in October. Patrick Wardle, a security [...]
