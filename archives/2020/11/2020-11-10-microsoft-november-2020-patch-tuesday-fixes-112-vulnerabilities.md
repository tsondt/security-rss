Title: Microsoft November 2020 Patch Tuesday fixes 112 vulnerabilities
Date: 2020-11-10T13:35:03
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-november-2020-patch-tuesday-fixes-112-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/microsoft-november-2020-patch-tuesday-fixes-112-vulnerabilities/){:target="_blank" rel="noopener"}

> Today is Microsoft's November 2020 Patch Tuesday, and Microsoft has patched 112 security vulnerabilities, including one zero-day disclosed by Google Project Zero last week. [...]
