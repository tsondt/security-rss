Title: Police to Livestream Ring Camera Footage of Mississippi Residents
Date: 2020-11-04T13:42:50+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Cloud Security;Privacy;Web Security;ACLU;amazon;consumer;home security;law enforcement;Mississippi;police surveillance;Ring;Smart Home
Slug: police-to-livestream-ring-camera-footage-of-mississippi-residents

[Source](https://threatpost.com/police-livestream-ring-camera-mississippi/160936/){:target="_blank" rel="noopener"}

> A Mississippi pilot program that allows police to livestream private camera footage sparks privacy fears from the ACLU. [...]
