Title: Google Chrome 87 Closes High-Severity ‘NAT Slipstreaming’ Hole
Date: 2020-11-18T17:37:45+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;chrome 87;Chrome 87.0.4280.66;CVE-2020-16022;google;Google Chrome;high severity flaw;Linux;Mac;NAT device;NAT Streamslipping;TCP;UDP;Windows
Slug: google-chrome-87-closes-high-severity-nat-slipstreaming-hole

[Source](https://threatpost.com/google-chrome-87-nat-slipstreaming-flaw/161344/){:target="_blank" rel="noopener"}

> Overall Google's Chrome 87 release fixed 33 security vulnerabilities. [...]
