Title: Cybercrime isn’t the exciting career it’s cracked up to be, say academics
Date: 2020-11-06T11:45:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: cybercrime-isnt-the-exciting-career-its-cracked-up-to-be-say-academics

[Source](https://portswigger.net/daily-swig/cybercrime-isnt-the-exciting-career-its-cracked-up-to-be-say-academics){:target="_blank" rel="noopener"}

> Life on the wild side is often tedious, repetitive, and not altogether lucrative [...]
