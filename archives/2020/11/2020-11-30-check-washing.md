Title: Check Washing
Date: 2020-11-30T15:22:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: banking;fraud
Slug: check-washing

[Source](https://www.schneier.com/blog/archives/2020/11/check-washing.html){:target="_blank" rel="noopener"}

> I can’t believe that check washing is still a thing: “Check washing” is a practice where thieves break into mailboxes (or otherwise steal mail), find envelopes with checks, then use special solvents to remove the information on that check (except for the signature) and then change the payee and the amount to a bank account under their control so that it could be deposited at out-state-banks and oftentimes by a mobile phone. The article suggests a solution: stop using paper checks. [...]
