Title: WordPress plugin bugs can let attackers hijack up to 100K sites
Date: 2020-11-09T18:29:23
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: wordpress-plugin-bugs-can-let-attackers-hijack-up-to-100k-sites

[Source](https://www.bleepingcomputer.com/news/security/wordpress-plugin-bugs-can-let-attackers-hijack-up-to-100k-sites/){:target="_blank" rel="noopener"}

> Admins of WordPress sites who use the Ultimate Member plugin are urged to update it to the latest version to block attacks attempting to exploit multiple critical and easy to exploit vulnerabilities that could lead to site takeovers. [...]
