Title: EncroChat hack evidence wasn't obtained illegally, High Court of England and Wales rules – trial judges will decide whether to admit it
Date: 2020-11-13T17:17:27+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: encrochat-hack-evidence-wasnt-obtained-illegally-high-court-of-england-and-wales-rules-trial-judges-will-decide-whether-to-admit-it

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/13/encrochat_hack_judicial_review_judgment/){:target="_blank" rel="noopener"}

> Blow to UK suspects in wake of Franco-Dutch investigation The contents of messages from encrypted chat service EncroChat may be admissible as evidence in English criminal trials, the High Court in London, England has ruled.... [...]
