Title: Hackers work 24 hours a day – when will you schedule your security training?
Date: 2020-11-08T22:00:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: hackers-work-24-hours-a-day-when-will-you-schedule-your-security-training

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/08/sans_training_in_se_asia/){:target="_blank" rel="noopener"}

> It’s SANS training time in Tokyo, Singapore, and India Promo We might all be living under various degrees of lockdown, but that doesn’t mean you can’t sharpen up your security skills with some of the best instructors around – and all at early bird prices, if you’re quick.... [...]
