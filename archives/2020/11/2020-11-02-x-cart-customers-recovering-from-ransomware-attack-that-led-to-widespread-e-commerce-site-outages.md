Title: X-Cart customers recovering from ransomware attack that led to widespread e-commerce site outages
Date: 2020-11-02T15:10:10+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: x-cart-customers-recovering-from-ransomware-attack-that-led-to-widespread-e-commerce-site-outages

[Source](https://portswigger.net/daily-swig/x-cart-customers-recovering-from-ransomware-attack-that-led-to-widespread-e-commerce-site-outages){:target="_blank" rel="noopener"}

> Missing order information and setting changes are apparently hampering recovery efforts [...]
