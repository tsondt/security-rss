Title: Digging into the Dark Web: How Security Researchers Learn to Think Like the Bad Guys
Date: 2020-11-12T18:12:44+00:00
Author: Aamir Lakhani
Category: Threatpost
Tags: Breach;Cloud Security;Critical Infrastructure;Hacks;InfoSec Insider;Malware;Vulnerabilities;Web Security;Aamir Lakhani;cybercriminals;Dark Web;darknet;Fortinet;hacker forum;Hackers;infosec insider;malware;Security Researchers;threat research;underground forum
Slug: digging-into-the-dark-web-how-security-researchers-learn-to-think-like-the-bad-guys

[Source](https://threatpost.com/dark-web-security-researchers-bad-guys/161172/){:target="_blank" rel="noopener"}

> Hacker forums are a rich source of threat intelligence. [...]
