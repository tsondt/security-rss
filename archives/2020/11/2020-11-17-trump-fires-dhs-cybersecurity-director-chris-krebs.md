Title: Trump fires DHS cybersecurity director Chris Krebs
Date: 2020-11-17T22:51:39
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Government
Slug: trump-fires-dhs-cybersecurity-director-chris-krebs

[Source](https://www.bleepingcomputer.com/news/security/trump-fires-dhs-cybersecurity-director-chris-krebs/){:target="_blank" rel="noopener"}

> President Trump has fired Chris Krebs, Director of the Cybersecurity and Infrastructure Security Agency (CISA), after Krebs disputed claims that the U.S. 2020 Presidential Election was insecure and fraudulent. [...]
