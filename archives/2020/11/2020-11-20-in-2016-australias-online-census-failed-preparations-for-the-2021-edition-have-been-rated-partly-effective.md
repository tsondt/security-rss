Title: In 2016 Australia's online census failed. Preparations for the 2021 edition have been rated 'partly effective'
Date: 2020-11-20T02:58:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: in-2016-australias-online-census-failed-preparations-for-the-2021-edition-have-been-rated-partly-effective

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/20/australia_census_2021_audit/){:target="_blank" rel="noopener"}

> Devs can make unauthorised changes, data integrity is a work in progress, security is not there yet... and there's just nine months to go In 2016 Australia's online census crashed and burned after legitimate attempts to complete the survey were mistaken for a DDoS attack, the routers funnelling traffic failed, and disaster recovery plans did likewise.... [...]
