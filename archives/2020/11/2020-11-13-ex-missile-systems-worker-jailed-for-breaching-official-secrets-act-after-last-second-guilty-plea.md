Title: Ex-missile systems worker jailed for breaching Official Secrets Act after last-second guilty plea
Date: 2020-11-13T20:05:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ex-missile-systems-worker-jailed-for-breaching-official-secrets-act-after-last-second-guilty-plea

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/13/simon_finch_missile_secrets_leaker_jailed/){:target="_blank" rel="noopener"}

> Also copped to RIPA breach after ignoring police demand to hand over passwords The former BAE Systems worker accused of sending details of a UK missile system to hostile foreign powers and of ignoring police demands to hand over his device passwords, has been jailed.... [...]
