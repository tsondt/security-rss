Title: Trump fires cybersecurity boss Chris Krebs for doing his job: Securing the election and telling the truth about it
Date: 2020-11-18T01:52:44+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: trump-fires-cybersecurity-boss-chris-krebs-for-doing-his-job-securing-the-election-and-telling-the-truth-about-it

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/18/trump_fires_krebs/){:target="_blank" rel="noopener"}

> Terminated by presidential tweet that piled on the baseless election-rigging allegations CISA director sought to counter President Donald Trump tonight fired the boss of the US government's Cybersecurity and Infrastructure Security Agency ( CISA ), the very organisation his administration formed with the aim of shoring up America's computer networks from hackers.... [...]
