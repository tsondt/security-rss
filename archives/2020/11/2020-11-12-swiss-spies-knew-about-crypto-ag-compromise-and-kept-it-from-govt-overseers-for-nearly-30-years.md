Title: Swiss spies knew about Crypto AG compromise – and kept it from govt overseers for nearly 30 years
Date: 2020-11-12T13:17:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: swiss-spies-knew-about-crypto-ag-compromise-and-kept-it-from-govt-overseers-for-nearly-30-years

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/12/crypto_ag_swiss_parliament_report/){:target="_blank" rel="noopener"}

> Wider government only told of encryption machine nobbling a year after it ended Swiss politicians only found out last year that cipher machine company Crypto AG was (quite literally) owned by the US and Germany during the Cold War, a striking report from its parliament has revealed.... [...]
