Title: Gold seller JM Bullion hacked to steal customers' credit cards
Date: 2020-11-01T12:53:10
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: gold-seller-jm-bullion-hacked-to-steal-customers-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/gold-seller-jm-bullion-hacked-to-steal-customers-credit-cards/){:target="_blank" rel="noopener"}

> Precious metal online retailer JM Bullion has disclosed a data breach after their site was hacked to include malicious scripts that stole customers' credit card information. [...]
