Title: End to end encryption? In Android's default messaging app? Don't worry, nobody else noticed either
Date: 2020-11-20T18:56:12+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: end-to-end-encryption-in-androids-default-messaging-app-dont-worry-nobody-else-noticed-either

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/20/google_rcs_e2e_brouhaha/){:target="_blank" rel="noopener"}

> RCS throws the dice Analysis Google is rolling out end-to-end encryption in the unloved and unwanted Android Rich Communication Services, as part of a renewed hope people might use messaging services controlled by the Chocolate Factory.... [...]
