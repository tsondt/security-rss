Title: Snap-crappy: 183 Brit local authorities operate 80,000 CCTV cams between them, says surveillance watchdog
Date: 2020-11-06T11:01:11+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: snap-crappy-183-brit-local-authorities-operate-80000-cctv-cams-between-them-says-surveillance-watchdog

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/06/surveillance_camera_commissioner_80000_half_uk_councils/){:target="_blank" rel="noopener"}

> Please make sure you're obeying the law, outgoing commissioner pleads "There are over 6,000 systems and 80,000 cameras in operation across 183 LAs!" So exclaimed the UK's outgoing Surveillance Camera Commissioner as he detailed just how many council CCTV cameras there are across the nation.... [...]
