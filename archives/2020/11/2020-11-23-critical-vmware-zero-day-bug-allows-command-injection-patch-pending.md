Title: Critical VMware Zero-Day Bug Allows Command Injection; Patch Pending
Date: 2020-11-23T21:46:22+00:00
Author: Tom Spring
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;0-day;Command injection;CVE-2020-4006;privilege escalation;security vulnerability;VMware Identity Manager;VMware Workspace One Access;vmware zero-day;zero-day
Slug: critical-vmware-zero-day-bug-allows-command-injection-patch-pending

[Source](https://threatpost.com/vmware-zero-day-patch-pending/161523/){:target="_blank" rel="noopener"}

> VMware explained it has no patch for a critical escalation-of-privileges bug that impacts both Windows and Linux operating systems and its Workspace One. [...]
