Title: Rampant CNAME misconfiguration leaves thousands of organizations open to subdomain takeover attacks –&nbsp;research
Date: 2020-11-25T14:46:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: rampant-cname-misconfiguration-leaves-thousands-of-organizations-open-to-subdomain-takeover-attacks-research

[Source](https://portswigger.net/daily-swig/rampant-cname-misconfiguration-leaves-thousands-of-organizations-open-to-subdomain-takeover-attacks-nbsp-research){:target="_blank" rel="noopener"}

> Security researchers discover more than 400,00 at-risk subdomains during an automated internet trawl [...]
