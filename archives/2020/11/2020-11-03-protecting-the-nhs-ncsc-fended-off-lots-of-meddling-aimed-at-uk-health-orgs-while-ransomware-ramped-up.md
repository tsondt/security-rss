Title: Protecting the NHS: NCSC fended off lots of meddling aimed at UK health orgs while ransomware ramped up
Date: 2020-11-03T12:28:03+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: protecting-the-nhs-ncsc-fended-off-lots-of-meddling-aimed-at-uk-health-orgs-while-ransomware-ramped-up

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/03/ncsc_annual_report_nhs_ransomware/){:target="_blank" rel="noopener"}

> But annual report doesn't mention China once The National Cyber Security Centre fended off more than 700 cyber attacks directed against the British state over the last year, of which about a quarter were COVID-19 related.... [...]
