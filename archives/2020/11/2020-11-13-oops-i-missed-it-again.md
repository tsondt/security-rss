Title: Oops, I missed it again!
Date: 2020-11-13T10:01:00-08:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: oops-i-missed-it-again

[Source](https://googleprojectzero.blogspot.com/2020/11/oops-i-missed-it-again.html){:target="_blank" rel="noopener"}

> Written by Brandon Azad, when working at Project Zero This is a quick anecdotal post describing one of the more frustrating aspects of vulnerability research: realizing that you missed a bug that was staring you in the face only once you see the patched version! Some suspicious code After writing the oob_timestamp exploit, I spent some time trying to find another vulnerability to exploit. Typically, it's a lot easier to develop an exploit when you already have a research platform (read: another exploit) available to help with your analysis, for example by dumping kernel memory to ensure that your heap [...]
