Title: Scammer sentenced for stealing $9M from adoption, automotive firms
Date: 2020-11-19T13:54:14
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: scammer-sentenced-for-stealing-9m-from-adoption-automotive-firms

[Source](https://www.bleepingcomputer.com/news/security/scammer-sentenced-for-stealing-9m-from-adoption-automotive-firms/){:target="_blank" rel="noopener"}

> A Florida man was sentenced to 37 months in prison earlier this week for his involvement in a business account takeover scheme that resulted in more than $9 million in total financial losses. [...]
