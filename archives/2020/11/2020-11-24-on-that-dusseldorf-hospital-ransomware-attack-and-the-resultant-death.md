Title: On That Dusseldorf Hospital Ransomware Attack and the Resultant Death
Date: 2020-11-24T12:01:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cyberattack;cybercrime;Germany;medicine;ransomware
Slug: on-that-dusseldorf-hospital-ransomware-attack-and-the-resultant-death

[Source](https://www.schneier.com/blog/archives/2020/11/on-that-dusseldorf-hospital-ransomware-attack-and-the-resultant-death.html){:target="_blank" rel="noopener"}

> Wired has a detailed story about the ransomware attack on a Dusseldorf hospital, the one that resulted in an ambulance being redirected to a more distant hospital and the patient dying. The police wanted to prosecute the ransomware attackers for negligent homicide, but the details were more complicated: After a detailed investigation involving consultations with medical professionals, an autopsy, and a minute-by-minute breakdown of events, Hartmann believes that the severity of the victim’s medical diagnosis at the time she was picked up was such that she would have died regardless of which hospital she had been admitted to. “The delay [...]
