Title: DNS cache poisoning attacks return due to Linux weakness
Date: 2020-11-12T15:55:04
Author: Ax Sharma
Category: BleepingComputer
Tags: Linux
Slug: dns-cache-poisoning-attacks-return-due-to-linux-weakness

[Source](https://www.bleepingcomputer.com/news/security/dns-cache-poisoning-attacks-return-due-to-linux-weakness/){:target="_blank" rel="noopener"}

> Researchers from Tsinghua University and the University of California have identified a new method that can be used to conduct DNS cache poisoning attacks. The findings reopen a vulnerability that had been discovered by Kaminsky in 2008 and thought to have been resolved. [...]
