Title: Credential-Stuffing Attack Hits The North Face
Date: 2020-11-13T16:07:30+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security;credential stuffing attack;Customer Data;cybercriminals;data breach;hack;Password;stolen password;The North Face;thenorthface.com
Slug: credential-stuffing-attack-hits-the-north-face

[Source](https://threatpost.com/credential-stuffing-attack-north-face/161190/){:target="_blank" rel="noopener"}

> The North Face has reset an undisclosed number of customer accounts after detecting a credential-stuffing attack on its website. [...]
