Title: APT Groups Finding Success with Mix of Old and New Tools
Date: 2020-11-03T19:18:44+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Most Recent ThreatLists;Vulnerabilities;Web Security;advanced persistent threats;apt;APT groups;apt report;DeathStalker;Kaspersky;malware;mosaicregressor;new tools;q3 2020;ttps;UEFI firmware
Slug: apt-groups-finding-success-with-mix-of-old-and-new-tools

[Source](https://threatpost.com/apt-groups-success-mix-tools/160927/){:target="_blank" rel="noopener"}

> The APT threat landscape is a mixed bag of tried-and-true tactics and cutting-edge techniques, largely supercharged by geo-politics, a report finds. [...]
