Title: Gitpaste-12 Worm Targets Linux Servers, IoT Devices
Date: 2020-11-06T17:34:00+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security;Apache Struts;botnet;Cryptocurrency Mining;CVE-2017-14135;CVE-2017-5638;CVE-2020-10987;github;Gitpaste-12;Linux;malware;Pastebin;Worm
Slug: gitpaste-12-worm-targets-linux-servers-iot-devices

[Source](https://threatpost.com/gitpaste-12-worm-linux-servers-iot-devices/161016/){:target="_blank" rel="noopener"}

> The newly discovered malware uses GitHub and Pastebin to house component code, and harbors 12 different initial attack vectors. [...]
