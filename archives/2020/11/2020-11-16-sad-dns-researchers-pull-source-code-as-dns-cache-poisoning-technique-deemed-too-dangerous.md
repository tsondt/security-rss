Title: SAD DNS: Researchers pull source code as DNS cache poisoning technique deemed ‘too dangerous’
Date: 2020-11-16T11:52:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: sad-dns-researchers-pull-source-code-as-dns-cache-poisoning-technique-deemed-too-dangerous

[Source](https://portswigger.net/daily-swig/sad-dns-researchers-pull-source-code-as-dns-cache-poisoning-technique-deemed-too-dangerous){:target="_blank" rel="noopener"}

> ‘We removed the code from GitHub to protect those vulnerable servers and give them time to fix the vulnerability’ [...]
