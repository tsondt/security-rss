Title: Silver Peak SD-WAN Bugs Allow for Network Takeover
Date: 2020-11-11T21:04:06+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Vulnerabilities;CVE-2020–12145;CVE-2020–12146;CVE-2020–12147;RCE;remote code execution;sd-wan;Security Vulnerabilities;silver peak;unauthenticated attackers;unity orchestrator
Slug: silver-peak-sd-wan-bugs-allow-for-network-takeover

[Source](https://threatpost.com/silver-peak-sd-wan-bugs-network-takeover/161142/){:target="_blank" rel="noopener"}

> Three security vulnerabilities can be chained to enable unauthenticated remote code execution. [...]
