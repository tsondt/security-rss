Title: FBI warns of recently registered domains spoofing its sites
Date: 2020-11-23T11:40:03
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: fbi-warns-of-recently-registered-domains-spoofing-its-sites

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-recently-registered-domains-spoofing-its-sites/){:target="_blank" rel="noopener"}

> The U.S. Federal Bureau of Investigation (FBI) is warning the general public of the risks behind recently registered FBI-related domains that spoof some of the federal law enforcement agency's official websites. [...]
