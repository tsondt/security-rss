Title: Spotify Users Hit with Rash of Account Takeovers
Date: 2020-11-23T18:50:40+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Hacks;Web Security;account takeover;Credential stuffing;cyberattack;elasticsearch database;music streaming;password reuse;Spotify;spotify credentials;vpnMentor
Slug: spotify-users-hit-with-rash-of-account-takeovers

[Source](https://threatpost.com/spotify-account-takeovers/161495/){:target="_blank" rel="noopener"}

> Users of the music streaming service were targeted by attackers using credential-stuffing approaches. [...]
