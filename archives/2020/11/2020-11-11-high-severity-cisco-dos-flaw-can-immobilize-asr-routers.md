Title: High-Severity Cisco DoS Flaw Can Immobilize ASR Routers
Date: 2020-11-11T14:45:50+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;Cisco;Cisco ASR 9000 series;cisco IOS;Cisco IOS XR;CVE-2020-26070;Denial of Service;DoS;high severity flaw;patch
Slug: high-severity-cisco-dos-flaw-can-immobilize-asr-routers

[Source](https://threatpost.com/high-severity-cisco-dos-flaw-asr-routers/161115/){:target="_blank" rel="noopener"}

> The flaw stems from an issue with the ingress packet processing function of Cisco IOS XR software. [...]
