Title: Combining encryption and signing with AWS KMS asymmetric keys
Date: 2020-11-12T17:32:17+00:00
Author: J.D. Bean
Category: AWS Security
Tags: Advanced (300);AWS Key Management Service;Security, Identity, & Compliance;Asymmetric Cryptography;AWS KMS;digital signature;Encryption;Security Blog
Slug: combining-encryption-and-signing-with-aws-kms-asymmetric-keys

[Source](https://aws.amazon.com/blogs/security/combining-encryption-and-signing-with-aws-asymmetric-keys/){:target="_blank" rel="noopener"}

> In this post, I discuss how to use AWS Key Management Service (KMS) to combine asymmetric digital signature and asymmetric encryption of the same data. The addition of support for asymmetric keys in AWS KMS has exciting use cases for customers. The ability to create, manage, and use public and private key pairs with KMS [...]
