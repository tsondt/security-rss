Title: Nation-State Attackers Actively Target COVID-19 Vaccine-Makers
Date: 2020-11-13T18:11:09+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Critical Infrastructure;Government;Hacks;Web Security;advanced persistent threats;APT28;APTs;cerium;covid-19 research;Cyberattacks;Fancy Bear;hack;Lazarus Group;Microsoft;nation state;North Korea;pharmaceutical companies;research theft;russia;Sofacy;State sponsored;Strontium;vaccine research;vaccine-maker;zinc
Slug: nation-state-attackers-actively-target-covid-19-vaccine-makers

[Source](https://threatpost.com/russia-north-korea-attacking-covid-19-vaccine-makers/161205/){:target="_blank" rel="noopener"}

> Three major APTs are involved in ongoing compromises at pharma and clinical organizations involved in COVID-19 research, Microsoft says. [...]
