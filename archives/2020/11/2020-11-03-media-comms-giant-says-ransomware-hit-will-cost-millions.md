Title: Media Comms Giant Says Ransomware Hit Will Cost Millions
Date: 2020-11-03T15:50:46+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;$7 million;bottom line;cyberattack;cyberattack cost;fiscal 2021;isentia;lost revenue;mediaportal;ransomware;remediation costs
Slug: media-comms-giant-says-ransomware-hit-will-cost-millions

[Source](https://threatpost.com/media-comms-giant-ransomware-cost-millions/160904/){:target="_blank" rel="noopener"}

> Aussie firm Isentia said "remediation and foregone revenue" could total $8.5 million AUS or more. [...]
