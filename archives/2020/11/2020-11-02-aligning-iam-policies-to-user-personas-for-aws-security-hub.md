Title: Aligning IAM policies to user personas for AWS Security Hub
Date: 2020-11-02T20:05:23+00:00
Author: Vaibhawa Kumar
Category: AWS Security
Tags: Advanced (300);AWS Identity and Access Management (IAM);AWS Security Hub;Security, Identity, & Compliance;Access Control;Identity;Security Blog
Slug: aligning-iam-policies-to-user-personas-for-aws-security-hub

[Source](https://aws.amazon.com/blogs/security/aligning-iam-policies-to-user-personas-for-aws-security-hub/){:target="_blank" rel="noopener"}

> AWS Security Hub provides you with a comprehensive view of your security posture across your accounts in Amazon Web Services (AWS) and gives you the ability to take action on your high-priority security alerts. There are several different user personas that use Security Hub, and they typically require different AWS Identity and Access Management (IAM) [...]
