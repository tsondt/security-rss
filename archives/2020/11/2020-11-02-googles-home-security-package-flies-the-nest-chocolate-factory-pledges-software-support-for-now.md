Title: Google's home security package flies the Nest, Chocolate Factory pledges software support – for now
Date: 2020-11-02T13:15:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: googles-home-security-package-flies-the-nest-chocolate-factory-pledges-software-support-for-now

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/02/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: Immigration lawyers for Mountain View breached, SonarQube hack worse than thought, and more In brief Bad news for those who have bought into the Nest Secure home surveillance system – Google has surprised many by halting further deployments.... [...]
