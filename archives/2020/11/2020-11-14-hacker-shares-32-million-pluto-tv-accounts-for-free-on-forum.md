Title: Hacker shares 3.2 million Pluto TV accounts for free on forum
Date: 2020-11-14T10:33:44
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: hacker-shares-32-million-pluto-tv-accounts-for-free-on-forum

[Source](https://www.bleepingcomputer.com/news/security/hacker-shares-32-million-pluto-tv-accounts-for-free-on-forum/){:target="_blank" rel="noopener"}

> A hacker is sharing what they state are 3.2 million Pluto TV user records that were stolen during a data breach. [...]
