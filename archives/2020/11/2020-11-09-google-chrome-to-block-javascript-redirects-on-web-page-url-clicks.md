Title: Google Chrome to block JavaScript redirects on web page URL clicks
Date: 2020-11-09T14:37:24
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Software
Slug: google-chrome-to-block-javascript-redirects-on-web-page-url-clicks

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-to-block-javascript-redirects-on-web-page-url-clicks/){:target="_blank" rel="noopener"}

> Google Chrome is getting a new feature that increases security when clicking on web page links that open URLs in a new window or tab. [...]
