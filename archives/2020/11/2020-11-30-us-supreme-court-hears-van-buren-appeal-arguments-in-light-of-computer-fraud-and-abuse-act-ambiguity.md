Title: US Supreme Court hears Van Buren appeal arguments in light of Computer Fraud and Abuse Act ambiguity
Date: 2020-11-30T16:53:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-supreme-court-hears-van-buren-appeal-arguments-in-light-of-computer-fraud-and-abuse-act-ambiguity

[Source](https://portswigger.net/daily-swig/us-supreme-court-hears-van-buren-appeal-arguments-in-light-of-computer-fraud-and-abuse-act-ambiguity){:target="_blank" rel="noopener"}

> Ruling over interpretation of aging law could have a chilling or liberating effect on security research [...]
