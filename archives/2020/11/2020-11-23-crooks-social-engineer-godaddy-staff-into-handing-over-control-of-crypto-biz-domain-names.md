Title: Crooks social-engineer GoDaddy staff into handing over control of crypto-biz domain names
Date: 2020-11-23T22:52:12+00:00
Author: Team Register
Category: The Register
Tags: 
Slug: crooks-social-engineer-godaddy-staff-into-handing-over-control-of-crypto-biz-domain-names

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/23/godaddy_dns_hijack/){:target="_blank" rel="noopener"}

> Web traffic, email redirected, personal info exposed in DNS hijacking Miscreants were able to hijack traffic and email destined for various cryptocurrency-related websites this month – by hoodwinking GoDaddy employees.... [...]
