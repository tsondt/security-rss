Title: You can protect the company from hackers, but can you protect the company from the CEO?
Date: 2020-11-20T07:00:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: you-can-protect-the-company-from-hackers-but-can-you-protect-the-company-from-the-ceo

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/20/stop_phishers_getting_in/){:target="_blank" rel="noopener"}

> Spear-phishers love the executive suite. Here’s how to stop them getting in Webcast If you’ve ever wondered what the CEO does all day, you’re not the only one. Cyber-attackers give the matter a lot of thought, which is why a well-crafted spear-phishing attack can be so hard to defend against.... [...]
