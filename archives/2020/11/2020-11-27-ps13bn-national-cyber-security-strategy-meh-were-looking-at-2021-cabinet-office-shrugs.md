Title: £1.3bn National Cyber Security Strategy? Meh – we're looking at 2021, Cabinet Office shrugs
Date: 2020-11-27T10:04:44+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ps13bn-national-cyber-security-strategy-meh-were-looking-at-2021-cabinet-office-shrugs

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/27/national_cyber_security_progress_report/){:target="_blank" rel="noopener"}

> 'Progress' report shows nobody's really paying attention any more How is Britain's £1.3bn National Cyber Security Strategy going? Nobody really cares any more – even the Cabinet Office, judging by its latest progress report.... [...]
