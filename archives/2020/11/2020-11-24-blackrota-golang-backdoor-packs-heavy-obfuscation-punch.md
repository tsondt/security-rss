Title: Blackrota Golang Backdoor Packs Heavy Obfuscation Punch
Date: 2020-11-24T15:57:40+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware;backdoor;Blackrota;docker flaw;Docker Remote API;EKANS ransomware;ELF;go language;gobfuscate;golang;honeypot;malware;obfuscation;reverse analysis;security vulnerability;Snake;unauthorized access
Slug: blackrota-golang-backdoor-packs-heavy-obfuscation-punch

[Source](https://threatpost.com/blackrota-golang-backdoor-obfuscation/161544/){:target="_blank" rel="noopener"}

> Blackrota is targeting a security bug in Docker, but is nearly impossible to reverse-analyze. [...]
