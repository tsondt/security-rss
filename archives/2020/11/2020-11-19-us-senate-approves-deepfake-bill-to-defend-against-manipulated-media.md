Title: US Senate approves deepfake bill to defend against manipulated media
Date: 2020-11-19T20:35:53+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: us-senate-approves-deepfake-bill-to-defend-against-manipulated-media

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/19/us_senate_deepfake/){:target="_blank" rel="noopener"}

> Proposed legislation calls for research to detect synthetic shams online On Wednesday, proposed US legislation to fund defenses against realistic computer-generated media known as deepfakes was approved by the US Senate and the bill now awaits consideration in the US House of Representatives.... [...]
