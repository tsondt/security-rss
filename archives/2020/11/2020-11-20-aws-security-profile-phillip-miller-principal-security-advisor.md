Title: AWS Security Profile: Phillip Miller, Principal Security Advisor
Date: 2020-11-20T21:08:17+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;AWS Security Profile;re:Invent 2020;Security Blog
Slug: aws-security-profile-phillip-miller-principal-security-advisor

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-phillip-miller-principal-security-advisor/){:target="_blank" rel="noopener"}

> In the weeks leading up to re:Invent, we’ll share conversations we’ve had with people at AWS who will be presenting, and get a sneak peek at their work. How long have you been at AWS and what do you do in your current role? I’ve been at AWS since September 2019. I help executives and [...]
