Title: Microsoft engineer stole $10M, used colleagues as scapegoats
Date: 2020-11-10T12:56:05
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: CryptoCurrency;Microsoft
Slug: microsoft-engineer-stole-10m-used-colleagues-as-scapegoats

[Source](https://www.bleepingcomputer.com/news/security/microsoft-engineer-stole-10m-used-colleagues-as-scapegoats/){:target="_blank" rel="noopener"}

> Volodymyr Kvashuk, a Ukrainian citizen and former Microsoft software engineer, was sentenced to nine years in prison for stealing over $10 million worth of currency stored value (CSV) including gift cards over a span of two years. [...]
