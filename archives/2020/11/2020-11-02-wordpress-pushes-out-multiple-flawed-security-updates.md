Title: WordPress Pushes Out Multiple Flawed Security Updates
Date: 2020-11-02T17:41:49+00:00
Author: Tom Spring
Category: Threatpost
Tags: Vulnerabilities;Web Security;5.2.x;5.3.x;5.4.x;5.5.x;MySQL;wordpress;WordPress 5.5.2;WordPress 5.5.3;WordPress AutoUpdate
Slug: wordpress-pushes-out-multiple-flawed-security-updates

[Source](https://threatpost.com/wordpress-flawed-security-updates/160849/){:target="_blank" rel="noopener"}

> WordPress bungles critical security 5.5.2 fix and saves face next day with 5.5.3 update. [...]
