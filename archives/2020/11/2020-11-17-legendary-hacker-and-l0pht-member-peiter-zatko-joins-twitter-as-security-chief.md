Title: Legendary hacker and L0pht member Peiter Zatko joins Twitter as security chief
Date: 2020-11-17T14:03:13+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: legendary-hacker-and-l0pht-member-peiter-zatko-joins-twitter-as-security-chief

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/17/peiter_zatko_twitter_security_chief/){:target="_blank" rel="noopener"}

> Mudge work to be done after high-profile Bitcoin scam earlier this year Twitter has hired legendary hacker Peiter "Mudge" Zatko as head of security.... [...]
