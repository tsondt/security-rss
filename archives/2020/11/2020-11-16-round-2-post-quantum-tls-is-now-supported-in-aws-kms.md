Title: Round 2 post-quantum TLS is now supported in AWS KMS
Date: 2020-11-16T20:25:58+00:00
Author: Alex Weibel
Category: AWS Security
Tags: Advanced (300);AWS Key Management Service;Security, Identity, & Compliance;AWS KMS;crypto;cryptography;Encryption;key exchange;NIST;post quantum;s2n;Security Blog;SSL;TLS
Slug: round-2-post-quantum-tls-is-now-supported-in-aws-kms

[Source](https://aws.amazon.com/blogs/security/round-2-post-quantum-tls-is-now-supported-in-aws-kms/){:target="_blank" rel="noopener"}

> AWS Key Management Service (AWS KMS) now supports three new hybrid post-quantum key exchange algorithms for the Transport Layer Security (TLS) 1.2 encryption protocol that’s used when connecting to AWS KMS API endpoints. These new hybrid post-quantum algorithms combine the proven security of a classical key exchange with the potential quantum-safe properties of new post-quantum [...]
