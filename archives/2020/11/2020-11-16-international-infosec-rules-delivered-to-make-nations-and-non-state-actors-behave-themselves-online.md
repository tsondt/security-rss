Title: International infosec rules delivered to make nations and non-state actors behave themselves online
Date: 2020-11-16T07:42:22+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: international-infosec-rules-delivered-to-make-nations-and-non-state-actors-behave-themselves-online

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/16/gcsc_final_report/){:target="_blank" rel="noopener"}

> Don't hack, don't backdoor, don't hurt the internet... and don't expect rapid adoption because there's still a lot of multilateral work to be done The Global Commission on the Stability of Cyberspace (GCSC), a group that works to develop policy the world can follow to keep the internet stable and secure, late last week delivered a final report that outlines its vision for how the nations of the world should behave online.... [...]
