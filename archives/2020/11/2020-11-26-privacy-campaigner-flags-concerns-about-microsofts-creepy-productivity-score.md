Title: Privacy campaigner flags concerns about Microsoft's creepy Productivity Score
Date: 2020-11-26T10:53:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: privacy-campaigner-flags-concerns-about-microsofts-creepy-productivity-score

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/26/productivity_score/){:target="_blank" rel="noopener"}

> Watching, always watching Microsoft's Productivity Score has put in a public appearance in Microsoft 365 and attracted the ire of privacy campaigners and activists.... [...]
