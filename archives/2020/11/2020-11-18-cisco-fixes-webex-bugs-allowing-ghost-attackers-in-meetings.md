Title: Cisco fixes WebEx bugs allowing 'ghost' attackers in meetings
Date: 2020-11-18T13:33:42
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cisco-fixes-webex-bugs-allowing-ghost-attackers-in-meetings

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-webex-bugs-allowing-ghost-attackers-in-meetings/){:target="_blank" rel="noopener"}

> Cisco has fixed today three Webex security vulnerabilities that would have allowed unauthenticated remote attackers to join ongoing meetings as ghost participants. [...]
