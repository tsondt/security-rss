Title: Friday Squid Blogging: Underwater Robot Uses Squid-Like Propulsion
Date: 2020-11-13T22:09:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: robotics;squid
Slug: friday-squid-blogging-underwater-robot-uses-squid-like-propulsion

[Source](https://www.schneier.com/blog/archives/2020/11/friday-squid-blogging-underwater-robot-uses-squid-like-propulsion.html){:target="_blank" rel="noopener"}

> This is neat : By generating powerful streams of water, UCSD’s squid-like robot can swim untethered. The “squidbot” carries its own power source, and has the room to hold more, including a sensor or camera for underwater exploration. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
