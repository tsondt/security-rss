Title: Pandemic, A Driving Force in 2021 Financial Crime
Date: 2020-11-30T17:46:24+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Malware;Most Recent ThreatLists;Vulnerabilities;Web Security;2021 predictions;Bitcoin;Cyberattacks;DDoS;Extortion;financial cybercrime;financially motivated;Kaspersky;magecart;ransomware;Zero Day Exploits
Slug: pandemic-a-driving-force-in-2021-financial-crime

[Source](https://threatpost.com/2021-financial-crime-covid-19/161665/){:target="_blank" rel="noopener"}

> Ransomware gangs with zero-days and more players overall will characterize financially motivated cyberattacks next year. [...]
