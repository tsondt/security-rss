Title: Facebook Messenger bug allowed Android users to spy on each other
Date: 2020-11-19T14:59:25
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Mobile
Slug: facebook-messenger-bug-allowed-android-users-to-spy-on-each-other

[Source](https://www.bleepingcomputer.com/news/security/facebook-messenger-bug-allowed-android-users-to-spy-on-each-other/){:target="_blank" rel="noopener"}

> Facebook fixed a critical flaw in the Facebook Messenger for Android messaging app that allowed callers to listen to other users' surroundings without permission before the person on the other end picked up the call. [...]
