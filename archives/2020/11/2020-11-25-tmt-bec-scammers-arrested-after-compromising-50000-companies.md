Title: TMT BEC scammers arrested after compromising 50,000 companies
Date: 2020-11-25T15:26:22
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: tmt-bec-scammers-arrested-after-compromising-50000-companies

[Source](https://www.bleepingcomputer.com/news/security/tmt-bec-scammers-arrested-after-compromising-50-000-companies/){:target="_blank" rel="noopener"}

> Following a year-long investigation led by Interpol, three members of a prolific cybergang with a confirmed victim count of about 50,000 organizations have been arrested recently in Lagos, Nigeria. [...]
