Title: Manchester United email servers remain offline amid what is being called a 'ransomware' attack
Date: 2020-11-27T16:15:06+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: manchester-united-email-servers-remain-offline-amid-what-is-being-called-a-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/27/manchester_united_ransomware_report/){:target="_blank" rel="noopener"}

> UK data watchdog has been told and 'forensic' probe is ongoing Players' managers looking to lift salaries by a couple of million pounds or so better check their email read receipts: a full week after Manchester United was hit by hackers, many of its systems remain offline, with at least one report claiming the club is being shaken down for ransom.... [...]
