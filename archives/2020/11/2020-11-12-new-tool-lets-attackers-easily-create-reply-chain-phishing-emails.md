Title: New tool lets attackers easily create reply-chain phishing emails
Date: 2020-11-12T13:32:41
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: new-tool-lets-attackers-easily-create-reply-chain-phishing-emails

[Source](https://www.bleepingcomputer.com/news/security/new-tool-lets-attackers-easily-create-reply-chain-phishing-emails/){:target="_blank" rel="noopener"}

> A new email tool advertised on a cybercriminal forum provides a stealthier method for carrying out fraud or malware attacks by allowing messages to be injected directly into the victim's inbox. [...]
