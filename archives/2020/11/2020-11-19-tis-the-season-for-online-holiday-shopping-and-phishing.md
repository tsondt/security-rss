Title: Tis’ the Season for Online Holiday Shopping; and Phishing
Date: 2020-11-19T19:25:45+00:00
Author: Brian Foster
Category: Threatpost
Tags: InfoSec Insider;Web Security;brian foster;COVID-19;holiday season;infosec insider;mobiliron;online shopping;phishing lures;Smishing;Spear Phishing;top phishing types;Vishing
Slug: tis-the-season-for-online-holiday-shopping-and-phishing

[Source](https://threatpost.com/online-holiday-shopping-phishing/161412/){:target="_blank" rel="noopener"}

> Watch out for these top phishing approaches this holiday season. [...]
