Title: Microsoft Exchange Attack Exposes New xHunt Backdoors
Date: 2020-11-09T15:53:26+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Cloud Security;Hacks;Malware;Vulnerabilities;backdoor;CASHY200;Kuwait;Microsoft;Microsoft Exchange;Microsoft Exchange Server attack;PowerShell;Snugy;TriFive;w3sp.exe;xHunt;Xhunt threat group
Slug: microsoft-exchange-attack-exposes-new-xhunt-backdoors

[Source](https://threatpost.com/microsoft-exchange-attack-xhunt-backdoors/161041/){:target="_blank" rel="noopener"}

> An attack on the Microsoft Exchange server of an organization in Kuwait revealed two never-before-seen Powershell backdoors. [...]
