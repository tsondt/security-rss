Title: GrowDiaries Exposes Emails, Passwords of 1.4M Cannabis Growers
Date: 2020-11-04T22:25:35+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Vulnerabilities;Web Security;cannabis data breach;card skimmer;data breach;data exposure;database;growdiaries;magecart;MD5;Phishing;phishing attack;stuffing attack;Tor Browser;unc1945
Slug: growdiaries-exposes-emails-passwords-of-14m-cannabis-growers

[Source](https://threatpost.com/growdiaries-emails-passwords-cannabis-growers/160969/){:target="_blank" rel="noopener"}

> Cannabis journaling platform GrowDiaries exposed more than 3.4 million user records online, many from countries where pot is illegal. [...]
