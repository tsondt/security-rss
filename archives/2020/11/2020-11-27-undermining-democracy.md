Title: Undermining Democracy
Date: 2020-11-27T12:10:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: disinformation;essays;national security policy;voting
Slug: undermining-democracy

[Source](https://www.schneier.com/blog/archives/2020/11/undermining-democracy.html){:target="_blank" rel="noopener"}

> Last Thursday, Rudy Giuliani, a Trump campaign lawyer, alleged a widespread voting conspiracy involving Venezuela, Cuba, and China. Another lawyer, Sidney Powell, argued that Mr. Trump won in a landslide, the entire election in swing states should be overturned and the legislatures should make sure that the electors are selected for the president. The Republican National Committee swung in to support her false claim that Mr. Trump won in a landslide, while Michigan election officials have tried to stop the certification of the vote. It is wildly unlikely that their efforts can block Joe Biden from becoming president. But they [...]
