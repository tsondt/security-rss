Title: Joe Biden Campaign Subdomain Down After Hacktivist Defacement
Date: 2020-11-23T16:46:37+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security;hacktivist;Joe biden;Joe biden campaign;president elect;Turkey;U.S. Government;U.S. presidential election;Website Defacement;Website hack;website security
Slug: joe-biden-campaign-subdomain-down-after-hacktivist-defacement

[Source](https://threatpost.com/joe-biden-campaign-website-hacktivist-defacement/161471/){:target="_blank" rel="noopener"}

> A Turkish hacktivist defaced a subdomain of the president-elect's campaign website. [...]
