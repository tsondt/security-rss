Title: MacOS Users Targeted By OceanLotus Backdoor
Date: 2020-11-30T17:52:50+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Malware;APT32;backdoor;macOS;macOS malware;malware;microsoft word;oceanlotus;OceanLotus APT;payload;Vietnamese cyberattack;ZIP archive
Slug: macos-users-targeted-by-oceanlotus-backdoor

[Source](https://threatpost.com/macos-users-targeted-oceanlotus-backdoor/161655/){:target="_blank" rel="noopener"}

> The new backdoor comes with multiple payloads and new detection evasion tactics. [...]
