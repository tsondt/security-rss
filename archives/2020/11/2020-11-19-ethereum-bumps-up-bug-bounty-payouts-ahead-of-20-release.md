Title: Ethereum bumps up bug bounty payouts ahead of 2.0 release
Date: 2020-11-19T14:29:44+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: ethereum-bumps-up-bug-bounty-payouts-ahead-of-20-release

[Source](https://portswigger.net/daily-swig/ethereum-bumps-up-bug-bounty-payouts-ahead-of-2-0-release){:target="_blank" rel="noopener"}

> Security researchers can earn themselves up to $50,000 for finding flaws in the cryptocurrency platform [...]
