Title: Coil payments platform leaks user emails in 'Privacy Policy' update
Date: 2020-11-17T09:11:11
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: coil-payments-platform-leaks-user-emails-in-privacy-policy-update

[Source](https://www.bleepingcomputer.com/news/security/coil-payments-platform-leaks-user-emails-in-privacy-policy-update/){:target="_blank" rel="noopener"}

> Micropayments platform Coil, used by content creators and popular blogs accidentally exposed the email addresses of some users in a mass email announcement. [...]
