Title: Ghimob Android Banking Trojan Targets 153 Mobile Apps
Date: 2020-11-10T16:41:29+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Mobile Security;Android;Banking Fraud;banking trojan;Brazil;cybercriminal;ghimob;google;Guildma;mobile app;Tetrade
Slug: ghimob-android-banking-trojan-targets-153-mobile-apps

[Source](https://threatpost.com/ghimob-android-banking-trojan/161075/){:target="_blank" rel="noopener"}

> A banking trojan is targeting mobile app users in Brazil - and researchers warn that its operator has big plans to expand abroad. [...]
