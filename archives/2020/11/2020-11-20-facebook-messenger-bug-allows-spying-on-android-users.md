Title: Facebook Messenger Bug Allows Spying on Android Users
Date: 2020-11-20T15:11:25+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;Android;apple;Facebook;flaw;google;Google Project Zero;messenger;mobile apps;mobile devices;security bug;Spying;video calls;voice calls;vulnerability
Slug: facebook-messenger-bug-allows-spying-on-android-users

[Source](https://threatpost.com/facebook-messenger-bug-spying-android/161435/){:target="_blank" rel="noopener"}

> The company patched a vulnerability that could connected video and audio calls without the knowledge of the person receiving them. [...]
