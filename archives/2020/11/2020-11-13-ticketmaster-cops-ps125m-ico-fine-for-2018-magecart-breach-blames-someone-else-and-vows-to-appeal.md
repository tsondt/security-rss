Title: Ticketmaster cops £1.25m ICO fine for 2018 Magecart breach, blames someone else and vows to appeal
Date: 2020-11-13T15:30:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ticketmaster-cops-ps125m-ico-fine-for-2018-magecart-breach-blames-someone-else-and-vows-to-appeal

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/13/ticketmaster_fined_1_25m_magecart_breach/){:target="_blank" rel="noopener"}

> Own your screwups, growls irate watchdog The Information Commissioner’s Office has fined Ticketmaster £1.25m after the site’s operators failed to spot a Magecart card skimmer infection until after 9 million customers’ details had been slurped by criminals.... [...]
