Title: Google to launch root store to validate web security certificates
Date: 2020-11-04T14:04:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-to-launch-root-store-to-validate-web-security-certificates

[Source](https://portswigger.net/daily-swig/google-to-launch-root-store-to-validate-web-security-certificates){:target="_blank" rel="noopener"}

> The Chrome Root Store aims to improve browser interoperability across different operating systems [...]
