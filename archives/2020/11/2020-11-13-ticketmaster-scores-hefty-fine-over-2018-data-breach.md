Title: Ticketmaster Scores Hefty Fine Over 2018 Data Breach
Date: 2020-11-13T17:04:48+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Government;Hacks;Malware;Web Security;$1.65 million;1.25 million;chatbot;data breach;Europe;fine;GDPR;General Data Protection Regulation;ICO;magecart;ticketmaster uk
Slug: ticketmaster-scores-hefty-fine-over-2018-data-breach

[Source](https://threatpost.com/ticketmaster-fine-2018-data-breach/161198/){:target="_blank" rel="noopener"}

> The events giant faces a GDPR-related penalty in the U.K., and more could follow. [...]
