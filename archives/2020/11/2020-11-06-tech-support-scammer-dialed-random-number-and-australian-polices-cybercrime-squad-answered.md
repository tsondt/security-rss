Title: Tech support scammer dialed random number and Australian Police’s cybercrime squad answered
Date: 2020-11-06T03:05:51+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: tech-support-scammer-dialed-random-number-and-australian-polices-cybercrime-squad-answered

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/06/sa_police_support_scam_intercept/){:target="_blank" rel="noopener"}

> Cops used the opportunity to figure out remote access traps A tech support scammer making random phone calls in the hope of finding a victim called the cybercrime squad of an Australian police force, which used the happy accident to document the scam and inform the public what to watch out for.... [...]
