Title: Zoom lied to users about end-to-end encryption for years, FTC says
Date: 2020-11-09T19:27:35+00:00
Author: Jon Brodkin
Category: Ars Technica
Tags: Biz & IT;Policy;FTC;zoom;zoomopener
Slug: zoom-lied-to-users-about-end-to-end-encryption-for-years-ftc-says

[Source](https://arstechnica.com/?p=1721606){:target="_blank" rel="noopener"}

> Enlarge / Zoom founder and CEO Eric Yuan speaks before the Nasdaq opening bell ceremony on April 18, 2019, in New York City as the company announced its IPO. (credit: Getty Images | Kena Betancur ) Zoom has agreed to upgrade its security practices in a tentative settlement with the Federal Trade Commission, which alleges that Zoom lied to users for years by claiming it offered end-to-end encryption. "[S]ince at least 2016, Zoom misled users by touting that it offered 'end-to-end, 256-bit encryption' to secure users' communications, when in fact it provided a lower level of security," the FTC said [...]
