Title: The CEO’s chuckling at their email… you better check your security defenses
Date: 2020-11-30T18:00:09+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: the-ceos-chuckling-at-their-email-you-better-check-your-security-defenses

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/30/save_execs_from_themselves/){:target="_blank" rel="noopener"}

> Join us to learn how to save execs from themselves Webcast You have to hand it to the cyber-criminals. They really know how to mix a perfectly balanced cocktail of software engineering and human insight when it comes to crafting the perfect spear-phishing attack.... [...]
