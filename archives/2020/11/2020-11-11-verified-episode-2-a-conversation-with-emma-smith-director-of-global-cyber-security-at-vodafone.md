Title: Verified, episode 2 – A Conversation with Emma Smith, Director of Global Cyber Security at Vodafone
Date: 2020-11-11T21:08:05+00:00
Author: Stephen Schmidt
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Cloud security;cybersecurity;diversity;Emma Smith;enterprise IT security;inclusion;Privacy;Security Blog;security talent;team-building;Vodafone
Slug: verified-episode-2-a-conversation-with-emma-smith-director-of-global-cyber-security-at-vodafone

[Source](https://aws.amazon.com/blogs/security/verified-episode-2-conversation-with-emma-smith-director-of-global-cyber-security-at-vodafone/){:target="_blank" rel="noopener"}

> Over the past 8 months, it’s become more important for us all to stay in contact with peers around the globe. Today, I’m proud to bring you the second episode of our new video series, Verified: Presented by AWS re:Inforce. Even though we couldn’t be together this year at re:Inforce, our annual security conference, we [...]
