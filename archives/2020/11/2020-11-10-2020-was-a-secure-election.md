Title: 2020 Was a Secure Election
Date: 2020-11-10T12:40:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;disinformation;voting
Slug: 2020-was-a-secure-election

[Source](https://www.schneier.com/blog/archives/2020/11/2020-was-a-secure-election.html){:target="_blank" rel="noopener"}

> Over at Lawfare: “ 2020 Is An Election Security Success Story (So Far).” What’s more, the voting itself was remarkably smooth. It was only a few months ago that professionals and analysts who monitor election administration were alarmed at how badly unprepared the country was for voting during a pandemic. Some of the primaries were disasters. There were not clear rules in many states for voting by mail or sufficient opportunities for voting early. There was an acute shortage of poll workers. Yet the United States saw unprecedented turnout over the last few weeks. Many states handled voting by mail [...]
