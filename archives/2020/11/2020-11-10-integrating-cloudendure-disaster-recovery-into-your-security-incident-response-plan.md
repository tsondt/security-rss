Title: Integrating CloudEndure Disaster Recovery into your security incident response plan
Date: 2020-11-10T19:27:03+00:00
Author: Gonen Stein
Category: AWS Security
Tags: AWS Well-Architected Framework;Best Practices;CloudEndure Disaster Recovery;Intermediate (200);Security, Identity, & Compliance;Business continuity;Disaster Recovery;Incident response;IT resilience;Security Blog
Slug: integrating-cloudendure-disaster-recovery-into-your-security-incident-response-plan

[Source](https://aws.amazon.com/blogs/security/integrating-cloudendure-disaster-recovery-into-your-security-incident-response-plan/){:target="_blank" rel="noopener"}

> An incident response plan (also known as procedure) contains the detailed actions an organization takes to prepare for a security incident in its IT environment. It also includes the mechanisms to detect, analyze, contain, eradicate, and recover from a security incident. Every incident response plan should contain a section on recovery, which outlines scenarios ranging [...]
