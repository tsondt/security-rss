Title: Friday Squid Blogging: Diplomoceras Maximum
Date: 2020-11-27T22:33:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid
Slug: friday-squid-blogging-diplomoceras-maximum

[Source](https://www.schneier.com/blog/archives/2020/11/friday-squid-blogging-diplomoceras-maximum.html){:target="_blank" rel="noopener"}

> Diplomoceras maximum is an ancient squid-like creature. It lived about 68 million years ago, looked kind of like a giant paperclip, and may have had a lifespan of 200 years. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
