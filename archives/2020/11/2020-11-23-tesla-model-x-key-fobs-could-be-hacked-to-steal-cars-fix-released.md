Title: Tesla Model X key fobs could be hacked to steal cars, fix released
Date: 2020-11-23T13:54:54
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: tesla-model-x-key-fobs-could-be-hacked-to-steal-cars-fix-released

[Source](https://www.bleepingcomputer.com/news/security/tesla-model-x-key-fobs-could-be-hacked-to-steal-cars-fix-released/){:target="_blank" rel="noopener"}

> Researchers at the University of Leuven in Belgium found vulnerabilities in the keyless entry system of the Tesla Model X that would have allowed attackers to steal the $100,000 car within just a few minutes. [...]
