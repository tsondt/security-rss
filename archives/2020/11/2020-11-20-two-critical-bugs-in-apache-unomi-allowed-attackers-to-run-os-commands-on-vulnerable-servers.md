Title: Two critical bugs in Apache Unomi allowed attackers to run OS commands on vulnerable servers
Date: 2020-11-20T13:42:24+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: two-critical-bugs-in-apache-unomi-allowed-attackers-to-run-os-commands-on-vulnerable-servers

[Source](https://portswigger.net/daily-swig/two-critical-bugs-in-apache-unomi-allowed-attackers-to-run-os-commands-on-vulnerable-servers){:target="_blank" rel="noopener"}

> Fresh set of RCE flaws addressed as researchers discover shortcomings in previous patch [...]
