Title: Bug Bounty Radar // The latest bug bounty programs for November 2020
Date: 2020-11-30T15:39:20+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: bug-bounty-radar-the-latest-bug-bounty-programs-for-november-2020

[Source](https://portswigger.net/daily-swig/bug-bounty-radar-the-latest-bug-bounty-programs-for-november-2020){:target="_blank" rel="noopener"}

> New web targets for the discerning hacker [...]
