Title: Chesapeake Regional Healthcare data breach exposes 23,000 individuals’ sensitive information
Date: 2020-11-04T15:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: chesapeake-regional-healthcare-data-breach-exposes-23000-individuals-sensitive-information

[Source](https://portswigger.net/daily-swig/chesapeake-regional-healthcare-data-breach-exposes-23-000-individuals-sensitive-information){:target="_blank" rel="noopener"}

> Widespread attack on Blackbaud fundraising software claims yet more victims [...]
