Title: Sneaky Office 365 phishing inverts images to evade detection
Date: 2020-11-04T09:00:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: sneaky-office-365-phishing-inverts-images-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/sneaky-office-365-phishing-inverts-images-to-evade-detection/){:target="_blank" rel="noopener"}

> A creative Office 365 phishing campaign has been inverting images used as backgrounds for landing pages to avoid getting flagged as malicious by crawlers designed to spot phishing sites. [...]
