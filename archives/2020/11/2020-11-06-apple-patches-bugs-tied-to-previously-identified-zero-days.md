Title: Apple Patches Bugs Tied to Previously Identified Zero-Days
Date: 2020-11-06T14:05:37+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Mobile Security;Vulnerabilities;apple;google;Google Project Zero;ios;iPadOS;iphone;ipod;threat actors;vulnerabilities;Windows;zero-day vulnerabilities
Slug: apple-patches-bugs-tied-to-previously-identified-zero-days

[Source](https://threatpost.com/apple-patches-bugs-zero-days/161010/){:target="_blank" rel="noopener"}

> The actively exploited vulnerabilities discovered by Project Zero exist across iPhone, iPad and iPod devices. [...]
