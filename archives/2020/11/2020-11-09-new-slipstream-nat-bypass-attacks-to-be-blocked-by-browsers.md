Title: New Slipstream NAT bypass attacks to be blocked by browsers
Date: 2020-11-09T16:09:08
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: new-slipstream-nat-bypass-attacks-to-be-blocked-by-browsers

[Source](https://www.bleepingcomputer.com/news/security/new-slipstream-nat-bypass-attacks-to-be-blocked-by-browsers/){:target="_blank" rel="noopener"}

> Web browser vendors are planning to block a new attack technique that would allow attackers to bypass a victim's NAT/firewall to gain access to any TCP/UDP service hosted on their devices. [...]
