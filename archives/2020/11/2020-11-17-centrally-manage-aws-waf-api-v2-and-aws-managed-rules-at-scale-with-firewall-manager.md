Title: Centrally manage AWS WAF (API v2) and AWS Managed Rules at scale with Firewall Manager
Date: 2020-11-17T23:51:41+00:00
Author: Umesh Ramesh
Category: AWS Security
Tags: Advanced (300);AWS Firewall Manager;AWS WAF;Security, Identity, & Compliance;Security Blog
Slug: centrally-manage-aws-waf-api-v2-and-aws-managed-rules-at-scale-with-firewall-manager

[Source](https://aws.amazon.com/blogs/security/centrally-manage-aws-waf-api-v2-and-aws-managed-rules-at-scale-with-firewall-manager/){:target="_blank" rel="noopener"}

> Since AWS Firewall Manager was introduced in 2018, it has evolved with many more features and today also supports the newest version of AWS WAF, as well as the latest AWS WAF APIs (AWS WAFV2), and AWS Managed Rules for AWS WAF. (Note that the original AWS WAF APIs are still available and supported under [...]
