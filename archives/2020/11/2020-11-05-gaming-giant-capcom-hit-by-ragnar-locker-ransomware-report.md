Title: Gaming Giant Capcom Hit By Ragnar Locker Ransomware: Report
Date: 2020-11-05T20:49:34+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Capcom;cyberattack;Darkstalkers;game;ragnar locker;ransomware;resident evil;stolen data;Street Fighter
Slug: gaming-giant-capcom-hit-by-ragnar-locker-ransomware-report

[Source](https://threatpost.com/gaming-giant-capcom-ragnar-locker-ransomware/160996/){:target="_blank" rel="noopener"}

> The Resident Evil creator reportedly been hit in a ransomware attack that stole 1TB of sensitive data. [...]
