Title: Ransomware gang hacks Facebook account to run extortion ads
Date: 2020-11-11T02:30:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomware-gang-hacks-facebook-account-to-run-extortion-ads

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-hacks-facebook-account-to-run-extortion-ads/){:target="_blank" rel="noopener"}

> ​A ransomware group has now started to run Facebook advertisements to pressure victims to pay a ransom. [...]
