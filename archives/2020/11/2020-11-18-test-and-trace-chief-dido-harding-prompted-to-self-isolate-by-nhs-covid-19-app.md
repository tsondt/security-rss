Title: Test and Trace chief Dido Harding prompted to self-isolate by NHS COVID-19 app
Date: 2020-11-18T12:11:46+00:00
Author: Matthew Hughes
Category: The Register
Tags: 
Slug: test-and-trace-chief-dido-harding-prompted-to-self-isolate-by-nhs-covid-19-app

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/18/test_and_trace_chief_dido_harding_self_isolating/){:target="_blank" rel="noopener"}

> Threatens plenty of Zoom sessions in the days ahead... just as well they've updated security in recent weeks There's nothing quite like eating your own dog food, as Test and Trace chief Baroness Dido Harding has learned after being instructed to self-isolate by the NHS COVID-19 contact-tracing app overnight.... [...]
