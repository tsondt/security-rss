Title: Oracle issues emergency patch for critical WebLogic Server flaw
Date: 2020-11-02T14:06:43
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: oracle-issues-emergency-patch-for-critical-weblogic-server-flaw

[Source](https://www.bleepingcomputer.com/news/security/oracle-issues-emergency-patch-for-critical-weblogic-server-flaw/){:target="_blank" rel="noopener"}

> Oracle issued an out-of-band security update over the weekend to address a critical remote code execution (RCE) vulnerability impacting multiple Oracle WebLogic Server versions. [...]
