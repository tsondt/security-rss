Title: HMRC smishing tax scam targets UK banking customers
Date: 2020-11-08T13:10:10
Author: Ax Sharma
Category: BleepingComputer
Tags: Government
Slug: hmrc-smishing-tax-scam-targets-uk-banking-customers

[Source](https://www.bleepingcomputer.com/news/security/hmrc-smishing-tax-scam-targets-uk-banking-customers/){:target="_blank" rel="noopener"}

> An advanced HMRC tax rebate scam has been targeting UK residents this week via text messages (SMS). The smishing campaign is concerning as it employs multiple HMRC phishing domains and tactics, with new domains being added every day as older ones get flagged by spam filters. [...]
