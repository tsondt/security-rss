Title: Patch Tuesday, November 2020 Edition
Date: 2020-11-11T01:56:41+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;Bob Huber;CVE-2020-15999;CVE-2020-16875;CVE-2020-17051;CVE-2020-17087;Dustin Childs;Microsoft Exchange Server;Tenable;trend micro;Windows Network File System;Zero Day Initiative
Slug: patch-tuesday-november-2020-edition

[Source](https://krebsonsecurity.com/2020/11/patch-tuesday-november-2020-edition/){:target="_blank" rel="noopener"}

> Adobe and Microsoft each issued a bevy of updates today to plug critical security holes in their software. Microsoft’s release includes fixes for 112 separate flaws, including one zero-day vulnerability that is already being exploited to attack Windows users. Microsoft also is taking flak for changing its security advisories and limiting the amount of information disclosed about each bug. Some 17 of the 112 issues fixed in today’s patch batch involve “critical” problems in Windows, or those that can be exploited by malware or malcontents to seize complete, remote control over a vulnerable Windows computer without any help from users. [...]
