Title: Proofpoint survey: IT security leaders worry about and are ill-prepared to defeat cyber-attacks
Date: 2020-11-05T07:30:12+00:00
Author: Eira Hayward
Category: The Register
Tags: 
Slug: proofpoint-survey-it-security-leaders-worry-about-and-are-ill-prepared-to-defeat-cyber-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/05/proofpoint_survey_it_security_leaders/){:target="_blank" rel="noopener"}

> Remote workers are now the perimeter Sponsored IT security leaders say they are ill-prepared for a cyber attack and believe that human error and a lack of security awareness are major risk factors for their organisations, according to a series of reports and surveys from cybersecurity vendor Proofpoint. But there are some marked variations in both the rates and the types of cyber attack between the regions surveyed.... [...]
