Title: Popular stock photo service hit by data breach, 8.3M records for sale
Date: 2020-11-12T13:07:32
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: popular-stock-photo-service-hit-by-data-breach-83m-records-for-sale

[Source](https://www.bleepingcomputer.com/news/security/popular-stock-photo-service-hit-by-data-breach-83m-records-for-sale/){:target="_blank" rel="noopener"}

> Stock photo site 123RF has suffered a data breach after a hacker began selling a database containing 8.3 million user records on a hacker forum. [...]
