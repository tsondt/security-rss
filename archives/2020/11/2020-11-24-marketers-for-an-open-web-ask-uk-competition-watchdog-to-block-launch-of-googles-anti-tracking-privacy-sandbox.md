Title: Marketers for an Open Web ask UK competition watchdog to block launch of Google's anti-tracking Privacy Sandbox
Date: 2020-11-24T07:28:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: marketers-for-an-open-web-ask-uk-competition-watchdog-to-block-launch-of-googles-anti-tracking-privacy-sandbox

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/24/google_mow_chrome/){:target="_blank" rel="noopener"}

> Group claims adtech 'has nothing to do with privacy' but is rather an attempt 'to take control of the web' Google's Privacy Sandbox took another knock today as Marketers for an Open Web (MOW) wrote to the UK's Competition and Markets Authority (CMA) requesting a block on the technology's launch.... [...]
