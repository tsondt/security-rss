Title: Network hacking and ransomware fueling global cybercrime surge
Date: 2020-11-26T12:22:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: network-hacking-and-ransomware-fueling-global-cybercrime-surge

[Source](https://portswigger.net/daily-swig/network-hacking-and-ransomware-fueling-global-cybercrime-surge){:target="_blank" rel="noopener"}

> Carding market doubles in the midst of coronavirus pandemic, reports Group-IB [...]
