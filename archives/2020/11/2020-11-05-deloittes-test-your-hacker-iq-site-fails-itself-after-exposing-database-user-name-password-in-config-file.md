Title: Deloitte's 'Test your Hacker IQ' site fails itself after exposing database user name, password in config file
Date: 2020-11-05T08:28:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: deloittes-test-your-hacker-iq-site-fails-itself-after-exposing-database-user-name-password-in-config-file

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/05/deloitte_hacker_test/){:target="_blank" rel="noopener"}

> Security quiz site created by advisors includes inadvertent bonus round A website created for global consultancy Deloitte to quiz people on knowledge of hacking tactics has proven itself vulnerable to hacking.... [...]
