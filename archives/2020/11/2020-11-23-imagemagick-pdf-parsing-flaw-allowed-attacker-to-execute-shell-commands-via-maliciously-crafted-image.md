Title: ImageMagick PDF-parsing flaw allowed attacker to execute shell commands via maliciously crafted image
Date: 2020-11-23T16:40:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: imagemagick-pdf-parsing-flaw-allowed-attacker-to-execute-shell-commands-via-maliciously-crafted-image

[Source](https://portswigger.net/daily-swig/imagemagick-pdf-parsing-flaw-allowed-attacker-to-execute-shell-commands-via-maliciously-crafted-image){:target="_blank" rel="noopener"}

> Exploit inspired by notorious ‘ImageTragick’ bug from 2016 [...]
