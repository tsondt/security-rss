Title: re:Invent 2020 – Your guide to AWS Identity and Data Protection sessions
Date: 2020-11-30T18:54:57+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;cryptography;Data protection;Identity;Privacy;re:Invent 2020;Security Blog
Slug: reinvent-2020-your-guide-to-aws-identity-and-data-protection-sessions

[Source](https://aws.amazon.com/blogs/security/reinvent-2020-your-guide-to-aws-identity-and-data-protection-sessions/){:target="_blank" rel="noopener"}

> AWS re:Invent will certainly be different in 2020! Instead of seeing you all in Las Vegas, this year re:Invent will be a free, three-week virtual conference. One thing that will remain the same is the variety of sessions, including many Security, Identity, and Compliance sessions. As we developed sessions, we looked to customers—asking where they [...]
