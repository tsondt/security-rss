Title: Was that November's Patch Tuesday? Already? Oh, no, it's just Adobe issuing 14 emergency security fixes
Date: 2020-11-04T06:28:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: was-that-novembers-patch-tuesday-already-oh-no-its-just-adobe-issuing-14-emergency-security-fixes

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/04/adobe_emergency_patch/){:target="_blank" rel="noopener"}

> Critical Acrobat, Reader flaws evidently couldn't wait until next week Adobe on Tuesday published updated versions of its Acrobat and Reader software to fix fourteen flaws, four of which have been designated "critical." These updates should be installed as soon as possible to close off their vulnerabilities.... [...]
