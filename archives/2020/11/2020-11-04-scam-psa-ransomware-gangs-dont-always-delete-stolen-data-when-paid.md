Title: Scam PSA: Ransomware gangs don't always delete stolen data when paid
Date: 2020-11-04T17:47:08
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: scam-psa-ransomware-gangs-dont-always-delete-stolen-data-when-paid

[Source](https://www.bleepingcomputer.com/news/security/scam-psa-ransomware-gangs-dont-always-delete-stolen-data-when-paid/){:target="_blank" rel="noopener"}

> Ransomware gangs are increasingly failing to keep their promise to delete stolen data after a victim pays a ransom. [...]
