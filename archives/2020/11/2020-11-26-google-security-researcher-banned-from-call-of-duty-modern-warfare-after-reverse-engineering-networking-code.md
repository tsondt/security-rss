Title: Google security researcher banned from Call of Duty: Modern Warfare after ‘reverse engineering networking code’
Date: 2020-11-26T14:45:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-security-researcher-banned-from-call-of-duty-modern-warfare-after-reverse-engineering-networking-code

[Source](https://portswigger.net/daily-swig/google-security-researcher-banned-from-call-of-duty-modern-warfare-after-reverse-engineering-networking-code){:target="_blank" rel="noopener"}

> Ned Williamson urges video game developers to accommodate legitimate research [...]
