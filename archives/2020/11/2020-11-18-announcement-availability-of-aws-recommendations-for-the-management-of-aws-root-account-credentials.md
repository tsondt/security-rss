Title: Announcement: Availability of AWS Recommendations for the management of AWS root account credentials
Date: 2020-11-18T19:40:39+00:00
Author: Jonathan Jenkyn
Category: AWS Security
Tags: Announcements;AWS Organizations;Foundational (100);Security, Identity, & Compliance;Account structure;Assume-Role;AWS IAM;Break-Glass;MFA;MFA management;organizational unit;OUs;passwords;Privileged;root account;Security Blog
Slug: announcement-availability-of-aws-recommendations-for-the-management-of-aws-root-account-credentials

[Source](https://aws.amazon.com/blogs/security/announcement-availability-of-aws-recommendations-for-management-of-aws-root-account-credentials/){:target="_blank" rel="noopener"}

> When AWS customers open their first account, they assume the responsibility for securely managing access to their root account credentials, under the Shared Responsibility Model. Initially protected by a password, it is the responsibility of each AWS customer to make decisions based on their operational and security requirements as to how they configure and manage [...]
