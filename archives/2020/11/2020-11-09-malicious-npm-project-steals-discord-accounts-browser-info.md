Title: Malicious NPM project steals Discord accounts, browser info
Date: 2020-11-09T17:37:59
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: malicious-npm-project-steals-discord-accounts-browser-info

[Source](https://www.bleepingcomputer.com/news/security/malicious-npm-project-steals-discord-accounts-browser-info/){:target="_blank" rel="noopener"}

> A heavily obfuscated and malicious NPM project is used to steal Discord user tokens and browser information from unsuspecting users. [...]
