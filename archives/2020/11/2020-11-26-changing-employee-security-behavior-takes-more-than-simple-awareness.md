Title: Changing Employee Security Behavior Takes More Than Simple Awareness
Date: 2020-11-26T14:00:32+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cloud Security;Hacks;Malware;Mobile Security;Most Recent ThreatLists;Vulnerabilities;Web Security;behavioral audit;changing security behavior;corporate cybersecurity;employee security behavior;isf;password hygiene;security awareness program;Security practices;training
Slug: changing-employee-security-behavior-takes-more-than-simple-awareness

[Source](https://threatpost.com/changing-employee-security-behavior-awareness/161607/){:target="_blank" rel="noopener"}

> Designing a behavioral change program requires an audit of existing security practices and where the sticking points are. [...]
