Title: How AI Is powering a new generation of cyber-attacks
Date: 2020-11-18T18:02:13+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: how-ai-is-powering-a-new-generation-of-cyber-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/18/the_battle_of_the_algorithms/){:target="_blank" rel="noopener"}

> The battle of the algorithms has begun Sponsored It was 2017 and a hacker had gained access to a digital system at an organization in India. At first it seemed like just a normal intrusion - the kind that happens thousands of times each day. But this one was different.... [...]
