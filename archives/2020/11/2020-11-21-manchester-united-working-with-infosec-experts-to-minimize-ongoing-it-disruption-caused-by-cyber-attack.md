Title: Manchester United working with infosec experts to 'minimize ongoing IT disruption' caused by 'cyber attack'
Date: 2020-11-21T15:41:40+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: manchester-united-working-with-infosec-experts-to-minimize-ongoing-it-disruption-caused-by-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/21/manchester_united_working_with_infosec/){:target="_blank" rel="noopener"}

> No data leaked, says football club Manchester United is working with infosec pros to “minimize the ongoing IT disruption” that it says was caused by an assault on its tech systems.... [...]
