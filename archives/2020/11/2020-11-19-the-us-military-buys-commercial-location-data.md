Title: The US Military Buys Commercial Location Data
Date: 2020-11-19T15:37:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: data collection;GPS;law enforcement;military
Slug: the-us-military-buys-commercial-location-data

[Source](https://www.schneier.com/blog/archives/2020/11/the-us-military-buys-commercial-location-data.html){:target="_blank" rel="noopener"}

> Vice has a long article about how the US military buys commercial location data worldwide. The U.S. military is buying the granular movement data of people around the world, harvested from innocuous-seeming apps, Motherboard has learned. The most popular app among a group Motherboard analyzed connected to this sort of data sale is a Muslim prayer and Quran app that has more than 98 million downloads worldwide. Others include a Muslim dating app, a popular Craigslist app, an app for following storms, and a “level” app that can be used to help, for example, install shelves in a bedroom. This [...]
