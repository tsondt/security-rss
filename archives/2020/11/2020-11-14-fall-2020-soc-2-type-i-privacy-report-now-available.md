Title: Fall 2020 SOC 2 Type I Privacy report now available
Date: 2020-11-14T19:31:58+00:00
Author: Ninad Naik
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Privacy Report;AWS SOC Reports;Security Blog
Slug: fall-2020-soc-2-type-i-privacy-report-now-available

[Source](https://aws.amazon.com/blogs/security/fall-2020-soc-2-type-i-privacy-report-now-available/){:target="_blank" rel="noopener"}

> Your privacy considerations are at the core of our compliance work, and at AWS, we are focused on the protection of your content while using Amazon Web Services. Our Fall 2020 SOC 2 Type I Privacy report is now available, demonstrating the privacy compliance commitments we made to you. The Fall 2020 SOC 2 Type [...]
