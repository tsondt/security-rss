Title: The North Face resets passwords after credential stuffing attack
Date: 2020-11-12T17:18:34
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: the-north-face-resets-passwords-after-credential-stuffing-attack

[Source](https://www.bleepingcomputer.com/news/security/the-north-face-resets-passwords-after-credential-stuffing-attack/){:target="_blank" rel="noopener"}

> Outdoor retail giant The North Face has reset the passwords of an undisclosed number of customers following a successful credential stuffing attack that took place last month, on October 9th. [...]
