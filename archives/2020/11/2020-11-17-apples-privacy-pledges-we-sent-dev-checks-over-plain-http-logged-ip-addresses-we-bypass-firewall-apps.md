Title: Apple's privacy pledges: We sent dev checks over plain HTTP, logged IP addresses. We bypass firewall apps
Date: 2020-11-17T07:51:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: apples-privacy-pledges-we-sent-dev-checks-over-plain-http-logged-ip-addresses-we-bypass-firewall-apps

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/17/apple_big_sur_privacy/){:target="_blank" rel="noopener"}

> Big Sur highlights shortcomings in OCSP comms, APIs Analysis Apple plans to revise the way it checks the trustworthiness of Mac applications when they're run – after server problems last week during the launch of macOS Big Sur prevented people's desktop apps from starting.... [...]
