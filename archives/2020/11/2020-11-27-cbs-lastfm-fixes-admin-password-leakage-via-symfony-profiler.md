Title: CBS Last.fm fixes admin password leakage via Symfony profiler
Date: 2020-11-27T09:43:05
Author: Ax Sharma
Category: BleepingComputer
Tags: 
Slug: cbs-lastfm-fixes-admin-password-leakage-via-symfony-profiler

[Source](https://www.bleepingcomputer.com/news/security/cbs-lastfm-fixes-admin-password-leakage-via-symfony-profiler/){:target="_blank" rel="noopener"}

> This week, British music streaming service, Last.fm has fixed a credentials leak on their systems. The leak occurred due to a misconfigured Symfony profiler, exposing admin username and password. [...]
