Title: Brazil's court system under massive RansomExx ransomware attack
Date: 2020-11-05T16:09:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: brazils-court-system-under-massive-ransomexx-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/brazils-court-system-under-massive-ransomexx-ransomware-attack/){:target="_blank" rel="noopener"}

> Brazil's Superior Court of Justice was hit by a ransomware attack on Tuesday during judgment sessions that were taking place over video conference. [...]
