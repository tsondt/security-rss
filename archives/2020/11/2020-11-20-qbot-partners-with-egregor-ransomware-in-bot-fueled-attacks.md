Title: QBot partners with Egregor ransomware in bot-fueled attacks
Date: 2020-11-20T05:00:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: qbot-partners-with-egregor-ransomware-in-bot-fueled-attacks

[Source](https://www.bleepingcomputer.com/news/security/qbot-partners-with-egregor-ransomware-in-bot-fueled-attacks/){:target="_blank" rel="noopener"}

> The Qbot banking trojan has dropped the ProLock ransomware in favor of the Egregor ransomware who burst into activity in September. [...]
