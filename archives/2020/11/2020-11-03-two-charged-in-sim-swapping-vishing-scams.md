Title: Two Charged in SIM Swapping, Vishing Scams
Date: 2020-11-03T18:30:54+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;SIM Swapping;Web Fraud 2.0;Champagne;Jordan K. Milleson;Kyell A. Bryan;ogusers;SIM swapping;SWATting;vishing
Slug: two-charged-in-sim-swapping-vishing-scams

[Source](https://krebsonsecurity.com/2020/11/two-charged-in-sim-swapping-vishing-scams/){:target="_blank" rel="noopener"}

> Two young men from the eastern United States have been hit with identity theft and conspiracy charges for allegedly stealing bitcoin and social media accounts by tricking employees at wireless phone companies into giving away credentials needed to remotely access and modify customer account information. Prosecutors say Jordan K. Milleson, 21 of Timonium, Md. and 19-year-old Kingston, Pa. resident Kyell A. Bryan hijacked social media and bitcoin accounts using a mix of voice phishing or “ vishing ” attacks and “ SIM swapping,” a form of fraud that involves bribing or tricking employees at mobile phone companies. Investigators allege the [...]
