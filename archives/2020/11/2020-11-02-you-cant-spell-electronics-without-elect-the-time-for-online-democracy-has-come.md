Title: You can't spell 'electronics' without 'elect': The time for online democracy has come
Date: 2020-11-02T11:00:14+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: you-cant-spell-electronics-without-elect-the-time-for-online-democracy-has-come

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/02/electronic_voting/){:target="_blank" rel="noopener"}

> Software, security, distributed systems, process-based engineering... e-voting might not be such a bad idea Column E-voting over the internet is by common consent a bad idea.... [...]
