Title: Experiment reveals differences in secret leak detection on Git code repositories
Date: 2020-11-16T15:47:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: experiment-reveals-differences-in-secret-leak-detection-on-git-code-repositories

[Source](https://portswigger.net/daily-swig/experiment-reveals-differences-in-secret-leak-detection-on-git-code-repositories){:target="_blank" rel="noopener"}

> Do you want to know a secret? [...]
