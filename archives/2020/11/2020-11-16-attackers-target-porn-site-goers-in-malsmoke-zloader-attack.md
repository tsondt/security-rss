Title: Attackers Target Porn Site Goers in ‘Malsmoke’ Zloader Attack
Date: 2020-11-16T21:47:33+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Web Security;bravoporn;executable;Java Update;malsmoke;Malvertising;malware;Xhamster;ZLoader
Slug: attackers-target-porn-site-goers-in-malsmoke-zloader-attack

[Source](https://threatpost.com/attackers-porn-malsmoke-zloader-attack/161277/){:target="_blank" rel="noopener"}

> A fake Java update found on various porn sites actually downloads the well-known Zloader malware. [...]
