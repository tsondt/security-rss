Title: AWS Security Profiles: Colm MacCárthaigh, Senior Principal Engineer
Date: 2020-11-24T18:28:49+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;AWS Security Profile;re:Invent 2020;Security Blog
Slug: aws-security-profiles-colm-maccarthaigh-senior-principal-engineer

[Source](https://aws.amazon.com/blogs/security/aws-security-profiles-colm-maccarthaigh-senior-principal-engineer/){:target="_blank" rel="noopener"}

> In the weeks leading up to re:Invent, we’ll share conversations we’ve had with people at AWS who will be presenting, and get a sneak peek at their work. How long have you been at AWS and what do you do in your current role? I joined in 2008 to help build Amazon CloudFront, our content [...]
