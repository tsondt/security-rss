Title: Feds throw book at eBay execs who deny they had anything to do with cyberstalking of site's critics
Date: 2020-11-04T22:47:09+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: feds-throw-book-at-ebay-execs-who-deny-they-had-anything-to-do-with-cyberstalking-of-sites-critics

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/04/feds_stalking_ebay/){:target="_blank" rel="noopener"}

> James Baugh, David Harville hit with several new counts Two senior eBay executives who have refused to join their colleagues and plead guilty to charges of cyberstalking have been hit with a string of fresh charges.... [...]
