Title: Egregor ransomware shoots ransom notes out of victims' printers
Date: 2020-11-18T17:25:04
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: egregor-ransomware-shoots-ransom-notes-out-of-victims-printers

[Source](https://www.bleepingcomputer.com/news/security/egregor-ransomware-shoots-ransom-notes-out-of-victims-printers/){:target="_blank" rel="noopener"}

> The Egregor ransomware uses a novel approach to get a victim's attention after an attack - shoot ransom notes from all available printers. [...]
