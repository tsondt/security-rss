Title: Google Services Weaponized to Bypass Security in Phishing, BEC Campaigns
Date: 2020-11-20T20:56:10+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Web Security;armorblox;BEC attacks;Business Email Compromise;bypass;Cyberattacks;email gateways;email security;Firebase;google docs;Google Forms;Google Services;microsoft teams;Phishing;social engineering
Slug: google-services-weaponized-to-bypass-security-in-phishing-bec-campaigns

[Source](https://threatpost.com/google-services-weaponized-to-bypass-security-in-phishing-bec-campaigns/161467/){:target="_blank" rel="noopener"}

> Attackers exploiting an array of Google Services, including Forms, Firebase, Docs and more to boost phishing and BEC campaigns. [...]
