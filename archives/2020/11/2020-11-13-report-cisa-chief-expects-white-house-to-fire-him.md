Title: Report: CISA Chief Expects White House to Fire Him
Date: 2020-11-13T12:54:20+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;Web Security;Bryan Are;christopher krebs;CISA;Cybersecurity;Cybersecurity and Infrastructure Security Agency (CISA);Department of Homeland Security;firing;President Trump;White House
Slug: report-cisa-chief-expects-white-house-to-fire-him

[Source](https://threatpost.com/report-cisa-chief-expects-white-house-to-fire-him/161185/){:target="_blank" rel="noopener"}

> Chris Krebs, the first and current U.S. cybersecurity director, said his protection of election process drew ire from Trump administration. [...]
