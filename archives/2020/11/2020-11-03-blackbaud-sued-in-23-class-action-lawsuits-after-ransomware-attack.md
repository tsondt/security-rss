Title: Blackbaud sued in 23 class action lawsuits after ransomware attack
Date: 2020-11-03T10:56:22
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: blackbaud-sued-in-23-class-action-lawsuits-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/blackbaud-sued-in-23-class-action-lawsuits-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Leading cloud software provider Blackbaud has been sued in 23 proposed consumer class action cases in the U.S. and Canada related to the ransomware attack that the company suffered in May 2020. [...]
