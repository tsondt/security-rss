Title: Try to avoid thinking of the internet as a flashy new battlefield, warns former NCSC chief
Date: 2020-11-11T16:47:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: try-to-avoid-thinking-of-the-internet-as-a-flashy-new-battlefield-warns-former-ncsc-chief

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/11/ciaran_martin_speech_cyber_policy/){:target="_blank" rel="noopener"}

> Plus: Naming 'n' shaming doesn't stop hostile countries having a pop the UK The former head of the National Cyber Security Centre has warned that some British government figures have a “profound lack of understanding” of cyberspace, online warfare and information security.... [...]
