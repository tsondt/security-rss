Title: Cisco Zero-Day in AnyConnect Secure Mobility Client Remains Unpatched
Date: 2020-11-05T15:16:26+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Web Security;anyconnect secure mobility client;Cisco;cisco SD-WAN;Cisco WebEx;cisco zero-day;CVE-2020-3556;CVE-2020-3573;CVE-2020-3588;CVE-2020-3603;CVE-2020-3604;patch;vulnerability
Slug: cisco-zero-day-in-anyconnect-secure-mobility-client-remains-unpatched

[Source](https://threatpost.com/cisco-zero-day-anyconnect-secure-patch/160988/){:target="_blank" rel="noopener"}

> Cisco also disclosed high-severity vulnerabilities in its Webex and SD-WAN products. [...]
