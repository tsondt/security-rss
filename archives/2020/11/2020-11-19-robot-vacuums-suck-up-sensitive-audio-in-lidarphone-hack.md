Title: Robot Vacuums Suck Up Sensitive Audio in ‘LidarPhone’ Hack
Date: 2020-11-19T22:03:23+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;IoT;Web Security;academic research;ARM Cortex-M;Data Hack;Dustcloud software stack;Eavesdropping;Lidar;LidarPhone;Privacy;robot vacuum;vacuum cleaner;Xiaomi Roborock
Slug: robot-vacuums-suck-up-sensitive-audio-in-lidarphone-hack

[Source](https://threatpost.com/robot-vacuums-audio-lidarphone-hack/161421/){:target="_blank" rel="noopener"}

> Researchers have unveiled an attack that allows attackers to eavesdrop on homeowners inside their homes, through the LiDAR sensors on their robot vacuums. [...]
