Title: Brit Conservative Party used 10 million people's names to derive their country of origin, ethnicity and religion according to ICO report
Date: 2020-11-12T15:30:05+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: brit-conservative-party-used-10-million-peoples-names-to-derive-their-country-of-origin-ethnicity-and-religion-according-to-ico-report

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/12/uk_conservative_party_used_peoples/){:target="_blank" rel="noopener"}

> Bought 'estimated onomastic data' tagged onto data of millions of Brit voters The UK's ruling Conservative Party has been using personal data in a way that spots an individual's likely county of origin, ethnic origin and religion based on their first and last name.... [...]
