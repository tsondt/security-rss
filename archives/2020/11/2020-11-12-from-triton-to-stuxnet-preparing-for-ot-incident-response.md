Title: From Triton to Stuxnet: Preparing for OT Incident Response
Date: 2020-11-12T14:00:11+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Critical Infrastructure;Newsmaker Interviews;Podcasts;Web Security;coronavirus;COVID-19;Crash Override;critical infrastructure;dragos;factories;ICS;Incident response;Industrial;industrial control system;industrial malware;Industroyer;IR;lesley carhart;manufacturing security;operational technology;OT;podcast;Stuxnet;Trisis;Triton
Slug: from-triton-to-stuxnet-preparing-for-ot-incident-response

[Source](https://threatpost.com/triton-stuxnet-ot-incident-response/161147/){:target="_blank" rel="noopener"}

> Lesley Carhart, with Dragos, gives Threatpost a behind-the-scenes look at how industrial companies are faring during the COVID-19 pandemic - and how they can prepare for future threats. [...]
