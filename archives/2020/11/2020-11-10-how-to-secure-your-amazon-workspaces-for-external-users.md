Title: How to secure your Amazon WorkSpaces for external users
Date: 2020-11-10T23:52:13+00:00
Author: Olivia Carline
Category: AWS Security
Tags: Amazon WorkSpaces;Intermediate (200);Security, Identity, & Compliance;Security Blog
Slug: how-to-secure-your-amazon-workspaces-for-external-users

[Source](https://aws.amazon.com/blogs/security/how-to-secure-your-amazon-workspaces-for-external-users/){:target="_blank" rel="noopener"}

> In response to the current shift towards a remote workforce, companies are providing greater access to corporate applications from a range of different devices. Amazon WorkSpaces is a desktop-as-a-service solution that can be used to quickly deploy cloud-based desktops to your external users, including employees, third-party vendors, and consultants. Amazon WorkSpaces desktops are accessible from [...]
