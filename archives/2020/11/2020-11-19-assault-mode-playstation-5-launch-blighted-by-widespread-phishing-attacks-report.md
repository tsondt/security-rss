Title: Assault mode: PlayStation 5 launch blighted by widespread phishing attacks – report
Date: 2020-11-19T12:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: assault-mode-playstation-5-launch-blighted-by-widespread-phishing-attacks-report

[Source](https://portswigger.net/daily-swig/assault-mode-playstation-5-launch-blighted-by-widespread-phishing-attacks-report){:target="_blank" rel="noopener"}

> Console release is prime target for cybercriminals [...]
