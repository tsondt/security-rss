Title: 2021 Healthcare Cybersecurity Priorities: Experts Weigh In
Date: 2020-11-28T15:00:10+00:00
Author: Becky Bracken
Category: Threatpost
Tags: IoT;Webinars;breach;CSO;Healthcare;Hospital;Medical device security;MIoT;Ordr;Patching;ransomware;webinar;Windows XP
Slug: 2021-healthcare-cybersecurity-priorities-experts-weigh-in

[Source](https://threatpost.com/2021-healthcare-cybersecurity-priorities/161596/){:target="_blank" rel="noopener"}

> Hackers are putting a bullseye on healthcare. Experts explore why hospitals are being singled out and what any company can do to better protect themselves. [...]
