Title: Europol campaign urges e-commerce retailers to #SellSafe this Black Friday
Date: 2020-11-09T16:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: europol-campaign-urges-e-commerce-retailers-to-sellsafe-this-black-friday

[Source](https://portswigger.net/daily-swig/europol-campaign-urges-e-commerce-retailers-to-sellsafe-this-black-friday){:target="_blank" rel="noopener"}

> European webstores should act now to prevent cybercrime during discount bonanza, agency urges [...]
