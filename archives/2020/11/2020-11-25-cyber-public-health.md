Title: Cyber Public Health
Date: 2020-11-25T12:25:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: cybersecurity;infrastructure;threat models;video
Slug: cyber-public-health

[Source](https://www.schneier.com/blog/archives/2020/11/cyber-public-health.html){:target="_blank" rel="noopener"}

> In a lecture, Adam Shostack makes the case for a discipline of cyber public health. It would relate to cybersecurity in a similar way that public health relates to medicine. [...]
