Title: New RegretLocker ransomware targets Windows virtual machines
Date: 2020-11-03T17:31:19
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: new-regretlocker-ransomware-targets-windows-virtual-machines

[Source](https://www.bleepingcomputer.com/news/security/new-regretlocker-ransomware-targets-windows-virtual-machines/){:target="_blank" rel="noopener"}

> A new ransomware called RegretLocker uses a variety of advanced features that allows it to encrypt virtual hard drives and close open files for encryption. [...]
