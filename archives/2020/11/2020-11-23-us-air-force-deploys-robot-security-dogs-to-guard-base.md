Title: US Air Force deploys robot security dogs to guard base
Date: 2020-11-23T12:30:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: us-air-force-deploys-robot-security-dogs-to-guard-base

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/23/in_brief_security/){:target="_blank" rel="noopener"}

> Do they byte? In Brief Tyndall Air Force Base in Florida is now guarded by robotic canines that will patrol the area before popping back to their kennels for a recharge.... [...]
