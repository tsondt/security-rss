Title: Major BEC Phishing Ring Cracked Open with 3 Arrests
Date: 2020-11-25T17:05:37+00:00
Author: Tara Seals
Category: Threatpost
Tags: Government;Malware;Web Security;arrests;BEC;BEC attacks;Business Email Compromise;cybercriminals;global attacks;interpol;malware;nigeria;scam;social engineering
Slug: major-bec-phishing-ring-cracked-open-with-3-arrests

[Source](https://threatpost.com/bec-phishing-ring-3-arrests/161616/){:target="_blank" rel="noopener"}

> Some 50,000 targeted victims have been identified so far in a massive, global scam enterprise that involves 26 different malwares. [...]
