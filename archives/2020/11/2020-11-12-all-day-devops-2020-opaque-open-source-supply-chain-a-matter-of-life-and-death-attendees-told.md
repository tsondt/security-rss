Title: All Day DevOps 2020: Opaque open source supply chain a matter of life and death, attendees told
Date: 2020-11-12T16:25:55+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: all-day-devops-2020-opaque-open-source-supply-chain-a-matter-of-life-and-death-attendees-told

[Source](https://portswigger.net/daily-swig/all-day-devops-2020-opaque-open-source-supply-chain-a-matter-of-life-and-death-attendees-told){:target="_blank" rel="noopener"}

> An explosion of supply chain attacks is having potentially fatal real-world consequences [...]
