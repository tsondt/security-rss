Title: Cold storage giant Americold hit by cyberattack, services impacted
Date: 2020-11-16T19:02:10
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: cold-storage-giant-americold-hit-by-cyberattack-services-impacted

[Source](https://www.bleepingcomputer.com/news/security/cold-storage-giant-americold-hit-by-cyberattack-services-impacted/){:target="_blank" rel="noopener"}

> Cold storage giant Americold is currently dealing with a cyberattack impacting their operations, including phone systems, email, inventory management, and order fulfillment. [...]
