Title: Animal Jam kids' virtual world hit by data breach, impacts 46M accounts
Date: 2020-11-11T19:23:12
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Gaming;Software
Slug: animal-jam-kids-virtual-world-hit-by-data-breach-impacts-46m-accounts

[Source](https://www.bleepingcomputer.com/news/security/animal-jam-kids-virtual-world-hit-by-data-breach-impacts-46m-accounts/){:target="_blank" rel="noopener"}

> The immensely popular children's online playground Animal Jam has suffered a data breach impacting 46 million accounts. [...]
