Title: Enhancing our privacy commitments to customers
Date: 2020-11-12T17:00:00+00:00
Author: Royal Hansen
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: enhancing-our-privacy-commitments-to-customers

[Source](https://cloud.google.com/blog/products/identity-security/expanding-our-privacy-commitments-to-customers/){:target="_blank" rel="noopener"}

> Around the world, companies in every industry rely on our cloud services to run their businesses, and we take that responsibility seriously. That’s why we’re focused on providing industry-leading security and product capabilities, certifications, and commitments, along with transparency and visibility into when and how customer data is accessed. Today, we’re expanding on these commitments and sharing an update on our latest work in this area. Commitment to privacy Our Google Cloud Enterprise Privacy Commitments outline how we protect the privacy of customers whenever they use Google Workspace, G Suite for Education and Google Cloud Platform (GCP). There are two [...]
