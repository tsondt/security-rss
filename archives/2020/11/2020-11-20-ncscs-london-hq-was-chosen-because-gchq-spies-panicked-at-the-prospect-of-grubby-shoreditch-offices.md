Title: NCSC's London HQ was chosen because GCHQ spies panicked at the prospect of grubby Shoreditch offices
Date: 2020-11-20T11:29:13+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ncscs-london-hq-was-chosen-because-gchq-spies-panicked-at-the-prospect-of-grubby-shoreditch-offices

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/20/ncsc_nova_south_hq_isc_report/){:target="_blank" rel="noopener"}

> Tech hipsters? On our doorstep? The Silicon what? The National Cyber Security Centre picked its London HQ building not because it was the best or most cost-efficient location – but because the agency "prioritised image over cost", a Parliamentary committee has said.... [...]
