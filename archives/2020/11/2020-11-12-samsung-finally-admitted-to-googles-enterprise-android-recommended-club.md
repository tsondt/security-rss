Title: Samsung finally admitted to Google’s Enterprise Android Recommended club
Date: 2020-11-12T05:58:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: samsung-finally-admitted-to-googles-enterprise-android-recommended-club

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/12/samsung_joins_android_recommended_scheme/){:target="_blank" rel="noopener"}

> Knox and Google device enrolment now play nice together Samsung regularly tops Android handset sales charts and has arguably done more than any other handset-maker to make the OS. Yet the Korean company did not make the list at the launch of the Android Enterprise Recommended program, a scheme that Google created in early 2018 to point out which ‘Droids are ready to offer enterprise-grade services like remote management and swift security updates.... [...]
