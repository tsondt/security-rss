Title: Colossal Intel Update Anchored by Critical Privilege-Escalation Bugs
Date: 2020-11-10T20:59:04+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;Active Management Technology;AMT;CVE-2020-12321;CVE-2020-8752;Intel;intel wireless bluetooth;next unit computing;NUC;security advisory
Slug: colossal-intel-update-anchored-by-critical-privilege-escalation-bugs

[Source](https://threatpost.com/intel-update-critical-privilege-escalation-bugs/161087/){:target="_blank" rel="noopener"}

> Intel released 40 security advisories in total, addressing critical- and high-severity flaws across its Active Management Technology, Wireless Bluetooth and NUC products. [...]
