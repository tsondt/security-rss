Title: Computer Misuse Act: Most UK cybersecurity pros fear breaking the law by simply doing their jobs
Date: 2020-11-19T16:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: computer-misuse-act-most-uk-cybersecurity-pros-fear-breaking-the-law-by-simply-doing-their-jobs

[Source](https://portswigger.net/daily-swig/computer-misuse-act-most-uk-cybersecurity-pros-fear-breaking-the-law-by-simply-doing-their-jobs){:target="_blank" rel="noopener"}

> Obsolete provisions of aging UK legislation risk criminalizing pen testers [...]
