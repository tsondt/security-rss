Title: How's this for the ultimate gaming achievement? Half-Life 2's Gnome Chompski is going to space – in real life
Date: 2020-11-03T16:28:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: hows-this-for-the-ultimate-gaming-achievement-half-life-2s-gnome-chompski-is-going-to-space-in-real-life

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/03/looking_for_the_ultimate_gaming/){:target="_blank" rel="noopener"}

> Drag racing in orbit with Rocket Lab Rocket Lab is planning a 30-satellite launch via its 16th Electron launch from New Zealand. The payload will include a statue of Gnome Chompski*, presumably in an attempt to unlock hitherto unknown Half-Life achievements.... [...]
