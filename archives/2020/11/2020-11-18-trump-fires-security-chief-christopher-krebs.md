Title: Trump Fires Security Chief Christopher Krebs
Date: 2020-11-18T16:02:32+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Christopher Krebs;CISA;Cybersecurity and Infrastructure Security Agency;President Trump;Rumor Control;Sen. Angus King;Sen. Richard Burr;U.S. Department of Homeland Security;Y2K
Slug: trump-fires-security-chief-christopher-krebs

[Source](https://krebsonsecurity.com/2020/11/trump-fires-security-chief-christopher-krebs/){:target="_blank" rel="noopener"}

> President Trump on Tuesday fired his top election security official Christopher Krebs (no relation). The dismissal came via Twitter two weeks to the day after Trump lost an election he baselessly claims was stolen by widespread voting fraud. Chris Krebs. Image: CISA. Krebs, 43, is a former Microsoft executive appointed by Trump to head the Cybersecurity and Infrastructure Security Agency (CISA), a division of the U.S. Department of Homeland Security. As part of that role, Krebs organized federal and state efforts to improve election security, and to dispel disinformation about the integrity of the voting process. Krebs’ dismissal was hardly [...]
