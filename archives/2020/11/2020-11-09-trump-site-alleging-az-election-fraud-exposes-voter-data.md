Title: Trump Site Alleging AZ Election Fraud Exposes Voter Data
Date: 2020-11-09T20:49:40+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Privacy;Vulnerabilities;Web Security;APR;arizona;data breach;dontpushthegreenbutton;Maricopa county;phoenix;sql injection;Trump campaign;trump. voter fraud;voter data
Slug: trump-site-alleging-az-election-fraud-exposes-voter-data

[Source](https://threatpost.com/trump-site-alleging-az-election-fraud-exposes-voter-data/161068/){:target="_blank" rel="noopener"}

> Slapdash setup of Trump website collecting reports of Maricopa County in-person vote irregularities exposed 163,000 voter data records to fraud, via SQL injection. [...]
