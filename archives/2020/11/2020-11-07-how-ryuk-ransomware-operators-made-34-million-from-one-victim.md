Title: How Ryuk Ransomware operators made $34 million from one victim
Date: 2020-11-07T03:44:28
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: how-ryuk-ransomware-operators-made-34-million-from-one-victim

[Source](https://www.bleepingcomputer.com/news/security/how-ryuk-ransomware-operators-made-34-million-from-one-victim/){:target="_blank" rel="noopener"}

> One hacker group that is targeting high-revenue companies with Ryuk ransomware received $34 million from one victim in exchange for the decryption key that unlocked their computers. [...]
