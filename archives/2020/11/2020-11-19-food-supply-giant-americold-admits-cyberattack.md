Title: Food-Supply Giant Americold Admits Cyberattack
Date: 2020-11-19T16:56:24+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Malware;americold;cold storage;conagra;COVID-19 vaccine;cyberattack;food supply chain;operations;OT;ransomware;vaccine distribution;vaccine storage
Slug: food-supply-giant-americold-admits-cyberattack

[Source](https://threatpost.com/food-supply-americold-cyberattack/161402/){:target="_blank" rel="noopener"}

> A reported ransomware attack took down operations at the company, which in talks for COVID-19 vaccine-distribution contracts. [...]
