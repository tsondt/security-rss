Title: Keep it safe and simple:  Security enhancements in Google Meet, Gmail, and admin controls
Date: 2020-11-18T17:00:00+00:00
Author: Sam Lugani
Category: GCP Security
Tags: Google Workspace;Identity & Security
Slug: keep-it-safe-and-simple-security-enhancements-in-google-meet-gmail-and-admin-controls

[Source](https://cloud.google.com/blog/products/identity-security/google-workspace-security-updates-november-2020/){:target="_blank" rel="noopener"}

> Customers count on Google Workspace’s security to help protect collaboration and communication worldwide. That’s why we are constantly developing security innovations that provide our users, admins, and their organizations with a safer experience when using our products. And it’s this trust that has led us to a new milestone - today, more than 250 million 30-day active devices licensed through Google Workspace, Cloud Identity and Chrome Enterprise are managed by our endpoint management solution. Within Google Workspace and Cloud Identity, multiple devices per user can be managed at no additional cost. In further service of our customers’ security, today we’re [...]
