Title: “Privacy Nutrition Labels” in Apple’s App Store
Date: 2020-11-12T12:22:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: privacy-nutrition-labels-in-apples-app-store

[Source](https://www.schneier.com/blog/archives/2020/11/privacy-nutrition-labels-in-apples-app-store.html){:target="_blank" rel="noopener"}

> Apple will start requiring standardized privacy labels for apps in its app store, starting in December: Apple allows data disclosure to be optional if all of the following conditions apply: if it’s not used for tracking, advertising or marketing; if it’s not shared with a data broker; if collection is infrequent, unrelated to the app’s primary function, and optional; and if the user chooses to provide the data in conjunction with clear disclosure, the user’s name or account name is prominently displayed with the submission. Otherwise, the privacy labeling is mandatory and requires a fair amount of detail. Developers must [...]
