Title: Automation software slinger SaltStack warns of stop-watching-the-election-and-patch-now bugs
Date: 2020-11-04T02:45:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: automation-software-slinger-saltstack-warns-of-stop-watching-the-election-and-patch-now-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/04/saltstack_security/){:target="_blank" rel="noopener"}

> Fixes look to have landed in GitHub well ahead of disclosure SaltStack has officially revealed three bugs in its code – two of them seemingly critical – and told users: “We strongly recommend that you prioritize this update.” But the biz appears to have known about the bugs for months and quietly patched them over the summer.... [...]
