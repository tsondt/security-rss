Title: Nvidia Warns Windows Gamers of GeForce NOW Flaw
Date: 2020-11-11T19:03:15+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;chip level;CPU;CVE-2020-8694;CVE-2020-8695;CVE‑2020‑5992;flaw;Gaming;geforce;Geforce NOW;GPU;hardware;high severity flaw;Intel;Nvidia;Platypus;security vulnerability;Side-channel attack;Windows
Slug: nvidia-warns-windows-gamers-of-geforce-now-flaw

[Source](https://threatpost.com/nvidia-windows-gamers-geforce-now-flaw/161132/){:target="_blank" rel="noopener"}

> Both Nvidia and Intel faced severe security issues this week - including a high-severity bug in Nvidia's GeForce NOW. [...]
