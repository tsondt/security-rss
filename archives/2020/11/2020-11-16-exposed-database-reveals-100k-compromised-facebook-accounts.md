Title: Exposed Database Reveals 100K+ Compromised Facebook Accounts
Date: 2020-11-16T16:53:53+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Facebook;Hacks;Web Security;account takeover;Bitcoin;bitcoin scam;elastic search;exposed databased;facebook account;Fraud;scam;see who visits your profile
Slug: exposed-database-reveals-100k-compromised-facebook-accounts

[Source](https://threatpost.com/exposed-database-100k-facebook-accounts/161247/){:target="_blank" rel="noopener"}

> Cybercriminals left an ElasticSearch database exposed, revealing a global attack that compromised Facebook accounts and used them to scam others. [...]
