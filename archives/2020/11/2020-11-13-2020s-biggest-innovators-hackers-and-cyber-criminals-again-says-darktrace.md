Title: 2020's biggest innovators? Hackers and cyber-criminals, again, says Darktrace
Date: 2020-11-13T07:00:04+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2020s-biggest-innovators-hackers-and-cyber-criminals-again-says-darktrace

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/13/learn_think_like_an_attacker/){:target="_blank" rel="noopener"}

> Learn to think like an attacker so you can start fighting back Webcast This year has turned corporate IT upside down, scuppering digital transformation plans as tech teams struggle to keep the lights on and support a suddenly remote workforce.... [...]
