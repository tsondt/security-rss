Title: Animal Jam Hacked, 46M Records Roam the Dark Web
Date: 2020-11-12T21:33:28+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Breach;Hacks;Web Security;Animal Jam;breach;cyberattack;Dark Web;data breach;Gaming;gaming attacks;hack;hacker forum;kids' game;Minecraft;parent emails;Password;Phishing;ransomware;stolen data;user records;WildWorks
Slug: animal-jam-hacked-46m-records-roam-the-dark-web

[Source](https://threatpost.com/animal-jam-hack-data-breach/161177/){:target="_blank" rel="noopener"}

> Animal Jam, just the latest in a string of attacks on gaming apps, has adopted a transparent communications strategy after stolen data turned up on a criminal forum. [...]
