Title: Biotech research firm Miltenyi Biotec hit by ransomware, data leaked
Date: 2020-11-13T17:45:20
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: biotech-research-firm-miltenyi-biotec-hit-by-ransomware-data-leaked

[Source](https://www.bleepingcomputer.com/news/security/biotech-research-firm-miltenyi-biotec-hit-by-ransomware-data-leaked/){:target="_blank" rel="noopener"}

> Biomedical and clinical research company Miltenyi Biotec says that it has fully restored systems after a malware attack that took place last month and affected the firm's global IT infrastructure. [...]
