Title: Laptop mega-manufacturer Compal hit by DoppelPaymer ransomware – same one that hit German hospital
Date: 2020-11-09T23:35:00+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: laptop-mega-manufacturer-compal-hit-by-doppelpaymer-ransomware-same-one-that-hit-german-hospital

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/09/compal_ransomware_report/){:target="_blank" rel="noopener"}

> Crooks want $17m for decryption key Compal, the world’s second-largest white-label laptop manufacturer, has been hit by the file-scrambling DoppelPaymer ransomware gang – and the hackers want $17m in cryptocurrency before they'll hand over the decryption key.... [...]
