Title: Stick a fork in SGX, it's done: Intel's cloud-server security defeated by $30 chip and electrical shenanigans
Date: 2020-11-14T10:13:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: stick-a-fork-in-sgx-its-done-intels-cloud-server-security-defeated-by-30-chip-and-electrical-shenanigans

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/14/intel_sgx_protection_broken/){:target="_blank" rel="noopener"}

> VoltPillager breaks enclave confidentiality, calls anti-rogue data-center operator promise into question Boffins at the University of Birmingham in the UK have developed yet another way to compromise the confidentiality of Intel's Software Guard Extensions (SGX) secure enclaves, supposed "safe rooms" for sensitive computation.... [...]
