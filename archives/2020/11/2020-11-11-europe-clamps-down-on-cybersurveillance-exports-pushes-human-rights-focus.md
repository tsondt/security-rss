Title: Europe clamps down on cybersurveillance exports, pushes human rights focus
Date: 2020-11-11T07:29:13+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: europe-clamps-down-on-cybersurveillance-exports-pushes-human-rights-focus

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/11/eu_cybersurveillance_laws/){:target="_blank" rel="noopener"}

> No selling to evil folks albeit with a few big loopholes for some The European Union has tightened up export rules on cybersurveillance tools in an effort to limit their spread to repressive regimes.... [...]
