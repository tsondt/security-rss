Title: Insider threat: Corrupt Microsoft testing engineer jailed over $10m gift card scam
Date: 2020-11-10T14:09:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: insider-threat-corrupt-microsoft-testing-engineer-jailed-over-10m-gift-card-scam

[Source](https://portswigger.net/daily-swig/insider-threat-corrupt-microsoft-testing-engineer-jailed-over-10m-gift-card-scam){:target="_blank" rel="noopener"}

> Digital gift cards were sold online and laundered via bitcoin tumblers [...]
