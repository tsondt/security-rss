Title: Fake Microsoft Teams updates lead to Cobalt Strike deployment
Date: 2020-11-09T14:03:41
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: fake-microsoft-teams-updates-lead-to-cobalt-strike-deployment

[Source](https://www.bleepingcomputer.com/news/security/fake-microsoft-teams-updates-lead-to-cobalt-strike-deployment/){:target="_blank" rel="noopener"}

> Ransomware operators are using malicious fake ads for Microsoft Teams updates to infect systems with backdoors that deployed Cobalt Strike to compromise the rest of the network. [...]
