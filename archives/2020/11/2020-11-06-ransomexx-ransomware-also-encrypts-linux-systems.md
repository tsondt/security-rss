Title: RansomExx ransomware also encrypts Linux systems
Date: 2020-11-06T13:57:08
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: ransomexx-ransomware-also-encrypts-linux-systems

[Source](https://www.bleepingcomputer.com/news/security/ransomexx-ransomware-also-encrypts-linux-systems/){:target="_blank" rel="noopener"}

> With companies commonly using a mixed environment of Windows and Linux servers, ransomware operations have increasingly started to create Linux versions of their malware to ensure they encrypt all critical data. [...]
