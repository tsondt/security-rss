Title: Firefox 83 boosts security with HTTPS-Only mode, zero-day fix
Date: 2020-11-17T10:37:43
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: firefox-83-boosts-security-with-https-only-mode-zero-day-fix

[Source](https://www.bleepingcomputer.com/news/software/firefox-83-boosts-security-with-https-only-mode-zero-day-fix/){:target="_blank" rel="noopener"}

> Mozilla Firefox 83 was released today with a new feature called 'HTTPS-Only Mode' that secures your browsing sessions by rewriting URLs to secure HTTPS versions. [...]
