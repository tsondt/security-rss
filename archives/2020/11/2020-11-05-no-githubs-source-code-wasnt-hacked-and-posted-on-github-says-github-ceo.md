Title: No, GitHub's source code wasn't hacked and posted on GitHub, says GitHub CEO
Date: 2020-11-05T07:57:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: no-githubs-source-code-wasnt-hacked-and-posted-on-github-says-github-ceo

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/05/github_not_hacked_and_cloned/){:target="_blank" rel="noopener"}

> Nat Friedman says they'll make it harder to impersonate unsigned commits GitHub's CEO has denied that the site's source code was posted to GitHub.... [...]
