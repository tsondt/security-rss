Title: TikTok fixes bugs allowing account takeover with one click
Date: 2020-11-23T18:28:53
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: tiktok-fixes-bugs-allowing-account-takeover-with-one-click

[Source](https://www.bleepingcomputer.com/news/security/tiktok-fixes-bugs-allowing-account-takeover-with-one-click/){:target="_blank" rel="noopener"}

> TikTok has addressed two vulnerabilities that could have allowed attackers to take over accounts with a single click when chained together for users who signed-up via third-party apps. [...]
