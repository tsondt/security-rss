Title: Office 365 will let admins review Microsoft Forms phishing attempts
Date: 2020-11-07T12:45:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: office-365-will-let-admins-review-microsoft-forms-phishing-attempts

[Source](https://www.bleepingcomputer.com/news/security/office-365-will-let-admins-review-microsoft-forms-phishing-attempts/){:target="_blank" rel="noopener"}

> Microsoft is working on adding a new Microsoft Forms phishing attempt review feature that will allow Office 365 admins to confirm and block forms that try to maliciously harvest sensitive data. [...]
