Title: How to record a video of Amazon AppStream 2.0 streaming sessions
Date: 2020-11-13T18:21:03+00:00
Author: Nicolas Malaval
Category: AWS Security
Tags: Advanced (300);Amazon AppStream 2.0;Security, Identity, & Compliance;AppStream 2.0;FFmpeg;Screen recording;Security Blog;Session recording
Slug: how-to-record-a-video-of-amazon-appstream-20-streaming-sessions

[Source](https://aws.amazon.com/blogs/security/how-to-record-video-of-amazon-appstream-2-0-streaming-sessions/){:target="_blank" rel="noopener"}

> Amazon AppStream 2.0 is a fully managed service that lets you stream applications and desktops to your users. In this post, I’ll show you how to record a video of AppStream 2.0 streaming sessions by using FFmpeg, a popular media framework. There are many use cases for session recording, such as auditing administrative access, troubleshooting [...]
