Title: ‘Your connection is not private’ – One in three Android devices set to block Let’s Encrypt-certified websites in 2021
Date: 2020-11-10T16:20:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: your-connection-is-not-private-one-in-three-android-devices-set-to-block-lets-encrypt-certified-websites-in-2021

[Source](https://portswigger.net/daily-swig/your-connection-is-not-private-one-in-three-android-devices-set-to-block-lets-encrypt-certified-websites-in-2021){:target="_blank" rel="noopener"}

> Switching to Firefox will address issue related to unsupported OS [...]
