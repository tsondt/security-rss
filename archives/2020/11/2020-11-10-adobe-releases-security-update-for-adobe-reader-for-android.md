Title: Adobe releases security update for Adobe Reader for Android
Date: 2020-11-10T11:57:19
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: adobe-releases-security-update-for-adobe-reader-for-android

[Source](https://www.bleepingcomputer.com/news/security/adobe-releases-security-update-for-adobe-reader-for-android/){:target="_blank" rel="noopener"}

> Adobe has released security updates to address vulnerabilities classified as 'Important' in Adobe Reader for Android and Adobe Connect. [...]
