Title: Automatically update security groups for Amazon CloudFront IP ranges using AWS Lambda
Date: 2020-11-20T22:01:14+00:00
Author: Yeshwanth Kottu
Category: AWS Security
Tags: Amazon CloudFront;AWS Lambda;AWS WAF;Expert (400);Security, Identity, & Compliance;Amazon EC2;Amazon SNS;AWS IAM;Security Blog;Security groups
Slug: automatically-update-security-groups-for-amazon-cloudfront-ip-ranges-using-aws-lambda

[Source](https://aws.amazon.com/blogs/security/automatically-update-security-groups-for-amazon-cloudfront-ip-ranges-using-aws-lambda/){:target="_blank" rel="noopener"}

> Amazon CloudFront is a content delivery network that can help you increase the performance of your web applications and significantly lower the latency of delivering content to your customers. For CloudFront to access an origin (the source of the content behind CloudFront), the origin has to be publicly available and reachable. Anyone with the origin [...]
