Title: Oracle Rushes Emergency Fix for Critical WebLogic Server Flaw
Date: 2020-11-03T13:57:26+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Vulnerabilities;CVE-2020-14750;October 2020 critical patch update;Oracle;oracle flaw;oracle update;oracle web logic;Out of band patch;RCE;remote code execution;remote code execution flaw;weblogic server
Slug: oracle-rushes-emergency-fix-for-critical-weblogic-server-flaw

[Source](https://threatpost.com/oracle-update-weblogic-server-flaw/160889/){:target="_blank" rel="noopener"}

> The remote code-execution flaw (CVE-2020-14750) is low-complexity and requires no user interaction to exploit. [...]
