Title: Friday Squid Blogging: Ram’s Horn Squid Video
Date: 2020-11-20T22:13:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: squid;video
Slug: friday-squid-blogging-rams-horn-squid-video

[Source](https://www.schneier.com/blog/archives/2020/11/friday-squid-blogging-rams-horn-squid-video.html){:target="_blank" rel="noopener"}

> This is the first video footage of a ram’s horn squid ( Spirula spirula ). As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
