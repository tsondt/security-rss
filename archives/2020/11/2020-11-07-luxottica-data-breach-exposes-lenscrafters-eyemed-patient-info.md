Title: Luxottica data breach exposes LensCrafters, EyeMed patient info
Date: 2020-11-07T09:15:15
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: luxottica-data-breach-exposes-lenscrafters-eyemed-patient-info

[Source](https://www.bleepingcomputer.com/news/security/luxottica-data-breach-exposes-lenscrafters-eyemed-patient-info/){:target="_blank" rel="noopener"}

> A Luxottica data breach has exposed the personal and protected health information for patients of LensCrafters, Target Optical, EyeMed, and other eye care practices. [...]
