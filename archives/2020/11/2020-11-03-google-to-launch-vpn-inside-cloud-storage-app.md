Title: Google to launch VPN inside cloud storage app
Date: 2020-11-03T17:30:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: google-to-launch-vpn-inside-cloud-storage-app

[Source](https://portswigger.net/daily-swig/google-to-launch-vpn-inside-cloud-storage-app){:target="_blank" rel="noopener"}

> Internet giant will roll out privacy-enhancing tool for Google One users [...]
