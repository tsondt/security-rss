Title: How to Update Your Remote Access Policy – And Why You Should Now
Date: 2020-11-25T15:25:53+00:00
Author: Amit Bareket
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Vulnerabilities;Web Security;Amit Bareket;Cybersecurity;cybersecurity risks;infosec insider;password manager;perimeter 81;remote access policy;remote work;Security Audit;Single sign on;work from home;Zero Trust
Slug: how-to-update-your-remote-access-policy-and-why-you-should-now

[Source](https://threatpost.com/how-to-update-remote-access-policy/161589/){:target="_blank" rel="noopener"}

> Reducing the risks of remote work starts with updating the access policies of yesterday. [...]
