Title: This year’s biggest innovators? Hackers and cybercriminals. Again
Date: 2020-11-16T06:00:08+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: this-years-biggest-innovators-hackers-and-cybercriminals-again

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/16/learn_to_think_like_an_attacker/){:target="_blank" rel="noopener"}

> Learn to think like an attacker so you can start fighting back Webcast This year has turned corporate IT upside down, scuppering digital transformation plans as tech teams struggle to keep the lights on and support a suddenly remote workforce.... [...]
