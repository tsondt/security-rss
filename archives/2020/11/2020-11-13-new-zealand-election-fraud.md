Title: New Zealand Election Fraud
Date: 2020-11-13T12:25:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: new-zealand-election-fraud

[Source](https://www.schneier.com/blog/archives/2020/11/new-zealand-election-fraud.html){:target="_blank" rel="noopener"}

> It seems that this election season has not gone without fraud. In New Zealand, a vote for “Bird of the Year” has been marred by fraudulent votes : More than 1,500 fraudulent votes were cast in the early hours of Monday in the country’s annual bird election, briefly pushing the Little-Spotted Kiwi to the top of the leaderboard, organizers and environmental organization Forest & Bird announced Tuesday. Those votes — which were discovered by the election’s official scrutineers — have since been removed. According to election spokesperson Laura Keown, the votes were cast using fake email addresses that were all [...]
