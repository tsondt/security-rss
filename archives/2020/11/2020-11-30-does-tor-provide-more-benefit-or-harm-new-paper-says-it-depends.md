Title: Does Tor provide more benefit or harm? New paper says it depends
Date: 2020-11-30T23:00:45+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;anonymity;privacy;tor
Slug: does-tor-provide-more-benefit-or-harm-new-paper-says-it-depends

[Source](https://arstechnica.com/?p=1726503){:target="_blank" rel="noopener"}

> Enlarge (credit: Westend61 / Getty Images ) The Tor anonymity network has generated controversy almost constantly since its inception almost two decades ago. Supporters say it’s a vital service for protecting online privacy and circumventing censorship, particularly in countries with poor human rights records. Critics, meanwhile, argue that Tor shields criminals distributing child-abuse images, trafficking in illegal drugs, and engaging in other illicit activities. Researchers on Monday unveiled new estimates that attempt to measure the potential harms and benefits of Tor. They found that, worldwide, almost 7 percent of Tor users connect to hidden services, which the researchers contend are [...]
