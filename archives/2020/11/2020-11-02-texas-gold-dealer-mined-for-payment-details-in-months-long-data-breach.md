Title: Texas Gold-Dealer Mined for Payment Details in Months-Long Data Breach
Date: 2020-11-02T17:16:43+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Hacks;Vulnerabilities;Web Security;Code Injection;data breach;data breach notice;email notice;gold dealer;jm bullion;magecart;payment card skimmer;precious metals;Reddit;vulnerable website
Slug: texas-gold-dealer-mined-for-payment-details-in-months-long-data-breach

[Source](https://threatpost.com/texas-gold-dealer-payment-data-breach/160846/){:target="_blank" rel="noopener"}

> JM Bullion fell victim to a payment-card skimmer, which was in place for five months. [...]
