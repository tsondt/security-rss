Title: Criticalstudies.org sounds pretty important, right? Wrong: USA says it’s an Iranian fake news front
Date: 2020-11-05T06:57:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: criticalstudiesorg-sounds-pretty-important-right-wrong-usa-says-its-an-iranian-fake-news-front

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/05/usa_seizes_more_iran_linked_domains/){:target="_blank" rel="noopener"}

> So it's been seized, along with a bunch of other sites, with Cloudflare accounts providing critical evidence On US presidential election day, 3 November, the nation's Federal Bureau of Investigation acted to seize 27 domains it says Iran used to conduct disinformation campaigns.... [...]
