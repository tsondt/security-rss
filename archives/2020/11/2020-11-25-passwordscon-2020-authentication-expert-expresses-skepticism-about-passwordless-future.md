Title: PasswordsCon 2020: Authentication expert expresses skepticism about ‘passwordless’ future
Date: 2020-11-25T17:46:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: passwordscon-2020-authentication-expert-expresses-skepticism-about-passwordless-future

[Source](https://portswigger.net/daily-swig/passwordscon-2020-authentication-expert-expresses-skepticism-about-passwordless-future){:target="_blank" rel="noopener"}

> ‘Password + 2FA’ approach more likely to become preferred method of authentication, one expert argues [...]
