Title: The Week in Ransomware - November 13th 2020 - Extortion gone wild
Date: 2020-11-14T00:42:37
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: the-week-in-ransomware-november-13th-2020-extortion-gone-wild

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-13th-2020-extortion-gone-wild/){:target="_blank" rel="noopener"}

> There were not many known large ransomware attacks this week, but we have seen ransomware operations evolving their tactics to extort their victims further. [...]
