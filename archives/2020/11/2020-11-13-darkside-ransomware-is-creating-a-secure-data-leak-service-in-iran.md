Title: DarkSide ransomware is creating a secure data leak service in Iran
Date: 2020-11-13T03:00:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: darkside-ransomware-is-creating-a-secure-data-leak-service-in-iran

[Source](https://www.bleepingcomputer.com/news/security/darkside-ransomware-is-creating-a-secure-data-leak-service-in-iran/){:target="_blank" rel="noopener"}

> The DarkSide Ransomware operation claims they are creating a distributed storage system in Iran to store and leak data stolen from victims. To show they mean business, the ransomware gang has deposited $320 thousand on a hacker forum. [...]
