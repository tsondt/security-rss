Title: Australia’s tax office to invest in ‘selfie’ identification technology
Date: 2020-11-27T17:32:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: australias-tax-office-to-invest-in-selfie-identification-technology

[Source](https://portswigger.net/daily-swig/australias-tax-office-to-invest-in-selfie-identification-technology){:target="_blank" rel="noopener"}

> Authorities plan to introduce online identity checks using smartphones [...]
