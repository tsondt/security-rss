Title: VMware reveals critical hypervisor bugs found at Chinese white hat hacking comp. One lets guests run code on hosts
Date: 2020-11-20T04:26:56+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: vmware-reveals-critical-hypervisor-bugs-found-at-chinese-white-hat-hacking-comp-one-lets-guests-run-code-on-hosts

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/20/vmware_esxi_flaws/){:target="_blank" rel="noopener"}

> ESXi, Cloud Foundation, and desktop hypervisor users should get patching VMware has revealed and repaired the flaws in its hypervisor discovered at China’s Tianfu Cup white hat hacking competition.... [...]
