Title: Folksam data breach leaks info of 1M Swedes to Google, Facebook, more
Date: 2020-11-03T14:45:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: folksam-data-breach-leaks-info-of-1m-swedes-to-google-facebook-more

[Source](https://www.bleepingcomputer.com/news/security/folksam-data-breach-leaks-info-of-1m-swedes-to-google-facebook-more/){:target="_blank" rel="noopener"}

> Folksam, one of the largest insurance companies in Sweden, today disclosed a data breach affecting around 1 million Swedes after sharing customers' personal info with multiple technology giants. [...]
