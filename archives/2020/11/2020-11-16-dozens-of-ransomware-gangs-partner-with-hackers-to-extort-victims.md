Title: Dozens of ransomware gangs partner with hackers to extort victims
Date: 2020-11-16T14:36:33
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: dozens-of-ransomware-gangs-partner-with-hackers-to-extort-victims

[Source](https://www.bleepingcomputer.com/news/security/dozens-of-ransomware-gangs-partner-with-hackers-to-extort-victims/){:target="_blank" rel="noopener"}

> Ransomware-as-a-service (RaaS) crews are actively looking for affiliates to split profits obtained in outsourced ransomware attacks targeting high profile public and private organizations. [...]
