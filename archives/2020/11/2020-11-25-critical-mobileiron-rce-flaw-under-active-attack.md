Title: Critical MobileIron RCE Flaw Under Active Attack
Date: 2020-11-25T16:55:48+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;active attack;critical flaw;CVE-2020-1472;CVE-2020-15505;Exploit;healthcare security;Local Government Security;Microsoft Windows;mobileiron;Netlogon;RCE;remote code execution;zerologon
Slug: critical-mobileiron-rce-flaw-under-active-attack

[Source](https://threatpost.com/critical-mobileiron-rce-flaw-attack/161600/){:target="_blank" rel="noopener"}

> Attackers are targeting the critical remote code-execution flaw to compromise systems in the healthcare, local government, logistics and legal sectors, among others. [...]
