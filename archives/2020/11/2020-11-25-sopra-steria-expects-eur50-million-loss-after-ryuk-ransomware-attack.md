Title: Sopra Steria expects €50 million loss after Ryuk ransomware attack
Date: 2020-11-25T15:40:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: sopra-steria-expects-eur50-million-loss-after-ryuk-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/sopra-steria-expects-50-million-loss-after-ryuk-ransomware-attack/){:target="_blank" rel="noopener"}

> French IT services giant Sopra Steria said today in an official statement that the October Ryuk ransomware attack will lead to a loss of between €40 million and €50 million. [...]
