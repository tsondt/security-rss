Title: ‘Triggerless’ backdoors can infect machine learning models without leaving a trace – research
Date: 2020-11-02T12:48:04+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: triggerless-backdoors-can-infect-machine-learning-models-without-leaving-a-trace-research

[Source](https://portswigger.net/daily-swig/triggerless-backdoors-can-infect-machine-learning-models-without-leaving-a-trace-research){:target="_blank" rel="noopener"}

> New attack bypasses most of the current defense methods being used to protect deep learning systems [...]
