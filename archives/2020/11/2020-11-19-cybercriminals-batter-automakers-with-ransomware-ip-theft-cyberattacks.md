Title: Cybercriminals Batter Automakers With Ransomware, IP Theft Cyberattacks
Date: 2020-11-19T14:00:45+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Critical Infrastructure;Hacks;Malware;Podcasts;Automotive Security;Car Security;cyberattack;cybercriminal;Honda;insider threat;intsights;IP theft;paul proudhomme;Phishing;podcast;ransomware;Toyota;toyota australia
Slug: cybercriminals-batter-automakers-with-ransomware-ip-theft-cyberattacks

[Source](https://threatpost.com/cybercriminals-automakers-ransomware-ip-theft-cyberattacks/161362/){:target="_blank" rel="noopener"}

> While the industry focus is on vehicle hacking, when it comes to the automotive industry cybercriminals are opting for less complex and sophisticated attacks - from phishing to ransomware. [...]
