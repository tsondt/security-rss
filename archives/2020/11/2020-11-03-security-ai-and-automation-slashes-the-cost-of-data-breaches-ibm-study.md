Title: Security AI and automation slashes the cost of data breaches – IBM study
Date: 2020-11-03T16:45:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-ai-and-automation-slashes-the-cost-of-data-breaches-ibm-study

[Source](https://portswigger.net/daily-swig/security-ai-and-automation-slashes-the-cost-of-data-breaches-ibm-study){:target="_blank" rel="noopener"}

> Augmenting or replacing human intervention cut per-breach losses by $3.58m [...]
