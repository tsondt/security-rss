Title: Campari hit by Ragnar Locker Ransomware, $15 million demanded
Date: 2020-11-05T18:19:50
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: campari-hit-by-ragnar-locker-ransomware-15-million-demanded

[Source](https://www.bleepingcomputer.com/news/security/campari-hit-by-ragnar-locker-ransomware-15-million-demanded/){:target="_blank" rel="noopener"}

> Italian liquor company Campari Group was hit by a Ragnar Locker ransomware attack, where 2 TB of unencrypted files was allegedly stolen. To recover their files, Ragnar Locker is demanding $15 million. [...]
