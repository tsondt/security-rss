Title: US govt behind $1 billion Bitcoin transfer of Silk Road funds
Date: 2020-11-05T13:30:57
Author: Ax Sharma
Category: BleepingComputer
Tags: CryptoCurrency
Slug: us-govt-behind-1-billion-bitcoin-transfer-of-silk-road-funds

[Source](https://www.bleepingcomputer.com/news/security/us-govt-behind-1-billion-bitcoin-transfer-of-silk-road-funds/){:target="_blank" rel="noopener"}

> More details have emerged on the password-protected Bitcoin wallet which had been emptied out the night of the US Presidential Election. [...]
