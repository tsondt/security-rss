Title: Windows 7 and Server 2008 zero-day bug gets a free patch
Date: 2020-11-25T12:55:49
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: windows-7-and-server-2008-zero-day-bug-gets-a-free-patch

[Source](https://www.bleepingcomputer.com/news/security/windows-7-and-server-2008-zero-day-bug-gets-a-free-patch/){:target="_blank" rel="noopener"}

> An unpatched local privilege escalation (LPE) vulnerability affecting all Windows 7 and Server 2008 R2 devices received a free and temporary fix today through the 0patch platform. [...]
