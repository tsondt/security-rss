Title: Scalper-Bots Shake Down Desperate PS5, Xbox Series X Shoppers
Date: 2020-11-10T20:40:33+00:00
Author: Becky Bracken
Category: Threatpost
Tags: IoT;Web Security;bots;eBay;launch;markups;PlayStation PS5;pre-order PS5;pre-order Xbox;resellers;scalpers;Xbox Series X
Slug: scalper-bots-shake-down-desperate-ps5-xbox-series-x-shoppers

[Source](https://threatpost.com/scalper-bots-shake-down-desperate-ps5-xbox-series-x-shoppers/161090/){:target="_blank" rel="noopener"}

> Retail bots are helping scalpers scoop up PS5, Xbox Series X inventory and charge massive markups. [...]
