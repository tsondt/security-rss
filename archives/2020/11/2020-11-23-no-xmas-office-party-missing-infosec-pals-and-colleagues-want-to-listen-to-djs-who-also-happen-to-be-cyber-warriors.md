Title: No Xmas office party? Missing infosec pals and colleagues? Want to listen to DJs who also happen to be cyber warriors?
Date: 2020-11-23T10:15:11+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: no-xmas-office-party-missing-infosec-pals-and-colleagues-want-to-listen-to-djs-who-also-happen-to-be-cyber-warriors

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/23/cyber_house_party/){:target="_blank" rel="noopener"}

> Cyber House Party charity event scheduled for 17 December – just bring your dosh and (maybe) some ear plugs Locked up indoors with nothing to do as the evenings draw closer? If lighthearted chats about cyber security are your thing, followed up by some banging dance tunes, then we have just the event – all in the name of charity, of course.... [...]
