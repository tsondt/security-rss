Title: Security concerns doused as PayPal brings cryptocurrency to the masses
Date: 2020-11-10T15:10:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: security-concerns-doused-as-paypal-brings-cryptocurrency-to-the-masses

[Source](https://portswigger.net/daily-swig/security-concerns-doused-as-paypal-brings-cryptocurrency-to-the-masses){:target="_blank" rel="noopener"}

> Payment platform expanding to include Bitcoin, Ethereum, and other digital currencies [...]
