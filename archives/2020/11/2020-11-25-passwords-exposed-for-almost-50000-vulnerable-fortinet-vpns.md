Title: Passwords exposed for almost 50,000 vulnerable Fortinet VPNs
Date: 2020-11-25T08:16:46
Author: Ax Sharma
Category: BleepingComputer
Tags: Technology
Slug: passwords-exposed-for-almost-50000-vulnerable-fortinet-vpns

[Source](https://www.bleepingcomputer.com/news/security/passwords-exposed-for-almost-50-000-vulnerable-fortinet-vpns/){:target="_blank" rel="noopener"}

> A hacker has now leaked the credentials of almost 50,000 Fortinet SSL VPNs vulnerable to CVE-2018-13379. Exploits for these VPNs had been posted over the weekend on hacker forums, as reported by BleepingComputer. [...]
