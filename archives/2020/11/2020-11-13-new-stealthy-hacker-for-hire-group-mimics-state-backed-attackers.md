Title: New stealthy hacker-for-hire group mimics state-backed attackers
Date: 2020-11-13T12:24:15
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: new-stealthy-hacker-for-hire-group-mimics-state-backed-attackers

[Source](https://www.bleepingcomputer.com/news/security/new-stealthy-hacker-for-hire-group-mimics-state-backed-attackers/){:target="_blank" rel="noopener"}

> A new mercenary hacker group tracked as CostaRicto by BlackBerry researchers is selling its services to entities requiring APT-level hacking expertise in cyber-espionage campaigns spanning the globe and targeting a multitude of industry sectors. [...]
