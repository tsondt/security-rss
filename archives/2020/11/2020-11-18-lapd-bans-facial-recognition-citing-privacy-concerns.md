Title: LAPD Bans Facial Recognition, Citing Privacy Concerns
Date: 2020-11-18T19:56:11+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cloud Security;Government;Privacy;ban;biometric facial recognition;Biometrics;Clearview AI;LAPD;law enforcement;Lawsuit;Los Angeles;police department
Slug: lapd-bans-facial-recognition-citing-privacy-concerns

[Source](https://threatpost.com/lapd-facial-recognition-privacy-concerns/161364/){:target="_blank" rel="noopener"}

> The department has said no thanks to the Clearview AI platform, after an expose showing that officers had used it 475 times during a trial period alone. [...]
