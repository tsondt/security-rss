Title: China-linked hacking gang ‘APT10’ named as probable actor behind extended attacks on Japanese companies
Date: 2020-11-19T03:58:04+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: china-linked-hacking-gang-apt10-named-as-probable-actor-behind-extended-attacks-on-japanese-companies

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/19/apt_10_china_japan_attack/){:target="_blank" rel="noopener"}

> Campaign even targeted branch offices inside China and sought secrets of automotive and engineering companies Broadcom’s security subsidiary Symantec has named a China-linked hacking gang known as “APT 10” and “Cicada” as the probable source of a year-long attack on Japanese interests around the world.... [...]
