Title: I'll give you my passwords if you investigate police corruption, accused missile systems leaker told cops
Date: 2020-11-03T10:15:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: ill-give-you-my-passwords-if-you-investigate-police-corruption-accused-missile-systems-leaker-told-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/03/simon_finch_trial_missile_leaks_passwords/){:target="_blank" rel="noopener"}

> Ex-BAE Systems bod's letter read to Old Bailey A former BAE Systems engineer accused of failing to hand over his device passwords to Merseyside Police vowed not to give them up until a watchdog investigated his allegations that police workers had perverted the course of justice, the Old Bailey heard.... [...]
