Title: NVIDIA fixes severe flaw in GeForce NOW cloud gaming service
Date: 2020-11-11T11:40:56
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Software
Slug: nvidia-fixes-severe-flaw-in-geforce-now-cloud-gaming-service

[Source](https://www.bleepingcomputer.com/news/security/nvidia-fixes-severe-flaw-in-geforce-now-cloud-gaming-service/){:target="_blank" rel="noopener"}

> NVIDIA released a security update for the GeForce Now cloud gaming Windows app to address a vulnerability that could allow attackers to execute arbitrary code or escalate privileges on systems running unpatched software. [...]
