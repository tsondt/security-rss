Title: ‘Minecraft Mods’ Attack More Than 1 Million Android Devices
Date: 2020-11-24T20:04:19+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Malware;Mobile Security;Web Security;abusive ads;Advertising;Android malware;Darcy Minecraft Mod;Gaming;google play;Kaspersky;kids games;malicious mods;malware;Minecraft;Minecraft mod apps;Minecraft Modpacks;Minecraft Mods;mobile malware;Mods for Minecraft ACPE;scammers;Seeded for Minecraft ACPE;Textures for Minecraft ACPE;Zone Modding Minecraft
Slug: minecraft-mods-attack-more-than-1-million-android-devices

[Source](https://threatpost.com/minecraft-mods-attack-android-devices/161567/){:target="_blank" rel="noopener"}

> Fake Minecraft Modpacks on Google Play deliver millions of abusive ads and make normal phone use impossible. [...]
