Title: Ubuntu's Gnome desktop could be tricked into giving root access
Date: 2020-11-10T11:41:22
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: ubuntus-gnome-desktop-could-be-tricked-into-giving-root-access

[Source](https://www.bleepingcomputer.com/news/security/ubuntus-gnome-desktop-could-be-tricked-into-giving-root-access/){:target="_blank" rel="noopener"}

> A vulnerability in GNOME Display Manager (gdm) could allow a standard user to create accounts with increased privileges, giving a local attacker a path to run code with administrator permissions (root). [...]
