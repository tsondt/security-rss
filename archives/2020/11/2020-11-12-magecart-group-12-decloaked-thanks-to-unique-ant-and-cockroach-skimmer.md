Title: Magecart group 12 decloaked thanks to unique ‘Ant and Cockroach’ skimmer
Date: 2020-11-12T17:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: magecart-group-12-decloaked-thanks-to-unique-ant-and-cockroach-skimmer

[Source](https://portswigger.net/daily-swig/magecart-group-12-decloaked-thanks-to-unique-ant-and-cockroach-skimmer){:target="_blank" rel="noopener"}

> Cybercrime group thought to be behind spate of online attacks [...]
