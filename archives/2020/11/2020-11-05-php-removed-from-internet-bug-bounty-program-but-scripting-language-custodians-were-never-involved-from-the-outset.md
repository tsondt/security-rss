Title: PHP removed from Internet Bug Bounty program – but scripting language custodians were ‘never involved’ from the outset
Date: 2020-11-05T15:14:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: php-removed-from-internet-bug-bounty-program-but-scripting-language-custodians-were-never-involved-from-the-outset

[Source](https://portswigger.net/daily-swig/php-removed-from-internet-bug-bounty-program-but-scripting-language-custodians-were-never-involved-from-the-outset){:target="_blank" rel="noopener"}

> IBB panel will continue to reward hackers who have already submitted PHP vulnerability reports [...]
