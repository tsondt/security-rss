Title: Zoom Snooping: How Body Language Can Spill Your Password
Date: 2020-11-05T20:34:21+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Hacks;Privacy;blurring;Cisco WebEx;Hangouts;keystroke;keystroke inference;pixilation;Side-channel attack;skype;video conferencing;video security;zoom
Slug: zoom-snooping-how-body-language-can-spill-your-password

[Source](https://threatpost.com/zoom-snooping-passwords/161000/){:target="_blank" rel="noopener"}

> Researchers figure out how to read what people are typing during a Zoom call using shoulder movements. [...]
