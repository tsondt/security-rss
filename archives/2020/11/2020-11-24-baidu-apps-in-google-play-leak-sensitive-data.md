Title: Baidu Apps in Google Play Leak Sensitive Data
Date: 2020-11-24T17:36:36+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Privacy;Vulnerabilities;Web Security;Baidu;Data leakage;google play;IMEI;IMSI;intercept phone calls;mobile app;user tracking
Slug: baidu-apps-in-google-play-leak-sensitive-data

[Source](https://threatpost.com/baidu-apps-google-play-data/161556/){:target="_blank" rel="noopener"}

> Cyberattackers could use the information to track users across devices, disable phone service, or intercept messages and phone calls. [...]
