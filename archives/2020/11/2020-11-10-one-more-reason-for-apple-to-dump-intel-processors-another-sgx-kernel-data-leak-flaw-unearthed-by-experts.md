Title: One more reason for Apple to dump Intel processors: Another SGX, kernel data-leak flaw unearthed by experts
Date: 2020-11-10T18:00:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: one-more-reason-for-apple-to-dump-intel-processors-another-sgx-kernel-data-leak-flaw-unearthed-by-experts

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/10/intel_sgx_side_channel/){:target="_blank" rel="noopener"}

> Obscure interface lets you monitor chip activity with code as if you were physically plugged into it Updated Boffins based in Austria, Germany, and the UK have identified yet another data-leaking side-channel flaw affecting Intel processors, and potentially other chips, that exposes cryptographic secrets in memory.... [...]
