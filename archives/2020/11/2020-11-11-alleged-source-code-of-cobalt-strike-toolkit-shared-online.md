Title: Alleged source code of Cobalt Strike toolkit shared online
Date: 2020-11-11T16:05:20
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: alleged-source-code-of-cobalt-strike-toolkit-shared-online

[Source](https://www.bleepingcomputer.com/news/security/alleged-source-code-of-cobalt-strike-toolkit-shared-online/){:target="_blank" rel="noopener"}

> The source code for the widely-used Cobalt Strike post-exploitation toolkit has allegedly been leaked online in a GitHub repository. [...]
