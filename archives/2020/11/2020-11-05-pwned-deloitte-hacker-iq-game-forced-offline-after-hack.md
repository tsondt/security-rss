Title: Pwned: Deloitte Hacker IQ game forced offline after hack
Date: 2020-11-05T14:12:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: pwned-deloitte-hacker-iq-game-forced-offline-after-hack

[Source](https://portswigger.net/daily-swig/pwned-deloitte-hacker-iq-game-forced-offline-after-hack){:target="_blank" rel="noopener"}

> Consultancy firm’s cybersecurity quiz pulled after researcher exposed vulnerability [...]
