Title: Twitter hires veteran hacker Mudge as head of security
Date: 2020-11-17T13:16:53+00:00
Author: Alex Hern Technology editor
Category: The Guardian
Tags: Twitter;Hacking;Internet;Media;Technology;Data and computer security;US news;World news;Business;Social media;Digital media
Slug: twitter-hires-veteran-hacker-mudge-as-head-of-security

[Source](https://www.theguardian.com/technology/2020/nov/17/twitter-hires-veteran-hacker-mudge-as-head-of-security){:target="_blank" rel="noopener"}

> Peiter Zatko’s appointment follows mass attack on social media platform in July Twitter has appointed one of the world’s most respected hackers as its new head of security in the wake of a humiliating mass attack in July. The company has placed Peiter Zatko in charge of protecting its platform from threats of all varieties, poaching him from the payments startup Stripe. Zatko is better known as Mudge, his handle for more than 20 years of operation on both sides of the information security arena. Related: Why are public thinkers flocking to Substack? | Sean Monahan Continue reading... [...]
