Title: Cybercrime Moves to the Cloud to Accelerate Attacks Amid Data Glut
Date: 2020-11-16T13:00:59+00:00
Author: Tara Seals
Category: Threatpost
Tags: Breach;Cloud Security;Most Recent ThreatLists;Web Security;cloud acceleration;clouds of logs;Credential Theft;Cyberattacks;cybercrime;Dark Web;data breach;data cache;market;monetization;pay for access;stolen data;Trend Micro;underground economy
Slug: cybercrime-moves-to-the-cloud-to-accelerate-attacks-amid-data-glut

[Source](https://threatpost.com/cybercrime-cloud-accelerate-attacks-data-glut/161243/){:target="_blank" rel="noopener"}

> A report on the underground economy finds that malicious actors are offering cloud-based troves of stolen data, accessible with handy tools to slice and dice what's on offer. [...]
