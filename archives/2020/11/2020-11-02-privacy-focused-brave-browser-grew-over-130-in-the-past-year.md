Title: Privacy-focused Brave browser grew over 130% in the past year
Date: 2020-11-02T18:49:52
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software
Slug: privacy-focused-brave-browser-grew-over-130-in-the-past-year

[Source](https://www.bleepingcomputer.com/news/software/privacy-focused-brave-browser-grew-over-130-percent-in-the-past-year/){:target="_blank" rel="noopener"}

> Brave Browser, the privacy-focused web browser, announced today that it grew in usage by over 130% in its first year of the release of its 'Stable' version. [...]
