Title: Kids' gaming website Animal Jam breached after miscreants spot private AWS key on pwned Slack channel
Date: 2020-11-12T17:28:10+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: kids-gaming-website-animal-jam-breached-after-miscreants-spot-private-aws-key-on-pwned-slack-channel

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/12/animal_jam_breached/){:target="_blank" rel="noopener"}

> Tens of millions of usernames and passwords go walkies amid claims of decryption Child-friendly games website Animal Jam suffered a hack that exposed 46 million user records after a staff Slack channel was compromised by malicious people who discovered a private AWS key.... [...]
