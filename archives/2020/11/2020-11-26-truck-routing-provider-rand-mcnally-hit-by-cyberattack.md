Title: Truck routing provider Rand McNally hit by cyberattack
Date: 2020-11-26T13:45:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: truck-routing-provider-rand-mcnally-hit-by-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/truck-routing-provider-rand-mcnally-hit-by-cyberattack/){:target="_blank" rel="noopener"}

> Chicago-based transportation technology firm Rand McNally is working on restoring network functionality following a cyberattack that hit its systems earlier this week. [...]
