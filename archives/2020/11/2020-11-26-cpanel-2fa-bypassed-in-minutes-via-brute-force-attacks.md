Title: cPanel 2FA bypassed in minutes via brute-force attacks
Date: 2020-11-26T09:51:39
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: cpanel-2fa-bypassed-in-minutes-via-brute-force-attacks

[Source](https://www.bleepingcomputer.com/news/security/cpanel-2fa-bypassed-in-minutes-via-brute-force-attacks/){:target="_blank" rel="noopener"}

> A security flaw in the cPanel web hosting control panel allows attackers to circumvent two-factor authentication (2FA) checks via brute-force attacks for domains managed using vulnerable cPanel & WebHost Manager (WHM) versions. [...]
