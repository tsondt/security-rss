Title: Apple to Deliver ‘Privacy Labels’ for Apps, Revealing Data-Sharing Details
Date: 2020-11-10T17:12:05+00:00
Author: Tara Seals
Category: Threatpost
Tags: Mobile Security;Privacy;App Store;apple;apps;Data sharing;dec. 8;Developers;ios;macOS;mobile apps;privacy labels;privacy policies;third parties
Slug: apple-to-deliver-privacy-labels-for-apps-revealing-data-sharing-details

[Source](https://threatpost.com/apple-privacy-labels-apps-data-sharing/161081/){:target="_blank" rel="noopener"}

> Developers will have to reveal how data is shared with any “third-party partners,” which include analytics tools, advertising networks, third-party SDKs or other external vendors. [...]
