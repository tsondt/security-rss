Title: India PM calls on nation's youth to 'vaccinate digital products against cyber-attacks and viruses'
Date: 2020-11-20T05:15:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: india-pm-calls-on-nations-youth-to-vaccinate-digital-products-against-cyber-attacks-and-viruses

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/20/narendra_modi_it_policy_speech/){:target="_blank" rel="noopener"}

> And hints at new local data governance push Indian Prime Minister Narendra Modi has called on the nation's technology industry to start designing products for the world, and for youth to create new digital defences.... [...]
