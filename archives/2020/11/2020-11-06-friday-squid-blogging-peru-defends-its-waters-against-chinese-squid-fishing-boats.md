Title: Friday Squid Blogging: Peru Defends Its Waters against Chinese Squid Fishing Boats
Date: 2020-11-06T22:01:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: 
Slug: friday-squid-blogging-peru-defends-its-waters-against-chinese-squid-fishing-boats

[Source](https://www.schneier.com/blog/archives/2020/11/friday-squid-blogging-peru-defends-its-waters-against-chinese-squid-fishing-boats.html){:target="_blank" rel="noopener"}

> Squid geopolitics. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
