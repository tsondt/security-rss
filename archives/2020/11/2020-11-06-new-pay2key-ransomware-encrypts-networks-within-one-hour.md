Title: New Pay2Key ransomware encrypts networks within one hour
Date: 2020-11-06T14:32:06
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: new-pay2key-ransomware-encrypts-networks-within-one-hour

[Source](https://www.bleepingcomputer.com/news/security/new-pay2key-ransomware-encrypts-networks-within-one-hour/){:target="_blank" rel="noopener"}

> A new ransomware called Pay2Key has been targeting organizations from Israel and Brazil, encrypting their networks within an hour in targeted attacks still under investigation. [...]
