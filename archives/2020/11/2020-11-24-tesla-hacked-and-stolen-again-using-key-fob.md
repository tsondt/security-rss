Title: Tesla Hacked and Stolen Again Using Key Fob
Date: 2020-11-24T12:59:12+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Vulnerabilities;attack;Bluetooth Low Energy;COSIC;electric vehicles;hack;Hackers;key fob;keyless entry;Lennert Wouters;Tesla;Tesla Model S;Tesla model X;Wireless
Slug: tesla-hacked-and-stolen-again-using-key-fob

[Source](https://threatpost.com/tesla-hacked-stolen-key-fob/161530/){:target="_blank" rel="noopener"}

> Belgian researchers demonstrate third attack on the car manufacturer’s keyless entry system, this time to break into a Model X within minutes. [...]
