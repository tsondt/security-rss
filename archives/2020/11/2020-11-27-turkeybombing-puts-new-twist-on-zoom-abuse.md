Title: TurkeyBombing Puts New Twist on Zoom Abuse
Date: 2020-11-27T17:33:57+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: conference calls;COVID-19;Credential Theft;Hackers;online meetings;Phishing;Thanksgiving;TheAnalyst;threat actors;zoom;ZoomBombing
Slug: turkeybombing-puts-new-twist-on-zoom-abuse

[Source](https://threatpost.com/turkeybombing-zoom-abuse/161646/){:target="_blank" rel="noopener"}

> Threat actors already stole nearly 4,000 credentials before the holiday was even over, according to report. [...]
