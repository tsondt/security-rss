Title: Decloaked: Magecart group 12 tracked thanks to unique ‘Ant and Cockroach’&nbsp;skimmer
Date: 2020-11-12T17:31:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: decloaked-magecart-group-12-tracked-thanks-to-unique-ant-and-cockroach-skimmer

[Source](https://portswigger.net/daily-swig/decloaked-magecart-group-12-tracked-thanks-to-unique-ant-and-cockroach-nbsp-skimmer){:target="_blank" rel="noopener"}

> Cybercrime group thought to be behind spate of online attacks [...]
