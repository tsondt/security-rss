Title: WordPress Sites Open to Code Injection Attacks via Welcart e-Commerce Bug
Date: 2020-11-06T21:56:40+00:00
Author: Tara Seals
Category: Threatpost
Tags: Vulnerabilities;Web Security;Bug;Code Injection;Denial of Service;e-commerce;information disclosure;patch;PHP Object Injection;plugin;security vulnerability;welcart;WordFence;wordpress
Slug: wordpress-sites-open-to-code-injection-attacks-via-welcart-e-commerce-bug

[Source](https://threatpost.com/wordpress_open_to_attacks_welcart_bug/161037/){:target="_blank" rel="noopener"}

> The shopping cart application contains a PHP object-injection bug. [...]
