Title: Smart Doorbells on Amazon, eBay, Harbor Serious Security Issues
Date: 2020-11-24T17:46:45+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: IoT;Malware;Podcasts;amazon;Connected Device;data encryption;eBay;Password;smart doorbell;Victure
Slug: smart-doorbells-on-amazon-ebay-harbor-serious-security-issues

[Source](https://threatpost.com/smart-doorbells-on-amazon-ebay-harbor-serious-security-issues/161510/){:target="_blank" rel="noopener"}

> Matt Lewis, with NCC Group, talks to Threatpost about a slew of security and privacy issues found in smart doorbells that are being sold on Amazon and eBay. [...]
