Title: Investigate VPC flow with Amazon Detective
Date: 2020-11-16T23:22:06+00:00
Author: Ross Warren
Category: AWS Security
Tags: Advanced (300);Amazon Detective;Security, Identity, & Compliance;Logs;Monitoring;Network;Security Blog;VPC flow
Slug: investigate-vpc-flow-with-amazon-detective

[Source](https://aws.amazon.com/blogs/security/investigate-vpc-flow-with-amazon-detective/){:target="_blank" rel="noopener"}

> Many Amazon Web Services (AWS) customers need enhanced insight into IP network flow. Traditionally, cost, the complexity of collection, and the time required for analysis has led to incomplete investigations of network flows. Having good telemetry is paramount, and VPC Flow Logs are a very important part of a robust centralized logging architecture. The information [...]
