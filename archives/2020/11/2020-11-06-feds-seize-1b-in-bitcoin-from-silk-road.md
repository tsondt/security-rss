Title: Feds Seize $1B in Bitcoin from Silk Road
Date: 2020-11-06T19:55:46+00:00
Author: Tara Seals
Category: Threatpost
Tags: Cryptography;Government;Hacks;$1 billion;Bitcoin;FBI;Hacked;illegal funds;illegal marketplace;investigation;IRS;seized;Silk Road;stolen funds;takedown
Slug: feds-seize-1b-in-bitcoin-from-silk-road

[Source](https://threatpost.com/feds-seize-1b-bitcoin-silk-road/161027/){:target="_blank" rel="noopener"}

> The illegal marketplace was hacked prior to it's takedown -- the IRS has now tracked down those stolen funds, it said. [...]
