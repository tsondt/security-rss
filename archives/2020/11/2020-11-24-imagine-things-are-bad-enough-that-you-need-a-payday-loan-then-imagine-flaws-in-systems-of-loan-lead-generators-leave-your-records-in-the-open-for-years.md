Title: Imagine things are bad enough that you need a payday loan. Then imagine flaws in systems of loan lead generators leave your records in the open... for years
Date: 2020-11-24T10:28:13+00:00
Author: Danny Bradbury
Category: The Register
Tags: 
Slug: imagine-things-are-bad-enough-that-you-need-a-payday-loan-then-imagine-flaws-in-systems-of-loan-lead-generators-leave-your-records-in-the-open-for-years

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/24/payday_loan_lead_generators_fix/){:target="_blank" rel="noopener"}

> Leaky data systems fixed now, but the issue affected millions Feature Two separate internet affiliate networks have closed vulnerabilities that exposed potentially millions of records in one of the most sensitive areas: payday loans.... [...]
