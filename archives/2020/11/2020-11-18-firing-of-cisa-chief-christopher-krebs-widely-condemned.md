Title: Firing of CISA Chief Christopher Krebs Widely Condemned
Date: 2020-11-18T12:58:15+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Government;christopher krebs;CISA;Cybersecurity;Cybersecurity and Infrastructure Security Agency (CISA);Department of Homeland Security;Election;President Trump;White House
Slug: firing-of-cisa-chief-christopher-krebs-widely-condemned

[Source](https://threatpost.com/firing-of-krebs-condemned/161338/){:target="_blank" rel="noopener"}

> President Trump fired US cybersecurity chief over Twitter Tuesday, an act widely condemned within the cybersecurity community. [...]
