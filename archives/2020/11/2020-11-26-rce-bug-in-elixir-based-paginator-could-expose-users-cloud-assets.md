Title: RCE bug in Elixir-based Paginator could expose users’ cloud assets
Date: 2020-11-26T15:42:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: rce-bug-in-elixir-based-paginator-could-expose-users-cloud-assets

[Source](https://portswigger.net/daily-swig/rce-bug-in-elixir-based-paginator-could-expose-users-cloud-assets){:target="_blank" rel="noopener"}

> Developers patch vulnerability in open source project within 24 hours [...]
