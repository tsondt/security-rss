Title: US mental health provider admits email breach exposed patient data
Date: 2020-11-13T15:58:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: us-mental-health-provider-admits-email-breach-exposed-patient-data

[Source](https://portswigger.net/daily-swig/us-mental-health-provider-admits-email-breach-exposed-patient-data){:target="_blank" rel="noopener"}

> 27,500 affected after cybercriminals accessed employee email accounts [...]
