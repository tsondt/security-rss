Title: AWS includes open-source Suricata for stateful inspection with Network Firewall service
Date: 2020-11-19T19:10:10+00:00
Author: Tim Anderson
Category: The Register
Tags: 
Slug: aws-includes-open-source-suricata-for-stateful-inspection-with-network-firewall-service

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/19/aws_adopts_open_source_suricata/){:target="_blank" rel="noopener"}

> Enhanced network security for AWS virtual private cloud – while Microsoft previews Azure Firewall Premium AWS has announced Network Firewall, a new service drawing on the open-source Suricata project.... [...]
