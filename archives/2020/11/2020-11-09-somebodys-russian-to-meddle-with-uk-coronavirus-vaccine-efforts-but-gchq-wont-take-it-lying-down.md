Title: Somebody's Russian to meddle with UK coronavirus vaccine efforts, but GCHQ won't take it lying down
Date: 2020-11-09T15:52:07+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: somebodys-russian-to-meddle-with-uk-coronavirus-vaccine-efforts-but-gchq-wont-take-it-lying-down

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/09/gchq_hacks_russia_vaccine_disinfo/){:target="_blank" rel="noopener"}

> Offensive cyber operation includes 'encrypting' Vlad and Chums' disinfo servers British eavesdropping agency GCHQ is actively hacking Russian attempts to undermine coronavirus vaccine efforts, according to The Times.... [...]
