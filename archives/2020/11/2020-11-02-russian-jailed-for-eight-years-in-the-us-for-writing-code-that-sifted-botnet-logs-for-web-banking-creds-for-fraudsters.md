Title: Russian jailed for eight years in the US for writing code that sifted botnet logs for web banking creds for fraudsters
Date: 2020-11-02T23:58:12+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: russian-jailed-for-eight-years-in-the-us-for-writing-code-that-sifted-botnet-logs-for-web-banking-creds-for-fraudsters

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/02/botnet_brovko_jailed/){:target="_blank" rel="noopener"}

> Harvested usernames, passwords used to drain victims' coffers A Russian programmer has been sentenced to eight years behind bars in America for his part in a massive cybercriminal network that hacked into and drained victims' bank accounts.... [...]
