Title: Apple's global security boss accused of bribing cops with 200 free iPads in exchange for concealed gun permits
Date: 2020-11-23T21:17:06+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: apples-global-security-boss-accused-of-bribing-cops-with-200-free-ipads-in-exchange-for-concealed-gun-permits

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/23/apple_global_security_head_charged/){:target="_blank" rel="noopener"}

> Exec, two officers, one other charged Apple's head of global security tried to bung cops hundreds of free iPads in exchange for special gun permits, it is claimed.... [...]
