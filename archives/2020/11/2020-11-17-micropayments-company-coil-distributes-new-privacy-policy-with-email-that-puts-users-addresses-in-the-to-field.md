Title: Micropayments company Coil distributes new privacy policy with email that puts users' addresses in the ‘To:’ field
Date: 2020-11-17T04:58:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: micropayments-company-coil-distributes-new-privacy-policy-with-email-that-puts-users-addresses-in-the-to-field

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/17/coil_email_data_breach/){:target="_blank" rel="noopener"}

> Hundreds of email addresses exposed, customers predictably less-than-thrilled Micropayments company Coil has emailed users its new privacy policy but placed hundreds of their addresses in the “To:” field and therefore breached their privacy.... [...]
