Title: Ransomware Group Turns to Facebook Ads
Date: 2020-11-10T17:09:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;The Coming Storm;Chris Hodson;Emsisoft;Fabian Wosar;Ragnar Locker;ransomware
Slug: ransomware-group-turns-to-facebook-ads

[Source](https://krebsonsecurity.com/2020/11/ransomware-group-turns-to-facebook-ads/){:target="_blank" rel="noopener"}

> It’s bad enough that many ransomware gangs now have blogs where they publish data stolen from companies that refuse to make an extortion payment. Now, one crime group has started using hacked Facebook accounts to run ads publicly pressuring their ransomware victims into paying up. On the evening of Monday, Nov. 9, an ad campaign apparently taken out by the Ragnar Locker Team began appearing on Facebook. The ad was designed to turn the screws to the Italian beverage vendor Campari Group, which acknowledged on Nov. 3 that its computer systems had been sidelined by a malware attack. On Nov. [...]
