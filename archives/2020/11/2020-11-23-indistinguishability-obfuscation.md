Title: Indistinguishability Obfuscation
Date: 2020-11-23T12:04:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;cryptography;encryption
Slug: indistinguishability-obfuscation

[Source](https://www.schneier.com/blog/archives/2020/11/indistinguishability-obfuscation.html){:target="_blank" rel="noopener"}

> Quanta magazine recently published a breathless article on indistinguishability obfuscation — calling it the “‘crown jewel’ of cryptography” — and saying that it had finally been achieved, based on a recently published paper. I want to add some caveats to the discussion. Basically, obfuscation makes a computer program “unintelligible” by performing its functionality. Indistinguishability obfuscation is more relaxed. It just means that two different programs that perform the same functionality can’t be distinguished from each other. A good definition is in this paper. This is a pretty amazing theoretical result, and one to be excited about. We can now do [...]
