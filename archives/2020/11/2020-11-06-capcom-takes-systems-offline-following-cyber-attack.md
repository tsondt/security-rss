Title: Capcom takes systems offline following cyber-attack
Date: 2020-11-06T13:32:26+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: capcom-takes-systems-offline-following-cyber-attack

[Source](https://portswigger.net/daily-swig/capcom-takes-systems-offline-following-cyber-attack){:target="_blank" rel="noopener"}

> You lose! [...]
