Title: Botnet Attackers Turn to Vulnerable IoT Devices
Date: 2020-11-13T18:22:11+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Podcasts;Web Security;botnet;DDoS;Distributed Denial of Service;edge computing;Fortinet;Internet of things;IoT;TrickBot
Slug: botnet-attackers-turn-to-vulnerable-iot-devices

[Source](https://threatpost.com/botnet-attackers-turn-to-vulnerable-iot-devices/161210/){:target="_blank" rel="noopener"}

> Cybercriminals are leveraging the multitudes of vulnerable connected devices with botnets that launch dangerous distributed denial-of-service (DDoS) attacks. [...]
