Title: Cyberattackers Serve Up Custom Backdoor for Oracle Restaurant Software
Date: 2020-11-12T22:19:20+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;backdoor;credit card data;ESET;Hospitality;information stealer;Malware analysis;MICROS Restaurant Enterprise Series (RES) 3700 POS;modpipe;modular;Oracle;point of sale;pos;restaurants
Slug: cyberattackers-serve-up-custom-backdoor-for-oracle-restaurant-software

[Source](https://threatpost.com/cyberattackers-custom-backdoor-oracle-restaurant/161180/){:target="_blank" rel="noopener"}

> The modular malware is highly sophisticated but may not be able to capture credit-card info. [...]
