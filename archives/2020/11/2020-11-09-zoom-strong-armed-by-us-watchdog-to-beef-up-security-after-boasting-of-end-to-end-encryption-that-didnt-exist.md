Title: Zoom strong-armed by US watchdog to beef up security after boasting of end-to-end encryption that didn't exist
Date: 2020-11-09T21:03:32+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: zoom-strong-armed-by-us-watchdog-to-beef-up-security-after-boasting-of-end-to-end-encryption-that-didnt-exist

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/09/zoom_ftc_deal/){:target="_blank" rel="noopener"}

> Vid-chat giant promises never again to make 'misrepresentations about its privacy and security practices' Zoom has been forced to agree to a range of security improvements in a settlement with America's consumer watchdog, the Federal Trade Commission, as a result of earlier wrongly claiming it offered true 256-bit end-to-end encryption.... [...]
