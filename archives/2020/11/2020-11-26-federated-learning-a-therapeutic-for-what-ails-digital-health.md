Title: Federated Learning: A Therapeutic for what Ails Digital Health
Date: 2020-11-26T14:00:52+00:00
Author: Becky Bracken
Category: Threatpost
Tags: Cryptography;ai;artificial intelligence;brain tumor;Data Privacy;Federated Learning;federation;Healthcare data;machine learning;ml
Slug: federated-learning-a-therapeutic-for-what-ails-digital-health

[Source](https://threatpost.com/federated-learning-a-ails-digital-health/161633/){:target="_blank" rel="noopener"}

> Researchers show the promise of Federated Learning to protect patient privacy and improve healthcare outcomes across the world. [...]
