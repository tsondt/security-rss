Title: Microsoft brings its on-die Pluton security processor to Intel, AMD CPUs
Date: 2020-11-17T09:00:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft
Slug: microsoft-brings-its-on-die-pluton-security-processor-to-intel-amd-cpus

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-brings-its-on-die-pluton-security-processor-to-intel-amd-cpus/){:target="_blank" rel="noopener"}

> Microsoft is integrating its Pluton security processor directly into Intel, AMD, and Qualcomm CPUs to better secure Windows PCs. [...]
