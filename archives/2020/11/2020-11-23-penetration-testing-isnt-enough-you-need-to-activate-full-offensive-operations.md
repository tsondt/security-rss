Title: Penetration testing isn’t enough, you need to activate full offensive operations
Date: 2020-11-23T16:00:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: penetration-testing-isnt-enough-you-need-to-activate-full-offensive-operations

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/23/sans_institute_course_lineup/){:target="_blank" rel="noopener"}

> SANS Institute expands course lineup to help you think like a hacker Promo When it comes to cyber attacks, it’s not enough to just sit there, hoping miscreants will pass you by. You have to think like a hacker, stress testing your own systems to ensure they are as tight as possible.... [...]
