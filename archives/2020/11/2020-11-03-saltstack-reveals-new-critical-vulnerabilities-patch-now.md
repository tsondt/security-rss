Title: SaltStack reveals new critical vulnerabilities, patch now
Date: 2020-11-03T14:33:09
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: saltstack-reveals-new-critical-vulnerabilities-patch-now

[Source](https://www.bleepingcomputer.com/news/security/saltstack-reveals-new-critical-vulnerabilities-patch-now/){:target="_blank" rel="noopener"}

> SaltStack has released information on 3 vulnerabilities. 2 being critical, impacting Salt versions 3002 and prior. Users are encouraged to patch their Salt instances immediately. [...]
