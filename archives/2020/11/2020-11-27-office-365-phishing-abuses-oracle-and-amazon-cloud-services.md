Title: Office 365 phishing abuses Oracle and Amazon cloud services
Date: 2020-11-27T13:43:07
Author: Ionut Ilascu
Category: BleepingComputer
Tags: 
Slug: office-365-phishing-abuses-oracle-and-amazon-cloud-services

[Source](https://www.bleepingcomputer.com/news/security/office-365-phishing-abuses-oracle-and-amazon-cloud-services/){:target="_blank" rel="noopener"}

> A rather complex phishing scheme for stealing Office 365 credentials from small and medium-sized businesses in the U.S. and Australia combines cloud services from Oracle and Amazon into its infrastructure. [...]
