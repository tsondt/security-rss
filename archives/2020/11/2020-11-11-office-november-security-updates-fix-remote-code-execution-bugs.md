Title: Office November security updates fix remote code execution bugs
Date: 2020-11-11T13:10:15
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft
Slug: office-november-security-updates-fix-remote-code-execution-bugs

[Source](https://www.bleepingcomputer.com/news/security/office-november-security-updates-fix-remote-code-execution-bugs/){:target="_blank" rel="noopener"}

> Microsoft has released the November 2020 Office security updates with a total of 22 updates and 5 cumulative updates for 7 different products, fixing 14 vulnerabilities with five of them potentially enabling remote attackers to execute arbitrary code on vulnerable systems. [...]
