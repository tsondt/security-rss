Title: Toymaker Mattel Hit by Ransomware Attack
Date: 2020-11-04T16:37:30+00:00
Author: Tom Spring
Category: Threatpost
Tags: Hacks;Malware;cognizant;matell;ransomware;US Securities Exchange Commission
Slug: toymaker-mattel-hit-by-ransomware-attack

[Source](https://threatpost.com/mattel-hit-by-ransomware/160947/){:target="_blank" rel="noopener"}

> Financial disclosure filings describe a ransomware attack that delivered a weak punch. [...]
