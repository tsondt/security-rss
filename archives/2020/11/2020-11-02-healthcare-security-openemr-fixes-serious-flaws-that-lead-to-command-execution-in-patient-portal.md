Title: Healthcare security: OpenEMR fixes serious flaws that lead to command execution in patient portal
Date: 2020-11-02T16:34:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: healthcare-security-openemr-fixes-serious-flaws-that-lead-to-command-execution-in-patient-portal

[Source](https://portswigger.net/daily-swig/healthcare-security-openemr-fixes-serious-flaws-that-lead-to-command-execution-in-patient-portal){:target="_blank" rel="noopener"}

> Chained together, the flaws could imperil sensitive patient data and critical medical infrastructure [...]
