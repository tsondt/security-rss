Title: Trump lawsuit site to report rejected votes leaked voter data
Date: 2020-11-08T12:04:48
Author: Ax Sharma
Category: BleepingComputer
Tags: Government
Slug: trump-lawsuit-site-to-report-rejected-votes-leaked-voter-data

[Source](https://www.bleepingcomputer.com/news/security/trump-lawsuit-site-to-report-rejected-votes-leaked-voter-data/){:target="_blank" rel="noopener"}

> The DontTouchTheGreenButton website just launched by the Trump campaign in relation to the lawsuit filed on rejected votes in Arizona is leaking voter data. [...]
