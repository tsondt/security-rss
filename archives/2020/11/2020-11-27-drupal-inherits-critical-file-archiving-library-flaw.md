Title: Drupal inherits critical file archiving library flaw
Date: 2020-11-27T14:20:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: drupal-inherits-critical-file-archiving-library-flaw

[Source](https://portswigger.net/daily-swig/drupal-inherits-critical-file-archiving-library-flaw){:target="_blank" rel="noopener"}

> Patching needed to defend against code execution risk [...]
