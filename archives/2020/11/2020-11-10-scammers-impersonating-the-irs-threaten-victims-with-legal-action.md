Title: Scammers impersonating the IRS threaten victims with legal action
Date: 2020-11-10T11:22:31
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: scammers-impersonating-the-irs-threaten-victims-with-legal-action

[Source](https://www.bleepingcomputer.com/news/security/scammers-impersonating-the-irs-threaten-victims-with-legal-action/){:target="_blank" rel="noopener"}

> Aggressive scammers are impersonating the U.S. Internal Revenue Service (IRS) in spoofed e-mails designed to trick potential victims into paying fabricated outstanding amounts related to missed or late payments. [...]
