Title: Tim Berners-Lee asks everyone to do new biz a Solid and let him have another crack at fixing the Web's privacy
Date: 2020-11-10T07:55:10+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: tim-berners-lee-asks-everyone-to-do-new-biz-a-solid-and-let-him-have-another-crack-at-fixing-the-webs-privacy

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/10/inrupt_server_bernereslee/){:target="_blank" rel="noopener"}

> Lauches enterprise server trying to make 'pod' data a thing at scale Inventor of the world wide web, Tim Berners-Lee, is having another crack at fixing the internet’s biggest problems with the launch of a new enterprise server.... [...]
