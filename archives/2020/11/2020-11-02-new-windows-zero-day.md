Title: New Windows Zero-Day
Date: 2020-11-02T20:01:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: exploits;Windows;zero-day
Slug: new-windows-zero-day

[Source](https://www.schneier.com/blog/archives/2020/11/new-windows-zero-day.html){:target="_blank" rel="noopener"}

> Google’s Project Zero has discovered and published a buffer overflow vulnerability in the Windows Kernel Cryptography Driver. The exploit doesn’t affect the cryptography, but allows attackers to escalate system privileges: Attackers were combining an exploit for it with a separate one targeting a recently fixed flaw in Chrome. The former allowed the latter to escape a security sandbox so the latter could execute code on vulnerable machines. The vulnerability is being exploited in the wild, although Microsoft says it’s not being exploited widely. Everyone expects a fix in the next Patch Tuesday cycle. [...]
