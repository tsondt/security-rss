Title: Bugs in Critical Infrastructure Gear Allow Sophisticated Cyberattacks
Date: 2020-11-12T16:52:08+00:00
Author: Tara Seals
Category: Threatpost
Tags: Critical Infrastructure;Vulnerabilities;Critical infrastructure attacks;EcoStruxure Machine Expert;firmware;ICS;M221;operational technology;OT;PLCs;programmable logic controllers;Schneider Electric;security bypass;Security Vulnerabilities;TrustWave;utilities
Slug: bugs-in-critical-infrastructure-gear-allow-sophisticated-cyberattacks

[Source](https://threatpost.com/bugs-critical-infrastructure-gear-attacks/161164/){:target="_blank" rel="noopener"}

> Security problems in Schneider Electric programmable logic controllers allow compromise of the hardware, responsible for physical plant operations. [...]
