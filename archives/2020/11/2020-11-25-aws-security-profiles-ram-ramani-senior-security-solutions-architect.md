Title: AWS Security Profiles: Ram Ramani, Senior Security Solutions Architect
Date: 2020-11-25T18:39:44+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;AWS Security Profile;re:Invent 2020;Security Blog
Slug: aws-security-profiles-ram-ramani-senior-security-solutions-architect

[Source](https://aws.amazon.com/blogs/security/aws-security-profiles-ram-ramani-senior-security-solutions-architect/){:target="_blank" rel="noopener"}

> In the weeks leading up to re:Invent, we’ll share conversations we’ve had with people at AWS who will be presenting, and get a sneak peek at their work. How long have you been at AWS? I’ve been at AWS for 4 years. What’s your favorite part of your job? The ability to channel the technologist, [...]
