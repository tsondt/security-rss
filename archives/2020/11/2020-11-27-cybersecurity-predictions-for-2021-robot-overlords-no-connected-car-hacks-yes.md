Title: Cybersecurity Predictions for 2021: Robot Overlords No, Connected Car Hacks Yes
Date: 2020-11-27T14:00:32+00:00
Author: Tara Seals
Category: Threatpost
Tags: Bug Bounty;Cloud Security;Critical Infrastructure;Cryptography;InfoSec Insider;IoT;Malware;Vulnerabilities;Web Security;2021 predictions;connected cars;Cryptocurrency;Cybersecurity;cybersecurity defense;extended detection and response;gurucul;infosec insider;Internet of things;mfa;ransomware;ransomware evolution;risk-based access control;robot overlords;Saryu Nayyar;Single sign on;XDR;Zero Trust;zero-day
Slug: cybersecurity-predictions-for-2021-robot-overlords-no-connected-car-hacks-yes

[Source](https://threatpost.com/cybersecurity-predictions-2021-robot-overlords-connected-car/161594/){:target="_blank" rel="noopener"}

> While 2021 will present evolving threats and new challenges, it will also offer new tools and technologies that will we hope shift the balance towards the defense. [...]
