Title: Election security fears doused with reality: Top officials say Nov 3 'was the most secure in American history.' The end
Date: 2020-11-13T02:04:38+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: election-security-fears-doused-with-reality-top-officials-say-nov-3-was-the-most-secure-in-american-history-the-end

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/13/us_election_security_fears_doused/){:target="_blank" rel="noopener"}

> 'No evidence that any voting system deleted or lost votes, changed votes, or was in any way compromised' After months of fretting about the possibility that the 2020 US election might be derailed by tampering or foreign interference, nothing notable happened.... [...]
