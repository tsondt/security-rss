Title: GitHub breaks site layout after forgetting to renew certificate
Date: 2020-11-02T16:31:22
Author: Ax Sharma
Category: BleepingComputer
Tags: Software
Slug: github-breaks-site-layout-after-forgetting-to-renew-certificate

[Source](https://www.bleepingcomputer.com/news/security/github-breaks-site-layout-after-forgetting-to-renew-certificate/){:target="_blank" rel="noopener"}

> This morning, GitHub's pristine layout vanished in what looks like a miss on the company's part in renewing an SSL certificate. The expired certificate prevented numerous resources like images, JavaScript, and CSS stylesheets from loading correctly on GitHub. [...]
