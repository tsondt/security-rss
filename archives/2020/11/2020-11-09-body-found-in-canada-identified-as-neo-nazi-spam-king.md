Title: Body Found in Canada Identified as Neo-Nazi Spam King
Date: 2020-11-09T04:58:19+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;AOL;Brian McWilliams;CTV;Davis Wolfgang Hawke;Jesse James;Spam Kings
Slug: body-found-in-canada-identified-as-neo-nazi-spam-king

[Source](https://krebsonsecurity.com/2020/11/body-found-in-canada-identified-as-neo-nazi-spam-king/){:target="_blank" rel="noopener"}

> The body of a man found shot inside a burned out vehicle in Canada three years ago has been identified as that of Davis Wolfgang Hawke, a prolific spammer and neo-Nazi who led a failed anti-government march on Washington, D.C. in 1999, according to news reports. Homicide detectives said they originally thought the man found June 14, 2017 in a torched SUV on a logging road in Squamish, British Columbia was a local rock climber known to others in the area as a politically progressive vegan named Jesse James. Davis Wolfgang Hawke. Image: Spam Kings, by Brian McWilliams. But according [...]
