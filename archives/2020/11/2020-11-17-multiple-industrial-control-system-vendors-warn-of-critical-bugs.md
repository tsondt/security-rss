Title: Multiple Industrial Control System Vendors Warn of Critical Bugs
Date: 2020-11-17T22:38:27+00:00
Author: Tom Spring
Category: Threatpost
Tags: Critical Infrastructure;Vulnerabilities;Buffer Overflow;claroty;Denial of Service;EtherNet/IP;ICS;industrial control system;johnson controls;Real Time Automation;Schneider Electric;Sensormatic Electronics
Slug: multiple-industrial-control-system-vendors-warn-of-critical-bugs

[Source](https://threatpost.com/ics-vendors-warn-critical-bugs/161333/){:target="_blank" rel="noopener"}

> Four industrial control system vendors each announced vulnerabilities that ranged from critical to high-severity. [...]
