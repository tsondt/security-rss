Title: Code42 Incydr Series: Why Most Companies Can’t Stop Departing Employee Data Theft
Date: 2020-11-04T14:00:16+00:00
Author: Pat Cooper
Category: Threatpost
Tags: Web Security;data theft;departing employee risk;employee data theft;employee risk
Slug: code42-incydr-series-why-most-companies-cant-stop-departing-employee-data-theft

[Source](https://threatpost.com/code42-incydr-series-why-most-companies-cant-stop-departing-employee-data-theft/160879/){:target="_blank" rel="noopener"}

> According to Code42’s Data Exposure Report, 63% of employees say they brought data with them from their previous employer to their current employer. [...]
