Title: Minecraft Apps on Google Play Fleece Players Out of Big Money
Date: 2020-11-11T17:47:20+00:00
Author: Tara Seals
Category: Threatpost
Tags: Malware;Mobile Security;30;Android;Avast;Boys and Girls Skins;fleeceware;game mods;google play;Live Wallpapers HD & 3D Background;Maps for Minecraft PE;Maps Skins and Mods for Minecraft.;Master for Minecraft;MasterCraft for Minecraft;Minecraft;mobile apps;Mods;roblox;scam apps;skins;Skins for Roblox;wallpaper
Slug: minecraft-apps-on-google-play-fleece-players-out-of-big-money

[Source](https://threatpost.com/minecraft-apps-google-play-fleece-players/161125/){:target="_blank" rel="noopener"}

> Seven mobile apps for Android sneakily charge fans of Minecraft and Roblox hundreds of dollars per month. [...]
