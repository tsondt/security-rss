Title: Post-Breach, Peatix Data Reportedly Found on Instagram, Telegram
Date: 2020-11-24T21:02:58+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Breach;Hacks;Web Security;breach;Credential stuffing;data breach;Instagram;password spraying;peatix;phishing attack;stolen data;telegram;user account data
Slug: post-breach-peatix-data-reportedly-found-on-instagram-telegram

[Source](https://threatpost.com/breach-peatix-data-instagram-telegram/161560/){:target="_blank" rel="noopener"}

> Events application Peatix this week disclosed a data breach, after user account information reportedly began circulating on Instagram and Telegram. [...]
