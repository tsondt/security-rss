Title: Cyberattack on UVM Health Network Impedes Chemotherapy Appointments
Date: 2020-11-09T20:15:55+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Hacks;Vulnerabilities;Web Security;coronavirus;COVID-19;cyberattack;Healthcare;Hospital;malware;Ransomware Attack;UVM health network;UVM Medical Center
Slug: cyberattack-on-uvm-health-network-impedes-chemotherapy-appointments

[Source](https://threatpost.com/cyberattack-uvm-health-network/161059/){:target="_blank" rel="noopener"}

> The cyberattack has halted chemotherapy, mammogram and screening appointments, and led to 300 staff being furloughed or reassigned. [...]
