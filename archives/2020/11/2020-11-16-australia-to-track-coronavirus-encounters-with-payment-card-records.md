Title: Australia to track Coronavirus encounters with payment card records
Date: 2020-11-16T02:00:41+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: australia-to-track-coronavirus-encounters-with-payment-card-records

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/16/australia_contact_tracing_review_payment_recommendation/){:target="_blank" rel="noopener"}

> Plan calls to link government data across jurisdictions, even sharing airline records to track outbreaks and people who may be at risk of infection Australia will develop the capability to use payment records in the service of coronavirus contact tracing.... [...]
