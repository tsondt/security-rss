Title: Determining What Video Conference Participants Are Typing from Watching Shoulder Movements
Date: 2020-11-04T16:28:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: academic papers;side-channel attacks;video
Slug: determining-what-video-conference-participants-are-typing-from-watching-shoulder-movements

[Source](https://www.schneier.com/blog/archives/2020/11/determining-what-video-conference-participants-are-typing-from-watching-shoulder-movements.html){:target="_blank" rel="noopener"}

> Accuracy isn’t great, but that it can be done at all is impressive. Murtuza Jadiwala, a computer science professor heading the research project, said his team was able to identify the contents of texts by examining body movement of the participants. Specifically, they focused on the movement of their shoulders and arms to extrapolate the actions of their fingers as they typed. Given the widespread use of high-resolution web cams during conference calls, Jadiwala was able to record and analyze slight pixel shifts around users’ shoulders to determine if they were moving left or right, forward or backward. He then [...]
