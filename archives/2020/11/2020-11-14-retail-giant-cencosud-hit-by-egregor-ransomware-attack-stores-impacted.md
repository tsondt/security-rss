Title: Retail giant Cencosud hit by Egregor Ransomware attack, stores impacted
Date: 2020-11-14T19:07:38
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: retail-giant-cencosud-hit-by-egregor-ransomware-attack-stores-impacted

[Source](https://www.bleepingcomputer.com/news/security/retail-giant-cencosud-hit-by-egregor-ransomware-attack-stores-impacted/){:target="_blank" rel="noopener"}

> Chilean-based multinational retail company Cencosud has suffered a cyberattack by the Egregor ransomware operation that impacts services at stores. [...]
