Title: Pennsylvania county pays 500K ransom to DoppelPaymer ransomware
Date: 2020-11-29T14:21:31
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: pennsylvania-county-pays-500k-ransom-to-doppelpaymer-ransomware

[Source](https://www.bleepingcomputer.com/news/security/pennsylvania-county-pays-500k-ransom-to-doppelpaymer-ransomware/){:target="_blank" rel="noopener"}

> Delaware County, Pennsylvania has paid a $500,000 ransom after their systems were hit by the DoppelPaymer ransomware last weekend. [...]
