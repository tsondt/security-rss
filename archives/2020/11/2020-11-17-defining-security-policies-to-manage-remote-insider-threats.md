Title: Defining Security Policies to Manage Remote Insider Threats
Date: 2020-11-17T21:34:13+00:00
Author: Justin Jett
Category: Threatpost
Tags: Cloud Security;InfoSec Insider;Web Security;COVID-19;infosec insider;Insider threats;justin jett;plixar;remote insider threats;security policies;split tunnel vpn;stay at home orders;VPN;work from home
Slug: defining-security-policies-to-manage-remote-insider-threats

[Source](https://threatpost.com/defining-policies-manage-remote-insider-threats/161327/){:target="_blank" rel="noopener"}

> This is the time to define the new normal; having well-defined policies in place will help businesses maintain its security posture while bolstering the security of the ever-increasing work-from-home population. [...]
