Title: Joe Biden's 'Vote Joe' website defaced by Turkish Hackers
Date: 2020-11-21T09:22:18
Author: Ax Sharma
Category: BleepingComputer
Tags: Government
Slug: joe-bidens-vote-joe-website-defaced-by-turkish-hackers

[Source](https://www.bleepingcomputer.com/news/security/joe-bidens-vote-joe-website-defaced-by-turkish-hackers/){:target="_blank" rel="noopener"}

> The Vote Joe site set up by Biden Presidential campaign was hacked this week and defaced by a Turkish group called RootAyyıldız. The defacement appears to have lasted for over 24 hours. [...]
