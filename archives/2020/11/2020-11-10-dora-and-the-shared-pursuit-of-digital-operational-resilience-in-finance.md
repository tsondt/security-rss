Title: DORA and the shared pursuit of digital operational resilience in finance
Date: 2020-11-10T18:00:00+00:00
Author: Pablo Chavez
Category: GCP Security
Tags: Google Cloud Platform;Compliance;Financial Services;Identity & Security
Slug: dora-and-the-shared-pursuit-of-digital-operational-resilience-in-finance

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-and-digital-operational-resilience-for-the-financial-sector-dora/){:target="_blank" rel="noopener"}

> If you are a financial entity in the European Union (EU), the new draft regulation from the European Commission on Digital Operational Resilience for the Financial Sector (DORA) is likely top of mind. DORA aims to consolidate and upgrade existing Information and Communications Technology (ICT) risk management requirements, and is also introducing a new framework for direct oversight of critical ICT service providers by financial regulators in the EU. Where the criteria are met, this would apply to cloud service providers like Google Cloud. It’s important to know that DORA is still in draft and is going through the legislative [...]
