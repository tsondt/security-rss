Title: Apple iOS Safari feature can be used to share "fake news" headlines
Date: 2020-11-14T08:15:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Apple
Slug: apple-ios-safari-feature-can-be-used-to-share-fake-news-headlines

[Source](https://www.bleepingcomputer.com/news/security/apple-ios-safari-feature-can-be-used-to-share-fake-news-headlines/){:target="_blank" rel="noopener"}

> A link-sharing feature in iOS versions of Apple Safari browser makes it possible for iPhone, iPad, and iPod Touch users to alter headlines when sharing parts of webpages. A researcher has raised concerns this feature can be abused not only for pulling harmless pranks but for sharing "fake news" having a wider impact. [...]
