Title: Israeli spyware maker NSO channels Hollywood spy thrillers in appeal for legal immunity in WhatsApp battle
Date: 2020-11-17T23:22:25+00:00
Author: Kieren McCarthy
Category: The Register
Tags: 
Slug: israeli-spyware-maker-nso-channels-hollywood-spy-thrillers-in-appeal-for-legal-immunity-in-whatsapp-battle

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/17/israeli_hacking_group_goes_hollywood/){:target="_blank" rel="noopener"}

> In latest court bout, snooper biz seems to ask: Are you sure you want to open this can of worms? Israeli spyware maker NSO Group has taken a leaf out of Hollywood in an attempt to avoid any legal repercussions from making and selling tools that hack WhatsApp users' phones.... [...]
