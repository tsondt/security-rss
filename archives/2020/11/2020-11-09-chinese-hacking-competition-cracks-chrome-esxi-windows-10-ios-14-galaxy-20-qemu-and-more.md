Title: Chinese hacking competition cracks Chrome, ESXi, Windows 10, iOS 14, Galaxy 20, Qemu, and more
Date: 2020-11-09T07:11:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: chinese-hacking-competition-cracks-chrome-esxi-windows-10-ios-14-galaxy-20-qemu-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/09/tianfu_cup/){:target="_blank" rel="noopener"}

> VMware warns of incoming security fix after attackers get root on host VMware has taken the unusual step of warning about an imminent security advisory after a Chinese team successfully popped its flagship product.... [...]
