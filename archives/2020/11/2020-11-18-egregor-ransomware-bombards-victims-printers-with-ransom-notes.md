Title: Egregor ransomware bombards victims' printers with ransom notes
Date: 2020-11-18T17:25:04
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: egregor-ransomware-bombards-victims-printers-with-ransom-notes

[Source](https://www.bleepingcomputer.com/news/security/egregor-ransomware-bombards-victims-printers-with-ransom-notes/){:target="_blank" rel="noopener"}

> The Egregor ransomware uses a novel approach to get a victim's attention after an attack - shoot ransom notes from all available printers. [...]
