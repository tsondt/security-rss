Title: <i>Street Fighter</i> maker says soz after ransomware hadoukens servers, puts 350,000 folks' data at risk of theft
Date: 2020-11-16T15:30:09+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: street-fighter-maker-says-soz-after-ransomware-hadoukens-servers-puts-350000-folks-data-at-risk-of-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/16/capcom_ransomware_attack/){:target="_blank" rel="noopener"}

> Capcom KO'd by 'criminal organisation that calls itself Ragnar Locker' Japanese games giant Capcom, the company behind the 33-year-old Street Fighter franchise, has issued "deepest apologies" to customers and other stakeholders whose details may have been accessed by miscreants during a ransomware infection.... [...]
