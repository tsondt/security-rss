Title: Google Forms Abused to Phish AT&T Credentials
Date: 2020-11-04T21:48:22+00:00
Author: Lindsey O'Donnell
Category: Threatpost
Tags: Web Security;AT&T;google;Google Forms;Microsoft;Office 365;OneDrive;Outlook;phishing attack;T-Mobile;Wells Fargo
Slug: google-forms-abused-to-phish-att-credentials

[Source](https://threatpost.com/google-forms-abused-to-phish-att-credentials/160957/){:target="_blank" rel="noopener"}

> More than 200 Google Forms impersonate top brands - including Microsoft OneDrive, Office 365, and Wells Fargo - to steal victims' credentials. [...]
