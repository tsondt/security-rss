Title: Ransomware attack shutters Brazilian courts. But did attackers breach the virtual machine divide?
Date: 2020-11-06T04:31:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: ransomware-attack-shutters-brazilian-courts-but-did-attackers-breach-the-virtual-machine-divide

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/06/brazil_court_ransomware/){:target="_blank" rel="noopener"}

> Six-day outage predicted as rebuild commences from untouched backups Brazil’s Superior Tribunal de Justiça has temporarily shut down after a suspected ransomware attack.... [...]
