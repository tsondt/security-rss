Title: MasterChef, Big Brother producer hit by DoppelPaymer ransomware
Date: 2020-11-27T10:44:33
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: 
Slug: masterchef-big-brother-producer-hit-by-doppelpaymer-ransomware

[Source](https://www.bleepingcomputer.com/news/security/masterchef-big-brother-producer-hit-by-doppelpaymer-ransomware/){:target="_blank" rel="noopener"}

> French multinational production and distribution firm Banijay Group SAS was hit earlier this month by a DoppelPaymer ransomware attack and had sensitive information stolen by the ransomware operators during the incident. [...]
