Title: Mount Locker ransomware now targets your TurboTax tax returns
Date: 2020-11-19T16:09:18
Author: Lawrence Abrams
Category: BleepingComputer
Tags: 
Slug: mount-locker-ransomware-now-targets-your-turbotax-tax-returns

[Source](https://www.bleepingcomputer.com/news/security/mount-locker-ransomware-now-targets-your-turbotax-tax-returns/){:target="_blank" rel="noopener"}

> The Mount Locker ransomware operation is gearing up for the tax season by specifically targeting TurboTax returns for encryption. [...]
