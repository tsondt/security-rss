Title: None of our apps (except those 3) could secretly slurp Facebook user details, devs rage to High Court of England and Wales
Date: 2020-11-03T17:20:05+00:00
Author: Gareth Corfield
Category: The Register
Tags: 
Slug: none-of-our-apps-except-those-3-could-secretly-slurp-facebook-user-details-devs-rage-to-high-court-of-england-and-wales

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/03/facebook_mobiburn_oak_smart_high_court/){:target="_blank" rel="noopener"}

> Small Brit firm pushes back against Zuckerborg sueball Mobile app developers accused by Facebook of deploying “malicious” SDKs to scrape users’ data from the social network have hit back, telling London’s High Court that nearly all their apps were “not capable” of harvesting data from Facebook itself.... [...]
