Title: The ones who brought you Let's Encrypt, bring you: Tools for gathering anonymized app usage metrics from netizens
Date: 2020-11-18T14:00:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: the-ones-who-brought-you-lets-encrypt-bring-you-tools-for-gathering-anonymized-app-usage-metrics-from-netizens

[Source](https://go.theregister.com/feed/www.theregister.com/2020/11/18/isrg_prio_services/){:target="_blank" rel="noopener"}

> Tech tackles two things: 'Aggregate statistics to improve an application, maintain the privacy of the people' The Internet Security Research Group (ISRG) has a plan to allow companies to collect information about how people are using their products while protecting the privacy of those generating the data.... [...]
