Title: New Bytecode Alliance Brings the Security, Ubiquity, and Interoperability of the Web to the World of Pervasive Computing
Date: 2019-11-12T08:40:45+00:00
Author: Mozilla
Category: Mozilla Security
Tags: Press Releases;Bytecode Alliance;Fastly;Intel;mozilla;Red Hat;WebAssembly
Slug: new-bytecode-alliance-brings-the-security-ubiquity-and-interoperability-of-the-web-to-the-world-of-pervasive-computing

[Source](https://blog.mozilla.org/blog/2019/11/12/new-bytecode-alliance-brings-the-security-ubiquity-and-interoperability-of-the-web-to-the-world-of-pervasive-computing/){:target="_blank" rel="noopener"}

> New community effort will create a new cross-platform, cross-device computing runtime based on the unique advantages of WebAssembly MOUNTAIN VIEW, California, November 12, 2019 — The Bytecode Alliance is a... Read more The post New Bytecode Alliance Brings the Security, Ubiquity, and Interoperability of the Web to the World of Pervasive Computing appeared first on The Mozilla Blog. [...]
