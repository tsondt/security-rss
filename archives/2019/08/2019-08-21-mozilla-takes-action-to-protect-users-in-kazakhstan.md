Title: Mozilla takes action to protect users in Kazakhstan
Date: 2019-08-21T10:01:36+00:00
Author: Mozilla
Category: Mozilla Security
Tags: About Mozilla;General;Press Releases;Privacy
Slug: mozilla-takes-action-to-protect-users-in-kazakhstan

[Source](https://blog.mozilla.org/blog/2019/08/21/mozilla-takes-action-to-protect-users-in-kazakhstan/){:target="_blank" rel="noopener"}

> Today, Mozilla and Google took action to protect the online security and privacy of individuals in Kazakhstan. Together the companies deployed technical solutions within Firefox and Chrome to block the... Read more The post Mozilla takes action to protect users in Kazakhstan appeared first on The Mozilla Blog. [...]
