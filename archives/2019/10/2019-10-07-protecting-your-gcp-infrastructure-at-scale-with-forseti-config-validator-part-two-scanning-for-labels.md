Title: Protecting your GCP infrastructure at scale with Forseti Config Validator part two: Scanning for labels
Date: 2019-10-07T16:00:00+00:00
Author: Adrien Walkowiak
Category: GCP Security
Tags: Google Cloud Platform;Identity & Security
Slug: protecting-your-gcp-infrastructure-at-scale-with-forseti-config-validator-part-two-scanning-for-labels

[Source](https://cloud.google.com/blog/products/identity-security/protecting-your-gcp-infrastructure-at-scale-with-forseti-config-validator-part-two-scanning-for-labels/){:target="_blank" rel="noopener"}

> Welcome back to our series on best practices for managing and securing your Google Cloud infrastructure at scale. In a previous post, we talked about how to use the open-source tools Forseti and Config Validator to scan for non-compliant tools in your environment. Today, we’ll go one step further and show you another best practice for security operations: the systematic use of labels. Labels are created using the Cloud Resource Manager API. There are a lot of ways using labels can help you, but the most common use cases are security, operations and billing. We recommend using labels to add [...]
