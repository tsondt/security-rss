Title: Attacks on Citrix NetScaler systems linked to ransomware actor
Date: 2023-08-28T18:19:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-28-attacks-on-citrix-netscaler-systems-linked-to-ransomware-actor

[Source](https://www.bleepingcomputer.com/news/security/attacks-on-citrix-netscaler-systems-linked-to-ransomware-actor/){:target="_blank" rel="noopener"}

> A threat actor believed to be tied to the FIN8 hacking group exploits the CVE-2023-3519 remote code execution flaw to compromise unpatched Citrix NetScaler systems in domain-wide attacks. [...]
