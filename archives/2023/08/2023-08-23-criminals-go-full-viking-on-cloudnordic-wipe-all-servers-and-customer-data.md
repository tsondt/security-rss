Title: Criminals go full Viking on CloudNordic, wipe all servers and customer data
Date: 2023-08-23T07:26:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-23-criminals-go-full-viking-on-cloudnordic-wipe-all-servers-and-customer-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/23/ransomware_wipes_cloudnordic/){:target="_blank" rel="noopener"}

> IT outfit says it can't — and won't — pay the ransom demand CloudNordic has told customers to consider all of their data lost following a ransomware infection that encrypted the large Danish cloud provider's servers and "paralyzed CloudNordic completely," according to the IT outfit's online confession.... [...]
