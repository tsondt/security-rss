Title: New Collide+Power side-channel attack impacts almost all CPUs
Date: 2023-08-02T13:37:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-02-new-collidepower-side-channel-attack-impacts-almost-all-cpus

[Source](https://www.bleepingcomputer.com/news/security/new-collide-pluspower-side-channel-attack-impacts-almost-all-cpus/){:target="_blank" rel="noopener"}

> A new software-based power side-channel attack called 'Collide+Power' was discovered, impacting almost all CPUs and potentially allowing data to leak. However, the researchers warn that the flaw is low-risk and will likely not be used in attacks on end users. [...]
