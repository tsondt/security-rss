Title: North Korean hackers 'ScarCruft' breached Russian missile maker
Date: 2023-08-07T10:57:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-07-north-korean-hackers-scarcruft-breached-russian-missile-maker

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-scarcruft-breached-russian-missile-maker/){:target="_blank" rel="noopener"}

> The North Korean state-sponsored hacking group ScarCruft has been linked to a cyberattack on the IT infrastructure and email server for NPO Mashinostroyeniya, a Russian space rocket designer and intercontinental ballistic missile engineering organization. [...]
