Title: Spain warns of LockBit Locker ransomware phishing attacks
Date: 2023-08-28T14:25:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-28-spain-warns-of-lockbit-locker-ransomware-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/spain-warns-of-lockbit-locker-ransomware-phishing-attacks/){:target="_blank" rel="noopener"}

> The National Police of Spain is warning of an ongoing 'LockBit Locker' ransomware campaign targeting architecture companies in the country through phishing emails. [...]
