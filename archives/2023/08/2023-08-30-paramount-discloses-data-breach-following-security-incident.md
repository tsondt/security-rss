Title: Paramount discloses data breach following security incident
Date: 2023-08-30T19:08:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-30-paramount-discloses-data-breach-following-security-incident

[Source](https://www.bleepingcomputer.com/news/security/paramount-discloses-data-breach-following-security-incident/){:target="_blank" rel="noopener"}

> American entertainment giant Paramount Global disclosed a data breach after its systems got hacked and attackers gained access to personally identifiable information (PII). [...]
