Title: Couple admit they laundered $4B in stolen Bitcoins after Bitfinex super-heist
Date: 2023-08-04T01:11:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-08-04-couple-admit-they-laundered-4b-in-stolen-bitcoins-after-bitfinex-super-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/04/couple_bitfinex_hack_guilty/){:target="_blank" rel="noopener"}

> A man, a plan, and Razzlekhan fought the law – and the law won Ilya Lichtenstein and Heather Morgan on Thursday pleaded guilty to money-laundering charges related to the 2016 theft of some 120,000 Bitcoins from Hong Kong-based Bitfinex.... [...]
