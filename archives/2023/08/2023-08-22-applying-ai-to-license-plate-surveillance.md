Title: Applying AI to License Plate Surveillance
Date: 2023-08-22T11:04:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cars;privacy;surveillance
Slug: 2023-08-22-applying-ai-to-license-plate-surveillance

[Source](https://www.schneier.com/blog/archives/2023/08/applying-ai-to-license-plate-surveillance.html){:target="_blank" rel="noopener"}

> License plate scanners aren’t new. Neither is using them for bulk surveillance. What’s new is that AI is being used on the data, identifying “suspicious” vehicle behavior: Typically, Automatic License Plate Recognition (ALPR) technology is used to search for plates linked to specific crimes. But in this case it was used to examine the driving patterns of anyone passing one of Westchester County’s 480 cameras over a two-year period. Zayas’ lawyer Ben Gold contested the AI-gathered evidence against his client, decrying it as “dragnet surveillance.” And he had the data to back it up. A FOIA he filed with the [...]
