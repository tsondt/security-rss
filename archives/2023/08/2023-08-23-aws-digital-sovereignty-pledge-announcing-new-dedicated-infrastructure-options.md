Title: AWS Digital Sovereignty Pledge: Announcing new dedicated infrastructure options
Date: 2023-08-23T16:55:10+00:00
Author: Matt Garman
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Digital Sovereignty Pledge;Digital Sovereignty;Security Blog;Sovereign-by-design
Slug: 2023-08-23-aws-digital-sovereignty-pledge-announcing-new-dedicated-infrastructure-options

[Source](https://aws.amazon.com/blogs/security/aws-digital-sovereignty-pledge-announcing-new-dedicated-infrastructure-options/){:target="_blank" rel="noopener"}

> At AWS, we’re committed to helping our customers meet digital sovereignty requirements. Last year, I announced the AWS Digital Sovereignty Pledge, our commitment to offering all AWS customers the most advanced set of sovereignty controls and features available in the cloud. Our approach is to continue to make AWS sovereign-by-design—as it has been from day [...]
