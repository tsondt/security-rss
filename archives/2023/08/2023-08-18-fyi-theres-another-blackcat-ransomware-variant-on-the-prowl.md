Title: FYI: There's another BlackCat ransomware variant on the prowl
Date: 2023-08-18T21:33:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-18-fyi-theres-another-blackcat-ransomware-variant-on-the-prowl

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/18/microsoft_spots_new_blackcat_ransomware/){:target="_blank" rel="noopener"}

> Bad kitty, no catnip for you Here's a heads up. Another version of BlackCat ransomware has been spotted extorting victims. This variant embeds two tools, we're told: the network toolkit Impacket for lateral movement within compromised environments, and Remcom for remote code execution.... [...]
