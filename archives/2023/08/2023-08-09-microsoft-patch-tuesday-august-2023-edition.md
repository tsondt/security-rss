Title: Microsoft Patch Tuesday, August 2023 Edition
Date: 2023-08-09T02:22:57+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;adobe;CVE-2023-21709;CVE-2023-36884;CVE-2023-36910;CVE-2023-38180;Immersive Labs;microsoft;Microsoft Patch Tuesday August 2023;Nikolas Cemerikic;Satnam Narang;Tenable
Slug: 2023-08-09-microsoft-patch-tuesday-august-2023-edition

[Source](https://krebsonsecurity.com/2023/08/microsoft-patch-tuesday-august-2023-edition/){:target="_blank" rel="noopener"}

> Microsoft Corp. today issued software updates to plug more than 70 security holes in its Windows operating systems and related products, including multiple zero-day vulnerabilities currently being exploited in the wild. Six of the flaws fixed today earned Microsoft’s “critical” rating, meaning malware or miscreants could use them to install software on a vulnerable Windows system without any help from users. Last month, Microsoft acknowledged a series of zero-day vulnerabilities in a variety of Microsoft products that were discovered and exploited in-the-wild attacks. They were assigned a single placeholder designation of CVE-2023-36884. Satnam Narang, senior staff research engineer at Tenable, [...]
