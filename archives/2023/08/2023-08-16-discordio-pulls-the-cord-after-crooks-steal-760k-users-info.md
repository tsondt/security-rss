Title: Discord.io pulls the cord after crooks steal 760K users' info
Date: 2023-08-16T22:58:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-16-discordio-pulls-the-cord-after-crooks-steal-760k-users-info

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/16/discordio_shuts_down_security/){:target="_blank" rel="noopener"}

> Cleanup will involve 'complete rewrite of our website's code' Discord.io has shut down "for the foreseeable future," after crooks stole, and then put up for sale, data belonging to all 760,000 of the service's users.... [...]
