Title: LOLEKHosted admin arrested for aiding Netwalker ransomware gang
Date: 2023-08-11T11:59:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-11-lolekhosted-admin-arrested-for-aiding-netwalker-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/lolekhosted-admin-arrested-for-aiding-netwalker-ransomware-gang/){:target="_blank" rel="noopener"}

> Police have taken down the Lolek bulletproof hosting provider, arresting five individuals and seizing servers for allegedly facilitating Netwalker ransomware attacks and other malicious activities. [...]
