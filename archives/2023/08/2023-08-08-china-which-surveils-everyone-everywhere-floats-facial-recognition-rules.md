Title: China – which surveils everyone everywhere – floats facial recognition rules
Date: 2023-08-08T10:39:42+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-08-08-china-which-surveils-everyone-everywhere-floats-facial-recognition-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/08/china_facial_recognition_rules/){:target="_blank" rel="noopener"}

> Regulator says with a straight face that it should not be allowed to analyze ethnicity China has released draft regulations to govern the country's facial recognition technology that include prohibitions on its use to analyze race or ethnicity.... [...]
