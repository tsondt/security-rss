Title: You're not seeing double – yet another UK copshop is confessing to a data leak
Date: 2023-08-15T11:28:25+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-08-15-youre-not-seeing-double-yet-another-uk-copshop-is-confessing-to-a-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/15/norfolk_and_suffolk_police_data_breach/){:target="_blank" rel="noopener"}

> Norfolk and Suffolk constabularies admit to accidentally including raw crime data in FoI responses Norfolk and Suffolk police have stepped forward to admit that a “technical issue” resulted in raw data pertaining to crime reports accidentally being included in Freedom of Information responses.... [...]
