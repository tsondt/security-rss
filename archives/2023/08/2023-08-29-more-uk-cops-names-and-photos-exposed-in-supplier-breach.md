Title: More UK cops' names and photos exposed in supplier breach
Date: 2023-08-29T11:35:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-29-more-uk-cops-names-and-photos-exposed-in-supplier-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/29/met_police_data_breach/){:target="_blank" rel="noopener"}

> All 47,000 Met Police officers and staff reportedly accessed in break-in London's Metropolitan Police has said a third-party data breach exposed staff and officers' names, ranks, photos, vetting levels, and salary information.... [...]
