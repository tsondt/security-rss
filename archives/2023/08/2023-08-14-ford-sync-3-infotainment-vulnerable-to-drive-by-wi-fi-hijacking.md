Title: Ford SYNC 3 infotainment vulnerable to drive-by Wi-Fi hijacking
Date: 2023-08-14T20:48:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-08-14-ford-sync-3-infotainment-vulnerable-to-drive-by-wi-fi-hijacking

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/14/ford_sync_vulnerability/){:target="_blank" rel="noopener"}

> Don't panic, says automaker, but if you do, just turn off wireless for now Ford has suggested owners of vehicles equipped with its SYNC 3 infotainment system disable the Wi-Fi lest someone nearby exploits a buffer-overflow vulnerability and hijacks the equipment.... [...]
