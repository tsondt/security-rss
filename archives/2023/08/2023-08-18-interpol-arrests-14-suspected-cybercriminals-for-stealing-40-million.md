Title: Interpol arrests 14 suspected cybercriminals for stealing $40 million
Date: 2023-08-18T10:39:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-08-18-interpol-arrests-14-suspected-cybercriminals-for-stealing-40-million

[Source](https://www.bleepingcomputer.com/news/security/interpol-arrests-14-suspected-cybercriminals-for-stealing-40-million/){:target="_blank" rel="noopener"}

> An international law enforcement operation led by Interpol has led to the arrest of 14 suspected cybercriminals in an operation codenamed 'Africa Cyber Surge II,' launched in April 2023. [...]
