Title: Lazarus hackers deploy fake VMware PyPI packages in VMConnect attacks
Date: 2023-08-31T14:47:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-31-lazarus-hackers-deploy-fake-vmware-pypi-packages-in-vmconnect-attacks

[Source](https://www.bleepingcomputer.com/news/security/lazarus-hackers-deploy-fake-vmware-pypi-packages-in-vmconnect-attacks/){:target="_blank" rel="noopener"}

> North Korean state-sponsored hackers have uploaded malicious packages to the PyPI (Python Package Index) repository, camouflaging one of them as a VMware vSphere connector module named vConnector. [...]
