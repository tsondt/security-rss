Title: Retail chain Hot Topic discloses wave of credential-stuffing attacks
Date: 2023-08-01T11:02:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-01-retail-chain-hot-topic-discloses-wave-of-credential-stuffing-attacks

[Source](https://www.bleepingcomputer.com/news/security/retail-chain-hot-topic-discloses-wave-of-credential-stuffing-attacks/){:target="_blank" rel="noopener"}

> American apparel retailer Hot Topic is notifying customers about multiple cyberattacks between February 7 and June 21 that resulted in exposing sensitive information to hackers. [...]
