Title: MoustachedBouncer hackers use AiTM attacks to spy on diplomats
Date: 2023-08-10T12:56:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-10-moustachedbouncer-hackers-use-aitm-attacks-to-spy-on-diplomats

[Source](https://www.bleepingcomputer.com/news/security/moustachedbouncer-hackers-use-aitm-attacks-to-spy-on-diplomats/){:target="_blank" rel="noopener"}

> A cyberespionage group named 'MoustachedBouncer' has been observed using adversary-in-the-middle (AitM) attacks at ISPs to hack foreign embassies in Belarus. [...]
