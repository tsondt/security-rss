Title: Kroll data breach exposes info of FTX, BlockFi, Genesis creditors
Date: 2023-08-25T10:10:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-08-25-kroll-data-breach-exposes-info-of-ftx-blockfi-genesis-creditors

[Source](https://www.bleepingcomputer.com/news/security/kroll-data-breach-exposes-info-of-ftx-blockfi-genesis-creditors/){:target="_blank" rel="noopener"}

> Multiple reports on social media warn of a data breach at financial and risk advisory company Kroll that resulted in exposing to an unauthorized third-party the personal data of some credit claimants. [...]
