Title: Microsoft finds vulnerabilities it says could be used to shut down power plants
Date: 2023-08-11T20:42:49+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;codesys;critical infrastructure;industrial control systems;microsoft
Slug: 2023-08-11-microsoft-finds-vulnerabilities-it-says-could-be-used-to-shut-down-power-plants

[Source](https://arstechnica.com/?p=1960538){:target="_blank" rel="noopener"}

> Enlarge (credit: Rockwell Automation) On Friday, Microsoft disclosed 15 high-severity vulnerabilities in a widely used collection of tools used to program operational devices inside industrial facilities such as plants for power generation, factory automation, energy automation, and process automation. The company warned that while exploiting the code-execution and denial-of-service vulnerabilities was difficult, it enabled threat actors to “inflict great damage on targets." The vulnerabilities affect the CODESYS V3 software development kit. Developers inside companies such as Schneider Electric and WAGO use the platform-independent tools to develop programmable logic controllers, the toaster-sized devices that open and close valves, turn rotors, and [...]
