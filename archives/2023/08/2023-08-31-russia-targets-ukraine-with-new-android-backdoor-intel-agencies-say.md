Title: Russia targets Ukraine with new Android backdoor, intel agencies say
Date: 2023-08-31T19:52:37+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;android;malware;russia;Ukraine
Slug: 2023-08-31-russia-targets-ukraine-with-new-android-backdoor-intel-agencies-say

[Source](https://arstechnica.com/?p=1964854){:target="_blank" rel="noopener"}

> Enlarge / Ukrainian soldiers. (credit: Getty Images) Russia’s military intelligence unit has been targeting Ukrainian Android devices with “Infamous Chisel,” the tracking name for new malware that’s designed to backdoor devices and steal critical information, Western intelligence agencies said on Thursday. “Infamous Chisel is a collection of components which enable persistent access to an infected Android device over the Tor network, and which periodically collates and exfiltrates victim information from compromised devices,” intelligence officials from the UK, US, Canada, Australia, and New Zealand wrote. “The information exfiltrated is a combination of system device information, commercial application information and applications specific [...]
