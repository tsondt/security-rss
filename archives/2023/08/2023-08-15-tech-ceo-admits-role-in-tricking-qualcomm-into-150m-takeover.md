Title: Tech CEO admits role in tricking Qualcomm into $150M takeover
Date: 2023-08-15T10:27:11+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-08-15-tech-ceo-admits-role-in-tricking-qualcomm-into-150m-takeover

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/15/abreezio_ceo_qualcomm_deal/){:target="_blank" rel="noopener"}

> Abreezio? Maybe not, but it was a plea deal The former chief executive of a company that was sold to Qualcomm for more than $150 million has pleaded guilty to one count of money laundering relating to a $1.5 million transaction involving proceeds from the deal.... [...]
