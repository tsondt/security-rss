Title: PSNI apologies to police officers after unprecedented data breach – video
Date: 2023-08-09T17:57:54+00:00
Author: 
Category: The Guardian
Tags: Northern Ireland;Freedom of information;Data and computer security;UK news;World news
Slug: 2023-08-09-psni-apologies-to-police-officers-after-unprecedented-data-breach-video

[Source](https://www.theguardian.com/uk-news/video/2023/aug/09/psni-apologies-to-police-officers-after-unprecedented-data-breach-video){:target="_blank" rel="noopener"}

> The UK Information Commissioner’s Office has launched an investigation into an unprecedented data breach that disclosed details of more than 10,000 police officers and staff in Northern Ireland. The agency, which regulates data privacy laws, is working with the Police Service of Northern Ireland to establish the level of risk amid warnings that the leak may compel officers to leave the force or move house. The PSNI blamed human error for the release of an Excel spreadsheet that was published on an FoI website and removed as soon as police discovered the mistake. Chris Todd, a PSNI assistant chief constable, [...]
