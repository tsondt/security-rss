Title: Prepare for plenty more pain from Ivanti's MDM flaws, warn cyber agencies
Date: 2023-08-03T07:38:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-03-prepare-for-plenty-more-pain-from-ivantis-mdm-flaws-warn-cyber-agencies

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/03/ivanti_cisa_norway_attack/){:target="_blank" rel="noopener"}

> Invaders already spent four or more months frolicking inside Norwegian government servers Intruders who exploited a critical Ivanti bug to compromise 12 Norwegian government agencies spent at least four months looking around the organizations' systems and stealing data before the intrusion was discovered and stopped.... [...]
