Title: New AI capabilities that can help address your security challenges
Date: 2023-08-29T12:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Google Cloud Next;Security & Identity
Slug: 2023-08-29-new-ai-capabilities-that-can-help-address-your-security-challenges

[Source](https://cloud.google.com/blog/products/identity-security/security-ai-next23/){:target="_blank" rel="noopener"}

> At Google Cloud, we continue to address pervasive and fundamental security challenges: the exponential growth in threats, the toil it takes for security teams to achieve desired outcomes, and the chronic shortage of security talent. At Google Cloud Next, we are leaning in to help solve these challenges by supercharging security with Duet AI, as well as bringing innovation and enhancements across our security operations and cloud platforms. Addressing top security challenges with AI We are taking a holistic approach to both securing AI as well as infusing AI to enhance security products. We start with posture, governance, and compliance [...]
