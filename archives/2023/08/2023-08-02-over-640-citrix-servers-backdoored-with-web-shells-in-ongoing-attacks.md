Title: Over 640 Citrix servers backdoored with web shells in ongoing attacks
Date: 2023-08-02T14:23:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-02-over-640-citrix-servers-backdoored-with-web-shells-in-ongoing-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-640-citrix-servers-backdoored-with-web-shells-in-ongoing-attacks/){:target="_blank" rel="noopener"}

> Hundreds of Citrix Netscaler ADC and Gateway servers have already been breached and backdoored in a series of attacks targeting a critical remote code execution (RCE) vulnerability tracked as CVE-2023-3519. [...]
