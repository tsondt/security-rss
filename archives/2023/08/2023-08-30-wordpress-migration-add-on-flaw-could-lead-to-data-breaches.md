Title: WordPress migration add-on flaw could lead to data breaches
Date: 2023-08-30T14:37:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-30-wordpress-migration-add-on-flaw-could-lead-to-data-breaches

[Source](https://www.bleepingcomputer.com/news/security/wordpress-migration-add-on-flaw-could-lead-to-data-breaches/){:target="_blank" rel="noopener"}

> All-in-One WP Migration, a popular data migration plugin for WordPress sites that has 5 million active installations, suffers from unauthenticated access token manipulation that could allow attackers to access sensitive site information. [...]
