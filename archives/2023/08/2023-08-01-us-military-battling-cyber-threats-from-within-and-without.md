Title: US military battling cyber threats from within and without
Date: 2023-08-01T07:29:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-01-us-military-battling-cyber-threats-from-within-and-without

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/01/us_military_cybersecurity/){:target="_blank" rel="noopener"}

> As if attacks from China weren't enough, one of the Air Force's own has reportedly gone rogue The US government is fighting a pair of cyber security incidents, one involving Chinese spies who potentially gained access to crucial American computer networks and the other related to an Air Force engineer allegedly compromised communications security by stealing sensitive equipment and taking it home.... [...]
