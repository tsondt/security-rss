Title: Dissident republicans obtained leaked police data, says PSNI chief
Date: 2023-08-14T18:20:20+00:00
Author: Rory Carroll Ireland correspondent
Category: The Guardian
Tags: Northern Ireland;Police;Sinn Féin;UK news;Data and computer security
Slug: 2023-08-14-dissident-republicans-obtained-leaked-police-data-says-psni-chief

[Source](https://www.theguardian.com/uk-news/2023/aug/14/document-psni-data-leak-belfast-wall-threat){:target="_blank" rel="noopener"}

> Force’s chief constable believes thousands of officers’ personal details are in paramilitary hands Republican paramilitaries have obtained the information that leaked in a Police Service of Northern Ireland data breach, according to the force’s chief constable. Simon Byrne said on Monday that he believed dissident republicans had the dataset that mistakenly disclosed the personal details of more than 10,000 officers and staff last week. Continue reading... [...]
