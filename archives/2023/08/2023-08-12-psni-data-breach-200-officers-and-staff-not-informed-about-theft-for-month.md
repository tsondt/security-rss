Title: PSNI data breach: 200 officers and staff not informed about theft for month
Date: 2023-08-12T18:38:40+00:00
Author: Nadeem Badshah and agency
Category: The Guardian
Tags: Northern Ireland;Police;UK news;Data and computer security
Slug: 2023-08-12-psni-data-breach-200-officers-and-staff-not-informed-about-theft-for-month

[Source](https://www.theguardian.com/uk-news/2023/aug/12/psni-data-breach-200-officers-and-staff-not-informed-about-theft-for-month){:target="_blank" rel="noopener"}

> Police-issued laptop, radio and documents stolen from car in Northern Ireland on 6 July About 200 police officers and staff were not informed about the theft of devices and documents with data potentially affecting them for almost a month, the Police Service of Northern Ireland (PSNI) has confirmed. A police-issued laptop, radio and documents were stolen on 6 July from the car which is understood to belong to a superintendent. Continue reading... [...]
