Title: Popular open source project Moq criticized for quietly collecting data
Date: 2023-08-09T13:42:12-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-08-09-popular-open-source-project-moq-criticized-for-quietly-collecting-data

[Source](https://www.bleepingcomputer.com/news/security/popular-open-source-project-moq-criticized-for-quietly-collecting-data/){:target="_blank" rel="noopener"}

> Open source project Moq (pronounced "Mock") has drawn sharp criticism for quietly including a controversial dependency in its latest release. Moq's 4.20.0 release from this week included another project, SponsorLink, which caused an uproar among open source software consumers, who likened the move to a breach of trust. [...]
