Title: Kremlin-backed Sandworm strikes Android devices with data-stealing Infamous Chisel
Date: 2023-08-31T19:13:23+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-31-kremlin-backed-sandworm-strikes-android-devices-with-data-stealing-infamous-chisel

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/31/sandworm_infamous_chisel/){:target="_blank" rel="noopener"}

> Five Eyes nations warn of hit against Ukrainian military systems Russia's Sandworm crew is using an Android malware strain dubbed Infamous Chisel to remotely access Ukrainian soldiers' devices, monitor network traffic, access files, and steal sensitive information, according to a Five Eyes report published Thursday.... [...]
