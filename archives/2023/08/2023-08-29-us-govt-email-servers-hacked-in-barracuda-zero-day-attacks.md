Title: US govt email servers hacked in Barracuda zero-day attacks
Date: 2023-08-29T08:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-08-29-us-govt-email-servers-hacked-in-barracuda-zero-day-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-govt-email-servers-hacked-in-barracuda-zero-day-attacks/){:target="_blank" rel="noopener"}

> Suspected Chinese hackers disproportionately targeted and breached government and government-linked organizations worldwide in recent attacks targeting a Barracuda Email Security Gateway (ESG) zero-day, with a focus on entities across the Americas. [...]
