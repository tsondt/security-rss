Title: Kali Linux 2023.3 released with 9 new tools, internal changes
Date: 2023-08-23T12:32:55-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-08-23-kali-linux-20233-released-with-9-new-tools-internal-changes

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20233-released-with-9-new-tools-internal-changes/){:target="_blank" rel="noopener"}

> Kali Linux 2023.3, the third version of 2023, is now available for download, with nine new tools and internal optimizations. [...]
