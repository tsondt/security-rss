Title: SEC fines fintech crypto fund that promised 2,700% returns
Date: 2023-08-22T15:34:07+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-08-22-sec-fines-fintech-crypto-fund-that-promised-2700-returns

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/22/sec_titan_fintech_fine/){:target="_blank" rel="noopener"}

> Titan Global Capital Management to pay $1m to those it advised without admitting fault A New York fintech biz is set to pay $1 million in fines under a US Securities and Exchange Commission order that claims it advertised "annualized" returns on Titan Crypto of up to 2,700 percent, a number based on a "purely hypothetical account."... [...]
