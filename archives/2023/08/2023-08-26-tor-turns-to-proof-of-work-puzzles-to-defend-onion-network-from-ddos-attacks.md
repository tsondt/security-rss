Title: Tor turns to proof-of-work puzzles to defend onion network from DDoS attacks
Date: 2023-08-26T08:31:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-08-26-tor-turns-to-proof-of-work-puzzles-to-defend-onion-network-from-ddos-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/26/tor_tweaks_onion_routing_software/){:target="_blank" rel="noopener"}

> No miners were involved in this story Tor, which stands for The Onion Router, weathered a massive distributed denial-of-service (DDoS) storm from June last year through to May.... [...]
