Title: Google released first quantum-resilient FIDO2 key implementation
Date: 2023-08-16T14:35:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-08-16-google-released-first-quantum-resilient-fido2-key-implementation

[Source](https://www.bleepingcomputer.com/news/security/google-released-first-quantum-resilient-fido2-key-implementation/){:target="_blank" rel="noopener"}

> Google has announced the first open-source quantum resilient FIDO2 security key implementation, which uses a unique ECC/Dilithium hybrid signature schema co-created with ETH Zurich. [...]
