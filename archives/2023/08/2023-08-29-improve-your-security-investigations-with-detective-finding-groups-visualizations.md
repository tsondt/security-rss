Title: Improve your security investigations with Detective finding groups visualizations
Date: 2023-08-29T15:55:14+00:00
Author: Rich Vorwaller
Category: AWS Security
Tags: Amazon Detective;Amazon GuardDuty;Amazon Inspector;AWS Security Hub;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Detective;Security Blog;Visualization
Slug: 2023-08-29-improve-your-security-investigations-with-detective-finding-groups-visualizations

[Source](https://aws.amazon.com/blogs/security/improve-your-security-investigations-with-detective-finding-groups-visualizations/){:target="_blank" rel="noopener"}

> At AWS, we often hear from customers that they want expanded security coverage for the multiple services that they use on AWS. However, alert fatigue is a common challenge that customers face as we introduce new security protections. The challenge becomes how to operationalize, identify, and prioritize alerts that represent real risk. In this post, [...]
