Title: Leaseweb is restoring ‘critical’ systems after security breach
Date: 2023-08-25T10:59:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-25-leaseweb-is-restoring-critical-systems-after-security-breach

[Source](https://www.bleepingcomputer.com/news/security/leaseweb-is-restoring-critical-systems-after-security-breach/){:target="_blank" rel="noopener"}

> Leaseweb, one of the world's largest cloud and hosting providers, notified people that it's working on restoring "critical" systems disabled following a recent security breach. [...]
