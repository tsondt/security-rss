Title: FBI warns of patched Barracuda ESG appliances still being hacked
Date: 2023-08-24T15:09:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-24-fbi-warns-of-patched-barracuda-esg-appliances-still-being-hacked

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-patched-barracuda-esg-appliances-still-being-hacked/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation warned that patches for a critical Barracuda Email Security Gateway (ESG) remote command injection flaw are "ineffective," and patched appliances are still being compromised in ongoing attacks. [...]
