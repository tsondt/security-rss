Title: How fame-seeking teenagers hacked some of the world’s biggest targets
Date: 2023-08-11T00:09:56+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;fido2;homeland security department;lapsus$;mfa
Slug: 2023-08-11-how-fame-seeking-teenagers-hacked-some-of-the-worlds-biggest-targets

[Source](https://arstechnica.com/?p=1960309){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A ragtag bunch of amateur hackers, many of them teenagers with little technical training, have been so adept at breaching large targets, including Microsoft, Okta, Nvidia, and Globant, that the federal government is studying their methods to get a better grounding in cybersecurity. The group, known as Lapsus$, is a loosely organized group that employs hacking techniques that, while decidedly unsophisticated, have proved highly effective. What the group lacks in software exploitation, it makes up for with persistence and creativity. One example is their technique for bypassing MFA (multi-factor authentication) at well-defended organizations. Studying the Lapsus$ [...]
