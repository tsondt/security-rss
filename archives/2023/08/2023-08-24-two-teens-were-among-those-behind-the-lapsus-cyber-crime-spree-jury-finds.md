Title: Two teens were among those behind the Lapsus$ cyber-crime spree, jury finds
Date: 2023-08-24T07:33:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-24-two-teens-were-among-those-behind-the-lapsus-cyber-crime-spree-jury-finds

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/24/two_teens_lapsus_jury/){:target="_blank" rel="noopener"}

> From BT and Nvidia to Grand Theft Auto 6, pair went on a total tear Two teenage members of the chaotic Lapsus$ cyber-crime gang helped compromise computer systems of Uber and Nvidia, and also blackmailed Grand Theft Auto maker Rockstar Games among other high-profile victims, a jury has decided.... [...]
