Title: Hacktivists fund their operations using common cybercrime tactics
Date: 2023-08-03T14:59:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-03-hacktivists-fund-their-operations-using-common-cybercrime-tactics

[Source](https://www.bleepingcomputer.com/news/security/hacktivists-fund-their-operations-using-common-cybercrime-tactics/){:target="_blank" rel="noopener"}

> Hacktivist groups that operate for political or ideological motives employ a broad range of funding methods to support their operations. [...]
