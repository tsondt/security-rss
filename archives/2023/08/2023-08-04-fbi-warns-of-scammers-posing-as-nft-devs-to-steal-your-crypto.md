Title: FBI warns of scammers posing as NFT devs to steal your crypto
Date: 2023-08-04T14:11:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-08-04-fbi-warns-of-scammers-posing-as-nft-devs-to-steal-your-crypto

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-scammers-posing-as-nft-devs-to-steal-your-crypto/){:target="_blank" rel="noopener"}

> The FBI warned today of fraudsters posing as Non-Fungible Token (NFT) developers to prey upon NFT enthusiasts and steal their cryptocurrency and NFT assets. [...]
