Title: New Microsoft Azure AD CTS feature can be abused for lateral movement
Date: 2023-08-03T18:55:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;Microsoft
Slug: 2023-08-03-new-microsoft-azure-ad-cts-feature-can-be-abused-for-lateral-movement

[Source](https://www.bleepingcomputer.com/news/security/new-microsoft-azure-ad-cts-feature-can-be-abused-for-lateral-movement/){:target="_blank" rel="noopener"}

> Microsoft's new Azure Active Directory Cross-Tenant Synchronization (CTS) feature, introduced in June 2023, has created a new potential attack surface that might allow threat actors to more easily spread laterally to other Azure tenants. [...]
