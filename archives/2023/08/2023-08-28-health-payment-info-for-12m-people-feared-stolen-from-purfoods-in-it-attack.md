Title: Health, payment info for 1.2M people feared stolen from Purfoods in IT attack
Date: 2023-08-28T21:45:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-28-health-payment-info-for-12m-people-feared-stolen-from-purfoods-in-it-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/28/purfoods_meal_data_theft/){:target="_blank" rel="noopener"}

> Meal delivery biz leaves bitter taste Purfoods has notified more than 1.2 million people that their personal and medical data — including payment card and bank account numbers, security codes, and some protected health information — may have been stolen from its servers during what sounds like a ransomware infection earlier this year.... [...]
