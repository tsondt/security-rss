Title: Sextortion suspects on trial after teen victim dies from a self-inflicted gunshot wound
Date: 2023-08-14T23:28:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-14-sextortion-suspects-on-trial-after-teen-victim-dies-from-a-self-inflicted-gunshot-wound

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/14/sextortion_suspects_arrested/){:target="_blank" rel="noopener"}

> Trio alleged to have blackmailed over 100 targets after threats of intimate image release Two Nigerian men have been extradited to the US and were scheduled to appear in deferral court on Monday, charged with sextortion and causing the death of one of their victims: a teen who was found dead from a self-inflicted gunshot wound.... [...]
