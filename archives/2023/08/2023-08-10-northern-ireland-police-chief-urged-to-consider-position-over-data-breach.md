Title: Northern Ireland police chief urged to consider position over data breach
Date: 2023-08-10T08:51:19+00:00
Author: Aubrey Allegretti Chief political correspondent
Category: The Guardian
Tags: Northern Ireland;Data protection;UK news;Police;Information commissioner;Data and computer security;Freedom of information
Slug: 2023-08-10-northern-ireland-police-chief-urged-to-consider-position-over-data-breach

[Source](https://www.theguardian.com/uk-news/2023/aug/10/psni-northern-ireland-police-chief-urged-to-consider-position-over-data-breach){:target="_blank" rel="noopener"}

> DUP MP Sammy Wilson says serious questions must be asked at highest level of PSNI amid fears over safety of officers The head of policing in Northern Ireland has been urged to consider his position over the mass breach of officers’ data amid warnings that terrorists could use the information to carry out attacks. Sammy Wilson, a Democratic Unionist party MP, suggested Simon Byrne’s future as chief constable might not be sustainable. Continue reading... [...]
