Title: Northern Ireland police may have endangered its own officers by posting details online in error
Date: 2023-08-09T13:00:15+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-08-09-northern-ireland-police-may-have-endangered-its-own-officers-by-posting-details-online-in-error

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/09/psni_data_breach/){:target="_blank" rel="noopener"}

> At least it was a blunder and not a hostile attack, unlike what happened to another UK public body this week A spreadsheet containing details of serving Northern Ireland police officers was mistakenly posted online yesterday, potentially endangering the safety of officers, given the volatile politics of the region.... [...]
