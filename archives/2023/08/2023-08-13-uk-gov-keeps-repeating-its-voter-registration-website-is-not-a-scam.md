Title: UK gov keeps repeating its voter registration website is NOT a scam
Date: 2023-08-13T07:07:03-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-08-13-uk-gov-keeps-repeating-its-voter-registration-website-is-not-a-scam

[Source](https://www.bleepingcomputer.com/news/security/uk-gov-keeps-repeating-its-voter-registration-website-is-not-a-scam/){:target="_blank" rel="noopener"}

> Every year local government bodies or councils across Britain contact residents, asking them to update their voter details on the electoral register if these have changed. To do so, residents are asked to visit HouseholdResponse.com, a domain that looks anything but official and has often confused people, who mistake it for a scam. [...]
