Title: Why Every Security Practitioner Should Attend mWISE
Date: 2023-08-02T10:01:02-04:00
Author: Sponsored by Mandiant
Category: BleepingComputer
Tags: Security
Slug: 2023-08-02-why-every-security-practitioner-should-attend-mwise

[Source](https://www.bleepingcomputer.com/news/security/why-every-security-practitioner-should-attend-mwise/){:target="_blank" rel="noopener"}

> What's in store for mWISE 2023? 80+ curated sessions. 90+ hand-picked speakers. 7 session tracks. It's taking place September 18-20, 2023 in Washington, DC. Register now and get $300 off a full conference pass, which includes access to all the sessions, evening receptions, and events. [...]
