Title: How to manage a mass password reset due to a ransomware attack
Date: 2023-08-01T10:02:04-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-08-01-how-to-manage-a-mass-password-reset-due-to-a-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/how-to-manage-a-mass-password-reset-due-to-a-ransomware-attack/){:target="_blank" rel="noopener"}

> Resetting the passwords for thousands of people after a ransomware attack is challenging, to say the least, for any IT team. Learn more from Specops Software on why organizations are forced into mass password resets and how to make the process manageable. [...]
