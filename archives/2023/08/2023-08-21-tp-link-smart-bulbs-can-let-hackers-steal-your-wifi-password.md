Title: TP-Link smart bulbs can let hackers steal your WiFi password
Date: 2023-08-21T15:55:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-21-tp-link-smart-bulbs-can-let-hackers-steal-your-wifi-password

[Source](https://www.bleepingcomputer.com/news/security/tp-link-smart-bulbs-can-let-hackers-steal-your-wifi-password/){:target="_blank" rel="noopener"}

> Researchers from Italy and the UK have discovered four vulnerabilities in the TP-Link Tapo L530E smart bulb and TP-Link's Tapo app, which could allow attackers to steal their target's WiFi password. [...]
