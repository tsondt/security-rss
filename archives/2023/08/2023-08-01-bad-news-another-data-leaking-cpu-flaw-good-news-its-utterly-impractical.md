Title: Bad news: Another data-leaking CPU flaw. Good news: It's utterly impractical
Date: 2023-08-01T17:00:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-08-01-bad-news-another-data-leaking-cpu-flaw-good-news-its-utterly-impractical

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/01/collide_power_cpu_attack/){:target="_blank" rel="noopener"}

> Collide+Power vulnerability leaks secrets bit by bit - but could take months or years to learn a useful secret Boffins in Austria and Germany have devised a power-monitoring side-channel attack on modern computer chips that exposes sensitive data, but very slowly.... [...]
