Title: Brit healthcare body rapped for WhatsApp chat sharing patient data
Date: 2023-08-03T09:26:07+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-08-03-brit-healthcare-body-rapped-for-whatsapp-chat-sharing-patient-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/03/nhs_ico_warning/){:target="_blank" rel="noopener"}

> Time for a proper secure clinical image transfer system, perhaps? Staff at NHS Lanarkshire - which serves over half a million Scottish residents - used WhatsApp to swap photos and personal info about patients, including children's names and addresses.... [...]
