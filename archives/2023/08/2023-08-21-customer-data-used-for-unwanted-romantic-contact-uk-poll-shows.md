Title: Customer data used for unwanted romantic contact, UK poll shows
Date: 2023-08-21T23:01:29+00:00
Author: Hibaq Farah
Category: The Guardian
Tags: Data protection;Young people;Internet safety;Business;Dating;Life and style;Society;Information commissioner;UK news;Consumer affairs;Money;GDPR;Technology;Privacy;Data and computer security
Slug: 2023-08-21-customer-data-used-for-unwanted-romantic-contact-uk-poll-shows

[Source](https://www.theguardian.com/technology/2023/aug/22/customer-data-unwanted-romantic-contact-poll){:target="_blank" rel="noopener"}

> Almost one in three people aged 18-34 have been messaged by staff after giving personal details to a business Almost one in three people aged 18-34 have received unwanted romantic contact after giving their personal information to a business, a UK poll has shown. The Information Commissioner’s Office (ICO) has called for recipients of such texts to come forward to help the regulator gather evidence of the impact of this phenomenon. The ICO has an online form for people who want to report an experience of unwanted contact. Continue reading... [...]
