Title: New PaperCut critical bug exposes unpatched servers to RCE attacks
Date: 2023-08-04T16:23:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-04-new-papercut-critical-bug-exposes-unpatched-servers-to-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-papercut-critical-bug-exposes-unpatched-servers-to-rce-attacks/){:target="_blank" rel="noopener"}

> PaperCut recently fixed a critical security vulnerability in its NG/MF print management software that allows unauthenticated attackers to gain remote code execution on unpatched Windows servers. [...]
