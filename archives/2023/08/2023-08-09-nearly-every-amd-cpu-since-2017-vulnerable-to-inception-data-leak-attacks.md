Title: Nearly every AMD CPU since 2017 vulnerable to Inception data-leak attacks
Date: 2023-08-09T22:52:36+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2023-08-09-nearly-every-amd-cpu-since-2017-vulnerable-to-inception-data-leak-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/09/amd_inception/){:target="_blank" rel="noopener"}

> It's like a nesting doll of security flaws AMD processor users, you have another data-leaking vulnerability to deal with: like Zenbleed, this latest hole can be to steal sensitive data from a running vulnerable machine.... [...]
