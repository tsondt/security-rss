Title: INTERPOL shutters '16shop' phishing-as-a-service outfit
Date: 2023-08-09T03:02:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-08-09-interpol-shutters-16shop-phishing-as-a-service-outfit

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/09/interpol_16shop_phishing_shutdown/){:target="_blank" rel="noopener"}

> Alleged administrator cuffed in Indonesia, associate arrested in Japan, accused of selling fake Amazons for $60 INTERPOL has revealed a successful investigation into a phishing-as-a-service operation named "16shop" with arrests of alleged operators made in Indonesia and Japan and the platform shut down.... [...]
