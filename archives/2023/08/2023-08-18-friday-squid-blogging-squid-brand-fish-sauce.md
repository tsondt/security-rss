Title: Friday Squid Blogging: Squid Brand Fish Sauce
Date: 2023-08-18T21:02:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-08-18-friday-squid-blogging-squid-brand-fish-sauce

[Source](https://www.schneier.com/blog/archives/2023/08/friday-squid-blogging-squid-brand-fish-sauce.html){:target="_blank" rel="noopener"}

> Squid Brand is a Thai company that makes fish sauce : It is part of Squid Brand’s range of “personalized healthy fish sauces” that cater to different consumer groups, which include the Mild Fish Sauce for Kids and Mild Fish Sauce for Silver Ages. It also has a Vegan Fish Sauce. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
