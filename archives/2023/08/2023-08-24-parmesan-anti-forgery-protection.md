Title: Parmesan Anti-Forgery Protection
Date: 2023-08-24T11:24:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;forgery
Slug: 2023-08-24-parmesan-anti-forgery-protection

[Source](https://www.schneier.com/blog/archives/2023/08/parmesan-anti-forgery-protection.html){:target="_blank" rel="noopener"}

> The Guardian is reporting about microchips in wheels of Parmesan cheese as an anti-forgery measure. [...]
