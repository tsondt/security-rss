Title: The Week in Ransomware - August 11th 2023 - Targeting Healthcare
Date: 2023-08-11T18:04:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-11-the-week-in-ransomware-august-11th-2023-targeting-healthcare

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-august-11th-2023-targeting-healthcare/){:target="_blank" rel="noopener"}

> While some ransomware operations claim not to target hospitals, one relatively new ransomware gang named Rhysida doesn't seem to care. [...]
