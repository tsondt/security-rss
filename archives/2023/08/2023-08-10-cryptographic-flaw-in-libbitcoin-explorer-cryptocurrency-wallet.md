Title: Cryptographic Flaw in Libbitcoin Explorer Cryptocurrency Wallet
Date: 2023-08-10T11:12:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;keys;random numbers
Slug: 2023-08-10-cryptographic-flaw-in-libbitcoin-explorer-cryptocurrency-wallet

[Source](https://www.schneier.com/blog/archives/2023/08/cryptographic-flaw-in-libbitcoin-explorer-cryptocurrency-wallet.html){:target="_blank" rel="noopener"}

> Cryptographic flaws still matter. Here’s a flaw in the random-number generator used to create private keys. The seed has only 32 bits of entropy. Seems like this flaw is being exploited in the wild. [...]
