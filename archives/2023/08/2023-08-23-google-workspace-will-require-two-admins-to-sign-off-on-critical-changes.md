Title: Google Workspace will require two admins to sign off on critical changes
Date: 2023-08-23T12:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-08-23-google-workspace-will-require-two-admins-to-sign-off-on-critical-changes

[Source](https://www.bleepingcomputer.com/news/google/google-workspace-will-require-two-admins-to-sign-off-on-critical-changes/){:target="_blank" rel="noopener"}

> Google announced today new cybersecurity defense controls that will allow security teams to thwart account takeover attempts and social engineering attacks targeting Workspace users. [...]
