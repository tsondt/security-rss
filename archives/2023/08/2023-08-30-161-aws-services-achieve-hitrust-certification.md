Title: 161 AWS services achieve HITRUST certification
Date: 2023-08-30T17:44:41+00:00
Author: Mark Weech
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS HITRUST;AWS HITRUST Inheritance;Compliance reports;Security Assurance;Security Blog
Slug: 2023-08-30-161-aws-services-achieve-hitrust-certification

[Source](https://aws.amazon.com/blogs/security/161-aws-services-achieve-hitrust-certification/){:target="_blank" rel="noopener"}

> The Amazon Web Services (AWS) HITRUST Compliance Team is excited to announce that 161 AWS services have been certified for the HITRUST CSF version 11.0.1 for the 2023 cycle. The full list of AWS services, which were audited by a third-party assessor and certified under the HITRUST CSF, is now available on our Services in [...]
