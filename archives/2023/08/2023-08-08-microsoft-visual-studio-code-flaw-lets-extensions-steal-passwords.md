Title: Microsoft Visual Studio Code flaw lets extensions steal passwords
Date: 2023-08-08T17:49:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-08-08-microsoft-visual-studio-code-flaw-lets-extensions-steal-passwords

[Source](https://www.bleepingcomputer.com/news/security/microsoft-visual-studio-code-flaw-lets-extensions-steal-passwords/){:target="_blank" rel="noopener"}

> Microsoft's Visual Studio Code (VS Code) code editor and development environment contains a flaw that allows malicious extensions to retrieve authentication tokens stored in Windows, Linux, and macOS credential managers. [...]
