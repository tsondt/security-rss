Title: Over 3,000 Openfire servers vulnerable to takover attacks
Date: 2023-08-23T15:36:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-23-over-3000-openfire-servers-vulnerable-to-takover-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-3-000-openfire-servers-vulnerable-to-takover-attacks/){:target="_blank" rel="noopener"}

> Thousands of Openfire servers remain vulnerable to CVE-2023-32315, an actively exploited and path traversal vulnerability that allows an unauthenticated user to create new admin accounts. [...]
