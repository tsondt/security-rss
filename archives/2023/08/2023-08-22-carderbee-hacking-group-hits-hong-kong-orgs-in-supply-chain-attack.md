Title: Carderbee hacking group hits Hong Kong orgs in supply chain attack
Date: 2023-08-22T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-22-carderbee-hacking-group-hits-hong-kong-orgs-in-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/carderbee-hacking-group-hits-hong-kong-orgs-in-supply-chain-attack/){:target="_blank" rel="noopener"}

> A previously unidentified APT hacking group named 'Carderbee' was observed attacking organizations in Hong Kong and other regions in Asia, using legitimate software to infect targets' computers with the PlugX malware. [...]
