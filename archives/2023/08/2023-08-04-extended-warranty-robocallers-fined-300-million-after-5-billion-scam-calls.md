Title: Extended warranty robocallers fined $300 million after 5 billion scam calls
Date: 2023-08-04T10:54:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-08-04-extended-warranty-robocallers-fined-300-million-after-5-billion-scam-calls

[Source](https://www.bleepingcomputer.com/news/security/extended-warranty-robocallers-fined-300-million-after-5-billion-scam-calls/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) has announced a record-breaking $299,997,000 fine imposed on an international network of companies for placing five billion robocalls to more than 500 million phone numbers over three months in 2021. [...]
