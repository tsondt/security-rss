Title: Socket moves beyond JavaScript and Python and gets into Go
Date: 2023-08-02T01:58:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-08-02-socket-moves-beyond-javascript-and-python-and-gets-into-go

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/02/socket_go_funding/){:target="_blank" rel="noopener"}

> CEO, fresh with funds, lays out the dependency dilemma Interview Open source security biz Socket is extending its source code dependency checker, which previously addressed only JavaScript and Python, by adding support for checking Go code.... [...]
