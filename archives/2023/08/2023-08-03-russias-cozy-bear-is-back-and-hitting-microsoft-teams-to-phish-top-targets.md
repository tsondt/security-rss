Title: Russia's Cozy Bear is back and hitting Microsoft Teams to phish top targets
Date: 2023-08-03T21:24:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-03-russias-cozy-bear-is-back-and-hitting-microsoft-teams-to-phish-top-targets

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/03/microsoft_teams_cozy_bear/){:target="_blank" rel="noopener"}

> Plus: Tenable CEO blasts Redmond's bug disclosure habits An infamous Kremlin-backed gang has been using Microsoft Teams chats in attempts to phish marks in governments, NGOs, and IT businesses, according to the Windows giant.... [...]
