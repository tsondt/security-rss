Title: Ivanti discloses new critical auth bypass bug in MobileIron Core
Date: 2023-08-02T16:49:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-02-ivanti-discloses-new-critical-auth-bypass-bug-in-mobileiron-core

[Source](https://www.bleepingcomputer.com/news/security/ivanti-discloses-new-critical-auth-bypass-bug-in-mobileiron-core/){:target="_blank" rel="noopener"}

> IT software company Ivanti disclosed today a new critical security vulnerability in its MobileIron Core mobile device management software. [...]
