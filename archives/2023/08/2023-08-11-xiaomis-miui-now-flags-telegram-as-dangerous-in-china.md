Title: Xiaomi's MIUI now flags Telegram as dangerous in China
Date: 2023-08-11T12:54:35-04:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Technology;Security
Slug: 2023-08-11-xiaomis-miui-now-flags-telegram-as-dangerous-in-china

[Source](https://www.bleepingcomputer.com/news/technology/xiaomis-miui-now-flags-telegram-as-dangerous-in-china/){:target="_blank" rel="noopener"}

> Asian smartphone giant Xiaomi is now blocking Telegram from being installed on devices using its MIUI system and firmware interface. [...]
