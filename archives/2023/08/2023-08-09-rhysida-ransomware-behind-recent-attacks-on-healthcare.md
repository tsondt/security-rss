Title: Rhysida ransomware behind recent attacks on healthcare
Date: 2023-08-09T14:31:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-08-09-rhysida-ransomware-behind-recent-attacks-on-healthcare

[Source](https://www.bleepingcomputer.com/news/security/rhysida-ransomware-behind-recent-attacks-on-healthcare/){:target="_blank" rel="noopener"}

> The Rhysida ransomware as a service (RaaS) operation that emerged in May 2023 is gradually leaving the period of obscurity behind, as a recent wave of attacks on healthcare organizations has forced government agencies and cybersecurity companies to pay closer attention to its operations. [...]
