Title: Lapsus$ hackers took SIM-swapping attacks to the next level
Date: 2023-08-11T02:23:46-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-08-11-lapsus-hackers-took-sim-swapping-attacks-to-the-next-level

[Source](https://www.bleepingcomputer.com/news/security/lapsus-hackers-took-sim-swapping-attacks-to-the-next-level/){:target="_blank" rel="noopener"}

> The U.S. government released a report after analyzing simple techniques, e.g. SIM swapping, used by the Lapsus$ extortion group to breach dozens of organizations with a strong security posture. [...]
