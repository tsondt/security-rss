Title: UK Electoral Commission Hacked
Date: 2023-08-16T11:17:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;hacking;voting
Slug: 2023-08-16-uk-electoral-commission-hacked

[Source](https://www.schneier.com/blog/archives/2023/08/uk-electoral-commission-hacked.html){:target="_blank" rel="noopener"}

> The UK Electoral Commission discovered last year that it was hacked the year before. That’s fourteen months between the hack and the discovery. It doesn’t know who was behind the hack. We worked with external security experts and the National Cyber Security Centre to investigate and secure our systems. If the hack was by a major government, the odds are really low that it has resecured its systems—unless it burned the network to the ground and rebuilt it from scratch (which seems unlikely). [...]
