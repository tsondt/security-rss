Title: Sneaky Amazon Google ad leads to Microsoft support scam
Date: 2023-08-21T13:52:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-08-21-sneaky-amazon-google-ad-leads-to-microsoft-support-scam

[Source](https://www.bleepingcomputer.com/news/security/sneaky-amazon-google-ad-leads-to-microsoft-support-scam/){:target="_blank" rel="noopener"}

> A legitimate-looking ad for Amazon in Google search results redirects visitors to a Microsoft Defender tech support scam that locks up their browser. [...]
