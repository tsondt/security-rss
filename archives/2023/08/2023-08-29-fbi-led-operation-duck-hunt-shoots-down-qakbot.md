Title: FBI-led Operation Duck Hunt shoots down Qakbot
Date: 2023-08-29T20:03:31+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-29-fbi-led-operation-duck-hunt-shoots-down-qakbot

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/29/duck_hunt_qakbot/){:target="_blank" rel="noopener"}

> Totally plucked: Agents remotely roast Windows botnet malware on victims' machines Uncle Sam today said an international law enforcement effort dismantled Qakbot, aka QBot, a notorious botnet and malware loader responsible for losses totaling hundreds of millions of dollars worldwide, and seized more than $8.6 million in illicit cryptocurrency.... [...]
