Title: Qakbot botnet dismantled after infecting over 700,000 computers
Date: 2023-08-29T12:54:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-29-qakbot-botnet-dismantled-after-infecting-over-700000-computers

[Source](https://www.bleepingcomputer.com/news/security/qakbot-botnet-dismantled-after-infecting-over-700-000-computers/){:target="_blank" rel="noopener"}

> Qakbot, one of the largest and longest-running botnets to date, was taken down following a multinational law enforcement operation spearheaded by the FBI and known as Operation 'Duck Hunt.' [...]
