Title: US government to investigate China's Microsoft email breach
Date: 2023-08-14T02:58:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-08-14-us-government-to-investigate-chinas-microsoft-email-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/14/us_government_investigates_microsoft_breach/){:target="_blank" rel="noopener"}

> PLUS: Phishing campaign targets the C-suite; Cybercrime arrests in EU and Africa; and more Infosec in brief The July breach of Microsoft Exchange Online by suspected Chinese hackers is the next topic up for review by the Department of Homeland Security's Cyber Safety Review Board (CSRB).... [...]
