Title: Remotely Stopping Polish Trains
Date: 2023-08-28T11:05:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cybersecurity;radio;Russia;transportation
Slug: 2023-08-28-remotely-stopping-polish-trains

[Source](https://www.schneier.com/blog/archives/2023/08/remotely-stopping-polish-trains.html){:target="_blank" rel="noopener"}

> Turns out that it’s easy to broadcast radio commands that force Polish trains to stop:...the saboteurs appear to have sent simple so-called “radio-stop” commands via radio frequency to the trains they targeted. Because the trains use a radio system that lacks encryption or authentication for those commands, Olejnik says, anyone with as little as $30 of off-the-shelf radio equipment can broadcast the command to a Polish train­—sending a series of three acoustic tones at a 150.100 megahertz frequency­—and trigger their emergency stop function. “It is three tonal messages sent consecutively. Once the radio equipment receives it, the locomotive goes to [...]
