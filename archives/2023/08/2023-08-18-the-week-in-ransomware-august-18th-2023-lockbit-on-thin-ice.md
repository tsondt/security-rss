Title: The Week in Ransomware - August 18th 2023 - LockBit on Thin Ice
Date: 2023-08-18T17:07:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-18-the-week-in-ransomware-august-18th-2023-lockbit-on-thin-ice

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-august-18th-2023-lockbit-on-thin-ice/){:target="_blank" rel="noopener"}

> While there was quite a bit of ransomware news this week, the highlighted story was the release of Jon DiMaggio's third article in the Ransomware Diaries series, with the focus of this article on the LockBit ransomware operation. [...]
