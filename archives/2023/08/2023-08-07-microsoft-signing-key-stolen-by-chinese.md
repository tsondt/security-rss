Title: Microsoft Signing Key Stolen by Chinese
Date: 2023-08-07T11:03:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;backdoors;China;cybersecurity;hacking;keys;Microsoft
Slug: 2023-08-07-microsoft-signing-key-stolen-by-chinese

[Source](https://www.schneier.com/blog/archives/2023/08/microsoft-signing-key-stolen-by-chinese.html){:target="_blank" rel="noopener"}

> A bunch of networks, including US Government networks, have been hacked by the Chinese. The hackers used forged authentication tokens to access user email, using a stolen Microsoft Azure account consumer signing key. Congress wants answers. The phrase “ negligent security practices ” is being tossed about—and with good reason. Master signing keys are not supposed to be left around, waiting to be stolen. Actually, two things went badly wrong here. The first is that Azure accepted an expired signing key, implying a vulnerability in whatever is supposed to check key validity. The second is that this key was supposed [...]
