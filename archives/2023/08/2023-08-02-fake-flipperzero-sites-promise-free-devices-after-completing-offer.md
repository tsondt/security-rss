Title: Fake FlipperZero sites promise free devices after completing offer
Date: 2023-08-02T18:23:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-02-fake-flipperzero-sites-promise-free-devices-after-completing-offer

[Source](https://www.bleepingcomputer.com/news/security/fake-flipperzero-sites-promise-free-devices-after-completing-offer/){:target="_blank" rel="noopener"}

> A site impersonating Flipper Devices promises a free Flipper Zero after completing an offer but only leads to shady browser extensions and scam sites. [...]
