Title: Hacking AI Resume Screening with Text in a White Font
Date: 2023-08-01T11:11:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;hacking;steganography;trust
Slug: 2023-08-01-hacking-ai-resume-screening-with-text-in-a-white-font

[Source](https://www.schneier.com/blog/archives/2023/08/hacking-ai-resume-screening-with-text-in-a-white-font.html){:target="_blank" rel="noopener"}

> The Washington Post is reporting on a hack to fool automatic resume sorting programs: putting text in a white font. The idea is that the programs rely primarily on simple pattern matching, and the trick is to copy a list of relevant keywords—or the published job description—into the resume in a white font. The computer will process the text, but humans won’t see it. Clever. I’m not sure it’s actually useful in getting a job, though. Eventually the humans will figure out that the applicant doesn’t actually have the required skills. But...maybe. [...]
