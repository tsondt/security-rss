Title: Old-school hacktivism is back because it never went away
Date: 2023-08-03T19:44:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-08-03-old-school-hacktivism-is-back-because-it-never-went-away

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/03/old_school_hacktivism_is_back/){:target="_blank" rel="noopener"}

> Mysterious Team Bangladesh has carried out 846 attacks since June 2022, mostly DDoS Hacktivism may have dropped off of organization radars over the past few years, but it is now very visibly coming from what is believed to be Bangladesh, thanks to a group tracked by cybersecurity firm Group-IB.... [...]
