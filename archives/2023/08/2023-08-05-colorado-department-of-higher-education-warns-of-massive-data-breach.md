Title: Colorado Department of Higher Education warns of massive data breach
Date: 2023-08-05T12:16:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-05-colorado-department-of-higher-education-warns-of-massive-data-breach

[Source](https://www.bleepingcomputer.com/news/security/colorado-department-of-higher-education-warns-of-massive-data-breach/){:target="_blank" rel="noopener"}

> The Colorado Department of Higher Education (CDHE) discloses a massive data breach impacting students, past students, and teachers after suffering a ransomware attack in June. [...]
