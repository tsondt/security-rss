Title: Man arrested in Northern Ireland police data leak as more incidents come to light
Date: 2023-08-17T12:03:59+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-08-17-man-arrested-in-northern-ireland-police-data-leak-as-more-incidents-come-to-light

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/17/man_arrested_in_ni_police/){:target="_blank" rel="noopener"}

> Plus laptop and radio with yet more officers details reportedly nicked from car A man was arrested in Northern Ireland for suspected Collection of Terrorist Information following an incident where police mistakenly leaked details that identified 10,000 serving officers, but he has now been released on bail.... [...]
