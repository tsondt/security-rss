Title: Barracuda gateway attacks: How Chinese snoops keep a grip on victims' networks
Date: 2023-08-30T23:00:48+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-30-barracuda-gateway-attacks-how-chinese-snoops-keep-a-grip-on-victims-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/30/mandiant_barracuda_esg_bug/){:target="_blank" rel="noopener"}

> Backdoors detailed, plus CISA releases more IOCs for IT depts to check Nearly a third of organizations compromised by Chinese cyberspies via a critical bug in some Barracuda Email Security Gateways were government units, according to Mandiant.... [...]
