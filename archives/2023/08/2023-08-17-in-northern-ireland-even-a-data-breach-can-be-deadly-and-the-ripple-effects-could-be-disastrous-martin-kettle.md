Title: In Northern Ireland, even a data breach can be deadly – and the ripple effects could be disastrous | Martin Kettle
Date: 2023-08-17T07:00:10+00:00
Author: Martin Kettle
Category: The Guardian
Tags: Northern Ireland;Good Friday agreement;UK news;Ireland;Police;Data and computer security
Slug: 2023-08-17-in-northern-ireland-even-a-data-breach-can-be-deadly-and-the-ripple-effects-could-be-disastrous-martin-kettle

[Source](https://www.theguardian.com/commentisfree/2023/aug/17/lesson-northern-ireland-police-data-breach-cruelty-the-troubles){:target="_blank" rel="noopener"}

> Details about PSNI officers appear to be in the possession of people who may terrorise and kill them. The return of power-sharing has never been more urgent Half of the people who live in these islands have no adult memory at all of the Northern Ireland troubles. Too many of those who can remember them have allowed the bombings, shootings, riots and violence to slip from their minds in the 25 years that have passed since a peace treaty was signed in 1998. But last week’s data leak by the Police Service of Northern Ireland (PSNI) ought to be a [...]
