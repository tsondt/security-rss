Title: Microsoft: Stealthy Flax Typhoon hackers use LOLBins to evade detection
Date: 2023-08-25T11:56:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-25-microsoft-stealthy-flax-typhoon-hackers-use-lolbins-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/microsoft-stealthy-flax-typhoon-hackers-use-lolbins-to-evade-detection/){:target="_blank" rel="noopener"}

> Microsoft has identified a new hacking group it now tracks as Flax Typhoon that argets government agencies and education, critical manufacturing, and information technology organizations likely for espionage purposes. [...]
