Title: Additional IL5 services available for the DoD with Google Assured Workloads
Date: 2023-08-09T16:00:00+00:00
Author: Jordan Sembower
Category: GCP Security
Tags: Security & Identity;Public Sector
Slug: 2023-08-09-additional-il5-services-available-for-the-dod-with-google-assured-workloads

[Source](https://cloud.google.com/blog/topics/public-sector/additional-il5-services-available-dod-google-assured-workloads/){:target="_blank" rel="noopener"}

> Department of Defense (DoD) customers can now deploy DoD SRG Impact Level 5 (IL5) workloads on Google Cloud through Assured Workloads, providing customers with unparalleled flexibility in any of Google’s U.S. regions including one of the world's largest publicly available machine learning hubs. Google provides the most extensive data center footprint for IL5 workloads of any cloud service provider. Since Google Cloud’s 2022 announcement on DoD IL5 workloads and our commitment to rapidly expand Google Cloud services with IL5 authorization, we have made 10 services available at IL5 and will continue to expand those services and provide updates as more [...]
