Title: Electoral Commission apologises for security breach involving UK voters’ data
Date: 2023-08-08T17:26:42+00:00
Author: Rowena Mason and Hibaq Farah
Category: The Guardian
Tags: Electoral Commission;Data and computer security;Hacking;Data protection;Information commissioner;Politics;UK news;Technology;Privacy;Cybercrime
Slug: 2023-08-08-electoral-commission-apologises-for-security-breach-involving-uk-voters-data

[Source](https://www.theguardian.com/technology/2023/aug/08/uk-electoral-commission-registers-targeted-by-hostile-hackers){:target="_blank" rel="noopener"}

> Names and addresses of 40 million registered voters were accessible as far back as 2021 after cyber-attack Confidence in the UK’s electoral regulator has been thrown into question after it emerged a hostile cyber-attack accessing the data of 40 million voters went undetected for a year and the public was not told for another 10 months. The Electoral Commission apologised for the security breach in which the names and addresses of all voters registered between 2014 and 2022 were open to “hostile actors” as far back as August 2021. Continue reading... [...]
