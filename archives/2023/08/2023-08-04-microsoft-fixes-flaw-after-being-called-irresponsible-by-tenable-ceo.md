Title: Microsoft fixes flaw after being called irresponsible by Tenable CEO
Date: 2023-08-04T18:54:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-08-04-microsoft-fixes-flaw-after-being-called-irresponsible-by-tenable-ceo

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-flaw-after-being-called-irresponsible-by-tenable-ceo/){:target="_blank" rel="noopener"}

> Microsoft fixed a security flaw in the Power Platform Custom Connectors feature that let unauthenticated attackers access cross-tenant applications and Azure customers' sensitive data after being called "grossly irresponsible" by Tenable's CEO. [...]
