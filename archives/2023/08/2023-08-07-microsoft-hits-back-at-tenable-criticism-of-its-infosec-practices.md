Title: Microsoft hits back at Tenable criticism of its infosec practices
Date: 2023-08-07T05:40:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-08-07-microsoft-hits-back-at-tenable-criticism-of-its-infosec-practices

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/07/microsoft_power_platform_tenable_criticism/){:target="_blank" rel="noopener"}

> 'Not all fixes are equal,' argues Redmond, and this one for the Power Platform didn't need to be rushed Microsoft has explained why it seemingly took its time to fix a flaw reported to it by infosec intelligence vendor Tenable.... [...]
