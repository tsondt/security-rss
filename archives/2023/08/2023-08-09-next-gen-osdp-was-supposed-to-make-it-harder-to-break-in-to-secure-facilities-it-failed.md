Title: Next-gen OSDP was supposed to make it harder to break in to secure facilities. It failed.
Date: 2023-08-09T14:30:07+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;open supervised device protocol;osdp;PACs;physical access control systems
Slug: 2023-08-09-next-gen-osdp-was-supposed-to-make-it-harder-to-break-in-to-secure-facilities-it-failed

[Source](https://arstechnica.com/?p=1959810){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Researchers have discovered a suite of vulnerabilities that largely break a next-generation protocol that was designed to prevent the hacking of access control systems used at secure facilities on US military bases and buildings belonging to federal, state, and local governments and private organizations. The next-generation mechanism, known as Secure Channel, was added about 10 years ago to an open standard known as OSDP, short for the Open Supervised Device Protocol. Like an earlier protocol, known as Wiegand, OSDP provides a framework for connecting card readers, fingerprint scanners, and other types of peripheral devices to control [...]
