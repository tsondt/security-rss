Title: Rust devs push back as Serde project ships precompiled binaries
Date: 2023-08-19T09:55:57-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-08-19-rust-devs-push-back-as-serde-project-ships-precompiled-binaries

[Source](https://www.bleepingcomputer.com/news/security/rust-devs-push-back-as-serde-project-ships-precompiled-binaries/){:target="_blank" rel="noopener"}

> Serde, a popular Rust (de)serialization project, has decided to ship its serde_derive macro as a precompiled binary. This has generated a fair amount of concern among some developers who highlight the future legal and technical issues this may pose, along with a potential for supply chain attacks. [...]
