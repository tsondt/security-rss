Title: Hackers ask $120,000 for access to multi-billion auction house
Date: 2023-08-17T16:17:18-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-08-17-hackers-ask-120000-for-access-to-multi-billion-auction-house

[Source](https://www.bleepingcomputer.com/news/security/hackers-ask-120-000-for-access-to-multi-billion-auction-house/){:target="_blank" rel="noopener"}

> Hackers have breached the network of a major auction house and offered access to whoever was willing to pay $120,000. [...]
