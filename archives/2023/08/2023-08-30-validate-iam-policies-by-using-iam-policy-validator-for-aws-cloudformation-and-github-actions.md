Title: Validate IAM policies by using IAM Policy Validator for AWS CloudFormation and GitHub Actions
Date: 2023-08-30T13:04:28+00:00
Author: Mitch Beaumont
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Technical How-to;AWS IAM;AWS Identity and Access Management;AWS Identity and Access Management (IAM);Devops;DevSecOps;IAM;IAM Access Analyzer;Identity and Access Management;Security Blog;Web identity federation
Slug: 2023-08-30-validate-iam-policies-by-using-iam-policy-validator-for-aws-cloudformation-and-github-actions

[Source](https://aws.amazon.com/blogs/security/validate-iam-policies-by-using-iam-policy-validator-for-aws-cloudformation-and-github-actions/){:target="_blank" rel="noopener"}

> In this blog post, I’ll show you how to automate the validation of AWS Identity and Access Management (IAM) policies by using a combination of the IAM Policy Validator for AWS CloudFormation (cfn-policy-validator) and GitHub Actions. Policy validation is an approach that is designed to minimize the deployment of unwanted IAM identity-based and resource-based policies [...]
