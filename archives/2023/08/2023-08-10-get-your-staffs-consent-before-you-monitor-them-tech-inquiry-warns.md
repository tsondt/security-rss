Title: Get your staff's consent before you monitor them, tech inquiry warns
Date: 2023-08-10T10:00:08+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-08-10-get-your-staffs-consent-before-you-monitor-them-tech-inquiry-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/10/workplace_monitoring_select_committee/){:target="_blank" rel="noopener"}

> Plus: British government's push to reform data protection is working against the cause Companies that monitor their employees should only do so after they consult with and get consent from the staffers they are watching or tracking.... [...]
