Title: You Can’t Rush Post-Quantum-Computing Cryptography Standards
Date: 2023-08-08T11:13:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;encryption;national security policy;NIST;quantum computing;security standards
Slug: 2023-08-08-you-cant-rush-post-quantum-computing-cryptography-standards

[Source](https://www.schneier.com/blog/archives/2023/08/you-cant-rush-post-quantum-computing-standards.html){:target="_blank" rel="noopener"}

> I just read an article complaining that NIST is taking too long in finalizing its post-quantum-computing cryptography standards. This process has been going on since 2016, and since that time there has been a huge increase in quantum technology and an equally large increase in quantum understanding and interest. Yet seven years later, we have only four algorithms, although last week NIST announced that a number of other candidates are under consideration, a process that is expected to take “several years. The delay in developing quantum-resistant algorithms is especially troubling given the time it will take to get those products [...]
