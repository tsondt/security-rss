Title: Vietnam admits it has just ten percent of the infosec pros it needs
Date: 2023-08-17T02:59:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-08-17-vietnam-admits-it-has-just-ten-percent-of-the-infosec-pros-it-needs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/17/vietnam_infosec_shortage_data_crackdown/){:target="_blank" rel="noopener"}

> Which is a problem, because local orgs are leaking data and shadowy traders are cashing in Vietnam’s Ministry of Information and Communications has admitted the nation has a vast shortfall of infosec pros.... [...]
