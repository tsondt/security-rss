Title: LinkedIn accounts hacked in widespread hijacking campaign
Date: 2023-08-15T17:21:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-15-linkedin-accounts-hacked-in-widespread-hijacking-campaign

[Source](https://www.bleepingcomputer.com/news/security/linkedin-accounts-hacked-in-widespread-hijacking-campaign/){:target="_blank" rel="noopener"}

> LinkedIn is being targeted in a wave of account hacks resulting in many accounts being locked out for security reasons or ultimately hijacked by attackers. [...]
