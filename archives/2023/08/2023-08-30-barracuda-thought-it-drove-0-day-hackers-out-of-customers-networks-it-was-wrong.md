Title: Barracuda thought it drove 0-day hackers out of customers’ networks. It was wrong.
Date: 2023-08-30T17:31:04+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;Barracuda;email security gateway;esg;hacking;unc4841
Slug: 2023-08-30-barracuda-thought-it-drove-0-day-hackers-out-of-customers-networks-it-was-wrong

[Source](https://arstechnica.com/?p=1964217){:target="_blank" rel="noopener"}

> Enlarge (credit: Steve McDowell / Agefotostock ) In late May, researchers drove out a team of China state hackers who over the previous seven months had exploited a critical vulnerability that gave them backdoors into the networks of a who’s who of sensitive organizations. Barracuda, the security vendor whose Email Security Gateway was being exploited, had deployed a patch starting on May 18, and a few days later, a script was designed to eradicate the hackers, who in some cases had enjoyed backdoor access since the previous October. But the attackers had other plans. Unbeknownst to Barracuda and researchers at [...]
