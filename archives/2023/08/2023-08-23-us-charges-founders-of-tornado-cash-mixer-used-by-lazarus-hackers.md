Title: US charges founders of Tornado Cash mixer used by Lazarus hackers
Date: 2023-08-23T13:39:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-23-us-charges-founders-of-tornado-cash-mixer-used-by-lazarus-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-charges-founders-of-tornado-cash-mixer-used-by-lazarus-hackers/){:target="_blank" rel="noopener"}

> The U.S. Justice Department charged two Tornado Cash founders with helping criminals, including the notorious North Korean Lazarus hacking group, launder over $1 billion worth of stolen cryptocurrency through their decentralized crypto mixing service. [...]
