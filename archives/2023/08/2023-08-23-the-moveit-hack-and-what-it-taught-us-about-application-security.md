Title: The MOVEit hack and what it taught us about application security
Date: 2023-08-23T10:01:02-04:00
Author: Sponsored by Outpost24
Category: BleepingComputer
Tags: Security
Slug: 2023-08-23-the-moveit-hack-and-what-it-taught-us-about-application-security

[Source](https://www.bleepingcomputer.com/news/security/the-moveit-hack-and-what-it-taught-us-about-application-security/){:target="_blank" rel="noopener"}

> When a cyberattack like the 2023 MOVEit hack makes global news headlines, attention often focuses on the names of the affected organizations. This article from @Outpost24 overviews the Moveit hack and aims to draw some important actionable takeaways for your business. [...]
