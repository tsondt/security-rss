Title: Threat actors use beta apps to bypass mobile app store security
Date: 2023-08-14T18:13:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile;Software
Slug: 2023-08-14-threat-actors-use-beta-apps-to-bypass-mobile-app-store-security

[Source](https://www.bleepingcomputer.com/news/security/threat-actors-use-beta-apps-to-bypass-mobile-app-store-security/){:target="_blank" rel="noopener"}

> The FBI is warning of a new tactic used by cybercriminals where they promote malicious "beta" versions of cryptocurrency investment apps on popular mobile app stores that are then used to steal crypto. [...]
