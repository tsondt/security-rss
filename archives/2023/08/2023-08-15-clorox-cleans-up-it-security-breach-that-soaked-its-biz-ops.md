Title: Clorox cleans up IT security breach that soaked its biz ops
Date: 2023-08-15T22:22:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-15-clorox-cleans-up-it-security-breach-that-soaked-its-biz-ops

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/15/clorox_cleans_up_security_breach/){:target="_blank" rel="noopener"}

> Plus: Medical records for 4M people within reach of Clop gang after IBM MOVEit deployment hit The Clorox Company has some cleaning up to do as some of its IT systems remain offline and operations "temporarily impaired" following a security breach.... [...]
