Title: FBI warns of increasing cryptocurrency recovery scams
Date: 2023-08-14T13:02:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-14-fbi-warns-of-increasing-cryptocurrency-recovery-scams

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-increasing-cryptocurrency-recovery-scams/){:target="_blank" rel="noopener"}

> The FBI is warning of an increase in scammers pretending to be recovery companies that can help victims of cryptocurrency investment scams recover lost assets. [...]
