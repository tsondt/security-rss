Title: Stalkerware slinger LetMeSpy shuts down for good after database robbery
Date: 2023-08-07T21:12:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-07-stalkerware-slinger-letmespy-shuts-down-for-good-after-database-robbery

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/07/letmespy_shuts_down/){:target="_blank" rel="noopener"}

> If you can't trust a spyware developer with your info, who can you trust? Stalkerware slinger LetMeSpy will shut down for good this month after a miscreant breached its servers and stole a heap of data in June.... [...]
