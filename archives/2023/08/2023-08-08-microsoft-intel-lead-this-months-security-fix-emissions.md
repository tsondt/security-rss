Title: Microsoft, Intel lead this month's security fix emissions
Date: 2023-08-08T23:18:35+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-08-microsoft-intel-lead-this-months-security-fix-emissions

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/08/microsoft_intel_august_patch_tuesday/){:target="_blank" rel="noopener"}

> Downfall processor leaks, Teams holes, VPN clients at risk, and more Patch Tuesday Microsoft's August patch party seems almost boring compared to the other security fires it's been putting out lately.... [...]
