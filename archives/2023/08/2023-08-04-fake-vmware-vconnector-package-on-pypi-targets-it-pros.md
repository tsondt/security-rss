Title: Fake VMware vConnector package on PyPI targets IT pros
Date: 2023-08-04T07:37:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-04-fake-vmware-vconnector-package-on-pypi-targets-it-pros

[Source](https://www.bleepingcomputer.com/news/security/fake-vmware-vconnector-package-on-pypi-targets-it-pros/){:target="_blank" rel="noopener"}

> A malicious package that mimics the VMware vSphere connector module 'vConnector' was uploaded on the Python Package Index (PyPI) under the name 'VMConnect,' targeting IT professionals. [...]
