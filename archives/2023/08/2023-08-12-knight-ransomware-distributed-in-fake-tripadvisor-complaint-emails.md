Title: Knight ransomware distributed in fake Tripadvisor complaint emails
Date: 2023-08-12T11:16:08-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-12-knight-ransomware-distributed-in-fake-tripadvisor-complaint-emails

[Source](https://www.bleepingcomputer.com/news/security/knight-ransomware-distributed-in-fake-tripadvisor-complaint-emails/){:target="_blank" rel="noopener"}

> The Knight ransomware is being distributed in an ongoing spam campaign that pretends to be TripAdvisor complaints. [...]
