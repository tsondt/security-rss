Title: UK voter data within reach of miscreants who hacked Electoral Commission
Date: 2023-08-08T15:52:15+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-08-08-uk-voter-data-within-reach-of-miscreants-who-hacked-electoral-commission

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/08/uk_electoral_commission_hacked_voter/){:target="_blank" rel="noopener"}

> 'It doesn't help if the organization responsible for the integrity of elections' gets pwned The IT infrastructure of the UK's Electoral Commission was broken into by miscreants, who will have had access to names and addresses of voters, as well as the election oversight body's email and unspecified other systems.... [...]
