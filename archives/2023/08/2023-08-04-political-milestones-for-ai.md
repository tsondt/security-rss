Title: Political Milestones for AI
Date: 2023-08-04T11:07:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;ChatGPT;essays;policy
Slug: 2023-08-04-political-milestones-for-ai

[Source](https://www.schneier.com/blog/archives/2023/08/political-milestones-for-ai.html){:target="_blank" rel="noopener"}

> ChatGPT was released just nine months ago, and we are still learning how it will affect our daily lives, our careers, and even our systems of self-governance. But when it comes to how AI may threaten our democracy, much of the public conversation lacks imagination. People talk about the danger of campaigns that attack opponents with fake images (or fake audio or video) because we already have decades of experience dealing with doctored images. We’re on the lookout for foreign governments that spread misinformation because we were traumatized by the 2016 US presidential election. And we worry that AI-generated opinions [...]
