Title: Major U.S. energy org targeted in QR code phishing attack
Date: 2023-08-16T10:16:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-16-major-us-energy-org-targeted-in-qr-code-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/major-us-energy-org-targeted-in-qr-code-phishing-attack/){:target="_blank" rel="noopener"}

> A phishing campaign was observed predominantly targeting a notable energy company in the US, employing QR codes to slip malicious emails into inboxes and bypass security. [...]
