Title: Interpol arrests 14 who allegedly scammed $40m from victims in 'cyber surge'
Date: 2023-08-20T07:18:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-20-interpol-arrests-14-who-allegedly-scammed-40m-from-victims-in-cyber-surge

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/20/interpol_africa_arrests/){:target="_blank" rel="noopener"}

> Cops credit security shops with an assist, tho it's a drop in the ocean An Interpol-led operation arrested 14 suspects and identified 20,674 "suspicious" networks spanning 25 African countries that international cops have linked to more than $40 million in cybercrime losses.... [...]
