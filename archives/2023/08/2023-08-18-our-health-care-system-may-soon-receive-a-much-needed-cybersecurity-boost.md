Title: Our health care system may soon receive a much-needed cybersecurity boost
Date: 2023-08-18T12:00:16+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Health;Security;cybersecurity;department of health;healthcare;healthcare IT;syndication
Slug: 2023-08-18-our-health-care-system-may-soon-receive-a-much-needed-cybersecurity-boost

[Source](https://arstechnica.com/?p=1961745){:target="_blank" rel="noopener"}

> Enlarge (credit: Lorenzo Capunata/Getty ) The Advanced Research Projects Agency for Health (Arpa-H), a research support agency within the United States Department of Health and Human Services, said today that it is launching an initiative to find and help fund the development of cybersecurity technologies that can specifically improve defenses for digital infrastructure in US health care. Dubbed the Digital Health Security project, also known as Digiheals, the effort will allow researchers and technologists to submit proposals beginning today through September 7 for cybersecurity tools geared specifically to health care systems, hospitals and clinics, and health-related devices. For more than [...]
