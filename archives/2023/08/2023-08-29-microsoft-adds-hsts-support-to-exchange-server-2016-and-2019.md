Title: Microsoft adds HSTS support to Exchange Server 2016 and 2019
Date: 2023-08-29T12:19:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-08-29-microsoft-adds-hsts-support-to-exchange-server-2016-and-2019

[Source](https://www.bleepingcomputer.com/news/security/microsoft-adds-hsts-support-to-exchange-server-2016-and-2019/){:target="_blank" rel="noopener"}

> Microsoft announced today that Exchange Server 2016 and 2019 now come with support for HTTP Strict Transport Security (also known as HSTS). [...]
