Title: U.S. Hacks QakBot, Quietly Removes Botnet Infections
Date: 2023-08-29T18:35:25+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Ransomware;The Coming Storm;DOJ;Don Alway;fbi;Federal Bureau of Investigation;Martin Estrada;Qakbot;Qbot;U.S. Department of Justice
Slug: 2023-08-29-us-hacks-qakbot-quietly-removes-botnet-infections

[Source](https://krebsonsecurity.com/2023/08/u-s-hacks-qakbot-quietly-removes-botnet-infections/){:target="_blank" rel="noopener"}

> The U.S. government today announced a coordinated crackdown against QakBot, a complex malware family used by multiple cybercrime groups to lay the groundwork for ransomware infections. The international law enforcement operation involved seizing control over the botnet’s online infrastructure, and quietly removing the Qakbot malware from tens of thousands of infected Microsoft Windows computers. Dutch authorities inside a data center with servers tied to the botnet. Image: Dutch National Police. In an international operation announced today dubbed “ Duck Hunt,” the U.S. Department of Justice (DOJ) and Federal Bureau of Investigation (FBI) said they obtained court orders to remove Qakbot [...]
