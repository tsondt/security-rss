Title: Karma Catches Up to Global Phishing Service 16Shop
Date: 2023-08-17T19:58:56+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;Web Fraud 2.0;16Shop;Akamai;BandungXploiter;Constella Intelligence;Cyberthread.id;Devilscream;fbi;Interpol;mcafee;Riswanda Noor Supatra;Rizky Mauluna Sidik
Slug: 2023-08-17-karma-catches-up-to-global-phishing-service-16shop

[Source](https://krebsonsecurity.com/2023/08/karma-catches-up-to-global-phishing-service-16shop/){:target="_blank" rel="noopener"}

> You’ve probably never heard of “ 16Shop,” but there’s a good chance someone using it has tried to phish you. A 16Shop phishing page spoofing Apple and targeting Japanese users. Image: Akamai.com. The international police organization INTERPOL said last week it had shuttered the notorious 16Shop, a popular phishing-as-a-service platform launched in 2017 that made it simple for even complete novices to conduct complex and convincing phishing scams. INTERPOL said authorities in Indonesia arrested the 21-year-old proprietor and one of his alleged facilitators, and that a third suspect was apprehended in Japan. The INTERPOL statement says the platform sold hacking [...]
