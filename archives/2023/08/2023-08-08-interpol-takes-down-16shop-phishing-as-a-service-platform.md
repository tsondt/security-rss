Title: Interpol takes down 16shop phishing-as-a-service platform
Date: 2023-08-08T13:21:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-08-interpol-takes-down-16shop-phishing-as-a-service-platform

[Source](https://www.bleepingcomputer.com/news/security/interpol-takes-down-16shop-phishing-as-a-service-platform/){:target="_blank" rel="noopener"}

> A joint operation between Interpol and cybersecurity firms has led to an arrest and shutdown of the notorious 16shop phishing-as-a-service (PhaaS) platform. [...]
