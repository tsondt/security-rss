Title: Ransomware hackers dwell time drops to 5 days, RDP still widely used
Date: 2023-08-24T14:18:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-24-ransomware-hackers-dwell-time-drops-to-5-days-rdp-still-widely-used

[Source](https://www.bleepingcomputer.com/news/security/ransomware-hackers-dwell-time-drops-to-5-days-rdp-still-widely-used/){:target="_blank" rel="noopener"}

> Ransomware threat actors are spending less time on compromised networks before security solutions sound the alarm. In the first half of the year the hackers' median dwell time dropped to five days from nine in 2022 [...]
