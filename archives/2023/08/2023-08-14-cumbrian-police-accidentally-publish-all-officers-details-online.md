Title: Cumbrian Police accidentally publish all officers' details online
Date: 2023-08-14T11:38:43+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-08-14-cumbrian-police-accidentally-publish-all-officers-details-online

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/14/cumbrian_police_accidentally_published_officer_details_online/){:target="_blank" rel="noopener"}

> Names, job titles and salaries included in unwitting leak Cumbria Constabulary inadvertently published the names and salaries of all its officers and staff online earlier this year, making it the second UK force in a fortnight to admit disclosing personal information about its employees.... [...]
