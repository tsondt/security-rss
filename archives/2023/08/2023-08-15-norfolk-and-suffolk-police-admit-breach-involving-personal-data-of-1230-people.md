Title: Norfolk and Suffolk police admit breach involving personal data of 1,230 people
Date: 2023-08-15T12:08:45+00:00
Author: Vikram Dodd Police and crime correspondent
Category: The Guardian
Tags: Police;Norfolk;UK news;Freedom of information;Crime;Suffolk;Data and computer security
Slug: 2023-08-15-norfolk-and-suffolk-police-admit-breach-involving-personal-data-of-1230-people

[Source](https://www.theguardian.com/uk-news/2023/aug/15/norfolk-and-suffolk-police-identify-data-breach){:target="_blank" rel="noopener"}

> Information about victims of crime, witnesses and suspects included with freedom of information responses, forces say Two police forces in England have admitted mishandling the sensitive data of victims, witnesses and suspects in cases including domestic abuse incidents, sexual offences, assaults, thefts and hate crime. Norfolk and Suffolk police said the data of 1,230 people was included in files responding to freedom of information requests and apologised. Continue reading... [...]
