Title: The Inability to Simultaneously Verify Sentience, Location, and Identity
Date: 2023-08-11T11:08:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;identification;trust
Slug: 2023-08-11-the-inability-to-simultaneously-verify-sentience-location-and-identity

[Source](https://www.schneier.com/blog/archives/2023/08/the-inability-to-simultaneously-verify-sentience-location-and-identity.html){:target="_blank" rel="noopener"}

> Really interesting “systematization of knowledge” paper : “SoK: The Ghost Trilemma” Abstract: Trolls, bots, and sybils distort online discourse and compromise the security of networked platforms. User identity is central to the vectors of attack and manipulation employed in these contexts. However it has long seemed that, try as it might, the security community has been unable to stem the rising tide of such problems. We posit the Ghost Trilemma, that there are three key properties of identity—sentience, location, and uniqueness—that cannot be simultaneously verified in a fully-decentralized setting. Many fully-decentralized systems—whether for communication or social coordination—grapple with this trilemma [...]
