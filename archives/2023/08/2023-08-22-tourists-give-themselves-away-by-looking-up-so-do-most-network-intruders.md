Title: Tourists Give Themselves Away by Looking Up. So Do Most Network Intruders.
Date: 2023-08-22T17:45:28+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ransomware;Security Tools;The Coming Storm;Assetnote;Canary Tokens;Cisco Talos;Haroon Meer;Hazel Burton;Shubham Shah;Thinkst
Slug: 2023-08-22-tourists-give-themselves-away-by-looking-up-so-do-most-network-intruders

[Source](https://krebsonsecurity.com/2023/08/tourists-give-themselves-away-by-looking-up-so-do-most-network-intruders/){:target="_blank" rel="noopener"}

> In large metropolitan areas, tourists are often easy to spot because they’re far more inclined than locals to gaze upward at the surrounding skyscrapers. Security experts say this same tourist dynamic is a dead giveaway in virtually all computer intrusions that lead to devastating attacks like data theft and ransomware, and that more organizations should set simple virtual tripwires that sound the alarm when authorized users and devices are spotted exhibiting this behavior. In a blog post published last month, Cisco Talos said it was seeing a worrisome “increase in the rate of high-sophistication attacks on network infrastructure.” Cisco’s warning [...]
