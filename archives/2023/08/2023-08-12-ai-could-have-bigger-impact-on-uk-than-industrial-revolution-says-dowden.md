Title: AI could have bigger impact on UK than Industrial Revolution, says Dowden
Date: 2023-08-12T12:24:46+00:00
Author: Tobi Thomas
Category: The Guardian
Tags: Artificial intelligence (AI);Oliver Dowden;Politics;UK news;Cyberwar;Computing;Hacking;Data and computer security;Data protection;Cybercrime
Slug: 2023-08-12-ai-could-have-bigger-impact-on-uk-than-industrial-revolution-says-dowden

[Source](https://www.theguardian.com/technology/2023/aug/12/ai-could-have-bigger-impact-on-uk-than-industrial-revolution-says-dowden){:target="_blank" rel="noopener"}

> Deputy PM says technology may aid faster government decisions – but warns of massive hacking risks Artificial intelligence could have a more significant impact on Britain than the Industrial Revolution, the deputy prime minister has said, but warned it could be used by hackers to access sensitive information from the government. Oliver Dowden said AI could speed up productivity and perform boring aspects of jobs. Continue reading... [...]
