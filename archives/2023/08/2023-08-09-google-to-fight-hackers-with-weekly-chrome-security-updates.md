Title: Google to fight hackers with weekly Chrome security updates
Date: 2023-08-09T11:30:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-08-09-google-to-fight-hackers-with-weekly-chrome-security-updates

[Source](https://www.bleepingcomputer.com/news/google/google-to-fight-hackers-with-weekly-chrome-security-updates/){:target="_blank" rel="noopener"}

> Google has changed the Google Chrome security updates schedule from bi-weekly to weekly to address the growing patch gap problem that allows threat actors extra time to exploit published n-day and zero-day flaws. [...]
