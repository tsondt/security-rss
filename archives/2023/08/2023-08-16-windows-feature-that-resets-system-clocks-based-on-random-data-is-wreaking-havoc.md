Title: Windows feature that resets system clocks based on random data is wreaking havoc
Date: 2023-08-16T17:23:29+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;Certificates;secure time seeding;ssl;system clock;Windows
Slug: 2023-08-16-windows-feature-that-resets-system-clocks-based-on-random-data-is-wreaking-havoc

[Source](https://arstechnica.com/?p=1961136){:target="_blank" rel="noopener"}

> Enlarge A few months ago, an engineer in a data center in Norway encountered some perplexing errors that caused a Windows server to suddenly reset its system clock to 55 days in the future. The engineer relied on the server to maintain a routing table that tracked cell phone numbers in real time as they moved from one carrier to the other. A jump of eight weeks had dire consequences because it caused numbers that had yet to be transferred to be listed as having already been moved and numbers that had already been transferred to be reported as pending. [...]
