Title: How Malicious Android Apps Slip Into Disguise
Date: 2023-08-03T11:22:55+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Web Fraud 2.0;Aleksandr Eremin;Anatsa;Android malware;Ars Technica;Google Play;ThreatFabric
Slug: 2023-08-03-how-malicious-android-apps-slip-into-disguise

[Source](https://krebsonsecurity.com/2023/08/how-malicious-android-apps-slip-into-disguise/){:target="_blank" rel="noopener"}

> Researchers say mobile malware purveyors have been abusing a bug in the Google Android platform that lets them sneak malicious code into mobile apps and evade security scanning tools. Google says it has updated its app malware detection mechanisms in response to the new research. At issue is a mobile malware obfuscation method identified by researchers at ThreatFabric, a security firm based in Amsterdam. Aleksandr Eremin, a senior malware analyst at the company, told KrebsOnSecurity they recently encountered a number of mobile banking trojans abusing a bug present in all Android OS versions that involves corrupting components of an app [...]
