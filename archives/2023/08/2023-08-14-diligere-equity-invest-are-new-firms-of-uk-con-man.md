Title: Diligere, Equity-Invest Are New Firms of U.K. Con Man
Date: 2023-08-14T20:13:22+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ardelis Solutions;Codes2You;Diligere;Equity-Invest;John Bernard;John Clifton Davies;The Inside Knowledge
Slug: 2023-08-14-diligere-equity-invest-are-new-firms-of-uk-con-man

[Source](https://krebsonsecurity.com/2023/08/diligere-equity-invest-are-new-firms-of-u-k-con-man/){:target="_blank" rel="noopener"}

> John Clifton Davies, a convicted fraudster estimated to have bilked dozens of technology startups out of more than $30 million through phony investment schemes, has a brand new pair of scam companies that are busy dashing startup dreams: A fake investment firm called Equity-Invest[.]ch, and Diligere[.]co.uk, a scam due diligence company that Equity-Invest insists all investment partners use. A native of the United Kingdom, Mr. Davies absconded from justice before being convicted on multiple counts of fraud in 2015. Prior to his conviction, Davies served 16 months in jail before being cleared on suspicion of murdering his third wife on [...]
