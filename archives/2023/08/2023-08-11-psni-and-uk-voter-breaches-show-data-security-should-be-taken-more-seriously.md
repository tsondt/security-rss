Title: PSNI and UK voter breaches show data security should be taken more seriously
Date: 2023-08-11T11:32:54+00:00
Author: Dan Milmo and Rory Carroll
Category: The Guardian
Tags: Data and computer security;Data protection;Freedom of information;Northern Ireland;Technology;Internet safety;UK news;Electoral Commission;Politics
Slug: 2023-08-11-psni-and-uk-voter-breaches-show-data-security-should-be-taken-more-seriously

[Source](https://www.theguardian.com/technology/2023/aug/11/psni-voter-breaches-data-risks-taken-more-seriously){:target="_blank" rel="noopener"}

> There were 9,000 breaches of personal information last year but experts say not enough is being done to stop them “It’s brutal. People are wondering if they should resign, or move house, or get fortified gates. You can feel the anger.” The comment from a former officer in the Police Service of Northern Ireland (PSNI) underlines the real-life consequences of an all-too frequent occurrence: a data breach. Continue reading... [...]
