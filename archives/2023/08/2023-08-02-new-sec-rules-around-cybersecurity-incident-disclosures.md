Title: New SEC Rules around Cybersecurity Incident Disclosures
Date: 2023-08-02T11:04:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cybersecurity;disclosure;national security policy;risk assessment;risks
Slug: 2023-08-02-new-sec-rules-around-cybersecurity-incident-disclosures

[Source](https://www.schneier.com/blog/archives/2023/08/new-sec-rules-around-cybersecurity-incident-disclosures.html){:target="_blank" rel="noopener"}

> The US Securities and Exchange Commission adopted final rules around the disclosure of cybersecurity incidents. There are two basic rules: Public companies must “disclose any cybersecurity incident they determine to be material” within four days, with potential delays if there is a national security risk. Public companies must “describe their processes, if any, for assessing, identifying, and managing material risks from cybersecurity threats” in their annual filings. The rules go into effect this December. In an email newsletter, Melissa Hathaway wrote: Now that the rule is final, companies have approximately six months to one year to document and operationalize the [...]
