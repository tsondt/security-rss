Title: MalDoc in PDFs: Hiding malicious Word docs in PDF files
Date: 2023-08-28T16:32:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-28-maldoc-in-pdfs-hiding-malicious-word-docs-in-pdf-files

[Source](https://www.bleepingcomputer.com/news/security/maldoc-in-pdfs-hiding-malicious-word-docs-in-pdf-files/){:target="_blank" rel="noopener"}

> Japan's computer emergency response team (JPCERT) is sharing a new 'MalDoc in PDF' attack detected in July 2023 that bypasses detection by embedding malicious Word files into PDFs. [...]
