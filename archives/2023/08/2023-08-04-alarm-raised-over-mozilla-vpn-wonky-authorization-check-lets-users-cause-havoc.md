Title: Alarm raised over Mozilla VPN: Wonky authorization check lets users cause havoc
Date: 2023-08-04T19:48:32+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-08-04-alarm-raised-over-mozilla-vpn-wonky-authorization-check-lets-users-cause-havoc

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/04/mozilla_vpn_linux_flaw/){:target="_blank" rel="noopener"}

> SUSE security engineer goes public on unfixed client hole after disclosure drama A security engineer at Linux distro maker SUSE has published an advisory for a flaw in the Mozilla VPN client for Linux that has yet to be addressed in a publicly released fix because the disclosure process went off the rails.... [...]
