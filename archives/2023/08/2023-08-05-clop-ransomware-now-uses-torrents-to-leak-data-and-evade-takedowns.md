Title: Clop ransomware now uses torrents to leak data and evade takedowns
Date: 2023-08-05T11:16:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-05-clop-ransomware-now-uses-torrents-to-leak-data-and-evade-takedowns

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-now-uses-torrents-to-leak-data-and-evade-takedowns/){:target="_blank" rel="noopener"}

> The Clop ransomware gang has once again altered extortion tactics and is now using torrents to leak data stolen in MOVEit attacks. [...]
