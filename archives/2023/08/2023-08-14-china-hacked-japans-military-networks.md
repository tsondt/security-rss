Title: China Hacked Japan’s Military Networks
Date: 2023-08-14T11:02:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;cyberespionage;cybersecurity;espionage;hacking;Japan;military
Slug: 2023-08-14-china-hacked-japans-military-networks

[Source](https://www.schneier.com/blog/archives/2023/08/china-hacked-japans-military-networks.html){:target="_blank" rel="noopener"}

> The NSA discovered the intrusion in 2020—we don’t know how—and alerted the Japanese. The Washington Post has the story : The hackers had deep, persistent access and appeared to be after anything they could get their hands on—plans, capabilities, assessments of military shortcomings, according to three former senior U.S. officials, who were among a dozen current and former U.S. and Japanese officials interviewed, who spoke on the condition of anonymity because of the matter’s sensitivity. [...] The 2020 penetration was so disturbing that Gen. Paul Nakasone, the head of the NSA and U.S. Cyber Command, and Matthew Pottinger, who was [...]
