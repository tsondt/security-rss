Title: Zoom Can Spy on Your Calls and Use the Conversation to Train AI, But Says That It Won’t
Date: 2023-08-15T11:03:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;privacy;surveillance;terms of service;videoconferencing
Slug: 2023-08-15-zoom-can-spy-on-your-calls-and-use-the-conversation-to-train-ai-but-says-that-it-wont

[Source](https://www.schneier.com/blog/archives/2023/08/zoom-can-spy-on-your-calls-and-use-the-conversation-to-train-ai-but-says-that-it-wont.html){:target="_blank" rel="noopener"}

> This is why we need regulation: Zoom updated its Terms of Service in March, spelling out that the company reserves the right to train AI on user data with no mention of a way to opt out. On Monday, the company said in a blog post that there’s no need to worry about that. Zoom execs swear the company won’t actually train its AI on your video calls without permission, even though the Terms of Service still say it can. Of course, these are Terms of Service. They can change at any time. Zoom can renege on its promise at [...]
