Title: Discord.io confirms breach after hacker steals data of 760K users
Date: 2023-08-14T17:40:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-14-discordio-confirms-breach-after-hacker-steals-data-of-760k-users

[Source](https://www.bleepingcomputer.com/news/security/discordio-confirms-breach-after-hacker-steals-data-of-760k-users/){:target="_blank" rel="noopener"}

> The Discord.io custom invite service has temporarily shut down after suffering a data breach exposing the information of 760,000 members. [...]
