Title: Cost considerations and common options for AWS Network Firewall log management
Date: 2023-08-14T18:47:47+00:00
Author: Sharon Li
Category: AWS Security
Tags: Advanced (300);AWS Network Firewall;Security, Identity, & Compliance;Thought Leadership;Security Blog
Slug: 2023-08-14-cost-considerations-and-common-options-for-aws-network-firewall-log-management

[Source](https://aws.amazon.com/blogs/security/cost-considerations-and-common-options-for-aws-network-firewall-log-management/){:target="_blank" rel="noopener"}

> When you’re designing a security strategy for your organization, firewalls provide the first line of defense against threats. Amazon Web Services (AWS) offers AWS Network Firewall, a stateful, managed network firewall that includes intrusion detection and prevention (IDP) for your Amazon Virtual Private Cloud (VPC). Logging plays a vital role in any firewall policy, as [...]
