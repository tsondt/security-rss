Title: Missouri warns that health info was stolen in IBM MOVEit data breach
Date: 2023-08-09T15:50:57-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-09-missouri-warns-that-health-info-was-stolen-in-ibm-moveit-data-breach

[Source](https://www.bleepingcomputer.com/news/security/missouri-warns-that-health-info-was-stolen-in-ibm-moveit-data-breach/){:target="_blank" rel="noopener"}

> Missouri's Department of Social Services warns that protected Medicaid healthcare information was exposed in a data breach after IBM suffered a MOVEit data theft attack. [...]
