Title: How to Connect Your On-Premises Active Directory to AWS Using AD Connector
Date: 2023-08-17T16:36:48+00:00
Author: Jeremy Cowan
Category: AWS Security
Tags: Advanced (300);AWS Directory Service;How-To;Security, Identity, & Compliance;AD Connector;Best of;Federation;Security Blog;SSO
Slug: 2023-08-17-how-to-connect-your-on-premises-active-directory-to-aws-using-ad-connector

[Source](https://aws.amazon.com/blogs/security/how-to-connect-your-on-premises-active-directory-to-aws-using-ad-connector/){:target="_blank" rel="noopener"}

> August 17, 2023: We updated the instructions and screenshots in this post to align with changes to the AWS Management Console. April 25, 2023: We’ve updated this blog post to include more security learning resources. AD Connector is designed to give you an easy way to establish a trusted relationship between your Active Directory and [...]
