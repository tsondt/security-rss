Title: Data breach at French govt agency exposes info of 10 million people
Date: 2023-08-25T13:01:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-25-data-breach-at-french-govt-agency-exposes-info-of-10-million-people

[Source](https://www.bleepingcomputer.com/news/security/data-breach-at-french-govt-agency-exposes-info-of-10-million-people/){:target="_blank" rel="noopener"}

> Pôle emploi, France's governmental unemployment registration and financial aid agency, is informing of a data breach that exposed data belonging to 10 million individuals. [...]
