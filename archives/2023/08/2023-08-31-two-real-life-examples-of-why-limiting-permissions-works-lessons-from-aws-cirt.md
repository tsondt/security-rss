Title: Two real-life examples of why limiting permissions works: Lessons from AWS CIRT
Date: 2023-08-31T14:03:48+00:00
Author: Richard Billington
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;AWS Incident Response;Incident response;least privilege;Security Blog;Threat Detection & Incident Response
Slug: 2023-08-31-two-real-life-examples-of-why-limiting-permissions-works-lessons-from-aws-cirt

[Source](https://aws.amazon.com/blogs/security/two-real-life-examples-of-why-limiting-permissions-works-lessons-from-aws-cirt/){:target="_blank" rel="noopener"}

> Welcome to another blog post from the AWS Customer Incident Response Team (CIRT)! For this post, we’re looking at two events that the team was involved in from the viewpoint of a regularly discussed but sometimes misunderstood subject, least privilege. Specifically, we consider the idea that the benefit of reducing permissions in real-life use cases [...]
