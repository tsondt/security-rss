Title: Leak of 75k employee records was insiders' fault, claims Tesla
Date: 2023-08-21T17:35:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-08-21-leak-of-75k-employee-records-was-insiders-fault-claims-tesla

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/21/breach_of_75k_employee_records/){:target="_blank" rel="noopener"}

> Identity Access Management? What's that? Insiders are to blame for a May data breach at Tesla, the company claimed in filings after news of the incident was reported months ago by German media.... [...]
