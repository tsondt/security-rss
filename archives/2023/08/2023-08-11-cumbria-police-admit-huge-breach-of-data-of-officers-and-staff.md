Title: Cumbria police admit huge breach of data of officers and staff
Date: 2023-08-11T13:07:49+00:00
Author: Vikram Dodd Police and crime correspondent
Category: The Guardian
Tags: Police;Cumbria;UK news;Data and computer security;Data protection;Privacy;Information commissioner
Slug: 2023-08-11-cumbria-police-admit-huge-breach-of-data-of-officers-and-staff

[Source](https://www.theguardian.com/uk-news/2023/aug/11/cumbria-police-admits-huge-breach-of-data-of-officers-and-staff){:target="_blank" rel="noopener"}

> Exclusive: Accidental publishing of names and salaries happened in March and follows scandal over PSNI leak Another British police force has experienced a huge breach of the data of all its officers and staff, the Guardian has learned. Cumbria police has admitted accidentally publishing the names and salaries of every one of its more than 2,000 employees and has apologised. Continue reading... [...]
