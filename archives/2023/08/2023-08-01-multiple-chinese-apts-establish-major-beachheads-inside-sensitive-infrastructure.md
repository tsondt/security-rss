Title: Multiple Chinese APTs establish major beachheads inside sensitive infrastructure
Date: 2023-08-01T12:29:14+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;advanced persistent threat;APT;apt31;china;microsoft;zirconium
Slug: 2023-08-01-multiple-chinese-apts-establish-major-beachheads-inside-sensitive-infrastructure

[Source](https://arstechnica.com/?p=1958000){:target="_blank" rel="noopener"}

> Enlarge (credit: Steve McDowell / Agefotostock ) Hacking teams working for the Chinese government are intent on burrowing into the farthest reaches of sensitive infrastructure, much of it belonging to the US, and establishing permanent presences there if possible. In the past two years, they have scored some wins that could seriously threaten national security. If that wasn’t clear before, three reports released in the past week make it abundantly so. In one published by security firm Kaspersky, researchers detailed a suite of advanced spying tools used over the past two years by one group to establish a “permanent channel [...]
