Title: Bitwarden releases free and open-source E2EE Secrets Manager
Date: 2023-08-23T15:04:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-08-23-bitwarden-releases-free-and-open-source-e2ee-secrets-manager

[Source](https://www.bleepingcomputer.com/news/security/bitwarden-releases-free-and-open-source-e2ee-secrets-manager/){:target="_blank" rel="noopener"}

> Bitwarden, the maker of the popular open-source password manager tool, has released 'Secrets Manager,' an end-to-end encrypted secrets manager for IT professionals, software development teams, and the DevOps industry. [...]
