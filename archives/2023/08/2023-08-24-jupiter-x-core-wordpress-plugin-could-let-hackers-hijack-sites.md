Title: Jupiter X Core WordPress plugin could let hackers hijack sites
Date: 2023-08-24T13:26:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-24-jupiter-x-core-wordpress-plugin-could-let-hackers-hijack-sites

[Source](https://www.bleepingcomputer.com/news/security/jupiter-x-core-wordpress-plugin-could-let-hackers-hijack-sites/){:target="_blank" rel="noopener"}

> Two vulnerabilities affecting some version of Jupiter X Core, a premium plugin for setting up WordPress and WooCommerce websites, allow hijacking accounts and uploading files without authentication. [...]
