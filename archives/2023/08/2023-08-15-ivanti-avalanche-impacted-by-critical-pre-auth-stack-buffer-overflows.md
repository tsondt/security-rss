Title: Ivanti Avalanche impacted by critical pre-auth stack buffer overflows
Date: 2023-08-15T18:05:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-15-ivanti-avalanche-impacted-by-critical-pre-auth-stack-buffer-overflows

[Source](https://www.bleepingcomputer.com/news/security/ivanti-avalanche-impacted-by-critical-pre-auth-stack-buffer-overflows/){:target="_blank" rel="noopener"}

> Two stack-based buffer overflows collectively tracked as CVE-2023-32560 impact Ivanti Avalanche, an enterprise mobility management (EMM) solution designed to manage, monitor, and secure a wide range of mobile devices. [...]
