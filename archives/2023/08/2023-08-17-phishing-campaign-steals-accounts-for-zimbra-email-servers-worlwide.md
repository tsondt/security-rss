Title: Phishing campaign steals accounts for Zimbra email servers worlwide
Date: 2023-08-17T13:22:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-17-phishing-campaign-steals-accounts-for-zimbra-email-servers-worlwide

[Source](https://www.bleepingcomputer.com/news/security/phishing-campaign-steals-accounts-for-zimbra-email-servers-worlwide/){:target="_blank" rel="noopener"}

> An ongoing phishing campaign has been underway since at least April 2023 that attempts to steal credentials for Zimbra Collaboration email servers worldwide. [...]
