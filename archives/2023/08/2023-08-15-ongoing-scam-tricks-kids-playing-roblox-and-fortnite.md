Title: Ongoing scam tricks kids playing Roblox and Fortnite
Date: 2023-08-15T20:57:35+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Gaming;Security;fortnite;roblox;scam;syndication
Slug: 2023-08-15-ongoing-scam-tricks-kids-playing-roblox-and-fortnite

[Source](https://arstechnica.com/?p=1961085){:target="_blank" rel="noopener"}

> Enlarge (credit: Savusia Konstantin | Getty Images ) Thousands of websites belonging to US government agencies, leading universities, and professional organizations have been hijacked over the last half decade and used to push scammy offers and promotions, new research has found. Many of these scams are aimed at children and attempt to trick them into downloading apps, malware, or submitting personal details in exchange for nonexistent rewards in Fortnite and Roblox. For more than three years, security researcher Zach Edwards has been tracking these website hijackings and scams. He says the activity can be linked back to the activities of [...]
