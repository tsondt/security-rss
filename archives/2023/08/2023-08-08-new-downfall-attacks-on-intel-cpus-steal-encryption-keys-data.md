Title: New Downfall attacks on Intel CPUs steal encryption keys, data
Date: 2023-08-08T13:00:00-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-08-08-new-downfall-attacks-on-intel-cpus-steal-encryption-keys-data

[Source](https://www.bleepingcomputer.com/news/security/new-downfall-attacks-on-intel-cpus-steal-encryption-keys-data/){:target="_blank" rel="noopener"}

> A senior research scientist at Google has devised new CPU attacks to exploit a vulnerability dubbed Downfall that affects multiple Intel microprocessor families and allows stealing passwords, encryption keys, and private data like emails, messages, or banking info from users that share the same computer. [...]
