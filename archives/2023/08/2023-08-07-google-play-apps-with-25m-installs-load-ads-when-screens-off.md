Title: Google Play apps with 2.5M installs load ads when screen's off
Date: 2023-08-07T11:50:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-08-07-google-play-apps-with-25m-installs-load-ads-when-screens-off

[Source](https://www.bleepingcomputer.com/news/security/google-play-apps-with-25m-installs-load-ads-when-screens-off/){:target="_blank" rel="noopener"}

> The Google Play store was infiltrated by 43 Android applications with 2.5 million installs that secretly displayed advertisements while a phone's screen was off, running down a device's battery. [...]
