Title: Google’s $30-per-month “Duet” AI will craft awkward emails, images for you
Date: 2023-08-29T20:27:02+00:00
Author: Benj Edwards
Category: Ars Technica
Tags: AI;Biz & IT;Google;Tech;AI ethics;AI privacy;ChatGPT;chatgtp;google;Google Bard;Google docs;google workspace;Goolge Duet;large language models;machine learning;openai;PaLM 2;privacy
Slug: 2023-08-29-googles-30-per-month-duet-ai-will-craft-awkward-emails-images-for-you

[Source](https://arstechnica.com/?p=1964020){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images / Benj Edwards ) On Tuesday, Google announced the launch of its Duet AI assistant across its Workspace apps, including Docs, Gmail, Drive, Slides, and more. First announced in May at Google I/O, Duet has been in testing for some time, but it is now available to paid Google Workspace business users (what Google calls its suite of cloud productivity apps) for $30 a month in addition to regular Workspace fees. Duet is not just one thing—instead, it's a blanket brand name for a multitude of different AI capabilities and probably should have been called "Google [...]
