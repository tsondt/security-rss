Title: Ongoing Duo outage causes Azure Auth authentication errors
Date: 2023-08-21T12:26:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Technology;Security
Slug: 2023-08-21-ongoing-duo-outage-causes-azure-auth-authentication-errors

[Source](https://www.bleepingcomputer.com/news/technology/ongoing-duo-outage-causes-azure-auth-authentication-errors/){:target="_blank" rel="noopener"}

> Cisco-owned multi-factor authentication (MFA) provider Duo Security is investigating an ongoing outage that has been causing authentication failures and errors starting three hours ago. [...]
