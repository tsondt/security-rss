Title: Apple opens 2024 applications to get ‘security research’ iPhones
Date: 2023-08-30T16:38:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-08-30-apple-opens-2024-applications-to-get-security-research-iphones

[Source](https://www.bleepingcomputer.com/news/apple/apple-opens-2024-applications-to-get-security-research-iphones/){:target="_blank" rel="noopener"}

> Apple announced today that iOS security researchers can now apply for a Security Research Device (SRD) by the end of October. [...]
