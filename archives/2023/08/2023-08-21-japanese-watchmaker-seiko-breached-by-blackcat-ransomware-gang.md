Title: Japanese watchmaker Seiko breached by BlackCat ransomware gang
Date: 2023-08-21T10:40:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-21-japanese-watchmaker-seiko-breached-by-blackcat-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/japanese-watchmaker-seiko-breached-by-blackcat-ransomware-gang/){:target="_blank" rel="noopener"}

> The BlackCat/ALPHV ransomware gang has added Seiko to its extortion site, claiming responsibility for a cyberattack disclosed by the Japanese firm earlier this month. [...]
