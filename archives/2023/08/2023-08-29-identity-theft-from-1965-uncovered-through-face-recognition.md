Title: Identity Theft from 1965 Uncovered through Face Recognition
Date: 2023-08-29T11:03:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;biometrics;face recognition;fraud;identity theft
Slug: 2023-08-29-identity-theft-from-1965-uncovered-through-face-recognition

[Source](https://www.schneier.com/blog/archives/2023/08/identity-theft-from-1965-uncovered-through-face-recognition.html){:target="_blank" rel="noopener"}

> Interesting story : Napoleon Gonzalez, of Etna, assumed the identity of his brother in 1965, a quarter century after his sibling’s death as an infant, and used the stolen identity to obtain Social Security benefits under both identities, multiple passports and state identification cards, law enforcement officials said. [...] A new investigation was launched in 2020 after facial identification software indicated Gonzalez’s face was on two state identification cards. The facial recognition technology is used by the Maine Bureau of Motor Vehicles to ensure no one obtains multiple credentials or credentials under someone else’s name, said Emily Cook, spokesperson for [...]
