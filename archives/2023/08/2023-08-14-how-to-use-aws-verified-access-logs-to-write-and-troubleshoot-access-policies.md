Title: How to use AWS Verified Access logs to write and troubleshoot access policies
Date: 2023-08-14T14:50:14+00:00
Author: Ankush Goyal
Category: AWS Security
Tags: AWS Verified Access;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon Verified Access;Amazon Web Services;AWS;Security Blog;Zero Trust
Slug: 2023-08-14-how-to-use-aws-verified-access-logs-to-write-and-troubleshoot-access-policies

[Source](https://aws.amazon.com/blogs/security/how-to-use-aws-verified-access-logs-to-write-and-troubleshoot-access-policies/){:target="_blank" rel="noopener"}

> On June 19, 2023, AWS Verified Access introduced improved logging functionality; Verified Access now logs more extensive user context information received from the trust providers. This improved logging feature simplifies administration and troubleshooting of application access policies while adhering to zero-trust principles. In this blog post, we will show you how to manage the Verified Access [...]
