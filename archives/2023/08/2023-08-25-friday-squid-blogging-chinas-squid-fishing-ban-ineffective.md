Title: Friday Squid Blogging: China’s Squid Fishing Ban Ineffective
Date: 2023-08-25T21:06:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-08-25-friday-squid-blogging-chinas-squid-fishing-ban-ineffective

[Source](https://www.schneier.com/blog/archives/2023/08/friday-squid-blogging-chinas-squid-fishing-ban-ineffective.html){:target="_blank" rel="noopener"}

> China imposed a “pilot program banning fishing in parts of the south-west Atlantic Ocean from July to October, and parts of the eastern Pacific Ocean from September to December.” However, the conservation group Oceana analyzed the data and figured out that the Chinese weren’t fishing in those areas in those months, anyway. < blockquote>In the south-west Atlantic moratorium area, Oceana found there had been no fishing conducted by Chinese fleets in the same time period in 2019. Between 1,800 and 8,500 fishing hours were detected in the zone in each of the five years to 2019. In the eastern Pacific [...]
