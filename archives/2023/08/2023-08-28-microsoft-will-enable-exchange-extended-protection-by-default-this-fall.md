Title: Microsoft will enable Exchange Extended Protection by default this fall
Date: 2023-08-28T15:20:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-08-28-microsoft-will-enable-exchange-extended-protection-by-default-this-fall

[Source](https://www.bleepingcomputer.com/news/security/microsoft-will-enable-exchange-extended-protection-by-default-this-fall/){:target="_blank" rel="noopener"}

> Microsoft announced today that Windows Extended Protection will be enabled by default on servers running Exchange Server 2019 starting this fall after installing the 2023 H2 Cumulative Update (CU14). [...]
