Title: The Guardian view on Northern Ireland’s data leak: putting lives at risk | Editorial
Date: 2023-08-10T17:28:02+00:00
Author: Editorial
Category: The Guardian
Tags: Northern Ireland;Police;Data and computer security;Documentary films;Film
Slug: 2023-08-10-the-guardian-view-on-northern-irelands-data-leak-putting-lives-at-risk-editorial

[Source](https://www.theguardian.com/commentisfree/2023/aug/10/the-guardian-view-on-northern-irelands-data-leak-putting-lives-at-risk){:target="_blank" rel="noopener"}

> The police’s error is hard to forgive, but ministers must treat Northern Ireland’s wider human safety needs as a priority too A new documentary film recounts a grim story from the Northern Ireland Troubles. Half a century ago, Thomas Niedermayer was a German businessman living in Belfast. At Christmas in 1973, he was kidnapped from his home by the IRA, possibly to be traded for imprisoned bombers, and murdered. His body was found in a shallow grave in 1980. Ten years on, his widow, Ingeborg, took her own life. A year after that, the Niedermayers’ younger daughter, Renate, killed herself. [...]
