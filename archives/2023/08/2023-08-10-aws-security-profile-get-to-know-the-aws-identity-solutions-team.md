Title: AWS Security Profile: Get to know the AWS Identity Solutions team
Date: 2023-08-10T17:27:26+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Identity;AWS Security Profile;AWS Security Profiles;Security Blog
Slug: 2023-08-10-aws-security-profile-get-to-know-the-aws-identity-solutions-team

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-get-to-know-the-aws-identity-solutions-team/){:target="_blank" rel="noopener"}

> Remek Hetman, Principal Solutions Architect on the Identity Solutions team In this profile, I met with Ilya Epshteyn, Senior Manager of the AWS Identity Solutions team, to chat about his team and what they’re working on. Let’s start with the basics. What does the Identity Solutions team do? We are a team of specialist solutions [...]
