Title: “Downfall” bug affects years of Intel CPUs, can leak encryption keys and more
Date: 2023-08-09T19:12:26+00:00
Author: Andrew Cunningham
Category: Ars Technica
Tags: Biz & IT;Security;Tech;amd epyc;AMD Ryzen;amd threadripper;Intel;intel core;intel cpus;Intel Xeon;server CPUs;vulnerability
Slug: 2023-08-09-downfall-bug-affects-years-of-intel-cpus-can-leak-encryption-keys-and-more

[Source](https://arstechnica.com/?p=1959890){:target="_blank" rel="noopener"}

> Enlarge / An 8th-generation Intel Core desktop CPU, one of several CPU generations affected by the Downfall bug. (credit: Mark Walton) It's a big week for CPU security vulnerabilities. Yesterday, different security researchers published details on two different vulnerabilities, one affecting multiple generations of Intel processors and another affecting the newest AMD CPUs. " Downfall " and " Inception " (respectively) are different bugs, but both involve modern processors' extensive use of speculative execution (a la the original Meltdown and Spectre bugs ), both are described as being of "medium" severity, and both can be patched either with OS-level microcode [...]
