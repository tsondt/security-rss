Title: Hacking campaign bruteforces Cisco VPNs to breach networks
Date: 2023-08-30T12:00:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-30-hacking-campaign-bruteforces-cisco-vpns-to-breach-networks

[Source](https://www.bleepingcomputer.com/news/security/hacking-campaign-bruteforces-cisco-vpns-to-breach-networks/){:target="_blank" rel="noopener"}

> Hackers are targeting Cisco Adaptive Security Appliance (ASA) SSL VPNs in credential stuffing and brute-force attacks that take advantage of lapses in security defenses, such as not enforcing multi-factor authentication (MFA). [...]
