Title: Crypto botnet on X is powered by ChatGPT
Date: 2023-08-22T13:21:52+00:00
Author: WIRED
Category: Ars Technica
Tags: AI;Biz & IT;Security;botnet;ChatGPT;openai;syndication
Slug: 2023-08-22-crypto-botnet-on-x-is-powered-by-chatgpt

[Source](https://arstechnica.com/?p=1962144){:target="_blank" rel="noopener"}

> Enlarge (credit: sakchai vongsasiripat/Getty Image) ChatGPT may well revolutionize web search, streamline office chores, and remake education, but the smooth-talking chatbot has also found work as a social media crypto huckster. Researchers at Indiana University Bloomington discovered a botnet powered by ChatGPT operating on X—the social network formerly known as Twitter—in May of this year. The botnet, which the researchers dub Fox8 because of its connection to cryptocurrency websites bearing some variation of the same name, consisted of 1,140 accounts. Many of them seemed to use ChatGPT to craft social media posts and to reply to each other’s posts. The [...]
