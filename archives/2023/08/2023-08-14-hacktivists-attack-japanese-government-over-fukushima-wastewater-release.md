Title: Hacktivists attack Japanese government over Fukushima wastewater release
Date: 2023-08-14T05:58:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-08-14-hacktivists-attack-japanese-government-over-fukushima-wastewater-release

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/14/hactivitsts_claim_japanese_government_attack/){:target="_blank" rel="noopener"}

> Claiming affiliation with Anonymous, e-hippies want more debate over radioactive flows Entities using the name and iconography of Anonymous (EUTNAIOA) claim to have conducted cyber protests against the Japanese government for actions related to the release of wastewater from the Fukushima Daini Nuclear Power Plant.... [...]
