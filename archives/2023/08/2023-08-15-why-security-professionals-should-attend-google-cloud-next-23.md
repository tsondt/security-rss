Title: Why security professionals should attend Google Cloud Next ‘23
Date: 2023-08-15T16:00:00+00:00
Author: Greg Smith
Category: GCP Security
Tags: Google Cloud Next;Security & Identity
Slug: 2023-08-15-why-security-professionals-should-attend-google-cloud-next-23

[Source](https://cloud.google.com/blog/products/identity-security/why-security-professionals-should-attend-google-cloud-next-23/){:target="_blank" rel="noopener"}

> Google Cloud Next ‘23 is coming to the Moscone Center in San Francisco starting Tuesday, August 29th. This year, we’re featuring a packed Security Professionals track, with more than 40 sessions covering the latest in threat intelligence, modern security operations, and advanced cloud security. Attendees will have the opportunity to meet with the engineers and product leaders building the next generation of cybersecurity solutions. Security executives and practitioners can learn from an exciting lineup of speakers, including experts from Google Cloud and leaders from Goldman Sachs, Mayo Clinic, Snap, Uber, and more. Register here and get ready to defend your [...]
