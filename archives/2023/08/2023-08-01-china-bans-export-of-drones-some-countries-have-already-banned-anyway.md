Title: China bans export of drones some countries have already banned anyway
Date: 2023-08-01T06:00:15+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-08-01-china-bans-export-of-drones-some-countries-have-already-banned-anyway

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/01/china_announces_drone_export_ban/){:target="_blank" rel="noopener"}

> Some say retaliation for sanctions, but Beijing says it just wants world peace China introduced restrictions on Monday that mean would-be exporters will require a license to ship certain drones and related equipment out of the Middle Kingdom.... [...]
