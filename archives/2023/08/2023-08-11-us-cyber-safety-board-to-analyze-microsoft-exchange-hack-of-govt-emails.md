Title: US cyber safety board to analyze Microsoft Exchange hack of govt emails
Date: 2023-08-11T13:35:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;Microsoft
Slug: 2023-08-11-us-cyber-safety-board-to-analyze-microsoft-exchange-hack-of-govt-emails

[Source](https://www.bleepingcomputer.com/news/security/us-cyber-safety-board-to-analyze-microsoft-exchange-hack-of-govt-emails/){:target="_blank" rel="noopener"}

> The Department of Homeland Security's Cyber Safety Review Board (CSRB) has announced plans to conduct an in-depth review of cloud security practices following recent Chinese hacks of Microsoft Exchange accounts used by US government agencies. [...]
