Title: Toyota Japan back on the road after probably-not-cyber attack halted production
Date: 2023-08-30T03:58:54+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-08-30-toyota-japan-back-on-the-road-after-probably-not-cyber-attack-halted-production

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/30/toyota_japan_production_resumes/){:target="_blank" rel="noopener"}

> Malfunction took 14 plants offline for 36 hours. Oh, what a... nah, too obvious Toyota Japan has recovered from what it's described as a "malfunction in the production order system" that halted production on 28 lines across 14 plants starting on Monday evening.... [...]
