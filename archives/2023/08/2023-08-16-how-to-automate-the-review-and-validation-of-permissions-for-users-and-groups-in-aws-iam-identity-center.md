Title: How to automate the review and validation of permissions for users and groups in AWS IAM Identity Center
Date: 2023-08-16T18:37:20+00:00
Author: Yee Fei Ooi
Category: AWS Security
Tags: Advanced (300);AWS IAM Identity Center;Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-08-16-how-to-automate-the-review-and-validation-of-permissions-for-users-and-groups-in-aws-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/how-to-automate-the-review-and-validation-of-permissions-for-users-and-groups-in-aws-iam-identity-center/){:target="_blank" rel="noopener"}

> AWS IAM Identity Center (successor to AWS Single Sign-On) is widely used by organizations to centrally manage federated access to their Amazon Web Services (AWS) environment. As organizations grow, it’s crucial that they maintain control of access to their environment and conduct regular reviews of existing granted permissions to maintain a good security posture. With [...]
