Title: Chronicle CyberShield - Google Cloud’s Approach to Strengthen Nation-wide Cyber Defense
Date: 2023-08-18T21:41:29+00:00
Author: Philip Maurer
Category: GCP Security
Tags: Security & Identity
Slug: 2023-08-18-chronicle-cybershield-google-clouds-approach-to-strengthen-nation-wide-cyber-defense

[Source](https://cloud-blog-transform.corp.google.com/blog/products/identity-security/introducing-chronicle-cybershield/){:target="_blank" rel="noopener"}

> One of the primary functions of any government is to protect its citizens, institutions, infrastructure and way of life. With the rise of the global Internet, the world is more connected and traditional borders do not exist, meaning those same citizens, institutions, infrastructure, and way of life are at greater risk of malicious activity online. The threat profile of many governments has evolved and it is more important than ever to protect and defend critical online services. Of the intrusions investigated by Mandiant in 2022, response efforts for government-related organizations captured 25% of all investigations, compared to 9% in 2021. [...]
