Title: Mom’s Meals discloses data breach impacting 1.2 million people
Date: 2023-08-28T10:24:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-08-28-moms-meals-discloses-data-breach-impacting-12-million-people

[Source](https://www.bleepingcomputer.com/news/security/moms-meals-discloses-data-breach-impacting-12-million-people/){:target="_blank" rel="noopener"}

> PurFoods, which conducts business in the U.S. as 'Mom's Meals,' is warning of a data breach after the personal information of 1.2 million customers and employees was stolen in a ransomware attack. [...]
