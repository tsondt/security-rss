Title: University cuts itself off from internet after mystery security snafu
Date: 2023-08-29T21:37:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-08-29-university-cuts-itself-off-from-internet-after-mystery-security-snafu

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/29/university_michigan_outage/){:target="_blank" rel="noopener"}

> Halls of learning are stuck offline, but go Wolverines! The University of Michigan has isolated itself from the internet but, hey, everything's fine!... [...]
