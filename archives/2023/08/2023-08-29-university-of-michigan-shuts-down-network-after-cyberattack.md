Title: University of Michigan shuts down network after cyberattack
Date: 2023-08-29T10:35:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-08-29-university-of-michigan-shuts-down-network-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/university-of-michigan-shuts-down-network-after-cyberattack/){:target="_blank" rel="noopener"}

> The University of Michigan has taken all of its systems and services offline to deal with a cybersecurity incident, causing a widespread impact on online services the night before classes started. [...]
