Title: Threat actors abuse Google AMP for evasive phishing attacks
Date: 2023-08-01T13:43:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Healthcare
Slug: 2023-08-01-threat-actors-abuse-google-amp-for-evasive-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/threat-actors-abuse-google-amp-for-evasive-phishing-attacks/){:target="_blank" rel="noopener"}

> Security researchers are warning of increased phishing activity that abuses Google Accelerated Mobile Pages (AMP) to bypass email security measures and get to inboxes of enterprise employees. [...]
