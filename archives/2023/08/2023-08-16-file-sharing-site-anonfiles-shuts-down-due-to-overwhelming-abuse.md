Title: File sharing site Anonfiles shuts down due to overwhelming abuse
Date: 2023-08-16T19:25:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-16-file-sharing-site-anonfiles-shuts-down-due-to-overwhelming-abuse

[Source](https://www.bleepingcomputer.com/news/security/file-sharing-site-anonfiles-shuts-down-due-to-overwhelming-abuse/){:target="_blank" rel="noopener"}

> Anonfiles, a popular service for sharing files anonymously, has shut down after saying it can no longer deal with the overwhelming abuse by its users. [...]
