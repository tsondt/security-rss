Title: Hosting firm says it lost all customer data after ransomware attack
Date: 2023-08-23T10:40:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2023-08-23-hosting-firm-says-it-lost-all-customer-data-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/hosting-firm-says-it-lost-all-customer-data-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Danish hosting firms CloudNordic and AzeroCloud have suffered ransomware attacks, causing the loss of the majority of customer data and forcing the hosting providers to shut down all systems, including websites, email, and customer sites. [...]
