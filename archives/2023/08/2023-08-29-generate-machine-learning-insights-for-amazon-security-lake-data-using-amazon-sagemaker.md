Title: Generate machine learning insights for Amazon Security Lake data using Amazon SageMaker
Date: 2023-08-29T19:30:56+00:00
Author: Jonathan Nguyen
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Technical How-to;Amazon Security Lake;analytics;Machine learning;SageMaker;Security;Security Blog
Slug: 2023-08-29-generate-machine-learning-insights-for-amazon-security-lake-data-using-amazon-sagemaker

[Source](https://aws.amazon.com/blogs/security/generate-machine-learning-insights-for-amazon-security-lake-data-using-amazon-sagemaker/){:target="_blank" rel="noopener"}

> Amazon Security Lake automatically centralizes the collection of security-related logs and events from integrated AWS and third-party services. With the increasing amount of security data available, it can be challenging knowing what data to focus on and which tools to use. You can use native AWS services such as Amazon QuickSight, Amazon OpenSearch, and Amazon [...]
