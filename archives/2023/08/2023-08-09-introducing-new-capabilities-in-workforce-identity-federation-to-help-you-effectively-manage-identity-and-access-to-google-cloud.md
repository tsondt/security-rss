Title: Introducing new capabilities in Workforce Identity Federation to help you effectively manage identity and access to Google Cloud
Date: 2023-08-09T16:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Security & Identity
Slug: 2023-08-09-introducing-new-capabilities-in-workforce-identity-federation-to-help-you-effectively-manage-identity-and-access-to-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/workforce-identity-federation-now-can-help-simplify-access-to-google-cloud/){:target="_blank" rel="noopener"}

> We are excited to announce new security features, management options, and product integrations for Workforce Identity Federation, our Identity and Access Management offering that allows you to rapidly onboard user identities from external identity providers (IdPs) for direct, secure access to Google Cloud services and resources. Workforce Identity Federation is built on an identity federation approach instead of Directory Synchronization, an option which can simplify identity lifecycle management for the cloud by leveraging your existing identity systems. We are using Workforce Identity Federation to provide flexible workforce access for our Google Cloud environment. Before using Workforce Identity, if we wanted [...]
