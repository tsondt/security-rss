Title: Electoral Commission and PSNI data breaches: what we know so far
Date: 2023-08-09T10:08:59+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: Electoral Commission;Data and computer security;Technology;Hacking;Information commissioner;UK news;Northern Ireland;Police
Slug: 2023-08-09-electoral-commission-and-psni-data-breaches-what-we-know-so-far

[Source](https://www.theguardian.com/politics/2023/aug/09/electoral-commission-and-psni-data-breaches-what-we-know-so-far){:target="_blank" rel="noopener"}

> Russia named as likely culprit in cyber-attack on election watchdog, while police service accidentally publishes staff details The UK election watchdog and Northern Ireland’s police service both announced serious data breaches on Tuesday, in the latest example of the vulnerability of personal details to hacks and human error. The UK data regulator, the Information Commissioner’s Office (ICO), is looking at the incidents, which have raised immediate safety concerns over the consequences of leaking personal data. Here is what has happened and what we know so far. Continue reading... [...]
