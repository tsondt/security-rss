Title: UK Electoral Commission data breach exposes 8 years of voter data
Date: 2023-08-08T10:06:08-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-08-08-uk-electoral-commission-data-breach-exposes-8-years-of-voter-data

[Source](https://www.bleepingcomputer.com/news/security/uk-electoral-commission-data-breach-exposes-8-years-of-voter-data/){:target="_blank" rel="noopener"}

> The UK Electoral Commission disclosed a massive data breach exposing the personal information of anyone who registered to vote in the United Kingdom between 2014 and 2022. [...]
