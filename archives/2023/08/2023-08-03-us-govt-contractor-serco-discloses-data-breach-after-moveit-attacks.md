Title: US govt contractor Serco discloses data breach after MoveIT attacks
Date: 2023-08-03T12:39:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-03-us-govt-contractor-serco-discloses-data-breach-after-moveit-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-govt-contractor-serco-discloses-data-breach-after-moveit-attacks/){:target="_blank" rel="noopener"}

> Serco Inc, the Americas division of multinational outsourcing company Serco Group, has disclosed a data breach after attackers stole the personal information of over 10,000 individuals from a third-party vendor's MoveIT managed file transfer (MFT) server. [...]
