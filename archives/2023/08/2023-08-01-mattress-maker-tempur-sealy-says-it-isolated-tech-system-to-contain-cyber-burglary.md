Title: Mattress maker Tempur Sealy says it isolated tech system to contain cyber burglary
Date: 2023-08-01T14:31:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-08-01-mattress-maker-tempur-sealy-says-it-isolated-tech-system-to-contain-cyber-burglary

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/01/tempur_sealy_isolated_tech_system/){:target="_blank" rel="noopener"}

> Sleeping giant says no sign yet personal info was stolen Tempur Sealy, among the world's largest providers of bedding, has notified the Securities and Exchange Commission of a digital burglary by cyber crims that forced it to isolate parts of the tech infrastructure.... [...]
