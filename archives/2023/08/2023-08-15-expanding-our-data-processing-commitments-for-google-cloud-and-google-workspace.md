Title: Expanding our data processing commitments for Google Cloud and Google Workspace
Date: 2023-08-15T16:00:00+00:00
Author: Marc Crandall
Category: GCP Security
Tags: Productivity & Collaboration;Security & Identity
Slug: 2023-08-15-expanding-our-data-processing-commitments-for-google-cloud-and-google-workspace

[Source](https://cloud.google.com/blog/products/identity-security/cdpa-2-0/){:target="_blank" rel="noopener"}

> At Google Cloud, we are committed to meeting our customers’ evolving data processing and security requirements. To this end, we are pleased to announce the next version of the Cloud Data Processing Addendum (CDPA), which updates our data processing and security terms for Google Cloud, Google Workspace (including Workspace for Education), Cloud Identity (when purchased separately), and Looker (original) customers worldwide. The CDPA (Partners) includes equivalent updates for Google Cloud and Looker (original) partners. These versions of the CDPA are designed to strengthen and consolidate our global privacy and security commitments for all of the products listed above in one [...]
