Title: Last call for mWISE, the security conference for frontline practitioners.
Date: 2023-08-24T10:02:01-04:00
Author: Sponsored by Mandiant
Category: BleepingComputer
Tags: Security
Slug: 2023-08-24-last-call-for-mwise-the-security-conference-for-frontline-practitioners

[Source](https://www.bleepingcomputer.com/news/security/last-call-for-mwise-the-security-conference-for-frontline-practitioners/){:target="_blank" rel="noopener"}

> We're down to the final weeks of registration for mWISE, the community-focused cybersecurity conference from Mandiant. Learn more from Mandiant about the available attendance options and what you should expect. [...]
