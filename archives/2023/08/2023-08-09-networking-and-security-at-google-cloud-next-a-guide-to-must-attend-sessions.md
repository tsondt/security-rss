Title: Networking and security at Google Cloud Next: A guide to must-attend sessions
Date: 2023-08-09T16:00:00+00:00
Author: Susan Wu
Category: GCP Security
Tags: Security & Identity;Google Cloud Next;Networking
Slug: 2023-08-09-networking-and-security-at-google-cloud-next-a-guide-to-must-attend-sessions

[Source](https://cloud.google.com/blog/products/networking/networking-and-networking-security-sessions-at-next23/){:target="_blank" rel="noopener"}

> In just a few days, Google Cloud Next returns to San Francisco as a large, in-person, three-day event. There, you’ll learn all about the technologies you need to build, connect, and secure all your cloud-first, Kubernetes, and AI/ML workloads. You’ll gain hands-on experience on the latest cloud networking and network security technologies, and you’ll expand your peer network. If your role involves designing cloud networks, implementing cybersecurity, or you just want to keep your tabs on the latest network connectivity and security trends, Next ‘23 is the place for you. Here is a list of specially curated content for the [...]
