Title: Canon warns printer users to manually wipe Wi-Fi settings before discarding
Date: 2023-08-01T23:41:17+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Canon;printer;reset;wi-fi
Slug: 2023-08-01-canon-warns-printer-users-to-manually-wipe-wi-fi-settings-before-discarding

[Source](https://arstechnica.com/?p=1958242){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Printer manufacturer Canon is warning that sensitive Wi-Fi settings don’t automatically get wiped during resets, so customers should manually delete them before selling, discarding, or getting them repaired to prevent the settings from falling into the wrong hands. “Sensitive information on the Wi-Fi connection settings stored in the memories of inkjet printers (home and office/large format) may not be deleted by the usual initialization process,” company officials wrote in an advisory on Monday. They went on to say that manual wiping should occur “when your printer may be in the hand of any third party, such [...]
