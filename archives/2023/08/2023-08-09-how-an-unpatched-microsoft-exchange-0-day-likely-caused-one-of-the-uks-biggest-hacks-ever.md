Title: How an unpatched Microsoft Exchange 0-day likely caused one of the UK’s biggest hacks ever
Date: 2023-08-09T21:58:04+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Exchange Server;microsoft;zeroday
Slug: 2023-08-09-how-an-unpatched-microsoft-exchange-0-day-likely-caused-one-of-the-uks-biggest-hacks-ever

[Source](https://arstechnica.com/?p=1959987){:target="_blank" rel="noopener"}

> Enlarge / Building with Microsoft logo. (credit: Getty Images) It’s looking more and more likely that a critical zero-day vulnerability that went unfixed for more than a month in Microsoft Exchange was the cause of one of the UK’s biggest hacks ever—the breach of the country’s Electoral Commission, which exposed data for as many as 40 million residents. Electoral Commission officials disclosed the breach on Tuesday. They said that they discovered the intrusion last October when they found “suspicious activity” on their networks and that “hostile actors had first accessed the systems in August 2021.” That means the attackers were [...]
