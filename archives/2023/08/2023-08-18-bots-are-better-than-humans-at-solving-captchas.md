Title: Bots Are Better than Humans at Solving CAPTCHAs
Date: 2023-08-18T11:04:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;captchas;machine learning
Slug: 2023-08-18-bots-are-better-than-humans-at-solving-captchas

[Source](https://www.schneier.com/blog/archives/2023/08/bots-are-better-than-humans-at-solving-captchas.html){:target="_blank" rel="noopener"}

> Interesting research: “ An Empirical Study & Evaluation of Modern CAPTCHAs “: Abstract: For nearly two decades, CAPTCHAS have been widely used as a means of protection against bots. Throughout the years, as their use grew, techniques to defeat or bypass CAPTCHAS have continued to improve. Meanwhile, CAPTCHAS have also evolved in terms of sophistication and diversity, becoming increasingly difficult to solve for both bots (machines) and humans. Given this long-standing and still-ongoing arms race, it is critical to investigate how long it takes legitimate users to solve modern CAPTCHAS, and how they are perceived by those users. In this [...]
