Title: FBI: Lazarus hackers readying to cash out $41 million in stolen crypto
Date: 2023-08-23T10:53:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-08-23-fbi-lazarus-hackers-readying-to-cash-out-41-million-in-stolen-crypto

[Source](https://www.bleepingcomputer.com/news/security/fbi-lazarus-hackers-readying-to-cash-out-41-million-in-stolen-crypto/){:target="_blank" rel="noopener"}

> The FBI warned that North Koreans are likely readying to cash out tens of millions worth of stolen cryptocurrency out of hundreds of millions stolen in the last year alone. [...]
