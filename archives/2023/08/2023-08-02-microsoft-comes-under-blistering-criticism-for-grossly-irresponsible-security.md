Title: Microsoft comes under blistering criticism for “grossly irresponsible” security
Date: 2023-08-02T23:09:19+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;microsoft
Slug: 2023-08-02-microsoft-comes-under-blistering-criticism-for-grossly-irresponsible-security

[Source](https://arstechnica.com/?p=1958508){:target="_blank" rel="noopener"}

> Enlarge (credit: Drew Angerer | Getty Images ) Microsoft has once again come under blistering criticism for the security practices of Azure and its other cloud offerings, with the CEO of security firm Tenable saying Microsoft is “grossly irresponsible” and mired in a “culture of toxic obfuscation.” The comments from Amit Yoran, chairman and CEO of Tenable, come six days after Sen. Ron Wyden (D-Ore.) blasted Microsoft for what he said were “ negligent cybersecurity practices ” that enabled hackers backed by the Chinese government to steal hundreds of thousands of emails from cloud customers, including officials in the US [...]
