Title: Pulling the strings
Date: 2023-08-24T12:49:05+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-08-24-pulling-the-strings

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/24/pulling_the_strings/){:target="_blank" rel="noopener"}

> The critical rise of generative AI use in ransomware attacks on applications Webinar It's a fact of life that ransomware is a constant threat, like a dark cloud on every horizon. Recent research suggests that the volume of attacks has doubled in the last year.... [...]
