Title: Discord starts notifying users affected by March data breach
Date: 2023-08-23T14:45:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-23-discord-starts-notifying-users-affected-by-march-data-breach

[Source](https://www.bleepingcomputer.com/news/security/discord-starts-notifying-users-affected-by-march-data-breach/){:target="_blank" rel="noopener"}

> Starting on Monday, Discord has been reaching out to users affected by a data breach disclosed earlier this year to let them know what Personal Identifying Information (PII) was exposed in the incident. [...]
