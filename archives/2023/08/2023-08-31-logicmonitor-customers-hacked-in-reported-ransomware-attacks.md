Title: LogicMonitor customers hacked in reported ransomware attacks
Date: 2023-08-31T14:24:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-31-logicmonitor-customers-hacked-in-reported-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/logicmonitor-customers-hacked-in-reported-ransomware-attacks/){:target="_blank" rel="noopener"}

> Network monitoring company LogicMonitor confirmed today that some users of its SaaS platform have fallen victim to cyberattacks. [...]
