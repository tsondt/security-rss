Title: Configure fine-grained access to your resources shared using AWS Resource Access Manager
Date: 2023-08-03T16:29:02+00:00
Author: Fabian Labat
Category: AWS Security
Tags: Best Practices;Intermediate (200);Technical How-to;Amazon VPC;AWS Identity and Access Management (IAM);AWS Organizations;Network and content delivery;Resource Access Manager (RAM);Security Blog
Slug: 2023-08-03-configure-fine-grained-access-to-your-resources-shared-using-aws-resource-access-manager

[Source](https://aws.amazon.com/blogs/security/configure-fine-grained-access-to-your-resources-shared-using-aws-resource-access-manager/){:target="_blank" rel="noopener"}

> You can use AWS Resource Access Manager (AWS RAM) to securely, simply, and consistently share supported resource types within your organization or organizational units (OUs) and across AWS accounts. This means you can provision your resources once and use AWS RAM to share them with accounts. With AWS RAM, the accounts that receive the shared [...]
