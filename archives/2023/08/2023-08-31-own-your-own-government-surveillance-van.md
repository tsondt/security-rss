Title: Own Your Own Government Surveillance Van
Date: 2023-08-31T11:06:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;privacy;surveillance
Slug: 2023-08-31-own-your-own-government-surveillance-van

[Source](https://www.schneier.com/blog/archives/2023/08/own-your-own-government-surveillance-van.html){:target="_blank" rel="noopener"}

> A used government surveillance van is for sale in Chicago: So how was this van turned into a mobile spying center? Well, let’s start with how it has more LCD monitors than a Counterstrike LAN party. They can be used to monitor any of six different video inputs including a videoscope camera. A videoscope and a borescope are very similar as they’re both cameras on the ends of optical fibers, so the same tech you’d use to inspect cylinder walls is also useful for surveillance. Kind of cool, right? Multiple Sony DVD-based video recorders store footage captured by cameras, audio [...]
