Title: Forever 21 data breach: hackers accessed info of 500,000
Date: 2023-08-31T15:23:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-31-forever-21-data-breach-hackers-accessed-info-of-500000

[Source](https://www.bleepingcomputer.com/news/security/forever-21-data-breach-hackers-accessed-info-of-500-000/){:target="_blank" rel="noopener"}

> Forever 21 clothing and accessories retailer is sending data breach notifications to more than half a million individuals who had their personal information exposed to network intruders. [...]
