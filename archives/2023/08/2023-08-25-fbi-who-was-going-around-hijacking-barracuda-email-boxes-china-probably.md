Title: FBI: Who was going around hijacking Barracuda email boxes? China, probably
Date: 2023-08-25T00:17:49+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-25-fbi-who-was-going-around-hijacking-barracuda-email-boxes-china-probably

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/25/fbi_china_barracuda/){:target="_blank" rel="noopener"}

> Joins in the chorus of advice to bin the gear instead of trying for a fix The FBI has warned owners of Barracuda Email Security Gateway (ESG) appliances the devices are likely undergoing attack by snoops linked to China, and removing the machines from service remains the safest course of action.... [...]
