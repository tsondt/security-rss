Title: Beware cool-looking beta crypto-apps. They may be money-stealing fakes
Date: 2023-08-14T22:22:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-14-beware-cool-looking-beta-crypto-apps-they-may-be-money-stealing-fakes

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/14/fbi_mobile_beta_testing_apps/){:target="_blank" rel="noopener"}

> Try out a hot new thing before official launch? Something smells phishy The FBI has warned of a scam in which criminals lure people into installing what they think are pre-release beta-grade phone apps to try out – only for the software to be laced with malware.... [...]
