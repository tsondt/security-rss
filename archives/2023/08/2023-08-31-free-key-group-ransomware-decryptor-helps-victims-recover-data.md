Title: Free Key Group ransomware decryptor helps victims recover data
Date: 2023-08-31T12:21:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-31-free-key-group-ransomware-decryptor-helps-victims-recover-data

[Source](https://www.bleepingcomputer.com/news/security/free-key-group-ransomware-decryptor-helps-victims-recover-data/){:target="_blank" rel="noopener"}

> Researchers took advantage of a weakness in the encryption scheme of Key Group ransomware and developed a decryption tool that lets some victims to recover their files for free. [...]
