Title: Electoral Commission had internet-facing server with unpatched vuln
Date: 2023-08-11T11:47:49+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-08-11-electoral-commission-had-internet-facing-server-with-unpatched-vuln

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/11/electoral_commission_vulnerability/){:target="_blank" rel="noopener"}

> ProxyNotShell vulnerability could be how UK body got pwned, suggests infosec expert The hacking of the UK’s Electoral Commission was potentially facilitated by the exploitation of a vulnerability in Microsoft Exchange, according to a security expert.... [...]
