Title: Google announces new algorithm that makes FIDO encryption safe from quantum computers
Date: 2023-08-18T20:01:25+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Fido;passkeys;quantum computing
Slug: 2023-08-18-google-announces-new-algorithm-that-makes-fido-encryption-safe-from-quantum-computers

[Source](https://arstechnica.com/?p=1961906){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) The FIDO2 industry standard adopted five years ago provides the most secure known way to log in to websites because it doesn’t rely on passwords and has the most secure form of built-in two-factor authentication. Like many existing security schemes today, though, FIDO faces an ominous if distant threat from quantum computing, which one day will cause the currently rock-solid cryptography the standard uses to completely crumble. Over the past decade, mathematicians and engineers have scrambled to head off this cryptopocalypse with the advent of PQC—short for post-quantum cryptography—a class of encryption that uses algorithms resistant [...]
