Title: The Week in Ransomware - August 4th 2023 - Targeting VMware ESXi
Date: 2023-08-04T19:12:19-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-04-the-week-in-ransomware-august-4th-2023-targeting-vmware-esxi

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-august-4th-2023-targeting-vmware-esxi/){:target="_blank" rel="noopener"}

> Ransomware gangs continue to prioritize targeting VMware ESXi servers, with almost every active ransomware gang creating custom Linux encryptors for this purpose. [...]
