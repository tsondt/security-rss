Title: New BitForge cryptocurrency wallet flaws lets hackers steal crypto
Date: 2023-08-09T17:15:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: CryptoCurrency;Security
Slug: 2023-08-09-new-bitforge-cryptocurrency-wallet-flaws-lets-hackers-steal-crypto

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/new-bitforge-cryptocurrency-wallet-flaws-lets-hackers-steal-crypto/){:target="_blank" rel="noopener"}

> Multiple zero-day vulnerabilities named 'BitForge' in the implementation of widely used cryptographic protocols like GG-18, GG-20, and Lindell 17 affected popular cryptocurrency wallet providers, including Coinbase, ZenGo, Binance, and many more. [...]
