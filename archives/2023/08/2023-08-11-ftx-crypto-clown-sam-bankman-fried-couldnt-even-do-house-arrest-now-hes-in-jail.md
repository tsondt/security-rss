Title: FTX crypto-clown Sam Bankman-Fried couldn't even do house arrest. Now he's in jail
Date: 2023-08-11T22:15:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-08-11-ftx-crypto-clown-sam-bankman-fried-couldnt-even-do-house-arrest-now-hes-in-jail

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/11/sam_bankman_fried_jailed/){:target="_blank" rel="noopener"}

> Feds argue leaks to press amount to witness tampering Sam Bankman-Fried (SBF), former chief executive of crypto-disaster FTX, who has been awaiting trial for his firm's failure while in home detention with his family, has been sent to jail for attempting to intimidate witnesses.... [...]
