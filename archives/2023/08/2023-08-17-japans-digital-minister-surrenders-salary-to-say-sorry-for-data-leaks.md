Title: Japan's digital minister surrenders salary to say sorry for data leaks
Date: 2023-08-17T04:58:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-08-17-japans-digital-minister-surrenders-salary-to-say-sorry-for-data-leaks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/17/japanese_minister_offers_up_salary/){:target="_blank" rel="noopener"}

> The My Number card mess remains unsolved as trust in e-government remains muted Japan’s digital minister has doubled down on a June promise to penalize himself for the poor rollout of the country’s digital ID, My Number Card, by offering up three months salary on Tuesday.... [...]
