Title: There's a good chance your VPN is vulnerable to privacy-menacing TunnelCrack attack
Date: 2023-08-10T20:37:52+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-10-theres-a-good-chance-your-vpn-is-vulnerable-to-privacy-menacing-tunnelcrack-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/10/tunnelcrack_vpn/){:target="_blank" rel="noopener"}

> Especially on Apple gear, uni team says A couple of techniques collectively known as TunnelCrack can, in the right circumstances, be used by snoops to force victims' network traffic to go outside their encrypted VPNs, it was demonstrated this week.... [...]
