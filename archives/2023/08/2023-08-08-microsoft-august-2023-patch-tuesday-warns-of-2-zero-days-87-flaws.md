Title: Microsoft August 2023 Patch Tuesday warns of 2 zero-days, 87 flaws
Date: 2023-08-08T13:54:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-08-08-microsoft-august-2023-patch-tuesday-warns-of-2-zero-days-87-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-august-2023-patch-tuesday-warns-of-2-zero-days-87-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's August 2023 Patch Tuesday, with security updates for 87 flaws, including two actively exploited and twenty-three remote code execution vulnerabilities. [...]
