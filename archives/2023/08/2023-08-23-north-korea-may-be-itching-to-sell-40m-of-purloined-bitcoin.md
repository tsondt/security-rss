Title: North Korea may be itching to sell $40m of purloined Bitcoin
Date: 2023-08-23T18:45:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-23-north-korea-may-be-itching-to-sell-40m-of-purloined-bitcoin

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/23/fbi_dprk_cyber_crooks/){:target="_blank" rel="noopener"}

> Those weapons programs aren't going to fund themselves Lazarus Group, the infamous cryptocurrency thieves backed by North Korea, may try to liquidate a stash of stolen Bitcoin worth more than $40 million, according to the FBI.... [...]
