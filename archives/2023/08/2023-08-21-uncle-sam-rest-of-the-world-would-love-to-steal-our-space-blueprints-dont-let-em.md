Title: Uncle Sam: Rest of the world would love to steal our space blueprints – don't let 'em
Date: 2023-08-21T21:54:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-21-uncle-sam-rest-of-the-world-would-love-to-steal-our-space-blueprints-dont-let-em

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/21/us_satellite_hacking/){:target="_blank" rel="noopener"}

> If spies aren't swiping designs via joint ventures, they're breaking into IT networks and mulling sat hijackings With America outspending the rest of the world on space technologies, those systems and their blueprints are a highly alluring and lucrative target for sticky-fingered spies, Uncle Sam has reminded industry.... [...]
