Title: Google Chrome to shield encryption keys from promised quantum computers
Date: 2023-08-12T10:27:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-08-12-google-chrome-to-shield-encryption-keys-from-promised-quantum-computers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/12/google_chrome_kem/){:target="_blank" rel="noopener"}

> QC crypto-cracking coming in 5, 10, maybe 50 years, so act... now? Google has started deploying a hybrid key encapsulation mechanism (KEM) to protect the sharing of symmetric encryption secrets during the establishment of secure TLS network connections.... [...]
