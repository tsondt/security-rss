Title: Friday Squid Blogging: 2023 Squid Oil Global Market Report
Date: 2023-08-04T21:07:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-08-04-friday-squid-blogging-2023-squid-oil-global-market-report

[Source](https://www.schneier.com/blog/archives/2023/08/friday-squid-blogging-2023-squid-oil-global-market-report.html){:target="_blank" rel="noopener"}

> I had no idea that squid contain sufficient oil to be worth extracting. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
