Title: Almost 2,000 Citrix NetScaler servers backdoored in hacking campaign
Date: 2023-08-15T15:41:38-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-08-15-almost-2000-citrix-netscaler-servers-backdoored-in-hacking-campaign

[Source](https://www.bleepingcomputer.com/news/security/almost-2-000-citrix-netscaler-servers-backdoored-in-hacking-campaign/){:target="_blank" rel="noopener"}

> A threat actor has compromised close to 2,000 thousand Citrix NetScaler servers in a massive campaign exploiting the critical-severity remote code execution tracked as CVE-2023-3519. [...]
