Title: Google removes fake Signal and Telegram apps hosted on Play
Date: 2023-08-30T22:09:14+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;google play;signal;telegram
Slug: 2023-08-30-google-removes-fake-signal-and-telegram-apps-hosted-on-play

[Source](https://arstechnica.com/?p=1964551){:target="_blank" rel="noopener"}

> Enlarge (credit: Mateusz Slodkowski/SOPA Images/LightRocket via Getty Images) Researchers on Wednesday said they found fake apps in Google Play that masqueraded as legitimate ones for the Signal and Telegram messaging platforms. The malicious apps could pull messages or other sensitive information from legitimate accounts when users took certain actions. An app with the name Signal Plus Messenger was available on Play for nine months and had been downloaded from Play roughly 100 times before Google took it down last April after being tipped off by security firm ESET. It was also available in the Samsung app store and on signalplus[.]org, [...]
