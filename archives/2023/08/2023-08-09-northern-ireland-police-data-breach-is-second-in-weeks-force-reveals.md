Title: Northern Ireland police data breach is second in weeks, force reveals
Date: 2023-08-09T18:11:42+00:00
Author: Rory Carroll and Vikram Dodd
Category: The Guardian
Tags: Northern Ireland;Information commissioner;Police;Data and computer security;Freedom of information;UK news
Slug: 2023-08-09-northern-ireland-police-data-breach-is-second-in-weeks-force-reveals

[Source](https://www.theguardian.com/uk-news/2023/aug/09/police-officers-in-northern-ireland-may-leave-force-or-move-after-data-breach-psni){:target="_blank" rel="noopener"}

> Admission that document with names of 200 staff was stolen in July follows revelation of ‘monumental’ data breach on Tuesday The Police Service of Northern Ireland has admitted that this week’s “monumental” data breach followed an earlier leak of names of hundreds of officers and staff, deepening a crisis over the mishandling of personal information that could be used to target employees. The disclosures sowed anxiety in the ranks and prompted some officers to relocate in case paramilitaries tried to exploit the situation – the terrorism threat level in the region is categorised as severe, meaning the chances of an [...]
