Title: MTE As Implemented, Part 1: Implementation Testing
Date: 2023-08-02T09:30:00.001000-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2023-08-02-mte-as-implemented-part-1-implementation-testing

[Source](https://googleprojectzero.blogspot.com/2023/08/mte-as-implemented-part-1.html){:target="_blank" rel="noopener"}

> By Mark Brand, Project Zero Background In 2018, in the v8.5a version of the ARM architecture, ARM proposed a hardware implementation of tagged memory, referred to as MTE (Memory Tagging Extensions). Through mid-2022 and early 2023, Project Zero had access to pre-production hardware implementing this instruction set extension to evaluate the security properties of the implementation. In particular, we're interested in whether it's possible to use this instruction set extension to implement effective security mitigations, or whether its use is limited to debugging/fault detection purposes. As of the v8.5a specification, MTE can operate in two distinct modes, which are switched [...]
