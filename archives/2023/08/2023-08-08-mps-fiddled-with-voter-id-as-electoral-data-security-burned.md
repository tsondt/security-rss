Title: MPs fiddled with voter ID as electoral data security burned
Date: 2023-08-08T16:55:32+00:00
Author: Heather Stewart
Category: The Guardian
Tags: Electoral Commission;Hacking;Data protection;Data and computer security;Cybercrime;Politics;UK news;Technology
Slug: 2023-08-08-mps-fiddled-with-voter-id-as-electoral-data-security-burned

[Source](https://www.theguardian.com/technology/2023/aug/08/mps-fiddled-with-voter-id-as-electoral-data-security-burned-electoral-commission-hack){:target="_blank" rel="noopener"}

> Electoral Commission hack is reminder of importance of protecting democratic system where it counts It turns out that while Conservative ministers were spending hours of parliamentary time in 2021-22 introducing requirements for voters to produce ID at polling stations – to protect elections against a threat most experts believed was negligible – the Electoral Commission was being hacked by “hostile actors”. These hackers, who have not been identified and whose motivations are unclear, were able to access the data, such as home addresses, of millions of voters, many of whom choose not to make that information publicly available. Continue reading... [...]
