Title: Cyber-extortionists pillage Colorado education dept
Date: 2023-08-08T19:19:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-08-08-cyber-extortionists-pillage-colorado-education-dept

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/08/colorado_education_hacking/){:target="_blank" rel="noopener"}

> Hey, breacher, leave those kids alone Data going back as far as nearly 20 years may have been stolen from the Colorado Department of Higher Education (CDHE) after ransomware extortionists breached the government body's IT systems.... [...]
