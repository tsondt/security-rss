Title: Hacked UK voter data could be used to target disinformation, warn experts
Date: 2023-08-09T17:21:51+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: Electoral Commission;Artificial intelligence (AI);Cybercrime;Data and computer security;Data protection;Hacking;ChatGPT;UK news
Slug: 2023-08-09-hacked-uk-voter-data-could-be-used-to-target-disinformation-warn-experts

[Source](https://www.theguardian.com/politics/2023/aug/09/hacked-uk-electoral-commission-data-target-voter-disinformation-warn-expert){:target="_blank" rel="noopener"}

> Data from Electoral Commission breach could allow rogue actors to create AI-generated messages in effort to manipulate elections Data accessed in the Electoral Commission hack could help state-backed actors target voters with AI-generated disinformation, experts have warned. The UK elections watchdog revealed on Tuesday that a hostile cyber-attack had been able to access the names and addresses of all voters registered between 2014 and 2022. Continue reading... [...]
