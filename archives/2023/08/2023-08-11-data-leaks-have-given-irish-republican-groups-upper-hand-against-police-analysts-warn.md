Title: Data leaks have given Irish republican groups ‘upper hand’ against police, analysts warn
Date: 2023-08-11T16:46:20+00:00
Author: Rory Carroll Ireland correspondent
Category: The Guardian
Tags: Northern Ireland;Police;UK news;Data and computer security;Northern Irish politics
Slug: 2023-08-11-data-leaks-have-given-irish-republican-groups-upper-hand-against-police-analysts-warn

[Source](https://www.theguardian.com/uk-news/2023/aug/11/data-leaks-irish-republican-groups-upper-hand-against-police-analysts-warn){:target="_blank" rel="noopener"}

> Breaches hurt police morale and may help republican paramilitaries intimidate officers and their families Police data breaches in Northern Ireland have given republican paramilitaries a powerful tool to intimidate, demoralise and target officers and their families for years to come, according to security experts. The New IRA and other groups have gained the “upper hand” and will be able to use the unprecedented leaks of officers’ personal information to carry out psychological and possibly physical attacks, the analysts warned. Continue reading... [...]
