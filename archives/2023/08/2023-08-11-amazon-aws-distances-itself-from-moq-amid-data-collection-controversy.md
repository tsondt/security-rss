Title: Amazon AWS distances itself from Moq amid data collection controversy
Date: 2023-08-11T10:04:52-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-08-11-amazon-aws-distances-itself-from-moq-amid-data-collection-controversy

[Source](https://www.bleepingcomputer.com/news/security/amazon-aws-distances-itself-from-moq-amid-data-collection-controversy/){:target="_blank" rel="noopener"}

> Amazon AWS has withdrawn its association with open source project Moq after the project drew sharp criticism for its quiet addition of data collection features, as first reported by BleepingComputer. [...]
