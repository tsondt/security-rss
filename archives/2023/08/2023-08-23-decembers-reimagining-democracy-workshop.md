Title: December’s Reimagining Democracy Workshop
Date: 2023-08-23T11:06:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;infrastructure;voting
Slug: 2023-08-23-decembers-reimagining-democracy-workshop

[Source](https://www.schneier.com/blog/archives/2023/08/decembers-reimagining-democracy-workshop.html){:target="_blank" rel="noopener"}

> Imagine that we’ve all—all of us, all of society—landed on some alien planet, and we have to form a government: clean slate. We don’t have any legacy systems from the US or any other country. We don’t have any special or unique interests to perturb our thinking. How would we govern ourselves? It’s unlikely that we would use the systems we have today. The modern representative democracy was the best form of government that mid-eighteenth-century technology could conceive of. The twenty-first century is a different place scientifically, technically and socially. For example, the mid-eighteenth-century democracies were designed under the assumption [...]
