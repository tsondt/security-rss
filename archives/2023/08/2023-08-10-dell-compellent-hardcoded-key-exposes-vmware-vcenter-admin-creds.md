Title: Dell Compellent hardcoded key exposes VMware vCenter admin creds
Date: 2023-08-10T10:38:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-10-dell-compellent-hardcoded-key-exposes-vmware-vcenter-admin-creds

[Source](https://www.bleepingcomputer.com/news/security/dell-compellent-hardcoded-key-exposes-vmware-vcenter-admin-creds/){:target="_blank" rel="noopener"}

> An unfixed hardcoded encryption key flaw in Dell's Compellent Integration Tools for VMware (CITV) allows attackers to decrypt stored vCenter admin credentials and retrieve the cleartext password. [...]
