Title: How we designed Cedar to be intuitive to use, fast, and safe
Date: 2023-08-21T20:33:10+00:00
Author: Emina Torlak
Category: AWS Security
Tags: Advanced (300);Amazon Verified Permissions;Security, Identity, & Compliance;Technical How-to;Amazon Verified Access;Automated reasoning;Open source;Security Blog
Slug: 2023-08-21-how-we-designed-cedar-to-be-intuitive-to-use-fast-and-safe

[Source](https://aws.amazon.com/blogs/security/how-we-designed-cedar-to-be-intuitive-to-use-fast-and-safe/){:target="_blank" rel="noopener"}

> This post is a deep dive into the design of Cedar, an open source language for writing and evaluating authorization policies. Using Cedar, you can control access to your application’s resources in a modular and reusable way. You write Cedar policies that express your application’s permissions, and the application uses Cedar’s authorization engine to decide which [...]
