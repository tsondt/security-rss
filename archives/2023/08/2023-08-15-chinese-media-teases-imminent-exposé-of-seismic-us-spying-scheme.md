Title: Chinese media teases imminent exposé of seismic US spying scheme
Date: 2023-08-15T01:57:26+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-08-15-chinese-media-teases-imminent-exposé-of-seismic-us-spying-scheme

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/15/china_seismic_us_spying_expose/){:target="_blank" rel="noopener"}

> Again labels America a hacker empire over alleged backdoors found in earthquake monitoring kit China's Global Times, a state-controlled media outlet, has teased an imminent exposé of alleged US attacks on seismic data measurement stations.... [...]
