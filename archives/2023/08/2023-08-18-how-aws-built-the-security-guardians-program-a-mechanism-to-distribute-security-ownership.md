Title: How AWS built the Security Guardians program, a mechanism to distribute security ownership
Date: 2023-08-18T20:34:36+00:00
Author: Ana Malhotra
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Thought Leadership;Application security;Security;Security Blog
Slug: 2023-08-18-how-aws-built-the-security-guardians-program-a-mechanism-to-distribute-security-ownership

[Source](https://aws.amazon.com/blogs/security/how-aws-built-the-security-guardians-program-a-mechanism-to-distribute-security-ownership/){:target="_blank" rel="noopener"}

> Product security teams play a critical role to help ensure that new services, products, and features are built and shipped securely to customers. However, since security teams are in the product launch path, they can form a bottleneck if organizations struggle to scale their security teams to support their growing product development teams. In this [...]
