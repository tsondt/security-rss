Title: Malicious extensions can abuse VS Code flaw to steal auth tokens
Date: 2023-08-08T17:49:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-08-08-malicious-extensions-can-abuse-vs-code-flaw-to-steal-auth-tokens

[Source](https://www.bleepingcomputer.com/news/security/malicious-extensions-can-abuse-vs-code-flaw-to-steal-auth-tokens/){:target="_blank" rel="noopener"}

> Microsoft's Visual Studio Code (VS Code) code editor and development environment contains a flaw that allows malicious extensions to retrieve authentication tokens stored in Windows, Linux, and macOS credential managers. [...]
