Title: New acoustic attack steals data from keystrokes with 95% accuracy
Date: 2023-08-05T10:09:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-05-new-acoustic-attack-steals-data-from-keystrokes-with-95-accuracy

[Source](https://www.bleepingcomputer.com/news/security/new-acoustic-attack-steals-data-from-keystrokes-with-95-percent-accuracy/){:target="_blank" rel="noopener"}

> A team of researchers from British universities has trained a deep learning model that can steal data from keyboard keystrokes recorded using a microphone with an accuracy of 95%. [...]
