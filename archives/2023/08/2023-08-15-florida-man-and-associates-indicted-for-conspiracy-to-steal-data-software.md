Title: Florida Man and associates indicted for conspiracy to steal data, software
Date: 2023-08-15T06:58:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-08-15-florida-man-and-associates-indicted-for-conspiracy-to-steal-data-software

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/15/georgia_trump_indictment_data_theft/){:target="_blank" rel="noopener"}

> Voting machines and their info allegedly accessed without authorization by keen golfer's gofers Authorities in the US state of Georgia have indicted a famous Floridian and his loyal associates on counts including theft of data, software, and personal information.... [...]
