Title: Spring 2023 SOC reports now available in Spanish
Date: 2023-08-30T16:49:44+00:00
Author: Andrew Najjar
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC 1;AWS SOC 2;AWS SOC 3;AWS SOC Reports;Security Blog
Slug: 2023-08-30-spring-2023-soc-reports-now-available-in-spanish

[Source](https://aws.amazon.com/blogs/security/spring-2023-soc-reports-now-available-in-spanish/){:target="_blank" rel="noopener"}

> Spanish version » We continue to listen to our customers, regulators, and stakeholders to understand their needs regarding audit, assurance, certification, and attestation programs at Amazon Web Services (AWS). We’re pleased to announce that Spring 2023 System and Organization Controls (SOC) 1, SOC 2, and SOC 3 reports are now available in Spanish. These translated [...]
