Title: High severity vuln in WinRAR could allow code to run when files are opened
Date: 2023-08-21T13:35:06+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-08-21-high-severity-vuln-in-winrar-could-allow-code-to-run-when-files-are-opened

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/21/winrar_vuln_could_allow_code/){:target="_blank" rel="noopener"}

> Update now: Millions of users potentially impacted, plus uncounted warez folks Users of the popular WinRAR compression and archiving tool should update now to avoid a vulnerability that allows code to be run when a user opens a RAR file.... [...]
