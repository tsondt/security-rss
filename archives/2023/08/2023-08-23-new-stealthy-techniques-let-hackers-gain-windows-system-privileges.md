Title: New stealthy techniques let hackers gain Windows SYSTEM privileges
Date: 2023-08-23T14:30:31-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-08-23-new-stealthy-techniques-let-hackers-gain-windows-system-privileges

[Source](https://www.bleepingcomputer.com/news/security/new-stealthy-techniques-let-hackers-gain-windows-system-privileges/){:target="_blank" rel="noopener"}

> Security researchers have released NoFilter, a tool that abuses the Windows Filtering Platform to elevate a user's privileges to increases privileges to SYSTEM, the highest permission level on Windows. [...]
