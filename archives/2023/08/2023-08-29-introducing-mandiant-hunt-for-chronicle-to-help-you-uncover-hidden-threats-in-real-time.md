Title: Introducing Mandiant Hunt for Chronicle to help you uncover hidden threats in real-time
Date: 2023-08-29T12:00:00+00:00
Author: Shelly Tzoumas
Category: GCP Security
Tags: Google Cloud Next;Security & Identity
Slug: 2023-08-29-introducing-mandiant-hunt-for-chronicle-to-help-you-uncover-hidden-threats-in-real-time

[Source](https://cloud.google.com/blog/products/identity-security/introducing-mandiant-hunt-for-chronicle/){:target="_blank" rel="noopener"}

> Chronicle Security Operations is an essential security suite for any organization that wants to stay on top of the threats that they face. As part of our security announcements today at Google Cloud Next, we’re introducing our always-on AI collaborator Duet AI in Chronicle. Available in preview and expected to be generally available later this year, Duet AI in Chronicle provides generative AI-powered assistance to cloud defenders where and when they need it. It can help transform threat detection, investigation, and response for cyber defenders by simplifying search, complex data analysis, and threat detection engineering, to reduce toil and elevate [...]
