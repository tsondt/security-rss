Title: Microsoft ain't happy with Russia-led UN cybercrime treaty
Date: 2023-08-30T18:23:41+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-30-microsoft-aint-happy-with-russia-led-un-cybercrime-treaty

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/30/microsoft_un_cybercrime_treaty/){:target="_blank" rel="noopener"}

> Could be used to put ethical hackers, and citizens, behind bars A controversial United Nations proposal has a new foe, Microsoft, which has joined the growing number of organizations warning delegates that the draft version of the UN cybercrime treaty only succeeds in justifying state surveillance — not stopping criminals, as originally intended.... [...]
