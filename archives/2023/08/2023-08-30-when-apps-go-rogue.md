Title: When Apps Go Rogue
Date: 2023-08-30T13:39:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;botnets
Slug: 2023-08-30-when-apps-go-rogue

[Source](https://www.schneier.com/blog/archives/2023/08/when-apps-go-rogue.html){:target="_blank" rel="noopener"}

> Interesting story of an Apple Macintosh app that went rogue. Basically, it was a good app until one particular update...when it went bad. With more official macOS features added in 2021 that enabled the “Night Shift” dark mode, the NightOwl app was left forlorn and forgotten on many older Macs. Few of those supposed tens of thousands of users likely noticed when the app they ran in the background of their older Macs was bought by another company, nor when earlier this year that company silently updated the dark mode app so that it hijacked their machines in order to [...]
