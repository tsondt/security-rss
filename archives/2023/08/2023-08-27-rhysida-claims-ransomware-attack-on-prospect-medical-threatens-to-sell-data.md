Title: Rhysida claims ransomware attack on Prospect Medical, threatens to sell data
Date: 2023-08-27T18:37:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-27-rhysida-claims-ransomware-attack-on-prospect-medical-threatens-to-sell-data

[Source](https://www.bleepingcomputer.com/news/security/rhysida-claims-ransomware-attack-on-prospect-medical-threatens-to-sell-data/){:target="_blank" rel="noopener"}

> The Rhysida ransomware gang has claimed responsibility for the massive cyberattack on Prospect Medical Holdings, claiming to have stolen 500,000 social security numbers, corporate documents, and patient records. [...]
