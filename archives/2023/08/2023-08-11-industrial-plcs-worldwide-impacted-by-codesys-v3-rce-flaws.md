Title: Industrial PLCs worldwide impacted by CODESYS V3 RCE flaws
Date: 2023-08-11T10:33:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-11-industrial-plcs-worldwide-impacted-by-codesys-v3-rce-flaws

[Source](https://www.bleepingcomputer.com/news/security/industrial-plcs-worldwide-impacted-by-codesys-v3-rce-flaws/){:target="_blank" rel="noopener"}

> Millions of PLC (programmable logic controllers) used in industrial environments worldwide are at risk to 15 vulnerabilities in the CODESYS V3 software development kit, allowing remote code execution (RCE) and denial of service (DoS) attacks. [...]
