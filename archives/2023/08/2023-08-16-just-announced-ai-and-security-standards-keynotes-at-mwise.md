Title: Just announced: AI and security standards keynotes at mWISE
Date: 2023-08-16T10:02:01-04:00
Author: Sponsored by Mandiant
Category: BleepingComputer
Tags: Security
Slug: 2023-08-16-just-announced-ai-and-security-standards-keynotes-at-mwise

[Source](https://www.bleepingcomputer.com/news/security/just-announced-ai-and-security-standards-keynotes-at-mwise/){:target="_blank" rel="noopener"}

> Get ready for the mWISE cybersecurity conference from Mandiant, taking place September 18-20, 2023 in Washington, DC. mWISE just announced new keynote panels focused on Artificial Intelligence (AI) and advanced adversaries. [...]
