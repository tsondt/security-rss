Title: Hackers increasingly abuse Cloudflare Tunnels for stealthy connections
Date: 2023-08-07T16:03:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-07-hackers-increasingly-abuse-cloudflare-tunnels-for-stealthy-connections

[Source](https://www.bleepingcomputer.com/news/security/hackers-increasingly-abuse-cloudflare-tunnels-for-stealthy-connections/){:target="_blank" rel="noopener"}

> Hackers are increasingly abusing the legitimate Cloudflare Tunnels feature to create stealthy HTTPS connections from compromised devices, bypass firewalls, and maintain long-term persistence. [...]
