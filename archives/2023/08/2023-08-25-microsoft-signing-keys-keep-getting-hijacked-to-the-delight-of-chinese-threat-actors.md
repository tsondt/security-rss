Title: Microsoft signing keys keep getting hijacked, to the delight of Chinese threat actors
Date: 2023-08-25T13:17:24+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;digigal certificate;drivers;microsoft;rootkits;Windows
Slug: 2023-08-25-microsoft-signing-keys-keep-getting-hijacked-to-the-delight-of-chinese-threat-actors

[Source](https://arstechnica.com/?p=1963184){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) In July, security researchers revealed a sobering discovery: hundreds of pieces of malware used by multiple hacker groups to infect Windows devices had been digitally signed and validated as safe by Microsoft itself. On Tuesday, a different set of researchers made a similarly solemn announcement: Microsoft’s digital keys had been hijacked to sign yet more malware for use by a previously unknown threat actor in a supply-chain attack that infected roughly 100 carefully selected victims. The malware, researchers from Symantec’s Threat Hunter Team reported, was digitally signed with a certificate for use in what is alternatively [...]
