Title: Scraped data of 2.6 million Duolingo users released on hacking forum
Date: 2023-08-22T18:50:04-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-08-22-scraped-data-of-26-million-duolingo-users-released-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/scraped-data-of-26-million-duolingo-users-released-on-hacking-forum/){:target="_blank" rel="noopener"}

> The scraped data of 2.6 million DuoLingo users was leaked on a hacking forum, allowing threat actors to conduct targeted phishing attacks using the exposed information. [...]
