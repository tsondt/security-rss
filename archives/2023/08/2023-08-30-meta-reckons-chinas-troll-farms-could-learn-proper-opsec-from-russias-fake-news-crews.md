Title: Meta reckons China's troll farms could learn proper OpSec from Russia's fake news crews
Date: 2023-08-30T00:58:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-30-meta-reckons-chinas-troll-farms-could-learn-proper-opsec-from-russias-fake-news-crews

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/30/meta_coordinated_inauthentic_behaviour_report/){:target="_blank" rel="noopener"}

> Claims to have taken down two colossal networks, with 'Secondary Infektion' schooling 'Spamouflage' Russia appears to be "better" at running online trolling campaigns aimed at pushing its political narratives than China, according to Meta's latest Adversarial Threat Report.... [...]
