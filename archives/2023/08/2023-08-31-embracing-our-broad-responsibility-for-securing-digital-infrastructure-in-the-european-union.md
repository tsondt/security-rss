Title: Embracing our broad responsibility for securing digital infrastructure in the European Union
Date: 2023-08-31T19:04:46+00:00
Author: Frank Adelmann
Category: AWS Security
Tags: Foundational (100);Intermediate (200);Security, Identity, & Compliance;Thought Leadership;Security Assurance;Security Blog
Slug: 2023-08-31-embracing-our-broad-responsibility-for-securing-digital-infrastructure-in-the-european-union

[Source](https://aws.amazon.com/blogs/security/embracing-our-broad-responsibility-for-securing-digital-infrastructure-in-the-european-union/){:target="_blank" rel="noopener"}

> Over the past few decades, digital technologies have brought tremendous benefits to our societies, governments, businesses, and everyday lives. However, the more we depend on them for critical applications, the more we must do so securely. The increasing reliance on these systems comes with a broad responsibility for society, companies, and governments. At Amazon Web [...]
