Title: Teach a Man to Phish and He’s Set for Life
Date: 2023-08-04T13:49:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;Check Point Software;LinkedIn;microsoft;Microsoft 365;phishing;right to left override
Slug: 2023-08-04-teach-a-man-to-phish-and-hes-set-for-life

[Source](https://krebsonsecurity.com/2023/08/teach-a-man-to-phish-and-hes-set-for-life/){:target="_blank" rel="noopener"}

> One frustrating aspect of email phishing is the frequency with which scammers fall back on tried-and-true methods that really have no business working these days. Like attaching a phishing email to a traditional, clean email message, or leveraging link redirects on LinkedIn, or abusing an encoding method that makes it easy to disguise booby-trapped Microsoft Windows files as relatively harmless documents. KrebsOnSecurity recently heard from a reader who was puzzled over an email he’d just received saying he needed to review and complete a supplied W-9 tax form. The missive was made to appear as if it were part of [...]
