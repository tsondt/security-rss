Title: Detecting “Violations of Social Norms” in Text with AI
Date: 2023-08-17T11:07:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;DARPA;Internet and society
Slug: 2023-08-17-detecting-violations-of-social-norms-in-text-with-ai

[Source](https://www.schneier.com/blog/archives/2023/08/detecting-violations-of-social-norms-in-text-with-ai.html){:target="_blank" rel="noopener"}

> Researchers are trying to use AI to detect “social norms violations.” Feels a little sketchy right now, but this is the sort of thing that AIs will get better at. (Like all of these systems, anything but a very low false positive rate makes the detection useless in practice.) News article. [...]
