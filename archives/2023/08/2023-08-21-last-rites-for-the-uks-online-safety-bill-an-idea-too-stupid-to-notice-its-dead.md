Title: Last rites for the UK's Online Safety Bill, an idea too stupid to notice it's dead
Date: 2023-08-21T08:31:09+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2023-08-21-last-rites-for-the-uks-online-safety-bill-an-idea-too-stupid-to-notice-its-dead

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/21/opinion_column_monday/){:target="_blank" rel="noopener"}

> Snoopers Charter: Dead cows don't snitch Opinion Information wants to be free. This usefully ambiguous battle cry has been the mischievous slogan of hackers since early networking thinker Stuart Brand coined it in the early 1980s. Intended as part of a discussion about the inherent contradictions of intellectual property, it has bestowed irony in many other places since.... [...]
