Title: Trojanized Signal and Telegram apps on Google Play delivered spyware
Date: 2023-08-30T11:16:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-08-30-trojanized-signal-and-telegram-apps-on-google-play-delivered-spyware

[Source](https://www.bleepingcomputer.com/news/security/trojanized-signal-and-telegram-apps-on-google-play-delivered-spyware/){:target="_blank" rel="noopener"}

> Trojanized Signal and Telegram apps containing the BadBazaar spyware were uploaded onto Google Play and Samsung Galaxy Store by a Chinese APT hacking group known as GREF. [...]
