Title: White House Announces AI Cybersecurity Challenge
Date: 2023-08-21T11:10:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cybersecurity;DARPA;infrastructure
Slug: 2023-08-21-white-house-announces-ai-cybersecurity-challenge

[Source](https://www.schneier.com/blog/archives/2023/08/white-house-announces-ai-cybersecurity-challenge.html){:target="_blank" rel="noopener"}

> At Black Hat last week, the White House announced an AI Cyber Challenge. Gizmodo reports : The new AI cyber challenge (which is being abbreviated “AIxCC”) will have a number of different phases. Interested would-be competitors can now submit their proposals to the Small Business Innovation Research program for evaluation and, eventually, selected teams will participate in a 2024 “qualifying event.” During that event, the top 20 teams will be invited to a semifinal competition at that year’s DEF CON, another large cybersecurity conference, where the field will be further whittled down. [...] To secure the top spot in DARPA’s [...]
