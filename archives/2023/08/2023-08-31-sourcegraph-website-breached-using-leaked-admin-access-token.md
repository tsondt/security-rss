Title: Sourcegraph website breached using leaked admin access token
Date: 2023-08-31T17:03:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-08-31-sourcegraph-website-breached-using-leaked-admin-access-token

[Source](https://www.bleepingcomputer.com/news/security/sourcegraph-website-breached-using-leaked-admin-access-token/){:target="_blank" rel="noopener"}

> AI-powered coding platform Sourcegraph revealed that its website was breached this week using a site-admin access token accidentally leaked online on July 14th. [...]
