Title: Two US Navy sailors charged with giving Chinese spies secret military info
Date: 2023-08-04T22:03:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-04-two-us-navy-sailors-charged-with-giving-chinese-spies-secret-military-info

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/04/us_navy_sailors_china_spies/){:target="_blank" rel="noopener"}

> 'Quite obviously f**king espionage,' one suspect allegedly blabbed Two US Navy service members appeared in federal court Thursday accused of espionage and stealing sensitive military information for China in separate cases.... [...]
