Title: North Korean hackers behind malicious VMConnect PyPI campaign
Date: 2023-08-31T14:47:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-31-north-korean-hackers-behind-malicious-vmconnect-pypi-campaign

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-behind-malicious-vmconnect-pypi-campaign/){:target="_blank" rel="noopener"}

> North Korean state-sponsored hackers are behind the VMConnect campaign that uploaded to the PyPI (Python Package Index) repository malicious packages, one of them mimicking the VMware vSphere connector module vConnector. [...]
