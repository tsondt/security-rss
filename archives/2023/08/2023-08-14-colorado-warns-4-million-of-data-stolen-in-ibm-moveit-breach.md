Title: Colorado warns 4 million of data stolen in IBM MOVEit breach
Date: 2023-08-14T08:42:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-14-colorado-warns-4-million-of-data-stolen-in-ibm-moveit-breach

[Source](https://www.bleepingcomputer.com/news/security/colorado-warns-4-million-of-data-stolen-in-ibm-moveit-breach/){:target="_blank" rel="noopener"}

> The Colorado Department of Health Care Policy & Financing (HCPF) is alerting more than four million individuals of a data breach that impacted their personal and health information. [...]
