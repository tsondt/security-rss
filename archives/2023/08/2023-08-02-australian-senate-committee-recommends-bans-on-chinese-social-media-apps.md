Title: Australian Senate committee recommends bans on Chinese social media apps
Date: 2023-08-02T06:30:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-08-02-australian-senate-committee-recommends-bans-on-chinese-social-media-apps

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/02/australia_foreign_interference_social_report/){:target="_blank" rel="noopener"}

> WeChat accused of 'contempt for Parliament' as transparency rules floated for platforms An Australian Senate Committee has recommended banning Chinese social media apps in the land down under, on grounds the Communist Party of China uses them to spread propaganda and misinformation.... [...]
