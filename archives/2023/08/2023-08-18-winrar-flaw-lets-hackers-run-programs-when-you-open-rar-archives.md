Title: WinRAR flaw lets hackers run programs when you open RAR archives
Date: 2023-08-18T13:20:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-08-18-winrar-flaw-lets-hackers-run-programs-when-you-open-rar-archives

[Source](https://www.bleepingcomputer.com/news/security/winrar-flaw-lets-hackers-run-programs-when-you-open-rar-archives/){:target="_blank" rel="noopener"}

> A high-severity vulnerability has been fixed in WinRAR, the popular file archiver utility for Windows used by millions, that can execute commands on a computer simply by opening an archive. [...]
