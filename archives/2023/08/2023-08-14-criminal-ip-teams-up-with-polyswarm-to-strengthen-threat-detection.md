Title: Criminal IP Teams Up with PolySwarm to Strengthen Threat Detection
Date: 2023-08-14T10:02:01-04:00
Author: Sponsored by Criminal IP
Category: BleepingComputer
Tags: Security
Slug: 2023-08-14-criminal-ip-teams-up-with-polyswarm-to-strengthen-threat-detection

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-teams-up-with-polyswarm-to-strengthen-threat-detection/){:target="_blank" rel="noopener"}

> The addition of Criminal IP as a new contributor to PolySwarm's malicious URL detection represents a significant leap in specialized threat identification. Learn more from Criminal IP about this new collaboration. [...]
