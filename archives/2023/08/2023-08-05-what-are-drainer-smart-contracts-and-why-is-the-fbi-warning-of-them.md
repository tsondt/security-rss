Title: What are “drainer smart contracts” and why is the FBI warning of them?
Date: 2023-08-05T15:01:13+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;hackers;NFT;NFTs;smart contracts
Slug: 2023-08-05-what-are-drainer-smart-contracts-and-why-is-the-fbi-warning-of-them

[Source](https://arstechnica.com/?p=1959148){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) The FBI is advising potential NFT buyers to be on the lookout for malicious websites that use “drainer smart contracts” to surreptitiously loot cryptocurrency wallets. The websites present themselves as outlets for legitimate NFT projects that provide new offerings. They’re promoted by compromised social media accounts belonging to known NFT developers or accounts made to look like such accounts. Posts frequently try to create a sense of urgency by using phrases such as “limited supply” or by referring to the promotion as a “surprise” or the result of a previously unannounced token minting. “The spoofed websites [...]
