Title: Back to school security against ransomware attacks on K-12 and colleges
Date: 2023-08-15T10:01:02-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-08-15-back-to-school-security-against-ransomware-attacks-on-k-12-and-colleges

[Source](https://www.bleepingcomputer.com/news/security/back-to-school-security-against-ransomware-attacks-on-k-12-and-colleges/){:target="_blank" rel="noopener"}

> As we get back to school, K-12 and colleges are increasingly at risk from ransomware and data theft attacks. Learn more from Specops Software on the steps IT teams at education institutes can take to protect their care orgs from disruption and stolen data. [...]
