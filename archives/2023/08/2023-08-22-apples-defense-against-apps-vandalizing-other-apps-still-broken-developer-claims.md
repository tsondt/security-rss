Title: Apple's defense against apps vandalizing other apps still broken, developer claims
Date: 2023-08-22T08:27:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-08-22-apples-defense-against-apps-vandalizing-other-apps-still-broken-developer-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/22/apple_macos_app_management/){:target="_blank" rel="noopener"}

> Cupertino appears to be blasé about long-standing macOS bug, so coder has blabbed Updated Apple last year introduced a security feature called App Management that's designed to prevent one application from modifying another without authorization under macOS Ventura – but a developer claims it’s not very good at its job under some circumstances.... [...]
