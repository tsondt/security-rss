Title: Apple security boss faces iPads-for-gun-permits bribery charge... again
Date: 2023-08-29T20:32:11+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-08-29-apple-security-boss-faces-ipads-for-gun-permits-bribery-charge-again

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/29/apples_chief_security_officer_facing/){:target="_blank" rel="noopener"}

> 'We will continue fighting this case' global chief's lawyer tells us An appeals court has reversed a 2021 decision to drop a bribery charge against Apple's head of global security, who is accused of donating iPads worth up to $80,000 to a sheriff's office in exchange for giving his Cupertino agents concealed carry weapon licenses.... [...]
