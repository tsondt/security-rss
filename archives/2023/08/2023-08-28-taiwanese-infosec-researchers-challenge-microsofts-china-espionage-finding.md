Title: Taiwanese infosec researchers challenge Microsoft's China espionage finding
Date: 2023-08-28T02:58:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-08-28-taiwanese-infosec-researchers-challenge-microsofts-china-espionage-finding

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/28/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> PLUS: India calls for global action on AI and crypto; Vietnam seeks cybersecurity independence; China bans AI prescribing drugs Asia In Brief Taiwan-based infosec consultancy Team T5 has disputed Microsoft's alleged timeline of just when a Beijing-linked attack group named Flax Typhoon commenced its campaigns.... [...]
