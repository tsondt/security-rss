Title: Kroll Employee SIM-Swapped for Crypto Investor Data
Date: 2023-08-25T18:05:10+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Latest Warnings;BlockFi;FTX;Kroll breach;SIM swapping;T-Mobile
Slug: 2023-08-25-kroll-employee-sim-swapped-for-crypto-investor-data

[Source](https://krebsonsecurity.com/2023/08/kroll-employee-sim-swapped-for-crypto-investor-data/){:target="_blank" rel="noopener"}

> Security consulting giant Kroll disclosed today that a SIM-swapping attack against one of its employees led to the theft of user information for multiple cryptocurrency platforms that are relying on Kroll services in their ongoing bankruptcy proceedings. And there are indications that fraudsters may already be exploiting the stolen data in phishing attacks. Cryptocurrency lender BlockFi and the now-collapsed crypto trading platform FTX each disclosed data breaches this week thanks to a recent SIM-swapping attack targeting an employee of Kroll — the company handling both firms’ bankruptcy restructuring. In a statement released today, New York City-based Kroll said it was [...]
