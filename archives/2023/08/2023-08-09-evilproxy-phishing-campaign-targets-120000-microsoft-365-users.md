Title: EvilProxy phishing campaign targets 120,000 Microsoft 365 users
Date: 2023-08-09T05:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-09-evilproxy-phishing-campaign-targets-120000-microsoft-365-users

[Source](https://www.bleepingcomputer.com/news/security/evilproxy-phishing-campaign-targets-120-000-microsoft-365-users/){:target="_blank" rel="noopener"}

> EvilProxy is becoming one of the more popular phishing platforms to target MFA-protected accounts, with researchers seeing 120,000 phishing emails sent to over a hundred organizations to steal Microsoft 365 accounts. [...]
