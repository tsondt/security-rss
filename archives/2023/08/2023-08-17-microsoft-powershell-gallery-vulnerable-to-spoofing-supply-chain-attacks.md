Title: Microsoft PowerShell Gallery vulnerable to spoofing, supply chain attacks
Date: 2023-08-17T16:00:56-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-08-17-microsoft-powershell-gallery-vulnerable-to-spoofing-supply-chain-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-powershell-gallery-vulnerable-to-spoofing-supply-chain-attacks/){:target="_blank" rel="noopener"}

> Lax policies for package naming on Microsoft's PowerShell Gallery code repository allow threat actors to perform typosquatting attacks, spoof popular packages and potentially lay the ground for massive supply chain attacks. [...]
