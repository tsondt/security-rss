Title: Classiscam fraud-as-a-service expands, now targets banks and 251 brands
Date: 2023-08-31T04:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-31-classiscam-fraud-as-a-service-expands-now-targets-banks-and-251-brands

[Source](https://www.bleepingcomputer.com/news/security/classiscam-fraud-as-a-service-expands-now-targets-banks-and-251-brands/){:target="_blank" rel="noopener"}

> The "Classiscam" scam-as-a-service operation has broadened its reach worldwide, targeting many more brands, countries, and industries, causing more significant financial damage than before. [...]
