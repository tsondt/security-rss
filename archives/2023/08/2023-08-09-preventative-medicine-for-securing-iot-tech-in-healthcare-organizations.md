Title: Preventative medicine for securing IoT tech in healthcare organizations
Date: 2023-08-09T10:02:04-04:00
Author: Sponsored by Outpost24
Category: BleepingComputer
Tags: Security
Slug: 2023-08-09-preventative-medicine-for-securing-iot-tech-in-healthcare-organizations

[Source](https://www.bleepingcomputer.com/news/security/preventative-medicine-for-securing-iot-tech-in-healthcare-organizations/){:target="_blank" rel="noopener"}

> Healthcare organizations are increasingly at risk from threat actors targeting Internet of Medical Things. Learn more from Outpost24 on how attack surface management can secure the IoMT devices. [...]
