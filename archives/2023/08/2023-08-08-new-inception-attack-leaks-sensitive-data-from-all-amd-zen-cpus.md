Title: New Inception attack leaks sensitive data from all AMD Zen CPUs
Date: 2023-08-08T11:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-08-new-inception-attack-leaks-sensitive-data-from-all-amd-zen-cpus

[Source](https://www.bleepingcomputer.com/news/security/new-inception-attack-leaks-sensitive-data-from-all-amd-zen-cpus/){:target="_blank" rel="noopener"}

> Researchers have discovered a new and powerful transient execution attack called 'Inception' that can leak privileged secrets and data using unprivileged processes on all AMD Zen CPUs, including the latest models. [...]
