Title: Tornado Cash 'laundered over $1B' in criminal crypto-coins
Date: 2023-08-23T22:45:18+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-08-23-tornado-cash-laundered-over-1b-in-criminal-crypto-coins

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/23/tornado_cash_founders_indicted/){:target="_blank" rel="noopener"}

> Founder Roman Storm cuffed on conspiracy, sanctions busting charges Two founders of Tornado Cash were formally accused by US prosecutors today of laundering more than $1 billion in criminal proceeds through their cryptocurrency mixer.... [...]
