Title: North Korean hackers had access to Russian missile maker for months, say researchers
Date: 2023-08-08T06:27:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-08-08-north-korean-hackers-had-access-to-russian-missile-maker-for-months-say-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/08/north_korea_hacked_russian_missilemaker/){:target="_blank" rel="noopener"}

> Kim Jong Un's cyber-goons aren't above attacking the regime's few friends Two North Korean hacker groups had access to the internal systems of Russian missile and satellite developer NPO Mashinostoyeniya for five to six months, cyber security firm SentinelOne asserted on Monday. The attack illustrates potential North Korean efforts to advance development of missile and other military tech via cyber espionage.... [...]
