Title: Lapsus$ teen hackers convicted of high-profile cyberattacks
Date: 2023-08-23T18:17:45-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-08-23-lapsus-teen-hackers-convicted-of-high-profile-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/lapsus-teen-hackers-convicted-of-high-profile-cyberattacks/){:target="_blank" rel="noopener"}

> A London jury has found that an 18-year-old member of the Lapsus$ data extortion gang helped hack multiple high-profile companies, stole data from them, and demanded a ransom threatening to leak the information. [...]
