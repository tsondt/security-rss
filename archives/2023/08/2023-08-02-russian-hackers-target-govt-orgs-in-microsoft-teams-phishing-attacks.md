Title: Russian hackers target govt orgs in Microsoft Teams phishing attacks
Date: 2023-08-02T15:52:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-08-02-russian-hackers-target-govt-orgs-in-microsoft-teams-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-target-govt-orgs-in-microsoft-teams-phishing-attacks/){:target="_blank" rel="noopener"}

> Microsoft says a hacking group tracked as APT29 and linked to Russia's Foreign Intelligence Service (SVR) targeted dozens of organizations worldwide, including government agencies, in Microsoft Teams phishing attacks. [...]
