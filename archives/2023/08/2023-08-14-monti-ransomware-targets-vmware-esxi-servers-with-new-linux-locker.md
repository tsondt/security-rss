Title: Monti ransomware targets VMware ESXi servers with new Linux locker
Date: 2023-08-14T12:12:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-14-monti-ransomware-targets-vmware-esxi-servers-with-new-linux-locker

[Source](https://www.bleepingcomputer.com/news/security/monti-ransomware-targets-vmware-esxi-servers-with-new-linux-locker/){:target="_blank" rel="noopener"}

> The Monti ransomware has returned to action after a two-month hiatus, now targeting primarily legal and government organizations, and VMware ESXi servers using a new Linux variant that is vastly different from its predecessors. [...]
