Title: US Cyber Command boss says China's spooky cyber skills still behind
Date: 2023-08-11T05:27:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-08-11-us-cyber-command-boss-says-chinas-spooky-cyber-skills-still-behind

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/11/nsa_boss_says_chinas_hackers/){:target="_blank" rel="noopener"}

> Paul Nakasone rates the Middle Kingdom a 'pacing challenge' The boss of US Cyber Command has opined that China's cyber and surveillance capabilities are not ahead of, or even comparable to, to those of the United States.... [...]
