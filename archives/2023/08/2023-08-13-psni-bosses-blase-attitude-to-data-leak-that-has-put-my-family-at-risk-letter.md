Title: PSNI bosses’ blase attitude to data leak that has put my family at risk | Letter
Date: 2023-08-13T16:30:47+00:00
Author: Guardian Staff
Category: The Guardian
Tags: Police;UK news;Technology;Data and computer security;Northern Ireland
Slug: 2023-08-13-psni-bosses-blase-attitude-to-data-leak-that-has-put-my-family-at-risk-letter

[Source](https://www.theguardian.com/uk-news/2023/aug/13/psni-bosses-blase-attitude-to-data-leak-that-has-put-my-family-at-risk){:target="_blank" rel="noopener"}

> A serving member of Police Service Northern Ireland on the sacrifices that have been made and the lack of support from the top I’m a serving officer with the Police Service of Northern Ireland, from a nationalist background in West Belfast. I couldn’t join the RUC because it would have been too dangerous because of who I was and where I was from. Nevertheless, I took the plunge and eventually joined the PSNI. My immediate family had to be schooled with an elaborate backstory. Wider family were not told – I made my siblings lie to their children for years [...]
