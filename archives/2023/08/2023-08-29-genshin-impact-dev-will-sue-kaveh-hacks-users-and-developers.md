Title: Genshin Impact dev will sue Kaveh Hacks users and developers
Date: 2023-08-29T09:20:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming;Legal
Slug: 2023-08-29-genshin-impact-dev-will-sue-kaveh-hacks-users-and-developers

[Source](https://www.bleepingcomputer.com/news/security/genshin-impact-dev-will-sue-kaveh-hacks-users-and-developers/){:target="_blank" rel="noopener"}

> Genshin Impact developer miHoYohas responded to an in-game hacking situation that has caused problems recently in its player community, warning that they would take legal action against those responsible. [...]
