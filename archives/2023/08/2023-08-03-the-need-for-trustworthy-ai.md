Title: The Need for Trustworthy AI
Date: 2023-08-03T11:17:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;trust
Slug: 2023-08-03-the-need-for-trustworthy-ai

[Source](https://www.schneier.com/blog/archives/2023/08/the-need-for-trustworthy-ai.html){:target="_blank" rel="noopener"}

> If you ask Alexa, Amazon’s voice assistant AI system, whether Amazon is a monopoly, it responds by saying it doesn’t know. It doesn’t take much to make it lambaste the other tech giants, but it’s silent about its own corporate parent’s misdeeds. When Alexa responds in this way, it’s obvious that it is putting its developer’s interests ahead of yours. Usually, though, it’s not so obvious whom an AI system is serving. To avoid being exploited by these systems, people will need to learn to approach AI skeptically. That means deliberately constructing the input you give it and thinking critically [...]
