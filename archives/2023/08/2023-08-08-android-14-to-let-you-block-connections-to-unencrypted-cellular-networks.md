Title: Android 14 to let you block connections to unencrypted cellular networks
Date: 2023-08-08T12:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Google;Mobile;Security
Slug: 2023-08-08-android-14-to-let-you-block-connections-to-unencrypted-cellular-networks

[Source](https://www.bleepingcomputer.com/news/google/android-14-to-let-you-block-connections-to-unencrypted-cellular-networks/){:target="_blank" rel="noopener"}

> Google has announced new cellular security features for its upcoming Android 14, expected later this month, that aim to protect business data and communications. [...]
