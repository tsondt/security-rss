Title: Thousands of Android APKs use compression trick to thwart analysis
Date: 2023-08-17T10:51:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-17-thousands-of-android-apks-use-compression-trick-to-thwart-analysis

[Source](https://www.bleepingcomputer.com/news/security/thousands-of-android-apks-use-compression-trick-to-thwart-analysis/){:target="_blank" rel="noopener"}

> Threat actors increasingly distribute malicious Android APKs (packaged app installers) that resist decompilation using unsupported, unknown, or heavily tweaked compression algorithms. [...]
