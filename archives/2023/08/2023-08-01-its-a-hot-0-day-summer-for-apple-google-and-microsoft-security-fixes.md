Title: It’s a hot 0-day summer for Apple, Google, and Microsoft security fixes
Date: 2023-08-01T17:55:39+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security;android;apple;google;microsoft;patches;security patches;syndication;zero-day
Slug: 2023-08-01-its-a-hot-0-day-summer-for-apple-google-and-microsoft-security-fixes

[Source](https://arstechnica.com/?p=1958098){:target="_blank" rel="noopener"}

> Enlarge (credit: WIRED staff ) The summer patch cycle shows no signs of slowing down, with tech giants Apple, Google, and Microsoft releasing multiple updates to fix flaws being used in real-life attacks. July also saw serious bugs squashed by enterprise software firms SAP, Citrix, and Oracle. Here’s everything you need to know about the major patches released during the month. Apple iOS and iPadOS 16.6 Apple had a busy July after issuing two separate security updates during the month. The iPhone maker’s first update came in the form of a security-only Rapid Security Response patch. Read 26 remaining paragraphs [...]
