Title: The devil in the detail
Date: 2023-08-22T12:46:10+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-08-22-the-devil-in-the-detail

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/22/the_devil_in_the_detail/){:target="_blank" rel="noopener"}

> How AI is powering ransomware attacks on applications Webinar You could be forgiven for wondering if anything can ever again be completely straightforward or demonstrably authentic in a world where generative AI can masquerade convincingly as your mother, or express itself in the exact language your best friend might use.... [...]
