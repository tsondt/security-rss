Title: AI researchers claim 93% accuracy in detecting keystrokes over Zoom audio
Date: 2023-08-07T18:17:14+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: AI;Biz & IT;Tech;attention;Keyboards;keylogging;keystrokes;MacBook;Macbooks;machine learning;side channel attack;side channel attacks;web conferencing;zoom
Slug: 2023-08-07-ai-researchers-claim-93-accuracy-in-detecting-keystrokes-over-zoom-audio

[Source](https://arstechnica.com/?p=1959255){:target="_blank" rel="noopener"}

> Enlarge / Some people hate to hear other people's keyboards on video calls, but AI-backed side channel attackers? They say crank that gain. (credit: Getty Images) By recording keystrokes and training a deep learning model, three researchers claim to have achieved upwards of 90 percent accuracy in interpreting remote keystrokes, based on the sound profiles of individual keys. In their paper A Practical Deep Learning-Based Acoustic Side Channel Attack on Keyboards ( full PDF ), UK researchers Joshua Harrison, Ehsan Toreini, and Marhyam Mehrnezhad claim that the trio of ubiquitous machine learning, microphones, and video calls "present a greater threat [...]
