Title: CyberShield: helping governments stand united against cyber attacks
Date: 2023-08-10T13:00:00+00:00
Author: Eric Doerr
Category: GCP Security
Tags: Security & Identity;Public Sector
Slug: 2023-08-10-cybershield-helping-governments-stand-united-against-cyber-attacks

[Source](https://cloud.google.com/blog/topics/public-sector/cybershield-helping-governments-stand-united-against-cyber-attacks/){:target="_blank" rel="noopener"}

> At the Cyber Week conference in June, we were excited to announce our strategic partnership with the Israel National Cyber Directorate (INCD) to modernize security operations under the wider scope of Israel’s own Cyber Dome mission. Our teams worked together to leverage Google Cloud’s Chronicle Security Operations platform, which includes automation, analytics, threat intelligence, and AI to create a multi tier national solution that includes Israel’s national security operations center (SOC), sector SOCs, and participating organizations. “Combining the right people with the right technology is how you do cyber defense the right way,” said Gaby Portnoy, Director General of the [...]
