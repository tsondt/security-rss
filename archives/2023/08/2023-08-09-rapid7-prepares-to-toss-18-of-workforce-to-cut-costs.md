Title: Rapid7 prepares to toss 18% of workforce to cut costs
Date: 2023-08-09T18:00:13+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-08-09-rapid7-prepares-to-toss-18-of-workforce-to-cut-costs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/09/rapid_7_redundancies/){:target="_blank" rel="noopener"}

> Operating expenses almost as high as actual turnover in latest quarterly numbers Rapid7 is initiating a restructuring process that will involve shedding 18 percent of its workforce after net losses widened over the most recent quarter.... [...]
