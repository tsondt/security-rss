Title: Akira ransomware targets Cisco VPNs to breach organizations
Date: 2023-08-22T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-22-akira-ransomware-targets-cisco-vpns-to-breach-organizations

[Source](https://www.bleepingcomputer.com/news/security/akira-ransomware-targets-cisco-vpns-to-breach-organizations/){:target="_blank" rel="noopener"}

> There's mounting evidence that Akira ransomware targets Cisco VPN (virtual private network) products as an attack vector to breach corporate networks, steal, and eventually encrypt data. [...]
