Title: Cybersecurity experts say the west has failed to learn lessons from Ukraine
Date: 2023-08-29T13:18:05+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;Security;Cyberwarfare;hacking;russia;Russian invasion of Ukraine;syndication;Ukraine
Slug: 2023-08-29-cybersecurity-experts-say-the-west-has-failed-to-learn-lessons-from-ukraine

[Source](https://arstechnica.com/?p=1963971){:target="_blank" rel="noopener"}

> Enlarge / Viktor Zhora from Ukraine’s information protection service, says cyber has become a major component of hybrid warfare. (credit: Dragonflypd.com/Black Hat) Viktor Zhora, the public face of Ukraine’s success against Russian cyberattacks, received a hero’s welcome earlier this month on stage at Black Hat, the world’s biggest cybersecurity gathering, in Las Vegas. “The adversary has trained us a lot since 2014,” the year that Russia annexed Crimea, said the deputy chair at Ukraine’s special communication and information protection service. “We evolved by the time of the full-scale invasion [in February last year] when cyber became a major component of [...]
