Title: Good news for Key Group ransomware victims: Free decryptor out now
Date: 2023-08-31T22:47:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-31-good-news-for-key-group-ransomware-victims-free-decryptor-out-now

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/31/key_group_ransomware_decryptor/){:target="_blank" rel="noopener"}

> That's what we call a static shock Even ransomware operators make mistakes, and in the case of ransomware gang the Key Group, a cryptographic error allowed a team of security researchers to develop and release a decryption tool to restore scrambled files.... [...]
