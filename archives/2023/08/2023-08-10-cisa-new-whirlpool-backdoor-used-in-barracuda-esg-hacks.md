Title: CISA: New Whirlpool backdoor used in Barracuda ESG hacks
Date: 2023-08-10T12:06:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-10-cisa-new-whirlpool-backdoor-used-in-barracuda-esg-hacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-new-whirlpool-backdoor-used-in-barracuda-esg-hacks/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity & Infrastructure Security Agency (CISA) has discovered a new backdoor malware named 'Whirlpool' used in attacks on compromised Barracuda Email Security Gateway (ESG) devices. [...]
