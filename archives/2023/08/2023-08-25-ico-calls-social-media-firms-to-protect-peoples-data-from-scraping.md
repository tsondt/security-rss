Title: ICO calls social media firms to protect people's data from scraping
Date: 2023-08-25T12:28:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-08-25-ico-calls-social-media-firms-to-protect-peoples-data-from-scraping

[Source](https://www.bleepingcomputer.com/news/security/ico-calls-social-media-firms-to-protect-peoples-data-from-scraping/){:target="_blank" rel="noopener"}

> UK's Information Commissioner's Office (ICO), together with eleven data protection and privacy authorities from around the world, have published a statement calling social media platforms to up their protections against data scrapers. [...]
