Title: Microsoft DNS boo-boo breaks Hotmail for users around the globe
Date: 2023-08-21T03:34:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-08-21-microsoft-dns-boo-boo-breaks-hotmail-for-users-around-the-globe

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/21/microsoft_dns_booboo_breaks_hotmail/){:target="_blank" rel="noopener"}

> ALSO: NYC says kthxbye to TikTok, slain Microsoft exec's wife indicted, and some ASAP patch warnings Infosec in brief Someone at Microsoft has some explaining to do after a messed-up DNS record caused emails sent from accounts using Microsoft's Outlook Hotmail service to be rejected and directed to spam folders starting on Thursday.... [...]
