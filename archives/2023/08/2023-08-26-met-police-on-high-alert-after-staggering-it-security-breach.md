Title: Met police on high alert after ‘staggering IT security’ breach
Date: 2023-08-26T22:33:27+00:00
Author: PA Media
Category: The Guardian
Tags: Metropolitan police;Police;UK news;Data and computer security
Slug: 2023-08-26-met-police-on-high-alert-after-staggering-it-security-breach

[Source](https://www.theguardian.com/uk-news/2023/aug/26/met-police-on-high-alert-after-it-system-holding-officers-details-hacked){:target="_blank" rel="noopener"}

> Potential leaked data from system with access to names, ranks and photos of officers could do ‘incalculable damage in the wrong hands’ The Metropolitan police is on high alert after a security breach involving the IT system of one of its suppliers, the force said. Scotland Yard is working with the company to understand the scale of the incident but said on Saturday evening that any leaked data could do “incalculable damage” in the wrong hands. Continue reading... [...]
