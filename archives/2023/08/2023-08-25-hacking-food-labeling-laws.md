Title: Hacking Food Labeling Laws
Date: 2023-08-25T11:03:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;hacking;marketing
Slug: 2023-08-25-hacking-food-labeling-laws

[Source](https://www.schneier.com/blog/archives/2023/08/hacking-food-labeling-laws.html){:target="_blank" rel="noopener"}

> This article talks about new Mexican laws about food labeling, and the lengths to which food manufacturers are going to ensure that they are not effective. There are the typical high-pressure lobbying tactics and lawsuits. But there’s also examples of companies hacking the laws: Companies like Coca-Cola and Kraft Heinz have begun designing their products so that their packages don’t have a true front or back, but rather two nearly identical labels—except for the fact that only one side has the required warning. As a result, supermarket clerks often place the products with the warning facing inward, effectively hiding it. [...]
