Title: Announcing general availability of Cloud NAT support for network services Standard Tier
Date: 2023-08-11T16:00:00+00:00
Author: Udit Bhatia
Category: GCP Security
Tags: Networking;Security & Identity
Slug: 2023-08-11-announcing-general-availability-of-cloud-nat-support-for-network-services-standard-tier

[Source](https://cloud.google.com/blog/products/identity-security/announcing-ga-of-cloud-nat-support-for-standard-tier-egress/){:target="_blank" rel="noopener"}

> We are excited to announce general availability of Cloud NAT support for network services Standard Tier. Standard Tier delivers traffic from Google Cloud resources to external systems by routing it over the internet. Premier and Standard are network service tiers that let you optimize connectivity between systems on the internet and your Google Cloud instances. Premium Tier delivers traffic on Google's premium backbone, while Standard Tier uses regular ISP networks. With Cloud NAT support for Standard Tier, you can now get the benefits of Cloud NAT with the cost savings from the Standard Tier. Cloud NAT is a powerful tool [...]
