Title: 'Millions' of spammy emails with no opt-out? That'll cost you $650K, Experian
Date: 2023-08-22T21:58:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-08-22-millions-of-spammy-emails-with-no-opt-out-thatll-cost-you-650k-experian

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/22/experian_doj_ftc/){:target="_blank" rel="noopener"}

> Credit-reporting giant disagrees with FTC, will hand over the pocket change to make Feds go away Experian has agreed to cough up $650,000 after being accused of spamming people with no opt-out button.... [...]
