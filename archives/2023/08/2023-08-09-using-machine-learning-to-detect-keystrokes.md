Title: Using Machine Learning to Detect Keystrokes
Date: 2023-08-09T11:08:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;machine learning;side-channel attacks;smartphones
Slug: 2023-08-09-using-machine-learning-to-detect-keystrokes

[Source](https://www.schneier.com/blog/archives/2023/08/using-machine-learning-to-detect-keystrokes.html){:target="_blank" rel="noopener"}

> Researchers have trained a ML model to detect keystrokes by sound with 95% accuracy. “A Practical Deep Learning-Based Acoustic Side Channel Attack on Keyboards” Abstract: With recent developments in deep learning, the ubiquity of microphones and the rise in online services via personal devices, acoustic side channel attacks present a greater threat to keyboards than ever. This paper presents a practical implementation of a state-of-the-art deep learning model in order to classify laptop keystrokes, using a smartphone integrated microphone. When trained on keystrokes recorded by a nearby phone, the classifier achieved an accuracy of 95%, the highest accuracy seen without [...]
