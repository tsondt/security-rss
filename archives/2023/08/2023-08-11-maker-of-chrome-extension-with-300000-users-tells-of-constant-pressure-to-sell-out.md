Title: Maker of Chrome extension with 300,000+ users tells of constant pressure to sell out
Date: 2023-08-11T17:29:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-08-11-maker-of-chrome-extension-with-300000-users-tells-of-constant-pressure-to-sell-out

[Source](https://go.theregister.com/feed/www.theregister.com/2023/08/11/chrome_extension_developer_pressure/){:target="_blank" rel="noopener"}

> Anyone with sizable audience in this surveillance economy is invited to stuff their add-ons with tracking and ads Interview In the past nine years, Oleg Anashkin, a software developer based in San Jose, California, has received more than 130 solicitations to monetize his Chrome browser extension, Hover Zoom+.... [...]
