Title: Friday Squid Blogging: NIWA Annual Squid Survey
Date: 2023-08-11T21:09:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-08-11-friday-squid-blogging-niwa-annual-squid-survey

[Source](https://www.schneier.com/blog/archives/2023/08/friday-squid-blogging-niwa-annual-squid-survey.html){:target="_blank" rel="noopener"}

> Results from the National Institute of Water and Atmospheric Research Limited annual squid survey : This year, the team unearthed spectacular large hooked squids, weighing about 15kg and sitting at 2m long, a Taningia—­which has the largest known light organs in the animal kingdom­—and a few species that remain very rare in collections worldwide, such as the “scaled” squid Lepidoteuthis and the Batoteuthis skolops. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
