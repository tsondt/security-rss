Title: Tesla infotainment jailbreak unlocks paid features, extracts secrets
Date: 2023-08-06T11:06:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-08-06-tesla-infotainment-jailbreak-unlocks-paid-features-extracts-secrets

[Source](https://www.bleepingcomputer.com/news/security/tesla-infotainment-jailbreak-unlocks-paid-features-extracts-secrets/){:target="_blank" rel="noopener"}

> Researchers from the Technical University of Berlin have developed a method to hack the AMD-based infotainment systems used in all recent Tesla car models and make it run any software they choose, aka achieve 'jailbreak.' [...]
