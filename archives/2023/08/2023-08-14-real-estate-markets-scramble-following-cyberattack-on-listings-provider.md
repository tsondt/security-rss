Title: Real estate markets scramble following cyberattack on listings provider
Date: 2023-08-14T21:59:05+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;cyberattack;MLS;ransomware;real estate
Slug: 2023-08-14-real-estate-markets-scramble-following-cyberattack-on-listings-provider

[Source](https://arstechnica.com/?p=1960824){:target="_blank" rel="noopener"}

> Enlarge / MLS (Multiple Listing Service). (credit: Getty Images) Home buyers, sellers, real estate agents, and listing websites throughout the US have been stymied for five days by a cyberattack on a California company that provides a crucial online service used to track home listings. The attack, which commenced last Wednesday, hit Rapottoni, a software and services provider that supplies Multiple Listing Services to regional real estate groups nationwide. Better known as MLS, it provides instant access to data on which homes are coming to the market, purchase offers, and sales of listed homes. MLS has become essential for connecting [...]
