Title: Brave Search adds private image and video search capability
Date: 2023-08-03T12:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-08-03-brave-search-adds-private-image-and-video-search-capability

[Source](https://www.bleepingcomputer.com/news/security/brave-search-adds-private-image-and-video-search-capability/){:target="_blank" rel="noopener"}

> The privacy-focused search engine Brave Search has finally introduced its own, independent image and video search capabilities, breaking free from relying on Bing and Google for media search. [...]
