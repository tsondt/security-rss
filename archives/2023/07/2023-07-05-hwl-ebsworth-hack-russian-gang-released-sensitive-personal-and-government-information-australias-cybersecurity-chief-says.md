Title: HWL Ebsworth hack: Russian gang released ‘sensitive personal and government information’, Australia’s cybersecurity chief says
Date: 2023-07-05T08:11:35+00:00
Author: Henry Belot
Category: The Guardian
Tags: Cybercrime;Australia news;Internet;Data and computer security;Privacy
Slug: 2023-07-05-hwl-ebsworth-hack-russian-gang-released-sensitive-personal-and-government-information-australias-cybersecurity-chief-says

[Source](https://www.theguardian.com/technology/2023/jul/05/hwl-ebsworth-hack-russian-gang-released-sensitive-personal-and-government-information-australian-cybersecurity-chief-says){:target="_blank" rel="noopener"}

> National cybersecurity coordinator Darren Goldie says ransomware gang ALPHV/Blackcat is responsible for posting the material online Get our morning and afternoon news emails, free app or daily news podcast Sensitive and personal government information has been stolen from law firm HWL Ebsworth by a Russian ransomware gang and posted online, Australia’s new cybersecurity chief says. The significant breach was confirmed by new national cybersecurity coordinator, Darren Goldie, who said he was still working with the law firm to understand how many Australians have been affected. Sign up for Guardian Australia’s free morning and afternoon email newsletters for your daily news [...]
