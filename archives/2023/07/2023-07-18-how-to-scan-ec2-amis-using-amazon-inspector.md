Title: How to scan EC2 AMIs using Amazon Inspector
Date: 2023-07-18T16:43:50+00:00
Author: Luke Notley
Category: AWS Security
Tags: Amazon Inspector;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon EC2;AWS Systems Manager;EC2;Security Blog
Slug: 2023-07-18-how-to-scan-ec2-amis-using-amazon-inspector

[Source](https://aws.amazon.com/blogs/security/how-to-scan-ec2-amis-using-amazon-inspector/){:target="_blank" rel="noopener"}

> Amazon Inspector is an automated vulnerability management service that continually scans Amazon Web Services (AWS) workloads for software vulnerabilities and unintended network exposure. Amazon Inspector supports vulnerability reporting and deep inspection of Amazon Elastic Compute Cloud (Amazon EC2) instances, container images stored in Amazon Elastic Container Registry (Amazon ECR), and AWS Lambda functions. Operating system [...]
