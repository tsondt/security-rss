Title: MOVEit body count closes in on 400 orgs, 20M+ individuals
Date: 2023-07-20T21:01:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-20-moveit-body-count-closes-in-on-400-orgs-20m-individuals

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/20/moveit_victim_count/){:target="_blank" rel="noopener"}

> 'One of the most significant hacks of recent years,' we're told The number of victims and costs tied to the MOVEit file transfer hack continues to climb as the fallout from the massive supply chain attack enters week seven.... [...]
