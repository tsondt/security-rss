Title: Over 130,000 solar energy monitoring systems exposed online
Date: 2023-07-06T05:04:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-06-over-130000-solar-energy-monitoring-systems-exposed-online

[Source](https://www.bleepingcomputer.com/news/security/over-130-000-solar-energy-monitoring-systems-exposed-online/){:target="_blank" rel="noopener"}

> Security researchers are warning that tens of thousands of photovoltaic (PV) monitoring and diagnostic systems are reachable over the public web, making them potential targets for hackers. [...]
