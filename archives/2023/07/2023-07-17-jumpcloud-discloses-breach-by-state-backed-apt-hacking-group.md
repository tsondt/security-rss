Title: JumpCloud discloses breach by state-backed APT hacking group
Date: 2023-07-17T09:20:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-17-jumpcloud-discloses-breach-by-state-backed-apt-hacking-group

[Source](https://www.bleepingcomputer.com/news/security/jumpcloud-discloses-breach-by-state-backed-apt-hacking-group/){:target="_blank" rel="noopener"}

> US-based enterprise software firm JumpCloud says a state-backed hacking group breached its systems almost one month ago as part of a highly targeted attack focused on a limited set of customers. [...]
