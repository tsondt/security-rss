Title: Chinese companies evade sanctions, fuel Moscow’s war on Ukraine, says report
Date: 2023-07-28T19:27:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-07-28-chinese-companies-evade-sanctions-fuel-moscows-war-on-ukraine-says-report

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/28/chinese_companies_evade_sanctions_fuel/){:target="_blank" rel="noopener"}

> PRC semiconductor exports curiously rose 19% y-o-y for first 9 months of 2022 Chinese companies, including state-owned defense companies, are evading tech sanctions and fueling Moscow’s war in Ukraine, according to a US report released on Thursday.... [...]
