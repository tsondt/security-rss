Title: Microsoft enhances Windows 11 Phishing Protection with new features
Date: 2023-07-23T13:02:41-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-07-23-microsoft-enhances-windows-11-phishing-protection-with-new-features

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-enhances-windows-11-phishing-protection-with-new-features/){:target="_blank" rel="noopener"}

> Microsoft is further enhancing the Windows 11 Enhanced Phishing Protection by testing a new feature that warns users when they copy and paste their Windows password into websites and documents. [...]
