Title: Hackers target European government entities in SmugX campaign
Date: 2023-07-03T12:44:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-03-hackers-target-european-government-entities-in-smugx-campaign

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-european-government-entities-in-smugx-campaign/){:target="_blank" rel="noopener"}

> A phishing campaign that security researchers named SmugX and attributed to a Chinese threat actor has been targeting embassies and foreign affairs ministries in the UK, France, Sweden, Ukraine, Czech, Hungary, and Slovakia, since December 2022. [...]
