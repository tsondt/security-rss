Title: AWS re:Inforce 2023: Key announcements and session highlights
Date: 2023-07-21T12:49:36+00:00
Author: Nisha Amthul
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS re:Inforce;Live Events;re:Inforce;re:Inforce 2023;Security Blog
Slug: 2023-07-21-aws-reinforce-2023-key-announcements-and-session-highlights

[Source](https://aws.amazon.com/blogs/security/aws-reinforce-2023-key-announcements-and-session-highlights/){:target="_blank" rel="noopener"}

> Thank you to everyone who participated in AWS re:Inforce 2023, both virtually and in-person. The conference featured a lineup of over 250 engaging sessions and hands-on labs, in collaboration with more than 80 AWS partner sponsors, over two days of immersive cloud security learning. The keynote was delivered by CJ Moses, AWS Chief Information Security [...]
