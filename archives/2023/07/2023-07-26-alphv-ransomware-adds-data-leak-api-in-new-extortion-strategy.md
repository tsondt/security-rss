Title: ALPHV ransomware adds data leak API in new extortion strategy
Date: 2023-07-26T02:34:46-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-07-26-alphv-ransomware-adds-data-leak-api-in-new-extortion-strategy

[Source](https://www.bleepingcomputer.com/news/security/alphv-ransomware-adds-data-leak-api-in-new-extortion-strategy/){:target="_blank" rel="noopener"}

> The ALPHV ransomware gang, also referred to as BlackCat, is trying to put more pressure on their victims to pay a ransom by providing an API for their leak site to increase visibility for their attacks. [...]
