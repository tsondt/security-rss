Title: Dublin Airport staff pay data 'compromised' by criminals
Date: 2023-07-03T15:14:59+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-07-03-dublin-airport-staff-pay-data-compromised-by-criminals

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/03/dublin_airport_data/){:target="_blank" rel="noopener"}

> Attackers accessed it via third-party services provider, says management group It's an awkward Monday for Dublin Airport after pay and benefits details for some 2,000 staff were apparently "compromised" following a recent attack on professional service provider Aon.... [...]
