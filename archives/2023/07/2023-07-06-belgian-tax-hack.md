Title: Belgian Tax Hack
Date: 2023-07-06T11:03:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hacking
Slug: 2023-07-06-belgian-tax-hack

[Source](https://www.schneier.com/blog/archives/2023/07/belgian-tax-hack.html){:target="_blank" rel="noopener"}

> Here’s a fascinating tax hack from Belgium (listen to the details here, episode #484 of “No Such Thing as a Fish,” at 28:00). Basically, it’s about a music festival on the border between Belgium and Holland. The stage was in Holland, but the crowd was in Belgium. When the copyright collector came around, they argued that they didn’t have to pay any tax because the audience was in a different country. Supposedly it worked. [...]
