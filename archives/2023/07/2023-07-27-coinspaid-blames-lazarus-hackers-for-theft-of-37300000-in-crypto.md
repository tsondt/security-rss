Title: CoinsPaid blames Lazarus hackers for theft of $37,300,000 in crypto
Date: 2023-07-27T17:58:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-07-27-coinspaid-blames-lazarus-hackers-for-theft-of-37300000-in-crypto

[Source](https://www.bleepingcomputer.com/news/security/coinspaid-blames-lazarus-hackers-for-theft-of-37-300-000-in-crypto/){:target="_blank" rel="noopener"}

> Estonian crypto-payments service provider CoinsPaid has announced that it experienced a cyber attack on July 22nd, 2023, that resulted in the theft of $37,200,000 worth of cryptocurrency. [...]
