Title: Ransomware payments on record-breaking trajectory for 2023
Date: 2023-07-12T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-12-ransomware-payments-on-record-breaking-trajectory-for-2023

[Source](https://www.bleepingcomputer.com/news/security/ransomware-payments-on-record-breaking-trajectory-for-2023/){:target="_blank" rel="noopener"}

> Data from the first half of the year indicates that ransomware activity is on track to break previous records, seeing a rise in the number of payments, both big and small. [...]
