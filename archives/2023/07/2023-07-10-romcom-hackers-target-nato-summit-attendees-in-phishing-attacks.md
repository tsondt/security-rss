Title: RomCom hackers target NATO Summit attendees in phishing attacks
Date: 2023-07-10T16:44:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-10-romcom-hackers-target-nato-summit-attendees-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/romcom-hackers-target-nato-summit-attendees-in-phishing-attacks/){:target="_blank" rel="noopener"}

> A threat actor referred to as 'RomCom' has been targeting organizations supporting Ukraine and guests of the upcoming NATO Summit set to start tomorrow in Vilnius, Lithuania. [...]
