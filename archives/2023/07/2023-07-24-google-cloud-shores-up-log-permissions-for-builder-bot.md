Title: Google Cloud shores up log permissions for builder bot
Date: 2023-07-24T04:08:27+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-07-24-google-cloud-shores-up-log-permissions-for-builder-bot

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/24/infosec_in_brief/){:target="_blank" rel="noopener"}

> ALSO: Amazon's child-sized COPPA fine, smart tech security labels coming to the US, and this week's critical vulns Infosec in brief Google Cloud has fixed an issue in which it gave away a little too much info in its audit logs to a service account.... [...]
