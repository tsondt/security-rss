Title: BreachForums database and private chats for sale in hacker data breach
Date: 2023-07-27T17:36:56-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-07-27-breachforums-database-and-private-chats-for-sale-in-hacker-data-breach

[Source](https://www.bleepingcomputer.com/news/security/breachforums-database-and-private-chats-for-sale-in-hacker-data-breach/){:target="_blank" rel="noopener"}

> While consumers are usually the ones worried about their information being exposed in data breaches, it's now the hacker's turn, as the notorious Breached cybercrime forum's database is up for sale and member data shared with Have I Been Pwned. [...]
