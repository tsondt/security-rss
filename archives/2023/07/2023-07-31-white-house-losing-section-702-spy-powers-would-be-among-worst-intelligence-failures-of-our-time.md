Title: White House: Losing Section 702 spy powers would be among 'worst intelligence failures of our time'
Date: 2023-07-31T19:58:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-31-white-house-losing-section-702-spy-powers-would-be-among-worst-intelligence-failures-of-our-time

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/31/biden_section_702_intelligence/){:target="_blank" rel="noopener"}

> As expert panel suggests some tweaks to boost public's confidence in FISA The White House has weighed in on the Section 702 debate, urging lawmakers to reauthorize, "without new and operationally damaging restrictions," the controversial snooping powers before they expire at the end of the year.... [...]
