Title: Almost 40% of Ubuntu users vulnerable to new privilege elevation flaws
Date: 2023-07-26T14:51:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-07-26-almost-40-of-ubuntu-users-vulnerable-to-new-privilege-elevation-flaws

[Source](https://www.bleepingcomputer.com/news/security/almost-40-percent-of-ubuntu-users-vulnerable-to-new-privilege-elevation-flaws/){:target="_blank" rel="noopener"}

> Two Linux vulnerabilities introduced recently into the Ubuntu kernel create the potential for unprivileged local users to gain elevated privileges on a massive number of devices. [...]
