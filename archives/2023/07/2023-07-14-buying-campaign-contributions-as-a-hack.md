Title: Buying Campaign Contributions as a Hack
Date: 2023-07-14T11:09:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;debates;hacking;voting
Slug: 2023-07-14-buying-campaign-contributions-as-a-hack

[Source](https://www.schneier.com/blog/archives/2023/07/buying-campaign-contributions-as-a-hack.html){:target="_blank" rel="noopener"}

> The first Republican primary debate has a popularity threshold to determine who gets to appear: 40,000 individual contributors. Now there are a lot of conventional ways a candidate can get that many contributors. Doug Burgum came up with a novel idea: buy them : A long-shot contender at the bottom of recent polls, Mr. Burgum is offering $20 gift cards to the first 50,000 people who donate at least $1 to his campaign. And one lucky donor, as his campaign advertised on Facebook, will have the chance to win a Yeti Tundra 45 cooler that typically costs more than $300—just [...]
