Title: Under CISA <s>pressure</s> collaboration, Microsoft makes cloud security logs available for free
Date: 2023-07-20T12:30:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-07-20-under-cisa-pressure-collaboration-microsoft-makes-cloud-security-logs-available-for-free

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/20/under_cisa_spressures_collaboration_microsoft/){:target="_blank" rel="noopener"}

> In hindsight, it's probably good practice to give clients access to cloud logs Microsoft announced on Wednesday it would provide all customers free access to cloud security logs – a service usually reserved for premium clients – within weeks of a reveal that government officials' cloud-based emails were targets of an alleged China-based hack.... [...]
