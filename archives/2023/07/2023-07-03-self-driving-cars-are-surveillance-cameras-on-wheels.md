Title: Self-Driving Cars Are Surveillance Cameras on Wheels
Date: 2023-07-03T11:04:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;crime;law enforcement;privacy;surveillance
Slug: 2023-07-03-self-driving-cars-are-surveillance-cameras-on-wheels

[Source](https://www.schneier.com/blog/archives/2023/07/self-driving-cars-are-surveillance-cameras-on-wheels.html){:target="_blank" rel="noopener"}

> Police are already using self-driving car footage as video evidence: While security cameras are commonplace in American cities, self-driving cars represent a new level of access for law enforcement ­ and a new method for encroachment on privacy, advocates say. Crisscrossing the city on their routes, self-driving cars capture a wider swath of footage. And it’s easier for law enforcement to turn to one company with a large repository of videos and a dedicated response team than to reach out to all the businesses in a neighborhood with security systems. “We’ve known for a long time that they are essentially [...]
