Title: Stolen Microsoft key may have opened up a lot more than US govt email inboxes
Date: 2023-07-21T22:58:49+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-21-stolen-microsoft-key-may-have-opened-up-a-lot-more-than-us-govt-email-inboxes

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/21/microsoft_key_skeleton/){:target="_blank" rel="noopener"}

> How does the Azure giant come back from this? A stolen Microsoft security key may have allowed Beijing-backed spies to break into a lot more than just Outlook and Exchange Online email accounts.... [...]
