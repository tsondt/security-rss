Title: Ambulance patient records system hauled offline for cyber-attack probe
Date: 2023-07-26T09:01:42+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-07-26-ambulance-patient-records-system-hauled-offline-for-cyber-attack-probe

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/26/uk_ambulance_services_cyber_attack/){:target="_blank" rel="noopener"}

> UK trusts serving 12 million people affected as vendor awaits results of forensic investigation Several UK NHS ambulance organizations have been struggling to record patient data and pass it to other providers following a cyber-attack aimed at health software company Ortivus.... [...]
