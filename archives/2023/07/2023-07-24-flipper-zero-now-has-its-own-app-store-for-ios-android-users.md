Title: Flipper Zero now has its own app store for iOS, Android users
Date: 2023-07-24T13:22:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-07-24-flipper-zero-now-has-its-own-app-store-for-ios-android-users

[Source](https://www.bleepingcomputer.com/news/security/flipper-zero-now-has-its-own-app-store-for-ios-android-users/){:target="_blank" rel="noopener"}

> The Flipper Zero team has launched its very own 'Flipper Apps' mobile app store, allowing mobile users to install 3rd-party apps and extend the functionality of the popular wireless pen-testing tool. [...]
