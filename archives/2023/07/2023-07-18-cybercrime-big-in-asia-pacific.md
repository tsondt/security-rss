Title: Cybercrime – big in Asia Pacific
Date: 2023-07-18T02:43:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-07-18-cybercrime-big-in-asia-pacific

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/18/cybercrime_big_in_asia_pacific/){:target="_blank" rel="noopener"}

> SANS first DFIR Summit in Asia gives organizations in Asia Pacific an opportunity to build their cyber security expertise Sponsored Post Kroll's latest State of Incident Response: APAC report suggests that over half of all organizations in Asia Pacific (59 percent) have experienced a cyber incident, of which a third (32 percent) have suffered multiple incidents.... [...]
