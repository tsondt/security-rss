Title: Ukraine takes down massive bot farm, seizes 150,000 SIM cards
Date: 2023-07-19T07:03:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-19-ukraine-takes-down-massive-bot-farm-seizes-150000-sim-cards

[Source](https://www.bleepingcomputer.com/news/security/ukraine-takes-down-massive-bot-farm-seizes-150-000-sim-cards/){:target="_blank" rel="noopener"}

> Cyber ​​Police Department of the National Police of Ukraine dismantled another massive bot farm linked to more than 100 individuals after searches at almost two dozen locations. [...]
