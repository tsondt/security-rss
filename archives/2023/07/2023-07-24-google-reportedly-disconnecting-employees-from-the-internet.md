Title: Google Reportedly Disconnecting Employees from the Internet
Date: 2023-07-24T11:09:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;Google;Internet
Slug: 2023-07-24-google-reportedly-disconnecting-employees-from-the-internet

[Source](https://www.schneier.com/blog/archives/2023/07/google-reportedly-disconnecting-employees-from-the-internet.html){:target="_blank" rel="noopener"}

> Supposedly Google is starting a pilot program of disabling Internet connectivity from employee computers: The company will disable internet access on the select desktops, with the exception of internal web-based tools and Google-owned websites like Google Drive and Gmail. Some workers who need the internet to do their job will get exceptions, the company stated in materials. Google has not confirmed this story. More news articles. [...]
