Title: CISA orders govt agencies to mitigate Windows and Office zero-days
Date: 2023-07-18T04:41:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-18-cisa-orders-govt-agencies-to-mitigate-windows-and-office-zero-days

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-govt-agencies-to-mitigate-windows-and-office-zero-days/){:target="_blank" rel="noopener"}

> CISA ordered federal agencies to mitigate remote code execution zero-days affecting Windows and Office products that were exploited by the Russian-based RomCom cybercriminal group in NATO phishing attacks. [...]
