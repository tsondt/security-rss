Title: SEO Expert Hired and Fired By Ashley Madison Turned on Company, Promising Revenge
Date: 2023-07-13T21:45:02+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ne'er-Do-Well News;Ashley Madison hack;hulu;Impact Team;William Webster Harrison
Slug: 2023-07-13-seo-expert-hired-and-fired-by-ashley-madison-turned-on-company-promising-revenge

[Source](https://krebsonsecurity.com/2023/07/seo-expert-hired-and-fired-by-ashley-madison-turned-on-company-promising-revenge/){:target="_blank" rel="noopener"}

> [This is Part II of a story published here last week on reporting that went into a new Hulu documentary series on the 2015 Ashley Madison hack.] It was around 9 p.m. on Sunday, July 19, when I received a message through the contact form on KrebsOnSecurity.com that the marital infidelity website AshleyMadison.com had been hacked. The message contained links to confidential Ashley Madison documents, and included a manifesto that said a hacker group calling itself the Impact Team was prepared to leak data on all 37 million users unless Ashley Madison and a sister property voluntarily closed down within [...]
