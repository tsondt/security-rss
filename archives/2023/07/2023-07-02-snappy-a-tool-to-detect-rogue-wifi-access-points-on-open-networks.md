Title: Snappy: A tool to detect rogue WiFi access points on open networks
Date: 2023-07-02T10:17:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-02-snappy-a-tool-to-detect-rogue-wifi-access-points-on-open-networks

[Source](https://www.bleepingcomputer.com/news/security/snappy-a-tool-to-detect-rogue-wifi-access-points-on-open-networks/){:target="_blank" rel="noopener"}

> Cybersecurity researchers have released a new tool called 'Snappy' that can help detect fake or rogue WiFi access points that attempts to steal data from unsuspecting people. [...]
