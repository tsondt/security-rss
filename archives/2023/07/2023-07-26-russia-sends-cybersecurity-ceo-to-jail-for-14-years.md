Title: Russia Sends Cybersecurity CEO to Jail for 14 Years
Date: 2023-07-26T17:29:40+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: The Coming Storm;Evil Corp.;Fancy Bear;Financial Times;Group-IB;GRU;Ilya Sachkov;Kaspersky Lab;Maksim Yakubets;Max Seddon;Ruslan Stoyanov;Sergei Mikhailov
Slug: 2023-07-26-russia-sends-cybersecurity-ceo-to-jail-for-14-years

[Source](https://krebsonsecurity.com/2023/07/russia-sends-cybersecurity-ceo-to-jail-for-14-years/){:target="_blank" rel="noopener"}

> The Russian government today handed down a treason conviction and 14-year prison sentence on Iyla Sachkov, the former founder and CEO of one of Russia’s largest cybersecurity firms. Sachkov, 37, has been detained for nearly two years under charges that the Kremlin has kept classified and hidden from public view, and he joins a growing roster of former Russian cybercrime fighters who are now serving hard time for farcical treason convictions. Ilya Sachkov. Image: Group-IB.com. In 2003, Sachkov founded Group-IB, a cybersecurity and digital forensics company that quickly earned a reputation for exposing and disrupting large-scale cybercrime operations, including quite [...]
