Title: Beijing wants to make the Great Firewall of China even greater
Date: 2023-07-17T18:28:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-07-17-beijing-wants-to-make-the-great-firewall-of-china-even-greater

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/17/great_firewall_even_greater/){:target="_blank" rel="noopener"}

> Also more fiery, with vague but firm orders to create a 'security barrier' Over the weekend Chinese president Xi Jinping gave a directive to officials to build a Beijing-supervised "security barrier" around its internet.... [...]
