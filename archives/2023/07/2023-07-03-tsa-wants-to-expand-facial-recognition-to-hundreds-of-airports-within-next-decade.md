Title: TSA wants to expand facial recognition to hundreds of airports within next decade
Date: 2023-07-03T22:12:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-03-tsa-wants-to-expand-facial-recognition-to-hundreds-of-airports-within-next-decade

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/03/tsa_facial_recognition_airport/){:target="_blank" rel="noopener"}

> Digital rights folks, as you can imagine, want the tech grounded America's Transportation Security Agency (TSA) intends to expand its facial-recognition program used to screen US air travel passengers to 430 domestic airports in under a decade.... [...]
