Title: Kevin Mitnick Died
Date: 2023-07-20T19:44:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hacking;history of security
Slug: 2023-07-20-kevin-mitnick-died

[Source](https://www.schneier.com/blog/archives/2023/07/kevin-mitnick-died.html){:target="_blank" rel="noopener"}

> Obituary. [...]
