Title: Linux version of Abyss Locker ransomware targets VMware ESXi servers
Date: 2023-07-29T11:17:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-07-29-linux-version-of-abyss-locker-ransomware-targets-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-abyss-locker-ransomware-targets-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> The Abyss Locker operation is the latest to develop a Linux encryptor to target VMware's ESXi virtual machines platform in attacks on the enterprise. [...]
