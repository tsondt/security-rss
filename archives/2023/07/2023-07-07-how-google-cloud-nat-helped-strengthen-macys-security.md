Title: How Google Cloud NAT helped strengthen Macy’s security
Date: 2023-07-07T16:00:00+00:00
Author: Brandon Maltzman
Category: GCP Security
Tags: Networking;Security & Identity
Slug: 2023-07-07-how-google-cloud-nat-helped-strengthen-macys-security

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-nat-helped-strengthen-macys-security/){:target="_blank" rel="noopener"}

> Macy’s is well known for its high-end fashion worldwide. What is not as well known are the strong measures it takes to ensure its customers’ data remains secure. When Macy’s decided to move its infrastructure from on-premises to Google Cloud, it required the move be done without sacrificing security or degrading the user experience. Migrating from on-premises to the cloud isn’t always a simple feat, especially when one of Macy’s key requirements was a managed solution that could secure their workloads’ internet access without impacting throughput and latency. Implementing Cloud NAT at scale Applying security safeguards without creating additional friction [...]
