Title: Fooling an AI Article Writer
Date: 2023-07-27T11:04:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;games;social engineering
Slug: 2023-07-27-fooling-an-ai-article-writer

[Source](https://www.schneier.com/blog/archives/2023/07/fooling-an-ai-article-writer.html){:target="_blank" rel="noopener"}

> World of Warcraft players wrote about a fictional game element, “Glorbo,” on a subreddit for the game, trying to entice an AI bot to write an article about it. It worked : And it...worked. Zleague auto-published a post titled “World of Warcraft Players Excited For Glorbo’s Introduction.” [...] That is...all essentially nonsense. The article was left online for a while but has finally been taken down ( here’s a mirror, it’s hilarious ). All the authors listed as having bylines on the site are fake. It appears this entire thing is run with close to zero oversight. Expect lots more [...]
