Title: Friday Squid Blogging: Balloon Squid
Date: 2023-07-14T21:00:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-07-14-friday-squid-blogging-balloon-squid

[Source](https://www.schneier.com/blog/archives/2023/07/friday-squid-blogging-balloon-squid.html){:target="_blank" rel="noopener"}

> Masayoshi Matsumoto is a “master balloon artist,” and he made a squid (and other animals). As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
