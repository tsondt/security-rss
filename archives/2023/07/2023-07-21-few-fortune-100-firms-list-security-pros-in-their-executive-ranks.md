Title: Few Fortune 100 Firms List Security Pros in Their Executive Ranks
Date: 2023-07-21T19:11:16+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Accenture;Datos Insights;IANS;security maturity;Tari Schreider
Slug: 2023-07-21-few-fortune-100-firms-list-security-pros-in-their-executive-ranks

[Source](https://krebsonsecurity.com/2023/07/few-fortune-100-firms-list-security-pros-in-their-executive-ranks/){:target="_blank" rel="noopener"}

> Many things have changed since 2018, such as the names of the companies in the Fortune 100 list. But one aspect of that vaunted list that hasn’t shifted much since is that very few of these companies list any security professionals within their top executive ranks. The next time you receive a breach notification letter that invariably says a company you trusted places a top priority on customer security and privacy, consider this: Only four of the Fortune 100 companies currently list a security professional in the executive leadership pages of their websites. This is actually down from five of [...]
