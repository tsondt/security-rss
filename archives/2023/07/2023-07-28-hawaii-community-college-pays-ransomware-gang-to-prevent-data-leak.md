Title: Hawai'i Community College pays ransomware gang to prevent data leak
Date: 2023-07-28T09:45:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-07-28-hawaii-community-college-pays-ransomware-gang-to-prevent-data-leak

[Source](https://www.bleepingcomputer.com/news/security/hawaii-community-college-pays-ransomware-gang-to-prevent-data-leak/){:target="_blank" rel="noopener"}

> The Hawaiʻi Community College has admitted that it paid a ransom to ransomware actors to prevent the leaking of stolen data of approximately 28,000 people. [...]
