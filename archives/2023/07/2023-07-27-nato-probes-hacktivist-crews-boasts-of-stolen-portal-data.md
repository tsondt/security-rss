Title: NATO probes hacktivist crew's boasts of stolen portal data
Date: 2023-07-27T22:33:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-27-nato-probes-hacktivist-crews-boasts-of-stolen-portal-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/27/nato_investigates_hack/){:target="_blank" rel="noopener"}

> 'Gay furry hackers' say it's in response to 'attacks on human rights' and noooothing to do with Russia-Ukraine NATO is investigating claims by miscreants that they broke into the military alliance's unclassified information-sharing and collaboration IT environment, stole information belonging to 31 nations, and leaked 845 MB of compressed data.... [...]
