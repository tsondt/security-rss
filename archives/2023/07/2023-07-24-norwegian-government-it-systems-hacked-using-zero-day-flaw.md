Title: Norwegian government IT systems hacked using zero-day flaw
Date: 2023-07-24T11:14:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-07-24-norwegian-government-it-systems-hacked-using-zero-day-flaw

[Source](https://www.bleepingcomputer.com/news/security/norwegian-government-it-systems-hacked-using-zero-day-flaw/){:target="_blank" rel="noopener"}

> The Norwegian government is warning that its ICT platform used by 12 ministries has suffered a cyberattack after hackers exploited a zero-day vulnerability in third-party software. [...]
