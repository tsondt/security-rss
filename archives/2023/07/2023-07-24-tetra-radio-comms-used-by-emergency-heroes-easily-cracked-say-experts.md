Title: TETRA radio comms used by emergency heroes easily cracked, say experts
Date: 2023-07-24T23:20:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-07-24-tetra-radio-comms-used-by-emergency-heroes-easily-cracked-say-experts

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/24/tetra_hardware_backdoor_opened_by/){:target="_blank" rel="noopener"}

> If it looks like a backdoor, walks like a backdoor, maybe it's a... Midnight Blue, a security firm based in the Netherlands, has found five vulnerabilities that affect Terrestrial Trunked Radio (TETRA), used in Europe, the United Kingdom, and many other countries by government agencies, law enforcement, and emergency services organizations.... [...]
