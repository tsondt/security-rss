Title: Using Workforce Identity Federation with API-based web applications
Date: 2023-07-21T16:00:00+00:00
Author: Artur Kuliński
Category: GCP Security
Tags: Developers & Practitioners;Security & Identity
Slug: 2023-07-21-using-workforce-identity-federation-with-api-based-web-applications

[Source](https://cloud.google.com/blog/products/identity-security/using-workforce-identity-federation-with-api-based-web-applications/){:target="_blank" rel="noopener"}

> Workforce Identity Federation allows use of an external identity provider (IdP) to authenticate and authorize users (including employees, partners, and contractors) to Google Cloud resources without provisioning identities in Cloud Identity. Before its introduction, only identities existing within Cloud Identity could be used with Cloud Identity Access Management (IAM). Here’s how to configure an example Javascript web application hosted in Google Cloud to call Google Cloud APIs after being authenticated with an Azure AD using Workforce Identity Federation. Workforce Identity can be used with IdPs supporting OpenID Connect (OIDC) or SAML 2.0. You can read more about it in our [...]
