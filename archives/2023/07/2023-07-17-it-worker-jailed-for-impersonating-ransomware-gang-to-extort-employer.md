Title: IT worker jailed for impersonating ransomware gang to extort employer
Date: 2023-07-17T10:47:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-17-it-worker-jailed-for-impersonating-ransomware-gang-to-extort-employer

[Source](https://www.bleepingcomputer.com/news/security/it-worker-jailed-for-impersonating-ransomware-gang-to-extort-employer/){:target="_blank" rel="noopener"}

> 28-year-old Ashley Liles, a former IT employee, has been sentenced to over three years in prison for attempting to blackmail his employer during a ransomware attack. [...]
