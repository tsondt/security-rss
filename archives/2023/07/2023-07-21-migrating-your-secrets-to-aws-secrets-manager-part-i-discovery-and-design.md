Title: Migrating your secrets to AWS Secrets Manager, Part I: Discovery and design
Date: 2023-07-21T16:44:20+00:00
Author: Eric Swamy
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Technical How-to;AWS Secrets Manager;Identity;migration;secrets;Secrets Manager;Security;Security Blog
Slug: 2023-07-21-migrating-your-secrets-to-aws-secrets-manager-part-i-discovery-and-design

[Source](https://aws.amazon.com/blogs/security/migrating-your-secrets-to-aws-secrets-manager-part-i-discovery-and-design/){:target="_blank" rel="noopener"}

> “An ounce of prevention is worth a pound of cure.” – Benjamin Franklin A secret can be defined as sensitive information that is not intended to be known or disclosed to unauthorized individuals, entities, or processes. Secrets like API keys, passwords, and SSH keys provide access to confidential systems and resources, but it can be [...]
