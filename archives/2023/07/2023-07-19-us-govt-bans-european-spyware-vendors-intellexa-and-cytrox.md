Title: US govt bans European spyware vendors Intellexa and Cytrox
Date: 2023-07-19T06:18:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-19-us-govt-bans-european-spyware-vendors-intellexa-and-cytrox

[Source](https://www.bleepingcomputer.com/news/security/us-govt-bans-european-spyware-vendors-intellexa-and-cytrox/){:target="_blank" rel="noopener"}

> The U.S. government has banned European commercial spyware manufacturers Intellexa and Cytrox, citing risks to U.S. national security and foreign policy interests. [...]
