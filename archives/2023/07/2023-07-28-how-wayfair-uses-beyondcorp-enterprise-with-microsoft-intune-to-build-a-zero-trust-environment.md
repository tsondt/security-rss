Title: How Wayfair uses BeyondCorp Enterprise with Microsoft Intune to build a Zero Trust environment
Date: 2023-07-28T16:00:00+00:00
Author: Cristian Rodriguez
Category: GCP Security
Tags: Security & Identity
Slug: 2023-07-28-how-wayfair-uses-beyondcorp-enterprise-with-microsoft-intune-to-build-a-zero-trust-environment

[Source](https://cloud.google.com/blog/products/identity-security/how-wayfair-uses-beyondcorp-enterprise/){:target="_blank" rel="noopener"}

> A Zero Trust approach to security can help you safeguard your users, devices, apps and data. Using Google Cloud’s BeyondCorp Enterprise, organizations can enforce strict, context-aware access controls with authentication and authorization for a variety of devices, anywhere to help strengthen their security posture. The ability to craft rich, contextual access policies depends on the availability of signals about users, their devices, and their current access context, such as location. We designed BeyondCorp Enterprise to be an extensible solution where customers can integrate and incorporate signals from their other technology vendors into their own Zero Trust access policies. One of [...]
