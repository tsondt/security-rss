Title: U.S. preparing Cyber Trust Mark for more secure smart devices
Date: 2023-07-18T21:12:18-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-07-18-us-preparing-cyber-trust-mark-for-more-secure-smart-devices

[Source](https://www.bleepingcomputer.com/news/security/us-preparing-cyber-trust-mark-for-more-secure-smart-devices/){:target="_blank" rel="noopener"}

> A new cybersecurity certification and labeling program called U.S. Cyber Trust Mark is being shaped to help U.S. consumers choose connected devices that are more secure and resilient to hacker attacks. [...]
