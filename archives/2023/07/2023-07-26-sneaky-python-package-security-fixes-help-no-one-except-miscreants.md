Title: Sneaky Python package security fixes help no one – except miscreants
Date: 2023-07-26T07:28:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-07-26-sneaky-python-package-security-fixes-help-no-one-except-miscreants

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/26/python_silent_security_fixes/){:target="_blank" rel="noopener"}

> Good thing these eggheads have created a database of patches Python security fixes often happen through "silent" code commits, without an associated Common Vulnerabilities and Exposures (CVE) identifier, according to a group of computer security researchers.... [...]
