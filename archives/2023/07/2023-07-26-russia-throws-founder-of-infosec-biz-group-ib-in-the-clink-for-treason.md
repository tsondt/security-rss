Title: Russia throws founder of infosec biz Group-IB in the clink for treason
Date: 2023-07-26T20:31:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-26-russia-throws-founder-of-infosec-biz-group-ib-in-the-clink-for-treason

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/26/russia_groupib_founder_prison/){:target="_blank" rel="noopener"}

> Sachkov faces 14-year stretch after 'unreasonably rushed trial' A Russian court has sentenced Ilya Sachkov, the founder of security research house Group-IB, to 14 years in a maximum-security prison after finding the executive guilty of high treason.... [...]
