Title: Barracuda working on fix for ongoing Email Gateway login issues
Date: 2023-07-07T11:19:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-07-barracuda-working-on-fix-for-ongoing-email-gateway-login-issues

[Source](https://www.bleepingcomputer.com/news/security/barracuda-working-on-fix-for-ongoing-email-gateway-login-issues/){:target="_blank" rel="noopener"}

> Email and network security firm Barracuda is working to fix an ongoing issue that triggers invalid login errors and prevents Email Gateway Defense users from signing into their accounts. [...]
