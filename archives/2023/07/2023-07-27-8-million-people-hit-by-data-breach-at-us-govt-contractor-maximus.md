Title: 8 million people hit by data breach at US govt contractor Maximus
Date: 2023-07-27T10:15:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-27-8-million-people-hit-by-data-breach-at-us-govt-contractor-maximus

[Source](https://www.bleepingcomputer.com/news/security/8-million-people-hit-by-data-breach-at-us-govt-contractor-maximus/){:target="_blank" rel="noopener"}

> U.S. government services contractor Maximus has disclosed a data breach warning that hackers stole the personal data of 8 to 11 million people during the recent MOVEit Transfer data-theft attacks. [...]
