Title: AMD Zenbleed chip bug leaks secrets fast and easy
Date: 2023-07-24T20:41:28+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-24-amd-zenbleed-chip-bug-leaks-secrets-fast-and-easy

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/24/amd_zenbleed_bug/){:target="_blank" rel="noopener"}

> Zen 2 flaw more simple than Spectre, exploit code already out there – get patching when you can AMD has started issuing some patches for its processors affected by a serious silicon-level bug dubbed Zenbleed that can be exploited by rogue users and malware to steal passwords, cryptographic keys, and other secrets from software running on a vulnerable system.... [...]
