Title: RIP Kevin Mitnick: Former most-wanted hacker dies at 59
Date: 2023-07-20T18:01:10+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2023-07-20-rip-kevin-mitnick-former-most-wanted-hacker-dies-at-59

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/20/kevin_mitnick_obit/){:target="_blank" rel="noopener"}

> Tributes paid to husband, father, son and rogue-turned-consultant Obit Kevin Mitnick, probably the world's most-famous computer hacker – and subsequently writer, public speaker, and security consultant – has succumbed to pancreatic cancer. He was 59.... [...]
