Title: Apple confirms WebKit security updates break browsing on some sites
Date: 2023-07-11T11:42:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2023-07-11-apple-confirms-webkit-security-updates-break-browsing-on-some-sites

[Source](https://www.bleepingcomputer.com/news/security/apple-confirms-webkit-security-updates-break-browsing-on-some-sites/){:target="_blank" rel="noopener"}

> Apple confirmed today that emergency security updates released on Monday to address a zero-day bug exploited in attacks break browsing on some websites, and new ones will be released soon to address this known issue. [...]
