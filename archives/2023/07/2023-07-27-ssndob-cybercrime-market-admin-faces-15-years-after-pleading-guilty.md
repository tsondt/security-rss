Title: SSNDOB cybercrime market admin faces 15 years after pleading guilty
Date: 2023-07-27T14:08:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-07-27-ssndob-cybercrime-market-admin-faces-15-years-after-pleading-guilty

[Source](https://www.bleepingcomputer.com/news/security/ssndob-cybercrime-market-admin-faces-15-years-after-pleading-guilty/){:target="_blank" rel="noopener"}

> A Ukrainian man, Vitalii Chychasov, has pleaded guilty in the United States to conspiracy to commit access device fraud and trafficking in unauthorized access devices through the now-shutdown SSNDOB Marketplace. [...]
