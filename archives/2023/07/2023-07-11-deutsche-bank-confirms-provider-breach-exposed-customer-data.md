Title: Deutsche Bank confirms provider breach exposed customer data
Date: 2023-07-11T11:51:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-11-deutsche-bank-confirms-provider-breach-exposed-customer-data

[Source](https://www.bleepingcomputer.com/news/security/deutsche-bank-confirms-provider-breach-exposed-customer-data/){:target="_blank" rel="noopener"}

> Deutsche Bank AG has confirmed to BleepingComputer that a data breach on one of its service providers has exposed its customers' data in a likely MOVEit Transfer data-theft attack. [...]
