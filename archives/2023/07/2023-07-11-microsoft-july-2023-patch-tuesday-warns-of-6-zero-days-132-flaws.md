Title: Microsoft July 2023 Patch Tuesday warns of 6 zero-days, 132 flaws
Date: 2023-07-11T13:49:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-07-11-microsoft-july-2023-patch-tuesday-warns-of-6-zero-days-132-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-july-2023-patch-tuesday-warns-of-6-zero-days-132-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's July 2023 Patch Tuesday, with security updates for 132 flaws, including six actively exploited and thirty-seven remote code execution vulnerabilities. [...]
