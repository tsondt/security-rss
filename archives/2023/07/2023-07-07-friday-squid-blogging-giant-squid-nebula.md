Title: Friday Squid Blogging: Giant Squid Nebula
Date: 2023-07-07T21:08:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-07-07-friday-squid-blogging-giant-squid-nebula

[Source](https://www.schneier.com/blog/archives/2023/07/friday-squid-blogging-giant-squid-nebula.html){:target="_blank" rel="noopener"}

> Pretty : A mysterious squid-like cosmic cloud, this nebula is very faint, but also very large in planet Earth’s sky. In the image, composed with 30 hours of narrowband image data, it spans nearly three full moons toward the royal constellation Cepheus. Discovered in 2011 by French astro-imager Nicolas Outters, the Squid Nebula’s bipolar shape is distinguished here by the telltale blue-green emission from doubly ionized oxygen atoms. Though apparently surrounded by the reddish hydrogen emission region Sh2-129, the true distance and nature of the Squid Nebula have been difficult to determine. Still, a more recent investigation suggests Ou4 really [...]
