Title: Israel's largest oil refinery website offline after DDoS attack
Date: 2023-07-30T05:40:03-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-07-30-israels-largest-oil-refinery-website-offline-after-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/israels-largest-oil-refinery-website-offline-after-ddos-attack/){:target="_blank" rel="noopener"}

> Website of Israel's largest oil refinery operator, BAZAN Group is inaccessible from most parts of the world as threat actors claim to have hacked the Group's cyber systems. [...]
