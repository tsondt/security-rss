Title: Cloud CISO Perspectives: Early July 2023
Date: 2023-07-20T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2023-07-20-cloud-ciso-perspectives-early-july-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-early-july-2023/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for July 2023. Today, I’ll be asking my colleague Royal Hansen, vice president of Privacy, Safety, and Security Engineering at Google, about AI, security, and risk topics. This year has been a banner year for artificial intelligence (AI), and especially for AI and security. There’s been a huge surge of interest in AI and how it can be applied to many fields, including security. However, in the pursuit of progress within these new frontiers of innovation, there need to be clear industry security standards for building and deploying this technology in a responsible [...]
