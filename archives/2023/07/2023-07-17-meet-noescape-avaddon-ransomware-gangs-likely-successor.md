Title: Meet NoEscape: Avaddon ransomware gang's likely successor
Date: 2023-07-17T10:15:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-07-17-meet-noescape-avaddon-ransomware-gangs-likely-successor

[Source](https://www.bleepingcomputer.com/news/security/meet-noescape-avaddon-ransomware-gangs-likely-successor/){:target="_blank" rel="noopener"}

> The new NoEscape ransomware operation is believed to be a rebrand of Avaddon, a ransomware gang that shut down and released its decryption keys in 2021. [...]
