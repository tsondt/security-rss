Title: Razer investigates data breach claims, resets user sessions
Date: 2023-07-10T11:00:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2023-07-10-razer-investigates-data-breach-claims-resets-user-sessions

[Source](https://www.bleepingcomputer.com/news/security/razer-investigates-data-breach-claims-resets-user-sessions/){:target="_blank" rel="noopener"}

> Gaming gear company Razer reacted to recent rumors of a massive data breach with a short statement on Twitter, letting users know that they started an investigation into the matter. [...]
