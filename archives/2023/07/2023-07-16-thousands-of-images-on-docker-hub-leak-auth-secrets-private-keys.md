Title: Thousands of images on Docker Hub leak auth secrets, private keys
Date: 2023-07-16T10:09:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-16-thousands-of-images-on-docker-hub-leak-auth-secrets-private-keys

[Source](https://www.bleepingcomputer.com/news/security/thousands-of-images-on-docker-hub-leak-auth-secrets-private-keys/){:target="_blank" rel="noopener"}

> Researchers at the RWTH Aachen University in Germany published a study revealing that tens of thousands of container images hosted on Docker Hub contain confidential secrets, exposing software, online platforms, and users to a massive attack surface. [...]
