Title: Tech support scammers go analog, ask victims to mail bundles of cash
Date: 2023-07-19T21:00:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-07-19-tech-support-scammers-go-analog-ask-victims-to-mail-bundles-of-cash

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/19/tech_support_analog/){:target="_blank" rel="noopener"}

> The approach is the same, but never mind the crypto or gift cards Cybercriminals are taking their business offline in a new approach to familiar technical support scams recently identified by the US Federal Bureau of Investigation.... [...]
