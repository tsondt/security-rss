Title: SonicWall warns admins to patch critical auth bypass bugs immediately
Date: 2023-07-12T16:08:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-12-sonicwall-warns-admins-to-patch-critical-auth-bypass-bugs-immediately

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-warns-admins-to-patch-critical-auth-bypass-bugs-immediately/){:target="_blank" rel="noopener"}

> SonicWall warned customers today to urgently patch multiple critical vulnerabilities impacting the company's Global Management System (GMS) firewall management and Analytics network reporting engine software suites. [...]
