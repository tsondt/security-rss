Title: New StackRot Linux kernel flaw allows privilege escalation
Date: 2023-07-06T03:27:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-07-06-new-stackrot-linux-kernel-flaw-allows-privilege-escalation

[Source](https://www.bleepingcomputer.com/news/security/new-stackrot-linux-kernel-flaw-allows-privilege-escalation/){:target="_blank" rel="noopener"}

> A new privilege escalation vulnerability impacting Linux was discovered, enabling unprivileged local users to compromise the kernel and elevate their rights to attain root-level access. [...]
