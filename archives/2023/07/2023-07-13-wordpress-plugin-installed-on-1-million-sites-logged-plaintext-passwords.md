Title: WordPress plugin installed on 1 million+ sites logged plaintext passwords
Date: 2023-07-13T19:19:44+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;passwords;plugins;wordpress
Slug: 2023-07-13-wordpress-plugin-installed-on-1-million-sites-logged-plaintext-passwords

[Source](https://arstechnica.com/?p=1953744){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) All-In-One Security, a WordPress security plugin installed on more than 1 million websites, has issued a security update after being caught three weeks ago logging plaintext passwords and storing them in a database accessible to website admins. The passwords were logged when users of a site using the plugin, typically abbreviated as AIOS, logged in, the developer of AIOS said Thursday. The developer said the logging was the result of a bug introduced in May in version 5.1.9. Version 5.2.0 released Thursday fixes the bug and also “deletes the problematic data from the database.” The database [...]
