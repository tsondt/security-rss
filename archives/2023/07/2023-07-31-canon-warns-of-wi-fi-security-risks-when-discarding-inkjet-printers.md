Title: Canon warns of Wi-Fi security risks when discarding inkjet printers
Date: 2023-07-31T12:51:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-07-31-canon-warns-of-wi-fi-security-risks-when-discarding-inkjet-printers

[Source](https://www.bleepingcomputer.com/news/security/canon-warns-of-wi-fi-security-risks-when-discarding-inkjet-printers/){:target="_blank" rel="noopener"}

> Canon is warning users of home, office, and large format inkjet printers that their Wi-Fi connection settings stored in the devices' memories are not wiped, as they should, during initialization, allowing others to gain access to the data. [...]
