Title: Top Suspect in 2015 Ashley Madison Hack Committed Suicide in 2014
Date: 2023-07-07T19:55:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ashley Madison;Ashley Madison hack;Avid Life Media;Disney+;hulu;Jeremy Bullock;Noel Biderman;Wall to Wall Media;Warner Bros.;William Webster Harrison
Slug: 2023-07-07-top-suspect-in-2015-ashley-madison-hack-committed-suicide-in-2014

[Source](https://krebsonsecurity.com/2023/07/top-suspect-in-2015-ashley-madison-hack-committed-suicide-in-2014/){:target="_blank" rel="noopener"}

> When the marital infidelity website AshleyMadison.com learned in July 2015 that hackers were threatening to publish data stolen from 37 million users, the company’s then-CEO Noel Biderman was quick to point the finger at an unnamed former contractor. But as a new documentary series on Hulu reveals [ SPOILER ALERT! ], there was just one problem with that theory: Their top suspect had killed himself more than a year before the hackers began publishing stolen user data. The new documentary, The Ashley Madison Affair, begins airing today on Hulu in the United States and on Disney+ in the United Kingdom. [...]
