Title: Privacy of Printing Services
Date: 2023-07-11T11:57:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2023-07-11-privacy-of-printing-services

[Source](https://www.schneier.com/blog/archives/2023/07/privacy-of-printing-services.html){:target="_blank" rel="noopener"}

> The Washington Post has an article about popular printing services, and whether or not they read your documents and mine the data when you use them for printing: Ideally, printing services should avoid storing the content of your files, or at least delete daily. Print services should also communicate clearly upfront what information they’re collecting and why. Some services, like the New York Public Library and PrintWithMe, do both. Others dodged our questions about what data they collect, how long they store it and whom they share it with. Some—including Canon, FedEx and Staples—declined to answer basic questions about their [...]
