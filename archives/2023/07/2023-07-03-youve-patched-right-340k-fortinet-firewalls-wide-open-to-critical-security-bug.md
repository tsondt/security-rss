Title: You've patched right? '340K+ Fortinet firewalls' wide open to critical security bug
Date: 2023-07-03T23:17:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-03-youve-patched-right-340k-fortinet-firewalls-wide-open-to-critical-security-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/03/338000_fortinet_firewalls_vulnerability/){:target="_blank" rel="noopener"}

> That's a vulnerability that's under attack, fix available... cancel those July 4th plans, perhaps? More than 338,000 FortiGate firewalls are still unpatched and vulnerable to CVE-2023-27997, a critical bug Fortinet fixed last month that's being exploited in the wild.... [...]
