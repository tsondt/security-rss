Title: Florida man accused of hoarding America's secrets faces fresh charges
Date: 2023-07-29T00:59:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-07-29-florida-man-accused-of-hoarding-americas-secrets-faces-fresh-charges

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/29/florida_man_video_indictment/){:target="_blank" rel="noopener"}

> Mar-a-Lago IT director told 'the boss wanted the server deleted' Federal prosecutors have expanded their criminal case against a famous Floridian and his loyal minions for allegedly mishandling national security secrets and not being forthright about the storage and handling of hundreds of classified documents.... [...]
