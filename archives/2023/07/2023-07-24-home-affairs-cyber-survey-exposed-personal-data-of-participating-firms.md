Title: Home affairs cyber survey exposed personal data of participating firms
Date: 2023-07-24T15:00:15+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Data and computer security;Australia news;Technology;Optus;Cybercrime;Hacking;Australian politics;Business
Slug: 2023-07-24-home-affairs-cyber-survey-exposed-personal-data-of-participating-firms

[Source](https://www.theguardian.com/technology/2023/jul/24/home-affairs-cyber-survey-exposed-personal-data-of-participating-firms){:target="_blank" rel="noopener"}

> Shadow minister says leak of ‘sensitive’ information after research into the Optus and Medibank hacks was ‘deeply ironic’ Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast The home affairs department exposed the personal information of more than 50 small business survey participants who were sought for their views on cybersecurity, Guardian Australia can reveal. The names, business names, phone numbers and emails of the participants in the survey were published on the parliament website in response to a question on notice from May’s Budget estimates [...]
