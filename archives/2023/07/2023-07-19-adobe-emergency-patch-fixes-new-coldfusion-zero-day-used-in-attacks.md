Title: Adobe emergency patch fixes new ColdFusion zero-day used in attacks
Date: 2023-07-19T16:37:41-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-07-19-adobe-emergency-patch-fixes-new-coldfusion-zero-day-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/adobe-emergency-patch-fixes-new-coldfusion-zero-day-used-in-attacks/){:target="_blank" rel="noopener"}

> Adobe released an emergency ColdFusion security update that fixes critical vulnerabilities, including a fix for a new zero-day exploited in attacks. [...]
