Title: Suspected bank-infecting OPERA1ER crime boss cuffed
Date: 2023-07-05T19:40:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-05-suspected-bank-infecting-opera1er-crime-boss-cuffed

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/05/interpol_opera1er_arrest/){:target="_blank" rel="noopener"}

> Cops reckon gang swiped as much as $30M from financial orgs International cops have arrested a suspected "key figure" of a cybercrime group dubbed OPERA1ER that has stolen as much as $30 million from more than 30 banks and financial orgs across 15 countries.... [...]
