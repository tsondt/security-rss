Title: OpenAI credentials stolen by the thousands for sale on the dark web
Date: 2023-07-19T15:26:33-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-07-19-openai-credentials-stolen-by-the-thousands-for-sale-on-the-dark-web

[Source](https://www.bleepingcomputer.com/news/security/openai-credentials-stolen-by-the-thousands-for-sale-on-the-dark-web/){:target="_blank" rel="noopener"}

> Threat actors are showing an increased interest in generative artificial intelligence tools, with hundreds of thousands of OpenAI credentials for sale on the dark web and access to a malicious alternative for ChatGPT. [...]
