Title: Japan’s largest port stops operations after ransomware attack
Date: 2023-07-05T06:00:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-05-japans-largest-port-stops-operations-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/japans-largest-port-stops-operations-after-ransomware-attack/){:target="_blank" rel="noopener"}

> The Port of Nagoya, the largest and busiest port in Japan, has been targeted in a ransomware attack that currently impacts the operation of container terminals. [...]
