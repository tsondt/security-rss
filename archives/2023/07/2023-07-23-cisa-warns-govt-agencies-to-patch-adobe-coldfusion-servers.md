Title: CISA warns govt agencies to patch Adobe ColdFusion servers
Date: 2023-07-23T10:11:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-23-cisa-warns-govt-agencies-to-patch-adobe-coldfusion-servers

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-govt-agencies-to-patch-adobe-coldfusion-servers/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) has given federal agencies three weeks to secure Adobe ColdFusion servers on their networks against two critical security flaws exploited in attacks, one of them as a zero-day. [...]
