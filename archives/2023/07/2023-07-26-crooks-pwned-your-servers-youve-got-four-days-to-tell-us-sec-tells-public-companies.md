Title: Crooks pwned your servers? You've got four days to tell us, SEC tells public companies
Date: 2023-07-26T23:48:51+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-26-crooks-pwned-your-servers-youve-got-four-days-to-tell-us-sec-tells-public-companies

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/26/sec_reporting_security/){:target="_blank" rel="noopener"}

> Cripes, they actually sound serious Public companies that suffer a computer crime likely to cause a "material" hit to an investor will soon face a four-day time limit to disclose the incident, according to rules approved today by the US Securities and Exchange Commission.... [...]
