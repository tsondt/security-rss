Title: Google Analytics data transfer to U.S. brings $1 million fine to Swedish firms
Date: 2023-07-04T11:19:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Legal
Slug: 2023-07-04-google-analytics-data-transfer-to-us-brings-1-million-fine-to-swedish-firms

[Source](https://www.bleepingcomputer.com/news/security/google-analytics-data-transfer-to-us-brings-1-million-fine-to-swedish-firms/){:target="_blank" rel="noopener"}

> The Swedish Authority for Privacy Protection (Integritetsskyddsmyndigheten - IMY) has fined two companies with 12.3 million SEK (€1 million/$1.1 million) for using Google Analytics and warned two others about the same practice. [...]
