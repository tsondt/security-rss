Title: Cybersecurity firm Sophos impersonated by new SophosEncrypt ransomware
Date: 2023-07-18T16:47:21-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-07-18-cybersecurity-firm-sophos-impersonated-by-new-sophosencrypt-ransomware

[Source](https://www.bleepingcomputer.com/news/security/cybersecurity-firm-sophos-impersonated-by-new-sophosencrypt-ransomware/){:target="_blank" rel="noopener"}

> Cybersecurity vendor Sophos is being impersonated by a new ransomware-as-a-service called SophosEncrypt, with the threat actors using the company name for their operation. [...]
