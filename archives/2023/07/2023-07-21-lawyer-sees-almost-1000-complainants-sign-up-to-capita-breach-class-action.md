Title: Lawyer sees almost 1,000 complainants sign up to Capita breach class action
Date: 2023-07-21T10:38:14+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-07-21-lawyer-sees-almost-1000-complainants-sign-up-to-capita-breach-class-action

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/21/capita_breach_class_action/){:target="_blank" rel="noopener"}

> 95% pertain to pension schemes administered by outsourcing giant, says Barings Law The law firm that last month sent a Letter of Claim to Capita over a security breach in late March says it has signed up nearly 1,000 clients as it prepares a class action lawsuit aimed at the outsourcing giant.... [...]
