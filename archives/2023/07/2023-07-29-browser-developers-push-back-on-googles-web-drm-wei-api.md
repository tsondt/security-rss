Title: Browser developers push back on Google's “web DRM” WEI API
Date: 2023-07-29T10:11:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Google;Security;Software;Technology
Slug: 2023-07-29-browser-developers-push-back-on-googles-web-drm-wei-api

[Source](https://www.bleepingcomputer.com/news/google/browser-developers-push-back-on-googles-web-drm-wei-api/){:target="_blank" rel="noopener"}

> Google's plans to introduce the Web Environment Integrity (WEI) API on Chrome has been met with fierce backlash from internet software developers, drawing criticism for limiting user freedom and undermining the core principles of the open web. [...]
