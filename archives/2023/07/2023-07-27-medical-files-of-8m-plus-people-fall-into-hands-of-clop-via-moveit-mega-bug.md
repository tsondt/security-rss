Title: Medical files of 8M-plus people fall into hands of Clop via MOVEit mega-bug
Date: 2023-07-27T20:01:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-27-medical-files-of-8m-plus-people-fall-into-hands-of-clop-via-moveit-mega-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/27/maximus_deloitte_moveit_hack/){:target="_blank" rel="noopener"}

> Maximus plus Deloitte and Chuck E. Cheese join 500+ victim orgs Accounting giant Deloitte, pizza and birthday party chain Chuck E. Cheese, government contractor Maximus, and the Hallmark Channel are among the latest victims that the Russian ransomware crew Clop claims to have compromised via the MOVEit vulnerability.... [...]
