Title: Frontline Security Practitioners Reveal the Latest About AI
Date: 2023-07-17T10:02:04-04:00
Author: Sponsored by Mandiant
Category: BleepingComputer
Tags: Security
Slug: 2023-07-17-frontline-security-practitioners-reveal-the-latest-about-ai

[Source](https://www.bleepingcomputer.com/news/security/frontline-security-practitioners-reveal-the-latest-about-ai/){:target="_blank" rel="noopener"}

> Organizers at mWISE, the anticipated cybersecurity conference from Mandiant, now part of Google Cloud, have released this year's session catalog. Learn more from Mandiant about the upcoming mWise sessions. [...]
