Title: Russian state hackers lure Western diplomats with BMW car ads
Date: 2023-07-12T15:01:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-12-russian-state-hackers-lure-western-diplomats-with-bmw-car-ads

[Source](https://www.bleepingcomputer.com/news/security/russian-state-hackers-lure-western-diplomats-with-bmw-car-ads/){:target="_blank" rel="noopener"}

> The Russian state-sponsored hacking group 'APT29' (aka Nobelium, Cloaked Ursa) has been using unconventional lures like car listings to entice diplomats in Ukraine to click on malicious links that deliver malware. [...]
