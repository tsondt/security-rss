Title: Introducing Cloud Armor WAF enhancements to help protect your web application and API service
Date: 2023-07-27T16:00:00+00:00
Author: Shane Wang
Category: GCP Security
Tags: Networking;Security & Identity
Slug: 2023-07-27-introducing-cloud-armor-waf-enhancements-to-help-protect-your-web-application-and-api-service

[Source](https://cloud.google.com/blog/products/identity-security/introducing-cloud-armor-waf-enhancements/){:target="_blank" rel="noopener"}

> Organizations migrating web applications and their APIs to the cloud need solutions to protect them from exploits and distributed denial of service (DDoS) attacks. Google Cloud Armor is a network security service that provides defenses against DDoS and OWASP Top 10 risks. We are excited to introduce several new features in Cloud Armor: granular rate limiting with the ability to combine multiple keys; and more flexibility in creating IP-based custom rules based on the origins of requests to further enhance DDoS and web application protection. Enforcing rate limiting with one or a combination of unique key identifiers We announced Cloud [...]
