Title: JumpCloud hack linked to North Korea after OPSEC mistake
Date: 2023-07-24T13:12:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-24-jumpcloud-hack-linked-to-north-korea-after-opsec-mistake

[Source](https://www.bleepingcomputer.com/news/security/jumpcloud-hack-linked-to-north-korea-after-opsec-mistake/){:target="_blank" rel="noopener"}

> A hacking unit of North Korea's Reconnaissance General Bureau (RGB) was linked to the JumpCloud breach after the attackers made an operational security (OPSEC) mistake, inadvertently exposing their real-world IP addresses. [...]
