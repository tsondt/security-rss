Title: NATO investigates alleged data theft by SiegedSec hackers
Date: 2023-07-26T12:26:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-26-nato-investigates-alleged-data-theft-by-siegedsec-hackers

[Source](https://www.bleepingcomputer.com/news/security/nato-investigates-alleged-data-theft-by-siegedsec-hackers/){:target="_blank" rel="noopener"}

> NATO has confirmed that its IT team is investigating claims about an alleged data-theft hack on the Communities of Interest (COI) Cooperation Portal by a hacking group known as SiegedSec. [...]
