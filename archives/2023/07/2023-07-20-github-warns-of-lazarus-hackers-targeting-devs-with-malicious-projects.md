Title: GitHub warns of Lazarus hackers targeting devs with malicious projects
Date: 2023-07-20T18:48:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-07-20-github-warns-of-lazarus-hackers-targeting-devs-with-malicious-projects

[Source](https://www.bleepingcomputer.com/news/security/github-warns-of-lazarus-hackers-targeting-devs-with-malicious-projects/){:target="_blank" rel="noopener"}

> GitHub is warning of a social engineering campaign targeting the accounts of developers in the blockchain, cryptocurrency, online gambling, and cybersecurity sectors to infect their devices with malware. [...]
