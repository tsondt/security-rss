Title: Twitter's bot spam keeps getting worse — it's about porn this time
Date: 2023-07-02T11:05:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-07-02-twitters-bot-spam-keeps-getting-worse-its-about-porn-this-time

[Source](https://www.bleepingcomputer.com/news/security/twitters-bot-spam-keeps-getting-worse-its-about-porn-this-time/){:target="_blank" rel="noopener"}

> Forget crypto spam accounts, Twitter's got another problem which involves bots and accounts promoting adult content and infiltrating Direct Messages and interactions on the platform. And there doesn't seem to be an easy solution in sight. [...]
