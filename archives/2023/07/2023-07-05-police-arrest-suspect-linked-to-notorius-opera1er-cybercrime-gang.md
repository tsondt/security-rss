Title: Police arrest suspect linked to notorius OPERA1ER cybercrime gang
Date: 2023-07-05T10:16:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-05-police-arrest-suspect-linked-to-notorius-opera1er-cybercrime-gang

[Source](https://www.bleepingcomputer.com/news/security/police-arrest-suspect-linked-to-notorius-opera1er-cybercrime-gang/){:target="_blank" rel="noopener"}

> Law enforcement has detained a suspect believed to be a key member of the OPERA1ER cybercrime group, which has targeted mobile banking services and financial institutions in malware, phishing, and Business Email Compromise (BEC) campaigns. [...]
