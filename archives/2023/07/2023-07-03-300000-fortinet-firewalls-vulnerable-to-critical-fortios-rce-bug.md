Title: 300,000+ Fortinet firewalls vulnerable to critical FortiOS RCE bug
Date: 2023-07-03T07:54:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-03-300000-fortinet-firewalls-vulnerable-to-critical-fortios-rce-bug

[Source](https://www.bleepingcomputer.com/news/security/300-000-plus-fortinet-firewalls-vulnerable-to-critical-fortios-rce-bug/){:target="_blank" rel="noopener"}

> Hundreds of thousands of FortiGate firewalls are vulnerable to a critical security issue identified as CVE-2023-27997, almost a month after Fortinet released an update that addresses the problem. [...]
