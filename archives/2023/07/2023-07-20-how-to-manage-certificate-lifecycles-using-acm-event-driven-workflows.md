Title: How to manage certificate lifecycles using ACM event-driven workflows
Date: 2023-07-20T13:44:34+00:00
Author: Shahna Campbell
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;ACM;Amazon EventBridge;AWS Certificate Manager;Security Blog
Slug: 2023-07-20-how-to-manage-certificate-lifecycles-using-acm-event-driven-workflows

[Source](https://aws.amazon.com/blogs/security/how-to-manage-certificate-lifecycles-using-acm-event-driven-workflows/){:target="_blank" rel="noopener"}

> With AWS Certificate Manager (ACM), you can simplify certificate lifecycle management by using event-driven workflows to notify or take action on expiring TLS certificates in your organization. Using ACM, you can provision, manage, and deploy public and private TLS certificates for use with integrated AWS services like Amazon CloudFront and Elastic Load Balancing (ELB), as well [...]
