Title: US senator victim-blames Microsoft for Chinese hack
Date: 2023-07-31T00:59:42+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-07-31-us-senator-victim-blames-microsoft-for-chinese-hack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/31/infosec_in_brief/){:target="_blank" rel="noopener"}

> ALSO: China says US hacked it right back, BreachForums users have been pwned, and this week's critical vulns Infosec in brief US senator Ron Wyden (D-OR) thinks it's Microsoft's fault that Chinese hackers broke into Exchange Online, and he wants three separate government agencies to launch investigations and hold the Windows giant "responsible for its negligent cyber security practices."... [...]
