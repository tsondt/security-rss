Title: Think tank calls for monitoring of Chinese AI-enabled products
Date: 2023-07-27T18:54:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-07-27-think-tank-calls-for-monitoring-of-chinese-ai-enabled-products

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/27/think_tanks_calls_for_monitoring/){:target="_blank" rel="noopener"}

> Will make regulating China’s 5G telecom equipment look like a cinch Chinese made AI-enabled products should spark similar concerns to Middle Kingdom sourced 5G equipment and therefore be regulated, said think tank Australian Strategic Policy Institute (ASPI) on Thursday.... [...]
