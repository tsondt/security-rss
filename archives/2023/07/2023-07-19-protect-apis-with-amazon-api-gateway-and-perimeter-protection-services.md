Title: Protect APIs with Amazon API Gateway and perimeter protection services
Date: 2023-07-19T18:32:07+00:00
Author: Pengfei Shao
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Amazon API Gateway;Amazon CloudFront;AWS WAF;Lambda@Edge;Network and content delivery;Security;Security Blog
Slug: 2023-07-19-protect-apis-with-amazon-api-gateway-and-perimeter-protection-services

[Source](https://aws.amazon.com/blogs/security/protect-apis-with-amazon-api-gateway-and-perimeter-protection-services/){:target="_blank" rel="noopener"}

> As Amazon Web Services (AWS) customers build new applications, APIs have been key to driving the adoption of these offerings. APIs simplify client integration and provide for efficient operations and management of applications by offering standard contracts for data exchange. APIs are also the front door to hosted applications that need to be effectively secured, [...]
