Title: Former employee charged for attacking water treatment plant
Date: 2023-07-10T12:57:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-10-former-employee-charged-for-attacking-water-treatment-plant

[Source](https://www.bleepingcomputer.com/news/security/former-employee-charged-for-attacking-water-treatment-plant/){:target="_blank" rel="noopener"}

> A former employee of Discovery Bay Water Treatment Facility in California was indicted by a federal grand jury for intentionally attempting to cause malfunction to the facility's safety and protection systems. [...]
