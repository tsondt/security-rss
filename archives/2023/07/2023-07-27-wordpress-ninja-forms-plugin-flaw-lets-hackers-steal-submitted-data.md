Title: WordPress Ninja Forms plugin flaw lets hackers steal submitted data
Date: 2023-07-27T13:00:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-27-wordpress-ninja-forms-plugin-flaw-lets-hackers-steal-submitted-data

[Source](https://www.bleepingcomputer.com/news/security/wordpress-ninja-forms-plugin-flaw-lets-hackers-steal-submitted-data/){:target="_blank" rel="noopener"}

> Popular WordPress form-building plugin Ninja Forms contains three vulnerabilities that could allow attackers to achieve privilege escalation and steal user data. [...]
