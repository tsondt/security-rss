Title: Microsoft takes pains to obscure role in 0-days that caused email breach
Date: 2023-07-14T22:19:55+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;azure;breach;Exchange;microsoft;zerday
Slug: 2023-07-14-microsoft-takes-pains-to-obscure-role-in-0-days-that-caused-email-breach

[Source](https://arstechnica.com/?p=1954171){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images | Aurich Lawson) On Friday, Microsoft attempted to explain the cause of a breach that gave hackers working for the Chinese government access to the email accounts of 25 organizations—reportedly including the US Departments of State and Commerce and other sensitive organizations. In a post on Friday, the company indicated that the compromise resulted from three exploited vulnerabilities in either its Exchange Online email service or Azure Active Directory, an identity service that manages single sign-on and multifactor authentication for large organizations. Microsoft’s Threat Intelligence team said that Storm-0558, a China-based hacking outfit that conducts espionage [...]
