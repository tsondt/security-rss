Title: Friday Squid Blogging: Zaqistan Flag
Date: 2023-07-28T21:01:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-07-28-friday-squid-blogging-zaqistan-flag

[Source](https://www.schneier.com/blog/archives/2023/07/friday-squid-blogging-zaqistan-flag.html){:target="_blank" rel="noopener"}

> The fictional nation of Zaqistan (in Utah) has a squid on its flag. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
