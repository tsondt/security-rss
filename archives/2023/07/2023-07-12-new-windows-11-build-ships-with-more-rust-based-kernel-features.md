Title: New Windows 11 build ships with more Rust-based Kernel features
Date: 2023-07-12T14:37:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-07-12-new-windows-11-build-ships-with-more-rust-based-kernel-features

[Source](https://www.bleepingcomputer.com/news/microsoft/new-windows-11-build-ships-with-more-rust-based-kernel-features/){:target="_blank" rel="noopener"}

> Microsoft announced that the latest Windows 11 build shipping to Insiders in the Canary channel comes with additional Windows Kernel components rewritten in the memory safety-focused Rust programming language. [...]
