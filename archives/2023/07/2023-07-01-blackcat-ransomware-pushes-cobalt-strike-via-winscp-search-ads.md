Title: BlackCat ransomware pushes Cobalt Strike via WinSCP search ads
Date: 2023-07-01T11:18:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-01-blackcat-ransomware-pushes-cobalt-strike-via-winscp-search-ads

[Source](https://www.bleepingcomputer.com/news/security/blackcat-ransomware-pushes-cobalt-strike-via-winscp-search-ads/){:target="_blank" rel="noopener"}

> The BlackCat ransomware group (aka ALPHV) is running malvertizing campaigns to lure people into fake pages that mimic the official website of the WinSCP file-transfer application for Windows but instead push malware-ridden installers. [...]
