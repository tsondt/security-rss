Title: Typo watch: 'Millions of emails' for US military sent to .ml addresses in error
Date: 2023-07-18T00:40:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-07-18-typo-watch-millions-of-emails-for-us-military-sent-to-ml-addresses-in-error

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/18/us_military_mali_email_typos/){:target="_blank" rel="noopener"}

> Good thing Mali isn't best pals with Russia right no– oh, shoot For the past decade, millions of emails destined for.mil US military addresses were actually directed at.ml addresses, that being the top-level domain for the African nation of Mali, it's claimed.... [...]
