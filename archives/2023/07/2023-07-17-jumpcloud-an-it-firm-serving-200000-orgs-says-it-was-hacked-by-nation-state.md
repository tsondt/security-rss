Title: JumpCloud, an IT firm serving 200,000 orgs, says it was hacked by nation-state
Date: 2023-07-17T20:06:00+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;jumpcloud;nation state;security breach
Slug: 2023-07-17-jumpcloud-an-it-firm-serving-200000-orgs-says-it-was-hacked-by-nation-state

[Source](https://arstechnica.com/?p=1954537){:target="_blank" rel="noopener"}

> Enlarge JumpCloud, a cloud-based IT management service that lists Cars.com, GoFundMe, and Foursquare among its 5,000 paying customers, experienced a security breach carried out by hackers working for a nation-state, the company said last week. The attack began on June 22 as a spear-phishing campaign, the company revealed last Wednesday. As part of that incident, JumpCloud said, the “sophisticated nation-state sponsored threat actor” gained access to an unspecified part of the JumpCloud internal network. Although investigators at the time found no evidence any customers were affected, the company said it rotated account credentials, rebuilt its systems, and took other defensive [...]
