Title: OSPAR 2023 report now available with 153 services in scope
Date: 2023-07-20T17:51:09+00:00
Author: Joseph Goh
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance reports;OSPAR;Security Blog;Singapore
Slug: 2023-07-20-ospar-2023-report-now-available-with-153-services-in-scope

[Source](https://aws.amazon.com/blogs/security/ospar-2023-report-now-available-with-153-services-in-scope/){:target="_blank" rel="noopener"}

> We’re pleased to announce the completion of our annual Outsourced Service Provider’s Audit Report (OSPAR) audit cycle on July 1, 2023. The 2023 OSPAR certification cycle includes the addition of nine new services in scope, bringing the total number of services in scope to 153 in the AWS Asia Pacific (Singapore) Region. Newly added services [...]
