Title: New York Using AI to Detect Subway Fare Evasion
Date: 2023-07-25T11:05:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cheating;law enforcement;public transit
Slug: 2023-07-25-new-york-using-ai-to-detect-subway-fare-evasion

[Source](https://www.schneier.com/blog/archives/2023/07/new-york-using-ai-to-detect-subway-fare-evasion.html){:target="_blank" rel="noopener"}

> The details are scant—the article is based on a “heavily redacted” contract—but the New York subway authority is using an “AI system” to detect people who don’t pay the subway fare. Joana Flores, an MTA spokesperson, said the AI system doesn’t flag fare evaders to New York police, but she declined to comment on whether that policy could change. A police spokesperson declined to comment. If we spent just one-tenth of the effort we spend prosecuting the poor on prosecuting the rich, it would be a very different world. [...]
