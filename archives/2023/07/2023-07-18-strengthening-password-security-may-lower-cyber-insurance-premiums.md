Title: Strengthening Password Security may Lower Cyber Insurance Premiums
Date: 2023-07-18T10:01:02-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-07-18-strengthening-password-security-may-lower-cyber-insurance-premiums

[Source](https://www.bleepingcomputer.com/news/security/strengthening-password-security-may-lower-cyber-insurance-premiums/){:target="_blank" rel="noopener"}

> When insurers assess an organization's cybersecurity posture, password security is a key element considered. Learn more from Specops Software on how password security can affect your insurance premiums. [...]
