Title: Infosec watchers: TeamTNT crew may blast holes in Azure, Google Cloud users
Date: 2023-07-15T08:28:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-07-15-infosec-watchers-teamtnt-crew-may-blast-holes-in-azure-google-cloud-users

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/15/teamtnt_aws_azure_google/){:target="_blank" rel="noopener"}

> Why limit yourself to only stealing AWS credentials? A criminal crew with a history of deploying malware to harvest credentials from Amazon Web Services accounts may expand its attention to organizations using Microsoft Azure and Google Cloud Platform.... [...]
