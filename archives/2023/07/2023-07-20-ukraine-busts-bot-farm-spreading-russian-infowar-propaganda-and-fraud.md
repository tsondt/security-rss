Title: Ukraine busts bot farm spreading Russian infowar propaganda and fraud
Date: 2023-07-20T07:30:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-20-ukraine-busts-bot-farm-spreading-russian-infowar-propaganda-and-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/20/ukraine_busts_russian_bot_farm/){:target="_blank" rel="noopener"}

> Plus: Spanish cops arrest Ukrainian scareware dev after ten-year hunt Ukrainian cops have disrupted a massive bot farm with more than 100 operators allegedly spreading fake news about the Russian invasion, leaking personal information belonging to Ukrainian citizens, and instigating fraud schemes.... [...]
