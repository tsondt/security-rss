Title: VirusTotal: We're sorry someone fat-fingered and exposed 5,600 users
Date: 2023-07-21T20:58:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-21-virustotal-were-sorry-someone-fat-fingered-and-exposed-5600-users

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/21/virustotal_data_exposure_apology/){:target="_blank" rel="noopener"}

> File under PEBCAK VirusTotal today issued a mea culpa, saying a blunder earlier this week by one of its staff exposed information belonging to 5,600 customers, including the email addresses of US Cyber Command, FBI, and NSA employees.... [...]
