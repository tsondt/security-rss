Title: RAM-ramming Rowhammer is back – to uniquely fingerprint devices
Date: 2023-07-05T21:14:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-07-05-ram-ramming-rowhammer-is-back-to-uniquely-fingerprint-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/05/rowhammer_memory_identification/){:target="_blank" rel="noopener"}

> Just use it sparingly, as it may crash equipment or burn out memory Boffins at the University of California, Davis have devised a purportedly practical way to apply a memory abuse technique called Rowhammer to build unique, stable device fingerprints.... [...]
