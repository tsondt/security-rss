Title: Microsoft denies data breach, theft of 30 million customer accounts
Date: 2023-07-03T14:38:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-07-03-microsoft-denies-data-breach-theft-of-30-million-customer-accounts

[Source](https://www.bleepingcomputer.com/news/security/microsoft-denies-data-breach-theft-of-30-million-customer-accounts/){:target="_blank" rel="noopener"}

> Microsoft has denied the claims of the so-called hacktivists "Anonymous Sudan" that they breached the company's servers and stole credentials for 30 million customer accounts. [...]
