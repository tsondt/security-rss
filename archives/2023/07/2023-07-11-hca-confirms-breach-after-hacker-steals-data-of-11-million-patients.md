Title: HCA confirms breach after hacker steals data of 11 million patients
Date: 2023-07-11T10:59:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-07-11-hca-confirms-breach-after-hacker-steals-data-of-11-million-patients

[Source](https://www.bleepingcomputer.com/news/security/hca-confirms-breach-after-hacker-steals-data-of-11-million-patients/){:target="_blank" rel="noopener"}

> HCA Healthcare disclosed a data breach impacting an estimated 11 million patients who received care at one of its hospitals and clinics after a threat actor posted samples of stolen data on a hacking forum. [...]
