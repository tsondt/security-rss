Title: JumpCloud breach traced back to North Korean state hackers
Date: 2023-07-20T08:25:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-20-jumpcloud-breach-traced-back-to-north-korean-state-hackers

[Source](https://www.bleepingcomputer.com/news/security/jumpcloud-breach-traced-back-to-north-korean-state-hackers/){:target="_blank" rel="noopener"}

> US-based enterprise software company JumpCloud was breached by North Korean Lazarus Group hackers, according to security researchers at SentinelOne, CrowdStrike, and Mandiant. [...]
