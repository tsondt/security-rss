Title: Spring 2023 PCI DSS and 3DS compliance packages available now
Date: 2023-07-13T13:47:08+00:00
Author: Nivetha Chandran
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;Compliance reports;PCI;PCI DSS;Security Blog
Slug: 2023-07-13-spring-2023-pci-dss-and-3ds-compliance-packages-available-now

[Source](https://aws.amazon.com/blogs/security/spring-2023-pci-dss-and-3ds-compliance-packages-available-now/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce that seven additional AWS services have been added to the scope of our Payment Card Industry Data Security Standard (PCI DSS) and Payment Card Industry Three-Domain Secure (PCI 3DS) certifications. The compliance package for PCI DSS and 3DS includes the Attestation of Compliance (AOC), which shows that [...]
