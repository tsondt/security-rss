Title: Ransomware Affiliates, Triple Extortion, and the Dark Web Ecosystem
Date: 2023-07-06T10:00:00-04:00
Author: Sponsored by Flare
Category: BleepingComputer
Tags: Security
Slug: 2023-07-06-ransomware-affiliates-triple-extortion-and-the-dark-web-ecosystem

[Source](https://www.bleepingcomputer.com/news/security/ransomware-affiliates-triple-extortion-and-the-dark-web-ecosystem/){:target="_blank" rel="noopener"}

> In recent years a complex cybercrime ecosystem has emerged across Tor and illicit channels on Telegram. In this article, Flare explains how ransomware gangs and initial access brokers utilize this ecosystem. [...]
