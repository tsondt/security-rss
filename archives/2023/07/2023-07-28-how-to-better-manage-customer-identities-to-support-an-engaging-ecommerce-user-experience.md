Title: How to better manage customer identities to support an engaging ecommerce user experience
Date: 2023-07-28T16:00:00+00:00
Author: Rohit Mishra
Category: GCP Security
Tags: Retail;Security & Identity
Slug: 2023-07-28-how-to-better-manage-customer-identities-to-support-an-engaging-ecommerce-user-experience

[Source](https://cloud.google.com/blog/products/identity-security/how-to-better-manage-customer-ids-to-support-user-experience/){:target="_blank" rel="noopener"}

> One of the fundamental questions that retailers constantly ask themselves is how well they know their customers and how they can build deeper relationships. Knowing who their customers are can have a direct impact on improving customer engagement, which can lead to increased conversion rates, customer loyalty, lifetime value, and higher return on marketing spend. Retailers have started to use innovative new methods like identity graphs, which connect information from multiple sources such as internal databases, marketing systems, online interactions and social media platforms. While these new methods have shown promise, the best and most effective approach remains customer self-identification [...]
