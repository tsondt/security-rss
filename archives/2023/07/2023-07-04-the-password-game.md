Title: The Password Game
Date: 2023-07-04T11:12:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;games;humor;passwords
Slug: 2023-07-04-the-password-game

[Source](https://www.schneier.com/blog/archives/2023/07/the-password-game.html){:target="_blank" rel="noopener"}

> Amusing parody of password rules. BoingBoing : For example, at a certain level, your password must include today’s Wordle answer. And then there’s rule #27: “At least 50% of your password must be in the Wingdings font.” [...]
