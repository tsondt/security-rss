Title: Introducing predictable cost options for Cloud Data Loss Prevention
Date: 2023-07-31T16:00:00+00:00
Author: Scott Ellis
Category: GCP Security
Tags: Security & Identity
Slug: 2023-07-31-introducing-predictable-cost-options-for-cloud-data-loss-prevention

[Source](https://cloud.google.com/blog/products/identity-security/introducing-predictable-cost-options-for-cloud-data-loss-prevention/){:target="_blank" rel="noopener"}

> Cloud Data Loss Prevention's Discovery service can help you discover, profile, and monitor sensitive data across your entire organization or just select projects. We've expanded the service to cover BigQuery and BigLake, with more cloud analytics services that manage sensitive data coming later this year. Continuous visibility of where your sensitive data is stored and processed can help inform your organization’s data security, privacy, and governance operations. It can also be leveraged in other security products like Security Command Center and Chronicle to help you prioritize cloud security operations according to risk. As organizations roll out discovery across their increasingly [...]
