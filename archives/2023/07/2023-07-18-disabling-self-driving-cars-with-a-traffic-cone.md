Title: Disabling Self-Driving Cars with a Traffic Cone
Date: 2023-07-18T11:13:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cars;hacking
Slug: 2023-07-18-disabling-self-driving-cars-with-a-traffic-cone

[Source](https://www.schneier.com/blog/archives/2023/07/disabling-self-driving-cars-with-a-traffic-cone.html){:target="_blank" rel="noopener"}

> You can disable a self-driving car by putting a traffic cone on its hood: The group got the idea for the conings by chance. The person claims a few of them walking together one night saw a cone on the hood of an AV, which appeared disabled. They weren’t sure at the time which came first; perhaps someone had placed the cone on the AV’s hood to signify it was disabled rather than the other way around. But, it gave them an idea, and when they tested it, they found that a cone on a hood renders the vehicles little [...]
