Title: US senator blasts Microsoft for “negligent cybersecurity practices”
Date: 2023-07-27T20:29:15+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;azure;Exchange;hacking;microsoft
Slug: 2023-07-27-us-senator-blasts-microsoft-for-negligent-cybersecurity-practices

[Source](https://arstechnica.com/?p=1957158){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A US senator is calling on the Justice Department to hold Microsoft responsible for “negligent cybersecurity practices” that enabled Chinese espionage hackers to steal hundreds of thousands of emails from cloud customers, including officials in the US Departments of State and Commerce. “Holding Microsoft responsible for its negligence will require a whole-of-government effort,” Ron Wyden (D-Ore.) wrote in a letter. It was sent on Thursday to the heads of the Justice Department, Cybersecurity and Infrastructure Security Agency, and the Federal Trade Commission. Bending over backward Wyden’s remarks echo those of other critics who say Microsoft is [...]
