Title: Windows 10 KB5028244 update released with 19 fixes, improved security
Date: 2023-07-26T13:04:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-07-26-windows-10-kb5028244-update-released-with-19-fixes-improved-security

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-10-kb5028244-update-released-with-19-fixes-improved-security/){:target="_blank" rel="noopener"}

> Microsoft has released the optional KB5028244 Preview cumulative update for Windows 10 22H2 with 19 fixes or changes, including an update to the Vulnerable Driver Blocklist to block BYOVD attacks. [...]
