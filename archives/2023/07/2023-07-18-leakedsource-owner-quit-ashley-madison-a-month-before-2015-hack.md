Title: LeakedSource Owner Quit Ashley Madison a Month Before 2015 Hack
Date: 2023-07-18T14:57:04+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ne'er-Do-Well News;abusewithus;AgentJags;Ashley Madison hack;Ashleymadison.com;Constella Intelligence;domaintools;Eric Malek;hulu;Impact Team;Jordan Evan Bloom;LeakedSource;Near-reality.com;Noel Biderman;ownagegaming1@gmail.com;PicTrace;Royal Canadian Mounted Police;Runescape;Trevor Sykes
Slug: 2023-07-18-leakedsource-owner-quit-ashley-madison-a-month-before-2015-hack

[Source](https://krebsonsecurity.com/2023/07/leakedsource-owner-quit-ashley-madison-a-month-before-2015-hack/){:target="_blank" rel="noopener"}

> [This is Part III in a series on research conducted for a recent Hulu documentary on the 2015 hack of marital infidelity website AshleyMadison.com.] In 2019, a Canadian company called Defiant Tech Inc. pleaded guilty to running LeakedSource[.]com, a service that sold access to billions of passwords and other data exposed in countless data breaches. KrebsOnSecurity has learned that the owner of Defiant Tech, a 32-year-old Ontario man named Jordan Evan Bloom, was hired in late 2014 as a developer for the marital infidelity site AshleyMadison.com. Bloom resigned from AshleyMadison citing health reasons in June 2015 — less than one [...]
