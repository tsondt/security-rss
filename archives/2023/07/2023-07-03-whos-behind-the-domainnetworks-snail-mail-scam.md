Title: Who’s Behind the DomainNetworks Snail Mail Scam?
Date: 2023-07-03T14:56:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Better Business Bureau;Constella Intelligence;distributorinvoice@mail.com;Domainnetworks;domaintools;Eliran Benz;Houzz;publicwww;Sam Alon;shenhavgroup@gmail.com;Shmuel Orit Alon;tropicglobal@gmail.com;ubsagency@gmail.com;United Business Service;United Business Services;US Domain Authority LLC;weblistingsinc.net;whmcs.com
Slug: 2023-07-03-whos-behind-the-domainnetworks-snail-mail-scam

[Source](https://krebsonsecurity.com/2023/07/whos-behind-the-domainnetworks-snail-mail-scam/){:target="_blank" rel="noopener"}

> If you’ve ever owned a domain name, the chances are good that at some point you’ve received a snail mail letter which appears to be a bill for a domain or website-related services. In reality, these misleading missives try to trick people into paying for useless services they never ordered, don’t need, and probably will never receive. Here’s a look at the most recent incarnation of this scam — DomainNetworks — and some clues about who may be behind it. The DomainNetworks mailer may reference a domain that is or was at one point registered to your name and address. [...]
