Title: Researchers find deliberate backdoor in police radio encryption algorithm
Date: 2023-07-25T13:05:32+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security;encryption;police radios;syndication;TETRA
Slug: 2023-07-25-researchers-find-deliberate-backdoor-in-police-radio-encryption-algorithm

[Source](https://arstechnica.com/?p=1956349){:target="_blank" rel="noopener"}

> Enlarge (credit: Evgen_Prozhyrko via Getty ) For more than 25 years, a technology used for critical data and voice radio communications around the world has been shrouded in secrecy to prevent anyone from closely scrutinizing its security properties for vulnerabilities. But now it’s finally getting a public airing thanks to a small group of researchers in the Netherlands who got their hands on its viscera and found serious flaws, including a deliberate backdoor. The backdoor, known for years by vendors that sold the technology but not necessarily by customers, exists in an encryption algorithm baked into radios sold for commercial [...]
