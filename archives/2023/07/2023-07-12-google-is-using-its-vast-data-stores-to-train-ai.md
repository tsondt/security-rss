Title: Google Is Using Its Vast Data Stores to Train AI
Date: 2023-07-12T14:50:05+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;data collection;Google;privacy
Slug: 2023-07-12-google-is-using-its-vast-data-stores-to-train-ai

[Source](https://www.schneier.com/blog/archives/2023/07/google-is-using-its-vast-data-stores-to-train-ai.html){:target="_blank" rel="noopener"}

> No surprise, but Google just changed its privacy policy to reflect broader uses of all the surveillance data it has captured over the years: Research and development : Google uses information to improve our services and to develop new products, features and technologies that benefit our users and the public. For example, we use publicly available information to help train Google’s AI models and build products and features like Google Translate, Bard, and Cloud AI capabilities. (I quote the privacy policy as of today. The Mastodon link quotes the privacy policy from ten days ago. So things are changing fast.) [...]
