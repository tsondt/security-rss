Title: Microsoft still unsure how hackers stole Azure AD signing key
Date: 2023-07-14T16:18:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-07-14-microsoft-still-unsure-how-hackers-stole-azure-ad-signing-key

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-still-unsure-how-hackers-stole-azure-ad-signing-key/){:target="_blank" rel="noopener"}

> Microsoft says it still doesn't know how Chinese hackers stole an inactive Microsoft account (MSA) consumer signing key used to breach the Exchange Online and Azure AD accounts of two dozen organizations, including government agencies. [...]
