Title: Apple re-releases zero-day patch after fixing browsing issue
Date: 2023-07-12T17:27:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-07-12-apple-re-releases-zero-day-patch-after-fixing-browsing-issue

[Source](https://www.bleepingcomputer.com/news/apple/apple-re-releases-zero-day-patch-after-fixing-browsing-issue/){:target="_blank" rel="noopener"}

> Apple fixed and re-released emergency security updates addressing a WebKit zero-day vulnerability exploited in attacks. The initial patches had to be withdrawn on Monday due to browsing issues on certain websites. [...]
