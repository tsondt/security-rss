Title: Commentary on the Implementation Plan for the 2023 US National Cybersecurity Strategy
Date: 2023-07-20T11:12:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;national security policy
Slug: 2023-07-20-commentary-on-the-implementation-plan-for-the-2023-us-national-cybersecurity-strategy

[Source](https://www.schneier.com/blog/archives/2023/07/commentary-on-the-implementation-plan-for-the-2023-us-national-cybersecurity-strategy.html){:target="_blank" rel="noopener"}

> The Atlantic Council released a detailed commentary on the White House’s new “Implementation Plan for the 2023 US National Cybersecurity Strategy.” Lots of interesting bits. So far, at least three trends emerge: First, the plan contains a (somewhat) more concrete list of actions than its parent strategy, with useful delineation of lead and supporting agencies, as well as timelines aplenty. By assigning each action a designated lead and timeline, and by including a new nominal section (6) focused entirely on assessing effectiveness and continued iteration, the ONCD suggests that this is not so much a standalone text as the framework [...]
