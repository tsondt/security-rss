Title: AWS achieves its third ISMAP authorization in Japan
Date: 2023-07-03T14:13:19+00:00
Author: Hidetoshi Takeuchi
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS security;Compliance;ISMAP;Japan;Security;Security Blog
Slug: 2023-07-03-aws-achieves-its-third-ismap-authorization-in-japan

[Source](https://aws.amazon.com/blogs/security/aws-achieves-its-third-ismap-authorization-in-japan/){:target="_blank" rel="noopener"}

> Earning and maintaining customer trust is an ongoing commitment at Amazon Web Services (AWS). Our customers’ security requirements drive the scope and portfolio of the compliance reports, attestations, and certifications that we pursue. We’re excited to announce that AWS has achieved authorization under the Information System Security Management and Assessment Program (ISMAP), effective from April [...]
