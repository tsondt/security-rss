Title: Encryption-breaking, password-leaking bug in many AMD CPUs could take months to fix
Date: 2023-07-25T16:31:57+00:00
Author: Andrew Cunningham
Category: Ars Technica
Tags: Biz & IT;Security;Tech;AMD Ryzen;meltdown;Ryzen 3000;Ryzen 4000;ryzen 5 3600;ryzen 5000;ryzen 7000;zen 2;zenbleed
Slug: 2023-07-25-encryption-breaking-password-leaking-bug-in-many-amd-cpus-could-take-months-to-fix

[Source](https://arstechnica.com/?p=1956383){:target="_blank" rel="noopener"}

> Enlarge (credit: AMD) A recently disclosed bug in many of AMD's newer consumer, workstation, and server processors can cause the chips to leak data at a rate of up to 30 kilobytes per core per second, writes Tavis Ormandy, a member of Google's Project Zero security team. Executed properly, the so-called "Zenbleed" vulnerability (CVE-2023-20593) could give attackers access to encryption keys and root and user passwords, along with other sensitive data from any system using a CPU based on AMD's Zen 2 architecture. The bug allows attackers to swipe data from a CPU's registers. Modern processors attempt to speed up [...]
