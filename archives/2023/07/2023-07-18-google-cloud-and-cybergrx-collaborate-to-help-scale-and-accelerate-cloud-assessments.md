Title: Google Cloud and CyberGRX collaborate to help scale and accelerate cloud assessments
Date: 2023-07-18T16:00:00+00:00
Author: Rita Zurbrigg
Category: GCP Security
Tags: Partners;Security & Identity
Slug: 2023-07-18-google-cloud-and-cybergrx-collaborate-to-help-scale-and-accelerate-cloud-assessments

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-and-cybergrx-collaborate-to-help-scale-accelerate-assessments/){:target="_blank" rel="noopener"}

> Risk managers know there is one assessment type that’s foundational for every risk management program: the vendor risk assessment. Understanding the risk posture of your vendors and third parties, including your cloud providers, is an important part of an effective risk management program. While collecting and analyzing information can often be time-consuming for risk managers, Google Cloud collaborates with third-party risk management (TPRM) providers to make the process easier. These TPRM organizations provide independent due diligence services and platforms to help automate vendor risk management based on their inspection of security, privacy, business continuity, and operational resiliency controls, aligned with [...]
