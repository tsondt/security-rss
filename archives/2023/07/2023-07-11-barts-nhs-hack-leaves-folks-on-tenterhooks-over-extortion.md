Title: Barts NHS hack leaves folks on tenterhooks over extortion
Date: 2023-07-11T07:32:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2023-07-11-barts-nhs-hack-leaves-folks-on-tenterhooks-over-extortion

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/11/barts_blackcat_theft/){:target="_blank" rel="noopener"}

> BlackCat pounces on 7TB of data and theatens to release it Staff at one of the UK's largest hospital groups have spent a nervous week wondering if private data, stolen from their employer's IT systems by a ransomware gang, is going to be splurged online after a deadline to prevent publication passed.... [...]
