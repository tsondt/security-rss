Title: Microsoft whips up unrest after revealing Azure AD name change
Date: 2023-07-12T17:02:11+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-07-12-microsoft-whips-up-unrest-after-revealing-azure-ad-name-change

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/12/azure_ad_name_change/){:target="_blank" rel="noopener"}

> Ditching it after a decade? Devs warn of the hours to correct documentation and chaos it'll cause Microsoft is causing a stir among some tech pros after confirming it plans to rename Azure AD to Entra.... [...]
