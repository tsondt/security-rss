Title: BreachForums owner Pompompurin pleads guilty to hacking charges
Date: 2023-07-14T11:31:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-14-breachforums-owner-pompompurin-pleads-guilty-to-hacking-charges

[Source](https://www.bleepingcomputer.com/news/security/breachforums-owner-pompompurin-pleads-guilty-to-hacking-charges/){:target="_blank" rel="noopener"}

> 20-year-old Conor Brian Fitzpatrick aka Pompompurin, the owner of the notorious BreachForums (aka Breached) hacking forum, has pleaded guilty to charges of hacking and possession of child pornography. [...]
