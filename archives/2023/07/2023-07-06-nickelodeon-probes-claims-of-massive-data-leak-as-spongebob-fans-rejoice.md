Title: Nickelodeon probes claims of massive data leak as SpongeBob fans rejoice
Date: 2023-07-06T22:45:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-06-nickelodeon-probes-claims-of-massive-data-leak-as-spongebob-fans-rejoice

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/06/nickelodeon_confirms_data_leak/){:target="_blank" rel="noopener"}

> TV network's attorneys 'on a DMCA rampage'... are you sure you're ready, kids? Nickelodeon says it is probing claims that "decades old" material was stolen from it and leaked online. This follows reports on social media that someone had dumped 500GB of snatched animation files. Hilarity, and many SpongeBob SquarePants memes, ensued.... [...]
