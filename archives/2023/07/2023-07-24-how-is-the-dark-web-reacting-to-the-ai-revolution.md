Title: How is the Dark Web Reacting to the AI Revolution?
Date: 2023-07-24T10:01:02-04:00
Author: Sponsored by Flare
Category: BleepingComputer
Tags: Security
Slug: 2023-07-24-how-is-the-dark-web-reacting-to-the-ai-revolution

[Source](https://www.bleepingcomputer.com/news/security/how-is-the-dark-web-reacting-to-the-ai-revolution/){:target="_blank" rel="noopener"}

> Cybercriminals are already utilizing and creating malicious tools based on open source AI language models for phishing and malware development. Learn more from Flare about how threat actors are beginning to use AI. [...]
