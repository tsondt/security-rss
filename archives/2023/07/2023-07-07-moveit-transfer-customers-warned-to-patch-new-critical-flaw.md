Title: MOVEit Transfer customers warned to patch new critical flaw
Date: 2023-07-07T08:35:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-07-moveit-transfer-customers-warned-to-patch-new-critical-flaw

[Source](https://www.bleepingcomputer.com/news/security/moveit-transfer-customers-warned-to-patch-new-critical-flaw/){:target="_blank" rel="noopener"}

> MOVEit Transfer, the software at the center of the recent massive spree of Clop ransomware breaches, has received an update that fixes a critical-severity SQL injection bug and two other less severe vulnerabilities. [...]
