Title: How to Receive Alerts When Your IAM Configuration Changes
Date: 2023-07-31T15:50:35+00:00
Author: Dylan Souvage
Category: AWS Security
Tags: AWS Identity and Access Management (IAM);How-To;Intermediate (200);Security, Identity, & Compliance;alarms;alerts;Amazon CloudTrail;Amazon CloudWatch;Amazon EventBridge;Amazon SNS;AWS CloudTrail;AWS Identity and Access Management;filter patterns;IAM;Identity and Access Management;Organizations;Security Blog
Slug: 2023-07-31-how-to-receive-alerts-when-your-iam-configuration-changes

[Source](https://aws.amazon.com/blogs/security/how-to-receive-alerts-when-your-iam-configuration-changes/){:target="_blank" rel="noopener"}

> July 27, 2023: This post was originally published February 5, 2015, and received a major update July 31, 2023. As an Amazon Web Services (AWS) administrator, it’s crucial for you to implement robust protective controls to maintain your security configuration. Employing a detective control mechanism to monitor changes to the configuration serves as an additional [...]
