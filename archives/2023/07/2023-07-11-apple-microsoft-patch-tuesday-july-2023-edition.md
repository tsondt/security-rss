Title: Apple & Microsoft Patch Tuesday, July 2023 Edition
Date: 2023-07-11T22:55:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;Adam Barnett;Andrew Brandt;Apple zero-day;Cisco;CVE-2023-32046;CVE-2023-32049;CVE-2023-35311;CVE-2023-36884;Immersive Labs;iOS 16.5.1;Kevin Breen;macOS 13.4.1;Patch Tuesday July 2023;Rapid7;Safari 16.5.2;sophos;Storm-0978;trend micro
Slug: 2023-07-11-apple-microsoft-patch-tuesday-july-2023-edition

[Source](https://krebsonsecurity.com/2023/07/apple-microsoft-patch-tuesday-july-2023-edition/){:target="_blank" rel="noopener"}

> Microsoft Corp. today released software updates to quash 130 security bugs in its Windows operating systems and related software, including at least five flaws that are already seeing active exploitation. Meanwhile, Apple customers have their own zero-day woes again this month: On Monday, Apple issued (and then quickly pulled) an emergency update to fix a zero-day vulnerability that is being exploited on MacOS and iOS devices. On July 10, Apple pushed a “Rapid Security Response” update to fix a code execution flaw in the Webkit browser component built into iOS, iPadOS, and macOS Ventura. Almost as soon as the patch [...]
