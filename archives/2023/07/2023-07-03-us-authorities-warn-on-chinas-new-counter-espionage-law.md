Title: US authorities warn on China's new counter-espionage law
Date: 2023-07-03T06:28:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-07-03-us-authorities-warn-on-chinas-new-counter-espionage-law

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/03/china_espionage_law_update_warning/){:target="_blank" rel="noopener"}

> Almost anything you download from China could be considered spying, but at least one analyst isn't worried The United States' National Counterintelligence and Security Center (NCSC) has warned that China's updated Counter-Espionage law – which came into effect on July 1 – is dangerously ambiguous and could pose a risk to global business.... [...]
