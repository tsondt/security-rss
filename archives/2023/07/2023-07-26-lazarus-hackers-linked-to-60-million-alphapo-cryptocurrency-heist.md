Title: Lazarus hackers linked to $60 million Alphapo cryptocurrency heist
Date: 2023-07-26T16:19:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-07-26-lazarus-hackers-linked-to-60-million-alphapo-cryptocurrency-heist

[Source](https://www.bleepingcomputer.com/news/security/lazarus-hackers-linked-to-60-million-alphapo-cryptocurrency-heist/){:target="_blank" rel="noopener"}

> Blockchain analysts blame the North Korean Lazarus hacking group for a recent attack on payment processing platform Alphapo where the attackers stole almost $60 million in crypto. [...]
