Title: New Python tool checks NPM packages for manifest confusion issues
Date: 2023-07-04T07:01:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-07-04-new-python-tool-checks-npm-packages-for-manifest-confusion-issues

[Source](https://www.bleepingcomputer.com/news/security/new-python-tool-checks-npm-packages-for-manifest-confusion-issues/){:target="_blank" rel="noopener"}

> A security researcher and system administrator has developed a tool that can help users check for manifest mismatches in packages from the NPM JavaScript software registry. [...]
