Title: Critical TootRoot bug lets attackers hijack Mastodon servers
Date: 2023-07-07T12:40:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-07-critical-tootroot-bug-lets-attackers-hijack-mastodon-servers

[Source](https://www.bleepingcomputer.com/news/security/critical-tootroot-bug-lets-attackers-hijack-mastodon-servers/){:target="_blank" rel="noopener"}

> Mastodon, the free and open-source decentralized social networking platform, has patched four vulnerabilities, including a critical one that allows hackers to create arbitrary files on instance-hosting servers using specially crafted media files. [...]
