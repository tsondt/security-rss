Title: Quick: Manually patch this Zimbra bug that's under attack
Date: 2023-07-17T21:49:44+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-17-quick-manually-patch-this-zimbra-bug-thats-under-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/17/patch_zimbra_alert/){:target="_blank" rel="noopener"}

> Smells like Russian cyber spies (again) A vulnerability in Zimbra's software is being exploited right now by miscreants to compromise systems and attack selected government organizations, experts reckon.... [...]
