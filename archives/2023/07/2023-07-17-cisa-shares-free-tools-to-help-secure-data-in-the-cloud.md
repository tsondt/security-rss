Title: CISA shares free tools to help secure data in the cloud
Date: 2023-07-17T13:14:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-17-cisa-shares-free-tools-to-help-secure-data-in-the-cloud

[Source](https://www.bleepingcomputer.com/news/security/cisa-shares-free-tools-to-help-secure-data-in-the-cloud/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) has shared a factsheet providing details on free tools and guidance for securing digital assets after switching to the cloud from on-premises environments. [...]
