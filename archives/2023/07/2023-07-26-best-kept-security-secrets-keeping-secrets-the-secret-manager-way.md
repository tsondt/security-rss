Title: Best Kept Security Secrets: Keeping secrets, the Secret Manager way
Date: 2023-07-26T16:00:00+00:00
Author: Aswin Viswanathan
Category: GCP Security
Tags: Security & Identity
Slug: 2023-07-26-best-kept-security-secrets-keeping-secrets-the-secret-manager-way

[Source](https://cloud.google.com/blog/products/identity-security/best-kept-security-secrets-keeping-secrets-the-secret-manager-way/){:target="_blank" rel="noopener"}

> Can you keep a secret? Many people struggle to keep secrets. So do organizations. An organization’s digital credentials — its passwords, API keys, tokens, and encryption keys baked into its software code — are its vital secrets, used to authenticate access to protected resources and services. The security of these secrets represent one of the biggest risks that enterprises must manage when securing application infrastructure and environments. Fortunately, we have an important tool to help our customers better manage their secrets: Google Cloud Secret Manager. Secret Manager is a secure and efficient tool to centrally store, access, manage, and audit [...]
