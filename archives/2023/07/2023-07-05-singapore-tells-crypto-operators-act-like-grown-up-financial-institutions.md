Title: Singapore tells crypto operators: act like grown up financial institutions
Date: 2023-07-05T06:24:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-07-05-singapore-tells-crypto-operators-act-like-grown-up-financial-institutions

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/05/singapore_joins_nations_treating_crypto/){:target="_blank" rel="noopener"}

> Digital payment skeptics of the world, unite! You have nothing to lose but grifters and crims Singapore has joined the ranks of nations requiring digital payment operators to follow the same sort of regulations and customer protection requirements that apply to conventional financial institutions.... [...]
