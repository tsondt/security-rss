Title: Stolen Microsoft key offered widespread access to Microsoft cloud services
Date: 2023-07-21T17:08:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-07-21-stolen-microsoft-key-offered-widespread-access-to-microsoft-cloud-services

[Source](https://www.bleepingcomputer.com/news/security/stolen-microsoft-key-offered-widespread-access-to-microsoft-cloud-services/){:target="_blank" rel="noopener"}

> The Microsoft consumer signing key stolen by Storm-0558 Chinese hackers provided them with access far beyond the Exchange Online and Outlook.com accounts that Redmond said were compromised, according to Wiz security researchers. [...]
