Title: French Police Will Be Able to Spy on People through Their Cell Phones
Date: 2023-07-13T11:20:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;France;geolocation;privacy;surveillance
Slug: 2023-07-13-french-police-will-be-able-to-spy-on-people-through-their-cell-phones

[Source](https://www.schneier.com/blog/archives/2023/07/french-police-will-be-able-to-spy-on-people-through-their-cell-phones.html){:target="_blank" rel="noopener"}

> The French police are getting new surveillance powers : French police should be able to spy on suspects by remotely activating the camera, microphone and GPS of their phones and other devices, lawmakers agreed late on Wednesday, July 5. [...] Covering laptops, cars and other connected objects as well as phones, the measure would allow the geolocation of suspects in crimes punishable by at least five years’ jail. Devices could also be remotely activated to record sound and images of people suspected of terror offenses, as well as delinquency and organized crime. [...] During a debate on Wednesday, MPs in [...]
