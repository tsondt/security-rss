Title: Fortinet warns of critical RCE flaw in FortiOS, FortiProxy devices
Date: 2023-07-12T10:40:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-12-fortinet-warns-of-critical-rce-flaw-in-fortios-fortiproxy-devices

[Source](https://www.bleepingcomputer.com/news/security/fortinet-warns-of-critical-rce-flaw-in-fortios-fortiproxy-devices/){:target="_blank" rel="noopener"}

> Fortinet has disclosed a critical severity flaw impacting FortiOS and FortiProxy, allowing a remote attacker to perform arbitrary code execution on vulnerable devices. [...]
