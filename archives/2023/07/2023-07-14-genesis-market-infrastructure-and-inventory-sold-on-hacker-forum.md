Title: Genesis Market infrastructure and inventory sold on hacker forum
Date: 2023-07-14T16:29:43-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-07-14-genesis-market-infrastructure-and-inventory-sold-on-hacker-forum

[Source](https://www.bleepingcomputer.com/news/security/genesis-market-infrastructure-and-inventory-sold-on-hacker-forum/){:target="_blank" rel="noopener"}

> The administrators of the Genesis Market for stolen credentials announced on a hacker forum that they sold the store and a new owner would get the reins "next month." [...]
