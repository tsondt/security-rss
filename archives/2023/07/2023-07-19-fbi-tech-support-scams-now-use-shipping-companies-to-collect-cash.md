Title: FBI: Tech support scams now use shipping companies to collect cash
Date: 2023-07-19T05:17:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-19-fbi-tech-support-scams-now-use-shipping-companies-to-collect-cash

[Source](https://www.bleepingcomputer.com/news/security/fbi-tech-support-scams-now-use-shipping-companies-to-collect-cash/){:target="_blank" rel="noopener"}

> FBI warns of a surge in tech support scams targeting the elderly across the United States and urging victims to dispatch cash concealed within magazines or similar items through shipping firms. [...]
