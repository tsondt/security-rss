Title: Tracking Down a Suspect through Cell Phone Records
Date: 2023-07-17T11:13:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;crime;forensics
Slug: 2023-07-17-tracking-down-a-suspect-through-cell-phone-records

[Source](https://www.schneier.com/blog/archives/2023/07/tracking-down-a-suspect-through-cell-phone-records.html){:target="_blank" rel="noopener"}

> Interesting forensics in connection with a serial killer arrest: Investigators went through phone records collected from both midtown Manhattan and the Massapequa Park area of Long Island—two areas connected to a “burner phone” they had tied to the killings. (In court, prosecutors later said the burner phone was identified via an email account used to “solicit and arrange for sexual activity.” The victims had all been Craigslist escorts, according to officials.) They then narrowed records collected by cell towers to thousands, then to hundreds, and finally down to a handful of people who could match a suspect in the killings. [...]
