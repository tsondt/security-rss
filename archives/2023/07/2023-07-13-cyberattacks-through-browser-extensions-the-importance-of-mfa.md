Title: Cyberattacks through Browser Extensions – the Importance of MFA
Date: 2023-07-13T10:02:01-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-07-13-cyberattacks-through-browser-extensions-the-importance-of-mfa

[Source](https://www.bleepingcomputer.com/news/security/cyberattacks-through-browser-extensions-the-importance-of-mfa/){:target="_blank" rel="noopener"}

> More and more attacks are occurring via browser extensions or user-profile installations of tools. Learn more about these attacks from Specops Software and what you can do to protect yourself. [...]
