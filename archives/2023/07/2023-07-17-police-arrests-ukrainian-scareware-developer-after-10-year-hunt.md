Title: Police arrests Ukrainian scareware developer after 10-year hunt
Date: 2023-07-17T11:40:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-17-police-arrests-ukrainian-scareware-developer-after-10-year-hunt

[Source](https://www.bleepingcomputer.com/news/security/police-arrests-ukrainian-scareware-developer-after-10-year-hunt/){:target="_blank" rel="noopener"}

> The Spanish National Police has apprehended a Ukrainian national wanted internationally for his involvement in a scareware operation spanning from 2006 to 2011. [...]
