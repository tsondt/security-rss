Title: SEC now requires companies to disclose cyberattacks in 4 days
Date: 2023-07-26T14:41:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-26-sec-now-requires-companies-to-disclose-cyberattacks-in-4-days

[Source](https://www.bleepingcomputer.com/news/security/sec-now-requires-companies-to-disclose-cyberattacks-in-4-days/){:target="_blank" rel="noopener"}

> The U.S. Securities and Exchange Commission has adopted new rules requiring publicly traded companies to disclose cyberattacks within four business days after determining they're material incidents. [...]
