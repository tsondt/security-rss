Title: Google Workspace earns Dutch government's stamp of approval
Date: 2023-07-06T21:00:00+00:00
Author: Joris Schoonis
Category: GCP Security
Tags: Public Sector;Productivity & Collaboration;Security & Identity
Slug: 2023-07-06-google-workspace-earns-dutch-governments-stamp-of-approval

[Source](https://cloud.google.com/blog/products/identity-security/dutch-government-affirms-workspace/){:target="_blank" rel="noopener"}

> Editor's note : This post was originally published on the Google Workspace blog. At Google, we are committed to the privacy and security of our customers and to fostering trust in our cloud technologies. We regularly engage with customers, privacy regulators, and policymakers around the globe to understand evolving expectations and to adapt our products accordingly. On July 5, 2023, the Dutch Ministry of Education affirmed to the Dutch Parliament that Google has delivered on the commitments it made as part of the data protection impact assessment (DPIA), conducted by the Dutch government and education sector representatives. This means that [...]
