Title: Swiss visa appointments cancelled in UK due to 'IT incident'
Date: 2023-07-27T07:53:25-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-07-27-swiss-visa-appointments-cancelled-in-uk-due-to-it-incident

[Source](https://www.bleepingcomputer.com/news/security/swiss-visa-appointments-cancelled-in-uk-due-to-it-incident/){:target="_blank" rel="noopener"}

> All appointments for Swiss Schengen tourist and transit visa applicants have been cancelled across the UK. TLScontact, the Swiss government's chosen IT provider for facilitating visa applicants for citizens of third countries, has blamed an 'IT incident' at its London, Manchester, and Edinburgh centers for appointment cancellations. [...]
