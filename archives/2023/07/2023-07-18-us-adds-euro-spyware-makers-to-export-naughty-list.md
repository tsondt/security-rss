Title: US adds Euro spyware makers to export naughty list
Date: 2023-07-18T23:42:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-18-us-adds-euro-spyware-makers-to-export-naughty-list

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/18/us_sanctions_commercial_spyware/){:target="_blank" rel="noopener"}

> Predator dev joins Pegasus slinger The US government on Tuesday added commercial spyware makers Intellexa and Cytrox to its Entity List, saying the duo are a possible threat to national security.... [...]
