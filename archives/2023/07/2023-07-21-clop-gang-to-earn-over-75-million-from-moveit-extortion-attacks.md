Title: Clop gang to earn over $75 million from MOVEit extortion attacks
Date: 2023-07-21T12:34:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-07-21-clop-gang-to-earn-over-75-million-from-moveit-extortion-attacks

[Source](https://www.bleepingcomputer.com/news/security/clop-gang-to-earn-over-75-million-from-moveit-extortion-attacks/){:target="_blank" rel="noopener"}

> The Clop ransomware gang is expected to earn between $75-100 million from extorting victims of their massive MOVEit data theft campaign. [...]
