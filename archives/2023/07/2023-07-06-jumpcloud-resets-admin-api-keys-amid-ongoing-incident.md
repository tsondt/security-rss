Title: JumpCloud resets admin API keys amid ‘ongoing incident’
Date: 2023-07-06T06:23:56-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-07-06-jumpcloud-resets-admin-api-keys-amid-ongoing-incident

[Source](https://www.bleepingcomputer.com/news/security/jumpcloud-resets-admin-api-keys-amid-ongoing-incident/){:target="_blank" rel="noopener"}

> JumpCloud, a US-based enterprise software firm is notifying several customers of an "ongoing incident." As a caution, the company has invalidated existing admin API keys to protect its customer organizations. Headquartered in Colorado, the cloud-based directory-as-a-service platform serves over 180,000 organizations across the world. [...]
