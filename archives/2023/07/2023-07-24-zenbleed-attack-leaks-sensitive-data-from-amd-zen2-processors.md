Title: Zenbleed attack leaks sensitive data from AMD Zen2 processors
Date: 2023-07-24T17:28:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-07-24-zenbleed-attack-leaks-sensitive-data-from-amd-zen2-processors

[Source](https://www.bleepingcomputer.com/news/security/zenbleed-attack-leaks-sensitive-data-from-amd-zen2-processors/){:target="_blank" rel="noopener"}

> Google's security researcher Tavis Ormandy discovered a new vulnerability impacting AMD Zen2 CPUs that could allow a malicious actor to steal sensitive data, such as passwords and encryption keys, at a rate of 30KB/sec from each CPU core. [...]
