Title: P2PInfect server botnet spreads using Redis replication feature
Date: 2023-07-31T11:31:42-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-07-31-p2pinfect-server-botnet-spreads-using-redis-replication-feature

[Source](https://www.bleepingcomputer.com/news/security/p2pinfect-server-botnet-spreads-using-redis-replication-feature/){:target="_blank" rel="noopener"}

> Threat actors are actively targeting exposed instances of the Redis open-source data store with a peer-to-peer self-replicating worm with versions for both Windows and Linux that the malware authors named P2Pinfect. [...]
