Title: Capita staffers told attackers stole data from its own pension fund
Date: 2023-07-07T12:11:10+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-07-07-capita-staffers-told-attackers-stole-data-from-its-own-pension-fund

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/07/capita_pension_cyber_attack/){:target="_blank" rel="noopener"}

> Three months after mega breach by Russian cybercrime group Capita has informed some of its employees that its own pension fund was among the victims of a cybercrime attack on its system, resulting in the theft of their personal details, they say.... [...]
