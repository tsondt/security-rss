Title: More US States are ramping up data privacy laws in 2023
Date: 2023-07-25T10:02:04-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-07-25-more-us-states-are-ramping-up-data-privacy-laws-in-2023

[Source](https://www.bleepingcomputer.com/news/security/more-us-states-are-ramping-up-data-privacy-laws-in-2023/){:target="_blank" rel="noopener"}

> Legislation moves slowly, but in 2023 almost all five of the below regulations will take effect, making it a huge year for state data privacy acts. Learn more from Specops Software about the US privacy laws and what it means for your organization.. [...]
