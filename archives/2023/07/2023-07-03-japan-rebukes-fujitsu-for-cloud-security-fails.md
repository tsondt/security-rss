Title: Japan rebukes Fujitsu for cloud security fails
Date: 2023-07-03T01:35:46+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-07-03-japan-rebukes-fujitsu-for-cloud-security-fails

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/03/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> PLUS: Philippines cyber-slave raid; South Korea’s crypto crackdown; AWS boosts Chinese exports; and more Asia In Brief Japan's government last Friday rebuked Fujitsu for shabby cloud security.... [...]
