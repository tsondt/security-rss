Title: VirusTotal apologizes for data leak affecting 5,600 customers
Date: 2023-07-21T05:35:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-21-virustotal-apologizes-for-data-leak-affecting-5600-customers

[Source](https://www.bleepingcomputer.com/news/security/virustotal-apologizes-for-data-leak-affecting-5-600-customers/){:target="_blank" rel="noopener"}

> VirusTotal apologized on Friday for leaking the information of over 5,600 customers after an employee mistakenly uploaded a CSV file containing their info to the platform last month. [...]
