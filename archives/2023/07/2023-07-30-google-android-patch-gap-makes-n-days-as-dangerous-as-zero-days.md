Title: Google: Android patch gap makes n-days as dangerous as zero-days
Date: 2023-07-30T10:16:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-07-30-google-android-patch-gap-makes-n-days-as-dangerous-as-zero-days

[Source](https://www.bleepingcomputer.com/news/security/google-android-patch-gap-makes-n-days-as-dangerous-as-zero-days/){:target="_blank" rel="noopener"}

> Google has published its annual 0-day vulnerability report, presenting in-the-wild exploitation stats from 2022 and highlighting a long-standing problem in the Android platform that elevates the value and use of disclosed flaws for extended periods. [...]
