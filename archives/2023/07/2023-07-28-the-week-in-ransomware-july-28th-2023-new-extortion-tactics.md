Title: The Week in Ransomware - July 28th 2023 - New extortion tactics
Date: 2023-07-28T16:01:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-07-28-the-week-in-ransomware-july-28th-2023-new-extortion-tactics

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-july-28th-2023-new-extortion-tactics/){:target="_blank" rel="noopener"}

> With ransom payments declining, ransomware gangs are evolving their extortion tactics to utilize new methods to pressure victims. [...]
