Title: Microsoft previews Defender for IoT firmware analysis service
Date: 2023-07-26T17:48:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-07-26-microsoft-previews-defender-for-iot-firmware-analysis-service

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-previews-defender-for-iot-firmware-analysis-service/){:target="_blank" rel="noopener"}

> Microsoft announced the public preview of a new Defender for IoT feature that helps analyze the firmware of embedded Linux devices like routers for security vulnerabilities and common weaknesses. [...]
