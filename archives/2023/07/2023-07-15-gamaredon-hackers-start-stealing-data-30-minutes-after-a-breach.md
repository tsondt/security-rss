Title: Gamaredon hackers start stealing data 30 minutes after a breach
Date: 2023-07-15T10:07:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-15-gamaredon-hackers-start-stealing-data-30-minutes-after-a-breach

[Source](https://www.bleepingcomputer.com/news/security/gamaredon-hackers-start-stealing-data-30-minutes-after-a-breach/){:target="_blank" rel="noopener"}

> Ukraine's Computer Emergency Response Team (CERT-UA) is warning that the Gamaredon hacking operates in rapid attacks, stealing data from breached systems in under an hour. [...]
