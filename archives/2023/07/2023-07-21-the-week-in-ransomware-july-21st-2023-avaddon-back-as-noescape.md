Title: The Week in Ransomware - July 21st 2023 - Avaddon Back as NoEscape
Date: 2023-07-21T16:01:13-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-07-21-the-week-in-ransomware-july-21st-2023-avaddon-back-as-noescape

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-july-21st-2023-avaddon-back-as-noescape/){:target="_blank" rel="noopener"}

> This edition of the Week in Ransomware covers the last two weeks of news, as we could not cover it last week, and includes quite a bit of new information, including the return of the Avaddon ransomware gang. [...]
