Title: Consolidating controls in Security Hub: The new controls view and consolidated findings
Date: 2023-07-12T16:31:47+00:00
Author: Emmanuel Isimah
Category: AWS Security
Tags: AWS Security Hub;Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Incident response;Security;Security Blog;Security Hub
Slug: 2023-07-12-consolidating-controls-in-security-hub-the-new-controls-view-and-consolidated-findings

[Source](https://aws.amazon.com/blogs/security/consolidating-controls-in-security-hub-the-new-controls-view-and-consolidated-findings/){:target="_blank" rel="noopener"}

> In this blog post, we focus on two recently released features of AWS Security Hub: the consolidated controls view and consolidated control findings. You can use these features to manage controls across standards and to consolidate findings, which can help you significantly reduce finding noise and administrative overhead. Security Hub is a cloud security posture [...]
