Title: Critical RCE found in popular Ghostscript open-source PDF library
Date: 2023-07-12T12:46:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-12-critical-rce-found-in-popular-ghostscript-open-source-pdf-library

[Source](https://www.bleepingcomputer.com/news/security/critical-rce-found-in-popular-ghostscript-open-source-pdf-library/){:target="_blank" rel="noopener"}

> Ghostscript, an open-source interpreter for PostScript language and PDF files widely used in Linux, has been found vulnerable to a critical-severity remote code execution flaw. [...]
