Title: Colorado State University says data breach impacts students, staff
Date: 2023-07-14T10:23:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-07-14-colorado-state-university-says-data-breach-impacts-students-staff

[Source](https://www.bleepingcomputer.com/news/security/colorado-state-university-says-data-breach-impacts-students-staff/){:target="_blank" rel="noopener"}

> Colorado State University (CSU) has confirmed that the Clop ransomware operation stole sensitive personal information of current and former students and employees during the recent MOVEit Transfer data-theft attacks. [...]
