Title: Introducing time-bound key authentication for service accounts
Date: 2023-07-27T16:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Security & Identity
Slug: 2023-07-27-introducing-time-bound-key-authentication-for-service-accounts

[Source](https://cloud.google.com/blog/products/identity-security/introducing-time-bound-key-authentication-for-service-accounts/){:target="_blank" rel="noopener"}

> We’re continually working to improve Google Cloud’s Identity and Access Management (IAM) capabilities to help secure and govern your cloud environment. When organizations need to grant external applications permission to access Google Cloud APIs and resources there are several options. While many customers have embraced our updated guidance for authentication that includes using Workload identity federation where possible, service account keys are still widely used for authenticating external apps. To help address security challenges that may arise from the use of service account keys, we are excited to introduce service account key expiry. With this capability, customers can now configure [...]
