Title: Firmware vulnerabilities in millions of computers could give hackers superuser status
Date: 2023-07-20T19:29:52+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;baseboard management controller;bmc;remote code execution;updates;vulnerabilities
Slug: 2023-07-20-firmware-vulnerabilities-in-millions-of-computers-could-give-hackers-superuser-status

[Source](https://arstechnica.com/?p=1955540){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Two years ago, ransomware crooks breached hardware-maker Gigabyte and dumped more than 112 gigabytes of data that included information from some of its most important supply-chain partners, including Intel and AMD. Now researchers are warning that the leaked information revealed what could amount to critical zero-day vulnerabilities that could imperil huge swaths of the computing world. The vulnerabilities reside inside firmware that Duluth, Georgia-based AMI makes for BMCs (baseboard management controllers). These tiny computers soldered into the motherboard of servers allow cloud centers, and sometimes their customers, to streamline the remote management of vast fleets [...]
