Title: Apps with 1.5M installs on Google Play send your data to China
Date: 2023-07-06T14:43:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-07-06-apps-with-15m-installs-on-google-play-send-your-data-to-china

[Source](https://www.bleepingcomputer.com/news/security/apps-with-15m-installs-on-google-play-send-your-data-to-china/){:target="_blank" rel="noopener"}

> Security researchers discovered two malicious file management applications on Google Play with a collective installation count of over 1.5 million that collected excessive user data that goes well beyond what's needed to offer the promised functionality. [...]
