Title: Norway says Ivanti zero-day was used to hack govt IT systems
Date: 2023-07-25T02:42:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-07-25-norway-says-ivanti-zero-day-was-used-to-hack-govt-it-systems

[Source](https://www.bleepingcomputer.com/news/security/norway-says-ivanti-zero-day-was-used-to-hack-govt-it-systems/){:target="_blank" rel="noopener"}

> The Norwegian National Security Authority (NSM) has confirmed that attackers used a zero-day vulnerability in Ivanti's Endpoint Manager Mobile (EPMM) solution to breach a software platform used by 12 ministries in the country. [...]
