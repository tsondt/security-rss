Title: Microsoft expands access to cloud logging data for free after Exchange hacks
Date: 2023-07-19T11:39:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-07-19-microsoft-expands-access-to-cloud-logging-data-for-free-after-exchange-hacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-expands-access-to-cloud-logging-data-for-free-after-exchange-hacks/){:target="_blank" rel="noopener"}

> Microsoft is expanding access to additional cloud logging data for customers worldwide at no additional cost, allowing easier detection of breached networks and accounts. [...]
