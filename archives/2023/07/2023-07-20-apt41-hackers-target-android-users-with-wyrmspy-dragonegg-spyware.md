Title: APT41 hackers target Android users with WyrmSpy, DragonEgg spyware
Date: 2023-07-20T07:01:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-20-apt41-hackers-target-android-users-with-wyrmspy-dragonegg-spyware

[Source](https://www.bleepingcomputer.com/news/security/apt41-hackers-target-android-users-with-wyrmspy-dragonegg-spyware/){:target="_blank" rel="noopener"}

> The Chinese state-backed APT41 hacking group is targeting Android devices with two newly discovered spyware strains dubbed WyrmSpy and DragonEgg by Lookout security researchers. [...]
