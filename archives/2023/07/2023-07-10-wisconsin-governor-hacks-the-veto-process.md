Title: Wisconsin Governor Hacks the Veto Process
Date: 2023-07-10T11:24:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;hacking;laws;loopholes
Slug: 2023-07-10-wisconsin-governor-hacks-the-veto-process

[Source](https://www.schneier.com/blog/archives/2023/07/wisconsin-governor-hacks-the-veto-process.html){:target="_blank" rel="noopener"}

> In my latest book, A Hacker’s Mind, I wrote about hacks as loophole exploiting. This is a great example: The Wisconsin governor used his line-item veto powers—supposedly unique in their specificity—to change a one-year funding increase into a 400-year funding increase. He took this wording: Section 402. 121.905 (3) (c) 9. of the statues is created to read: 121.903 (3) (c) 9. For the limit for the 2023-24 school year and the 2024-25 school year, add $325 to the result under par. (b). And he deleted these words, numbers, and punctuation marks: Section 402. 121.905 (3) (c) 9. of the [...]
