Title: Critical AMI MegaRAC bugs can let hackers brick vulnerable servers
Date: 2023-07-20T12:30:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-20-critical-ami-megarac-bugs-can-let-hackers-brick-vulnerable-servers

[Source](https://www.bleepingcomputer.com/news/security/critical-ami-megarac-bugs-can-let-hackers-brick-vulnerable-servers/){:target="_blank" rel="noopener"}

> Two new critical severity vulnerabilities have been discovered in the MegaRAC Baseboard Management Controller (BMC) software made by hardware and software company American Megatrends International. [...]
