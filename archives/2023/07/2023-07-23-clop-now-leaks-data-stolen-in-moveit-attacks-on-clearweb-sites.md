Title: Clop now leaks data stolen in MOVEit attacks on clearweb sites
Date: 2023-07-23T15:10:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-07-23-clop-now-leaks-data-stolen-in-moveit-attacks-on-clearweb-sites

[Source](https://www.bleepingcomputer.com/news/security/clop-now-leaks-data-stolen-in-moveit-attacks-on-clearweb-sites/){:target="_blank" rel="noopener"}

> The Clop ransomware gang is copying an ALPHV ransomware gang extortion tactic by creating Internet-accessible websites dedicated to specific victims, making it easier to leak stolen data and further pressuring victims into paying a ransom. [...]
