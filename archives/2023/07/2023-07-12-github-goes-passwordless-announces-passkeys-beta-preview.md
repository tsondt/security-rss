Title: GitHub goes passwordless, announces passkeys beta preview
Date: 2023-07-12T11:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-12-github-goes-passwordless-announces-passkeys-beta-preview

[Source](https://www.bleepingcomputer.com/news/security/github-goes-passwordless-announces-passkeys-beta-preview/){:target="_blank" rel="noopener"}

> GitHub announced today the introduction of passwordless authentication support in public beta, allowing users to upgrade from security keys to passkeys. [...]
