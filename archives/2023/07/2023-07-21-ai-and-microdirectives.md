Title: AI and Microdirectives
Date: 2023-07-21T11:16:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;courts;essays;law enforcement;laws;privacy;surveillance
Slug: 2023-07-21-ai-and-microdirectives

[Source](https://www.schneier.com/blog/archives/2023/07/ai-and-microdirectives.html){:target="_blank" rel="noopener"}

> Imagine a future in which AIs automatically interpret—and enforce—laws. All day and every day, you constantly receive highly personalized instructions for how to comply with the law, sent directly by your government and law enforcement. You’re told how to cross the street, how fast to drive on the way to work, and what you’re allowed to say or do online—if you’re in any situation that might have legal implications, you’re told exactly what to do, in real time. Imagine that the computer system formulating these personal legal directives at mass scale is so complex that no one can explain how [...]
