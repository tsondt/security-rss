Title: Hackers steal Signal, WhatsApp user data with fake Android chat app
Date: 2023-07-31T15:26:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-07-31-hackers-steal-signal-whatsapp-user-data-with-fake-android-chat-app

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-signal-whatsapp-user-data-with-fake-android-chat-app/){:target="_blank" rel="noopener"}

> Hackers are using a fake Android app named 'SafeChat' to infect devices with spyware malware that steals call logs, texts, and GPS locations from phones. [...]
