Title: Microsoft: Chinese hackers breached US govt Exchange email accounts
Date: 2023-07-12T08:51:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-07-12-microsoft-chinese-hackers-breached-us-govt-exchange-email-accounts

[Source](https://www.bleepingcomputer.com/news/security/microsoft-chinese-hackers-breached-us-govt-exchange-email-accounts/){:target="_blank" rel="noopener"}

> A Chinese hacking group has breached the email accounts of more than two dozen organizations worldwide, including U.S. and Western European government agencies, according to Microsoft. [...]
