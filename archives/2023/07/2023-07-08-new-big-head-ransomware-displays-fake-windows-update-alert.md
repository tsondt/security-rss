Title: New ‘Big Head’ ransomware displays fake Windows update alert
Date: 2023-07-08T10:23:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-08-new-big-head-ransomware-displays-fake-windows-update-alert

[Source](https://www.bleepingcomputer.com/news/security/new-big-head-ransomware-displays-fake-windows-update-alert/){:target="_blank" rel="noopener"}

> Security researchers have dissected a recently emerged ransomware strain named 'Big Head' that may be spreading through malvertising that promotes fake Windows updates and Microsoft Word installers. [...]
