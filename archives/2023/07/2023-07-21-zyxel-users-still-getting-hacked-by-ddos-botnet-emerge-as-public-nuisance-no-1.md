Title: Zyxel users still getting hacked by DDoS botnet emerge as public nuisance No. 1
Date: 2023-07-21T18:51:53+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;botnet;exploit;vulnerability;zyxel
Slug: 2023-07-21-zyxel-users-still-getting-hacked-by-ddos-botnet-emerge-as-public-nuisance-no-1

[Source](https://arstechnica.com/?p=1955893){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson / Ars Technica ) Organizations that have yet to patch a 9.8-severity vulnerability in network devices made by Zyxel have emerged as public nuisance No. 1 as a sizable number of them continue to be exploited and wrangled into botnets that wage DDoS attacks. Zyxel patched the flaw on April 25. Five weeks later, Shadowserver, an organization that monitors Internet threats in real time, warned that many Zyxel firewalls and VPN servers had been compromised in attacks that showed no signs of stopping. The Shadowserver assessment at the time was: “If you have a vulnerable device [...]
