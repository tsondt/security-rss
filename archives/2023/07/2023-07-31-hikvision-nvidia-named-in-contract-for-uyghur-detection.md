Title: Hikvision, Nvidia named in contract for 'Uyghur detection'
Date: 2023-07-31T12:25:15+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-07-31-hikvision-nvidia-named-in-contract-for-uyghur-detection

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/31/beijing_hikvision_nvidia_uyghur/){:target="_blank" rel="noopener"}

> GPU giant says you can't stop secondary sales, surveillance gear maker maintains innocence Updated Video surveillance equipment maker Hikvision was paid $6 million by the Chinese government last year to provide technology that could identify members of the nation's Uyghur people, a Muslim ethnic majority, according to physical security monitoring org IPVM.... [...]
