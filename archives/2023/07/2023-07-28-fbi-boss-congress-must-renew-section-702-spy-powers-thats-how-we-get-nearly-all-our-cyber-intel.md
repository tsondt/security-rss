Title: FBI boss: Congress must renew Section 702 spy powers – that's how we get nearly all our cyber intel
Date: 2023-07-28T19:52:49+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-28-fbi-boss-congress-must-renew-section-702-spy-powers-thats-how-we-get-nearly-all-our-cyber-intel

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/28/fbi_section_702/){:target="_blank" rel="noopener"}

> Also: China's 'got a bigger hacking program than that of every major nation combined' Nearly all of the FBI's technical intelligence on malicious "cyber actors" in the first half of this year was obtained via Section 702 searches, according to FBI Director Christopher Wray.... [...]
