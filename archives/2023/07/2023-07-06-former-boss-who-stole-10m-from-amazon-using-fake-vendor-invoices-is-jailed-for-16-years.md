Title: Former boss who stole $10M from Amazon using fake vendor invoices is jailed for 16 years
Date: 2023-07-06T00:28:41+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-06-former-boss-who-stole-10m-from-amazon-using-fake-vendor-invoices-is-jailed-for-16-years

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/06/amazon_manager_fraud/){:target="_blank" rel="noopener"}

> Prime doesn't pay – well, not that much, anyway A former Amazon manager described by prosecutors as the "mastermind" behind a nearly $10 million scheme to steal money from the online megaretailer using fake invoices has been sentenced to 16 years behind bars in federal prison.... [...]
