Title: Nickelodeon investigates breach after leak of 'decades old’ data
Date: 2023-07-06T11:03:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-06-nickelodeon-investigates-breach-after-leak-of-decades-old-data

[Source](https://www.bleepingcomputer.com/news/security/nickelodeon-investigates-breach-after-leak-of-decades-old-data/){:target="_blank" rel="noopener"}

> Nickelodeon has confirmed that the data leaked from an alleged breach of the company is legitimate but some of it appears to be decades old. [...]
