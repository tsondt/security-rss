Title: Cloud CISO Perspectives: Late July 2023
Date: 2023-07-31T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2023-07-31-cloud-ciso-perspectives-late-july-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-late-july-2023/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for July 2023. Security, privacy, and protecting data is one of the major drivers behind the accelerated transformations in healthcare. Cyber threats to healthcare are on the rise, including compromised access and data, ransomware, and exploitation of vulnerabilities — and not enough is being done to stem this dangerous trend. It’s vital that healthcare leaders and boards take action to better protect patients and their data, argues Taylor Lehmann, our cybersecurity and healthcare expert and director in Google Cloud’s Office of the CISO, in his guest column below. As with all Cloud CISO [...]
