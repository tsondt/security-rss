Title: Migrating your secrets to AWS Secrets Manager, Part 2: Implementation
Date: 2023-07-21T16:44:32+00:00
Author: Adesh Gairola
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Technical How-to;AWS Identity and Access Management (IAM);AWS Secrets Manager;IAM Roles Anywhere;Identity;migration;secrets;Secrets Manager;Security;Security Blog
Slug: 2023-07-21-migrating-your-secrets-to-aws-secrets-manager-part-2-implementation

[Source](https://aws.amazon.com/blogs/security/migrating-your-secrets-to-aws-secrets-manager-part-2-implementation/){:target="_blank" rel="noopener"}

> In Part 1 of this series, we provided guidance on how to discover and classify secrets and design a migration solution for customers who plan to migrate secrets to AWS Secrets Manager. We also mentioned steps that you can take to enable preventative and detective controls for Secrets Manager. In this post, we discuss how [...]
