Title: VMware fixes bug exposing CF API admin credentials in audit logs
Date: 2023-07-25T11:45:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-25-vmware-fixes-bug-exposing-cf-api-admin-credentials-in-audit-logs

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-bug-exposing-cf-api-admin-credentials-in-audit-logs/){:target="_blank" rel="noopener"}

> VMware has patched an information disclosure vulnerability in VMware Tanzu Application Service for VMs (TAS for VMs) and Isolation Segment caused by credentials being logged and exposed via system audit logs. [...]
