Title: How to enforce multi-party approval for creating Matter-compliant certificate authorities
Date: 2023-07-12T19:55:00+00:00
Author: Ram Ramani
Category: AWS Security
Tags: AWS Private Certificate Authority;Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS IoT;AWS Private CA;Security Blog
Slug: 2023-07-12-how-to-enforce-multi-party-approval-for-creating-matter-compliant-certificate-authorities

[Source](https://aws.amazon.com/blogs/security/how-to-enforce-multi-party-approval-for-creating-matter-compliant-certificate-authorities/){:target="_blank" rel="noopener"}

> Customers who build smart home devices using the Matter protocol from the Connectivity Standards Alliance (CSA) need to create and maintain digital certificates, called device attestation certificates (DACs), to allow their devices to interoperate with devices from other vendors. DACs must be issued by a Matter device attestation certificate authority (CA). The CSA mandates multi-party [...]
