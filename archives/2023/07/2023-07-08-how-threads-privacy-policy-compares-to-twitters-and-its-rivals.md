Title: How Threads’ privacy policy compares to Twitter’s (and its rivals’)
Date: 2023-07-08T11:08:28+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2023-07-08-how-threads-privacy-policy-compares-to-twitters-and-its-rivals

[Source](https://arstechnica.com/?p=1952168){:target="_blank" rel="noopener"}

> Enlarge (credit: AHMET YARALI/Getty ) Meta's long-awaited Twitter alternative is here, and it's called Threads. The new social media app launches at a time when alternatives, like Bluesky, Mastodon, and Spill, are vying for users who are dissatisfied with Elon Musk's handling of Twitter's user experience, with its newly introduced rate limits and an uptick in hate speech. Meta owns Facebook, Instagram, and WhatsApp, so the company’s attempt to recreate an online experience similar to Twitter is likely to attract plenty of normies, lurkers, and nomadic shitposters. Meta is working to incorporate Threads as part of the online Fediverse, a [...]
