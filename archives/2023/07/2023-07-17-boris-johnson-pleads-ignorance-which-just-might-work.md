Title: Boris Johnson pleads ignorance, which just might work
Date: 2023-07-17T02:20:53+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-07-17-boris-johnson-pleads-ignorance-which-just-might-work

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/17/infosec_in_brief/){:target="_blank" rel="noopener"}

> ALSO: More high-profile MOVEit victims; CVSS 4.0 coming soon; and a long list of critical vulnerabilities Infosec in brief Former UK prime minister Boris Johnson lobbed a wrench into the works of the country's COVID-19 inquiry by claiming he couldn't remember the passcode to unlock an old phone being sought by investigators.... [...]
