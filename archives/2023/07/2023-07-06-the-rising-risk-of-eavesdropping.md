Title: The rising risk of eavesdropping
Date: 2023-07-06T08:57:11+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-07-06-the-rising-risk-of-eavesdropping

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/06/the_rising_risk_of_eavesdropping/){:target="_blank" rel="noopener"}

> How to deal with the evolving threat to our sensitive communications Webinar There is a folk tale of a woman, who on being told a secret burned to tell someone what she had heard. Believing that it was safe to do so, she whispered the secret into a hole in the ground only to hear it broadcast far and wide.... [...]
