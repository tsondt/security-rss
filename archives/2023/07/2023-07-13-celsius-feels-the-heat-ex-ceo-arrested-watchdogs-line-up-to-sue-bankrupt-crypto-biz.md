Title: Celsius feels the heat: Ex-CEO arrested, watchdogs line up to sue bankrupt crypto biz
Date: 2023-07-13T20:48:23+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-07-13-celsius-feels-the-heat-ex-ceo-arrested-watchdogs-line-up-to-sue-bankrupt-crypto-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/13/celsius_lawsuit_ceo/){:target="_blank" rel="noopener"}

> Exec faces fraud charges, one regulator wants $5 billion fine Alex Mashinsky, the now-former CEO of collapsed cryptocurrency concern Celsius, today faces charges of fraud as prosecutors and watchdogs pile in.... [...]
