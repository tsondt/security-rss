Title: Apple fixes new zero-day used in attacks against iPhones, Macs
Date: 2023-07-24T14:36:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-07-24-apple-fixes-new-zero-day-used-in-attacks-against-iphones-macs

[Source](https://www.bleepingcomputer.com/news/apple/apple-fixes-new-zero-day-used-in-attacks-against-iphones-macs/){:target="_blank" rel="noopener"}

> Apple has released security updates to address zero-day vulnerabilities exploited in attacks targeting iPhones, Macs, and iPads. [...]
