Title: Liberté, Égalité, Spyware: France okays cops snooping on phones
Date: 2023-07-10T05:33:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-07-10-liberté-égalité-spyware-france-okays-cops-snooping-on-phones

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/10/in_brief_security/){:target="_blank" rel="noopener"}

> ALSO: Shell fails to learn from past leaks; hundreds of solar plants found open to Mirai; and this week's crit vulns Infosec in brief With riots rocking the country, French parliamentarians have passed a bill granting law enforcement the right to snoop on suspects via "the remote activation of an electronic device without the knowledge or consent of its owner."... [...]
