Title: Backdoor in TETRA Police Radios
Date: 2023-07-26T11:05:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;cryptography;eavesdropping;encryption;infrastructure;law enforcement;police;radio
Slug: 2023-07-26-backdoor-in-tetra-police-radios

[Source](https://www.schneier.com/blog/archives/2023/07/backdoor-in-tetra-police-radios.html){:target="_blank" rel="noopener"}

> Seems that there is a deliberate backdoor in the twenty-year-old TErrestrial Trunked RAdio (TETRA) standard used by police forces around the world. The European Telecommunications Standards Institute (ETSI), an organization that standardizes technologies across the industry, first created TETRA in 1995. Since then, TETRA has been used in products, including radios, sold by Motorola, Airbus, and more. Crucially, TETRA is not open-source. Instead, it relies on what the researchers describe in their presentation slides as “secret, proprietary cryptography,” meaning it is typically difficult for outside experts to verify how secure the standard really is. The researchers said they worked around [...]
