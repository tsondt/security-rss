Title: What would sustainable security even look like?
Date: 2023-07-31T08:30:13+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2023-07-31-what-would-sustainable-security-even-look-like

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/31/opinion_column_sustainable_infosec/){:target="_blank" rel="noopener"}

> Clue: Nothing like what’s on offer today Opinion "There seems to be something wrong with our bloody ships today," fumed Admiral David Beatty during 1916's Battle of Jutland. Fair enough: three of the Royal Navy's finest vessels had just blown up and sank.... [...]
