Title: Configuring Workload Identity Federation for GitHub actions and Terraform Cloud
Date: 2023-07-07T16:00:00+00:00
Author: Sanmay Mishra
Category: GCP Security
Tags: DevOps & SRE;Developers & Practitioners;Security & Identity
Slug: 2023-07-07-configuring-workload-identity-federation-for-github-actions-and-terraform-cloud

[Source](https://cloud.google.com/blog/products/identity-security/secure-your-use-of-third-party-tools-with-identity-federation/){:target="_blank" rel="noopener"}

> Join us as we build on the concept and use cases of Workload Identity Federation, showcasing the security benefits of "keyless authentication.” We will dive into how Workload Identity Federation can be used in the context of CI/CD pipelines and tools that are commonly found in enterprise environments. Workload Identity Federation can be integrated with external providers, such as Gitlab, GitHub actions, and Terraform Cloud. We will show how the tokens issued by the external providers can be mapped to various attributes and how it can be used to evaluate conditions to restrict which identities can authenticate. Prerequisites A Google [...]
