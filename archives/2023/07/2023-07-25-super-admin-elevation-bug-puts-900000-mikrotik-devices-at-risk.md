Title: Super Admin elevation bug puts 900,000 MikroTik devices at risk
Date: 2023-07-25T18:08:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-25-super-admin-elevation-bug-puts-900000-mikrotik-devices-at-risk

[Source](https://www.bleepingcomputer.com/news/security/super-admin-elevation-bug-puts-900-000-mikrotik-devices-at-risk/){:target="_blank" rel="noopener"}

> A critical severity 'Super Admin' privilege elevation flaw puts over 900,000 MikroTik RouterOS routers at risk, potentially enabling attackers to take full control over a device and remain undetected. [...]
