Title: Practice Your Security Prompting Skills
Date: 2023-07-19T17:03:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;games;LLM;passwords
Slug: 2023-07-19-practice-your-security-prompting-skills

[Source](https://www.schneier.com/blog/archives/2023/07/practice-your-security-prompting-skills.html){:target="_blank" rel="noopener"}

> Gandalf is an interactive LLM game where the goal is to get the chatbot to reveal its password. There are eight levels of difficulty, as the chatbot gets increasingly restrictive instructions as to how it will answer. It’s a great teaching tool. I am stuck on Level 7. Feel free to give hints and discuss strategy in the comments below. I probably won’t look at them until I’ve cracked the last level. [...]
