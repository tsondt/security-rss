Title: Class-Action Lawsuit for Scraping Data without Permission
Date: 2023-07-05T11:14:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;ChatGPT;courts
Slug: 2023-07-05-class-action-lawsuit-for-scraping-data-without-permission

[Source](https://www.schneier.com/blog/archives/2023/07/class-action-lawsuit-for-scraping-data-without-permission.html){:target="_blank" rel="noopener"}

> I have mixed feelings about this class-action lawsuit against OpenAI and Microsoft, claiming that it “scraped 300 billion words from the internet” without either registering as a data broker or obtaining consent. On the one hand, I want this to be a protected fair use of public data. On the other hand, I want us all to be compensated for our uniquely human ability to generate language. There’s an interesting wrinkle on this. A recent paper showed that using AI generated text to train another AI invariably “causes irreversible defects.” From a summary : The tails of the original content [...]
