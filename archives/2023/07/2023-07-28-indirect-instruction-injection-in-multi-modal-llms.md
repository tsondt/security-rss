Title: Indirect Instruction Injection in Multi-Modal LLMs
Date: 2023-07-28T11:06:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;LLM;machine learning
Slug: 2023-07-28-indirect-instruction-injection-in-multi-modal-llms

[Source](https://www.schneier.com/blog/archives/2023/07/indirect-instruction-injection-in-multi-modal-llms.html){:target="_blank" rel="noopener"}

> Interesting research: “ (Ab)using Images and Sounds for Indirect Instruction Injection in Multi-Modal LLMs “: Abstract: We demonstrate how images and sounds can be used for indirect prompt and instruction injection in multi-modal LLMs. An attacker generates an adversarial perturbation corresponding to the prompt and blends it into an image or audio recording. When the user asks the (unmodified, benign) model about the perturbed image or audio, the perturbation steers the model to output the attacker-chosen text and/or make the subsequent dialog follow the attacker’s instruction. We illustrate this attack with several proof-of-concept examples targeting LLaVa and PandaGPT. [...]
