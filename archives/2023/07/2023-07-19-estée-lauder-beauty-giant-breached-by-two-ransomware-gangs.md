Title: Estée Lauder beauty giant breached by two ransomware gangs
Date: 2023-07-19T19:51:24-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-07-19-estée-lauder-beauty-giant-breached-by-two-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/est-e-lauder-beauty-giant-breached-by-two-ransomware-gangs/){:target="_blank" rel="noopener"}

> Two ransomware actors, ALPHV/BlackCat and Clop, have listed beauty company Estée Lauder on their data leak sites as a victim of separate attacks. [...]
