Title: Recycling giant TOMRA pulls systems offline following 'extensive cyberattack'
Date: 2023-07-18T12:59:10+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-07-18-recycling-giant-tomra-pulls-systems-offline-following-extensive-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/18/tomra_cyberattack/){:target="_blank" rel="noopener"}

> Says baddies launched attack at weekend, isolates parts of tech infrastructure to contain spread Norwegian mining and recycling giant TOMRA says it has isolated tech systems as it deals with an "extensive cyberattack."... [...]
