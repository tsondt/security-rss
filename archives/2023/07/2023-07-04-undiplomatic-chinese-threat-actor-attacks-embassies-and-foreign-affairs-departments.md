Title: Undiplomatic Chinese threat actor attacks embassies and foreign affairs departments
Date: 2023-07-04T05:29:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-07-04-undiplomatic-chinese-threat-actor-attacks-embassies-and-foreign-affairs-departments

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/04/smugx_europe_china_attack_europe/){:target="_blank" rel="noopener"}

> Sneaky HTML smuggling signals MustangPanda shift towards Europe, Checkpoint charges Infosec outfit Checkpoint says it's spotted a Chinese actor targeting diplomatic facilities around Europe.... [...]
