Title: HWL Ebsworth hack: Queensland says its files were taken after criminals release Victorian documents
Date: 2023-07-14T11:10:34+00:00
Author: Adeshola Ore and Eden Gillespie
Category: The Guardian
Tags: Cybercrime;Victoria;Internet;Data and computer security;Privacy;Australia news;Hacking;Victorian politics
Slug: 2023-07-14-hwl-ebsworth-hack-queensland-says-its-files-were-taken-after-criminals-release-victorian-documents

[Source](https://www.theguardian.com/technology/2023/jul/14/hwl-ebsworth-hack-sensitive-victorian-government-documents-released-criminals){:target="_blank" rel="noopener"}

> State’s chief information security officer says information from Victorian departments and agencies was accessed Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Highly sensitive legal documents from the Victorian government have been published on the dark web by cybercriminals, with Queensland also confirming files from at least one of its departments are included in the breach. The breach is connected to data that was stolen from the law firm HWL Ebsworth in April by a Russian-linked ransomware gang, known as ALPHV/Blackcat, and posted online. Sign [...]
