Title: North Korean satellite had no military utility for spying, says South Korea
Date: 2023-07-06T00:30:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-07-06-north-korean-satellite-had-no-military-utility-for-spying-says-south-korea

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/06/north_korean_satellite_had_no/){:target="_blank" rel="noopener"}

> Lends credence to theory that Pyongyang is testing ballistic missiles against international rules A North Korean satellite allegedly designed for reconnaissance was not viable for its alleged intended purpose, according to South Korea's military on Wednesday.... [...]
