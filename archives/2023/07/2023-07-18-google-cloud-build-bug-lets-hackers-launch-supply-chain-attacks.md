Title: Google Cloud Build bug lets hackers launch supply chain attacks
Date: 2023-07-18T09:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-18-google-cloud-build-bug-lets-hackers-launch-supply-chain-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-cloud-build-bug-lets-hackers-launch-supply-chain-attacks/){:target="_blank" rel="noopener"}

> A critical design flaw in the Google Cloud Build service discovered by cloud security firm Orca Security can let attackers escalate privileges, providing them with almost nearly-full and unauthorized access to Google Artifact Registry code repositories. [...]
