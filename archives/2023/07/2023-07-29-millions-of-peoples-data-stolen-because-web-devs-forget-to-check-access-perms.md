Title: Millions of people's data stolen because web devs forget to check access perms
Date: 2023-07-29T00:09:38+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-29-millions-of-peoples-data-stolen-because-web-devs-forget-to-check-access-perms

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/29/cisa_nsa_idor_australia/){:target="_blank" rel="noopener"}

> IDORs of the storm Personal, financial, and health information belonging to millions of folks has been stolen via a particular class of website vulnerability, say cybersecurity agencies in the US and Australia. They're urging developers to review their code and squish these bugs for good.... [...]
