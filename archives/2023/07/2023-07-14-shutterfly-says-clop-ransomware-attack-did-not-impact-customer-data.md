Title: Shutterfly says Clop ransomware attack did not impact customer data
Date: 2023-07-14T04:09:40-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-07-14-shutterfly-says-clop-ransomware-attack-did-not-impact-customer-data

[Source](https://www.bleepingcomputer.com/news/security/shutterfly-says-clop-ransomware-attack-did-not-impact-customer-data/){:target="_blank" rel="noopener"}

> Shutterfly, an online retail and photography manufacturing platform, is among the latest victims hit by Clop ransomware. Over the last few months, Clop ransomware gang has been exploiting a vulnerability in the MOVEit File Transfer utility to breach hundreds of companies to steal their data and attempt extortion against them. [...]
