Title: Ivanti plugs critical bug – but not before it was used against Norwegian government
Date: 2023-07-26T06:27:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-07-26-ivanti-plugs-critical-bug-but-not-before-it-was-used-against-norwegian-government

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/26/ivanti_patch_norway_ciso/){:target="_blank" rel="noopener"}

> Uncle Sam warns sysadmins to get patching as soon as possible A critical security flaw in Ivanti's mobile endpoint management code was exploited and used to compromise 12 Norwegian government agencies before the vendor plugged the hole.... [...]
