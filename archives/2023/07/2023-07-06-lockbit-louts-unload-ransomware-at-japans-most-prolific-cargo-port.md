Title: LockBit louts unload ransomware at Japan’s most prolific cargo port
Date: 2023-07-06T03:13:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-07-06-lockbit-louts-unload-ransomware-at-japans-most-prolific-cargo-port

[Source](https://go.theregister.com/feed/www.theregister.com/2023/07/06/lockbit_nagoya_attack/){:target="_blank" rel="noopener"}

> Nagoya Harbor hit the rocks yesterday but looks to be afloat once more The port of Nagoya – which shifted 2.68 million shipping containers and 164 million tons of cargo in 2022 – has moved precious few in the last 24 hours after finding itself the latest victim of Russia's notorious LockBit ransomware gang.... [...]
