Title: WordPress AIOS plugin used by 1M sites logged plaintext passwords
Date: 2023-07-14T11:55:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-07-14-wordpress-aios-plugin-used-by-1m-sites-logged-plaintext-passwords

[Source](https://www.bleepingcomputer.com/news/security/wordpress-aios-plugin-used-by-1m-sites-logged-plaintext-passwords/){:target="_blank" rel="noopener"}

> The All-In-One Security (AIOS) WordPress security plugin, used by over a million WordPress sites, was found to be logging plaintext passwords from user login attempts to the site's database, putting account security at risk. [...]
