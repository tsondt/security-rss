Title: Cisco SD-WAN vManage impacted by unauthenticated REST API access
Date: 2023-07-13T17:53:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2023-07-13-cisco-sd-wan-vmanage-impacted-by-unauthenticated-rest-api-access

[Source](https://www.bleepingcomputer.com/news/security/cisco-sd-wan-vmanage-impacted-by-unauthenticated-rest-api-access/){:target="_blank" rel="noopener"}

> The Cisco SD-WAN vManage management software is impacted by a flaw that allows an unauthenticated, remote attacker to gain read or limited write permissions to the configuration of the affected instance. [...]
