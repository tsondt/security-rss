Title: How Amazon CodeGuru Security helps you effectively balance security and velocity
Date: 2023-07-19T15:30:20+00:00
Author: Leo da Silva
Category: AWS Security
Tags: Advanced (300);Amazon CodeGuru;Security, Identity, & Compliance;Technical How-to;Thought Leadership;Security;Security Blog
Slug: 2023-07-19-how-amazon-codeguru-security-helps-you-effectively-balance-security-and-velocity

[Source](https://aws.amazon.com/blogs/security/how_amazon_codeguru_security_helps_effectively_balance_security_and_velocity/){:target="_blank" rel="noopener"}

> Software development is a well-established process—developers write code, review it, build artifacts, and deploy the application. They then monitor the application using data to improve the code. This process is often repeated many times over. As Amazon Web Services (AWS) customers embrace modern software development practices, they sometimes face challenges with the use of third-party [...]
