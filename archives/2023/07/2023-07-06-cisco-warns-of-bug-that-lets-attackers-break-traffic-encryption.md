Title: Cisco warns of bug that lets attackers break traffic encryption
Date: 2023-07-06T06:35:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-06-cisco-warns-of-bug-that-lets-attackers-break-traffic-encryption

[Source](https://www.bleepingcomputer.com/news/security/cisco-warns-of-bug-that-lets-attackers-break-traffic-encryption/){:target="_blank" rel="noopener"}

> Cisco warned customers today of a high-severity vulnerability impacting some data center switch models and allowing attackers to tamper with encrypted traffic. [...]
