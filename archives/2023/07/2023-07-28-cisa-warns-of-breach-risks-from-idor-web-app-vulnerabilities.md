Title: CISA warns of breach risks from IDOR web app vulnerabilities
Date: 2023-07-28T12:10:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-07-28-cisa-warns-of-breach-risks-from-idor-web-app-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-of-breach-risks-from-idor-web-app-vulnerabilities/){:target="_blank" rel="noopener"}

> CISA warned today of the significant breach risks linked to insecure direct object reference (IDOR) vulnerabilities impacting web applications in a joint advisory with the Australian Cyber Security Centre (ACSC) and U.S. National Security Agency (NSA). [...]
