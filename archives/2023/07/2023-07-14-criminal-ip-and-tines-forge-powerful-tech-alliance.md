Title: Criminal IP and Tines Forge Powerful Tech Alliance
Date: 2023-07-14T10:01:02-04:00
Author: Sponsored by Criminal IP
Category: BleepingComputer
Tags: Security
Slug: 2023-07-14-criminal-ip-and-tines-forge-powerful-tech-alliance

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-and-tines-forge-powerful-tech-alliance/){:target="_blank" rel="noopener"}

> Criminal IP, a leading Cyber Threat Intelligence search engine, has formed a powerful alliance with Tines, a renowned provider of no-code automation solutions. [...]
