Title: Attackers find new ways to deliver DDoSes with “alarming” sophistication
Date: 2023-07-19T20:02:38+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;DDoS;distributed denial of service attack;DNS;domain name system
Slug: 2023-07-19-attackers-find-new-ways-to-deliver-ddoses-with-alarming-sophistication

[Source](https://arstechnica.com/?p=1955118){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson / Getty) The protracted arms race between criminals who wage distributed denial-of-service attacks and the defenders who attempt to stop them continues, as the former embraces “alarming” new methods to make their online offensives more powerful and destructive, researchers from content-delivery network Cloudflare reported Wednesday. With a global network spanning more than 300 cities in more than 100 countries around the world, Cloudflare has visibility into these types of attacks that’s shared by only a handful of other companies. The company said it delivers more than 63 million network requests per second and more than 2 [...]
