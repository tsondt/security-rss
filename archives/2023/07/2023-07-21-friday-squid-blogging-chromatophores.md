Title: Friday Squid Blogging: Chromatophores
Date: 2023-07-21T21:10:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-07-21-friday-squid-blogging-chromatophores

[Source](https://www.schneier.com/blog/archives/2023/07/friday-squid-blogging-chromatophores.html){:target="_blank" rel="noopener"}

> Neat : Chromatophores are tiny color-changing cells in cephalopods. Watch them blink back and forth from purple to white on this squid’s skin in an Instagram video taken by Drew Chicone... It’s completely hypnotic to watch these tiny cells flash with color. It’s as if the squid has a little sky full of twinkling stars on its skin. This has to be one of the coolest looking sea creatures I’ve seen. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
