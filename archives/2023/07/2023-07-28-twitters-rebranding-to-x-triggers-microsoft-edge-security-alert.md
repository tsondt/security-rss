Title: Twitter's rebranding to 'X' triggers Microsoft Edge security alert
Date: 2023-07-28T12:30:25-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-07-28-twitters-rebranding-to-x-triggers-microsoft-edge-security-alert

[Source](https://www.bleepingcomputer.com/news/security/twitters-rebranding-to-x-triggers-microsoft-edge-security-alert/){:target="_blank" rel="noopener"}

> Microsoft Edge web browser has been displaying security warnings after Twitter changed its name to 'X'. It's got to do with a security feature dubbed 'Progressive Web App Icon change', designed to keep users safe during app icon or name changes. [...]
