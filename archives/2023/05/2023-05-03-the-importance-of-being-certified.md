Title: The importance of being certified
Date: 2023-05-03T03:20:12+00:00
Author: Rob Jaques
Category: The Register
Tags: 
Slug: 2023-05-03-the-importance-of-being-certified

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/03/the_importance_of_being_certified/){:target="_blank" rel="noopener"}

> New GIAC Security Professional and revamped GIAC Security Expert qualifications offer increased choice and flexibility for cybersecurity pros Sponsored Post The importance of certifications such as the GIAC (Global Information Assurance Certification) has never been greater for infosec professionals. Because adding them to the CV will not only improve individual skill levels, but also differentiate candidates in an increasingly competitive cyber security industry.... [...]
