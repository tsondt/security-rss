Title: Food distribution giant Sysco warns of data breach after cyberattack
Date: 2023-05-09T15:47:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-09-food-distribution-giant-sysco-warns-of-data-breach-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/food-distribution-giant-sysco-warns-of-data-breach-after-cyberattack/){:target="_blank" rel="noopener"}

> Sysco, a leading global food distribution company, has confirmed that its network was breached earlier this year by attackers who stole sensitive information, including business, customer, and employee data. [...]
