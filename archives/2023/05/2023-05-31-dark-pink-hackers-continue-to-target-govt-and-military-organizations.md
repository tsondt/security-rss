Title: Dark Pink hackers continue to target govt and military organizations
Date: 2023-05-31T04:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-31-dark-pink-hackers-continue-to-target-govt-and-military-organizations

[Source](https://www.bleepingcomputer.com/news/security/dark-pink-hackers-continue-to-target-govt-and-military-organizations/){:target="_blank" rel="noopener"}

> The Dark Pink APT hacking group continues to be very active in 2023, observed targeting government, military, and education organizations in Indonesia, Brunei, and Vietnam. [...]
