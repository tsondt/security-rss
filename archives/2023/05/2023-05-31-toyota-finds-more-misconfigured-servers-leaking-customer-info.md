Title: Toyota finds more misconfigured servers leaking customer info
Date: 2023-05-31T10:46:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-31-toyota-finds-more-misconfigured-servers-leaking-customer-info

[Source](https://www.bleepingcomputer.com/news/security/toyota-finds-more-misconfigured-servers-leaking-customer-info/){:target="_blank" rel="noopener"}

> Toyota Motor Corporation has discovered two additional misconfigured cloud services that leaked car owners' personal information for over seven years. [...]
