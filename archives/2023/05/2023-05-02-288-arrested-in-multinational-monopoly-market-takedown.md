Title: 288 arrested in multinational Monopoly Market takedown
Date: 2023-05-02T19:55:20+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-02-288-arrested-in-multinational-monopoly-market-takedown

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/02/monopoly_market_arrests/){:target="_blank" rel="noopener"}

> US tells criminals it 'will find you' and has a particular set of skills In an international operation 288 people have been arrested across the US, Europe and South America after allegedly selling opioids on the now-shuttered Monopoly Market dark web drug trafficking marketplace, according to US and European law enforcement.... [...]
