Title: Airline exposes passenger info to others due to a 'technical error'
Date: 2023-05-15T11:04:36-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-05-15-airline-exposes-passenger-info-to-others-due-to-a-technical-error

[Source](https://www.bleepingcomputer.com/news/security/airline-exposes-passenger-info-to-others-due-to-a-technical-error/){:target="_blank" rel="noopener"}

> airBaltic, Latvia's flag carrier has acknowledged that an 'internal technical error' exposed reservation details of some of its passengers to other airBaltic passengers. [...]
