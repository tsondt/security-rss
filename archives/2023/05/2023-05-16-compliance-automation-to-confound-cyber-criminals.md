Title: Compliance automation to confound cyber criminals
Date: 2023-05-16T14:03:05+00:00
Author: Rob Jaques
Category: The Register
Tags: 
Slug: 2023-05-16-compliance-automation-to-confound-cyber-criminals

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/16/compliance_automation_to_confound_cyber/){:target="_blank" rel="noopener"}

> How you can streamline the auditing process while improving compliance and security Sponsored Post Eminent US businessman Norman Ralph Augustine - who served as United States Under Secretary of the Army, as well as chairman and CEO of the Lockheed Martin Corporation - pointed to the importance of audit and compliance when he famously commented: "Two-thirds of the Earth's surface is covered with water. The other third is covered with auditors from headquarters."... [...]
