Title: EU proposes spyware Tech Lab to keep Big Brother governments in check
Date: 2023-05-09T16:40:11+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-05-09-eu-proposes-spyware-tech-lab-to-keep-big-brother-governments-in-check

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/09/pega_commitee_report/){:target="_blank" rel="noopener"}

> Potential roles for IT pros and lawyers, European city location included Tired of working for an egomaniacal startup boss or dull enterprise biz? A new org has been proposed called the Tech Lab, where you'd investigate the worst kinds of surveillance by governments on their citizens. In which despotic state, you ask? Surprise! You could base yourself in any European city.... [...]
