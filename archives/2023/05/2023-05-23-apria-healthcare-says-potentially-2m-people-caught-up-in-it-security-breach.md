Title: Apria Healthcare says potentially 2M people caught up in IT security breach
Date: 2023-05-23T23:58:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-23-apria-healthcare-says-potentially-2m-people-caught-up-in-it-security-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/23/apria_healthcare_breach/){:target="_blank" rel="noopener"}

> Took two years to tell us 'small number of emails' accessed Personal and financial data describing almost 1.9 million Apria Healthcare patients and employees may have been accessed by crooks who breached the company's networks over a series of months in 2019 and 2021.... [...]
