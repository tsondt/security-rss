Title: ScanSource says ransomware attack behind multi-day outages
Date: 2023-05-17T10:41:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-17-scansource-says-ransomware-attack-behind-multi-day-outages

[Source](https://www.bleepingcomputer.com/news/security/scansource-says-ransomware-attack-behind-multi-day-outages/){:target="_blank" rel="noopener"}

> Technology provider ScanSource has announced it has fallen victim to a ransomware attack impacting some of its systems, business operations, and customer portals. [...]
