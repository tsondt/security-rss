Title: Friday Squid Blogging: Online Cephalopod Course
Date: 2023-05-26T21:05:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-05-26-friday-squid-blogging-online-cephalopod-course

[Source](https://www.schneier.com/blog/archives/2023/05/friday-squid-blogging-online-cephalopod-course.html){:target="_blank" rel="noopener"}

> Atlas Obscura has a five-part online course on cephalopods, taught by squid biologist Dr. Sarah McAnulty. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
