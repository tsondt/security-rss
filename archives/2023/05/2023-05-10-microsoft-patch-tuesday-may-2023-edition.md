Title: Microsoft Patch Tuesday, May 2023 Edition
Date: 2023-05-10T01:19:58+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;Adam Barnett;BlackLotus;CVE-2023-24932;CVE-2023-24941;CVE-2023-28283;CVE-2023-29325;CVE-2023-29336;Immersive Labs;Kevin Breen;Rapid7;sans internet storm center
Slug: 2023-05-10-microsoft-patch-tuesday-may-2023-edition

[Source](https://krebsonsecurity.com/2023/05/microsoft-patch-tuesday-may-2023-edition/){:target="_blank" rel="noopener"}

> Microsoft today released software updates to fix at least four dozen security holes in its Windows operating systems and other software, including patches for two zero-day vulnerabilities that are already being exploited in active attacks. First up in May’s zero-day flaws is CVE-2023-29336, which is an “elevation of privilege” weakness in Windows which has a low attack complexity, requires low privileges, and no user interaction. However, as the SANS Internet Storm Center points out, the attack vector for this bug is local. “Local Privilege escalation vulnerabilities are a key part of attackers’ objectives,” said Kevin Breen, director of cyber threat [...]
