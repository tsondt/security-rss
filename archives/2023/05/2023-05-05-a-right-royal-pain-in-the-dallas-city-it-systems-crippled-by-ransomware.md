Title: A right Royal pain in the Dallas: City IT systems crippled by ransomware
Date: 2023-05-05T19:19:54+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-05-05-a-right-royal-pain-in-the-dallas-city-it-systems-crippled-by-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/05/dallas_royal_ransomeare/){:target="_blank" rel="noopener"}

> Texas officials preach limited government... but not this limited The city of Dallas, Texas, is working to restore city services following a ransomware attack that crippled its IT systems.... [...]
