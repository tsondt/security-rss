Title: Level Finance crypto exchange hacked after two security audits
Date: 2023-05-02T18:32:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-05-02-level-finance-crypto-exchange-hacked-after-two-security-audits

[Source](https://www.bleepingcomputer.com/news/security/level-finance-crypto-exchange-hacked-after-two-security-audits/){:target="_blank" rel="noopener"}

> Hackers exploited a Level Finance smart contract vulnerability to drain 214,000 LVL tokens from the decentralized exchange and swapped them for 3,345 BNB, worth approximately $1,100,000. [...]
