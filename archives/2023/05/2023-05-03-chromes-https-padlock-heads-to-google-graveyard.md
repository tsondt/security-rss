Title: Chrome's HTTPS padlock heads to Google Graveyard
Date: 2023-05-03T20:03:39+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-05-03-chromes-https-padlock-heads-to-google-graveyard

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/03/google_chrome_padlock/){:target="_blank" rel="noopener"}

> As blue check marks start showing up in Gmail Logowatch Google plans to retire the padlock icon that appears in the Chrome status bar during a secure HTTPS web browsing session because the interface graphic has outlived its usefulness.... [...]
