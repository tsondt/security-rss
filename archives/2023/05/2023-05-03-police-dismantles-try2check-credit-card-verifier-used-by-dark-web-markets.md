Title: Police dismantles Try2Check credit card verifier used by dark web markets
Date: 2023-05-03T12:11:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-03-police-dismantles-try2check-credit-card-verifier-used-by-dark-web-markets

[Source](https://www.bleepingcomputer.com/news/security/police-dismantles-try2check-credit-card-verifier-used-by-dark-web-markets/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice announced today the indictment of Russian citizen Denis Gennadievich Kulkov, suspected of running a stolen credit card checking operation that generated tens of millions in revenue. [...]
