Title: GitLab 'strongly recommends' patching max severity flaw ASAP
Date: 2023-05-24T15:25:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-24-gitlab-strongly-recommends-patching-max-severity-flaw-asap

[Source](https://www.bleepingcomputer.com/news/security/gitlab-strongly-recommends-patching-max-severity-flaw-asap/){:target="_blank" rel="noopener"}

> GitLab has released an emergency security update, version 16.0.1, to address a maximum severity (CVSS v3.1 score: 10.0) path traversal flaw tracked as CVE-2023-2825. [...]
