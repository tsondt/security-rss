Title: Friday Squid Blogging: Peruvian Squid-Fishing Regulation Drives Chinese Fleets Away
Date: 2023-05-19T21:06:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;hacking;squid
Slug: 2023-05-19-friday-squid-blogging-peruvian-squid-fishing-regulation-drives-chinese-fleets-away

[Source](https://www.schneier.com/blog/archives/2023/05/friday-squid-blogging-peruvian-squid-fishing-regulation-drives-chinese-fleets-away.html){:target="_blank" rel="noopener"}

> A Peruvian oversight law has the opposite effect: Peru in 2020 began requiring any foreign fishing boat entering its ports to use a vessel monitoring system allowing its activities to be tracked in real time 24 hours a day. The equipment, which tracks a vessel’s geographic position and fishing activity through a proprietary satellite communication system, sought to provide authorities with visibility into several hundred Chinese squid vessels that every year amass off the west coast of South America. [...] Instead of increasing oversight, the new Peruvian regulations appear to have driven Chinese ships away from the country’s ports—and kept [...]
