Title: Expeditionary Cyberspace Operations
Date: 2023-05-26T11:12:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cyberwar;national security policy
Slug: 2023-05-26-expeditionary-cyberspace-operations

[Source](https://www.schneier.com/blog/archives/2023/05/expeditionary-cyberspace-operations.html){:target="_blank" rel="noopener"}

> Cyberspace operations now officially has a physical dimension, meaning that the United States has official military doctrine about cyberattacks that also involve an actual human gaining physical access to a piece of computing infrastructure. A revised version of Joint Publication 3-12 Cyberspace Operations—published in December 2022 and while unclassified, is only available to those with DoD common access cards, according to a Joint Staff spokesperson—officially provides a definition for “expeditionary cyberspace operations,” which are “[c]yberspace operations that require the deployment of cyberspace forces within the physical domains.” [...] “Developing access to targets in or through cyberspace follows a process that [...]
