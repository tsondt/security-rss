Title: Philly Inquirer says Cuba ransomware gang's data leak claims are fake news
Date: 2023-05-24T20:26:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-24-philly-inquirer-says-cuba-ransomware-gangs-data-leak-claims-are-fake-news

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/24/philly_inquirer_cuba_ransomware/){:target="_blank" rel="noopener"}

> Now that's a Rocky relationship The Philadelphia Inquirer has punched back at the Cuba ransomware gang after the criminals leaked what they said were files stolen from the newspaper.... [...]
