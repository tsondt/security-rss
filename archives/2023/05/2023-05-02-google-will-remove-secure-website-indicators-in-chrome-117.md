Title: Google will remove secure website indicators in Chrome 117
Date: 2023-05-02T15:27:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-05-02-google-will-remove-secure-website-indicators-in-chrome-117

[Source](https://www.bleepingcomputer.com/news/google/google-will-remove-secure-website-indicators-in-chrome-117/){:target="_blank" rel="noopener"}

> Google announced today that the lock icon, long thought to be a sign of website security and trustworthiness, will soon be changed with a new icon that doesn't imply that a site is secure or should be trusted. [...]
