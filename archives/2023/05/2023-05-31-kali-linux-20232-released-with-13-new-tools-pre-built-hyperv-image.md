Title: Kali Linux 2023.2 released with 13 new tools, pre-built HyperV image
Date: 2023-05-31T15:55:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-05-31-kali-linux-20232-released-with-13-new-tools-pre-built-hyperv-image

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20232-released-with-13-new-tools-pre-built-hyperv-image/){:target="_blank" rel="noopener"}

> Kali Linux 2023.2, the second version of 2023, is now available with a pre-built Hyper-V image and thirteen new tools, including the Evilginx framework for stealing credentials and session cookies. [...]
