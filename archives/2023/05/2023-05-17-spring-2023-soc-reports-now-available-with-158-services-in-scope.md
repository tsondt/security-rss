Title: Spring 2023 SOC reports now available with 158 services in scope
Date: 2023-05-17T17:31:15+00:00
Author: Andrew Najjar
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC 1;AWS SOC 2;AWS SOC 3;AWS SOC Reports;Security Blog
Slug: 2023-05-17-spring-2023-soc-reports-now-available-with-158-services-in-scope

[Source](https://aws.amazon.com/blogs/security/spring-2023-soc-reports-now-available-with-158-services-in-scope/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we’re committed to providing our customers with continued assurance over the security, availability, confidentiality, and privacy of the AWS control environment. We’re proud to deliver the Spring 2023 System and Organization Controls (SOC) 1, 2 and 3 reports, which cover October 1, 2022, to March 31, 2023, to support your [...]
