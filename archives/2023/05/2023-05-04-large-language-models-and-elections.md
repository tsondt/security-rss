Title: Large Language Models and Elections
Date: 2023-05-04T10:45:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;deep fake;essays;transparency;voting
Slug: 2023-05-04-large-language-models-and-elections

[Source](https://www.schneier.com/blog/archives/2023/05/large-language-models-and-elections.html){:target="_blank" rel="noopener"}

> Earlier this week, the Republican National Committee released a video that it claims was “built entirely with AI imagery.” The content of the ad isn’t especially novel—a dystopian vision of America under a second term with President Joe Biden—but the deliberate emphasis on the technology used to create it stands out: It’s a “ Daisy ” moment for the 2020s. We should expect more of this kind of thing. The applications of AI to political advertising have not escaped campaigners, who are already “ pressure testing ” possible uses for the technology. In the 2024 presidential election campaign, you can [...]
