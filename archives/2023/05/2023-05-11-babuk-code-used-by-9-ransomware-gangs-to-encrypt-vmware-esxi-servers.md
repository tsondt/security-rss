Title: Babuk code used by 9 ransomware gangs to encrypt VMWare ESXi servers
Date: 2023-05-11T14:04:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-11-babuk-code-used-by-9-ransomware-gangs-to-encrypt-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/babuk-code-used-by-9-ransomware-gangs-to-encrypt-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> An increasing number of ransomware operations are adopting the leaked Babuk ransomware source code to create Linux encryptors targeting VMware ESXi servers. [...]
