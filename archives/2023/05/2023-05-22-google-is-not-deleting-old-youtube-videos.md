Title: Google Is Not Deleting Old YouTube Videos
Date: 2023-05-22T11:15:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Google;social media;videos
Slug: 2023-05-22-google-is-not-deleting-old-youtube-videos

[Source](https://www.schneier.com/blog/archives/2023/05/google-is-not-deleting-old-youtube-videos.html){:target="_blank" rel="noopener"}

> Google has backtracked on its plan to delete inactive YouTube videos—at least for now. Of course, it could change its mind anytime it wants. It would be nice if this would get people to think about the vulnerabilities inherent in letting a for-profit monopoly decide what of human creativity is worth saving. [...]
