Title: Barracuda warns of email gateways breached via zero-day flaw
Date: 2023-05-24T11:42:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-24-barracuda-warns-of-email-gateways-breached-via-zero-day-flaw

[Source](https://www.bleepingcomputer.com/news/security/barracuda-warns-of-email-gateways-breached-via-zero-day-flaw/){:target="_blank" rel="noopener"}

> Barracuda, a company known for its email and network security solutions, warned customers today that some of their Email Security Gateway (ESG) appliances were breached last week by targeting a now-patched zero-day vulnerability. [...]
