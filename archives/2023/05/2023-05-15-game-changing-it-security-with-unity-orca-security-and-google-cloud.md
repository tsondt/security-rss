Title: Game-changing IT security with Unity, Orca Security, and Google Cloud
Date: 2023-05-15T16:00:00+00:00
Author: Apurva Dave
Category: GCP Security
Tags: Security & Identity;Media & Entertainment;Partners
Slug: 2023-05-15-game-changing-it-security-with-unity-orca-security-and-google-cloud

[Source](https://cloud.google.com/blog/topics/partners/game-changing-it-security-with-unity-orca-security-and-google-cloud/){:target="_blank" rel="noopener"}

> Every month, more than 1 million creators worldwide use Unity’s expansive platform to develop games, create beautiful visual effects, and design everything from electric cars to skyscrapers. The company’s comprehensive suite of solutions makes it easier to create, run, and monetize 2D and 3D content. Unity has long recognized the importance of security not just to protect critical networks and sensitive information, but also to maintain an always-on, undistracted experience for its users. Every millisecond counts for the platform that is used to make 70% of the top 1,000 mobile games globally, and its infrastructure needs to be optimally resilient [...]
