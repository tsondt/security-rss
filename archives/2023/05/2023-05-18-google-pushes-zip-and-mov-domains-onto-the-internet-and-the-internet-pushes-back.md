Title: Google pushes .zip and .mov domains onto the Internet, and the Internet pushes back
Date: 2023-05-18T21:47:41+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security
Slug: 2023-05-18-google-pushes-zip-and-mov-domains-onto-the-internet-and-the-internet-pushes-back

[Source](https://arstechnica.com/?p=1940171){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson | Getty Images) A recent move by Google to populate the Internet with eight new top-level domains is prompting concerns that two of the additions could be a boon to online scammers who trick people into clicking on malicious links. Frequently abbreviated as TLD, a top-level domain is the rightmost segment of a domain name. In the early days of the Internet, they helped classify the purpose, geographic region, or operator of a given domain. The.com TLD, for instance, corresponded to sites run by commercial entities,.org was used for nonprofit organizations,.net for Internet or network entities,.edu [...]
