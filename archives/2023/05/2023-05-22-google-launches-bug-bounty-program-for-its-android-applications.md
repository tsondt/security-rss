Title: Google launches bug bounty program for its Android applications
Date: 2023-05-22T17:18:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-05-22-google-launches-bug-bounty-program-for-its-android-applications

[Source](https://www.bleepingcomputer.com/news/google/google-launches-bug-bounty-program-for-its-android-applications/){:target="_blank" rel="noopener"}

> Google has launched the Mobile Vulnerability Rewards Program (Mobile VRP), a new bug bounty program that will pay security researchers for flaws found in the company's Android applications. [...]
