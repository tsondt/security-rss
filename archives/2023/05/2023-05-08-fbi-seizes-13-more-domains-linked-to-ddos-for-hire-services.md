Title: FBI seizes 13 more domains linked to DDoS-for-hire services
Date: 2023-05-08T16:29:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-08-fbi-seizes-13-more-domains-linked-to-ddos-for-hire-services

[Source](https://www.bleepingcomputer.com/news/security/fbi-seizes-13-more-domains-linked-to-ddos-for-hire-services/){:target="_blank" rel="noopener"}

> The U.S. Justice Department announced today the seizure of 13 more domains linked to DDoS-for-hire platforms, also known as 'booter' or 'stressor' services. [...]
