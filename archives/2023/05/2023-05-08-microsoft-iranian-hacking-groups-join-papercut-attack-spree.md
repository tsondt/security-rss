Title: Microsoft: Iranian hacking groups join Papercut attack spree
Date: 2023-05-08T10:47:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-05-08-microsoft-iranian-hacking-groups-join-papercut-attack-spree

[Source](https://www.bleepingcomputer.com/news/security/microsoft-iranian-hacking-groups-join-papercut-attack-spree/){:target="_blank" rel="noopener"}

> Microsoft says Iranian state-backed hackers have joined the ongoing assault targeting vulnerable PaperCut MF/NG print management servers. [...]
