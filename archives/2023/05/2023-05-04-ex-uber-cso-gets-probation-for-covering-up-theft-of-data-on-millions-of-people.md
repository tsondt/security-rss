Title: Ex-Uber CSO gets probation for covering up theft of data on millions of people
Date: 2023-05-04T23:20:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-04-ex-uber-cso-gets-probation-for-covering-up-theft-of-data-on-millions-of-people

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/04/uber_cso_joe_sullivan_sentenced/){:target="_blank" rel="noopener"}

> Exec begged judge for leniency – and it worked Joe Sullivan won't serve any serious time behind bars for his role in covering up Uber's 2016 computer security breach and trying to pass off a ransom payment as a bug bounty.... [...]
