Title: UK's GDPR replacement could wipe out oversight of live facial recognition
Date: 2023-05-19T09:34:10+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-05-19-uks-gdpr-replacement-could-wipe-out-oversight-of-live-facial-recognition

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/19/dpib_2_surveillance_oversight/){:target="_blank" rel="noopener"}

> Question not whether UK police should use facial recog, but how, says surveillance chief Biometrics and surveillance camera commissioner Professor Fraser Sampson has warned that independent oversight of facial recognition is at risk just as the policing minister plans to "embed" it into the force.... [...]
