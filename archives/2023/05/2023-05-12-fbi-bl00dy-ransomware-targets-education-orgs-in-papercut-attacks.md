Title: FBI: Bl00dy Ransomware targets education orgs in PaperCut attacks
Date: 2023-05-12T12:51:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-05-12-fbi-bl00dy-ransomware-targets-education-orgs-in-papercut-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-bl00dy-ransomware-targets-education-orgs-in-papercut-attacks/){:target="_blank" rel="noopener"}

> The FBI and CISA issued a joint advisory to warn that the Bl00dy Ransomware gang is now also actively exploiting a PaperCut remote-code execution vulnerability to gain initial access to networks. [...]
