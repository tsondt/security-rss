Title: New hacking forum leaks data of 478,000 RaidForums members
Date: 2023-05-29T21:55:55-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-29-new-hacking-forum-leaks-data-of-478000-raidforums-members

[Source](https://www.bleepingcomputer.com/news/security/new-hacking-forum-leaks-data-of-478-000-raidforums-members/){:target="_blank" rel="noopener"}

> A database for the notorious RaidForums hacking forums has been leaked online, allowing threat actors and security researchers insight into the people who frequented the forum. [...]
