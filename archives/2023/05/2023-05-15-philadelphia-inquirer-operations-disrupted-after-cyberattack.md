Title: Philadelphia Inquirer operations disrupted after cyberattack
Date: 2023-05-15T18:28:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-15-philadelphia-inquirer-operations-disrupted-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/philadelphia-inquirer-operations-disrupted-after-cyberattack/){:target="_blank" rel="noopener"}

> The Philadelphia Inquirer daily newspaper is working on restoring systems impacted by what was described as a cyberattack that hit its network over the weekend. [...]
