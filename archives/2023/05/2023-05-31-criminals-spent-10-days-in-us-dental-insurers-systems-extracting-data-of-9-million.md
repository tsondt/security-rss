Title: Criminals spent 10 days in US dental insurer's systems extracting data of 9 million
Date: 2023-05-31T17:32:10+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-05-31-criminals-spent-10-days-in-us-dental-insurers-systems-extracting-data-of-9-million

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/31/mcna_breach/){:target="_blank" rel="noopener"}

> LockBit gang claimed 'trophy' of spilling low income families' details. Their parents must be proud The criminals who hit one of the biggest government-backed dental care and insurance providers in the US earlier this year hung about for 10 days while they extracted info on nearly 9 million people, including kids from poverty-stricken homes.... [...]
