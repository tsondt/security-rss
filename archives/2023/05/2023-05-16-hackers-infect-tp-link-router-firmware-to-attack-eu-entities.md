Title: Hackers infect TP-Link router firmware to attack EU entities
Date: 2023-05-16T12:25:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-16-hackers-infect-tp-link-router-firmware-to-attack-eu-entities

[Source](https://www.bleepingcomputer.com/news/security/hackers-infect-tp-link-router-firmware-to-attack-eu-entities/){:target="_blank" rel="noopener"}

> A Chinese state-sponsored hacking group named "Camaro Dragon" infects residential TP-Link routers with a custom "Horse Shell" malware used to attack European foreign affairs organizations. [...]
