Title: UK cops score legal win in EncroChat snooping op
Date: 2023-05-12T06:08:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-12-uk-cops-score-legal-win-in-encrochat-snooping-op

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/12/nca_encrochat_warrants/){:target="_blank" rel="noopener"}

> But tribunal punts on whether data was intercepted in transit The UK's National Crime Agency has partially won an important legal battle in a case that challenged the warrants used to obtain messages from cyber crook hangout EncroChat.... [...]
