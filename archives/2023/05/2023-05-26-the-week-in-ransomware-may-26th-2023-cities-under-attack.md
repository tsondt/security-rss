Title: The Week in Ransomware - May 26th 2023 - Cities Under Attack
Date: 2023-05-26T16:45:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-26-the-week-in-ransomware-may-26th-2023-cities-under-attack

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-26th-2023-cities-under-attack/){:target="_blank" rel="noopener"}

> Ransomware gangs continue to hammer local governments in attacks, taking down IT systems and disrupting cities' online services. [...]
