Title: Pentagon explosion hoax goes viral after verified Twitter accounts push
Date: 2023-05-22T14:14:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-22-pentagon-explosion-hoax-goes-viral-after-verified-twitter-accounts-push

[Source](https://www.bleepingcomputer.com/news/security/pentagon-explosion-hoax-goes-viral-after-verified-twitter-accounts-push/){:target="_blank" rel="noopener"}

> Highly realistic AI-generated images depicting an explosion near the Pentagon that went viral on Twitter caused the stock market to dip briefly earlier today. [...]
