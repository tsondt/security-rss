Title: Data loss costs are going up – and not just for those who choose to pay thieves
Date: 2023-05-02T10:41:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-02-data-loss-costs-are-going-up-and-not-just-for-those-who-choose-to-pay-thieves

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/02/data_breach_costs_rise/){:target="_blank" rel="noopener"}

> Ransoms, investigations, and breach-related lawsuits are hitting companies in the wallet, law firm says Data loss – particularly from ransomware attacks – has always been a costly proposition for enterprises. However, the price organizations have to pay is going up, not only in terms of the ransom demanded but also for the cost of investigating attacks and the lawsuits that increasingly follow in the wake of such breaches.... [...]
