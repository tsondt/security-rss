Title: Micro-Star International Signing Key Stolen
Date: 2023-05-15T11:18:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;ransomware;signatures;supply chain
Slug: 2023-05-15-micro-star-international-signing-key-stolen

[Source](https://www.schneier.com/blog/archives/2023/05/micro-star-international-signing-key-stolen.html){:target="_blank" rel="noopener"}

> Micro-Star International—aka MSI—had its UEFI signing key stolen last month. This raises the possibility that the leaked key could push out updates that would infect a computer’s most nether regions without triggering a warning. To make matters worse, Matrosov said, MSI doesn’t have an automated patching process the way Dell, HP, and many larger hardware makers do. Consequently, MSI doesn’t provide the same kind of key revocation capabilities. Delivering a signed payload isn’t as easy as all that. “Gaining the kind of control required to compromise a software build system is generally a non-trivial event that requires a great deal [...]
