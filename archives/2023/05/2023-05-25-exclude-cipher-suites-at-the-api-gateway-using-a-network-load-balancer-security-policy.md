Title: Exclude cipher suites at the API gateway using a Network Load Balancer security policy
Date: 2023-05-25T17:50:06+00:00
Author: Sid Singh
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Amazon API Gateway;Network Load Balancers;Security Blog
Slug: 2023-05-25-exclude-cipher-suites-at-the-api-gateway-using-a-network-load-balancer-security-policy

[Source](https://aws.amazon.com/blogs/security/exclude-cipher-suites-at-the-api-gateway-using-a-network-load-balancer-security-policy/){:target="_blank" rel="noopener"}

> In this blog post, we will show you how to use Amazon Elastic Load Balancing (ELB)—specifically a Network Load Balancer—to apply a more granular control on the cipher suites that are used between clients and servers when establishing an SSL/TLS connection with Amazon API Gateway. The solution uses virtual private cloud (VPC) endpoints (powered by [...]
