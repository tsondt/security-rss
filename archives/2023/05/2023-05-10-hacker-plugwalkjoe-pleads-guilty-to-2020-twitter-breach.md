Title: Hacker ‘PlugwalkJoe’ pleads guilty to 2020 Twitter breach
Date: 2023-05-10T09:48:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-05-10-hacker-plugwalkjoe-pleads-guilty-to-2020-twitter-breach

[Source](https://www.bleepingcomputer.com/news/security/hacker-plugwalkjoe-pleads-guilty-to-2020-twitter-breach/){:target="_blank" rel="noopener"}

> Joseph James O'Connor, aka 'PlugwalkJoke,' has pleaded guilty to multiple cybercrime offenses, including SIM swapping attacks, cyberstalking, computer hacking, and hijacking high-profile accounts on Twitter and TikTok. [...]
