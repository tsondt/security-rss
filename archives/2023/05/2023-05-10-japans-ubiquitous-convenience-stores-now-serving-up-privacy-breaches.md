Title: Japan's ubiquitous convenience stores now serving up privacy breaches
Date: 2023-05-10T03:31:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-05-10-japans-ubiquitous-convenience-stores-now-serving-up-privacy-breaches

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/10/fujitsu_japan_convenience_store_data_breach/){:target="_blank" rel="noopener"}

> Fujitsu in the frame for foul up with government document dispersal app Japan's minister for digital transformation and digital reform, Taro Kono, has apologized after a government app breached citizens' privacy.... [...]
