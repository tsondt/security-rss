Title: Researcher hijacks popular Packagist PHP packages to get a job
Date: 2023-05-03T11:30:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-05-03-researcher-hijacks-popular-packagist-php-packages-to-get-a-job

[Source](https://www.bleepingcomputer.com/news/security/researcher-hijacks-popular-packagist-php-packages-to-get-a-job/){:target="_blank" rel="noopener"}

> A researcher hijacked over a dozen Packagist packages—with some having been installed hundreds of millions of times over the course of their lifetime. The researcher reached out to BleepingComputer stating that by hijacking these packages he hopes to get a job. And, he seems pretty confident that this would work. [...]
