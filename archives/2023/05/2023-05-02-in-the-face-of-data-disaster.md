Title: In the face of data disaster
Date: 2023-05-02T14:14:07+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-05-02-in-the-face-of-data-disaster

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/02/in_the_face_of_data/){:target="_blank" rel="noopener"}

> How to recover from cyber attacks on Microsoft 365 Webinar Every organization needs a full set of data recovery tools. The sort that will get you back up and running quickly after a ransomware attack, outage, or accidental data deletion. And it's best to be prepared in advance rather than deal with the data disaster face to face when it happens.... [...]
