Title: WordPress force installs critical Jetpack patch on 5 million sites
Date: 2023-05-30T18:01:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-30-wordpress-force-installs-critical-jetpack-patch-on-5-million-sites

[Source](https://www.bleepingcomputer.com/news/security/wordpress-force-installs-critical-jetpack-patch-on-5-million-sites/){:target="_blank" rel="noopener"}

> Automattic, the company behind the open-source WordPress content management system, has started force installing a security patch on millions of websites today to address a critical vulnerability in the Jetpack WordPress plug-in. [...]
