Title: Microsoft disarms push notification bombers with number matching in Authenticator
Date: 2023-05-09T19:45:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-09-microsoft-disarms-push-notification-bombers-with-number-matching-in-authenticator

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/09/microsoft_authenticator_number_matching/){:target="_blank" rel="noopener"}

> Mandatory measure against attackers who spam MFA folks into submission Microsoft is hoping to curb a growing threat to multi-factor authentication (MFA) by enforcing a number-matching step for those using Microsoft Authenticator push notifications when signing into services.... [...]
