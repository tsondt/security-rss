Title: Lazarus hackers target Windows IIS web servers for initial access
Date: 2023-05-29T09:00:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-29-lazarus-hackers-target-windows-iis-web-servers-for-initial-access

[Source](https://www.bleepingcomputer.com/news/security/lazarus-hackers-target-windows-iis-web-servers-for-initial-access/){:target="_blank" rel="noopener"}

> The notorious North Korean state-backed hackers, known as the Lazarus Group, are now targeting vulnerable Windows Internet Information Services (IIS) web servers to gain initial access to corporate networks. [...]
