Title: AI Hacking Village at DEF CON This Year
Date: 2023-05-08T15:29:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;disclosure;hacking
Slug: 2023-05-08-ai-hacking-village-at-def-con-this-year

[Source](https://www.schneier.com/blog/archives/2023/05/ai-hacking-village-at-def-con-this-year.html){:target="_blank" rel="noopener"}

> At DEF CON this year, Anthropic, Google, Hugging Face, Microsoft, NVIDIA, OpenAI and Stability AI will all open up their models for attack. The DEF CON event will rely on an evaluation platform developed by Scale AI, a California company that produces training for AI applications. Participants will be given laptops to use to attack the models. Any bugs discovered will be disclosed using industry-standard responsible disclosure practices. [...]
