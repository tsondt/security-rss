Title: Android phones are vulnerable to fingerprint brute-force attacks
Date: 2023-05-21T10:12:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-05-21-android-phones-are-vulnerable-to-fingerprint-brute-force-attacks

[Source](https://www.bleepingcomputer.com/news/security/android-phones-are-vulnerable-to-fingerprint-brute-force-attacks/){:target="_blank" rel="noopener"}

> Researchers at Tencent Labs and Zhejiang University have presented a new attack called 'BrutePrint,' which brute-forces fingerprints on modern smartphones to bypass user authentication and take control of the device. [...]
