Title: Upstart encryption app walks back privacy claims, pulls from stores after probe
Date: 2023-05-17T06:30:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-17-upstart-encryption-app-walks-back-privacy-claims-pulls-from-stores-after-probe

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/17/converso_e2ee_app/){:target="_blank" rel="noopener"}

> Try not leaving a database full of user info, chats, keys exposed, eh? A new-ish messaging service that claimed to put privacy first has pulled its end-to-end encryption claims from its website and its app from both the Apple and Google software stores after being called out online.... [...]
