Title: ‘Operation Magalenha’ targets credentials of 30 Portuguese banks
Date: 2023-05-25T07:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-25-operation-magalenha-targets-credentials-of-30-portuguese-banks

[Source](https://www.bleepingcomputer.com/news/security/operation-magalenha-targets-credentials-of-30-portuguese-banks/){:target="_blank" rel="noopener"}

> A Brazilian hacking group has been targeting thirty Portuguese government and private financial institutions since 2021 in a malicious campaign called 'Operation Magalenha.' [...]
