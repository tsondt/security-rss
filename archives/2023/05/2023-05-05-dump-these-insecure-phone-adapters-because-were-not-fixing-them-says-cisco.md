Title: Dump these insecure phone adapters because we're not fixing them, says Cisco
Date: 2023-05-05T21:04:23+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-05-dump-these-insecure-phone-adapters-because-were-not-fixing-them-says-cisco

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/05/cisco_phone_adapter_vulnerabilitty/){:target="_blank" rel="noopener"}

> Security hole ranks 9.8 out of 10 in severity, 0 out of 10 in patch availability There is a critical security flaw in a Cisco phone adapter, and the business technology giant says the only step to take is dumping the hardware and migrating to new kit.... [...]
