Title: Indiana, Iowa, and Tennessee Pass Comprehensive Privacy Laws
Date: 2023-05-24T11:23:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;laws;privacy
Slug: 2023-05-24-indiana-iowa-and-tennessee-pass-comprehensive-privacy-laws

[Source](https://www.schneier.com/blog/archives/2023/05/indiana-iowa-and-tennessee-pass-comprehensive-privacy-laws.html){:target="_blank" rel="noopener"}

> It’s been a big month for US data privacy. Indiana, Iowa, and Tennessee all passed state privacy laws, bringing the total number of states with a privacy law up to eight. No private right of action in any of those, which means it’s up to the states to enforce the laws. [...]
