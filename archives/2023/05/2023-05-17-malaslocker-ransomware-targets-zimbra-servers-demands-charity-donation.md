Title: MalasLocker ransomware targets Zimbra servers, demands charity donation
Date: 2023-05-17T18:13:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-17-malaslocker-ransomware-targets-zimbra-servers-demands-charity-donation

[Source](https://www.bleepingcomputer.com/news/security/malaslocker-ransomware-targets-zimbra-servers-demands-charity-donation/){:target="_blank" rel="noopener"}

> A new ransomware operation is hacking Zimbra servers to steal emails and encrypt files. However, instead of demanding a ransom payment, the threat actors claim to require a donation to charity to provide an encryptor and prevent data leaking. [...]
