Title: 90+ orgs tell Slack to stop slacking when it comes to full encryption
Date: 2023-05-30T22:53:18+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-30-90-orgs-tell-slack-to-stop-slacking-when-it-comes-to-full-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/30/slack_e2ee_protest/){:target="_blank" rel="noopener"}

> Protests planned for Wednesday in San Francisco and Denver A coalition of 90-plus groups, including Fight for the Future and Mozilla, will descend upon Slack's offices in San Francisco and Denver on Wednesday to ask on the collaboration app to protect users' conversations via end-to-end encryption (E2EE).... [...]
