Title: Russian hackers use WinRAR to wipe Ukraine state agency’s data
Date: 2023-05-03T16:41:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-03-russian-hackers-use-winrar-to-wipe-ukraine-state-agencys-data

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-use-winrar-to-wipe-ukraine-state-agencys-data/){:target="_blank" rel="noopener"}

> The Russian 'Sandworm' hacking group has been linked to an attack on Ukrainian state networks where WinRar was used to destroy data on government devices. [...]
