Title: Get custom data into Amazon Security Lake through ingesting Azure activity logs
Date: 2023-05-30T20:18:55+00:00
Author: Adam Plotzker
Category: AWS Security
Tags: Amazon Security Lake;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon Kinesis Streams;Automation;AWS Glue;Security Blog
Slug: 2023-05-30-get-custom-data-into-amazon-security-lake-through-ingesting-azure-activity-logs

[Source](https://aws.amazon.com/blogs/security/get-custom-data-into-amazon-security-lake-through-ingesting-azure-activity-logs/){:target="_blank" rel="noopener"}

> Amazon Security Lake automatically centralizes security data from both cloud and on-premises sources into a purpose-built data lake stored on a particular AWS delegated administrator account for Amazon Security Lake. In this blog post, I will show you how to configure your Amazon Security Lake solution with cloud activity data from Microsoft Azure Monitor activity [...]
