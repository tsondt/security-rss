Title: Your guide to the threat detection and incident response track at re:Inforce 2023
Date: 2023-05-18T19:51:16+00:00
Author: Celeste Bishop
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Events;Foundational (100);Security, Identity, & Compliance;Incident response;Live Events;re:Inforce;re:Inforce 2023;Security Blog;threat detection
Slug: 2023-05-18-your-guide-to-the-threat-detection-and-incident-response-track-at-reinforce-2023

[Source](https://aws.amazon.com/blogs/security/your-guide-to-the-threat-detection-and-incident-response-track-at-reinforce-2023/){:target="_blank" rel="noopener"}

> A full conference pass is $1,099. Register today with the code secure150off to receive a limited time $150 discount, while supplies last. AWS re:Inforce is back, and we can’t wait to welcome security builders to Anaheim, CA, on June 13 and 14. AWS re:Inforce is a security learning conference where you can gain skills and confidence [...]
