Title: Google brings dark web monitoring to all U.S. Gmail users
Date: 2023-05-10T18:24:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-05-10-google-brings-dark-web-monitoring-to-all-us-gmail-users

[Source](https://www.bleepingcomputer.com/news/google/google-brings-dark-web-monitoring-to-all-us-gmail-users/){:target="_blank" rel="noopener"}

> Google announced today that all Gmail users in the United States will soon be able to use the dark web report security feature to discover if their email address has been found on the dark web. [...]
