Title: Twitter admits 'security incident' made private Circles not so much
Date: 2023-05-08T21:18:24+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-08-twitter-admits-security-incident-made-private-circles-not-so-much

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/08/twitter_circle_security_incident/){:target="_blank" rel="noopener"}

> Perhaps one of the thousands of people laid off from the biz could have fixed it, just a thought Twitter has finally admitted a "security incident" caused some users' semi-private Twitter Circle tweets to show up on others' timelines.... [...]
