Title: Hot Pixels attack checks CPU temp, power changes to steal data
Date: 2023-05-27T10:08:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-27-hot-pixels-attack-checks-cpu-temp-power-changes-to-steal-data

[Source](https://www.bleepingcomputer.com/news/security/hot-pixels-attack-checks-cpu-temp-power-changes-to-steal-data/){:target="_blank" rel="noopener"}

> A team of researchers at Georgia Tech, the University of Michigan, and Ruhr University Bochum have developed a novel attack called "Hot Pixels," which can retrieve pixels from the content displayed in the target's browser and infer the navigation history. [...]
