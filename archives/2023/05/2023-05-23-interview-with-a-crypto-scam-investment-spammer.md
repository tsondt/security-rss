Title: Interview With a Crypto Scam Investment Spammer
Date: 2023-05-23T00:15:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;Web Fraud 2.0;Arkose Labs;Constella Intelligence;domaintools;edgard011012@gmail.com;Lolzteam;Mastodon;Mondi Group;moonxtrade;msr-sergey2015@yandex.ru;quot.pw;Renaud Chaput;Sergey Proshutinskiy;TGM;twitter;ципа
Slug: 2023-05-23-interview-with-a-crypto-scam-investment-spammer

[Source](https://krebsonsecurity.com/2023/05/interview-with-a-crypto-scam-investment-spammer/){:target="_blank" rel="noopener"}

> Social networks are constantly battling inauthentic bot accounts that send direct messages to users promoting scam cryptocurrency investment platforms. What follows is an interview with a Russian hacker responsible for a series of aggressive crypto spam campaigns that recently prompted several large Mastodon communities to temporarily halt new registrations. According to the hacker, their spam software has been in private use until the last few weeks, when it was released as open source code. Renaud Chaput is a freelance programmer working on modernizing and scaling the Mastodon project infrastructure — including joinmastodon.org, mastodon.online, and mastodon.social. Chaput said that on May [...]
