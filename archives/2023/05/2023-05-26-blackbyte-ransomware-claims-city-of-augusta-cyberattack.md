Title: BlackByte ransomware claims City of Augusta cyberattack
Date: 2023-05-26T09:27:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-26-blackbyte-ransomware-claims-city-of-augusta-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/blackbyte-ransomware-claims-city-of-augusta-cyberattack/){:target="_blank" rel="noopener"}

> The city of Augusta in Georgia, U.S., has confirmed that the most recent IT system outage was caused by unauthorized access to its network. [...]
