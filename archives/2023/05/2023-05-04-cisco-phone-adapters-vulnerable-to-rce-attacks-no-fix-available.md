Title: Cisco phone adapters vulnerable to RCE attacks, no fix available
Date: 2023-05-04T13:28:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-05-04-cisco-phone-adapters-vulnerable-to-rce-attacks-no-fix-available

[Source](https://www.bleepingcomputer.com/news/security/cisco-phone-adapters-vulnerable-to-rce-attacks-no-fix-available/){:target="_blank" rel="noopener"}

> Cisco has disclosed a vulnerability in the web-based management interface of Cisco SPA112 2-Port Phone Adapters, allowing an unauthenticated, remote attacker to execute arbitrary code on the devices. [...]
