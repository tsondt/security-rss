Title: Microsoft pulls Defender update fixing Windows LSA Protection bug
Date: 2023-05-17T14:12:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-05-17-microsoft-pulls-defender-update-fixing-windows-lsa-protection-bug

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-pulls-defender-update-fixing-windows-lsa-protection-bug/){:target="_blank" rel="noopener"}

> Microsoft has pulled a recent Microsoft Defender update that was supposed to fix a known issue triggering persistent restart alerts and Windows Security warnings that Local Security Authority (LSA) Protection is off. [...]
