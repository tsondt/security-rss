Title: Brave unveils new "Forgetful Browsing" anti-tracking feature
Date: 2023-05-14T10:14:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-14-brave-unveils-new-forgetful-browsing-anti-tracking-feature

[Source](https://www.bleepingcomputer.com/news/security/brave-unveils-new-forgetful-browsing-anti-tracking-feature/){:target="_blank" rel="noopener"}

> The privacy-focused Brave Browser is introducing a new "Forgetful Browsing" feature that prevents sites from re-identifying you on subsequent visits. [...]
