Title: An important system on project [REDACTED] was all [REDACTED] up
Date: 2023-05-15T07:30:06+00:00
Author: Matthew JC Powell
Category: The Register
Tags: 
Slug: 2023-05-15-an-important-system-on-project-redacted-was-all-redacted-up

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/15/who_me/){:target="_blank" rel="noopener"}

> Luckily, [REDACTED] was there to save the day Who Me? Welcome once again to the horrors of Monday, dear reader. But fear not – The Register is here to cushion the blow of the working week's resumption with a instalment of Who, Me?, our reader-contributed stories of tech gone awry.... [...]
