Title: Why Microsoft just patched a patch that squashed an under-attack Outlook bug
Date: 2023-05-12T23:17:41+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-12-why-microsoft-just-patched-a-patch-that-squashed-an-under-attack-outlook-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/12/microsoft_patches_second_flaw/){:target="_blank" rel="noopener"}

> Let's take a quick dive into Windows API Microsoft in March fixed an interesting security hole in Outlook that was exploited by miscreants to leak victims' Windows credentials. This week the IT giant fixed that fix as part of its monthly Patch Tuesday update.... [...]
