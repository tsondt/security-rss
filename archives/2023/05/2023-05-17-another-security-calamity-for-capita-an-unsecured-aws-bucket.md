Title: Another security calamity for Capita: An unsecured AWS bucket
Date: 2023-05-17T12:48:21+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-05-17-another-security-calamity-for-capita-an-unsecured-aws-bucket

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/17/another_security_calamity_for_capita/){:target="_blank" rel="noopener"}

> Colchester City Council says it and others caught up in new incident, reckons benefits data of local citizens exposed Capita is facing criticism about its security hygiene on a new front after an Amazon cloud bucket containing benefits data on residents in a south east England city council was left exposed to the public web.... [...]
