Title: Introducing Organization Restrictions, a new way to keep threat actors out
Date: 2023-05-04T16:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Networking;Security & Identity
Slug: 2023-05-04-introducing-organization-restrictions-a-new-way-to-keep-threat-actors-out

[Source](https://cloud.google.com/blog/products/identity-security/introducing-organization-restrictions-a-new-way-to-keep-threat-actors-out/){:target="_blank" rel="noopener"}

> In Google Cloud, IAM Policies provide administrators with fine-grained control over who can use resources within their Google Cloud organization. With Organization Restrictions, a new generally available Google Cloud security control, administrators can restrict users’ access to only resources and data in specifically authorized Google Cloud organizations. It does this by restricting Google Cloud organization access to traffic originating from corporate managed devices. Mitigating data exfiltration risks with Organization Restrictions Even for well-defended and managed Cloud organizations, there are multiple ways an attacker might seek to exfiltrate data. For example, a threat actor could create a rogue organization and grant [...]
