Title: AWS completes the 2023 Cyber Essentials Plus certification and NHS Data Security and Protection Toolkit assessment
Date: 2023-05-16T21:15:37+00:00
Author: Tariro Dongo
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS security;cybersecurity;Security Blog;United Kingdom
Slug: 2023-05-16-aws-completes-the-2023-cyber-essentials-plus-certification-and-nhs-data-security-and-protection-toolkit-assessment

[Source](https://aws.amazon.com/blogs/security/aws-completes-the-2023-cyber-essentials-plus-certification-and-nhs-data-security-and-protection-toolkit-assessment/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the successful completion of the United Kingdom Cyber Essentials Plus certification and the National Health Service Data Security and Protection Toolkit (NHS DSPT) assessment. The Cyber Essentials Plus certificate and NHS DSPT assessment are valid for one year until March 28, 2024, and June 30, 2024, respectively. [...]
