Title: A sneak peek at the identity and access management sessions for AWS re:Inforce 2023
Date: 2023-05-08T22:45:39+00:00
Author: Marc von Mandel
Category: AWS Security
Tags: Announcements;AWS Identity and Access Management (IAM);AWS re:Inforce;Events;Foundational (100);Security, Identity, & Compliance;Live Events;re:Inforce 2023;Security Blog
Slug: 2023-05-08-a-sneak-peek-at-the-identity-and-access-management-sessions-for-aws-reinforce-2023

[Source](https://aws.amazon.com/blogs/security/a-sneak-peek-at-the-identity-and-access-management-sessions-for-aws-reinforce-2023/){:target="_blank" rel="noopener"}

> A full conference pass is $1,099. Register today with the code secure150off to receive a limited time $150 discount, while supplies last. AWS re:Inforce 2023 is fast approaching, and this post can help you plan your agenda with a look at the sessions in the identity and access management track. AWS re:Inforce is a learning [...]
