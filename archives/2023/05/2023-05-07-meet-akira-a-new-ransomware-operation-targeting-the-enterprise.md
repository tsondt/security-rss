Title: Meet Akira — A new ransomware operation targeting the enterprise
Date: 2023-05-07T09:16:08-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-07-meet-akira-a-new-ransomware-operation-targeting-the-enterprise

[Source](https://www.bleepingcomputer.com/news/security/meet-akira-a-new-ransomware-operation-targeting-the-enterprise/){:target="_blank" rel="noopener"}

> The new Akira ransomware operation has slowly been building a list of victims as they breach corporate networks worldwide, encrypt files, and then demand million-dollar ransoms. [...]
