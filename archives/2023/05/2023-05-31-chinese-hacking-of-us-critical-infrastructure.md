Title: Chinese Hacking of US Critical Infrastructure
Date: 2023-05-31T14:53:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;cyberattack;cyberespionage;espionage;hacking;infrastructure;reports
Slug: 2023-05-31-chinese-hacking-of-us-critical-infrastructure

[Source](https://www.schneier.com/blog/archives/2023/05/chinese-hacking-of-us-critical-infrastructure.html){:target="_blank" rel="noopener"}

> Everyone is writing about an interagency and international report on Chinese hacking of US critical infrastructure. Lots of interesting details about how the group, called Volt Typhoon, accesses target networks and evades detection. [...]
