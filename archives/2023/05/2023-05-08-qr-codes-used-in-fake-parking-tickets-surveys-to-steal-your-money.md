Title: QR codes used in fake parking tickets, surveys to steal your money
Date: 2023-05-08T11:32:41-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-05-08-qr-codes-used-in-fake-parking-tickets-surveys-to-steal-your-money

[Source](https://www.bleepingcomputer.com/news/security/qr-codes-used-in-fake-parking-tickets-surveys-to-steal-your-money/){:target="_blank" rel="noopener"}

> As QR codes continue to be heavily used by legitimate organizations—from Super Bowl advertisements to enforcing parking fees and fines, scammers have crept in to abuse the very technology for their nefarious purposes. A woman in Singapore reportedly lost $20,000 after using a QR code to fill out a "survey" at a bubble tea shop. [...]
