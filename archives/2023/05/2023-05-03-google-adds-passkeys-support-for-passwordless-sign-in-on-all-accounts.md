Title: Google adds passkeys support for passwordless sign-in on all accounts
Date: 2023-05-03T08:57:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-03-google-adds-passkeys-support-for-passwordless-sign-in-on-all-accounts

[Source](https://www.bleepingcomputer.com/news/security/google-adds-passkeys-support-for-passwordless-sign-in-on-all-accounts/){:target="_blank" rel="noopener"}

> Google is rolling out support for passkeys for Google Accounts across all services and platforms, allowing users to sign into their Google accounts without entering a password or using 2-Step Verification (2SV) when logging in. [...]
