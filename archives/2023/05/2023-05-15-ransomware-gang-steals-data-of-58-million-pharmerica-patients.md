Title: Ransomware gang steals data of 5.8 million PharMerica patients
Date: 2023-05-15T14:10:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-05-15-ransomware-gang-steals-data-of-58-million-pharmerica-patients

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-steals-data-of-58-million-pharmerica-patients/){:target="_blank" rel="noopener"}

> Pharmacy services provider PharMerica has disclosed a massive data breach impacting over 5.8 million patients, exposing their medical data to hackers. [...]
