Title: North Korean hackers breached major hospital in Seoul to steal data
Date: 2023-05-10T17:16:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-05-10-north-korean-hackers-breached-major-hospital-in-seoul-to-steal-data

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-breached-major-hospital-in-seoul-to-steal-data/){:target="_blank" rel="noopener"}

> The Korean National Police Agency (KNPA) warned that North Korean hackers had breached the network of one of the country's largest hospitals, Seoul National University Hospital (SNUH), to steal sensitive medical information and personal details. [...]
