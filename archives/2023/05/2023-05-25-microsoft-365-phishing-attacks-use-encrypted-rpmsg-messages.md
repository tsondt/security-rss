Title: Microsoft 365 phishing attacks use encrypted RPMSG messages
Date: 2023-05-25T13:12:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-25-microsoft-365-phishing-attacks-use-encrypted-rpmsg-messages

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-phishing-attacks-use-encrypted-rpmsg-messages/){:target="_blank" rel="noopener"}

> Attackers are now using encrypted RPMSG attachments sent via compromised Microsoft 365 accounts to steal Microsoft credentials in targeted phishing attacks designed to evade detection by email security gateways. [...]
