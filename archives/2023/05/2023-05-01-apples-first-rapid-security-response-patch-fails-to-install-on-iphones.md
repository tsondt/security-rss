Title: Apple’s first Rapid Security Response patch fails to install on iPhones
Date: 2023-05-01T16:17:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-05-01-apples-first-rapid-security-response-patch-fails-to-install-on-iphones

[Source](https://www.bleepingcomputer.com/news/apple/apples-first-rapid-security-response-patch-fails-to-install-on-iphones/){:target="_blank" rel="noopener"}

> Apple has launched the first Rapid Security Response (RSR) patches for iOS 16.4.1 and macOS 13.3.1 devices, with some users having issues installing them on their iPhones. [...]
