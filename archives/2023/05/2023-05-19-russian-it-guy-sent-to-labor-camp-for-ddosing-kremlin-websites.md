Title: Russian IT guy sent to labor camp for DDoSing Kremlin websites
Date: 2023-05-19T20:14:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-19-russian-it-guy-sent-to-labor-camp-for-ddosing-kremlin-websites

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/19/russian_it_worker_ddos/){:target="_blank" rel="noopener"}

> Pro-Ukraine techie gets hard time A Russian IT worker accused of participating in pro-Ukraine denial of service attacks against Russian government websites has been sentenced to three years in a penal colony and ordered to pay 800,000 rubles (about $10,000).... [...]
