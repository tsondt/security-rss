Title: So the FBI 'persistently' abused its snoop powers. What's to worry about?
Date: 2023-05-25T14:30:08+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: 2023-05-25-so-the-fbi-persistently-abused-its-snoop-powers-whats-to-worry-about

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/25/register_kettle_fbi_podcast/){:target="_blank" rel="noopener"}

> When is warrantless surveillance warranted? Register Kettle If there's one thing that's more all the rage these days than this AI hype, it's warrantless spying by the Feds.... [...]
