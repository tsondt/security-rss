Title: LayerZero launches record-breaking $15M crypto bug bounty program
Date: 2023-05-18T10:31:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-18-layerzero-launches-record-breaking-15m-crypto-bug-bounty-program

[Source](https://www.bleepingcomputer.com/news/security/layerzero-launches-record-breaking-15m-crypto-bug-bounty-program/){:target="_blank" rel="noopener"}

> LayerZero Labs has launched a bug bounty on the Immunefi platform that offers a maximum reward of $15 million for critical smart contract and blockchain vulnerabilities, a figure that sets a new record in the crypto space. [...]
