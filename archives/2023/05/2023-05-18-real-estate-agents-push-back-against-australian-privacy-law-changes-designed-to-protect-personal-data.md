Title: Real estate agents push back against Australian privacy law changes designed to protect personal data
Date: 2023-05-18T15:00:04+00:00
Author: Paul Karp Chief political correspondent
Category: The Guardian
Tags: Privacy;Housing;Australia news;Data and computer security;Data protection;Renting;Business
Slug: 2023-05-18-real-estate-agents-push-back-against-australian-privacy-law-changes-designed-to-protect-personal-data

[Source](https://www.theguardian.com/australia-news/2023/may/19/real-estate-agents-push-back-against-australian-privacy-law-changes-designed-to-protect-personal-data){:target="_blank" rel="noopener"}

> Real Estate Institute of Australia president says additional layer of responsibility could force smaller agencies to close down Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Real estate agents are pushing back against proposed privacy law changes, saying small businesses should not face more red tape to keep customer and tenant data safe. The Real Estate Institute of Australia president, Hayden Groves, said an “additional layer of responsibility is really not necessary” on top of agents’ existing duties and increased regulatory risks could be “the [...]
