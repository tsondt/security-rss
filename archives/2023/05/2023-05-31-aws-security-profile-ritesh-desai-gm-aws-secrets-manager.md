Title: AWS Security Profile: Ritesh Desai, GM, AWS Secrets Manager
Date: 2023-05-31T18:52:36+00:00
Author: Roger Park
Category: AWS Security
Tags: AWS re:Inforce;Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Security Profile;AWS Security Profiles;Data protection;Live Events;re:Inforce 2023;Secrets management;Security Blog
Slug: 2023-05-31-aws-security-profile-ritesh-desai-gm-aws-secrets-manager

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-ritesh-desai-gm-aws-secrets-manager/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, we interview Amazon Web Services (AWS) thought leaders who help keep our customers safe and secure. This interview features Ritesh Desai, General Manager, AWS Secrets Manager, and re:Inforce 2023 session speaker, who shares thoughts on data protection, cloud security, secrets management, and more. What do you do in your [...]
