Title: New Buhti ransomware gang uses leaked Windows, Linux encryptors
Date: 2023-05-25T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-25-new-buhti-ransomware-gang-uses-leaked-windows-linux-encryptors

[Source](https://www.bleepingcomputer.com/news/security/new-buhti-ransomware-gang-uses-leaked-windows-linux-encryptors/){:target="_blank" rel="noopener"}

> A new ransomware operation named 'Buhti' uses the leaked code of the LockBit and Babuk ransomware families to target Windows and Linux systems, respectively. [...]
