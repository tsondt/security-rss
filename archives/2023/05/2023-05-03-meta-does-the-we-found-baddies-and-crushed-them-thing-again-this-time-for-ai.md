Title: Meta does the 'We found baddies and crushed them' thing again – this time for AI
Date: 2023-05-03T23:59:05+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2023-05-03-meta-does-the-we-found-baddies-and-crushed-them-thing-again-this-time-for-ai

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/03/facebook_removes_chatgpt_spoof_malware/){:target="_blank" rel="noopener"}

> Who would have thought crims would try using Facebook to fool people? Meta says it has shut down over 1,000 links related to ChatGPT that lead its users to malware, as criminals seek to profit from the current craze for generative AI.... [...]
