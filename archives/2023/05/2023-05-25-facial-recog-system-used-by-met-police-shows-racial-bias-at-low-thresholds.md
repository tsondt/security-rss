Title: Facial recog system used by Met Police shows racial bias at low thresholds
Date: 2023-05-25T10:34:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-05-25-facial-recog-system-used-by-met-police-shows-racial-bias-at-low-thresholds

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/25/facial_recognition_system_used_by/){:target="_blank" rel="noopener"}

> Tech used at King's Coronation employs higher thresholds on once-only watch-lists, Met tells MPs The UK Parliament has heard that a facial recognition system used by the Metropolitan police during the King’s Coronation can exhibit racial bias at certain thresholds.... [...]
