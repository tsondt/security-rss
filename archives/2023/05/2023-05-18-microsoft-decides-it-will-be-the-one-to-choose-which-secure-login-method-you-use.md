Title: Microsoft decides it will be the one to choose which secure login method you use
Date: 2023-05-18T17:32:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-18-microsoft-decides-it-will-be-the-one-to-choose-which-secure-login-method-you-use

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/18/microsoft_azure_system_authentication/){:target="_blank" rel="noopener"}

> Certificate-based authentication comes first and phones last Microsoft wants to take the decision of which multi-factor authentication (MFA) method to use out of the users' hands and into its own.... [...]
