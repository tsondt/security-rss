Title: Discord Admins Hacked by Malicious Bookmarks
Date: 2023-05-31T00:19:17+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Latest Warnings;Web Fraud 2.0;Aura Network;Berk Yilmaz;bookmarklets;Levatax;MetrixCoin;Nahmii;Nicholas Scavuzzo;Ocean Protocol
Slug: 2023-05-31-discord-admins-hacked-by-malicious-bookmarks

[Source](https://krebsonsecurity.com/2023/05/discord-admins-hacked-by-malicious-bookmarks/){:target="_blank" rel="noopener"}

> A number of Discord communities focused on cryptocurrency have been hacked this past month after their administrators were tricked into running malicious Javascript code disguised as a Web browser bookmark. This attack involves malicious Javascript that is added to one’s browser by dragging a component from a web page to one’s browser bookmarks. According to interviews with victims, several of the attacks began with an interview request from someone posing as a reporter for a crypto-focused news outlet online. Those who take the bait are sent a link to a Discord server that appears to be the official Discord of [...]
