Title: Microsoft Secure Boot Bug
Date: 2023-05-17T11:01:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Microsoft;vulnerabilities;zero-day
Slug: 2023-05-17-microsoft-secure-boot-bug

[Source](https://www.schneier.com/blog/archives/2023/05/microsoft-secure-boot-bug.html){:target="_blank" rel="noopener"}

> Microsoft is currently patching a zero-day Secure-Boot bug. The BlackLotus bootkit is the first-known real-world malware that can bypass Secure Boot protections, allowing for the execution of malicious code before your PC begins loading Windows and its many security protections. Secure Boot has been enabled by default for over a decade on most Windows PCs sold by companies like Dell, Lenovo, HP, Acer, and others. PCs running Windows 11 must have it enabled to meet the software’s system requirements. Microsoft says that the vulnerability can be exploited by an attacker with either physical access to a system or administrator rights [...]
