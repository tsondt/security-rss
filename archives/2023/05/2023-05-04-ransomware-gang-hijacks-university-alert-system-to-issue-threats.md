Title: Ransomware gang hijacks university alert system to issue threats
Date: 2023-05-04T11:21:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-05-04-ransomware-gang-hijacks-university-alert-system-to-issue-threats

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-hijacks-university-alert-system-to-issue-threats/){:target="_blank" rel="noopener"}

> The Avos ransomware gang hijacked Bluefield University's emergency broadcast system, "RamAlert," to send students and staff SMS texts and email alerts that their data was stolen and would soon be released. [...]
