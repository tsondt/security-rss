Title: New Linux kernel NetFilter flaw gives attackers root privileges
Date: 2023-05-09T12:49:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-05-09-new-linux-kernel-netfilter-flaw-gives-attackers-root-privileges

[Source](https://www.bleepingcomputer.com/news/security/new-linux-kernel-netfilter-flaw-gives-attackers-root-privileges/){:target="_blank" rel="noopener"}

> A new Linux NetFilter kernel flaw has been discovered, allowing unprivileged local users to escalate their privileges to root level, allowing complete control over a system. [...]
