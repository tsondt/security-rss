Title: Hackers start using double DLL sideloading to evade detection
Date: 2023-05-03T17:21:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-03-hackers-start-using-double-dll-sideloading-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/hackers-start-using-double-dll-sideloading-to-evade-detection/){:target="_blank" rel="noopener"}

> An APT hacking group known as "Dragon Breath," "Golden Eye Dog," or "APT-Q-27" is demonstrating a new trend of using several complex variations of the classic DLL sideloading technique to evade detection. [...]
