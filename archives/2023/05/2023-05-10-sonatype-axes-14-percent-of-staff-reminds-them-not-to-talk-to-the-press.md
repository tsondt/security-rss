Title: Sonatype axes 14 percent of staff, reminds them not to talk to the press
Date: 2023-05-10T20:38:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-10-sonatype-axes-14-percent-of-staff-reminds-them-not-to-talk-to-the-press

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/10/sonatype_job_cuts/){:target="_blank" rel="noopener"}

> Workers slam 'horrendous' handling of layoffs that left even 'engineering managers in the dark' Exclusive Software supply chain management biz Sonatype has laid off 14 percent of its global workforce, according to internal documents seen by The Register.... [...]
