Title: Dish confirms 300,000 people's data was exposed in February's attack
Date: 2023-05-23T16:43:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-05-23-dish-confirms-300000-peoples-data-was-exposed-in-februarys-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/23/dish_networks/){:target="_blank" rel="noopener"}

> But don't worry – we know it was deleted. Hmm. How would you know that? Dish Network has admitted that a February cybersecurity incident and associated multi-day outage led to the extraction of data on nearly 300,000 people, while also appearing to indirectly admit it may have paid cybercriminals to delete said data.... [...]
