Title: Microsoft enforces number matching to fight MFA fatigue attacks
Date: 2023-05-08T12:25:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-05-08-microsoft-enforces-number-matching-to-fight-mfa-fatigue-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-enforces-number-matching-to-fight-mfa-fatigue-attacks/){:target="_blank" rel="noopener"}

> Microsoft has started enforcing number matching in Microsoft Authenticator push notifications to fend off multi-factor authentication (MFA) fatigue attacks. [...]
