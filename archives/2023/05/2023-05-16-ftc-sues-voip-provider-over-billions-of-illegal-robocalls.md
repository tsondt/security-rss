Title: FTC sues VoIP provider over 'billions of illegal robocalls'
Date: 2023-05-16T00:27:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-16-ftc-sues-voip-provider-over-billions-of-illegal-robocalls

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/16/ftc_xcast_illegal_robocalls/){:target="_blank" rel="noopener"}

> XCast knew it was breaking the law and didn't hold back, watchdog says A VoIP provider was at the heart of billions of robocalls made over the past five years that broke a slew of US regulations, from enabling telemarketing scams to calling numbers on the National Do Not Call Registry, it is claimed.... [...]
