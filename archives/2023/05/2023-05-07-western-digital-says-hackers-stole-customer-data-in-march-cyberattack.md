Title: Western Digital says hackers stole customer data in March cyberattack
Date: 2023-05-07T12:10:45-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-07-western-digital-says-hackers-stole-customer-data-in-march-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/western-digital-says-hackers-stole-customer-data-in-march-cyberattack/){:target="_blank" rel="noopener"}

> Western Digital has taken its store offline and sent customers data breach notifications after confirming that hackers stole sensitive personal information in a March cyberattack. [...]
