Title: SolarWinds Detected Six Months Earlier
Date: 2023-05-03T10:13:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;breaches;hacking;intrusion detection;vulnerabilities
Slug: 2023-05-03-solarwinds-detected-six-months-earlier

[Source](https://www.schneier.com/blog/archives/2023/05/solarwinds-detected-six-months-earlier.html){:target="_blank" rel="noopener"}

> New reporting from Wired reveals that the Department of Justice detected the SolarWinds attack six months before Mandiant detected it in December 2020, but didn’t realize what it detected—and so ignored it. WIRED can now confirm that the operation was actually discovered by the DOJ six months earlier, in late May 2020­—but the scale and significance of the breach wasn’t immediately apparent. Suspicions were triggered when the department detected unusual traffic emanating from one of its servers that was running a trial version of the Orion software suite made by SolarWinds, according to sources familiar with the incident. The software, [...]
