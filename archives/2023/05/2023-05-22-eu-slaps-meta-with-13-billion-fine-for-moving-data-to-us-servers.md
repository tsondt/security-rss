Title: EU slaps Meta with $1.3 billion fine for moving data to US servers
Date: 2023-05-22T11:04:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Technology;Security
Slug: 2023-05-22-eu-slaps-meta-with-13-billion-fine-for-moving-data-to-us-servers

[Source](https://www.bleepingcomputer.com/news/technology/eu-slaps-meta-with-13-billion-fine-for-moving-data-to-us-servers/){:target="_blank" rel="noopener"}

> The Irish Data Protection Commission (DPC) has announced a $1.3 billion fine on Facebook after claiming that the company violated Article 46(1) of the GDPR (General Data Protection Regulation). [...]
