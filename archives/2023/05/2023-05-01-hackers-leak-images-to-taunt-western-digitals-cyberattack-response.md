Title: Hackers leak images to taunt Western Digital's cyberattack response
Date: 2023-05-01T08:28:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-01-hackers-leak-images-to-taunt-western-digitals-cyberattack-response

[Source](https://www.bleepingcomputer.com/news/security/hackers-leak-images-to-taunt-western-digitals-cyberattack-response/){:target="_blank" rel="noopener"}

> The ALPHV ransomware operation, aka BlackCat, has published screenshots of internal emails and video conferences stolen from Western Digital, indicating they likely had continued access to the company's systems even as the company responded to the breach. [...]
