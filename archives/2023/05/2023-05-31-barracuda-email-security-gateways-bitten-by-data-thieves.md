Title: Barracuda Email Security Gateways bitten by data thieves
Date: 2023-05-31T18:15:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-31-barracuda-email-security-gateways-bitten-by-data-thieves

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/31/datastealing_email_attack_bites_barracuda/){:target="_blank" rel="noopener"}

> Act now: Sea-themed backdoor malware injected via.tar-based hole A critical remote command injection vulnerability in some Barracuda Network devices that the vendor patched 11 days ago has been exploited by miscreants – for at least the past seven months.... [...]
