Title: Twitter admits to ‘security incident’ involving Circles tweets
Date: 2023-05-06T09:53:56+00:00
Author: Alex Hern UK technology editor
Category: The Guardian
Tags: Twitter;Technology;Media;Internet;Blogging;Digital media;Privacy;Data and computer security;Data protection
Slug: 2023-05-06-twitter-admits-to-security-incident-involving-circles-tweets

[Source](https://www.theguardian.com/technology/2023/may/06/twitter-admits-to-security-incident-involving-circles-tweets){:target="_blank" rel="noopener"}

> Feature allows users to set a list of friends and post tweets that only they are supposed to be able to read A privacy breach at Twitter published tweets that were never supposed to be seen by anyone but the poster’s closest friends to the site at large, the company has admitted after weeks of stonewalling reports. The site’s Circles feature allows users to set an exclusive list of friends and post tweets that only they can read. Similar to Instagram’s Close Friends setting, it allows users to share private thoughts, explicit images or unprofessional statements without risking sharing them [...]
