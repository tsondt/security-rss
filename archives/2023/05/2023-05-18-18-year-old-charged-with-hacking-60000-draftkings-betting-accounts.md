Title: 18-year-old charged with hacking 60,000 DraftKings betting accounts
Date: 2023-05-18T12:59:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-18-18-year-old-charged-with-hacking-60000-draftkings-betting-accounts

[Source](https://www.bleepingcomputer.com/news/security/18-year-old-charged-with-hacking-60-000-draftkings-betting-accounts/){:target="_blank" rel="noopener"}

> The Department of Justice revealed today that an 18-year-old man named Joseph Garrison from Wisconsin had been charged with hacking into the accounts of around 60,000 users of the DraftKings sports betting website in November 2022. [...]
