Title: Cybersecurity firm Dragos discloses cybersecurity incident, extortion attempt
Date: 2023-05-10T11:48:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-10-cybersecurity-firm-dragos-discloses-cybersecurity-incident-extortion-attempt

[Source](https://www.bleepingcomputer.com/news/security/cybersecurity-firm-dragos-discloses-cybersecurity-incident-extortion-attempt/){:target="_blank" rel="noopener"}

> Industrial cybersecurity company Dragos today disclosed what it describes as a "cybersecurity event" after a known cybercrime gang attempted to breach its defenses and infiltrate the internal network to encrypt devices. [...]
