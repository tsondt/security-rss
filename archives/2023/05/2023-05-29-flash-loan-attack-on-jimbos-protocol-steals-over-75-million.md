Title: Flash loan attack on Jimbos Protocol steals over $7.5 million
Date: 2023-05-29T11:20:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-29-flash-loan-attack-on-jimbos-protocol-steals-over-75-million

[Source](https://www.bleepingcomputer.com/news/security/flash-loan-attack-on-jimbos-protocol-steals-over-75-million/){:target="_blank" rel="noopener"}

> Jimbos Protocol, an Arbitrum-based DeFi project, has suffered a flash loan attack that resulted in the loss of more than of 4000 ETH tokens, currently valued at over $7,500,000. [...]
