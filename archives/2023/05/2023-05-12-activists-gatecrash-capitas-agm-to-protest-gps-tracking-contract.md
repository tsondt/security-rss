Title: Activists gatecrash Capita's AGM to protest GPS tracking contract
Date: 2023-05-12T10:36:15+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-05-12-activists-gatecrash-capitas-agm-to-protest-gps-tracking-contract

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/12/activists_gatecrash_capitas_agm_to/){:target="_blank" rel="noopener"}

> Outsourcer asked to take 'principled stance' We hear Privacy International and a few other campaign groups set up camp outside Capita's AGM in London yesterday protesting Capita's involvement as an outsourcer in a UK government GPS tracking contract.... [...]
