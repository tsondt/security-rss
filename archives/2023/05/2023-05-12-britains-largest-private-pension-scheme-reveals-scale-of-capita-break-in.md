Title: Britain's largest private pension scheme reveals scale of Capita break-in
Date: 2023-05-12T16:59:14+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-05-12-britains-largest-private-pension-scheme-reveals-scale-of-capita-break-in

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/12/uks_largest_private_pension_scheme/){:target="_blank" rel="noopener"}

> USS says burgled biz reckons data on 470,000 'active, deferred and retired' members may have been accessed Universities Superannuation Scheme, the UK’s largest private pension provider, says Capita has warned that details of almost half a million members were held on servers accessed during the recent breach.... [...]
