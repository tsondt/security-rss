Title: Two Microsoft Windows bugs under attack, one in Secure Boot with a manual fix
Date: 2023-05-09T23:15:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-09-two-microsoft-windows-bugs-under-attack-one-in-secure-boot-with-a-manual-fix

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/09/microsoft_may_patch_tuesday/){:target="_blank" rel="noopener"}

> On the plus side, this month's update batch is a bit smaller than usual Patch Tuesday May's Patch Tuesday brings some good and some bad news, and if you're a glass-half-full type, you'd lead off with Microsoft's relatively low number of security fixes: a mere 38.... [...]
