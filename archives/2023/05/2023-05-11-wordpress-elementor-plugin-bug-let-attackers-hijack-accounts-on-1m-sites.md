Title: WordPress Elementor plugin bug let attackers hijack accounts on 1M sites
Date: 2023-05-11T12:59:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-11-wordpress-elementor-plugin-bug-let-attackers-hijack-accounts-on-1m-sites

[Source](https://www.bleepingcomputer.com/news/security/wordpress-elementor-plugin-bug-let-attackers-hijack-accounts-on-1m-sites/){:target="_blank" rel="noopener"}

> One of WordPress's most popular Elementor plugins, "Essential Addons for Elementor," was found to be vulnerable to an unauthenticated privilege escalation that could allow remote attacks to gain administrator rights on the site. [...]
