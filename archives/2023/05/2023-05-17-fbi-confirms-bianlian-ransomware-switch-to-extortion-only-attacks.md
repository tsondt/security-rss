Title: FBI confirms BianLian ransomware switch to extortion only attacks
Date: 2023-05-17T08:45:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-17-fbi-confirms-bianlian-ransomware-switch-to-extortion-only-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-confirms-bianlian-ransomware-switch-to-extortion-only-attacks/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI), Cybersecurity and Infrastructure Security Agency (CISA), and Australian Cyber Security Centre (ACSC) have published a joint advisory to inform organizations of the latest tactics, techniques, and procedures (TTPs) and known indicators of compromise (IOCs) of the BianLian ransomware group. [...]
