Title: Upcoming Speaking Engagements
Date: 2023-05-14T16:05:20+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2023-05-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2023/05/upcoming-speaking-engagements-30.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at IT-S Now 2023 in Vienna, Austria, on June 2, 2023 at 8:30 AM CEST. The list is maintained on this page. [...]
