Title: CISA warns govt agencies of recently patched Barracuda zero-day
Date: 2023-05-27T12:14:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-27-cisa-warns-govt-agencies-of-recently-patched-barracuda-zero-day

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-govt-agencies-of-recently-patched-barracuda-zero-day/){:target="_blank" rel="noopener"}

> CISA warned of a recently patched zero-day vulnerability exploited last week to hack into Barracuda Email Security Gateway (ESG) appliances. [...]
