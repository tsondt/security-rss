Title: 3 new ways to authorize users to your private workloads on Cloud Run
Date: 2023-05-03T16:00:00+00:00
Author: Xiaowen Xin
Category: GCP Security
Tags: Security & Identity;Developers & Practitioners;Serverless
Slug: 2023-05-03-3-new-ways-to-authorize-users-to-your-private-workloads-on-cloud-run

[Source](https://cloud.google.com/blog/products/serverless/cloud-run-supports-new-authorization-mechanisms/){:target="_blank" rel="noopener"}

> More and more organizations are building applications on Cloud Run, a fully managed compute platform that lets you run containerized applications on top of Google’s infrastructure. Think web applications, real-time dashboards, APIs, microservices, batch data processing, testing and monitoring tools, data science inference models, and more. Today, we're excited to announce that it's easier than ever to build internal apps on Cloud Run. In this post, we'll introduce three common design patterns and what's new in Cloud Run to help implement these patterns. Internal Web Apps - enabled by the GA launch of Identity-Aware Proxy Internal APIs - enabled by [...]
