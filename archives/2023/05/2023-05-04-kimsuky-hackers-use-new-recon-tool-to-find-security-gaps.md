Title: Kimsuky hackers use new recon tool to find security gaps
Date: 2023-05-04T17:40:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-04-kimsuky-hackers-use-new-recon-tool-to-find-security-gaps

[Source](https://www.bleepingcomputer.com/news/security/kimsuky-hackers-use-new-recon-tool-to-find-security-gaps/){:target="_blank" rel="noopener"}

> The North Korean Kimsuky hacking group has been observed employing a new version of its reconnaissance malware, now called 'ReconShark,' in a cyberespionage campaign with a global reach. [...]
