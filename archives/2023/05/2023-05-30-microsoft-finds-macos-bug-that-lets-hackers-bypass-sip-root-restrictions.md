Title: Microsoft finds macOS bug that lets hackers bypass SIP root restrictions
Date: 2023-05-30T15:20:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple;Microsoft
Slug: 2023-05-30-microsoft-finds-macos-bug-that-lets-hackers-bypass-sip-root-restrictions

[Source](https://www.bleepingcomputer.com/news/security/microsoft-finds-macos-bug-that-lets-hackers-bypass-sip-root-restrictions/){:target="_blank" rel="noopener"}

> Apple has recently addressed a vulnerability that lets attackers with root privileges bypass System Integrity Protection (SIP) to install "undeletable" malware and access the victim's private data by circumventing Transparency, Consent, and Control (TCC) security checks. [...]
