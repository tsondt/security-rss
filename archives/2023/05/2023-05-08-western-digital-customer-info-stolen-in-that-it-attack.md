Title: Western Digital: Customer info stolen in that IT attack
Date: 2023-05-08T23:01:30+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-08-western-digital-customer-info-stolen-in-that-it-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/08/western_digital_customer_data/){:target="_blank" rel="noopener"}

> Hard times for buyers of these hard drives Customer information was stolen from the IT systems of Western Digital in that March IT security breach, forcing the storage manufacturer to shut down its online store until at least next week.... [...]
