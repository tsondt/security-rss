Title: Ted Chiang on the Risks of AI
Date: 2023-05-12T14:00:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;risks
Slug: 2023-05-12-ted-chiang-on-the-risks-of-ai

[Source](https://www.schneier.com/blog/archives/2023/05/ted-chiang-on-the-risks-of-ai.html){:target="_blank" rel="noopener"}

> Ted Chiang has an excellent essay in the New Yorker : “Will A.I. Become the New McKinsey?” The question we should be asking is: as A.I. becomes more powerful and flexible, is there any way to keep it from being another version of McKinsey? The question is worth considering across different meanings of the term “A.I.” If you think of A.I. as a broad set of technologies being marketed to companies to help them cut their costs, the question becomes: how do we keep those technologies from working as “capital’s willing executioners”? Alternatively, if you imagine A.I. as a semi-autonomous [...]
