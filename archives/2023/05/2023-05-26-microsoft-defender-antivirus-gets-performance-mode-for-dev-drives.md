Title: Microsoft Defender Antivirus gets ‘performance mode’ for Dev Drives
Date: 2023-05-26T15:42:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-05-26-microsoft-defender-antivirus-gets-performance-mode-for-dev-drives

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-antivirus-gets-performance-mode-for-dev-drives/){:target="_blank" rel="noopener"}

> Microsoft has introduced a new Microsoft Defender capability named "performance mode" for developers on Windows 11, tuned to reduce the impact of antivirus scans when analyzing files stored on Dev Drives. [...]
