Title: Uncle Sam strangles criminals' cashflow by reining in money mules
Date: 2023-05-23T00:01:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-23-uncle-sam-strangles-criminals-cashflow-by-reining-in-money-mules

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/23/us_law_enforcement_money_mules/){:target="_blank" rel="noopener"}

> Tech support scammer among those targeted by recent crackdowns Uncle Sam announced its commenced over 4,000 legal actions in three months — mostly harshly worded letters — to rein in "money mules" involved in romance scams, business email compromise, and other fraudulent schemes.... [...]
