Title: Researchers tell owners to “assume compromise” of unpatched Zyxel firewalls
Date: 2023-05-31T22:33:38+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;exploits;firewalls;vulnerabilities;zyxel
Slug: 2023-05-31-researchers-tell-owners-to-assume-compromise-of-unpatched-zyxel-firewalls

[Source](https://arstechnica.com/?p=1943400){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Firewalls made by Zyxel are being wrangled into a destructive botnet, which is taking control of them by exploiting a recently patched vulnerability with a severity rating of 9.8 out of a possible 10. “At this stage if you have a vulnerable device exposed, assume compromise,” officials from Shadowserver, an organization that monitors Internet threats in real time, warned four days ago. The officials said the exploits are coming from a botnet that’s similar to Mirai, which harnesses the collective bandwidth of thousands of compromised Internet devices to knock sites offline with distributed denial-of-service attacks. According [...]
