Title: IT employee impersonates ransomware gang to extort employer
Date: 2023-05-23T11:22:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-05-23-it-employee-impersonates-ransomware-gang-to-extort-employer

[Source](https://www.bleepingcomputer.com/news/security/it-employee-impersonates-ransomware-gang-to-extort-employer/){:target="_blank" rel="noopener"}

> A 28-year-old United Kingdom man from Fleetwood, Hertfordshire, has been convicted of unauthorized computer access with criminal intent and blackmailing his employer. [...]
