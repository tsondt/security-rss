Title: Dish Network likely paid ransom after recent ransomware attack
Date: 2023-05-19T11:34:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-19-dish-network-likely-paid-ransom-after-recent-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/dish-network-likely-paid-ransom-after-recent-ransomware-attack/){:target="_blank" rel="noopener"}

> Dish Network, an American television provider, most likely paid a ransom after being hit by a ransomware attack in February based on the wording used in data breach notification letters sent to impacted employees. [...]
