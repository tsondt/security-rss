Title: Alien versus Predator? No, this Android spyware works together
Date: 2023-05-27T01:23:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-27-alien-versus-predator-no-this-android-spyware-works-together

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/27/predator_analysis_talos/){:target="_blank" rel="noopener"}

> Phone-hugging code can record calls, read messages, track geolocation, access camera, other snooping The Android Predator spyware has more surveillance capabilities than previously suspected, according to analysis by Cisco Talos, with an assist from non-profit Citizen Lab in Canada.... [...]
