Title: US bans North Korean outsourcer and its feisty freelancers
Date: 2023-05-24T02:58:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-05-24-us-bans-north-korean-outsourcer-and-its-feisty-freelancers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/24/treasury_bans_more_dprk_techies/){:target="_blank" rel="noopener"}

> They do your work – usually from Russia and China – then send their wages home to pay for missiles When businesses go shopping for IT services, North Korea-controlled companies probably struggle to make it into many lists.... [...]
