Title: Feds, you'll need a warrant for that cellphone border search
Date: 2023-05-31T23:52:00+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-05-31-feds-youll-need-a-warrant-for-that-cellphone-border-search

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/31/us_border_phone_search/){:target="_blank" rel="noopener"}

> Here's a story with a twist A federal district judge has ruled that authorities must obtain a warrant to search an American citizen's cellphone at the border, barring exigent circumstances.... [...]
