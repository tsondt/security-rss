Title: China hasn't told Micron why it failed security review, or what its ban means
Date: 2023-05-23T02:58:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-05-23-china-hasnt-told-micron-why-it-failed-security-review-or-what-its-ban-means

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/23/micron_china_ban_uncertainty/){:target="_blank" rel="noopener"}

> US memory-maker forecasts single-digit revenue impact, and ongoing gloom in PC and smartmobe markets US memory-maker Micron has no idea why Chinese authorities have decided its products represent a security risk, or which customers it's not allowed to sell to.... [...]
