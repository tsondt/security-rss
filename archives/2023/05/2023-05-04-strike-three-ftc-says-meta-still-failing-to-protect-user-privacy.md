Title: Strike three: FTC says Meta still failing to protect user privacy
Date: 2023-05-04T16:08:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-05-04-strike-three-ftc-says-meta-still-failing-to-protect-user-privacy

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/04/strike_three_ftc_says_meta/){:target="_blank" rel="noopener"}

> Deals between Zuckercorp + FTC in 2012 and 2020 are being ignored, so time to get stricter, says commish The US Federal Trade Commission is preparing to take action against Facebook parent company Meta for a third time over claims it failed to protect user privacy, as required under a 2020 agreement Meta made with the regulator.... [...]
