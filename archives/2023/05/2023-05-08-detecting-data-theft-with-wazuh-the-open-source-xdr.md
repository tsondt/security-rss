Title: Detecting data theft with Wazuh, the open-source XDR
Date: 2023-05-08T10:05:10-04:00
Author: Sponsored by Wazuh
Category: BleepingComputer
Tags: Security
Slug: 2023-05-08-detecting-data-theft-with-wazuh-the-open-source-xdr

[Source](https://www.bleepingcomputer.com/news/security/detecting-data-theft-with-wazuh-the-open-source-xdr/){:target="_blank" rel="noopener"}

> Threat actors can steal data from organizations to sell to other malicious actors, making it a major risk for organizations. Wazuh, the free and open-source XDR/SIEM, offers several capabilities that protection against data theft. [...]
