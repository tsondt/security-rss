Title: Android apps with spyware installed 421 million times from Google Play
Date: 2023-05-30T10:38:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-05-30-android-apps-with-spyware-installed-421-million-times-from-google-play

[Source](https://www.bleepingcomputer.com/news/security/android-apps-with-spyware-installed-421-million-times-from-google-play/){:target="_blank" rel="noopener"}

> A new Android malware distributed as an advertisement SDK has been discovered in multiple apps, many previously on Google Play and collectively downloaded over 400 million times. [...]
