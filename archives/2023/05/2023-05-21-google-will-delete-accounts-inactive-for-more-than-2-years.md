Title: Google will delete accounts inactive for more than 2 years
Date: 2023-05-21T11:07:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-05-21-google-will-delete-accounts-inactive-for-more-than-2-years

[Source](https://www.bleepingcomputer.com/news/security/google-will-delete-accounts-inactive-for-more-than-2-years/){:target="_blank" rel="noopener"}

> Google has updated its policy for personal accounts across its services to allow a maximum period of inactivity of two years. [...]
