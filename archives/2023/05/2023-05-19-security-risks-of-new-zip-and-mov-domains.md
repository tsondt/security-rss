Title: Security Risks of New .zip and .mov Domains
Date: 2023-05-19T11:11:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;Google;phishing;vulnerabilities
Slug: 2023-05-19-security-risks-of-new-zip-and-mov-domains

[Source](https://www.schneier.com/blog/archives/2023/05/security-risks-of-new-zip-and-mov-domains.html){:target="_blank" rel="noopener"}

> Researchers are worried about Google’s.zip and.mov domains, because they are confusing. Mistaking a URL for a filename could be a security vulnerability. [...]
