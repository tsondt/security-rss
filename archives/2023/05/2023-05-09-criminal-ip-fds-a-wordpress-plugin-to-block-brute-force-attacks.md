Title: Criminal IP FDS: A WordPress Plugin to Block Brute Force Attacks
Date: 2023-05-09T10:01:02-04:00
Author: Sponsored by AI Spera
Category: BleepingComputer
Tags: Security
Slug: 2023-05-09-criminal-ip-fds-a-wordpress-plugin-to-block-brute-force-attacks

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-fds-a-wordpress-plugin-to-block-brute-force-attacks/){:target="_blank" rel="noopener"}

> As cybersecurity threats continue to evolve, brute-force attacks have become a growing concern. To address this issue, AI Spera released a new WordPress plugin called Anti-Brute Force, Login Fraud Detector, also known as Criminal IP FDS (Fraud Detection System). [...]
