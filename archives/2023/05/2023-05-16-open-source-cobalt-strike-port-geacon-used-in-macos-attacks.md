Title: Open-source Cobalt Strike port 'Geacon' used in macOS attacks
Date: 2023-05-16T08:10:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-16-open-source-cobalt-strike-port-geacon-used-in-macos-attacks

[Source](https://www.bleepingcomputer.com/news/security/open-source-cobalt-strike-port-geacon-used-in-macos-attacks/){:target="_blank" rel="noopener"}

> Geacon, a Go-based implementation of the beacon from the widely abused penetration testing suite Cobalt Strike, is being used more and more to target macOS devices. [...]
