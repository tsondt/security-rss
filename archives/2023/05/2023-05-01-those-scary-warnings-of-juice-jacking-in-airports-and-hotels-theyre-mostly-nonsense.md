Title: Those scary warnings of juice jacking in airports and hotels? They’re mostly nonsense
Date: 2023-05-01T11:00:34+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;android;hacking;iPhones;juice hacking
Slug: 2023-05-01-those-scary-warnings-of-juice-jacking-in-airports-and-hotels-theyre-mostly-nonsense

[Source](https://arstechnica.com/?p=1935117){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson | Getty Images) Federal authorities, tech pundits, and news outlets want you to be on the lookout for a scary cyberattack that can hack your phone when you do nothing more than plug it into a public charging station. These warnings of “juice jacking,” as the threat has come to be known, have been circulating for more than a decade. Earlier this month, though, juice jacking fears hit a new high when the FBI and Federal Communications Commission issued new, baseless warnings that generated ominous-sounding news reports from hundreds of outlets. NPR reported that the crime [...]
