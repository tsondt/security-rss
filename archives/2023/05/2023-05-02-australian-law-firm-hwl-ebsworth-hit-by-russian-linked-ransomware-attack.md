Title: Australian law firm HWL Ebsworth hit by Russian-linked ransomware attack
Date: 2023-05-02T03:14:40+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Data and computer security;Australia news;Privacy;Cybercrime;Internet;Technology;Malware
Slug: 2023-05-02-australian-law-firm-hwl-ebsworth-hit-by-russian-linked-ransomware-attack

[Source](https://www.theguardian.com/technology/2023/may/02/australian-law-firm-hwl-ebsworth-hit-by-russian-linked-ransomware-attack){:target="_blank" rel="noopener"}

> Cyberattack resulted in hacking of 4TB of data including IDs, finance reports, accounting data, client documents and credit card details Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast The Australian commercial law firm HWL Ebsworth has fallen victim to a ransomware attack, with Russian-linked hackers claiming to have obtained client information and employee data. Late last week, the ALPHV/Blackcat ransomware group posted on its website that 4TB of company data had been hacked, including employee CVs, IDs, financial reports, accounting data, client documentation, credit card [...]
