Title: Emby shuts down user media servers hacked in recent attack
Date: 2023-05-26T10:56:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-26-emby-shuts-down-user-media-servers-hacked-in-recent-attack

[Source](https://www.bleepingcomputer.com/news/security/emby-shuts-down-user-media-servers-hacked-in-recent-attack/){:target="_blank" rel="noopener"}

> Emby says it remotely shut down an undisclosed number of user-hosted media server instances that were recently hacked by exploiting a previously known vulnerability and an insecure admin account configuration. [...]
