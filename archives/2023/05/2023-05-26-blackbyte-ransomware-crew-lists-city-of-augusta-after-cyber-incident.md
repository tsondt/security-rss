Title: BlackByte ransomware crew lists city of Augusta after cyber 'incident'
Date: 2023-05-26T01:34:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-26-blackbyte-ransomware-crew-lists-city-of-augusta-after-cyber-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/26/blackbyte_augusta_malware/){:target="_blank" rel="noopener"}

> Mayor promises to comment on Friday BlackByte ransomware crew has claimed Augusta, Georgia, as its latest victim, following what the US city's mayor has, so far, only called a cyber "incident."... [...]
