Title: 23-year-old Brit linked to 2020 Twitter attack and SIM-swap scheme pleads guilty
Date: 2023-05-10T12:32:10+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-05-10-23-year-old-brit-linked-to-2020-twitter-attack-and-sim-swap-scheme-pleads-guilty

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/10/guilty_plea_twitter_o_connor_case/){:target="_blank" rel="noopener"}

> Admits to cyberstalking, wire fraud charges as Feds take $700k off him A 23-year-old British citizen has confessed to "multiple schemes" involving computer crimes, including playing a part in the July 2020 Twitter attack that saw the accounts of Amazon CEO Jeff Bezos, Kanye West, and former President Barack Obama hijacked by an unidentified crew.... [...]
