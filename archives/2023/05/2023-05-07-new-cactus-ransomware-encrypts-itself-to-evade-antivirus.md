Title: New Cactus ransomware encrypts itself to evade antivirus
Date: 2023-05-07T12:25:13-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-05-07-new-cactus-ransomware-encrypts-itself-to-evade-antivirus

[Source](https://www.bleepingcomputer.com/news/security/new-cactus-ransomware-encrypts-itself-to-evade-antivirus/){:target="_blank" rel="noopener"}

> A new ransomware operation called Cactus has been exploiting vulnerabilities in VPN appliances for initial access to networks of "large commercial entities." [...]
