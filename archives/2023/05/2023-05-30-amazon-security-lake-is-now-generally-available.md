Title: Amazon Security Lake is now generally available
Date: 2023-05-30T20:14:56+00:00
Author: Ross Warren
Category: AWS Security
Tags: Amazon Security Lake;Announcements;Foundational (100);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-05-30-amazon-security-lake-is-now-generally-available

[Source](https://aws.amazon.com/blogs/security/amazon-security-lake-is-now-generally-available/){:target="_blank" rel="noopener"}

> Today we are thrilled to announce the general availability of Amazon Security Lake, first announced in a preview release at 2022 re:Invent. Security Lake centralizes security data from Amazon Web Services (AWS) environments, software as a service (SaaS) providers, on-premises, and cloud sources into a purpose-built data lake that is stored in your AWS account. [...]
