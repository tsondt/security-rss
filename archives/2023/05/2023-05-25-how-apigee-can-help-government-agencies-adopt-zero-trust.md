Title: How Apigee can help government agencies adopt Zero Trust
Date: 2023-05-25T16:00:00+00:00
Author: Saurabh Chhatwal
Category: GCP Security
Tags: Apigee;Security & Identity;Public Sector
Slug: 2023-05-25-how-apigee-can-help-government-agencies-adopt-zero-trust

[Source](https://cloud.google.com/blog/topics/public-sector/how-apigee-can-help-government-agencies-adopt-zero-trust/){:target="_blank" rel="noopener"}

> Securely sharing data is critical to building an effective government application ecosystem. Rather than building new applications, APIs can enable government leaders to gather data-driven insights within their existing technical environments. With the help of APIs, agencies can bring application-based information together to support their objectives. U.S. government agencies are now encouraged to adopt a Zero Trust security architecture to detect and defend against cyber attacks. API protection is a core principle to implement Zero Trust architecture. As Gartner® said in its Innovation Insight for API Protection report, “By 2026, 40% of organizations will select their web application and API [...]
