Title: WordPress plugin hole puts '2 million websites' at risk
Date: 2023-05-08T22:22:01+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-08-wordpress-plugin-hole-puts-2-million-websites-at-risk

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/08/wordpress_plugin_vulnerability/){:target="_blank" rel="noopener"}

> XSS marks the spot WordPress users with the Advanced Custom Fields plugin on their website should upgrade after the discovery of a vulnerability in the code that could open up sites and their visitors to cross-site scripting (XSS) attacks.... [...]
