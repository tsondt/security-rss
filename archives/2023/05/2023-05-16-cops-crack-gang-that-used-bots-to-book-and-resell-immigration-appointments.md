Title: Cops crack gang that used bots to book and resell immigration appointments
Date: 2023-05-16T07:18:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-16-cops-crack-gang-that-used-bots-to-book-and-resell-immigration-appointments

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/16/spain_bot_immigration_booking_scam/){:target="_blank" rel="noopener"}

> Keeping files that mention 'robot rental' may not have been the best way to cover their tracks Police have arrested 69 people alleged to have used bots to book up nearly all of Spain's available appointments with immigration officials, and then sold those meeting slots for between €30 and €200 ($33 to $218) to aspiring migrants.... [...]
