Title: Russian Hacker “Wazawaka” Indicted for Ransomware
Date: 2023-05-16T21:33:43+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Babuk ransomware;Boriselcin;Hive ransomware;LockBit ransomware;Mikhail Pavolovich Matveev;Orange;RAMP;Uhodiransomwar;Wazawaka
Slug: 2023-05-16-russian-hacker-wazawaka-indicted-for-ransomware

[Source](https://krebsonsecurity.com/2023/05/russian-hacker-wazawaka-indicted-for-ransomware/){:target="_blank" rel="noopener"}

> A Russian man identified by KrebsOnSecurity in January 2022 as a prolific and vocal member of several top ransomware groups was the subject of two indictments unsealed by the Justice Department today. U.S. prosecutors say Mikhail Pavolovich Matveev, a.k.a. “ Wazawaka ” and “ Boriselcin ” worked with three different ransomware gangs that extorted hundreds of millions of dollars from companies, schools, hospitals and government agencies. An FBI wanted poster for Matveev. Indictments returned in New Jersey and the District of Columbia allege that Matveev was involved in a conspiracy to distribute ransomware from three different strains or affiliate groups, [...]
