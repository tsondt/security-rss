Title: Friday Squid Blogging: Giant Squid Video
Date: 2023-05-12T21:04:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2023-05-12-friday-squid-blogging-giant-squid-video

[Source](https://www.schneier.com/blog/archives/2023/05/friday-squid-blogging-giant-squid-video.html){:target="_blank" rel="noopener"}

> A video—authentic, not a deep fake—of a giant squid close to the surface. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
