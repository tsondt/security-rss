Title: Teen in court after '$600K swiped from DraftKings gamblers'
Date: 2023-05-19T23:56:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-19-teen-in-court-after-600k-swiped-from-draftkings-gamblers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/19/draftkings_attacker_arrested/){:target="_blank" rel="noopener"}

> Bet he didn't expect these computer hacking charges An 18-year-old Wisconsin man has been charged with allegedly playing a central role in the theft of $600,000 from DraftKings customer accounts.... [...]
