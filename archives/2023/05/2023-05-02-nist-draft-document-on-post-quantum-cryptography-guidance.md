Title: NIST Draft Document on Post-Quantum Cryptography Guidance
Date: 2023-05-02T14:10:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2023-05-02-nist-draft-document-on-post-quantum-cryptography-guidance

[Source](https://www.schneier.com/blog/archives/2023/05/nist-draft-document-on-post-quantum-cryptography-guidance.html){:target="_blank" rel="noopener"}

> NIST has release a draft of Special Publication1800-38A: Migration to Post-Quantum Cryptography: Preparation for Considering the Implementation and Adoption of Quantum Safe Cryptography.” It’s only four pages long, and it doesn’t have a lot of detail—more “volumes” are coming, with more information—but it’s well worth reading. We are going to need to migrate to quantum-resistant public-key algorithms, and the sooner we implement key agility the easier it will be to do so. News article. [...]
