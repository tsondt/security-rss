Title: Russia's APT28 targets Ukraine government with bogus Windows updates
Date: 2023-05-02T06:37:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-02-russias-apt28-targets-ukraine-government-with-bogus-windows-updates

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/02/russia_apt28_ukraine_phishing/){:target="_blank" rel="noopener"}

> Nasty emails designed to infect systems with info-stealing malware The Kremlin-backed threat group APT28 is flooding Ukrainian government agencies with email messages about bogus Windows updates in the hope of dropping malware that will exfiltrate system data.... [...]
