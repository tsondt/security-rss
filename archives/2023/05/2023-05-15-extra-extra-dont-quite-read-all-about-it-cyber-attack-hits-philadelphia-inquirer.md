Title: Extra! Extra! Don’t quite read all about it: Cyber attack hits Philadelphia Inquirer
Date: 2023-05-15T21:44:29+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-15-extra-extra-dont-quite-read-all-about-it-cyber-attack-hits-philadelphia-inquirer

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/15/philadelphia_inquirer_cyber_incident/){:target="_blank" rel="noopener"}

> Breaking news, literally A cyber "incident" stopped The Philadelphia Inquirer's presses over the weekend, halting the Sunday edition's print edition and shutting down the newspaper's offices to staff until at least Tuesday.... [...]
