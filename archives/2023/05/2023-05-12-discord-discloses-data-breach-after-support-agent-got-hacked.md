Title: Discord discloses data breach after support agent got hacked
Date: 2023-05-12T15:05:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-12-discord-discloses-data-breach-after-support-agent-got-hacked

[Source](https://www.bleepingcomputer.com/news/security/discord-discloses-data-breach-after-support-agent-got-hacked/){:target="_blank" rel="noopener"}

> Discord is notifying users of a data breach that occurred after the account of a third-party support agent was compromised. [...]
