Title: T-Mobile US suffers second data theft within months
Date: 2023-05-08T04:31:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-05-08-t-mobile-us-suffers-second-data-theft-within-months

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/08/in_brief_security/){:target="_blank" rel="noopener"}

> Also, Capita's buckets are leaking, ransomware attackers deliver demands via emergency alert, and this week's critical vulns in brief We'd say you'll never guess which telco admitted to a security breakdown last week, but you totally will: T-Mobile US, and for the second time (so far) this year.... [...]
