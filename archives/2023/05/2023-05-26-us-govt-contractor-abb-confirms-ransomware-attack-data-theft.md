Title: US govt contractor ABB confirms ransomware attack, data theft
Date: 2023-05-26T12:33:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-26-us-govt-contractor-abb-confirms-ransomware-attack-data-theft

[Source](https://www.bleepingcomputer.com/news/security/us-govt-contractor-abb-confirms-ransomware-attack-data-theft/){:target="_blank" rel="noopener"}

> Swiss tech multinational and U.S. government contractor ABB has confirmed that some of its systems were impacted by a ransomware attack, previously described by the company as "an IT security incident." [...]
