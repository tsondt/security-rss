Title: Intel investigating leak of Intel Boot Guard private keys after MSI breach
Date: 2023-05-08T13:31:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-05-08-intel-investigating-leak-of-intel-boot-guard-private-keys-after-msi-breach

[Source](https://www.bleepingcomputer.com/news/security/intel-investigating-leak-of-intel-boot-guard-private-keys-after-msi-breach/){:target="_blank" rel="noopener"}

> Intel is investigating the leak of alleged private keys used by the Intel BootGuard security feature, potentially impacting its ability to block the installation of malicious UEFI firmware on MSI devices. [...]
