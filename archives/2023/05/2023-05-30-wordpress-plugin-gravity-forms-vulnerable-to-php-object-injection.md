Title: WordPress plugin ‘Gravity Forms’ vulnerable to PHP object injection
Date: 2023-05-30T15:42:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-30-wordpress-plugin-gravity-forms-vulnerable-to-php-object-injection

[Source](https://www.bleepingcomputer.com/news/security/wordpress-plugin-gravity-forms-vulnerable-to-php-object-injection/){:target="_blank" rel="noopener"}

> The premium WordPress plugin 'Gravity Forms,' currently used by over 930,000 websites, is vulnerable to unauthenticated PHP Object Injection. [...]
