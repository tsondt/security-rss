Title: The Week in Ransomware - May 5th 2023 - Targeting the public sector
Date: 2023-05-05T16:07:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-05-the-week-in-ransomware-may-5th-2023-targeting-the-public-sector

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-5th-2023-targeting-the-public-sector/){:target="_blank" rel="noopener"}

> This week's ransomware news has been dominated by a Royal ransomware attack on the City of Dallas that took down part of the IT infrastructure. [...]
