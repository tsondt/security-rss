Title: Share and query encrypted data in AWS Clean Rooms
Date: 2023-05-16T19:20:00+00:00
Author: Jonathan Herzog
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Technical How-to;AWS Clean Rooms;cryptography;Security Blog
Slug: 2023-05-16-share-and-query-encrypted-data-in-aws-clean-rooms

[Source](https://aws.amazon.com/blogs/security/share-and-query-encrypted-data-in-aws-clean-rooms/){:target="_blank" rel="noopener"}

> In this post, we’d like to introduce you to the cryptographic computing feature of AWS Clean Rooms. With AWS Clean Rooms, customers can run collaborative data-query sessions on sensitive data sets that live in different AWS accounts, and can do so without having to share, aggregate, or replicate the data. When customers also use the [...]
