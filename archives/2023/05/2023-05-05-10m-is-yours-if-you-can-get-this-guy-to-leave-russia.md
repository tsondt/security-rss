Title: $10M Is Yours If You Can Get This Guy to Leave Russia
Date: 2023-05-05T01:50:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;79608229389;Constella Intelligence;Denis Gennadievich Kulkov;Intel 471;Joker's stash;K022YB190;KreenJo;Mazafaka;Nordex;Nordexin;nordia@yandex.ru;polkas@bk.ru;Try2Check;U.S. Department of Justice;U.S. Department of State;U.S. Secret Service;Unicc;Vault Market
Slug: 2023-05-05-10m-is-yours-if-you-can-get-this-guy-to-leave-russia

[Source](https://krebsonsecurity.com/2023/05/10m-is-yours-if-you-can-get-this-guy-to-leave-russia/){:target="_blank" rel="noopener"}

> The U.S. government this week put a $10 million bounty on a Russian man who for the past 18 years operated Try2Check, one of the cybercrime underground’s most trusted services for checking the validity of stolen credit card data. U.S. authorities say 43-year-old Denis Kulkov ‘s card-checking service made him at least $18 million, which he used to buy a Ferrari, Land Rover, and other luxury items. Denis Kulkov, a.k.a. “Nordex,” in his Ferrari. Image: USDOJ. Launched in 2005, Try2Check soon was processing more than a million card-checking transactions per month — charging 20 cents per transaction. Cybercriminals turned to [...]
