Title: Feds offer $10m reward for info on alleged Russian ransomware crim
Date: 2023-05-17T00:30:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-17-feds-offer-10m-reward-for-info-on-alleged-russian-ransomware-crim

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/17/feds_charge_russian_ransomware/){:target="_blank" rel="noopener"}

> Infecting cops' computers is one way to put a target on your back The Feds have sanctioned a Russian national accused of using LockBit, Babuk, and Hive ransomware to extort a law enforcement agency and nonprofit healthcare organization in New Jersey, and the Metropolitan Police Department in Washington DC, among "numerous" other victim organizations in the US and globally.... [...]
