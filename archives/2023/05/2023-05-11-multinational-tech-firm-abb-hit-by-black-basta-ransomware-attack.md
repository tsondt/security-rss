Title: Multinational tech firm ABB hit by Black Basta ransomware attack
Date: 2023-05-11T17:05:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-11-multinational-tech-firm-abb-hit-by-black-basta-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/multinational-tech-firm-abb-hit-by-black-basta-ransomware-attack/){:target="_blank" rel="noopener"}

> Swiss multinational company ABB, a leading electrification and automation technology provider, has suffered a Black Basta ransomware attack, reportedly impacting business operations. [...]
