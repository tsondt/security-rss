Title: India to send official whassup to WhatsApp after massive spamstorm
Date: 2023-05-12T01:57:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-05-12-india-to-send-official-whassup-to-whatsapp-after-massive-spamstorm

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/12/india_whatsapp_spam_privacy_demands/){:target="_blank" rel="noopener"}

> In a weird way, we can blame this on AI being a better bet than blockchain India's IT minister Rajeev Chandrasekhar will ask WhatsApp to explain what's up, after the Meta-owned messaging service experienced a dramatic increase in spam calls.... [...]
