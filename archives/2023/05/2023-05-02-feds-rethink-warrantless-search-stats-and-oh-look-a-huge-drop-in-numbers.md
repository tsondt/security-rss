Title: Feds rethink warrantless search stats and – oh look, a huge drop in numbers
Date: 2023-05-02T01:56:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-02-feds-rethink-warrantless-search-stats-and-oh-look-a-huge-drop-in-numbers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/02/fbi_section_702_searches_2022/){:target="_blank" rel="noopener"}

> 119,000 instances of homeland snooping as the power to do so comes under review Warrantless searches of US residents' communications by the FBI dropped sharply last year – from about 3.4 million in 2021 to 119,383 in 2022, according to Uncle Sam.... [...]
