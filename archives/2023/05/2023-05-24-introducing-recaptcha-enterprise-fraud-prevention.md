Title: Introducing reCAPTCHA Enterprise Fraud Prevention
Date: 2023-05-24T16:00:00+00:00
Author: Jason Fedor
Category: GCP Security
Tags: Security & Identity
Slug: 2023-05-24-introducing-recaptcha-enterprise-fraud-prevention

[Source](https://cloud.google.com/blog/products/identity-security/introducing-recaptcha-enterprise-fraud-prevention/){:target="_blank" rel="noopener"}

> Today, we are putting the power of Google’s insights and intelligence in the hands of risk, fraud, and security teams everywhere. We are pleased to announce the general availability of reCAPTCHA Enterprise Fraud Prevention, a new product that uses Google's own fraud models, machine learning, and intelligence from protecting more than 6 million websites to help stop payment fraud. reCAPTCHA Enterprise Fraud Prevention can help protect payment transactions by identifying targeted manual attacks and large-scale fraud attempts. It automatically trains fraud models based on behavior and transaction data to identify events that are likely fraudulent and could cause a dispute [...]
