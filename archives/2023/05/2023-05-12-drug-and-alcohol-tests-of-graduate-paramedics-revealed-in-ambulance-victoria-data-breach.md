Title: Drug and alcohol tests of graduate paramedics revealed in Ambulance Victoria data breach
Date: 2023-05-12T00:11:24+00:00
Author: Nino Bucci
Category: The Guardian
Tags: Victoria;Data and computer security;Health;Privacy;Emergency services;Australia news;Data protection;Australian trade unions
Slug: 2023-05-12-drug-and-alcohol-tests-of-graduate-paramedics-revealed-in-ambulance-victoria-data-breach

[Source](https://www.theguardian.com/australia-news/2023/may/11/ambulance-victoria-data-breach-reveals-drug-and-alcohol-tests-of-graduate-paramedics){:target="_blank" rel="noopener"}

> Privacy watchdog will be asked to investigate after information became available for all employees to view on the staff intranet Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast The confidential drug and alcohol test results of graduate paramedics were available for every Ambulance Victoria staff member to view under a significant breach that is set to be reported to the state’s privacy watchdog. According to an email sent late Thursday to members of the Victorian Ambulance Union, confidential spreadsheets relating to pre-employment testing of graduate [...]
