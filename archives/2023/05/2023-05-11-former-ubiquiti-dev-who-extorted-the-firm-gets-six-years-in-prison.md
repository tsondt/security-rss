Title: Former Ubiquiti dev who extorted the firm gets six years in prison
Date: 2023-05-11T10:55:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-05-11-former-ubiquiti-dev-who-extorted-the-firm-gets-six-years-in-prison

[Source](https://www.bleepingcomputer.com/news/security/former-ubiquiti-dev-who-extorted-the-firm-gets-six-years-in-prison/){:target="_blank" rel="noopener"}

> Nickolas Sharp, a former senior developer of Ubiquiti, was sentenced to six years in prison for stealing company data, attempting to extort his employer, and aiding the publication of misleading news articles that severely impacted the firm's market capitalization. [...]
