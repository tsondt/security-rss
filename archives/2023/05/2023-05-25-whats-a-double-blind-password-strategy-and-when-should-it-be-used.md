Title: What’s a Double-Blind Password Strategy and When Should It Be Used
Date: 2023-05-25T10:04:02-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-05-25-whats-a-double-blind-password-strategy-and-when-should-it-be-used

[Source](https://www.bleepingcomputer.com/news/security/whats-a-double-blind-password-strategy-and-when-should-it-be-used/){:target="_blank" rel="noopener"}

> Strategies such as the double-blind password strategy can be effective, but only if end-users are fully adopting the practice. Learn more about when and how to use it. [...]
