Title: Arms maker Rheinmetall confirms BlackBasta ransomware attack
Date: 2023-05-23T12:02:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-23-arms-maker-rheinmetall-confirms-blackbasta-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/arms-maker-rheinmetall-confirms-blackbasta-ransomware-attack/){:target="_blank" rel="noopener"}

> German automotive and arms manufacturer Rheinmetall AG confirms that it suffered a BlackBasta ransomware attack that impacted its civilian business. [...]
