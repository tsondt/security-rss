Title: Thinking straight in the SoC: How AI erases cognitive bias
Date: 2023-05-31T08:59:10+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2023-05-31-thinking-straight-in-the-soc-how-ai-erases-cognitive-bias

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/31/thinking_straight_in_the_soc/){:target="_blank" rel="noopener"}

> The whispering voice presents an alternative point of view to steer cyber security pros in the right direction Sponsored Feature What do bears and cyber criminals have in common? Both of them are scary, and they both have the same effect on security teams.... [...]
