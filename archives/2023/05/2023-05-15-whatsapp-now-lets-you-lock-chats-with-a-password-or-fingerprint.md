Title: WhatsApp now lets you lock chats with a password or fingerprint
Date: 2023-05-15T14:20:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-15-whatsapp-now-lets-you-lock-chats-with-a-password-or-fingerprint

[Source](https://www.bleepingcomputer.com/news/security/whatsapp-now-lets-you-lock-chats-with-a-password-or-fingerprint/){:target="_blank" rel="noopener"}

> Meta is now rolling out 'Chat Lock,' a new WhatsApp privacy feature allowing users to block others from accessing their most personal conversations. [...]
