Title: Russian ransomware affiliate charged with attacks on critical infrastructure
Date: 2023-05-16T11:57:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-16-russian-ransomware-affiliate-charged-with-attacks-on-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/russian-ransomware-affiliate-charged-with-attacks-on-critical-infrastructure/){:target="_blank" rel="noopener"}

> The U.S. Justice Department has filed charges against a Russian citizen named Mikhail Pavlovich Matveev (also known as Wazawaka or Boriselcin) for involvement in three ransomware operations that targeted victims across the United States. [...]
