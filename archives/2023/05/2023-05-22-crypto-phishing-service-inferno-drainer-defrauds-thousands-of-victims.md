Title: Crypto phishing service Inferno Drainer defrauds thousands of victims
Date: 2023-05-22T12:49:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-05-22-crypto-phishing-service-inferno-drainer-defrauds-thousands-of-victims

[Source](https://www.bleepingcomputer.com/news/security/crypto-phishing-service-inferno-drainer-defrauds-thousands-of-victims/){:target="_blank" rel="noopener"}

> A cryptocurrency phishing and scam service called 'Inferno Drainer' has reportedly stolen over $5.9 million worth of crypto from 4,888 victims. [...]
