Title: Delivering on the AWS Digital Sovereignty Pledge: Control without compromise
Date: 2023-05-04T14:33:23+00:00
Author: Matt Garman
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Digital Sovereignty Pledge;AWS Nitro System;Data protection;Digital Sovereignty;EU Data Protection;Security Blog;Sovereign-by-design
Slug: 2023-05-04-delivering-on-the-aws-digital-sovereignty-pledge-control-without-compromise

[Source](https://aws.amazon.com/blogs/security/delivering-on-the-aws-digital-sovereignty-pledge-control-without-compromise/){:target="_blank" rel="noopener"}

> At AWS, earning and maintaining customer trust is the foundation of our business. We understand that protecting customer data is key to achieving this. We also know that trust must continue to be earned through transparency and assurances. In November 2022, we announced the new AWS Digital Sovereignty Pledge, our commitment to offering all AWS [...]
