Title: What should protection for your 365 data really look like?
Date: 2023-05-10T14:12:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-05-10-what-should-protection-for-your-365-data-really-look-like

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/10/what_should_protection_for_your/){:target="_blank" rel="noopener"}

> Don't let the cyber-criminals spread through your enterprise Sponsored Microsoft 365 has worked its way into so many facets of our organizations that it can be hard to imagine what life would be like without it.... [...]
