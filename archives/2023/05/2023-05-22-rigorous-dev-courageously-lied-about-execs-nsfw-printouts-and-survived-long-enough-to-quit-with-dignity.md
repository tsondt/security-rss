Title: Rigorous dev courageously lied about exec's NSFW printouts - and survived long enough to quit with dignity
Date: 2023-05-22T07:32:06+00:00
Author: Matthew JC Powell
Category: The Register
Tags: 
Slug: 2023-05-22-rigorous-dev-courageously-lied-about-execs-nsfw-printouts-and-survived-long-enough-to-quit-with-dignity

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/22/who_me/){:target="_blank" rel="noopener"}

> Log files don't lie and in this case one nasty incident spoke to a far deeper malaise Who, Me? Wait? What? Is it Monday already? Not to fear, gentle readerfolk, for Uncle Reg is here with another instalment of Who, Me? – tales of readers having a much worse day than you. Enjoy the schadenfreude.... [...]
