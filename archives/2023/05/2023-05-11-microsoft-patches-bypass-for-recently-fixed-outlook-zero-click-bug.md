Title: Microsoft patches bypass for recently fixed Outlook zero-click bug
Date: 2023-05-11T18:46:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-05-11-microsoft-patches-bypass-for-recently-fixed-outlook-zero-click-bug

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-patches-bypass-for-recently-fixed-outlook-zero-click-bug/){:target="_blank" rel="noopener"}

> Microsoft fixed a security vulnerability this week that could be used by remote attackers to bypass recent patches for a critical Outlook zero-day security flaw abused in the wild. [...]
