Title: More UK councils caught by Capita's open AWS bucket blunder
Date: 2023-05-22T12:13:01+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-05-22-more-uk-councils-caught-by-capitas-open-aws-bucket-blunder

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/22/capita_security_pensions_aws_bucket_city_councils/){:target="_blank" rel="noopener"}

> As for March megabreach? M&S and Guinness maker Diageo warn pension members about data risks The bad news train keeps rolling for Capita, with more local British councils surfacing to say their data was put on the line by an unsecured AWS bucket, and, separately, pension clients warning of possible data theft in March's mega breach.... [...]
