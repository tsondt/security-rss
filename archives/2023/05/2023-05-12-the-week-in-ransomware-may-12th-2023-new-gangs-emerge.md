Title: The Week in Ransomware - May 12th 2023 - New Gangs Emerge
Date: 2023-05-12T17:23:41-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-12-the-week-in-ransomware-may-12th-2023-new-gangs-emerge

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-12th-2023-new-gangs-emerge/){:target="_blank" rel="noopener"}

> This week we have multiple reports of new ransomware families targeting the enterprise, named Cactus and Akira, both increasingly active as they target the enterprise. [...]
