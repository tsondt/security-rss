Title: Cloud CISO Perspectives: Early May 2023
Date: 2023-05-19T16:00:00+00:00
Author: M.K. Palmore
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2023-05-19-cloud-ciso-perspectives-early-may-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-early-may-2023/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for May 2023. This month, we’re featuring guest author MK Palmore, director in our Office of the CISO. MK will be discussing our new Google Cybersecurity Certificate and how it can help organizations close the security talent gap. Before I turn the mic over to MK, I’d like to thank everyone who attended our panels, presentations, keynotes, and visited our booth at the RSA Conference last month. It was a pleasure to see old friends and make new ones, and to share the excitement over our Security AI Workbench announcement and our plans [...]
