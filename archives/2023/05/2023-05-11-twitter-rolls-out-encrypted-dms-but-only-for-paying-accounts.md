Title: Twitter rolls out encrypted DMs, but only for paying accounts
Date: 2023-05-11T10:02:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-11-twitter-rolls-out-encrypted-dms-but-only-for-paying-accounts

[Source](https://www.bleepingcomputer.com/news/security/twitter-rolls-out-encrypted-dms-but-only-for-paying-accounts/){:target="_blank" rel="noopener"}

> Twitter has launched its 'Encrypted Direct Messages' feature allowing paid Twitter Blue subscribers to send end-to-end encrypted messages to other users on the platform. [...]
