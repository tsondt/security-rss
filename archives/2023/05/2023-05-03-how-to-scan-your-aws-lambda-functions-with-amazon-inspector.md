Title: How to scan your AWS Lambda functions with Amazon Inspector
Date: 2023-05-03T15:56:41+00:00
Author: Vamsi Vikash Ankam
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon CodeGuru;Amazon Inspector;AWS Lambda;Remediation;Security Blog
Slug: 2023-05-03-how-to-scan-your-aws-lambda-functions-with-amazon-inspector

[Source](https://aws.amazon.com/blogs/security/how-to-scan-your-aws-lambda-functions-with-amazon-inspector/){:target="_blank" rel="noopener"}

> Amazon Inspector is a vulnerability management and application security service that helps improve the security of your workloads. It automatically scans applications for vulnerabilities and provides you with a detailed list of security findings, prioritized by their severity level, as well as remediation instructions. In this blog post, we’ll introduce new features from Amazon Inspector [...]
