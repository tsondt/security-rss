Title: TikTok to let Oracle view source code, algorithm, and content moderation
Date: 2023-05-23T14:36:06+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-05-23-tiktok-to-let-oracle-view-source-code-algorithm-and-content-moderation

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/23/tiktok_offers_oracle_view_of/){:target="_blank" rel="noopener"}

> It's all in the name of national security as Trump-era collab continues in Project Texas TikTok, the social video platform used by around 150 million people in the US, is set to hand access to its source code, algorithm and content moderation material to Oracle in a bid to allay data protection and national security concerns stateside.... [...]
