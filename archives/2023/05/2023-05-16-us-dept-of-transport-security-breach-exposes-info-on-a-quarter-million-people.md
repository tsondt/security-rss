Title: US Dept of Transport security breach exposes info on a quarter-million people
Date: 2023-05-16T21:30:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-05-16-us-dept-of-transport-security-breach-exposes-info-on-a-quarter-million-people

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/16/us_dot_pii_security_breach_exposure/){:target="_blank" rel="noopener"}

> Not the first time Uncle Sam has had the wheels come off its IT systems A US Department of Transportation computer system used to reimburse federal government employees for commuting costs somehow suffered a security breach that exposed the personal info for 237,000 current and former workers.... [...]
