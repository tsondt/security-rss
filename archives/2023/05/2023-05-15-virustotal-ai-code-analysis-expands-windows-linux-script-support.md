Title: VirusTotal AI code analysis expands Windows, Linux script support
Date: 2023-05-15T15:54:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-15-virustotal-ai-code-analysis-expands-windows-linux-script-support

[Source](https://www.bleepingcomputer.com/news/security/virustotal-ai-code-analysis-expands-windows-linux-script-support/){:target="_blank" rel="noopener"}

> Google has added support for more scripting languages to VirusTotal Code Insight, a recently introduced artificial intelligence-based code analysis feature. [...]
