Title: Zyxel warns of critical vulnerabilities in firewall and VPN devices
Date: 2023-05-25T09:31:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-25-zyxel-warns-of-critical-vulnerabilities-in-firewall-and-vpn-devices

[Source](https://www.bleepingcomputer.com/news/security/zyxel-warns-of-critical-vulnerabilities-in-firewall-and-vpn-devices/){:target="_blank" rel="noopener"}

> Zyxel is warning customers of two critical-severity vulnerabilities in several of its firewall and VPN products that attackers could leverage without authentication. [...]
