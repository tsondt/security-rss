Title: Detect threats to your data stored in RDS databases by using GuardDuty
Date: 2023-05-10T18:17:17+00:00
Author: Marshall Jones
Category: AWS Security
Tags: Advanced (300);Amazon Aurora;Best Practices;Security, Identity, & Compliance;Amazon GuardDuty;Amazon RDS;Security Blog
Slug: 2023-05-10-detect-threats-to-your-data-stored-in-rds-databases-by-using-guardduty

[Source](https://aws.amazon.com/blogs/security/detect-threats-to-your-data-stored-in-rds-databases-by-using-guardduty/){:target="_blank" rel="noopener"}

> With Amazon Relational Database Service (Amazon RDS), you can set up, operate, and scale a relational database in the AWS Cloud. Amazon RDS provides cost-efficient, resizable capacity for an industry-standard relational database and manages common database administration tasks. If you use Amazon RDS for your workloads, you can now use Amazon GuardDuty RDS Protection to [...]
