Title: How to solve customer challenges when security patching Google Kubernetes Engine
Date: 2023-05-11T16:00:00+00:00
Author: Stan Trepetin
Category: GCP Security
Tags: Containers & Kubernetes;Security & Identity
Slug: 2023-05-11-how-to-solve-customer-challenges-when-security-patching-google-kubernetes-engine

[Source](https://cloud.google.com/blog/products/identity-security/how-to-solve-challenges-when-security-patching-google-kubernetes-engine/){:target="_blank" rel="noopener"}

> Editor's note: This blog post has been adapted from the April 2023 Threat Horizons Report. Cloud customers are increasingly running their compute workloads in Kubernetes clusters due to the availability, flexibility, and security they provide. Just like other IT assets, these clusters need to be routinely patched to keep their features current and to install security and bug fixes. Reviewing data collected between 2021 and 2022, Google has found that Google Kubernetes Engine (GKE) customers sometimes delay security patching their clusters. The primary cause of these delays is the concern that patching might inadvertently interrupt production operations. While delaying security [...]
