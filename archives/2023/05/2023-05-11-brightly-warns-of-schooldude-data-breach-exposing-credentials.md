Title: Brightly warns of SchoolDude data breach exposing credentials
Date: 2023-05-11T16:25:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-11-brightly-warns-of-schooldude-data-breach-exposing-credentials

[Source](https://www.bleepingcomputer.com/news/security/brightly-warns-of-schooldude-data-breach-exposing-credentials/){:target="_blank" rel="noopener"}

> U.S. tech company and Siemens subsidiary Brightly Software is notifying customers that their personal information and credentials were stolen by attackers who gained access to the database of its SchoolDude online platform. [...]
