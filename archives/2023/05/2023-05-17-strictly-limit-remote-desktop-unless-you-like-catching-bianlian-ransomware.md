Title: 'Strictly limit' remote desktop – unless you like catching BianLian ransomware
Date: 2023-05-17T20:32:46+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-17-strictly-limit-remote-desktop-unless-you-like-catching-bianlian-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/17/fbi_cisa_bianlian_advisory/){:target="_blank" rel="noopener"}

> Do it or don't. We're not cops. But the FBI are, and they have this to say The FBI and friends have warned organizations to "strictly limit the use of RDP and other remote desktop services" to avoid BianLian infections and the ransomware gang's extortion attempts that follow the data encryption.... [...]
