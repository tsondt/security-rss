Title: 1. This crypto-coin is called Jimbo. 2. $8m was stolen from its devs in flash loan attack
Date: 2023-05-30T23:56:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-30-1-this-crypto-coin-is-called-jimbo-2-8m-was-stolen-from-its-devs-in-flash-loan-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/30/jimbos_protocol_defi_attack/){:target="_blank" rel="noopener"}

> 3. It's asked for 90% of the digital dosh back, or else it'll beg the cops for help Just days after releasing the second – and supposedly more stable and secure – version of its decentralized finance (DeFi) app, Jimbos Protocol over the weekend was hit by attackers who stole stole 4,090 ETH tokens from the project worth about $7.5 million.... [...]
