Title: Cloud Data Loss Prevention’s sensitive data intelligence service is now available in Security Command Center
Date: 2023-05-16T16:00:00+00:00
Author: Scott Ellis
Category: GCP Security
Tags: Security & Identity
Slug: 2023-05-16-cloud-data-loss-preventions-sensitive-data-intelligence-service-is-now-available-in-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/dlp-sensitive-data-intelligence-now-available-in-scc/){:target="_blank" rel="noopener"}

> Our Cloud Data Loss Prevention (Cloud DLP) discovery service can monitor and profile your data warehouse to bring awareness of where sensitive data is stored and processed. Profiling is also useful for confirming that data is not being stored and processed where you don’t want it. But how can you make use of this intelligence within your existing security and governance workflows so you can reduce risk? Starting today, we have integrated Cloud DLP’s sensitive-data discovery service with Security Command Center, our platform-native security and risk management solution. By bringing together sensitive data intelligence with your security controls, security teams [...]
