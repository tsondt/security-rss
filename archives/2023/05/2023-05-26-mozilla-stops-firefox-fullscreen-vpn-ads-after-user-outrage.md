Title: Mozilla stops Firefox fullscreen VPN ads after user outrage
Date: 2023-05-26T10:52:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-26-mozilla-stops-firefox-fullscreen-vpn-ads-after-user-outrage

[Source](https://www.bleepingcomputer.com/news/security/mozilla-stops-firefox-fullscreen-vpn-ads-after-user-outrage/){:target="_blank" rel="noopener"}

> Firefox users have been complaining about very intrusive full-screen advertisements promoting Mozilla VPN displayed in the web browser when navigating an unrelated page. [...]
