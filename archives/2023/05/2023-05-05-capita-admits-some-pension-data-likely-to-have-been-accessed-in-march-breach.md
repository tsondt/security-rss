Title: Capita admits some pension data 'likely' to have been accessed in March breach
Date: 2023-05-05T11:57:29+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-05-05-capita-admits-some-pension-data-likely-to-have-been-accessed-in-march-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/05/capita_pension_data_breach/){:target="_blank" rel="noopener"}

> Weeks after outsourcer admits 'cyber incident' more warnings issued Capita is telling pension customers that some data contained within its systems was potentially accessed when criminals broke into the outsourcing giant's tech infrastructure earlier this year.... [...]
