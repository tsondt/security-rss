Title: Cisco squashes critical bugs in small biz switches
Date: 2023-05-18T22:31:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-18-cisco-squashes-critical-bugs-in-small-biz-switches

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/18/cisco_patches_small_biz_switches/){:target="_blank" rel="noopener"}

> You'll want to patch these as proof-of-concept exploit code is out there already Cisco rolled out patches for four critical security vulnerabilities in several of its network switches for small businesses that can be exploited to remotely hijack the equipment.... [...]
