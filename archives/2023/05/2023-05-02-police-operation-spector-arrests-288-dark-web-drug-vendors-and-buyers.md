Title: Police operation 'SpecTor' arrests 288 dark web drug vendors and buyers
Date: 2023-05-02T10:40:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-05-02-police-operation-spector-arrests-288-dark-web-drug-vendors-and-buyers

[Source](https://www.bleepingcomputer.com/news/security/police-operation-spector-arrests-288-dark-web-drug-vendors-and-buyers/){:target="_blank" rel="noopener"}

> An international law enforcement operation codenamed 'SpecTor' has arrested 288 dark web vendors and customers worldwide, with police seizing €50.8 million ($55.9M) in cash and cryptocurrency. [...]
