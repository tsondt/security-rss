Title: UK competition watchdog launches review of AI market
Date: 2023-05-04T13:14:28+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: Artificial intelligence (AI);ChatGPT;Competition and Markets Authority;Business;Computing;Regulators;Technology;Data and computer security;Privacy;UK news;World news;Technology sector
Slug: 2023-05-04-uk-competition-watchdog-launches-review-of-ai-market

[Source](https://www.theguardian.com/technology/2023/may/04/uk-competition-watchdog-launches-review-ai-market-artificial-intelligence){:target="_blank" rel="noopener"}

> CMA to look at underlying systems of artificial intelligence tools amid concerns over false information Business live – latest updates The UK competition watchdog has fired a shot across the bows of companies racing to commercialise artificial intelligence technology, announcing a review of the sector as fears grow over the spread of misinformation and major disruption in the jobs market. As pressure builds on global regulators to increase their scrutiny of the technology, the Competition and Markets Authority said it would look at the underlying systems, or foundation models, behind AI tools such as ChatGPT. The initial review, described by [...]
