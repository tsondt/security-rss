Title: Clever ‘File Archiver In The Browser’ phishing trick uses ZIP domains
Date: 2023-05-28T11:18:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-28-clever-file-archiver-in-the-browser-phishing-trick-uses-zip-domains

[Source](https://www.bleepingcomputer.com/news/security/clever-file-archiver-in-the-browser-phishing-trick-uses-zip-domains/){:target="_blank" rel="noopener"}

> A new 'File Archivers in the Browser' phishing kit abuses ZIP domains by displaying fake WinRAR or Windows File Explorer windows in the browser to convince users to launch malicious files. [...]
