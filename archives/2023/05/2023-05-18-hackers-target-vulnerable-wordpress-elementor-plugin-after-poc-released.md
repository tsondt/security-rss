Title: Hackers target vulnerable Wordpress Elementor plugin after PoC released
Date: 2023-05-18T12:36:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-18-hackers-target-vulnerable-wordpress-elementor-plugin-after-poc-released

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-vulnerable-wordpress-elementor-plugin-after-poc-released/){:target="_blank" rel="noopener"}

> Hackers are now actively probing for vulnerable Essential Addons for Elementor plugin versions on thousands of WordPress websites in massive Internet scans, attempting to exploit a critical account password reset flaw disclosed earlier in the month. [...]
