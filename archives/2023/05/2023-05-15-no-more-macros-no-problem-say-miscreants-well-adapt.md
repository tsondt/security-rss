Title: No more macros? No problem, say miscreants, we'll adapt
Date: 2023-05-15T16:32:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-15-no-more-macros-no-problem-say-miscreants-well-adapt

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/15/proofpoint_microsoft_macros_cybercrime/){:target="_blank" rel="noopener"}

> Microsoft blocking 'net scripts sparked 'monumental shift' in attacks Microsoft's decision to block internet-sourced macros by default last year is forcing attackers to find new and creative ways to compromise systems and deliver malware, according to threat researchers at Proofpoint.... [...]
