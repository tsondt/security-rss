Title: MCNA Dental data breach impacts 8.9 million people after ransomware attack
Date: 2023-05-29T09:49:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-29-mcna-dental-data-breach-impacts-89-million-people-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/mcna-dental-data-breach-impacts-89-million-people-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Managed Care of North America (MCNA) Dental has published a data breach notification on its website, informing almost 9 million patients that their personal data were compromised. [...]
