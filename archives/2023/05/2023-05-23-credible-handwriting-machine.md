Title: Credible Handwriting Machine
Date: 2023-05-23T11:15:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;biometrics;ChatGPT;robotics
Slug: 2023-05-23-credible-handwriting-machine

[Source](https://www.schneier.com/blog/archives/2023/05/credible-handwriting-machine.html){:target="_blank" rel="noopener"}

> In case you don’t have enough to worry about, someone has built a credible handwriting machine: This is still a work in progress, but the project seeks to solve one of the biggest problems with other homework machines, such as this one that I covered a few months ago after it blew up on social media. The problem with most homework machines is that they’re too perfect. Not only is their content output too well-written for most students, but they also have perfect grammar and punctuation ­ something even we professional writers fail to consistently achieve. Most importantly, the machine’s [...]
