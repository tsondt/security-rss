Title: Let white-hat hackers stick a probe in those voting machines, say senators
Date: 2023-05-11T21:35:50+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-11-let-white-hat-hackers-stick-a-probe-in-those-voting-machines-say-senators

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/11/us_voting_system_pen_testing/){:target="_blank" rel="noopener"}

> HAVA go at breaking electronic ballot box security US voting machines would undergo deeper examination for computer security holes under proposed bipartisan legislation.... [...]
