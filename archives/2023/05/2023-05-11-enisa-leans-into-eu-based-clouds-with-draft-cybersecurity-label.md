Title: ENISA leans into EU-based clouds with draft cybersecurity label
Date: 2023-05-11T12:44:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-11-enisa-leans-into-eu-based-clouds-with-draft-cybersecurity-label

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/11/eu_cybersecurity_label_scheme_faces/){:target="_blank" rel="noopener"}

> Time for AWS and pals to start thinking about JVs? Cloud services providers that aren't based in Europe — like the Big Three — may have to team up with a cloud that is operated and maintained from the EU if they want ENISA's stamp of approval for handling sensitive data.... [...]
