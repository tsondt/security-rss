Title: Apple, Google propose anti-stalking spec for Bluetooth tracker tags
Date: 2023-05-02T21:00:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-05-02-apple-google-propose-anti-stalking-spec-for-bluetooth-tracker-tags

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/02/apple_google_antistalking_bluetooth/){:target="_blank" rel="noopener"}

> We moved fast and broke things, people got harassed and murdered, so let's revisit privacy Apple and Google have come together to develop an industry specification to prevent "unwanted tracking," otherwise known as stalking, via Bluetooth location tracking tags.... [...]
