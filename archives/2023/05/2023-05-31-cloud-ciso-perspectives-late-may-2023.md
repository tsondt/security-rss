Title: Cloud CISO Perspectives: Late May 2023
Date: 2023-05-31T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2023-05-31-cloud-ciso-perspectives-late-may-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-late-may-2023/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for May 2023. I hope you all enjoyed our previous newsletter from my Office of the CISO colleague MK Palmore, on Google’s new cybersecurity certification and how it can help prepare aspiring cybersecurity experts for their next career steps. Before I jump into my column today, I’d like to encourage everyone to sign up for our annual Security Summit, coming in just a few weeks on June 13-14. This year, we’ll explore the latest technologies and strategies from Google Cloud, Mandiant, and our partners to help protect your business, your customers, and your [...]
