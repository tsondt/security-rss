Title: New ZIP domains spark debate among cybersecurity experts
Date: 2023-05-16T18:48:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Google;Technology
Slug: 2023-05-16-new-zip-domains-spark-debate-among-cybersecurity-experts

[Source](https://www.bleepingcomputer.com/news/security/new-zip-domains-spark-debate-among-cybersecurity-experts/){:target="_blank" rel="noopener"}

> Cybersecurity researchers and IT admins have raised concerns over Google's new ZIP and MOV Internet domains, warning that threat actors could use them for phishing attacks and malware delivery. [...]
