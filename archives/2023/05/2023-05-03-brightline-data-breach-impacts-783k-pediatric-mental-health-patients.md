Title: Brightline data breach impacts 783K pediatric mental health patients
Date: 2023-05-03T10:33:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-03-brightline-data-breach-impacts-783k-pediatric-mental-health-patients

[Source](https://www.bleepingcomputer.com/news/security/brightline-data-breach-impacts-783k-pediatric-mental-health-patients/){:target="_blank" rel="noopener"}

> Pediatric mental health provider Brightline is warning patients that it suffered a data breach impacting 783,606 people after a ransomware gang stole data using a zero-day vulnerability in its Fortra GoAnywhere MFT secure file-sharing platform. [...]
