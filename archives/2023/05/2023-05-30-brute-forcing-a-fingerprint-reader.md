Title: Brute-Forcing a Fingerprint Reader
Date: 2023-05-30T11:16:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;authentication;cracking;fingerprints;smartphones
Slug: 2023-05-30-brute-forcing-a-fingerprint-reader

[Source](https://www.schneier.com/blog/archives/2023/05/brute-forcing-a-fingerprint-reader.html){:target="_blank" rel="noopener"}

> It’s neither hard nor expensive : Unlike password authentication, which requires a direct match between what is inputted and what’s stored in a database, fingerprint authentication determines a match using a reference threshold. As a result, a successful fingerprint brute-force attack requires only that an inputted image provides an acceptable approximation of an image in the fingerprint database. BrutePrint manipulates the false acceptance rate (FAR) to increase the threshold so fewer approximate images are accepted. BrutePrint acts as an adversary in the middle between the fingerprint sensor and the trusted execution environment and exploits vulnerabilities that allow for unlimited guesses. [...]
