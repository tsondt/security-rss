Title: Spanish police dismantle phishing operation linked to crime ring
Date: 2023-05-09T17:29:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-09-spanish-police-dismantle-phishing-operation-linked-to-crime-ring

[Source](https://www.bleepingcomputer.com/news/security/spanish-police-dismantle-phishing-operation-linked-to-crime-ring/){:target="_blank" rel="noopener"}

> The National Police of Spain have arrested two hackers, 15 members of a criminal organization, and another 23 people involved in illegal financial operations in Madrid and Seville for alleged bank scams. [...]
