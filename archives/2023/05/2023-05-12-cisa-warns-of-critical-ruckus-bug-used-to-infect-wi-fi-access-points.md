Title: CISA warns of critical Ruckus bug used to infect Wi-Fi access points
Date: 2023-05-12T13:43:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-12-cisa-warns-of-critical-ruckus-bug-used-to-infect-wi-fi-access-points

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-of-critical-ruckus-bug-used-to-infect-wi-fi-access-points/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) warned today of a critical remote code execution (RCE) flaw in the Ruckus Wireless Admin panel actively exploited by a recently discovered DDoS botnet. [...]
