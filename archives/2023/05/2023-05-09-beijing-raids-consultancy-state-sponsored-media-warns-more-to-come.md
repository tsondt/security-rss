Title: Beijing raids consultancy, State-sponsored media warns more to come
Date: 2023-05-09T07:40:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-05-09-beijing-raids-consultancy-state-sponsored-media-warns-more-to-come

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/09/beijing_raids_consultancy_firm_capvision/){:target="_blank" rel="noopener"}

> Retaliation or national security? Beijing sent a message to foreign businesses this week when it launched an investigation into Shanghai-based Capvision Partners on the grounds of national security, accusing the consultancy firm of failure to prevent espionage.... [...]
