Title: Modern Auth comes to on-prem Exchange Server gear
Date: 2023-05-08T16:15:15+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-08-modern-auth-comes-to-on-prem-exchange-server-gear

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/08/microsoft_exchange_server_authentication/){:target="_blank" rel="noopener"}

> Guess this'll have to do while we wait for *checks notes* ES 2025 Microsoft last year said that it was putting off the next version of Exchange Server until the second half of 2025 so engineers could continue bulking up the security of a product that has become a popular target of cybercriminals.... [...]
