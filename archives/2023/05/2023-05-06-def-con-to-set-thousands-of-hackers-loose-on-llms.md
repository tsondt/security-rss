Title: DEF CON to set thousands of hackers loose on LLMs
Date: 2023-05-06T17:20:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-06-def-con-to-set-thousands-of-hackers-loose-on-llms

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/06/ai_hacking_defcon/){:target="_blank" rel="noopener"}

> Can't wait to see how these AI models hold up against a weekend of red-teaming by infosec's village people This year's DEF CON AI Village has invited hackers to show up, dive in, and find bugs and biases in large language models (LLMs) built by OpenAI, Google, Anthropic, and others.... [...]
