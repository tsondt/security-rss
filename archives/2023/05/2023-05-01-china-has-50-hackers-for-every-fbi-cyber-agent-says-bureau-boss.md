Title: China has 50 hackers for every FBI cyber agent, says Bureau boss
Date: 2023-05-01T02:32:19+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-05-01-china-has-50-hackers-for-every-fbi-cyber-agent-says-bureau-boss

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/01/fbi_director_wray_china_testimony/){:target="_blank" rel="noopener"}

> Combatting it is going to take more money. Lots of more money. China has 50 hackers for every one of the FBI's cyber-centric agents, the Bureau's director told a congressional committee last week.... [...]
