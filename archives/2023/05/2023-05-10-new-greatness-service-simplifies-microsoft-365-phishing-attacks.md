Title: New 'Greatness' service simplifies Microsoft 365 phishing attacks
Date: 2023-05-10T08:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-05-10-new-greatness-service-simplifies-microsoft-365-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-greatness-service-simplifies-microsoft-365-phishing-attacks/){:target="_blank" rel="noopener"}

> The Phishing-as-a-Service (PhaaS) platform named 'Greatness' has seen a spike in activity as it targets organizations using Microsoft 365 in the United States, Canada, the U.K., Australia, and South Africa. [...]
