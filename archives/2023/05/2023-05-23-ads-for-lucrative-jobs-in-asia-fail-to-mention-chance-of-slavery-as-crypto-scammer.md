Title: Ads for lucrative jobs in Asia fail to mention chance of slavery as crypto-scammer
Date: 2023-05-23T05:58:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-05-23-ads-for-lucrative-jobs-in-asia-fail-to-mention-chance-of-slavery-as-crypto-scammer

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/23/fbi_warns_on_asian_jobs/){:target="_blank" rel="noopener"}

> FBI warns jobseekers to be very skeptical of working holidays in Cambodia The FBI has issued a warning about fake job ads that recruit workers into forced labor operations in Southeast Asia – some of which enslave visitors and force them to participate in cryptocurrency scams.... [...]
