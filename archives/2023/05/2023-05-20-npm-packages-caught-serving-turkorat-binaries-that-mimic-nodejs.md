Title: npm packages caught serving TurkoRAT binaries that mimic NodeJS
Date: 2023-05-20T09:06:20-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-05-20-npm-packages-caught-serving-turkorat-binaries-that-mimic-nodejs

[Source](https://www.bleepingcomputer.com/news/security/npm-packages-caught-serving-turkorat-binaries-that-mimic-nodejs/){:target="_blank" rel="noopener"}

> Researchers have discovered multiple npm packages named after NodeJS libraries that even pack a Windows executable that resembles NodeJS but instead drops a sinister trojan. [...]
