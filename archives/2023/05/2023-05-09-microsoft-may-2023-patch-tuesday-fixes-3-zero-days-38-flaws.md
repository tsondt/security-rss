Title: Microsoft May 2023 Patch Tuesday fixes 3 zero-days, 38 flaws
Date: 2023-05-09T13:50:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-05-09-microsoft-may-2023-patch-tuesday-fixes-3-zero-days-38-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-may-2023-patch-tuesday-fixes-3-zero-days-38-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's May 2023 Patch Tuesday, and security updates fix three zero-day vulnerabilities and a total of 38 flaws. [...]
