Title: Partnering with Health-ISAC to strengthen the European healthcare system
Date: 2023-05-22T16:00:00+00:00
Author: Taylor Lehmann
Category: GCP Security
Tags: Security & Identity
Slug: 2023-05-22-partnering-with-health-isac-to-strengthen-the-european-healthcare-system

[Source](https://cloud.google.com/blog/products/identity-security/partnering-with-health-isac-to-strengthen-the-european-healthcare-system/){:target="_blank" rel="noopener"}

> Last July, Google Cloud launched our ambassador partnership with the Health Information Sharing and Analysis Center (Health-ISAC) and committed to working with industry leaders to better protect our healthcare ecosystem. Securing healthcare technology and data is a global challenge, and to meet it security professionals need to have better channels for sharing information and effective security practices. To that end, we’re pleased to announce that our relationship with Health-ISAC is now expanding to include CISOs and security leaders in Europe, the Middle East, and Africa (EMEA). On May 23, 2023, we’ll join the Health-ISAC on a 17-city tour across the [...]
