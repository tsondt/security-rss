Title: Terminator antivirus killer is a vulnerable Windows driver in disguise
Date: 2023-05-31T15:25:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-31-terminator-antivirus-killer-is-a-vulnerable-windows-driver-in-disguise

[Source](https://www.bleepingcomputer.com/news/security/terminator-antivirus-killer-is-a-vulnerable-windows-driver-in-disguise/){:target="_blank" rel="noopener"}

> A threat actor known as Spyboy is promoting a Windows defense evasion tool called "Terminator" on the Russian-speaking forum RAMP (short for Russian Anonymous Marketplace). [...]
