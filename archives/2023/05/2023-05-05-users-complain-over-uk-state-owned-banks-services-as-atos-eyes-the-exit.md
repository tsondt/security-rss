Title: Users complain over UK state-owned bank's services as Atos eyes the exit
Date: 2023-05-05T08:30:12+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-05-05-users-complain-over-uk-state-owned-banks-services-as-atos-eyes-the-exit

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/05/uk_nationalised_bank_hit_with_complaint/){:target="_blank" rel="noopener"}

> National Savings & Investment contracting for massive tech deals as customers complain of 2FA failure Updated The UK National Savings and Investment bank is being bombarded with complaints over failing online security and authentication features which customers say have locked them out of their accounts.... [...]
