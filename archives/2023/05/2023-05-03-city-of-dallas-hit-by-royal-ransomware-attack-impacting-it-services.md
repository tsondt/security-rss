Title: City of Dallas hit by Royal ransomware attack impacting IT services
Date: 2023-05-03T18:13:55-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-03-city-of-dallas-hit-by-royal-ransomware-attack-impacting-it-services

[Source](https://www.bleepingcomputer.com/news/security/city-of-dallas-hit-by-royal-ransomware-attack-impacting-it-services/){:target="_blank" rel="noopener"}

> The City of Dallas, Texas, has suffered a Royal ransomware attack, causing it to shut down some of its IT systems to prevent the attack's spread. [...]
