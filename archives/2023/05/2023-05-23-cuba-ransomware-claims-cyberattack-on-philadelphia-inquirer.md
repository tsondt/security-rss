Title: Cuba ransomware claims cyberattack on Philadelphia Inquirer
Date: 2023-05-23T09:54:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-23-cuba-ransomware-claims-cyberattack-on-philadelphia-inquirer

[Source](https://www.bleepingcomputer.com/news/security/cuba-ransomware-claims-cyberattack-on-philadelphia-inquirer/){:target="_blank" rel="noopener"}

> The Cuba ransomware gang has claimed responsibility for this month's cyberattack on The Philadelphia Inquirer, which temporarily disrupted the newspaper's distribution and disrupted some business operations. [...]
