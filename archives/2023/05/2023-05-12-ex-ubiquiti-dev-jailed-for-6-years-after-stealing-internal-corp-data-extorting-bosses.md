Title: Ex-Ubiquiti dev jailed for 6 years after stealing internal corp data, extorting bosses
Date: 2023-05-12T20:28:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-12-ex-ubiquiti-dev-jailed-for-6-years-after-stealing-internal-corp-data-extorting-bosses

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/12/exubiquiti_developer_jailed/){:target="_blank" rel="noopener"}

> Momentary lapse in VPN led to stretch in the cooler, $1.6m bill Nickolas Sharp has been sentenced to six years in prison and ordered to pay almost $1.6 million to his now-former employer Ubiquiti – after stealing gigabytes of corporate data from the biz and then trying to extort almost $2 million from his bosses while posing as an anonymous hacker.... [...]
