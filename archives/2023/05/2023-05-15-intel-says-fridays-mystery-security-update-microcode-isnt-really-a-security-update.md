Title: Intel says Friday's mystery 'security update' microcode isn't really a security update
Date: 2023-05-15T22:15:16+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2023-05-15-intel-says-fridays-mystery-security-update-microcode-isnt-really-a-security-update

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/15/intel_mystery_microcode/){:target="_blank" rel="noopener"}

> We're all for encouraging people to squash bugs but this is an odd way to do it False alarm: despite a patch notes suggesting otherwise, that mysterious blob of microcode released for many Intel microprocessors last week was not a security update, the x86 giant says.... [...]
