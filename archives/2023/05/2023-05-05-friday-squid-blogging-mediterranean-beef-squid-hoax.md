Title: Friday Squid Blogging: “Mediterranean Beef Squid” Hoax
Date: 2023-05-05T21:12:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hoaxes;squid
Slug: 2023-05-05-friday-squid-blogging-mediterranean-beef-squid-hoax

[Source](https://www.schneier.com/blog/archives/2023/05/friday-squid-blogging-mediterranean-beef-squid-hoax.html){:target="_blank" rel="noopener"}

> The viral video of the “Mediterranean beef squid”is a hoax. It’s not even a deep fake; it’s a plastic toy. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
