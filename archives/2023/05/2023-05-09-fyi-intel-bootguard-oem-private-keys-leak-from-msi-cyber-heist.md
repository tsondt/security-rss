Title: FYI: Intel BootGuard OEM private keys leak from MSI cyber heist
Date: 2023-05-09T02:27:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-09-fyi-intel-bootguard-oem-private-keys-leak-from-msi-cyber-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/09/intel_oem_private_keys_leaked/){:target="_blank" rel="noopener"}

> Plus: Court-ordered domain seizures of DDoS-for-hire sites Updated Intel is investigating reports that BootGuard private keys, used to protect PCs from hidden malware, were leaked when data belonging to Micro-Star International (MSI) was stolen and dumped online.... [...]
