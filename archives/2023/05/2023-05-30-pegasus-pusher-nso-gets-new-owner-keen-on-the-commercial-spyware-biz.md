Title: Pegasus-pusher NSO gets new owner keen on the commercial spyware biz
Date: 2023-05-30T19:15:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-30-pegasus-pusher-nso-gets-new-owner-keen-on-the-commercial-spyware-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/30/nso_owner_hacking/){:target="_blank" rel="noopener"}

> Investors roll the dice against government sanctions and lawsuits Spyware maker NSO Group has a new ringleader, as the notorious biz seeks to revamp its image amid new reports that the company's Pegasus malware is targeting yet more human rights advocates and journalists.... [...]
