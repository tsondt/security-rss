Title: New asset query simplifies asset inventory management in Security Command Center
Date: 2023-05-04T16:00:00+00:00
Author: Sunny Rissland
Category: GCP Security
Tags: Security & Identity
Slug: 2023-05-04-new-asset-query-simplifies-asset-inventory-management-in-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/new-asset-query-simplifies-asset-inventory-management-in-security-command-center/){:target="_blank" rel="noopener"}

> As our cloud customers scale their environments, they need to manage cloud resources and policies. Our biggest customers have millions of assets in their Google Cloud environments. Securing growing environments requires tools to help discover, monitor, and secure cloud assets. To help, Security Command Center (SCC), our security and risk management solution, now includes new asset query functionality designed to make it easier for IT and security teams to identify assets in large, complex environments. Security Command Center users can now perform SQL-like queries to get detailed information on where assets are located and how they are configured. This includes [...]
