Title: It's 2023 and Sri Lanka doesn't have a cyber security authority
Date: 2023-05-26T00:42:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-05-26-its-2023-and-sri-lanka-doesnt-have-a-cyber-security-authority

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/26/sri_lanka_cybersecurity_authority/){:target="_blank" rel="noopener"}

> All should change this year as the country passes its Cyber Security Bill Sri Lanka's Ministry of Technology has confirmed it will have a cyber security authority – at some point.... [...]
