Title: Labor to appoint dedicated privacy commissioner to combat data breaches
Date: 2023-05-02T11:00:49+00:00
Author: Stephanie Convery
Category: The Guardian
Tags: Australia news;Privacy;Australian politics;Labor party;Data and computer security
Slug: 2023-05-02-labor-to-appoint-dedicated-privacy-commissioner-to-combat-data-breaches

[Source](https://www.theguardian.com/world/2023/may/02/labor-to-appoint-dedicated-privacy-commissioner-to-combat-data-breaches){:target="_blank" rel="noopener"}

> The Office of the Australian Information Commissioner will also be restored to a three-commissioner structure after defunding by Coalition Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast The federal government will appoint a dedicated privacy commissioner to deal with the increasing threat of data breaches, the attorney general has announced. Mark Dreyfus revealed late on Tuesday evening that the Albanese government would also restore the Office of the Australian Information Commissioner (OAIC) to a three-commissioner structure, saying the appointments were necessary to deal with “the [...]
