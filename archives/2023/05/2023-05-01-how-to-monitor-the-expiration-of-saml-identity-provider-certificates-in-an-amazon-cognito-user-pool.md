Title: How to monitor the expiration of SAML identity provider certificates in an Amazon Cognito user pool
Date: 2023-05-01T19:22:10+00:00
Author: Karthik Nagarajan
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon Cognito;Identity;SAML;Security Blog
Slug: 2023-05-01-how-to-monitor-the-expiration-of-saml-identity-provider-certificates-in-an-amazon-cognito-user-pool

[Source](https://aws.amazon.com/blogs/security/how-to-monitor-the-expiration-of-saml-identity-provider-certificates-in-an-amazon-cognito-user-pool/){:target="_blank" rel="noopener"}

> With Amazon Cognito user pools, you can configure third-party SAML identity providers (IdPs) so that users can log in by using the IdP credentials. The Amazon Cognito user pool manages the federation and handling of tokens returned by a configured SAML IdP. It uses the public certificate of the SAML IdP to verify the signature [...]
