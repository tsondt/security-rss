Title: On the Poisoning of LLMs
Date: 2023-05-25T11:05:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;ChatGPT;hacking;secrecy;snake oil
Slug: 2023-05-25-on-the-poisoning-of-llms

[Source](https://www.schneier.com/blog/archives/2023/05/on-the-poisoning-of-llms.html){:target="_blank" rel="noopener"}

> Interesting essay on the poisoning of LLMs—ChatGPT in particular: Given that we’ve known about model poisoning for years, and given the strong incentives the black-hat SEO crowd has to manipulate results, it’s entirely possible that bad actors have been poisoning ChatGPT for months. We don’t know because OpenAI doesn’t talk about their processes, how they validate the prompts they use for training, how they vet their training data set, or how they fine-tune ChatGPT. Their secrecy means we don’t know if ChatGPT has been safely managed. They’ll also have to update their training data set at some point. They can’t [...]
