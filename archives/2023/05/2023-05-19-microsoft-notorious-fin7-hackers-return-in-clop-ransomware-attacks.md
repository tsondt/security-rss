Title: Microsoft: Notorious FIN7 hackers return in Clop ransomware attacks
Date: 2023-05-19T13:06:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-05-19-microsoft-notorious-fin7-hackers-return-in-clop-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-notorious-fin7-hackers-return-in-clop-ransomware-attacks/){:target="_blank" rel="noopener"}

> A financially motivated cybercriminal group known as FIN7 resurfaced last month, with Microsoft threat analysts linking it to attacks where the end goal was the deployment of Clop ransomware payloads on victims' networks. [...]
