Title: GitHub now auto-blocks token and API key leaks for all repos
Date: 2023-05-09T17:42:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-09-github-now-auto-blocks-token-and-api-key-leaks-for-all-repos

[Source](https://www.bleepingcomputer.com/news/security/github-now-auto-blocks-token-and-api-key-leaks-for-all-repos/){:target="_blank" rel="noopener"}

> GitHub is now automatically blocking the leak of sensitive information like API keys and access tokens for all public code repositories. [...]
