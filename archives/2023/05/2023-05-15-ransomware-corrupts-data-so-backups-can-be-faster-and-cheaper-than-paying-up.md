Title: Ransomware corrupts data, so backups can be faster and cheaper than paying up
Date: 2023-05-15T06:32:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-05-15-ransomware-corrupts-data-so-backups-can-be-faster-and-cheaper-than-paying-up

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/15/ransomware_corrupts_data/){:target="_blank" rel="noopener"}

> Smash and grab raids don’t leave time for careful encryption Ransomware actors aim to spend the shortest amount of time possible inside your systems, and that means the encryption they employ is shoddy and often corrupts your data. That in turn means restoration after paying ransoms is often a more expensive chore than just deciding not to pay and working from our own backups.... [...]
