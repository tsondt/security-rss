Title: Some potential: How bad software updates could over-volt, brick remote servers
Date: 2023-05-15T18:39:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-05-15-some-potential-how-bad-software-updates-could-over-volt-brick-remote-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/15/pmfault_attack/){:target="_blank" rel="noopener"}

> PMFault – from the eggheads who brought you Plundervolt and Voltpillager Video Presenting at Black Hat Asia 2023, two infosec researchers detailed how remote updates can be exploited to modify voltage on a Supermicro motherboard and remotely brick machines.... [...]
