Title: Customer checklist for eIDAS regulation now available
Date: 2023-05-10T00:51:41+00:00
Author: Borja Larrumbide
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;European Union;Security Blog
Slug: 2023-05-10-customer-checklist-for-eidas-regulation-now-available

[Source](https://aws.amazon.com/blogs/security/customer-checklist-for-eidas-regulation-now-available/){:target="_blank" rel="noopener"}

> AWS is pleased to announce the publication of a checklist to help customers align with the requirements of the European Union’s electronic identification, authentication, and trust services (eIDAS) regulation regarding the use of electronic identities and trust services. The eIDAS regulation covers electronic identification and trust services for electronic transactions in the European single market. [...]
