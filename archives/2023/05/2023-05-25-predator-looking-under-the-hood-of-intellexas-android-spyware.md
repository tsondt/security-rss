Title: Predator: Looking under the hood of Intellexa’s Android spyware
Date: 2023-05-25T14:57:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-05-25-predator-looking-under-the-hood-of-intellexas-android-spyware

[Source](https://www.bleepingcomputer.com/news/security/predator-looking-under-the-hood-of-intellexas-android-spyware/){:target="_blank" rel="noopener"}

> Security researchers at Cisco Talos and the Citizen Lab have presented a new technical analysis of the commercial Android spyware 'Predator' and its loader 'Alien,' sharing its data-theft capabilities and other operational details. [...]
