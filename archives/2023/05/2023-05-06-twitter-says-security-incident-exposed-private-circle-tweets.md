Title: Twitter says 'security incident' exposed private Circle tweets
Date: 2023-05-06T12:29:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-05-06-twitter-says-security-incident-exposed-private-circle-tweets

[Source](https://www.bleepingcomputer.com/news/security/twitter-says-security-incident-exposed-private-circle-tweets/){:target="_blank" rel="noopener"}

> Twitter disclosed that a 'security incident' caused private tweets sent to Twitter Circles to show publicly to users outside of the Circle. [...]
