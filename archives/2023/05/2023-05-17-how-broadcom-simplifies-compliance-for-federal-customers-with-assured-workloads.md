Title: How Broadcom simplifies compliance for federal customers with Assured Workloads
Date: 2023-05-17T16:00:00+00:00
Author: Ganesh Janakiraman
Category: GCP Security
Tags: Security & Identity
Slug: 2023-05-17-how-broadcom-simplifies-compliance-for-federal-customers-with-assured-workloads

[Source](https://cloud.google.com/blog/products/identity-security/how-broadcom-simplifies-compliance-for-federal-customers-with-assured-workloads/){:target="_blank" rel="noopener"}

> Broadcom is a global technology leader that designs, develops, and supplies many semiconductor and infrastructure software solutions. Broadcom’s category-leading product portfolio serves critical markets including data center, networking, software, broadband, wireless, storage, and industrial sectors. Our customers, many of whom operate in the federal public sector, need ready-made SaaS solutions to secure their infrastructure while meeting compliance requirements. As head of platform engineering for Broadcom and its subsidiaries, my team is responsible for designing, testing, and building the common orchestration platform and services for our federal customers. Since early 2021, we’ve partnered with Google Cloud to deliver best-in-class cloud solutions [...]
