Title: FBI seizes 9 crypto exchanges used to launder ransomware payments
Date: 2023-05-02T15:52:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Legal
Slug: 2023-05-02-fbi-seizes-9-crypto-exchanges-used-to-launder-ransomware-payments

[Source](https://www.bleepingcomputer.com/news/security/fbi-seizes-9-crypto-exchanges-used-to-launder-ransomware-payments/){:target="_blank" rel="noopener"}

> The FBI and Ukrainian police have seized nine cryptocurrency exchange websites that facilitated money laundering for scammers and cybercriminals, including ransomware actors. [...]
