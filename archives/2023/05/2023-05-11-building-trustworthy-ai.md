Title: Building Trustworthy AI
Date: 2023-05-11T11:17:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;risks;trust
Slug: 2023-05-11-building-trustworthy-ai

[Source](https://www.schneier.com/blog/archives/2023/05/building-trustworthy-ai.html){:target="_blank" rel="noopener"}

> We will all soon get into the habit of using AI tools for help with everyday problems and tasks. We should get in the habit of questioning the motives, incentives, and capabilities behind them, too. Imagine you’re using an AI chatbot to plan a vacation. Did it suggest a particular resort because it knows your preferences, or because the company is getting a kickback from the hotel chain? Later, when you’re using another AI chatbot to learn about a complex economic issue, is the chatbot reflecting your politics or the politics of the company that trained it? For AI to [...]
