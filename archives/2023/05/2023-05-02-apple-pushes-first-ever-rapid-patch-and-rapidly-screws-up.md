Title: Apple pushes first-ever 'rapid' patch – and rapidly screws up
Date: 2023-05-02T23:30:43+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-02-apple-pushes-first-ever-rapid-patch-and-rapidly-screws-up

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/02/apple_rapid_patch/){:target="_blank" rel="noopener"}

> Maybe you're just installing it wrong? Apple on Monday pushed to some iPhones and Macs its first-ever rapid security fix.... [...]
