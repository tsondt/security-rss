Title: Phishing Domains Tanked After Meta Sued Freenom
Date: 2023-05-26T16:37:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;AWPG Ecrime Exchange;Dave Piscitello;Freenom;Interisle Consulting Group;Mastodon;Meta;Namecheap;OpenPhish;phishtank;spamhaus
Slug: 2023-05-26-phishing-domains-tanked-after-meta-sued-freenom

[Source](https://krebsonsecurity.com/2023/05/phishing-domains-tanked-after-meta-sued-freenom/){:target="_blank" rel="noopener"}

> The number of phishing websites tied to domain name registrar Freenom dropped precipitously in the months surrounding a recent lawsuit from social networking giant Meta, which alleged the free domain name provider has a long history of ignoring abuse complaints about phishing websites while monetizing traffic to those abusive domains. The volume of phishing websites registered through Freenom dropped considerably since the registrar was sued by Meta. Image: Interisle Consulting. Freenom is the domain name registry service provider for five so-called “country code top level domains” (ccTLDs), including.cf for the Central African Republic;.ga for Gabon;.gq for Equatorial Guinea;.ml for Mali; [...]
