Title: 'Top three Balkans drug kingpins' arrested after cops crack their Sky ECC chats
Date: 2023-05-13T07:14:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-13-top-three-balkans-drug-kingpins-arrested-after-cops-crack-their-sky-ecc-chats

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/13/drug_arrests_sky_ecc/){:target="_blank" rel="noopener"}

> Maybe try carrier pigeons instead European police arrested three people in Belgrade described as "the biggest" drug lords in the Balkans in what cops are chalking up to another win in dismantling Sky ECC's encrypted messaging app last year.... [...]
