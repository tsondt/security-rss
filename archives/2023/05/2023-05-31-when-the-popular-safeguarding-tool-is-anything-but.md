Title: When the popular safeguarding tool is anything but
Date: 2023-05-31T13:11:15+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-05-31-when-the-popular-safeguarding-tool-is-anything-but

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/31/when_the_popular_safeguarding_tool/){:target="_blank" rel="noopener"}

> How to stave off software supply chain attacks Webinar A software supply chain attack is a hugely painful form of infiltration which can paralyse any business or organization. An attack like a lethal snake bite where the poison silently and swiftly infects your whole software base.... [...]
