Title: Iranian hackers use new Moneybird ransomware to attack Israeli orgs
Date: 2023-05-24T12:28:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-24-iranian-hackers-use-new-moneybird-ransomware-to-attack-israeli-orgs

[Source](https://www.bleepingcomputer.com/news/security/iranian-hackers-use-new-moneybird-ransomware-to-attack-israeli-orgs/){:target="_blank" rel="noopener"}

> A suspected Iranian state-supported threat actor known as 'Agrius' is now deploying a new ransomware strain named 'Moneybird' against Israeli organizations. [...]
