Title: ASUS routers knocked offline worldwide by bad security update
Date: 2023-05-19T12:11:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Hardware;Security;Technology
Slug: 2023-05-19-asus-routers-knocked-offline-worldwide-by-bad-security-update

[Source](https://www.bleepingcomputer.com/news/hardware/asus-routers-knocked-offline-worldwide-by-bad-security-update/){:target="_blank" rel="noopener"}

> ASUS has apologized to its customers for a server-side security maintenance error that has caused a wide range of impacted router models to lose network connectivity. [...]
