Title: New ransomware decryptor recovers data from partially encrypted files
Date: 2023-05-10T12:16:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-10-new-ransomware-decryptor-recovers-data-from-partially-encrypted-files

[Source](https://www.bleepingcomputer.com/news/security/new-ransomware-decryptor-recovers-data-from-partially-encrypted-files/){:target="_blank" rel="noopener"}

> Security researchers have shared a new Python-based ransomware recovery tool named 'White Phoenix' on GitHub, which lets victims of ransomware strains that use intermittent encryption recover their files for free. [...]
