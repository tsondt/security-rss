Title: T-Mobile discloses second data breach since the start of 2023
Date: 2023-05-01T13:28:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-01-t-mobile-discloses-second-data-breach-since-the-start-of-2023

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-discloses-second-data-breach-since-the-start-of-2023/){:target="_blank" rel="noopener"}

> T-Mobile disclosed the second data breach of 2023 after discovering that attackers had access to the personal information of hundreds of customers for more than a month, starting late February 2023. [...]
