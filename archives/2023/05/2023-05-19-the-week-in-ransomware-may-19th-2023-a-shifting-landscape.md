Title: The Week in Ransomware - May 19th 2023 - A Shifting Landscape
Date: 2023-05-19T16:27:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-05-19-the-week-in-ransomware-may-19th-2023-a-shifting-landscape

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-19th-2023-a-shifting-landscape/){:target="_blank" rel="noopener"}

> In the ever-shifting ransomware landscape, we saw new ransomware gangs emerge, threat actors return from a long absence, operations shifting extortion tactics, and a flurry of attacks on the enterprise. [...]
