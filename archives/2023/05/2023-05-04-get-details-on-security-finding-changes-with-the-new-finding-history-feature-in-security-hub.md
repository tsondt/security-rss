Title: Get details on security finding changes with the new Finding History feature in Security Hub
Date: 2023-05-04T22:16:51+00:00
Author: Nicholas Jaeger
Category: AWS Security
Tags: Announcements;Best Practices;Foundational (100);Security, Identity, & Compliance;AWS Security Hub;Security;Security Blog
Slug: 2023-05-04-get-details-on-security-finding-changes-with-the-new-finding-history-feature-in-security-hub

[Source](https://aws.amazon.com/blogs/security/get-details-on-security-finding-changes-with-the-new-finding-history-feature-in-security-hub/){:target="_blank" rel="noopener"}

> In today’s evolving security threat landscape, security teams increasingly require tools to detect and track security findings to protect their organizations’ assets. One objective of cloud security posture management is to identify and address security findings in a timely and effective manner. AWS Security Hub aggregates, organizes, and prioritizes security alerts and findings from various [...]
