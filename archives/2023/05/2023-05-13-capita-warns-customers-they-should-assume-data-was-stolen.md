Title: Capita warns customers they should assume data was stolen
Date: 2023-05-13T10:05:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-13-capita-warns-customers-they-should-assume-data-was-stolen

[Source](https://www.bleepingcomputer.com/news/security/capita-warns-customers-they-should-assume-data-was-stolen/){:target="_blank" rel="noopener"}

> Business process outsourcing firm Capita is warning customers to assume that their data was stolen in a cyberattack that affected its systems in early April. [...]
