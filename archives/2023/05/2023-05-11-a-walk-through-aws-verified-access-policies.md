Title: A walk through AWS Verified Access policies
Date: 2023-05-11T19:34:17+00:00
Author: Riggs Goodman III
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Amazon Verified Access;Amazon VPC;Network and content delivery;Security;Security Blog
Slug: 2023-05-11-a-walk-through-aws-verified-access-policies

[Source](https://aws.amazon.com/blogs/security/a-walk-through-aws-verified-access-policies/){:target="_blank" rel="noopener"}

> AWS Verified Access helps improve your organization’s security posture by using security trust providers to grant access to applications. This service grants access to applications only when the user’s identity and the user’s device meet configured security requirements. In this blog post, we will provide an overview of trust providers and policies, then walk through [...]
