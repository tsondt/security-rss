Title: IT giant Bitmarck shuts down customer, internal systems after cyberattack
Date: 2023-05-01T18:55:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-01-it-giant-bitmarck-shuts-down-customer-internal-systems-after-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/01/bitmarck_data_breach/){:target="_blank" rel="noopener"}

> Patient data 'was and is never endangered', says medical tech slinger German IT services provider Bitmarck has shut down all of its customer and internal systems, including entire datacenters in some cases, following a cyberattack.... [...]
