Title: Luxottica confirms 2021 data breach after info of 70M leaks online
Date: 2023-05-19T09:37:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-19-luxottica-confirms-2021-data-breach-after-info-of-70m-leaks-online

[Source](https://www.bleepingcomputer.com/news/security/luxottica-confirms-2021-data-breach-after-info-of-70m-leaks-online/){:target="_blank" rel="noopener"}

> Luxottica has confirmed one of its partners suffered a data breach in 2021 that exposed the personal information of 70 million customers after a database was posted this month for free on hacking forums. [...]
