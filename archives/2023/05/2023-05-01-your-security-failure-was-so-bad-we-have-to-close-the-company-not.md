Title: Your security failure was so bad we have to close the company … NOT!
Date: 2023-05-01T07:31:14+00:00
Author: Matthew JC Powell
Category: The Register
Tags: 
Slug: 2023-05-01-your-security-failure-was-so-bad-we-have-to-close-the-company-not

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/01/who_me/){:target="_blank" rel="noopener"}

> There are pranks, and savage pranks, and this prank when the CTO and HR ganged up on a very stressed techie Who, Me? Welcome once again, gentle reader, to the safe space we call Who, Me? in which Reg readers can confess to the naughty or not-quite-competent things they did at work, knowing they will not be judged.... [...]
