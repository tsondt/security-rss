Title: Chinese hackers breach US critical infrastructure in stealthy attacks
Date: 2023-05-24T16:43:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-05-24-chinese-hackers-breach-us-critical-infrastructure-in-stealthy-attacks

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-breach-us-critical-infrastructure-in-stealthy-attacks/){:target="_blank" rel="noopener"}

> Microsoft says a Chinese cyberespionage group it tracks as Volt Typhoon has been targeting critical infrastructure organizations across the United States, including Guam, since at least mid-2021. [...]
