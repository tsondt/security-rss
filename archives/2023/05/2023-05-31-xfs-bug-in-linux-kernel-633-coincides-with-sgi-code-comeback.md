Title: XFS bug in Linux kernel 6.3.3 coincides with SGI code comeback
Date: 2023-05-31T13:30:11+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2023-05-31-xfs-bug-in-linux-kernel-633-coincides-with-sgi-code-comeback

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/31/bugs_in_ex_sgi_xfs/){:target="_blank" rel="noopener"}

> G.N.U. Silicon Graphics: a company is not dead while its name is still spoken SGI may be no more but people are still using its code – and some more of that code may be about to enjoy a revival.... [...]
