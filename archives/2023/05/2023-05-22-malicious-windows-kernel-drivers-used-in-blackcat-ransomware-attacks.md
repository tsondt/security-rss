Title: Malicious Windows kernel drivers used in BlackCat ransomware attacks
Date: 2023-05-22T14:23:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-22-malicious-windows-kernel-drivers-used-in-blackcat-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/malicious-windows-kernel-drivers-used-in-blackcat-ransomware-attacks/){:target="_blank" rel="noopener"}

> The ALPHV ransomware group (aka BlackCat) was observed employing signed malicious Windows kernel drivers to evade detection by security software during attacks. [...]
