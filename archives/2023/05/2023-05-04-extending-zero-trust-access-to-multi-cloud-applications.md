Title: Extending Zero Trust access to multi-cloud applications
Date: 2023-05-04T16:00:00+00:00
Author: Rahul Ramachandran
Category: GCP Security
Tags: Security & Identity
Slug: 2023-05-04-extending-zero-trust-access-to-multi-cloud-applications

[Source](https://cloud.google.com/blog/products/identity-security/now-extending-zero-trust-security-to-multi-cloud-applications/){:target="_blank" rel="noopener"}

> At Google Cloud, we are committed to supporting our customers who want to use applications hosted outside of Google Cloud. In 2022, we were named the Overall Leader in the 2022 KuppingerCole Zero Trust Network Access Leadership Compass, in part because we introduced the BeyondCorp Enterprise app connector. The app connector can help customers provide Zero Trust access to applications in multi-cloud environments. To help make it easier for administrators to connect and configure applications hosted outside Google Cloud, our enhanced BeyondCorp Enterprise app onboarding experience includes a new step-by-step workflow to onboard web applications and auto-provision load balancers and [...]
