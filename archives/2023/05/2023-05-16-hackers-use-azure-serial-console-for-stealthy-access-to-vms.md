Title: Hackers use Azure Serial Console for stealthy access to VMs
Date: 2023-05-16T20:57:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-05-16-hackers-use-azure-serial-console-for-stealthy-access-to-vms

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-azure-serial-console-for-stealthy-access-to-vms/){:target="_blank" rel="noopener"}

> A financially motivated cybergang tracked by Mandiant as 'UNC3944' is using phishing and SIM swapping attacks to hijack Microsoft Azure admin accounts and gain access to virtual machines. [...]
