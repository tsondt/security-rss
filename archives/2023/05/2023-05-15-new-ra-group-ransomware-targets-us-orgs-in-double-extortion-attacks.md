Title: New RA Group ransomware targets U.S. orgs in double-extortion attacks
Date: 2023-05-15T10:27:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-15-new-ra-group-ransomware-targets-us-orgs-in-double-extortion-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-ra-group-ransomware-targets-us-orgs-in-double-extortion-attacks/){:target="_blank" rel="noopener"}

> A new ransomware group named 'RA Group' is targeting pharmaceutical, insurance, wealth management, and manufacturing firms in the United States and South Korea. [...]
