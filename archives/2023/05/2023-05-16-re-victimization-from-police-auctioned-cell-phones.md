Title: Re-Victimization from Police-Auctioned Cell Phones
Date: 2023-05-16T12:20:58+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Dave Levin;Graykey;Propertyroom.com;University of Maryland
Slug: 2023-05-16-re-victimization-from-police-auctioned-cell-phones

[Source](https://krebsonsecurity.com/2023/05/re-victimization-from-police-auctioned-cell-phones/){:target="_blank" rel="noopener"}

> Countless smartphones seized in arrests and searches by police forces across the United States are being auctioned online without first having the data on them erased, a practice that can lead to crime victims being re-victimized, a new study found. In response, the largest online marketplace for items seized in U.S. law enforcement investigations says it now ensures that all phones sold through its platform will be data-wiped prior to auction. Researchers at the University of Maryland last year purchased 228 smartphones sold “as-is” from PropertyRoom.com, which bills itself as the largest auction house for police departments in the United [...]
