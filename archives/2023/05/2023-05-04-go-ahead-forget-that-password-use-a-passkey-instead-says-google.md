Title: Go ahead, forget that password. Use a passkey instead, says Google
Date: 2023-05-04T00:32:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-04-go-ahead-forget-that-password-use-a-passkey-instead-says-google

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/04/google_passkey/){:target="_blank" rel="noopener"}

> 'But they're gonna take my thumbs' hits different in 2023 Google wants to take us further into a passwordless future by allowing personal account holders to login using passkeys rather than using passphrases and multifactor authentication (MFA).... [...]
