Title: New York county still dealing with ransomware eight months after attack
Date: 2023-05-29T06:30:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-05-29-new-york-county-still-dealing-with-ransomware-eight-months-after-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/29/security_in_brief/){:target="_blank" rel="noopener"}

> Also: iSpoof no more, Edmodo fined more than it can pay, UK is #1 (in CC theft), and the week's critical vulns security in brief The fallout from an eight-month-old cyber attack on a county in Long Island, New York has devolved into mud-slinging as leaders try to figure out just what is going on.... [...]
