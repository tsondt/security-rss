Title: Six million patients' data feared stolen from PharMerica
Date: 2023-05-18T00:20:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-18-six-million-patients-data-feared-stolen-from-pharmerica

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/18/pharmerica_data_breach/){:target="_blank" rel="noopener"}

> Cue the inevitable class action lawsuit PharMerica, one of the largest pharmacy service providers in the US, has revealed its IT systems were breached – and it's feared the intruders stole personal and healthcare data belonging to more than 5.8 million past customers... [...]
