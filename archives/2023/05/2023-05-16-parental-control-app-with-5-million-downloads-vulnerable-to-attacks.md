Title: Parental control app with 5 million downloads vulnerable to attacks
Date: 2023-05-16T09:44:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-05-16-parental-control-app-with-5-million-downloads-vulnerable-to-attacks

[Source](https://www.bleepingcomputer.com/news/security/parental-control-app-with-5-million-downloads-vulnerable-to-attacks/){:target="_blank" rel="noopener"}

> Kiddowares 'Parental Control - Kids Place' app for Android is impacted by multiple vulnerabilities that could enable attackers to upload arbitrary files on protected devices, steal user credentials, and allow children to bypass restrictions without the parents noticing. [...]
