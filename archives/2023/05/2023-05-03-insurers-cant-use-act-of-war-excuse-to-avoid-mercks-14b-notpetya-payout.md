Title: Insurers can't use 'act of war' excuse to avoid Merck's $1.4B NotPetya payout
Date: 2023-05-03T21:22:31+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-03-insurers-cant-use-act-of-war-excuse-to-avoid-mercks-14b-notpetya-payout

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/03/merck_14bn_insurance_payout_upheld/){:target="_blank" rel="noopener"}

> 'The get-out-of-jail-free card option has been removed' as one expert put it Merck's insurers can't use an "act of war" clause to deny the pharmaceutical giant an enormous payout to clean up its NotPetya infection, a court has ruled.... [...]
