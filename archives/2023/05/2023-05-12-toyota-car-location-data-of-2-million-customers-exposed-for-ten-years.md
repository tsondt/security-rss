Title: Toyota: Car location data of 2 million customers exposed for ten years
Date: 2023-05-12T10:50:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-12-toyota-car-location-data-of-2-million-customers-exposed-for-ten-years

[Source](https://www.bleepingcomputer.com/news/security/toyota-car-location-data-of-2-million-customers-exposed-for-ten-years/){:target="_blank" rel="noopener"}

> Toyota Motor Corporation disclosed a data breach on its cloud environment that exposed the car-location information of 2,150,000 customers for ten years, between November 6, 2013, and April 17, 2023. [...]
