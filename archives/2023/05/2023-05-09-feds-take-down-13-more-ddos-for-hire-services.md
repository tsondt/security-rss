Title: Feds Take Down 13 More DDoS-for-Hire Services
Date: 2023-05-09T14:05:44+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: DDoS-for-Hire;Ne'er-Do-Well News;Angel Manuel Colon Jr.;booters;Cambridge University Cybercrime Center;Cory Anthony Palmer;ddos-for-hire;fbi;Jeremiah Sam Evans Miller;John M. Dobbs;Matthew Gatrel;Richard Clayton;Shamar Shattock;stressers;U.S. Department of Justice
Slug: 2023-05-09-feds-take-down-13-more-ddos-for-hire-services

[Source](https://krebsonsecurity.com/2023/05/feds-take-down-13-more-ddos-for-hire-services/){:target="_blank" rel="noopener"}

> The U.S. Federal Bureau of Investigation (FBI) this week seized 13 domain names connected to “ booter ” services that let paying customers launch crippling distributed denial-of-service (DDoS) attacks. Ten of the domains are reincarnations of DDoS-for-hire services the FBI seized in December 2022, when it charged six U.S. men with computer crimes for allegedly operating booters. Booter services are advertised through a variety of methods, including Dark Web forums, chat platforms and even youtube.com. They accept payment via PayPal, Google Wallet, and/or cryptocurrencies, and subscriptions can range in price from just a few dollars to several hundred per month. [...]
