Title: 1Password explains scary Secret Key and password change alerts
Date: 2023-05-02T16:29:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-02-1password-explains-scary-secret-key-and-password-change-alerts

[Source](https://www.bleepingcomputer.com/news/security/1password-explains-scary-secret-key-and-password-change-alerts/){:target="_blank" rel="noopener"}

> 1Password says a recent incident that caused customers to receive notifications about changed passwords was the result of service disruption and not a security breach. [...]
