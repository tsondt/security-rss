Title: Twitter adds new DM features, and Musk says E2EE is here, starting today
Date: 2023-05-10T16:55:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-05-10-twitter-adds-new-dm-features-and-musk-says-e2ee-is-here-starting-today

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/10/twitter_adds_new_dm_features/){:target="_blank" rel="noopener"}

> We'll believe our DMs are encrypted when someone provides proof, thanks Updated Twitter has rolled out some quality of life updates for direct messages on the platform, and CEO Elon Musk reckons the site is to start encrypting DMs, beginning today, without providing proof that's the case.... [...]
