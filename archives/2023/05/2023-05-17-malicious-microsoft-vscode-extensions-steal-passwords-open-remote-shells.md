Title: Malicious Microsoft VSCode extensions steal passwords, open remote shells
Date: 2023-05-17T12:37:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-05-17-malicious-microsoft-vscode-extensions-steal-passwords-open-remote-shells

[Source](https://www.bleepingcomputer.com/news/security/malicious-microsoft-vscode-extensions-steal-passwords-open-remote-shells/){:target="_blank" rel="noopener"}

> Cybercriminals are starting to target Microsoft's VSCode Marketplace, uploading three malicious Visual Studio extensions that Windows developers downloaded 46,600 times. [...]
