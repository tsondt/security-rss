Title: Don't panic. Google offering scary .zip and .mov domains is not the end of the world
Date: 2023-05-17T09:22:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-05-17-dont-panic-google-offering-scary-zip-and-mov-domains-is-not-the-end-of-the-world

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/17/google_zip_mov_domains/){:target="_blank" rel="noopener"}

> Did we forget about.pl,.sh and oh yeah,.com ? Comment In early May, Google Domains added support for eight new top-level domains, two of which –.zip, and.mov – raised the hackles of the security community.... [...]
