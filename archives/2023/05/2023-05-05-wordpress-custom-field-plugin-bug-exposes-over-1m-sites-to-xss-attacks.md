Title: WordPress custom field plugin bug exposes over 1M sites to XSS attacks
Date: 2023-05-05T10:57:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-05-wordpress-custom-field-plugin-bug-exposes-over-1m-sites-to-xss-attacks

[Source](https://www.bleepingcomputer.com/news/security/wordpress-custom-field-plugin-bug-exposes-over-1m-sites-to-xss-attacks/){:target="_blank" rel="noopener"}

> Security researchers warn that the 'Advanced Custom Fields' and 'Advanced Custom Fields Pro' WordPress plugins, with millions of installs, are vulnerable to cross-site scripting attacks (XSS). [...]
