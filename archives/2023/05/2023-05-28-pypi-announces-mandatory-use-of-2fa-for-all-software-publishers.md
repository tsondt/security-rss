Title: PyPI announces mandatory use of 2FA for all software publishers
Date: 2023-05-28T10:09:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-28-pypi-announces-mandatory-use-of-2fa-for-all-software-publishers

[Source](https://www.bleepingcomputer.com/news/security/pypi-announces-mandatory-use-of-2fa-for-all-software-publishers/){:target="_blank" rel="noopener"}

> The Python Package Index (PyPI) has announced that it will require every account that manages a project on the platform to have two-factor authentication (2FA) turned on by the end of the year. [...]
