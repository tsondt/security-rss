Title: Toyota's bungling of customer privacy is becoming a pattern
Date: 2023-05-15T02:26:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-05-15-toyotas-bungling-of-customer-privacy-is-becoming-a-pattern

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/15/toyota_bungles_privacy_of_japanese/){:target="_blank" rel="noopener"}

> Also: 3D printing gun mods = jail time; France fines Clearview AI for ignoring fine; this week's critical vulns, and more in brief Japanese automaker Toyota has admitted yet again to mishandling customer data – this time saying it exposed information on more than two million Japanese customers for the past decade, thanks to a misconfigured cloud environment.... [...]
