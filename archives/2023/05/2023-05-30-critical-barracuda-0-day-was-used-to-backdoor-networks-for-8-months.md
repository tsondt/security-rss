Title: Critical Barracuda 0-day was used to backdoor networks for 8 months
Date: 2023-05-30T23:58:34+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;backdoor;Barracuda;email;zeroday
Slug: 2023-05-30-critical-barracuda-0-day-was-used-to-backdoor-networks-for-8-months

[Source](https://arstechnica.com/?p=1943076){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) A critical vulnerability patched 10 days ago in widely used email software from IT security company Barracuda Networks has been under active exploitation since October. The vulnerability has been used to install multiple pieces of malware inside large organization networks and steal data, Barracuda said Tuesday. The software bug, tracked as CVE-2023-2868, is a remote command injection vulnerability that stems from incomplete input validation of user-supplied.tar files, which are used to pack or archive multiple files. When file names are formatted in a particular way, an attacker can execute system commands through the QX operator, [...]
