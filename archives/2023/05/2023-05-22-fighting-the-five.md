Title: Fighting the five
Date: 2023-05-22T10:36:55+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-05-22-fighting-the-five

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/22/fighting_the_five/){:target="_blank" rel="noopener"}

> Hear SANS cyber security experts share advice on how to defend your organization against the latest threats Sponsored Post Cyber criminals never stop learning so nor should you. Fresh security hacks are being concocted and deployed every week, so it's a good idea for cyber security professionals to pool their knowledge when working out how best to defend against them.... [...]
