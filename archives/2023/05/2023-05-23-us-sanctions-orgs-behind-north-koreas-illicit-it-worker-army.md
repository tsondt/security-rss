Title: US sanctions orgs behind North Korea’s ‘illicit’ IT worker army
Date: 2023-05-23T12:38:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-23-us-sanctions-orgs-behind-north-koreas-illicit-it-worker-army

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-orgs-behind-north-koreas-illicit-it-worker-army/){:target="_blank" rel="noopener"}

> The Treasury Department's Office of Foreign Assets Control (OFAC) announced sanctions today against four entities and one individual for their involvement in illicit IT worker schemes and cyberattacks generating revenue to finance North Korea's weapons development programs. [...]
