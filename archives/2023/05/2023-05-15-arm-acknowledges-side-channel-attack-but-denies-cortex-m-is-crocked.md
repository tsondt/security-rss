Title: Arm acknowledges side-channel attack but denies Cortex-M is crocked
Date: 2023-05-15T05:36:03+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-05-15-arm-acknowledges-side-channel-attack-but-denies-cortex-m-is-crocked

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/15/mcu_side_channel_attack/){:target="_blank" rel="noopener"}

> Spectre-esque exploit figures out when interesting info might be in memory Black Hat Asia Arm issued a statement last Friday declaring that a successful side attack on its TrustZone-enabled Cortex-M based systems was "not a failure of the protection offered by the architecture."... [...]
