Title: Five Eyes and Microsoft accuse China of attacking US infrastructure again
Date: 2023-05-25T03:30:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-05-25-five-eyes-and-microsoft-accuse-china-of-attacking-us-infrastructure-again

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/25/china_volt_typhoon_attacks/){:target="_blank" rel="noopener"}

> Defeating Volt Typhoon will be hard, because the attacks look like legit Windows admin activity China has attacked critical infrastructure organizations in the US using a "living off the land" attack that hides offensive action among everyday Windows admin activity.... [...]
