Title: China labels USA 'Empire of hacking' based on old Wikileaks dumps
Date: 2023-05-05T02:32:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-05-05-china-labels-usa-empire-of-hacking-based-on-old-wikileaks-dumps

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/05/china_labels_us_hacking_empire/){:target="_blank" rel="noopener"}

> Pot, meet kettle, both containing weak sauce The National Computer Virus Emergency Response Center of China and local infosec outfit 360 Total Security have conducted an investigation called "The Matrix" that found the CIA conducts offensive cyber ops, and labelled the United States an "Empire of Hacking".... [...]
