Title: Promising Jobs at the U.S. Postal Service, ‘US Job Services’ Leaks Customer Data
Date: 2023-05-02T22:08:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Latest Warnings;Farmer's Market;federaljobscenter.com;Gary Plott;Muhammed Tabish Mirza;Nextlevelsupportcenters;Postal Career Placement LLC;Postal Job Services Inc.;Postal Operations Inc.;Russell Ramage;Ryan Rawls;Smart Logistics;Stephanie Dayton;tab.webcoder@gmail.com;U.S. Federal Trade Commission;United States Postal Service;US Job Services;USPS;USPS jobs
Slug: 2023-05-02-promising-jobs-at-the-us-postal-service-us-job-services-leaks-customer-data

[Source](https://krebsonsecurity.com/2023/05/promising-jobs-at-the-u-s-postal-service-us-job-services-leaks-customer-data/){:target="_blank" rel="noopener"}

> A sprawling online company based in Georgia that has made tens of millions of dollars purporting to sell access to jobs at the United States Postal Service (USPS) has exposed its internal IT operations and database of nearly 900,000 customers. The leaked records indicate the network’s chief technology officer in Pakistan has been hacked for the past year, and that the entire operation was created by the principals of a Tennessee-based telemarketing firm that has promoted USPS employment websites since 2016. The website FederalJobsCenter promises to get you a job at the USPS in 30 days or your money back. [...]
