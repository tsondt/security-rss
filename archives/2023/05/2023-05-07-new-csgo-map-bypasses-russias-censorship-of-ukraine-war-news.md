Title: New CS:GO map bypasses Russia's censorship of Ukraine war news
Date: 2023-05-07T10:16:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-07-new-csgo-map-bypasses-russias-censorship-of-ukraine-war-news

[Source](https://www.bleepingcomputer.com/news/security/new-cs-go-map-bypasses-russias-censorship-of-ukraine-war-news/){:target="_blank" rel="noopener"}

> Finish newspaper Helsinin Sanomat has created a custom Counter-Strike: Global Offensive (CS:GO) map explicitly made to bypass Russian news censorship and smuggle information about the war in Ukraine to Russian players. [...]
