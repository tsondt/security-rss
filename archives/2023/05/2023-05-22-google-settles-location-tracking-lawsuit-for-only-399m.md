Title: Google settles location tracking lawsuit for only $39.9M
Date: 2023-05-22T14:45:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-05-22-google-settles-location-tracking-lawsuit-for-only-399m

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/22/google_gets_another_great_deal/){:target="_blank" rel="noopener"}

> Also, more OEM Android malware, Google's bug reports (mostly) ditch CVEs, and this week's critical vulns in brief Google has settled another location tracking lawsuit, yet again being fined a relative pittance.... [...]
