Title: Capita looking at a bill of £20M over breach clean-up costs
Date: 2023-05-10T11:00:50+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-05-10-capita-looking-at-a-bill-of-20m-over-breach-clean-up-costs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/10/capita_breach_costs/){:target="_blank" rel="noopener"}

> Analyst says expense 'no small drop in ocean' but reputational damage could be 'far greater' Britain's leaky outsourcing behemoth Capita is warning investors that the clean-up bill for its recent digital break-in will cost up to £20 million ($25.24 million).... [...]
