Title: Ransomware-as-a-service groups rain money on their affiliates
Date: 2023-05-17T01:58:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-05-17-ransomware-as-a-service-groups-rain-money-on-their-affiliates

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/17/ransomware_affiliates_money/){:target="_blank" rel="noopener"}

> Qilin gang crims can earn up to 85 percent of extortion cash, or jail Business is very good for affiliates of the Qilin ransomware-as-a-service (RaaS) group, which is very bad for the rest of us.... [...]
