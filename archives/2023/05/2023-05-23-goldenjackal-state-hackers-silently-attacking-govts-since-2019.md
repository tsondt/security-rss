Title: GoldenJackal state hackers silently attacking govts since 2019
Date: 2023-05-23T18:53:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-05-23-goldenjackal-state-hackers-silently-attacking-govts-since-2019

[Source](https://www.bleepingcomputer.com/news/security/goldenjackal-state-hackers-silently-attacking-govts-since-2019/){:target="_blank" rel="noopener"}

> A relatively unknown advanced persistent threat (APT) group named 'GoldenJackal' has been targeting government and diplomatic entities in Asia since 2019 for espionage. [...]
