Title: Google adds account sync for Authenticator, without E2EE
Date: 2023-05-01T11:04:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-05-01-google-adds-account-sync-for-authenticator-without-e2ee

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/01/google_adds_account_sync_for/){:target="_blank" rel="noopener"}

> Also: Your Salesforce Community site might be leaking; a new CPU side-channel; and this week's critical vunls in brief You may have heard news this week that Google is finally updating its authenticator app to add Google account synchronization. Before you rush to ensure your two-factor secrets are safe in the event you lose your device, take heed: The sync process isn't end-to-end encrypted.... [...]
