Title: ALPHV gang claims ransomware attack on Constellation Software
Date: 2023-05-05T11:04:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-05-05-alphv-gang-claims-ransomware-attack-on-constellation-software

[Source](https://www.bleepingcomputer.com/news/security/alphv-gang-claims-ransomware-attack-on-constellation-software/){:target="_blank" rel="noopener"}

> Canadian diversified software company Constellation Software confirmed on Thursday that some of its systems were breached by threat actors who also stole personal information and business data. [...]
