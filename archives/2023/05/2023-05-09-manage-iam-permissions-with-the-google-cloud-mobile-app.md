Title: Manage IAM permissions with the Google Cloud mobile app
Date: 2023-05-09T16:00:00+00:00
Author: Jolene Teo
Category: GCP Security
Tags: Security & Identity
Slug: 2023-05-09-manage-iam-permissions-with-the-google-cloud-mobile-app

[Source](https://cloud.google.com/blog/products/identity-security/manage-iam-permissions-with-the-google-cloud-mobile-app/){:target="_blank" rel="noopener"}

> What’s new with Permissions Management on the Cloud Mobile App Identity and Access Management (IAM) is the foundation of a strong cloud security posture, ensuring that the right access and permissions for cloud resources are granted across your organization. The Google Cloud mobile app gives cloud administrators the ability to quickly and easily manage their organization’s cloud identities and access from the mobile platform of their choice. Permissions management is one of the top user-requested features for the Cloud mobile app based on feedback we’ve received. The Permissions tab is used by more than half of our mobile users every [...]
