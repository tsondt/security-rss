Title: Faster AWS cloud connections with TLS 1.3
Date: 2023-05-23T18:49:23+00:00
Author: Kate Rodgers
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;FedRAMP;Security Blog;TLS;Transport Layer Security
Slug: 2023-05-23-faster-aws-cloud-connections-with-tls-13

[Source](https://aws.amazon.com/blogs/security/faster-aws-cloud-connections-with-tls-1-3/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we strive to continuously improve customer experience by delivering a cloud computing environment that supports the most modern security technologies. To improve the overall performance of your connections, we have already started to enable TLS version 1.3 globally across our AWS service API endpoints, and will complete this process by [...]
