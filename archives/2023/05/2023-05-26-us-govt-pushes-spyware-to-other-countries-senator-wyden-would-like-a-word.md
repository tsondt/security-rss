Title: US govt pushes spyware to other countries? Senator Wyden would like a word
Date: 2023-05-26T21:03:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-05-26-us-govt-pushes-spyware-to-other-countries-senator-wyden-would-like-a-word

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/26/wyden_ita_spyware_policy/){:target="_blank" rel="noopener"}

> Uncle Sam confirms it's saying nothing The US International Trade Administration (ITA) has admitted it promotes the sale of American-approved commercial spyware to foreign governments, and won't answer questions about it, according to US Senator Ron Wyden (D-OR).... [...]
