Title: IT security analyst admits hijacking cyber attack to pocket ransom payments
Date: 2023-05-24T08:30:10+00:00
Author: Richard Currie
Category: The Register
Tags: 
Slug: 2023-05-24-it-security-analyst-admits-hijacking-cyber-attack-to-pocket-ransom-payments

[Source](https://go.theregister.com/feed/www.theregister.com/2023/05/24/it_security_analyst_blackmail/){:target="_blank" rel="noopener"}

> Ashley Liles altered blackmail emails in bid to make off with £300,000 in Bitcoin A former IT security analyst at Oxford Biomedica has admitted, five years after the fact, to turning to the dark side – by hijacking a cyber attack against his own company in an attempt to divert any ransom payments to himself.... [...]
