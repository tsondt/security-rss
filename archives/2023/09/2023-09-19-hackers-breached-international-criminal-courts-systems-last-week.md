Title: Hackers breached International Criminal Court’s systems last week
Date: 2023-09-19T16:24:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-19-hackers-breached-international-criminal-courts-systems-last-week

[Source](https://www.bleepingcomputer.com/news/security/hackers-breached-international-criminal-courts-systems-last-week/){:target="_blank" rel="noopener"}

> The International Criminal Court (ICC) disclosed a cyberattack on Tuesday after discovering last week that its systems had been breached. [...]
