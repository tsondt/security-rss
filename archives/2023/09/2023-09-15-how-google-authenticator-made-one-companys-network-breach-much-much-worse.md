Title: How Google Authenticator made one company’s network breach much, much worse
Date: 2023-09-15T17:20:35+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Google;Security;google authenticator
Slug: 2023-09-15-how-google-authenticator-made-one-companys-network-breach-much-much-worse

[Source](https://arstechnica.com/?p=1968685){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) A security company is calling out a feature in Google’s authenticator app that it says made a recent internal network breach much worse. Retool, which helps customers secure their software development platforms, made the criticism on Wednesday in a post disclosing a compromise of its customer support system. The breach gave the attackers responsible access to the accounts of 27 customers, all in the cryptocurrency industry. The attack started when a Retool employee clicked a link in a text message purporting to come from a member of the company’s IT team. “Dark patterns” It warned [...]
