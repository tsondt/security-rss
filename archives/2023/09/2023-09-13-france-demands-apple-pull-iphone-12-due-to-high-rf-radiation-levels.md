Title: France demands Apple pull iPhone 12 due to high RF radiation levels
Date: 2023-09-13T13:41:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2023-09-13-france-demands-apple-pull-iphone-12-due-to-high-rf-radiation-levels

[Source](https://www.bleepingcomputer.com/news/security/france-demands-apple-pull-iphone-12-due-to-high-rf-radiation-levels/){:target="_blank" rel="noopener"}

> The Agence Nationale des Fréquences (ANFR) has asked Apple to withdraw iPhone 12 smartphones from the French market because the device emits radiofrequency energy that is beyond the limit permitted to be absorbed by the human body. [...]
