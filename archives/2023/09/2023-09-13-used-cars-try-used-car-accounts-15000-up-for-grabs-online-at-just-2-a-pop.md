Title: Used cars? Try used car accounts: 15,000 up for grabs online at just $2 a pop
Date: 2023-09-13T12:15:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-13-used-cars-try-used-car-accounts-15000-up-for-grabs-online-at-just-2-a-pop

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/13/car_account_theft/){:target="_blank" rel="noopener"}

> Cut and shut is so last century, now it's copy and clone Researchers have found almost 15,000 automotive accounts for sale online and pointed at a credential-stuffing attack that targeted car makers.... [...]
