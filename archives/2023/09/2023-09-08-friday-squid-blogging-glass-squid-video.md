Title: Friday Squid Blogging: Glass Squid Video
Date: 2023-09-08T21:03:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-09-08-friday-squid-blogging-glass-squid-video

[Source](https://www.schneier.com/blog/archives/2023/09/friday-squid-blogging-glass-squid-video.html){:target="_blank" rel="noopener"}

> Here’s a fantastic video of Taonius Borealis, a glass squid, from NOAA. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
