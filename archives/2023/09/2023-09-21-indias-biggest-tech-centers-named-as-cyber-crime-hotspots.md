Title: India's biggest tech centers named as cyber crime hotspots
Date: 2023-09-21T06:57:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-09-21-indias-biggest-tech-centers-named-as-cyber-crime-hotspots

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/21/india_cybercrime_trends_report/){:target="_blank" rel="noopener"}

> Global tech companies' Bharat offices attract the wrong sort of interest India is grappling with a three-and-a-half year surge in cyber crime, with analysis suggesting cities like Bengaluru and Gurugram – centers of India's tech development – are hubs of this activity.... [...]
