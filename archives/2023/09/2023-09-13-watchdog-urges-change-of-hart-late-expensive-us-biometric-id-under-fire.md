Title: Watchdog urges change of HART: Late, expensive US biometric ID under fire
Date: 2023-09-13T20:00:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-09-13-watchdog-urges-change-of-hart-late-expensive-us-biometric-id-under-fire

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/13/federal_watchdog_hart_biometrics/){:target="_blank" rel="noopener"}

> Homeland Security told to mind costs, fix up privacy controls Twice delayed and over budget, the US Department of Homeland Security (DHS) has been told by the Government Accountability Office (GAO) that it needs to correct shortcomings in its biometric identification program.... [...]
