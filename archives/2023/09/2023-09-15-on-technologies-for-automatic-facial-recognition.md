Title: On Technologies for Automatic Facial Recognition
Date: 2023-09-15T11:15:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;biometrics;face recognition;identification
Slug: 2023-09-15-on-technologies-for-automatic-facial-recognition

[Source](https://www.schneier.com/blog/archives/2023/09/on-technologies-for-automatic-facial-recognition.html){:target="_blank" rel="noopener"}

> Interesting article on technologies that will automatically identify people: With technology like that on Mr. Leyvand’s head, Facebook could prevent users from ever forgetting a colleague’s name, give a reminder at a cocktail party that an acquaintance had kids to ask about or help find someone at a crowded conference. However, six years later, the company now known as Meta has not released a version of that product and Mr. Leyvand has departed for Apple to work on its Vision Pro augmented reality glasses. The technology is here. Maybe the implementation is still dorky, but that will change. The social [...]
