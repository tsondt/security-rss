Title: Mixin suspends deposits and withdrawals after $200m cryptocurrency heist
Date: 2023-09-25T18:34:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-25-mixin-suspends-deposits-and-withdrawals-after-200m-cryptocurrency-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/25/mixin_200m_heist/){:target="_blank" rel="noopener"}

> Cloud provider blamed for loss of 20% of exchange's capital Mixin Network confirmd on Monday that it has "temporarily suspended" all deposit and withdrawal services after hackers broke into a database and stole about $200 million in funds from the Hong-Kong based cryptocurrency firm.... [...]
