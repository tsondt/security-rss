Title: Security researcher stopped at US border for investigating crypto scam
Date: 2023-09-28T10:52:59-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-09-28-security-researcher-stopped-at-us-border-for-investigating-crypto-scam

[Source](https://www.bleepingcomputer.com/news/security/security-researcher-stopped-at-us-border-for-investigating-crypto-scam/){:target="_blank" rel="noopener"}

> Security researcher Sam Curry describes a stressful situation he encountered upon his return to the U.S. when border officials and federal agents seized and searched his electronic devices. Curry was further served with a 'Grand Jury' subpoena that demanded him to appear in court for testimony. [...]
