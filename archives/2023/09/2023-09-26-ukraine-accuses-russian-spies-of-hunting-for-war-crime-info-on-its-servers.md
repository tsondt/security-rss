Title: Ukraine accuses Russian spies of hunting for war-crime info on its servers
Date: 2023-09-26T08:00:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-26-ukraine-accuses-russian-spies-of-hunting-for-war-crime-info-on-its-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/26/ukraine_russian_war_crimes_report/){:target="_blank" rel="noopener"}

> Russian have shifted tactics in the first half of 2023, with mixed results The Ukrainian State Service of Special Communications and Information Protection (SSSCIP) has claimed that Russian cyberspies are targeting its servers looking for data about alleged Kremlin-backed war crimes.... [...]
