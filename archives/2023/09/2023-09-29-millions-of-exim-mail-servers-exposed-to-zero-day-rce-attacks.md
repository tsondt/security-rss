Title: Millions of Exim mail servers exposed to zero-day RCE attacks
Date: 2023-09-29T16:11:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-29-millions-of-exim-mail-servers-exposed-to-zero-day-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/millions-of-exim-mail-servers-exposed-to-zero-day-rce-attacks/){:target="_blank" rel="noopener"}

> A critical zero-day vulnerability in all versions of Exim mail transfer agent (MTA) software can let unauthenticated attackers gain remote code execution (RCE) on Internet-exposed servers. [...]
