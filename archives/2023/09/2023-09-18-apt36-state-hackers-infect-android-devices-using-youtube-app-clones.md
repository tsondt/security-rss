Title: APT36 state hackers infect Android devices using YouTube app clones
Date: 2023-09-18T18:06:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-18-apt36-state-hackers-infect-android-devices-using-youtube-app-clones

[Source](https://www.bleepingcomputer.com/news/security/apt36-state-hackers-infect-android-devices-using-youtube-app-clones/){:target="_blank" rel="noopener"}

> The APT36 hacking group, aka 'Transparent Tribe,' has been observed using at least three Android apps that mimic YouTube to infect devices with their signature remote access trojan (RAT), 'CapraRAT.' [...]
