Title: Pizza Hut Australia hack: data breach exposes customer information and order details
Date: 2023-09-20T06:58:09+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Australia news;Cybercrime;Data and computer security;Hacking
Slug: 2023-09-20-pizza-hut-australia-hack-data-breach-exposes-customer-information-and-order-details

[Source](https://www.theguardian.com/australia-news/2023/sep/20/pizza-hut-hack-australia-data-breach-passwords-information-leak){:target="_blank" rel="noopener"}

> Company says it believes about 193,000 customers are affected by the breach, which it spotted in early September Follow our Australia news live blog for latest updates Get our morning and afternoon news emails, free app or daily news podcast Pizza Hut’s Australian operations have been hit by a cyber-attack, the company says, with customer data including delivery addresses and order details stolen in the hack. In an email to customers on Wednesday, Pizza Hut Australia’s chief executive, Phil Reed, said the company became aware in early September that there had been “unauthorised third party” access to some of the [...]
