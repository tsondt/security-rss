Title: Chrome, Firefox and more caught with their WebP down, offer hasty patch-up
Date: 2023-09-12T15:00:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-12-chrome-firefox-and-more-caught-with-their-webp-down-offer-hasty-patch-up

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/12/chrome_browser_webp_exploit/){:target="_blank" rel="noopener"}

> Exploit observed in the wild against codec lib in browsers, apps Updated Google and Mozilla have rushed out a fix for a vulnerability within their browsers – Chrome and Firefox, respectively – noting an exploit already exists in the wild.... [...]
