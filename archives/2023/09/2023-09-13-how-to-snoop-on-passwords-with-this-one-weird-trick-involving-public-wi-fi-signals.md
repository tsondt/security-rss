Title: How to snoop on passwords with this one weird trick (involving public Wi-Fi signals)
Date: 2023-09-13T10:45:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-09-13-how-to-snoop-on-passwords-with-this-one-weird-trick-involving-public-wi-fi-signals

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/13/wifi_beamforming_side_channel_attack/){:target="_blank" rel="noopener"}

> Fun technique – but how practical is it? Some smart cookies at institutions in China and Singapore have devised a technique for reading keystrokes and pilfering passwords or passcodes from Wi-Fi-connected mobile devices on public networks, without any hardware hacking.... [...]
