Title: Scattered Spider traps 100+ victims in its web as it moves into ransomware
Date: 2023-09-15T21:25:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-15-scattered-spider-traps-100-victims-in-its-web-as-it-moves-into-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/15/scattered_spider_snares_100_victims/){:target="_blank" rel="noopener"}

> Mandiant warns casino raiders are doubling down on 'monetization strategies' Scattered Spider, the crew behind at least one of the recent Las Vegas casino IT security breaches, has already hit some 100 organizations during its so-far brief tenure in the cybercrime scene, according to Mandiant.... [...]
