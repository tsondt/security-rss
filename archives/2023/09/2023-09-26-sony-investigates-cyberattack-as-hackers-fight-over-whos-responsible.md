Title: Sony investigates cyberattack as hackers fight over who's responsible
Date: 2023-09-26T15:07:16-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-09-26-sony-investigates-cyberattack-as-hackers-fight-over-whos-responsible

[Source](https://www.bleepingcomputer.com/news/security/sony-investigates-cyberattack-as-hackers-fight-over-whos-responsible/){:target="_blank" rel="noopener"}

> Sony says that it is investigating allegations of a cyberattack this week as different hackers have stepped up to claim responsibility for the purported hack. Thus far, over 3.14 GB of uncompressed data, allegedly belonging to Sony, has been dumped on hacker forums. [...]
