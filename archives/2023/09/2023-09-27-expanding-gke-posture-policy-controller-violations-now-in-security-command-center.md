Title: Expanding GKE posture: Policy Controller violations now in Security Command Center
Date: 2023-09-27T16:42:19+00:00
Author: Tim Wingerter
Category: GCP Security
Tags: Application Modernization;Compliance;Containers & Kubernetes;Security & Identity
Slug: 2023-09-27-expanding-gke-posture-policy-controller-violations-now-in-security-command-center

[Source](https://cloud-blog-transform.corp.google.com/blog/products/identity-security/expanding-gke-posture-policy-controller-violations-now-in-security-command-center/){:target="_blank" rel="noopener"}

> Customers using Kubernetes at scale need consistent guardrails for how resources are used across their environments to improve security, resource management, and flexibility. Customers have told us that they need an easy way to apply and view those policy guardrails, so we launched the Policy Controller dashboard and added support for all GKE environments. We received further feedback from Security Administrators that policy and compliance violation reports for GKE should be available alongside security insights from across their Google Cloud estate. To address this, we are excited to announce a fully managed integration to surface Policy Controller (CIS Kubernetes Benchmark [...]
