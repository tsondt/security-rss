Title: Ransomware attack hits Sri Lanka government, causing data loss
Date: 2023-09-13T03:48:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-09-13-ransomware-attack-hits-sri-lanka-government-causing-data-loss

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/13/ransomware_attack_hits_sri_lanka/){:target="_blank" rel="noopener"}

> Running unsupported and unpatched versions of Exchange Server will do that to a country Sri Lanka's Computer Emergency Readiness Team (CERT) is currently investigating a ransomware attack on the government's cloud infrastructure that affected around 5,000 email accounts, it revealed on Tuesday.... [...]
