Title: A Closer Look at the Snatch Data Ransom Group
Date: 2023-09-30T19:47:57+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;Ransomware;CISA;Constella Intelligence;Databreaches.net;fbi;Flashpoint;Perchatka;Semen7907;Semyon Tretyakov;Snatch ransomware;Snatch Team;tretyakov-files@yandex.ru
Slug: 2023-09-30-a-closer-look-at-the-snatch-data-ransom-group

[Source](https://krebsonsecurity.com/2023/09/a-closer-look-at-the-snatch-data-ransom-group/){:target="_blank" rel="noopener"}

> Earlier this week, KrebsOnSecurity revealed that the darknet website for the Snatch ransomware group was leaking data about its users and the crime gang’s internal operations. Today, we’ll take a closer look at the history of Snatch, its alleged founder, and their claims that everyone has confused them with a different, older ransomware group by the same name. According to a September 20, 2023 joint advisory from the FBI and the U.S. Cybersecurity and Infrastructure Security Administratio n (CISA), Snatch was originally named Team Truniger, based on the nickname of the group’s founder and organizer — Truniger. The FBI/CISA report [...]
