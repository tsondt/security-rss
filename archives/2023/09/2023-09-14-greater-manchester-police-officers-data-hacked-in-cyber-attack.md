Title: Greater Manchester police officers’ data hacked in cyber-attack
Date: 2023-09-14T19:24:02+00:00
Author: Josh Halliday and Matthew Weaver
Category: The Guardian
Tags: Police;Greater Manchester;Manchester;Cybercrime;UK news;Data and computer security;Technology
Slug: 2023-09-14-greater-manchester-police-officers-data-hacked-in-cyber-attack

[Source](https://www.theguardian.com/uk-news/2023/sep/14/greater-manchester-police-officers-data-hacked-in-cyber-attack){:target="_blank" rel="noopener"}

> Details of thousands of officers may have been taken in ransomware attack on third-party supplier The personal details of tens of thousands of public sector workers could have been breached in a cyber-attack that has hit two of Britain’s biggest police forces, an expert has said. More than 12,500 Greater Manchester police (GMP) officers and staff were put on alert on Thursday that their private data had been compromised in a hack that also hit the Metropolitan police last month. Continue reading... [...]
