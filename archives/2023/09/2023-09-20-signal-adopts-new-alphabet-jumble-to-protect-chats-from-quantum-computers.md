Title: Signal adopts new alphabet jumble to protect chats from quantum computers
Date: 2023-09-20T20:28:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-09-20-signal-adopts-new-alphabet-jumble-to-protect-chats-from-quantum-computers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/20/signal_adopts_new_alphabet_jumble/){:target="_blank" rel="noopener"}

> X3DH readied for retirement as PQXDH is rolled out Signal has adopted a new key agreement protocol in an effort to keep encrypted Signal chat messages protected from any future quantum computers.... [...]
