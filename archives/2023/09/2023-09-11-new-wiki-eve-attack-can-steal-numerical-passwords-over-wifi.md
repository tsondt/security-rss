Title: New WiKI-Eve attack can steal numerical passwords over WiFi
Date: 2023-09-11T16:30:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-09-11-new-wiki-eve-attack-can-steal-numerical-passwords-over-wifi

[Source](https://www.bleepingcomputer.com/news/security/new-wiki-eve-attack-can-steal-numerical-passwords-over-wifi/){:target="_blank" rel="noopener"}

> A new attack dubbed 'WiKI-Eve' can intercept the cleartext transmissions of smartphones connected to modern WiFi routers and deduce individual numeric keystrokes at an accuracy rate of up to 90%, allowing numerical passwords to be stolen. [...]
