Title: US-Canada water org confirms 'cybersecurity incident' after ransomware crew threatens leak
Date: 2023-09-15T00:15:26+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-15-us-canada-water-org-confirms-cybersecurity-incident-after-ransomware-crew-threatens-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/15/ijc_noescape_ransomware/){:target="_blank" rel="noopener"}

> NoEscape promises 'colossal wave of problems' if IJC doesn't pay up The International Joint Commission, a body that manages water rights along the US-Canada border, has confirmed its IT security was targeted, after a ransomware gang claimed it stole 80GB of data from the organization.... [...]
