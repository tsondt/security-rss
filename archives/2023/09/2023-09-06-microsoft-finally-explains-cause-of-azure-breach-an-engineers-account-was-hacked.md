Title: Microsoft finally explains cause of Azure breach: An engineer’s account was hacked
Date: 2023-09-06T21:11:07+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;azure;microsoft;signing key;storm-0558
Slug: 2023-09-06-microsoft-finally-explains-cause-of-azure-breach-an-engineers-account-was-hacked

[Source](https://arstechnica.com/?p=1965985){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Microsoft said the corporate account of one of its engineers was hacked by a highly skilled threat actor that acquired a signing key used to hack dozens of Azure and Exchange accounts belonging to high-profile users. The disclosure solves two mysteries at the center of a disclosure Microsoft made in July. The company said that hackers tracked as Storm-0558 had been inside its corporate network for more than a month and had gained access to Azure and Exchange accounts, several of which were later identified as belonging to the US Departments of State and Commerce. Storm-0558 [...]
